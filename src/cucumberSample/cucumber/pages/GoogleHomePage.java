package cucumberSample.cucumber.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Browser;

public class GoogleHomePage {

    @FindBy(xpath = "//input[@id='lst-ib']")
    private WebElement clickOnGoogleMainSearchBox;

    @FindBy(xpath = "//*[@id='rso']//h3/a")
    private WebElement searchTextResult;

    public GoogleHomePage() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void clickOnGoogleMainSearchPage() {
        clickOnGoogleMainSearchBox.click();
    }

    public void typeTextInGoogleSearchMainPage(String text) {
        clickOnGoogleMainSearchBox.sendKeys(text);
    }

    public String waitForTextAfterSearchEntry() throws InterruptedException {
        clickOnGoogleMainSearchBox.sendKeys(Keys.ENTER);
        Thread.sleep(1000);
        return searchTextResult.getText();
    }
}
