package cucumberSample.resources;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberSample.cucumber.pages.GoogleHomePage;
import utils.Browser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GoogleHomePageStepDefs {

    private static String googleSampleBaseURL = "https://www.google.com";
    GoogleHomePage googleHomePage;

    @Given("^I am on the Google home page$")
    public void i_am_on_the_Google_main_page() throws Exception {
        Browser.openBrowser(googleSampleBaseURL);
    }

    @When("^I click on Google Main Page Search bar$")
    public void i_click_on_Google_Main_Page_Search_box() {
        googleHomePage = new GoogleHomePage();
        googleHomePage.clickOnGoogleMainSearchPage();
    }

    @Then("^I type Shane in the Google Main Search bar and select return$")
    public void type_text_in_main_Google_page_search_box() {
        googleHomePage.typeTextInGoogleSearchMainPage("Shane");
    }

    @Then("^I should see shane - YouTube text appear in Google search results$")
    public void i_should_see_text_appear_in_Google_page_search_box() throws InterruptedException {
        assertThat(googleHomePage.waitForTextAfterSearchEntry(), is("shane - YouTube"));
    }

    @Then("^Close Browser$")
    public void close_Browser() {
        Browser.closeBrowser();
    }

    @Given("^On the page itself$")
    public void on_the_page_itself() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
