package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.R360DatabaseAccess;
import r360.pages.Reports;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.FileOperations;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class Reports_User_Activity_Reports extends BaseTest {

	private String mainHandle;
	
	@BeforeClass(alwaysRun=true)
	public void setup() throws Exception {
		DataPrep();
		Reports.getToPage();
		mainHandle = Browser.driver.getWindowHandle();
	}
	
	private void DataPrep() throws Exception {
		int userID = R360DatabaseAccess.scalarInt(
				"Select Top(1) UserID\n" +
						"\t\tFrom RecHubUser.Users\n" +
						"\t\tWhere LogonName='AutoTester'");
		HashMap<String, String> rowData = new HashMap<String, String>() {{
			put("IsDeleted","0");
			put("AuditDateKey","20171220");
			put("UserID",Integer.toString(userID));
			put("WorkStationID","-1");
			put("AuditApplicationKey","3");
			put("AuditEventKey","3");
			put("AuditKey","{NEWGUID}");
			put("CreationDate","{GETDATE}");
			put("ModificationDate","12/20/2017 4:00:00 PM");
		}};
		R360DatabaseAccess.delete("WFSDB_R360.RecHubAudit.factEventAuditSummary", rowData, new String[] {"UserID", "AuditDateKey"});
		R360DatabaseAccess.insert("WFSDB_R360.RecHubAudit.factEventAuditSummary", rowData);
	}
	
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 10)
	public void PT12684_UserActivityReport() throws Exception {
		DataBuilder.delete_Files_WithExtention_FromLocation(".CSV", Browser.downloadLocation);
		Reports.expand_groupingHeader(Reports.receivables360_Online_ReportsText);
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, "User Activity Report");
		Reports.select_UserActivityReport_Values("CSV", "12/20/2017", "12/20/2017", "Auto Esther", "Page View");
		Reports.click_GenerateReport_Button();
		FileOperations.wait_For_File_To_Exist(Browser.downloadLocation + "\\" + Reports.userActivityReport_CSVFileName);
		TableData csvResults = Reports.returnCSV_Results(Reports.userActivityReport_CSVFileName);
		BaseUI.verify_true_AndLog(csvResults.rowCount() > 0, "Found rows in the User Activity Report CSV", "No data made it to the CSV File");
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		BaseUI.close_ExtraWindows(mainHandle);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
