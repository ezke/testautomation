package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.data.ImportData;
import r360.pages.*;
import utils.BaseUI;
import utils.Browser;
import utils.Is;
import utils.ResultWriter;

public class PreDepositExceptionsTests extends BaseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        ImportData.import_PreDepositExceptions_Data();
    }

    @BeforeMethod(alwaysRun = true)
    public void setupMethod() throws Exception {
        Navigation.navigate_Exceptions_PreDepositExceptions();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12366_Step5_VerifySummaryColumnsAndData() throws Exception {
        PreDepositExceptionsPage prdePage = new PreDepositExceptionsPage();
        prdePage.searchPreDeposit("7890001");
        prdePage.sortBy("Batch", true);
        BaseUI.assertThat(prdePage.GetGridHeaderList(), Is.equalTo(new String[]{
            "Entity",
            "Workgroup",
            "Batch",
            "Transaction Count",
            "Deadline",
            "Status",
            "User",
            "Deposit Date",
            "Process Date",
            "Payment Source"
        }));
        BaseUI.assertThat(prdePage.getGridItemText(1, "Transaction Count"), Is.equalTo("2"));
        BaseUI.assertThat(prdePage.getGridItemText(3, "Batch"), Is.equalTo("3"));
        BaseUI.assertThat(prdePage.getGridItemText(4, "Workgroup"), Is.equalTo("7890001 - Post Deposit Test Workgroup"));
        BaseUI.assertThat(prdePage.getGridItemText(2, "Entity"), Is.equalTo("WFS\\AutomationTest\\AutomationImports"));
        BaseUI.assertThat(prdePage.getGridItemText(1, "Payment Source"), Is.equalTo("integraPAY"));
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12366_Step7_VerifyBatchGridHoverMessage() throws Exception {
        PreDepositExceptionsPage prdePage = new PreDepositExceptionsPage();
        prdePage.searchPreDeposit("7890001");
        PreDepositExceptionsBatchDetailPage batchDetailPage = prdePage.clickGridItem(1, "Batch");
        BaseUI.assertThat(batchDetailPage.getGridHoverMessage(), Is.equalTo("Click here to view Exceptions Transaction Detail"));
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12366_Step9_VerifySaveButtonUponReject() throws Exception {
        PreDepositExceptionsPage prdePage = new PreDepositExceptionsPage();

        prdePage.searchPreDeposit("7890001");
        PreDepositExceptionsBatchDetailPage batchDetailPage = prdePage.clickGridItem(1, "Batch");
        batchDetailPage.lockBatch();
        PreDepositExceptionsTransactionDetailPage transactionDetailPage = batchDetailPage.clickGridItem(2, "RT");
        transactionDetailPage.rejectTransaction();
        transactionDetailPage.saveTransaction();
        batchDetailPage = transactionDetailPage.returnToBatchDetail();
        BaseUI.assertThat(batchDetailPage.getGridItemText(2, "Status"), Is.equalTo("Rejected"));
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
