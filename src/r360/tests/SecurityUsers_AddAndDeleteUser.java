package r360.tests;

import java.text.MessageFormat;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_AddAndDeleteUser extends BaseTest {

	private String newEntity = "entityAddAndDelete";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityAddAndDelete Description";
	private String entityExternalID = "entityAddAndDelete ID 616";
	String columnName = "Login Name";
	String loginName = "Testa123";

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

		Navigation.navigate_Admin_Entities();
		// if(BaseUI.pageSourceContainsString(newEntity)){
		// SecurityAdministration.deleteGroups_ForGivenEntity(newEntity);
		// SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
		// SecurityAdministration.navigate_toEntityTab();
		// SecurityAdministration.deleteEntity(newEntity);
		// }
		if (!BaseUI.pageSourceContainsString(newEntity)) {
			SecurityAdministration.launchAddEntityModal();
			SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		}
		
		SecurityAdministration.navigate_toUserTab();
		SecurityAdministration.selectNode(newEntity);
		
		if (BaseUI.pageSourceContainsString(loginName)) {
			SecurityAdministration.deleteUser(columnName, loginName);
		}
		
	}

	// Start of test

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void TC86099_entityAddUserCancelFunctionality() throws Exception {
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdminAddUser.enterLoginName(loginName);
		SecurityAdminAddUser.clickCancelButton();
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT6887_SecurityAdminUsersAddUser() throws Exception {
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser(loginName, "Wausau#1", "John", "Barett", "test@test.com");
		WebElement element = SecurityAdministration.userManagementGrid
				.get_row_ByColumnName_And_ColumnMatchingText(columnName, loginName);
		BaseUI.verify_true_AndLog(element != null, MessageFormat.format("Users {0} was Not deleted.", loginName),
				MessageFormat.format("Users {0} was deleted.", loginName));
		SecurityAdministration.deleteUser(columnName, loginName);

	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void TC86102_DeleteUser_NogroupsAssigned() throws Exception {
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser("TestAutoForDelete", "Wausau#1", "Test", "testauto", "test@test.com");
		SecurityAdministration.deleteUser(columnName, "TestAutoForDelete");
		WebElement element = SecurityAdministration.userManagementGrid
				.get_row_ByColumnName_And_ColumnMatchingText(columnName, "TestAutoForDelete");
		BaseUI.verify_true_AndLog(element == null, MessageFormat.format("Users {0} was deleted.", "TestAutoForDelete"),
				MessageFormat.format("Users {0} was NOT deleted.", "TestAutoForDelete"));
		Thread.sleep(3000);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void TC204579_DeleteUser_GroupsAssigned_VerifyError_RemoveGroupsAssigned_AndCanDeleteUser()
			throws Exception {
		// adding groups to User
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser("testauto", "Manage#1", "test", "testauto", "test@auto.com");
		SecurityAdmin_UserManageModal.navigateToGroupsTab();
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup("groupName");
		SecurityAdministration.navigate_toUserTab();

		// Moving selected group to associatedGrid
		SecurityAdministration.launchEditUserModal_forGivenUser("testauto");
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		SecurityAdmin_UserManageModal.click_saveButton();
		BaseUI.click(
				SecurityAdministration.userManagementGrid.check_box_for_GivenColumnAndText(columnName, "testauto"));
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserErrorPopup"));
		BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserDeleteButton"));
		Thread.sleep(2000);

		// Moving GroupsAssigned to availabeGrid
		SecurityAdministration.launchEditUserModal_forGivenUser("testauto");
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumn_NoResultsFound("Name");
		SecurityAdmin_UserManageModal.click_saveButton();
		SecurityAdministration.deleteUser(columnName, "testauto");

		WebElement element = SecurityAdministration.userManagementGrid
				.get_row_ByColumnName_And_ColumnMatchingText(columnName, "testauto");
		BaseUI.verify_true_AndLog(element == null, MessageFormat.format("Users {0} was deleted.", "testauto"),
				MessageFormat.format("Users {0} was NOT deleted.", "testauto"));
		Thread.sleep(2000);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void TC229846_DeleteUSer_NewAddedUser() throws Exception {
		SecurityAdministration.launchAddUserModal();
		SecurityAdmin_UserManageModal.addUser("testNewUser", "Wausau#1", "John", "Barett", "test@test.com");
		SecurityAdministration.deleteUser(columnName, "testNewUser");
		Pagination userGrid = new Pagination("userGrid");
		WebElement element = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, "testNewUser");
		BaseUI.verify_true_AndLog(element == null, MessageFormat.format("Users {0} was deleted.", "testNewUser"),
				MessageFormat.format("Users {0} was NOT deleted.", "testNewUser"));
		Thread.sleep(2000);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void TC258252_DeleteMultipleUsers_NoGroupsAssigned() throws Exception {
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser("testUser1", "Wausau#1", "Test1", "Barett", "test@test.com");
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser("testUser2", "Wausau#1", "Test2", "Grow", "test@test.com");
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser("testUser3", "Wausau#1", "Test3", "Brand", "test@test.com");
		Pagination userGrid = new Pagination("userGrid");
		userGrid.check_all_checkbox();
		BaseUI.verifyElementEnabled(Locator.lookupElement("entityUserDeleteButton"));
		Thread.sleep(2000);
		SecurityAdminAddUser.clickDeleteButton();
		Thread.sleep(1000);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserConfirmDeleteBox"));
		SecurityAdminAddUser.clickConfirmDeleteButton();
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		WebElement element1 = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, "testUser1");
		BaseUI.verify_true_AndLog(element1 == null, MessageFormat.format("Users {0} was deleted.", "testUser1"),
				MessageFormat.format("Users {0} was NOT deleted.", "testUser1"));

		WebElement element2 = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, "testUser2");
		BaseUI.verify_true_AndLog(element2 == null, MessageFormat.format("Users {0} was deleted.", "testUser2"),
				MessageFormat.format("Users {0} was NOT deleted.", "testUser2"));

		WebElement element3 = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, "testUser3");
		BaseUI.verify_true_AndLog(element3 == null, MessageFormat.format("Users {0} was deleted.", "testUser3"),
				MessageFormat.format("Users {0} was NOT deleted.", "testUser3"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserDeleteButton"));
		Thread.sleep(2000);
	}

	@AfterMethod(alwaysRun = true)
	public void teardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		// if(BaseUI.pageSourceContainsString(newEntity)){
		// SecurityAdministration.deleteGroups_ForGivenEntity(newEntity);
		// SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
		// SecurityAdministration.navigate_toEntityTab();
		// SecurityAdministration.deleteEntity(newEntity);
		// Thread.sleep(3000);
		// }
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
