package r360.tests;

import java.util.Date;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.NotificationsDetails;
import r360.pages.Notifications_Page;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class NotificationDetailTest extends BaseTest {
	private Date _startDate;
	private String _message = "FIT Simple File Drop Test";
	private String _attachmentFile;
	
	@BeforeClass(alwaysRun=true)
	public void setup() throws Exception {
		_startDate = new Date();
		_attachmentFile = NotificationsDetails.getAttachmentName(_startDate);
		ImportData.fit_SimpleFileDrop(_attachmentFile, _startDate, _message);
		Browser.openBrowser(GlobalVariables.environment.BaseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Notifications();
		Notifications_Page.enterAttachmentName(_attachmentFile);
		Notifications_Page.enterDateRange(_startDate, _startDate);
		Notifications_Page.clickSearch();
		Notifications_Page.clickFirstRow();
	}
	
	//Validating the Attachment File Name and the Download Button on the Notifications Detail Page
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT12506_checkNotificationDetailPage() throws Exception
	{
		TableData table = NotificationsDetails.getResults();
		BaseUI.baseStringCompare("File", _attachmentFile, table.dataRow(0).get("File"));
		BaseUI.verifyElementExists("notifications_Detail_Download_Icon", null, null);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
