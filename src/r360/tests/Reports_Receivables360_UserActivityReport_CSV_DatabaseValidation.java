package r360.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.Reports;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.FileOperations;
import utils.ResultWriter;
import utils.TableData;

public class Reports_Receivables360_UserActivityReport_CSV_DatabaseValidation extends BaseTest {

	// https://prod.practitest.com/p/5395/tests/974476/edit#steps_tab

	String mainHandle = "";
	String report = "User Activity Report";
	final String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
	final String databaseFormat_TodaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0,
			Reports.database_DateFormat);

	final String date1_WebFormat = "03/15/2018";
	final String date2_WebFormat = "03/18/2018";
	final String date1_DatabaseFormat = "20180315";
	final String date2_DatabaseFormat = "20180318";

	String dateCreated = "";
	String dateCreated_Format = "MM/dd/yyyy hh:mm a";

	TableData databaseData;
	TableData csvData;

	String eventType = "Uncategorized";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		DataBuilder.delete_Files_WithExtention_FromLocation(".CSV", Browser.downloadLocation);
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
		mainHandle = Browser.driver.getWindowHandle();

		Navigation.navigate_Reports();
		Reports.click_groupingHeader(Reports.receivables360_Online_ReportsText);
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, report);
		Reports.select_UserActivityReport_Values("CSV", date1_WebFormat, date2_WebFormat, null, eventType);

		dateCreated = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateCreated_Format);
		Reports.click_GenerateReport_Button();
		databaseData = Reports.return_auditInfo_ForDates(date1_DatabaseFormat, date2_DatabaseFormat, eventType);
		FileOperations.wait_For_File_To_Exist(Browser.downloadLocation + "\\" + Reports.userActivityReport_CSVFileName);
		csvData = Reports.returnCSV_Results(Reports.userActivityReport_CSVFileName);
		BaseUI.verify_true_AndLog(csvData.data.size() > 0, "Found CSV Data.", "Did NOT find CSV Data.");
		BaseUI.verify_true_AndLog(databaseData.data.size() > 0, "Found Database Data.", "Did NOT find Database Data.");
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_Entity_ColumnMatches() throws Throwable {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Entity", "WFS", csvData.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_DateRange_ColumnMatches() throws Throwable {
		String dateExpectedString = "Date Range: " + date1_WebFormat + " - " + date2_WebFormat;
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("DateRange", dateExpectedString, csvData.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_ReportCreated_ColumnMatches() throws Throwable {

		String actualDate = csvData.data.get(0).get("ReportCreated")
				.substring(15, csvData.data.get(0).get("ReportCreated").indexOf(" by")).trim();
		BaseUI.verify_Date_IsBetween_DateRange(
				BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(dateCreated, dateCreated_Format,
						dateCreated_Format, -1),
				BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(dateCreated, dateCreated_Format,
						dateCreated_Format, 1),
				actualDate, dateCreated_Format);
		String reportCreated = "Report created " + actualDate + " by " + GlobalVariables.login;

		BaseUI.verify_TableColumn_AllColumnValues_MatchString("ReportCreated", reportCreated, csvData.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_And_Database_AuditMessage_Matches() throws Throwable {
		BaseUI.verify_TableColumns_Match("AuditMessage", databaseData.data, csvData.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_And_Database_User_Matches() throws Throwable {
		BaseUI.verify_TableColumns_Match("User", databaseData.data, csvData.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_And_Database_AuditDateTime_Matches() throws Throwable {
		// BaseUI.verify_TableColumns_Match("AuditDateTime", databaseData.data,
		// csvData.data);
		BaseUI.verify_TableColumns_Match("CreationDate", "AuditDateTime", databaseData.data, csvData.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_And_Database_EventType_Matches_Uncategorized() throws Throwable {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("SelectedEventType", "Selected Event Type: " + eventType,
				csvData.data);
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("EventType", eventType, databaseData.data);
	}

	// Didn't have anything to compare this cell against so the validation is pretty
	// weak.  Validated that table is not empty in setup.
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_ActualEventType_NotEmpty() throws Throwable {
		for (HashMap<String, String> row : csvData.data) {
			BaseUI.verify_String_Not_EmptyOrNull(row.get("EventType").trim());
		}
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3110_Verify_CSV_SelectedUsers_MatchExpected() throws Throwable {
		String selectedUsers = "Selected User: WFS-All Users";
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("SelectedUsers", selectedUsers, csvData.data);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			BaseUI.close_ExtraWindows(mainHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
