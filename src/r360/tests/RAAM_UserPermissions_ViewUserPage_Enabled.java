package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class RAAM_UserPermissions_ViewUserPage_Enabled extends BaseTest {

	Pagination userManagementGrid;
	Pagination availableGroupsGrid;
	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	String userName = "AutomationTest";

	@BeforeClass (alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions,
				GlobalVariables.passsword_allPermissions);
		// Navigation.navigate_Admin_Users();
		// All tests should start at the same starting point.
		Navigation.navigate_Dashboard();
		userManagementGrid = new Pagination("userGrid");

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT8559_Raaam_SecurityAdmin_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT8559_Raaam_SecurityAdmin_UserOption_Listed() throws Throwable {
		Navigation.navigate_openMenu("Admin");
		BaseUI.verify_true_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "Users"),
				"Admin>Users menu item exists", "Could not find Admin>Users menu item.");
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Users"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT8559_Raaam_SecurityAdmin_UserOption_goesTo_AdminUsersPage_UserManagmentTitle_Displayed()
			throws Throwable {
		Navigation.navigate_openMenu("Admin");
		Navigation.navigate_Admin_Users();
		BaseUI.verifyElementAppearsByString("securityUsers_UsersManagementTitle");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT8559_Raaam_SecurityAdmin_UserOption_goesTo_AdminUsersPage_EntityTreeView_Displayed()
			throws Throwable {
		BaseUI.verifyElementAppearsByString("entityTree");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT8559_Raaam_SecurityAdmin_UserOption_goesTo_AdminUsersPage_userGrid_Displayed() throws Throwable {
		BaseUI.verifyElementAppearsByString("securityUsers_UserGrid");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 6)
	public void PT8559_Raaam_SecurityAdmin_UserOption_goesTo_AdminUsersPage_AddUserButton_Displayed() throws Throwable {
		BaseUI.verifyElementAppearsByString("entityUserAddButton");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 7)
	public void PT8559_Raaam_SecurityAdmin_UserOption_goesTo_AdminUsersPage_DeleteUserButton_Displayed()
			throws Throwable {
		BaseUI.verifyElementAppearsByString("entityUserDeleteButton");
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
