package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import r360.pages.*;
import utils.BaseUI;
import utils.Browser;
import utils.Is;
import utils.ResultWriter;

public class PostDepositExceptionsLockedTests extends BaseTest {
    private static final String SEARCH_STRING = "111";
    private static final String AUTO_DATA_SEARCH_STRING = "7890002";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
        cleanupExtraInvoice(SEARCH_STRING);
    }

    private void cleanupExtraInvoice(String searchString) throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage = postDepositExceptionsPage.searchPostDeposit(searchString);
        PostDepositExceptionsTransactionDetailPage postDepositExceptionsTransactionDetailPage = postDepositExceptionsPage.selectTransactionTwo();
        if(postDepositExceptionsTransactionDetailPage.getInvoiceCount()==5) {
            postDepositExceptionsTransactionDetailPage.deleteInvoice();
            postDepositExceptionsTransactionDetailPage = postDepositExceptionsTransactionDetailPage.confirmDeleteTransaction();
            postDepositExceptionsTransactionDetailPage.savePostDeposit();
        }
        postDepositExceptionsTransactionDetailPage.returnToPostDepositExceptionsSummary();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT11874_VerifyUnlockCancel() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage = postDepositExceptionsPage.searchPostDeposit(AUTO_DATA_SEARCH_STRING);
        postDepositExceptionsPage = postDepositExceptionsPage.sortByDepositDateHeading(false);
        PostDepositExceptionsTransactionDetailPage postDepositExceptionsTransactionDetailPage = postDepositExceptionsPage.selectTransactionTwo();
        postDepositExceptionsPage = postDepositExceptionsTransactionDetailPage.returnToPostDepositExceptionsSummary();
        postDepositExceptionsPage = postDepositExceptionsPage.searchPostDeposit(AUTO_DATA_SEARCH_STRING);
        postDepositExceptionsPage = postDepositExceptionsPage.sortByDepositDateHeading(false);
        postDepositExceptionsPage.clickLockOfRow(2);
        postDepositExceptionsPage.clickUnlockModalCancelButton();
        BaseUI.assertThat(postDepositExceptionsPage.isRowLocked(2), Is.equalTo(true));
        postDepositExceptionsPage.clickLockOfRow(2);
        postDepositExceptionsPage.clickUnlockModalUnlockButton();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT5676_VerifyActionButtonAccept() {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage = postDepositExceptionsPage.searchPostDeposit(AUTO_DATA_SEARCH_STRING);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(postDepositExceptionsPage.isDepositDateHeadingVisible(), "Deposit Data Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isAmountHeadingVisible(), "Amount Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isEntityHeadingVisible(), "Entity Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isWorkgroupHeadingVisible(), "WorkGroup Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isPaymentSourceHeadingVisible(), "Payment Source Heading is visible ");
        softAssert.assertTrue(postDepositExceptionsPage.isPaymentTypeHeadingVisible(), "Payment Type Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isBatchIDHeadingVisible(), "Batch ID Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isTransactionIDHeadingVisible(), "Transaction Heading is visible");
        softAssert.assertEquals(postDepositExceptionsPage.getRowTitleAttribute(), "Click here to view Exceptions Transaction Detail", "Transaction Detail Message is correct");
        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12180_ValidatePostDepositLockSave() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage = postDepositExceptionsPage.searchPostDeposit("7890004");
        PostDepositExceptionsTransactionDetailPage postDepositExceptionsTransactionDetailPage = postDepositExceptionsPage.selectFirstTransaction();
        postDepositExceptionsTransactionDetailPage.waitForAddRelatedItemButton();
        postDepositExceptionsTransactionDetailPage.scrollBar(270);
        postDepositExceptionsTransactionDetailPage.addRelatedItem();
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox1("9.00");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox2("14.00");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox3("160");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox4("05/12/2018");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox5("16");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox6("05");
        postDepositExceptionsTransactionDetailPage.savePostDeposit();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox1(), "9.00", "ItemBox1 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox2(), "14.00", "ItemBox2 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox3(), "160", "ItemBox3 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox4(), "05/12/2018", "ItemBox4 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox5(), "16", "ItemBox5 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox6(), "05", "ItemBox6 Value is Corr");
        softAssert.assertAll();
        postDepositExceptionsTransactionDetailPage.savePostDeposit();
        postDepositExceptionsTransactionDetailPage.returnToPostDepositExceptionsSummary();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12198_ValidatePostDepositLockDelete() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage = postDepositExceptionsPage.searchPostDeposit(SEARCH_STRING);
        PostDepositExceptionsTransactionDetailPage postDepositExceptionsTransactionDetailPage = postDepositExceptionsPage.selectTransactionTwo();
        postDepositExceptionsTransactionDetailPage.scrollBar(270);
        postDepositExceptionsTransactionDetailPage.waitForAddRelatedItemButton();
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox1("0.00");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox2("0.00");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox3("120");
        postDepositExceptionsTransactionDetailPage.selectaddRelatedItemBox4("05222018");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox1(), "0.00", "ItemBox1 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox2(), "0.00", "ItemBox2 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox3(), "120", "ItemBox3 Value is Correct");
        softAssert.assertEquals(postDepositExceptionsTransactionDetailPage.getaddRelatedItemBox4(), "05222018", "ItemBox4 Value is Correct");
        softAssert.assertAll();
        postDepositExceptionsTransactionDetailPage.deleteInvoice();
        postDepositExceptionsTransactionDetailPage = postDepositExceptionsTransactionDetailPage.confirmDeleteTransaction();
        postDepositExceptionsTransactionDetailPage.savePostDeposit();
        postDepositExceptionsTransactionDetailPage.returnToPostDepositExceptionsSummary();
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
