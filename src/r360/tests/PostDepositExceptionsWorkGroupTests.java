package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.*;
import utils.Browser;
import utils.ResultWriter;

public class PostDepositExceptionsWorkGroupTests extends BaseTest {
    private final String BATCH_ID="874";
    private final String STUB_SEQUENCE="22";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Throwable {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
        CleanupAfterTest();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12025_ValidatePostDepositExceptions() throws Exception {
       PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
       postDepositExceptionsPage.searchPostDeposit(BATCH_ID);
       PostDepositExceptionsTransactionDetailPage transDetailPage = postDepositExceptionsPage.selectDestinctTransaction();
       transDetailPage = transDetailPage.searchStub(STUB_SEQUENCE);
       transDetailPage.validateDepositInError();
       transDetailPage.validateDepositKFITypeInError();
       transDetailPage.typeTextPostDepositAccount("123456789abcdefghijklmnopqrstuvwwxyz~`!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?");
       transDetailPage.typeTextDepositKFIType("123456789abcdefghijklmnopqrstuvwwxyz~`!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?");
       transDetailPage = transDetailPage.savePostDeposit();
       transDetailPage = transDetailPage.searchStub(STUB_SEQUENCE);
       transDetailPage.validateDepositIsGood();
       transDetailPage.validateDepositKFITypeIsGood();
    }

    private void CleanupAfterTest() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.searchPostDeposit(BATCH_ID);
        PostDepositExceptionsTransactionDetailPage transDetailPage = postDepositExceptionsPage.selectDestinctTransaction();
        transDetailPage = transDetailPage.searchStub(STUB_SEQUENCE);
        transDetailPage.typeTextPostDepositAccount("");
        transDetailPage.typeTextDepositKFIType("");
        transDetailPage.savePostDeposit();
        transDetailPage.returnToPostDepositExceptionsSummary();
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}


