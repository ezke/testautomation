package r360.tests;

import java.util.Date;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.DataAnalysisLib;
import r360.data.DataImportQueue;
import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.Navigation;
import utils.*;

public class DIT_ImageRPS_Import extends BaseTest {
	private Date _startDate;
	private String workGroupName = "2234322 - Automation-Filter";
	private String paymentType = "Check";
	private String paymentSource = "AUTOMATIONIMAGERPS";
	private String batchID = "9020";
	
	@BeforeClass(alwaysRun=true)
	public void setup() {
		_startDate = new Date();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	//Steps 1-3
	public void PT2498_checkImportSuccessfullyThruQueue() throws Exception
	{
		_startDate = new Date();
		ImportData.attachment_Drop("Check.jpg", GlobalVariables.environment.FileImportAttachmentFolder + "\\MyCheck.jpg");
		ImportData.dit_ImageRPSDrop(GlobalVariables.environment.FileImportAttachmentFolder + "\\MyCheck.jpg", _startDate);
		BaseUI.verify_true_AndLog(
				DataImportQueue.checkDataImportQueue("AutomationImageRPS", 1, _startDate),
				"Found Successful Data Import Queue record.", 
				"Failed to find a Successful Data Import Queue record."
		);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	//Steps 4...
	public void PT2498_checkBatchSummaryResults() throws Exception 
	{
		String[] expectedValues = new String[]{
			"Batch: " + batchID,
			"Payment Source: " + paymentSource,
			"Payment Type: " + paymentType,
			"Transaction Count: 4",
			"Payment Count: 0",
			"Document Count: 8",
			"Batch Total: $0.00"
		};
		BatchSummary.getToPage();													//Steps 4-5
		BatchSummary.setDateRange(_startDate, _startDate);							//Step 6
		BatchSummary.setPaymentTypeDropdown(paymentType);
		BatchSummary.workGroupSelector.search_forText(workGroupName);				//Step 7
        BatchSummary.setPaymentSourceDropdown(paymentSource);
		BatchSummary.setSearchTest(batchID);
		BatchSummary.click_Search();												//Step 8
		TableData results = BatchSummary.return_SearchResults();
		DataAnalysisLib.validateRowContent(expectedValues, results.dataRow(0)); 	//Step 9
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			if(Browser.driver!=null) {
				Navigation.signOut();
			}

		} finally {
			Browser.closeBrowser();
		}
	}
}