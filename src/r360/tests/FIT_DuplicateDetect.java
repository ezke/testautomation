package r360.tests;

import java.util.Date;
import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.data.ImportData;
import r360.pages.GlobalVariables;
import r360.pages.Navigation;
import r360.pages.Notifications_Page;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;


// https://prod.practitest.com/p/5395/tests/967789/edit

public class FIT_DuplicateDetect extends BaseTest {
	private Date _startDate;
	private String _attachmentFile1 = "\\DupDetect.txt";
	private String _attachmentFile2 = "\\Test\\DupDetect.txt";
	
	@BeforeClass(alwaysRun=true)
	public void setup_method() throws Throwable {
		_startDate = new Date();
		//Step 1
		BaseUI.verify_true_AndLog(
				Notifications_Page.IsDupDetectActive(), 
				"The Duplicate Detect setting is active and will allow this test to work.", 
				"Duplicate Detect is disabled so this test can not be done.");
		ImportData.fit_ImageRPSDrop(GlobalVariables.environment.FileImportAttachmentFolder + _attachmentFile1, _startDate);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT2915_checkImportSuccessfullyThruQueue() throws Exception
	{
		BaseUI.verify_true_AndLog(
				Notifications_Page.checkDataImportQueue("Auto_ImageRPS_00", _startDate),
				"Found Successful Data Import Queue record for the first drop.", 
				"Failed to find a Successful Data Import Queue record for the first drop."
		);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT2915_dropClone() throws Exception
	{
		Date _nextDropDate = new Date();
		
		ImportData.fit_ImageRPSDrop(GlobalVariables.environment.FileImportAttachmentFolder + _attachmentFile2, _startDate);
		Notifications_Page.getToPage();
		BaseUI.verify_true_AndLog(
				Notifications_Page.checkDataImportQueue("Auto_ImageRPS_00", _nextDropDate),
				"Found Successful Data Import Queue record for the second drop.", 
				"Failed to find a Successful Data Import Queue record for the second drop."
		);
	}	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT2915_checkNotificationPage() throws Exception
	{
		//Steps 7-8
		Notifications_Page.enterAttachmentName("DupDetect");
		Notifications_Page.enterDateRange(_startDate, _startDate);
		Notifications_Page.clickSearch();
		Notifications_Page.clickFirstRow();
		String attachmentContent = Notifications_Page.getAttachmentContents();
		BaseUI.baseStringCompare("Attachment Contents", "DupDetect #2", attachmentContent);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}