package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.windows.IconServices;
import utils.BaseUI;
import utils.ResultWriter;

import java.net.MalformedURLException;

public class ICON_Services_Test extends BaseTest {
    private IconServices _icon;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws MalformedURLException {
        _icon = new IconServices();
    }

    @Test(groups = {"module_ICON", "smoke_Tests", "all_Tests"})
    public void DoBasicUpdate() {
        String xml =
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<RemitXMLCommand xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org.2001/XMLSchema\">\n" +
                "  <ICONInitialize Logon=\"ICONTester\"  \n" +
                "                  Password=\"Wausau#1\"  \n" +
                "                  PaymentSource=\"ImageRPS_Elkhorn\" />\n" +
                "  <CommandList>\n" +
                "    <Command CommandType=\"UpdateKeywords\">\n" +
                "      <QueryKeywords>\n" +
                "        <QueryKeyword Name=\"Batch ID\" Value=\"830\" />\n" +
                "        <QueryKeyword Name=\"Batch Date\" Value=\"09/22/2017\" />\n" +
                "        <QueryKeyword Name=\"Site ID\" Value=\"1\" />\n" +
                "        <QueryKeyword Name=\"Client ID\" Value=\"99\" />\n" +
                "        <QueryKeyword Name=\"P1 Sequence\" Value=\"\" />\n" +
                "      </QueryKeywords>\n" +
                "      <ActionKeywords>\n" +
                "        <ActionKeyword Name=\"P1 Operator ID\" Value=\"KYRIS\" />\n" +
                "      </ActionKeywords>\n" +
                "    </Command>\n" +
                "  </CommandList>\n" +
                "</RemitXMLCommand>";
        _icon.setRequestXML(xml);
        _icon.makeRequest();
        BaseUI.baseStringCompare(
                "Response Message",
                "Return: True - Batch Data update completed successfully.",
                _icon.readResults()
        );
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result, (screenshotPath) -> _icon.saveScreenShot(screenshotPath));
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() {
        _icon.close();
    }
}
