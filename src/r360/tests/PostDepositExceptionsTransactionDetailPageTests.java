package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import r360.pages.*;
import utils.Browser;
import utils.ResultWriter;

public class PostDepositExceptionsTransactionDetailPageTests extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        Browser.driver.manage().window().maximize();
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.selectFirstWorkGroup();
        Navigation.signOut();
        Navigation.return_Application();
        LoginPage.login(GlobalVariables.autoNoUnlock_Entity, GlobalVariables.autoNoUnlock_User,
                GlobalVariables.autoNoUnlock_Password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT11810_ValidatePostDepositLock() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        PostDepositExceptionsTransactionDetailPage transDetailPage = postDepositExceptionsPage.selectFirstUnlockedTransactionElement();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(transDetailPage.getLockedByLabel(), "Locked By:", "Locked By Label is Correct");
        softAssert.assertEquals(transDetailPage.getLockedLabelOwner(), "Auto No Unlock User", "Locked By Value is Correct");
        softAssert.assertEquals(transDetailPage.getBatchLabel(), "Batch ID:", "Batch ID Label is Correct");
        softAssert.assertEquals(transDetailPage.getPaymentTypeLabel(), "Payment Type:", "Check Label is Correct");
        softAssert.assertEquals(transDetailPage.getPaymentSourceLabel(), "Payment Source:", "Payment Source is Correct");
        softAssert.assertEquals(transDetailPage.getDepositDateLabel(), "Deposit Date:", "Deposit Date Label is Correct");
        softAssert.assertEquals(transDetailPage.getEntityLabel(), "Entity:", "Entity Label is Correct");
        softAssert.assertEquals(transDetailPage.getWorkgroupLabel(), "Workgroup:", "Workgroup Label is Correct");
        transDetailPage.returnToPostDepositExceptionsSummary();
        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT11831_ValidatePostDepositUnlock() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.selectFirstWorkGroup();
        Navigation.signOut();
        Navigation.return_Application();
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
        postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.selectFirstTransaction();
        Navigation.signOut();
        Navigation.return_Application();
        LoginPage.login(GlobalVariables.autoNoUnlock_Entity, GlobalVariables.autoNoUnlock_User,
                GlobalVariables.autoNoUnlock_Password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT11877_ValidatePostDepositButton() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.selectFirstLockedTransactionElement();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertFalse(postDepositExceptionsPage.isLockButtonEnabled(), "Check Unlock Button is Enabled");
        softAssert.assertAll();
        postDepositExceptionsPage.selectCancel();
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
