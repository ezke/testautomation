package r360.tests.advancedSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.*;

public class AdvancedSearch_DisplayWireInvoicesMVCConversion extends BaseTest {

    private String workgroup = "98 - Wire_Elkhorn";
    private String date1 = "05-31-2018";
    //<<Payment Sequence>>, <<Account Number>>, <<Payment Amount>>, <<R/T>>, <<Check/Trace/Ref Number>>, <<DDA>>, <<Payer>> and with all Field Options   from [PMT] and Press >
    String[] displayFieldsToAdd = {"Payment Sequence", "Account Number", "Payment Amount", "R/T", "Check/Trace/Ref Number", "DDA"
            , "Payer",
            "[PMT].ABA",
            "[PMT].Account Number",
            "[PMT].ACH Batch Number",
            "[PMT].Amount",
            "[PMT].BAI Bank Reference",
            "[PMT].BAI Customer Reference",
            "[PMT].BAI File Creation Date",
            "[PMT].BAI File ID",
            "[PMT].Company Data",
            "[PMT].Company ID",
            "[PMT].Company Name",
            "[PMT].Customer Account Number",
            "[PMT].DDA",
            "[PMT].Descriptive Date",
            "[PMT].Effective Date",
            "[PMT].Electronic Transaction Code",
            "[PMT].Entry Description",
            "[PMT].Individual ID",
            "[PMT].Individual Name",
            "[PMT].OFAC Indicator 1",
            "[PMT].OFAC Indicator 2",
            "[PMT].Originating DFI",
            "[PMT].Originator Account",
            "[PMT].Originator Info",
            "[PMT].Originator Name",
            "[PMT].Receiving Company",
            "[PMT].Remitter Name",
            "[PMT].Sending Bank",
            "[PMT].Serial",
            "[PMT].Service Class Code",
            "[PMT].Settlement Date",
            "[PMT].Standard Entry Class",
            "[PMT].Trace Number",
            "[PMT].Transaction Code",
            "[PMT].Type Code"
    };

    TableData results;

    @BeforeClass
    public void setup_method() throws Throwable {
        if (Browser.driver == null) {

            Browser.openBrowser(GlobalVariables.baseURL, "chrome");

            LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

            Navigation.navigate_Search_AdvancedSearch();

            Browser.driver.manage().window().maximize();

            AdvancedSearch.workGroupSelector.search_forText(workgroup);
            AdvancedSearch.setDateRange(date1, date1);

            AdvancedSearch.paymentTypeDropdown.select_EntityValue("Wire");

            AdvancedSearch.add_DisplayFields(displayFieldsToAdd);

            AdvancedSearch.click_Search();
            AdvancedSearch.select_ShowRowsDropdownValue(100);
            results = AdvancedSearch.return_SearchResults();
        }
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3588_AdvancedSearchWireViewImagelinksPaymentSequence() {
        String headerText = "Payment Sequence";
        String paymentSequenceRow1 = results.data.get(0).get(headerText);
        BaseUI.verify_true_AndLog(  BaseUI.string_isNumeric(paymentSequenceRow1),"Payment Sequence is displayed",
                "Payment Sequence is not displayed correctly");
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3588_AdvancedSearchWireViewImagelinksPaymentAmount() {
        String headerText = "Payment Amount";
        String paymentAmountForRow1 = results.data.get(0).get(headerText);
        BaseUI.verify_true_AndLog(paymentAmountForRow1.startsWith("$"), "Matched $", "Did not have a $");
        BaseUI.verify_true_AndLog(BaseUI.string_IsDouble(paymentAmountForRow1.replace("$", "").replace(",", "")),
                "Payment Amount is displayed correctly", "Payment Amount is not displayed correctly");
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3588_AdvancedSearchWireViewImagelinksCheckTraceRefNum() {
        String headerText = "Check/Trace/Ref Number";
        String paymentCheckTraceRefNumForRow1 = results.data.get(0).get(headerText);
        BaseUI.verify_true_AndLog( BaseUI.string_isNumeric(paymentCheckTraceRefNumForRow1), "CheckTraceRefNum field is correct", "CheckTraceRefNum is not displayed correctly");
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3588_AdvancedSearchWireViewImagelinksDDA() {
        String headerText = "DDA";
        String paymentDDAForRow1 = results.data.get(0).get(headerText);
        BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(paymentDDAForRow1), "DDA field is correct", "DDA is not displayed correctly");
    }

    /*
    For now there is no data in R360 being returned for Account Number and R/T so tests commented out for now
     @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3588_AdvancedSearchWireViewImagelinksAccountNumber() {
        String headerText = "Account Number";
        String paymentAccountNumberForRow1 = results.data.get(0).get(headerText);
        BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(paymentAccountNumberForRow1), "Account Number displayed correctly", "Account Number displayed incorrectly");
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3588_AdvancedSearchWireViewImagelinksRT()  {
        String headerText = "R/T";
        String paymentRTForRow1 = results.data.get(0).get(headerText);
        BaseUI.verify_true_AndLog(paymentRTForRow1.startsWith("$"), "Matched $", "Did not have a $");
        BaseUI.verify_true_AndLog(BaseUI.string_IsDouble(paymentAmountDateForRow1.replace("$", "").replace(",", "")),
                "Pass", "Fail");
    }
     */

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
