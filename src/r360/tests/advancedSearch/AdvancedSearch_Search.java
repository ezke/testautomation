package r360.tests.advancedSearch;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.*;

public class AdvancedSearch_Search extends BaseTest {

    TableData results;
    TableData paymentData;
    TableData stubData;

    private String workgroup = "2234322";  //workgroup contains data for Payment Batch Sequence
    private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
    private String date2 = date1;
    private String date3 = ImportData.return_PreviousDate90Day_FormattedForWebsite();

    @BeforeClass
    public void setup_method() throws Throwable {
        if (Browser.driver == null) {

            Browser.openBrowser(GlobalVariables.baseURL);
            LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 60);

            Navigation.navigate_Search_AdvancedSearch();

            AdvancedSearch.workGroupSelector.search_forText(workgroup);
            AdvancedSearch.setDateRange(date1, date2);
        }
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void TC3717_SearchStandardFieldsSequenceLessThan() throws Exception {
        String field1 = "Payment Batch Sequence";
        String operator1 = "Is Less Than";
        String value1 = "4";
        String[] displayFieldsToAdd = {"Payment Batch Sequence"};
        AdvancedSearch.setDateRange(date3, date2);
        AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
        AdvancedSearch.add_DisplayFields(displayFieldsToAdd);
        AdvancedSearch.click_Search();
        TableData results = AdvancedSearch.return_SearchResults();
        AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field1, operator1, value1);
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3507_ValidateSelectivePrint() throws Exception {
        AdvancedSearch.setDateRange(date2, date2);
        AdvancedSearch.click_Search();
        AdvancedSearch.verify_ResultsPage_Has_PDFView_Button();
        AdvancedSearch.verify_ResultsPage_Has_DownloadTest_Button();
        AdvancedSearch.verify_ResultsPage_Has_DownloadResults_Button();
        AdvancedSearch.verify_ResultsPage_Has_Selective_Print_Mode_Button();
        AdvancedSearch.click_ResultsPage_Selective_Print_Mode_Button();
        AdvancedSearch.return_ResultsPage_Selective_Print_Mode_Button_Text();
        AdvancedSearch.verify_ResultsPage_Has_Inclusion_Checkboxes();
        AdvancedSearch.verify_ResultsPage_UnderSelectivePrint_Has_InvoicePayment_Dropdown();
        AdvancedSearch.verify_ResultsPage_Has_ViewSelectedButton();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3736_QueryBuilderBatchSequenceLessThan() throws Exception {
        String field1 = "Payment Batch Sequence";
        String operator1 = "Is Less Than";
        String value1 = "4";
        String[] displayFieldsToAdd = {"Payment Batch Sequence", "Batch Number"};
        AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
        AdvancedSearch.add_DisplayFields(displayFieldsToAdd);
        AdvancedSearch.click_Search();
        TableData results = AdvancedSearch.return_SearchResults();
        AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field1, operator1, value1);
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3737_QueryBuilderBatchSequenceEquals() throws Exception {
        String field1 = "Payment Batch Sequence";
        String operator1 = "Equals";
        String value1 = "3";
        String[] displayFieldsToAdd = {"Payment Batch Sequence", "Batch Number"};
        AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
        AdvancedSearch.add_DisplayFields(displayFieldsToAdd);
        AdvancedSearch.click_Search();
        TableData results = AdvancedSearch.return_SearchResults();
        AdvancedSearch.verify_ResultsPage_ResultsEqualExpected(results, field1, value1);
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT3738_QueryBuilderBatchNumberGreaterEqual() throws Exception {
        String field1 = "Payment Batch Sequence";
        String operator1 = "Is Greater Than";
        String value1 = "2";
        String[] displayFieldsToAdd = {"Payment Batch Sequence", "Batch Number"};
        AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
        AdvancedSearch.add_DisplayFields(displayFieldsToAdd);
        AdvancedSearch.click_Search();
        TableData results = AdvancedSearch.return_SearchResults();
        AdvancedSearch.verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(results, field1, operator1, value1);
    }

    @Test(groups = {"all_Tests"})
    public void PT3144_AdvancedSearch_DisplayFields_DDA() throws Exception {
        String field = "DDA";

        AdvancedSearch.addDisplayField(field);
        AdvancedSearch.click_Search();
        AdvancedSearch.select_ShowRowsDropdownValue(100);

        BaseUI.scroll_to_element(Locator.lookupElement("advancedSearch_TableHeader_ByText", field, null));
        BaseUI.verifyElementAppears(Locator.lookupElement("advancedSearch_TableHeader_ByText", field, null));
    }

    @Test(groups = {"all_Tests"})
    public void PT3143_AdvancedSearch_DisplayFields_AllFields() throws Exception {

        ArrayList<String> availableFields = AdvancedSearch.return_AvailableFieldsList();
        BaseUI.verify_true_AndLog(availableFields.size() > 0, "Found Available Fields.",
                "Did NOT find Available Fields.");

        AdvancedSearch.add_All_AvailableFields();
        AdvancedSearch.click_Search();
        AdvancedSearch.select_ShowRowsDropdownValue(100);

        // Extract table data.
        results = AdvancedSearch.return_SearchResults();

        for (String field : availableFields) {
            BaseUI.scroll_to_element(Locator.lookupElement("advancedSearch_TableHeader_ByText", field, null));
            BaseUI.verifyElementAppears(Locator.lookupElement("advancedSearch_TableHeader_ByText", field, null));
        }
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        if (BaseUI.elementExists("advancedSearch_Results_BreadcrumbHeaderLink_AdvancedSearch", null, null)) {
            AdvancedSearch.click_AdvancedSearch_Breadcrumb();
        }

        if (AdvancedSearch.paymentTypeDropdown.return_EntityValue().contains("All")) {
            AdvancedSearch.paymentTypeDropdown.reset_Dropdown();
        }

        if (AdvancedSearch.paymentSourceDropdown.return_EntityValue().contains("All")) {
            AdvancedSearch.paymentSourceDropdown.reset_Dropdown();
        }

        if (AdvancedSearch.sortByDropdown.return_EntityValue().equals("Select one...")) {
            AdvancedSearch.sortByDropdown.reset_Dropdown();
        }

        AdvancedSearch.click_RemoveAll_SelectedFields();
        AdvancedSearch.removeFirstFilterIfItExists();
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
