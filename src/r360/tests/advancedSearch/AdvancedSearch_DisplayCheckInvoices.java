package r360.tests.advancedSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_DisplayCheckInvoices extends BaseTest {
	
	TableData results;
	private String mainWindowHandle;
	TableData pdfData;
	private String workgroupString;

	private String workgroup = "2234322";

	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {

			Browser.openBrowser(GlobalVariables.baseURL, "firefox");

			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);

			AdvancedSearch.paymentTypeDropdown.select_EntityValue("Check");
			AdvancedSearch.click_Search();
			mainWindowHandle = Browser.driver.getWindowHandle();

			results = AdvancedSearch.return_SearchResults();
			workgroupString = BaseUI.getTextFromField(Locator.lookupElement("advancedSearch_WorkGroupValue"));

			BaseUI.ClickAndSwitchWindow(
					Locator.lookupElement("advancedSearch_ViewTransactionImageLink_ByIndex", "1", null), false, 15000);
			// pdfData = AdvancedSearch.return_PDF_Data();

			Thread.sleep(500);
			Navigation.wait_for_LoadingSpinner_ToDisappear();
		}
	}


	@Test(groups = { "all_Tests", "firefox_only" })
	public void  PT3687_ImageRPSCheckImagelinks() throws Exception{
		//New Test Case Assigned but looks like a duplicate of  PT3142_AdvancedSearch_ComparePDF_Workgroup
		String field = "Workgroup";
		String pdfText = AdvancedSearch.return_Text_FromPDF_Field(field);
		BaseUI.baseStringCompare(field, workgroupString, pdfText);

	}


	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT3142_AdvancedSearch_ComparePDF_DepositDate() throws Exception {
		String field = "Deposit Date";

		String pdfText = AdvancedSearch.return_Text_FromPDF_Field(field);
		
		results.replace_DateFormat_shortened();
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}



	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT3142_AdvancedSearch_ComparePDF_Workgroup() throws Exception {
		String field = "Workgroup";
		String pdfText = AdvancedSearch.return_Text_FromPDF_Field(field);

		BaseUI.baseStringCompare(field, workgroupString, pdfText);
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT3142_AdvancedSearch_ComparePDF_BatchNumber() throws Exception {
		String field = "Batch Number";
		String pdfText = AdvancedSearch.return_Text_FromPDF_Field("Batch:");
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT3142_AdvancedSearch_ComparePDF_Transaction() throws Exception {
		String field = "Transaction";

		String pdfText = AdvancedSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT3142_AdvancedSearch_ComparePDF_BatchID() throws Exception {
		String field = "Batch ID";

		String pdfText = AdvancedSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}


	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Browser.driver.switchTo().window(mainWindowHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
