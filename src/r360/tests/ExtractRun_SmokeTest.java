package r360.tests;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.windows.ExtractRun;
import utils.BaseUI;
import utils.FileOperations;
import utils.ResultWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExtractRun_SmokeTest extends BaseTest {
    private ExtractRun _extractRun;
    private String _outputFile;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        if(GlobalVariables.environment.ClientURL == null) {
            throw new SkipException(
                    "Skipping all the tests as no URL was specified for the WhiteSeed driver"
            );
        }
        else {
            _outputFile = GlobalVariables.environment.ClientDriveUNC + "\\WFSApps\\RecHub\\ExtractDesignManager\\Output\\AutomationOutput.txt";
            _extractRun = ExtractRun.OpenExtractRun();
        }
    }

    @Test(groups = {"module_Extract_Run", "smoke_Tests", "all_Tests"})
    public void PT12872_ExtractRun() throws Exception {
        startClean();

        File file = new File(_outputFile);

        _extractRun.logIn();
        _extractRun.openDefinition("D:\\WFSApps\\RecHub\\ExtractDesignManager\\ExtractDefinitions\\AutomatedTest.xml");
        _extractRun.run();
        FileOperations.wait_For_File_To_Exist(_outputFile);
        BaseUI.verify_true_AndLog(file.exists(), "Results file exists.", "Results file not found.");
        BaseUI.verify_true_AndLog(file.length() > 0, "File does contain data.", "The file is empty.");
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result, (screenshotPath)-> {
            try(FileOutputStream screenshotStream = new FileOutputStream(screenshotPath)) {
                screenshotStream.write(_extractRun.getScreenShot());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        if(_extractRun!=null) {
            _extractRun.close();
        }
    }

    private void startClean() {
        File file = new  File(_outputFile);
        if(file.exists()) {
            file.delete();
        }
    }
}
