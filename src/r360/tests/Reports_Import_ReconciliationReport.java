package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.Reports;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class Reports_Import_ReconciliationReport extends BaseTest {

	// https://prod.practitest.com/p/5395/tests/969103/edit#steps_tab

	String mainHandle = "";
	String report = "Import Reconciliation Report";
	String paymentSource = "AutomationACH";


	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
		mainHandle = Browser.driver.getWindowHandle();
		Navigation.navigate_Reports();
		Reports.expand_groupingHeader(Reports.receivables360_Online_ReportsText);

		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, report);
	}

	// Step 2
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3038_SelectImportReconciliationReport_GenerateReportAs() throws Throwable {
		Reports.generateReportAs_dropdown.verify_SelectedDropdownValue("PDF");
	}

	// Step 2
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3038_SelectImportReconciliationReport_StartDateVisible() throws Throwable {
		Reports.startDate_DatePicker.verify_DropdownDisplayed();
	}

	// Step 2
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3038_SelectImportReconciliationReport_EndDateVisible() throws Throwable {
		Reports.endDate_DatePicker.verify_DropdownDisplayed();
	}

	// Step 2
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3038_SelectImportReconciliationReport_PaymentSource() throws Throwable {
		Reports.paymentSource_dropdown.verify_SelectedDropdownValue("Select one...");
	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3038_VerifyPaymentSources_Selected() throws Throwable {
		Reports.select_ImportReconciliationReport_Values(null, null, null, paymentSource);
		Reports.paymentSource_dropdown.verify_SelectedDropdownValue(paymentSource);

	}
	

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT3038_VerifyPDF_Title_matches() throws Throwable {
		Reports.click_GenerateReport_Button();
		Reports.verify_PDF_Title_MatchesExpected("Import Reconciliation");
	}


	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			BaseUI.close_ExtraWindows(mainHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
