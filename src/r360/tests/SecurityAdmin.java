package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SecurityAdmin extends BaseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
    }

    // Entity Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 190)
    public void PT12880_Steps1To4_EntityInitalPresentation_SmokeTest() throws Exception {
        Navigation.navigate_Admin_Entities();
        BaseUI.verifyElementExists("securityAdminEntityName", null, null);
    }

    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 191)
    public void PT12880_Step5_EntityInitalPresentation_SmokeTest() throws Exception {
        BaseUI.verifyElementExists("securityAdmin_ActiveTab", "Entities", null);
    }

    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 191)
    public void PT12880_Step6_EntityInitalPresentation_SmokeTest() throws Exception {
        BaseUI.verifyElementExists("securityEntities_DeleteButton", null, null);
        BaseUI.verifyElementExists("securityEntities_EditButton", null, null);
        BaseUI.verifyElementExists("securityEntities_AddButton", null, null);
    }

    @AfterMethod(alwaysRun = true)
    public void testTearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void finalTearDown() throws Exception {
        if (BaseUI.elementAppears(Locator.lookupElement("Navigate_SignOut"))) {
            Navigation.signOut();
        }
        Browser.closeBrowser();
    }
}
