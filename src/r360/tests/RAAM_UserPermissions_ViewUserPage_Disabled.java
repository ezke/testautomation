package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class RAAM_UserPermissions_ViewUserPage_Disabled extends BaseTest {

	
	Pagination userManagementGrid;
	Pagination availableGroupsGrid;
	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	String userName = "AutomationTest";
	

	@BeforeClass (alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_viewUserPageDisabled, GlobalVariables.userName_viewUserPageDisabled, 
				GlobalVariables.passsword_viewUserPageDisabled, 0);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT8559_Raaam_SecurityAdmin_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT8559_Raaam_SecurityAdmin_NO_UserOption_Listed() throws Throwable {
		Navigation.navigate_openMenu("Admin");
		BaseUI.verify_false_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "Users"), 
				"No find Admin>Users menu item listed.",
				"Admin>Users menu item exists");
	}

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
