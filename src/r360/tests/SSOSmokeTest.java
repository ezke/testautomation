package r360.tests;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.Navigation;
import r360.pages.SSOTestClient;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class SSOSmokeTest extends BaseTest {
    private SSOTestClient _ssoClient;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        if(GlobalVariables.environment.SSOClientURL == null) {
            throw new SkipException(
                    "Skipping all the tests as no URL was specified for the Test SSO Client"
            );
        }
        else {
            Browser.openBrowser(GlobalVariables.environment.SSOClientURL);
            _ssoClient = new SSOTestClient();
        }
    }

    @Test(groups = { "all_Tests", "smoke_Tests" }, priority = 300)
    public void UseToken() {
        _ssoClient.enterEntity(GlobalVariables.entityName);
        _ssoClient.enterUser(GlobalVariables.login);
        _ssoClient.clickNext();
        _ssoClient.clickLogin();
        BaseUI.verifyElementAppears(BaseUI.waitForElementToBeDisplayed_ButDontFail("dshbrd_Element_To_Wait_For_ForPageLoad", null, null, 50));
    }

    @AfterMethod(alwaysRun = true)
    public void testTearDown(ITestResult results) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(results);
    }

    @AfterClass(alwaysRun = true)
    public void finalTearDown() throws Exception {
        if(_ssoClient!=null) {
            Navigation.signOut();
            Browser.closeBrowser();
        }
    }
}
