package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import r360.pages.PostDepositExceptionsPage;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import utils.Browser;
import utils.ResultWriter;

public class PostDepositExceptionsTests extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT11664_ValidateColumnsDisplayCorrectData() {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(postDepositExceptionsPage.isDepositDateHeadingVisible(), "Deposit Data Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isAmountHeadingVisible(), "Amount Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isEntityHeadingVisible(), "Entity Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isWorkgroupHeadingVisible(), "WorkGroup Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isPaymentSourceHeadingVisible(), "Payment Source Heading is visible ");
        softAssert.assertTrue(postDepositExceptionsPage.isPaymentTypeHeadingVisible(), "Payment Type Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isBatchIDHeadingVisible(), "Batch ID Heading is visible");
        softAssert.assertTrue(postDepositExceptionsPage.isTransactionIDHeadingVisible(), "Transaction Heading is visible");
        softAssert.assertAll();
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
