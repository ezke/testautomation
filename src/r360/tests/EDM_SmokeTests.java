package r360.tests;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.windows.ExtractDesignManager;
import utils.BaseUI;
import utils.FileOperations;
import utils.ResultWriter;

import java.io.File;

public class EDM_SmokeTests extends BaseTest {
    private ExtractDesignManager _edm;
    private String _outputFile;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        if(GlobalVariables.environment.ClientURL == null) {
            throw new SkipException(
                    "Skipping all the tests as no URL was specified for the WhiteSeed driver"
            );
        }
        else {
            _outputFile = GlobalVariables.environment.ClientDriveUNC + "\\WFSApps\\RecHub\\ExtractDesignManager\\Output\\AutomationOutput.txt";
            _edm = new ExtractDesignManager();
        }
    }

    @Test(groups = {"module_EDM", "smoke_Tests", "all_Tests"})
    public void PT12855_ExtractDesignManager_SmokeTest () throws Exception {
        File file = new File(_outputFile);

        _edm.logIn();
        _edm.openDefinition( GlobalVariables.environment.EDMDefintionFolder + "\\AutomatedTest.xml");
        _edm.runDefinition();
        FileOperations.wait_For_File_To_Exist(_outputFile);
        BaseUI.verify_true_AndLog(file.exists(), "Results file exists.", "Results file not found.");
        BaseUI.verify_true_AndLog(file.length() > 0, "File does contain data.", "The file is empty.");
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result, (screenshotPath) -> _edm.saveScreenShot(screenshotPath));
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        if(_edm != null) {
            _edm.close();
        }
    }
}
