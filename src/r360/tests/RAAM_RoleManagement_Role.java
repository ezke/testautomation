package r360.tests;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdministration;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_RoleManagement_Role extends BaseTest {

	String parentEntity = "AutomationTest";
	String testEntity = "automation_roleManage1";

	@BeforeClass (alwaysRun=true) 
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password,0);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.expandNode(parentEntity);
		if (BaseUI.pageSourceContainsString(testEntity)) {
			SecurityAdministration.deleteEntity_withRoles(testEntity);
			SecurityAdministration.expandNode(parentEntity);
		}

		SecurityAdministration.selectNode(parentEntity);

		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");

		SecurityAdministration.navigate_toRolesTab();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6760_RoleManage_AddRole_Cancel() throws Throwable {
		String roleName = "testRole1";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName, roleDescription, roleApplication);
		SecurityAdmin_RolesManagementModal.click_Cancel();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName, false);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6760_RoleManage_AddRole_Save() throws Throwable {
		String roleName = "testRole2";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName, true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6760_RoleManage_AddRole_SaveButton_DefaultsToGreyedOut() throws Throwable {
		SecurityAdministration.launchAddRoleModal();
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT6761_RoleManage_AddRole_SaveButton_NoLongerGreyedOut_AfterRequiredInfo() throws Throwable {
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields("testRole", "", "Framework");
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6758_RoleManage_AddRole_RoleName_UnableToEnter_InvalidChars() throws Throwable {
		String[] invalidSpecialChars = { "a$", "b%", "c^", "d!", "e@", "f#", "g%", "h*" };
		SecurityAdministration.launchAddRoleModal();

		for (String character : invalidSpecialChars) {
			SecurityAdmin_RolesManagementModal.verifyErrorMessage_afterEnteringtext_ForGivenField(
					"scrtyAdmin_RolesMdl_RoleNameTxt", character,
					"Only letters, digits, spaces and & , . : ' ( ) - _ are allowed.");
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6758_RoleManage_AddRole_RoleName_AbleToEnter_ValidSpecialChars() throws Throwable {
		String validCharacters = "&,.:'()-_";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt",
				validCharacters, validCharacters);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6758_RoleManage_AddRole_RoleName_AbleToEnter_UpTo50Chars() throws Throwable {
		String validCharacters = "NeedToGetTo50CharsAboutHalfWay_OkayGettingCloseEnd1";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt",
				validCharacters, validCharacters.substring(0, 50));
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6758_RoleManage_AddRole_RoleName_Save_50Chars_for51Entered() throws Throwable {
		String roleName = "NeedToGetTo50CharsAboutHalfWay_OkayGettingCloseEnd2";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName.substring(0, 50), true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6756_RoleManage_AddRole_Save_UnableToSaveDuplicatRole() throws Throwable {
		String roleName = "testRoleForDupe1";
		String roleDescription = "testDupeDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName, roleDescription, roleApplication);
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdmin_RolesManagementModal.verifyErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt",
				"Role name must be unique.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6756_RoleManage_AddRole_Save_UnableToSaveDuplicatRole_ButSavesWhenUpdated() throws Throwable {
		String roleName = "testRoleForDupe2";
		String roleName2 = "testRoleForDupe3";
		String roleDescription = "testDupeDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName, roleDescription, roleApplication);
		SecurityAdmin_RolesManagementModal.click_Save();
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), roleName2);
		BaseUI.tabThroughField("scrtyAdmin_RolesMdl_RoleNameTxt");
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName2, true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT6755_RoleManage_SaveGreyedOut_ForJustRoleNameEntered() throws Throwable {
		String roleName = "testRoleForRequiredFieldValidation";
		SecurityAdministration.launchAddRoleModal();
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), roleName);
		BaseUI.tabThroughField("scrtyAdmin_RolesMdl_RoleNameTxt");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT6755_RoleManage_SaveEnabled_ForRoleNameAndApplicationEntered() throws Throwable {
		String roleName = "testRoleForRequiredFieldValidation";
		SecurityAdministration.launchAddRoleModal();
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), roleName);
		SecurityAdmin_RolesManagementModal.select_Application("Framework");
		BaseUI.tabThroughField("scrtyAdmin_RolesMdl_RoleNameTxt");
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT6755_RoleManage_RoleNameRequired_ClearedOutRoleName() throws Throwable {
		String roleName = "testRoleForRequiredFieldValidation1";
		String roleDescription = "testRoleNameRequiredDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName, roleDescription, roleApplication);
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), "");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		SecurityAdmin_RolesManagementModal.verifyErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt",
				"This field is required.");
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT6755_RoleManage_ApplicationRequired_ResetApplication() throws Throwable {
		String roleName = "testRoleForRequiredFieldValidation1";
		String roleDescription = "testRoleNameRequiredDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName, roleDescription, roleApplication);
		SecurityAdmin_RolesManagementModal.appDropdwn.reset_Dropdown();
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));

		SecurityAdmin_RolesManagementModal.verifyErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleAppDropdwn",
				"This field is required.");
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6754_RoleManage_DeleteRole_Cancel() throws Throwable {
		String roleName = "testDeleteRoleCancel1";
		String roleDescription = "testDeleteRoleDescription1";
		String roleApplication = "Framework";

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		WebElement roleCheckbox = SecurityAdministration.roleGrid.check_box_for_GivenColumnAndText("Name", roleName);
		BaseUI.checkCheckbox(roleCheckbox);
		SecurityAdministration.click_DeleteRolesButton();
		SecurityAdministration.cancel_DeleteRolesModal();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName, true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6757_RoleManage_SameName_FromDifferentEntity_AbleToAdd() throws Throwable {
		String roleDescription = "testDuplicateRoleDescription3";
		String roleApplication = "Framework";

		// find our pre-existing role.
		SecurityAdministration.selectNode(parentEntity);
		String otherRoleName = SecurityAdministration.roleGrid.getTopEntries("Name", 1).get(0);
		BaseUI.verify_String_Not_EmptyOrNull(otherRoleName);

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(otherRoleName, roleDescription, roleApplication);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", otherRoleName, true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6759_RoleManage_AddRole_Description_UnableToEnter_InvalidChars() throws Throwable {
		String[] invalidSpecialChars = { "a$", "b%", "c^", "d!", "e@", "f#", "g%", "h*" };
		SecurityAdministration.launchAddRoleModal();

		for (String character : invalidSpecialChars) {
			SecurityAdmin_RolesManagementModal.verifyErrorMessage_afterEnteringtext_ForGivenField(
					"scrtyAdmin_RolesMdl_RoleDescTxt", character,
					"Only letters, digits, spaces and & , . : ' ( ) - _ are allowed.");
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6759_RoleManage_AddRole_Description_AbleToEnter_ValidSpecialChars() throws Throwable {
		String validCharacters = "&,.:'()-_";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleDescTxt",
				validCharacters, validCharacters);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6759_RoleManage_AddRole_Description_AbleToEnter_UpTo80Chars() throws Throwable {
		String validCharacters = "NeedToGetTo81Chars_Thistakesforeveromg_AboutHalfWay_OkayGettingCloseEndJust2more1";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleDescTxt",
				validCharacters, validCharacters.substring(0, 80));
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6759_RoleManage_AddRole_Description_Save_80Chars_for81Entered() throws Throwable {
		String roleName = "roleMax80CharTest";
		String roleDescription = "NeedToGetTo81Chars_Thistakesforeveromg_AboutHalfWay_OkayGettingCloseEndJust2more1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description",
				roleDescription.substring(0, 80), true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6752_RoleManage_AddRole_DeleteRole() throws Throwable {
		String roleName = "newRoleToDelete";
		String roleDescription = "stuff";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		SecurityAdministration.deleteRole_ForGivenRoleName(roleName);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName, false);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6715_RoleManage_UpdateRole_Cancel() throws Throwable {
		String roleName = "testRoleToUpdate";
		String roleDescription = "testRoleToUpdateDescription";
		String roleApplication = "Framework";
		String roleName2 = "testingChangeToName";
		String roleDescription2 = "testingChangeToDescription";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName2, roleDescription2, roleApplication);
		SecurityAdmin_RolesManagementModal.click_Cancel();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName, true);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description", roleDescription, true);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName2, false);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description", roleDescription2, false);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6715_RoleManage_UpdateRole_ChangeRoleDescription_AndSave() throws Throwable {
		String roleName = "testRoleToUpdate_UpdateDescription";
		String roleDescription = "testRole_UpdatingDescription";
		String roleApplication = "Framework";
		String roleDescription2 = "testingChange_UpdatedDescription";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleDescTxt"), roleDescription2);
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description", roleDescription, false);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description", roleDescription2, true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6713_RoleManage_UpdateRole_ChangeRoleName_AndSave() throws Throwable {
		String roleName = "testRoleToUpdate_UpdateRoleName";
		String roleDescription = "testDescription";
		String roleApplication = "Framework";
		String roleName2 = "testingChange_UpdatedRoleName";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), roleName2);
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"));
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName, false);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName2, true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6712_RoleManage_UpdateRole_Description_UnableToEnter_InvalidChars() throws Throwable {
		String roleName = "test_checkDescValidationsOnUpdate";
		String roleDescription = "testDescription";
		String roleApplication = "Framework";
		String[] invalidSpecialChars = { "a$", "b%", "c^", "d!", "e@", "f#", "g%", "h*" };
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		for (String character : invalidSpecialChars) {
			SecurityAdmin_RolesManagementModal.verifyErrorMessage_afterEnteringtext_ForGivenField(
					"scrtyAdmin_RolesMdl_RoleDescTxt", character,
					"Only letters, digits, spaces and & , . : ' ( ) - _ are allowed.");
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6712_RoleManage_UpdateRole_Description_AbleToEnter_ValidSpecialChars() throws Throwable {
		String roleName = "test_checkDescValidationsOnUpdate1";
		String roleDescription = "testDescription";
		String roleApplication = "Framework";
		String validCharacters = "&,.:'()-_";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);

		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleDescTxt",
				validCharacters, validCharacters);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6712_RoleManage_UpdateRole_Description_AbleToEnter_UpTo80Chars() throws Throwable {
		String roleName = "test_checkDescValidationsOnUpdate2";
		String roleDescription = "testDescription";
		String roleApplication = "Framework";
		String validCharacters = "NeedToGetTo81Chars_Thistakesforeveromg_AboutHalfWay_OkayGettingCloseEndJust2more1";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleDescTxt",
				validCharacters, validCharacters.substring(0, 80));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6705_RoleManage_UpdateRole_ChangeName_ToBeDuplicate_AndSave() throws Throwable {
		String roleName = "testRoleForDupe6";
		String roleName2 = "testRoleForDupe7";
		String roleDescription = "testDupeDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName2, roleDescription, roleApplication);
		
		SecurityAdministration.roleGrid.filter_textbox_SendKeys("Name", roleName2);
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName2);
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName, roleDescription, roleApplication);
		SecurityAdmin_RolesManagementModal.click_Save();
		
		SecurityAdmin_RolesManagementModal.verifyErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt", "Role name must be unique.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		
		//Change name back to non-du
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleName2, roleDescription, roleApplication);
		SecurityAdmin_RolesManagementModal.click_Save();
		//SecurityAdministration.roleGrid.filter_textbox_ClearFilter("Name");
		SecurityAdministration.roleGrid.filter_textbox_SendKeys("Name", roleName2);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleName2, true);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6707_RoleManage_UpdateRole_NameValidation_UpdateToAlpahNumeric() throws Throwable {
		String roleName = "testRoleForUpdateName1";
		String newRoleName = "updatedRoleName";
		String roleDescription = "Test Description";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt", newRoleName, newRoleName);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6707_RoleManage_UpdateRole_NameValidation_UpdateToInvalidSpecialChar() throws Throwable {
		String roleName = "test_RoleForUpdateName2";
		String roleDescription = "testDescription";
		String roleApplication = "Framework";
		String[] invalidSpecialChars = { "a$", "b%", "c^", "d!", "e@", "f#", "g%", "h*" };
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		for (String character : invalidSpecialChars) {
			SecurityAdmin_RolesManagementModal.verifyErrorMessage_afterEnteringtext_ForGivenField(
					"scrtyAdmin_RolesMdl_RoleNameTxt", character,
					"Only letters, digits, spaces and & , . : ' ( ) - _ are allowed.");
		}
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6707_RoleManage_UpdateRole_NameValidation_UpdateToValidSpecialChar() throws Throwable {
		String roleName = "test_RoleForUpdateName3";
		String roleDescription = "testDescription";
		String roleApplication = "Framework";
		String validCharacters = "&,.:'()-_";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);

		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt",
				validCharacters, validCharacters);
	}
	

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6707_RoleManage_UpdateRole_NameValidation_UpdateTo51Chars_OnlyAllows50() throws Throwable {
		String roleName = "test_MaxCharsName";
		String roleName2 = "NeedToGetTo50CharsAboutHalfWay_OkayGettingCloseEn13";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.verifyNO_ErrorMessage_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt", roleName2, roleName2.substring(0,50));

	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6706_RoleManage_UpdateName_SameName_FromDifferentEntity_AbleToAdd() throws Throwable {
		String originalName = "test_ForDupes1";
		String roleDescription = "testDuplicateRoleDescription3";
		String roleApplication = "Framework";

		// find our pre-existing role.
		SecurityAdministration.selectNode(parentEntity);
		String otherRoleName = SecurityAdministration.roleGrid.getTopEntries("Name", 1).get(0);
		BaseUI.verify_String_Not_EmptyOrNull(otherRoleName);

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(originalName, roleDescription, roleApplication);
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(originalName);
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), otherRoleName);
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"));
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", otherRoleName, true);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6704_RoleManage_UpdateRole_VerifyRequiredField_NameFieldRequired() throws Throwable {
		String roleName = "test_NameFieldRequired1";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		SecurityAdmin_RolesManagementModal.verifyErrorMessage_afterEnteringtext_ForGivenField("scrtyAdmin_RolesMdl_RoleNameTxt", "", "This field is required.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6704_RoleManage_UpdateRole_VerifyRequiredField_ApplicationScopeRequired() throws Throwable {
		String roleName = "test_NameFieldRequired2";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		SecurityAdmin_RolesManagementModal.appDropdwn.reset_Dropdown();

		SecurityAdmin_RolesManagementModal.verifyErrorMessage_ForGivenField(SecurityAdmin_RolesManagementModal.appDropdwn.dropdown(), "This field is required.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT6704_RoleManage_UpdateRole_VerifyRequiredField_ApplicationScopeRequired_ApplicationReAdded() throws Throwable {
		String roleName = "test_NameFieldRequired3";
		String roleDescription = "testDescription1";
		String roleApplication = "Framework";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleName, roleDescription, roleApplication);
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		SecurityAdmin_RolesManagementModal.appDropdwn.reset_Dropdown();
		SecurityAdmin_RolesManagementModal.appDropdwn.select_EntityValue("Framework");
		BaseUI.verifyElementEnabled(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		if (Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed()) {
			SecurityAdmin_RolesManagementModal.click_Cancel();
		}
		
		if(!BaseUI.getTextFromInputBox(SecurityAdministration.roleGrid.filter_textbox_ByColumnName("Name")).equals(""))
		{
			SecurityAdministration.roleGrid.filter_textbox_ClearFilter("Name");	
		}
		
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		if (BaseUI.pageSourceContainsString("Getting the role summaries failed")) {
			BaseUI.click(BaseUI.waitForElementToBeClickable("error_CloseButton", null, null));
			Thread.sleep(500);
		}
		
		try {
			Navigation.navigate_Admin_Entities();
			SecurityAdministration.expandNode(parentEntity);
			if (BaseUI.pageSourceContainsString(testEntity)) {
				SecurityAdministration.deleteEntity_withRoles(testEntity);
				SecurityAdministration.expandNode(parentEntity);
			}
			
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
