package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.Reports;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Reports_UserPermissions_ViewSecurity_SecurityAdminReports_UserActivityReport_Enabled_Tests extends BaseTest {

	// https://prod.practitest.com/p/5395/tests/974476/edit#steps_tab

	String mainHandle = "";
	String groupingToCheck = "Security Admin Reports";
	String report = "User Activity Report";
	final String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");
		LoginPage.login(GlobalVariables.reportsPermission_Entity, GlobalVariables.reportsPermission_User,
				GlobalVariables.reportsPermission_Password, 0);
		mainHandle = Browser.driver.getWindowHandle();
	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT7092_Verify_Reports_MenuOptionDisplayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
	}

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT7092_Navigate_Reports() throws Throwable {
		Navigation.navigate_Reports();
		Reports.verify_Reports_Title_Appears();
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT7092_Verify_SecurityAdminReportsGrouping_Visible_ButCollapsed() throws Throwable {

		Reports.verify_GroupingAppears_ByGroupingText(groupingToCheck);
		Reports.verify_Group_ExpandedOrCollapsed(groupingToCheck, false);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT7092_Click_SecurityAdminReportsGrouping_Uncollapses() throws Throwable {
		Reports.click_groupingHeader(groupingToCheck);
		Reports.verify_Group_ExpandedOrCollapsed(groupingToCheck, true);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT7092_Verify_SecurityAdminReportsGrouping_ReportListMatches() throws Throwable {
		String[] expectedEntries = { "Group Profile Report", "User Activity Report" };
		Reports.verify_Grouping_Displayed_Matches_expected(groupingToCheck, expectedEntries);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT7092_Verify_SecurityAdminReportsGrouping_UserActivityReport_RightPanel_GenerateReportAs()
			throws Throwable {
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(groupingToCheck, report);
		Reports.generateReportAs_dropdown.verify_SelectedDropdownValue("PDF");

	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT7092_Verify_SecurityAdminReportsGrouping_UserActivityReport_RightPanel_StartDateAppears()
			throws Throwable {
		Reports.startDate_DatePicker.verify_DropdownDisplayed();
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT7092_Verify_SecurityAdminReportsGrouping_UserActivityReport_RightPanel_EndDateAppears()
			throws Throwable {
		Reports.endDate_DatePicker.verify_DropdownDisplayed();
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT7092_Verify_SecurityAdminReportsGrouping_UserActivityReport_RightPanel_User_HasCorrectValue() {
		String expectedEntityText = "Selected: WFS";
		Reports.userEntityExpanderDropdown.verify_CurrentText(expectedEntityText);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT7092_Verify_SecurityAdminReportsGrouping_UserActivityReport_RightPanel_HasCorrectValue() {
		Reports.eventType_dropdown.verify_SelectedDropdownValue("Select one...");
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT7092_Verify_SecurityAdminReportsGrouping_UserActivityReport_RightPanel_GenerateReportButtonAppears() {

		BaseUI.verifyElementAppears(Locator.lookupElement("reports_GenerateReportButton"));
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT7092_Click_GenerateReport_Verify_ReportTitle() throws Throwable {
		String expectedTitle = "Security Administration User Activity Report";

		Reports.click_GenerateReport_Button();
		Reports.verify_PDF_Title_MatchesExpected(expectedTitle);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			BaseUI.close_ExtraWindows(mainHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
