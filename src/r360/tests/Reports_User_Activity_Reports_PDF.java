package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.Navigation;
import r360.pages.Reports;
import utils.*;

public class Reports_User_Activity_Reports_PDF extends BaseTest {

	private String mainHandle;
	
	@BeforeClass(alwaysRun=true)
	public void setup() throws Exception {

		Reports.getToPage("firefox");
		mainHandle = Browser.driver.getWindowHandle();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 10)
	public void PT12683_UserActivityReport() throws Exception {
		String startDate = "12/20/2017";
		String endDate = "12/20/2017";
		String expectedTitle = "User Activity Report";

		DataBuilder.delete_Files_WithExtention_FromLocation(".PDF", Browser.downloadLocation);
		Reports.expand_groupingHeader(Reports.receivables360_Online_ReportsText);
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, "User Activity Report");
		Reports.select_UserActivityReport_Values("PDF", startDate, endDate, "Auto Esther", "Page View");
		Reports.click_GenerateReport_Button();
		BaseUI.waitForElementToBeDisplayed("reportsPDF_UserActivityReport_ReportCreated", "", "", 5);
		Reports.verify_PDF_FirstRow_HasValues(startDate, "12/21/2017", "WFS", null, null);
		Reports.verify_PDF_Title_MatchesExpected(expectedTitle);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		BaseUI.close_ExtraWindows(mainHandle);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
