package r360.tests;

import java.util.Date;
import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.data.ImportData;
import r360.pages.*;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

// https://prod.practitest.com/p/5395/tests/1306740/edit
// https://prod.practitest.com/p/5395/tests/1003643/edit

public class Notifications extends BaseTest {
	private Date _startDate;
	private String _message = "FIT Simple File Drop Test";
	private String _attachmentFile;
	
	@BeforeClass(alwaysRun=true)
	public void setup_method() throws Throwable {
		_startDate = new Date();
		_attachmentFile = Notifications_Page.getAttachmentName(_startDate);
		//Step 3
		ImportData.fit_SimpleFileDrop(_attachmentFile, _startDate, _message);
		Notifications_Page.getToPage();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT12506_checkFitLogForErrors() throws Exception
	{
		//Step 4
		BaseUI.verify_false_AndLog(
				Notifications_Page.CheckLogForErrors(_startDate), 
				"No Errors were found in log.", 
				"Errors were logged."
		);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT11581_checkFitLogForMessageList() throws Exception
	{
		//Step 4
		BaseUI.verify_true_AndLog(
				Notifications_Page.CheckLogForMileMarkers(_startDate), 
				"Log indicates the drop processed successfully.", 
				"Not all the proper log messages where found."
		);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT12506_checkImportSuccessfullyThruQueue() throws Exception
	{
		//Steps 5 - 6
		BaseUI.verify_true_AndLog(
				Notifications_Page.checkDataImportQueue("Auto_SimpleFileDrop_00", _startDate),
				"Found Successful Data Import Queue record.", 
				"Failed to find a Successful Data Import Queue record."
		);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT12506_checkNotificationPage() throws Exception
	{
		//Steps 7-8
		Notifications_Page.enterAttachmentName(_attachmentFile);
		Notifications_Page.enterDateRange(_startDate, _startDate);
		Notifications_Page.clickSearch();
		TableData table = Notifications_Page.getResults();
		BaseUI.baseStringCompare("First row's message", _message, table.dataRow(0).get("Message"));
		Notifications_Page.clickFirstRow();
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
	
