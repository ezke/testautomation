package r360.tests;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.*;
import utils.BaseUI;
import utils.Browser;
import utils.Is;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Dashboard_Tests extends BaseTest {


    @BeforeClass(alwaysRun=true)
    public void setup_method() throws Exception {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12346_Steps2to3_DashboardAmountAndCountsSummaryReceivablesSummarySections() {
        BaseUI.assertThat("Check if pie charts are displayed", Dashboard.arePieChartsVisible(), Is.equalTo(true));
        BaseUI.assertThat("Check if Receivables Summary grid is displayed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(true));
        BaseUI.assertThat("Check if Collapse Buttons are displayed", Dashboard.areCollapseButtonsVisible(), Is.equalTo(true));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12346_Step4_DashboardMinAmountsAndCounts() {
        Dashboard.collapseAmountsAndCounts();
        BaseUI.assertThat("Check if pie charts are collapsed", Dashboard.arePieChartsVisible(), Is.equalTo(false));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12346_PT12350_Step4_DashboardMinReceivablesSummary() {
        Dashboard.collapseAmountsAndCounts();
        Dashboard.collapseReceivablesSummary();
        BaseUI.assertThat("Check if Receivables Summary grid is collapsed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(false));
        BaseUI.assertThat("Check if Expand Buttons are displayed", Dashboard.areExpandButtonsVisible(), Is.equalTo(true));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12346_Step5_DashboardExpandsAmountsAndCounts() {
        Dashboard.collapseAmountsAndCounts();
        Dashboard.collapseReceivablesSummary();
        Dashboard.expandAmountsAndCounts();
        BaseUI.assertThat("Check if pie charts are expanded", Dashboard.arePieChartsVisible(), Is.equalTo(true));
        BaseUI.assertThat("Ensure that Receivables Summary grid is still collapsed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(false));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12346_Step5_DashboardExpandsReceivablesSummary() {
        Dashboard.collapseReceivablesSummary();
        Dashboard.expandReceivablesSummary();
        BaseUI.assertThat("Check if Receivables Summary grid is expanded", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(true));
        BaseUI.assertThat("Check if Collapse Buttons are displayed", Dashboard.areCollapseButtonsVisible(), Is.equalTo(true));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12371_Steps1to4_MinStateIsRetainedThruSession_AfterBatchSummary() throws Exception {
        Dashboard.collapseAmountsAndCounts();
        Navigation.navigate_BatchSummary();
        Navigation.navigate_Dashboard();
        BaseUI.assertThat("Check if pie charts are collapsed", Dashboard.arePieChartsVisible(), Is.equalTo(false));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12371_Steps5to7_MinStateIsRetainedThruSession_AfterEntities() throws Throwable {
        Dashboard.collapseAmountsAndCounts();
        goOnRabbitTrail();
        Navigation.navigate_Dashboard();
        BaseUI.assertThat("Check if pie charts are collapsed", Dashboard.arePieChartsVisible(), Is.equalTo(false));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12371_Step8_MinStateIsRetainedThruSession_AfterEntities() throws Throwable {
        Dashboard.collapseReceivablesSummary();
        goOnRabbitTrail();
        Navigation.navigate_Dashboard();
        BaseUI.assertThat("Check if Receivables Summary grid is collapsed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(false));
    }

    private BigDecimal roundToHundredths(BigDecimal value) {
        return value.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12153_Step3_PercentagesTotalOneHundred() throws Throwable {
        ArrayList<Dashboard.LegendItem> legendItems=Dashboard.return_AmountsPieChartLegendList();
        BigDecimal total = legendItems.stream().map(item->item.getPercent()).reduce(BigDecimal.ZERO, BigDecimal::add);
        total=roundToHundredths(total);
        BaseUI.assertThat("Check if percentages of the Amounts Legend total 100", total, Is.equalTo(new BigDecimal("100.00")));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12153_Step4_PercentagesTotalOneHundred() throws Throwable {
        ArrayList<Dashboard.LegendItem> legendItems=Dashboard.return_PaymentsPieChartLegendList();
        BigDecimal total = legendItems.stream().map(item->item.getPercent()).reduce(BigDecimal.ZERO, BigDecimal::add);
        total=roundToHundredths(total);
        BaseUI.assertThat("Check if percentages of the Payments Legend total 100", total, Is.equalTo(new BigDecimal("100.00")));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12154_Step3_ValuesTotalMatchesTotalAmount() throws Throwable {
        BigDecimal displayedTotal = Dashboard.return_AmountsDisplayedTotal();
        ArrayList<Dashboard.LegendItem> legendItems=Dashboard.return_AmountsPieChartLegendList();
        BigDecimal total = legendItems.stream().map(item->item.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
        total=roundToHundredths(total);
        displayedTotal=roundToHundredths(displayedTotal);
        BaseUI.assertThat("Check if amounts of the Amounts Legend total the displayed total", total, Is.equalTo(displayedTotal));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT112154_Step4_PercentagesTotalOneHundred() throws Throwable {
        BigDecimal displayedTotal = Dashboard.return_PaymentsDisplayedTotal();
        ArrayList<Dashboard.LegendItem> legendItems=Dashboard.return_PaymentsPieChartLegendList();
        BigDecimal total = legendItems.stream().map(item->item.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
        total=roundToHundredths(total);
        displayedTotal=roundToHundredths(displayedTotal);
        BaseUI.assertThat("Check if percentages of the Payments Legend total the displayed total", total, Is.equalTo(displayedTotal));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12373_Step2_ValidateTheDDAGroupingIsAvailable() {
        BaseUI.assertThat("Check if DDA grouping is available", Dashboard.isDDAGroupingVisible(), Is.equalTo(true));
	}

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12369_Steps2to3_MinAmountsAndCountsRemainUponRefresh() {
        Dashboard.collapseAmountsAndCounts();
        Dashboard.clickRefresh();
        Dashboard.WaitForPageToLoad();
        BaseUI.assertThat("Check if pie charts are collapsed", Dashboard.arePieChartsVisible(), Is.equalTo(false));
        BaseUI.assertThat("Check if Receivables Summary grid is collapsed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(true));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12369_Steps4to5_MinBothRemainUponRefresh() {
        Dashboard.collapseAmountsAndCounts();
        Dashboard.collapseReceivablesSummary();
        Dashboard.clickRefresh();
        Dashboard.WaitForPageToLoad();
        BaseUI.assertThat("Check if pie charts are collapsed", Dashboard.arePieChartsVisible(), Is.equalTo(false));
        BaseUI.assertThat("Check if Receivables Summary grid is collapsed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(false));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12369_Steps6to7_MinReceivablesSummaryRetainedUponRefresh() {
        Dashboard.collapseReceivablesSummary();
        Dashboard.clickRefresh();
        Dashboard.WaitForPageToLoad();
        BaseUI.assertThat("Check if pie charts are collapsed", Dashboard.arePieChartsVisible(), Is.equalTo(true));
        BaseUI.assertThat("Check if Receivables Summary grid is collapsed", Dashboard.isReceivablesSummaryVisible(), Is.equalTo(false));
    }

    @Test(groups = { "all_Tests", "critical_Tests" })
    public void PT12412_OnlyShowPaymentTypesOfWorkgroup() throws Exception {
        loginAsADifferentUser(GlobalVariables.automationImports_Entity, GlobalVariables.automationImports_User, GlobalVariables.automationImports_Password);
        ArrayList<Dashboard.LegendItem> amountsLegend = Dashboard.return_AmountsPieChartLegendList();
        BaseUI.assertThat(amountsLegend.stream().map(item->item.getName()).toArray(), Is.equalTo(new String[]{"Check"}));
    }

    private void loginAsADifferentUser(String entity, String user, String password) throws Exception {
        Navigation.signOut();
        Navigation.return_Application();
        LoginPage.login(entity, user, password);
    }

    private void goOnRabbitTrail() throws Throwable {
        Navigation.navigate_Admin_Entities();
        Navigation.navigate_Admin_Workgroups();
        EntitySelector.selectNodePath("AutomationTest/AutomationImports");
        Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
        Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
        Admin_Workgroups.launch_EditWorkGroupSettings_Modal("Post Deposit Test Workgroup");
        Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DataEntryFields_Tab();
        Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        if(!Dashboard.arePieChartsVisible()) {
            Dashboard.expandAmountsAndCounts();
        }
        if(!Dashboard.isReceivablesSummaryVisible()) {
            Dashboard.expandReceivablesSummary();
        }
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}