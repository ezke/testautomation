package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.Reports;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.FileOperations;
import utils.ResultWriter;
import utils.TableData;

public class Reports_VerifyUserActivityReport_CSV_FileFormat_Tests extends BaseTest {

	// https://prod.practitest.com/p/5395/tests/969103/edit#steps_tab

	String mainHandle = "";
	String report = "User Activity Report";
	String generateReportAs = "CSV";
	// String eventType = "Reports";
	String eventType = "-- All --";

	String userGroups = "WFS-All Users";
	TableData csvResults;

	final String previousDate = BaseUI.getDateAsString_InRelationToTodaysDate(-30, "MM/dd/yyyy");
	final String laterDate = BaseUI.getDateAsString_InRelationToTodaysDate(30, "MM/dd/yyyy");

	String entity = "WFS";
	String timeRequested = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		DataBuilder.delete_Files_WithExtention_FromLocation(".CSV", Browser.downloadLocation);
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
		mainHandle = Browser.driver.getWindowHandle();
		Navigation.navigate_Reports();
		Reports.expand_groupingHeader(Reports.receivables360_Online_ReportsText);

		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, report);
		Reports.select_UserActivityReport_Values(generateReportAs, previousDate, laterDate, null, null);

		Reports.click_GenerateReport_Button();
		timeRequested = BaseUI.getDateAsString_InRelationToTodaysDate(0, Reports.reportCreated_TimeFormat);
		FileOperations.wait_For_File_To_Exist(Browser.downloadLocation + "\\" + Reports.userActivityReport_CSVFileName);
		csvResults = Reports.returnCSV_Results(Reports.userActivityReport_CSVFileName);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3117_Verify_CSV_FileGenerated_Rows_Match_TheFilters() throws Throwable {
		Reports.verify_Rows_MatcheExpectedValues(csvResults, entity, previousDate, laterDate, GlobalVariables.login,
				userGroups, eventType, timeRequested);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		csvResults = null;
		try {
			BaseUI.close_ExtraWindows(mainHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
