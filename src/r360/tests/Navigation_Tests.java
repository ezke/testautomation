package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Navigation_Tests extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 10, true);
    }

    // Notifications Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 1)
    public void PT12691_Notifications() throws Exception {
        Navigation.navigate_Notifications();
        BaseUI.verifyElementExists("notifications_FromDate_Input", null, null);
    }

    // Post-Deposit Exceptions Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 2)
    public void PT12685_Exceptions_PostDeposit_Exceptions() throws Exception {
        Navigation.navigate_Exceptions_PostDepositExceptions();
        BaseUI.verifyElementExists("postDepositExceptionsPageTitle", null, null);
    }

    // Pre-Depost Exceptions Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 3)
    public void PT12686_Exceptions_PreDeposit_Exceptions() throws Exception {
        Navigation.navigate_Exceptions_PreDepositExceptions();
        BaseUI.verifyElementExists("preDepositExceptionsPageTitle", null, null);
    }

    // Pre-Depost Exceptions Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 40)
    public void PT12687_Exceptions_RPS_Web_Exceptions() throws Exception {
        Navigation.navigate_Exceptions_RPS_Web_Exceptions();
        BaseUI.verifyElementExists("rpsWebExceptionsPageTitle", null, null);
    }

    /*
     * //RPS Web Exceptions Smoke Test
     *
     * @Test(groups = { "all_Tests", "smoke_Tests" }, priority = 4) public void
     * PT12686_Exceptions_RPS_Web_Exceptions () throws Exception {
     * LoginPage.login(GlobalVariables.entityName, GlobalVariables.login,
     * GlobalVariables.password);
     * Navigation.navigate_Exceptions_RPS_Web_Exceptions();
     * //BaseUI.verifyElementExists("rpsWebExceptionsPageTitle", null, null); }
     */

    // Advanced Search Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 50)
    public void PT12688_Advanced_Search() throws Exception {
        Navigation.navigate_Search_AdvancedSearch();
        BaseUI.verifyElementExists("advancedSearch_Title", null, null);
    }

    // Reports Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 60)
    public void PT12690_Reports() throws Exception {
        Navigation.navigate_Notifications();
        BaseUI.verifyElementExists("notifications_FromDate_Input", null, null);
    }

    // Alerts Manager Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 70)
    public void PT12705_Alerts_Manager() throws Exception {
        Navigation.navigate_Admin_Alerts_Manager();
        BaseUI.verifyElementExists("alertManager", null, null);
    }

    // User Preferences Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 80)
    public void PT12704_User_Preferences() throws Exception {
        Navigation.navigate_Admin_UserPreferences();
        BaseUI.verifyElementExists("userPreferences", null, null);
    }

    // Search Manage Queries Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 90)
    public void PT12689_Search_ManageQueries() throws Exception {
        Navigation.navigate_Search_ManageQueries();
        BaseUI.verifyElementExists("mngQueries_Page_Header", null, null);
    }

    // Payment Search Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 100)
    public void PT12706_Payment_Search() throws Exception {
        Navigation.navigate_Search_PaymentSearch();
        BaseUI.verifyElementExists("paymentSearch_SearchButton", null, null);
    }

    // Batch Summary Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 120)
    public void PT12692_Batch_Summary() throws Exception {
        Navigation.navigate_BatchSummary();
        BaseUI.verifyElementExists("batchSum_SearchButton", null, null);
    }

    // Batch Detail Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 130)
    public void PT12692_Batch_Detail() throws Exception {
        Navigation.navigate_BatchSummary();
        BatchSummary.setDateRange("12/11/2017", "12/11/2017");
        BatchSummary.workGroupSelector.search_forText("2234322");
        BatchSummary.click_Search();

        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        BaseUI.verifyElementExists("batchDetail_PrintButton", null, null);
    }

    // Extract Scheduler Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 140)
    public void PT12703_Extract_Scheduler() throws Exception {
        Navigation.navigate_Admin_Extracts_Configuration_Manager();
        BaseUI.verifyElementExists("admin_ExtractConfigManager_AddExtractDefinitionButton", null, null);
    }

    // User Management Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 150)
    public void PT12700_User_Management() throws Exception {
        Navigation.navigate_Admin_Users();
        BaseUI.verifyElementExists("admin_Users_Tab", null, null);
    }

    // Role Management Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 160)
    public void PT12701_Role_Management() throws Exception {
        Navigation.navigate_Admin_Roles();
        BaseUI.verifyElementExists("admin_Roles_Tab", null, null);
    }

    // Group Management Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 170)
    public void PT12702_Group_Management() throws Exception {
        Navigation.navigate_Admin_UserGroups();
        BaseUI.verifyElementExists("admin_Groups_Tab", null, null);
    }

    // Branding Smoke Test
    @Test(groups = {"all_Tests", "smoke_Tests"}, priority = 180)
    public void PT173_Group_Management() throws Exception {
        Navigation.navigate_Admin_Branding();
        BaseUI.verifyElementExists("branding_BrandingManager_Label", null, null);
    }

    @AfterMethod(alwaysRun = true)
    public void testTearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Browser.navigateTo(GlobalVariables.environment.BaseURL);
    }

    @AfterClass(alwaysRun = true)
    public void finalTearDown() throws Exception {
        if (BaseUI.elementAppears(Locator.lookupElement("Navigate_SignOut"))) {
            Navigation.signOut();
        }
        Browser.closeBrowser();
    }
}