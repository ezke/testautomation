package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.data.DataImportQueue;
import r360.data.ImportData;
import r360.data.R360DatabaseAccess;
import r360.pages.*;
import utils.BaseUI;
import utils.Browser;
import utils.Is;
import utils.ResultWriter;

import java.util.Date;

public class PostDepositExceptionsTests_AutomationImports extends BaseTest {
    PostDepositExceptionsTransactionDetailPage pdeTransactionDetailPage=null;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Date startDate=new Date();
        R360DatabaseAccess.PrepPostDepExcReimport("7890001", new Date(), "90016");
        R360DatabaseAccess.PrepPostDepExcReimport("7890002", new Date(), "90018");
        R360DatabaseAccess.PrepPostDepExcReimport("7890002", new Date(), "90019");
        R360DatabaseAccess.PrepPostDepExcReimport("7890004", new Date(), "90020");
        ImportData.import_Workgroup_7890001_Data_WithTodaysDate();
        ImportData.import_Workgroup_7890004_Data_WithTodaysDate();
        DataImportQueue.waitForImport("AutomationImageRPS", 1, startDate);
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        Browser.driver.manage().window().maximize();
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Exceptions_PostDepositExceptions();
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12274_AllowAcceptOfUnresolvedTransactionAfterConfirm() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.sortByDepositDateHeading(false);
        postDepositExceptionsPage.searchPostDeposit("90016");
        pdeTransactionDetailPage = postDepositExceptionsPage.selectFirstTransaction();
        pdeTransactionDetailPage = pdeTransactionDetailPage.clickAccept();
        BaseUI.assertThat(pdeTransactionDetailPage.getPageTitle(), Is.equalTo("Transaction 2 Details"));
        postDepositExceptionsPage= pdeTransactionDetailPage.returnToPostDepositExceptionsSummary();
        postDepositExceptionsPage.sortByDepositDateHeading(false);
        postDepositExceptionsPage=postDepositExceptionsPage.searchPostDeposit("90016");
        BaseUI.assertThat(postDepositExceptionsPage.getTransactionIDFromRow(0), Is.equalTo("2"));
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12278_Allow_Single_Decimal_Currency_Values() throws Exception {
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.sortByDepositDateHeading(false);
        postDepositExceptionsPage.searchPostDeposit("90016");
        pdeTransactionDetailPage = postDepositExceptionsPage.selectFirstTransaction();
        pdeTransactionDetailPage.addRelatedItem();
        pdeTransactionDetailPage.populateStub("MyFirstField", 1, "Fixed");
        pdeTransactionDetailPage.populateStub("MyFirstField", 2, "New");
        pdeTransactionDetailPage.populateStub("MyCurrencyField", 2, "3.0");
        pdeTransactionDetailPage = pdeTransactionDetailPage.clickNextTransaction();
        pdeTransactionDetailPage = pdeTransactionDetailPage.clickPreviousTransaction();
        BaseUI.assertThat(pdeTransactionDetailPage.queryStub("MyCurrencyField", 2) , Is.equalTo("$3.00"));
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12366_Make_Sure_Blank_Currency_Fields_Imports() throws Exception {
        Date startDate=new Date();
        ImportData.import_Data("PDE_ImportFile_WG_7890002_Blank_Amount.dat", ImportData.batchPaymentType.Cash, GlobalVariables.environment);
        DataImportQueue.waitForImport("AutomationImageRPS", 1, startDate);
        BaseUI.assertThat(DataImportQueue.checkFileSuccessfullyImported("AutomationImageRPS", 1, startDate), Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "critical_Tests"})
    public void PT12366_Validate_Save_Button_Functionality() throws Exception {
        Date startDate=new Date();
        ImportData.import_Data("PDE_ImportFile_WG_7890002_Without_Amount.dat", ImportData.batchPaymentType.Cash, GlobalVariables.environment);
        DataImportQueue.waitForImport("AutomationImageRPS", 1, startDate);
        PostDepositExceptionsPage postDepositExceptionsPage = new PostDepositExceptionsPage();
        postDepositExceptionsPage.sortByDepositDateHeading(false);
        postDepositExceptionsPage.searchPostDeposit("90019");
        pdeTransactionDetailPage = postDepositExceptionsPage.selectFirstTransaction();
        BaseUI.assertThat(pdeTransactionDetailPage.queryStubBackcolor("Amount", 1), Is.equalTo("rgba(169, 68, 66, 1)"));
        BaseUI.assertThat(pdeTransactionDetailPage.getLockedLabelOwner(), Is.equalTo("Dominic Giallombardo"));
        pdeTransactionDetailPage.populateStub("Amount", 1, "8.00");
        pdeTransactionDetailPage.savePostDeposit();
        postDepositExceptionsPage=pdeTransactionDetailPage.returnToPostDepositExceptionsSummary();
        postDepositExceptionsPage.sortByDepositDateHeading(false);
        postDepositExceptionsPage.searchPostDeposit("90019");
        pdeTransactionDetailPage = postDepositExceptionsPage.selectFirstTransaction();
        BaseUI.assertThat(pdeTransactionDetailPage.queryStub("Amount", 1), Is.equalTo("$8.00"));
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        if(pdeTransactionDetailPage!=null) {
            if(pdeTransactionDetailPage.ConfirmPageTitle()) {
                pdeTransactionDetailPage.returnToPostDepositExceptionsSummary();
                pdeTransactionDetailPage = null;
            }
        }
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
