package r360.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.Reports;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class Reports_Receivables360_UserActivityReport_PDF_Tests extends BaseTest {

	// https://prod.practitest.com/p/5395/tests/974476/edit#steps_tab

	String mainHandle = "";
	String report = "User Activity Report";
	final String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
	String startDate = "";
	String endDate = "";
	String expectedTime = "";
	String expectedTitle = "User Activity Report";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
		mainHandle = Browser.driver.getWindowHandle();
		startDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(todaysDate, "MM/dd/yyyy", "MM/dd/yyyy", -30);
		endDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(todaysDate, "MM/dd/yyyy", "MM/dd/yyyy", 30);
		Navigation.navigate_Reports();
		Reports.click_groupingHeader(Reports.receivables360_Online_ReportsText);
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, report);
		Reports.select_UserActivityReport_Values("PDF", startDate, endDate, null, null);

		expectedTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, Reports.reportCreated_TimeFormat);
		Reports.click_GenerateReport_Button();

	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3116_Verify_PDF_Title() throws Throwable {
		Reports.verify_PDF_Title_MatchesExpected(expectedTitle);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3116_Verify_PDF_Filters() throws Throwable {
		BaseUI.waitForElementToBeDisplayed("reportsPDF_UserActivityReport_ReportCreated", "", "", 5);
		Reports.verify_PDF_UserActivityReport_FiltersMatchExpected(GlobalVariables.login, "WFS-All Users", expectedTime,
				startDate, endDate, "-- All --");

	}
	

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3116_Verify_PDF_FirstRow() throws Throwable {
		Reports.verify_PDF_FirstRow_HasValues(startDate, endDate, "WFS", null, null);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			BaseUI.close_ExtraWindows(mainHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
