package r360.tests;

import org.testng.annotations.BeforeSuite;

import r360.data.ImportData;
import utils.Locator;
import utils.Selenium;

public abstract class BaseTest {
	@BeforeSuite(alwaysRun=true)
	public void config_setup_method() throws Throwable {
		Selenium.verifySeleniumIsRunning();
		ImportData.retrieve_EnvironmentData();
		Locator.loadObjectRepository(
				"\\src\\wfsCommon\\wfsCommonRepo.txt",
				"\\src\\r360\\r360ObjectRepo.txt");
	}
}
