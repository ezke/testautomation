package r360.tests;

import org.testng.annotations.Test;
import utils.BaseUI;
import java.io.*;

public class APISmokeTest {
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PingFIT() throws Exception {
		Process p = Runtime.getRuntime().exec(new String[] {"WebServiceCall\\WebServiceCall.exe", "https://rechubqaimg02.qalabs.nwk/FileImportServices/FileDataService.svc", "ping", "fit"});
		
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	
		String result = input.readLine(); 
		BaseUI.baseStringCompare("FIT Result", "Result: Pong", result);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 2)
	public void PingDIT() throws Exception {
		Process p = Runtime.getRuntime().exec(new String[] {"WebServiceCall\\WebServiceCall.exe", "https://rechubqaimg02.qalabs.nwk/DataImportServices/DataImportService.svc", "ping", "dit"});
		
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	
		String result = input.readLine(); 
		BaseUI.baseStringCompare("DIT Result", "Result: Pong", result);
	}
}
