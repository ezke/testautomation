package r360.EnvironmentDefinition;

import java.util.HashMap;

public class Environment {
    public String IntegraPayDBConnection = "";
    public String Name="";
	public String ClientDriveUNC="";
	public String ClientURL="";
	public String EDMDefintionFolder="";
	public String FileImportAttachmentFolder="";
	public String ClientLogFolder="";
	public String DBConnection="";
	public String BaseURL="";
	public String RAAMDBConnection = "";
	public String FrameworkDBConnection = "";
	public String SSOClientURL="";
	
	public Environment(String name, HashMap<String, String> data) {
		Name=name;
		ClientDriveUNC = data.get("ClientDriveUNC");
		ClientURL = data.get("ClientBaseURL");
		ClientLogFolder = data.get("ClientLogFolder");
		EDMDefintionFolder = data.get("EDMDefinitionFolder");
		DBConnection = data.get("DBConnection");
		BaseURL = data.get("BaseURL");
		FileImportAttachmentFolder =  data.get("FileImportAttachmentFolder");
		RAAMDBConnection = data.get("RAAMDBConnection");
		FrameworkDBConnection = data.get("FrameworkDBConnection");
		SSOClientURL = data.get("SSOClientURL");
		IntegraPayDBConnection = data.get("IntegraPayDBConnection");
	}

	/*
	    createDefaultEnvironment
	        Creates an instance of the Environment class populated with default values.  This is mostly
	        to allow methods to be upgraded to take an Environment class but need to be backwards compatible
	        with methods that do not have an Environment instance.  With this method an overload of the upgraded
	        method can me overloaded with its original signature and a default Environment instance can be
	        supplied when cascading into the new version of the method.
	 */
	public static Environment createDefaultEnvironment() {
		return new Environment("Default", new HashMap<String, String>() {{
			put("BaseURL","https://rechubqaweb02.qalabs.nwk/Framework/");
			put("ClientLogFolder","\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\Logfiles");
			put("ClientDriveUNC","\\\\rechubqaclnt02.qalabs.nwk\\e$");
			put("EDMDefinitionFolder","D:\\WFSApps\\RecHub\\ExtractDesignManager\\ExtractDefinitions");
			put("FileImportAttachmentFolder","E:\\WFSApps\\RecHub\\Data\\Attachments");
			put("DBConnection","jdbc:sqlserver://rechubqadb02.qalabs.nwk;user=sa;password=Wausau#1;databaseName=WFSDB_R360");
			put("RAAMDBConnection","jdbc:sqlserver://rechubqadb02.qalabs.nwk;user=sa;password=Wausau#1;databaseName=RAAM");
			put("FrameworkDBConnection","jdbc:sqlserver://rechubqadb02.qalabs.nwk;user=sa;password=Wausau#1;databaseName=WFSPortal");
		}});
	}
}
