package r360.dataPrep;

import r360.data.ImportData;
import r360.pages.GlobalVariables;

public class Import_Workgroups {

    public static void main(String[] args) throws Exception {

        ImportData.retrieve_EnvironmentData();

        System.out.println("Importing data for environment: " + GlobalVariables.environment.Name);

        ImportData.import_Workgroup_1123211_Data_WithTodaysDate();
        ImportData.import_Workgroup_2234322_Data_WithTodaysDate();
        ImportData.import_Workgroup_33338888_Data_WithTodaysDate();
        ImportData.import_Workgroup_7890001_Data_WithTodaysDate();
        ImportData.import_Workgroup_7890003_Data_WithTodaysDate();
    }
}
