package r360.tests_ignored.securityAdmin_Roles;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import wfsCommon.pages.EntitySelector;

public class RAAM_Search_RolesManagement extends BaseTest {

	private String testEntity = "testEntity_WithLotsOfStuff";
	private String testRole = "anotherTestRoleA0";
	private String testDescriptionToSave = "Test Description for Modify Role1";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

		Navigation.navigate_Admin_Entities();
		// SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode("WFS");

	}

	@Test
	public void PT6779_Search_RoleManage_ModifyDescription_AndCancel() throws Throwable {
		String newDescription = "Test Description for Modify Role2";

		EntitySelector.search_ByText("_Wi");
		EntitySelector.select_Results(testEntity);
		SecurityAdministration.navigate_toRolesTab();
		SecurityAdministration.launchEditRoleModal_forGivenRole(testRole);
		
		SecurityAdmin_RolesManagementModal.roleDescription.enterTextIntoField_AndTab(newDescription);
		SecurityAdmin_RolesManagementModal.click_Cancel();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description", newDescription, false);

		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);

	}

	@Test
	public void PT6778_Search_RoleManage_ModifyDescription_AndSave() throws Throwable {
		EntitySelector.search_ByText("_Wi");
		EntitySelector.select_Results(testEntity);
		SecurityAdministration.navigate_toRolesTab();
		
		if (BaseUI.pageSourceContainsString(testDescriptionToSave)) {
			SecurityAdministration.launchEditRoleModal_forGivenRole(testRole);
			SecurityAdmin_RolesManagementModal.roleDescription.enterTextIntoField_AndTab("");
			SecurityAdmin_RolesManagementModal.click_Save();
		}
	
		
		SecurityAdministration.launchEditRoleModal_forGivenRole(testRole);
		SecurityAdmin_RolesManagementModal.roleDescription.enterTextIntoField_AndTab(testDescriptionToSave);
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Description", testDescriptionToSave, true);

		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		if(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed())
		{
			SecurityAdmin_RolesManagementModal.click_Cancel();
		}
			
		EntitySelector.close_SearchDropdown();
		EntitySelector.reset_SearchResults();
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {

			if (BaseUI.pageSourceContainsString(testDescriptionToSave)) {
				SecurityAdministration.launchEditRoleModal_forGivenRole(testRole);
				SecurityAdmin_RolesManagementModal.roleDescription.enterTextIntoField_AndTab("");
				SecurityAdmin_RolesManagementModal.click_Save();
				
				Navigation.signOut();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
