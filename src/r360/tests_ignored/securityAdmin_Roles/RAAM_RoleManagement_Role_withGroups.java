package r360.tests_ignored.securityAdmin_Roles;

import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_RoleManagement_Role_withGroups extends BaseTest {

	private String parentEntity = "AutomationTest";
	private String testEntity = "automation_roleManage2";
	private String roleName = "testRoleGroups";
	private String roleNameToDelete = "testRoleToDelete";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

		Navigation.navigate_Admin_Roles();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.remove_AllGroups_FromGivenRole(roleName);
		SecurityAdministration.remove_AllPolicies_FromGivenRole(roleName);
		if (BaseUI.pageSourceContainsString(roleNameToDelete)) {
			SecurityAdministration.deleteRole_ForGivenRoleName_WithGroupsAndPolicies(roleNameToDelete);
		}

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

	}

	@Test
	public void PT6719_RoleManage_UpdateRole_AddMultipleGroupsToRole() throws Throwable {
		String group1 = "roleManagementTestGroup1: automation_roleManage2";
		String group2 = "roleManagementTestGroup2: automation_roleManage2";
		String group3 = "roleManagementTestGroup3: automation_roleManage2";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		ArrayList<WebElement> checkboxes = new ArrayList<WebElement>();
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group1));
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group2));
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group3));
		// Check all of the checkboxes.
		for (WebElement checkbox : checkboxes) {
			BaseUI.checkCheckbox(checkbox);
		}
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfUsers = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users", roleName);
		String numberOfGroups = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleName);

		BaseUI.baseStringCompare("Number of Users", "3", numberOfUsers);
		BaseUI.baseStringCompare("Number of Groups", "3", numberOfGroups);

	}

	@Test
	public void PT6717_RoleManage_UpdateRole_AddOneGroupToRole() throws Throwable {
		String group1 = "roleManagementTestGroup1: automation_roleManage2";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();

		SecurityAdmin_RolesManagementModal.check_AvailableGroupCheckbox(group1);

		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfUsers = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users", roleName);
		String numberOfGroups = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleName);

		BaseUI.baseStringCompare("Number of Users", "1", numberOfUsers);
		BaseUI.baseStringCompare("Number of Groups", "1", numberOfGroups);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.groupManage_AssociatedGroups.verifyEntryExists_OR_DoesntExist("Name", group1,
				true);

	}

	@Test
	public void PT6716_RoleManage_UpdateRole_AddOnePolicyToRole() throws Throwable {
		String policy1 = "Branding - Manage Branding Page";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();

		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);

		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);

		BaseUI.baseStringCompare("Number of Policies", "1", numberOfPolicies);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.policyManage_AssociatedPolicies
				.verifyEntryExists_OR_DoesntExist("Description", policy1, true);
	}

	@Test
	public void PT6718_RoleManage_UpdateRole_AddMultiplePoliciesToRole() throws Throwable {
		String policy1 = "Branding - Manage Branding Page";
		String policy2 = "Branding - View Branding Page";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();

		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy2);

		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);

		BaseUI.baseStringCompare("Number of Policies", "2", numberOfPolicies);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.policyManage_AssociatedPolicies
				.verifyEntryExists_OR_DoesntExist("Description", policy1, true);
		SecurityAdmin_RolesManagementModal.policyManage_AssociatedPolicies
				.verifyEntryExists_OR_DoesntExist("Description", policy2, true);
	}

	@Test
	public void PT6718_RoleManage_UpdateRole_AddPolicy_AndGroup() throws Throwable {
		String group1 = "anotherTest2: automation_roleManage2";
		String policy1 = "Branding - Manage Branding Page";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);

		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailableGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();

		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();

		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);
		String numberOfGroups = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleName);
		String numberOfUsers = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users", roleName);

		BaseUI.baseStringCompare("Number of Policies", "1", numberOfPolicies);
		BaseUI.baseStringCompare("Number of Groups", "1", numberOfGroups);
		BaseUI.baseStringCompare("Number of Users", "1", numberOfUsers);
	}

	@Test
	public void PT6753_RoleManage_CannotDeleteRole_HasGroup_And_Policy() throws Throwable {
		String group1 = "anotherTest2: automation_roleManage2";
		String policy1 = "Branding - Manage Branding Page";

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleNameToDelete, "desc", "Framework");

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleNameToDelete);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailableGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();

		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();

		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.verify_UnableToDelete_Role(roleNameToDelete);

		// BaseUI.checkCheckbox(
		// SecurityAdministration.roleGrid.check_box_for_GivenColumnAndText("Name",
		// roleNameToDelete));
		// BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		// String expectedErrorMessage = "The Role " + roleNameToDelete
		// + " has Groups and/or Policies associated with it and cannot be
		// deleted.";
		// BaseUI.verifyElementHasExpectedText("error_MessagePopup",
		// expectedErrorMessage);
		// BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_Roles_DeleteButton"));
		// BaseUI.uncheckCheckbox(
		// SecurityAdministration.roleGrid.check_box_for_GivenColumnAndText("Name",
		// roleNameToDelete));

	}

	@Test
	public void PT6753_RoleManage_CannotDeleteRole_HasGroup_And_Policy_AbleToRemoveAndDelete() throws Throwable {
		String group1 = "anotherTest2: automation_roleManage2";
		String policy1 = "Branding - Manage Branding Page";

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(roleNameToDelete, "desc", "Framework");

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleNameToDelete);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailableGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();

		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();

		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.verify_UnableToDelete_Role(roleNameToDelete);
		SecurityAdministration.click_DeleteRoleModal_CancelButton();
		
		SecurityAdministration.remove_AllGroups_FromGivenRole(roleNameToDelete);
		SecurityAdministration.remove_AllPolicies_FromGivenRole(roleNameToDelete);
		SecurityAdministration.deleteRole_ForGivenRoleName(roleNameToDelete);
		BaseUI.verify_false_AndLog(BaseUI.pageSourceContainsString(roleNameToDelete), "Role was successfully deleted.",
				"Role STILL exists");
	}

	@Test
	public void PT6708_RoleManage_UpdateRole_RemoveOnePolicy() throws Throwable {
		String policy1 = "Branding - Manage Branding Page";
		String policy2 = "Branding - View Branding Page";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();

		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy2);

		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);

		BaseUI.baseStringCompare("Number of Policies", "2", numberOfPolicies);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AssociatedPolicyCheckbox(policy2);
		SecurityAdmin_RolesManagementModal.remove_selectedPolicies_FromAssociatedGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);
		BaseUI.baseStringCompare("Number of Policies", "1", numberOfPolicies);
	}

	@Test
	public void PT6709_RoleManage_UpdateRole_RemoveOneGroup() throws Throwable {
		String group1 = "roleManagementTestGroup1: automation_roleManage2";
		String group2 = "roleManagementTestGroup2: automation_roleManage2";
		String group3 = "roleManagementTestGroup3: automation_roleManage2";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		ArrayList<WebElement> checkboxes = new ArrayList<WebElement>();
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group1));
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group2));
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group3));
		// Check all of the checkboxes.
		for (WebElement checkbox : checkboxes) {
			BaseUI.checkCheckbox(checkbox);
		}
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfUsers = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users", roleName);
		String numberOfGroups = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleName);

		BaseUI.baseStringCompare("Number of Users", "3", numberOfUsers);
		BaseUI.baseStringCompare("Number of Groups", "3", numberOfGroups);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AssociatedGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.remove_selectedGroups_FromAssociatedGrid();
		SecurityAdmin_RolesManagementModal.click_Save();

		numberOfUsers = SecurityAdministration.roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users",
				roleName);
		numberOfGroups = SecurityAdministration.roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups",
				roleName);

		BaseUI.baseStringCompare("Number of Users", "2", numberOfUsers);
		BaseUI.baseStringCompare("Number of Groups", "2", numberOfGroups);
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.verifyEntryExists_OR_DoesntExist("Name", group1,
				true);
	}

	@Test
	public void PT6709_RoleManage_UpdateRole_RemoveMultipleGroups() throws Throwable {
		String group1 = "roleManagementTestGroup1: automation_roleManage2";
		String group2 = "roleManagementTestGroup2: automation_roleManage2";
		String group3 = "roleManagementTestGroup3: automation_roleManage2";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		ArrayList<WebElement> checkboxes = new ArrayList<WebElement>();
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group1));
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group2));
		checkboxes.add(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups
				.check_box_for_GivenColumnAndText("Name", group3));
		// Check all of the checkboxes.
		for (WebElement checkbox : checkboxes) {
			BaseUI.checkCheckbox(checkbox);
		}
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfUsers = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users", roleName);
		String numberOfGroups = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleName);

		BaseUI.baseStringCompare("Number of Users", "3", numberOfUsers);
		BaseUI.baseStringCompare("Number of Groups", "3", numberOfGroups);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AssociatedGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.check_AssociatedGroupCheckbox(group2);
		SecurityAdmin_RolesManagementModal.remove_selectedGroups_FromAssociatedGrid();
		SecurityAdmin_RolesManagementModal.click_Save();

		numberOfUsers = SecurityAdministration.roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users",
				roleName);
		numberOfGroups = SecurityAdministration.roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups",
				roleName);

		BaseUI.baseStringCompare("Number of Users", "1", numberOfUsers);
		BaseUI.baseStringCompare("Number of Groups", "1", numberOfGroups);
		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.verifyEntryExists_OR_DoesntExist("Name", group1,
				true);
		SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.verifyEntryExists_OR_DoesntExist("Name", group2,
				true);
	}

	@Test
	public void PT6708_RoleManage_UpdateRole_RemoveMultiplePolicies() throws Throwable {
		String policy1 = "Branding - Manage Branding Page";
		String policy2 = "Branding - View Branding Page";

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();

		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy2);

		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);

		BaseUI.baseStringCompare("Number of Policies", "2", numberOfPolicies);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AssociatedPolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.check_AssociatedPolicyCheckbox(policy2);
		SecurityAdmin_RolesManagementModal.remove_selectedPolicies_FromAssociatedGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		numberOfPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleName);
		BaseUI.baseStringCompare("Number of Policies", "", numberOfPolicies);

		SecurityAdministration.launchEditRoleModal_forGivenRole(roleName);
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.policyManage_AvailablePolicies
				.verifyEntryExists_OR_DoesntExist("Description", policy1, true);
		SecurityAdmin_RolesManagementModal.policyManage_AvailablePolicies
				.verifyEntryExists_OR_DoesntExist("Description", policy2, true);
	}

	@Test
	public void PT6761_and_PT6686_RoleManage_AddRole_SaveButton_NoLongerGreyedOut_AfterRequiredInfo()
			throws Throwable {
		String policy1 = "Branding - Manage Branding Page";
		String group1 = "roleManagementTestGroup1: automation_roleManage2";
		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleNameToDelete, "", "Framework");
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailableGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		String numUsers = SecurityAdministration.roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users",
				roleNameToDelete);
		String numPolicies = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies", roleNameToDelete);
		String numGroups = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleNameToDelete);
		BaseUI.baseStringCompare("Number of Users", "1", numUsers);
		BaseUI.baseStringCompare("Number of Policies", "1", numPolicies);
		BaseUI.baseStringCompare("Number of Groups", "1", numGroups);
		BaseUI.verifyElementDoesNOTExist("error_MessagePopup", null, null);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal"));
	}

	@Test
	public void PT6687_RoleManage_DeleteRole() throws Throwable {
		String policy1 = "Branding - Manage Branding Page";
		String group1 = "roleManagementTestGroup1: automation_roleManage2";

		if (BaseUI.pageSourceContainsString(roleNameToDelete)) {
			SecurityAdministration.deleteRole_ForGivenRoleName(roleNameToDelete);
		}

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields(roleNameToDelete, "", "Framework");
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailableGroupCheckbox(group1);
		SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.navigate_PolicyManagementTab();
		SecurityAdmin_RolesManagementModal.check_AvailablePolicyCheckbox(policy1);
		SecurityAdmin_RolesManagementModal.move_selectedPolicies_FromAvailableGrid();
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.remove_AllGroups_FromGivenRole(roleNameToDelete);
		SecurityAdministration.remove_AllPolicies_FromGivenRole(roleNameToDelete);
		SecurityAdministration.deleteRole_ForGivenRoleName(roleNameToDelete);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", roleNameToDelete, false);
	}

	@Test
	public void PT6685_RoleManage_EditRole_ChangeDescription() throws Throwable {
		String newRoleName = "newRoleToEdit264268";
		String roleDescription = "test_RoleDescription";
		String roleDescription2 = "test_roleDescriptionChange";
		String roleApp = "Framework";

		if (BaseUI.pageSourceContainsString(newRoleName)) {
			SecurityAdministration.deleteRole_ForGivenRoleName(newRoleName);
		}

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(newRoleName, roleDescription, roleApp);
		SecurityAdministration.launchEditRoleModal_forGivenRole(newRoleName);
		SecurityAdmin_RolesManagementModal.roleDescription.enterTextIntoField_AndTab(roleDescription2);
		SecurityAdmin_RolesManagementModal.click_Save();
		String roleDescriptionOnPage = SecurityAdministration.roleGrid
				.get_CellText_ByHeaderName_AndRowIdentifyingText("Description", newRoleName);
		BaseUI.baseStringCompare("Role Description", roleDescription2, roleDescriptionOnPage);
		BaseUI.verifyElementDoesNOTExist("error_MessagePopup", null, null);
	}

	@Test
	public void PT6685_RoleManage_EditRole_ChangeName() throws Throwable {
		String newRoleName = "newRoleToEdit264264";
		String roleDescription = "test_RoleDescription";
		String newRoleName2 = "newRoleToEdit264264_2";
		String roleApp = "Framework";

		if (BaseUI.pageSourceContainsString(newRoleName2)) {
			SecurityAdministration.deleteRole_ForGivenRoleName(newRoleName2);
		}

		if (BaseUI.pageSourceContainsString(newRoleName)) {
			SecurityAdministration.deleteRole_ForGivenRoleName(newRoleName);
		}

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.addRole(newRoleName, roleDescription, roleApp);
		SecurityAdministration.launchEditRoleModal_forGivenRole(newRoleName);
		SecurityAdmin_RolesManagementModal.roleName.enterTextIntoField_AndTab(newRoleName2);
		SecurityAdmin_RolesManagementModal.click_Save();
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", newRoleName2, true);
		SecurityAdministration.roleGrid.verifyEntryExists_OR_DoesntExist("Name", newRoleName, false);
		BaseUI.verifyElementDoesNOTExist("error_MessagePopup", null, null);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		// Check for the error message from one of our tests.
		Navigation.close_ToastError_If_ItAppears();
		// if (BaseUI.pageSourceContainsString("has Groups and/or Policies
		// associated with it and cannot be deleted.")
		// || BaseUI.pageSourceContainsString("Getting the role summaries
		// failed")) {
		// BaseUI.click(Locator.lookupElement("error_CloseButton"));
		// Thread.sleep(500);
		// }

		if (BaseUI.elementAppears(Locator.lookupElement("scrtyAdmin_Roles_DeleteModal"))) {
			SecurityAdministration.click_DeleteRoleModal_CancelButton();
		}

		if (Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed()) {
			SecurityAdmin_RolesManagementModal.click_Cancel();
		}

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.roleGrid.filter_textbox_SendKeys("Name", roleNameToDelete);
		if (BaseUI.pageSourceContainsString(roleNameToDelete)) {
			SecurityAdministration.deleteRole_ForGivenRoleName_WithGroupsAndPolicies(roleNameToDelete);
		}
		SecurityAdministration.roleGrid.filter_textbox_ClearFilter("Name");

		SecurityAdministration.remove_AllGroups_FromGivenRole(roleName);
		SecurityAdministration.remove_AllPolicies_FromGivenRole(roleName);
		SecurityAdministration.selectNode(testEntity);

		if (BaseUI.pageSourceContainsString("has Groups and/or Policies associated with it and cannot be deleted.")
				|| BaseUI.pageSourceContainsString("Getting the role  summaries failed")) {
			BaseUI.click(Locator.lookupElement("error_CloseButton"));
			Thread.sleep(500);
		}
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
