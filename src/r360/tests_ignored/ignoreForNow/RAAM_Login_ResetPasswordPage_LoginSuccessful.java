package r360.tests_ignored.ignoreForNow;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mailosaur.model.Email;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;

public class RAAM_Login_ResetPasswordPage_LoginSuccessful extends BaseTest {
	
	private String newEntity = "Auto_Email_TestLogin";
	private String entityType = "Financial Institution";
	private String entityDescription = "email test Description";
	private String entityExternalID = "email test ID 92";
	String columnName = "Login Name";
	String columnText = "emaillogin";
	String loginName = columnText;
	String firstName = "Tom";
	String lastName = "Cruise";
	String userPassword = "Wausau#1";
	String email = "testingr360email" + Email_API.email_Ending;
	String oldPassword = userPassword;
	String newPassword = "Microsoft#1";

	String linkFromEmail = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Email_API.delete_Emails_ByRecipient(email);
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
		Navigation.navigate_Admin_Entities();
		if (BaseUI.pageSourceContainsString(newEntity)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();
	}

	@Test(groups = { "all_Tests" }, priority = 1)
	public void PT6486_PasswordEmail_RecievedEmail() throws Exception {
		LoginPage.clickForgotPasswordLink();
		String emailSubject1 = "Reset Password";
		LoginPage.resetForgotPasswordForm(newEntity, loginName, emailSubject1);

		Email_API.Wait_ForEmail_ToExist(email, 60);
		Email firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		linkFromEmail = Email_API.return_FirstLink_FromEmail(firstEmail);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "Did NOT find email.");
	}

	@Test(groups = { "all_Tests" }, priority = 2)
	public void PT6486_PasswordEmail_PasswordResetModalAppears_AfterNavigatingToLink() throws Exception {
		Browser.navigateTo(linkFromEmail);
		Navigation.navigate_Past_CertificateError();
		EmailChecker.verifyResetPasswordModalAppears();
	}

	@Test(groups = { "all_Tests" }, priority = 3)
	public void PT6498_PasswordEmail_ResetPassword_Successful() throws Exception {
		EmailChecker.enterResetPasswordForm(newEntity, columnText, newPassword);
		Browser.navigateTo(GlobalVariables.baseURL);
		LoginPage.login(newEntity, columnText, newPassword, 0, false);
		
		HomePage.verifyHeaderTitleAppears();

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			Email_API.delete_Emails_ByRecipient(email);
			if (BaseUI.elementAppears(Locator.lookupElement("Navigate_SignOut"))) {
				Navigation.signOut();
			}
			Browser.navigateTo(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
			Navigation.navigate_Admin_Entities();
			if (BaseUI.pageSourceContainsString(newEntity)) {
				SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
				SecurityAdministration.navigate_toEntityTab();
				SecurityAdministration.deleteEntity(newEntity);
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
