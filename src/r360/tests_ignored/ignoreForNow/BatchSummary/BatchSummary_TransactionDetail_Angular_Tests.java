package r360.tests_ignored.ignoreForNow.BatchSummary;

import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.pages.TransactionDetailsPage;
import org.testng.asserts.SoftAssert;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import org.openqa.selenium.WebDriver;
import utils.ResultWriter;

public class BatchSummary_TransactionDetail_Angular_Tests extends BaseTest {

    public WebDriver driver;
	private String date1 =  BaseUI.getDateAsString_InRelationToTodaysDate(0,"MM/dd/yyyy");
	private String date2 = BaseUI.getDateAsString_InRelationToTodaysDate(1, "MM/dd/yyyy");
	String workgroup = "2234322 - Automation-Filter";
	private Integer rowNumber = 1;

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		driver=Browser.openBrowser(GlobalVariables.baseURL);
		//driver=Browser.openBrowser("https://r36016qaweb01.qalabs.nwk/Framework/");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

	}

	@Test(groups = {"all_Tests","regression_Tests","module_r360_DashboardBatchSummaryTransactionDetails" })
	public void DashboardBatchSummaryTransactionDetailsTest() throws Exception {
		TransactionDetailsPage TransactionDetailsPage = new TransactionDetailsPage();
		SoftAssert softAssert = new SoftAssert();
		Dashboard.receivables_depositDate.enter_Date(date1);
		Dashboard.click_OnFirstRowReceivablesTable();
		BatchSummary.click_Cell_BatchSummary("Batch", 1);
		BatchSummary.click_Cell_BatchDetail("Transaction", 1);

		//check Transaction Detail Header
		softAssert.assertTrue(TransactionDetailsPage.isTransactionDetailPageHeaderVisible(), "Transaction Details Page Header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDashboardInHeaderPathLinkVisible(), "Dashboard In Header Path Link is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchSummaryInHeaderPathLinkVisible(),"BatchSummary In Header Path Link is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchDetailInHeaderPathLinkVisible(), "Batch Detail In Header Path Link is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPrintIconInheaderVisible(), "Print Icon is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRefreshIconInHeaderVisible(), "Refresh Icon is visible");
		//check Transaction Details
		softAssert.assertTrue(TransactionDetailsPage.isTransactionDetailsLabelVisible(),"Transaction Details Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBankLabelVisible(),"Bank Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isWorkgroupLabelVisible(), "Workgroup Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDepositDateLabelVisible(), "Deposit Date Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchLabelVisible(), "Batch Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isAccountSiteCodeLabelVisible(), "Account Site Code Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchSiteCodeLabelVisible(), "Batch Site Code Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchCueIDLabelVisible(),"Batch Cue ID Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isTransactionLabelVisible(), "Transaction Label is visible");
		//check Transaction Detail Payment Items
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsheaderVisible(), "Payment Items header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsSearchFieldVisible(),"Payment Items Search Field is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShowEntriesVisible(), "Payment Items Show Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow1000EntriesVisible(), "Payment Items Show 1000 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow100EntriesVisible(), "Payment Items Show 100 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow50EntriesVisible(),"Payment Items Show 50 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow25EntriesVisible(),"Payment Items Show 25 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow10EntriesVisible(), "Payment Items Show 10 Entries is visible");
		//check Transaction Detail Related Items
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsheaderVisible(), "Related Items header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsSearchFieldVisible(),"Related Items Search Field is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShowEntriesVisible(), "Related Items Show Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow1000EntriesVisible(), "Related Items Show 1000 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow100EntriesVisible(), "Related Items Show 100 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow50EntriesVisible(),"Related Items Show 50 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow25EntriesVisible(),"Related Items Show 25 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow10EntriesVisible(), "Related Items Show 10 Entries is visible");
		//check Transaction Detail Documents
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsheaderVisible(),"Documents header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsSearchFieldVisible(),"Documents Search Field is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShowEntriesVisible(),"Documents Show Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow1000EntriesVisible(), "Documents Show 1000 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow100EntriesVisible(), "Documents Show 100 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow50EntriesVisible(),"Documents Show 50 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow25EntriesVisible(),"Documents Show 25 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow10EntriesVisible(), "Documents Show 10 Entries is visible");
		softAssert.assertAll();
	}


	@Test(groups = {"all_Tests","regression_Tests","module_r360_BatchSummaryTransactionDetails" })
	public void BatchSummaryTransactionDetailsTest() throws Exception {
		TransactionDetailsPage TransactionDetailsPage = new TransactionDetailsPage();
		SoftAssert softAssert = new SoftAssert();

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.click_Search();
		BatchSummary.click_Cell_BatchSummary("Batch", 1);
		BatchSummary.click_Cell_BatchDetail("Transaction", 1);

		//check Transaction Detail Header
		softAssert.assertTrue(TransactionDetailsPage.isTransactionDetailPageHeaderVisible(), "Transaction Details Page Header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchSummaryInHeaderPathLinkVisible(),"BatchSummary In Header Path Link is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchDetailInHeaderPathLinkVisible(), "Batch Detail In Header Path Link is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPrintIconInheaderVisible(), "Print Icon is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRefreshIconInHeaderVisible(), "Refresh Icon is visible");
		//check Transaction Details
		softAssert.assertTrue(TransactionDetailsPage.isTransactionDetailsLabelVisible(),"Transaction Details Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBankLabelVisible(),"Bank Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isWorkgroupLabelVisible(), "Workgroup Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDepositDateLabelVisible(), "Deposit Date Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchLabelVisible(), "Batch Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isAccountSiteCodeLabelVisible(), "Account Site Code Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchSiteCodeLabelVisible(), "Batch Site Code Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isBatchCueIDLabelVisible(),"Batch Cue ID Label is visible");
		softAssert.assertTrue(TransactionDetailsPage.isTransactionLabelVisible(), "Transaction Label is visible");
		//check Transaction Detail Payment Items
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsheaderVisible(), "Payment Items header is visible");
		//softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsDataEnteryFieldsVisible(),"Payment Items Data Entery Fields is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShowEntriesVisible(), "Payment Items Show Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow1000EntriesVisible(), "Payment Items Show 1000 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow100EntriesVisible(), "Payment Items Show 100 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow50EntriesVisible(),"Payment Items Show 50 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow25EntriesVisible(),"Payment Items Show 25 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isPaymentItemsShow10EntriesVisible(), "Payment Items Show 10 Entries is visible");
		//check Transaction Detail Related Items
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsheaderVisible(), "Related Items header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShowEntriesVisible(), "Related Items Show Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow1000EntriesVisible(), "Related Items Show 1000 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow100EntriesVisible(), "Related Items Show 100 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow50EntriesVisible(),"Related Items Show 50 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow25EntriesVisible(),"Related Items Show 25 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isRelatedItemsShow10EntriesVisible(), "Related Items Show 10 Entries is visible");
		//check Transaction Detail Documents
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsheaderVisible(),"Documents header is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShowEntriesVisible(),"Documents Show Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow1000EntriesVisible(), "Documents Show 1000 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow100EntriesVisible(), "Documents Show 100 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow50EntriesVisible(),"Documents Show 50 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow25EntriesVisible(),"Documents Show 25 Entries is visible");
		softAssert.assertTrue(TransactionDetailsPage.isDocumentsShow10EntriesVisible(), "Documents Show 10 Entries is visible");
		softAssert.assertAll();
	}

	@AfterMethod(alwaysRun = true)
	public void TearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		if (BaseUI.elementAppears(Locator.lookupElement("Navigate_SignOut"))) {
			Navigation.signOut();
		}
		Browser.closeBrowser();
	}
	
	
}
