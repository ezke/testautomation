package r360.tests_ignored.ignoreForNow;


import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.BankMaintenance;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class BankMaint_Smoketests extends BaseTest {

	
	@BeforeClass(alwaysRun=true)
	public void setup_method() throws Exception
	{
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_BankMaintenance();
		BankMaintenance.sort_ID_Column_Ascending();
		Thread.sleep(1000);
	}
	
	@Test
	public void PT888_BankMaintenance_UI_Validation_ColumnHeader_ID_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_header_ID"));
	}
	
	@Test
	public void PT888_BankMaintenance_UI_Validation_ColumnHeader_Name_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_header_Name"));
	}
	
	@Test
	public void PT888_BankMaintenance_UI_Validation_ColumnHeader_Entity_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_header_Entity"));
	}
	
	@Test
	public void PT888_BankMaintenance_UI_Validation_ColumnHeader_AddButton_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_btn_Add"));
	}
	
	@Test
	public void PT887_BankMaintenance_UI_Validation_EditButton_InGrid_Displayed() throws Throwable
	{
		ArrayList<WebElement> edit_IconList = new ArrayList<WebElement>();
		edit_IconList = Locator.lookup_multipleElements("BankMain_lnk_Edit", null, null);
		
		BaseUI.verify_true_AndLog(edit_IconList.size() > 0, "Edit Icons were present on page.", "Edit Icons were NOT present on page.");
		
		for(WebElement editIcon : edit_IconList)
		{
			BaseUI.verifyElementAppears(editIcon);
		}
		
	}
	
	@Test
	public void PT886_BankMaintenance_UI_Validation_IDlist_InGrid_Displayed() throws Throwable
	{
		ArrayList<WebElement> idList = new ArrayList<WebElement>();
		idList = Locator.lookup_multipleElements("BankMain_list_of_IDs", null, null);
		
		BaseUI.verify_true_AndLog(idList.size() > 0, "IDs were present on page.", "IDs were NOT present on page.");
		
		for(WebElement id_element : idList)
		{
			BaseUI.verifyElementAppears(id_element);
		}
		
	}
	
	@Test
	public void PT886_BankMaintenance_UI_Validation_Namelist_InGrid_Displayed() throws Throwable
	{
		ArrayList<WebElement> nameList = new ArrayList<WebElement>();
		nameList = Locator.lookup_multipleElements("BankMain_list_of_Names", null, null);
		
		BaseUI.verify_true_AndLog(nameList.size() > 0, "Names were present on page.", "Names were NOT present on page.");
		
		for(WebElement name_element : nameList)
		{
			BaseUI.verifyElementAppears(name_element);
		}
		
	}
	
	@Test
	public void PT886_BankMaintenance_UI_Validation_Entitylist_InGrid_Displayed() throws Throwable
	{
		ArrayList<WebElement> entityList = new ArrayList<WebElement>();
		entityList = Locator.lookup_multipleElements("BankMain_list_of_Names", null, null);
		
		BaseUI.verify_true_AndLog(entityList.size() > 0, "Entities were present on page.", "Entities were NOT present on page.");
		
		for(WebElement entity_element : entityList)
		{
			BaseUI.verifyElementAppears(entity_element);
		}
		
	}
	
	@Test
	public void PT881_BankMaintenance_UI_Validation_Pagination_FirstPageIcon_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_btn_gridNavigate_first"));
	}
	
	@Test
	public void PT881_BankMaintenance_UI_Validation_Pagination_PreviousPageIcon_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_btn_gridNavigate_prev"));
	}
	
	@Test
	public void PT881_BankMaintenance_UI_Validation_Pagination_NextPageIcon_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_btn_gridNavigate_next"));
	}
	
	@Test
	public void PT881_BankMaintenance_UI_Validation_Pagination_LastPageIcon_Displayed() throws Throwable
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("BankMain_btn_gridNavigate_last"));
	}

	


	@AfterMethod(alwaysRun=true)
	public void writeResult(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	
	
	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
