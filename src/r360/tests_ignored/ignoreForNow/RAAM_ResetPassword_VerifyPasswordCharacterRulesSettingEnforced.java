package r360.tests_ignored.ignoreForNow;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;
import wfsCommon.pages.Pagination;

public class RAAM_ResetPassword_VerifyPasswordCharacterRulesSettingEnforced extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "Testing101";
	String columnName = "Login Name";
	String loginName = "emailtest101";
	String userPassword = "Wausau#1";
	String newPassword = "Microsoft#1";

	String minLength = "8";
	String maxRepeatingChars = "3";
	String minNumChars = "1";
	String minSpecialChars = "1";
	String minUppercaseChars = "1";
	String minLowercaseChars = "1";

	// putsbox username and password
	String username = "testingr360email@putsbox.com";
	String password = "windows#1";
	String emailName = "casimir_steuber";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verifyPasswordConfigurationCharacterRulesSettingValue(minLength,
				maxRepeatingChars, minNumChars, minSpecialChars, minUppercaseChars, minLowercaseChars);
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(2000);
		String emailSubject = "Reset Password";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		// verify if email is sent
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		Thread.sleep(2000);
		// String winHandleBefore = Browser.driver.getWindowHandle();
		EmailChecker.clickOnLinkBySubjectText(emailSubject);
		// for (String winHandleAfter : Browser.driver.getWindowHandles()){
		// Browser.driver.switchTo().window(winHandleAfter);
		// }
		Thread.sleep(2000);
		EmailChecker.clickOnEmailLink();
	}

	@Test(groups = { "all_Tests" })
	public void PT6510_ResetPassword_VerifyPasswordMinLengthSettingEnforced_LessThanDefinedLength() throws Exception {
		String newPassword = "wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum length of 8 characters.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);

	}

	@Test(groups = { "all_Tests" })
	public void PT6510_ResetPassword_VerifyPasswordMinLengthSettingEnforced_EqualToDefinedMinLength()
			throws Exception {
		String newPassword = "wausaufs";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum length of 8 characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6510_ResetPassword_VerifyPasswordMinLengthSettingEnforced_MoreThanDefinedLength() throws Exception {
		String newPassword = "wausaufsdd";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum length of 8 characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6492_ResetPassword_VerifyPasswordMinLengthSettingEnforced_LessThanDefinedMinUppercase()
			throws Exception {
		String newPassword = "wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 uppercase letters.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);

	}

	@Test
	public void PT6492_ResetPassword_VerifyPasswordMinLengthSettingEnforced_EqualToDefinedMinUppercase()
			throws Exception {
		String newPassword = "Wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 uppercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6492_ResetPassword_VerifyPasswordMinLengthSettingEnforced_MoreThanDefinedMinUppercase()
			throws Exception {
		String newPassword = "WAusau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 uppercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6511_ResetPassword_VerifyPasswordMinLengthSettingEnforced_LessThanDefinedMinLowercase()
			throws Exception {
		String newPassword = "WAU";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 lowercase letters.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);

	}

	@Test(groups = { "all_Tests" })
	public void PT6511_ResetPassword_VerifyPasswordMinLengthSettingEnforced_EqualToDefinedMinLowercase()
			throws Exception {
		String newPassword = "Wa";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 lowercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6511_ResetPassword_VerifyPasswordMinLengthSettingEnforced_MoreThanDefinedMinLowercase()
			throws Exception {
		String newPassword = "Wau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 lowercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6512_ResetPassword_VerifyPasswordMinLengthSettingEnforced_LessThanDefinedMinNum() throws Exception {
		String newPassword = "Wau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 digits.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);

	}

	@Test(groups = { "all_Tests" })
	public void PT6512_ResetPassword_VerifyPasswordMinLengthSettingEnforced_EqualToDefinedMinNum() throws Exception {
		String newPassword = "Wau1";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 digits.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6512_ResetPassword_VerifyPasswordMinLengthSettingEnforced_MoreThanDefinedMinNum() throws Exception {
		String newPassword = "Wau12";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 digits.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6513_ResetPassword_VerifyPasswordMinLengthSettingEnforced_LessThanDefinedMinSpecialChar()
			throws Exception {
		String newPassword = "Wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 special (non-alphanumeric) characters.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);

	}

	@Test(groups = { "all_Tests" })
	public void PT6513_ResetPassword_VerifyPasswordMinLengthSettingEnforced_EqualToDefinedMinSpecialChar()
			throws Exception {
		String newPassword = "Wau#";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 special (non-alphanumeric) characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6513_ResetPassword_VerifyPasswordMinLengthSettingEnforced_MoreThanDefinedMinSpecialChar()
			throws Exception {
		String newPassword = "Wau#@";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password must have minimum of 1 special (non-alphanumeric) characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6509_ResetPassword_VerifyPasswordMinRepeatingCharSettingEnforced_MoreThanDefinedMinRepeatingChar()
			throws Exception {
		String newPassword = "wwwwau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password cannot have more than 3 characters in a row.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);

	}

	@Test(groups = { "all_Tests" })
	public void PT6509_ResetPassword_VerifyPasswordMinRepeatingCharSettingEnforced_EqualToDefinedMinRepeatingChar()
			throws Exception {
		String newPassword = "wwwau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password cannot have more than 3 characters in a row.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@Test(groups = { "all_Tests" })
	public void PT6509_ResetPassword_VerifyPasswordMinRepeatingCharSettingEnforced_LessThanDefinedMinRepeatingChar()
			throws Exception {
		String newPassword = "wwau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError = "Password cannot have more than 3 characters in a row.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		EmailChecker.clearResetPasswordFields();

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}
