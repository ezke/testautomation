package r360.tests_ignored.ignoreForNow;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.BankMaintenance;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;


public class BankMaint_AddBank_Cancel extends BaseTest {

	String entity = "BMO Canada Bank";
	String id = "999999988";
	String name = "BlahBlahBlah";
	
	@BeforeClass(alwaysRun=true)
	public void setup_method() throws Exception
	{
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_BankMaintenance();
		BankMaintenance.launch_AddBank_Modal();
		BankMaintenance.add_Bank(entity, id, name, false);
		BankMaintenance.sort_ID_Column_Descending();
		Thread.sleep(1000);
	}
	
	
	// Start of test
	@Test
	public void PT862_Verify_Bank_Not_Saved_Name_Not_in_List	() throws Exception {
		BaseUI.verify_false_AndLog(BankMaintenance.bool_NameInList(name), "Bank not found.", "Bank was found.");

	}
	
	@Test
	public void PT862_Verify_Bank_Not_Saved_ID_Not_in_List	() throws Exception {
		BaseUI.verify_false_AndLog(BankMaintenance.bool_idInList(id), "Bank not found.", "Bank was found.");

	}
	
	@Test
	public void PT862_Verify_Bank_Not_Saved_Entity_Not_in_List	() throws Exception {
		BaseUI.verify_false_AndLog(BankMaintenance.bool_entityInList(entity), "Bank not found.", "Bank was found.");

	}

	@AfterMethod(alwaysRun=true)
	public void writeResult(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
