package r360.tests_ignored.ignoreForNow;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;

public class RAAM_Login_ResetPassword_FirstEmailInactive_UseLinkTwice extends BaseTest {
	
	private String newEntity = "Auto_Email_TestingLogin";
	private String entityType = "Financial Institution";
	private String entityDescription = "email testing Description";
	private String entityExternalID = "email testing ID 82";
	String columnName = "Login Name";String columnText = "testemaillogin";String loginName = columnText;
	String firstName = "testemail";String lastName = "loginemail";String userPassword = "Wausau#1";
	String email = "testingr360email@putsbox.com";String oldPassword = userPassword;
	String newPassword = "Microsoft#1";
	
	//putsbox username and password
		String username = "testingr360email@putsbox.com";
		String password = "windows#1";
		String emailName = "testingr360email";
		
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		if(BaseUI.pageSourceContainsString(newEntity)){
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		Thread.sleep(1000);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();	
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		Thread.sleep(2000);
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();
	}
	@Test(groups = { "all_Tests" })
	public void PT6490_PasswordEmail_SecondEmailCausesFirstLinkToBeInactive() throws Exception{
		String emailSubject1 = "First email password";
		String emailSubject2 = "Second email password";
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(2000);
		LoginPage.resetForgotPasswordForm(newEntity, loginName, emailSubject1);
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		//verify if first email is sent
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject1);
		Thread.sleep(2000);
		Browser.closeBrowser();
		Browser.openBrowser(GlobalVariables.baseURL,"chrome");
		Thread.sleep(2000);
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(2000);
		LoginPage.resetForgotPasswordForm(newEntity, loginName, emailSubject2);
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		//verify if second email is sent
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject2);
		Thread.sleep(2000);
		//switching to new tab to click the link
		String winHandleBefore = Browser.driver.getWindowHandle();
		EmailChecker.clickOnLinkBySubjectText(emailSubject1);
		EmailChecker.clickOnEmailLink();
		//verify error message after clicking on first email
		EmailChecker.verifyLinkErrorMessage();
		EmailChecker.switchWindow_Or_NavigateBack(winHandleBefore);
		//click on second email to reset password
		EmailChecker.clickOnEmailLink_ResetPassword(emailSubject2, newEntity, columnText, newPassword);
		EmailChecker.deleteAllEmailReceived();
		Browser.closeBrowser();
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(newEntity, columnText, newPassword);
		HomePage.verifyHeaderTitleAppears();
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();

	}
	
	@Test(groups = { "all_Tests" })
	public void PT6491_Login_PasswordEmail_UseLinkTwice() throws Exception{
		String emailSubject1 = "Reset Password Twice";
		String newPassword1 ="Delivery#1";
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(2000);
		LoginPage.resetForgotPasswordForm(newEntity, loginName, emailSubject1);
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		//verify if email is sent
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject1);
		Thread.sleep(2000);
		//reset the password 
		EmailChecker.clickOnEmailLink_ResetPassword(emailSubject1, newEntity, columnText, newPassword1);
		String winHandleBefore = Browser.driver.getWindowHandle();
		EmailChecker.clickOnLinkBySubjectText(emailSubject1);
		EmailChecker.clickOnEmailLink();
		//verify error message after using the link twice
		EmailChecker.verifyLinkErrorMessage();
		EmailChecker.switchWindow_Or_NavigateBack(winHandleBefore);
		EmailChecker.deleteAllEmailReceived();
		Browser.closeBrowser();
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {	
		Browser.openBrowser(GlobalVariables.baseURL,"chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		if(BaseUI.pageSourceContainsString(newEntity)){
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		Browser.closeBrowser();
	}
}
