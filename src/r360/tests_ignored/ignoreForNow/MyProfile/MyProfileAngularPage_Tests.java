package r360.tests_ignored.ignoreForNow.MyProfile;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.*;

public class MyProfileAngularPage_Tests extends BaseTest {
    private String date1 =  BaseUI.getDateAsString_InRelationToTodaysDate(0,"MM/dd/yyyy");
    private String date2 = BaseUI.getDateAsString_InRelationToTodaysDate(1, "MM/dd/yyyy");
    private String workgroup = "2234322 - Automation-Filter";

    private String login=GlobalVariables.login;

    @BeforeMethod(alwaysRun = true)
    public void setup() throws Exception {
       Browser.openBrowser(GlobalVariables.baseURL);
       LoginPage.login(GlobalVariables.entityName,login, GlobalVariables.password, 10, true);
    }

    @Test(groups = { "all_Tests","regression_Tests","module_r360_MyProfileAngularPage"})
    public void Validate_MP_on_Reports() throws Exception {
        Navigation.navigate_Reports();
        VerifyMyProfileCanBeAccessedFromNonAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Reports!!!");
    }

    @Test(groups = { "all_Tests","regression_Tests","module_r360_MyProfileAngularPage"})
    public void Validate_MP_on_Dashboard() throws Exception {
        Navigation.navigate_Dashboard();
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Dashboard!!!");
   }
   
    @Test(groups = {"all_Tests","regression_Tests","module_r360_MyProfileAngularPage"})
    public void Validate_MP_on_BatchSummary() throws Exception {
        Navigation.navigate_BatchSummary();
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Batch Summary!!!");
    }
	

    @Test(groups = {"all_Tests","regression_Tests","module_r360_MyProfileAngularPage" })
    public void Validate_MP_on_DashboardBatchSummary() throws Exception {
        TableData receivableSummariesData;
        Navigation.navigate_Dashboard();
        Dashboard.receivables_depositDate.enter_Date(date1);
        Dashboard.click_OnFirstRowReceivablesTable();
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from DashBoard BatchSummary!!!");
    }

    @Test(groups = {"all_Tests","regression_Tests","module_r360_MyProfileAngularPage" })
    public void Validate_MP_on_BatchSummaryBatchDetail() throws Exception {
        Navigation.navigate_BatchSummary();
        BatchSummary.setDateRange(date1, date2);
        BatchSummary.workGroupSelector.search_forText(workgroup);
        BatchSummary.click_Search();
        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Batch Summary Batch Details!!!");

    }

    @Test(groups = {"all_Tests","regression_Tests","module_r360_MyProfileAngularPage" })
    public void Validate_MP_on_DashboardBatchSummaryBatchDetail() throws Exception {
        Navigation.navigate_Dashboard();
        Dashboard.receivables_depositDate.enter_Date(date1);
        Dashboard.click_OnFirstRowReceivablesTable();
        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Dashboard Batch Summary Batch Details!!!");
    }
  
    @Test(groups = {"all_Tests","regression_Tests","module_r360_UserActivityReport"  })
    public void  Validate_MP_on_BatchSummaryBatchDetailTransactionDetail() throws Exception {
        Navigation.navigate_BatchSummary();
        BatchSummary.setDateRange(date1, date2);
        BatchSummary.workGroupSelector.search_forText(workgroup);
        BatchSummary.click_Search();
        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        BatchSummary.click_Cell_BatchDetail("Transaction", 1);
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Batch Summary Batch Detail Transaction Details!!");
    }

    @Test(groups = {"all_Tests","regression_Tests","module_r360_MyProfileAngularPage" })
    public void Validate_MP_on_DashboardBatchSummaryBatchDetailTransactionDetail() throws Exception {
        Navigation.navigate_Dashboard();
        Dashboard.receivables_depositDate.enter_Date(date1);
        Dashboard.click_OnFirstRowReceivablesTable();
        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        BatchSummary.click_Cell_BatchDetail("Transaction", 1);
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Dashboard Batch Summary Batch Detail Transaction Details!!");
    }
 
    @Test(groups = {"all_Tests","regression_Tests","module_r360_MyProfileAngularPage"  })
    public void Validate_MP_on_AlertManager() throws Exception {
        Navigation.navigate_Admin_Alerts_Manager();
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from Alert Manager!!");
    }

    @Test(groups = { "all_Tests","regression_Tests","module_r360_MyProfileAngularPage"  })
    public void Validate_MP_on_UserPreferences() throws Exception {
        Navigation.navigate_Admin_UserPreferences();
        VerifyMyProfileCanBeAccessedFromAngularPage();
        VerifyMyProfileChangePassword();
        BaseUI.log_Status("Successfully Verified User can Access My Profile page from User Preferences!!");
    }

    public void VerifyMyProfileCanBeAccessedFromAngularPage() throws Exception {
        MyProfilePage myProfilePage = new MyProfilePage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(myProfilePage.isUserProfileCaretSignAngularVisible(), "My Profile Caret is visible");
        softAssert.assertAll();
        myProfilePage.getUserProfileCaretSignAngularXpath().click();
        softAssert.assertTrue(myProfilePage.isMyProfileAngularVisible(), "My Profile is visible From Angular Page");
        softAssert.assertAll();
        myProfilePage.getMyProfileAngularXpath().click();
        Thread.sleep(1000);
        VerifyMyProfilePage();
        BaseUI.log_Status("Successfully Verified My Profile Can Be Accessed From Angular Pages!!");
    }

    public void VerifyMyProfileCanBeAccessedFromNonAngularPage() throws Exception {
        MyProfilePage myProfilePage = new MyProfilePage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(myProfilePage.isUserProfileCaretSignNonAngularVisible(), "My Profile Caret is visible From Non-Angular Page");
        softAssert.assertAll();
        myProfilePage.getUserProfileCaretSignNonAngularXpath().click();
        softAssert.assertTrue(myProfilePage.isMyProfileNonAngularVisible(), "My Profile is visible From Non-Angular Page");
        softAssert.assertAll();
        myProfilePage.getMyProfileNonAngularXpath().click();
        Thread.sleep(1000);
        VerifyMyProfilePage();
        BaseUI.log_Status("Successfully Verified My Profile Can Be Accessed From non-Angular Pages!!");
    }

    public void VerifyMyProfilePage() throws Exception {
        MyProfilePage myProfilePage = new MyProfilePage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(myProfilePage.getmyprofiledata_validate(login), "Getting Data for My Profile Page");
        softAssert.assertTrue(myProfilePage.isMyProfileheaderlabelVisible(), "My Profile header is visible");
        softAssert.assertTrue(myProfilePage.isLoginNameLabelVisible(), "Login Name Label is visible");
        softAssert.assertTrue(myProfilePage.isNameLabelVisible(), "Name Label is visible");
        softAssert.assertTrue(myProfilePage.isEmailLabelVisible(), "Email Label is visible");
        softAssert.assertTrue(myProfilePage.isPasswordLabelVisible(), "Password Label is visible");
        softAssert.assertAll();
        BaseUI.log_Status("Successfully Verified My Profile Page exist!!!!");
   }

    public void VerifyMyProfileChangePassword() throws Exception {
        MyProfilePage myProfilePage = new MyProfilePage();
        SoftAssert softAssert = new SoftAssert();
        myProfilePage.getChangePasswordButtonXpath().click();
        myProfilePage.getCurrentPasswordTextBoxXpath().sendKeys("!@#$%^&*()_+=TestPassword1234567890");
        myProfilePage.getNewPasswordTextBoxXpath().sendKeys("!@#$%^&*(4567890");
        myProfilePage.getConfirmNewPasswordTextBoxXpath().sendKeys("!@#$%^&*()_+=Tsword67890");
        myProfilePage.getSaveButtonXpath().click();
        softAssert.assertTrue(myProfilePage.isNewPwdDoNotMatchVisible(), "New Passwords do not match is visible");
        softAssert.assertAll();
        myProfilePage.getCancelButtonXpath().click();
        myProfilePage.getChangePasswordButtonXpath().click();
        myProfilePage.getCurrentPasswordTextBoxXpath().sendKeys("1234567890");
        myProfilePage.getNewPasswordTextBoxXpath().sendKeys("1234567890");
        myProfilePage.getConfirmNewPasswordTextBoxXpath().sendKeys("1234567890");
        myProfilePage.getSaveButtonXpath().click();
        Thread.sleep(1000);
        softAssert.assertTrue(myProfilePage.isCurPwdDoNotMatchVisible(), "Password does not match current password is visible");
        softAssert.assertAll();
        myProfilePage.getCancelButtonXpath().click();
        myProfilePage.getChangePasswordButtonXpath().click();
        myProfilePage.getCurrentPasswordTextBoxXpath().sendKeys("Wausau#10");
        myProfilePage.getNewPasswordTextBoxXpath().sendKeys("Wausau#10");
        myProfilePage.getConfirmNewPasswordTextBoxXpath().sendKeys("Wausau#10");
        myProfilePage.getSaveButtonXpath().click();
        Thread.sleep(1000);
        softAssert.assertTrue( myProfilePage.CheckforNoReuse180orChangeComplete(), "Check for No Reuse 180 days or Change Complete is visible");
        softAssert.assertAll();
        BaseUI.log_Status("Successfully Verified My Profile Change Password Works!!!");
    }

    @AfterMethod(alwaysRun = true)
    public void testTearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        if (BaseUI.elementAppears(Locator.lookupElement("Navigate_SignOut"))) {
            Navigation.signOut();
        }
        Browser.closeBrowser();
    }
}
