package r360.tests_ignored.ignoreForNow;

import java.text.MessageFormat;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;
import wfsCommon.pages.Pagination;


public class RAAM_Login_ResetPassword_EmailVerification extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "Testing101";
	String loginName = "emailtesting";
	String userPassword = "Wausau#1";

	
	
	//putsbox username and password
	String username = "testingr360email@putsbox.com";
	String password = "windows#1";
	String emailName = "derrick";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6471_Login_ForgotPasswordForm_EntityNameInvalid_VerifyEmailNotSent() throws Exception{
		String invalidEntity = "test%$^3";
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(invalidEntity, loginName);
		String emailSubject = "Reset Password InvalidEntity";	
		LoginPage.resetForgotPasswordForm(invalidEntity, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubject);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6475_Login_ForgotPasswordForm_LoginNameInvalid_VerifyEmailNotSent() throws Exception{
		String invalidLoginName = "test%$^3";
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, invalidLoginName);
		String emailSubject = "Reset Password InvalidLoginName";
		LoginPage.resetForgotPasswordForm(nodeText, invalidLoginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubject);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6466_Login_ForgotPasswordForm_BlankLoginPageAFterEmailSent() throws Exception{
		String invalidPassword = "window123";
		LoginPage.login(nodeText, loginName, invalidPassword);
		LoginPage.verifyLoginPageError();
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(2000);
		String emailSubject = "Reset Password";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		WebElement entityField = Locator.lookupElement("LoginPage_txt_EntityName");
		if(entityField.getText().trim().equals("")){
			BaseUI.verify_false_AndLog(entityField==null, MessageFormat.format("Entity Field is blank", entityField), 
					MessageFormat.format("Entity Field is NOT blank", entityField));
		}
		WebElement loginNameField = Locator.lookupElement("LoginPage_txt_UserName");
		if(loginNameField.getText().trim().equals("")){
			BaseUI.verify_false_AndLog(loginNameField==null, MessageFormat.format("LoginName Field is blank", loginNameField), 
					MessageFormat.format("LoginName Field is NOT blank", loginNameField));
		}
		WebElement passwordField = Locator.lookupElement("LoginPage_txt_Password");
		if(passwordField.getText().trim().equals("")){
			BaseUI.verify_false_AndLog(passwordField==null, MessageFormat.format("Password Field is blank", passwordField), 
					MessageFormat.format("Password Field is NOT blank", passwordField));
		}
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		Thread.sleep(2000);
		
	}
	@Test(groups = { "all_Tests" })
	public void PT6465_Login_ForgotPasswordForm_AllFieldsCorrect_VerifyEmailSent() throws Exception{
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginName);
		String emailSubject = "Reset Password";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		Thread.sleep(2000);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6469_Login_ForgotPasswordForm_CorrectLoginName_IncorrectEntity() throws Exception{
		String incorrectEntity = "WFS";
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(incorrectEntity, loginName);
		String emailSubject = "Reset Password Incorrect Entity";
		LoginPage.resetForgotPasswordForm(incorrectEntity, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubject);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6476_Login_ForgotPasswordForm_ResetPasswordLink_UserLockedOut() throws Exception{
		String loginNameLocked = "emailtest_lockeduser";
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginNameLocked);
		String emailSubject = "Reset Password Locked User";
		LoginPage.resetForgotPasswordForm(nodeText, loginNameLocked, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubject);
	}
	
	@Test (groups = { "all_Tests" })
	public void PT6482_Login_ForgotPasswordForm_SubjectLineWithSymbols() throws Exception{
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginName);
		String emailSubject = "!@~`#$%^&*()_+-;=|}{[]:/.,<?>";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		Thread.sleep(2000);
		String expectedSubjectLine = "!@~`#$%^&*()_+-;=|}{[]:/.,<?>";
		WebElement element = Locator.lookupElement("putsbox_Subject_List", expectedSubjectLine, null);
		String subject = element.getText();
		BaseUI.baseStringCompare("SubjectLine", expectedSubjectLine, subject);
		
	}
	
	
	@Test(groups = { "all_Tests" })
	public void PT6473_Login_ForgotPasswordForm_IncorrectLoginName_CorrectEntity() throws Exception{
		String incorrectLoginName = "AutomationTest";//user of WFS entity
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, incorrectLoginName);
		String emailSubject = "Reset Password Incorrect LoginName";
		LoginPage.resetForgotPasswordForm(nodeText, incorrectLoginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubject);
		Thread.sleep(2000);
			
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6468_Login_ForgotPasswordForm_CancelMouseHover() throws Exception{
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginName);
		BaseUI.enterText(Locator.lookupElement("loginPageForgotPasswordEntityText"), nodeText);
		BaseUI.enterText(Locator.lookupElement("loginPageForgotPasswordLoginNameText"), loginName);
		String emailSubjectLine = "Reset Password";
		BaseUI.enterText(Locator.lookupElement("loginPageForgotPasswordEmailSubjectLineText"), emailSubjectLine);
		BaseUI.elementHover(Locator.lookupElement("loginPageForgotPasswordCancelButton"));
		Thread.sleep(200);
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordCancelButton"));
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		HomePage.verifyHeaderLogoAppears();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubjectLine);
		Thread.sleep(2000);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6485_Login_ForgotPassword_EmailAddressVerification() throws Exception{
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginName);
		String emailSubject = "Reset Password";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		LoginPage.clearLoginFields();
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		Thread.sleep(2000);
		SecurityAdministration.selectNode(nodeText);
		SecurityAdministration.launchEditUserModal_forGivenUser(loginName);
		String expectedValue = "derrick@putsbox.com";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityUserInfoEmail", expectedValue);
		SecurityAdminAddUser.cancelEntityUserUpdatemodal();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("putsbox_EmailAddress_Validation", expectedValue);	
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6480_Login_ForgotPasswordForm_SubjectLine_1000Characters() throws Exception{
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginName);
		String emailSubject = "thisistotest12345alphanumericcharaterssoicantestmyautomationtestthatiamwritingsofaricaneriteotherautomationtestificompletethissofaiiamcountingonly146characterineedocountmorethan1000characterformytesthowiamsupposetocount1000charactersigdhyeasagshagshaghgdygeqahjwtsaygwgtswgygsgajsywgddgahgvsaewtgssavgdhdsfsghajkabhxzgfsagwyauauKSHSHGGgshgshagywgsgdhgdhsyedhghsghgdfhsgdhgshbxcx*******5555555&&&&&&&&&&&shdghgsdhaghsagqwuwuyyegsdgdgahshghdgdggaswtegevdvavagdvevwtwtwtwgssgahgsahghgsahsgteeegeggagsgsahgshsghsaghsghsdaghdsggssagheetetasgfgasfgsdahfgsahfdgsafdagshfshagsadsyeyueyuewyu##############uuuuuuuuuuuuuuuuuuuhsdggggggggggggggggggxxxxxxxxxxxxxxxxxxxxxzajkshjadhagsdhsgdhgdheytgvvcxzvbxzbvxzvxzvzxbvxbvxnbvxznvzxnbvczhdsgahgdahsgdhasghasghsaghasgyetyerytrytryrtyetyutyetgsdhsgdhasjghasghsagajsgsjgahghsagdshagshaghasghasgshdagyetettewggasgdghsadhhhhhhhhhhhhhhhhagetyetrqtyqtgsahjsagahsghasgsgasaggshjgsasaghjgashghgdsahgsahgsahgsagsahsgsaghghshsagsahjgsahjgdsajgsjhgsasahgsasgshgsajhd gshsa234 4";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		String subjectLineText = BaseUI.getTextFromField(Locator.lookupElement("putsbox_Subject_ByText", emailSubject, null));
		BaseUI.verify_true_AndLog(subjectLineText.length() <= 1000, "Subject length was less than or equal to 1000.",
				"Subject length was NOT less than or equal to 1000.");

	}
	
	@Test(groups = { "all_Tests" })
	public void PT6478_Login_ForgotPasswordForm_ResetPassword_InactiveUser() throws Exception{
		String loginNameInactive = "testemail_inactiveuser";
		EmailChecker.login_ClickForgotPassword_ResetPasswordForm(nodeText, loginNameInactive);
		String emailSubject = "Reset Password Inactive User";
		LoginPage.resetForgotPasswordForm(nodeText, loginNameInactive, emailSubject);
		LoginPage.verifyForgotPasswordEmailConfirmationMessage();
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		EmailChecker.verifyEmailNotReceived(username, password, emailName, emailSubject);
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		EmailChecker.deleteAllEmailReceived();
		Browser.closeBrowser();
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {	
		Browser.closeBrowser();
	}
}
