package r360.tests_ignored.ignoreForNow;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.BankMaintenance;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BankMaint_AddBank_FieldValidation extends BaseTest {

	String entity = "BMO Canada Bank";
	String id = "2";
	String name = "BlahBlahBlah";
	String name_InvalidLength = "12333333333333334444444444xdffffffffffffffffffffffaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddddddddddddddaaaaaaaaaaddddfffffaaaaaaa";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
		Navigation.navigate_Admin_BankMaintenance();
		BankMaintenance.launch_AddBank_Modal();
		BankMaintenance.add_Bank("", "", "", true);

	}

	@Test
	public void PT866_EntityFI_ErrorText() {
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_error_label_EntityFI"));
		BaseUI.verifyElementHasExpectedText("BankAddModal_error_label_EntityFI", "Please select the Entity/FI owner.");
	}

	@Test
	public void PT866_ID_ErrorText() {
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_error_label_ID"));
		BaseUI.verifyElementHasExpectedText("BankAddModal_error_label_ID", "ID must be 1-9 digits in length.");
	}

	@Test
	public void PT866_Name_ErrorText() {
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_error_label_Name"));
		BaseUI.verifyElementHasExpectedText("BankAddModal_error_label_Name", "Name is required.");
	}

	@Test
	public void PT866_EntityFI_Error_Still_Displayed() throws Exception {
		BankMaintenance.add_Bank("", id, name, true);
		Thread.sleep(500);
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_error_label_EntityFI"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("BankAddModal_error_label_ID"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("BankAddModal_error_label_Name"));
	}

	@Test
	public void PT867_ID_Error_Still_Displayed() throws Exception {
		BankMaintenance.add_Bank(entity, "", name, true);
		Thread.sleep(500);
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_error_label_ID"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("BankAddModal_error_label_EntityFI"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("BankAddModal_error_label_Name"));
	}

	@Test
	public void PT868_Name_Error_Still_Displayed() throws Exception {
		BankMaintenance.add_Bank(entity, id, "", true);
		Thread.sleep(500);
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_error_label_Name"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("BankAddModal_error_label_EntityFI"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("BankAddModal_error_label_ID"));
	}

	@Test
	public void PT864_Name_Fields_Validation_Length_doesnt_exceed_128() throws Exception {
		BankMaintenance.add_Bank(entity, "", name_InvalidLength);
		Thread.sleep(500);
		String name_Input_Text = BaseUI.getTextFromInputBox(Locator.lookupElement("BankAddModal_txt_Name"));

		BaseUI.verify_true_AndLog(name_Input_Text.length() <= 128, "Name length was less than or equal to 128.",
				"Name length was NOT less than or equal to 128.");
	}

	@Test
	public void PT864_Name_Fields_Validation_Invalid_SpecialCharacter_ATSymbol() throws Exception {
		BankMaintenance.add_Bank(entity, "", "@");
		BankMaintenance.AddBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT864_Name_Fields_Validation_Invalid_SpecialCharacter_PlusSymbol() throws Exception {
		BankMaintenance.add_Bank(entity, "", "+");
		BankMaintenance.AddBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT864_Name_Fields_Validation_Invalid_SpecialCharacter_EqualsSymbol() throws Exception {
		BankMaintenance.add_Bank(entity, "", "=");
		BankMaintenance.AddBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT864_Name_Fields_Validation_Valid_SpecialCharacters() throws Exception {
		BankMaintenance.add_Bank(entity, "", "&,.:'()_-");
		Thread.sleep(500);
		BaseUI.tabThroughField("BankAddModal_txt_Name");
		Thread.sleep(500);

		WebElement nameErrorLabel = Locator.lookupElement("BankAddModal_error_label_Name");
		BaseUI.verifyElementDoesNotAppear(nameErrorLabel);

	}

	@Test
	public void PT864_ID_Fields_Validation_Invalid_Characters_Letters() throws Exception {
		BankMaintenance.add_Bank(entity, "abcde", "");
		BankMaintenance.AddBank_verifyID_charactersNotValid();
	}

	@Test
	public void PT864_ID_Fields_Validation_Invalid_Characters_ATSymbol() throws Exception {
		BankMaintenance.add_Bank(entity, "@", "");
		BankMaintenance.AddBank_verifyID_charactersNotValid();
	}

	@Test
	public void PT864_ID_Fields_Validation_Invalid_Characters_EqualsSymbol() throws Exception {
		BankMaintenance.add_Bank(entity, "=", "");
		BankMaintenance.AddBank_verifyID_charactersNotValid();
	}

	@Test
	public void PT864_ID_Fields_Validation_Invalid_Characters_Ampersand() throws Exception {
		BankMaintenance.add_Bank(entity, "&", "");
		BankMaintenance.AddBank_verifyID_charactersNotValid();
	}

	@Test
	public void PT864_ID_Fields_Validation_Invalid_Characters_NegativeNumber() throws Exception {
		BankMaintenance.add_Bank(entity, "-5", "");
		BankMaintenance.AddBank_verifyID_charactersNotValid();
	}

	@Test
	public void PT864_ID_Fields_Validation_Invalid_Characters_NumberTooLarge() throws Exception {
		BankMaintenance.add_Bank(entity, "9147483647", "");
		BankMaintenance.AddBank_verifyID_charactersNotValid();
	}

	@Test
	public void PT864_ID_Fields_Validation_ID_Used_By_Other_Bank() throws Exception {
		BankMaintenance.add_Bank(entity, "1", "PT864_BankTestName");
		Thread.sleep(500);
		BaseUI.tabThroughField("BankAddModal_txt_Name");

		BaseUI.click(Locator.lookupElement("BankAddModal_btn_Save"));
		Thread.sleep(1000);
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_ErrorPopup_ID_InUse", "1", null));

	}

	@Test
	public void PT885_BankMaintenance_AddBank_Labels_Displayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_EntityFI_Label"));
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_ID_Label"));
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_Name_Label"));
	}

	@Test
	public void PT885_BankMaintenance_AddBank_SaveAndCancelButtons_Displayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_btn_Save"));
		BaseUI.verifyElementAppears(Locator.lookupElement("BankAddModal_btn_Cancel"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.close_ToastError_If_ItAppears();
		if (BaseUI.elementAppears(Locator.lookupElement("BankAddModal_btn_Cancel"))) {
			BankMaintenance.click_AddBank_Cancel();
		}
		BankMaintenance.launch_AddBank_Modal();
		BankMaintenance.click_AddBank_Save();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			if (Locator.lookupElement("BankAddModal_modl_AddBank").isDisplayed()) {
				BankMaintenance.click_AddBank_Cancel();
			}
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
