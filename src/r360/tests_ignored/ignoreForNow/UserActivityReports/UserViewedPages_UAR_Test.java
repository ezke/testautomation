package r360.tests_ignored.ignoreForNow.UserActivityReports;

import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.*;


public class UserViewedPages_UAR_Test  extends BaseTest{
    private String report = "User Activity Report";
    private String eventType = "-- All --";
    private String CSVeventype = "Viewed Page";


    @BeforeMethod(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 10, true);
    }

    // Validate that the User Activity Report has the "Viewed Page"  entries  for DashBoard
    @Test(groups = { "all_Tests","regression_Tests","module_r360_UserActivityReport" }, priority = 1)
    public void PT13015_ValidateUAR_CSV_HasViewedPage_forDashboard() throws Exception {
        // Dashboard user land on page test
        Navigation.navigate_Dashboard();
        String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "Dashboard";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
    }
    // Validate that the User Activity Report has the "Viewed Page"  entries  for BatchSummary
    @Test(groups = {"all_Tests","regression_Tests","module_r360_UserActivityReport"  }, priority = 2)
    public void PT13016_ValidateUAR_CSV_HasViewedPage_forBatchSummary() throws Exception {
        // Batch Summary user land on page test
        Navigation.navigate_BatchSummary();
		String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "BatchSummary";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
    }
    // Validate that the User Activity Report has the "Viewed Page"  entries  for BatchSummary
    @Test(groups = {"all_Tests","regression_Tests","module_r360_UserActivityReport"  }, priority = 3)
    public void PT13016_ValidateUAR_CSV_HasViewedPagefromDashboardBatchSummary() throws Exception {
        TableData receivableSummariesData;
        Navigation.navigate_Dashboard();
        receivableSummariesData = Dashboard.extract_ReceivablesSummary_Tables();
        BaseUI.verify_true_AndLog(receivableSummariesData.data.size() > 0,
                "Found table data on Receivables Summary page.",
                "Unable to extract table data from Receivables Summary page.");
        Dashboard.click_OnFirstRowReceivablesTable();
        //Batch Summary Angular page user land on page
        String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "BatchSummary";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
    }
    // Validate that the User Activity Report has the "Viewed Page"  entries  for BatchSummary
    @Test(groups = {"all_Tests","regression_Tests","module_r360_UserActivityReport"  }, priority = 4)
    public void PT13016_ValidateUAR_CSV_HasViewedPage_forBatchDetail() throws Exception {
        TableData receivableSummariesData;
        Navigation.navigate_Dashboard();
        receivableSummariesData = Dashboard.extract_ReceivablesSummary_Tables();
        BaseUI.verify_true_AndLog(receivableSummariesData.data.size() > 0,
                "Found table data on Receivables Summary page.",
                "Unable to extract table data from Receivables Summary page.");
        Dashboard.click_OnFirstRowReceivablesTable();
        TableData batchSummaryTableData = BatchSummary.return_SearchResults();
        BaseUI.verify_true_AndLog(batchSummaryTableData.data.size() > 0,
                "Found table data on Batch Summary page.",
                "Unable to extract table data from Batch Summary page.");
        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        //Batch Detail  user land on page
        String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "BatchDetail";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
    }
    // Validate that the User Activity Report has the "Viewed Page"  entries  for BatchSummary
    @Test(groups = {"all_Tests","regression_Tests","module_r360_UserActivityReport"  }, priority = 5)
    public void PT13016_ValidateUAR_CSV_HasViewedPage_forTransactionDetail() throws Exception {
        TableData receivableSummariesData;
        Navigation.navigate_Dashboard();
        receivableSummariesData = Dashboard.extract_ReceivablesSummary_Tables();
        BaseUI.verify_true_AndLog(receivableSummariesData.data.size() > 0,
                "Found table data on Receivables Summary page.",
                "Unable to extract table data from Receivables Summary page.");
        Dashboard.click_OnFirstRowReceivablesTable();
        TableData batchSummaryTableData = BatchSummary.return_SearchResults();
        BaseUI.verify_true_AndLog(batchSummaryTableData.data.size() > 0,
                "Found table data on Batch Summary page.",
                "Unable to extract table data from Batch Summary page.");
        BatchSummary.click_Cell_BatchSummary("Batch", 1);
        TableData batchDetailSummaryTableData = BatchSummary.return_DetailSearchResults();
        BaseUI.verify_true_AndLog(batchDetailSummaryTableData.data.size() > 0,
                "Found table data on Batch Detail page.",
                "Unable to extract table data from Batch Detail page.");
        BatchSummary.click_Cell_BatchDetail("Transaction", 1);
        //Transaction Detial user land on page
        String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "TransactionDetail";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
    }

    // Validate that the User Activity Report has the "Viewed Page"  entries  for AlertManager
    @Test(groups = {"all_Tests","regression_Tests","module_r360_UserActivityReport"  }, priority = 6)
    public void PT13017_ValidateUAR_CSV_HasViewedPage_forAlertManager() throws Exception {
        // Alerts Manager user land on page
        Navigation.navigate_Admin_Alerts_Manager();
        String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "AlertManager";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
    }

    //Validate that the User Activity Report has the "Viewed Page"  entries  for UserPreferences
    @Test(groups = { "all_Tests","regression_Tests","module_r360_UserActivityReport"  }, priority = 7)
    public void PT13018_ValidateUAR_CSV_HasViewedPage_forUserPreferences() throws Exception {
        // User Preferences user land on page
        Navigation.navigate_Admin_UserPreferences();
        String AuditDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy h:mm a");
        String AuditMessage = "UserPreferences";
        Reports.generate_UAR_VerifyEventTypeExist(report, Navigation.SetandValidateUserFullName(GlobalVariables.login),eventType,AuditDateTime,CSVeventype,AuditMessage);
      }

    @AfterMethod(alwaysRun = true)
    public void testTearDown(ITestResult result) throws Exception {
        if (BaseUI.elementAppears(Locator.lookupElement("Navigate_SignOut"))) {
            Navigation.signOut();
        }
        ResultWriter.checkForFailureAndScreenshot(result);
        Browser.closeBrowser();
    }

}
