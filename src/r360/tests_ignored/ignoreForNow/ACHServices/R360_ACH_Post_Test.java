package r360.tests_ignored.ignoreForNow.ACHServices;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import r360.pages.GlobalVariables;
import r360.tests.BaseTest;
import utils.BaseUI;
import java.io.File;

public class R360_ACH_Post_Test extends BaseTest {

    @Test
    public void AchPostTest() {
        SoftAssert softAssertion = new SoftAssert();
        String userDirectory = System.getProperty("user.dir");

        // Specify the base URL to the RESTful web service
        //      "http://rechubqaimg02.qalabs.nwk:8001/api/ACH";
        RestAssured.baseURI = GlobalVariables.achURI;

        // Request body
        //  Good ACH file   "/src/r360/data/ACH_ValidFile.txt");
        File file = new File(userDirectory + GlobalVariables.goodAchFile);

        Response response = null;
        try {
            response = API.RestAssured.postWithFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Print resonse
        //String responseBody = response.getBody().asString();
        //System.out.println("Response Body is =>  " + responseBody);

        int statCode = response.getStatusCode();
        BaseUI.verify_true_AndLog((statCode == 200), (String.format("Received expected status code: %d", statCode)), (String.format("Received unexpected status code: %d", statCode)));

    }
}




