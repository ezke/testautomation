package r360.tests_ignored.ignoreForNow.ACHServices;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
//import org.testng.Assert;
import org.testng.annotations.Test;
import r360.tests.BaseTest;
import utils.BaseUI;

//import java.io.File;

public class R360_ACH_Post_No_File_Test extends BaseTest {

    @Test
    public void AchPostNoFileTest() {

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = "http://rechubqaimg02.qalabs.nwk:8001/api/ACH";

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.
        RequestSpecification Request = RestAssured.given();

        // Request header
        Request.header("Content-Type", "multipart/form-data");

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        //Response response = Request.request(Method.GET, "/get");
        Response response = null;
        try {
            response = Request.request(Method.POST);
        } catch (Exception c) {
            response = null;
            String cause = c.getMessage();
        }

        // Now let us print the body of the message to see what response
        // we have recieved from the server
        String responseBody = response.getBody().asString();
        System.out.println("Response Body is =>  " + responseBody);

        int statCode = response.getStatusCode();
        BaseUI.verify_true_AndLog((statCode == 500), (String.format("Received expected status code: %d", statCode)), (String.format("Received unexpected status code: %d", statCode)));

    }
}




