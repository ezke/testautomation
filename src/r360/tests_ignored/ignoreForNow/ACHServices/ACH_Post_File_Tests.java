package r360.tests_ignored.ignoreForNow.ACHServices;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import r360.pages.GlobalVariables;
import r360.tests.BaseTest;

import java.io.File;


public class ACH_Post_File_Tests extends BaseTest {

    String userDirectory;

    @Test(groups = {"all_Tests","regression_Tests","module_r360_ACHPostFile" } , priority = 1)
    public void PT9143_AchPostValidateTest() {
        userDirectory = System.getProperty("user.dir");
        SoftAssert softAssert = new SoftAssert();

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = GlobalVariables.achURI;
        RestAssured.defaultParser = Parser.JSON;

        // Request body
        File file = new File(userDirectory + GlobalVariables.goodAchFile);

        Response response = null;
        try {
            response = API.RestAssured.postWithFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Check status code
        int statCode = response.getStatusCode();
        softAssert.assertTrue((statCode == 201), String.format("Received expected status code: %d", statCode));
        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests","regression_Tests","module_r360_ACHPostFile" } , priority = 2)
    public void PT9143_AchPostGetValidateFileTest() {
        userDirectory = System.getProperty("user.dir");
        SoftAssert softAssert = new SoftAssert();

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = GlobalVariables.achURI;
        RestAssured.defaultParser = Parser.JSON;

        // Request body
        File file = new File(userDirectory + GlobalVariables.goodAchFile);

        Response response = null;
        try {
            response = API.RestAssured.postWithFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Check status code
        int statCode = response.getStatusCode();
        softAssert.assertTrue((statCode == 201), (String.format("Received expected status code: %d", statCode)));

        // Get link with which to request status
        JsonPath jsonPathEvaluator = response.jsonPath();
        String fileIdString = jsonPathEvaluator.get("data.fileId");

        // Construct URL
        String url = GlobalVariables.achURI + "/" + fileIdString;
        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = url;
        RestAssured.defaultParser = Parser.JSON;

        // Issue GET
        response = null;
        try {
            response = API.RestAssured.getReponseFromURL();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Check status code
        int statCodeGet = response.getStatusCode();
        softAssert.assertTrue((statCodeGet == 200), (String.format("Received expected status code: %d", statCodeGet)));
        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests","regression_Tests","module_r360_ACHPostFile" } , priority = 3)
    public void PT9143_AchPostGetValidateNegativeFileTest() {
        userDirectory = System.getProperty("user.dir");
        SoftAssert softAssert = new SoftAssert();

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = GlobalVariables.achURI;
        RestAssured.defaultParser = Parser.JSON;

        // Request body
        File file = new File(userDirectory + GlobalVariables.badAchFile);

        Response response = null;
        try {
            response = API.RestAssured.postWithFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Check status code
        int statCode = response.getStatusCode();
        softAssert.assertTrue((statCode == 201), (String.format("Received expected status code: %d", statCode)));

        // Get link with which to request status
        JsonPath jsonPathEvaluator = response.jsonPath();
        String fileIdString = jsonPathEvaluator.get("data.fileId");

        // Construct URL
        String url = GlobalVariables.achURI + "/" + fileIdString;
        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = url;
        RestAssured.defaultParser = Parser.JSON;

        // Issue GET
        response = null;
        try {
            response = API.RestAssured.getReponseFromURL();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Check status code
        int statCodeGet = response.getStatusCode();
        softAssert.assertTrue((statCodeGet == 200), (String.format("Received expected status code: %d", statCodeGet)));

        // Get link with which to request status
        JsonPath jsonPathChecker = response.jsonPath();
        int dataValue = jsonPathChecker.get("data");

        // We expect a -1 for a bad file, though I don't get that now.
        softAssert.assertTrue((dataValue != 2), (String.format("Received expected data value: %d", dataValue)));
        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests","regression_Tests","module_r360_ACHPostFile" } , priority = 4)
    public void PT9143_AchPostNoFileTest() {

        SoftAssert softAssert = new SoftAssert();

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = GlobalVariables.achURI;
        RestAssured.defaultParser = Parser.JSON;

        File file = null;

        Response response = null;
        try {
            response = API.RestAssured.postWithFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Now let us print the body of the message to see what response
        // we have recieved from the server
        String responseBody = response.getBody().asString();
        System.out.println("Response Body is =>  " + responseBody);

        int statCode = response.getStatusCode();
        softAssert.assertEquals(statCode, 400, (String.format("Received status code: %d", statCode)));
        softAssert.assertAll();
    }
}


