package r360.tests_ignored.ignoreForNow.ACHServices;

import io.restassured.RestAssured;

import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import r360.pages.GlobalVariables;
import r360.tests.BaseTest;
import utils.DatabaseConnection;
import utils.TableData;

import java.util.HashMap;
import java.util.Map;

public class PT9000_R360_Search_Post_Test extends BaseTest {

    @Test
    public void PT9000_Search_Post_Test() {
        SoftAssert softAssert = new SoftAssert();
        String userDirectory = System.getProperty("user.dir");

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = GlobalVariables.getToken2016Url;
        RestAssured.defaultParser = Parser.JSON;

        // Get Token
        String UsernameString = "username";
        String PasswordString = "password";
        String ArgUsername = "sa";
        String ArgPassword = "Wausau#1";

        Map<String, String> tokenReq = new HashMap<>();
        tokenReq.put(UsernameString, ArgUsername);
        tokenReq.put(PasswordString, ArgPassword);

        Response response = null;
        try {
            response = API.RestAssured.getToken(tokenReq, UsernameString, ArgUsername, PasswordString, ArgPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // status code should be 200
        int statCode = response.getStatusCode();
        softAssert.assertTrue((statCode == 200), (String.format("Received (from get token) expected status code: %d", statCode)));

        // obtain abd save token string
        JsonPath jsonPathChecker = response.jsonPath();
        String queryString = "token";
        String tokenString = jsonPathChecker.get(queryString).toString();

        String BankIdString = "BankId";
        String WorkgroupIdString = "WorkgroupId";
        String StartDateString = "StartDate";
        String EndDateString = "EndDate";

        String ArgBankId = "68";
        String ArgWorkgroupId = "68";
        String ArgStartDate = "2018-04-01";
        String ArgEndDate = "2018-07-16";

        Map<String, String> hm = new HashMap<>();
        hm.put(BankIdString, ArgBankId);
        hm.put(WorkgroupIdString, ArgWorkgroupId);
        hm.put(StartDateString, ArgStartDate);
        hm.put(EndDateString, ArgEndDate);
        hm.put(UsernameString, ArgUsername);
        hm.put(PasswordString, ArgPassword);

        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = GlobalVariables.search2016Url;
        RestAssured.defaultParser = Parser.JSON;

        response = null;
        try {
            response = API.RestAssured.postSearch(hm, ("Bearer " + tokenString), UsernameString, ArgUsername, PasswordString, ArgPassword, BankIdString, ArgBankId, WorkgroupIdString, ArgWorkgroupId, StartDateString, ArgStartDate, EndDateString, ArgEndDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // status code should be 200
        statCode = response.getStatusCode();
        softAssert.assertTrue((statCode == 200), (String.format("Received (from search) expected status code: %d", statCode)));

        // Capture the number of search results to compare with number from the daatabase query.
        jsonPathChecker = response.jsonPath();
        int numRows = jsonPathChecker.get("searchResults");

        //HashMap<String, String> batchMap = jsonPathChecker.get("batches[0]");
        //int numRows = batchMap.size();

        // Now execute database quesy and compare numRows

        int dbNumRows = 0;

        String dbUrl = GlobalVariables.databaseUrl;

        TableData data = null;

        try {
            data = DatabaseConnection.runSQLServerStoredProcQueryFourArgs(dbUrl, "68", "68", "20180401", "20180716");
            dbNumRows = data.rowCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Compare number of rows. It shoudl be non-zero and be equal
        softAssert.assertTrue((numRows != 0), (String.format("Number of batches should be non-zero")));

        softAssert.assertTrue((numRows == dbNumRows), (String.format("Number of batches should be equal API vs DB")));

        softAssert.assertAll();
    }
}




