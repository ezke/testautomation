package r360.tests_ignored.ignoreForNow;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;

public class RAAM_ResetPassword_VerifyUserRulesSetToTrue extends BaseTest {
	private String nodeText = "Testing101";
	String loginName = "emailtesting";
	String userPassword = "Wausau#3";
	String oldPassword = userPassword;
	
	//putsbox username and password
		String username = "testingr360email@putsbox.com";
		String password = "windows#1";
		String emailName = "derrick";
		
		@BeforeClass(alwaysRun = true)
		public void setup() throws Exception {
			Browser.openBrowser(GlobalVariables.baseURL, "chrome");
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
			Navigation.navigate_Admin_Entities();
			Thread.sleep(2000);
			SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
			SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
			String expectedValue = "180";
			SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigPasswordReuse", expectedValue);
			if(Locator.lookupElement("entityEntityUpdateModal").isDisplayed()){
				SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToTrue();
			}
			HomePage.clickSignOutLink();
			HomePage.clickReturnToApplicationLink();	
			LoginPage.clickForgotPasswordLink();
			Thread.sleep(2000);
			String emailSubject = "Reset Password";
			LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
			Browser.closeBrowser();
			Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
			//verify if email is sent
			EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
			Thread.sleep(2000);
			//String winHandleBefore = Browser.driver.getWindowHandle();
			EmailChecker.clickOnLinkBySubjectText(emailSubject);
//			for (String winHandleAfter : Browser.driver.getWindowHandles()){
//				Browser.driver.switchTo().window(winHandleAfter);
//			}
			Thread.sleep(2000);
			EmailChecker.clickOnEmailLink();
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6502_ResetPassword_VerifyPasswordCanContainLoginNameSetToTrue() throws Exception{
			String newPassword = "emailtesting";
			EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "Login name is not allowed in password.";
			BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6504_ResetPassword_VerifyPasswordCanContainReverseLoginNameSetToTrue() throws Exception{
			String newPassword = "gnitsetliame";
			EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "Login name is not allowed in password.";
			BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6506_ResetPassword_VerifyPasswordCanContainFirstNameSetToTrue() throws Exception{
			String newPassword = "Johnemail";
			EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "First name is not allowed in password.";
			BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6508_ResetPassword_VerifyPasswordCanContainLastNameSetToTrue() throws Exception{
			String newPassword = "Tenesky";
			EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "LastName name is not allowed in password.";
			BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6500_ResetPassword_VerifyDaysBeforePasswordReUseIsEnforced() throws Exception{
			String newPassword = userPassword;
			EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "Passwords cannot be reused in less than 180 days.";
			WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
			String errorMessage = element.getText();
			BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6496_ResetPassword_PasswordsDoNotMatch() throws InterruptedException{
			String newPassword = "Manage#1";
			String confirmNewPassword = "Microsoft#1";
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_EnityText"), nodeText);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_LoginNameText"), loginName);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_NewPasswordText"), newPassword);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_ConfirmNewPasswordText"), confirmNewPassword);
			BaseUI.click(Locator.lookupElement("email_ResetPasswordForm_Submit_Button"));
			Thread.sleep(4000);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "New Password and Confirm New Password do not match";
			WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
			String errorMessage = element.getText();
			BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6497_ResetPassword_PasswordsNotFilledIn() throws InterruptedException{
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_EnityText"), nodeText);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_LoginNameText"), loginName);
			BaseUI.click(Locator.lookupElement("email_ResetPasswordForm_Submit_Button"));
			Thread.sleep(4000);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "All fields must be filled in";
			WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
			String errorMessage = element.getText();
			BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6494_ResetPassword_EntityNotFilledIn() throws InterruptedException{
			String newPassword ="Microsoft#1";
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_LoginNameText"), loginName);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_NewPasswordText"), newPassword);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_ConfirmNewPasswordText"), newPassword);
			BaseUI.click(Locator.lookupElement("email_ResetPasswordForm_Submit_Button"));
			Thread.sleep(4000);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "All fields must be filled in";
			WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
			String errorMessage = element.getText();
			BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6495_ResetPassword_LoginNameNotFilledIn() throws InterruptedException{
			String newPassword ="Microsoft#1";
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_EnityText"), nodeText);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_NewPasswordText"), newPassword);
			BaseUI.enterText(Locator.lookupElement("email_ResetPasswordForm_ConfirmNewPasswordText"), newPassword);
			BaseUI.click(Locator.lookupElement("email_ResetPasswordForm_Submit_Button"));
			Thread.sleep(4000);
			EmailChecker.verifyResetPasswordForm_ErrorMessage();
			String expectedError= "All fields must be filled in";
			WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
			String errorMessage = element.getText();
			BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
		}
		@AfterMethod(alwaysRun = true)
		public void writeResult(ITestResult result) throws Exception {
			ResultWriter.checkForFailureAndScreenshot(result);
			EmailChecker.clearResetPasswordFields();
			
		}
		@AfterClass(alwaysRun = true)
		public void writeResultsAndTearDown() throws Exception {
			Browser.closeBrowser();
			Browser.openBrowser(GlobalVariables.baseURL, "chrome");
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
			Navigation.navigate_Admin_Entities();
			Thread.sleep(2000);
			SecurityAdministration.selectNode(nodeText);
			SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
			SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
			if(Locator.lookupElement("entityEntityUpdateModal").isDisplayed()){
				SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToFalse();
			}
			Browser.closeBrowser();
		}
}
