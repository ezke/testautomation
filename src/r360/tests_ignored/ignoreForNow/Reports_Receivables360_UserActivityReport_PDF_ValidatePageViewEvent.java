package r360.tests_ignored.ignoreForNow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.Reports;
import r360.tests.BaseTest;
import utils.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Reports_Receivables360_UserActivityReport_PDF_ValidatePageViewEvent extends BaseTest {
    private final String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String mainHandle = "";
    private String report = "User Activity Report";
    private String startDate = "";
    private String endDate = "";
    private String expectedTime = "";
    private String expectedTitle = "User Activity Report";
    private String eventType = "Page View";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL, "firefox");
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
        mainHandle = Browser.driver.getWindowHandle();
    }

    @Test(groups = {"all_Tests", "critical_Tests"}, priority = 10)
    public void PT12683_UserActivityReport() throws Exception {
        startDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(todaysDate, "MM/dd/yyyy", "MM/dd/yyyy", -0);
        endDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(todaysDate, "MM/dd/yyyy", "MM/dd/yyyy", 0);
        Navigation.navigate_Dashboard();
        Navigation.navigate_BatchSummary();
        Navigation.navigate_Admin_UserPreferences();
        Navigation.navigate_Reports();
        Navigation.navigate_Admin_Alerts_Manager();
        Navigation.navigate_Reports();
        Reports.generateAndNavigateToPDFReport(startDate, endDate, eventType, report);
        String totalPages = Browser.driver.findElement(By.id("numPages")).getText().substring(3);

        BaseUI.log_Status("Pages in report so far: " + totalPages);
        int totalPages1 = Integer.parseInt(totalPages);
        WebElement secondLastPage = Reports.getSecondLastPage(totalPages1);

        WebElement pdfText = secondLastPage.findElement(By.className("textLayer"));

        List<WebElement> textDivs = pdfText.findElements(By.xpath(".//div"));

        BaseUI.log_Status("List size = " + textDivs.size());
        String eventTypeColumn = "", fieldText = "", auditTimeColumn = "", auditMessageColumn = "";

        Iterator<WebElement> itr1 = textDivs.iterator();

        WebElement test;
        //Navigate through the list of individual strings in the user activity report to identify column style for event audit date/time, event name and audit message columns
        while (itr1.hasNext()) {
            test = itr1.next();
            fieldText = test.getText();

            if (fieldText.equals("Audit Date/Time")) {
                auditTimeColumn = test.getAttribute("style");
                auditTimeColumn = auditTimeColumn.substring(0, 15);
            } else if (fieldText.equals("Event Type")) {
                eventTypeColumn = test.getAttribute("style");
                eventTypeColumn = eventTypeColumn.substring(0, 15);
            } else if (fieldText.equals("Audit Message")) {
                auditMessageColumn = test.getAttribute("style");
                auditMessageColumn = auditMessageColumn.substring(0, 15);
            }
        }

        Iterator<WebElement> itr2 = textDivs.iterator();
        BaseUI.log_Status("Navigate through the list to find items matching event type style");

        //Method to convert list of events and the event details to a hashmap (one entry equivalent to a row in User Activity Report)
        HashMap<Integer, List<String>> selectedData = Reports.extractDataFromListtoHashmap(itr2, auditTimeColumn, eventTypeColumn, auditMessageColumn);

        //Console output : HashMap containing the whole list of page view events extracted from the User Activity report
        BaseUI.log_Status("Final output:");
        for (HashMap.Entry<Integer, List<String>> entry : selectedData.entrySet()) {
            BaseUI.log_Status(entry.getKey() + ":" + entry.getValue());
        }

        //Method  to scan through the list of events to verify if the page view events and the events associated with that page view get displayed correctly.
        boolean error = Reports.verifyEventEntriesInUserActivityReport(selectedData);
        BaseUI.log_Status("error = " + error);
        if (error) {
            BaseUI.log_AndFail("Failed in one of the report entries");
        } else {
            BaseUI.log_Status("All event entries verified!");
        }
    }

    @AfterMethod(alwaysRun = true)
    public void writeResult(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        BaseUI.close_ExtraWindows(mainHandle);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}