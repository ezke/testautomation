package r360.tests_ignored.ignoreForNow;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;

public class RAAM_ResetPassword_VerifyUserRulesSetToFalse extends BaseTest {
	private String nodeText = "Testing101";
	String columnName = "Login Name";
	
	String loginName = "emailtesting201";
	String userPassword = "Wausau#1";
	String oldPassword = userPassword;
	
	//putsbox username and password
	String username = "testingr360email@putsbox.com";
	String password = "windows#1";
	String emailName = "maritza_altenwerth";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		if(Locator.lookupElement("entityEntityUpdateModal").isDisplayed()){
			SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToFalse();
		}
		Thread.sleep(1000);
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();	
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(2000);
		String emailSubject = "Reset Password";
		LoginPage.resetForgotPasswordForm(nodeText, loginName, emailSubject);
		Browser.closeBrowser();
		Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
		//verify if email is sent
		EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject);
		Thread.sleep(2000);
		EmailChecker.clickOnLinkBySubjectText(emailSubject);
		Thread.sleep(2000);
		EmailChecker.clickOnEmailLink();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6501_ResetPassword_VerifyPasswordCanContainLoginNameSetToFalseWithLoginName() throws Exception{
		String newPassword = "emailtesting201";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "Login name is not allowed in password.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6501_ResetPassword_VerifyPasswordCanContainLoginNameSetToFalseWithoutLoginName() throws Exception{
		String newPassword = "wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "Login name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6503_ResetPassword_VerifyPasswordCanContainReverseLoginNameSetToFalseWithReverseLoginName() throws Exception{
		String newPassword = "102gnitsetliame";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "Reversed login name is not allowed in password.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6503_ResetPassword_VerifyPasswordCanContainReverseLoginNameSetToFalseWithoutReverseLoginName() throws Exception{
		String newPassword = "wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "Reversed login name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6505_ResetPassword_VerifyPasswordCanContainFirstNameSetToFalseWithFirstName() throws Exception{
		String newPassword = "Maritza";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "First name is not allowed in password.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6505_ResetPassword_VerifyPasswordCanContainReverseLoginNameSetToFalseWithoutFirstName() throws Exception{
		String newPassword = "wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "First name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6507_ResetPassword_VerifyPasswordCanContainLastNameSetToFalseWithLastName() throws Exception{
		String newPassword = "Altenwerth";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "Last name is not allowed in password.";
		WebElement element = Locator.lookupElement("email_ResetPasswordForm_ErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("ResetPasswordError", expectedError, errorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT6507_ResetPassword_VerifyPasswordCanContainLastNameSetToFalseWithoutLastName() throws Exception{
		String newPassword = "wausau";
		EmailChecker.enterResetPasswordForm(nodeText, loginName, newPassword);
		EmailChecker.verifyResetPasswordForm_ErrorMessage();
		String expectedError= "Last name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("email_ResetPasswordForm_ErrorMessage", expectedError);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		EmailChecker.clearResetPasswordFields();
		
	}
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
		
	}
}
