package r360.tests_ignored.ignoreForNow;


import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.BankMaintenance;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;

public class BankMaint_Sort extends BaseTest {

	//Pagination bankGrid;
	
	@BeforeClass(alwaysRun=true)
	public void setup_method() throws Exception
	{
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_BankMaintenance();
		//bankGrid = BankMaintenance.bankGrid;
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT883_BankMaintenance_Sort_ID_Ascending() throws Throwable
	{
		BankMaintenance.bankGrid.click_sort_ByColumnName("Name");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		BankMaintenance.bankGrid.verifyColumnAscending_numeric("Id");
	
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT883_BankMaintenance_Sort_ID_Descending() throws Throwable
	{
		BankMaintenance.bankGrid.click_sort_ByColumnName("Name");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		
		BankMaintenance.bankGrid.verifyColumnDescending_numeric("Id");
	
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT884_BankMaintenance_Sort_Name_Ascending() throws Throwable
	{
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Name");
		
		BankMaintenance.bankGrid.verifyColumnAscending_alphabetical("Name");
	
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT884_BankMaintenance_Sort_Name_Descending() throws Throwable
	{
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Name");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Name");
		
		BankMaintenance.bankGrid.verifyColumnDescending_alphabetical("Name");
	
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT882_BankMaintenance_Sort_Entity_Ascending() throws Throwable
	{
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Entity/FI");
		
		BankMaintenance.bankGrid.verifyColumnAscending_alphabetical("Entity/FI");
	
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT882_BankMaintenance_Sort_Entity_Descending() throws Throwable
	{
		BankMaintenance.bankGrid.click_sort_ByColumnName("Id");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Entity/FI");
		BankMaintenance.bankGrid.click_sort_ByColumnName("Entity/FI");
		
		BankMaintenance.bankGrid.verifyColumnDescending_alphabetical("Entity/FI");
	
	}
	
	

	@Test(groups = {"all_tests", "pagination"})
	public void PT884_BankMaintenance_Sort_Name_Defaults_To_Ascending() throws Throwable
	{
		Navigation.navigate_Search_PaymentSearch();
		Navigation.navigate_Admin_BankMaintenance();
		BankMaintenance.bankGrid.verifyColumnAscending_alphabetical("Name");
	
	}


	@Test(groups = {"all_tests", "pagination"})
	public void PT881_Navigation_MultiPage_NextPage_ListsAreDifferent() throws Throwable {
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		previous_pages_texts.addAll(BankMaintenance.bankGrid.columns_Texts_List(BankMaintenance.bankGrid.columns_Elements_List("Name")));
		BankMaintenance.bankGrid.click_next();
		BankMaintenance.bankGrid.verifyColumnAscending_alphabetical("Name");

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		current_pages_texts.addAll(BankMaintenance.bankGrid.columns_Texts_List(BankMaintenance.bankGrid.columns_Elements_List("Name")));

		BankMaintenance.bankGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT881_Navigation_MultiPage_NextPage() throws Throwable {
		BankMaintenance.bankGrid.click_next();
		BankMaintenance.bankGrid.verifyFirstButtonEnabled();
		BankMaintenance.bankGrid.verifyPreviousButtonEnabled();
		BankMaintenance.bankGrid.verifyNextButtonEnabled();
		BankMaintenance.bankGrid.verifyLastButtonEnabled();
	}



	@Test(groups = {"all_tests", "pagination"})
	public void PT881_Navigation_MultiPage_LastPage() throws Throwable {

		BankMaintenance.bankGrid.click_last();
		BankMaintenance.bankGrid.verifyFirstButtonEnabled();
		BankMaintenance.bankGrid.verifyPreviousButtonEnabled();
		BankMaintenance.bankGrid.verifyNextButtonDisabled();
		BankMaintenance.bankGrid.verifyLastButtonDisabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT881_Navigation_MultiPage_previousPage() throws Throwable {
		BankMaintenance.bankGrid.click_last();
		BankMaintenance.bankGrid.click_previous();
		BankMaintenance.bankGrid.verifyFirstButtonEnabled();
		BankMaintenance.bankGrid.verifyPreviousButtonEnabled();
		BankMaintenance.bankGrid.verifyNextButtonEnabled();
		BankMaintenance.bankGrid.verifyLastButtonEnabled();
	}
	@Test(groups = {"all_tests", "pagination"})
	public void PT881_Navigation_MultiPage_firstPage() throws Throwable {
		BankMaintenance.bankGrid.click_last();
		BankMaintenance.bankGrid.click_first();
		BankMaintenance.bankGrid.verifyFirstButtonDisabled();
		BankMaintenance.bankGrid.verifyPreviousButtonDisabled();
		BankMaintenance.bankGrid.verifyNextButtonEnabled();
		BankMaintenance.bankGrid.verifyLastButtonEnabled();
	}

	
	
	@AfterMethod(alwaysRun=true)
	public void writeResult(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
		BankMaintenance.bankGrid.click_first();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
