package r360.tests_ignored.ignoreForNow;

import java.text.MessageFormat;
import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.BankMaintenance;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BankMaint_EditBank_FieldValidation extends BaseTest {

	String id = "3";
	String originalName = "TestBank";

	@BeforeClass(alwaysRun=true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_BankMaintenance();
		BankMaintenance.sort_ID_Column_Ascending();
		Thread.sleep(1000);
	}

	@Test
	public void PT879_BankMaintenance_EditBank_NameField_Required() throws Throwable {
		BankMaintenance.editBank_name(id, "", true);

		WebElement errorText = Locator.lookupElement("BankMain_EditBank_error_Name");
		BaseUI.verifyElementAppears(errorText);
		BaseUI.verifyElementHasExpectedText("BankMain_EditBank_error_Name", "Name is required.");
	}

	@Test
	public void PT878_BankMaintenance_EditBank_ID_is_ReadOnly() throws Throwable {
		BankMaintenance.launch_Edit_By_ID(id);
		WebElement id_textfield = Locator.lookupElement("BankMain_EditBank_ID_textField");

		// Verify element is disabled.
		BaseUI.verifyElementDisabled(id_textfield);
	}

	@Test
	public void PT880_EditBank_Name_Fields_Validation_Invalid_SpecialCharacter_ATSymbol() throws Throwable {
		BankMaintenance.editBank_name(id, "@");
		BankMaintenance.EditBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT880_EditBank_Name_Fields_Validation_Invalid_SpecialCharacter_ExclamationMark() throws Throwable {
		BankMaintenance.editBank_name(id, "!");
		BankMaintenance.EditBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT880_EditBank_Name_Fields_Validation_Invalid_SpecialCharacter_HashTag() throws Throwable {
		BankMaintenance.editBank_name(id, "#");
		BankMaintenance.EditBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT880_EditBank_Name_Fields_Validation_Invalid_SpecialCharacter_DollarSign() throws Throwable {
		BankMaintenance.editBank_name(id, "$");
		BankMaintenance.EditBank_verifyName_charactersNotValid();
	}

	@Test
	public void PT880_EditBank_Entity_Field_Disabled() throws Throwable {
		BankMaintenance.launch_Edit_By_ID(id);
		WebElement entity_input = Locator.lookupElement("BankMain_EditBank_Entity_textField");

		// Assert element is disabled.
		BaseUI.verifyElementDisabled(entity_input);
	}

	@Test
	public void PT874_EditBank_Entity_FieldText_Matches_BankMaint() throws Throwable {
		String bankMaint_Entity_Text = BaseUI.getTextFromField(Locator.lookupElement("BankMain_Entity_ByID", id, null));
		BankMaintenance.launch_Edit_By_ID(id);
		WebElement entity_input = Locator.lookupElement("BankMain_EditBank_Entity_textField");

		BaseUI.verify_true_AndLog(bankMaint_Entity_Text.equals(BaseUI.getTextFromInputBox(entity_input)),
				"Values matched.", MessageFormat.format("Expected: {0}, but saw {1}", bankMaint_Entity_Text,
						BaseUI.getTextFromInputBox(entity_input)));
	}

	@Test
	public void PT873_EditBank_UpdateName_ClickCancel_DoesntUpdate() throws Throwable {
		String newName = "newTest1";
		BankMaintenance.editBank_name(id, newName, false);
		Boolean name_in_list = BankMaintenance.bool_NameInList(newName);

		// Name should not appear in list.
		Assert.assertFalse(name_in_list, MessageFormat.format("Name: {0} appeared in the list.", newName));
		BaseUI.verify_false_AndLog(name_in_list, "Name not in list.",
				MessageFormat.format("Name: {0} appeared in the list.", newName));
		Thread.sleep(6000);
	}

	@Test
	public void PT872_EditBank_UpdateName_ClickSave_ListUpdates() throws Throwable {
		String newName = "newTest2";
		Boolean name_in_list = null;
		// Before we test anything, make sure our new name isn't already in the
		// list.

		name_in_list = BankMaintenance.bool_NameInList(newName);
		// reset test if wrong name is already in list.
		if (name_in_list) {
			BankMaintenance.editBank_name(id, originalName, true);
		}

		BankMaintenance.editBank_name(id, newName, true);
		// We'll check the bool at the end.
		name_in_list = BankMaintenance.bool_NameInList(newName);
		// Reset test back to original name before running Assert.
		BankMaintenance.editBank_name(id, originalName, true);
		Thread.sleep(3000);

		BaseUI.verify_true_AndLog(name_in_list, MessageFormat.format("Name: {0} appeared in the list.", newName),
				MessageFormat.format("Name: {0} did not appear in the list.", newName));
		// Verify that Edit Bank Modal no longer exists.
		Assert.assertTrue(!Browser.driver.getPageSource().contains("bankEditForm"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(1000);

		// Determine if Edit form is displayed and click the Cancel button if it
		// exists.
		if (Browser.driver.getPageSource().contains("bankEditForm"))
			;
		{
			ArrayList<WebElement> elementList = Locator.lookup_multipleElements("BankMain_EditBank_btn_Cancel", null,
					null);
			if (elementList.size() > 0) {
				BaseUI.click(elementList.get(0));
				Thread.sleep(2000);
			}
		}
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
