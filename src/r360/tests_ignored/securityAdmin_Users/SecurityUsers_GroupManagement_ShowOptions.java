package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_GroupManagement_ShowOptions extends BaseTest {

	
//	Pagination userManagementGrid;
//	Pagination availableGroupsGrid;
//	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	String userName = "AutoTest1DontAlter";
	

//	@Override
//	public void setup_method() throws Exception {
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		SecurityAdministration.selectNode(nodeText);

		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();

		// declare our Available Groups grid that we'll use later.

		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
		
		//Verify over 100 entries available.
		Assert.assertTrue(SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count() > 100, "Test Data did not contain over 100 entries.");
	}
	
	//Have  no way of knowing what is exactly on the page due to the scrolling.  
	//So I will check the checkbox for all and move them over to Associated List.
	//Then I'll check Associated List and make sure everything is in there.
	@Test
	public void PT6908_SecurityUser_Management_AvailableList_AllShowsAll() throws Throwable {
		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		
	}
	
	@Test
	public void PT6908_SecurityUser_Management_AvailableList_Show10() throws Throwable {
		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 10));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_10();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(10);
		
	}
	
	@Test
	public void PT6908_SecurityUser_Management_AvailableList_Show25() throws Throwable {
		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 25));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_25();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(25);
	}
	
	@Test
	public void PT6908_SecurityUser_Management_AvailableList_Show50() throws Throwable {
		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 50));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_50();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(50);
	}

	@Test
	public void PT6908_SecurityUser_Management_AvailableList_Show100() throws Throwable {
		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 100));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_100();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(100);
	}


	
	///Associated Group tests
	///
	///
	///
	
	// Have no way of knowing what is exactly on the page due to the scrolling.
	// So I will check the checkbox for all and move them over to Associated
	// List.
	// Then I'll check Associated List and make sure everything is in there.
	@Test
	public void PT6904_SecurityUser_Management_AssociatedList_AllShowsAll() throws Throwable {
		// Start test by moving all of the Available grid entries to the
		// Associated Grid.
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();

		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();

		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));

		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
	}
	
	@Test
	public void PT6904_SecurityUser_Management_AssociatedList_Show10() throws Throwable {
		// Start test by moving all of the Available grid entries to the
		// Associated Grid.
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", 10));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_10();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		

		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(10);
		
	}
	
	@Test
	public void PT6904_SecurityUser_Management_AssociatedList_Show25() throws Throwable {
		// Start test by moving all of the Available grid entries to the
		// Associated Grid.
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", 25));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_25();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		

		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(25);
	}
	
	@Test
	public void PT6904_SecurityUser_Management_AssociatedList_Show50() throws Throwable {
		// Start test by moving all of the Available grid entries to the
		// Associated Grid.
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", 50));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_50();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		

		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(50);
	}
	
	@Test
	public void PT6904_SecurityUser_Management_AssociatedList_Show100() throws Throwable {
		// Start test by moving all of the Available grid entries to the
		// Associated Grid.
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedEntityList = new ArrayList<String>();
		associatedEntityList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", 100));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_100();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		

		ArrayList<String> availableEntityList = new ArrayList<String>();
		availableEntityList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(availableEntityList, associatedEntityList);
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(100);
	}
	

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		//Click Cancel and then re-launch window.
		SecurityAdmin_UserManageModal.click_cancelButton();
		
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		SecurityAdmin_UserManageModal.click_cancelButton();
		Thread.sleep(1000);
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
