package r360.tests_ignored.securityAdmin_Users;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import wfsCommon.pages.Pagination;

public class SecurityUsers_UpdateUser_VerifyPasswordUserRulesTrueValidation extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "WFS";
	private String newEntity = "entityUpdateUsersRulesTrue";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityUpdateUsersRulesTrue Description";
	private String entityExternalID = "TestExternal ID 726";

	String columnName = "Login Name";
	String columnText = "autotest";
	String loginName = "autotest";
	String firstName = "John";
	String lastName = "Barett";
	String userPassword = "Manage#1";
	String email = "test@test.com";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		// deleting entity if it exists
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteEntity(newEntity);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		// Password configuration User Name Rules set to true
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToTrue();

		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(2000);
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		userManagementGrid = new Pagination("userGrid");
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		SecurityAdminAddUser.clickUpdatePasswordButton();
	}

	@Test
	public void PT6981_PasswordCanContainLoginNameSetToTrueAllLowercase() throws Exception {
		String valueToEnter = "autotest";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6981_PasswordCanContainLoginNameSetToTrueAllUppercase() throws Exception {
		String valueToEnter = "AUTOTEST";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6981_PasswordCanContainLoginNameSetToTrueLowercaseAndUppercase() throws Exception {
		String valueToEnter = "AUTOtest";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6983_PasswordCanContainReversedLoginNameSetToTrueAllLowercase() throws Exception {
		String valueToEnter = "tsetotua";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6983_PasswordCanContainReversedLoginNameSetToTrueAllUppercase() throws Exception {
		String valueToEnter = "TSETOTUA";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6983_PasswordCanContainReversedLoginNameSetToTrueLowercaseAndUppercase() throws Exception {
		String valueToEnter = "tsetOTUA";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6985_PasswordCanContainFirstNameSetToTrueAllLowercase() throws Exception {
		String valueToEnter = "john";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6985_PasswordCanContainFirstNameSetToTrueAllUppercase() throws Exception {
		String valueToEnter = "JOHN";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6985_PasswordCanContainFirstNameSetToTrueLowercaseAndUppercase() throws Exception {
		String valueToEnter = "JOhn";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6987_PasswordCanContainUsersLastNameSetTotrueAllLowercase() throws Exception {
		String valueToEnter = "barett";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6987_PasswordCanContainUsersLastNameSetTotrueAllUppercase() throws Exception {
		String valueToEnter = "BARETT";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6880_PasswordCanContainUsersLastNameSetTotrueLowercaseAndUppercase() throws Exception {
		String valueToEnter = "barETT";
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@AfterMethod(alwaysRun = true)
	public void teardown() throws Exception {

		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(2000);
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		SecurityAdminAddUser.clickUpdatePasswordButton();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.deleteUser(columnName, columnText);
		SecurityAdministration.navigate_toEntityTab();
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteEntity(newEntity);
		}
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
