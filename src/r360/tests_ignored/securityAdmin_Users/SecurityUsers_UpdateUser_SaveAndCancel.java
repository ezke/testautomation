package r360.tests_ignored.securityAdmin_Users;


import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class SecurityUsers_UpdateUser_SaveAndCancel extends BaseTest {

	Pagination userManagementGrid;
	private String nodeText = "WFS";
	String columnName = "Login Name";
	String columnText = "Edituser";
	String previousLoginName = "Autotestedit";
	

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		// Browser.defaultBrowser = "firefox";
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		SecurityAdministration.selectNode(nodeText);
		String textToEnter = "mrc";
		EntitySelector.search_ByText(textToEnter);
		Thread.sleep(1000);
		EntitySelector.select_Results("mrc");
		EntitySelector.verify_OnlySpecificEntityPresent(textToEnter);

		if(BaseUI.pageSourceContainsString(previousLoginName)){
			SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, previousLoginName);
			SecurityAdminAddUser.enterFirstName("Tom");
			SecurityAdminAddUser.enterLastName("Cruise");
			SecurityAdminAddUser.enterEmailAddress("email@test.com");
			SecurityAdminAddUser.tabThroughEmailAddress();
			Thread.sleep(1000);
			SecurityAdminAddUser.clickSaveButton();
			Thread.sleep(1000);
		}
		
		if(BaseUI.pageSourceContainsString(columnText)){
			SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
			SecurityAdminAddUser.enterFirstName("Freddy");
			SecurityAdminAddUser.tabThroughEmailAddress();
			Thread.sleep(1000);
			SecurityAdminAddUser.clickSaveButton();
			Thread.sleep(1000);
		}

		userManagementGrid = new Pagination("userGrid");

	}

	@Test
	public void PT7004_UpdateUserSaveUpdates() throws Exception {
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		SecurityAdminAddUser.enterLastName("Test"+GlobalVariables.randomNum);
		SecurityAdminAddUser.tabThroughLastName();
		SecurityAdminAddUser.clickSaveButton();
	}

	@Test
	public void PT7016_ModifyUsersCancelchanges() throws Exception {
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		String valueToSet = "test123";
		SecurityAdminAddUser.enterFirstName(valueToSet);
		SecurityAdminAddUser.tabThroughEmailAddress();
		SecurityAdminAddUser.clickCancelButton();
		WebElement element = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText("First Name", "test123");
		BaseUI.verifyElementDoesNotAppear(element);

	}

	@Test
	public void PT7017_ModifyUsersSaveChanges() throws Exception {
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		String valueToSet = "Standford";
		SecurityAdminAddUser.enterFirstName(valueToSet);
		SecurityAdminAddUser.tabThroughFirstName();
		SecurityAdminAddUser.clickSaveButton();
		WebElement element = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText("First Name", "Standford");
		BaseUI.verifyElementAppears(element);
	}

	@Test
	public void PT6979_UpdateUser_UserUpdatesAllfields() throws Exception {
		String existingLoginName = "Autotestedit";
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, existingLoginName);
		String loginName = "testedit";
		String firstName = "John";
		String middleInitial = "A";
		String lastName = "Barett";
		String email = "test@edit.com";

		SecurityAdminAddUser.enterLoginName(loginName);
		SecurityAdminAddUser.enterFirstName(firstName);
		SecurityAdminAddUser.enterMiddleInitial(middleInitial);
		SecurityAdminAddUser.enterLastName(lastName);
		SecurityAdminAddUser.enterEmailAddress(email);
		SecurityAdminAddUser.tabThroughEmailAddress();
		if (Locator.lookupElement("entityUserAddUserInfoActiveCheckbox").isSelected()) {
			SecurityAdminAddUser.uncheckActiveBox();
		} else {
			SecurityAdminAddUser.checkActiveBox();
		}
		if (Locator.lookupElement("entityUserAddUserInfoAccountIsLockedCheckbox").isSelected()) {
			SecurityAdminAddUser.uncheckAccountIsLockedBox();
		} else {
			SecurityAdminAddUser.checkAccountIsLockedBox();
		}
		if (Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isSelected()) {
			SecurityAdminAddUser.unCheckChangePasswordAtnextLogin();
		} else {
			SecurityAdminAddUser.checkChangePasswordAtnextLogin();
		}
		Thread.sleep(2000);
		SecurityAdminAddUser.clickSaveButton();
		WebElement element1 = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText("Login Name", loginName);
		BaseUI.verifyElementAppears(element1);
		WebElement element2 = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText("First Name", firstName);
		BaseUI.verifyElementAppears(element2);
		WebElement element3 = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText("MI", middleInitial);
		BaseUI.verifyElementAppears(element3);
		WebElement element4 = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText("Email Address", email);
		BaseUI.verifyElementAppears(element4);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		// Updating loginName to previous loginName
		String loginName = "testedit";
		String previousLoginName = "Autotestedit";
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, loginName);
		if (Locator.lookupElement("entityUserEdit_Modal").isDisplayed()) {
			SecurityAdminAddUser.enterLoginName(previousLoginName);
			Thread.sleep(1000);
			SecurityAdminAddUser.clickSaveButton();
			Thread.sleep(1000);
		}
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
