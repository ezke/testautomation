package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Pagin_SecurityUsers_GridDisplay_FilterTests extends BaseTest {

	// Pagination userManagementGrid;
	String nodeText = "AutomationTest";
	String loginName = "TestUser";
	String loginName1 = "TestUser1";
	String lastName = "Ser";
	String lastName1 = "Ser1";
	String firstName = "user";
	String firstName1 = "user1";
	String email = "user@stuff.com";
	String email1 = "testUser1@imaginaryaddress.com";

	@BeforeClass(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		// userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);

	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", loginName);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name",
				loginName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_ClearFilter_AllPreviousLoginsAppear() throws Throwable {
		// get first list of names
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("Login Name")));

		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", loginName);
		BaseUI.click(SecurityAdministration.userManagementGrid.editButton_ByColumnName_andColumnText("Login Name",
				loginName1));
		Thread.sleep(5000);
		BaseUI.click(Locator.lookupElement("entityUserEdit_CancelButton"));
		Thread.sleep(2000);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name",
				loginName);
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("Login Name");

		// get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("Login Name")));

		// verify our 2 lists match.
		SecurityAdministration.userManagementGrid.verifyPagesListsMatch(list_of_names, current_list_of_names);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName_NoResults() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", "abcdf");
		SecurityAdministration.userManagementGrid.verifyColumn_NoResultsFound("Login Name");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName_AllCaps_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", loginName.toUpperCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name",
				loginName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName_LowerCase_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", loginName.toLowerCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name",
				loginName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName_MatchByNumbersAndLetters() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", loginName1);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name",
				loginName1);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName_MatchByNumbers() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", "1");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name", "1");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6896_SecurityUser_FilterBy_LoginName_SpecialCharacter() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Login Name", "@");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Login Name", "@");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", lastName);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", lastName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_ClearFilter_AllPreviousUsersAppear() throws Throwable {
		// get first list of names
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("Last Name")));

		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", lastName);
		BaseUI.click(SecurityAdministration.userManagementGrid.editButton_ByColumnName_andColumnText("Last Name",
				lastName.toLowerCase()));
		Thread.sleep(5000);
		BaseUI.click(Locator.lookupElement("entityUserEdit_CancelButton"));
		Thread.sleep(2000);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", lastName);
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("Last Name");

		// get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("Last Name")));

		// verify our 2 lists match.
		SecurityAdministration.userManagementGrid.verifyPagesListsMatch(list_of_names, current_list_of_names);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName_NoResults() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", "abcdf");
		SecurityAdministration.userManagementGrid.verifyColumn_NoResultsFound("Last Name");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName_AllCaps_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", lastName.toUpperCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", lastName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName_LowerCase_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", lastName.toLowerCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", lastName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName_MatchByNumbersAndLetters() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", lastName1);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", lastName1);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName_MatchByNumbers() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", "1");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", "1");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6895_SecurityUser_FilterBy_LastName_SpecialCharacter() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Last Name", "'");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Last Name", "'");
	}

	/// Email Filter Tests
	///
	///
	///

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_FirstName() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", firstName);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name",
				firstName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_ClearFilter_AllPreviousUsersAppear() throws Throwable {
		// get first list of names
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("First Name")));

		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", firstName);
		BaseUI.click(
				SecurityAdministration.userManagementGrid.editButton_ByColumnName_andColumnText("First Name", "User"));
		Thread.sleep(5000);
		BaseUI.click(Locator.lookupElement("entityUserEdit_CancelButton"));
		Thread.sleep(2000);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name",
				firstName);
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("First Name");

		// get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("First Name")));

		// verify our 2 lists match.
		SecurityAdministration.userManagementGrid.verifyPagesListsMatch(list_of_names, current_list_of_names);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_FirstName_NoResults() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", "abcdf");
		SecurityAdministration.userManagementGrid.verifyColumn_NoResultsFound("First Name");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_FirstName_AllCaps_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", firstName.toUpperCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name",
				firstName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_FirstName_LowerCase_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", firstName.toLowerCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name",
				firstName);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_FirstName_MatchByNumbersAndLetters() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", firstName1);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name",
				firstName1);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6894_SecurityUser_FilterBy_FirstName_MatchByNumbers() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", "1");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name", "1");
	}

	@Test
	public void PT6894_SecurityUser_FilterBy_FirstName_SpecialCharacter() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("First Name", "'");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("First Name", "'");
	}

	/// Email Filter Tests
	///
	///
	///

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", email);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address", email);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_ClearFilter_AllPreviousUsersAppear() throws Throwable {
		// get first list of names
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("Email Address")));

		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", email);
		BaseUI.click(SecurityAdministration.userManagementGrid.editButton_ByColumnName_andColumnText("Email Address",
				email.toLowerCase()));
		Thread.sleep(5000);
		BaseUI.click(Locator.lookupElement("entityUserEdit_CancelButton"));
		Thread.sleep(2000);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address", email);
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("Email Address");

		// get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(SecurityAdministration.userManagementGrid
				.columns_Texts_List(SecurityAdministration.userManagementGrid.columns_Elements_List("Email Address")));

		// verify our 2 lists match.
		SecurityAdministration.userManagementGrid.verifyPagesListsMatch(list_of_names, current_list_of_names);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email_NoResults() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", "abcdf");
		SecurityAdministration.userManagementGrid.verifyColumn_NoResultsFound("Email Address");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email_AllCaps_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", email.toUpperCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address", email);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email_LowerCase_matchesAreCaseInsensitive() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", email.toLowerCase());
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address", email);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email_MatchByNumbersAndLetters() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", email1);
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address",
				email1);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email_MatchByNumbers() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", "1");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address", "1");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT6893_SecurityUser_FilterBy_Email_SpecialCharacter() throws Throwable {
		SecurityAdministration.userManagementGrid.filter_textbox_SendKeys("Email Address", "_");
		SecurityAdministration.userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText("Email Address", "_");
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(500);
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("Login Name");
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("Last Name");
		SecurityAdministration.userManagementGrid.filter_textbox_ClearFilter("First Name");
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}

}
