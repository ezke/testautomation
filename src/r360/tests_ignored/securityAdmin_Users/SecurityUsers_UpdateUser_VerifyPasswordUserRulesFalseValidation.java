package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_UpdateUser_VerifyPasswordUserRulesFalseValidation extends BaseTest {

	Pagination userManagementGrid;
	private String nodeText = "WFS";
	private String newEntity = "entityTestUpdateUsers";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityTestUpdateUsers Description";
	private String entityExternalID = "TestExternal ID 786";

	String columnName = "Login Name";
	String columnText = "autotest";
	String loginName = "autotest";
	String firstName = "John";
	String lastName = "Barett";
	String userPassword = "Manage#1";
	String email = "test@test.com";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.deleteEntity(newEntity);
		}

		SecurityAdministration.selectNode(nodeText);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.selectNode(newEntity);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		// Password configuration User Name Rules set to false
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToFalse();

		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		userManagementGrid = new Pagination("userGrid");
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		SecurityAdminAddUser.clickUpdatePasswordButton();
	}

	@Test
	public void PT6980_UpdateUserPasswordCanContainLoginNameSetToFalseAllLowercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "autotest";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Login name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6980_PasswordCanContainLoginNameSetToFalseAllUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "AUTOTEST";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Login name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6980_PasswordCanContainLoginNameSetToFalseLowercaseAndUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "AUTOtest";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Login name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6982_PasswordCanContainReverseLoginNameSetToFalseAllLowercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "tsetotua";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Reversed login name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6982_PasswordCanContainReverseLoginNameSetToFalseAllUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "TSETOTUA";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Reversed login name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6982_PasswordCanContainReverseLoginNameSetToFalseLowercaseAndUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "tsetOTUA";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Reversed login name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6984_PasswordCanContainUsersFirstNameSetToFalseAllLowercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "john";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. First name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6984_PasswordCanContainUsersFirstNameSetToFalseAllUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "JOHN";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. First name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6984_PasswordCanContainUsersFirstNameSetToFalseLowerCaseAndUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "joHN";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. First name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6986_PasswordCanContainUsersLastNameSetToFalseAllLowerCase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "barett";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Last name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6986_PasswordCanContainUsersLastNameSetToFalseAllUpperCase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "BARETT";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Last name is not allowed in password.",
				valueToEnter);
	}

	@Test
	public void PT6986_PasswordCanContainUsersLastNameSetToFalseLoweraseAndUpperCase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "BARett";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Last name is not allowed in password.",
				valueToEnter);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(2000);
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		SecurityAdminAddUser.clickUpdatePasswordButton();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.deleteUser(columnName, columnText);
		SecurityAdministration.navigate_toEntityTab();
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteEntity(newEntity);
		}
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
