package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_UpdateUser_VerifyPasswordCharacterRulesSettingEnforced extends BaseTest {
	private String nodeText = "WFS";
	private String newEntity = "entityEditPasswordConfigSettingEnforced";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityEditPasswordConfigSettingEnforced Description";
	private String entityExternalID = "TestExternal ID 716";

	String loginName = "Updateuser";
	String firstName = "Update";
	String lastName = "User";
	String userPassword = "Wausau#1";
	String email = "test@test.com";
	String columnName = "Login Name";
	String columnText = "Updateuser";
	String minLength = "8";
	String maxRepeatingChars = "3";
	String minNumChars = "1";
	String minSpecialChars = "1";
	String minUppercaseChars = "1";
	String minLowercaseChars = "1";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);

		SecurityAdministration.selectNode(nodeText);
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.deleteEntity(newEntity);
		}
		Thread.sleep(1000);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verifyPasswordConfigurationCharacterRulesSettingValue(minLength,
				maxRepeatingChars, minNumChars, minSpecialChars, minUppercaseChars, minLowercaseChars);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		SecurityAdminAddUser.clickUpdatePasswordButton();

	}

	@Test
	public void PT6999_UpdateUserVerifyPasswordMinLengthSettingEnforcedLessThanDefinedLength() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password that contain less char than defined min char
		String valueToEnter = "abctee";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6999_UpdateUserPasswordMinLengthSettingEnforcedEqualToDefinedMinLength() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Password that contain equal char to the defined min char
		String valueToEnter = "abctydef";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);

	}

	@Test
	public void PT6999_UpdateUserPasswordMinLengthSettingEnforcedMoreThanDefinedLength() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "abctydefabc";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);

	}

	@Test
	public void PT7003_UpdateUserPasswordMinUppercaseSettingEnforcedLessThanDefinedMinUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "abcty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7003_UpdateUserPasswordMinUppercaseSettingEnforcedEqualToDefinedMinUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "Abcty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);

	}

	@Test
	public void PT7003_UpdateUserPasswordMinUppercaseSettingEnforcedMoreThanDefinedMinUppercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "ABCty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7000_UpdateUserPasswordMinLowercaseSettingEnforcedLessThanDefinedMinLowercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "ABCTY";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7000_UpdateUserPasswordMinLowercaseSettingEnforcedEqualToDefinedMinLowercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "aBCTY";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7000_UpdateUserPasswordMinLowercaseSettingEnforcedMoreThanDefinedMinLowercase() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "abCTY";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7001_UpdateUserPasswordMinNumSettingEnforcedLessThanDefinedMinNum() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "abcty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7001_UpdateUserPasswordMinNumSettingEnforcedEqualToDefinedMinNum() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "abcty1";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7001_UpdateUserPasswordMinNumSettingEnforcedMoreThanDefinedMinNum() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "abcty11";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7002_UpdateUserPasswordMinSpecialCharSettingEnforcedLessThanDefinedMinSpecialChar()
			throws Exception {
		String valueToEnter = "abcty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT7002_UpdateUserPasswordMinSpecialCharSettingEnforcedEqualToDefinedMinSpecialChar()
			throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "@";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits.",
				valueToEnter);
	}

	@Test
	public void PT7002_UpdateUserPasswordMinSpecialCharSettingEnforcedMoreThanDefinedMinSpecialChar()
			throws Exception {
		String valueToEnter = "@*";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits.",
				valueToEnter);
	}

	@Test
	public void PT6998_UpdateUserPasswordMinRepeatingCharSettingEnforcedMoreThanDefinedMinRepeatingChar()
			throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "aaaaty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Password cannot have more than 3 characters in a row.",
				valueToEnter);
	}

	@Test
	public void PT6998_UpdateUserPasswordMinRepeatingCharSettingEnforcedEqualToDefinedMinRepeatingChar()
			throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "aaaty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6998_UpdateUserPasswordMinRepeatingCharSettingEnforcedLessThanDefinedMinRepeatingChar()
			throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "aaty";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
				"entityUserAddUserInfoPasswordTextValidation",
				"Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@AfterMethod(alwaysRun = true)
	public void teardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(2000);
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		SecurityAdminAddUser.clickUpdatePasswordButton();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.deleteUser(columnName, columnText);
		SecurityAdministration.navigate_toEntityTab();
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteEntity(newEntity);
		}
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
