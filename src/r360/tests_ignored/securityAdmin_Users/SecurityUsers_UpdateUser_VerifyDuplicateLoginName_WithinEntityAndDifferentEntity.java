package r360.tests_ignored.securityAdmin_Users;

import java.text.MessageFormat;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_UpdateUser_VerifyDuplicateLoginName_WithinEntityAndDifferentEntity extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "mrc";
	String nodeText1 = "WFS";
	String columnName = "Login Name";
	String columnText = "UpdateUser";
	String existingLoginName = "AutomationTest";
	String loginNameWFSEntity = "Test12356";

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Users();
		Thread.sleep(2000);
		SecurityAdministration.selectNode(nodeText1);

		// checking if user exists in different entity
		userManagementGrid = new Pagination("userGrid");
		userManagementGrid.filter_textbox_SendKeys(columnName, loginNameWFSEntity);
		userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText(columnName, loginNameWFSEntity);

		SecurityAdministration.selectNode(nodeText);
		Thread.sleep(1000);
		if(BaseUI.pageSourceContainsString(loginNameWFSEntity)){
			SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, loginNameWFSEntity);
			SecurityAdminAddUser.enterLoginName(columnText);
			SecurityAdminAddUser.tabThroughEmailAddress();
			SecurityAdminAddUser.clickSaveButton();
			
		}
		if(BaseUI.pageSourceContainsString(columnText)){
			SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
			Thread.sleep(2000);
		}

	}

	@Test
	public void PT6993_UpdateUserVerifyDuplicateLoginCanNotBeUsedWithinSameEntity() throws Exception {
		SecurityAdminAddUser.enterLoginName(existingLoginName);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.verifyUniqueLoginNameErrorAppears();
		SecurityAdminAddUser.verifySaveButtonDisabled();
		String newLoginName = "autouser1";
		SecurityAdminAddUser.enterLoginName(newLoginName);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.verifyUniqueLoginNameErrorDoesNotAppear();
		SecurityAdminAddUser.enterLoginName(columnText);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.clickSaveButton();
		WebElement element1 = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, columnText);
		BaseUI.verify_false_AndLog(element1 == null, MessageFormat.format("Users {0} does exist.", columnText),
				MessageFormat.format("Users {0} does NOT exist.", columnText));

	}

	@Test
	public void PT6992_UpdateUserVerifyDuplicateLoginNameCanBeUsedByDifferentEntity() throws Exception {
		WebElement element = userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText);
		BaseUI.click(element);
		Thread.sleep(2000);
		SecurityAdminAddUser.enterLoginName(loginNameWFSEntity);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.verifyUniqueLoginNameErrorDoesNotAppear();
		SecurityAdminAddUser.verifySaveButtonEnabled();
		SecurityAdminAddUser.clickSaveButton();
		WebElement element1 = userManagementGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName,
				loginNameWFSEntity);
		BaseUI.verify_false_AndLog(element1 == null, MessageFormat.format("Users {0} does exist.", loginNameWFSEntity),
				MessageFormat.format("Users {0} does NOT exist.", loginNameWFSEntity));
		Thread.sleep(2000);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		WebElement element = userManagementGrid.editButton_ByColumnName_andColumnText(columnName, loginNameWFSEntity);
		BaseUI.click(element);
		Thread.sleep(2000);
		SecurityAdminAddUser.enterLoginName(columnText);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.clickSaveButton();
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
