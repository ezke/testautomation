package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_GroupManagement_SelectAll_SinglePage extends BaseTest {

	
//	Pagination userManagementGrid;
//	Pagination availableGroupsGrid;
//	Pagination associatedGroupsGrid;
	String parentNode = "AutomationTest";
	String testEntity = "TestingSinglePage";

//	@Override
//	public void setup_method() throws Exception {
	@BeforeClass(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

//		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.expandNode(parentNode);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.launchEditUserModal_forGivenUser("singlepage");
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		

		// declare our Available Groups grid that we'll use later.
//		availableGroupsGrid = new Pagination("userPageEntityGroupsGrid");
//		associatedGroupsGrid = new Pagination("userPageUserGroupsGrid");
		
		//Verify less than 10 entries in list.
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyLessThan10Entries();
	}

	@Test
	public void PT7011_SecurityUser_Management_AvailableList_AllChecked() throws Throwable {
	
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyRows_AllCheckboxes_Checked();
		
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();

	}
	
	@Test
	public void PT7011_SecurityUser_Management_AvailableList_MoveAll_EmptiesAvailableList() throws Throwable {
	
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();		
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyColumn_NoResultsFound("Name");
	}
	
	@Test
	public void PT7011_SecurityUser_Management_AvailableList_MoveAll_AllEntries_InAssociatedList() throws Throwable {
		//Makes a list of all the Entries in the Available List.
		ArrayList<String> availableListEntries = new ArrayList<String>();
		availableListEntries.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Elements_List("Name")));
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();		
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		ArrayList<String> associatedListEntries = new ArrayList<String>();
		associatedListEntries.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Elements_List("Name")));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(availableListEntries, associatedListEntries);
	}
	
	@Test
	public void PT7010_SecurityUser_Management_AvailableList_MoveAll_AssociatedListCheckall() throws Throwable {
	
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();		
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyRows_AllCheckboxes_Checked();
	}
	
	@Test
	public void PT7010_SecurityUser_Management_AvailableList_MoveAll_AssociatedList_RemoveAll_FromAssociatedList_AssociatedListEmpty() throws Throwable {
	
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();		
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumn_NoResultsFound("Name");
	}
	
	
	@Test
	public void PT7010_SecurityUser_Management_AvailableList_MoveAll_AssociatedList_RemoveAll_FromAssociatedList_AvailableListCorrect() throws Throwable {
		//Makes a list of all the Entries in the Available List.
		
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();		
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		//Make a list of all the entries in Associated list.  At this point they should all be there.
		ArrayList<String> associatedListEntries = new ArrayList<String>();
		associatedListEntries.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Elements_List("Name")));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		ArrayList<String> availableListEntries = new ArrayList<String>();
		availableListEntries.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Elements_List("Name")));
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyPagesListsMatch(associatedListEntries, availableListEntries);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		//Click Cancel and then re-launch window.
		SecurityAdmin_UserManageModal.click_cancelButton();

		SecurityAdministration.launchEditUserModal_forGivenUser("singlepage");
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		
		
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		SecurityAdmin_UserManageModal.click_cancelButton();
		Thread.sleep(1000);
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
