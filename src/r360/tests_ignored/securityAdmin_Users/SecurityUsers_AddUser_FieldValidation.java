package r360.tests_ignored.securityAdmin_Users;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SecurityUsers_AddUser_FieldValidation extends BaseTest {
	String nodeText = "mrc";
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		Thread.sleep(2000);
		
		SecurityAdministration.selectNode(nodeText);
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(2000);
		
	}
	//Start of the test
	@Test
	public void PT6782_AddUser_FirstName_Alpha_Char_50() throws Exception{
		SecurityAdminAddUser.enterFirstName("aaetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshw");
		SecurityAdminAddUser.tabThroughFirstName();
		Thread.sleep(2000);
		String textToVerify= "aaetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshw";
		BaseUI.verifyValueOfField("entityUserAddUserInfoFirstNameText", textToVerify);	
	}
	@Test
	public void PT6782_AddUser_FirstName_Alpha_Numeric_Char_50() throws Exception{
		SecurityAdminAddUser.enterFirstName("aaetewyetywet156489gagwahsgtea784123569iytesuyesh6");
		SecurityAdminAddUser.tabThroughFirstName();
		Thread.sleep(2000);
		String textToVerify= "aaetewyetywet156489gagwahsgtea784123569iytesuyesh6";
		BaseUI.verifyValueOfField("entityUserAddUserInfoFirstNameText", textToVerify);	
	}
	@Test
	public void PT6782_AddUser_FirstName_Alpha_Numeric_Valid_Symbols() throws Exception{
		SecurityAdminAddUser.enterFirstName("abd123,.&()_-:");
		SecurityAdminAddUser.tabThroughFirstName();
		Thread.sleep(2000);
		String textToVerify= "abd123,.&()_-:";
		BaseUI.verifyValueOfField("entityUserAddUserInfoFirstNameText", textToVerify);	
	}
	@Test
	public void PT6782_AddUser_FirstName_Alpha_Numeric_Invalid_Symbols() throws Exception{
		SecurityAdminAddUser.enterFirstName("abd123 ,.&()_-:[[\\**");
		SecurityAdminAddUser.tabThroughFirstName();
		Thread.sleep(2000);
		SecurityAdminAddUser.verifyFirstNameErrorBoxDisplayedForSpecialCharacter();
		
	}
	@Test
	public void PT6782_AddUser_FirstName_Alpha_Numeric_Length_DoesNot_Exceeds_50Char() throws Exception{
		SecurityAdminAddUser.enterFirstName(GlobalVariables.sixtyfiveCharacter);
		String firstNameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("entityUserAddUserInfoFirstNameText"));
		Assert.assertTrue(firstNameInputText.length() <= 50, "Name length was not less than or equal to 50.");
		SecurityAdminAddUser.tabThroughFirstName();
		Thread.sleep(2000);
		
	}
	
	@Test
	public void PT6863_AddUser_LastName_Alpha_Char_50() throws Exception{
		SecurityAdminAddUser.enterLastName("aaetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshw");
		SecurityAdminAddUser.tabThroughLastName();
		Thread.sleep(2000);
		String textToVerify= "aaetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshw";
		BaseUI.verifyValueOfField("entityUserAddUserInfoLastNameText", textToVerify);	
	}
	
	@Test
	public void PT6863_AddUser_LastName_Alpha_Numeric_Char_50() throws Exception{
		SecurityAdminAddUser.enterLastName("aaetewyetywet156489gagwahsgtea784123569iytesuyesh6");
		SecurityAdminAddUser.tabThroughLastName();
		Thread.sleep(2000);
		String textToVerify= "aaetewyetywet156489gagwahsgtea784123569iytesuyesh6";
		BaseUI.verifyValueOfField("entityUserAddUserInfoLastNameText", textToVerify);	
	}
	
	@Test
	public void PT6863_AddUser_LastName_Alpha_Numeric_Valid_Symbols() throws Exception{
		SecurityAdminAddUser.enterLastName("abd123,.&()_-:");
		SecurityAdminAddUser.tabThroughLastName();
		Thread.sleep(2000);
		String textToVerify= "abd123,.&()_-:";
		BaseUI.verifyValueOfField("entityUserAddUserInfoLastNameText", textToVerify);	
	}
	
	@Test
	public void PT6863_AddUser_LastName_Alpha_Numeric_Invalid_Symbols() throws Exception{
		SecurityAdminAddUser.enterLastName("abd123 ,.&()_-:[[\\**");
		SecurityAdminAddUser.tabThroughLastName();
		Thread.sleep(2000);
		String textToVerify= "abd123 ,.&()_-:[[\\**";
		BaseUI.verifyValueOfField("entityUserAddUserInfoLastNameText", textToVerify);	
	}
	
	@Test
	public void PT6863_AddUser_LastName_Alpha_Numeric_Length_DoesNot_Exceeds_50Char() throws Exception{
		SecurityAdminAddUser.enterFirstName(GlobalVariables.sixtyfiveCharacter);
		String lastNameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("entityUserAddUserInfoLastNameText"));
		Assert.assertTrue(lastNameInputText.length() <= 50, "Name length was not less than or equal to 50.");
		SecurityAdminAddUser.tabThroughLastName();
		Thread.sleep(2000);
	}
	
	@Test
	public void PT6865_AddUser_MiddleName_Alpha_Char_One() throws InterruptedException{
		SecurityAdminAddUser.enterMiddleInitial("A");
		SecurityAdminAddUser.tabThroughMiddleInitial();
		Thread.sleep(2000);
		String textToVerify= "A";
		BaseUI.verifyValueOfField("entityUserAddUserInfoM.I.Text", textToVerify);	
	}
	
	@Test
	public void PT6865_AddUser_MiddleName_Numeric_Char_Error() throws InterruptedException{
		SecurityAdminAddUser.enterMiddleInitial("1");
		SecurityAdminAddUser.tabThroughMiddleInitial();
		Thread.sleep(2000);
		SecurityAdminAddUser.verifyMiddleInitialErrorBoxDisplayed();	
	}
	
	@Test
	public void PT6865_AddUser_MiddleName_Alpha_Numeric_Symbol_Error() throws InterruptedException{
		SecurityAdminAddUser.enterMiddleInitial("*");
		SecurityAdminAddUser.tabThroughMiddleInitial();
		Thread.sleep(2000);
		SecurityAdminAddUser.verifyMiddleInitialErrorBoxDisplayed();	
	}
	
	@Test
	public void PT6864_AddUser_LoginName_Alpha_Char_256() throws Exception{
		String loginName = "thisistotestretartsdghgdshgfhjghaghgdhagshgeyghfdghaeadshdsjfhjsdhjsdhjahyeggfgdahgdhgaslphanumericcharaterssoicantestmyautomationtestthatiamwritingsofaricaneriteotherautomationtestificompletethissofaiiamcountingonlycharacterineehsgdhsadyegrsdhgsahgdhagye";
		SecurityAdminAddUser.enterLoginName(loginName);
		SecurityAdminAddUser.tabThroughLoginName();
		Thread.sleep(2000);
		String textToVerify= loginName;
		BaseUI.verifyValueOfField("entityUserAddUserInfoLoginNameText", textToVerify);	
	}
	
	@Test
	public void PT6864_AddUser_LoginName_Alpha_Numeric_Char_256() throws Exception{
		String loginName = "thisistotestretartsdghgds125874612hgdhagshgeyghfdghaeadshdsjfhjsdhjsdhjahyeggfgdahgdhgaslphanumericcharaterssoicantestmyautomationtestthatiamwritingsofaricane852754614514587tiontestificompletethissofaiiamcou45874581247845122351eehsgdhsadyegrsdhgsahgdhagye";
		SecurityAdminAddUser.enterLoginName(loginName);
		SecurityAdminAddUser.tabThroughLoginName();
		Thread.sleep(2000);
		String textToVerify= loginName;
		BaseUI.verifyValueOfField("entityUserAddUserInfoLoginNameText", textToVerify);	
	}
	
	@Test
	public void PT6864_AddUser_LoginName_Alpha_Numeric_Symbols_Char_256() throws Exception{
		String loginName = "thisistotestreta,.&_-:125874612hgdhagshgeyghfdghaeadshdsjfhjsdhjsdhjahyeggfgdahgdhgaslphanumericcharaterssoicantestmyautoma,.&_-:tthatiamwri,.&_-:aricane852754614514587tiontestifi,.&_-:hissofaiiamcou45874581247845122351edyegrsdhgsahgdhagye,.&()_-:,.&()_-:";
		SecurityAdminAddUser.enterLoginName(loginName);
		SecurityAdminAddUser.tabThroughLoginName();
		Thread.sleep(2000);
		String textToVerify= loginName;
		BaseUI.verifyValueOfField("entityUserAddUserInfoLoginNameText", textToVerify);	
	}
	
	@Test
	public void PT6864_AddUser_LoginName_Alpha_Numeric_Length_DoesNot_Exceeds_255Char() throws Exception{
		SecurityAdminAddUser.enterLoginName(GlobalVariables.genericAddUserLongString);
		String LoginnameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("entityUserAddUserInfoLoginNameText"));
		Assert.assertTrue(LoginnameInputText.length() <= 255, "Name length was not less than or equal to 255.");
		SecurityAdminAddUser.tabThroughLoginName();
		Thread.sleep(2000);
	}
	
	@Test
	public void PT6864_AddUser_LoginName_UniqueLoginName_Error() throws Exception{
		String loginName = "AutomationTest";
		SecurityAdminAddUser.enterLoginName(loginName);
		SecurityAdminAddUser.tabThroughLoginName();
		Thread.sleep(2000);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLoginNameValidation"));	
	}
	
	@Test
	public void PT6781_AddUser_EmailAddress_Without_Symbol() throws Exception{
		String emailAddress = "abc.gmail.com";
		SecurityAdminAddUser.enterEmailAddress(emailAddress);
		SecurityAdminAddUser.tabThroughEmailAddress();
		Thread.sleep(2000);
		SecurityAdminAddUser.verifyEmailAddressErrorBoxDisplayed();
	}
	
	@Test
	public void PT6781_AddUser_EmailAddress_Without_Dot_Symbol() throws Exception{
		String emailAddress = "abc@gmailcom";
		SecurityAdminAddUser.enterEmailAddress(emailAddress);
		SecurityAdminAddUser.tabThroughEmailAddress();
		Thread.sleep(2000);
		SecurityAdminAddUser.verifyEmailAddressErrorBoxDisplayed();
	}
	
	@Test
	public void PT6781_AddUser_EmailAddress_NothingAfter_Dot_Symbol() throws Exception{
		String emailAddress = "abc@gmail.";
		SecurityAdminAddUser.enterEmailAddress(emailAddress);
		SecurityAdminAddUser.tabThroughEmailAddress();
		Thread.sleep(2000);
		SecurityAdminAddUser.verifyEmailAddressErrorBoxDisplayed();
	}
	
	@Test
	public void PT6781_AddUser_EmailAddress_ValidEmail() throws Exception{
		SecurityAdminAddUser.enterEmailAddress(GlobalVariables.genericEmailAddress);
		SecurityAdminAddUser.tabThroughEmailAddress();
		SecurityAdminAddUser.verifyEmailAddress(GlobalVariables.genericEmailAddress);
		SecurityAdminAddUser.verifyEmailAddressErrorIsNotDisplayed();
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(2000);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
