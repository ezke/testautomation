package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_GroupManagement_SelectAll_MultiplePages extends BaseTest {

	
//	Pagination userManagementGrid;
//	Pagination availableGroupsGrid;
//	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	

//	@Override
//	public void setup_method() throws Exception {
	@BeforeClass(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		//SecurityAdministration.userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);

		SecurityAdministration.launchEditUserModal_forGivenUser("AutomationTest");
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();

		// declare our Available Groups grid that we'll use later.
//		availableGroupsGrid = new Pagination("userPageEntityGroupsGrid");
//		associatedGroupsGrid = new Pagination("userPageUserGroupsGrid");
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
		
		//Verify over 100 entries available.
		BaseUI.verify_true_AndLog(SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count() > 100, "Test data contained over 100 entries", 
				"Test Data did not contain over 100 entries.");
//		Assert.assertTrue(SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count() > 100, 
//				"Test Data did not contain over 100 entries.");
		//availableGroupsGrid.verifyCountOverIntEntries(availableGroupsGrid.get_row_Count());
	}

	@Test
	public void PT7009_SecurityUser_Management_AvailableList_AllChecked() throws Throwable {
	
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyRows_AllCheckboxes_Checked();
	}
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_MoveToAvailableList() throws Throwable {
		
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
	}

	@Test
	public void PT7009_SecurityUser_Management_AvailableList_MoveAllToAvailableList_ThenToAssociatedList() throws Throwable {
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(availableList, associatedList);
	}
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_SelectAutoAndClickAllCheckbox_AllCheckboxesChecked() throws Throwable {
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_Auto();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyRows_AllCheckboxes_Checked();
		
	}
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_Show10_Verify10Entries() throws Throwable {
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_10();
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyVisibleCountOfEntries(10);
	}
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_Show10_Verify10EntriesMoved_ToAssociatedList() throws Throwable {
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_10();
		Integer associatedGridPreviousCount = SecurityAdmin_UserManageModal.associatedGroupsGrid.get_row_Count();	
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Elements_List("Name")));
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(10 + associatedGridPreviousCount);
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(availableList, associatedList);
	}
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_Show25_Verify25EntriesMoved_ToAssociatedList() throws Throwable {
	
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_25();
		Integer associatedGridPreviousCount = SecurityAdmin_UserManageModal.associatedGroupsGrid.get_row_Count();	
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 25));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_25();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(25 + associatedGridPreviousCount);
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(availableList, associatedList);
		
	}
	
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_Show50_Verify50EntriesMoved_ToAssociatedList() throws Throwable {
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_50();
		Integer associatedGridPreviousCount = SecurityAdmin_UserManageModal.associatedGroupsGrid.get_row_Count();	
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 50));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_50();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(50 + associatedGridPreviousCount);
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(availableList, associatedList);
		
		
		
	}
	
	@Test
	public void PT7009_SecurityUser_Management_AvailableList_Show100_Verify100EntriesMoved_ToAssociatedList() throws Throwable {
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_100();
		Integer associatedGridPreviousCount = SecurityAdmin_UserManageModal.associatedGroupsGrid.get_row_Count();	
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getTopEntries("Name", 100));
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_100();
		SecurityAdmin_UserManageModal.availableGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.move_selected_FromAvailableGrid();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyCountOfEntries(100 + associatedGridPreviousCount);
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(availableList, associatedList);
	}
	
	
	
	
	//Associated Groups
	///
	///
	///
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_AllChecked() throws Throwable {
	
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyRows_AllCheckboxes_Checked();
	}
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_MoveToAvailableList() throws Throwable {
		
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getAllEntries("Name"));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
	}

	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_SelectAutoAndClickAllCheckbox_AllCheckboxesChecked() throws Throwable {
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_Auto();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyRows_AllCheckboxes_Checked();
	}
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_ShowAuto_VerifyAutoEntriesMoved_ToAvailableList() throws Throwable {
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_Auto();
		Integer availableGridPreviousCount = SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count();	
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Elements_List("Name")));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(associatedList.size() + availableGridPreviousCount);
		
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
	}
	
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_Show10_Verify10EntriesMoved_ToAvailableList() throws Throwable {
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_10();
		Integer availableGridPreviousCount = SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count();	
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Elements_List("Name")));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(10 + availableGridPreviousCount);
		
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
	}
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_Show25_Verify25EntriesMoved_ToAvailableList() throws Throwable {
	
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_25();
		Integer availableGridPreviousCount = SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count();
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", 25));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_25();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(25 + availableGridPreviousCount);
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
		
	}
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_Show50_Verify50EntriesMoved_ToAvailableList() throws Throwable {
	
		Integer intCount = 50;
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_50();
		Integer availableGridPreviousCount = SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count();
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", intCount));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_50();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(intCount + availableGridPreviousCount);
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
	}
	
	@Test
	public void PT7008_SecurityUser_Management_AssociatedList_Show100_Verify100EntriesMoved_ToAvailableList() throws Throwable {
	
		Integer intCount = 100;
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_100();
		Integer availableGridPreviousCount = SecurityAdmin_UserManageModal.availableGroupsGrid.get_row_Count();
		ArrayList<String> associatedList = new ArrayList<String>();
		associatedList.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.getTopEntries("Name", intCount));
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_100();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.check_all_checkbox();
		SecurityAdmin_UserManageModal.remove_selected_FromAssociatedGrid();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyCountOfEntries(intCount + availableGridPreviousCount);
		ArrayList<String> availableList = new ArrayList<String>();
		availableList.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.getAllEntries("Name"));
		
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifySecondListContains_AllFirstListEntries(associatedList, availableList);
		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		//Click Cancel and then re-launch window.
		SecurityAdmin_UserManageModal.click_cancelButton();
		Thread.sleep(1000);
		BaseUI.click(SecurityAdministration.userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", "AutomationTest"));
		Thread.sleep(2000);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.availableGroupsGrid.show_All();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.show_All();
		
		
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		SecurityAdmin_UserManageModal.click_cancelButton();
		Thread.sleep(1000);
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
