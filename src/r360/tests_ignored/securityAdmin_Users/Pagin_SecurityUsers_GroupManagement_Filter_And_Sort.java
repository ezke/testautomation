package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class Pagin_SecurityUsers_GroupManagement_Filter_And_Sort extends BaseTest {

	//Pagination userManagementGrid;
//	Pagination availableGroupsGrid;
//	Pagination associatedGroupsGrid;
	String nodeText = "AutomationTest";
	

//	@Override
//	public void setup_method() throws Exception {
	@BeforeClass(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		//userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);

		BaseUI.click(SecurityAdministration.userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", "SomeoneName"));
		Thread.sleep(3000);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();

		// declare our Available Groups grid that we'll use later.
//		availableGroupsGrid = new Pagination("userPageEntityGroupsGrid");
//		associatedGroupsGrid = new Pagination("userPageUserGroupsGrid");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void PT7013_SecurityUser_Management_AvailableList_Filter_VerifyWorks() throws Throwable {
		String groupName = "anothertest";
		SecurityAdmin_UserManageModal.availableGroupsGrid.filter_textbox_SendKeys("Name", groupName);
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyColumn_TextInList_ALLValues_ContainText("Name", groupName);
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT7013_SecurityUser_Management_AvailableList_Filter_VerifyFilterClear_Works() throws Throwable {
		String groupName = "anothertest";
		
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Elements_List("Name")));
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.filter_textbox_SendKeys("Name", groupName);
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyColumn_TextInList_ALLValues_ContainText("Name", groupName);
		SecurityAdmin_UserManageModal.availableGroupsGrid.filter_textbox_ClearFilter("Name");
		
		//get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.availableGroupsGrid.columns_Elements_List("Name")));
		
		//verify our 2 lists match.
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyPagesListsMatch(list_of_names, current_list_of_names);
		
	}
	
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT7012_SecurityUser_Management_AssociatedList_Filter_VerifyWorks() throws Throwable {
		String groupName = "anothertest";
		SecurityAdmin_UserManageModal.associatedGroupsGrid.filter_textbox_SendKeys("Name", groupName);
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumn_TextInList_ALLValues_ContainText("Name", groupName);
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT7012_SecurityUser_Management_AssociatedList_Filter_VerifyFilterClear_Works() throws Throwable {
		String groupName = "anothertest";
		
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Elements_List("Name")));
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.filter_textbox_SendKeys("Name", groupName);
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumn_TextInList_ALLValues_ContainText("Name", groupName);
		
		//get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Texts_List(
				SecurityAdmin_UserManageModal.associatedGroupsGrid.columns_Elements_List("Name")));
		
		//verify our 2 lists match.
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyPagesListsMatch(list_of_names, current_list_of_names);
		
	}
	
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT6910_SecurityUser_Management_AvailableList_SortBy_Name_DefaultsToAscending() throws Throwable {
		SecurityAdmin_UserManageModal.navigate_to_User_InformationTab();
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyColumnAscending_alphabetical("Name");
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT6910_SecurityUser_Management_AvailableList_SortBy_Name_Descending() throws Throwable {
		SecurityAdmin_UserManageModal.navigate_to_User_InformationTab();
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.availableGroupsGrid.click_sort_ByColumnName("Name");
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyColumnDescending_alphabetical("Name");
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void PT6910_SecurityUser_Management_AvailableList_SortBy_Name_Ascending() throws Throwable {
		SecurityAdmin_UserManageModal.navigate_to_User_InformationTab();
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.availableGroupsGrid.click_sort_ByColumnName("Name");
		SecurityAdmin_UserManageModal.availableGroupsGrid.click_sort_ByColumnName("Name");
		
		SecurityAdmin_UserManageModal.availableGroupsGrid.verifyColumnAscending_alphabetical("Name");
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void TC295481_SecurityUser_Management_AssociatedList_SortBy_Name_DefaultsToAscending() throws Throwable {
		SecurityAdmin_UserManageModal.navigate_to_User_InformationTab();
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");
		
	}
	
	
	@Test(groups = {"all_tests", "pagination"})
	public void TC295481_SecurityUser_Management_AssociatedList_SortBy_Name_Descending() throws Throwable {
		SecurityAdmin_UserManageModal.navigate_to_User_InformationTab();
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.click_sort_ByColumnName("Name");
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumnDescending_alphabetical("Name");
		
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void TC295481_SecurityUser_Management_AssociatedList_SortBy_Name_Ascending() throws Throwable {
		SecurityAdmin_UserManageModal.navigate_to_User_InformationTab();
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		SecurityAdmin_UserManageModal.associatedGroupsGrid.click_sort_ByColumnName("Name");
		SecurityAdmin_UserManageModal.associatedGroupsGrid.click_sort_ByColumnName("Name");
		
		SecurityAdmin_UserManageModal.associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");
		
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		SecurityAdmin_UserManageModal.availableGroupsGrid.filter_textbox_ClearFilter("Name");
		SecurityAdmin_UserManageModal.associatedGroupsGrid.filter_textbox_ClearFilter("Name");
		
		Thread.sleep(1000);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		if(BaseUI.elementExists("entityUserEdit_Modal", null, null)){
			SecurityAdmin_UserManageModal.click_cancelButton();
		}
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		};
	}
}
