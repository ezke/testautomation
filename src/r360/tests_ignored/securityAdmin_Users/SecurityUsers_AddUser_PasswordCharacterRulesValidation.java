package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SecurityUsers_AddUser_PasswordCharacterRulesValidation extends BaseTest {
	String nodeText = "mrc";
	String loginName = "AutoTest123";
	String firstName = "John";
	String lastName = "Barett";
	String email = "test@test.com";
	
	String minLength = "8";	String maxRepeatingChars ="3";	String minNumChars = "1";
	String minSpecialChars = "1"; String minUppercaseChars = "1"; String minLowercaseChars = "1";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);

		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verifyPasswordConfigurationCharacterRulesSettingValue(minLength, maxRepeatingChars, 
				minNumChars, minSpecialChars, minUppercaseChars, minLowercaseChars);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();		
		Thread.sleep(2000);
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation(loginName, firstName, lastName, email);
		Thread.sleep(1000);
			
		}
	
		@Test
		public void PT6882_PasswordMinimumChraraterLessThan8() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			//Password that contain less char than defined min char
			String valueToEnter="abctee";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters." , valueToEnter);
			
			
		}
		@Test
		public void PT6882_PasswordMinimumChraraterEqualTo8() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			//Password that contain equal char to the defined min char
			String valueToEnter="abctydef";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters." , valueToEnter);
			
		}
		@Test
		public void PT6882_PasswordMinimumChraraterMoreThan8() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter="abctydefabc";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters." , valueToEnter);
			
		}
		@Test
		public void PT6886_PasswordMinimumUppercaseCharactersLessThan1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "abcty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6886_PasswordMinimumUppercaseCharactersEqualTo1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "Abcty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6886_PasswordMinimumUppercaseCharactersMoreThan1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "ABCty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6883_PasswordMinimumLowercaseCharactersLessThan1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "ABCTY";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6883_PasswordMinimumLowercaseCharactersEqualTO1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "aBCTY";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6883_PasswordMinimumLowercaseCharactersMoreThan1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "abCTY";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6884_PasswordMinimumNumericCharactersLessThan1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "abcty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6884_PasswordMinimumNumericCharactersEqualTO1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "abcty1";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6884_PasswordMinimumNumericCharactersMoreThan1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "abcty11";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6885_PasswordMinimumSpecialCharactersLessThan1() throws Exception{
			String valueToEnter = "abcty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6885_PasswordMinimumSpecialCharactersEqualTO1() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "@";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits.", valueToEnter);
		}
		@Test
		public void PT6885_PasswordMinimumSpecialCharactersMoreThan1() throws Exception{
			String valueToEnter = "@*";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits.", valueToEnter);
		}
		@Test
		public void PT6881_PasswordMinimumRepeatingCharactersMoreThan3() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "aaaaty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Password cannot have more than 3 characters in a row.", valueToEnter);
		}
		@Test
		public void PT6881_PasswordMinimumRepeatingCharactersEqualTo3() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "aaaty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		@Test
		public void PT6881_PasswordMinimumRepeatingCharactersLessThan3() throws Exception{
			//if valueToEnter is updated, errormessage need to be updated accordingly
			String valueToEnter = "aaty";
			SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText",
					"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		}
		
		@AfterMethod(alwaysRun = true)
		public void teardown(ITestResult result) throws Exception{
			ResultWriter.checkForFailureAndScreenshot(result);
			SecurityAdminAddUser.clickCancelButton();
			Thread.sleep(2000);
			SecurityAdminAddUser.clickAddUserButton();
			Thread.sleep(1000);
			SecurityAdmin_UserManageModal.enterUsersForErrorValidation(loginName, firstName, lastName, email);
			Thread.sleep(1000);
		}

		
		
		@AfterClass(alwaysRun = true)
		public void writeResultsAndTearDown() throws Exception {
		if(Locator.lookupElement("entityUserUpdateModal").isDisplayed()){
			SecurityAdminAddUser.clickCancelButton();
		}
			try {
				Navigation.signOut();
			}
			finally {
				Browser.closeBrowser();
			}
		}
}

