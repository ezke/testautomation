package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_AddUser_PasswordUserRulesTrueValidation extends BaseTest {
	private String nodeText = "WFS";
	private String newEntity = "entityTest123";
	private String entityType = "Financial Institution";
	private String entityDescription = "Test123 Description";
	private String entityExternalID = "Test External ID 216";
	String firstName = "John";
	String lastName = "Barett";
	String email = "test@test.com";
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		if(BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)){
			SecurityAdministration.deleteEntity(newEntity);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		//SecurityAdministration.selectNode(newEntity);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToTrue();
		
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();		
		Thread.sleep(2000);
	}
	@Test
	public void PT6874_PasswordCanContainLoginNameSetToTrueAllLowercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "autotest";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6874_PasswordCanContainLoginNameSetToTrueAllUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOTEST", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "AUTOTEST";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@Test
	public void PT6874_PasswordCanContainLoginNameSetToTrueLowercaseAndUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOtest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "AUTOtest";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@Test
	public void PT6876_PasswordCanContainReversedLoginNameSetToTrueAllLowercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "tsetotua";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@Test
	public void PT6876_PasswordCanContainReversedLoginNameSetToTrueAllUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOTEST", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "TSETOTUA";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6876_PasswordCanContainReversedLoginNameSetToTrueLowercaseAndUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOtest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "tsetOTUA";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6878_PasswordCanContainFirstNameSetToTrueAllLowercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", "john", lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "john";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6878_PasswordCanContainFirstNameSetToTrueAllUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", "JOHN", lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "JOHN";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6878_PasswordCanContainFirstNameSetToTrueLowercaseAndUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", "JOhn", lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "JOhn";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6880_PasswordCanContainUsersLastNameSetTotrueAllLowercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, "barett", email);
		Thread.sleep(1000);
		String valueToEnter= "barett";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6880_PasswordCanContainUsersLastNameSetTotrueAllUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, "BARETT", email);
		Thread.sleep(1000);
		String valueToEnter= "BARETT";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6880_PasswordCanContainUsersLastNameSetTotrueLowercaseAndUppercase() throws Exception{
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, "barETT", email);
		Thread.sleep(1000);
		String valueToEnter= "barETT";
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Password error for other rules exists
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(2000);
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(1000);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.navigate_toEntityTab();
			// Remove entity if it exists.
			if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
				SecurityAdministration.deleteEntity(newEntity);
		}
			try {
				Navigation.signOut();
			}
			finally {
				Browser.closeBrowser();
			}
	}
}
