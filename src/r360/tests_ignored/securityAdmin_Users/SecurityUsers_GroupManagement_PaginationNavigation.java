package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.tests.BaseTest;
import wfsCommon.pages.Pagination;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_GroupManagement_PaginationNavigation extends BaseTest {

	Pagination availableGroupsGrid;
	Pagination associatedGroupsGrid;
	String nodeText = "mrc";

//	@Override
//	public void setup_method() throws Exception {
	@BeforeClass(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		SecurityAdministration.selectNode(nodeText);

		SecurityAdministration.launchEditUserModal_forGivenUser("AutomationTest");
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();

		// declare our Available Groups grid that we'll use later.
		availableGroupsGrid = new Pagination("userPageEntityGroupsGrid");
		associatedGroupsGrid = new Pagination("userPageUserGroupsGrid");
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_ListsAreDifferent() throws Throwable {
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		previous_pages_texts
				.addAll(availableGroupsGrid.columns_Texts_List(availableGroupsGrid.columns_Elements_List("Name")));
		availableGroupsGrid.click_next();
		availableGroupsGrid.verifyColumnAscending_alphabetical("Name");

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		current_pages_texts
				.addAll(availableGroupsGrid.columns_Texts_List(availableGroupsGrid.columns_Elements_List("Name")));

		availableGroupsGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstButtonEnabled() throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.verifyFirstButtonEnabled();
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_PreviousButtonEnabled() throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.verifyPreviousButtonEnabled();
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_NextButtonEnabled() throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.verifyNextButtonEnabled();
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_LastButtonEnabled() throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.verifyLastButtonEnabled();
	}



	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_PreviousPage_AscendingOrder() throws Throwable {
		
		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		
		//Click Next.
		availableGroupsGrid.click_next();
		previous_pages_texts
				.addAll(availableGroupsGrid.columns_Texts_List(availableGroupsGrid.columns_Elements_List("Name")));
		Thread.sleep(1000);
		availableGroupsGrid.verifyColumnAscending_alphabetical("Name");
		
		//Click Previous
		availableGroupsGrid.click_previous();
		current_pages_texts
				.addAll(availableGroupsGrid.columns_Texts_List(availableGroupsGrid.columns_Elements_List("Name")));
		availableGroupsGrid.verifyColumnAscending_alphabetical("Name");
		availableGroupsGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_PreviousPage_FirstButtonDisabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_previous();
		availableGroupsGrid.verifyFirstButtonDisabled();

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_PreviousPage_PreviousButtonDisabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_previous();
		availableGroupsGrid.verifyPreviousButtonDisabled();

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_PreviousPage_NextButtonEnabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_previous();
		availableGroupsGrid.verifyNextButtonEnabled();
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_PreviousPage_LastButtonEnabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_previous();
		availableGroupsGrid.verifyLastButtonEnabled();
	}
	
	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_AscendingOrder() throws Throwable {
		
		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();

		availableGroupsGrid.click_next();
		previous_pages_texts
				.addAll(availableGroupsGrid.columns_Texts_List(availableGroupsGrid.columns_Elements_List("Name")));
		Thread.sleep(1000);
		availableGroupsGrid.verifyColumnAscending_alphabetical("Name");

		availableGroupsGrid.click_first();
		current_pages_texts
				.addAll(availableGroupsGrid.columns_Texts_List(availableGroupsGrid.columns_Elements_List("Name")));
		availableGroupsGrid.verifyColumnAscending_alphabetical("Name");
		availableGroupsGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_FirstButtonDisabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_first();
		availableGroupsGrid.verifyFirstButtonDisabled();

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_PreviousButtonDisabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_first();
		availableGroupsGrid.verifyPreviousButtonDisabled();

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_NextButtonEnabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_first();
		availableGroupsGrid.verifyNextButtonEnabled();
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_LastButtonEnabled()
			throws Throwable {
		availableGroupsGrid.click_next();
		availableGroupsGrid.click_first();
		availableGroupsGrid.verifyLastButtonEnabled();
	}
	
	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_LastPage_AscendingOrder() throws Throwable {
		availableGroupsGrid.click_last();
		Thread.sleep(1000);
		availableGroupsGrid.verifyColumnAscending_alphabetical("Name");
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_LastPage_FirstButtonEnabled()
			throws Throwable {
		availableGroupsGrid.click_last();
		availableGroupsGrid.verifyFirstButtonEnabled();

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_LastPage_PreviousButtonEnabled()
			throws Throwable {
		availableGroupsGrid.click_last();
		availableGroupsGrid.verifyPreviousButtonEnabled();

	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_NextButtonDisabled()
			throws Throwable {
		availableGroupsGrid.click_last();
		availableGroupsGrid.verifyNextButtonDisabled();
	}

	@Test
	public void PT6906_SecurityUser_Management_AvailableList_NextPage_FirstPage_LastButtonDisabled()
			throws Throwable {
		availableGroupsGrid.click_last();
		availableGroupsGrid.verifyLastButtonDisabled();
	}
	
	
	
	////Associated Tests///
	///
	///
	///
	///
	
	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_ListsAreDifferent() throws Throwable {
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		previous_pages_texts
				.addAll(associatedGroupsGrid.columns_Texts_List(associatedGroupsGrid.columns_Elements_List("Name")));
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		current_pages_texts
				.addAll(associatedGroupsGrid.columns_Texts_List(associatedGroupsGrid.columns_Elements_List("Name")));

		associatedGroupsGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstButtonEnabled() throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.verifyFirstButtonEnabled();
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_PreviousButtonEnabled() throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.verifyPreviousButtonEnabled();
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_NextButtonEnabled() throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.verifyNextButtonEnabled();
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_LastButtonEnabled() throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.verifyLastButtonEnabled();
	}



	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_PreviousPage_AscendingOrder() throws Throwable {
		
		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		
		//Click Next.
		associatedGroupsGrid.click_next();
		previous_pages_texts
				.addAll(associatedGroupsGrid.columns_Texts_List(associatedGroupsGrid.columns_Elements_List("Name")));
		Thread.sleep(1000);
		associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");
		
		//Click Previous
		associatedGroupsGrid.click_previous();
		current_pages_texts
				.addAll(associatedGroupsGrid.columns_Texts_List(associatedGroupsGrid.columns_Elements_List("Name")));
		associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");
		associatedGroupsGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_PreviousPage_FirstButtonDisabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_previous();
		associatedGroupsGrid.verifyFirstButtonDisabled();

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_PreviousPage_PreviousButtonDisabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_previous();
		associatedGroupsGrid.verifyPreviousButtonDisabled();

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_PreviousPage_NextButtonEnabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_previous();
		associatedGroupsGrid.verifyNextButtonEnabled();
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_PreviousPage_LastButtonEnabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_previous();
		associatedGroupsGrid.verifyLastButtonEnabled();
	}
	
	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_AscendingOrder() throws Throwable {
		
		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();

		associatedGroupsGrid.click_next();
		previous_pages_texts
				.addAll(associatedGroupsGrid.columns_Texts_List(associatedGroupsGrid.columns_Elements_List("Name")));
		Thread.sleep(1000);
		associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");

		associatedGroupsGrid.click_first();
		current_pages_texts
				.addAll(associatedGroupsGrid.columns_Texts_List(associatedGroupsGrid.columns_Elements_List("Name")));
		associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");
		associatedGroupsGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_FirstButtonDisabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_first();
		associatedGroupsGrid.verifyFirstButtonDisabled();

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_PreviousButtonDisabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_first();
		associatedGroupsGrid.verifyPreviousButtonDisabled();

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_NextButtonEnabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_first();
		associatedGroupsGrid.verifyNextButtonEnabled();
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_LastButtonEnabled()
			throws Throwable {
		associatedGroupsGrid.click_next();
		associatedGroupsGrid.click_first();
		associatedGroupsGrid.verifyLastButtonEnabled();
	}
	
	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_LastPage_AscendingOrder() throws Throwable {
		associatedGroupsGrid.click_last();
		Thread.sleep(1000);
		associatedGroupsGrid.verifyColumnAscending_alphabetical("Name");
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_LastPage_FirstButtonEnabled()
			throws Throwable {
		associatedGroupsGrid.click_last();
		associatedGroupsGrid.verifyFirstButtonEnabled();

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_LastPage_PreviousButtonEnabled()
			throws Throwable {
		associatedGroupsGrid.click_last();
		associatedGroupsGrid.verifyPreviousButtonEnabled();

	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_NextButtonDisabled()
			throws Throwable {
		associatedGroupsGrid.click_last();
		associatedGroupsGrid.verifyNextButtonDisabled();
	}

	@Test
	public void PT6905_SecurityUser_Management_AssociatedList_NextPage_FirstPage_LastButtonDisabled()
			throws Throwable {
		associatedGroupsGrid.click_last();
		associatedGroupsGrid.verifyLastButtonDisabled();
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		associatedGroupsGrid.click_first();
		availableGroupsGrid.click_first();
		
		Thread.sleep(1000);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		SecurityAdmin_UserManageModal.click_cancelButton();
		Thread.sleep(1000);
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
