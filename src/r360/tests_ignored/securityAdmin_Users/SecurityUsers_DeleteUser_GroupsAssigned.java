package r360.tests_ignored.securityAdmin_Users;

import java.text.MessageFormat;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_DeleteUser_GroupsAssigned extends BaseTest {

	private String nodeText = "Testing101";
	Pagination userGrid = new Pagination("userGrid");
	String userName1 = "Autotest";
	String userName2 = "TestEdit";
	String userName3 = "testnewuser";
	String columnName = "Login Name";
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		//Browser.defaultBrowser = "internetexplorer";
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		SecurityAdministration.navigate_toUserTab();
		
	}
	
	@Test
	public void PT6889_SecurityUsersDeleteMultipleUsersGroupsAssigned() throws Exception{
		userGrid.check_all_checkbox();
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserErrorPopup"));
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityUserErrorPopupCrossCheckBox", null, null));	
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserDeleteButton"));
		WebElement element1 = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, userName1);
		BaseUI.verify_false_AndLog(element1==null,
				MessageFormat.format("Users {0} is NOT deleted.", "username1"),
				MessageFormat.format("Users {0} was deleted.", "username1"));
		WebElement element2 = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, userName2);
		BaseUI.verify_false_AndLog(element2==null,
				MessageFormat.format("Users {0} is NOT deleted.", "username2"),
				MessageFormat.format("Users {0} was deleted.", "username2"));
		WebElement element3 = userGrid.get_row_ByColumnName_And_ColumnMatchingText(columnName, userName3);
		BaseUI.verify_false_AndLog(element3==null,
				MessageFormat.format("Users {0} is NOT deleted.", "username3"),
				MessageFormat.format("Users {0} was deleted.", "username3"));
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
	}

	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
