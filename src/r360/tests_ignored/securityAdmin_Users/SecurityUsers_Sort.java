package r360.tests_ignored.securityAdmin_Users;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.tests.BaseTest;
import wfsCommon.pages.Pagination;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.ResultWriter;

public class SecurityUsers_Sort extends BaseTest {

	Pagination userManagementGrid;
	String nodeText = "Mosinee Bank";
	
//	@Override
//	public void setup_method() throws Exception
//	{
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		
		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);
		
	}
	
	@Test
	public void PT6900_SecurityUser_Sort_LoginName_DefaultsToAscending() throws Throwable
	{
		Thread.sleep(2000);
		Navigation.navigate_Admin_BankMaintenance();
		Thread.sleep(3000);
		Navigation.navigate_Admin_Users();
		Thread.sleep(3000);
		SecurityAdministration.selectNode(nodeText);
		userManagementGrid.verifyColumnAscending_alphabetical("Login Name");
	}
	
	@Test
	public void PT6900_SecurityUser_Sort_LoginName_Ascending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Last Name");
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.verifyColumnAscending_alphabetical("Login Name");
	}
	
	@Test
	public void PT6900_SecurityUser_Sort_LoginName_Descending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Last Name");
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.verifyColumnDescending_alphabetical("Login Name");
	}
	
	@Test
	public void PT6899_SecurityUser_Sort_LastName_Ascending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("Last Name");
		userManagementGrid.verifyColumnAscending_alphabetical("Last Name");
	}
	
	@Test
	public void PT6899_SecurityUser_Sort_LastName_Descending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("Last Name");
		userManagementGrid.click_sort_ByColumnName("Last Name");
		userManagementGrid.verifyColumnDescending_alphabetical("Last Name");
	}
	
	@Test
	public void PT6898_SecurityUser_Sort_FirstName_Ascending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("First Name");
		userManagementGrid.verifyColumnAscending_alphabetical("First Name");
	}
	
	@Test
	public void PT6898_SecurityUser_Sort_FirstName_Descending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("First Name");
		userManagementGrid.click_sort_ByColumnName("First Name");
		userManagementGrid.verifyColumnDescending_alphabetical("First Name");
	}
	
	@Test
	public void PT6901_SecurityUser_Sort_MiddleInitial_Ascending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("MI");
		userManagementGrid.verifyColumnAscending_alphabetical("MI");
	}
	
	@Test
	public void PT6901_SecurityUser_Sort_MiddleInitial_Descending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("MI");
		userManagementGrid.click_sort_ByColumnName("MI");
		userManagementGrid.verifyColumnDescending_alphabetical("MI");
	}
	
	@Test
	public void PT6897_SecurityUser_Sort_EmailAddress_Ascending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("Email Address");
		userManagementGrid.verifyColumnAscending_alphabetical("Email Address");
	}
	
	@Test
	public void PT6897_SecurityUser_Sort_EmailAddress_Descending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("Email Address");
		userManagementGrid.click_sort_ByColumnName("Email Address");
		userManagementGrid.verifyColumnDescending_alphabetical("Email Address");
	}

	@Test
	public void PT6902_SecurityUser_Sort_NumbOfGroups_Ascending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("# of Groups");
		userManagementGrid.verifyColumnAscending_numeric("# of Groups");
	}
	
	@Test
	public void PT6902_SecurityUser_Sort_NumbOfGroups_Descending() throws Throwable
	{
		userManagementGrid.click_sort_ByColumnName("Login Name");
		userManagementGrid.click_sort_ByColumnName("# of Groups");
		userManagementGrid.click_sort_ByColumnName("# of Groups");
		userManagementGrid.verifyColumnDescending_numeric("# of Groups");
	}


	@AfterMethod(alwaysRun=true)
	public void writeResult(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(1000);
	}
	
	
	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
