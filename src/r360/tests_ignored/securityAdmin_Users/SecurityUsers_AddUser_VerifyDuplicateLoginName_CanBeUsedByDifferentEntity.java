package r360.tests_ignored.securityAdmin_Users;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import wfsCommon.pages.Pagination;

public class SecurityUsers_AddUser_VerifyDuplicateLoginName_CanBeUsedByDifferentEntity extends BaseTest {
	String nodeText = "mrc";
	String nodeText1 = "WFS";
	Pagination userManagementGrid;
	String LoginName= "Test12356";
	String columnName = "Login Name";
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		Thread.sleep(2000);
		
		SecurityAdministration.selectNode(nodeText1);
		
		userManagementGrid = new Pagination("userGrid");
		userManagementGrid.filter_textbox_SendKeys(columnName, "Test12356");
		userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText(columnName, "Test12356");
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(2000);
}
	@Test
	public void PT6867_addUserVerifyDuplicateLoginNameDifferentEntity() throws Exception{
		SecurityAdminAddUser.enterLoginName(LoginName);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.verifyUniqueLoginNameErrorDoesNotAppear();
		SecurityAdminAddUser.enterFirstName(GlobalVariables.genericFirstName);
		SecurityAdminAddUser.enterLastName(GlobalVariables.genericLastName);
		SecurityAdminAddUser.enterEmailAddress(GlobalVariables.genericEmailAddress);
		Thread.sleep(1000);
		SecurityAdminAddUser.enterPassword(GlobalVariables.genericPassword);
		SecurityAdminAddUser.enterConfirmPassword(GlobalVariables.genericConfirmPassword);
		//SecurityAdminAddUser.tabThroughConfirmPassword();
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.clickSaveButton();
		Thread.sleep(2000);
		userManagementGrid.filter_textbox_SendKeys(columnName, "Test12356");
		userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText(columnName, "Test12356");
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdministration.deleteUser(columnName, "Test12356");
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}