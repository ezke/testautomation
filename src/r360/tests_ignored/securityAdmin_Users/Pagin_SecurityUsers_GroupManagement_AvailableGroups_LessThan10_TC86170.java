package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import wfsCommon.pages.Pagination;

public class Pagin_SecurityUsers_GroupManagement_AvailableGroups_LessThan10_TC86170 extends BaseTest {
	Pagination userManagementGrid;
	Pagination testGrid;
	String gridID = "userPageEntityGroupsGrid";

	String nodeToExpand = "AutomationTest";
	String nodeToSelect = "AutomationPermission";
	
	String userText = "AllPermissions";
	
	@BeforeClass
	public void pre_setup() throws Exception {
		//Override these methods to use Base Class for other testing.
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		
		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.expandNode(nodeToExpand);
		SecurityAdministration.selectNode(nodeToSelect);

		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", userText));
		Thread.sleep(3000);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
		

		// declare our Available Groups grid that we'll use later.
		testGrid = new Pagination(gridID);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void First_Button_Disabled() throws Throwable {
		testGrid.verifyFirstButtonDisabled();
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void Previous_Button_Disabled() throws Throwable {
		testGrid.verifyPreviousButtonDisabled();
	}

	
	@Test(groups = {"all_tests", "pagination"})
	public void Next_Button_Disabled() throws Throwable {
		testGrid.verifyNextButtonDisabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void Last_Button_Disabled() throws Throwable {
		testGrid.verifyLastButtonDisabled();
	}

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {

		testGrid.click_first();
		
		Thread.sleep(1000);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		Browser.closeBrowser();
	}
	
}
