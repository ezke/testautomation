package r360.tests_ignored.securityAdmin_Users;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import wfsCommon.pages.Pagination;
import wfsCommon.tests.*;

public class Pagin_SecurityUsers_GroupManagement_AvailableGroups_PT6906 extends Pagination_SecurityUsers_GroupManagement_Nav_Base_Class {
	
	Pagination userManagementGrid;
	Pagination testGrid;
	String gridID = "userPageEntityGroupsGrid";

	String nodeText = "mrc";
	
	@BeforeClass
	public void pre_setup() throws Exception {
		//Override these methods to use Base Class for other testing.

		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);

		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", "AutomationTest"));
		Thread.sleep(3000);
		BaseUI.click(Locator.lookupElement("entityUserEditGroupManage_TabLink"));
		Thread.sleep(2000);

		// declare our Available Groups grid that we'll use later.
		testGrid = new Pagination(gridID);
	}
	
	

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_ListsAreDifferent() throws Throwable {
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		previous_pages_texts
				.addAll(testGrid.columns_Texts_List(testGrid.columns_Elements_List("Name")));
		testGrid.click_next();
		testGrid.verifyColumnAscending_alphabetical("Name");

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		current_pages_texts
				.addAll(testGrid.columns_Texts_List(testGrid.columns_Elements_List("Name")));

		testGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstButtonEnabled() throws Throwable {
		testGrid.click_next();
		testGrid.verifyFirstButtonEnabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_PreviousButtonEnabled() throws Throwable {
		testGrid.click_next();
		testGrid.verifyPreviousButtonEnabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_NextButtonEnabled() throws Throwable {
		testGrid.click_next();
		testGrid.verifyNextButtonEnabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_LastButtonEnabled() throws Throwable {
		testGrid.click_next();
		testGrid.verifyLastButtonEnabled();
	}



	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_PreviousPage_AscendingOrder() throws Throwable {
		
		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		
		//Click Next.
		testGrid.click_next();
		previous_pages_texts
				.addAll(testGrid.columns_Texts_List(testGrid.columns_Elements_List("Name")));
		Thread.sleep(1000);
		testGrid.verifyColumnAscending_alphabetical("Name");
		
		//Click Previous
		testGrid.click_previous();
		current_pages_texts
				.addAll(testGrid.columns_Texts_List(testGrid.columns_Elements_List("Name")));
		testGrid.verifyColumnAscending_alphabetical("Name");
		testGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_PreviousPage_FirstButtonDisabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_previous();
		testGrid.verifyFirstButtonDisabled();

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_PreviousPage_PreviousButtonDisabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_previous();
		testGrid.verifyPreviousButtonDisabled();

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_PreviousPage_NextButtonEnabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_previous();
		testGrid.verifyNextButtonEnabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_PreviousPage_LastButtonEnabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_previous();
		testGrid.verifyLastButtonEnabled();
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_AscendingOrder() throws Throwable {
		
		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();

		testGrid.click_next();
		previous_pages_texts
				.addAll(testGrid.columns_Texts_List(testGrid.columns_Elements_List("Name")));
		Thread.sleep(1000);
		testGrid.verifyColumnAscending_alphabetical("Name");

		testGrid.click_first();
		current_pages_texts
				.addAll(testGrid.columns_Texts_List(testGrid.columns_Elements_List("Name")));
		testGrid.verifyColumnAscending_alphabetical("Name");
		testGrid.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_FirstButtonDisabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_first();
		testGrid.verifyFirstButtonDisabled();

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_PreviousButtonDisabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_first();
		testGrid.verifyPreviousButtonDisabled();

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_NextButtonEnabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_first();
		testGrid.verifyNextButtonEnabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_LastButtonEnabled()
			throws Throwable {
		testGrid.click_next();
		testGrid.click_first();
		testGrid.verifyLastButtonEnabled();
	}
	
	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_LastPage_AscendingOrder() throws Throwable {
		testGrid.click_last();
		Thread.sleep(1000);
		testGrid.verifyColumnAscending_alphabetical("Name");
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_LastPage_FirstButtonEnabled()
			throws Throwable {
		testGrid.click_last();
		testGrid.verifyFirstButtonEnabled();

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_LastPage_PreviousButtonEnabled()
			throws Throwable {
		testGrid.click_last();
		testGrid.verifyPreviousButtonEnabled();

	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_NextButtonDisabled()
			throws Throwable {
		testGrid.click_last();
		testGrid.verifyNextButtonDisabled();
	}

	@Test(groups = {"all_tests", "pagination"})
	public void SecurityUser_Management_NextPage_FirstPage_LastButtonDisabled()
			throws Throwable {
		testGrid.click_last();
		testGrid.verifyLastButtonDisabled();
	}
	
	
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {

		testGrid.click_first();
		
		Thread.sleep(1000);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			if(Locator.lookupElement("entityUserEdit_Modal").isDisplayed())
			{
				SecurityAdmin_UserManageModal.click_cancelButton();
			}
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
	
	
	
}
