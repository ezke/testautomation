package r360.tests_ignored.securityAdmin_Users;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import wfsCommon.pages.Pagination;

public class SecurityUsers_AddUser_DuplicateLoginName_CanNotBeUsedWithinSameEntity extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "mrc";
			String columnName = "Login Name";
			String columnText = "Test12356";
			String existingLoginName="AutomationTest";
			String newLoginName = "testusers123";
			String firstName = "test456";
			String lastName = "test789";
			String email = "aa@aa.com";
	
	
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Admin_Users();
		Thread.sleep(2000);
		
		SecurityAdministration.selectNode(nodeText);
		userManagementGrid = new Pagination("userGrid");
		
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(2000);
	}
	
	@Test
	public void PT6868_AddUserVerifyDuplicateLoginNameCanNotBeUsedWithinSameEntityExistingUser() throws Exception{
		//verifying if unique loginname error appears
		SecurityAdminAddUser.enterLoginName(existingLoginName);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.verifyUniqueLoginNameErrorAppears();
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation(newLoginName, firstName, lastName, email);
		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(3000);
	}
	
	@Test(priority = 1)
	public void PT6868_AddUserVerifyStandardPasswordSetToFalseUnableToEnterPassword1() throws Exception{
		SecurityAdministration.navigate_toEntityTab();
		Thread.sleep(2000);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.unCheckCheckBoxAndSaveOrCancel("entityEntityInfoStdPasswordCheckbox", true);	
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_EntityManageModal.verify_Textbox_Value_isNOT("entityUserAddUserInfoPasswordText", "password");
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
		SecurityAdminAddUser.enterLoginName(GlobalVariables.genericUniqueLoginName);
		SecurityAdminAddUser.tabThroughLoginName();
		SecurityAdminAddUser.verifyUniqueLoginNameErrorDoesNotAppear();
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(3000);
		}
	
	@Test(priority = 2)	
	public void PT6868_AddUserVerifyStandardPasswordToTrueAbleToEnterPassword() throws Exception{
		SecurityAdministration.navigate_toEntityTab();
		SecurityAdministration.launchEditEntityModal_forGivenEntity("mrc");
		SecurityAdmin_EntityManageModal.check_checkBox_And_SaveOrCancel("entityEntityInfoStdPasswordCheckbox", true);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation(columnText, firstName, lastName, email);
		Thread.sleep(1000);
		SecurityAdminAddUser.enterPassword(GlobalVariables.genericPassword);
		SecurityAdminAddUser.enterConfirmPassword(GlobalVariables.genericConfirmPassword);
		SecurityAdminAddUser.tabThroughConfirmPassword();
		Thread.sleep(1000);
		SecurityAdminAddUser.clickSaveButton();
		Thread.sleep(3000);
		userManagementGrid.filter_textbox_SendKeys(columnName, columnText);
		userManagementGrid.verifyColumn_TextInList_ALLValues_ContainText(columnName, columnText);
}

	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if(userManagementGrid.check_box_for_GivenColumnAndText(columnName, columnText).isDisplayed()){
			WebElement element = userManagementGrid.check_box_for_GivenColumnAndText(columnName, columnText);
			BaseUI.click(element);
			Thread.sleep(2000);
			SecurityAdminAddUser.clickDeleteButton();
			Thread.sleep(1000);
			SecurityAdminAddUser.clickConfirmDeleteButton();
			Thread.sleep(2000);
		}
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
	
}
	
