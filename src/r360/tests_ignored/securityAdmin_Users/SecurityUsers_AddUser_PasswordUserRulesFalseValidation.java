package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;



public class SecurityUsers_AddUser_PasswordUserRulesFalseValidation extends BaseTest {
	private String nodeText = "WFS";
	private String newEntity = "entityTestUsers";
	private String entityType = "Financial Institution";
	private String entityDescription = "test Description";
	private String entityExternalID = "TestExternal ID 316";
	String firstName = "John";
	String lastName = "Barett";
	String email = "test@test.com";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)){
			SecurityAdministration.deleteEntity(newEntity);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
//		SecurityAdministration.selectNode(newEntity);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToFalse();

		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();		
		Thread.sleep(2000);
		
	}
	@Test
	public void PT6873_PasswordCanContainLoginNameSetToFalseAllLowercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "autotest";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Login name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6873_PasswordCanContainLoginNameSetToFalseAllUppercase()throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOTEST", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "AUTOTEST";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Login name is not allowed in password.", valueToEnter);
	}
	
	@Test
	public void PT6873_PasswordCanContainLoginNameSetToFalseLowercaseAndUppercase()throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOtest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "AUTOtest";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Login name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6875_PasswordCanContainReverseUserNameSetToFalseAllLowercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("autotest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "tsetotua";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Reversed login name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6875_PasswordCanContainReverseUserNameSetToFalseAllUppercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOTEST", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "TSETOTUA";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Reversed login name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6875_PasswordCanContainReverseUserNameSetToFalseLowercaseAndUppercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("AUTOtest", firstName, lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "tsetOTUA";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Reversed login name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6877_PasswordCanContainUsersFirstNameSetToFalseAllLowercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("test123", "john", lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "john";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. First name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6877_PasswordCanContainUsersFirstNameSetToFalseAllUppercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("test123", "JOHN", lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "JOHN";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. First name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6877_PasswordCanContainUsersFirstNameSetToFalseLowerCaseAndUppercase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("test123", "joHN", lastName, email);
		Thread.sleep(1000);
		String valueToEnter= "joHN";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. First name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6879_PasswordCanContainUsersLastNameSetToFalseAllLowerCase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("test123", firstName, "barett", email);
		Thread.sleep(1000);
		String valueToEnter= "barett";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 uppercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Last name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6879_PasswordCanContainUsersLastNameSetToFalseAllUpperCase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("test123", firstName, "BARETT", email);
		Thread.sleep(1000);
		String valueToEnter= "BARETT";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 lowercase letters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Last name is not allowed in password.", valueToEnter);
	}
	@Test
	public void PT6879_PasswordCanContainUsersLastNameSetToFalseLoweraseAndUpperCase() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		SecurityAdmin_UserManageModal.enterUsersForErrorValidation("test123", firstName, "BARett", email);
		Thread.sleep(1000);
		String valueToEnter= "BARett";
		SecurityAdminAddUser.verifyPasswordError("entityUserAddUserInfoPasswordText", 
				"entityUserAddUserInfoPasswordTextValidation", "Password must have minimum length of 8 characters. Password must have minimum of 1 digits. Password must have minimum of 1 special (non-alphanumeric) characters. Last name is not allowed in password.", valueToEnter);
	}
		
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdminAddUser.clickCancelButton();
		Thread.sleep(2000);
		SecurityAdminAddUser.clickAddUserButton();
		Thread.sleep(1000);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.navigate_toEntityTab();
			// Remove entity if it exists.
			if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
				SecurityAdministration.deleteEntity(newEntity);
		}
			try {
				Navigation.signOut();
			}
			finally {
				Browser.closeBrowser();
			}
	}
	
}
