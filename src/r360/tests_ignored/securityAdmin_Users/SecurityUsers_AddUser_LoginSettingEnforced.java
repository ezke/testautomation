package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SecurityUsers_AddUser_LoginSettingEnforced extends BaseTest {
	private String nodeText = "WFS";
	private String newEntity = "entityLoginSettingEnforced";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityLoginSettingEnforced Description";
	private String entityExternalID = "TestExternal ID 516";
			String firstName = "John"; String lastName = "Barett"; String email = "test123@test.com";
			String inactiveDays = "60"; String maxLogins = "5"; String minLength = "5"; 
			String minNumericChars = "1"; String minAlphaChars= "4"; String minSpecialChars = "1";
			
			
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		
		SecurityAdministration.selectNode(nodeText);
		
		if(BaseUI.pageSourceContainsString(newEntity)){
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
		SecurityAdmin_EntityManageModal.set_LoginConfiguration_Settings(inactiveDays, maxLogins, minLength, minNumericChars, minAlphaChars, minSpecialChars);
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(3000);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();		
		Thread.sleep(2000);
	}
	
	@Test
	public void PT6870_AddUserLoginMinLengthSettingEnforcedLessThanMinLength() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "aut1";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
		
	}
	@Test
	public void PT6870_AddUserLoginMinLengthSettingEnforcedEqualToMinLength() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "autot";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@Test
	public void PT6870_AddUserLoginMinLengthSettingEnforcedMoreThanMinLength() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "auto12";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@Test
	public void PT6869_AdduserLoginMinAlphaSettingEnforcedLessThanMinAlpha() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Error message for other rules appears
		String valueToEnter = "aut1";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6869_AddUserLoginMinAlphaSettingEnforcedEqualToMinAlpha() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Error message for other rules appears
		String valueToEnter= "auto";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	
	@Test
	public void PT6869_AddUserLoginMinAlphaSettingEnforcedMoreThanMinAlpha() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Error message for other rules appears
		String valueToEnter= "autoa";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6871_AddUserLoginMinNumSettingEnforcedLessThanMinNum() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "autoa";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6871_AddUserLoginMinNumSettingEnforcedEqualToMinNum() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Error message for other rules appears
		String valueToEnter= "5";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6871_AddUserLoginMinNumSettingEnforcedMoreThanMinNum() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		//Error message for other rules appears
		String valueToEnter= "15";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6872_AddUserLoginMinSpecialCharSettingEnforcedLessThanMinSpecialChar() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "autoa";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}
	@Test
	public void PT6872_AddUserLoginMinSpecialCharSettingEnforcedEqualToMinSpecialChar() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "@";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 digits.", valueToEnter);
	}
	
	@Test
	public void PT6872_AddUserLoginMinSpecialCharSettingEnforcedMoreThanMinSpecialChar() throws Exception{
		//if valueToEnter is updated, errormessage need to be updated accordingly
		String valueToEnter= "@_";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText", 
				"entityUserAddUserInfoLoginNameValidation", "Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 digits.", valueToEnter);
	}
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.navigate_toEntityTab();
			// Remove entity if it exists.
		if(BaseUI.pageSourceContainsString(newEntity)){
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
			try {
				Navigation.signOut();
			}
			finally {
				Browser.closeBrowser();
			}
	}
		
}
