package r360.tests_ignored.securityAdmin_Users;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SecurityUsers_UpdateUser_SetActiveToTrueAndLockUser extends BaseTest {

	private String nodeText = "Testing101";
	private String userName = "testnewuser";
	private String password = "Wausau#1";

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		SecurityAdministration.selectNode(nodeText);
		Thread.sleep(1000);
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		if (Locator.lookupElement("entityUserAddUserInfoActiveCheckbox").isDisplayed()) {
			SecurityAdminAddUser.uncheckActiveBox();
		} else {
			SecurityAdminAddUser.verifyActiveBoxUnchecked();
		}
		if (Locator.lookupElement("entityUserAddUserInfoAccountIsLockedCheckbox").isDisplayed()) {
			SecurityAdminAddUser.uncheckAccountIsLockedBox();
		}
		SecurityAdminAddUser.clickSaveButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);

	}

	@Test
	public void PT6978_UpdateUserSetActiveToTrue() throws Exception {

		SecurityAdminAddUser.checkActiveBox();
		SecurityAdminAddUser.clickSaveButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		SecurityAdminAddUser.verifyActiveBoxChecked();
		SecurityAdminAddUser.clickCancelButton();
		Browser.closeBrowser();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(nodeText, userName, password);
		HomePage.verifyHeaderLogoAppears();
		HomePage.verifyHeaderTitleAppears();
		Browser.closeBrowser();

	}

	@Test
	public void PT6989_UpdateUserLockUser() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		SecurityAdministration.selectNode(nodeText);
		Thread.sleep(1000);
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		SecurityAdminAddUser.verifyAccountIsLockedBoxUnchecked();
		SecurityAdminAddUser.checkAccountIsLockedBox();
		SecurityAdminAddUser.clickSaveButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		SecurityAdminAddUser.verifyAccountIsLockedBoxChecked();
		SecurityAdminAddUser.clickCancelButton();
		Browser.closeBrowser();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(nodeText, userName, password);
		LoginPage.verifyLoginPageError();
		Thread.sleep(2000);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();

	}

}
