package r360.tests_ignored.securityAdmin_Users;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import wfsCommon.pages.Pagination;

public class SecurityUsers_UpdateUser_VerifyLoginSettingEnforced extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "WFS";
	private String newEntity = "entityEditLoginSettingEnforced";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityEditLoginSettingEnforced Description";
	private String entityExternalID = "TestExternal ID 616";
	String columnName = "Login Name";
	String columnText = "testing@1";
	String loginName = "testing@1";
	String userPassword = "Wausau#1";
	String firstName = "John";
	String lastName = "Barett";
	String email = "test123@test.com";
	String inactiveDays = "60";
	String maxLogins = "5";
	String minLength = "5";
	String minAlphaChars = "4";
	String minNumericChars = "1";
	String minSpecialChars = "1";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		// deleting Entity if it exists
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.deleteEntity(newEntity);
		}
		Thread.sleep(1000);
		SecurityAdministration.launchAddEntityModal();

		// creating new entity
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(newEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();

		// setting LoginConfig defined value greater than 0
		SecurityAdmin_EntityManageModal.set_LoginConfiguration_Settings(inactiveDays, maxLogins, minLength,
				minNumericChars, minAlphaChars, minSpecialChars);
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(3000);
		SecurityAdministration.navigate_toUserTab();

		// SecurityAdminAddUser.clickUsersTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		userManagementGrid = new Pagination("userGrid");
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
	}

	@Test
	public void PT6995_UpdateUserLoginMinLengthSettingEnforcedLessThanDefinedLength() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "aut1";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6995_UpdateUserLoginMinLengthSettingEnforcedEqualToMinLength() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "autot";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6995_UpdateUserLoginMinLengthSettingEnforcedMoreThanDefinedLength() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "auto12";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum of 1 special (non-alphanumeric) characters.", valueToEnter);
	}

	@Test
	public void PT6994_UpdateUserLoginMinAlphaSettingEnforcedLessThanDefinedAlpha() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Error message for other rules appears
		String valueToEnter = "aut1";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6994_UpdateUserLoginMinAlphaSettingEnforcedEqualToDefinedAlpha() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Error message for other rules appears
		String valueToEnter = "auto";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6994_UpdateUserLoginMinAlphaSettingEnforcedMoreThanDefinedAlpha() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Error message for other rules appears
		String valueToEnter = "autoa";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6996_UpdateUserLoginMinNumSettingEnforcedLessThanDefinedMinNum() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "autoa";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6996_UpdateUserLoginMinNumSettingEnforcedEqualToDefinedMinNum() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Error message for other rules appears
		String valueToEnter = "5";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6996_UpdateUserLoginMinNumSettingEnforcedMoreThanDefinedMinNum() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		// Error message for other rules appears
		String valueToEnter = "15";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6997_UpdateUserLoginMinSpecialCharSettingEnforcedLessThanDefinedMinSpecialChar() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "autoa";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum of 1 digits. Login Name must have minimum of 1 special (non-alphanumeric) characters.",
				valueToEnter);
	}

	@Test
	public void PT6997_UpdateUserLoginMinSpecialCharSettingEnforcedEqualToDefinedMinSpecialChar() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "@";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 digits.",
				valueToEnter);
	}

	@Test
	public void PT6997_UpdateUserLoginMinSpecialCharSettingEnforcedMoreThanDefinedMinSpecialChar() throws Exception {
		// if valueToEnter is updated, errormessage need to be updated
		// accordingly
		String valueToEnter = "@_";
		SecurityAdminAddUser.verifyError("entityUserAddUserInfoLoginNameText",
				"entityUserAddUserInfoLoginNameValidation",
				"Login Name must have minimum length of 5 characters. Login Name must have a minimum of 4 letters. Login Name must have minimum of 1 digits.",
				valueToEnter);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		SecurityAdminAddUser.clickCancelButton();
		SecurityAdministration.deleteUser(columnName, columnText);
		SecurityAdministration.navigate_toEntityTab();
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", newEntity, null)) {
			SecurityAdministration.deleteEntity(newEntity);
		}
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
