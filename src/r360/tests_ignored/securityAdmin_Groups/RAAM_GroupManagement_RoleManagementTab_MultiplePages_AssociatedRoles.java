package r360.tests_ignored.securityAdmin_Groups;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests_Code;

public class RAAM_GroupManagement_RoleManagementTab_MultiplePages_AssociatedRoles
		extends Pagin_GridDisplay_FilterTests_Code {

	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_WithLotsOfStuff";
	// String testEntity = "automation_roleManage1";

	@BeforeClass
	public void navigate_To_Page() throws Exception {
		setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
	}

	public void setVariables_AndData() {
		// gridID = "groupPageGroupRolesGrid";
		gridToTest = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid;
		// cancelButton_Identifier = "grpsManage_CancelButton";

		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "A3"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "B2"));

		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "yz"));
		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "yx"));

		filter_ByColumn_ClearFilter.add(new ClassObject_ForFieldAndValue("PT6356", "Name", "setB4"));

		// sort_ByColumn_AscendingAndDescending.add(new
		// ClassObject_ForFieldAndValue("Description", "Alpha"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("", "Name", "Alphabetical"));

		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6324", "multiple", "Name"));

		ShowNumber_testGenerator.add(new ClassObject_ForFieldAndValue("PT6326", "Name", ""));
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move10RolesToAvailableList() throws Throwable {

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_10();
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(10, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move25RolesToAvailableList() throws Throwable {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Ascending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Ascending("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_25();
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		if (!Browser.currentBrowser.equals("chrome")) {
			SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.scroll_ToTopOfList();
		}
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Descending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Ascending("Name");
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();

		if (!Browser.currentBrowser.equals("chrome")) {
			SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.scroll_ToTopOfList();
		}
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Descending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Ascending("Name");
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(25, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move50RolesToAvailableList() throws Throwable {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Descending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Descending("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_50();
		if (!Browser.currentBrowser.equals("chrome")) {
			SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.scroll_ToTopOfList();
		}
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Descending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Ascending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		if (!Browser.currentBrowser.equals("chrome")) {
			SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.scroll_ToTopOfList();
		}
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Descending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Ascending("Name");
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(50, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move100RolesToAvailableList() throws Throwable {

		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Descending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Descending("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_100();
		if (!Browser.currentBrowser.equals("chrome")) {
			SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.scroll_ToTopOfList();
		}
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(100, newList);
	}

	public void extend_AfterClassMethod() throws Exception {
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}

	}

	// public void extend_AfterMethod() throws Exception {
	// SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Ascending("Name");
	// SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Ascending("Name");
	// SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
	// SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
	// SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
	// }

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.close_ToastError_If_ItAppears();

		if (Browser.currentBrowser.equals("internetexplorer")) {
			Thread.sleep(1000);
		}
		Thread.sleep(500);
		if (BaseUI.elementAppears(Locator.lookupElement("grpsManage_Modal"))) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();

		Navigation.close_ToastError_If_ItAppears();

		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();

		

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			extend_AfterClassMethod();
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

	/// Data Driven Tests
	///
	///
	///

	@Test(dataProvider = "Sort", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Sort_By_ColumnName_Ascending(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Sort_By_ColumnName_Ascending(testCase, filterName, filterText);

	}

	@Test(dataProvider = "Sort", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Sort_By_ColumnName_Descending(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Sort_By_ColumnName_Descending(testCase, filterName, filterText);

	}

	@Test(dataProvider = "filter_Field", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Filter_By_ColumnName_WithGivenText(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Filter_By_ColumnName_WithGivenText(testCase, filterName, filterText);
	}

	@Test(dataProvider = "filter_Field_NoResults", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Filter_By_ColumnName_WithGivenText_NoResults(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Filter_By_ColumnName_WithGivenText_NoResults(testCase, filterName, filterText);
	}

	// Some TC's call for you to set a filter, launch a modal, close the modal
	// and then clear the filter.
	// You'll need to set the cancelButton_Identifier variable for these to
	// work. That will be the cancel button identifier
	// to close the modal window.
	@Test(dataProvider = "filter_Field_ClearFilter_AndLaunchModal", alwaysRun = true, groups = { "all_tests",
			"pagination" })
	public void Test_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(String testCase,
			String fieldName, String fieldText) throws Throwable {
		TC_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(testCase, fieldName, fieldText);
	}

	@Test(dataProvider = "filter_Field_ClearFilter", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_FilterAnd_ClearFilter_AllPreviousEntriesAppear(String testCase, String fieldName, String fieldText)
			throws Throwable {
		TC_FilterAnd_ClearFilter_AllPreviousEntriesAppear(testCase, fieldName, fieldText);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_ListsAreDifferent(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_ListsAreDifferent(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstButtonEnabled(testCaseName, testType, columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_NextButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(String testCaseName,
			String testType, String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(testCaseName, testType, columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(String testCaseName,
			String testType, String columnToCheck) throws Throwable {

		Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(testCaseName, testType, columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(String testCaseName,
			String testType, String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_LastPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_AscendingOrder(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(testCaseName, testType, columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_SinglePage_First_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_SinglePage_First_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_SinglePage_Previous_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_SinglePage_Previous_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_SinglePage_Next_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_SinglePage_Next_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_Navigation_SinglePage_Last_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_SinglePage_Last_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	///
	///
	/// End of Single page navigation tests.///

	/// Show Auto/All/100/50/25/10 tests
	///
	///

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_ShowTests_ShowAllOption(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_ShowAllOption(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_ShowTests_ShowAutoOption(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_ShowAutoOption(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_ShowTests_Show10Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show10Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_ShowTests_Show25Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show25Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_ShowTests_Show50Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show50Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = { "all_tests", "pagination" })
	public void Test_ShowTests_Show100Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show100Option(testCase, columnName, extraValue);
	}

}
