package r360.tests_ignored.securityAdmin_Groups;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_GroupManagement_withRoleAndUser extends BaseTest {

	String parentEntity = "AutomationTest";
	String testEntity = "autoGroups_UserAndPolicy";
	String groupName = "testGroup";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		SecurityAdministration.remove_AllRoles_FromGivenGroup(groupName);
		SecurityAdministration.remove_AllUsers_FromGivenGroup(groupName);

	}

	@Test
	public void PT6315_GroupManage_DeleteGroup_With1User_UnableTo() throws Throwable {
		String testUser = "testUser1";
		SecurityAdministration.launchEditGroupModal_forGivenGroup(groupName);
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
		SecurityAdmin_GroupsManagementModal.check_AvailableUserCheckbox(testUser);
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.click_Save();

		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", groupName));
		Thread.sleep(400);
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("error_MessageText"),
				"The Group " + groupName + " has Users and/or Roles associated with it and cannot be deleted.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_Groups_DeleteButton"));
	}

	@Test
	public void PT6315_GroupManage_DeleteGroup_With1Role_UnableTo() throws Throwable {
		
		String testRole_asSeenOnGroupManage_Roles = "testRole1_test: autoGroups_UserAndPolicy";
		SecurityAdministration.launchEditGroupModal_forGivenGroup(groupName);
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
		SecurityAdmin_GroupsManagementModal.check_AvailableRoleCheckbox(testRole_asSeenOnGroupManage_Roles);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.click_Save();

		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", groupName));
		Thread.sleep(400);
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("error_MessageText"),
				"The Group " + groupName + " has Users and/or Roles associated with it and cannot be deleted.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_Groups_DeleteButton"));
	}

	// Adds a group, adds a role. Removes role. Able to Delete Group
	@Test
	public void PT6315_GroupManage_DeleteGroup_RemovedARole_AbleToDelete() throws Throwable {
		String testGroup = "testGroupToDelete1";
		String testRole_asSeenOnGroupManage_Roles = "testRole1_test: autoGroups_UserAndPolicy";

		if (BaseUI.pageSourceContainsString(testGroup)) {
			SecurityAdministration.remove_AllRoles_FromGivenGroup(testGroup);
			SecurityAdministration.remove_AllUsers_FromGivenGroup(testGroup);
			SecurityAdministration.deleteGroup_ForGivenGroupName(testGroup);
		}

		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup(testGroup);
		SecurityAdministration.launchEditGroupModal_forGivenGroup(testGroup);
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
		SecurityAdmin_GroupsManagementModal.check_AvailableRoleCheckbox(testRole_asSeenOnGroupManage_Roles);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.click_Save();

		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", testGroup));
		Thread.sleep(400);
		BaseUI.click(Locator.lookupElement("error_CloseButton"));
		Thread.sleep(500);
		BaseUI.uncheckCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", testGroup));
		SecurityAdministration.remove_AllRoles_FromGivenGroup(testGroup);
		SecurityAdministration.deleteGroup_ForGivenGroupName(testGroup);
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Name", testGroup, false);
	}

	// Adds a group, adds a role. Removes role. Able to Delete Group
	@Test
	public void PT6315_GroupManage_DeleteGroup_RemovedAUser_AbleToDelete() throws Throwable {
		String testGroup = "testGroupToDelete2";
		String testUser = "testUser1";

		if (BaseUI.pageSourceContainsString(testGroup)) {
			SecurityAdministration.remove_AllRoles_FromGivenGroup(testGroup);
			SecurityAdministration.remove_AllUsers_FromGivenGroup(testGroup);
			SecurityAdministration.deleteGroup_ForGivenGroupName(testGroup);
		}

		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup(testGroup);
		SecurityAdministration.launchEditGroupModal_forGivenGroup(testGroup);
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
		SecurityAdmin_GroupsManagementModal.check_AvailableUserCheckbox(testUser);
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.click_Save();

		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", testGroup));
		Thread.sleep(400);
		BaseUI.click(Locator.lookupElement("error_CloseButton"));
		Thread.sleep(500);
		BaseUI.uncheckCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", testGroup));
		SecurityAdministration.remove_AllUsers_FromGivenGroup(testGroup);
		SecurityAdministration.deleteGroup_ForGivenGroupName(testGroup);
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Name", testGroup, false);
	}

	// Test asked to attempt multiple times to delete Group in different states.
	@Test
	public void PT6374_GroupManage_DeleteGroup_WorkingProperly() throws Throwable {
		String groupName_ToDelete = "testGroup_ToDelete";
		String testRole_asSeenOnGroupManage_Roles = "testRole1_test: autoGroups_UserAndPolicy";
		String testUser = "testUser1";

		if (BaseUI.pageSourceContainsString(groupName_ToDelete)) {
			SecurityAdministration.remove_AllRoles_FromGivenGroup(groupName_ToDelete);
			SecurityAdministration.remove_AllUsers_FromGivenGroup(groupName_ToDelete);
			SecurityAdministration.deleteGroup_ForGivenGroupName(groupName_ToDelete);
		}
		//
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.groupNameTxt.enterTextIntoField_AndTab(groupName_ToDelete);
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name",
				testRole_asSeenOnGroupManage_Roles);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testUser);
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.click_Save();
		SecurityAdministration.groupsGrid.check_Checkbox_ForColumnName_AndText("Name", groupName_ToDelete);
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		BaseUI.click(Locator.lookupElement("error_CloseButton"));
		Thread.sleep(500);
		SecurityAdministration.remove_AllUsers_FromGivenGroup(groupName_ToDelete);
		SecurityAdministration.groupsGrid.uncheck_Checkbox_ForColumnName_AndText("Name", groupName_ToDelete);
		SecurityAdministration.groupsGrid.check_Checkbox_ForColumnName_AndText("Name", groupName_ToDelete);
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		BaseUI.click(Locator.lookupElement("error_CloseButton"));
		Thread.sleep(500);
		SecurityAdministration.remove_AllRoles_FromGivenGroup(groupName_ToDelete);
		SecurityAdministration.groupsGrid.uncheck_Checkbox_ForColumnName_AndText("Name", groupName_ToDelete);
		SecurityAdministration.groupsGrid.check_Checkbox_ForColumnName_AndText("Name", groupName_ToDelete);
		BaseUI.verifyElementDoesNOTExist("error_MessagePopup", null, null);
		SecurityAdministration.click_DeleteGroupsButton();
		SecurityAdministration.cancel_DeleteGroupsModal();
		SecurityAdministration.deleteGroup_ForGivenGroupName(groupName_ToDelete);
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Name", groupName_ToDelete, false);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.pageSourceContainsString("toast-item toast-type-error")
				&& Locator.lookupElement("error_MessagePopup").isDisplayed()) {
			BaseUI.click(Locator.lookupElement("error_CloseButton"));
			Thread.sleep(500);
		}

		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}

		if (Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_Modal").isDisplayed()) {
			SecurityAdministration.cancel_DeleteGroupsModal();
		}
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.remove_AllRoles_FromGivenGroup(groupName);
		SecurityAdministration.remove_AllUsers_FromGivenGroup(groupName);
		BaseUI.uncheckCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", groupName));
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
