package r360.tests_ignored.securityAdmin_Groups;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_GroupManagement extends BaseTest {

	String parentEntity = "AutomationTest";
	String testEntity = "automation_groupManage1";
	String testGroup = "testGroup_ToDelete";

	@BeforeClass
	public void setup_method() throws Exception {
		try {
			Browser.openBrowser(GlobalVariables.baseURL);
		} catch (org.openqa.selenium.remote.UnreachableBrowserException e) {
			Browser.closeBrowser();;
			Browser.openBrowser(GlobalVariables.baseURL);

		}
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.expandNode(parentEntity);
		if (BaseUI.pageSourceContainsString(testEntity)) {
			SecurityAdministration.deleteEntity_withGroups(testEntity);
			SecurityAdministration.expandNode(parentEntity);
		}
		if (BaseUI.pageSourceContainsString(testGroup)) {
			SecurityAdministration.remove_AllRoles_FromGivenGroup(testGroup);
			SecurityAdministration.remove_AllUsers_FromGivenGroup(testGroup);
			SecurityAdministration.deleteGroup_ForGivenGroupName(testGroup);
		}

		SecurityAdministration.selectNode(parentEntity);

		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");

		SecurityAdministration.navigate_toGroupsTab();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

	}

	@Test
	public void PT6364_GroupManage_Description_80AlphaNumeric_CharsAccepted() throws Throwable {
		String description = "NeedToGetTo81Chars_Thistakesforeveromg_AboutHalfWay_OkayGettingCloseEndJust2more";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupDescriptiontxt"), description);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		String descriptionOutput = BaseUI.getTextFromInputBox(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		BaseUI.baseStringCompare("Description", description, descriptionOutput);
	}

	@Test
	public void PT6364_GroupManage_Description_SpecialCharacters_ThatAreAllowed() throws Throwable {
		String description = ",.&()_-:Stuff";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupDescriptiontxt"), description);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		String descriptionOutput = BaseUI.getTextFromInputBox(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		BaseUI.baseStringCompare("Description", description, descriptionOutput);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("grpsManage_GroupDescription_ErrorMessage"));
	}

	@Test
	public void PT6364_GroupManage_Description_SpecialCharacters_NotAllowed_ThrowsError() throws Throwable {
		String description = "@#Stuff";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupDescriptiontxt"), description);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		BaseUI.verifyElementAppears(Locator.lookupElement("grpsManage_GroupDescription_ErrorMessage"));
		BaseUI.verifyElementHasExpectedText("grpsManage_GroupDescription_ErrorMessage",
				"Only letters, digits, spaces and & , . : ' ( ) - _ are allowed.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("grpsManage_SaveButton"));

	}

	@Test
	public void PT6364_GroupManage_Description_81CharsEntered_Only80CharsAllowed() throws Throwable {
		String description = "NeedToGetTo81Chars_Thistakesforeveromg_AboutHalfWay_OkayGettingCloseEndJust2more1";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupDescriptiontxt"), description);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		String descriptionOutput = BaseUI.getTextFromInputBox(Locator.lookupElement("grpsManage_GroupDescriptiontxt"));
		BaseUI.baseStringCompare("Description", description.substring(0, 80), descriptionOutput);
	}

	@Test
	public void PT6363_GroupManage_GroupName_50AlphaNumeric_CharsAccepted() throws Throwable {
		String nameToEnter = "IneedFiftycharactersForThisTestThatImDoingAlmost50";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupNameTxt"), nameToEnter);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupNameTxt"));
		String nameOutput = BaseUI.getTextFromInputBox(Locator.lookupElement("grpsManage_GroupNameTxt"));
		BaseUI.baseStringCompare("Name Field", nameToEnter, nameOutput);
	}

	@Test
	public void PT6363_GroupManage_GroupName_SpecialCharacters_ThatAreAllowed() throws Throwable {
		String nameToEnter = ",.&()_-:Stuff";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupNameTxt"), nameToEnter);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupNameTxt"));
		String nameOutput = BaseUI.getTextFromInputBox(Locator.lookupElement("grpsManage_GroupNameTxt"));
		BaseUI.baseStringCompare("Name Field", nameToEnter, nameOutput);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("grpsManage_GroupName_ErrorMessage"));
	}

	@Test
	public void PT6363_GroupManage_GroupName_SpecialCharacters_NotAllowed_ThrowsError() throws Throwable {
		String nameToEnter = "@#Stuff";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupNameTxt"), nameToEnter);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupNameTxt"));
		BaseUI.verifyElementAppears(Locator.lookupElement("grpsManage_GroupName_ErrorMessage"));
		BaseUI.verifyElementHasExpectedText("grpsManage_GroupName_ErrorMessage",
				"Only letters, digits, spaces and & , . : ' ( ) - _ are allowed.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("grpsManage_SaveButton"));

	}

	@Test
	public void PT6363_GroupManage_51CharsEntered_Only50CharsAllowed() throws Throwable {
		String nameToEnter = "IneedFiftycharactersForThisTestThatImDoingAlmost501";
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.enterText(Locator.lookupElement("grpsManage_GroupNameTxt"), nameToEnter);
		BaseUI.tabThroughField(Locator.lookupElement("grpsManage_GroupNameTxt"));
		String nameFieldOutput = BaseUI.getTextFromInputBox(Locator.lookupElement("grpsManage_GroupNameTxt"));
		BaseUI.baseStringCompare("Name Field", nameToEnter.substring(0, 50), nameFieldOutput);
	}

	@Test
	public void PT6349_GroupManage_AddButton_LaunchesGroupsModal() throws Throwable {
		SecurityAdministration.launchAddGroupsModal();
		BaseUI.verifyElementHasFocus(Locator.lookupElement("grpsManage_GroupNameTxt"));
		BaseUI.verifyElementAppearsByString("grpsManage_SaveButton");
		BaseUI.verifyElementAppearsByString("grpsManage_CancelButton");
	}

	@Test
	public void PT6350_GroupManage_CancelButtonClosesModal() throws Throwable {
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.click_Cancel();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("grpsManage_Modal"));
	}

	@Test
	public void PT6351_GroupManage_SaveButtonSavesGroup() throws Throwable {
		String groupName = "testGroup1";
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup(groupName);
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Name", groupName, true);
	}

	@Test
	public void PT6352_GroupManage_Description_AbleToEnter_UpTo80Chars() throws Throwable {
		String validCharacters = "NeedToGetTo81Chars_Thistakesforeveromg_AboutHalfWay_OkayGettingCloseEndJust2more1";
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.groupDescriptionTxt.verifyNO_ErrorMessage_ForGivenField(validCharacters,
				validCharacters.substring(0, 80));
	}

	@Test
	public void PT6314_GroupManage_DeleteGroup_NoUsersOrRoles() throws Throwable {
		String groupName = "testGroup273";
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup(groupName);

		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", groupName));
		SecurityAdministration.deleteGroup_ForGivenGroupName(groupName);
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Name", groupName, false);
	}

	@Test
	public void PT6314_GroupManage_DeleteGroup_deleteModal_ContainsGroupName() throws Throwable {
		String groupName = "testGroup_ForDeleteModal";
		if (!BaseUI.pageSourceContainsString(groupName)) {
			SecurityAdministration.launchAddGroupsModal();
			SecurityAdmin_GroupsManagementModal.addGroup(groupName);
		}
		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", groupName));
		SecurityAdministration.click_DeleteGroupsButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_groupNameLabel"));
		BaseUI.verifyElementHasExpectedText("scrtyAdmin_Groups_DeleteModal_groupNameLabel", groupName);
	}

	@Test
	public void PT6315_GroupManage_DeleteGroup_With1User() throws Throwable {
		String groupName = "testGroup274";
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup(groupName);

		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", groupName));
		SecurityAdministration.deleteGroup_ForGivenGroupName(groupName);
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Name", groupName, false);
	}

	@Test
	public void PT6374_GroupManage_DeleteRole_ConfirmationWindow() throws Throwable {
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.addGroup(testGroup);
		BaseUI.checkCheckbox(SecurityAdministration.groupsGrid.check_box_for_GivenColumnAndText("Name", testGroup));
		SecurityAdministration.click_DeleteGroupsButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_Header"));
		BaseUI.verifyElementHasExpectedText("scrtyAdmin_Groups_DeleteModal_Header", "Confirm Deletion");
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_subHeader"));
		BaseUI.verifyElementHasExpectedText("scrtyAdmin_Groups_DeleteModal_subHeader",
				"The following groups will be deleted:");
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_roleName"));
		BaseUI.verifyElementHasExpectedText("scrtyAdmin_Groups_DeleteModal_groupNameLabel", testGroup);
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_subFooter"));
		BaseUI.verifyElementHasExpectedText("scrtyAdmin_Groups_DeleteModal_subFooter",
				"Deletion of a Group is a permanent operation. Would you like to continue?");
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}

		if (Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_Modal").isDisplayed()) {
			SecurityAdministration.cancel_DeleteGroupsModal();
		}

		if (BaseUI.pageSourceContainsString(testGroup)) {
			SecurityAdministration.remove_AllRoles_FromGivenGroup(testGroup);
			SecurityAdministration.remove_AllUsers_FromGivenGroup(testGroup);
			SecurityAdministration.deleteGroup_ForGivenGroupName(testGroup);
		}

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.navigate_Admin_Entities();
			SecurityAdministration.expandNode(parentEntity);
			if (BaseUI.pageSourceContainsString(testEntity)) {
				SecurityAdministration.deleteEntity_withGroups(testEntity);
			}

			Navigation.signOut();
		}

		finally {
			Browser.closeBrowser();
		}
	}

}
