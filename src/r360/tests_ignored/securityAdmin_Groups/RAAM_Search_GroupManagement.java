package r360.tests_ignored.securityAdmin_Groups;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class RAAM_Search_GroupManagement extends BaseTest {

	private String testEntity = "testEntity_WithLotsOfStuff";
	private String testGroup = "groupNumberA0";
	private String testDescriptionToSave = "Test Description for Modfiy Test1";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		// SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode("WFS");

	}

	@Test
	public void PT6373_Search_GroupManage_ModifyDescription_AndCancel() throws Throwable {
		String newDescription = "Test Description for Modfiy Test2";

		EntitySelector.search_ByText("_Wi");
		EntitySelector.select_Results(testEntity);
		SecurityAdministration.navigate_toGroupsTab();
		SecurityAdministration.launchEditGroupModal_forGivenGroup(testGroup);
		SecurityAdmin_GroupsManagementModal.groupDescriptionTxt.enterTextIntoField_AndTab(newDescription);
		SecurityAdmin_GroupsManagementModal.click_Cancel();
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Description", newDescription, false);

		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);

	}

	@Test
	public void PT6372_Search_GroupManage_ModifyDescription_AndSave() throws Throwable {

		EntitySelector.search_ByText("_Wi");
		EntitySelector.select_Results(testEntity);
		SecurityAdministration.navigate_toGroupsTab();
		
		if (BaseUI.pageSourceContainsString(testDescriptionToSave)) {
			SecurityAdministration.launchEditGroupModal_forGivenGroup(testGroup);
			SecurityAdmin_GroupsManagementModal.groupDescriptionTxt.enterTextIntoField_AndTab("");
			SecurityAdmin_GroupsManagementModal.click_Save();
		}
	
		SecurityAdministration.launchEditGroupModal_forGivenGroup(testGroup);
		SecurityAdmin_GroupsManagementModal.groupDescriptionTxt.enterTextIntoField_AndTab(testDescriptionToSave);
		SecurityAdmin_GroupsManagementModal.click_Save();
		SecurityAdministration.groupsGrid.verifyEntryExists_OR_DoesntExist("Description", testDescriptionToSave, true);

		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}
		
		EntitySelector.close_SearchDropdown();
		EntitySelector.reset_SearchResults();
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {

			if (BaseUI.pageSourceContainsString(testDescriptionToSave)) {
				SecurityAdministration.launchEditGroupModal_forGivenGroup(testGroup);
				SecurityAdmin_GroupsManagementModal.groupDescriptionTxt.enterTextIntoField_AndTab("");
				SecurityAdmin_GroupsManagementModal.click_Save();
			}
			
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
