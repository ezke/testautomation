package r360.tests_ignored.experimental;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests;

public class RAAM_GroupManagement_RoleManagementTab_MultiplePages_AssociatedRoles
		extends Pagin_GridDisplay_FilterTests {

	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_WithLotsOfStuff";
	// String testEntity = "automation_roleManage1";

	@Override
	public void navigate_To_Page() throws Exception {
		//setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
	}

	@Override
	public void setVariables_AndData() {
		// gridID = "groupPageGroupRolesGrid";
		gridToTest = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid;
		// cancelButton_Identifier = "grpsManage_CancelButton";

		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "A3"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "B2"));

		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "yz"));
		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6362", "Name", "yx"));

		filter_ByColumn_ClearFilter.add(new ClassObject_ForFieldAndValue("PT6356", "Name", "setB4"));

		// sort_ByColumn_AscendingAndDescending.add(new
		// ClassObject_ForFieldAndValue("Description", "Alpha"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("", "Name", "Alphabetical"));

		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6324", "multiple", "Name"));

		ShowNumber_testGenerator.add(new ClassObject_ForFieldAndValue("PT6326", "Name", ""));
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move10RolesToAvailableList() throws Throwable {

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_10();
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(10, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move25RolesToAvailableList() throws Throwable {

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_25();
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(25, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move50RolesToAvailableList() throws Throwable {

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_50();
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(50, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6301_GroupManage_RoleManageTab_AssociatedRoles_Move100RolesToAvailableList() throws Throwable {

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.show_100();
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid
				.getAvailableEntries_viaScroll("Name");

		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountOfEntries(100, newList);
	}

	public void extend_AfterClassMethod() throws Exception {
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}

	}

	@Override
	public void extend_AfterMethod() throws Exception {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.sort_Column_Ascending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.sort_Column_Ascending("Name");
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		Thread.sleep(500);
		
		SecurityAdmin_GroupsManagementModal.click_Cancel();
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();

		// Create a list of the unique fields that were in the Column tests
		// List.
		// We'll use this to determine what we have to clear out.
//		ArrayList<String> uniqueFields = new ArrayList<String>();
//		for (ClassObject_ForFieldAndValue dataSet : filter_ByColumn) {
//			if (!uniqueFields.contains(dataSet.fieldName)) {
//				uniqueFields.add(dataSet.fieldName);
//			}
//		}
//
//		for (String field : uniqueFields) {
//			if (!field.equals("") && field != null) {
//				gridToTest.filter_textbox_ClearFilter(field);
//			}
//		}
//
//		gridToTest.show_10();
//		// return to the first page.
//		if (gridToTest.firstPageButton().isEnabled()) {
//			gridToTest.click_first();
//		}
//
//		// Allows us to extend the AfterMethod to allow for additional non-data
//		// driven tests that are specific to the page.
//		// Simply override this method with the new code.
//		extend_AfterMethod();

	}

	
}
