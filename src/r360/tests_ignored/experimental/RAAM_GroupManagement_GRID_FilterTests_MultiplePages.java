package r360.tests_ignored.experimental;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests;

public class RAAM_GroupManagement_GRID_FilterTests_MultiplePages extends Pagin_GridDisplay_FilterTests {

	String parentEntity = "mrc";
	// String testEntity = "automation_roleManage1";

	@Override
	public void navigate_To_Page() throws Exception {
		//setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();

		SecurityAdministration.selectNode(parentEntity);
	}

	@Override
	public void setVariables_AndData() {

		gridToTest = SecurityAdministration.groupsGrid;

		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "AnotherTest"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "11"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "testgroup11"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "&()_"));
		filter_ByColumn
				.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "Test Description For Filters"));
		filter_ByColumn
				.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "TEST DESCRIPTION FOR FILTERS"));
		filter_ByColumn
				.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "test description for filters"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "12345"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "testdescript12345"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "&,.:()-_"));

		filter_ByColumn_ClearFilter.add(new ClassObject_ForFieldAndValue("PT6353", "Description", "12345"));
		filter_ByColumn_ClearFilter.add(new ClassObject_ForFieldAndValue("PT6355", "Name", "AnotherTest"));

		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "avafdfdsf"));
		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "testsomethingorother"));
		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "afdjkdsfjsklfkld"));
		filter_ByColumn_NoResults
				.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "testdescriptionthatappearsnowhere"));

		cancelButton_Identifier = "grpsManage_CancelButton";
		filter_ByColumn_ClearFilter_AndLaunchModal_ThenClose
				.add(new ClassObject_ForFieldAndValue("PT6316", "Description", "Test Description For Filters"));
		filter_ByColumn_ClearFilter_AndLaunchModal_ThenClose
				.add(new ClassObject_ForFieldAndValue("PT6317", "Name", "AnotherTest"));

		sort_ByColumn_AscendingAndDescending
				.add(new ClassObject_ForFieldAndValue("PT6318", "Description", "Alphabetical"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("PT6319", "Name", "Alphabetical"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("PT6320", "# of Roles", "Numeric"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("PT6321", "# of Users", "Numeric"));

		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6322", "multiple", "Name"));

		ShowNumber_testGenerator.add(new ClassObject_ForFieldAndValue("PT6339", "Name", ""));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(500);

		Navigation.navigate_Admin_UserGroups();

		SecurityAdministration.selectNode(parentEntity);

		// Create a list of the unique fields that were in the Column tests
		// List.
		// We'll use this to determine what we have to clear out.
//		ArrayList<String> uniqueFields = new ArrayList<String>();
//		for (ClassObject_ForFieldAndValue dataSet : filter_ByColumn) {
//			if (!uniqueFields.contains(dataSet.fieldName)) {
//				uniqueFields.add(dataSet.fieldName);
//			}
//		}
//
//		for (String field : uniqueFields) {
//			gridToTest.filter_textbox_ClearFilter(field);
//		}
//
//		gridToTest.show_10();
//		// return to the first page.
//		if (gridToTest.firstPageButton().isEnabled()) {
//			gridToTest.click_first();
//		}

		// Allows us to extend the AfterMethod to allow for additional non-data
		// driven tests that are specific to the page.
		// Simply override this method with the new code.

	}


}
