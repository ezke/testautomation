package r360.tests_ignored.experimental;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests;

public class RAAM_GroupManagement_UserManagementTab_AssociatedUsers_SinglePage extends Pagin_GridDisplay_FilterTests {

	String parentEntity = "AutomationTest";
	// String testEntity = "automation_roleManage1";

	@Override
	public void navigate_To_Page() throws Exception {
		//setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.selectNode(parentEntity);

		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
	}

	@Override
	public void setVariables_AndData() {
		// gridID = "groupPageGroupRolesGrid";
		gridToTest = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid;
		// cancelButton_Identifier = "grpsManage_CancelButton";

		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6360", "Name", "tu"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6360", "Name", "en"));

		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6360", "Name", "yz"));
		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6360", "Name", "yx"));

		filter_ByColumn_ClearFilter.add(new ClassObject_ForFieldAndValue("PT6358", "Name", "tu"));

		// sort_ByColumn_AscendingAndDescending.add(new
		// ClassObject_ForFieldAndValue("Description", "Alpha"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("", "Name", "Alphabetical"));

		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6343", "single", ""));
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6309_GroupManage_UsersManageTab_AssociatedUsers_CheckAllWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyCountLessThan_IntEntries(10);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyRows_AllCheckboxes_Checked();
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6309_GroupManage_UsersManageTab_AssociatedUsers_CheckAllAndMoveWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAllEntries("Name");

		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAllEntries("Name");

		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyPagesListsMatch(previousList, newList);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6311_GroupManage_RoleManageTab_AssociatedeRoles_CheckThreeGroupsAndMove() throws Throwable {
		String testentry1 = "newUser5";
		String testentry2 = "SomeoneName";
		String testentry3 = "TestUser1";

		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name",
				testentry1);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name",
				testentry2);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name",
				testentry3);
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();

		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1,
				true);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2,
				true);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3,
				true);

		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1,
				false);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2,
				false);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3,
				false);
	}

	@Test(alwaysRun = true, groups = { "all_tests", "pagination" })
	public void PT6311_GroupManage_RoleManageTab_AssociatedRoles_CheckOneGroupAndMove() throws Throwable {
		String testentry = "newUser5";

		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name", testentry);
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();

		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry,
				true);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry,
				false);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(500);

		// Create a list of the unique fields that were in the Column tests
		// List.
		// We'll use this to determine what we have to clear out.
		ArrayList<String> uniqueFields = new ArrayList<String>();
		for (ClassObject_ForFieldAndValue dataSet : filter_ByColumn) {
			if (!uniqueFields.contains(dataSet.fieldName)) {
				uniqueFields.add(dataSet.fieldName);
			}
		}

		for (String field : uniqueFields) {
			gridToTest.filter_textbox_ClearFilter(field);
		}

		gridToTest.show_10();
		// return to the first page.
		if (gridToTest.firstPageButton().isEnabled()) {
			gridToTest.click_first();
		}

		// Allows us to extend the AfterMethod to allow for additional non-data
		// driven tests that are specific to the page.
		// Simply override this method with the new code.
		extend_AfterMethod();

	}

	public void extend_AfterMethod() throws Exception {

		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.sort_Column_Ascending("Name");
	}

	@Override
	public void extend_AfterClassMethod() throws Exception {
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}
	}

}
