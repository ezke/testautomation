package r360.tests_ignored.experimental;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests;

public class RAAM_GroupManagement_UserManagementTab_MultiplePages_AvailableUsers_NameFilter extends Pagin_GridDisplay_FilterTests {

	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_WithLotsOfStuff";

	@Override
	public void navigate_To_Page() throws Exception {
		//setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
	}
	
	@Override
	public void setVariables_AndData() {
		//gridID = "groupPageGroupRolesGrid";
		gridToTest = SecurityAdmin_GroupsManagementModal.usersAvailableGrid;
		//cancelButton_Identifier = "grpsManage_CancelButton";

		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6344","multiple", "Name"));
		
		ShowNumber_testGenerator.add(new ClassObject_ForFieldAndValue("PT6346", "Name", ""));
	}
	
	
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6308_GroupManage_UsersManageTab_AvailableUsers_Move10UsersToAssociatedList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_10();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyCountOfEntries(10, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6308_GroupManage_UsersManageTab_AvailableUsers_Move25UsersToAssociatedList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_25();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyCountOfEntries(25, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6308_GroupManage_UsersManageTab_AvailableUsers_Move50UsersToAssociatedList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_50();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyCountOfEntries(50, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6308_GroupManage_UsersManageTab_AvailableUsers_Move100UsersToAssociatedList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_100();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyCountOfEntries(100, newList);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(500);

		// Create a list of the unique fields that were in the Column tests
		// List.
		// We'll use this to determine what we have to clear out.
		ArrayList<String> uniqueFields = new ArrayList<String>();
		for (ClassObject_ForFieldAndValue dataSet : filter_ByColumn) {
			if (!uniqueFields.contains(dataSet.fieldName)) {
				uniqueFields.add(dataSet.fieldName);
			}
		}

		for (String field : uniqueFields) {
			gridToTest.filter_textbox_ClearFilter(field);
		}

		gridToTest.show_10();
		// return to the first page.
		if (gridToTest.firstPageButton().isEnabled()) {
			gridToTest.click_first();
		}

		// Allows us to extend the AfterMethod to allow for additional non-data
		// driven tests that are specific to the page.
		// Simply override this method with the new code.
		extend_AfterMethod();

	}
	
	
	public void extend_AfterMethod() throws Exception {
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
	}
	
	
	
	@Override
	public void extend_AfterClassMethod() throws Exception {
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}
	}

	

}
