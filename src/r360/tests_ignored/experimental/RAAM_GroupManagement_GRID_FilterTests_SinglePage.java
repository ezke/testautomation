package r360.tests_ignored.experimental;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests;

public class RAAM_GroupManagement_GRID_FilterTests_SinglePage extends Pagin_GridDisplay_FilterTests {
	
	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_SinglePageTests";

	@Override
	public void navigate_To_Page() throws Exception {
		//setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Admin_UserGroups();
		
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
	}

	@Override
	public void setVariables_AndData() {
		gridToTest = SecurityAdministration.groupsGrid;
		
		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6323","single", ""));

	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(500);

		// Create a list of the unique fields that were in the Column tests
		// List.
		// We'll use this to determine what we have to clear out.
		ArrayList<String> uniqueFields = new ArrayList<String>();
		for (ClassObject_ForFieldAndValue dataSet : filter_ByColumn) {
			if (!uniqueFields.contains(dataSet.fieldName)) {
				uniqueFields.add(dataSet.fieldName);
			}
		}

		for (String field : uniqueFields) {
			gridToTest.filter_textbox_ClearFilter(field);
		}

		gridToTest.show_10();
		// return to the first page.
		if (gridToTest.firstPageButton().isEnabled()) {
			gridToTest.click_first();
		}

		// Allows us to extend the AfterMethod to allow for additional non-data
		// driven tests that are specific to the page.
		// Simply override this method with the new code.
		extend_AfterMethod();

	}
	

}
