package r360.tests_ignored.experimental;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests;

public class RAAM_GroupManagement_RoleManagementTab_AvailableRoles_SinglePage extends Pagin_GridDisplay_FilterTests {

	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_SinglePageTests";
	//String testEntity = "automation_roleManage1";

	@Override
	public void navigate_To_Page() throws Exception {
		//setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		
		SecurityAdministration.launchEditGroupModal_forGivenGroup("groupNumberA0");
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
	}
	
	@Override
	public void setVariables_AndData() {
		gridToTest = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid;
		
		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6328", "single", ""));
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6304_GroupManage_RoleManageTab_AvailableRoles_CheckAllWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountLessThan_IntEntries(10);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyRows_AllCheckboxes_Checked();
	}

	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6304_GroupManage_RoleManageTab_AvailableRoles_CheckAllAndMoveWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.getAllEntries("Name");
		
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.getAllEntries("Name");
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyPagesListsMatch(previousList, newList);
	}

	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6306_GroupManage_RoleManageTab_AvailableRoles_CheckOneGroupAndMove() throws Throwable {
		String testentry ="secondsetB0: testEntity_SinglePageTests";
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry, true);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry, false);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6306_GroupManage_RoleManageTab_AvailableRoles_CheckThreeGroupsAndMove() throws Throwable {
		String testentry1 ="secondsetB0: testEntity_SinglePageTests";
		String testentry2 ="secondsetB1: testEntity_SinglePageTests";
		String testentry3 ="secondsetB3: testEntity_SinglePageTests";
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry1);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry2);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry3);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1, true);
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2, true);
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3, true);
		
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1, false);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2, false);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3, false);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.uncheck_all_checkbox();
		
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
	}
	
	
}
