package r360.tests_ignored.experimental;

import java.util.ArrayList;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;

public class RAAM_GroupManagement_UserManagementTab_MultiplePages_AssociatedUsers_ShowTests extends BaseTest {

	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_WithLotsOfStuff";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		
	}
	

	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6307_GroupManage_UsersManageTab_AssociatedUsers_Move10UsersToAvailableList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_10();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyCountOfEntries(10, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6307_GroupManage_UsersManageTab_AssociatedUsers_Move25UsersToAvailableList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_25();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyCountOfEntries(25, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6307_GroupManage_UsersManageTab_AssociatedUsers_Move50UsersToAvailableList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_50();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyCountOfEntries(50, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6307_GroupManage_UsersManageTab_AssociatedUsers_Move100UsersToAvailableList() throws Throwable {
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_100();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAvailableEntries_viaScroll("Name");
		
		
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_All();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAvailableEntries_viaScroll("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyPagesListsMatch(previousList, newList);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyCountOfEntries(100, newList);
	}
	
	
	@AfterMethod
	public void afterMethod() throws Exception {
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.show_10();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_10();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.sort_Column_Ascending("Name");
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.sort_Column_Ascending("Name");
	}

}
