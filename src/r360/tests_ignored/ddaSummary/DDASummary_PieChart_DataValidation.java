package r360.tests_ignored.ddaSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.DDA_Summary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DDASummary_PieChart_DataValidation extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		//Browser.defaultBrowser = "internetexplorer";
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		// Navigation.navigate_Admin_Users();
		// All tests should start at the same starting point.
		Navigation.navigate_DDASummary();
		DDA_Summary.depositDate.enter_Date(BaseUI.getTodaysDate_AsString());
	}
	
	@Test(alwaysRun = true)
	public void PT629_TotalAmount_VerifyPieChartData() throws Throwable {
		DDA_Summary.verify_TotalAmounts_PieChart();
	}

	
	
	@Test(alwaysRun = true)
	public void PT629_PaymentCounts_VerifyPieChartData() throws Throwable {
		DDA_Summary.verify_TotalPayments_PieChart();
	}

	


	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
	
	
}//End of Class
