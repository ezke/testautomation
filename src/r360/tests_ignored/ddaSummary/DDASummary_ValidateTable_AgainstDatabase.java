package r360.tests_ignored.ddaSummary;

import java.text.MessageFormat;
import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import r360.pages.DDA_Summary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class DDASummary_ValidateTable_AgainstDatabase extends BaseTest {

	TableData ddaSummary_data;
	TableData databaseData;

	String todaysDate_ForWebsite = BaseUI.getDateAsString_InRelationToTodaysDate(0);
	String todaysDate_ForDatabase =BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");
	
	@BeforeClass
	public void setup_method() throws Exception {
		databaseData = DDA_Summary.return_DataBase_Data_For_DDASummary(todaysDate_ForDatabase);
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_DDASummary();

		//DDA_Summary.depositDate.enter_Date("08/06/2016");
		DDA_Summary.depositDate.enter_Date(todaysDate_ForWebsite);

		if (Browser.currentBrowser.equals("firefox")) {
			Navigation.wait_for_LoadingSpinner_ToDisappear();
		}
		// databaseData =
		// DDA_Summary.return_DataBase_Data_For_DDASummary("20160806");
		DDA_Summary.sort_Descending_ByColumnName("Total ($)");
		ddaSummary_data = DDA_Summary.extract_DDASummary_nonTitleLines();
		BaseUI.verify_true_AndLog(ddaSummary_data.data.size() > 0, "Found table data on DDA Summary page.",
				"Unable to extract table data from DDA Summary page.");

	}

	@DataProvider(name = "tableRow")
	public Object[][] createData_SortTests() throws Exception {
		//databaseData = DDA_Summary.return_DataBase_Data_For_DDASummary("20160806");
		

		
		BaseUI.verify_true_AndLog(databaseData.data.size() > 0, "Found database data.",
				"Failed to find database data.");
		Object[][] rowObject = new Object[databaseData.data.size()][2];

		for (int i = 0; i < databaseData.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = databaseData.data.get(i);
		}

		return rowObject;
	}

	@Test(dataProvider = "tableRow")
	public void TC_Compare_TableData_Against_DatabaseData(int index, HashMap<String, String> tableRow)
			throws Throwable {

		BaseUI.verify_true_AndLog(ddaSummary_data.data.size() == databaseData.data.size(),
				"Website table matches database in size.", "Website does NOT match database table in size.");

		String output = "";
		for (String key : ddaSummary_data.data.get(index).keySet()) {
			if (tableRow.get(key).equals(ddaSummary_data.data.get(index).get(key))) {

			} else {
				output += MessageFormat.format("For column {0}, expected value: {1}, but seeing value {2}", key,
						tableRow.get(key), ddaSummary_data.data.get(index).get(key))
						+ System.getProperty("line.separator");
			}

		}

		BaseUI.verify_true_AndLog(output.equals(""), "Table data matched table results on DDA Summary page.", output);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
