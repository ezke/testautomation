package r360.tests_ignored.ddaSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.DDA_Summary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DDASummary_tests extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		//Browser.defaultBrowser = "internetexplorer";
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		// Navigation.navigate_Admin_Users();
		// All tests should start at the same starting point.
		Navigation.navigate_DDASummary();

	}

	@Test(alwaysRun = true)
	public void TC168296_DDASummary_Header_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("ddaSum_Header"));
	}

	@Test(alwaysRun = true)
	public void PT629_DepositDateTextField_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("ddaSum_DepositDate_Textbox"));
	}

	@Test(alwaysRun = true)
	public void PT629_DepositDateCalendarIcon_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("ddaSum_CalendarIcon"));
	}

	@Test(alwaysRun = true)
	public void PT629_DepositDateCalendarText_DefaultsToTodaysDate() throws Throwable {
		Navigation.navigate_DDASummary();

		String todaysDate = DDA_Summary.depositDate.getTodaysDate_AsString();
		String inputBoxDate = BaseUI.getTextFromInputBox(Locator.lookupElement("ddaSum_DepositDate_Textbox"));

		BaseUI.baseStringCompare("Default Date", todaysDate, inputBoxDate);
	}

	@Test(alwaysRun = true)
	public void PT629_DepositDateCalendarText_EnterYesterdaysDate() throws Throwable {
		String yesterdaysDate = DDA_Summary.depositDate.getDateAsString_InRelationToTodaysDate(-1);
		DDA_Summary.depositDate.verify_DateEntered_IsDateViewed(yesterdaysDate);
	}

	@Test(alwaysRun = true)
	public void PT629_DepositDateCalendar_DatePicker_PickPreviousDate() throws Throwable {
		String twoDaysAgoDate = DDA_Summary.depositDate.getDateAsString_InRelationToTodaysDate(-2);
		DDA_Summary.depositDate.verify_DateSelectedFromCalendar_IsDateViewed(twoDaysAgoDate);
	}


	@Test(alwaysRun = true)
	public void PT629_TotalAmounts_VerifyPieChartLegend_MatchesDDASummaryList() throws Throwable {
		DDA_Summary.depositDate.enter_Date(BaseUI.getTodaysDate_AsString());
		DDA_Summary.verify_TotalAmounts_legendkey_Matches_DDASummaryList();
		
	}
	
//	@Test(alwaysRun = true)
//	public void PT629_TotalAmount_VerifyPieChartData() throws Throwable {
//		DDA_Summary.depositDate.enter_Date("09/27/2016");
//		DDA_Summary.verify_TotalAmounts_PieChart();
//		//DDA_Summary.VerifyDDASummary_Against_TotalAmounts_PieChart();
//	}

	@Test(alwaysRun = true)
	public void PT629_TotalAmount_VerifyPaymenTotals() throws Throwable {
		DDA_Summary.depositDate.enter_Date(BaseUI.getTodaysDate_AsString());
		DDA_Summary.verify_TotalAmounts_TotalAmount();
	}

	@Test(alwaysRun = true)
	public void PT629_PaymentCounts_VerifyTotalPayments() throws Throwable {
		DDA_Summary.depositDate.enter_Date(BaseUI.getTodaysDate_AsString());
		DDA_Summary.verify_PaymentCounts_TotalPayments();
	}
	
	
	@Test(alwaysRun = true)
	public void PT629_PaymentCounts_VerifyPieChartLegend_MatchesDDASummaryList() throws Throwable {
		String today=BaseUI.getTodaysDate_AsString();
		DDA_Summary.depositDate.enter_Date(today);
		DDA_Summary.verify_TotalCounts_legendkey_Matches_DDASummaryList();
		
	}
	
//	@Test(alwaysRun = true)
//	public void PT629_PaymentCounts_VerifyPieChartData() throws Throwable {
//		DDA_Summary.depositDate.enter_Date("09/27/2016");
//		DDA_Summary.verify_TotalPayments_PieChart();
//		//DDA_Summary.VerifyDDASummary_Against_TotalPayments_PieChart();
//	}
	
	
	@Test(alwaysRun = true)
	public void PT629_TotalAmount_11Entries_PieChartNotAvailable() throws Throwable {
		DDA_Summary.depositDate.enter_Date("08/10/2016");
		BaseUI.verifyElementAppears(Locator.lookupElement("ddaSum_TotalAmounts_NotAvailableMessage"));
	}
	
	@Test(alwaysRun = true)
	public void PT629_TotalPayments_11Entries_PieChartNotAvailable() throws Throwable {
		DDA_Summary.depositDate.enter_Date("08/10/2016");
		BaseUI.verifyElementAppears(Locator.lookupElement("ddaSum_TotalPayments_NotAvailableMessage"));
	}
	
	@Test(alwaysRun = true)
	public void PT629_TotalAmount_10Entries_Present_InPieChart() throws Throwable {
		DDA_Summary.depositDate.enter_Date("08/06/2016");
		DDA_Summary.verify_PieChart_Contains_CorrectEntries(10, DDA_Summary.TotalAmounts);
	}
	
	@Test(alwaysRun = true)
	public void PT629_TotalPayments_10Entries_Present_InPieChart() throws Throwable {
		DDA_Summary.depositDate.enter_Date("08/06/2016");
		DDA_Summary.verify_PieChart_Contains_CorrectEntries(10, DDA_Summary.PaymentCounts);
	}
	
	@Test(alwaysRun = true)
	public void PT629_TotalAmount_9Entries_Present_InPieChart() throws Throwable {
		DDA_Summary.depositDate.enter_Date("08/07/2016");
		DDA_Summary.verify_PieChart_Contains_CorrectEntries(9, DDA_Summary.TotalAmounts);
	}
	
	@Test(alwaysRun = true)
	public void PT629_TotalPayments_9Entries_Present_InPieChart() throws Throwable {
		DDA_Summary.depositDate.enter_Date("08/07/2016");
		DDA_Summary.verify_PieChart_Contains_CorrectEntries(9, DDA_Summary.PaymentCounts);
	}
	
	
	@Test(alwaysRun = true)
	public void TC183147_DepositDate_NoSpecialCharsAllowed() throws Throwable {
		DDA_Summary.depositDate.enter_Date("10!@#$%^&*()-_+={}[]|\\<>?,.`~/4/2017");
		String actualDepositDate = DDA_Summary.depositDate.get_Text();
		BaseUI.baseStringCompare("Deposit Date", "10/04/2017", actualDepositDate);
	}
	
//	@Test(alwaysRun = true)
//	public void TC_DatabaseTest() throws ClassNotFoundException, SQLException
//	{
//
//		String query = "declare @Date int" + System.getProperty("line.separator") 
//				+ "set @Date = '20160806'" + System.getProperty("line.separator")
//				+ "SELECT" + System.getProperty("line.separator")
//				+ 	   "[DepositDateKey]" +  System.getProperty("line.separator")
//				+ 	   ",b.DDA,d.EntityName as 'Entity',c.SiteClientAccountID as 'WG ID',c.LongName as Workgroup" +  System.getProperty("line.separator")
//				+ 	   ",e.LongName as 'Payment Source',f.LongName as 'Payment Type'" +  System.getProperty("line.separator")
//				+ 	   ",count(*) over (partition by b.dda)as 'Payment Count'" +  System.getProperty("line.separator")
//				+ 	   ",SUM(Amount) over (partition by b.dda)as 'Total($)'" +  System.getProperty("line.separator")
//				+ 	   "FROM [WFSDB_R360].[RecHubData].[factChecks] a" +  System.getProperty("line.separator")
//				+ 	   "inner join WFSDB_R360.RecHubData.dimDDAs b on a.DDAKey = b.DDAKey" +  System.getProperty("line.separator")
//				+ 	   "inner join WFSDB_R360.rechubdata.dimBatchSources e on e.BatchSourceKey = a.BatchSourceKey" +  System.getProperty("line.separator")
//				+ 	   "inner join WFSDB_R360.RecHubData.dimBatchPaymentTypes f on f.BatchPaymentTypeKey = a.BatchPaymentTypeKey" +  System.getProperty("line.separator")
//				+ 	   "inner join WFSDB_R360.RecHubData.dimClientAccounts c on a.ClientAccountKey = c.ClientAccountKey" +  System.getProperty("line.separator")
//				+ 	   "inner join raam.dbo.Entities d on d.EntityID = e.EntityID" +  System.getProperty("line.separator")
//				+ 	   "where" +  System.getProperty("line.separator")
//				+ 	   "IsDeleted = 0" +  System.getProperty("line.separator")
//				+ 	   "and DepositDateKey = @Date" +  System.getProperty("line.separator")
//				+ 	   "order by b.DDA desc" +  System.getProperty("line.separator");
//		
//		DatabaseConnection.runSQLServerQuery(query);
//		
//					
//	}
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		// All tests should start at the same starting point.
		Navigation.navigate_DDASummary();
//		
//		if (!BaseUI.pageSourceContainsString("<span class=\"portlet-text\">DDA Summary</span>")) {
//			Navigation.navigate_DDASummary();
//		}
//		DDA_Summary.PaymentCounts.check_ALL_legendBoxes();
//		DDA_Summary.PaymentTotals.check_ALL_legendBoxes();
//		
//		String todaysDate = DDA_Summary.depositDate.getTodaysDate_AsString();
//		DDA_Summary.depositDate.enter_Date(todaysDate);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
