package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class RAMM_Login_ChangePasswordAtNextLogin_AndVerifyCancelButton extends BaseTest {
	
	private String newEntity = "entity1Login";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityLogin Description";
	private String entityExternalID = "entityLogin ID 82";
	String columnName = "Login Name";
	String columnText = "testlogin";
	String loginName = columnText;
	String firstName = "test";
	String lastName = "login";
	String userPassword = "Wausau#1";
	String email = "test@login.com";
	String oldPassword = userPassword;
	String newPassword = "Manage#1";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		if (BaseUI.pageSourceContainsString(newEntity)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		Thread.sleep(1000);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		Thread.sleep(2000);
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();

	}

	@Test(priority = 1)
	public void PT6563_LoginChangePasswordAtNextLogin() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(newEntity, loginName, userPassword);
		LoginPage.changePassword(oldPassword, newPassword);
		LoginPage.clickChangePasswordSaveButton();
		Thread.sleep(5000);
		LoginPage.verifyLoginPageChangePasswordConfirmMessage();
		Thread.sleep(2000);
	}

	@Test
	public void PT6461_LoginChangePasswordScreenCancelButton() throws Exception {
		LoginPage.login(newEntity, loginName, userPassword);
		LoginPage.clickChangePasswordCancelButton();
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		// Remove entity if it exists.
		if (BaseUI.pageSourceContainsString(newEntity)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
