package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_UserPermissions_ViewAdmin_AndSecurityAdmin_Menus_Enabled extends BaseTest {

	String nodeText = "mrc";
	String userName = "AutomationTest";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions,
				GlobalVariables.passsword_allPermissions);

		Navigation.navigate_openMenu("Admin");
	}

	@Test
	public void PT6261_Raaam_SecurityAdmin_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}

	@Test
	public void PT6261_Raaam_SecurityAdmin_Entities_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Entities"));
	}

	@Test
	public void PT6261_Raaam_SecurityAdmin_Users_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Users"));
	}

	@Test
	public void PT6261_Raaam_SecurityAdmin_Roles_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Roles"));
	}

	@Test
	public void PT6261_Raaam_SecurityAdmin_UserGroups_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "User Groups"));
	}

	@Test
	public void PT6261_Raaam_SecurityAdmin_SubHeader_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubHeader", "Admin", "Security Admin"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
