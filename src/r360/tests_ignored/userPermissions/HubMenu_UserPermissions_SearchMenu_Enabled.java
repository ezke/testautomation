package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HubMenu_UserPermissions_SearchMenu_Enabled extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions,
				GlobalVariables.passsword_allPermissions);

		Navigation.navigate_openMenu("Search");
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3440_SearchMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Search", null));
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4095_SearchMenu_PaymentSearch_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Search", "Payment Search"));
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3230_SearchMenu_AdvancedSearch_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Search", "Advanced Search"));
	}
	
	@Test(groups = {"all_Tests"})
	public void SearchMenu_ManageQueries_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Search", "Manage Queries"));
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}

}
