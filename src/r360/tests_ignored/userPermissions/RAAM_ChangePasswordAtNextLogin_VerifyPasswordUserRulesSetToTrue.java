package r360.tests_ignored.userPermissions;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_ChangePasswordAtNextLogin_VerifyPasswordUserRulesSetToTrue extends BaseTest {
	private String nodeText = "Testing101";
	String columnName = "Login Name";
	String columnText = "TestUserRule";
	String loginName = columnText;
	String userPassword = "Wausau#1";
	String oldPassword = userPassword;
	String usedPassword = "Manage#1";
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		String expectedValue = "180";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigPasswordReuse", expectedValue);
		SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToTrue();
		SecurityAdministration.navigate_toUserTab();
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.checkChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();
	}
	@Test
	public void PT6571_ChangePasswordVerifyPasswordCanContainLoginNameSetToTrue() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "testuserrule";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Login name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6573_ChangePasswordVerifyPasswordCanContainReverseLoginNameSetToTrue() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "elurresutset";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Login name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6575_ChangePasswordVerifyPasswordCanContainFirstNameSetToTrue() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Tom";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "First name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6577_ChangePasswordVerifyPasswordCanContainLastNameSetToTrue() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Cruise";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "LastName name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6569_ChangePasswordVerifyDaysBeforePasswordReUseIsEnforced() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		//need to change password after 180days
		String valueToEnter = usedPassword;
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Passwords cannot be reused in less than 180 days.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		LoginPage.clickChangePasswordCancelButton();
		
	}
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		SecurityAdministration.selectNode(nodeText);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.setPasswordConfigurationUsersNameRulesToFalse();
		SecurityAdministration.navigate_toUserTab();
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.unCheckChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
