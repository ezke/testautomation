package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class RAMM_Login_ChangePasswordAtNextLogin_ErrorAndFocusVerification extends BaseTest {

	Pagination userManagementGrid;
	private String nodeText = "Testing101";
	String columnName = "Login Name";
	String columnText = "changepassword";
	String loginName = columnText;
	String userPassword = "Wausau#1";
	String oldPassword = userPassword;
	String newPassword = "Windows#1";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		Thread.sleep(2000);
		SecurityAdministration.selectNode(nodeText);
		userManagementGrid = new Pagination("userGrid");
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		if (Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()) {
			SecurityAdminAddUser.checkChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();
	}

	@Test
	public void PT6562_Login_ChangePasswordAtNextLogin_IncorrectOldPassword() throws Exception {
		LoginPage.login(nodeText, loginName, userPassword);
		String oldPassword = "wausau1";
		LoginPage.changePassword(oldPassword, newPassword);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();

	}

	@Test
	public void PT6566_LoginChangePasswordAtNextLogin_PasswordDoNotMatch() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		String confirmNewPassword = "Desktop#1";
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), newPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), confirmNewPassword);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();

	}

	@Test
	public void PT6561_LoginChangePasswordAtNextLogin_CursorFocusOnOldPassword() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("loginPageOldPasswordText"));

	}

	@Test
	public void PT6567_LoginChangePasswordAtNextLogin_PasswordsNotFilledIn() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		LoginPage.clickChangePasswordCancelButton();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		SecurityAdministration.selectNode(nodeText);
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		if (Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()) {
			SecurityAdminAddUser.unCheckChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
