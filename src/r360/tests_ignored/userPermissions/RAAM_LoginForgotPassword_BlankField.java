package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_LoginForgotPassword_BlankField extends BaseTest {
	String entity = "Testing101";
	String loginName = "Autotest";
	String emailSubjectLine = "Reset Password";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		
	}
	
	@Test
	public void PT6572_LoginForgotPasswordForm_ForgotPasswordHyperlinkDisplays() throws Exception{
		if (Browser.currentBrowser.equals("internetexplorer")) {
			Navigation.navigate_Past_CertificateError();
			BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPasswordLink"));
		}else{
			BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPasswordLink"));
		}
		LoginPage.clickForgotPasswordLink();
		LoginPage.verifyForgotPasswordModalAppears();
	}
	
	@Test
	public void PT6470_LoginForgotPasswordFormEntityNameBlank() throws Exception{
		LoginPage.clickForgotPasswordLink();
		LoginPage.enterForgotPasswordEntity("");
		LoginPage.enterForgotPasswordLoginName(loginName);
		LoginPage.enterForgotPasswordEmailSubjectLine(emailSubjectLine);
		LoginPage.clickForgotPasswordSubmitButton();
		String expectedValue = "All fields must be filled in";
		LoginPage.verifyForgotPasswordErrorMesage(expectedValue);
	}
	
	@Test
	public void PT6474_LoginForgotPasswordLoginNameBlank() throws Exception{
		LoginPage.clickForgotPasswordLink();
		LoginPage.enterForgotPasswordEntity(entity);
		LoginPage.enterForgotPasswordLoginName("");
		LoginPage.enterForgotPasswordEmailSubjectLine(emailSubjectLine);
		LoginPage.clickForgotPasswordSubmitButton();
		String expectedValue = "All fields must be filled in";
		LoginPage.verifyForgotPasswordErrorMesage(expectedValue);
	}
	
	@Test
	public void PT6479_LoginForgotPasswordSubjectLineBlank() throws Exception{
		LoginPage.clickForgotPasswordLink();
		LoginPage.enterForgotPasswordEntity(entity);
		LoginPage.enterForgotPasswordLoginName(loginName);
		LoginPage.enterForgotPasswordEmailSubjectLine("");
		LoginPage.clickForgotPasswordSubmitButton();
		String expectedValue = "All fields must be filled in";
		LoginPage.verifyForgotPasswordErrorMesage(expectedValue);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		LoginPage.clickForgotPasswordCancelButton();
		Thread.sleep(2000);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
}
