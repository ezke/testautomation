package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_UserPermissions_ViewEntityPage_Enabled extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions, GlobalVariables.passsword_allPermissions);
		//Navigation.navigate_Admin_Users();
		//All tests should start at the same starting point.
		Navigation.navigate_DDASummary();

	}
	
	@Test
	public void PT6260_Raaam_SecurityAdmin_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_Listed() throws Throwable {
		Navigation.navigate_openMenu("Admin");
		BaseUI.verify_true_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "Entities"), 
				"Admin>Entities menu item exists", "Could not find Admin>Entities menu item.");
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Entities"));
	}

	
	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_goesTo_AdminEntitiesPage_UserManagmentTitle_Displayed() throws Throwable {
		Navigation.navigate_Admin_Entities();
		BaseUI.verifyElementAppearsByString("securityEntities_EntityManagementTitle");
	}

	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_goesTo_AdminEntitiesPage_EntityTreeView_Displayed() throws Throwable {
		Navigation.navigate_Admin_Entities();
		BaseUI.verifyElementAppearsByString("entityTree");
	}
	
	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_goesTo_AdminEntitiesPage_EntityBody_Displayed() throws Throwable {
		Navigation.navigate_Admin_Entities();
		BaseUI.verifyElementAppearsByString("securityEntities_EntityTabBody");
	}
	
	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_goesTo_AdminEntitiesPage_AddEntityButton_Displayed() throws Throwable {
		Navigation.navigate_Admin_Entities();
		BaseUI.verifyElementAppearsByString("securityEntities_AddButton");
	}
	
	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_goesTo_AdminEntitiesPage_DeleteEntityButton_Displayed() throws Throwable {
		Navigation.navigate_Admin_Entities();
		BaseUI.verifyElementAppearsByString("securityEntities_DeleteButton");
	}
	
	@Test
	public void PT6260_Raaam_SecurityAdmin_EntitiesOption_goesTo_AdminEntitiesPage_EditEntityButton_Displayed() throws Throwable {
		Navigation.navigate_Admin_Entities();
		BaseUI.verifyElementAppearsByString("securityEntities_EditButton");
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		// All tests should start at the same starting point.
		Navigation.navigate_DDASummary();

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
