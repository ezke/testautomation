package r360.tests_ignored.userPermissions;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_ChangePasswordAtNextLogin_VerifyPasswordUserRulesSetToFalse extends BaseTest {
	private String nodeText = "Testing101";
	String columnName = "Login Name";
	String columnText = "TestUserRule";
	String loginName = columnText;
	String userPassword = "Wausau#1";
	String oldPassword = userPassword;
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verifyPasswordConfigurationUsersNameRulesSetToFalse();
		SecurityAdministration.navigate_toUserTab();
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.checkChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();
	}
	@Test
	public void PT6570_ChangePasswordVerifyPasswordCanContainLoginNameSetToFalseWithLoginName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "testuserrule";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Login name is not allowed in password.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6570_ChangePasswordVerifyPasswordCanContainLoginNameSetToFalseWithoutLoginName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Login name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6572_ChangePasswordVerifyPasswordCanContainReverseLoginNameSetToFalseWithReverseLoginName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "elurresutset";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Reversed login name is not allowed in password.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6572_ChangePasswordVerifyPasswordCanContainReverseLoginNameSetToFalseWithoutReverseLoginName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Reversed login name is not allowed in password";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6574_ChangePasswordVerifyPasswordCanContainFirstNameSetToFalseWithFirstName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Tom";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "First name is not allowed in password.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6574_ChangePasswordVerifyPasswordCanContainFirstNameSetToFalseWithoutFirstName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "First name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6576_ChangePasswordVerifyPasswordCanContainLastNameSetToFalseWithLastName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Cruise";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Last name is not allowed in password.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6576_ChangePasswordVerifyPasswordCanContainLastNameSetToFalseWithoutLastName() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Last name is not allowed in password.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		LoginPage.clickChangePasswordCancelButton();
		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		SecurityAdministration.selectNode(nodeText);
		SecurityAdmin_UserManageModal.click_EditButton_ByColumnName_andColumnText(columnName, columnText);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.unCheckChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
