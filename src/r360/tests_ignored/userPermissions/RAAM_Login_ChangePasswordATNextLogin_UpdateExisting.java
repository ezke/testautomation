package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class RAAM_Login_ChangePasswordATNextLogin_UpdateExisting extends BaseTest {
	Pagination userManagementGrid;
	private String newEntity = "entityUpdateExistingPassword";
	private String entityType = "Financial Institution";
	private String entityDescription = "entityupdateexistingpassword Description";
	private String entityExternalID = "entityLogin ID 95";
	String loginName = "testUpdateExistingloginpassword";
	String firstName = "test";
	String lastName = "login";
	String userPassword = "Wausau#1";
	String email = "test@login.com";
	String updatePassword = "Manage#1";
	String changePassword = "Windows#1";
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		if(BaseUI.pageSourceContainsString(newEntity)){
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
		Thread.sleep(1000);
		
		SecurityAdministration.navigate_toUserTab();
		SecurityAdminAddUser.clickAddUserButton();	
		SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
		Thread.sleep(2000);
		
		SecurityAdministration.launchEditUserModal_forGivenUser(loginName);
		SecurityAdminAddUser.updatePassword(updatePassword);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.checkChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();
	}
	
	
	
	@Test
	public void PT6564_ChangePasswordAtNextLogin_LoginPage_UpdateExisting() throws Exception{
		LoginPage.login(newEntity, loginName, updatePassword);
		LoginPage.changePassword(updatePassword, changePassword);
		LoginPage.clickChangePasswordSaveButton();
		Thread.sleep(5000);
		LoginPage.verifyLoginPageChangePasswordConfirmMessage();
		Thread.sleep(2000);
		LoginPage.clearLoginFields();
		LoginPage.login(newEntity, loginName, changePassword);
		HomePage.verifyHeaderTitleAppears();
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
			// Remove entity if it exists.
		if(BaseUI.pageSourceContainsString(newEntity)){
			SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(newEntity);
		}
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
