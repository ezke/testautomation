package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_UserPermissions_ViewRolesPage_Disabled extends BaseTest {
	String nodeText = "mrc";
	String userName = "AutomationTest";
	

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_viewRolesPageDisabled, GlobalVariables.userName_viewRolesPageDisabled, 
				GlobalVariables.password_viewRolesPageDisabled);
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
		Navigation.navigate_openMenu("Admin");
	
	}
	
	@Test
	public void PT8553_Raaam_SecurityAdmin_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Entities"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Users"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "User Groups"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "System Settings"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Branding"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Banks"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Payment Sources"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Workgroups"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "User Preferences"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Extract Config Manager"));
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Alert Manager"));
	}
	
	@Test
	public void PT8553_Raaam_SecurityAdmin_AdminMenu_NO_RoleOption_Listed() throws Throwable {
		BaseUI.verify_false_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "Roles"), 
				"As expected, Admin>Roles menu item does not exist.",
				"Admin>Roles menu item exists");
	}

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		// All tests should start at the same starting point.

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
