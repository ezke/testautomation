package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_UserPermissions_ViewRolesPage_Enabled extends BaseTest {

	String nodeText = "mrc";
	String userName = "AutomationTest";
	

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions, GlobalVariables.passsword_allPermissions);


	}
	
	@Test
	public void PT8554_Raaam_SecurityAdmin_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	@Test
	public void PT8554_Raaam_SecurityAdmin_RolesOption_Listed() throws Throwable {
		Navigation.navigate_openMenu("Admin");
		BaseUI.verify_true_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "Roles"), 
				"Admin>Roles menu item exists", "Could not find Admin>Roles menu item.");
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuSubItem", "Admin", "Roles"));
	}

	@Test
	public void PT8554_Raaam_SecurityAdmin_RolesOption_TakesYouTo_SecurityAdmin_RolesPage() throws Throwable {
		Navigation.navigate_Admin_Roles();
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Roles_Header"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		// All tests should start at the same starting point.
		Navigation.navigate_DDASummary();

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
