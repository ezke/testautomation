package r360.tests_ignored.userPermissions;



import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class RAAM_ChangePassword_VerifyPasswordCharacterRulesSettingEnforced extends BaseTest {
	Pagination userManagementGrid;
	private String nodeText = "Testing101";
	String columnName = "Login Name";
	String columnText = "changepassword";
	String loginName = columnText;
	String userPassword = "Wausau#1";
	String oldPassword = userPassword;
	String newPassword = "Windows#1";
	
	String minLength = "8"; String maxRepeatingChars = "3"; String minNumChars = "1";
	String minSpecialChars = "1"; String minUppercaseChars = "1"; String minLowercaseChars = "1";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Entities();
		Thread.sleep(2000);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(nodeText);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verifyPasswordConfigurationCharacterRulesSettingValue(minLength, maxRepeatingChars, minNumChars,
				minSpecialChars, minUppercaseChars, minLowercaseChars);
		SecurityAdministration.navigate_toUserTab();
		userManagementGrid = new Pagination ("userGrid");
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.checkChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		HomePage.clickSignOutLink();
		HomePage.clickReturnToApplicationLink();	
	}
	
	@Test
	public void PT6579_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedLessThanDefinedLength() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Password must have minimum length of 8 characters.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6579_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedEqualToDefinedMinLength() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausaufs";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Password must have minimum length of 8 characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6579_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedMoreThanDefinedLength() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausaufsdd";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Password must have minimum length of 8 characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6583_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedLessThanDefinedMinUppercase() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();
		String expectedError= "Password must have minimum of 1 uppercase letters.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	
	@Test
	public void PT6583_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedEqualToDefinedMinUppercase() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 uppercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6583_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedMoreThanDefinedMinUppercase() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "WAusau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 uppercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6580_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedLessThanDefinedMinLowercase() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "WAU";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 lowercase letters.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	
	@Test
	public void PT6580_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedEqualToDefinedMinLowercase() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wa";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 lowercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6580_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedMoreThanDefinedMinLowercase() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 lowercase letters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6581_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedLessThanDefinedMinNum() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 digits.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6581_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedEqualToDefinedMinNum() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wau1";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 digits.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6581_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedMoreThanDefinedMinNum() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wau12";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 digits.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6582_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedLessThanDefinedMinSpecialChar() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wausau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 special (non-alphanumeric) characters.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	
	@Test
	public void PT6582_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedEqualToDefinedMinSpecialChar() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wau#";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 special (non-alphanumeric) characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@Test
	public void PT6582_ChangePasswordAtNextLoginVerifyPasswordMinLengthSettingEnforcedMoreThanDefinedMinSpecialChar() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "Wau#@";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password must have minimum of 1 special (non-alphanumeric) characters.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6578_ChangePasswordAtNextLoginVerifyPasswordMinRepeatingCharSettingEnforcedMoreThanDefinedMinRepeatingChar() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wwwwau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password cannot have more than 3 characters in a row.";
		WebElement element = Locator.lookupElement("loginPageChangePasswordErrorMessageByText", expectedError, null);
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("changePasswordError", expectedError, errorMessage);
	}
	@Test
	public void PT6578_ChangePasswordAtNextLoginVerifyPasswordMinRepeatingCharSettingEnforcedEqualToDefinedMinRepeatingChar() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wwwau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password cannot have more than 3 characters in a row.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	@Test
	public void PT6578_ChangePasswordAtNextLoginVerifyPasswordMinRepeatingCharSettingEnforcedLessThanDefinedMinRepeatingChar() throws Exception{
		LoginPage.clearLoginFields();
		LoginPage.login(nodeText, loginName, userPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		String valueToEnter = "wwau";
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), valueToEnter);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), valueToEnter);
		LoginPage.clickChangePasswordSaveButton();
		LoginPage.verifyLoginPageChangePasswordErrorMessage();	
		String expectedError= "Password cannot have more than 3 characters in a row.";
		BaseUI.verifyElementDoesNotHaveExpectedText("loginPageChangePasswordErrorMessage", expectedError);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		LoginPage.clickChangePasswordCancelButton();
		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		LoginPage.clearLoginFields();
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
		SecurityAdministration.selectNode(nodeText);
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		if(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox").isDisplayed()){
			SecurityAdminAddUser.unCheckChangePasswordAtnextLogin();
		}
		SecurityAdminAddUser.clickSaveButton();
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
