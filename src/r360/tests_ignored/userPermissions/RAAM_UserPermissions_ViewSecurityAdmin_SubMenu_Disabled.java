package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_UserPermissions_ViewSecurityAdmin_SubMenu_Disabled extends BaseTest {
	String nodeText = "mrc";
	String userName = "AutomationTest";
	

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_viewSubAdminPagesDisabled, GlobalVariables.userName_viewSubAdminPagesDisabled, 
				GlobalVariables.password_viewSubAdminPagesDisabled);
		
		Navigation.navigate_openMenu("Admin");
	}
	

	@Test
	public void PT6262_Raaam_AdminMenu_Appears() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	
	@Test
	public void PT6262_Raaam_AdminMenu_SecurityAdmin_DoesNotExist() throws Throwable {
		BaseUI.verifyElementDoesNOTExist("Navigate_menuSubHeader", "Admin", "Security Admin");
	}
	
	@Test
	public void PT6262_Raaam_AdminMenu_Entities_DoesNotExist() throws Throwable {
		BaseUI.verifyElementDoesNOTExist("Navigate_menuSubItem", "Admin", "Entities");
	}
	
	@Test
	public void PT6262_Raaam_AdminMenu_Users_DoesNotExist() throws Throwable {
		BaseUI.verifyElementDoesNOTExist("Navigate_menuSubItem", "Admin", "Users");
	}
	
	@Test
	public void PT6262_Raaam_AdminMenu_Roles_DoesNotExist() throws Throwable {
		BaseUI.verifyElementDoesNOTExist("Navigate_menuSubItem", "Admin", "Roles");
	}
	
	@Test
	public void PT6262_Raaam_AdminMenu_UserGroups_DoesNotExist() throws Throwable {
		BaseUI.verifyElementDoesNOTExist("Navigate_menuSubItem", "Admin", "User Groups");
	}
	
//	@Test
//	public void PT8551_Raaam_SecurityAdmin_AdminMenu_NO_GroupOption_Listed() throws Throwable {
//		BaseUI.verify_false_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "User Groups"), 
//				"As expected, Admin>User Groups menu item does not exist.",
//				"Admin>User Groups menu item exists");
//	}

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
