package r360.tests_ignored.userPermissions;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;

public class RAAM_Login_VerifySystemUserUnableToLogIntoFramework extends BaseTest {
	private String entity = "WFS";
	String systemUserName = "DITTester";
	String userPassword = "Wausau#1";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
	}
	
	@Test
	public void PT6524_LoginPage_VerifySystemUserIsUnableToLogIntoFramework() throws Exception{
		LoginPage.login(entity, systemUserName, userPassword);
		LoginPage.verifyLoginPageError();
		
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
	
			Browser.closeBrowser();
	}
}
