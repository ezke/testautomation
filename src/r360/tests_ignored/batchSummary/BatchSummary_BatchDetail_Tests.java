package r360.tests_ignored.batchSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class BatchSummary_BatchDetail_Tests extends BaseTest {

	TableData results;

	String workgroup = "2234322 - Automation-Filter";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;
	private Integer rowNumber = 1;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.click_Search();

		results = BatchSummary.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");
		BatchSummary.click_Cell_BatchSummary("Batch ID", rowNumber);
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_Transaction_Appears() throws Exception {
		String headerText = "Transaction";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_PaymentAmount_Appears() throws Exception {
		String headerText = "Payment Amount";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_RT_Appears() throws Exception {
		String headerText = "R/T";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_AccountNumber_Appears() throws Exception {
		String headerText = "Account Number";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_CheckTraceRefNumber_Appears() throws Exception {
		String headerText = "Check/Trace/Ref Number";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_Payer_Appears() throws Exception {
		String headerText = "Payer";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_BatchDetail_Header_DDA_Appears() throws Exception {
		String headerText = "DDA";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_TableHeaders_ByText", headerText, null));
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummary_Workgroup_Match_BatchDetail_Workgroup() throws Exception {
		String batchDetailWorkgroup = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Workgroup:", null));
		BatchSummary.click_BatchSummary_Breadcrumb();
		String workgroupSelectorValue = BatchSummary.workGroupSelector.return_SelectedText().replace("Selected: ", "");
		BaseUI.baseStringCompare("Workgroup", batchDetailWorkgroup, workgroupSelectorValue);
		BatchSummary.click_Cell_BatchSummary("Batch ID", 1);
	}

	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummaryResult_DepositDate_Match_BatchDetail_HeaderDepositDate() throws Exception {
		String batchDetail_DepositDate = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Deposit Date:", null));
		BaseUI.baseStringCompare("Deposit Date", results.data.get(rowNumber - 1).get("Deposit Date"),
				batchDetail_DepositDate);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummaryResult_BatchNumber_Match_BatchDetail_HeaderBatchNumber() throws Exception {
		String batchDetail_BatchNumber = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Batch:", null));
		BaseUI.baseStringCompare("Batch", results.data.get(rowNumber - 1).get("Batch"),
				batchDetail_BatchNumber);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3852_BatchSummaryResult_BatchID_Match_BatchDetail_HeaderBatchID() throws Exception {
		String batchDetail_BatchID = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Batch ID:", null));
		BaseUI.baseStringCompare("Batch", results.data.get(rowNumber - 1).get("Batch ID"),
				batchDetail_BatchID);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
