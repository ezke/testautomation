package r360.tests_ignored.batchSummary;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class BatchSummary_Breadcrumbs_Tests extends BaseTest {

	private String workgroup = "2234322 - Automation-Filter";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.click_Search();

	}

	@Test(groups = { "all_Tests" })
	public void PT3949_BatchSummary_Breadcrumbs_BatchDetail() throws Throwable {
		String[] crumbOrder = { "Batch Summary", "Batch Detail" };
		BatchSummary.click_Cell_BatchSummary("Batch", 1);
		BatchSummary.verify_Breadcrumb(crumbOrder);
	}

	@Test(groups = { "all_Tests" })
	public void PT3951_BatchSummary_Breadcrumbs_TransactionDetail_Click_BatchDetailBreadcrumb() throws Throwable {
		String[] crumbOrder = { "Batch Summary", "Batch Detail", "Transaction Detail" };

		BatchSummary.click_Cell_BatchSummary("Batch", 1);
		BatchSummary.click_BatchDetailsCell_ByHeaderText_AndRowNumber("Transaction", 1);
		BatchSummary.verify_Breadcrumb(crumbOrder);
		BatchSummary.click_BatchDetail_Breadcrumb();
	}

	@Test(groups = { "all_Tests" })
	public void PT3952_BatchSummary_Breadcrumbs_TransactionDetail_Click_BatchSummaryBreadcrumb() throws Throwable {
		String[] crumbOrder = { "Batch Summary", "Batch Detail", "Transaction Detail" };

		BatchSummary.click_Cell_BatchSummary("Batch", 1);
		BatchSummary.click_BatchDetailsCell_ByHeaderText_AndRowNumber("Transaction", 1);
		BatchSummary.verify_Breadcrumb(crumbOrder);
	}

	@Test(groups = { "all_Tests" })
	public void PT3950_BatchSummary_BatchDetailLink_ExpectedTitle() throws Exception {
		WebElement firstBatchSummaryLink = BatchSummary.return_Link_ByColumnName_AndColumnRow_ForToolTip("Batch ID", 1);
		BaseUI.verifyElementHasExpectedAttributeValue(firstBatchSummaryLink, "title", "Click here for Batch Detail");
		BatchSummary.click_Cell_BatchSummary("Batch ID", 1);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		BatchSummary.click_BatchSummary_Breadcrumb();
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
