package r360.tests_ignored.batchSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class BatchSummary_TransactionDetail_Tests extends BaseTest {
	TableData results;
	TableData paymentItemResultTransaction1;
	TableData paymentItemResultTransaction2;
	TableData relatedItemsResultTable1;
	TableData relatedItemsResultTable2;
	

	String workgroup = "2234322 - Automation-Filter";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;
	private Integer rowNumber = 1;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.click_Search();

		results = BatchSummary.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");
		BatchSummary.click_Cell_BatchSummary("Batch", rowNumber);
		BatchSummary.click_BatchDetailsCell_ByHeaderText_AndRowNumber("Transaction", rowNumber);
	}
	
	@Test(priority = 10, groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_PaymentSequence_Appears() throws Exception {
		String headerText = "Payment Sequence";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}
	
	@Test(priority = 10, groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_RT_Appears() throws Exception {
		String headerText = "R/T";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}
	
	@Test(priority = 10,groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_AccountNumber_Appears() throws Exception {
		String headerText = "Account Number";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}

	@Test(priority = 10,groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_CheckTraceRefNumber_Appears() throws Exception {
		String headerText = "Check/Trace/Ref Number";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}
	
	@Test(priority = 10,groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_Payer_Appears() throws Exception {
		String headerText = "Payer";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}
	
	@Test(priority = 10,groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_DDA_Appears() throws Exception {
		String headerText = "DDA";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}
	
	@Test(priority = 10,groups = { "all_Tests" })
	public void PT3852_BatchSummary_TransactionDetail_PaymentItems_TableHeader_PaymentAmount_Appears() throws Exception {
		String headerText = "Payment Amount";
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_TransactionDetail_PaymentItems_TableHeaders_ByText", headerText, null));
	}
	
	@Test(priority = 20,groups = { "all_Tests" })
	public void PT3963_BatchSummary_TransactionDetail_PaymentItems_NavigateTo_NextTransaction() throws Exception {
		paymentItemResultTransaction1 = BatchSummary.return_TransactionDetailPaymentItemsResults();
		Integer number = 2;
		BatchSummary.enterTransactionDetail_TransactionNumber_And_ClickGo(number);
		paymentItemResultTransaction2 = BatchSummary.return_TransactionDetailPaymentItemsResults();
		BaseUI.verify_true_AndLog(paymentItemResultTransaction1!= paymentItemResultTransaction2, 
				"Transaction Detail PaymentItems Result Do Not Match", "Transaction Detail PaymentItems Result Match");
		
	}
	
	@Test(priority = 30, groups = { "all_Tests" })
	public void PT3963_BatchSummary_TransactionDetail_RelatedItems_NavigateTo_NextTransaction() throws Exception {
		BatchSummary.click_BatchSummary_Breadcrumb();
		BatchSummary.click_Cell_BatchSummary("Batch", rowNumber);
		BatchSummary.click_BatchDetailsCell_ByHeaderText_AndRowNumber("Transaction", rowNumber);
		
		relatedItemsResultTable1 = BatchSummary.return_TransactionDetailRelatedItemResults();
		Integer number = 2;
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_TransactionDetail_Transaction_TextBox"));
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_TransactionDetail_GoButton"));
		BatchSummary.enterTransactionDetail_TransactionNumber_And_ClickGo(number);
		relatedItemsResultTable2 = BatchSummary.return_TransactionDetailRelatedItemResults();
		BaseUI.verify_true_AndLog(relatedItemsResultTable1!= relatedItemsResultTable2, 
				"Transaction Detail RelatedItems Result Do Not Match", "Transaction Detail RelatedItems Result Match");
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
	
	
}
