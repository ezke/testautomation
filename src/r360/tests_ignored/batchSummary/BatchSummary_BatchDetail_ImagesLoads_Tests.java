package r360.tests_ignored.batchSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class BatchSummary_BatchDetail_ImagesLoads_Tests extends BaseTest {
	TableData results;
	private String mainWindowHandle;

	String workgroup = "33338888 - Automation-ACH-CTX";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.click_Search();

		results = BatchSummary.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");

		BatchSummary.click_Cell_BatchSummary("Batch ID", 1);
		mainWindowHandle = Browser.driver.getWindowHandle();

	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT2093_BatchSummary_BatchDetail_ViewPaymentImage_Loads() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("batchSum_BatchDetail_ViewPaymentImage_ByIndex", "1", null),
				false, 15000);
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_BatchDetail_PDFContainer"));
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT2094_BatchSummary_BatchDetail_ViewDocumentImage_Loads() throws Exception {
		BaseUI.ClickAndSwitchWindow(
				Locator.lookupElement("batchSum_BatchDetail_ViewTransactionImage_ByIndex", "1", null), false, 15000);
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_BatchDetail_PDFContainer"));
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT2092_BatchSummary_BatchDetail_ViewAllImageTransaction_Loads() throws Exception {
		BaseUI.ClickAndSwitchWindow(
				Locator.lookupElement("batchSum_BatchDetail_ViewAllImageTransaction_ByIndex", "1", null), false, 15000);
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_BatchDetail_PDFContainer"));
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT2095_BatchSummary_BatchDetail_ViewAllImageBatch_Loads() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("batchSum_BatchDetail_ViewAllImageBatch", null, null), false,
				15000);
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_BatchDetail_PDFContainer"));
	}

	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT2091_BatchSummary_BatchDetail_ClickPrint_Loads() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("batchSum_PrintButton", null, null), false, 15000);
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_BatchDetail_PDFContainer"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Browser.driver.switchTo().window(mainWindowHandle);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
