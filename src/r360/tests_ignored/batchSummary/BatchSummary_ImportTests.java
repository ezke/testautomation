package r360.tests_ignored.batchSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class BatchSummary_ImportTests extends BaseTest {

	TableData results;
	TableData relatedItemResults;
	TableData paymentItemResults;
	private String workgroup = "33338888 - Automation-ACH-CTX";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);

	}

	@Test(groups = { "all_Tests" })
	public void PT2088_BatchSummary_ResultPage_ACH_CTX_ImportsResults() throws Exception {
		BatchSummary.click_Search();

		results = BatchSummary.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");
	}

	@Test(groups = { "all_Tests" })
	public void PT2097_BatchSummary_NavigateTo_BatchDetail() throws Throwable {
		Integer rowNumber = 1;
		BatchSummary.click_Search();
		BatchSummary.verify_ClickingBatchSummary_TakesYouToBatchDetails_Page(rowNumber);
		BatchSummary.click_BatchSummary_Breadcrumb();

	}

	// contains TC249817 as well
	@Test(groups = { "all_Tests" })
	public void PT2090_BatchSummary_BatchDetail_NavigateTo_TransactionDetail() throws Throwable {
		Integer batchRowNumber = 1;
		Integer transactionRowNumber = 1;
		BatchSummary.click_Search();
		BatchSummary.verify_ClickingBatchDetailTransacation_TakesYouToTransactionDetails_Page(batchRowNumber,
				transactionRowNumber);
		BatchSummary.click_BatchSummary_Breadcrumb();

	}
	
	@Test(groups = { "all_Tests" })
	public void PT4305_BatchSummary_BatchDetail_NavigateTo_TransactionDetail_PaymentItems_Data_Loads() throws Throwable {
		Integer batchRowNumber = 1;
		Integer transactionRowNumber = 1;
		BatchSummary.click_Search();
		BatchSummary.verify_ClickingBatchDetailTransacation_TakesYouToTransactionDetails_Page(batchRowNumber,
				transactionRowNumber);
		paymentItemResults = BatchSummary.return_TransactionDetailPaymentItemsResults();
		BaseUI.verify_true_AndLog(paymentItemResults.data.size() > 0, "Found Transaction Detail Payment Items Results.",
				"Transaction Detail Payment Items Results were empty.");
		BatchSummary.click_BatchSummary_Breadcrumb();

	}
	
	@Test(groups = { "all_Tests" })
	public void PT4308_BatchSummary_BatchDetail_NavigateTo_TransactionDetail_RelatedItems_Data_Loads() throws Throwable {
		Integer batchRowNumber = 1;
		Integer transactionRowNumber = 1;
		BatchSummary.click_Search();
		BatchSummary.verify_ClickingBatchDetailTransacation_TakesYouToTransactionDetails_Page(batchRowNumber,
				transactionRowNumber);
		relatedItemResults = BatchSummary.return_TransactionDetailRelatedItemResults();
		BaseUI.verify_true_AndLog(relatedItemResults.data.size() > 0, "Found Transaction Detail Related Items Results.",
				"Transaction Detail Related Items Results were empty.");
		BatchSummary.click_BatchSummary_Breadcrumb();

	}
	
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		BatchSummary.click_Search();

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
