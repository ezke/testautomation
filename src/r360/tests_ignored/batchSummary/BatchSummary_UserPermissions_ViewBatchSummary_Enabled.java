package r360.tests_ignored.batchSummary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BatchSummary_UserPermissions_ViewBatchSummary_Enabled extends BaseTest {

	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions,
				GlobalVariables.passsword_allPermissions);

	}

	@Test(groups = { "all_tests" })
	public void PT3916_BatchSummary_Exist() throws Throwable {
		BaseUI.verifyElementExists("Navigate_toPage_ByNavText", "Batch Summary", null);
	}

	@Test(groups = { "all_tests" })
	public void PT3906_BatchSummary_MustSelect_Workgroup() throws Throwable {
		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.open_List();
		Actions action = new Actions(Browser.driver);

		WebElement element = Locator.findElement(
				By.xpath("//ul[contains(@class,'jqx-tree-dropdown')]/li/div[./text()='AutomationPermission']"));
		action.doubleClick(element).perform();
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		Navigation.close_ToastError_If_ItAppears();
		BatchSummary.workGroupSelector.close_List();
		

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
