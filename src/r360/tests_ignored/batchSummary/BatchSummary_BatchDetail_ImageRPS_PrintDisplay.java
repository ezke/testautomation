package r360.tests_ignored.batchSummary;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class BatchSummary_BatchDetail_ImageRPS_PrintDisplay extends BaseTest {
	TableData results;

	String workgroup = "2234322 - Automation-Filter";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;
	private String paymentSourceValue = "AutomationImageRPS";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.setPaymentSourceDropdown(paymentSourceValue);
		BatchSummary.click_Search();

		results = BatchSummary.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");

		BatchSummary.click_Cell_BatchSummary("Batch ID", 1);

	}
	
	@Test(groups = { "all_Tests" })
	public void PT3008_BatchSummary_BatchDetail_ImageRPSBatch_PrintIcon_Appears() throws Exception {
		Integer rowNumber = 1;
		BaseUI.verifyElementAppears(
				Locator.lookupElement("batchSum_BatchDetail_ViewImageRPSAudit_ByIndex", rowNumber.toString(), null));
	}	
	
	@Test(groups = { "all_Tests" })
	public void PT3008_BatchSummary_BatchDetail_ImageRPSBatch_PrintIcon_ExpectedTitle() throws Exception {
		WebElement firstBatchDetailLink = Locator.lookupElement("batchSum_BatchDetail_ViewImageRPSAudit_ByIndex","1", null);
		BaseUI.verifyElementHasExpectedAttributeValue(firstBatchDetailLink, "title", "View ImageRPS Audit");
		BatchSummary.click_BatchSummary_Breadcrumb();
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
