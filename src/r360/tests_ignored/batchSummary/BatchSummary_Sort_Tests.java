package r360.tests_ignored.batchSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.BatchSummary;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class BatchSummary_Sort_Tests extends BaseTest {

	TableData results;

	String workgroup = "33338888 - Automation-ACH-CTX";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_BatchSummary();
		BatchSummary.setDateRange(date1, date2);
		BatchSummary.workGroupSelector.search_forText(workgroup);
		BatchSummary.click_Search();

		results = BatchSummary.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_BatchID_Ascending() throws Exception {
		String headerText = "Batch ID";
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_BatchID_Descending() throws Exception {
		String headerText = "Batch ID";
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_Batch_Ascending() throws Exception {
		String headerText = "Batch";
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_Batch_Descending() throws Exception {
		String headerText = "Batch";
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_DepositDate_Ascending() throws Exception {
		String headerText = "Deposit Date";
		results.remove_Character("/", headerText);
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();
		newTableData.remove_Character("/", headerText);

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_DepositDate_Descending() throws Exception {
		String headerText = "Deposit Date";
		results.remove_Character("/", headerText);
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();
		newTableData.remove_Character("/", headerText);

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_PaymentSource_Ascending() throws Exception {
		String headerText = "Payment Source";
		results.sort_ByColumn_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_PaymentSource_Descending() throws Exception {
		String headerText = "Payment Source";
		results.sort_ByColumn_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_PaymentType_Ascending() throws Exception {
		String headerText = "Payment Type";
		results.sort_ByColumn_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_PaymentType_Descending() throws Exception {
		String headerText = "Payment Type";
		results.sort_ByColumn_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_TransactionCount_Ascending() throws Exception {
		String headerText = "Transaction Count";
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_TransactionCount_Descending() throws Exception {
		String headerText = "Transaction Count";
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_PaymentCount_Ascending() throws Exception {
		String headerText = "Payment Count";
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_PaymentCount_Descending() throws Exception {
		String headerText = "Payment Count";
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_DocumentCount_Ascending() throws Exception {
		String headerText = "Document Count";
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_DocumentCount_Descending() throws Exception {
		String headerText = "Document Count";
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_BatchTotal_Ascending() throws Exception {
		String headerText = "Batch Total";
		results.remove_Character("$", headerText);
		results.remove_Character(",", headerText);
		results.sort_ByColumn_numeric_Ascending(headerText);

		BatchSummary.sort_ByHeader_Ascending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();
		newTableData.remove_Character("$", headerText);
		newTableData.remove_Character(",", headerText);

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT2096_BatchSummary_TestingResults_BatchTotal_Descending() throws Exception {
		String headerText = "Batch Total";
		results.remove_Character("$", headerText);
		results.remove_Character(",", headerText);
		results.sort_ByColumn_numeric_Descending(headerText);

		BatchSummary.sort_ByHeader_Descending(headerText);
		TableData newTableData = BatchSummary.return_SearchResults();
		newTableData.remove_Character("$", headerText);
		newTableData.remove_Character(",", headerText);

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
