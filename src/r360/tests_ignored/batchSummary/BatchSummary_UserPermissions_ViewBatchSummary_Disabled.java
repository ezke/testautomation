package r360.tests_ignored.batchSummary;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class BatchSummary_UserPermissions_ViewBatchSummary_Disabled extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_viewBatchSummaryPageDisabled,
				GlobalVariables.userName_viewBatchSummaryPageDisabled,
				GlobalVariables.password_viewBatchSummaryPageDisabled);

	}

	@Test(groups = { "all_tests" })
	public void PT3915_BatchSummary_DoesNotExist() throws Throwable {
		BaseUI.verifyElementDoesNOTExist("Navigate_menuByText", "Batch Summary", null);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
