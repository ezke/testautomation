package r360.tests_ignored.dashboard;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.Dashboard;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Dashboard_PieCharts extends BaseTest {

	TableData receivableSummariesData;
	TableData databaseData;
	TableData totalAmountLegendData;

	String todaysDateForWebsite = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
	String todaysDateForDatabase = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

		Dashboard.dashboard_depositDate.enter_Date(todaysDateForWebsite);

		databaseData = Dashboard.return_DataBase_Data_For_PieCharts(todaysDateForDatabase);
		totalAmountLegendData = Dashboard.return_totalAmount_PieChartData();
	}

	@Test
	public void TC_Dashboard_TotalAmount_Correct() throws Throwable {
		String totalAmount = Dashboard.TotalAmount.return_PieChart_Title();
		BaseUI.baseStringPartialCompare("Pie Chart Title", "Total Amount", totalAmount);

		totalAmount = totalAmount.substring(totalAmount.indexOf("$"), totalAmount.length());
		totalAmount = totalAmount.replace("$", "").replace(",", "").trim();

		BaseUI.baseStringCompare("Total Amount", databaseData.return_Sum_ofColumn("total").toString(), totalAmount);

	}

	@Test
	public void TC_Dashboard_TotalTransactions_Correct() throws Throwable {
		String totalCount = Dashboard.TotalTransactions.return_PieChart_Title();
		BaseUI.baseStringPartialCompare("Pie Chart Title", "Total Transactions", totalCount);

		totalCount = totalCount.substring(totalCount.indexOf("-") + 1, totalCount.length()).trim();

		BaseUI.baseStringCompare("Total Transactions", databaseData.return_Sum_ofColumn("transaction_count").toString(),
				totalCount);

	}

	@Test
	public void TC_Dashboard_TotalAmount_PaymentTypes_Correct() throws Throwable {
		// ArrayList<String> paymentTypeList =
		// Dashboard.return_PieChartLegendList();

		BaseUI.verify_true_AndLog(totalAmountLegendData.data.size() == databaseData.data.size(),
				"Database Payment Types matched page Payment Types in count.",
				"Database Payment Types did NOT match page Payment Types in count.");

		BaseUI.verify_true_AndLog(totalAmountLegendData.data.size() > 0, "Found payment types.",
				"Did NOT find payment types.");

		for (HashMap<String, String> tableRow : databaseData.data) {
			// BaseUI.verify_true_AndLog(paymentTypeList.contains(tableRow.get("payment_type")),
			// MessageFormat.format("Found {0} in Pie Chart key",
			// tableRow.get("payment_type")),
			// MessageFormat.format("Could not find {0} in Pie Chart key",
			// tableRow.get("payment_type")));

			HashMap<String, String> matchingRow = totalAmountLegendData.return_Row_BasedOn_2MatchingFields(
					"payment_type", tableRow.get("payment_type"), "amount", tableRow.get("total"));
			BaseUI.verify_true_AndLog(matchingRow != null, "Found matching Database data.",
					"Did NOT find matching database data.");
		}

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
