package r360.tests_ignored.dashboard;

import java.text.MessageFormat;
import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.Dashboard;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Dashboard_TableTests_GroupBy_Workgroup extends BaseTest {

	TableData receivableSummariesData;
	TableData databaseData;

	private String date_forWebsite = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date_forDatabase = ImportData.return_TodaysDate_FormattedForDatabase();

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Dashboard();
		Dashboard.receivables_depositDate.enter_Date(date_forWebsite);
		Dashboard.receivables_GroupBy_dropdown.select_EntityValue("Workgroup");
		Thread.sleep(1000);
		// Need to add secondary sorting to match it up against our database.
		Dashboard.sort_ByColumn_Descending("Payment Type");
		receivableSummariesData = Dashboard.extract_ReceivablesSummary_Tables();
		BaseUI.verify_true_AndLog(receivableSummariesData.data.size() > 0,
				"Found table data on Receivables Summary page.",
				"Unable to extract table data from Receivables Summary page.");

	}

	@DataProvider(name = "Workgroup")
	public Object[][] createData_GroupBy_PaymentType() throws Exception {

		databaseData = Dashboard.return_DataBase_Data_For_ReceivablesSummary(date_forDatabase);
		databaseData.sort_ByColumn_Descending("payment_type");
		databaseData.sort_ByColumn_Ascending("workgroup");
		BaseUI.verify_true_AndLog(databaseData.data.size() > 0, "Found database data.",
				"Failed to find database data.");

		Object[][] rowObject = new Object[databaseData.data.size()][2];

		for (int i = 0; i < databaseData.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = databaseData.data.get(i);
		}

		return rowObject;
	}

	@Test(dataProvider = "Workgroup")
	public void TC_Compare_Data_AgainstDataBase_PaymentTypeGrouping(int index, HashMap<String, String> tableRow)
			throws Throwable {

		String grouping = "Workgroup";

		BaseUI.verify_true_AndLog(receivableSummariesData.data.size() == databaseData.data.size(),
				"Website table matches database in size.", "Website does NOT match database table in size.");

		String output = "";
		for (String key : receivableSummariesData.data.get(index).keySet()) {

			String databaseValue = tableRow.get(key);
			String websiteValue = receivableSummariesData.data.get(index).get(key);

			if (!key.equals(grouping) && tableRow.get(key).equals("Entity")) {

			} else if (!key.equals(grouping)
					&& !tableRow.get(key).equals(receivableSummariesData.data.get(index).get(key))) {

				//Doubles might be off due to rounding error.  This logic will check to see if doubles are actually within .02 of each other.
				if (BaseUI.string_IsDouble(databaseValue)) {

					Double databaseValue_AsDouble = Double.parseDouble(databaseValue);
					Double websiteValue_AsDouble = Double.parseDouble(websiteValue);

					Double difference = databaseValue_AsDouble - websiteValue_AsDouble;
					difference = Math.abs(difference);
					if (difference > .02) {
						output += MessageFormat.format("For column {0}, expected value: {1}, but seeing value {2}", key,
								tableRow.get(key), receivableSummariesData.data.get(index).get(key))
								+ System.getProperty("line.separator");
					}

				} else {
					output += MessageFormat.format("For column {0}, expected value: {1}, but seeing value {2}", key,
							tableRow.get(key), receivableSummariesData.data.get(index).get(key))
							+ System.getProperty("line.separator");
				}
			}

			if (key.equals(grouping)
					&& !tableRow.get("workgroup").equals(receivableSummariesData.data.get(index).get(key))) {
				output += MessageFormat.format("For column {0}, expected value: {1}, but seeing value {2}", key,
						tableRow.get("payment_type"), receivableSummariesData.data.get(index).get(key))
						+ System.getProperty("line.separator");
			}

		}

		BaseUI.verify_true_AndLog(output.equals(""), "Table data matched table results on DDA Summary page.", output);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
