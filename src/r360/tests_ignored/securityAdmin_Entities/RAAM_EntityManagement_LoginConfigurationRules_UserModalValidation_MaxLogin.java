package r360.tests_ignored.securityAdmin_Entities;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_LoginConfigurationRules_UserModalValidation_MaxLogin extends BaseTest {

	private String testEntity = "autoTestEntity_LoginsValidation1";
	private String nodeText = "AutomationTest";
	String userName = "testUserAB";
	String userPassword = "Testpword123!";
	String firstName = "testuserFName";
	String lastName = "testuserLName";
	String email = "testuserEmail@imaginaryemail.com";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.expandNode(nodeText);
		
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(testEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(testEntity);
		}
		SecurityAdministration.selectNode(nodeText);
		// create a new entity so we don't accidentally mess something up
		// somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
		SecurityAdmin_EntityManageModal.set_LoginRule("entityLoginConfigMaxLoginAttemptText", "4");
		SecurityAdministration.CreateUser_GivenEntity(testEntity, userName, userPassword, firstName, lastName, email);

		Navigation.signOut();
		Browser.driver.navigate().to(GlobalVariables.baseURL);
		Thread.sleep(300);

	}

	@Test
	public void PT6212_LoginRules_MinimumAlpha_UserManagement_ValidateError() throws Throwable {
		for(Integer i = 0; i < 4; i++)
		{
			//Attempts to login 4 times with wrong password.
			LoginPage.login(testEntity, userName, userPassword + i.toString());
			Thread.sleep(1000);
		}
		
		//Proper login attempt after it's been locked.  To demonstrate that it is locked.
		LoginPage.login(testEntity, userName, userPassword);
		Thread.sleep(100);
		
		WebElement errorMessage = Locator.lookupElement("LoginPage_ErrorMessage");
		BaseUI.verifyElementAppears(errorMessage);
		BaseUI.verifyElementHasExpectedText(errorMessage, 
				"Authentication failed, supplied login information was not valid.");
		
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		//took out the clean up here since test is already logged all the way out.
		//Will clean up next time test is run during the setup.
//		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
//			SecurityAdministration.deleteUsers_ForGivenEntity(testEntity);
//			SecurityAdministration.navigate_toEntityTab();
//			SecurityAdministration.deleteEntity(testEntity);
//		}
			Browser.closeBrowser();
	}
}
