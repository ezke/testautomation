package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_DeleteEntity extends BaseTest {

	private String testEntity = "autoTestEntity_DeleteEntity";
	private String nodeText = "WFS";
	
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteUsers_ForGivenEntity(testEntity);
			SecurityAdministration.navigate_toEntityTab();
			SecurityAdministration.deleteEntity(testEntity);
		}
		
		SecurityAdministration.selectNode(nodeText);
		// create a new entity so we don't accidentally mess something up
		// somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");
	}

	
	///Test Fixture not done
	
	@Test
	public void PT6188_LoginRules_MinimumSpecial_CannotPasteNegativeValue() throws Throwable {
		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
//		if (setup_Pass == true) {
//			// Re-launch the Edit Modal and take us back to the Passwords tab if
//			// it
//			// is not displayed.
			if (!Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
				SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
				SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
			} else {

			}
//		}
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}

		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
