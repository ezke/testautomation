package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_AuthenticationType extends BaseTest {

	private String parentEntity = "AutomationTest";
	private String testEntity = "test_AuthenticationSSO";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();

		SecurityAdministration.expandNode(parentEntity);

		if (BaseUI.pageSourceContainsString(testEntity)) {
			SecurityAdministration.deleteEntity(testEntity);
		}
		SecurityAdministration.selectNode(parentEntity);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");
		SecurityAdministration.selectNode(testEntity);
	}

	@Test
	public void PT6199_AuthenticationType_SSO_redirect_URL_without_HTTP_or_HTTPS() throws Throwable {
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		BaseUI.checkCheckbox("entityEntityInfoSingleSignOnCheckbox");
		SecurityAdmin_EntityManageModal.SignOutRedirectURL
				.verifyErrorMessage_afterEnteringtext_ForGivenField("www.google.com", "Please enter a valid URL.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	@Test
	public void PT6196_AuthenticationType_SSO_redirect_URL_HTTP() throws Throwable {
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		BaseUI.checkCheckbox("entityEntityInfoSingleSignOnCheckbox");
		SecurityAdmin_EntityManageModal.SignOutRedirectURL.verifyNO_ErrorMessage_ForGivenField(
				"http://www.google.com", "http://www.google.com");
		BaseUI.verifyElementEnabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}

	@Test
	public void PT6197_AuthenticationType_SSO_redirect_URL_HTTPS() throws Throwable {
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		BaseUI.checkCheckbox("entityEntityInfoSingleSignOnCheckbox");
		SecurityAdmin_EntityManageModal.SignOutRedirectURL.verifyNO_ErrorMessage_ForGivenField(
				"https://www.google.com", "https://www.google.com");
		BaseUI.verifyElementEnabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	@Test
	public void PT6198_AuthenticationType_SSO_redirect_URL_blank() throws Throwable {
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		BaseUI.checkCheckbox("entityEntityInfoSingleSignOnCheckbox");
		SecurityAdmin_EntityManageModal.SignOutRedirectURL.verifyNO_ErrorMessage_ForGivenField(
				"", "");
		BaseUI.verifyElementEnabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	@Test
	public void PT6198_AuthenticationType_defaultValue() throws Throwable {
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"), true);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			if (BaseUI.pageSourceContainsString(testEntity)) {
				SecurityAdministration.deleteEntity(testEntity);
			}
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
