package r360.tests_ignored.securityAdmin_Entities;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class RAAM_EntityManagement_SearchFunction extends BaseTest {

	private String testEntity = "autoTestEntity_SearchFnction";
	private String testEntityWithChildrenNodes = "AutomationTest";
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}
		Thread.sleep(4000);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");
		EntitySelector.selectNode("WFS");
	}

	
	
	@Test
	public void PT6277_Search_FilterEntities_verifyReturnedResultsAllMatch_first3letters() throws Throwable {
		String searchText = "tes";
		EntitySelector.search_ByText(searchText);
		EntitySelector.verifyResultsAllMatch(searchText);
	}
	
	@Test
	public void PT6277_Search_FilterEntities_SelectTestEntityAndVerifyCorrect_first3letters() throws Throwable {
		String searchText = "tes";
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
		Thread.sleep(100);

	}
	
	@Test
	public void PT6275_Search_FilterEntities_ResetFilter_first3letters() throws Throwable{
		String searchText = "tes";
		ArrayList<String> entitySelectorList = new ArrayList<String>();
		entitySelectorList.addAll(EntitySelector.return_List_OfEntities());
		
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
		EntitySelector.reset_SearchResults();
		EntitySelector.verifyPriorEntityList_MatchesCurrentListOfEntities(entitySelectorList);
	
	}
	
	@Test
	public void PT6277_Search_FilterEntities_verifyReturnedResultsAllMatch_last3letters() throws Throwable {
		String searchText = "ion";
		EntitySelector.search_ByText(searchText);
		EntitySelector.verifyResultsAllMatch(searchText);
	}
	
	@Test
	public void PT6277_Search_FilterEntities_SelectTestEntityAndVerifyCorrect_last3letters() throws Throwable {
		String searchText = "ion";
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
		Thread.sleep(100);

	}
	
	@Test
	public void PT6277_Search_FilterEntities_ResetFilter_last3letters() throws Throwable{
		String searchText = "chF";
		ArrayList<String> entitySelectorList = new ArrayList<String>();
		entitySelectorList.addAll(EntitySelector.return_List_OfEntities());
		
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
		EntitySelector.reset_SearchResults();
		EntitySelector.verifyPriorEntityList_MatchesCurrentListOfEntities(entitySelectorList);
	}
	
	@Test
	public void PT6277_Search_FilterEntities_verifyReturnedResultsAllMatch_mid3letters() throws Throwable {
		String searchText = "Fnc";
		EntitySelector.search_ByText(searchText);
		EntitySelector.verifyResultsAllMatch(searchText);
	}
	
	@Test
	public void PT6277_Search_FilterEntities_SelectTestEntityAndVerifyCorrect_mid3letters() throws Throwable {
		String searchText = "Fnc";
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
		Thread.sleep(100);

	}
	
	@Test
	public void PT6277_Search_FilterEntities_ResetFilter_mid3letters() throws Throwable{
		String searchText = "Fnc";
		ArrayList<String> entitySelectorList = new ArrayList<String>();
		entitySelectorList.addAll(EntitySelector.return_List_OfEntities());
		
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
		EntitySelector.reset_SearchResults();
		EntitySelector.verifyPriorEntityList_MatchesCurrentListOfEntities(entitySelectorList);
	}
	
	@Test
	public void PT6277_Search_FilterEntities_verify_NO_MatchingResults() throws Throwable {
		String searchText = "qxz";
		EntitySelector.search_ByText(searchText);
		EntitySelector.verifyNoResults(searchText);
	}
	
	@Test
	public void PT6274_Search_FilterEntities_EditSelected_Entity_Save() throws Throwable {
		String searchText = "tes";
		String textToEnter = "Test Description to test.";
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		
		SecurityAdmin_EntityManageModal.enter_Description(textToEnter);
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("entityDescription"), textToEnter);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
	}
	
	@Test
	public void PT6273_Search_FilterEntities_EditSelected_Entity_Cancel() throws Throwable {
		String searchText = "tes";
		String textToEnter = "Test Description to test for PT6273.";
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntity);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		
		SecurityAdmin_EntityManageModal.enter_Description(textToEnter);
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		BaseUI.verifyElementDoesNotHaveExpectedText("entityDescription", textToEnter);
		EntitySelector.verify_OnlySpecificEntityPresent(testEntity);
	}
	
	@Test
	public void PT6276_Search_FilterEntities_verify_ChildNodesMatch_AfterFiltered() throws Throwable {
		String searchText = "Aut";
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		ArrayList<String> priorList = new ArrayList<String>();
		priorList.addAll(EntitySelector.return_List_OfEntitiesAndChildren_ForGivenEntity(testEntityWithChildrenNodes));
		
		EntitySelector.search_ByText(searchText);
		EntitySelector.select_Results(testEntityWithChildrenNodes);
		EntitySelector.verify_Entity_AndChildNodes_Present(priorList, testEntityWithChildrenNodes);
	}
	
	//Cannot have any child nodes
	@Test
	public void PT6276_Search_FilterEntities_verify_SecondFilterMatches() throws Throwable {
		String searchText_ForMainEnt = "Aut";
		String searchText_ForChildEnt = "Ina";
		String entity_ChildNode = "testEntity_InactiveWorkGroupAssigned";
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.search_ByText(searchText_ForMainEnt);
		EntitySelector.select_Results(testEntityWithChildrenNodes);
		EntitySelector.search_ByText(searchText_ForChildEnt);
		EntitySelector.select_Results(entity_ChildNode);
		
		EntitySelector.verify_OnlySpecificEntityPresent(entity_ChildNode);
	}
	
	@Test
	public void PT6276_Search_FilterEntities_verify_ThirdFilterNoMatches() throws Throwable {
		String searchText_ForMainEnt = "Aut";
		String searchText_ForChildEnt = "Per";
		String thirdSearch = "xyz";
		String entity_ChildNode = "AutomationPermission";
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.search_ByText(searchText_ForMainEnt);
		EntitySelector.select_Results(testEntityWithChildrenNodes);
		EntitySelector.search_ByText(searchText_ForChildEnt);
		EntitySelector.select_Results(entity_ChildNode);
		EntitySelector.search_ByText(thirdSearch);
		EntitySelector.verifyNoResults();

	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		EntitySelector.close_SearchDropdown();
		if(Locator.lookupElement("entityTree_ResultsResetButton").isDisplayed())
		{
			EntitySelector.reset_SearchResults();
		}
		
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if(Locator.lookupElement("entityTree_ResultsResetButton").isDisplayed())
		{
			EntitySelector.reset_SearchResults();
		}
		
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
