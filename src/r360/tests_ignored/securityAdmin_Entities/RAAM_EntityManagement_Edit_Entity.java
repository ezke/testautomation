package r360.tests_ignored.securityAdmin_Entities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_Edit_Entity extends BaseTest {

	private String testEntity = "newTestAutoEntity";
	private String testEntityName2 = "2ndTestEntityName";
	private String entityType = "Financial Institution";
	private String entityDescription = "Test Description";
	private String entityExternalID = "Test External ID 1";
	private String nodeText = "WFS";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		// Remove entity if it exists.
		// if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null))
		// {
		if (BaseUI.pageSourceContainsString(testEntity)) {
			SecurityAdministration.deleteEntity(testEntity);
		}
		// if (BaseUI.elementExists("entityTreeNode_ByText", testEntityName2,
		// null)) {
		if (BaseUI.pageSourceContainsString(testEntityName2)) {
			SecurityAdministration.deleteEntity(testEntityName2);
		}
		SecurityAdministration.selectNode(nodeText);

		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, entityExternalID);
		SecurityAdministration.selectNode(testEntity);

	}

	@Test(alwaysRun = true)
	public void PT6246_EditEntity_ExternalEntityID_NotRequired() throws Throwable {
		String newEntityID = "";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, newEntityID);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("securityEntities_ExternEntityIDValue"), newEntityID);
	}

	@Test(alwaysRun = true)
	public void PT6246_EditEntity_ExternalEntityID_ChangedValue() throws Throwable {
		String newEntityID = "newTestExternID";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, newEntityID);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("securityEntities_ExternEntityIDValue"), newEntityID);
	}

	@Test(alwaysRun = true)
	public void PT6247_EditEntity_ExternalEntityID_ChangedValue_CannotMatchExistingExternalID() throws Throwable {
		String newEntityID = "WFSJRR";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		//SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, newEntityID);
		SecurityAdmin_EntityManageModal.verifyTextBoxStringInvalid_AfterSave(newEntityID,
				"entityEntityInfoExtEntityIdText", "External Entity ID must be unique.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}

	@Test(alwaysRun = true)
	public void PT6248_CreateNewEntity_ExternalEntityID_InvalidSpecialChars() throws Throwable {
		String[] specialChars = { "!", "@", "#", "$", "%", "^", "*", "~", "`", "+", "=", "{", "}", "[", "]", "\\", ":",
				";", "?", "//", "<", ">" };
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxCharsInvalid(specialChars, "entityEntityInfoExtEntityIdText",
				"Only letters, digits, spaces and & , . ' ( ) - _ are allowed.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}

	@Test(alwaysRun = true)
	public void PT6248_CreateNewEntity_ExternalEntityID_ClearInvalidChars_AndSave() throws Throwable {
		String invalidText = "!@#$%^*~`+={}[]\\:;?//<>";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, invalidText);
		SecurityAdmin_EntityManageModal.enter_ExternalEntityID(invalidText);
		SecurityAdmin_EntityManageModal.click_save();
		// error element text.
		WebElement errorText = Locator.lookupElement("entityEntityInfoExtEntityIdText").findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		BaseUI.verifyElementHasExpectedText(errorText, "Only letters, digits, spaces and & , . ' ( ) - _ are allowed.");
		// switched it to blank text and clicked save. Should then update as
		// expected.
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, "");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("securityEntities_ExternEntityIDValue"), "");
	}

	@Test(alwaysRun = true)
	public void PT6249_EditEntity_ExternalEntityID_ChangedValue_CanContainSpace() throws Throwable {
		String newEntityID = "New Entity ID";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, newEntityID);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("securityEntities_ExternEntityIDValue"), newEntityID);
	}

	@Test(alwaysRun = true)
	public void PT6250_EditEntity_ExternalEntityID_ChangedValue_CanValidSpecialChars() throws Throwable {
		String newEntityID = "&,.'()-_Stuff";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, newEntityID);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("securityEntities_ExternEntityIDValue"), newEntityID);
	}

	@Test(alwaysRun = true)
	public void PT6250_EditEntity_ExternalEntityID_ChangedEntityName_ExternalEntityID_NotRequired() throws Throwable {
		String newEntityID = "";
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.add_Entity(testEntityName2, entityType, entityDescription, newEntityID);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("securityEntities_ExternEntityIDValue"), newEntityID);
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		// check to see if our secondary Entity Name exists. If it does reset it
		// to the 1st entity name.
		// if (BaseUI.elementExists("entityTreeNode_ByText", testEntityName2,
		// null)) {
		if (BaseUI.pageSourceContainsString(testEntityName2)) {
			SecurityAdministration.selectNode(testEntityName2);
			SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntityName2);
			SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, entityExternalID);
		} else {
			// Code to reset Entity to default values.
			if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
				SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, entityExternalID);
			} else {
				SecurityAdministration.selectNode(testEntity);
				SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
				SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType, entityDescription, entityExternalID);
			}

		}
		SecurityAdministration.selectNode(testEntity);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {

			// Remove entity if it exists.
			if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
				SecurityAdministration.deleteEntity(testEntity);
			}

		} catch (Exception e) {
		}

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
