package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_LoginConfigurationRules extends BaseTest {

	private String testEntity = "autoTestEntity_Logins";
	private String nodeText = "WFS";
	
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		// create a new entity so we don't accidentally mess something up
		// somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
	}

	@Test
	public void PT6210_LoginRules_InactiveDays_CannotBeMoreThan60() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigInactiveDaysText",
				"Please enter a value less than or equal to 60.", "61");
	}

	@Test
	public void PT6210_LoginRules_InactiveDays_CannotBe0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigInactiveDaysText",
				"Please enter a value greater than or equal to 1.", "0");
	}

	@Test
	public void PT6210_LoginRules_InactiveDays_CannotBeBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigInactiveDaysText",
				"This field is required.", "");
	}

	@Test
	public void PT6210_LoginRules_InactiveDays_NoSpecialCharsAllowed() throws Throwable {
		String value = "/*-+abcd12";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigInactiveDaysText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigInactiveDaysText",
				"12");
	}
	
	@Test
	public void PT6210_LoginRules_InactiveDays_AllowsOnly2Characters() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigInactiveDaysText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigInactiveDaysText",
				"12");
	}
	
	@Test
	public void PT6210_LoginRules_InactiveDays_ClickingCancelDoesNotSave() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenLoginField(testEntity,
				"entityLoginConfigInactiveDaysText", "34");
	}
	
	@Test
	public void PT6210_LoginRules_InactiveDays_SaveChange() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigInactiveDaysText", "1");
	}
	
	@Test
	public void PT6211_LoginRules_MaximumLoginAttempts_CannotBeMoreThan5() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigMaxLoginAttemptText",
				"Please enter a value less than or equal to 5.", "6");
	}
	
	@Test
	public void PT6211_LoginRules_MaximumLoginAttempts_CannotBe0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigMaxLoginAttemptText",
				"Please enter a value greater than or equal to 1.", "0");
	}
	
	@Test
	public void PT6211_LoginRules_MaximumLoginAttempts_CannotBeBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigMaxLoginAttemptText",
				"This field is required.", "");
	}

	@Test
	public void PT6211_LoginRules_MaximumLoginAttempts_DoesNOTAllowMoreThan1Characters() throws Throwable {
		String value = "123";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigMaxLoginAttemptText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigMaxLoginAttemptText",
				"1");
	}
	
	@Test
	public void PT6211_LoginRules_MaximumLoginAttempts_ClickingCancelDoesNOTSave() throws Throwable {
		String value = "3";
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenLoginField(testEntity,
				"entityLoginConfigMaxLoginAttemptText", value);
	}
	
	@Test
	public void PT6211_LoginRules_MaximumLoginAttempts_ClickingSave() throws Throwable {
		String value = "4";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigMaxLoginAttemptText", value);
	}
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaCharacters_SaveAsBlank_ChangesTo0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField_BlankComesBackAs0(testEntity,
				"entityLoginConfigCharacterRulesMinAlphaCharText");
		
	}
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaCharacters_CannotContainSpecialChars() throws Throwable {
		String value = "/*-+abcd12";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinAlphaCharText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinAlphaCharText",
				"12");
	}
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaCharacters_DoesNOTAllowMoreThan3Characters() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinAlphaCharText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinAlphaCharText",
				"123");
	}
	
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaCharacters_ClickingSave() throws Throwable {
		String value = "17";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinAlphaCharText", value);
	}
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaCharacters_ClickingCancelDoesNOTSave() throws Throwable {
		String value = "8";
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinAlphaCharText", value);
	}
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaCharacters_CannotPasteNegativeValue() throws Throwable {
		
		String value = "-3";
		SecurityAdmin_EntityManageModal.verify_CannotPasteNegativeValue("entityLoginConfigCharacterRulesMinAlphaCharText", value, "Please enter a digit.", testEntity);
	}
	
	@Test
	public void PT6214_LoginRules_MinimumLength_DoesNOTAllowMoreThan3Characters() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinLengthText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinLengthText",
				"123");
	}
	
	@Test
	public void PT6214_LoginRules_MinimumLength_CannotBeBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigCharacterRulesMinLengthText",
				"This field is required.", "");
	}
	
	@Test
	public void PT6214_LoginRules_MinimumLength_ClickingSave() throws Throwable {
		String value = "8";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinLengthText", value);
	}
	
	@Test
	public void PT6215_LoginRules_MinimumLength_ClickingCancel() throws Throwable {
		String value = "4";
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinLengthText", value);
	}
	

	@Test
	public void PT6215_LoginRules_MinimumLength_CannotBe0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyTextBoxError("entityLoginConfigCharacterRulesMinLengthText",
				"Please enter a value greater than or equal to 1.", "0");
	}
	
	@Test
	public void PT6215_LoginRules_MinimumLength_DoesNOTAllowSpecialCharacters() throws Throwable {
		String value = "/*-+abcd34";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinLengthText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinLengthText",
				"34");
	}
	
	@Test
	public void PT6216_LoginRules_MinimumNumeric_CanBe0() throws Throwable {
		String value = "0";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinNumCharText", value);
	}
	
	@Test
	public void PT6216_LoginRules_MinimumNumeric_DoesNOTAllowSpecialCharacters() throws Throwable {
		String value = "/*-+abcd34";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinNumCharText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinNumCharText",
				"34");
	}
	
	
	@Test
	public void PT6216_LoginRules_MinimumNumeric_DoesNOTAllowMoreThan3Characters() throws Throwable {
		String value = "56789";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinNumCharText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinNumCharText",
				"567");
	}
	
	@Test
	public void PT6216_LoginRules_MinimumNumeric_ClickingSave() throws Throwable {
		String value = "4";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinNumCharText", value);
	}
	
	@Test
	public void PT6216_LoginRules_MinimumNumeric_ClickingCancel() throws Throwable {
		String value = "9";
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinNumCharText", value);
	}
	
	@Test
	public void PT6217_LoginRules_MinimumNumeric_CannotPasteNegativeValue() throws Throwable {
		String value = "-2";
		SecurityAdmin_EntityManageModal.verify_CannotPasteNegativeValue("entityLoginConfigCharacterRulesMinNumCharText", value, "Please enter a digit.", testEntity);
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_ClickingSaveFor0() throws Throwable {
		String value = "0";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinSpecialCharText", value);
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_SaveAsBlank_ChangesTo0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField_BlankComesBackAs0(testEntity,
				"entityLoginConfigCharacterRulesMinSpecialCharText");
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_DoesNotAllowSpecialAndAlphaChars() throws Throwable {
		String value = "/*-+abcd12";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinSpecialCharText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinSpecialCharText",
				"12");
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_CanNotBeOver3Chars() throws Throwable {
		String value = "2345";
		BaseUI.enterText(Locator.lookupElement("entityLoginConfigCharacterRulesMinSpecialCharText"),
				value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinSpecialCharText",
				"234");
	}
	
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_ClickingSave() throws Throwable {
		String value = "2";
		SecurityAdmin_EntityManageModal.verifyChange_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinSpecialCharText", value);
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_ClickingCancel() throws Throwable {
		String value = "7";
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenLoginField(testEntity,
				"entityLoginConfigCharacterRulesMinSpecialCharText", value);
	}
	
	@Test
	public void PT6219_LoginRules_MinimumSpecial_CannotPasteNegativeValue() throws Throwable {
		String value = "-2";
		SecurityAdmin_EntityManageModal.verify_CannotPasteNegativeValue("entityLoginConfigCharacterRulesMinSpecialCharText", value, "Please enter a digit.", testEntity);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
//		if (setup_Pass == true) {
			// Re-launch the Edit Modal and take us back to the Passwords tab if
			// it
			// is not displayed.
			if (!Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
				SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
				SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
			} else {

			}
//		}
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}

		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
