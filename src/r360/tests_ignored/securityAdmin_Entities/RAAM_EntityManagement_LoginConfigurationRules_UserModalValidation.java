package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_LoginConfigurationRules_UserModalValidation extends BaseTest {

	private String testEntity = "autoTestEntity_LoginsValidation2";
	private String nodeText = "AutomationTest";
	
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		
		SecurityAdministration.selectNode(nodeText);
		SecurityAdministration.expandNode(nodeText);
		
		
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
			SecurityAdministration.selectNode(nodeText);
			SecurityAdministration.expandNode(nodeText);
		}
		
		// create a new entity so we don't accidentally mess something up
		// somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
	}

	
	@Test
	public void PT6212_LoginRules_MinimumAlpha_UserManagement_ValidateError() throws Throwable {
		SecurityAdmin_EntityManageModal.verify_LoginValidation("entityLoginConfigCharacterRulesMinAlphaCharText", "8", "abcdefg12", "Login Name must have a minimum of 8 letters.");;
		
	}
	
	@Test
	public void PT6215_LoginRules_MinimumLength_UserManagement_ValidateError() throws Throwable {
		SecurityAdmin_EntityManageModal.verify_LoginValidation("entityLoginConfigCharacterRulesMinLengthText", "8", "abcdefg", "Login Name must have minimum length of 8 characters.");;
		
	}
	
	@Test
	public void PT6216_LoginRules_MinimumNumericChars_UserManagement_ValidateError() throws Throwable {
		SecurityAdmin_EntityManageModal.verify_LoginValidation("entityLoginConfigCharacterRulesMinNumCharText", "4", "abcdefgh123", "Login Name must have minimum of 4 digits.");;
		
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_UserManagement_ValidateError() throws Throwable {
		SecurityAdmin_EntityManageModal.verify_LoginValidation("entityLoginConfigCharacterRulesMinSpecialCharText", "2", "wfs_test", "Login Name must have minimum of 2 special (non-alphanumeric) characters.");;
		
	}
	
	@Test
	public void PT6218_LoginRules_MinimumSpecialChars_UserManagement_ValidateError_WrongSpecialChar() throws Throwable {
		SecurityAdmin_EntityManageModal.verify_LoginValidation("entityLoginConfigCharacterRulesMinSpecialCharText", "2", "wfs##test", "Only Letters, digits, and @ . _ are allowed.");;
		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
//		if (setup_Pass == true) {

			//first check if UserManagement Modal displayed and close it.
			if(Locator.lookupElement("entityUserEdit_Modal").isDisplayed())
			{
				SecurityAdmin_UserManageModal.click_cancelButton();
				SecurityAdministration.navigate_toEntityTab();
			}
			
			if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
				SecurityAdmin_EntityManageModal.reset_Modal_LoginTab(testEntity);
			} else {
				SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
				SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
			}
			SecurityAdmin_EntityManageModal.set_LoginConfiguration_Settings("60", "5", "5", "0", "4", "0");
			SecurityAdmin_EntityManageModal.save_EntityManagementModal();
			SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
			SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
			
//		}
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}

		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
