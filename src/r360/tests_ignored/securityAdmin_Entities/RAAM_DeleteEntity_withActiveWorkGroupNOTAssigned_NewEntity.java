package r360.tests_ignored.securityAdmin_Entities;

import java.text.MessageFormat;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AssignWorkgroupsModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class RAAM_DeleteEntity_withActiveWorkGroupNOTAssigned_NewEntity extends BaseTest {

	private String entityToExpand = "AutomationTest";
	private String testEntity = "NwEntty_ActvWrkGrp_NOTAssigned";
	private String workgroupToMove = "TestActiveWorkgroup_getsAddedToNewEntity";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		EntitySelector.expandNode(entityToExpand);

		SecurityAdministration.deleteEntity_With_Workgroups(testEntity, entityToExpand);
		
		EntitySelector.selectNode(entityToExpand);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Corporation");

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(entityToExpand);
		EntitySelector.selectNode(testEntity);

		Admin_Workgroups.addDefaultWorkGroup_Settings("360", "360");

		// Deviated from TC here, added pre-existing workgroup.
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.move_fromAvailable_toSelected(workgroupToMove);
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();

		// remove all work groups from the entity.
		Admin_Workgroups.remove_AllWorkgroupsFromEntity();

		Pagination workGroupGrid = new Pagination(Admin_Workgroups.workGroupSettings_GridID);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		workGroupGrid.verifyCountOfEntries(0);
		Navigation.navigate_Admin_Entities();
		EntitySelector.expandNode(entityToExpand);
		SecurityAdministration.deleteEntity_LaunchDeleteModal(testEntity);
	}


	@Test(priority = 1, groups = {"all_Tests"})
	public void PT6194_DeleteModal_Appears_CancelDoesNotDeleteEntity() throws Throwable {
		BaseUI.click(Locator.lookupElement("securityEntitiesDeleteModal_CancelButton"));
		Thread.sleep(2000);
		EntitySelector.expandNode(entityToExpand);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityTreeNode_ByText", testEntity, null));
		
	}
	
	
	@Test(priority = 2, groups = {"all_Tests"})
	public void PT6194_DeleteModal_Appears_AbleToDeleteEntity() throws Throwable {
		if(!Locator.lookupElement("entityDeleteModal").isDisplayed())
		{
			EntitySelector.expandNode(entityToExpand);
			SecurityAdministration.deleteEntity_LaunchDeleteModal(testEntity);
		}
		
		BaseUI.click(Locator.lookupElement("securityEntitiesDeleteModal_DeleteButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		EntitySelector.expandNode(entityToExpand);
		BaseUI.verify_false_AndLog(BaseUI.elementExists("entityTreeNode_ByText", testEntity, null),
				MessageFormat.format("Entity {0} does not exist.", testEntity),
				MessageFormat.format("Entity {0} was NOT deleted.", testEntity));
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if (Locator.lookupElement("entityDeleteModal").isDisplayed()) {
			BaseUI.click(Locator.lookupElement("securityEntitiesDeleteModal_CancelButton"));
			Thread.sleep(1000);
		}
		EntitySelector.expandNode(entityToExpand);
		SecurityAdministration.deleteEntity_With_Workgroups(testEntity, entityToExpand);

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
