package r360.tests_ignored.securityAdmin_Entities;

import java.text.MessageFormat;
import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class RAAM_EntityManagement_AddAndDelete_Entity extends BaseTest {

	private String testEntity = "newTestAutoEntity";
	private String entityType = "Financial Institution";
	private String nodeText = "AutomationTest";
	private String entityParent = "testEnt_WithChildEnts";
	private String entityChild = "testEnt_ChildEnt";
	
	

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, 
				GlobalVariables.password, 0);
		
		Navigation.navigate_Admin_Entities();
		
		SecurityAdministration.expandNode(nodeText);
		
		if(BaseUI.pageSourceContainsString(testEntity))
		{
			SecurityAdministration.deleteEntity(testEntity);
		}
		if(BaseUI.pageSourceContainsString(entityParent))
		{
			SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
	}
	
	@Test
	public void PT6185_CreateNewEntity_appearsInEntityTree() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityTreeNode_ByText", testEntity, null));
	}
	
	@Test
	public void PT6220_CreateNewEntity_noGroupsInGrid() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType);
		SecurityAdministration.navigate_toGroupsTab();
		
		Pagination groupsGrid = new Pagination("groupGrid");
		groupsGrid.verifyCountOfEntries(0);
		
	}
	
	@Test
	public void PT6221_CreateNewEntity_appearsInEntityTree() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType);
		SecurityAdministration.navigate_toRolesTab();
		
		Pagination rolesGrid = new Pagination("roleGrid");
		rolesGrid.verifyCountOfEntries(0);
		
	}
	
	@Test
	public void PT6201_CreateNewEntity_deleteEntity() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType);
		SecurityAdministration.deleteEntity(testEntity);
		BaseUI.verify_false_AndLog(BaseUI.elementExists("entityTreeNode_ByText", testEntity, null), 
				MessageFormat.format("Entity: {0} did NOT appear in list", testEntity),
				MessageFormat.format("Entity: {0} did appeared in list", testEntity));
	}
	
	@Test
	public void PT6240_CreateNewEntity_ExternalEntityID_InvalidSpecialChars() throws Throwable {
		String[] specialChars = {"!", "@", "#", "$", "%", "^", "*"};
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxCharsInvalid(specialChars, "entityEntityInfoExtEntityIdText", 
				"Only letters, digits, spaces and & , . ' ( ) - _ are allowed.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}

	@Test
	public void PT6240_CreateNewEntity_ExternalEntityID_ValidSpecialChars() throws Throwable {
		String specialChars = "&,.'()-_Stuff";
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxStringValid(specialChars, "entityEntityInfoExtEntityIdText");
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		BaseUI.verifyElementHasExpectedText("securityEntities_ExternEntityIDValue", specialChars);
	}

	@Test
	public void PT6241_CreateNewEntity_ExternalEntityID_NotRequired() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityTreeNode_ByText", testEntity, null));
		BaseUI.verifyElementHasExpectedText("securityEntities_ExternEntityIDValue", "");
	}
	
	@Test
	public void PT6242_CreateNewEntity_ExternalEntityID_MustBeUnique() throws Throwable {
		String non_unique_ExternalID = "WFSJRR";
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxStringInvalid_AfterSave(non_unique_ExternalID, "entityEntityInfoExtEntityIdText", 
				"External Entity ID must be unique.");
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	@Test
	public void PT6243_CreateNewEntity_ExternalEntityID_AllowsSpaces() throws Throwable {
		String textToEnter = "New Space Test";
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxStringValid(textToEnter, "entityEntityInfoExtEntityIdText");
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		BaseUI.verifyElementHasExpectedText("securityEntities_ExternEntityIDValue", textToEnter);
	}
	
	@Test
	public void PT6243_CreateNewEntity_ExternalEntityID_AllowsAlphaNumeric() throws Throwable {
		String textToEnter = "Test12345";
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxStringValid(textToEnter, "entityEntityInfoExtEntityIdText");
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		BaseUI.verifyElementHasExpectedText("securityEntities_ExternEntityIDValue", textToEnter);
	}
	
	@Test
	public void PT6245_CreateNewEntity_ExternalEntityID_AllowsValidID() throws Throwable {
		String textToEnter = "EntityTestID-ID";
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(testEntity, entityType);
		SecurityAdmin_EntityManageModal.verifyTextBoxStringValid(textToEnter, "entityEntityInfoExtEntityIdText");
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		BaseUI.verifyElementHasExpectedText("securityEntities_ExternEntityIDValue", textToEnter);
	}
	

	@Test
	public void PT6247_CreateNewEntity_ExternalEntityID_NotRequired() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, entityType);
		
		BaseUI.verifyElementAppears(Locator.lookupElement("entityTreeNode_ByText", testEntity, null));
		BaseUI.verifyElementHasExpectedText("securityEntities_ExternEntityIDValue", "");
	}
	
	@Test
	public void PT6206_CreateNewEntity_EntityName_OnlyAllows50Chars() throws Throwable {
		String testString = "1232safdfiuiuiuijkjkjererekjkjerere123456789Blue324";
		SecurityAdministration.launchAddEntityModal();
		WebElement entityNameElement = Locator.lookupElement("entityEntityInfoNameText");
		BaseUI.enterText(entityNameElement, testString);
		BaseUI.tabThroughField(entityNameElement);
		String textboxOutput = BaseUI.getTextFromInputBox(entityNameElement);
		BaseUI.baseStringCompare("Entity Name", testString.substring(0,50), textboxOutput);
		
	}
	
	@Test
	public void PT6207_CreateNewEntity_EntityName_Required() throws Throwable {
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("", entityType);
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoExtEntityIdText"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	@Test
	public void PT6207_CreateNewEntity_EntityName_Required_BlankSpace() throws Throwable {
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("", entityType);
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoExtEntityIdText"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	@Test
	public void PT6208_CreateNewEntity_EntityName_OnlyAllows50Chars_AndSaves() throws Throwable {
		String testString50Chars = "1232safdfiuiuiuijkjkjererekjkjerere123456789Blue12";
		String testString51Chars = "1232safdfiuiuiuijkjkjererekjkjerere123456789Blue123";
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", testString50Chars, null)) {
			SecurityAdministration.deleteEntity(testString50Chars);
		}
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testString51Chars, "Financial Institution");
		BaseUI.verifyElementExists("entityTreeNode_ByText", testString50Chars, null);
		
	}
	
	@Test
	public void PT6202_CreateNewEntity_Click_Cancel() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityEntityUpdateModal"));
	}
	
	
	@Test
	public void PT6204_CreateNewEntity_EntityDescription_OnlyAllows80Chars() throws Throwable {
		String testString81chars = "abc123helloworld4567890andstuffIneedtogetto81charsomgthisistakingforeveralmostthe";
		SecurityAdministration.launchAddEntityModal();
		WebElement entityDescriptionElement = Locator.lookupElement("entityEntityInfoDescriptionText");
		BaseUI.enterText(entityDescriptionElement, testString81chars);
		BaseUI.tabThroughField(entityDescriptionElement);
		String textboxOutput = BaseUI.getTextFromInputBox(entityDescriptionElement);
		BaseUI.baseStringCompare("Entity Name", testString81chars.substring(0,80), textboxOutput);
		
	}
	
	@Test
	public void PT6205_CreateNewEntity_EntityType_DefaultsTo_Choose() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		BaseUI.verifyElementHasExpectedText("entityEntityInfoEntityTypeDropdown_Text", "Choose...");
	}
	
	@Test
	public void PT6205_CreateNewEntity_EntityType_AbleToSelect_EachEntityType() throws Throwable {
		SecurityAdministration.launchAddEntityModal();
		//BaseUI.enter
		BaseUI.enterText(Locator.lookupElement("entityEntityInfoNameText"), testEntity);
		SecurityAdmin_EntityManageModal.click_EntityTypeDropdown();
		
		ArrayList<WebElement> entityTypeList = new ArrayList<WebElement>();
		entityTypeList.addAll(Locator.lookup_multipleElements("entityEntityInfoEntityTypeDropdown_ListItems", null, null));
		
		SecurityAdmin_EntityManageModal.click_EntityTypeDropdown_Close();
		
		for(Integer i = entityTypeList.size(); i > 0; i--)
		{
			String entityTypeText = SecurityAdmin_EntityManageModal.select_EntityValue_ByIndex(i);
			BaseUI.verifyElementEnabled(Locator.lookupElement("entityEntityInfoSaveButton"));
			SecurityAdmin_EntityManageModal.verify_EntityTypeDropdownAccurate(entityTypeText);
		}
	
	}
	
	@Test
	public void PT6205_CreateNewEntity_EntityType_StillSelected_AfterNavigatingToPasswordConfigTab() throws Throwable {
		String entityType = "Financial Institution";
		
		SecurityAdministration.launchAddEntityModal();
		BaseUI.enterText(Locator.lookupElement("entityEntityInfoNameText"), testEntity);
		SecurityAdmin_EntityManageModal.select_EntityValue(entityType);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.navigate_to_EntityInfoTab();
		SecurityAdmin_EntityManageModal.verify_EntityTypeDropdownAccurate(entityType);
	}
	
	@Test
	public void PT6205_CreateNewEntity_EntityType_SelectEntityType_AndCancel_EntityDoesNOTExist() throws Throwable {
		String entityType = "Holding Company";
		
		SecurityAdministration.launchAddEntityModal();
		BaseUI.enterText(Locator.lookupElement("entityEntityInfoNameText"), testEntity);
		SecurityAdmin_EntityManageModal.select_EntityValue(entityType);
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		BaseUI.verifyElementDoesNOTExist("entityTreeNode_ByText", testEntity, null);
	}
	
	@Test
	public void PT6205_CreateNewEntity_EntityType_SelectEntityType_clickX_ByEntity_ClearsEntityType() throws Throwable {
		String entityType = "Holding Company";
		
		SecurityAdministration.launchAddEntityModal();
		BaseUI.enterText(Locator.lookupElement("entityEntityInfoNameText"), testEntity);
		SecurityAdmin_EntityManageModal.select_EntityValue(entityType);
		SecurityAdmin_EntityManageModal.clear_selectedEntityType();
		BaseUI.tabThroughField("entityEntityInfoNameText");
		SecurityAdmin_EntityManageModal.verify_EntityTypeDropdownAccurate("Choose...");
//		BaseUI.tabThroughField("entityEntityInfoNameText");
//		BaseUI.verifyElementDisabled(Locator.lookupElement("entityEntityInfoSaveButton"));
		
	}
	
	@Test
	public void PT6200_DeleteEntity_WithChildEntities() throws Throwable {
		
		if(BaseUI.pageSourceContainsString(entityParent))
		{
			SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(entityParent, "Financial Institution");
		SecurityAdministration.selectNode(entityParent);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(entityChild, "Financial Institution");
		
		SecurityAdministration.selectNode(entityParent);
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityDeleteButton"));
		
		SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		
	}
	
	@Test
	public void PT6222_Entity_NewEntity_Inherit_ParentsAuthenticationType_StdPWordChecked_SingleSignOnChecked() throws Throwable {
		if(BaseUI.pageSourceContainsString(entityParent))
		{
			SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(entityParent, "Financial Institution");
		BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		
		SecurityAdministration.selectNode(entityParent);
		SecurityAdministration.launchAddEntityModal();
	
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"), true);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"), true);
	}
	
	@Test
	public void PT6222_Entity_NewEntity_Inherit_ParentsAuthenticationType_StdPWordUNChecked_SingleSignOnChecked() throws Throwable {
		if(BaseUI.pageSourceContainsString(entityParent))
		{
			SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(entityParent, "Financial Institution");
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		
		SecurityAdministration.selectNode(entityParent);
		SecurityAdministration.launchAddEntityModal();
	
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"), false);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"), true);
	}
	
	@Test
	public void PT6222_Entity_NewEntity_Inherit_ParentsAuthenticationType_StdPWordChecked_SingleSignOnUNChecked() throws Throwable {
		if(BaseUI.pageSourceContainsString(entityParent))
		{
			SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(entityParent, "Financial Institution");
		BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		
		SecurityAdministration.selectNode(entityParent);
		SecurityAdministration.launchAddEntityModal();
	
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"), true);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"), false);
	}
	
	@Test
	public void PT6222_Entity_NewEntity_Inherit_ParentsAuthenticationType_StdPWordUNChecked_SingleSignOnUNChecked() throws Throwable {
		if(BaseUI.pageSourceContainsString(entityParent))
		{
			SecurityAdministration.deleteEntity_WithChildEntities(entityParent);
		}
		
		SecurityAdministration.selectNode(nodeText);
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo(entityParent, "Financial Institution");
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();
		
		SecurityAdministration.selectNode(entityParent);
		SecurityAdministration.launchAddEntityModal();
	
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"), false);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"), false);
	}
	
	@Test
	public void PT6187_Entity_NewEntityAddButton_LaunchesAddEntityModal() throws Exception
	{
		SecurityAdministration.launchAddEntityModal();
		BaseUI.verifyElementAppears(Locator.lookupElement("entityEntityUpdateModal"));
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		//Cancel out of Add Entity Window if it exists.
		if(BaseUI.elementAppears(Locator.lookupElement("entityEntityUpdateModal")))
		{
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}
		
		if(Locator.lookupElement("entityDeleteModal").isDisplayed())
		{
			SecurityAdministration.cancel_deleteEntity();
		}
		
		SecurityAdministration.expandNode(nodeText);
		//Remove entity if it exists.
		if(BaseUI.pageSourceContainsString(testEntity))
		{
			SecurityAdministration.deleteEntity(testEntity);
		}
		
		SecurityAdministration.expandNode(nodeText);
		SecurityAdministration.selectNode(nodeText);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
