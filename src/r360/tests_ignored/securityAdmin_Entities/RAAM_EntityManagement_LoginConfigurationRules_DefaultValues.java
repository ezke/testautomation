package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_LoginConfigurationRules_DefaultValues extends BaseTest {

	private String testEntity = "autoTestEntity_Logins";
	private String nodeText = "WFS";
	
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		// create a new entity so we don't accidentally mess something up
		// somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
	}

	@Test
	public void PT6210_LoginRules_InactiveDays_DefaultValue() throws Throwable {
		String expectedValue = "60";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigInactiveDaysText", expectedValue);
	}
	
	@Test
	public void PT6211_LoginRules_MaxLoginAttempts_DefaultValue() throws Throwable {
		String expectedValue = "5";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigMaxLoginAttemptText", expectedValue);
	}
	
	@Test
	public void PT6212_LoginRules_MinimumAlphaChars_DefaultValue() throws Throwable {
		String expectedValue = "4";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinAlphaCharText", expectedValue);
	}
	
//	@Test
//	public void PT6215_LoginRules_MinimumLength_DefaultValue() throws Throwable {
//		String expectedValue = "4";
//		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinLengthText", expectedValue);
//	}
	
	@Test
	public void PT6216_LoginRules_MinimumNumeric_DefaultValue() throws Throwable {
		String expectedValue = "0";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinNumCharText", expectedValue);
	}

	@Test
	public void PT6218_LoginRules_MinimumSpecial_DefaultValue() throws Throwable {
		String expectedValue = "0";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityLoginConfigCharacterRulesMinSpecialCharText", expectedValue);
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {
		
		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}

		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
