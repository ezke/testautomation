package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_PasswordRules_DefaultValues extends BaseTest {

	private String testEntity = "autoTestEntity_Passwords";
	private String nodeText = "WFS";

	
	@BeforeClass
	public void setup_method() throws Exception
	{
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, 
				GlobalVariables.password);
		
		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		//create a new entity so we don't accidentally mess something up somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity("autoTestEntity_Passwords", "Financial Institution");
		
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
	}
	

	@Test
	public void PT6224_PasswordRules_MinimumCharacters_DefaultsTo8() throws Throwable {
		String expectedValue = "8";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinLengthText", expectedValue);
		
	}
	

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_DefaultsTo1() throws Throwable {
		String expectedValue = "1";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinLowercaseCharText",
				expectedValue);
	}


	@Test
	public void PT6227_PasswordRules_MinimumUPPERCASE_DefaultsTo1() throws Throwable {
		String expectedValue = "1";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinUppercaseCharText",
				expectedValue);
	}

	@Test
	public void PT6229_PasswordRules_DaysBeforePasswordExpires_DefaultsTo60() throws Throwable {
		String expectedValue = "60";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigDaysBeforePasswordExpires",
				expectedValue);
	}
	
	@Test
	public void PT6230_PasswordRules_NumberOfDaysReUse_DefaultsTo180() throws Throwable {
		String expectedValue = "180";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigPasswordReuse",
				expectedValue);
	}
	
	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_DefaultsTo1() throws Throwable {
		String expectedValue = "1";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinNumCharText",
				expectedValue);
	}
	
	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_DefaultsTo1() throws Throwable {
		String expectedValue = "1";
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinSpecialCharText",
				expectedValue);
	}
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {

		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}
		
		if(BaseUI.elementExists("entityTreeNode_ByText", testEntity, null))
		{
			SecurityAdministration.deleteEntity(testEntity);
		}
		
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
