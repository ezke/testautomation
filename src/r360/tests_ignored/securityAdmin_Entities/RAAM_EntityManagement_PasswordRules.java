package r360.tests_ignored.securityAdmin_Entities;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityManagement_PasswordRules extends BaseTest {

	private String testEntity = "autoTestEntity_Passwords";
	private String nodeText = "WFS";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		SecurityAdministration.selectNode(nodeText);
		// create a new entity so we don't accidentally mess something up
		// somewhere.
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity("autoTestEntity_Passwords", "Financial Institution");

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
	
	}

	@Test
	public void PT6223_PasswordRules_MaximumRepeating_NegativeValue_NOTAllowed() throws Throwable {
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMaxRepeatinCharText"), "-1");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMaxRepeatinCharText", "1");
	}

	@Test
	public void PT6224_PasswordRules_MinimumCharacters_lessThan8() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinLengthText",
				"entityPwdConfigCharacterRulesMinLengthText_validation",
				"Please enter a value greater than or equal to 8.", "7");
	}

	@Test
	public void PT6224_PasswordRules_MinimumCharacters_cantBe0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinLengthText",
				"entityPwdConfigCharacterRulesMinLengthText_validation",
				"Please enter a value greater than or equal to 8.", "0");
	}

	@Test
	public void PT6224_PasswordRules_MinimumCharacters_cantBeEmpty() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinLengthText",
				"entityPwdConfigCharacterRulesMinLengthText_validation", "This field is required.", "");
	}

	@Test
	public void PT6225_PasswordRules_MinimumUppercaseCharacters_NegativeValue_NOTAllowed() throws Throwable {
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"), "-1");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinUppercaseCharText", "1");
	}

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_changeTo2_AndSave() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMinLowercaseCharText", "2");

	}

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_changeTo999_withoutSaving() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMinLowercaseCharText", "999");
	}

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_cantBe0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinLowercaseCharText",
				"entityPwdConfigCharacterRulesMinLowercaseCharText_validation",
				"Please enter a value greater than or equal to 1.", "0");
	}

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_cantBeEmpty() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinLowercaseCharText",
				"entityPwdConfigCharacterRulesMinLowercaseCharText_validation", "This field is required.", "");
	}

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_AlphaAndSpecialChars_NOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinLowercaseCharText"),
				alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinLowercaseCharText",
				"123");
	}

	@Test
	public void PT6226_PasswordRules_MinimumLowercase_onlyAllows3Chars() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinLowercaseCharText"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinLowercaseCharText",
				"123");
	}

	@Test
	public void PT6227_PasswordRules_MinimumUPPERCASE_allows0() throws Throwable {
		String value = "0";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"), value);
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"));
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText_validation"));
	}

	@Test
	public void PT6227_PasswordRules_MinimumUPPERCASE_saveBlank_ComesBackAs0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField_BlankComesBackAs0(testEntity,
				"entityPwdConfigCharacterRulesMinUppercaseCharText",
				"entityPwdConfigCharacterRulesMinUppercaseCharText_validation");
	}

	@Test
	public void PT6227_PasswordRules_MinimumUPPERCASE_AlphaAndSpecialChars_NOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"),
				alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinUppercaseCharText",
				"123");
	}

	@Test
	public void PT6227_PasswordRules_MinimumUPPERCASE_onlyAllows3Chars() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinUppercaseCharText",
				"123");
	}

	@Test
	public void PT6228_PasswordRules_NumberOfAllowedRepeatChars_AlphaAndSpecialChars_NOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMaxRepeatinCharText"),
				alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMaxRepeatinCharText", "123");
	}

	@Test
	public void PT6228_PasswordRules_NumberOfAllowedRepeatChars_onlyAllows3Chars() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMaxRepeatinCharText"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMaxRepeatinCharText", "123");
	}

	@Test
	public void PT6228_PasswordRules_NumberOfAllowedRepeatChars_saveBlank_ComesBackAs0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField_BlankComesBackAs0(testEntity,
				"entityPwdConfigCharacterRulesMaxRepeatinCharText",
				"entityPwdConfigCharacterRulesMaxRepeatinCharText_validation");
	}

	@Test
	public void PT6228_PasswordRules_NumberOfAllowedRepeatChars_changeTo999_withoutSaving() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMaxRepeatinCharText", "999");
	}

	@Test
	public void PT6229_PasswordRules_NumberDaysBeforePasswordExpires_cantBe0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigDaysBeforePasswordExpires",
				"entityPwdConfigDaysBeforePasswordExpires_validation",
				"Please enter a value greater than or equal to 1.", "0");
	}

	@Test
	public void PT6229_PasswordRules_NumberDaysBeforePasswordExpires_cantBeBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigDaysBeforePasswordExpires",
				"entityPwdConfigDaysBeforePasswordExpires_validation", "This field is required.", "");
	}

	@Test
	public void PT6229_PasswordRules_NumberDaysBeforePasswordExpires_NOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigDaysBeforePasswordExpires"),
				alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigDaysBeforePasswordExpires", "12");
	}

	@Test
	public void PT6229_PasswordRules_NumberDaysBeforePasswordExpires_onlyAllows2Chars() throws Throwable {
		String value = "1234";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigDaysBeforePasswordExpires"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigDaysBeforePasswordExpires", "12");
	}

	@Test
	public void PT6229_PasswordRules_NumberDaysBeforePasswordExpires_changeTo50_withoutSaving() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenPasswordField(testEntity,
				"entityPwdConfigDaysBeforePasswordExpires", "70");
	}

	@Test
	public void PT6229_PasswordRules_NumberDaysBeforePasswordExpires_changeTo50_andSaving() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField(testEntity,
				"entityPwdConfigDaysBeforePasswordExpires", "50");
	}

	@Test
	public void PT6230_PasswordRules_NumberOfDaysBeforePasswordReUse_cantBeLessThan180() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigPasswordReuse",
				"entityPwdConfigPasswordReuse_validation", "Please enter a value greater than or equal to 180.", "179");
	}

	@Test
	public void PT6230_PasswordRules_NumberOfDaysBeforePasswordReUse_cantBeBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigPasswordReuse",
				"entityPwdConfigPasswordReuse_validation", "This field is required.", "");
	}

	@Test
	public void PT6230_PasswordRules_NumberOfDaysBeforePasswordReUse_SpecialCharsNOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigPasswordReuse"), alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigPasswordReuse", "123");
	}

	@Test
	public void PT6230_PasswordRules_NumberOfDaysBeforePasswordReUse_onlyAllowsUpTo3Digits() throws Throwable {
		String value = "5678";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigPasswordReuse"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigPasswordReuse", "567");
	}

	@Test
	public void PT6230_PasswordRules_NumberOfDaysBeforePasswordReUse_changeTo190_withoutSaving() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenPasswordField(testEntity,
				"entityPwdConfigPasswordReuse", "190");
	}

	@Test
	public void PT6230_PasswordRules_NumberOfDaysBeforePasswordReUse_changeTo360_andSave() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField(testEntity, "entityPwdConfigPasswordReuse",
				"360");
	}

	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_cannotAllow0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinNumCharText",
				"entityPwdConfigCharacterRulesMinNumCharText_validation",
				"Please enter a value greater than or equal to 1.", "0");
	}

	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_cannotAllowBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinNumCharText",
				"entityPwdConfigCharacterRulesMinNumCharText_validation", "This field is required.", "");
	}

	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_SpecialCharsNOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinNumCharText"),
				alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinNumCharText", "123");
	}

	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_allowsOnly3Chars() throws Throwable {
		String value = "5678";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinNumCharText"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinNumCharText", "567");
	}

	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_changeTo2_andSave() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMinNumCharText", "2");
	}

	@Test
	public void PT6231_PasswordRules_NumberOfNumericChars_changeTo999_andCancel() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMinNumCharText", "999");
	}

	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_cannotAllow0() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinNumCharText",
				"entityPwdConfigCharacterRulesMinNumCharText_validation",
				"Please enter a value greater than or equal to 1.", "0");
	}

	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_cannotAllowBlank() throws Throwable {
		SecurityAdmin_EntityManageModal.verifyPasswordError("entityPwdConfigCharacterRulesMinSpecialCharText",
				"entityPwdConfigCharacterRulesMinSpecialCharText_validation", "This field is required.", "");
	}

	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_SpecialCharsNOTAllowed() throws Throwable {
		String alphaAndSpecialChars = "(abcd/*-+)";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinSpecialCharText"),
				alphaAndSpecialChars + "123");
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinSpecialCharText", "123");
	}

	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_allowsOnly3Chars() throws Throwable {
		String value = "5678";
		BaseUI.enterText(Locator.lookupElement("entityPwdConfigCharacterRulesMinSpecialCharText"), value);
		SecurityAdmin_EntityManageModal.verify_Textbox_Value("entityPwdConfigCharacterRulesMinSpecialCharText", "567");
	}

	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_changeTo2_andSave() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChange_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMinSpecialCharText", "2");
	}

	@Test
	public void PT6232_PasswordRules_NumberOfSpecialChars_changeTo999_andCancel() throws Exception {
		SecurityAdmin_EntityManageModal.verifyChangeCancelled_toGivenPasswordField(testEntity,
				"entityPwdConfigCharacterRulesMinSpecialCharText", "999");
	}

	@Test
	public void PT6233_PasswordRules_CanContainUsernameCheckbox_CheckBox_AndCancel() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingCancel_doesntSave(testEntity,
				"entityPwdConfigCanContainUsernameCheckBox");

	}

	@Test
	public void PT6233_PasswordRules_CanContainUsernameCheckbox_CheckBox_AndSave() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingSave_Saves(testEntity,
				"entityPwdConfigCanContainUsernameCheckBox");

	}

	@Test
	public void PT6234_PasswordRules_CanContainReversedUsernameCheckbox_CheckBox_AndCancel() throws Exception {
		SecurityAdmin_EntityManageModal.unCheckCheckBoxAndSaveOrCancel("entityPwdConfigCanContainReversedUsernameCheckBox", true);
		
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingCancel_doesntSave(testEntity,
				"entityPwdConfigCanContainReversedUsernameCheckBox");

	}

	@Test
	public void PT6234_PasswordRules_CanContainReversedUsernameCheckbox_CheckBox_AndSave() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingSave_Saves(testEntity,
				"entityPwdConfigCanContainReversedUsernameCheckBox");

	}

	@Test
	public void PT6235_PasswordRules_CanContainFirstNameCheckbox_CheckBox_AndCancel() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingCancel_doesntSave(testEntity,
				"entityPwdConfigCanContainUsernameFirstnameCheckBox");

	}

	@Test
	public void PT6235_PasswordRules_CanContainFirstNameCheckbox_CheckBox_AndSave() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingSave_Saves(testEntity,
				"entityPwdConfigCanContainUsernameFirstnameCheckBox");

	}

	@Test
	public void PT6236_PasswordRules_CanContainLastNameCheckbox_CheckBox_AndCancel() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingCancel_doesntSave(testEntity,
				"entityPwdConfigCanContainUsernameLastnameCheckBox");

	}

	@Test
	public void PT6236_PasswordRules_CanContainLastNameCheckbox_CheckBox_AndSave() throws Exception {
		SecurityAdmin_EntityManageModal.verify_checkingCheckboxAndClickingSave_Saves(testEntity,
				"entityPwdConfigCanContainUsernameLastnameCheckBox");

	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
//		if (setup_Pass == true) {
			// Re-launch the Edit Modal and take us back to the Passwords tab if
			// it
			// is not displayed.
			if (!Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
				SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
				SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
			} else {

			}
//		}
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if (Locator.lookupElement("entityEntityUpdateModal").isDisplayed()) {
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		}

		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
