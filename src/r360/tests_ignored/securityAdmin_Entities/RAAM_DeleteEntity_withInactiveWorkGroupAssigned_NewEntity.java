package r360.tests_ignored.securityAdmin_Entities;


import java.text.MessageFormat;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AssignWorkgroupsModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class RAAM_DeleteEntity_withInactiveWorkGroupAssigned_NewEntity extends BaseTest {

	private String entityToExpand = "AutomationTest";
	private String testEntity = "testEntity_NewEntity_InactiveWorkGroup";
	private String workgroupToMove = "TestInactiveWorkgroup_ToNewEntity";
	
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		EntitySelector.expandNode(entityToExpand);
		
		if(BaseUI.elementExists("entityTreeNode_ByText", testEntity, null))
		{
			Navigation.navigate_Admin_Workgroups();
			EntitySelector.expandNode(entityToExpand);
			EntitySelector.selectNode_Scroll(testEntity);
			Admin_Workgroups.remove_AllWorkgroupsFromEntity();
			
			Navigation.navigate_Admin_Entities();
			EntitySelector.expandNode(entityToExpand);
			SecurityAdministration.deleteEntity(testEntity);
		}
		
		EntitySelector.selectNode_Scroll(entityToExpand);
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Corporation");
		
		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(entityToExpand);
		EntitySelector.selectNode_Scroll(testEntity);
		
		Thread.sleep(5000);
		Admin_Workgroups.addDefaultWorkGroup_Settings("360", "360");
		
		//Deviated from TC here, added pre-existing workgroup.
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.move_fromAvailable_toSelected(workgroupToMove);
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();
		
		Pagination workGroupGrid = new Pagination(Admin_Workgroups.workGroupSettings_GridID);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		workGroupGrid.verifyCountOfEntries(0);
		Admin_Workgroups.verify_WorkGroup_ActiveOrInactive_ByEntityAndWorkgroup(testEntity, workgroupToMove, false);
		Navigation.navigate_Admin_Entities();
		EntitySelector.expandNode(entityToExpand);
		SecurityAdministration.deleteEntity_LaunchDeleteModal(testEntity);
	}

	
	
	@Test
	public void PT6191_DeleteModal_Appears_ErrorMessageCorrect() throws Throwable {
		String errorMessage = MessageFormat.format(
				"Entity {0} cannot be deleted because it has one or more resources associated with it.",
				testEntity);
		WebElement ErrorElement = Locator.lookupElement("entityDeleteModal_Error1");
		String webErrorMessage = ErrorElement.getText();
		BaseUI.verifyElementAppears(ErrorElement);
		BaseUI.baseStringCompare("Error Message", errorMessage, webErrorMessage);
		
	}
	
	@Test
	public void PT6191_DeleteModal_Appears_entityNameCorrect() throws Throwable {
		WebElement entityName = Locator.lookupElement("entityDeleteModal_EntityName");
		String entityFromSite =	entityName.getText();
		BaseUI.verifyElementAppears(entityName);
		BaseUI.baseStringCompare("Entity Name", testEntity, entityFromSite);
	}
	
	@Test
	public void PT6191_DeleteModal_Appears_applicationCorrect() throws Throwable {
		String expectedText = "Receivables360 Online\u2122";
		WebElement applicationName = Locator.lookupElement("entityDeleteModal_TableCellByHeader", "Application", null);
		String applicationFromSite =	applicationName.getText();
		BaseUI.verifyElementAppears(applicationName);
		BaseUI.baseStringCompare("Application Name", expectedText, applicationFromSite);
	}
	
	@Test
	public void PT6191_DeleteModal_Appears_ResourceTypeCorrect() throws Throwable {
	
		String expectedText = "Workgroup";
		WebElement resourceType = 	Locator.lookupElement("entityDeleteModal_TableCellByHeader", "Resource Type", null);
		String resourceTypeFromSite = resourceType.getText();
		BaseUI.verifyElementAppears(resourceType);
		BaseUI.baseStringCompare("Resource Type", expectedText, resourceTypeFromSite);
	}
	
	@Test
	public void PT6191_DeleteModal_Appears_ResourceName() throws Throwable {
		//Resource Name.  Will need to update if WorkGroup ever changes.
		String expectedText = "123456777 - TestInactiveWorkgroup_ToNewEntity";
		WebElement resourceName = Locator.lookupElement("entityDeleteModal_TableCellByHeader", "Resource Name", null);
		String entityFromSite =	resourceName.getText();
		BaseUI.verifyElementAppears(resourceName);
		BaseUI.baseStringCompare("Resource Name", expectedText, entityFromSite);
	}
	
	
	@Test
	public void PT6191_DeleteModal_DeleteButton_Disabled() throws Throwable {
		WebElement deleteButton = Locator.lookupElement("securityEntitiesDeleteModal_DeleteButton");
		BaseUI.verifyElementAppears(deleteButton);
		BaseUI.verifyElementDisabled(deleteButton);
	}
	
	@Test
	public void PT6191_DeleteModal_CancelButton_Displayed() throws Throwable {
		WebElement cancelButton = Locator.lookupElement("securityEntitiesDeleteModal_CancelButton");
		BaseUI.verifyElementAppears(cancelButton);
	}
	
	@Test
	public void PT6191_DeleteModal_Appears_DeleteHeader() throws Throwable {
		//Resource Name.  Will need to update if WorkGroup ever changes.
		String expectedText = "Entity Deletion";
		WebElement headerElement = Locator.lookupElement("entityDeleteModal_Header");
		String entityFromSite =	headerElement.getText();
		BaseUI.verifyElementAppears(headerElement);
		BaseUI.baseStringCompare("Header Element", expectedText, entityFromSite);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		if(Locator.lookupElement("entityDeleteModal").isDisplayed())
		{
			BaseUI.click(Locator.lookupElement("securityEntitiesDeleteModal_CancelButton"));
			Thread.sleep(1000);
		}
		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(entityToExpand);
		EntitySelector.selectNode(testEntity);
		Admin_Workgroups.remove_AllWorkgroupsFromEntity();
		
		Navigation.navigate_Admin_Entities();
		EntitySelector.expandNode(entityToExpand);
		SecurityAdministration.deleteEntity(testEntity);

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
