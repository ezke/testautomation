package r360.tests_ignored.paymentSource;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PaymentSource_UserPermission_ManagePaymentSource_Enabled extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_AutomationPermission, GlobalVariables.userName_allPermissions,
				GlobalVariables.passsword_allPermissions);

		Navigation.navigate_Admin_PaymentSource();
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT1411_UserPermission_ManagePaymentSource_AddButton_Enabled() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_AddButton"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("pymtSource_AddButton"));

	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT1411_UserPermission_ManagePaymentSource_EditLink_Appears() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_EditLink_ByShortName", "AutomationACH", null));

	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT1411_UserPermission_ManagePaymentSource_DeleteLink_Appears() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_DeleteLink_ByShortName", "AutomationACH", null));

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
