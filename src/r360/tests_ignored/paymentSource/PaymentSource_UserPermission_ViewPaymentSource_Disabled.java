package r360.tests_ignored.paymentSource;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PaymentSource_UserPermission_ViewPaymentSource_Disabled extends BaseTest {
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_viewPaymentSourcePageDisabled, GlobalVariables.userName_viewPaymentSourcePageDisabled,
				GlobalVariables.password_viewPaymentSourcePageDisabled);

		Navigation.navigate_openMenu("Admin");
	}
	
	@Test(groups = {"all_Tests", "smoke_tests"})
	public void PT1412_AdminMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	@Test(groups = {"all_Tests", "smoke_tests"})
	public void PT1412_Admin_ViewPaymentSource_Disabled() throws Throwable {
		BaseUI.verify_false_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Admin", "Payment Source"), 
				"Admin>Payment Source menu item is NOT listed.",
				"Admin>Payment Source menu item exists");
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}

}
