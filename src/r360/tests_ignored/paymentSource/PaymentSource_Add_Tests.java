package r360.tests_ignored.paymentSource;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSources;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PaymentSource_Add_Tests extends BaseTest {

	private static String shortName = "(AutoTest)";
	private static String description = "5689";
	private static Boolean isActive = true;
	private static Boolean saveOrCancel = true;
	private static String system = "DIT";
	private static String entityFI = "001 Customer A FI";

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_PaymentSource();
		PaymentSources.paymentSourceGrid.sort_Column_Ascending("Short Name");
		if (BaseUI.pageSourceContainsString(shortName)) {
			PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
		}
		
		PaymentSources.launch_AddPaymentSource_Modal();
	
	}

	@Test(groups = { "all_Tests" , "smoke_tests"})
	public static void PT9082_AddPaymentSource() throws Exception {
		PaymentSources.add_PaymentSource(shortName, description, isActive, system, entityFI, saveOrCancel);
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Short Name", shortName, true);
		PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
		
	}
	
	@Test(groups = { "all_Tests" , "smoke_tests"})
	public static void PT9082_AddPaymentSource_And_Cancel() throws Exception {
		PaymentSources.add_PaymentSource(shortName, description, isActive, system, entityFI, false);
		PaymentSources.paymentSourceGrid.sort_Column_Ascending("Short Name");
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Short Name", shortName, false);
	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9087_AddPaymentSource_ShortName_OnlyAllows_30_Characters() throws Exception {
		String shortNameGreaterThan30 = "verulogngcgaractertotestsutomdyeurtsyetyftye";
		WebElement element = Locator.lookupElement("pymtSource_ShortName_TextBox");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("pymtSource_ShortName_TextBox"), shortNameGreaterThan30);
		BaseUI.tabThroughField(element);
		String shortNameText= BaseUI.getTextFromInputBox(element);
		BaseUI.verify_true_AndLog(shortNameText.length()<=30, "ShortName length was less than or equal to 30", "ShortName length was not less than or equal to 30");

	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9087_AddPaymentSource_ShortName_Empty_Error() throws Exception {
		PaymentSources.add_PaymentSource("", description, isActive, system, entityFI, true);
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_ShortName_Error"));
	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9085_AddPaymentSource_Description_OnlyAllows_128_Characters() throws Exception {
		String descriptionGreaterThan128 = "verulogngcgaractertotestsutomdyeurtsyetyftyedghgssgteyshyeyaggastegshatwgtxvbcbnhsjgdyegywyewtgqygtywshaghjgahsagdhsaghgahgsahgashgdhsah";
		WebElement element = Locator.lookupElement("pymtSource_Description_TextBox");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("pymtSource_Description_TextBox"), descriptionGreaterThan128);
		BaseUI.tabThroughField(element);
		String descriptionText= BaseUI.getTextFromInputBox(element);
		BaseUI.verify_true_AndLog(descriptionText.length()<=128, "Description length was less than or equal to 128", "Description length was not less than or equal to 128");

	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9085_AddPaymentSource_Description_Empty_Error() throws Exception {
		PaymentSources.add_PaymentSource(shortName, "", isActive, system, entityFI, true);
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_Description_Error"));
	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9083_AddPaymentSource_Active_CheckBox() throws Exception {
		PaymentSources.add_PaymentSource(shortName, description, isActive, system, entityFI, saveOrCancel);
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_ActiveCheck_ByShortName", shortName, null));
	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9088_AddPaymentSource_SystemDropdown_ExpectedDropdown() throws Exception {
		String systemDropdown = "ImageRPS";
		PaymentSources.system_Dropdown.select_EntityValue(systemDropdown);
		PaymentSources.system_Dropdown.verify_SelectedDropdownValue(systemDropdown);
	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9086_AddPaymentSource_EntityFIDropdown_ExpectedDropdown() throws Exception {
		String entityFIDropdown = "AutomationTest";
		PaymentSources.entityFI_Dropdown.select_EntityValue(entityFIDropdown);
		PaymentSources.entityFI_Dropdown.verify_SelectedDropdownValue(entityFIDropdown);
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("pymtSource_AddModal", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("pymtSource_AddModal"))) {
			PaymentSources.cancel_PaymentSourceModal();
		}
		PaymentSources.launch_AddPaymentSource_Modal();

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			if (BaseUI.elementExists("pymtSource_AddModal", null, null)
					&& BaseUI.elementAppears(Locator.lookupElement("pymtSource_AddModal"))) {
				PaymentSources.cancel_PaymentSourceModal();
			}
			
			if (BaseUI.pageSourceContainsString(shortName)) {
				PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
			}
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
