package r360.tests_ignored.paymentSource;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSources;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PaymentSource_Edit_Tests extends BaseTest {
	private static String shortName = "(Auto_Edit)";
	private static String description = "test";
	private static Boolean isActive = true;
	private static Boolean saveOrCancel = true;
	private static String system = "DIT";
	private static String entityFI = "001 Customer A FI";

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_PaymentSource();
		PaymentSources.paymentSourceGrid.sort_Column_Ascending("Short Name");
		if (BaseUI.pageSourceContainsString(shortName)) {
			PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
		}
		PaymentSources.launch_AddPaymentSource_Modal();
		PaymentSources.add_PaymentSource(shortName, description, isActive, system, entityFI, saveOrCancel);

		PaymentSources.launch_EditPaymentSource_ForGivenShortName(shortName);
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9104_EditPaymentSource_ShortName() throws Exception {
		String shortNameText = BaseUI.getTextFromField(Locator.lookupElement("pymtSource_Edit_ShortName_TextBox"));
		String expectedShortNameText = shortName;
		BaseUI.baseStringCompare("ShortName", expectedShortNameText, shortNameText);
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9104_EditPaymentSource_AllFields() throws Throwable {
		String description = "5689";
		Boolean isActive = false;
		Boolean saveOrCancel = true;
		String system = "ImageRPS";
		String entityFI = "AutomationTest";
		PaymentSources.edit_PaymentSource(description, isActive, system, entityFI, saveOrCancel);
		PaymentSources.verify_EditIconList_Appears();
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Long Name", description, true);
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9106_EditPaymentSource_And_Cancel() throws Throwable {
		String description = "testingEdit";
		Boolean isActive = true;
		Boolean saveOrCancel = false;
		String system = "integraPAY";
		String entityFI = "Mosinee Bank";
		PaymentSources.edit_PaymentSource(description, isActive, system, entityFI, saveOrCancel);
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Long Name", description, false);
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9107_EditPaymentSource_Description_OnlyAllows_128_Characters() throws Exception {
		String descriptionGreaterThan128 = "verulogngcgaractertotestsutomdyeurtsyetyftyedghgssgteyshyeyaggastegshatwgtxvbcbnhsjgdyegywyewtgqygtywshaghjgahsagdhsaghgahgsahgashgdhsah";
		WebElement element = Locator.lookupElement("pymtSource_Edit_Description_TextBox");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("pymtSource_Edit_Description_TextBox"),
				descriptionGreaterThan128);
		BaseUI.tabThroughField(element);
		String descriptionText = BaseUI.getTextFromInputBox(element);
		BaseUI.verify_true_AndLog(descriptionText.length() <= 128, "Description length was less than or equal to 128",
				"Description length was not less than or equal to 128");

	}
	
	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9107_EditPaymentSource_Description_Empty_Error() throws Throwable {
		PaymentSources.edit_PaymentSource("", isActive, system, entityFI, saveOrCancel);
		BaseUI.verifyElementAppears(Locator.lookupElement("pymtSource_Description_Error"));
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9105_EditPaymentSource_Active_CheckBox() throws Exception {
		if (Locator.lookupElement("pymtSource_Active_CheckBox").isSelected()) {
			BaseUI.uncheckCheckbox("pymtSource_Active_CheckBox");
		} else {
			BaseUI.checkCheckbox("pymtSource_Active_CheckBox");
		}
		PaymentSources.save_PaymentSourceModal();
		PaymentSources.launch_EditPaymentSource_ForGivenShortName(shortName);
		if (Locator.lookupElement("pymtSource_Active_CheckBox").isSelected()) {
			BaseUI.verifyCheckboxStatus(Locator.lookupElement("pymtSource_Active_CheckBox"), true);
		} else {
			BaseUI.verifyCheckboxStatus(Locator.lookupElement("pymtSource_Active_CheckBox"), false);
		}

	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9110_EditPaymentSource_SystemDropdown_ExpectedDropdown() throws Exception {
		String systemDropdown = "ImageRPS";
		PaymentSources.system_Dropdown.select_EntityValue(systemDropdown);
		PaymentSources.system_Dropdown.verify_SelectedDropdownValue(systemDropdown);

	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9108_EditPaymentSource_EntityFIDropdown_ExpectedDropdown() throws Exception {
		String entityFIDropdown = "AutomationTest";
		PaymentSources.entityFI_Dropdown.select_EntityValue(entityFIDropdown);
		PaymentSources.entityFI_Dropdown.verify_SelectedDropdownValue(entityFIDropdown);

	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("pymtSource_AddModal", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("pymtSource_AddModal"))) {
			PaymentSources.cancel_PaymentSourceModal();
		}
		PaymentSources.launch_EditPaymentSource_ForGivenShortName(shortName);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Throwable {
		try {

			if (BaseUI.elementExists("pymtSource_AddModal", null, null)
					&& BaseUI.elementAppears(Locator.lookupElement("pymtSource_AddModal"))) {
				PaymentSources.cancel_PaymentSourceModal();
			}

			if (BaseUI.pageSourceContainsString(shortName)) {
				PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
			}

			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
