package r360.tests_ignored.paymentSource;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSources;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PaymentSource_Delete_Tests extends BaseTest {
	private static String shortName = "(Auto_Delete_PymtSource)";
	private static String description = "delete";
	private static Boolean isActive = true;
	private static Boolean saveOrCancel = true;
	private static String system = "DIT";
	private static String entityFI = "001 Customer A FI";

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_PaymentSource();
		PaymentSources.paymentSourceGrid.sort_Column_Ascending("Short Name");
		if (BaseUI.pageSourceContainsString(shortName)) {
			PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
		}

	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9093_Delete_PaymentSource_ForGivenShortName() throws Throwable {
		PaymentSources.launch_AddPaymentSource_Modal();
		PaymentSources.add_PaymentSource(shortName, description, isActive, system, entityFI, saveOrCancel);
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Short Name", shortName, true);
		PaymentSources.verify_DeleteIconLinkList_Appears();
		PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Short Name", shortName, false);
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9094_Delete_PaymentSourceBatch_MultipleBatches_Error() throws Throwable {
		String shortNameMultipleBatches = "ACH-I1001";
		PaymentSources.delete_PaymentSource_ForGivenFieldName(shortNameMultipleBatches);
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		Navigation.close_ToastError_If_ItAppears();
	}

	@Test(groups = { "all_Tests", "smoke_tests" })
	public static void PT9095_Delete_PaymentSource_And_Cancel() throws Exception {
		PaymentSources.launch_AddPaymentSource_Modal();
		PaymentSources.add_PaymentSource(shortName, description, isActive, system, entityFI, saveOrCancel);
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Short Name", shortName, true);
		PaymentSources.launch_DeletePaymentSource_ForGivenShortName(shortName);
		PaymentSources.cancel_Delete_PaymentSource_Modal();
		PaymentSources.paymentSourceGrid.verifyEntryExists_OR_DoesntExist("Short Name", shortName, true);
		PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("pymtSource_AddModal", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("pymtSource_AddModal"))) {
			PaymentSources.cancel_PaymentSourceModal();
		}

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			if (BaseUI.elementExists("pymtSource_AddModal", null, null)
					&& BaseUI.elementAppears(Locator.lookupElement("pymtSource_AddModal"))) {
				PaymentSources.cancel_PaymentSourceModal();
			}

			if (BaseUI.pageSourceContainsString(shortName)) {
				PaymentSources.delete_PaymentSource_ForGivenFieldName(shortName);
			}
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
