package r360.tests_ignored.misc;

import java.text.MessageFormat;
import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import r360.data.Config_DataRetrieval;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_tests extends BaseTest {

	TableData results;

	private String workgroup = "";
	private String date1 = "";
	private String date2 = "";

	@BeforeClass
	public void setup_method() throws Exception {
		if (Browser.driver == null) {
			
			Config_DataRetrieval.get_ConfigData();
			Config_DataRetrieval.retrieve_DataSheet(Config_DataRetrieval.config_Data.get("fileName"));

			// do formatting on our table so that they'll match later on.
			Config_DataRetrieval.tableData.replace_DateFormat_shortened();
			Config_DataRetrieval.tableData.remove_LeadingZeros_fromNumbers();
			Config_DataRetrieval.tableData.replace_Null_With_Empty();
			Config_DataRetrieval.tableData.remove_TrailingZeros_fromNumbers();
			Config_DataRetrieval.tableData.remove_Character("\"");
			workgroup = Config_DataRetrieval.config_Data.get("workGroup");
			date1 = Config_DataRetrieval.config_Data.get("date1");
			date2 = Config_DataRetrieval.config_Data.get("date2");

			
			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);

			for (String key : Config_DataRetrieval.config_Data.keySet()) {
				if (key.startsWith("dataVariable")) {
					AdvancedSearch.addDisplayField(Config_DataRetrieval.config_Data.get(key));
				}
			}
			// AdvancedSearch.addDisplayField("Account Number");
			// AdvancedSearch.addDisplayField("R/T");
			// AdvancedSearch.addDisplayField("[INV].Amount");
			AdvancedSearch.click_Search();

			AdvancedSearch.select_ShowRowsDropdownValue(100);

			// Extract table data.
			results = AdvancedSearch.return_SearchResults();

			BaseUI.verify_true_AndLog(results.data.size() == Config_DataRetrieval.tableData.data.size(),
					"Search results list size matched expected results list.",
					MessageFormat.format(
							"Search results list size {0} did NOT match expected results list of size {1}.",
							results.data.size(), Config_DataRetrieval.tableData.data.size()));
		}
	}

	@DataProvider(name = "testData")
	public Object[][] createData() {
		Object[][] linksToNavigate = new Object[Config_DataRetrieval.tableData.rowCount()][2];

		for (int i = 0; i < Config_DataRetrieval.tableData.rowCount(); i++) {
			linksToNavigate[i][0] = i;
			linksToNavigate[i][1] = Config_DataRetrieval.tableData.data.get(i);
		}
		return linksToNavigate;
	}

	@Test(dataProvider = "testData", groups = {"all_Tests"})
	public void TC_AdvancedSearch_TestingResults(int rowNumber, HashMap<String, String> tableRow) throws Exception {

		Boolean testPass = true;
		String output = "";

		HashMap<String, String> resultRow = results.dataRow(rowNumber);
		for (String key : tableRow.keySet()) {
			if (!tableRow.get(key).equals(resultRow.get(key))) {
				testPass = false;
				output += System.lineSeparator()
						+ MessageFormat.format("For field {0}, expected value {1}, but seeing {2} on website.", key,
								tableRow.get(key), resultRow.get(key));
			}
		}

		BaseUI.verify_true_AndLog(testPass, MessageFormat.format("Line {0} passed.", rowNumber), output);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
