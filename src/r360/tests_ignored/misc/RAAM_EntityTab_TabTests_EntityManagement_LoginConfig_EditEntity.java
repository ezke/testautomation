package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityTab_TabTests_EntityManagement_LoginConfig_EditEntity extends BaseTest {

	String testEntity = "automation_TabTestEntity";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}
		
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_TabOnce_toInactiveDays() throws Throwable {
		// Reset modal
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("testEntity_ShouldNotSave", "Financial Institution");
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();

		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigInactiveDaysText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromInactiveDays_toMaxLoginAttempts() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigInactiveDaysText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigMaxLoginAttemptText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromMaxLoginAttempts_toMiniumumLength() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigMaxLoginAttemptText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigCharacterRulesMinLengthText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromMiniumumLength_toMinimumAlphaChars()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigCharacterRulesMinLengthText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigCharacterRulesMinAlphaCharText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromMinimumAlphaChars_toMinimumNumChars()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigCharacterRulesMinAlphaCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigCharacterRulesMinNumCharText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromMinimumNumChars_toMinimumSpecialChars()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigCharacterRulesMinNumCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigCharacterRulesMinSpecialCharText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromMinimumSpecialChars_toEnableRestrictedLoginNamesCheckbox()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigCharacterRulesMinSpecialCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigEnableRestrictedLoginNames"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromEnableRestrictedLoginNamesCheckbox_toSaveButton()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigEnableRestrictedLoginNames"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigSaveButton"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromSaveButton_toCancelButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityLoginConfigSaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityLoginConfigCancelButton"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromEntityInfoTab_toPasswordConfigTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_EntityInfoTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_PasswordConfigTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6264_SecurityAdmin_EditEntity_LoginConfig_fromPasswordConfigTab_toLoginConfigTab()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_PasswordConfigTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_LoginConfigTab"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		if(Locator.lookupElement("entityEntityUpdateModal").isDisplayed())
		{
			SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
			
		}
		
		// Remove entity if it exists.
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			SecurityAdministration.deleteEntity(testEntity);
		}
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
