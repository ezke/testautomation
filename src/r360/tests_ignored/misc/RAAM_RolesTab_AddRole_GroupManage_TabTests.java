package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_RolesTab_AddRole_GroupManage_TabTests extends BaseTest {

	private String parentEntity = "AutomationTest";
	private String testEntity = "autoGroups_UserAndPolicy";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Roles();

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.launchAddRoleModal();
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields("stuff", "", "Framework");
		SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromAvailableSelectAll_toAvailableNameFilter()
			throws Throwable {
		BaseUI.tabThroughField(SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.return_AllCheckbox());
		BaseUI.verifyElementHasFocus(
				SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.filter_textbox_ByColumnName("Name"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_throughAvailableTable() throws Throwable {
		SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.verifytTabFromFirstCheckboxToEndOfCheckboxes();
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromMove_toRemove() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_GrpManage_MoveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_GrpManage_RemoveButton"));

	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromRemove_toAssociatedSelectAll() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_GrpManage_RemoveButton"));
		BaseUI.verifyElementHasFocus(
				SecurityAdmin_RolesManagementModal.groupManage_AssociatedGroups.return_AllCheckbox());
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromAssociatedSelectAll_toAssociatedNameFilter()
			throws Throwable {
		BaseUI.tabThroughField(SecurityAdmin_RolesManagementModal.groupManage_AssociatedGroups.return_AllCheckbox());
		BaseUI.verifyElementHasFocus(
				SecurityAdmin_RolesManagementModal.groupManage_AssociatedGroups.filter_textbox_ByColumnName("Name"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_throughAssociatedTable() throws Throwable {
		if (SecurityAdmin_RolesManagementModal.groupManage_AssociatedGroups.get_visible_entry_Count().equals(0)) {
			WebElement firstCheckbox = SecurityAdmin_RolesManagementModal.groupManage_AvailableGroups.first_Checkbox();
			BaseUI.checkCheckbox(firstCheckbox);
			SecurityAdmin_RolesManagementModal.move_selectedGroups_FromAvailableGrid();
		}
		SecurityAdmin_RolesManagementModal.groupManage_AssociatedGroups.verifytTabFromFirstCheckboxToEndOfCheckboxes();
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromSave_toCancel() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_CancelButton"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromRoleInformationTab_toGroupManagementTab()
			throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_Nav_RoleInformationTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_Nav_GroupsTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6650_SecurityAdmin_Roles_GroupManage_fromGroupManagementTab_toPolicyManagement() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_Nav_GroupsTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_Nav_PolicyTab"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (!Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed()) {
			SecurityAdministration.launchAddRoleModal();
			SecurityAdmin_RolesManagementModal.navigate_GroupManagementTab();
		}
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			if (Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed()) {
				SecurityAdmin_RolesManagementModal.click_Cancel();
			}
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
