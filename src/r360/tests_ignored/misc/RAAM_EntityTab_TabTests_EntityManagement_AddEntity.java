package r360.tests_ignored.misc;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityTab_TabTests_EntityManagement_AddEntity extends BaseTest {

String testEntity = "automation_TabTestEntity";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, 
				GlobalVariables.password);
		
		Navigation.navigate_Admin_Entities();

		SecurityAdministration.launchAddEntityModal();
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_defaultsTo_EntityName() throws Throwable {
		//Reset modal so we can see default value.
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		SecurityAdministration.launchAddEntityModal();
		
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoNameText"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromEntityName_toEntityType() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoNameText"));
		BaseUI.enterKey(Locator.lookupElement("entityEntityInfoEntityTypeDropdown"), Keys.RETURN);
		Thread.sleep(300);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityEntityInfoEntityTypeDropdownList"));
		BaseUI.click(Locator.lookupElement("entityEntityInfoEntityTypeDropdown_ListItem_ByText", "Financial Institution", null));
		Thread.sleep(100);
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromEntityType_toEntityDescription() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoNameText"));
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoDescriptionText"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromEntityDescription_toExternalEntityID() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoDescriptionText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoExtEntityIdText"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromExternalEntityID_toReceivablesOnlineCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoExtEntityIdText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoSubAppR360Checkbox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromReceivablesOnlineCheckbox_toStandardPasswordCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoSubAppR360Checkbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromStandardPasswordCheckbox_toSingleSignOnCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromSingleSignOnCheckbox_toActivateCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoActiveCheckbox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromActivateCheckbox_toSaveButton() throws Throwable {
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("testEntity_ShouldNotSave", "Financial Institution");
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoActiveCheckbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoSaveButton"));
	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6184_SecurityAdmin_AddEntity_fromSaveButton_toCancelButton() throws Throwable {
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("testEntity_ShouldNotSave", "Financial Institution");
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoSaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoCancelButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_AddEntity_fromEntityInfoTab_toPasswordConfigTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_EntityInfoTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_PasswordConfigTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_AddEntity_fromPasswordConfigTab_toLoginConfigTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_PasswordConfigTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_LoginConfigTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_AddEntity_fromLoginConfigTab_toEntityName() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_LoginConfigTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfoNameText"));
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			if(Locator.lookupElement("entityEntityUpdateModal").isDisplayed())
			{
				SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
			}
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
