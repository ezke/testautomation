package r360.tests_ignored.misc;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;

public class Add_Things_ToEntities extends BaseTest {

	String columnName = "Name";
	String parentEntity = "AutomationTest";
	String entityName = "testEntity_WithLotsOfStuff";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.defaultBrowser = "firefox";
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		
	}

	@Test(groups = {"all_Tests"})
	public void Add_Users() throws Throwable {
		Navigation.navigate_Admin_Users();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(entityName);
		for (Integer i = 0; i < 10; i++) {
			SecurityAdministration.launchAddUserModal();
			SecurityAdmin_UserManageModal.addUser("seconduserB" + i.toString(), "Wausau#1", "test" + i.toString(), 
					"user"+ i.toString(), "testuser" + i.toString() + "@testuser.com");
		}
	}
	
	@Test(groups = {"all_Tests"})
	public void Add_Roles() throws Throwable {
		Navigation.navigate_Admin_Roles();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(entityName);
		for (Integer i = 0; i < 9; i++) {
			SecurityAdministration.launchAddRoleModal();
			SecurityAdmin_RolesManagementModal.addRole("secondsetB" + i.toString(), "Role Description " + i.toString(), "Framework");
		}
	}
	
	@Test(groups = {"all_Tests"})
	public void Add_Groups() throws Throwable {
		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(entityName);
		for (Integer i = 0; i < 9; i++) {
			SecurityAdministration.launchAddGroupsModal();
			SecurityAdmin_GroupsManagementModal.addGroup("groupNumberA" + i.toString());
		}
	}
	
	@Test(groups = {"all_Tests"})
	public void Sign_In_SignOut_Repeatedly() throws Throwable
	{
		Navigation.signOut();
		Browser.navigateTo(GlobalVariables.baseURL);
		
		for(int i =0; i < 100; i++)
		{
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
			Navigation.signOut();
			Browser.navigateTo(GlobalVariables.baseURL);
		}
		
		
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		Browser.closeBrowser();
	}
}
