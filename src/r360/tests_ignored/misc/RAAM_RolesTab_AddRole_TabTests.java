package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class RAAM_RolesTab_AddRole_TabTests extends BaseTest {

	private String parentEntity = "AutomationTest";
	private String testEntity = "autoGroups_UserAndPolicy";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Roles();

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);

		SecurityAdministration.launchAddRoleModal();
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_RolesManagement_RoleName_HasDefaultFocus() throws Throwable {
		SecurityAdmin_RolesManagementModal.click_Cancel();
		SecurityAdministration.launchAddRoleModal();
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.verify_true_AndLog(focusedElement.equals(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt")), "Focused element was Role Name Text field.", 
				"Focused element was NOT Role Name Text field.");
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromRoleName_toDescription() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleDescTxt"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromDescription_toApplication() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleDescTxt"));
		Thread.sleep(500);
		SecurityAdmin_RolesManagementModal.appDropdwn.verify_Dropdown_HasFocus();
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromApplication_toSave() throws Throwable {
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields("stuff", "", "Framework");
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleDescTxt"));
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromSave_toCancel() throws Throwable {
		SecurityAdmin_RolesManagementModal.enter_required_RoleFields("stuff", "", "Framework");
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_RolesMdl_CancelButton"));
	}

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (!Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed()) {
			SecurityAdministration.launchAddRoleModal();
		}
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			if(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleUpdateModal").isDisplayed())
			{
				SecurityAdmin_RolesManagementModal.click_Cancel();
			}
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
