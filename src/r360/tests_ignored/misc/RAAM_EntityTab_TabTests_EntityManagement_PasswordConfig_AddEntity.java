package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityTab_TabTests_EntityManagement_PasswordConfig_AddEntity extends BaseTest {

String testEntity = "automation_TabTestEntity";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, 
				GlobalVariables.password);
		
		Navigation.navigate_Admin_Entities();

		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("testEntity_ShouldNotSave", "Financial Institution");
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_TabOnce_toLoginConfigTab() throws Throwable {
		//Reset modal
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.enter_EntityRequiredInfo("testEntity_ShouldNotSave", "Financial Institution");
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_LoginConfigTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromLoginConfigTab_toDaysBeforePWordExpire() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_LoginConfigTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigDaysBeforePasswordExpires"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromDaysBeforePWordExpire_toDaysBeforePWordReuse() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigDaysBeforePasswordExpires"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigPasswordReuse"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromDaysBeforePWordReuse_toCanContainUserNameCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigPasswordReuse"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCanContainUsernameCheckBox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromCanContainUserNameCheckbox_toCanContainReverseUserNameCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCanContainUsernameCheckBox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCanContainReversedUsernameCheckBox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromCanContainReverseUserNameCheckbox_toCanContainUsersFirstName() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCanContainReversedUsernameCheckBox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCanContainUsernameFirstnameCheckBox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromCanContainUsersFirstName_toCanContainUsersLastName() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCanContainUsernameFirstnameCheckBox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCanContainUsernameLastnameCheckBox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromCanContainUsersLastName_toMinimumLength() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCanContainUsernameLastnameCheckBox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCharacterRulesMinLengthText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromMinimumLength_toMaximumRepeatingChars() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMinLengthText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCharacterRulesMaxRepeatinCharText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromMaximumRepeatingChars_toMinimumNumericChars() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMaxRepeatinCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCharacterRulesMinNumCharText"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromMinimumNumericChars_MinimumSpecialChars() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMinNumCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCharacterRulesMinSpecialCharText"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromMinimumSpecialChars_MinimumUpperCaseChars() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMinSpecialCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromMinimumUpperCaseChars_toMinimumLowerCase() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMinUppercaseCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCharacterRulesMinLowercaseCharText"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromMinimumLowerCase_toSaveButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigCharacterRulesMinLowercaseCharText"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigSaveButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromSaveButton_toCancelButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityPwdConfigSaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityPwdConfigCancelButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromEntityInfoTab_toPasswordConfigTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_EntityInfoTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_PasswordConfigTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6265_SecurityAdmin_AddEntity_PasswordConfig_fromPasswordConfigTab_toLoginConfigTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfo_PasswordConfigTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEntityInfo_LoginConfigTab"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		
		try {
			if(Locator.lookupElement("entityEntityUpdateModal").isDisplayed())
			{
				SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
			}
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
