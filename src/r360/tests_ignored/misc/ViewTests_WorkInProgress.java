package r360.tests_ignored.misc;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;

public class ViewTests_WorkInProgress extends BaseTest {

	String columnName = "Name";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		//Navigation.navigate_Admin_Users();
		//All tests should start at the same starting point.
		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.selectNode("mrc");
	}
	
	@Test
	public void TC_ShowAllOption() throws Throwable {
		SecurityAdministration.groupsGrid.verify_showAllOption(columnName);
	}
	
	@Test
	public void TC_ShowAutoOption() throws Throwable {
		SecurityAdministration.groupsGrid.verify_showAutoOption(columnName);
		SecurityAdministration.groupsGrid.verify_navigation_lists_DONT_Match(columnName);
	}
	
	@Test
	public void TC_Show10Option() throws Throwable {
		SecurityAdministration.groupsGrid.verify_show10Option(columnName);
		SecurityAdministration.groupsGrid.verify_navigation_lists_DONT_Match(columnName);
	}
	
	@Test
	public void TC_Show25Option() throws Throwable {
		SecurityAdministration.groupsGrid.verify_show25Option(columnName);
		SecurityAdministration.groupsGrid.verify_navigation_lists_DONT_Match(columnName);
	}
	
	@Test
	public void TC_Show50Option() throws Throwable {
		SecurityAdministration.groupsGrid.verify_show50Option(columnName);
		SecurityAdministration.groupsGrid.verify_navigation_lists_DONT_Match(columnName);
	}

	@Test
	public void TC_Show100Option() throws Throwable {
		SecurityAdministration.groupsGrid.verify_show100Option(columnName);
		SecurityAdministration.groupsGrid.verify_navigation_lists_DONT_Match(columnName);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdministration.groupsGrid.show_10();
		SecurityAdministration.groupsGrid.click_first();
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
