package r360.tests_ignored.misc;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_TabTests extends BaseTest {

	
	Pagination userManagementGrid;
	Pagination availableGroupsGrid;
	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	String userName = "AutomationTest";


	@BeforeMethod(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		userManagementGrid = new Pagination("userGrid");
//		SecurityAdministration.selectNode(nodeText);

	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_tabToDashboardMenu() throws Throwable {
		Navigation.navigate_Admin_Users();
		WebElement focusedElement =   Browser.driver.switchTo().activeElement();
		focusedElement.sendKeys(Keys.TAB);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromDashboardMenu_toBatchSummaryMenu() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromBatchSummary_toDDASummary() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromDDASummary_toExceptions() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromExceptions_toSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Search", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromSearch_toReports() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Search", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromReports_toNotifications() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromNotifications_toAdmin() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromAdmin_toSignOUtLink() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Admin", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_SignOut"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromSignOutLink_toClickHereToSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_SignOut"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityTreeNode_SearchBar"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromClickHereToSearch_toFirstEntityInList() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityTreeNode_SearchBar"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityTreeNode_FirstNode"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromFirstEntityInList_toEntityTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityTreeNode_FirstNode"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_EntitiesTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromEntityTabButton_toUsersTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_EntitiesTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_UsersTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromUsersTabButton_toRolesTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_UsersTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_RolesTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromRolesTabButton_toGroupsTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_RolesTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_GroupsTab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromGroupsTabButton_toAddButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_GroupsTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserAddButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromAddButton_toDeleteButton() throws Throwable {
		
		BaseUI.checkCheckbox(userManagementGrid.check_box_for_GivenColumnAndText("Login Name","AutoTest1"));
		BaseUI.tabThroughField(Locator.lookupElement("entityUserAddButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserDeleteButton"));
		BaseUI.uncheckCheckbox(userManagementGrid.check_box_for_GivenColumnAndText("Login Name","AutoTest1"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromSelectAllCheckbox_toLoginNameFilter() throws Throwable {
		
		BaseUI.tabThroughField(Locator.lookupElement("Pagin_header_checkbox"));
		BaseUI.verifyElementHasFocus(userManagementGrid.filter_textbox_ByColumnName("Login Name"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromLoginNameFilter_toLastNameFilter() throws Throwable {
		
		BaseUI.tabThroughField(userManagementGrid.filter_textbox_ByColumnName("Login Name"));
		BaseUI.verifyElementHasFocus(userManagementGrid.filter_textbox_ByColumnName("Last Name"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromLastNameFilter_toFirstNameFilter() throws Throwable {
		
		BaseUI.tabThroughField(userManagementGrid.filter_textbox_ByColumnName("Last Name"));
		BaseUI.verifyElementHasFocus(userManagementGrid.filter_textbox_ByColumnName("First Name"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromFirstNameFilter_toEmailAddressFilter() throws Throwable {
		
		BaseUI.tabThroughField(userManagementGrid.filter_textbox_ByColumnName("First Name"));
		BaseUI.verifyElementHasFocus(userManagementGrid.filter_textbox_ByColumnName("Email Address"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromEmailAddressFilter_toFirstUserCheckbox() throws Throwable {
		
		BaseUI.tabThroughField(userManagementGrid.filter_textbox_ByColumnName("Email Address"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Pagin_firstTable_checkbox", userManagementGrid.gridID, null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromFirstUserCheckbox_toFirstUserEditButton() throws Throwable {
		
		BaseUI.tabThroughField(Locator.lookupElement("Pagin_firstTable_checkbox", userManagementGrid.gridID, null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Pagin_firstTable_editButton", userManagementGrid.gridID, null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_fromFirstUserCheckbox_throughAllCheckboxesAndEditLinks() throws Throwable {

		userManagementGrid.verifytTabFromFirstCheckboxToEndOfCheckboxes_withEditUpdateLink();
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
