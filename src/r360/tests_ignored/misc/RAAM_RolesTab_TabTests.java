package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_RolesManagementModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_RolesTab_TabTests extends BaseTest {

	private String parentEntity = "AutomationTest";
	private String testEntity = "autoGroups_UserAndPolicy";
	private String testRole = "testRole_ForTabTests";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Roles();

		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		
		if(!BaseUI.pageSourceContainsString(testRole))
		{
			SecurityAdministration.launchAddRoleModal();
			SecurityAdmin_RolesManagementModal.addRole(testRole, "", "Framework");
		}
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromAdminRolesNavigation_toDashboardLink() throws Throwable {
		Navigation.navigate_Admin_Roles();
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromDashboardMenu_toBatchSummaryMenu() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromBatchSummary_toDDASummary() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromDDASummary_toExceptions() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromExceptions_toSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Search", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromSearch_toReports() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Search", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromReports_toNotifications() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromNotifications_toAdmin() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromAdmin_toSignOUtLink() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Admin", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_SignOut"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromSignOutLink_toClickHereToSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_SignOut"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityTreeNode_SearchBar"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromClickHereToSearch_toFirstEntityInList() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityTreeNode_SearchBar"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityTreeNode_FirstNode"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromFirstEntityInList_toEntityTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityTreeNode_FirstNode"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_EntitiesTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromEntityTabButton_toUsersTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_EntitiesTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_UsersTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromUsersTabButton_toRolesTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_UsersTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_RolesTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromRolesTabButton_toGroupsTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_RolesTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_GroupsTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromGroupsTabButton_toAddButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_GroupsTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_Roles_AddButton"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromAddButton_toDeleteButton() throws Throwable {
		SecurityAdministration.roleGrid.check_Checkbox_ForColumnName_AndText("Name", testRole);
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_Roles_AddButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("scrtyAdmin_Roles_DeleteButton"));
	}

	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromDeleteButton_toCheckAllCheckbox() throws Throwable {
		SecurityAdministration.roleGrid.check_Checkbox_ForColumnName_AndText("Name", testRole);
		BaseUI.tabThroughField(Locator.lookupElement("scrtyAdmin_Roles_DeleteButton"));
		BaseUI.verifyElementHasFocus(SecurityAdministration.roleGrid.return_AllCheckbox());
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromCheckAllCheckbox_toNameFilterTextBox() throws Throwable {
		BaseUI.tabThroughField(SecurityAdministration.roleGrid.return_AllCheckbox());
		BaseUI.verifyElementHasFocus(SecurityAdministration.roleGrid.filter_textbox_ByColumnName("Name"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromNameFilterTextBox_toDescriptionFilterTextBox() throws Throwable {
		BaseUI.tabThroughField(SecurityAdministration.roleGrid.filter_textbox_ByColumnName("Name"));
		BaseUI.verifyElementHasFocus(SecurityAdministration.roleGrid.filter_textbox_ByColumnName("Description"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromDescriptionFilterTextBox_toFirstUserCheckbox() throws Throwable {
		
		BaseUI.tabThroughField(SecurityAdministration.roleGrid.filter_textbox_ByColumnName("Description"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Pagin_firstTable_checkbox", SecurityAdministration.roleGrid.gridID, null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromFirstUserCheckbox_toFirstUserEditButton() throws Throwable {
		
		BaseUI.tabThroughField(Locator.lookupElement("Pagin_firstTable_checkbox", SecurityAdministration.roleGrid.gridID, null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Pagin_firstTable_editButton", SecurityAdministration.roleGrid.gridID, null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6649_SecurityAdmin_Roles_fromFirstUserCheckbox_throughAllCheckboxesAndEditLinks() throws Throwable {

		SecurityAdministration.roleGrid.verifytTabFromFirstCheckboxToEndOfCheckboxes_withEditUpdateLink();
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
