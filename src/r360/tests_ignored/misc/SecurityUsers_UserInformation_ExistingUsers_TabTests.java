package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_UserInformation_ExistingUsers_TabTests extends BaseTest {

	
	Pagination userManagementGrid;
	Pagination availableGroupsGrid;
	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	String userName = "AutomationTest";


	@BeforeMethod(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);

	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_defaults_toLoginNameTextbox() throws Throwable {
		Thread.sleep(2000);
		SecurityAdmin_UserManageModal.click_cancelButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		Thread.sleep(2000);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoLoginName"));
		
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromLoginName_toFirstName() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoLoginName"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoFirstName"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromFirstName_toMiddleInitial() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoFirstName"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoMiddleName"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromMiddleInitial_toLastName() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoMiddleName"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoLastName"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromLastName_toEmail() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoLastName"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoEmail"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromEmail_toPasswordField() throws Throwable {
		WebElement passwordTextbox = Locator.lookupElement("entityUserInfoPasswordTextbox");
		if(!passwordTextbox.isEnabled())
		{
			BaseUI.click(Locator.lookupElement("entityUserInfoUpdatePasswordButton"));
			Thread.sleep(100);
		}

		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoEmail"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoPasswordTextbox"));
	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromPasswordField_toConfirmPassword() throws Throwable {
	
		
		SecurityAdmin_UserManageModal.click_cancelButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		Thread.sleep(2000);
		BaseUI.click(Locator.lookupElement("entityUserInfoUpdatePasswordButton"));
		Thread.sleep(1000);
		
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoEmail"));
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoPasswordTextbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoConfirmPasswordTextbox"));

	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromConfirmPassword_toActiveCheckbox() throws Throwable {
		WebElement confirm_passwordTextbox = Locator.lookupElement("entityUserInfoConfirmPasswordTextbox");
		if (!confirm_passwordTextbox.isEnabled()) {
			BaseUI.click(Locator.lookupElement("entityUserInfoUpdatePasswordButton"));
			Thread.sleep(250);
		}

		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoConfirmPasswordTextbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoActiveCheckbox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromActiveCheckbox_toAccountLockedCheckbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoActiveCheckbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoAccountLockedCheckbox"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromAccountLockedCheckbox_toChangePasswordAtLoginCheckbox() throws Throwable {
		WebElement changePasswordCheckbox = Locator.lookupElement("entityUserInfoChangePassword_AtNextLogin");
		if(!changePasswordCheckbox.isEnabled())
		{
			SecurityAdmin_UserManageModal.click_cancelButton();
			SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		}
		
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoAccountLockedCheckbox"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoChangePassword_AtNextLogin"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromChangePasswordAtLoginCheckbox_toUpdatePasswordButton() throws Throwable {
		WebElement changePasswordCheckbox = Locator.lookupElement("entityUserInfoChangePassword_AtNextLogin");
		if(!changePasswordCheckbox.isEnabled())
		{
			SecurityAdmin_UserManageModal.click_cancelButton();
			SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		}
		
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoChangePassword_AtNextLogin"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserInfoUpdatePasswordButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromUpdatePasswordButton_toSaveButton() throws Throwable {
		SecurityAdmin_UserManageModal.click_cancelButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		Thread.sleep(2000);
		
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoUpdatePasswordButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEdit_SaveButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromSaveButton_toCancelButton() throws Throwable {
		SecurityAdmin_UserManageModal.click_cancelButton();
		SecurityAdministration.launchEditUserModal_forGivenUser(userName);
		Thread.sleep(2000);
		
		BaseUI.tabThroughField(Locator.lookupElement("entityUserEdit_SaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEdit_CancelButton"));
	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6797_SecurityUser_UserInfo_focus_fromUserInfoTab_toGroupManageTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserEditUserInfo_TabLink_Active"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEditGroupManage_TabLink"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {

		ResultWriter.checkForFailureAndScreenshot(result);
		
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
