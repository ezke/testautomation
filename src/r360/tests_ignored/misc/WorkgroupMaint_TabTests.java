package r360.tests_ignored.misc;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class WorkgroupMaint_TabTests extends BaseTest {
	public static final String workGroupSettings_GridID = "workgroupMaintenance";
	public static Pagination workGroupSettings_GRID = new Pagination(workGroupSettings_GridID);
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		SecurityAdministration.expandNode(testEntityWithChildrenNodes);
		SecurityAdministration.selectNode(testEntity);
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_FromWorkgroupsNavigation_toDashboardLink() throws Throwable {
		Navigation.navigate_Admin_Workgroups();
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
	}
					
//	@Test(groups = {"all_tests", "tab_tests"})
//	public void PT1286_WorkgroupMaint_fromDashboardMenu_toBatchSummaryMenu() throws Throwable {
//		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
//		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
//	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromBatchSummary_toDDASummary() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromDDASummary_toExceptions() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromExceptions_toSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Search", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromSearch_toReports() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Search", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromReports_toNotifications() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromNotifications_toAdmin() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromAdmin_toSignOutLink() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Admin", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_SignOut"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromSignOutLink_toRefreshButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_SignOut"));
		BaseUI.verifyElementHasFocus(Locator.findElement(By.xpath("//div[contains(@class, 'titleRefreshIcon')]//a[./*[@class='fa fa-refresh']]")));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromRefreshButton_toEntityWorkgroupDefaultsTab() throws Throwable {
		BaseUI.tabThroughField(Locator.findElement(By.xpath("//div[contains(@class, 'titleRefreshIcon')]//a[./*[@class='fa fa-refresh']]")));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("workgroups_EntityWorkGroupDefaults_Tab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromEntityWorkgroupDefaultsTab_toWorkgroupSettingsTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("workgroups_EntityWorkGroupDefaults_Tab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("workgroups_WorkgroupSettings_Tab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromWorkgroupSettingsTab_toEntityAssignmentTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("workgroups_WorkgroupSettings_Tab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("workgroups_EntityAssignment_Tab"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT1286_WorkgroupMaint_fromEntityAssignmentTab_toEditButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("workgroups_EntityAssignment_Tab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("workgroups_Defaults_AddButton"));
	}

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdministration.expandNode(testEntityWithChildrenNodes);
		SecurityAdministration.selectNode(testEntity);
	}
	
	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
