package r360.tests_ignored.misc;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.Pagination;

public class SecurityUsers_GroupManagement_TabTests_ExistingUser extends BaseTest {

	
	Pagination userManagementGrid;
	Pagination availableGroupsGrid;
	Pagination associatedGroupsGrid;
	String nodeText = "mrc";
	String userName = "AutomationTest";


	@BeforeMethod(alwaysRun = true)
	public void pre_setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();

		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);

		
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", userName));
		Thread.sleep(2000);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();

		// declare our Available Groups grid that we'll use later.
		availableGroupsGrid = new Pagination("userPageEntityGroupsGrid");
		associatedGroupsGrid = new Pagination("userPageUserGroupsGrid");
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromGroupTab_toSelectAllAvailable() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserEditGroupManage_TabLink"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Pagin_header_checkbox", availableGroupsGrid.gridID.toString(), null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromUserInfoTab_toGroupTab() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserEditUserInfo_TabLink"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEditGroupManage_TabLink_Active"));
	}
	
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromSelectAllAvailable_toNameFilterTextbox() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Pagin_header_checkbox", availableGroupsGrid.gridID.toString(), null));
		BaseUI.verifyElementHasFocus(availableGroupsGrid.filter_textbox_ByColumnName("Name"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromNameFilterTextbox_to1stAvailableGroupCheckbox() throws Throwable {
		BaseUI.tabThroughField(availableGroupsGrid.filter_textbox_ByColumnName("Name"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Pagin_firstTable_checkbox", availableGroupsGrid.gridID.toString(), null));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_ThroughAllAvailableGridCheckboxes() throws Throwable {
		availableGroupsGrid.verifytTabFromFirstCheckboxToEndOfCheckboxes();
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromEndOfAvailableGridCheckboxes_toMoveSelectedButton() throws Throwable {
		availableGroupsGrid.verifytTabFromFirstCheckboxToEndOfCheckboxes();
//		WebElement focusedElement =   Browser.driver.switchTo().activeElement();
//		focusedElement.sendKeys(Keys.TAB);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEditGroup_MoveSelectedButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromMoveMoveSelectedButton_toRemoveSelectedButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserEditGroup_MoveSelectedButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEditGroup_RemoveSelectedButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_ThroughAllAssociatedGridCheckboxes() throws Throwable {
		associatedGroupsGrid.verifytTabFromFirstCheckboxToEndOfCheckboxes();
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromEndOfAssociatedGridCheckboxes_toSaveButton() throws Throwable {
		associatedGroupsGrid.verifytTabFromFirstCheckboxToEndOfCheckboxes();
//		WebElement focusedElement =   Browser.driver.switchTo().activeElement();
//		focusedElement.sendKeys(Keys.TAB);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEdit_SaveButton"));
	}
	
	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6798_SecurityUser_Management_Tab_fromSaveButton_toCancelButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityUserEdit_SaveButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityUserEdit_CancelButton"));
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
