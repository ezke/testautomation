package r360.tests_ignored.misc;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RAAM_EntityTab_TabTests extends BaseTest {

	String testEntity = "automation_TabTestEntity";

	@BeforeMethod(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Entities();

		SecurityAdministration.launchAddEntityModal();
		SecurityAdmin_EntityManageModal.add_Entity(testEntity, "Financial Institution");
		SecurityAdministration.selectNode(testEntity);
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromAdminEntitiesNavigation_toDashboardLink() throws Throwable {
		Navigation.navigate_Admin_Entities();
		WebElement focusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.tabThroughField(focusedElement);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromDashboardMenu_toBatchSummaryMenu() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Dashboard", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromBatchSummary_toDDASummary() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Batch Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromDDASummary_toExceptions() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromExceptions_toSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Exceptions", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Search", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromSearch_toReports() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Search", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromReports_toNotifications() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Reports", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromNotifications_toAdmin() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_toPage_ByNavText", "Notifications", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_menuByText", "Admin", null));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromAdmin_toSignOUtLink() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_menuByText", "Admin", null));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("Navigate_SignOut"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromSignOutLink_toClickHereToSearch() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("Navigate_SignOut"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityTreeNode_SearchBar"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromClickHereToSearch_toFirstEntityInList() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityTreeNode_SearchBar"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityTreeNode_FirstNode"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromFirstEntityInList_toEntityTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityTreeNode_FirstNode"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_EntitiesTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromEntityTabButton_toUsersTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_EntitiesTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_UsersTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromUsersTabButton_toRolesTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_UsersTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_RolesTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromRolesTabButton_toGroupsTabButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_RolesTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityAdmin_GroupsTab"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromGroupsTabButton_toAddButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("securityAdmin_GroupsTab"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("securityEntities_AddButton"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromAddButton_toEditButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityAddbutton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityEditButton"));
	}

	@Test(groups = {"all_tests", "tab_tests"})
	public void PT6263_SecurityAdmin_Entities_fromEditButton_toDeleteButton() throws Throwable {
		BaseUI.tabThroughField(Locator.lookupElement("entityEditButton"));
		BaseUI.verifyElementHasFocus(Locator.lookupElement("entityDeleteButton"));
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {

			// Remove entity if it exists.
			if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
				SecurityAdministration.deleteEntity(testEntity);
			}

		} catch (Exception e) {
		}

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
