package r360.tests_ignored.misc;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests_Code;

public class RAAM_GroupManagement_UserManagementTab_AssociatedUsers_SinglePage extends Pagin_GridDisplay_FilterTests_Code {

	String parentEntity = "AutomationTest";
	//String testEntity = "automation_roleManage1";

	@BeforeClass
	public void navigate_To_Page() throws Exception {
		setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.selectNode(parentEntity);
		
		SecurityAdministration.launchAddGroupsModal();
		SecurityAdmin_GroupsManagementModal.navigate_UserManagementTab();
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
	}
	

	public void setVariables_AndData() {
		//gridID = "groupPageGroupRolesGrid";
		gridToTest = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid;
		//cancelButton_Identifier = "grpsManage_CancelButton";
		
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6360","Name", "tu"));
		filter_ByColumn.add(new ClassObject_ForFieldAndValue("PT6360","Name", "en"));

		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6360","Name", "yz"));
		filter_ByColumn_NoResults.add(new ClassObject_ForFieldAndValue("PT6360","Name", "yx"));
		
		filter_ByColumn_ClearFilter.add(new ClassObject_ForFieldAndValue("PT6358","Name", "tu"));

		//sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("Description", "Alpha"));
		sort_ByColumn_AscendingAndDescending.add(new ClassObject_ForFieldAndValue("","Name", "Alphabetical"));
		
		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6343","single", ""));
	}

	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6309_GroupManage_UsersManageTab_AssociatedUsers_CheckAllWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyCountLessThan_IntEntries(10);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyRows_AllCheckboxes_Checked();
	}

	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6309_GroupManage_UsersManageTab_AssociatedUsers_CheckAllAndMoveWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.getAllEntries("Name");
		
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.usersAvailableGrid.getAllEntries("Name");
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyPagesListsMatch(previousList, newList);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6311_GroupManage_RoleManageTab_AssociatedeRoles_CheckThreeGroupsAndMove() throws Throwable {
		String testentry1 ="newUser5";
		String testentry2 ="SomeoneName";
		String testentry3 ="TestUser1";
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name", testentry1);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name", testentry2);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name", testentry3);
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1, true);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2, true);
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3, true);
		
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1, false);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2, false);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3, false);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6311_GroupManage_RoleManageTab_AssociatedRoles_CheckOneGroupAndMove() throws Throwable {
		String testentry ="newUser5";
		
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.show_All();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.check_Checkbox_ForColumnName_AndText("Name", testentry);
		SecurityAdmin_GroupsManagementModal.remove_selectedUsers_FromAssociatedGrid();
		
		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry, true);
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry, false);
	}
	

	public void extend_AfterMethod() throws Exception {

		SecurityAdmin_GroupsManagementModal.usersAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.move_selectedUsers_FromAvailableGrid();
		SecurityAdmin_GroupsManagementModal.usersAssociatedGrid.sort_Column_Ascending("Name");
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		Thread.sleep(500);

		// Create a list of the unique fields that were in the Column tests
		// List.
		// We'll use this to determine what we have to clear out.
		ArrayList<String> uniqueFields = new ArrayList<String>();
		for (ClassObject_ForFieldAndValue dataSet : filter_ByColumn) {
			if (!uniqueFields.contains(dataSet.fieldName)) {
				uniqueFields.add(dataSet.fieldName);
			}
		}

		for (String field : uniqueFields) {
			gridToTest.filter_textbox_ClearFilter(field);
		}

		gridToTest.show_10();
		// return to the first page.
		if (gridToTest.firstPageButton().isEnabled()) {
			gridToTest.click_first();
		}

		// Allows us to extend the AfterMethod to allow for additional non-data
		// driven tests that are specific to the page.
		// Simply override this method with the new code.
		extend_AfterMethod();

	}

	public void extend_AfterClassMethod() throws Exception {
		if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
			SecurityAdmin_GroupsManagementModal.click_Cancel();
		}
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			extend_AfterClassMethod();
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}


	///Data Driven Tests
	///
	///
	///


	@Test(dataProvider = "Sort", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Sort_By_ColumnName_Ascending(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Sort_By_ColumnName_Ascending(testCase, filterName, filterText);
		
	}

	@Test(dataProvider = "Sort", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Sort_By_ColumnName_Descending(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Sort_By_ColumnName_Descending(testCase, filterName, filterText);
		
	}

	@Test(dataProvider = "filter_Field", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Filter_By_ColumnName_WithGivenText(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Filter_By_ColumnName_WithGivenText(testCase, filterName, filterText);
	}

	@Test(dataProvider = "filter_Field_NoResults", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Filter_By_ColumnName_WithGivenText_NoResults(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Filter_By_ColumnName_WithGivenText_NoResults(testCase, filterName, filterText);
	}

	// Some TC's call for you to set a filter, launch a modal, close the modal
	// and then clear the filter.
	// You'll need to set the cancelButton_Identifier variable for these to
	// work. That will be the cancel button identifier
	// to close the modal window.
	@Test(dataProvider = "filter_Field_ClearFilter_AndLaunchModal", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(String testCase,
			String fieldName, String fieldText) throws Throwable {
		TC_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(testCase, fieldName, fieldText);
	}

	@Test(dataProvider = "filter_Field_ClearFilter", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_FilterAnd_ClearFilter_AllPreviousEntriesAppear(String testCase, String fieldName, String fieldText)
			throws Throwable {
		TC_FilterAnd_ClearFilter_AllPreviousEntriesAppear(testCase, fieldName, fieldText);
	}

	
	
	
	
	
	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_ListsAreDifferent(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_ListsAreDifferent(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstButtonEnabled(testCaseName, testType,
				columnToCheck);
		
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_NextButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(testCaseName, testType,
				columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		
		Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(testCaseName, testType,
				columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_AscendingOrder(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(testCaseName, testType,
				columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(testCaseName, testType,
				columnToCheck);
	}
	
	
	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_First_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		Navigation_SinglePage_First_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_Previous_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_SinglePage_Previous_Button_Disabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_Next_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		Navigation_SinglePage_Next_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_Last_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		Navigation_SinglePage_Last_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	///
	///
	/// End of Single page navigation tests.///

	/// Show Auto/All/100/50/25/10 tests
	///
	///



	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_ShowAllOption(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_ShowAllOption(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_ShowAutoOption(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_ShowAutoOption(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show10Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show10Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show25Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show25Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show50Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show50Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show100Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show100Option(testCase, columnName, extraValue);
	}

	

}
