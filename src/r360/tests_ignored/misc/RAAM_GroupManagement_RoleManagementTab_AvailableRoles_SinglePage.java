package r360.tests_ignored.misc;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_GroupsManagementModal;
import r360.pages.SecurityAdministration;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.tests.Pagin_GridDisplay_FilterTests_Code;

public class RAAM_GroupManagement_RoleManagementTab_AvailableRoles_SinglePage extends Pagin_GridDisplay_FilterTests_Code {

	String parentEntity = "AutomationTest";
	String testEntity = "testEntity_SinglePageTests";
	//String testEntity = "automation_roleManage1";

	@BeforeClass
	public void navigate_To_Page() throws Exception {
		setVariables_AndData();
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_UserGroups();
		SecurityAdministration.expandNode(parentEntity);
		SecurityAdministration.selectNode(testEntity);
		
		SecurityAdministration.launchEditGroupModal_forGivenGroup("groupNumberA0");
		SecurityAdmin_GroupsManagementModal.navigate_RoleManagementTab();
	}
	
	public void setVariables_AndData() {
		gridToTest = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid;
		
		navigation_testGenerator.add(new ClassObject_ForFieldAndValue("PT6328", "single", ""));
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6304_GroupManage_RoleManageTab_AvailableRoles_CheckAllWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyCountLessThan_IntEntries(10);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyRows_AllCheckboxes_Checked();
	}

	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6304_GroupManage_RoleManageTab_AvailableRoles_CheckAllAndMoveWorks() throws Throwable {
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_all_checkbox();
		ArrayList<String> previousList = SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.getAllEntries("Name");
		
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		ArrayList<String> newList = SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.getAllEntries("Name");
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyPagesListsMatch(previousList, newList);
	}

	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6306_GroupManage_RoleManageTab_AvailableRoles_CheckOneGroupAndMove() throws Throwable {
		String testentry ="secondsetB0: testEntity_SinglePageTests";
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry, true);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry, false);
	}
	
	@Test(alwaysRun = true, groups = {"all_tests", "pagination"})
	public void PT6306_GroupManage_RoleManageTab_AvailableRoles_CheckThreeGroupsAndMove() throws Throwable {
		String testentry1 ="secondsetB0: testEntity_SinglePageTests";
		String testentry2 ="secondsetB1: testEntity_SinglePageTests";
		String testentry3 ="secondsetB3: testEntity_SinglePageTests";
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.show_All();
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry1);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry2);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.check_Checkbox_ForColumnName_AndText("Name", testentry3);
		SecurityAdmin_GroupsManagementModal.move_selectedRoles_FromAvailableGrid();
		
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1, true);
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2, true);
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3, true);
		
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry1, false);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry2, false);
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.verifyEntryExists_OR_DoesntExist("Name", testentry3, false);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		SecurityAdmin_GroupsManagementModal.rolesAvailableGrid.uncheck_all_checkbox();
		
		SecurityAdmin_GroupsManagementModal.rolesAssociatedGrid.check_all_checkbox();
		SecurityAdmin_GroupsManagementModal.remove_selectedRoles_FromAssociatedGrid();
	}
	
	
	
	
	

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			if (Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
				SecurityAdmin_GroupsManagementModal.click_Cancel();
			}
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}


	///Data Driven Tests
	///
	///
	///


	@Test(dataProvider = "Sort", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Sort_By_ColumnName_Ascending(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Sort_By_ColumnName_Ascending(testCase, filterName, filterText);
		
	}

	@Test(dataProvider = "Sort", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Sort_By_ColumnName_Descending(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Sort_By_ColumnName_Descending(testCase, filterName, filterText);
		
	}

	@Test(dataProvider = "filter_Field", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Filter_By_ColumnName_WithGivenText(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Filter_By_ColumnName_WithGivenText(testCase, filterName, filterText);
	}

	@Test(dataProvider = "filter_Field_NoResults", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Filter_By_ColumnName_WithGivenText_NoResults(String testCase, String filterName, String filterText)
			throws Throwable {
		TC_Filter_By_ColumnName_WithGivenText_NoResults(testCase, filterName, filterText);
	}

	// Some TC's call for you to set a filter, launch a modal, close the modal
	// and then clear the filter.
	// You'll need to set the cancelButton_Identifier variable for these to
	// work. That will be the cancel button identifier
	// to close the modal window.
	@Test(dataProvider = "filter_Field_ClearFilter_AndLaunchModal", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(String testCase,
			String fieldName, String fieldText) throws Throwable {
		TC_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(testCase, fieldName, fieldText);
	}

	@Test(dataProvider = "filter_Field_ClearFilter", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_FilterAnd_ClearFilter_AllPreviousEntriesAppear(String testCase, String fieldName, String fieldText)
			throws Throwable {
		TC_FilterAnd_ClearFilter_AllPreviousEntriesAppear(testCase, fieldName, fieldText);
	}

	
	
	
	
	
	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_ListsAreDifferent(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_ListsAreDifferent(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstButtonEnabled(testCaseName, testType,
				columnToCheck);
		
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_NextButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(testCaseName, testType,
				columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		
		Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(testCaseName, testType,
				columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_AscendingOrder(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(testCaseName, testType,
				columnToCheck);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(testCaseName, testType,
				columnToCheck);
	}
	
	
	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_First_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		Navigation_SinglePage_First_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_Previous_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		Navigation_SinglePage_Previous_Button_Disabled(testCaseName, testType,
				columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_Next_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		Navigation_SinglePage_Next_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	@Test(dataProvider = "navigation_single_page", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_Navigation_SinglePage_Last_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		Navigation_SinglePage_Last_Button_Disabled(testCaseName, testType, columnToCheck);
	}

	///
	///
	/// End of Single page navigation tests.///

	/// Show Auto/All/100/50/25/10 tests
	///
	///



	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_ShowAllOption(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_ShowAllOption(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_ShowAutoOption(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_ShowAutoOption(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show10Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show10Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show25Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show25Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show50Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show50Option(testCase, columnName, extraValue);
	}

	@Test(dataProvider = "show_Number", alwaysRun = true, groups = {"all_tests", "pagination"})
	public void Test_ShowTests_Show100Option(String testCase, String columnName, String extraValue) throws Throwable {
		ShowTests_Show100Option(testCase, columnName, extraValue);
	}

	
	
}
