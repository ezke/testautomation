package r360.tests_ignored.misc;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdminAddUser;
import r360.pages.SecurityAdmin_EntityManageModal;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import wfsCommon.pages.EmailChecker;

public class RAAM_Login_ResetPasswordPage_LoginSuccessful_old extends BaseTest {
	
	private String newEntity = "Auto_Email_TestLogin";
	private String entityType = "Financial Institution";
	private String entityDescription = "email test Description";
	private String entityExternalID = "email test ID 92";
	String columnName = "Login Name";String columnText = "emaillogin";String loginName = columnText;
	String firstName = "Tom";String lastName = "Cruise";String userPassword = "Wausau#1";
	String email = "reanna@putsbox.com";String oldPassword = userPassword;
	String newPassword = "Microsoft#1";
	
	//putsbox username and password
		String username = "testingr360email@putsbox.com";
		String password = "windows#1";
		String emailName = "reanna";

		@BeforeClass(alwaysRun = true)
		public void setup() throws Exception {
			Browser.openBrowser(GlobalVariables.baseURL, "chrome");
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
			Navigation.navigate_Admin_Entities();
			Thread.sleep(2000);
			if(BaseUI.pageSourceContainsString(newEntity)){
				SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
				SecurityAdministration.navigate_toEntityTab();
				SecurityAdministration.deleteEntity(newEntity);
			}
			SecurityAdministration.launchAddEntityModal();
			SecurityAdmin_EntityManageModal.add_Entity(newEntity, entityType, entityDescription, entityExternalID);
			Thread.sleep(1000);
			SecurityAdministration.navigate_toUserTab();
			SecurityAdminAddUser.clickAddUserButton();	
			SecurityAdmin_UserManageModal.addUser(loginName, userPassword, firstName, lastName, email);
			Thread.sleep(2000);
			Navigation.signOut();
			HomePage.clickReturnToApplicationLink();
		}
		
		@Test(groups = { "all_Tests" })
		public void PT6486_PasswordEmail_ResetPasswordFormAppears() throws Exception{
			LoginPage.clickForgotPasswordLink();
			Thread.sleep(2000);
			String emailSubject1 = "Reset Password";
			LoginPage.resetForgotPasswordForm(newEntity, loginName, emailSubject1);
			Browser.closeBrowser();
			Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
			Thread.sleep(3000);
			//verify if first email is sent
			EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject1);
			Thread.sleep(2000);
			String winHandleBefore = Browser.driver.getWindowHandle();
			EmailChecker.clickOnLinkBySubjectText(emailSubject1);
			Thread.sleep(2000);
			EmailChecker.clickOnEmailLink();
			Navigation.navigate_Past_CertificateError();
			EmailChecker.verifyResetPasswordModalAppears();
			EmailChecker.switchWindow_Or_NavigateBack(winHandleBefore);
//			Browser.driver.switchTo().window(winHandleBefore);
			EmailChecker.deleteAllEmailReceived();
			Browser.closeBrowser();
			Browser.openBrowser(GlobalVariables.baseURL, "chrome");
		}
		@Test(groups = { "all_Tests" })
		public void PT6498_PasswordEmail_ResetPassword_Successful() throws Exception{
			Navigation.navigate_Past_CertificateError();
			Thread.sleep(2000);
			LoginPage.clickForgotPasswordLink();
			Thread.sleep(2000);
			String emailSubject1 = "Reset Password";
			LoginPage.resetForgotPasswordForm(newEntity, loginName, emailSubject1);
			Browser.closeBrowser();
			Browser.openBrowser("http://putsbox.com/users/sign_in", "chrome");
			Thread.sleep(3000);
			//verify if first email is sent
			EmailChecker.verifyEmailReceived(username, password, emailName, emailSubject1);
			Thread.sleep(4000);
			EmailChecker.clickOnEmailLink_ResetPassword(emailSubject1, newEntity, columnText, newPassword);
			EmailChecker.deleteAllEmailReceived();
			Browser.closeBrowser();
			Browser.openBrowser(GlobalVariables.baseURL, "chrome");
			LoginPage.login(newEntity, columnText, newPassword);
			HomePage.verifyHeaderTitleAppears();
			Navigation.signOut();
			HomePage.clickReturnToApplicationLink();
		}
		
		@AfterMethod(alwaysRun = true)
		public void tearDown(ITestResult result) throws Exception {
			ResultWriter.checkForFailureAndScreenshot(result);
		}
		
		@AfterClass(alwaysRun = true)
		public void writeResultsAndTearDown() throws Exception {	
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
			Navigation.navigate_Admin_Entities();
			Thread.sleep(2000);
			if(BaseUI.pageSourceContainsString(newEntity)){
				SecurityAdministration.deleteUsers_ForGivenEntity(newEntity);
				SecurityAdministration.navigate_toEntityTab();
				SecurityAdministration.deleteEntity(newEntity);
			}
			Browser.closeBrowser();
		}
}
