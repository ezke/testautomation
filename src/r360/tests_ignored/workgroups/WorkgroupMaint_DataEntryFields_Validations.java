package r360.tests_ignored.workgroups;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class WorkgroupMaint_DataEntryFields_Validations extends BaseTest {

	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";
	
	private String testWorkGroup_LongName = "TestWorkGroup_NameChange";

	private String dataEntryField_Template = "Blank Data Entry Field";
	private Boolean dataEntryField_Active = true;
	private String dataEntryField_FieldName = "AutomationDataEntry";
	private String dataEntryField_UILabel = "AutomationDataEntry";
	private String dataEntryField_ItemType = "Payment";
	private String dataEntryField_DisplayOrder = "18";
	private String dataEntryField_DataType = "Text";
	private String dataEntryField_PaymentSource = "AutomationACH";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		
	}
	
	@Test
	public void PT1251_DataEntryFieldSetup_VerifyItemTypeList_Payment() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.itemType_Dropdown.select_EntityValue("Payment");
		Admin_Workgroups_AddingEditing_WorkgroupModal.itemType_Dropdown.verify_SelectedDropdownValue("Payment");
	}
	
	@Test
	public void PT1251_DataEntryFieldSetup_VerifyItemTypeList_Invoice() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.itemType_Dropdown.select_EntityValue("Invoice");
		Admin_Workgroups_AddingEditing_WorkgroupModal.itemType_Dropdown.verify_SelectedDropdownValue("Invoice");
	}
	
	@Test
	public void PT1273_DataEntryFieldSetup_VerifyDataTypeList_Text() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.select_EntityValue("Text");
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.verify_SelectedDropdownValue("Text");
	}
	
	@Test
	public void PT1273_DataEntryFieldSetup_VerifyDataTypeList_FloatingPoint() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.select_EntityValue("Floating Point");
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.verify_SelectedDropdownValue("Floating Point");
	}
	
	@Test
	public void PT1273_DataEntryFieldSetup_VerifyDataTypeList_Currency() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.select_EntityValue("Currency");
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.verify_SelectedDropdownValue("Currency");
	}
	
	@Test
	public void PT1273_DataEntryFieldSetup_VerifyDataTypeList_Date() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.select_EntityValue("Date");
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.verify_SelectedDropdownValue("Date");
	}
	
	@Test
	public void PT1267_DataEntryFieldSetup_Validation_FieldName_Blank() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active,"", 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "Required. (Allows alphanumeric, hyphens and underscores.)";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
	}
	
	@Test
	public void PT1267_DataEntryFieldSetup_Validation_FieldName_IncludesSpace() throws Exception{
		String fieldNameToEnter = "Account Field";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active,fieldNameToEnter, 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "Required. (Allows alphanumeric, hyphens and underscores.)";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
	}
	
	@Test
	public void PT1267_DataEntryFieldSetup_Validation_FieldName_IncludesSpecialCharacters() throws Exception{
		String fieldNameToEnter = "Account&1";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, fieldNameToEnter, 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "Required. (Allows alphanumeric, hyphens and underscores.)";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
	}
	
	@Test
	public void PT1267_DataEntryFieldSetup_Validation_FieldName_129Characters() throws Exception{
		String fieldNameToEnter = "verulogngcgaractertotestsutomdationwhyysgsgtevasgetdajdvgatwscsasnsnhdfehfgfgsghfghsfaghsafghsaghafghsfghgsagfdghasfghsafshgafasg";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, fieldNameToEnter, 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		String fieldName = BaseUI.getTextFromField(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName"));
		BaseUI.verify_true_AndLog(fieldName.length() <= 128, "FieldName length was less than or equal to 128.",
				"FieldName length was NOT less than or equal to 128.");
		
	}
	
	@Test
	public void PT1267_DataEntryFieldSetup_Validation_FieldName_ExistingName() throws Exception{
		String fieldNameToEnter = "AccountNumber";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, fieldNameToEnter, 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "The field name entered already exists.";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
		
	}
	
	@Test
	public void PT1268_DataEntryFieldSetup_Validation_DisplayOrderField_Blank() throws Exception{
		String displayOrderToEnter = "";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, displayOrderToEnter, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement displayOrder = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder_Error");
		String displayOrderError = displayOrder.getText();
		String expectedError = "Required (Allows numbers 0-255)";
		BaseUI.baseStringCompare("FieldName", expectedError, displayOrderError);
	}
	
	@Test
	public void PT1268_DataEntryFieldSetup_Validation_DisplayOrderField_Includes_SpecialCharacters() throws Exception{
		String displayOrderToEnter = "*&1";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, displayOrderToEnter, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement displayOrder = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder_Error");
		String displayOrderError = displayOrder.getText();
		String expectedError = "Required (Allows numbers 0-255)";
		BaseUI.baseStringCompare("FieldName", expectedError, displayOrderError);
	}
	
	@Test
	public void PT1268_DataEntryFieldSetup_Validation_DisplayOrderField_Includes_AlphaCharacters() throws Exception{
		String displayOrderToEnter = "ab1";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, displayOrderToEnter, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement displayOrder = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder_Error");
		String displayOrderError = displayOrder.getText();
		String expectedError = "Required (Allows numbers 0-255)";
		BaseUI.baseStringCompare("FieldName", expectedError, displayOrderError);
	}
	
	@Test
	public void PT1268_DataEntryFieldSetup_Validation_DisplayOrderField_4DigitNumber() throws Exception{
		String displayOrderToEnter = "1234";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, displayOrderToEnter, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		String displayOrder = BaseUI.getTextFromField(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder"));
		BaseUI.verify_true_AndLog(displayOrder.length() <= 3, "DisplayOrder length was less than or equal to 3.",
				"DisplayOrder length was NOT less than or equal to 3.");
	}
	
	@Test
	public void PT1268_DataEntryFieldSetup_Validation_DisplayOrderField_3DigitNumber_GreaterThan255() throws Exception{
		String displayOrderToEnter = "259";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, displayOrderToEnter, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement displayOrder = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder_Error");
		String displayOrderError = displayOrder.getText();
		String expectedError = "Required (Allows numbers 0-255)";
		BaseUI.baseStringCompare("FieldName", expectedError, displayOrderError);
	}
	
	@Test
	public void PT1266_DataEntryFieldSetup_Validation_ItemTypeDropdown_NoSelection() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName"), dataEntryField_FieldName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), dataEntryField_UILabel);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder"), dataEntryField_DisplayOrder);
		Admin_Workgroups_AddingEditing_WorkgroupModal.dataType_Dropdown.select_EntityValue(dataEntryField_DataType);
		Admin_Workgroups_AddingEditing_WorkgroupModal.paymentSource_Dropdown.select_EntityValue(dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement itemType = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_ItemType_Error");
		String itemTypeError = itemType.getText();
		String expectedError = "Selection Required.";
		BaseUI.baseStringCompare("FieldName", expectedError, itemTypeError);
	}
	
	@Test
	public void PT1266_DataEntryFieldSetup_Validation_ItemTypeDropdown_Cancel() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
		WebElement elementName = Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.get_row_ByColumnName_And_ColumnMatchingText("UI Label", "Acount1");
		BaseUI.verifyElementDoesNotAppear(elementName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
	}
	
	@Test
	public void PT1264_DataEntryFieldSetup_Validation_DataTypeDropdown_NoSelection() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName"), dataEntryField_FieldName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), dataEntryField_UILabel);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder"), dataEntryField_DisplayOrder);	
		Admin_Workgroups_AddingEditing_WorkgroupModal.itemType_Dropdown.select_EntityValue(dataEntryField_ItemType);
		Admin_Workgroups_AddingEditing_WorkgroupModal.paymentSource_Dropdown.select_EntityValue(dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement dataType = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DataType_Error");
		String dataTypeError = dataType.getText();
		String expectedError = "Selection Required.";
		BaseUI.baseStringCompare("FieldName", expectedError, dataTypeError);
	}
	
	@Test
	public void PT1264_DataEntryFieldSetup_Validation_DataTypeDropdown_Cancel() throws Exception{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, dataEntryField_Active, dataEntryField_FieldName, 
				dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
		WebElement elementName = Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.get_row_ByColumnName_And_ColumnMatchingText("UI Label", "Acount1");
		BaseUI.verifyElementDoesNotAppear(elementName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		if(BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		
	}
	
	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Throwable {

		try {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}

