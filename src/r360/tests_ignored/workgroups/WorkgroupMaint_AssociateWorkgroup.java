package r360.tests_ignored.workgroups;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AssignWorkgroupsModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class WorkgroupMaint_AssociateWorkgroup extends BaseTest {
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";
	
	private String workGroup1 = "TestWorkgroupTobeAssigned";
	private String workGroup2 = "TestMultipleWorkgroup1";
	private String workGroup3 = "TestMultipleWorkgroup2";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_EntityAssignment_Tab();
		if(BaseUI.pageSourceContainsString(workGroup1)){
			Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup1);
		}
		if(BaseUI.pageSourceContainsString(workGroup2)){
			Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup2);
		}
		if(BaseUI.pageSourceContainsString(workGroup3)){
			Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup3);
		}
	}
	
	@Test
	public void PT1396_AssociateWorkgroup_ViewEntityAssignmentTab() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("workgroups_EntityAssignment_Tab"));
	}
	
	@Test
	public void PT1397_AssociateWorkGroup_ManageEntityAssignmentTab() throws Exception{
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.assign_UnassignedWorkgroup(workGroup1);
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();

	}
	
	@Test
	public void PT8858_Assigning_AndUnassigning_SingleWorkgroup_ToAnEntity() throws Exception{
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Integer previousList = Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.get_row_Count();
		Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.verifyCountOfEntries(previousList);
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AvailableGrid_BulbIcon"));
		Admin_Workgroups_AssignWorkgroupsModal.move_fromAvailable_toSelected(workGroup1);
		
		Integer newList = Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.get_row_Count();
		Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.verifyCountOfEntries(newList);
		
		BaseUI.verify_true_AndLog(previousList != newList, "Previous Available List Do Not Match New Available List ", 
				"Previous Available List Match New Available List");
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();
		Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup1);
		
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.move_fromAvailable_toSelected(workGroup1);
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();
		
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode("testEntityWorkGroup");
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.verifyCountOfEntries(newList);
		Admin_Workgroups_AssignWorkgroupsModal.availableGrid.verifyEntryExists_OR_DoesntExist("Login Name", workGroup1, false);
		Admin_Workgroups_AssignWorkgroupsModal.cancel_changes();
	}
	//Assigns multiple workgroups, verifies previous records and current records in available grid
	// Selects another Entity under same FI Entity, verifies workgroup is not included in that Entity 
	@Test 
	public void TC169282_Assigning_AndUnassigning_MultipleWorkgroup_ToAnEntity() throws Exception{
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Integer previousList = Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.get_row_Count();
		Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.verifyCountOfEntries(previousList);
		BaseUI.click(Admin_Workgroups_AssignWorkgroupsModal.availableGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workGroup2));
		BaseUI.click(Admin_Workgroups_AssignWorkgroupsModal.availableGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workGroup3));
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AddSelectedButton"));
		Thread.sleep(300);
		
		Integer newList = Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.get_row_Count();
		Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.verifyCountOfEntries(newList);
		
		BaseUI.verify_true_AndLog(previousList != newList, "Previous Available List Do Not Match New Available List ", 
				"Previous Available List Match New Available List");
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();
		Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup2);
		Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup3);
		
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.showAll_AvailableGrid();
		BaseUI.click(Admin_Workgroups_AssignWorkgroupsModal.availableGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workGroup2));
		BaseUI.click(Admin_Workgroups_AssignWorkgroupsModal.availableGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workGroup3));
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AddSelectedButton"));
		Thread.sleep(300);
		Admin_Workgroups_AssignWorkgroupsModal.save_changes();
		
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode("testEntityWorkGroup");
		Admin_Workgroups.launch_AssignWorkgroupsmodal();
		Admin_Workgroups_AssignWorkgroupsModal.availablePagerGrid.verifyCountOfEntries(newList);
		Admin_Workgroups_AssignWorkgroupsModal.availableGrid.verifyEntryExists_OR_DoesntExist("Login Name", workGroup2, false);
		Admin_Workgroups_AssignWorkgroupsModal.availableGrid.verifyEntryExists_OR_DoesntExist("Login Name", workGroup3, false);
		Admin_Workgroups_AssignWorkgroupsModal.cancel_changes();
	}
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);
		
//		if(BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
//			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
//		}
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_EntityAssignment_Tab();
		if(BaseUI.pageSourceContainsString(workGroup1)){
			Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup1);
		}
		if(BaseUI.pageSourceContainsString(workGroup2)){
			Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup2);
		}
		if(BaseUI.pageSourceContainsString(workGroup3)){
			Admin_Workgroups.remove_Workgroup_ForGivenWorkgroup("Long Name", workGroup3);
		}
	}
	
	
	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
		
}
