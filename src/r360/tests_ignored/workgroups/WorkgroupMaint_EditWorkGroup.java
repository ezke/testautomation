package r360.tests_ignored.workgroups;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class WorkgroupMaint_EditWorkGroup extends BaseTest {

	public static final String workGroupSettings_GridID = "workgroupMaintenance";
	public static Pagination workGroupSettings_GRID = new Pagination(workGroupSettings_GridID);
	private String testEntityWithChildrenNodes = "AutomationTest";
	private String testEntity = "workGroupTests_Entity";
	private String testWorkGroup_LongName = "TestWorkGroup_NameChange";
	private String testWorkGroup_ShortName = "NameChange";
	private String testWorkGroup_WorkgroupID = "123456789";
	private String testWorkGroup_DataRetentionDays = "365";
	private String testWorkGroup_ImageRetentionDays = "365";
	private String testWorkGroup_FileGroup = "";
	private String testWorkGroup_InActive_Workgroup = "TestWorkgroup_InActive";

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");

	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_LongName() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_LongNameText",
				testWorkGroup_LongName);
	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_ShortName() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_ShortNameText",
				testWorkGroup_ShortName);
	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_DataRetentiondDays() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_DataRentionDaysText",
				testWorkGroup_DataRetentionDays);
	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_ImageRetentionDays() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_ImageRentionDaysText",
				testWorkGroup_ImageRetentionDays);
	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_FileGroup() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_FileGroupText",
				testWorkGroup_FileGroup);
	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_ActiveStatus() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_IsActive_Checkbox"), true);
	}

	@Test
	public void PT1284_WorkgroupMaint_Verify_WorkgroupID() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_WorkgroupID", testWorkGroup_WorkgroupID);
	}

	@Test
	public void PT1275_WorkgroupMaint_Verify_BankID_Disabled() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyElementDisabled(Locator.lookupElement("wrkgrp_AddEdit_BankText"));
	}

	@Test
	public void PT1275_WorkgroupMaint_Verify_WorkgroupID_Disabled() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyElementDisabled(Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID"));
	}

	@Test
	public void PT1285_WorkgroupMaint_Verify_AllActive_AndInActiveWorkgroup() throws Exception {
		Admin_Workgroups.verify_AllWorkGroups_ActiveOrInActive_ByEntity(testEntity);
	}
	
	@Test
	public void PT1285_WorkgroupMaint_Verify_ActiveWorkgroup() throws Exception {
		Admin_Workgroups.verify_WorkGroup_ActiveOrInactive_ByEntityAndWorkgroup_SortByAscending(testEntity,
				testWorkGroup_LongName, true);
	}

	@Test
	public void PT1285_WorkgroupMaint_Verify_InActiveWorkgroup() throws Exception {
		Admin_Workgroups.verify_WorkGroup_ActiveOrInactive_ByEntityAndWorkgroup_SortByAscending(testEntity,
				testWorkGroup_InActive_Workgroup, false);
	}

	@Test
	public void PT1300_WorkgroupMaintenance_ClickingEditButton_LandsOn_GeneralTab_WithEditTitle_Text() throws Throwable {
		String expectedTextWithLongName = "Editing Workgroup: 123456789 - TestWorkGroup_NameChange";
//		String expectedTextNoLongName = "Editing Workgroup: 99977 -";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		WebElement elementLongName = Locator.lookupElement("wrkgrp_AddEdit_Detail_Text");
		String actualTextWithLongName = elementLongName.getText();
		BaseUI.verifyElementAppears(elementLongName);
		BaseUI.baseStringCompare("Title", expectedTextWithLongName, actualTextWithLongName);
		BaseUI.verifyElementAppears(Locator.lookupElement("wrkgrp_AddEdit_General_Tab"));
//		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();

//		Admin_Workgroups.workGroupSettings_GRID.click_sort_ByColumnName("Workgroup ID");
//		Admin_Workgroups.workGroupSettings_GRID.click_sort_ByColumnName("Workgroup ID");
//		Admin_Workgroups.launch_EditWorkGroupSettings_Modal_ForGivenWorkgroupID(testWorkGroup_WorkgroupID);
//		WebElement elementNoLongName = Locator.lookupElement("wrkgrp_AddEdit_Details_Text");
//		String actualTextNoLongName = elementNoLongName.getText();
//		BaseUI.verifyElementAppears(elementNoLongName);
//		BaseUI.baseStringCompare("Title", expectedTextNoLongName, actualTextNoLongName);

	}

	@Test
	public void PT1299_WorkgroupMaintenance_ClickingEditButton_SameEditTitleText_OnBothGeneralTab_And_DDAAssociation() throws Throwable {
		String expectedTextWithLongName = "Editing Workgroup: 123456789 - TestWorkGroup_NameChange";
//		String expectedTextNoLongName = "Editing Workgroup: 99977 -";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		WebElement elementLongName = Locator.lookupElement("wrkgrp_AddEdit_Detail_Text");
		String actualTextWithLongName = elementLongName.getText();
		BaseUI.verifyElementAppears(elementLongName);
		BaseUI.baseStringCompare("Title", expectedTextWithLongName, actualTextWithLongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		BaseUI.verifyElementAppears(elementLongName);
		BaseUI.baseStringCompare("Title", expectedTextWithLongName, actualTextWithLongName);
//		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();

//		Admin_Workgroups.workGroupSettings_GRID.click_sort_ByColumnName("Workgroup ID");
//		Admin_Workgroups.workGroupSettings_GRID.click_sort_ByColumnName("Workgroup ID");
//		Admin_Workgroups.launch_EditWorkGroupSettings_Modal_ForGivenWorkgroupID(testWorkGroup_WorkgroupID);
//		WebElement elementNoLongName = Locator.lookupElement("wrkgrp_AddEdit_Details_Text");
//		String actualTextNoLongName = elementNoLongName.getText();
//		BaseUI.verifyElementAppears(elementNoLongName);
//		BaseUI.baseStringCompare("Title", expectedTextNoLongName, actualTextNoLongName);
//		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
//		BaseUI.verifyElementAppears(elementNoLongName);
//		BaseUI.baseStringCompare("Title", expectedTextNoLongName, actualTextNoLongName);

	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
