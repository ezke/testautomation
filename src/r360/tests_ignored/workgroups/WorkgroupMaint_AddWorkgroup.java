package r360.tests_ignored.workgroups;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class WorkgroupMaint_AddWorkgroup extends BaseTest {
	
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";
	
	private String testWorkGroup_BankName = "12321412 - TestBank";
	private String testWorkGroup_WorkgroupID = "586489";
	private String testWorkGroup_LongName = "TestWorkGroup_AddWorkgroup";
	private String testWorkGroup_ShortName = "AddWorkgroup";
	private String testWorkGroup_DataRetentionDays = "365";
	private String testWorkGroup_ImageRetentionDays = "365";
	private String testWorkGroup_FileGroup = "";
	private String testWorkGroup_ViewingDays = "255";
	private String testWorkGroup_MaxSearchDays = "255";
	private String testWorkGroup_DisplayBatchID = "Use Entity Setting";
	private String testWorkGroup_PaymentImageDisplay = "Use Entity Setting";
	private String testWorkGroup_DocumentImageDisplay = "Use Entity Setting";
	private Boolean testWorkGroup_isHOA = false;
	
	
	@BeforeClass
	public void setup_method() throws Throwable {
//		Deleting workgroup from database before any test runs
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("852745", "1159511", "-1");
		Thread.sleep(5000);
		
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1186_WorkgroupMaintenace_AddWorkgroup() throws Throwable{
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_Workgroup(testWorkGroup_BankName, testWorkGroup_WorkgroupID, 
				testWorkGroup_LongName, testWorkGroup_ShortName, testWorkGroup_DataRetentionDays, 
				testWorkGroup_ImageRetentionDays, testWorkGroup_FileGroup, testWorkGroup_ViewingDays, 
				testWorkGroup_MaxSearchDays, testWorkGroup_DisplayBatchID, testWorkGroup_PaymentImageDisplay, 
				testWorkGroup_DocumentImageDisplay, testWorkGroup_isHOA);
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1187_AddWorkgroup_Bank_EmptyField_Error() throws Throwable{
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID"), testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementBank = Locator.lookupElement("wrkgrp_AddEdit_Bank_Error");
		String errorMessage = elementBank.getText();
		String expected_BankError = "Selection Required.";
		BaseUI.baseStringCompare("Bank", expected_BankError, errorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1187_AddWorkgroup_WorkGroupID_EmptyField_Error() throws Throwable{
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.bankName_Dropdown.select_EntityValue(testWorkGroup_BankName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementWorkgroupID = Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID_Error");
		String errorMessage = elementWorkgroupID.getText();
		String expected_WorkgroupIDError = "The Workgroup ID must be a numeric value.";
		BaseUI.baseStringCompare("Bank", expected_WorkgroupIDError, errorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1187_AddWorkgroup_LongName_EmptyField_Error() throws Throwable{
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"), testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementLongName = Locator.lookupElement("wrkgrp_AddEdit_LongName_Error");
		String longNameErrorMessage = elementLongName.getText();
		String expectedError = "Required. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)";
		BaseUI.baseStringCompare("LongName", expectedError, longNameErrorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1187_AddWorkgroup_ShortName_EmptyField_Error() throws Throwable{
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"), testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementShortName = Locator.lookupElement("wrkgrp_AddEdit_ShortName_Error");
		String shortNameErrorMessage = elementShortName.getText();
		String expectedError = "Required. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)";
		BaseUI.baseStringCompare("LongName", expectedError, shortNameErrorMessage);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1384_AddWorkgroup_Bank_CanSelect_DifferentBankID() throws Throwable{
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.bankName_Dropdown.select_EntityValue("1159511 - Automation Imports Bank");
		Admin_Workgroups_AddingEditing_WorkgroupModal.bankName_Dropdown.select_EntityValue(testWorkGroup_BankName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID"), testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8857_AddWorkgroup_WorkgroupID_AlreadyExists_Error() throws Throwable{
		String workgroupIDExists = "852745";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, workgroupIDExists);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		BaseUI.verifyElementAppears(Locator.lookupElement("error_MessagePopup"));
		WebElement element = Locator.lookupElement("error_MessageText");
		String expectedErrorMessage = "Unable to add workgroup. Workgroup ID 852745 already exists.";
		String errorMessage = element.getText();
		BaseUI.baseStringCompare("Workgroup", expectedErrorMessage, errorMessage);
		Navigation.close_ToastError_If_ItAppears();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8857_AddWorkgroup_WorkgroupID_AlreadyExists_AddTo_DifferentBank_NoError() throws Throwable{
		String workgroupIDExists = "852745";
		String differentBank = "1159511 - Automation Imports Bank";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(differentBank, workgroupIDExists);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("852745", "1159511", "-1");
	}

	@Test(groups = { "all_Tests" })
	public void PT8873_AddWorkgroup_LongName_AlphaNumeric_Characters_NoError() throws Throwable{
		String longName_AlphaNumChar = "ABCDEfghi-1234567890";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(longName_AlphaNumChar, testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", longName_AlphaNumChar);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8855_AddWorkgroup_ShortName_AlphaNumeric_Characters_NoError() throws Throwable{
		String shortName_AlphaNumChar = "ABCDEfghi-1234567890";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, shortName_AlphaNumChar);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8852_AddWorkgroup_FileGroup_InvalidPath_Error() throws Throwable{
		String fileGroup_InvalidPath = "Users";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_FileGroupText"), fileGroup_InvalidPath);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		BaseUI.verifyElementAppears(Locator.lookupElement("wrkgrp_AddEdit_FileGroupText_Error"));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8852_AddWorkgroup_FileGroup_ValidPath_NoError() throws Throwable{
		String fileGroup_ValidPath = "C:\\Users\\Public\\Pictures\\Sample Pictures";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_FileGroupText"), fileGroup_ValidPath);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8856_AddWorkgroup_ViewingDays_GreaterThan_DataRetentionDays_Error() throws Throwable{
		String dataRetentionDays = "365";
		String viewingDays = "400";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRetentionDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_ViewingDaysText"), viewingDays);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		BaseUI.verifyElementAppears(Locator.lookupElement("wrkgrp_ViewingDaysText_Error"));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8856_AddWorkgroup_ViewingDays_EqualTo_DataRetentionDays_NoError() throws Throwable{
		String dataRetentionDays = "365";
		String viewingDays = "365";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRetentionDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_ViewingDaysText"), viewingDays);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8856_AddWorkgroup_ViewingDays_LessThan_DataRetentionDays_NoError() throws Throwable{
		String dataRetentionDays = "365";
		String viewingDays = "300";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRetentionDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_ViewingDaysText"), viewingDays);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8853_AddWorkgroup_MaxSearchDays_GreaterThan_DataRetentionDays_Error() throws Throwable{
		String dataRetentionDays = "365";
		String maxSearchDays = "400";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRetentionDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_MaxSearchDays"), maxSearchDays);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		BaseUI.verifyElementAppears(Locator.lookupElement("wrkgrp_MaxSearchDays_Error"));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8853_AddWorkgroup_MaxSearchDays_EqualTo_DataRetentionDays_NoError() throws Throwable{
		String dataRetentionDays = "365";
		String maxSearchDays = "365";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRetentionDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_MaxSearchDays"), maxSearchDays);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@Test(groups = { "all_Tests" })
	public void PT8853_AddWorkgroup_MaxSearchDays_LessThan_DataRetentionDays_NoError() throws Throwable{
		String dataRetentionDays = "365";
		String maxSearchDays = "300";
		Admin_Workgroups.launch_AddWorkgroupSettingsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_AddWorkgroupDefaultSettings(testWorkGroup_BankName, testWorkGroup_WorkgroupID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName, testWorkGroup_ShortName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRetentionDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_MaxSearchDays"), maxSearchDays);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		WebElement elementName = Admin_Workgroups.workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name", testWorkGroup_LongName);
		BaseUI.verifyElementAppears(elementName);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
	}
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		if(BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}
	}
	
	
	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Throwable {

		try {			
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("586489", "12321412", "-1");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database("852745", "1159511", "-1");
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
