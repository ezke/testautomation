package r360.tests_ignored.workgroups;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AssignWorkgroupsModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class WorkgroupMaint_UserPermission_ViewWorkroupMaint_Enabled extends BaseTest {
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";
	
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode(testEntity);
	
	}
	
	@Test
	public void PT1306_WorkgroupMaint_Verify_EntityAssignmentTab_Displayed() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("workgroups_EntityAssignment_Tab"));
		Admin_Workgroups.navigate_to_EntityAssignment_Tab();
	}
	
	@Test
	public void PT1302_WorkgroupMaint_Verify_AssignWorkgroup_Displayed() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("workgroups_EntityAssignment_Tab"));
		Admin_Workgroups.navigate_to_EntityAssignment_Tab();
		BaseUI.verifyElementAppears(Locator.lookupElement("workgroups_Assign_AssignWorkGroupsButton"));
		BaseUI.click(Locator.lookupElement("workgroups_Assign_AssignWorkGroupsButton"));
		Thread.sleep(2000);
		BaseUI.verifyElementAppears(Locator.lookupElement("wrkgrp_Assign_SaveButton"));
		Admin_Workgroups_AssignWorkgroupsModal.cancel_changes();
		BaseUI.verifyElementAppears(Locator.lookupElement("workgroups_Assign_RemoveWorkgroupLink"));
		
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		// All tests should start at the same starting point.
		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode(testEntity);

	}
	
	
	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
