package r360.tests_ignored.workgroups;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class WorkgroupMaint_DataEntryFields_AddAndDelete extends BaseTest {
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";

	private String testWorkGroup_LongName = "TestWorkgroup_DataEntryTest";

	private String dataEntryField_Template = "Blank Data Entry Field";
	private Boolean dataEntryField_Active = true;
	private String dataEntryField_FieldName = "AutomationDataEntry";
	private String dataEntryField_UILabel = "AutomationDataEntry";
	private String dataEntryField_ItemType = "Payment";
	private String dataEntryField_DisplayOrder = "18";
	private String dataEntryField_DataType = "Floating Point";
	private String dataEntryField_PaymentSource = "AutomationACH";

	private String testWorkgroup_DataEntryTest_WorkgroupID = "745852";
	private String testWorkgroup_BankID = "12321412";

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);

	}
	
	@Test
	public void PT1263_DataEntryField_AddNewField() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, dataEntryField_FieldName, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal
				.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("UI Label",dataEntryField_UILabel, true);
		// deleting data entry fields from database
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, dataEntryField_FieldName);
	}

	@Test
	public void PT1175_AddDataEntryFields_FieldName_Alphabets() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, dataEntryField_FieldName, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox"), true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal
				.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name",dataEntryField_FieldName, true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, dataEntryField_FieldName);

	}

	@Test
	public void PT1381_DataEntryFieldSetup_Add_FirstDataSet_Active() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, dataEntryField_FieldName, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox"), true);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal
				.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("UI Label",dataEntryField_UILabel, true);
		// deleting data entry fields from database
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, dataEntryField_FieldName);
	}

	@Test
	public void PT1382_DataEntryFieldSetup_Add_SecondDataSet_Active() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, dataEntryField_FieldName, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, "Text", dataEntryField_PaymentSource);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox"), true);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("UI Label",dataEntryField_UILabel, true);
		// deleting data entry fields from database
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, dataEntryField_FieldName);
	}

	@Test
	public void PT1383_DataEntryFieldSetup_Add_ThirdDataSet_Inactive() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, false,
				dataEntryField_FieldName, dataEntryField_UILabel, dataEntryField_ItemType, dataEntryField_DisplayOrder,
				"Currency", dataEntryField_PaymentSource);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal
				.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_HideInactiveField_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("UI Label",dataEntryField_UILabel, false);
		// deleting data entry fields from database
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, dataEntryField_FieldName);
	}

	@Test
	public void PT1170_AddDataEntryField_Verify_NameField_CanNotHave_DuplicateEntries() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "22", "Currency",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "The field name entered already exists.";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
	}

	@Test
	public void PT1171_AddDataEntryField_Verify_UILabel_CanNotHave_DuplicateEntries_DifferentDataType()
			throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField1", dataEntryField_ItemType, "22", "Currency",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "This UI Label already exists and has data type Text, 2 fields can not have the same UI Label and different data types";
		BaseUI.baseStringCompare("UI Label", expectedError, fieldNameError);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
	}

	@Test
	public void PT1176_AddDataEntryField_Verify_FieldName_AlphaNumerics() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test123", "UIField1", dataEntryField_ItemType, "1", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal
				.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DataEntryFields_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test123",true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test123");
	}

	@Test
	public void PT1183_AddDataEntryField_UILabel_DifferentDataType_And_DifferentWorkgroup() throws Throwable {
		String differentWorkgroup = "TestWorkgroup_AddDeleteTest";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test3", "UIField2", dataEntryField_ItemType, "23", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(differentWorkgroup);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test4", "UIField2", dataEntryField_ItemType, "24", "Currency",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test3");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("852745", testWorkgroup_BankID,
				"Test4");
	}

	@Test
	public void PT1179_AddDataEntryField_FieldName_SpecialCharacters_Error() throws Exception {
		String fieldName_SpecialCharacters = "Test@*&)";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, fieldName_SpecialCharacters, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "Required. (Allows alphanumeric, hyphens and underscores.)";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
	}

	@Test
	public void PT1177_AddDataEntryField_FieldName_With_Underscore() throws Throwable {
		String fieldName_WithUnderscore = "Test_123";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, fieldName_WithUnderscore, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name",fieldName_WithUnderscore, true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, fieldName_WithUnderscore);
	}

	@Test
	public void PT1178_AddDataEntryField_FieldName_With_Hyphen() throws Throwable {
		String fieldName_WithHyphen = "Test-123";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, fieldName_WithHyphen, dataEntryField_UILabel, dataEntryField_ItemType,
				dataEntryField_DisplayOrder, dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name",fieldName_WithHyphen, true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, fieldName_WithHyphen);
	}

	@Test
	public void PT1182_AddDataEntryField_SameUILabel_DifferentDataType_And_DifferentBankAndWorkgroup()
			throws Throwable {
		String differentWorkgroup = "TestWorkgroup_DifferentBank";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test5", "UIField3", dataEntryField_ItemType, "25", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(differentWorkgroup);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test6", "UIField3", dataEntryField_ItemType, "26", "Currency",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test5");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("526456", "1159511", "Test6");
	}

	@Test
	public void PT1174_AddDataEntryField_SameUILabel_DifferentDataType_And_PaymentSource_NoError() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField1", dataEntryField_ItemType, "22", "Currency",
				"AutomationImageRPS");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}

	@Test
	public void PT1172_AddDataEntryField_SameUILabel_DifferentDataType_And_ItemType_NoError() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField1", "Invoice", "22", "Currency", dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}

	@Test
	public void PT1173_AddDataEntryField_SameUILabel_DifferentDataType_And_MarkSense_Error() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",
				dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField1", dataEntryField_ItemType, "22", "Currency",dataEntryField_PaymentSource);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "This UI Label already exists and has data type Text, 2 fields can not have the same UI Label and different data types";
		BaseUI.baseStringCompare("UI Label", expectedError, fieldNameError);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");

	}

	@Test
	public void PT1166_AddDataEntryField_DifferentDataType_SameWorkgroup_Edit_UILabel_Error() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField2", dataEntryField_ItemType, "22", "Currency",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test2");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), "UIField1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();

		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "This UI Label already exists and has data type Text, 2 fields can not have the same UI Label and different data types";
		BaseUI.baseStringCompare("UI Label", expectedError, fieldNameError);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}

	// adds two data set with different data type in two different workgroup with same bankID
	// verifies editing UILabel does not throw an error
	@Test
	public void PT1181_AddDataEntryFields_DifferentDataType_And_WorkGroup_Edit_UILabel_NoError() throws Throwable {
		String differentWorkgroup = "TestWorkgroup_AddDeleteTest";
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal
				.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(differentWorkgroup);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField2", dataEntryField_ItemType, "22", "Currency",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(differentWorkgroup);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test1");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), "UIField1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("852745", testWorkgroup_BankID,"Test1");
	}

	@Test
	public void PT3460_AddDataEntryFields_SameUILabel_And_FieldNames_Different_ItemType() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Floating Point",dataEntryField_PaymentSource);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, false, "Test1",
				"UIField1", "Invoice", "21", "Floating Point", dataEntryField_PaymentSource);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test1", true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Item Type", "Invoice",
				true);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_HideInactiveField_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test1", true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Item Type", "Invoice",false);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
	}

	// adds two data set with different data type in two different workgroup and BankID
	// verifies editing UILabel does not throw an error
	@Test
	public void PT1180_AddDataEntryFields_DifferentDataType_WorkGroup_BankID_Edit_UILabel_NoError() throws Throwable {
		String differentWorkgroupAndBankID = "TestWorkgroup_DifferentBank";

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(differentWorkgroupAndBankID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField2", dataEntryField_ItemType, "22", "Currency",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(differentWorkgroupAndBankID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test1");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), "UIField1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("526456", "1159511", "Test1");
	}
	
	@Test
	public void PT3461_AddDataEntryFields_SameFieldNames_DifferentUILabel_And_ItemTypes() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", dataEntryField_DataType,dataEntryField_PaymentSource);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox"), true);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				false, "Test1", "UIField2", "Invoice", "21", dataEntryField_DataType,dataEntryField_PaymentSource);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_HideInactiveField_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test1", true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("UI Label", "UIField2", false);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
		
	}

	@Test
	public void PT1169_AddDataEntryFields_DifferentDataTypeAndPaymentSource_Edit_UILabel_NoError() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField2", dataEntryField_ItemType, "22", "Currency","AutomationImageRPS");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test2");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), "UIField1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}

	@Test
	public void PT1167_AddDataEntryFields_DifferentDataTypeAndItemType_Edit_UILabel_NoError() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField2", "Invoice", "22", "Currency", dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test2");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), "UIField1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}

	@Test
	public void PT3459_AddDataEntryFields_DifferentFieldNames_SameUILabels_And_ItemTypes() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", dataEntryField_DataType,dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template, false, "Test2",
				"UIField1", dataEntryField_ItemType, "22", dataEntryField_DataType, dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_HideInactiveField_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test1", true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test2",false);
		Admin_Workgroups_AddingEditing_WorkgroupModal
				.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}

	@Test
	public void PT1168_AddDataEntryFields_DifferentDataTypeAndMarkSense_Edit_UILabel_NoError() throws Throwable {
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();

		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test2", "UIField2", dataEntryField_ItemType, "22", "Currency",dataEntryField_PaymentSource);
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_MarkSense_Yes"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test2");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), "UIField1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}
	
	@Test
	public void PT3458_AddDataEntryFields_SameUILabels_DifferentFiledNames_And_ItemTypes() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", dataEntryField_DataType,dataEntryField_PaymentSource);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox"), true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				false, "Test2", "UIField1", "Invoice", "21", dataEntryField_DataType,dataEntryField_PaymentSource);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_HideInactiveField_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test1", true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test2", false);
		
		BaseUI.uncheckCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_HideInactiveField_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test1", true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.verifyEntryExists_OR_DoesntExist("Name", "Test2", true);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
	}
	
	@Test
	public void PT1378_AddDataEntryFields_PostDepositExceptionRules_Checkbox_Saves_WhenSelected() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields(dataEntryField_Template,
				dataEntryField_Active, "Test1", "UIField1", dataEntryField_ItemType, "21", "Text",dataEntryField_PaymentSource);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"), false);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test1");
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("Test1");
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"), true);
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
	}
	
	@Test
	public void PT1379_EditDataEntryFields_PostDepositExceptionRules_Checkbox_Saves_WhenSelected() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("AutomationTest");
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"), false);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("AutomationTest");
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"), false);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("AutomationTest");
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"), true);
		BaseUI.uncheckCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
	}
	
	@Test
	public void PT1380_DataEntryFields_PostDepositExceptionRules_Checkbox_DisabledByDefault() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName("AutomationTest");
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"), false);
		
	}
	
	

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Throwable {

		try {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
			
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test1");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test2");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("526456", "1159511", "Test6");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test5");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test-123");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test_123");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test3");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("852745", testWorkgroup_BankID,"Test4");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, "Test123");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase(testWorkgroup_DataEntryTest_WorkgroupID, testWorkgroup_BankID, dataEntryField_FieldName);
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("852745", testWorkgroup_BankID,"Test1");
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDatabase("526456", "1159511", "Test1");
			
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
