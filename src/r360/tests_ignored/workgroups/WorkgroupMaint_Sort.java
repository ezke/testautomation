package r360.tests_ignored.workgroups;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.Browser;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class WorkgroupMaint_Sort extends BaseTest {
	public static final String workGroupSettings_GridID = "workgroupMaintenance";
	public static Pagination workGroupSettings_GRID = new Pagination(workGroupSettings_GridID);
	private String testEntityWithChildrenNodes = "Fondy Bank";
	private String testEntity = "Fondy Inc";

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();

	}

	@Test
	public void PT8863_WorkgroupMaintenance_Sort_LongName_DefaultsToAscending() {
		workGroupSettings_GRID.verifyColumnAscending_alphabetical("Long Name");
	}

	@Test
	public void PT8863_WorkgroupMaintenance_Sort_LoginName_Ascending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Bank");
		workGroupSettings_GRID.click_sort_ByColumnName("Long Name");
		workGroupSettings_GRID.verifyColumnAscending_alphabetical("Long Name");

	}

	@Test
	public void PT8863_WorkgroupMaintenance_Sort_LongName_Descending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Long Name");
		workGroupSettings_GRID.verifyColumnDescending_alphabetical("Long Name");

	}

	@Test
	public void PT8863_WorkgroupMaintennace_Sort_Bank_Ascending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Bank");
		workGroupSettings_GRID.verifyColumnAscending_alphabetical("Bank");
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Sort_Bank_Descending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Bank");
		workGroupSettings_GRID.verifyColumnDescending_alphabetical("Bank");
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Sort_WorkgroupID_Ascending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Workgroup ID");
		workGroupSettings_GRID.verifyColumnAscending_numeric("Workgroup ID");
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Sort_WorkgroupID_Descending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Workgroup ID");
		workGroupSettings_GRID.verifyColumnDescending_numeric("Workgroup ID");
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Sort_Active_Ascending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Active");
		workGroupSettings_GRID.verifyColumnAscending("Active");
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Sort_Active_Descending() throws Exception {
		workGroupSettings_GRID.click_sort_ByColumnName("Active");
		workGroupSettings_GRID.verifyColumnDescending("Active");
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_NextPage() throws Exception {
		workGroupSettings_GRID.click_next();
		workGroupSettings_GRID.verifyFirstButtonEnabled();
		workGroupSettings_GRID.verifyPreviousButtonEnabled();
		workGroupSettings_GRID.verifyNextButtonEnabled();
		workGroupSettings_GRID.verifyLastButtonEnabled();

	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_FirstPage() throws Exception {
		workGroupSettings_GRID.click_last();
		workGroupSettings_GRID.click_first();
		workGroupSettings_GRID.verifyFirstButtonDisabled();
		workGroupSettings_GRID.verifyPreviousButtonDisabled();
		workGroupSettings_GRID.verifyNextButtonEnabled();
		workGroupSettings_GRID.verifyLastButtonEnabled();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_LastPage() throws Exception {
		workGroupSettings_GRID.click_last();
		workGroupSettings_GRID.verifyFirstButtonEnabled();
		workGroupSettings_GRID.verifyPreviousButtonEnabled();
		workGroupSettings_GRID.verifyNextButtonDisabled();
		workGroupSettings_GRID.verifyLastButtonDisabled();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_ShowAll() throws Exception {
		workGroupSettings_GRID.show_All();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_ShowAuto() throws Exception {
		workGroupSettings_GRID.show_Auto();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_Show_10() throws Exception {
		workGroupSettings_GRID.show_10();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_Show_25() throws Exception {
		workGroupSettings_GRID.show_25();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_Show_50() throws Exception {
		workGroupSettings_GRID.show_50();
	}

	@Test
	public void PT8863_WorkgroupMaintennace_Navigation_MultiPage_Show_100() throws Exception {
		workGroupSettings_GRID.show_100();
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);
		workGroupSettings_GRID.click_first();
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
