package r360.tests_ignored.workgroups;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class WorkgroupMaint_DataEntryFields_DifferentTemplate_Tests extends BaseTest {
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";

	private String testWorkGroup_LongName = "DataEntryFields_DifferentTemplate";
	private String workgroupID = "458794";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);

	}
	
	@Test(groups = { "all_Tests" })
	public void PT1348_DataEntryField_Add_WireTemplate() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields_NonBlankTemplate("Standard Wire - BAI2", "AutomationACH");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal
		.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		ArrayList<String> list = Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.getAvailableEntries_viaScroll("UI Label");
		BaseUI.verify_true_AndLog(list.size()>0, "Data Entry Fields contained values", "Data Entry Fields did not contain any values");
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDataBase_ForNonBlankTemplate(workgroupID);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1352_DataEntryField_WireTemplate_PaymentSource_Blank_Error() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.template_Dropdown.select_EntityValue("Standard Wire - BAI2");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PaymentSource_ACH_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "Selection Required.";
		BaseUI.baseStringCompare("FieldName", expectedError, fieldNameError);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT1347_DataEntryField_Add_ACHTemplate() throws Throwable{
		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_DataEntryFields_AddButton();
		Admin_Workgroups_AddingEditing_WorkgroupModal.add_DataEntryFields_NonBlankTemplate("ACH", "AutomationACH");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups_AddingEditing_WorkgroupModal
		.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
		ArrayList<String> list = Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.getAvailableEntries_viaScroll("UI Label");
		BaseUI.verify_true_AndLog(list.size()>0, "Data Entry Fields contained values", "Data Entry Fields did not contain any values");
		
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDataBase_ForNonBlankTemplate(workgroupID);
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}

		Admin_Workgroups_AddingEditing_WorkgroupModal.launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(testWorkGroup_LongName);
	}
	
	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Throwable {

		try {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
			Admin_Workgroups_AddingEditing_WorkgroupModal.delete_DataEntry_FromDataBase_ForNonBlankTemplate(workgroupID);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
	
}
