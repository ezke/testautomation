package r360.tests_ignored.workgroups;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class WorkgroupMaint_Updates extends BaseTest {

	public static final String workGroupSettings_GridID = "workgroupMaintenance";
	public static Pagination workGroupSettings_GRID = new Pagination(workGroupSettings_GridID);
	private String testEntity = "workGroupTests_Entity";
	private String testEntityWithChildrenNodes = "AutomationTest";

	private String testWorkGroup_LongName = "TestWorkGroup_NameChange";
	private String testWorkGroup_LongName_ChangeTo = "new_WorkgroupName";
	private String testWorkGroup_ShortName = "NameChange";
	private String testWorkGroup_ShortName_changeTo = "Change";
	private String testWorkGroup_DataRetentionDays = "365";
	private String testWorkGroup_ImageRetentionDays = "365";
	private String testWorkGroup_FileGroup = "";
	private String testWorkGroup_ViewingDays = "255";
	private String testWorkGroup_MaxSearchDays = "255";
	private String testWorkGroup_DisplayBatchID = "Use Entity Setting";
	private String testWorkGroup_PaymentImageDisplay = "Use Entity Setting";
	private String testWorkGroup_DocumentImageDisplay = "Use Entity Setting";
	private Boolean testWorkGroup_isHOA = false;

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");

		if (BaseUI.pageSourceContainsString(testWorkGroup_LongName_ChangeTo)) {
			Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName_ChangeTo);
		} else {
			Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		}
		Admin_Workgroups_AddingEditing_WorkgroupModal.reset_Modal(testWorkGroup_LongName, testWorkGroup_ShortName,
				testWorkGroup_DataRetentionDays, testWorkGroup_ImageRetentionDays, testWorkGroup_FileGroup,
				testWorkGroup_ViewingDays, testWorkGroup_MaxSearchDays, testWorkGroup_DisplayBatchID,
				testWorkGroup_PaymentImageDisplay, testWorkGroup_DocumentImageDisplay, testWorkGroup_isHOA);
	}

	@Test
	public void TC134026_Update_WorkgroupLongName() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"),
				testWorkGroup_LongName_ChangeTo);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementName = workGroupSettings_GRID.get_row_ByColumnName_And_ColumnMatchingText("Long Name",
				testWorkGroup_LongName_ChangeTo);
		BaseUI.verifyElementAppears(elementName);

	}

	@Test
	public void PT1283_Update_WorkgroupShortName() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"),
				testWorkGroup_ShortName_changeTo);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_ShortNameText",
				testWorkGroup_ShortName_changeTo);
	}

	@Test
	public void PT1278_Update_workgroupActiveStatus() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.uncheckCheckbox(Locator.lookupElement("wrkgrp_IsActive_Checkbox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_IsActive_Checkbox"), false);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_IsActive_Checkbox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_IsActive_Checkbox"), true);
	}

	@Test
	public void PT1279_Update_WorkgroupDataRetentionDays() throws Throwable {
		String valueToEnter = "400";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), valueToEnter);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_DataRentionDaysText",
				valueToEnter);
	}

	@Test
	public void PT1281_Update_WorkgroupImageRetentionDays() throws Throwable {
		String valueToEnter = "245";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ImageRentionDaysText"), valueToEnter);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_ImageRentionDaysText",
				valueToEnter);
	}

	@Test
	public void PT1260_DDAAssociation_VerifyRTList() throws Throwable {
		String columnName = "RT";
		String columnText1 = "000000089";
		String columnText2 = "000000090";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist(columnName, columnText1,
				true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist(columnName, columnText2,
				true);
	}

	@Test
	public void PT1260_DDAAssociation_VerifyDDAList() throws Throwable {
		String columnName = "DDA";
		String columnText1 = "00000000980980982";
		String columnText2 = "00000000980980980";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist(columnName, columnText1,
				true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist(columnName, columnText2,
				true);
	}

	@Test
	public void PT1253_DDAAssociation_AssociateNewDDA() throws Throwable {
		String columnName = "DDA";
		String rtNumber = "156423789";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.associate_RT_And_DDA_ToWorkgroup(rtNumber, ddaNumber);
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist("RT", rtNumber, true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist("DDA", ddaNumber, true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_EditWorkGroup_RT_And_DDA(columnName, ddaNumber);
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist("DDA", ddaNumber, false);
	}

	@Test
	public void PT1257_DDAAssociation_RemoveDDA() throws Throwable {
		String columnName = "DDA";
		String rtNumber = "456852145";
		String ddaNumber = "9634567891478526";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.associate_RT_And_DDA_ToWorkgroup(rtNumber, ddaNumber);
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.delete_EditWorkGroup_RT_And_DDA(columnName, ddaNumber);
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist("DDA", ddaNumber, false);
	}

	@Test
	public void PT1280_UpdateWorkgroup_FileGroup() throws Throwable {
		String expectedFileGroup = "C:\\Users\\Public\\Pictures\\Sample Pictures";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_FileGroupText"), expectedFileGroup);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_FileGroupText",
				expectedFileGroup);
	}

	@Test
	public void PT1277_UpdateWorkgroup_VerifyLongName_And_Shortname_RequiredFields() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"), "");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"), "");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementLongName = Locator.lookupElement("wrkgrp_AddEdit_LongName_Error");
		String longNameErrorMessage = elementLongName.getText();
		String expectedError = "Required. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)";
		BaseUI.baseStringCompare("LongName", expectedError, longNameErrorMessage);
		WebElement elementShortName = Locator.lookupElement("wrkgrp_AddEdit_ShortName_Error");
		String shortNameErrorMessage = elementShortName.getText();
		BaseUI.baseStringCompare("LongName", expectedError, shortNameErrorMessage);
	}

	@Test
	public void PT1277_UpdateWorkgroup_VerifyLongName_RequiredFields() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"), "");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"), testWorkGroup_ShortName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementLongName = Locator.lookupElement("wrkgrp_AddEdit_LongName_Error");
		String longNmaeErrorMessage = elementLongName.getText();
		String expectedError = "Required. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)";
		BaseUI.baseStringCompare("LongName", expectedError, longNmaeErrorMessage);
	}

	@Test
	public void PT1277_UpdateWorkgroup_VerifyShortName_RequiredFields() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"), testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"), "");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		WebElement elementShortName = Locator.lookupElement("wrkgrp_AddEdit_ShortName_Error");
		String shortNameErrorMessage = elementShortName.getText();
		String expectedError = "Required. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)";
		BaseUI.baseStringCompare("LongName", expectedError, shortNameErrorMessage);
	}

	@Test
	public void PT1258_DDAAssociation_RTField_2DigitNumeric_Validation() throws Throwable {
		String rtNumber = "25";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementRT = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/T_Error");
		String errorMessage = elementRT.getText();
		String expected_RTError = "The RT must be a 9-digit value.";
		BaseUI.baseStringCompare("RT", expected_RTError, errorMessage);
	}

	@Test
	public void PT1258_DDAAssociation_RTField_AlphaCharacters_Validation() throws Throwable {
		String rtNumber = "abc";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementRT = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/T_Error");
		String errorMessage = elementRT.getText();
		String expected_RTError = "The RT must be a 9-digit value.";
		BaseUI.baseStringCompare("RT", expected_RTError, errorMessage);
	}

	@Test
	public void PT1258_DDAAssociation_RTField_SpecialCharacters_Validation() throws Throwable {
		String rtNumber = "@#$";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementRT = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/T_Error");
		String errorMessage = elementRT.getText();
		String expected_RTError = "The RT must be a 9-digit value.";
		BaseUI.baseStringCompare("RT", expected_RTError, errorMessage);
	}

	@Test
	public void PT1258_DDAAssociation_RTField_ClearField_Validation() throws Throwable {
		String rtNumber = "";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementRT = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/T_Error");
		String errorMessage = elementRT.getText();
		String expected_RTError = "The RT must be a 9-digit value.";
		BaseUI.baseStringCompare("RT", expected_RTError, errorMessage);
	}

	@Test
	public void PT1258_DDAAssociation_RTField_8DigitNumeric_Validation() throws Throwable {
		String rtNumber = "56489745";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementRT = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/T_Error");
		String errorMessage = elementRT.getText();
		String expected_RTError = "The RT must be a 9-digit value.";
		BaseUI.baseStringCompare("RT", expected_RTError, errorMessage);
	}

	@Test
	public void PT1258_DDAAssociation_RTField_9DigitNumeric_And_Cancel_Validation() throws Throwable {
		String rtNumber = "564897455";
		String ddaNumber = "15642385274145678";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist("RT", rtNumber, false);
	}

	@Test
	public void PT1254_DDAAssociation_DDAField_36DigitNumeric_Validation() throws Throwable {
		String rtNumber = "564897455";
		String ddaNumber = "154645871203654789102546789451278458";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/TText"), rtNumber);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDAText"), ddaNumber);
		String ddaNumberField = BaseUI
				.getTextFromField(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDAText", ddaNumber, null));
		BaseUI.verify_true_AndLog(ddaNumberField.length() <= 35, "DDA length was less than or equal to 35.",
				"DDA length was NOT less than or equal to 35.");
	}

	@Test
	public void PT1254_DDAAssociation_DDAField_AlphaCharacters_Validation() throws Throwable {
		String rtNumber = "564897455";
		String ddaNumber = "absfrghur";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementDDA = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDA_Error");
		String errorMessage = elementDDA.getText();
		String expected_DDAError = "The DDA must be a numeric value.";
		BaseUI.baseStringCompare("DDA", expected_DDAError, errorMessage);
	}

	@Test
	public void PT1254_DDAAssociation_DDAField_SpecialCharacters_Validation() throws Throwable {
		String rtNumber = "564897455";
		String ddaNumber = "@#$%^&*";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementDDA = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDA_Error");
		String errorMessage = elementDDA.getText();
		String expected_DDAError = "The DDA must be a numeric value.";
		BaseUI.baseStringCompare("DDA", expected_DDAError, errorMessage);
	}

	@Test
	public void PT1254_DDAAssociation_DDAField_10DigitNumeric_And_Cancel_Validation() throws Throwable {
		String rtNumber = "564897455";
		String ddaNumber = "1564238527";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyEntryExists_OR_DoesntExist("DDA", ddaNumber, false);
	}

	@Test
	public void PT1259_DDAAssociation_VerifyDDA_Associated_WithOnly_OneWorkGroup() throws Throwable {
		String rtNumber = "568975456";
		String ddaNumber = "5678945812";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementDDA = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDA_Error");
		String errorMessage = elementDDA.getText();
		String expected_DDAError = "The DDA specified is already associated with a different workgroup and must be removed before it can be associated to this workgroup.";
		BaseUI.baseStringCompare("DDA", expected_DDAError, errorMessage);
	}

	@Test
	public void PT1261_DDAAssociation_VerifyDuplicateDDA_CannotBe_Associated_ToWorkGroup() throws Throwable {
		String rtNumber = "000000089";
		String ddaNumber = "00000000980980982";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		WebElement elementDDA = Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDA_Error");
		String errorMessage = elementDDA.getText();
		String expected_DDAError = "The DDA specified is already associated with the workgroup.";
		BaseUI.baseStringCompare("DDA", expected_DDAError, errorMessage);
	}

	@Test
	public void PT1256_DDAAssociation_DDAList_SortBy_RT() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnAscending_numeric("RT");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnAscending_numeric("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("RT");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("RT");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnDescending_numeric("RT");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnDescending_numeric("DDA");
	}

	@Test
	public void PT1256_DDAAssociation_DDAList_SortBy_DDA() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DDAAssociation_Tab();
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnAscending("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnAscending_numeric("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.click_sort_ByColumnName("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnDescending("DDA");
		Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid.verifyColumnDescending_numeric("DDA");
	}

	@Test
	public void PT8904_Update_WorkgroupViewingDays() throws Throwable {
		String valueToEnter = "225";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_ViewingDaysText"), valueToEnter);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_ViewingDaysText", valueToEnter);
	}

	@Test
	public void PT8897_Update_WorkgroupMaximumSearchDays() throws Throwable {
		String valueToEnter = "225";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_MaxSearchDays"), valueToEnter);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_MaxSearchDays", valueToEnter);
	}

	@Test
	public void PT8892_Update_WorkgroupPaymentImageDisplayMode() throws Throwable {
		String paymentImageDisplay = "Front and Back";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.paymentImageDisplay_Dropdown
				.select_EntityValue(paymentImageDisplay);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.paymentImageDisplay_Dropdown
				.verify_SelectedDropdownValue(paymentImageDisplay);
	}

	@Test
	public void PT8894_Update_WorkgroupDocumentImageDisplayMode() throws Throwable {
		String documentImageDisplay = "Front and Back";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.documentImageDisplay_Dropdown
				.select_EntityValue(documentImageDisplay);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.documentImageDisplay_Dropdown
				.verify_SelectedDropdownValue(documentImageDisplay);
	}

	@Test
	public void PT8895_Update_WorkgroupVerifyHOA() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_IsHOA_Checkbox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_IsHOA_Checkbox"), true);
	}

	@Test
	public void PT8893_Update_WorkgroupDisplayBatchIDMode() throws Throwable {
		String displayBatchID = "Yes";
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.displayBatchID_Dropdown.select_EntityValue(displayBatchID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.displayBatchID_Dropdown
				.verify_SelectedDropdownValue(displayBatchID);
	}

	@Test
	public void PT1330_Update_VerifyWorkgroupID_ReadOnly() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyElementDisabled(Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID"));
	}

	@Test
	public void PT8862_Update_verify_AllFields_Enabled_Or_Disabled() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.verifyElementDisabled(Locator.lookupElement("wrkgrp_AddEdit_BankText"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_AddEdit_ImageRentionDaysText"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_AddEdit_FileGroupText"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_ViewingDaysText"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_MaxSearchDays"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("wrkgrp_IsHOA_Checkbox"));
	}

	@Test
	public void PT8860_UpdateAllFields_And_CancelChanges() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_WorkgroupNames(testWorkGroup_LongName_ChangeTo,
				testWorkGroup_ShortName_changeTo);
		BaseUI.uncheckCheckbox(Locator.lookupElement("wrkgrp_IsActive_Checkbox"));
		Admin_Workgroups_AddingEditing_WorkgroupModal.set_Workgroup_ReportDisplaySettings("225", "225",
				testWorkGroup_DisplayBatchID, testWorkGroup_PaymentImageDisplay, testWorkGroup_DocumentImageDisplay,
				true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_LongNameText",
				testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_ShortNameText",
				testWorkGroup_ShortName);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("wrkgrp_IsActive_Checkbox"), true);
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_DataRentionDaysText", "365");
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_AddEdit_ImageRentionDaysText",
				"365");
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_ViewingDaysText", "255");
		Admin_Workgroups_AddingEditing_WorkgroupModal.verify_Textbox_Value("wrkgrp_MaxSearchDays", "255");
	}

	@Test
	public void PT1276_UpdateWorkgroup_UpdateAllGeneralFields() throws Throwable {
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.reset_Modal(testWorkGroup_LongName_ChangeTo, "update", "400",
				"375", "C:\\Users\\Public\\Pictures\\Sample Pictures", "220", "220", "Yes", "Front and Back",
				"Front and Back", true);
		Admin_Workgroups.workGroupSettings_GRID.verifyEntryExists_OR_DoesntExist("Long Name",
				testWorkGroup_LongName_ChangeTo, true);
	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(ITestResult result) throws Throwable {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}

		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");

		if (BaseUI.pageSourceContainsString(testWorkGroup_LongName_ChangeTo)) {
			Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName_ChangeTo);
		} else {
			Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		}
		Admin_Workgroups_AddingEditing_WorkgroupModal.reset_Modal(testWorkGroup_LongName, testWorkGroup_ShortName,
				testWorkGroup_DataRetentionDays, testWorkGroup_ImageRetentionDays, testWorkGroup_FileGroup,
				testWorkGroup_ViewingDays, testWorkGroup_MaxSearchDays, testWorkGroup_DisplayBatchID,
				testWorkGroup_PaymentImageDisplay, testWorkGroup_DocumentImageDisplay, testWorkGroup_isHOA);
	}

	@AfterClass(alwaysRun = true)
	public void fixtureTearDown() throws Exception {

		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
