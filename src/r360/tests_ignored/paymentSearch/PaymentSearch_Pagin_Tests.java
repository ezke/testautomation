package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PaymentSearch_Pagin_Tests extends BaseTest {
	TableData results;
	private String workgroup = "2234322 - Automation-Filter";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
		PaymentSearch.click_Search();
		
		// Extract table data.
		results = PaymentSearch.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Payment Search Results.",
				"Payment Search Results were empty.");
	
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4180_PaymentSearch_SearchResults_PreviousButton_Appears() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_Pagin_PreviousButton"));
		
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4180_PaymentSearch_SearchResults_NextButton_Appears() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_Pagin_NextButton"));
		
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4180_PaymentSearch_SearchResults_PreviousButton_Enabled() throws Exception{
		PaymentSearch.click_Page_ByPageNumber(2);
		BaseUI.verifyElementEnabled(Locator.lookupElement("paymentSearch_Pagin_PreviousButton"));
		
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4180_PaymentSearch_SearchResults_NextButton_Enabled() throws Exception{
		BaseUI.verifyElementEnabled(Locator.lookupElement("paymentSearch_Pagin_NextButton"));

	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT4180_PaymentSearch_SearchResults_DefaultResults_10() throws Exception{
		BaseUI.verify_true_AndLog(results.data.size() >= 10, "Payment Search Entries were >=10 in Search Results.",
				"Payment Search Results were empty.");

	}
	
	@Test(groups = {"all_Tests"})
	public void PT4180_PaymentSearch_SearchResults_SelectShow25_DecreasePageNumber() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_Pagin_ByPageNumber", "2", null));
		PaymentSearch.select_ShowRowsDropdownValue(25);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("paymentSearch_Pagin_ByPageNumber", "2", null));

	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		PaymentSearch.select_ShowRowsDropdownValue(10);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
