package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PaymentSearch_DisplayCheck_Tests extends BaseTest {
	TableData results;
	private String mainWindowHandle;
	
	private String workgroup = "2234322";

	private String date1 = "01/13/2017";
	private String date2 = "01/13/2017";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");

		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
		
		PaymentSearch.paymentTypeDropdown.select_EntityValue("Check");
		PaymentSearch.click_Search();
		
		mainWindowHandle = Browser.driver.getWindowHandle();
		
		results = PaymentSearch.return_SearchResults();
		
		BaseUI.ClickAndSwitchWindow(
				Locator.lookupElement("paymentSearch_ViewPaymentImageLink_ByIndex", "1", null), false, 15000);
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_DepositDate() throws Exception {
		String field = "Deposit Date";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		results.replace_DateFormat_shortened();
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
//	@Test(groups = { "all_Tests", "firefox_only" })
//	public void PT4193_PaymentSearch_ComparePDF_Workgroup() throws Exception {
//		String field = "Workgroup";
//
//		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
//		
//		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
//	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_Batch() throws Exception {
		String field = "Batch";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_Transaction() throws Exception {
		String field = "Transaction";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_BatchID() throws Exception {
		String field = "Batch ID";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_RT() throws Exception {
		String field = "R/T";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_AccountNumber() throws Exception {
		String field = "Account Number";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_CheckTraceRefNumber() throws Exception {
		String field = "Check/Trace/Ref Number";

		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get(field), pdfText);
	}
	
	@Test(groups = { "all_Tests", "firefox_only" })
	public void PT4193_PaymentSearch_ComparePDF_Amount() throws Exception {
		String field = "Amount";
		String pdfText = PaymentSearch.return_Text_FromPDF_Field(field);
		
		BaseUI.baseStringCompare(field, results.data.get(0).get("Payment Amount"), pdfText);
	
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Browser.driver.switchTo().window(mainWindowHandle);
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
