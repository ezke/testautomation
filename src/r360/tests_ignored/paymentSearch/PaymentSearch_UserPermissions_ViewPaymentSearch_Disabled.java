package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PaymentSearch_UserPermissions_ViewPaymentSearch_Disabled extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName_viewPaymentSearchPageDisabled, GlobalVariables.userName_viewPaymentSearchPageDisabled, 
				GlobalVariables.password_viewPaymentSearchPageDisabled);
		Navigation.navigate_openMenu("Search");
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4094_SearchMenu_Displayed() throws Throwable {
		BaseUI.verifyElementAppears(Locator.lookupElement("Navigate_menuByText", "Search", null));
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4094_SearchMenu_ViewPaymentSearch_Disabled() throws Throwable {
		BaseUI.verify_false_AndLog(BaseUI.elementExists("Navigate_menuSubItem", "Search", "Payment Search"), 
				"Search>Payment Search menu item is NOT listed.",
				"Search>Payment Search menu item exists");
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
