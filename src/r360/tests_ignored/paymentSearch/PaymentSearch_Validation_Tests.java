package r360.tests_ignored.paymentSearch;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PaymentSearch_Validation_Tests extends BaseTest {

	TableData results;
	private String workgroup = "2234322 - Automation-Filter";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_Searchbutton() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_SearchButton"));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ClearSearchbutton() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ClearSearchButton"));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_WorkGroup() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Workgroup";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_DepositDate() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Deposit Date";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_BatchID() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Batch ID";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_Batch() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Batch";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_PaymentSource() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Payment Source";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_PaymentType() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Payment Type";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_Transaction() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Transaction";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_PaymentAmount() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Payment Amount";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_RT() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "R/T";
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_AccountNumber() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Account Number";
		WebElement element = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		BaseUI.scroll_to_element(element);
		BaseUI.verifyElementAppears(element);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_CheckTraceRefNumber() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Check/Trace/Ref Number";
		WebElement element = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		BaseUI.scroll_to_element(element);
		BaseUI.verifyElementAppears(element);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_Payer() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "Payer";
		WebElement element = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		BaseUI.scroll_to_element(element);
		BaseUI.verifyElementAppears(element);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4167_PaymentSearch_PageLabels_Validation_ResultPage_DDA() throws Exception {
		PaymentSearch.click_Search();
		String headerText = "DDA";
		WebElement element = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		BaseUI.scroll_to_element(element);
		BaseUI.verifyElementAppears(element);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4165_PaymentSearch_Verify_Clicking_SearchButton_TakesToResultsPage() throws Exception {
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"));
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4165_PaymentSearch_Verify_Clicking_ClearSearchButton_ReturnsDefaultValues() throws Exception {
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		PaymentSearch.click_ClearSearchButton();
		PaymentSearch.verify_DefaultValues_forPaymentSearch();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4164_PaymentSearch_Verify_DateRange_DefaultsToCurrentDate() throws Exception {
		PaymentSearch.click_ClearSearchButton();
		String date1 = BaseUI.getTextFromInputBox(Locator.lookupElement("paymentSearch_fromDate"));
		String date2 = BaseUI.getTextFromInputBox(Locator.lookupElement("paymentSearch_toDate"));
		String todaysDate = ImportData.return_TodaysDate_FormattedForWebsite();

		BaseUI.baseStringCompare("From Date", todaysDate, date1);
		BaseUI.baseStringCompare("To Date", todaysDate, date2);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4164_PaymentSearch_Verify_ResultsPage_DepositDate_MatchPassedInDate() throws Exception {
		PaymentSearch.click_Search();
		PaymentSearch.select_ShowRowsDropdownValue(100);
		PaymentSearch.verify_Results_HaveCorrect_DepositDate(date1);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4168_PaymentSearch_EntityWorkgroup_Verify_DefaultText() throws Exception {
		PaymentSearch.click_ClearSearchButton();
		String expectedText = "Select an Entity or Workgroup";
		String actualText = PaymentSearch.workGroupSelector.return_SelectedText();
		BaseUI.baseStringCompare("Entity/Workgroup", expectedText, actualText);
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4168_PaymentSearch_EntityWorkgroup_Verify_PassedInValue_AfterClickingBreadcrumb() throws Exception {
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		String workgroupSelectorValue = PaymentSearch.workGroupSelector.return_SelectedText();
		BaseUI.baseStringCompare("Work Group Selector Text", "Selected: 2234322 - Automation-Filter", workgroupSelectorValue);
		
	}
	
	
	@Test(groups = { "all_Tests" })
	public void PT4159_PaymentSearch_PaymentSourceDropdown_Verify_PassedInValue_AfterClickingBreadcrumb() throws Exception {
		PaymentSearch.paymentSourceDropdown.select_EntityValue("AutomationACH");
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		PaymentSearch.paymentSourceDropdown.verify_SelectedDropdownValue("AutomationACH");
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4160_PaymentSearch_PaymentTypeDropdown_Verify_PassedInValue_AfterClickingBreadcrumb() throws Exception {
		PaymentSearch.paymentTypeDropdown.select_EntityValue("Check");
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		PaymentSearch.paymentTypeDropdown.verify_SelectedDropdownValue("Check");
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4163_PaymentSearch_SortByDropdown_Verify_PassedInValue_AfterClickingBreadcrumb() throws Exception {
		PaymentSearch.sortByDropdown.select_EntityValue("Batch / Ascending");
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		PaymentSearch.sortByDropdown.verify_SelectedDropdownValue("Batch / Ascending");
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4156_PaymentSearch_BatchLink_ExpectedTitle() throws Exception {
		PaymentSearch.click_Search();
		WebElement firstBatchLink = PaymentSearch.return_Link_ByColumnName_AndColumnRow("Batch", 1);
		BaseUI.verifyElementHasExpectedAttributeValue(firstBatchLink, "title", "View Batch");
		PaymentSearch.click_Cell_Link("Batch", 1);
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4156_PaymentSearch_TransactionLink_ExpectedTitle() throws Exception {
		PaymentSearch.click_Search();
		WebElement firstTransactionLink = PaymentSearch.return_Link_ByColumnName_AndColumnRow("Transaction", 1);
		BaseUI.verifyElementHasExpectedAttributeValue(firstTransactionLink, "title", "View Transaction");
		PaymentSearch.click_Cell_Link("Transaction", 1);
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4156_PaymentSearch_Breadcrumbs_BatchDetail() throws Exception {
		String[] crumbOrder = { "Payment Search", "Search Results", "Batch Detail" };

		PaymentSearch.click_Search();
		PaymentSearch.click_Cell_Link("Batch", 1);
		PaymentSearch.verify_Breadcrumb(crumbOrder);
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4156_PaymentSearch_Breadcrumbs_TransactionDetail() throws Exception {
		String[] crumbOrder = { "Payment Search", "Search Results", "Transaction Detail" };

		PaymentSearch.click_Search();
		PaymentSearch.click_Cell_Link("Transaction", 1);
		PaymentSearch.verify_Breadcrumb(crumbOrder);
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4158_PaymentSearch_SearchResults_ClickTransactionLink_navigatesTo_TransactionDetailPage() throws Exception {
		Integer rowNumber = 1;
		PaymentSearch.click_Search();
		PaymentSearch.verify_ClickingTransaction_TakesYouToTransactionDetails_Page(rowNumber);
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4157_PaymentSearch_SearchResults_ClickBatchLink_navigatesTo_BatchDetailPage() throws Exception {
		Integer rowNumber = 1;
		PaymentSearch.click_Search();
		PaymentSearch.verify_ClickingBatch_TakesYouToBatchDetails_Page(rowNumber);
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4172_PaymentSearch_SearchResults_Verify_ViewPaymentImageIcon() throws Exception {
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_ViewPaymentImageLink_List"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4172_PaymentSearch_SearchResults_ViewPaymentImageLink_ExpectedTitle() throws Exception {
		PaymentSearch.click_Search();
		WebElement firstViewPaymentImageLink = Locator.lookupElement("paymentSearch_ViewPaymentImageLink_ByIndex","1", null);
		BaseUI.verifyElementHasExpectedAttributeValue(firstViewPaymentImageLink, "title", "View Payment Image");
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4217_PaymentSearch_SearchResult_TransactionDetail_ImageLinkAppears() throws Exception {
		
		PaymentSearch.paymentTypeDropdown.select_EntityValue("Check");
		PaymentSearch.click_Search();
		PaymentSearch.click_Cell_Link("Transaction", 1);
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TransactionDetail_PaymentItems_ViewImageLink_ByIndex", "1", null));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4217_PaymentSearch_SearchResult_TransactionDetail_ItemReportLinkAppears() throws Exception {
		
		PaymentSearch.paymentTypeDropdown.select_EntityValue("Check");
		PaymentSearch.click_Search();
		PaymentSearch.click_Cell_Link("Transaction", 1);
		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TransactionDetail_PaymentItems_ViewItemReport_ByIndex", "1", null));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4228_PaymentSearch_ClearSearch_PaymentTypeAndPaymentSource_Returns_ToDefaultValue() throws Exception {
		
		PaymentSearch.paymentTypeDropdown.select_EntityValue("Check");
		PaymentSearch.paymentSourceDropdown.select_EntityValue("ACH");
		PaymentSearch.click_Search();
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		PaymentSearch.click_ClearSearchButton();
		PaymentSearch.paymentTypeDropdown.verify_SelectedDropdownValue("-- All --");
		PaymentSearch.paymentSourceDropdown.verify_SelectedDropdownValue("-- All --");
	
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4228_PaymentSearch_ClearSearch_SortBy_Returns_ToDefaultValue() throws Exception {
		
		PaymentSearch.sortByDropdown.select_EntityValue("Account / Ascending");
		PaymentSearch.click_Search();
		PaymentSearch.click_PaymentSearch_Breadcrumb();
		PaymentSearch.click_ClearSearchButton();
		PaymentSearch.sortByDropdown.verify_SelectedDropdownValue("Select one...");
	
	}


	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		if (BaseUI.elementExists("paymentSearch_ResultsPage_Modal", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"))) {
			PaymentSearch.click_PaymentSearch_Breadcrumb();
		}
		PaymentSearch.click_ClearSearchButton();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
	
	
}


