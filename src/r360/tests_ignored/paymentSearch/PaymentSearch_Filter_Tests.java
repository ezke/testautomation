package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PaymentSearch_Filter_Tests extends BaseTest {

	TableData results;
	private String workgroup = "11901 - Workgroup 11901";
	private String date1 = "11/16/2016";
	private String date2 = "11/16/2016";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4177_PaymentSearch_PageFilters_Payer_Contains_PassedInValue() throws Exception {
		String field = "Payer";
		String value = "Bob & Sons";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4177_PaymentSearch_PageFilters_Payer_DoNotContain_PassedInValue_NoResults() throws Exception {
		String field = "Payer";
		String operator = "Contains";
		String value = "Greg Shop";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4173_PaymentSearch_PageFilters_DDA_Contains_PassedInValue() throws Exception {
		String field = "DDA";
		String value = "123456789";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4173_PaymentSearch_PageFilters_DDA_Contains_AlphaCharacters_NoResults() throws Exception {
		String field = "DDA";
		String operator = "Contains";
		String value = "dfeg";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4173_PaymentSearch_PageFilters_DDA_Contains_2NumericNumbers() throws Exception {
		String field = "DDA";
		String value = "23";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4173_PaymentSearch_PageFilters_DDA_Contains_SpecialCharacters_NoResults() throws Exception {
		String field = "DDA";
		String operator = "Contains";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_Equals_200() throws Exception {
		String field = "Payment Amount";
		String value = "200.00";
		
		PaymentSearch.verify_Field_Equals_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_IsGreaterThanOrEqualTo_50() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Greater Than Or Equal To";
		String value = "50";
		PaymentSearch.add_Search_Criteria(1, field, operator, value);
		PaymentSearch.click_Search();
		PaymentSearch.select_ShowRowsDropdownValue(100);
		TableData results = PaymentSearch.return_SearchResults();
		PaymentSearch.verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(results, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_IsLessThanOrEqualTo_200() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Less Than Or Equal To";
		String value = "200";
		PaymentSearch.add_Search_Criteria(1, field, operator, value);
		PaymentSearch.click_Search();
		PaymentSearch.select_ShowRowsDropdownValue(100);
		TableData results = PaymentSearch.return_SearchResults();
		PaymentSearch.verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(results, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_Equals_AlphaCharacters_Error() throws Exception {
		String field = "Payment Amount";
		String operator = "Equals";
		String value = "abc";
		PaymentSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_IsGreaterThanOrEqualTo_AlphaCharacters_Error() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Greater Than Or Equal To";
		String value = "abc";
		PaymentSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_IsLessThanOrEqualTo_AlphaCharacters_Error() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Less Than Or Equal To";
		String value = "abc";
		PaymentSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_Equals_SpecialCharacters_Error() throws Exception {
		String field = "Payment Amount";
		String operator = "Equals";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_IsGreaterThanOrEqualTo_SpecialCharacters_Error() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Greater Than Or Equal To";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4178_PaymentSearch_PageFilters_PaymentAmount_IsLessThanOrEqualTo_SpecialCharacters_Error() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Less Than Or Equal To";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4187_PaymentSearch_PageFilters_AccountNumber_Contains_PassedInValue() throws Exception {
		String field = "Account Number";
		String value = "00826451";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4187_PaymentSearch_PageFilters_AccountNumber_Contains_AlphaCharacters_NoResults() throws Exception {
		String field = "Account Number";
		String operator = "Contains";
		String value = "abc";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4187_PaymentSearch_PageFilters_AccountNumber_Contains_SpecialCharacters_NoResults() throws Exception {
		String field = "Account Number";
		String operator = "Contains";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4188_PaymentSearch_PageFilters_CheckTraceRefNumber_Contains_PassedInValue() throws Exception {
		String field = "Check/Trace/Ref Number";
		String value = "1139";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4188_PaymentSearch_PageFilters_CheckTraceRefNumber_Contains_AlphaCharacters_NoResults() throws Exception {
		String field = "Check/Trace/Ref Number";
		String operator = "Contains";
		String value = "abc";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}

	@Test(groups = { "all_Tests" })
	public void PT4188_PaymentSearch_PageFilters_CheckTraceRefNumber_Contains_SpecialCharacters_NoResults() throws Exception {
		String field = "Check/Trace/Ref Number";
		String operator = "Contains";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4174_PaymentSearch_PageFilters_RT_Contains_PassedInValue() throws Exception {
		String field = "R/T";
		String value = "056080126";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4174_PaymentSearch_PageFilters_RT_Contains_AlphaCharacters_NoResults() throws Exception {
		String field = "R/T";
		String operator = "Contains";
		String value = "abc";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4174_PaymentSearch_PageFilters_RT_Contains_SpecialCharacters_NoResults() throws Exception {
		String field = "R/T";
		String operator = "Contains";
		String value = "@#$";
		PaymentSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4191_PaymentSearch_PageFilters_SortBy_Workgroup_Ascending() throws Exception {
		String dropdown_Value = "Workgroup / Ascending";
		String columnName = "Workgroup";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4191_PaymentSearch_PageFilters_SortBy_Workgroup_Descending() throws Exception {
		String dropdown_Value = "Workgroup / Descending";
		String columnName = "Workgroup";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4199_PaymentSearch_PageFilters_SortBy_DepositDate_Ascending() throws Exception {
		String dropdown_Value = "Deposit Date / Ascending";
		String columnName = "Deposit Date";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4199_PaymentSearch_PageFilters_SortBy_DepositDate_Descending() throws Exception {
		String dropdown_Value = "Deposit Date / Descending";
		String columnName = "Deposit Date";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4196_PaymentSearch_PageFilters_SortBy_BatchID_Ascending() throws Exception {
		String dropdown_Value = "Batch ID / Ascending";
		String columnName = "Batch ID";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4196_PaymentSearch_PageFilters_SortBy_BatchID_Descending() throws Exception {
		String dropdown_Value = "Batch ID / Descending";
		String columnName = "Batch ID";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4195_PaymentSearch_PageFilters_SortBy_Batch_Ascending() throws Exception {
		String dropdown_Value = "Batch / Ascending";
		String columnName = "Batch";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT4195_PaymentSearch_PageFilters_SortBy_Batch_Descending() throws Exception {
		String dropdown_Value = "Batch / Descending";
		String columnName = "Batch";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}
	
//	@Test(groups = { "all_Tests" })
//	public void PT4205_PaymentSearch_PageFilters_SortBy_Transaction_Ascending() throws Exception {
//		String dropdown_Value = "Transaction / Ascending";
//		String columnName = "Transaction";
//
//		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
//	}
//	
//	@Test(groups = { "all_Tests" })
//	public void PT4205_PaymentSearch_PageFilters_SortBy_Transaction_Descending() throws Exception {
//		String dropdown_Value = "Transaction / Descending";
//		String columnName = "Transaction";
//
//		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
//	}
	
	@Test(groups = { "all_Tests" })
	public void PT4198_PaymentSearch_PageFilters_SortBy_DDA_Ascending() throws Exception {
		String dropdown_Value = "DDA / Ascending";
		String columnName = "DDA";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4198_PaymentSearch_PageFilters_SortBy_DDA_Descending() throws Exception {
		String dropdown_Value = "DDA / Descending";
		String columnName = "DDA";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}
	
//	@Test(groups = { "all_Tests" })
//	public void PT4204_PaymentSearch_PageFilters_SortBy_RT_Ascending() throws Exception {
//		String dropdown_Value = "RT / Ascending";
//		String columnName = "R/T";
//
//		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
//	}
//	
//	@Test(groups = { "all_Tests" })
//	public void PT4204_PaymentSearch_PageFilters_SortBy_RT_Descending() throws Exception {
//		String dropdown_Value = "RT / Descending";
//		String columnName = "R/T";
//
//		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
//	}
	
	@Test(groups = { "all_Tests" })
	public void PT4194_PaymentSearch_PageFilters_SortBy_AccountNumber_Ascending() throws Exception {
		String dropdown_Value = "Account / Ascending";
		String columnName = "Account Number";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4194_PaymentSearch_PageFilters_SortBy_AccountNumber_Descending() throws Exception {
		String dropdown_Value = "Account / Descending";
		String columnName = "Account Number";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4197_PaymentSearch_PageFilters_SortBy_CheckTraceRefNumber_Ascending() throws Exception {
		String dropdown_Value = "Check/Trace/Ref Number / Ascending";
		String columnName = "Check/Trace/Ref Number";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4197_PaymentSearch_PageFilters_SortBy_CheckTraceRefNumber_Descending() throws Exception {
		String dropdown_Value = "Check/Trace/Ref Number / Descending";
		String columnName = "Check/Trace/Ref Number";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4200_PaymentSearch_PageFilters_SortBy_Payer_Ascending() throws Exception {
		String dropdown_Value = "Payer / Ascending";
		String columnName = "Payer";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4200_PaymentSearch_PageFilters_SortBy_Payer_Descending() throws Exception {
		String dropdown_Value = "Payer / Descending";
		String columnName = "Payer";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4202_PaymentSearch_PageFilters_SortBy_PaymentSource_Ascending() throws Exception {
		String dropdown_Value = "PaymentSource / Ascending";
		String columnName = "Payment Source";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT4202_PaymentSearch_PageFilters_SortBy_PaymentSource_Descending() throws Exception {
		String dropdown_Value = "PaymentSource / Descending";
		String columnName = "Payment Source";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4203_PaymentSearch_PageFilters_SortBy_PaymentType_Ascending() throws Exception {
		String dropdown_Value = "PaymentType / Ascending";
		String columnName = "Payment Type";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4203_PaymentSearch_PageFilters_SortBy_PaymentType_Descending() throws Exception {
		String dropdown_Value = "PaymentType / Descending";
		String columnName = "Payment Type";

		PaymentSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4214_PaymentSearch_PageFilters_Payer_Contains_EscapeCharacter_Apostrophe() throws Exception {
		String field = "Payer";
		String value = "'";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4213_PaymentSearch_PageFilters_Payer_Contains_EscapeCharacter_GreaterAndLessThanSign() throws Exception {
		String field = "Payer";
		String value = "<>";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4216_PaymentSearch_PageFilters_Payer_Contains_EscapeCharacter_LeftParentheses() throws Exception {
		String field = "Payer";
		String value = "(";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4216_PaymentSearch_PageFilters_Payer_Contains_EscapeCharacter_RightParentheses() throws Exception {
		String field = "Payer";
		String value = ")";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4215_PaymentSearch_PageFilters_Payer_Contains_EscapeCharacter_Ampersand() throws Exception {
		String field = "Payer";
		String value = "&";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4212_PaymentSearch_ViewPaymentImage_DoesNotAppear() throws Exception {
//		Workgroup that does not have image
		PaymentSearch.click_Search();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("paymentSearch_ViewPaymentImageLink_List"));
		PaymentSearch.click_PaymentSearch_Breadcrumb();
	}


	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.pageSourceContainsString("Please fill in all required criteria.")) {
			BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
		}
		
		if (BaseUI.pageSourceContainsString("Numeric Columns must have Numeric Values")) {
			BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
		}

		if (BaseUI.elementExists("paymentSearch_Results_BreadcrumbHeaderLink_PaymentSearch", null, null)) {
			PaymentSearch.click_PaymentSearch_Breadcrumb();
		}
		PaymentSearch.click_ClearSearchButton();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
