package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.HomePage;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;
import wfsCommon.pages.EntitySelector;

public class PaymentSearch_WorkgroupMaxSearchDaysSetting_PermissionTest extends BaseTest {
	TableData results;
	String workgroup = "303030 - 30";
	private String date1 = "05/01/2016";
	private String date2 = "05/15/2016";
	
	private String testEntityWithChildrenNodes = "Mosinee Bank";
	private String testEntity = "Mosinee Inc";
	private String testWorkGroup_LongName = "30";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");
		
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_MaxSearchDays"), "10");
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4264_PaymentSearch_MaxSearchDays_SetTo_10Days_With_SearchSpanAllPermission() throws Exception{
//		 Ensure user has the permission "Search - Deposit Days Search Span All".
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
		PaymentSearch.click_Search();
		
		// Extract table data.
		results = PaymentSearch.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Payment Search Results.", "Payment Search Results were empty.");
		Navigation.signOut();
		HomePage.clickReturnToApplicationLink();
	}
	
	@Test(groups = {"all_Tests"}, priority = 1)
	public void PT4263_PaymentSearch_MaxSearchDays_SetTo_10Days_WithNo_SearchSpanAllPermission() throws Exception{
//		Ensure user does not have the permission "Search - Deposit Days Search Span All".
		String username = "AutomationGuru";
		String password = "Wausau#1";
		LoginPage.login(GlobalVariables.entityName, username, password);
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
		PaymentSearch.click_Search();
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserErrorPopup"));
		Navigation.close_ToastError_If_ItAppears();

	}
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Throwable {
		try {
			Navigation.navigate_Admin_Workgroups();
			EntitySelector.expandNode(testEntityWithChildrenNodes);
			EntitySelector.selectNode_Scroll(testEntity);
			Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
			Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
			Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");
			
			Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_MaxSearchDays"), "");
			Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
	
	
}
