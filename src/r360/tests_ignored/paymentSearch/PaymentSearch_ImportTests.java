package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.data.ImportData;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PaymentSearch_ImportTests extends BaseTest {
	TableData results;
	private String workgroup = "33338888 - Automation-ACH-CTX";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
	}

	@Test(groups = { "all_Tests" })
	public void PT2131_PaymentSearch_ResultPage_ACH_CTX_ImportsResults() throws Exception {
		PaymentSearch.click_Search();

		results = PaymentSearch.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Batch Summary Results.",
				"Batch Summary Results were empty.");

	}

	@Test(groups = { "all_Tests" })
	public void PT2137_PaymentSearch_ResultPage_ACH_CTX_Payer_PassedInValue() throws Exception {
		String field = "Payer";
		String value = "ContinentalUSA";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);

	}

	@Test(groups = { "all_Tests" })
	public void PT2134_PaymentSearch_ResultPage_ACH_CTX_DDA_PassedInValue() throws Exception {
		String field = "DDA";
		String value = "9112345667";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);

	}

	@Test(groups = { "all_Tests" })
	public void PT2138_PaymentSearch_ResultPage_ACH_CTX_PaymentAmount_PassedInValue() throws Exception {
		String field = "Payment Amount";
		String operator = "Is Greater Than Or Equal To";
		String value = "1000";
		PaymentSearch.add_Search_Criteria(1, field, operator, value);
		PaymentSearch.click_Search();
		PaymentSearch.select_ShowRowsDropdownValue(100);
		TableData results = PaymentSearch.return_SearchResults();
		PaymentSearch.verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(results, field, operator, value);
	}

	@Test(groups = { "all_Tests" })
	public void PT2133_PaymentSearch_ResultPage_ACH_CTX_CheckTraceRefNumber_PassedInValue() throws Exception {
		String field = "Check/Trace/Ref Number";
		String value = "0910";
		PaymentSearch.verify_Field_Contains_Value(1, field, value);

	}

	@Test(groups = { "all_Tests" })
	public void PT2135_PaymentSearch_ResultPage_ACH_CTX_NavigateTo_BatchDetailPage() throws Exception {
		Integer rowNumber = 1;
		PaymentSearch.click_Search();
		PaymentSearch.verify_ClickingBatch_TakesYouToBatchDetails_Page(rowNumber);

	}

	@Test(groups = { "all_Tests" })
	public void PT2136_PaymentSearch_ResultPage_ACH_CTX_NavigateTo_TransactionDetailPage() throws Exception {
		Integer rowNumber = 1;
		PaymentSearch.click_Search();
		PaymentSearch.verify_ClickingTransaction_TakesYouToTransactionDetails_Page(rowNumber);

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.pageSourceContainsString("Please fill in all required criteria.")) {
			BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
		}

		if (BaseUI.pageSourceContainsString("Numeric Columns must have Numeric Values")) {
			BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
		}

		if (BaseUI.elementExists("paymentSearch_Results_BreadcrumbHeaderLink_PaymentSearch", null, null)) {
			PaymentSearch.click_PaymentSearch_Breadcrumb();
		}
		PaymentSearch.click_ClearSearchButton();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}
}
