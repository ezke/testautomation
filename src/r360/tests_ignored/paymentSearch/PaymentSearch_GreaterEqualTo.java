package r360.tests_ignored.paymentSearch;


import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.data.ImportData;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.*;

public class PaymentSearch_GreaterEqualTo extends BaseTest {

    private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
    private String date2 = date1;

    @BeforeClass
    public void setup_method() throws Throwable {
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
        Navigation.navigate_Search_PaymentSearch();
        PaymentSearch.setDateRange(date1, date2);
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_AscendingColumns() throws Exception {
        //PaymentSearch.select_ShowRowsDropdownValue(100);
        //Declare an array of multiple column search
        String arrayHeaders[] = {"Check/Trace/Ref Number", "Workgroup", "Batch ID"};


         /*Validate columns Check/Trace/Ref Number",
        "Workgroup" and "Batch ID in search results*/

        for (int i = 0; i < arrayHeaders.length; i++) {
            Navigation.navigate_Search_PaymentSearch();
            AdvancedSearch.click_Search();
            PaymentSearch.select_ShowRowsDropdownValue(100);
            PaymentSearch.sort_ByHeader_Ascending(arrayHeaders[i]);
            TableData newTableData = PaymentSearch.return_SearchResults();
            newTableData.verify_Column_Sorted_Numeric_Ascending(arrayHeaders[i]);
        }
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_NoCriteria() throws Exception {
        //   PaymentSearch.select_ShowRowsDropdownValue(10);
        PaymentSearch.click_PaymentSearch_Breadcrumb();
        PaymentSearch.click_AddCriteria();
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Please fill in all required criteria.");
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_NoOperatorValue() throws Exception {
        /* Added search without any operator and value*/
        String field1 = "Payment Amount";
        String operator1 = "Is Greater Than";
        String value1 = "2";
        // PaymentSearch.click_PaymentSearch_Breadcrumb();
        // PaymentSearch.click_AddCriteria();
        PaymentSearch.add_Search_Criteria(1, field1, "", "");
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Please fill in all required criteria.");
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_AlphaNumericFields() throws Exception {
        String field1 = "Payment Amount";
        String operator1 = "Is Greater Than";
        /* Added AlphaNumeric Characters to search*/
        //  Navigation.close_ToastError_If_ItAppears();
        PaymentSearch.click_AddCriteria();
        PaymentSearch.add_Search_Criteria(1, field1, operator1, "$$");
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Numeric Columns must have Numeric Values");
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_PaymentSearch_SpecialCharacters() throws Exception {
        /* Added Special Characters to search*/
        String field1 = "Payment Amount";
        String operator1 = "Is Greater Than";
        PaymentSearch.click_AddCriteria();
        PaymentSearch.add_Search_Criteria(1, field1, operator1, "'");
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Numeric Columns must have Numeric Values");
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_RegularCharacters() throws Exception {
        /* Added Regular search*/
        String field1 = "Payment Amount";
        String operator1 = "Is Greater Than";
        String value1 = "2";
        PaymentSearch.click_AddCriteria();
        PaymentSearch.add_Search_Criteria(1, field1, operator1, value1);
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Please fill in all required criteria.");
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_RegularSearchRemovingChars() throws Exception {
        /* Added Regular search removing value*/
        String field1 = "Payment Amount";
        String operator1 = "Is Greater Than";
        String value1 = "2";
        PaymentSearch.click_AddCriteria();
        PaymentSearch.add_Search_Criteria(1, field1, operator1, "");
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Please fill in all required criteria.");
    }

    @Test(groups = {"all_Tests", "critical_tests"})
    public void PT4235_PaymentSearch_PaymentSearch_EnormousNumber() throws Exception {
        /* Added Regular search with enormous value*/
        String field1 = "Payment Amount";
        String operator1 = "Is Greater Than";
        PaymentSearch.click_AddCriteria();
        PaymentSearch.add_Search_Criteria(1, field1, operator1, "1000000");
        PaymentSearch.click_Search();
        Navigation.validate_ToastError_Message_If_ItAppears("Please fill in all required criteria.");
    }

    @AfterMethod(alwaysRun = true)
    public void clearToastSearch(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.close_ToastError_If_ItAppears();
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}

