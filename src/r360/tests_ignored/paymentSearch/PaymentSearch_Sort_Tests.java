package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class PaymentSearch_Sort_Tests extends BaseTest {

	TableData results;

	String workgroup = "";
	private String date1 = "";
	private String date2 = "";
	
	@BeforeClass
	public void setup_method() throws Exception {
		
		workgroup = "595959 - namworkgroup59";
		date1 = "08/25/2016";
		date2 = "08/25/2016";
		
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
		PaymentSearch.click_Search();
		PaymentSearch.select_ShowRowsDropdownValue(100);
		
		// Extract table data.
		results = PaymentSearch.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Payment Search Results.", "Payment Search Results were empty.");
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Workgroup_Ascending() throws Exception{
		String headerText = "Workgroup";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Workgroup_Descending() throws Exception{
		String headerText = "Workgroup";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_DepositDate_Ascending() throws Exception{
		String headerText = "Deposit Date";
		results.remove_Character("/", headerText);
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		newTableData.remove_Character("/", headerText);
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Depositdate_Descending() throws Exception{
		String headerText = "Deposit Date";
		results.remove_Character("/", headerText);
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		newTableData.remove_Character("/", headerText);
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_BatchID_Ascending() throws Exception{
		String headerText = "Batch ID";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_BatchID_Descending() throws Exception{
		String headerText = "Batch ID";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Batch_Ascending() throws Exception{
		String headerText = "Batch";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Batch_Descending() throws Exception{
		String headerText = "Batch";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_PaymentSource_Ascending() throws Exception{
		String headerText = "Payment Source";
		results.sort_ByColumn_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_PaymentSource_Descending() throws Exception{
		String headerText = "Payment Source";
		results.sort_ByColumn_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_PaymentType_Ascending() throws Exception{
		String headerText = "Payment Type";
		results.sort_ByColumn_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_PaymentType_Descending() throws Exception{
		String headerText = "Payment Type";
		results.sort_ByColumn_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Transaction_Ascending() throws Exception{
		String headerText = "Transaction";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Transaction_Descending() throws Exception{
		String headerText = "Transaction";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_PaymentAmount_Ascending() throws Exception{
		String headerText = "Payment Amount";
		results.remove_Character("$", headerText);
		results.remove_Character(",", headerText);
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		newTableData.remove_Character("$", headerText);
		newTableData.remove_Character(",", headerText);
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_PaymentAmount_Descending() throws Exception{
		String headerText = "Payment Amount";
		results.remove_Character("$", headerText);
		results.remove_Character(",", headerText);
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		newTableData.remove_Character("$", headerText);
		newTableData.remove_Character(",", headerText);
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_RT_Ascending() throws Exception{
		String headerText = "R/T";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_RT_Descending() throws Exception{
		String headerText = "R/T";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_AccountNumber_Ascending() throws Exception{
		String headerText = "Account Number";
		results.sort_ByColumn_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_AccountNumber_Descending() throws Exception{
		String headerText = "Account Number";
		results.sort_ByColumn_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_CheckTraceRefNumber_Ascending() throws Exception{
		String headerText = "Check/Trace/Ref Number";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_CheckTraceRefNumber_Descending() throws Exception{
		String headerText = "Check/Trace/Ref Number";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Payer_Ascending() throws Exception{
		String headerText = "Payer";
		results.sort_ByColumn_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_Payer_Descending() throws Exception{
		String headerText = "Payer";
		results.sort_ByColumn_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_DDA_Ascending() throws Exception{
		String headerText = "DDA";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		PaymentSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT4268_PaymentSearch_TestingResults_DDA_Descending() throws Exception{
		String headerText = "DDA";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		PaymentSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = PaymentSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
	
}
