package r360.tests_ignored.paymentSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.Admin_Workgroups;
import r360.pages.Admin_Workgroups_AddingEditing_WorkgroupModal;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.PaymentSearch;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;
import wfsCommon.pages.EntitySelector;

public class PaymentSearch_WorkgroupSetting_DisplayBatchID extends BaseTest {

	TableData results;
	private String workgroup = "303030 - 30";
	private String date1 = "05/01/2016";
	private String date2 = "05/30/2016";	
	
	private String testEntityWithChildrenNodes = "Mosinee Bank";
	private String testEntity = "Mosinee Inc";
	private String testWorkGroup_LongName = "30";
	private String displayBatchID = "No";
	
	@BeforeClass
	public void setup_method() throws Throwable {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		
		Navigation.navigate_Admin_Workgroups();
		EntitySelector.expandNode(testEntityWithChildrenNodes);
		EntitySelector.selectNode_Scroll(testEntity);
		Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");
		
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
		Admin_Workgroups_AddingEditing_WorkgroupModal.displayBatchID_Dropdown.select_EntityValue(displayBatchID);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		
		Navigation.navigate_Search_PaymentSearch();
		PaymentSearch.workGroupSelector.search_forText(workgroup);
		PaymentSearch.setDateRange(date1, date2);
		
	}
	
	@Test(groups = { "all_Tests" })
	public void PT4181_PaymentSearch_WorkgroupSetting_SearchResults_BatchID_DoesNotAppear() throws Exception {
//		Display Batch ID set to "No" in Workgroup Edit settings 
//		To verify BatchID does not appear in results page of payment search
		PaymentSearch.click_Search();
		PaymentSearch.select_ShowRowsDropdownValue(100);
		String headerText = "Batch ID";
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null));
	}
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		if (BaseUI.elementExists("paymentSearch_ResultsPage_Modal", null, null)
				&& BaseUI.elementAppears(Locator.lookupElement("paymentSearch_ResultsPage_Modal"))) {
			PaymentSearch.click_PaymentSearch_Breadcrumb();
		}
		PaymentSearch.click_ClearSearchButton();
	}
	
	@AfterClass(alwaysRun = true)
	public void TearDown() throws Throwable {
		try {
			Navigation.navigate_Admin_Workgroups();
			EntitySelector.expandNode(testEntityWithChildrenNodes);
			EntitySelector.selectNode_Scroll(testEntity);
			Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
			Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
			Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");
			
			Admin_Workgroups.launch_EditWorkGroupSettings_Modal(testWorkGroup_LongName);
			Admin_Workgroups_AddingEditing_WorkgroupModal.displayBatchID_Dropdown.select_EntityValue("Yes");
			Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
			
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
