package r360.tests_ignored.advancedSearch;

import java.text.MessageFormat;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;


///
///Relies on Data imported from Import_Workgroups.java class.  Must run this class before running these tests.
///
public class AdvancedSearch_ImportTests_Wire_StubData extends BaseTest {
	
	TableData results;
	//TableData paymentData;
	TableData stubData;

	private String workgroup = "2234322";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	//TC's covered: 266940, 266942, 266946, 272864, 273002, 235418
	//Data is based on our Import calls.
	
	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {
			//ImportData.import_Workgroup_1123211_Data_WithTodaysDate();
			
			stubData = AdvancedSearch.return_StubData(ImportData.return_TodaysDate_FormattedForDatabase(), workgroup);
			//paymentData = AdvancedSearch.return_PaymentData(ImportData.return_TodaysDate_FormattedForDatabase(),
					//workgroup);

			//paymentData.remove_Rows_WithColumnName_WhereValues_DoNOTMatch("Payment Type", "Wire");
			stubData.remove_Rows_WithColumnName_WhereValues_DoNOTMatch("Payment Type", "Wire");
			
			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);

			AdvancedSearch.paymentTypeDropdown.select_EntityValue("Wire");
			
			String[] displayFields = {"Transaction", "Payment Type", "[INV].Account Number", "[INV].Amount", "[INV].Amount Paid",
					"[INV].BPR Account Number", "[INV].BPR Monetary Amount", "[INV].Invoice Number", "[INV].Reassociation Trace Number", 
					"[INV].RMR Discount Amount", "[INV].RMR Monetary Amount", "[INV].RMR Reference Number", "[INV].RMR Total Invoice Amount"};
			
			AdvancedSearch.add_DisplayFields(displayFields);
			
			//AdvancedSearch.add_Search_Criteria(0, "Transaction", operator, value);
			
			AdvancedSearch.click_Search();

			AdvancedSearch.select_ShowRowsDropdownValue(100);

			// Extract table data.
			results = AdvancedSearch.return_SearchResults();
			results.remove_Character("$");
			//paymentData.set_decimal_Precision(2);
			
			stubData.set_decimal_Precision(2);
			//Advanced Search Results have one entry that doesn't exist in the Stub data, so I'm removing that before we start comparing things.
			results.remove_Rows_WithColumnName_WhereValues_DoNOTMatch("Transaction", "2");
			
			BaseUI.verify_true_AndLog(results.data.size() == stubData.data.size(),
					"Search results list size matched expected results list.",
					MessageFormat.format(
							"Search results list size {0} did NOT match expected results list of size {1}.",
							results.data.size(), stubData.data.size()));
		}
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_Verify_ThereIsWireData() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Payment Type", "Wire", results.data);
	}
	
	



	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_PaymentType() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Type", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_AccountNumber() throws Exception {
		results.replaceKey_ForGivenKey("[INV].Account Number", "Account Number");
		BaseUI.verify_TableColumns_Match("Account Number", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_Amount() throws Exception {
		results.replaceKey_ForGivenKey("[INV].Amount", "Amount");
		BaseUI.verify_TableColumns_Match("Amount", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_BPR_AccountNumber() throws Exception {
		results.replaceKey_ForGivenKey("[INV].BPR Account Number", "BPR Account Number");
		BaseUI.verify_TableColumns_Match("BPR Account Number", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_BPR_MonetaryAmount() throws Exception {
		results.replaceKey_ForGivenKey("[INV].BPR Monetary Amount", "BPR Monetary Amount");
		BaseUI.verify_TableColumns_Match("BPR Monetary Amount", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_InvoiceNumber() throws Exception {
		results.replaceKey_ForGivenKey("[INV].Invoice Number", "Invoice Number");
		BaseUI.verify_TableColumns_Match("Invoice Number", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_ReassociationTrace_Number() throws Exception {
		results.replaceKey_ForGivenKey("[INV].Reassociation Trace Number", "Reassociation Trace Number");
		BaseUI.verify_TableColumns_Match("Reassociation Trace Number", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_RMR_DiscountAmount() throws Exception {
		results.replaceKey_ForGivenKey("[INV].RMR Discount Amount", "RMR Discount Amount");
		results.replace_Null_With_Empty();
		stubData.replace_Null_With_Empty();
		stubData.add_Column_IfItDoesntExist("RMR Discount Amount");
		BaseUI.verify_TableColumns_Match("RMR Discount Amount", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_RMR_MonetaryAmount() throws Exception {
		results.replaceKey_ForGivenKey("[INV].RMR Monetary Amount", "RMR Monetary Amount");
		BaseUI.verify_TableColumns_Match("RMR Monetary Amount", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_RMR_ReferenceNumber() throws Exception {
		results.replaceKey_ForGivenKey("[INV].RMR Reference Number", "RMR Reference Number");
		BaseUI.verify_TableColumns_Match("RMR Reference Number", stubData.data, results.data);
	}
	
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_StubData_RMR_TotalInvoiceAmount() throws Exception {
		results.replaceKey_ForGivenKey("[INV].RMR Total Invoice Amount", "RMR Total Invoice Amount");
		BaseUI.verify_TableColumns_Match("RMR Total Invoice Amount", stubData.data, results.data);
	}
	
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
