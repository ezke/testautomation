package r360.tests_ignored.advancedSearch;


import java.text.MessageFormat;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import r360.data.Config_DataRetrieval;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_DownloadAsText extends BaseTest {

	TableData results;
	TableData csvResults = new TableData();

	private String workgroup = "2234322";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	//Test Case 158589
	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {

			Browser.openBrowser(GlobalVariables.baseURL, "chrome");

			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);
			AdvancedSearch.click_Search();
			AdvancedSearch.select_ShowRowsDropdownValue(100);
			
			results = AdvancedSearch.return_SearchResults();
			
			csvResults = AdvancedSearch.returnCSV_Results();
			csvResults.remove_Character("\"");

			BaseUI.verify_true_AndLog(results.data.size() == csvResults.data.size(),
					"Search results list size matched expected results list.",
					MessageFormat.format(
							"Search results list size {0} did NOT match expected results list of size {1}.",
							results.data.size(), Config_DataRetrieval.tableData.data.size()));
		}
	}

	@DataProvider(name = "testData")
	public Object[][] createData() {
		Object[][] expectedResultRows = new Object[csvResults.data.size()][2];

		//i = row number.  0 would represent column 1.  1 represents column 2.
		for (int i = 0; i < csvResults.data.size(); i++) {
			expectedResultRows[i][0] = i;
			expectedResultRows[i][1] = csvResults.data.get(i);
		}
		return expectedResultRows;
	}

	@Test(dataProvider = "testData", groups = {"all_Tests"})
	public void TC_AdvancedSearch_TestingResults(Integer rowNumber, HashMap<String, String> tableRow) throws Exception {

		HashMap<String, String> resultRow = results.dataRow(rowNumber);
		
		BaseUI.verify_TableRow_Matches(rowNumber.toString(), tableRow, resultRow);

	
	}
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
