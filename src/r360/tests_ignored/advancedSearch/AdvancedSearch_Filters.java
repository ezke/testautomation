package r360.tests_ignored.advancedSearch;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_Filters extends BaseTest {

	TableData paymentData;
	TableData stubData;

	private String workgroup = "2234322";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {
			
			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);
		}
	}

	@Test(groups = { "all_Tests" })
	public void PT3176_AdvancedSearch_PageFilters_PaymentType_ACH() throws Exception {
		String paymentType = "ACH";

		AdvancedSearch.verify_PaymentType_Filter(paymentType);
	}

	@Test(groups = { "all_Tests" })
	public void PT3178_AdvancedSearch_PageFilters_PaymentType_Cash() throws Exception {
		String paymentType = "Cash";

		AdvancedSearch.verify_PaymentType_Filter(paymentType);
	}

	@Test(groups = { "all_Tests" })
	public void TC234739_AdvancedSearch_PageFilters_PaymentType_Wire() throws Exception {
		String paymentType = "Wire";

		AdvancedSearch.verify_PaymentType_Filter(paymentType);
	}

	@Test(groups = { "all_Tests" })
	public void PT3179_AdvancedSearch_PageFilters_PaymentType_Check() throws Exception {
		String paymentType = "Check";

		AdvancedSearch.verify_PaymentType_Filter(paymentType);
	}

	@Test(groups = { "all_Tests" })
	public void PT3182_AdvancedSearch_PageFilters_PaymentType_SWIFT() throws Exception {
		String paymentType = "SWIFT";

		AdvancedSearch.verify_PaymentType_Filter(paymentType);
	}

	@Test(groups = { "all_Tests" })
	public void PT3201_AdvancedSearch_TestingResults_AccountNumber_Ascending() throws Exception {
		String headerText = "Payment Type";
		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);

		AdvancedSearch.verify_SearchResults_SortAscending_ByColumnHeader(headerText);
	}

	@Test(groups = { "all_Tests" })
	public void PT3201_AdvancedSearch_TestingResults_AccountNumber_Descending() throws Exception {
		String headerText = "Payment Type";
		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);

		AdvancedSearch.verify_SearchResults_SortDescending_ByColumnHeader(headerText);
	}

	@Test(groups = { "all_Tests" })
	public void PT3386_AdvancedSearch_WorkgroupSelector_Reset_ClearsValue() throws Exception {
		AdvancedSearch.workGroupSelector.click_Reset();
		AdvancedSearch.workGroupSelector.close_List();
		AdvancedSearch.workGroupSelector.search_forText(workgroup);
		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);
		
		BaseUI.verifyElementHasExpectedPartialText("advancedSearch_WorkGroupValue", workgroup);

		AdvancedSearch.verify_Results_HaveCorrect_DepositDate(date1);

	}

	

	@Test(groups = { "all_Tests" })
	public void PT3366_AdvancedSearch_PageFilters_SortBy_AccountNumber() throws Exception {
		String dropdown_Value = "Account Number";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, dropdown_Value);
	}

	@Test(groups = { "all_Tests" })
	public void PT3367_AdvancedSearch_PageFilters_SortBy_AccountNumber() throws Exception {
		String dropdown_Value = "Batch";
		String columnName = "Batch Number";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3368_AdvancedSearch_PageFilters_SortBy_CheckTraceRefNumber() throws Exception {
		String dropdown_Value = "Check/Trace/Ref Number";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, dropdown_Value);
	}

	@Test(groups = { "all_Tests" })
	public void PT3371_AdvancedSearch_PageFilters_SortBy_PaymentAmount_Ascending() throws Exception {
		String dropdown_Value = "Payment Amount/Ascending";
		String columnName = "Payment Amount";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3371_AdvancedSearch_PageFilters_SortBy_PaymentAmount_Descending() throws Exception {
		String dropdown_Value = "Payment Amount/Descending";
		String columnName = "Payment Amount";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3372_AdvancedSearch_PageFilters_SortBy_PaymentSource_Ascending() throws Exception {
		String dropdown_Value = "Payment Source/Ascending";
		String columnName = "Payment Source";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3372_AdvancedSearch_PageFilters_SortBy_PaymentSource_Descending() throws Exception {
		String dropdown_Value = "Payment Source/Descending";
		String columnName = "Payment Source";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3373_AdvancedSearch_PageFilters_SortBy_PaymentType_Ascending() throws Exception {
		String dropdown_Value = "Payment Type/Ascending";
		String columnName = "Payment Type";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3373_AdvancedSearch_PageFilters_SortBy_PaymentType_Descending() throws Exception {
		String dropdown_Value = "Payment Type/Descending";
		String columnName = "Payment Type";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending(dropdown_Value, columnName);
	}

	@Test(groups = { "all_Tests" })
	public void PT3374_AdvancedSearch_PageFilters_SortBy_RT_Ascending() throws Exception {
		String dropdown_Value = "RT";
		String columnName = "R/T";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending(dropdown_Value, columnName);
	}


	@Test(groups = { "all_Tests" })
	public void PT3345_AdvancedSearch_PageFilters_PaymentSource_Clears() throws Exception {
		String value = "ACH";

		AdvancedSearch.paymentSourceDropdown.select_EntityValue(value);
		AdvancedSearch.paymentSourceDropdown.reset_Dropdown();
		AdvancedSearch.paymentSourceDropdown.verify_SelectedDropdownValue("-- All --");
	}

	@Test(groups = { "all_Tests" })
	public void PT3345_AdvancedSearch_PageFilters_PaymentSource_SearchForPaymentSource_NoResults() throws Exception {
		String value = "George";

		AdvancedSearch.paymentSourceDropdown.verify_NoResultsFound_ForEnteredText(value);
		AdvancedSearch.paymentSourceDropdown.verify_SelectedDropdownValue("-- All --");
	}

	@Test(groups = { "all_Tests" })
	public void PT3348_AdvancedSearch_PageFilters_PaymentType_Clears() throws Exception {
		String value = "ACH";

		AdvancedSearch.paymentTypeDropdown.select_EntityValue(value);
		AdvancedSearch.paymentTypeDropdown.reset_Dropdown();
		AdvancedSearch.paymentTypeDropdown.verify_SelectedDropdownValue("-- All --");
	}

	@Test(groups = { "all_Tests" })
	public void PT3348_AdvancedSearch_PageFilters_PaymentType_SearchForPaymentType_NoResults() throws Exception {
		String value = "George";

		AdvancedSearch.paymentTypeDropdown.verify_NoResultsFound_ForEnteredText(value);
		AdvancedSearch.paymentTypeDropdown.verify_SelectedDropdownValue("-- All --");
	}

	@Test(groups = { "all_Tests" })
	public void PT3369_AdvancedSearch_PageFilters_SortBy_PaymentAmountAscending_Clears() throws Exception {
		String field = "Payment Amount";
		String value = "Payment Amount/Ascending";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(value, field);
		AdvancedSearch.click_AdvancedSearch_Breadcrumb();

		AdvancedSearch.sortByDropdown.reset_Dropdown();
		AdvancedSearch.sortByDropdown.verify_SelectedDropdownValue("Select one...");
	}

	@Test(groups = { "all_Tests" })
	public void PT3369_AdvancedSearch_PageFilters_SortBy_PaymentAmountDescending_Clears_AndThenSelects_PaymentAmountAscending()
			throws Exception {
		String field = "Payment Amount";
		String value = "Payment Amount/Descending";
		String value2 = "Payment Amount/Ascending";

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(value, field);
		AdvancedSearch.click_AdvancedSearch_Breadcrumb();

		AdvancedSearch.sortByDropdown.reset_Dropdown();
		AdvancedSearch.sortByDropdown.verify_SelectedDropdownValue("Select one...");

		AdvancedSearch.verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(value2, field);
	}

	@Test(groups = { "all_Tests" })
	public void PT3376_AdvancedSearch_PageFilters_QueryBuilder_OperatorNotSelected_GivesErrorMessage()
			throws Exception {
		String field = "DDA";
		String operator = null;
		String value = "abc";

		AdvancedSearch.verify_QueryBuilder_MissingRequiredField(1, field, operator, value);
	}

	@Test(groups = { "all_Tests" })
	public void PT3432_AdvancedSearch_SearchResults_ClickTransaction_navigatesTo_TransactionDetailsPage()
			throws Exception {
		Integer rowNumber = 1;

		AdvancedSearch.click_Search();
		AdvancedSearch.verify_ClickingTransaction_TakesYouToTransactionDetails_Page(rowNumber);
	}

	@Test(groups = { "all_Tests" })
	public void PT3156_AdvancedSearch_BatchNumber_ExpectedTitle() throws Exception {

		AdvancedSearch.click_Search();

		WebElement firstBatchNumberLink = AdvancedSearch.return_Link_ByColumnName_AndColumnRow("Batch Number", 1);
		BaseUI.verifyElementHasExpectedAttributeValue(firstBatchNumberLink, "title", "View Batch");
		AdvancedSearch.click_Cell_Link("Batch Number", 1);
	}

	@Test(groups = { "all_Tests" })
	public void PT3156_AdvancedSearch_BreadCrumbs_BatchDetail() throws Exception {
		String[] crumbOrder = { "Advanced Search", "Search Results", "Batch Detail" };

		AdvancedSearch.click_Search();
		AdvancedSearch.click_Cell_Link("Batch Number", 1);
		AdvancedSearch.verify_Breadcrumb(crumbOrder);

	}

	@Test(groups = { "all_Tests" })
	public void PT3158_AdvancedSearch_Transaction_ExpectedTitle() throws Exception {

		AdvancedSearch.click_Search();

		WebElement firstBatchNumberLink = AdvancedSearch.return_Link_ByColumnName_AndColumnRow("Transaction", 1);
		BaseUI.verifyElementHasExpectedAttributeValue(firstBatchNumberLink, "title", "View Transaction");
		AdvancedSearch.click_Cell_Link("Transaction", 1);
	}

	@Test(groups = { "all_Tests" })
	public void PT3158_AdvancedSearch_BreadCrumbs_TransactionDetail() throws Exception {
		String[] crumbOrder = { "Advanced Search", "Search Results", "Transaction Detail" };

		AdvancedSearch.click_Search();
		AdvancedSearch.click_Cell_Link("Transaction", 1);
		AdvancedSearch.verify_Breadcrumb(crumbOrder);

	}

	@Test(groups = { "all_Tests" })
	public void PT3158_AdvancedSearch_BreadCrumbs_TransactionDetail_ClickingSearchResults_TakesYouBackTo_SearchResults()
			throws Exception {
		String[] crumbOrder = { "Advanced Search", "Search Results" };

		AdvancedSearch.click_Search();
		AdvancedSearch.click_Cell_Link("Transaction", 1);
		AdvancedSearch.click_SearchResults_Breadcrumb();
		AdvancedSearch.verify_Breadcrumb(crumbOrder);
	}

	@Test(groups = { "all_Tests" })
	public void PT3157_AdvancedSearch_BreadCrumbs_BatchDetail_TransactionDetail() throws Exception {
		String[] crumbOrder = { "Advanced Search", "Search Results", "Batch Detail", "Transaction Detail" };
		String expectedHoverText = "Click here for Transaction Detail";

		AdvancedSearch.click_Search();
		AdvancedSearch.click_Cell_Link("Batch Number", 1);
		AdvancedSearch.verify_BatchDetailsCell_Title_ByHeaderText_AndRowNumber("Transaction", 1, expectedHoverText);
		AdvancedSearch.click_BatchDetailsCell_ByHeaderText_AndRowNumber("Transaction", 1);
		AdvancedSearch.verify_Breadcrumb(crumbOrder);

		// Check the Crumb Order after we've gone back to Search Results page.
		String[] crumbOrder2 = { "Advanced Search", "Search Results" };

		AdvancedSearch.click_SearchResults_Breadcrumb();
		AdvancedSearch.verify_Breadcrumb(crumbOrder2);

	}

	@Test(groups = { "all_Tests" })
	public void PT3322_AdvancedSearch_PageFilters_PaymentAmount_IsGreaterThan_80_AND_ChecRefTrace_Equals_1733()
			throws Exception {
		String field1 = "Payment Amount";
		String operator1 = "Is Greater Than";
		String value1 = "80";

		String field2 = "Check/Trace/Ref Number";
		String operator2 = "Equals";
		String value2 = "1733";

		AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
		AdvancedSearch.add_Search_Criteria(2, field2, operator2, value2);

		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);
		TableData results = AdvancedSearch.return_SearchResults();

		AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field1, operator1, value1);
		AdvancedSearch.verify_ResultsPage_ResultsEqualExpected(results, field2, value2);
	}

	@Test(groups = { "all_Tests" })
	public void PT3315_AdvancedSearch_PageFilters_QueryBuilder_ValueNotSelected_GivesErrorMessage() throws Exception {
		String field = "DDA";
		String operator = "Equals";
		String value = null;

		AdvancedSearch.verify_QueryBuilder_MissingRequiredField(1, field, operator, value);
	}

	

	@Test(groups = { "all_Tests" })
	public void PT3398_AdvancedSearch_5DisplayFields_Added() throws Exception {
		String[] displayFieldsToAdd = { "Batch Number", "Payment Amount", "R/T", "Payment Sequence",
				"Check/Trace/Ref Number" };

		String field1 = "Batch Number";
		String operator1 = "Is Greater Than or Equal To";

		String value1 = "9020";
		String field2 = "Payment Amount";
		String operator2 = "Is Greater Than";
		String value2 = "35.00";

		String field3 = "R/T";
		String operator3 = "Contains";
		String value3 = "01";

		String field4 = "Payment Sequence";
		String operator4 = "Equals";
		String value4 = "1";

		String field5 = "Check/Trace/Ref Number";
		String operator5 = "Contains";
		String value5 = "33";

		AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
		AdvancedSearch.add_Search_Criteria(2, field2, operator2, value2);
		AdvancedSearch.add_Search_Criteria(3, field3, operator3, value3);
		AdvancedSearch.add_Search_Criteria(4, field4, operator4, value4);
		AdvancedSearch.add_Search_Criteria(5, field5, operator5, value5);
		AdvancedSearch.add_DisplayFields(displayFieldsToAdd);

		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);
		TableData results = AdvancedSearch.return_SearchResults();

		AdvancedSearch.verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(results, field1, operator1, value1);
		AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field2, operator2, value2);
		AdvancedSearch.verify_ResultsPage_Contains_Value(results, field3, value3);
		AdvancedSearch.verify_ResultsPage_ResultsEqualExpected(results, field4, value4);
		AdvancedSearch.verify_ResultsPage_Contains_Value(results, field5, value5);

		AdvancedSearch.verify_Correct_DisplayFields(displayFieldsToAdd);
	}

	@Test(groups = { "all_Tests" })
	public void PT3399_AdvancedSearch_4DisplayFields_Added() throws Exception {
		String[] displayFieldsToAdd = { "Payment Amount", "R/T", "Account Number", "Check/Trace/Ref Number" };
		String field1 = "Payment Amount";
		String operator1 = "Is Greater Than";
		String value1 = "80";

		String field2 = "Check/Trace/Ref Number";
		String operator2 = "Equals";
		String value2 = "1733";

		String field3 = "R/T";
		String operator3 = "Contains";
		String value3 = "5601";

		String field4 = "Account Number";
		String operator4 = "Begins With";
		String value4 = "963";

		AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
		AdvancedSearch.add_Search_Criteria(2, field2, operator2, value2);
		AdvancedSearch.add_Search_Criteria(3, field3, operator3, value3);
		AdvancedSearch.add_Search_Criteria(4, field4, operator4, value4);
		AdvancedSearch.add_DisplayFields(displayFieldsToAdd);

		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);
		TableData results = AdvancedSearch.return_SearchResults();

		AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field1, operator1, value1);
		AdvancedSearch.verify_ResultsPage_ResultsEqualExpected(results, field2, value2);
		AdvancedSearch.verify_ResultsPage_Contains_Value(results, field3, value3);
		AdvancedSearch.verify_ResultsPage_BeginsWith_Value(results, field4, value4);

		AdvancedSearch.verify_Correct_DisplayFields(displayFieldsToAdd);
	}

	@Test(groups = { "all_Tests" })
	public void PT3403_AdvancedSearch_3DisplayFields_Added() throws Exception {
		String[] displayFieldsToAdd = { "Batch Number", "Payment Amount", "R/T" };

		String field1 = "Batch Number";
		String operator1 = "Is Greater Than or Equal To";
		String value1 = "9020";

		String field2 = "Payment Amount";
		String operator2 = "Is Greater Than";
		String value2 = "35.00";

		String field3 = "R/T";
		String operator3 = "Contains";
		String value3 = "01";

		AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
		AdvancedSearch.add_Search_Criteria(2, field2, operator2, value2);
		AdvancedSearch.add_Search_Criteria(3, field3, operator3, value3);
		AdvancedSearch.add_DisplayFields(displayFieldsToAdd);

		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);
		TableData results = AdvancedSearch.return_SearchResults();

		AdvancedSearch.verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(results, field1, operator1, value1);
		AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field2, operator2, value2);
		AdvancedSearch.verify_ResultsPage_Contains_Value(results, field3, value3);

		AdvancedSearch.verify_Correct_DisplayFields(displayFieldsToAdd);
	}

	@Test(groups = { "all_Tests" })
	public void PT3404_AdvancedSearch_2DisplayFields_Added() throws Exception {
		String[] displayFieldsToAdd = { "Payment Amount", "Check/Trace/Ref Number" };
		String field1 = "Payment Amount";
		String operator1 = "Is Greater Than";
		String value1 = "80";

		String field2 = "Check/Trace/Ref Number";
		String operator2 = "Equals";
		String value2 = "1733";

		AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
		AdvancedSearch.add_Search_Criteria(2, field2, operator2, value2);
		AdvancedSearch.add_DisplayFields(displayFieldsToAdd);

		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);

		TableData results = AdvancedSearch.return_SearchResults();
		AdvancedSearch.verify_Correct_DisplayFields(displayFieldsToAdd);
		AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field1, operator1, value1);
		AdvancedSearch.verify_ResultsPage_ResultsEqualExpected(results, field2, value2);

	}

	@Test(groups = { "all_Tests" })
	public void PT3402_AdvancedSearch_1DisplayFields_Added() throws Exception {
		String[] displayFieldsToAdd = { "Invoice Batch Sequence" };
		String field1 = "Invoice Batch Sequence";
		String operator1 = "Is Greater Than";
		String value1 = "2";

		AdvancedSearch.add_Search_Criteria(1, field1, operator1, value1);
		AdvancedSearch.add_DisplayFields(displayFieldsToAdd);
		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);

		TableData results = AdvancedSearch.return_SearchResults();
		AdvancedSearch.verify_ResultsPage_GreaterThan_Or_LessThan(results, field1, operator1, value1);
		AdvancedSearch.verify_Correct_DisplayFields(displayFieldsToAdd);
	}

	@Test(groups = { "all_Tests" })
	public void PT3135_AdvancedSearch_QueryBuilder_PaymentSequence_GreaterThan_SpecialChars_CauseError()
			throws Exception {
		String field = "Payment Sequence";
		String operator = "Is Greater Than";
		String value = "@#$";

		AdvancedSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}

	@Test(groups = { "all_Tests" })
	public void PT3137_AdvancedSearch_QueryBuilder_PaymentSequence_LessThan_SpecialChars_CauseError()
			throws Exception {
		String field = "Payment Sequence";
		String operator = "Is Less Than";
		String value = "@#$";

		AdvancedSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}

	@Test(groups = { "all_Tests" })
	public void PT3133_AdvancedSearch_QueryBuilder_PaymentSequence_Equals_SpecialChars_CauseError() throws Exception {
		String field = "Payment Sequence";
		String operator = "Equals";
		String value = "@#$";

		AdvancedSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}

	@Test(groups = { "all_Tests" })
	public void PT3133_AdvancedSearch_QueryBuilder_PaymentSequence_Equals_AlphaChars_CauseError() throws Exception {
		String field = "Payment Sequence";
		String operator = "Equals";
		String value = "abc";

		AdvancedSearch.verify_QueryBuilder_NonNumericChars_Error(1, field, operator, value);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3387_AdvancedSearch_WorkgroupSelector_NoResultsOnFirstSearch_SecondSearchWorks() throws Exception {
		String workgroup_NoResults = "2234321";
		
		AdvancedSearch.workGroupSelector.click_Reset();
		AdvancedSearch.workGroupSelector.close_List();
		//Navigation.close_ToastError_If_ItAppears();
		
		AdvancedSearch.workGroupSelector.verify_NoResults(workgroup_NoResults);
		AdvancedSearch.workGroupSelector.close_List();
		

		AdvancedSearch.workGroupSelector.search_forText(workgroup);
		AdvancedSearch.click_Search();
		AdvancedSearch.select_ShowRowsDropdownValue(100);
		TableData results = AdvancedSearch.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search results returned.", "Unable to find search results.");
		
	}
	
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3316_AdvancedSearch_QueryBuilder_AccountNumber_BeginsWith_96() throws
	// Exception {
	// String field = "Account Number";
	// String value = "96";
	//
	// AdvancedSearch.verify_Field_BeginsWith_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3316_AdvancedSearch_QueryBuilder_AccountNumber_EndsWith_50() throws
	// Exception {
	// String field = "Account Number";
	// String value = "50";
	//
	// AdvancedSearch.verify_Field_EndsWith_Value(1, field, value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3316_AdvancedSearch_QueryBuilder_AccountNumber_Equals_96372050()
	// throws Exception {
	// String field = "Account Number";
	// String value = "96372050";
	//
	// AdvancedSearch.verify_Field_Equals_Value(1, field, value);
	// }
	//

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3319_AdvancedSearch_PageFilters_PaymentAmount_Equals_9456() throws
	// Exception {
	// String field = "Payment Amount";
	// String value = "94.56";
	//
	// AdvancedSearch.verify_Field_Equals_Value(1, field, value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3319_AdvancedSearch_PageFilters_PaymentAmount_IsGreaterThan_80()
	// throws Exception {
	// String field = "Payment Amount";
	// String value = "80";
	//
	// AdvancedSearch.verify_Field_IsGreaterThan_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3319_AdvancedSearch_PageFilters_PaymentAmount_IsLessThanThan_80()
	// throws Exception {
	// String field = "Payment Amount";
	// String value = "80";
	//
	// AdvancedSearch.verify_Field_IsLessThan_Value(1, field, value);
	// }
	//

	// @Test(groups = { "all_Tests" })
	// public void PT3323_AdvancedSearch_QueryBuilder_RT_BeginsWith_08()
	// throws Exception {
	// String field = "R/T";
	// String value = "08";
	//
	// AdvancedSearch.verify_Field_BeginsWith_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void PT3323_AdvancedSearch_QueryBuilder_RT_EndsWith_71() throws
	// Exception {
	// String field = "R/T";
	// String value = "71";
	//
	// AdvancedSearch.verify_Field_EndsWith_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void PT3323_AdvancedSearch_QueryBuilder_RT_Equals_101380933()
	// throws Exception {
	// String field = "R/T";
	// String value = "101380933";
	//
	// AdvancedSearch.verify_Field_Equals_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void PT3323_AdvancedSearch_QueryBuilder_RT_Contains_809() throws
	// Exception {
	// String field = "R/T";
	// String value = "809";
	//
	// AdvancedSearch.verify_Field_Contains_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3317_AdvancedSearch_QueryBuilder_CheckTraceRef_BeginsWith_17() throws
	// Exception {
	// String field = "Check/Trace/Ref Number";
	// String value = "17";
	//
	// AdvancedSearch.verify_Field_BeginsWith_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3317_AdvancedSearch_QueryBuilder_CheckTraceRef_EndsWith_85() throws
	// Exception {
	// String field = "Check/Trace/Ref Number";
	// String value = "85";
	//
	// AdvancedSearch.verify_Field_EndsWith_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3317_AdvancedSearch_QueryBuilder_CheckTraceRef_Equals_2385() throws
	// Exception {
	// String field = "Check/Trace/Ref Number";
	// String value = "2385";
	//
	// AdvancedSearch.verify_Field_Equals_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3317_AdvancedSearch_QueryBuilder_CheckTraceRef_Contains_38() throws
	// Exception {
	// String field = "Check/Trace/Ref Number";
	// String value = "38";
	//
	// AdvancedSearch.verify_Field_Contains_Value(1, field, value);
	// }
	
	// @Test(groups = { "all_Tests" })
		// public void
		// PT3132_AdvancedSearch_PageFilters_PaymentSequence_Equals_2() throws
		// Exception {
		// String field = "Payment Sequence";
		// String value = "2";
		//
		// AdvancedSearch.verify_Field_Equals_Value(1, field, value);
		// }

		// @Test(groups = { "all_Tests" })
		// public void
		// PT3134_AdvancedSearch_PageFilters_PaymentSequence_IsGreaterThan_2()
		// throws Exception {
		// String field = "Payment Sequence";
		// String value = "2";
		//
		// AdvancedSearch.verify_Field_IsGreaterThan_Value(1, field, value);
		// }
		//
		// @Test(groups = { "all_Tests" })
		// public void
		// PT3136_AdvancedSearch_PageFilters_PaymentSequence_IsLessThanThan_3()
		// throws Exception {
		// String field = "Payment Sequence";
		// String value = "2";
		//
		// AdvancedSearch.verify_Field_IsLessThan_Value(1, field, value);
		// }
	
	// @Test(groups = { "all_Tests" })
	// public void PT3128_AdvancedSearch_PageFilters_DDA_BeginsWith_814()
	// throws Exception {
	// String field = "DDA";
	// String value = "814";
	//
	// AdvancedSearch.verify_Field_BeginsWith_Value(1, field, value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3128_AdvancedSearch_PageFilters_DDA_BeginsWith_SpecialChars_NO_Results()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Begins With";
	// String value = "@!#";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3128_AdvancedSearch_PageFilters_DDA_BeginsWith_abc_NO_Results()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Begins With";
	// String value = "abc";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3128_AdvancedSearch_PageFilters_DDA_BeginsWith_EndingDigits_NO_Results()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Begins With";
	// String value = "789";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void PT3129_AdvancedSearch_PageFilters_DDA_Contains_980() throws
	// Exception {
	// String field = "DDA";
	// String value = "980";
	//
	// AdvancedSearch.verify_Field_Contains_Value(1, field, value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void PT3129_AdvancedSearch_PageFilters_DDA_Contains_End2Chars()
	// throws Exception {
	// String field = "DDA";
	// String value = "89";
	//
	// AdvancedSearch.verify_Field_Contains_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3129_AdvancedSearch_PageFilters_DDA_Contains_Middle2Chars() throws
	// Exception {
	// String field = "DDA";
	// String value = "12";
	//
	// AdvancedSearch.verify_Field_Contains_Value(1, field, value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3129_AdvancedSearch_PageFilters_DDA_Contains_abc_NO_Results() throws
	// Exception {
	// String field = "DDA";
	// String operator = "Contains";
	// String value = "abc";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3129_AdvancedSearch_PageFilters_DDA_Contains_Special_NO_Results()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Contains";
	// String value = "@!#";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void PT3130_AdvancedSearch_PageFilters_DDA_EndsWith_899() throws
	// Exception {
	// String field = "DDA";
	// String value = "899";
	//
	// AdvancedSearch.verify_Field_EndsWith_Value(1, field, value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_EndsWith_AlphaChars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Ends With";
	// String value = "abc";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_EndsWith_SpecialChars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Ends With";
	// String value = "@!#";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_EndsWith_Middle2Chars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Ends With";
	// String value = "80";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_EndsWith_First3Chars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Ends With";
	// String value = "814";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3131_AdvancedSearch_PageFilters_DDA_Equals_PassedInValue() throws
	// Exception {
	// String field = "DDA";
	// String value = "81428899";
	//
	// AdvancedSearch.verify_Field_Equals_Value(1, field, value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_Equals_StartingChars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Equals";
	// String value = "814";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_Equals_AlphaChars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Equals";
	// String value = "abc";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }
	//
	// @Test(groups = { "all_Tests" })
	// public void
	// PT3130_AdvancedSearch_PageFilters_DDA_Equals_SpecialChars_NoResults()
	// throws Exception {
	// String field = "DDA";
	// String operator = "Equals";
	// String value = "@#!";
	//
	// AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, operator,
	// value);
	// }

	// @Test(groups = { "all_Tests" })
	// public void
	// PT3159_AdvancedSearch_PageFilters_PaymentAmount_GreaterThan_NumberWithPeriod()
	// throws Exception {
	// String field = "Payment Amount";
	// String value = "50.";
	//
	// AdvancedSearch.verify_Field_IsGreaterThan_Value(1, field, value);
	// }

	// Test Case not matching page flow.
		// @Test(groups = { "all_Tests" })
		// public void
		// PT3315_AdvancedSearch_PageFilters_QueryBuilder_ComparisonNotSelected_GivesErrorMessage()
		// throws Exception {
		// String field = null;
		// String operator = null;
		// String value = "5";
		//
		// AdvancedSearch.verify_QueryBuilder_MissingRequiredField(1, field,
		// operator, value);
		// }

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.close_ToastError_If_ItAppears();
//		if (BaseUI.pageSourceContainsString("Please fill in all required criteria.")) {
//			BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
//		}

		if (BaseUI.elementExists("advancedSearch_Results_BreadcrumbHeaderLink_AdvancedSearch", null, null)) {
			AdvancedSearch.click_AdvancedSearch_Breadcrumb();
		}
		//Navigation.wait_for_LoadingSpinner_ToDisappear();
		AdvancedSearch.click_ClearSearchButton();
		AdvancedSearch.workGroupSelector.search_forText(workgroup);
		AdvancedSearch.setDateRange(date1, date2);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
