package r360.tests_ignored.advancedSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.ManageQueries;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_SavedQuery extends BaseTest {
	
	//TableData results;
	TableData paymentData;
	TableData stubData;

	private String workgroup = "2234322 - Automation-Filter";
	private String date1 = "12/06/2016";
	private String date2 = "12/07/2016";
	
	private String advancedSearch_QueryName = "AdvancedSearch_Query1";
	private String advancedSearch_QueryDescription = "First Query Description";
	String paymentType = "ACH";
	String paymentSource = "AutomationImageRPS";
	String sortBy = "DDA/Ascending";
	String searchCrit_field = "Payment Sequence";
	String searchCrit_Operator = "Is Greater Than";
	String searchCrit_Value = "1";
	String displayField = "Payment Sequence";

	
	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {
			//String date = ImportData.return_TodaysDate_FormattedForACH_CIE_Import();


			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.advncdSearch_Entity, GlobalVariables.advncdSearch_User, GlobalVariables.advncdSearch_Password, 0);

			Navigation.navigate_Search_ManageQueries();
			
			if(BaseUI.pageSourceContainsString(advancedSearch_QueryName))
			{
				ManageQueries.delete_Query(advancedSearch_QueryName);
			}
			
			Navigation.navigate_Search_AdvancedSearch();
	
			AdvancedSearch.create_SavedQuery(workgroup, date1, date2, paymentType, paymentSource, sortBy, searchCrit_field, searchCrit_Operator, 
					searchCrit_Value, displayField, advancedSearch_QueryName, advancedSearch_QueryDescription);
			
			Navigation.navigate_Search_ManageQueries();
			ManageQueries.uncheck_DefaultQuery(advancedSearch_QueryName);
			
			Navigation.navigate_Search_AdvancedSearch();

		}
		
	}

	
	@Test(groups = { "all_Tests" })
	public void PT3363_AdvancedSearch_SavedQuery_SelectQuery_ValuesMatch() throws Exception {
		AdvancedSearch.select_SavedQuery(advancedSearch_QueryName);
		AdvancedSearch.verify_Values_forAdvancedSearch(workgroup, paymentType, paymentSource, sortBy, searchCrit_field, searchCrit_Operator, 
				searchCrit_Value, displayField, advancedSearch_QueryName, advancedSearch_QueryDescription);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3363_AdvancedSearch_SavedQuery_NoDefaultQuery_ValuesAreDefaults() throws Exception {
		AdvancedSearch.verify_DefaultValues_forAdvancedSearch();
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3363_AdvancedSearch_SavedQuery_SetDefaultQuery_ValuesAreFromQuery() throws Exception {
		Navigation.navigate_Search_ManageQueries();
		ManageQueries.check_DefaultQuery(advancedSearch_QueryName);
		
		Navigation.navigate_Search_AdvancedSearch();
		AdvancedSearch.verify_Values_forAdvancedSearch(workgroup, paymentType, paymentSource, sortBy, searchCrit_field, searchCrit_Operator, 
				searchCrit_Value, displayField, advancedSearch_QueryName, advancedSearch_QueryDescription);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Search_ManageQueries();
		ManageQueries.uncheck_DefaultQuery(advancedSearch_QueryName);
		
		Navigation.navigate_Search_AdvancedSearch();
		

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.navigate_Search_ManageQueries();
			
			if(BaseUI.pageSourceContainsString(advancedSearch_QueryName))
			{
				ManageQueries.delete_Query(advancedSearch_QueryName);
			}
			
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
