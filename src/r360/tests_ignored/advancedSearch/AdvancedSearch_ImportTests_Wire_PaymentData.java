package r360.tests_ignored.advancedSearch;

import java.text.MessageFormat;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;


///
///Relies on Data imported from Import_Workgroups.java class.  Must run this class before running these tests.
///
public class AdvancedSearch_ImportTests_Wire_PaymentData extends BaseTest {
	
	TableData results;
	TableData paymentData;
	TableData stubData;

	private String workgroup = "2234322";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	//TC's covered: 266940, 266942, 266946, 272864, 273002, 235418
	//Data is based on our Import calls.
	
	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {
			//ImportData.import_Workgroup_1123211_Data_WithTodaysDate();
			
			//stubData = AdvancedSearch.return_StubData(ImportData.return_TodaysDate_FormattedForDatabase(), workgroup);
			paymentData = AdvancedSearch.return_PaymentData(ImportData.return_TodaysDate_FormattedForDatabase(),
					workgroup);

			paymentData.remove_Rows_WithColumnName_WhereValues_DoNOTMatch("Payment Type", "Wire");
			//stubData.remove_Rows_WithColumnName_WhereValues_DoNOTMatch("Payment Type", "Wire");
			
			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);

			AdvancedSearch.paymentTypeDropdown.select_EntityValue("Wire");
			
			String[] displayFields = {"Payment Type", "[PMT].BAI Bank Reference", "[PMT].BAI Customer Reference", "[PMT].BAI File Creation Date",
					"[PMT].BAI File ID", "[PMT].Customer Account Number", "[PMT].Type Code"};
			
			AdvancedSearch.add_DisplayFields(displayFields);
			
			AdvancedSearch.click_Search();

			AdvancedSearch.select_ShowRowsDropdownValue(100);

			// Extract table data.
			results = AdvancedSearch.return_SearchResults();
			results.remove_Character("$");
			paymentData.set_decimal_Precision(2);
			
			//stubData.set_decimal_Precision(2);

			BaseUI.verify_true_AndLog(results.data.size() == paymentData.data.size(),
					"Search results list size matched expected results list.",
					MessageFormat.format(
							"Search results list size {0} did NOT match expected results list of size {1}.",
							results.data.size(), paymentData.data.size()));
		}
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_Verify_ThereIsWireData() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Payment Type", "Wire", results.data);
	}
	
	



	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_PaymentType() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Type", paymentData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_BAI_Bank_Reference() throws Exception {
		results.replaceKey_ForGivenKey("[PMT].BAI Bank Reference", "BAI Bank Reference");
		BaseUI.verify_TableColumns_Match("BAI Bank Reference", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_BAI_Customer_Reference() throws Exception {
		results.replaceKey_ForGivenKey("[PMT].BAI Customer Reference", "BAI Customer Reference");
		BaseUI.verify_TableColumns_Match("BAI Customer Reference", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_BAI_File_Creation_Date() throws Exception {
		results.replaceKey_ForGivenKey("[PMT].BAI File Creation Date", "BAI File Creation Date");
		BaseUI.verify_TableColumns_Match("BAI File Creation Date", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_BAI_File_ID() throws Exception {
		results.replaceKey_ForGivenKey("[PMT].BAI File ID", "BAI File ID");
		BaseUI.verify_TableColumns_Match("BAI File ID", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_Customer_Account_Number() throws Exception {
		results.replaceKey_ForGivenKey("[PMT].Customer Account Number", "Customer Account Number");
		BaseUI.verify_TableColumns_Match("Customer Account Number", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void PT3410_AdvancedSearch_Type_Code() throws Exception {
		results.replaceKey_ForGivenKey("[PMT].Type Code", "Type Code");
		BaseUI.verify_TableColumns_Match("Type Code", paymentData.data, results.data);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
