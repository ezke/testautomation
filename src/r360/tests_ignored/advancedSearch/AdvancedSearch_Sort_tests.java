package r360.tests_ignored.advancedSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_Sort_tests extends BaseTest {

	TableData results;

	String workgroup = "";
	private String date1 = "";
	private String date2 = "";

	@BeforeClass
	public void setup_method() throws Exception {
		if (Browser.driver == null) {
			//Config_DataRetrieval.get_ConfigData();
			//Config_DataRetrieval.retrieve_DataSheet(Config_DataRetrieval.config_Data.get("fileName"));
			
//			workgroup = "99 - Omaha BankTesting";
//			date1 = "11/11/2016";
//			date2 = "11/11/2016";
			workgroup = "2234322";
			date1 = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
			date2 = date1;
			
			
			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);
			AdvancedSearch.click_Search();

			AdvancedSearch.select_ShowRowsDropdownValue(100);

			// Extract table data.
			results = AdvancedSearch.return_SearchResults();
			
			BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Advanced Search Results.", "Advanced Search Results were empty.");
		}
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3197_AdvancedSearch_TestingResults_CheckTraceRefNumber_Ascending() throws Exception {
		String headerText = "Check/Trace/Ref Number";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3197_AdvancedSearch_TestingResults_CheckTraceRefNumber_Descending() throws Exception {
		String headerText = "Check/Trace/Ref Number";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	@Test(groups = {"all_Tests"})
	public void PT3204_AdvancedSearch_TestingResults_AccountNumber_Ascending() throws Exception {
		String headerText = "Account Number";
		results.sort_ByColumn_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3204_AdvancedSearch_TestingResults_AccountNumber_Descending() throws Exception {
		String headerText = "Account Number";
		results.sort_ByColumn_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3205_AdvancedSearch_TestingResults_DDA_Ascending() throws Exception {
		String headerText = "DDA";
		results.sort_ByColumn_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3206_AdvancedSearch_TestingResults_DDA_Descending() throws Exception {
		String headerText = "DDA";
		results.sort_ByColumn_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3195_AdvancedSearch_TestingResults_BatchNumber_Ascending() throws Exception {
		String headerText = "Batch Number";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3195_AdvancedSearch_TestingResults_BatchNumber_Descending() throws Exception {
		String headerText = "Batch Number";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3196_AdvancedSearch_TestingResults_BatchID_Ascending() throws Exception {
		String headerText = "Batch ID";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void  PT3196_AdvancedSearch_TestingResults_BatchID_Descending() throws Exception {
		String headerText = "Batch ID";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3203_AdvancedSearch_TestingResults_TransactionNumber_Ascending() throws Exception {
		String headerText = "Transaction";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3203_AdvancedSearch_TestingResults_Transaction_Descending() throws Exception {
		String headerText = "Transaction";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	
	@Test(groups = {"all_Tests"})
	public void PT3202_AdvancedSearch_TestingResults_RT_Ascending() throws Exception {
		String headerText = "R/T";
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3202_AdvancedSearch_TestingResults_RT_Descending() throws Exception {
		String headerText = "R/T";
		results.sort_ByColumn_numeric_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	@Test(groups = {"all_Tests"})
	public void PT3199_AdvancedSearch_TestingResults_PaymentAmount_Ascending() throws Exception {
		String headerText = "Payment Amount";
		results.remove_Character("$", headerText);
		results.remove_Character(",", headerText);
		results.sort_ByColumn_numeric_Ascending(headerText);
		
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		newTableData.remove_Character("$", headerText);
		newTableData.remove_Character(",", headerText);
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3199_AdvancedSearch_TestingResults_PaymentAmount_Descending() throws Exception {
		String headerText = "Payment Amount";
		results.remove_Character("$", headerText);
		results.remove_Character(",", headerText);
		results.sort_ByColumn_numeric_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		newTableData.remove_Character("$", headerText);
		newTableData.remove_Character(",", headerText);
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	
	@Test(groups = {"all_Tests"})
	public void PT3200_AdvancedSearch_TestingResults_PaymentSource_Ascending() throws Exception {
		String headerText = "Payment Source";
		results.sort_ByColumn_Ascending(headerText);
		
		AdvancedSearch.sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	
	
	@Test(groups = {"all_Tests"})
	public void PT3200_AdvancedSearch_TestingResults_PaymentSource_Descending() throws Exception {
		String headerText = "Payment Source";
		results.sort_ByColumn_Descending(headerText);
		
		AdvancedSearch.sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();
		
		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}
	

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
