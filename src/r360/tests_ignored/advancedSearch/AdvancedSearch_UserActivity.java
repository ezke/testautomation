package r360.tests_ignored.advancedSearch;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.*;

public class AdvancedSearch_UserActivity extends BaseTest {

	TableData results;

	String workgroup = "";
	private String date1 = "";
	private String date2 = "";
	private String mainHandle;
	final String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
	String startDate = "";
	String endDate = "";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		if (Browser.driver == null) {
			date1 = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
			date2 = date1;
			Browser.openBrowser(GlobalVariables.baseURL, "firefox");
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);
			mainHandle = Browser.driver.getWindowHandle();
			startDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(todaysDate, "MM/dd/yyyy", "MM/dd/yyyy", -30);
			endDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(todaysDate, "MM/dd/yyyy", "MM/dd/yyyy", 30);
		}
	}
	
	@Test(groups = {"all_Tests","critical_Tests"})
	public void PT4020_AdvancedSearchUserACtivityQueryAuditing() throws Exception {
		String expectedTitle = "User Activity Report";
		Navigation.navigate_Search_ManageQueries();
		Navigation.navigate_Reports();
		Reports.expand_groupingHeader(Reports.receivables360_Online_ReportsText);
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, "User Activity Report");
		Reports.select_UserActivityReport_Values("PDF", date1, date2, "Dominic Giallombardo", "Page View");
		Reports.click_GenerateReport_Button();
		BaseUI.waitForElementToBeDisplayed("reportsPDF_UserActivityReport_ReportCreated", "", "", 5);
		Reports.verify_PDF_Title_MatchesExpected(expectedTitle);
		FileOperations.wait_For_File_To_Exist(Browser.downloadLocation + "\\" + Reports.userActivityReport_CSVFileName);
		Reports.verify_PDF_FirstRow_HasValues(startDate, endDate, "WFS", "Submitted / Retrieved Data", "DomGia");
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		BaseUI.close_ExtraWindows(mainHandle);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();

		}
	}
}
