package r360.tests_ignored.advancedSearch;

import java.text.MessageFormat;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;


///
///Relies on Data imported from Import_Workgroups.java class.  Must run this class before running these tests.
///
public class AdvancedSearch_ImportTests extends BaseTest {
	
	TableData results;
	TableData paymentData;
	TableData stubData;

	private String workgroup = "1123211";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	//TC's covered: 266940, 266942, 266946, 272864, 273002, 235418
	//Data is based on our Import calls.
	
	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {
			//ImportData.import_Workgroup_1123211_Data_WithTodaysDate();
			
			stubData = AdvancedSearch.return_StubData(ImportData.return_TodaysDate_FormattedForDatabase(), workgroup);
			paymentData = AdvancedSearch.return_PaymentData(ImportData.return_TodaysDate_FormattedForDatabase(),
					workgroup);

			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);

			AdvancedSearch.add_All_AvailableFields();
			AdvancedSearch.click_Search();

			AdvancedSearch.select_ShowRowsDropdownValue(100);

			// Extract table data.
			results = AdvancedSearch.return_SearchResults();
			results.remove_Character("$");
			paymentData.set_decimal_Precision(2);
			stubData.set_decimal_Precision(2);

			BaseUI.verify_true_AndLog(results.data.size() == paymentData.data.size(),
					"Search results list size matched expected results list.",
					MessageFormat.format(
							"Search results list size {0} did NOT match expected results list of size {1}.",
							results.data.size(), paymentData.data.size()));
		}
	}

	

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_DepositDates() throws Exception {
		paymentData.replace_DateFormats("Deposit Date", "yyyyMMdd", "MM/dd/yyyy");
		BaseUI.verify_TableColumns_Match("Deposit Date", paymentData.data, results.data);
		
		stubData.replace_DateFormats("Deposit Date", "yyyyMMdd", "MM/dd/yyyy");
		BaseUI.verify_TableColumns_Match("Deposit Date", stubData.data, results.data);

	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_BatchID() throws Exception {
		BaseUI.verify_TableColumns_Match("Batch ID", paymentData.data, results.data);
		BaseUI.verify_TableColumns_Match("Batch ID", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_BatchNumber() throws Exception {
		BaseUI.verify_TableColumns_Match("Batch Number", paymentData.data, results.data);
		BaseUI.verify_TableColumns_Match("Batch Number", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PaymentType() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Type", paymentData.data, results.data);
		BaseUI.verify_TableColumns_Match("Payment Type", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PaymentSource() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Source", paymentData.data, results.data);
		BaseUI.verify_TableColumns_Match("Payment Source", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_TransactionID() throws Exception {
		BaseUI.verify_TableColumns_Match("Transaction", paymentData.data, results.data);
		BaseUI.verify_TableColumns_Match("Transaction", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PaymentAmount() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Amount", paymentData.data, results.data);
		BaseUI.verify_TableColumns_Match("Payment Amount", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_INV_Amount() throws Exception {
		stubData.replaceKey_ForGivenKey("Amount", "[INV].Amount");
		BaseUI.verify_TableColumns_Match("[INV].Amount", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_INV_KFIType() throws Exception {
		stubData.replaceKey_ForGivenKey("KFIType", "[INV].KFIType");
		BaseUI.verify_TableColumns_Match("[INV].KFIType", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_INV_NewDateField() throws Exception {
		stubData.replaceKey_ForGivenKey("NewDateField", "[INV].NewDateField");
		stubData.replace_DateFormats("[INV].NewDateField", "M/d/yyyy", "MM/dd/yyyy");
		BaseUI.verify_TableColumns_Match("[INV].NewDateField", stubData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_InvoiceBatchSequence() throws Exception {
		BaseUI.verify_TableColumns_Match("Invoice Batch Sequence", stubData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PMT_NewDateField() throws Exception {
		paymentData.replaceKey_ForGivenKey("NewDateField", "[PMT].NewDateField");
		paymentData.replace_DateFormats("[PMT].NewDateField", "M/d/yyyy", "MM/dd/yyyy");
		BaseUI.verify_TableColumns_Match("[PMT].NewDateField", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PMT_CourtesyAmount() throws Exception {
		paymentData.replaceKey_ForGivenKey("CourtesyAmount", "[PMT].CourtesyAmount");
		BaseUI.verify_TableColumns_Match("[PMT].CourtesyAmount", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PMT_KFIType() throws Exception {
		paymentData.replaceKey_ForGivenKey("KFIType", "[PMT].KFIType");
		BaseUI.verify_TableColumns_Match("[PMT].KFIType", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PMT_CC2Field() throws Exception {
		paymentData.replaceKey_ForGivenKey("CC2Field", "[PMT].CC2Field");
		BaseUI.verify_TableColumns_Match("[PMT].CC2Field", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_R_T() throws Exception {
		BaseUI.verify_TableColumns_Match("R/T", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_AccountNumber() throws Exception {
		BaseUI.verify_TableColumns_Match("Account Number", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_CheckTraceRefNumber() throws Exception {
		BaseUI.verify_TableColumns_Match("Check/Trace/Ref Number", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_DDA() throws Exception {
		BaseUI.verify_TableColumns_Match("DDA", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_Payer() throws Exception {
		BaseUI.verify_TableColumns_Match("Payer", paymentData.data, results.data);
	}

	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PaymentSequence() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Sequence", paymentData.data, results.data);
	}
	
	@Test(groups = { "all_Tests" })
	public void TC_AdvancedSearch_PaymentBatchSequence() throws Exception {
		BaseUI.verify_TableColumns_Match("Payment Batch Sequence", paymentData.data, results.data);
	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
