package r360.tests_ignored.advancedSearch;

import java.text.MessageFormat;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import r360.data.ImportData;
import r360.pages.AdvancedSearch;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class AdvancedSearch_Filters_DataDriven extends BaseTest {
	
	// TableData results;
	TableData paymentData;
	TableData stubData;

	private String workgroup = "2234322";
	private String date1 = ImportData.return_TodaysDate_FormattedForWebsite();
	private String date2 = date1;

	@BeforeClass
	public void setup_method() throws Throwable {
		if (Browser.driver == null) {
			// String date =
			// ImportData.return_TodaysDate_FormattedForACH_CIE_Import();

			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password, 0);

			Navigation.navigate_Search_AdvancedSearch();

			AdvancedSearch.workGroupSelector.search_forText(workgroup);
			AdvancedSearch.setDateRange(date1, date2);
		}
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "dataForTest")
	public Object[][] createData() {
		Object[][] linksToNavigate = { { "TC199480", "Check/Trace/Ref Number", "2385", "Equals" },
				{ "TC199480", "Check/Trace/Ref Number", "38", "Contains" },
				{ "TC199480", "Check/Trace/Ref Number", "17", "BeginsWith" },
				{ "TC199480", "Check/Trace/Ref Number", "85", "EndsWith" }, { "PT3128", "DDA", "814", "BeginsWith" },
				{ "PT3128", "DDA", "@!#", "NoResults_BeginsWith" },
				{ "PT3128", "DDA", "abc", "NoResults_BeginsWith" },
				{ "PT3128", "DDA", "789", "NoResults_BeginsWith" }, { "PT3129", "DDA", "980", "Contains" },
				{ "PT3129_End2Chars", "DDA", "89", "Contains" }, { "PT3129_Middle2Chars", "DDA", "12", "Contains" },
				{ "PT3129", "DDA", "abc", "NoResults_Contains" },
				{ "PT3129_SpecialChars", "DDA", "@!#", "NoResults_Contains" },
				{ "PT3130", "DDA", "899", "EndsWith" }, { "PT3130", "DDA", "abc", "NoResults_EndsWith" },
				{ "PT3130", "DDA", "@!#", "NoResults_EndsWith" }, { "PT3130", "DDA", "80", "NoResults_EndsWith" },
				{ "PT3130_EndsWith_First3Chars", "DDA", "814", "NoResults_EndsWith" },
				{ "PT3131", "DDA", "81428899", "Equals" }, { "PT3130", "DDA", "814", "NoResults_Equals" },
				{ "PT3130", "DDA", "abc", "NoResults_Equals" }, { "PT3130", "DDA", "@!#", "NoResults_Equals" },
				{ "PT3132", "Payment Sequence", "2", "Equals" },
				{ "PT3132", "Payment Sequence", "2", "IsGreaterThan" },
				{ "PT3136", "Payment Sequence", "3", "IsLessThan" },
				{ "PT3159_ContainsPeriod", "Payment Amount", "50.", "IsGreaterThan" },
				{ "PT3316", "Account Number", "96", "BeginsWith" },
				{ "PT3316", "Account Number", "50", "EndsWith" },
				{ "PT3316", "Account Number", "96372050", "Equals" },
				{ "PT3319", "Payment Amount", "94.56", "Equals" },
				{ "PT3319", "Payment Amount", "80", "IsGreaterThan" },
				{ "PT3319", "Payment Amount", "80", "IsLessThan" }, { "PT3323", "R/T", "08", "BeginsWith" },
				{ "PT3323", "R/T", "71", "EndsWith" }, { "PT3323", "R/T", "101380933", "Equals" },
				{ "PT3323", "R/T", "809", "Contains" } };

		return linksToNavigate;
	}

	@Test(dataProvider = "dataForTest", alwaysRun = true)
	public void PT2756_Step3_testLinks(String testCaseName, String field, String value, String resultType)
			throws Exception {

		switch (resultType) {
		case "Equals":
			AdvancedSearch.verify_Field_Equals_Value(1, field, value);
			break;
		case "Contains":
			AdvancedSearch.verify_Field_Contains_Value(1, field, value);
			break;
		case "BeginsWith":
			AdvancedSearch.verify_Field_BeginsWith_Value(1, field, value);
			break;
		case "EndsWith":
			AdvancedSearch.verify_Field_EndsWith_Value(1, field, value);
			break;
		case "IsGreaterThan":
			AdvancedSearch.verify_Field_IsGreaterThan_Value(1, field, value);
			break;
		case "IsLessThan":
			AdvancedSearch.verify_Field_IsLessThan_Value(1, field, value);
			break;
		case "NoResults_BeginsWith":
			AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, "Begins With", value);
			break;
		case "NoResults_Contains":
			AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, "Contains", value);
			break;
		case "NoResults_EndsWith":
			AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, "Ends With", value);
			break;
		case "NoResults_Equals":
			AdvancedSearch.verify_QueryBuilder_ReturnsNoResults(1, field, "Equals", value);
			break;
		default:
			BaseUI.verify_true_AndLog(false, "", MessageFormat.format("Unrecognized result type: {0}", resultType));
			break;
		}

	}

	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		// if (BaseUI.pageSourceContainsString("Please fill in all required
		// criteria.")) {
		// BaseUI.click(Locator.lookupElement("entityUserErrorPopupCrossCheckBox"));
		// Thread.sleep(2000);
		// }
		Navigation.close_ToastError_If_ItAppears();

		if (BaseUI.elementExists("advancedSearch_Results_BreadcrumbHeaderLink_AdvancedSearch", null, null)) {
			AdvancedSearch.click_AdvancedSearch_Breadcrumb();
		}
		AdvancedSearch.click_ClearSearchButton();
		AdvancedSearch.workGroupSelector.search_forText(workgroup);
		AdvancedSearch.setDateRange(date1, date2);

	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {
		try {
			Navigation.signOut();
		} finally {
			Browser.closeBrowser();
		}
	}

}
