package r360.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import r360.data.R360DatabaseAccess;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;
import wfsCommon.pages.DatePicker;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.FilterExpander;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class BatchSummary {

	public static final String PAYMENT_TYPE_DROPDOWN = "selectbox-paymenttypes";
	public static final String PAYMENT_SOURCE_DROPDOWN = "selectbox-paymentsources";
	public static FilterExpander workGroupSelector = new FilterExpander("filterExpander");
	public static DatePicker dashboard_depositDate = new DatePicker("dshbrd_Dash_DepositDate", null, null);
	public static Dropdown receivables_GroupBy_dropdown = new Dropdown("dshbrd_RecievablesSummary_GroupBy_Dropdown",
			null, null);

	public static void getToPage() throws Exception {
		Browser.openBrowser(GlobalVariables.environment.BaseURL);

		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_BatchSummary();
	}
	
	public static void enter_Date(WebElement dateField, String date) {
		BaseUI.click(dateField);
		dateField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		dateField.sendKeys(date);
	}

	public static void setDateRange(String dateFrom, String dateTo) {
		enter_Date(Locator.lookupElement("batchSum_fromDate"), dateFrom);
		enter_Date(Locator.lookupElement("batchSum_toDate"), dateTo);
	}

	public static void setDateRange(Date dateFrom, Date dateTo) {
		SimpleDateFormat datepickerFormat = new SimpleDateFormat("MM/dd/yyyy");
		String from = datepickerFormat.format(dateFrom);
		String to = datepickerFormat.format(dateTo);
		
		enter_Date(Locator.lookupElement("batchSum_fromDate"), from);
		enter_Date(Locator.lookupElement("batchSum_toDate"), to);
	}

	public static void setPaymentTypeDropdown(String value) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dropdwn_Dropdown_ByID",PAYMENT_TYPE_DROPDOWN, null), value);
	}

	public static void click_Search() throws Exception {
		BaseUI.click(Locator.lookupElement("batchSum_SearchButton"));
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_BatchSummary_Breadcrumb() throws Exception {
		BaseUI.click(Locator.lookupElement("batchSum_BreadcrumbHeaderLink_BatchSummary"));
		BaseUI.waitForElementToBeDisplayed("batchSum_BatchDetail_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void click_BatchDetail_Breadcrumb() throws Exception {
		BaseUI.click(Locator.lookupElement("batchSum_BreadcrumbHeaderLink_BatchDetail"));
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		Thread.sleep(2000);
	}

	public static void verify_Breadcrumb(String[] crumbArray) {
		String breadCrumb = BaseUI.getTextFromField(Locator.lookupElement("batchSum_Breadcrumb_CurrentPage"));
		String[] breadCrumbSplit = breadCrumb.split(">");

		BaseUI.verify_true_AndLog(crumbArray.length == breadCrumbSplit.length, "Bread Crumb had expected length.",
				"Bread Crumb length did NOT match.");

		for (int i = 0; i < crumbArray.length; i++) {
			BaseUI.baseStringCompare("Breadcrumb Text", crumbArray[i], breadCrumbSplit[i].trim());
		}
	}

	public static ArrayList<String> available_Columns() {
		ArrayList<WebElement> headerElements = Locator.lookup_multipleElements("batchSum_Results_TableHeaders", null,
				null);
		ArrayList<String> headers = new ArrayList<String>();
		for (WebElement header : headerElements) {
			String headerText = header.getAttribute("innerText");
			headers.add(headerText.trim());
		}

		return headers;
	}

    public static ArrayList<String> available_BatchDetailColumns() {
        ArrayList<WebElement> headerElements = Locator.lookup_multipleElements("batchSum_BatchDetail_TableHeaders", null,
                null);
        ArrayList<String> headers = new ArrayList<String>();
        for (WebElement header : headerElements) {
            String headerText = header.getAttribute("innerText");
            headers.add(headerText);
        }

        return headers;
    }

	public static TableData return_SearchResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = available_Columns();

		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(Locator.lookup_multipleElements("batchSum_ResultRows", null, null));
		for (Integer i = 1; i <= rowList.size(); i++) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			ArrayList<WebElement> cellList = new ArrayList<WebElement>();
			cellList.addAll(Locator.lookup_multipleElements("batchSum_ResultRowByIndex", i.toString(), null));

			for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++) {
				String cellText = cellList.get(headerIndex).getAttribute("innerText");
				dataRow.put(headers.get(headerIndex), cellText.trim());
			}

			tableData.data.add(dataRow);
		}

		tableData.replace_Null_With_Empty();
		// tableData.replace_DateFormat_shortened();
		// tableData.remove_TrailingZeros_fromNumbers();
		return tableData;
	}

		public static TableData return_DetailSearchResults() throws Exception {
			TableData tableData = new TableData();
			ArrayList<String> headers = available_BatchDetailColumns();

			ArrayList<WebElement> rowList = new ArrayList<WebElement>();
			rowList.addAll(Locator.lookup_multipleElements("batchSum_BatchDetail_ResultRows", null, null));
			for (Integer i = 1; i <= rowList.size(); i++) {

				HashMap<String, String> dataRow = new HashMap<String, String>();
				ArrayList<WebElement> cellList = new ArrayList<WebElement>();
				cellList.addAll(Locator.lookup_multipleElements("batchSum_BatchDetail_ResultRowByIndex", i.toString(), null));

				for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++) {
					String cellText = cellList.get(headerIndex).getAttribute("innerText");
					dataRow.put(headers.get(headerIndex), cellText);
				}
				if (dataRow.containsKey("Transaction")) {
					dataRow.put("Batch", dataRow.get("Transaction"));
					dataRow.remove("Transaction");
				}

				tableData.data.add(dataRow);
			}

		tableData.replace_Null_With_Empty();
		// tableData.replace_DateFormat_shortened();
		// tableData.remove_TrailingZeros_fromNumbers();
		return tableData;
	}

	// rowNumber starts at 1.
	public static void click_Cell_BatchSummary(String columnName, Integer rowNumber) throws Exception {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("batchSum_ResultRowByIndex", rowNumber.toString(), null));

		BaseUI.click(cellList.get(headerIndex));
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();

	}

    // rowNumber starts at 1.
    public static void click_Cell_BatchDetail(String columnName, Integer rowNumber) throws Exception {
        ArrayList<String> headers = available_BatchDetailColumns();
        Integer headerIndex = headers.indexOf(columnName);

        ArrayList<WebElement> cellList = new ArrayList<WebElement>();
        cellList.addAll(Locator.lookup_multipleElements("batchSum_BatchDetail_ResultRowByIndex", rowNumber.toString(), null));

        BaseUI.click(cellList.get(headerIndex));
        Navigation.wait_for_LoadingSpinner_ToAppear();
        Navigation.wait_for_LoadingSpinner_ToDisappear();

    }

	// rowNumber numbers start at 1.
	public static WebElement return_BatchDetails_Cell_ByPassedInHeader_AndRowNumber(String columnName,
			Integer rowNumber) {
		ArrayList<WebElement> headerList = Locator.lookup_multipleElements("batchSum_BatchDetail_TableHeaders", null,
				null);

		Integer headerNumber = 0;
		for (int i = 0; i < headerList.size(); i++) {
			String headerText = BaseUI.getTextFromField(headerList.get(i));
			if (headerText.equals(columnName)) {
				headerNumber = headerNumber + 1;
			}
		}

		WebElement tableRow = Locator.lookupElement("batchSum_BatchDetails_TableRow_ByPassedInIndex", rowNumber.toString(),
				null);

		WebElement correctCell = tableRow.findElement(By.xpath("./td[" + headerNumber.toString() + "]"));

		return correctCell;
	}

	public static void click_BatchDetailsCell_ByHeaderText_AndRowNumber(String columnName, Integer rowNumber)
			throws Exception {
		WebElement elementToClick = return_BatchDetails_Cell_ByPassedInHeader_AndRowNumber(columnName, rowNumber);

		BaseUI.click(elementToClick);
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void verify_ClickingBatchSummary_TakesYouToBatchDetails_Page(Integer batchLineToClick)
			throws Exception {

		TableData tableRowData = BatchSummary.return_SearchResults();

		click_Cell_BatchSummary("Batch ID", batchLineToClick);
		Thread.sleep(100);

		String batchDetail_DepositDate = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Deposit Date:", null));

		String batchDetail_BatchNumber = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Batch:", null));

		String batchDetail_BatchID = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Batch ID:", null));

		BaseUI.baseStringCompare("Deposit Date", tableRowData.data.get(batchLineToClick - 1).get("Deposit Date"),
				batchDetail_DepositDate);

		BaseUI.baseStringCompare("Batch ID", tableRowData.data.get(batchLineToClick - 1).get("Batch ID"),
				batchDetail_BatchID);

		BaseUI.baseStringCompare("Batch Number", tableRowData.data.get(batchLineToClick - 1).get("Batch"),
				batchDetail_BatchNumber);

		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_BatchDetail_Header"));
	}

	public static void click_Header_ByText(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("batchSum_Results_TableHeaders_ByText", headerText, null);
		BaseUI.scroll_to_element(headerElement);
		BaseUI.click(headerElement);
		Thread.sleep(1500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void sort_ByHeader_Ascending(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("batchSum_Results_TableHeaders_ByText", headerText, null);
		if (headerElement.getAttribute("aria-sort") == null
				|| !BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "ascending")) {
			click_Header_ByText(headerText);

			if (!BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "ascending")) {
				click_Header_ByText(headerText);
			}

		}

	}

	public static void sort_ByHeader_Descending(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("batchSum_Results_TableHeaders_ByText", headerText, null);
		if (headerElement.getAttribute("aria-sort") == null
				|| !BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "descending")) {
			click_Header_ByText(headerText);

			if (!BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "descending")) {
				click_Header_ByText(headerText);
			}
		}
	}

	public static String return_Text_FromPDF_Field(String field) {
		WebElement pdfTextElement = Locator.lookupElement("batchSum_BatchPDF_FindValue_ByText", field, null);
		String pdfText = BaseUI.getTextFromField(pdfTextElement).trim();

		return pdfText;
	}

	public static void verify_ClickingBatchDetailTransacation_TakesYouToTransactionDetails_Page(
			Integer batchSumLineToClick, Integer batchDetailLineToClick) throws Exception {

		click_Cell_BatchSummary("Batch ID", batchSumLineToClick);
		Thread.sleep(100);

		String batchDetailWorkgroup = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Workgroup:", null));

		String batchDetail_DepositDate = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Deposit Date:", null));

		String batchDetail_BatchNumber = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Batch:", null));

		String batchDetail_BatchID = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_BatchDetail_Header_BatchDetail_ByText", "Batch ID:", null));

		BatchSummary.click_BatchDetailsCell_ByHeaderText_AndRowNumber("Transaction", batchDetailLineToClick);

		String transactionDetailWorkgroup = BaseUI.getTextFromField(Locator
				.lookupElement("batchSum_TransactionDetail_Header_TransactionDetail_ByText", "Workgroup:", null));

		String transactionDetail_DepositDate = BaseUI.getTextFromField(Locator
				.lookupElement("batchSum_TransactionDetail_Header_TransactionDetail_ByText", "Deposit Date:", null));

		String transactionDetail_BatchNumber = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_TransactionDetail_Header_TransactionDetail_ByText", "Batch:", null));

		String transactionDetail_BatchID = BaseUI.getTextFromField(
				Locator.lookupElement("batchSum_TransactionDetail_Header_TransactionDetail_ByText", "Batch ID:", null));

		BaseUI.baseStringCompare("Workgroup", batchDetailWorkgroup, transactionDetailWorkgroup);

		BaseUI.baseStringCompare("Deposit Date", batchDetail_DepositDate, transactionDetail_DepositDate);

		BaseUI.baseStringCompare("Batch ID", batchDetail_BatchID, transactionDetail_BatchID);

		BaseUI.baseStringCompare("Batch Number", batchDetail_BatchNumber, transactionDetail_BatchNumber);

		BaseUI.verifyElementAppears(Locator.lookupElement("batchSum_TransactionDetail_Header"));
	}

	public static WebElement return_Link_ByColumnName_AndColumnRow(String columnName, Integer rowNumber) {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("batchSum_ResultRowByIndex", rowNumber.toString(), null));

		return cellList.get(headerIndex);
	}

	public static ArrayList<String> available_Columns_For_TransactionDetailRelatedItem() {
		ArrayList<WebElement> headerElements = Locator
				.lookup_multipleElements("batchSum_TransactionDetail_RelatedItems_TableHeaders", null, null);
		ArrayList<String> headers = new ArrayList<String>();
		for (WebElement header : headerElements) {
			String headerText = header.getAttribute("innerText");
			headers.add(headerText);
		}

		return headers;
	}

	public static TableData return_TransactionDetailRelatedItemResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = available_Columns_For_TransactionDetailRelatedItem();

		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(
				Locator.lookup_multipleElements("batchSum_TransactionDetail_RelatedItems_ResultRows", null, null));
		for (Integer i = 1; i <= rowList.size(); i++) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			ArrayList<WebElement> cellList = new ArrayList<WebElement>();
			cellList.addAll(Locator.lookup_multipleElements(
					"batchSum_TransactionDetail_RelatedItems_ResultRows_ByIndex", i.toString(), null));

			for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++) {
				String cellText = cellList.get(headerIndex).getAttribute("innerText");
				dataRow.put(headers.get(headerIndex), cellText);
			}
			// if (dataRow.containsKey("Batch Id")) {
			// dataRow.put("Batch ID", dataRow.get("Batch Id"));
			// dataRow.remove("Batch Id");
			// }

			tableData.data.add(dataRow);
		}

		tableData.replace_Null_With_Empty();
		// tableData.replace_DateFormat_shortened();
		// tableData.remove_TrailingZeros_fromNumbers();
		return tableData;
	}

	public static WebElement return_Link_ByColumnName_AndColumnRow_ForToolTip(String columnName, Integer rowNumber) {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(
				Locator.lookup_multipleElements("batchSum_ResultRowByIndex_ToolTip", rowNumber.toString(), null));

		return cellList.get(headerIndex);
	}
	
	public static ArrayList<String> available_Columns_For_TransactionDetailPaymentItems() {
		ArrayList<WebElement> headerElements = Locator
				.lookup_multipleElements("batchSum_TransactionDetail_PaymentItems_TableHeaders", null, null);
		ArrayList<String> headers = new ArrayList<String>();
		for (WebElement header : headerElements) {
			String headerText = header.getAttribute("innerText");
			headers.add(headerText);
		}

		return headers;
	}
	
	public static TableData return_TransactionDetailPaymentItemsResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = available_Columns_For_TransactionDetailPaymentItems();

		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(
				Locator.lookup_multipleElements("batchSum_TransactionDetail_PaymentItems_ResultRows", null, null));
		for (Integer i = 1; i <= rowList.size(); i++) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			ArrayList<WebElement> cellList = new ArrayList<WebElement>();
			cellList.addAll(Locator.lookup_multipleElements(
					"batchSum_TransactionDetail_PaymentItems_ResultRows_ByIndex", i.toString(), null));

			for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++) {
				String cellText = cellList.get(headerIndex).getAttribute("innerText");
				dataRow.put(headers.get(headerIndex), cellText);
			}
			// if (dataRow.containsKey("Batch Id")) {
			// dataRow.put("Batch ID", dataRow.get("Batch Id"));
			// dataRow.remove("Batch Id");
			// }

			tableData.data.add(dataRow);
		}

		tableData.replace_Null_With_Empty();
		// tableData.replace_DateFormat_shortened();
		// tableData.remove_TrailingZeros_fromNumbers();
		return tableData;
	}
	
	public static void enterTransactionDetail_TransactionNumber_And_ClickGo(Integer numberToEnter) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("batchSum_TransactionDetail_Transaction_TextBox"), numberToEnter.toString());
		BaseUI.click(Locator.lookupElement("batchSum_TransactionDetail_GoButton"));
		Thread.sleep(2000);
	}

	public static void setSearchTest(String searchText) {
		BaseUI.enterText(Locator.lookupElement("batchSub_Search_Text"), searchText);		
	}

	public static void setPaymentSourceDropdown(String value) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dropdwn_Dropdown_ByID",PAYMENT_SOURCE_DROPDOWN, null), value);
	}
}
