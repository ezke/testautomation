package r360.pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.DatabaseConnection;
import utils.Locator;
import utils.TableData;

public class NotificationsDetails {
	

	
	public static void clickFirstRow() throws Exception {
		BaseUI.click(Locator.lookupElement("notifications_Table_Row_Message"));
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static ArrayList<String> getColumns() {
		ArrayList<WebElement> availableFields_List = Locator.lookup_multipleElements("notificationsDetails_ColumnList",
				null, null);
		ArrayList<String> availableFields_Text = new ArrayList<String>();

		for (WebElement availableField : availableFields_List) {
			availableFields_Text.add(BaseUI.getTextFromField(availableField));
		}
		return availableFields_Text;
	}
	
	public static  String getAttachmentName(Date startDate) {
		SimpleDateFormat attachmentFormat = new SimpleDateFormat("HHmmss");
		return "SimpleFileDropAttachment" + attachmentFormat.format(startDate);
	}
	
	
	
	public static TableData getResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = getColumns();

		if (Browser.currentBrowser.equals("chrome")) {
			tableData = BaseUI.tableExtractorV2("notificationsDetails_TableBody", headers.toArray(new String[0]));

		} else {
			tableData = BaseUI.tableExtractor_ByCell("notificationsDetails_TableBody_IE",
					headers.toArray(new String[0]));
		}
		return tableData;
	}
	
	public static boolean checkDataImportQueue(String clientProcessCode, Date startDate) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		String query = 
				"DECLARE @clientprocess_code VARCHAR(99)\n" +
				"DECLARE @startdate DATETIME\n" +
				"SET @startdate = '" + formatter.format(DateUtils.addMinutes(startDate, -1)) + "'\n" +
				"SET @clientprocess_code = '" + clientProcessCode + "'\n" + 
				"SELECT TOP 1\n" + 
				"		[DataImportQueueID]   --Unique Import ID\n" + 
				"	FROM [WFSDB_R360].[RecHubSystem].[DataImportQueue]\n" + 
				"	WHERE ClientProcessCode = @clientprocess_code\n" +
				"		AND CreationDate > @startdate\n" +
				"		AND QueueType = 2" +
				"		AND QueueStatus = 150" +
				"		AND ResponseStatus = 0";

		Boolean responseSuccessful = false;
		for (int i = 0; i < 24; i++) {
			TableData tableData = DatabaseConnection.runSQLServerQuery(query);
			if (tableData.data.size() > 0) {
				responseSuccessful = true;
				break;
			} else {
				Thread.sleep(15000);
			}
		}
		return responseSuccessful;
	}
}
