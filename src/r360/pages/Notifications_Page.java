package r360.pages;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Function;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import r360.data.LogFile_Scan;
import r360.data.R360DatabaseAccess;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.Locator;
import utils.TableData;

public class Notifications_Page {
	public static void getToPage() throws Exception {
		Browser.openBrowser(GlobalVariables.environment.BaseURL, "chrome");

		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Notifications();
	}
	
	public static void enterAttachmentName(String text) throws Exception {
		BaseUI.waitForElementToBeDisplayed("notificationsDetails_pageIdentifier", null, null, 10);
		Thread.sleep(1000);
		BaseUI.enterText(Locator.lookupElement("notifications_AttachmentName_Input"), text);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static boolean IsDupDetectActive() throws Exception {
		String sqlQuery =
				"SELECT Value" +
				"  FROM [WFSDB_R360].[RecHubConfig].[SystemSetup]" +
				"  Where Section='FIT'" +
				"	And SetupKey='FITDuplicateCheck'" +
				"	And Value = 1";
		return R360DatabaseAccess.query(sqlQuery).data.size() > 0;
	}

	public static void enterDateRange(Date fromDate, Date toDate) throws Exception {
		
		SimpleDateFormat datepickerFormat = new SimpleDateFormat("MM/dd/yyyy");
		WebElement fromControl = Locator.lookupElement("notifications_FromDate_Input"); 
		WebElement toControl = Locator.lookupElement("notifications_ToDate_Input"); 
		String from = datepickerFormat.format(fromDate);
		String to = datepickerFormat.format(toDate);
		
		clearAndSendKeys(fromControl, from);
		clearAndSendKeys(toControl, to);
	}

	public static void clickSearch() throws Exception {
		BaseUI.click(Locator.lookupElement("notifications_Search_Button"));
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void clickFirstRow() throws Exception {
		BaseUI.click(Locator.lookupElement("notifications_Table_Row_Message"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static ArrayList<String> getColumns() {
		ArrayList<WebElement> availableFields_List = Locator.lookup_multipleElements("notifications_ColumnList",
				null, null);
		ArrayList<String> availableFields_Text = new ArrayList<String>();

		for (WebElement availableField : availableFields_List) {
			availableFields_Text.add(BaseUI.getTextFromField(availableField));
		}
		return availableFields_Text;
	}

	public static TableData getResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = getColumns();

		if (Browser.currentBrowser.equals("chrome")) {
			tableData = BaseUI.tableExtractorV2("notifications_TableBody", headers.toArray(new String[0]));

		} else {
			tableData = BaseUI.tableExtractor_ByCell("notifications_TableBody_IE",
					headers.toArray(new String[0]));
		}
		return tableData;
	}
	
	private static void clearAndSendKeys(WebElement control, String value) {
		control.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		control.sendKeys(value);
	}
	
	public static boolean checkDataImportQueue(String clientProcessCode, Date startDate) throws Exception {
		try {
			BaseUI.wait_ForCondition_ToBeMet(
				()->R360DatabaseAccess.inDataImportQueue(clientProcessCode, 2, 0, startDate),
				180, 
				15000);
			return true;
		}
		catch(Exception ex) {
			return false;
		}
	}

	public static String getAttachmentName(Date startDate) {
		SimpleDateFormat attachmentFormat = new SimpleDateFormat("HHmmss");
		return "SimpleFileDropAttachment" + attachmentFormat.format(startDate);
	}
	
	public static boolean CheckLogForErrors(Date startDate) throws Exception 
	{
		String[] log = LogFile_Scan.getRecentLogLines(GlobalVariables.environment.ClientLogFolder, startDate);
		
		return any(log, (item)->((String)item).matches("[Ee]rror.*"));
	}
	
	public static boolean CheckLogForMileMarkers(Date startDate) throws Exception
	{
		String prefix = "^[^\\s]+ \\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2} [AP]M [^\\s]+\\s+";
		String[] patterns = {
				prefix + "Sending Notification files\\.\\.\\.\\s*$",
				prefix + "Sending Notification file: .*$",
				prefix + "Sending Notification data\\.\\.\\.\\s*$",
				prefix + "Notification data sent\\.",
				prefix + "Received response Xml from the server\\.",
				prefix + "Operation Successful",
				prefix + "Audit event successfully processed\\."
		};
		String[] log = LogFile_Scan.getRecentLogLines(GlobalVariables.environment.ClientLogFolder, startDate);
		return traceMileMarker(log, patterns, (item, filter)->((String)item).matches(((String)filter)));
	}
	
	private static boolean any(Object[] array, Function<Object, Boolean> check) {
		boolean result = false;
		
		for(Object item:array) {
			result |= check.apply(item);
		}
		return result;
	}
	
	private static interface matchMarkerOperation {
		boolean isMatch(Object item, Object marker);
	}
	
	private static boolean traceMileMarker(Object[] array, Object[] markers, matchMarkerOperation check) {
		boolean result = false;
		Queue<Object> markerQueue = new LinkedList<Object>(Arrays.asList(markers));
		
		for(Object item:array) {
			if(check.isMatch(item, markerQueue.peek())) {
				markerQueue.poll();
				if(markerQueue.peek()==null) {
					result = true;
					break;
				}
			}
		}
		return result;
	}
	
	public static String getAttachmentContents() throws Exception {
		String contents="";
		Thread.sleep(2000);
		File attachment = wait_For_Attachment_To_Download(Browser.downloadLocation, Locator.lookupElement("notifications_Detail_Download_Icon"));
		if(attachment != null) {
			contents=DataBuilder.Get_FileData_AsString(attachment);
		}
		else {
			BaseUI.log_AndFail("Unable to read downloaded attachment.");
		}
		return contents;
	}
	
	private static File getNewFile(File folder, Time minCreationTime) throws Exception {
		File result = null;
		
		for(File currentFile:folder.listFiles()) {
			BasicFileAttributes attr=Files.readAttributes(Paths.get(currentFile.getAbsolutePath()), BasicFileAttributes.class);
			Time fileTime = new Time(attr.creationTime().toMillis());
			if(fileTime.after(minCreationTime)) {
				result = currentFile;
				break;
			}
		}
		return result;
	}

	public static File wait_For_Attachment_To_Download(String folder, WebElement downloadElement) throws Exception {
		File file = null;
		Time startTime = new Time(System.currentTimeMillis());
		BaseUI.click(downloadElement);

		try {
			BaseUI.wait_ForCondition_ToBeMet(
				()->getNewFile(new File(folder), startTime) != null,
				3,
				500
			);
			file = getNewFile(new File(folder), startTime);
		}
		catch(Exception ex) {
			
		}
		return file;
	}
}
