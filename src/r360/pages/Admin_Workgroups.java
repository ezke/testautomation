package r360.pages;

import java.text.MessageFormat;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class Admin_Workgroups {

	public static final String workGroupSettings_GridID = "workgroupsBase";
	public static final String workGroupAssigned_GridID = "workgroupGrid";

	public static Pagination workGroupSettings_GRID = new Pagination(workGroupSettings_GridID);
	public static Pagination workGroupsAssigned_GRID = new Pagination(workGroupAssigned_GridID);

	public static void navigate_to_EntityWorkgroupDefaults_Tab() throws Exception {
		BaseUI.click(Locator.lookupElement("workgroups_EntityWorkGroupDefaults_Tab"));
		Thread.sleep(500);
	}

	public static void navigate_to_WorkgroupSettings_Tab() throws Exception {
		BaseUI.click_js(Locator.lookupElement("workgroups_WorkgroupSettings_Tab"));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void navigate_to_EntityAssignment_Tab() throws Exception {
		BaseUI.click(Locator.lookupElement("workgroups_EntityAssignment_Tab"));
		Thread.sleep(1000);
	}

	public static void launch_EditWorkgroupDefaults_Modal() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("workgroups_Defaults_AddButton"));
		BaseUI.waitForElementToBeDisplayed("wrkgrp_Modal", null, null);
		Thread.sleep(200);
	}

	public static void remove_AllWorkgroupsFromEntity() throws Exception {
		navigate_to_EntityAssignment_Tab();
		ArrayList<WebElement> workgroupList = new ArrayList<WebElement>();
		workgroupList.addAll(Locator.lookup_multipleElements("workgroups_Assign_RemoveWorkgroupLink", null, null));

		for (int i = 0; i < workgroupList.size(); i++) {
			WebElement unlinkLink = Locator.lookupElement("workgroups_Assign_RemoveWorkgroupLink");
			BaseUI.click(unlinkLink);
			Thread.sleep(500);
			BaseUI.click(Locator.lookupElement("workgroups_Assign_RemovePopup_Yes"));
			//Impossible to write a wait for this due to website design.  Seeing 3-4 loading spinners.
			Thread.sleep(20000);
		}

	}
	
	public static void remove_Workgroup_ForGivenWorkgroup(String columnName, String workgroupName) throws Exception{
		Pagination workGroupGrid = new Pagination(workGroupSettings_GridID);
		ArrayList<WebElement> columnList = new ArrayList<WebElement>();
		columnList.addAll(workGroupGrid.columns_Elements_List(columnName));
		
		WebElement removeButton = null;

		for (WebElement correctName : columnList) {
			if (correctName.getText().equals(workgroupName)) {
				removeButton = correctName.findElement(By.xpath(
						"./parent::div[contains(@class, 'ui-widget-content slick-row')][1]//a[./*[@class='fa fa-unlink']]"));
				break;
			}
		}
		BaseUI.click(removeButton);
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("workgroups_Assign_RemovePopup_Yes"));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// takes only the required fields.
	public static void addDefaultWorkGroup_Settings(String viewingDays, String maxSearchDays) throws Exception {
		navigate_to_EntityWorkgroupDefaults_Tab();
		launch_EditWorkgroupDefaults_Modal();
		Admin_Workgroups_EditWorkgroupDefaultsModal.set_Required_Workgroup_Defaults(viewingDays, maxSearchDays);

	}
	
	public static void launch_AddWorkgroupSettingsModal() throws Exception{
		navigate_to_WorkgroupSettings_Tab();
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_AddButton"));
		Thread.sleep(300);
	}

	public static void launch_AssignWorkgroupsmodal() throws Exception {
		navigate_to_EntityAssignment_Tab();
		BaseUI.click(Locator.lookupElement("workgroups_Assign_AssignWorkGroupsButton"));
		BaseUI.waitForElementToBeDisplayed("workgroups_AssignWorkGroups_Modal", null, null, 30);
		Thread.sleep(500);
	}

	public static void verify_WorkGroup_ActiveOrInactive_ByEntityAndWorkgroup(String entityName, String workgroupName,
			Boolean active) throws Exception {
		EntitySelector.selectNode(entityName);
		navigate_to_WorkgroupSettings_Tab();
		Pagination workGroupGrid = new Pagination(workGroupSettings_GridID);
		// Running into weird issue where order of table cells changes on first
		// navigating to page.
		// Sorting by a column gets the table cells to be rearranged in the
		// correct order.
		workGroupGrid.sort_Column_Ascending("Long Name");
		workGroupGrid.sort_Column_Descending("Long Name");
		WebElement gridRow = workGroupGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workgroupName);
		if(gridRow != null) {
			WebElement activeCell = gridRow
					.findElement(By.xpath(".//*[5][not(descendant::i)] | .//i[@class='fa fa-check']"));
			String activeCellClass = activeCell.getAttribute("class");
			Boolean testPass = activeCellClass.equals("fa fa-check") ? true : false;

			if (active) {
				BaseUI.verify_true_AndLog(testPass, MessageFormat.format("Workgroup {0} was active.", workgroupName),
						MessageFormat.format("Workgroup {0} was NOT active.", workgroupName));
			} else {
				BaseUI.verify_false_AndLog(testPass, MessageFormat.format("Workgroup {0} was NOT active.", workgroupName),
						MessageFormat.format("Workgroup {0} was active.", workgroupName));
			}
		}
	}
	
	public static void verify_WorkGroup_ActiveOrInactive_ByEntityAndWorkgroup_SortByAscending(String entityName, String workgroupName,
			Boolean active) throws Exception {
		EntitySelector.selectNode(entityName);
		navigate_to_WorkgroupSettings_Tab();
		Pagination workGroupGrid = new Pagination(workGroupSettings_GridID);
		// Running into weird issue where order of table cells changes on first
		// navigating to page.
		// Sorting by a column gets the table cells to be rearranged in the
		// correct order.
		workGroupGrid.sort_Column_Ascending("Long Name");
//		workGroupGrid.sort_Column_Descending("Long Name");
		WebElement gridRow = workGroupGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workgroupName);
		WebElement activeCell = gridRow
				.findElement(By.xpath(".//*[5][not(descendant::i)] | .//i[@class='fa fa-check']"));
		String activeCellClass = activeCell.getAttribute("class");
		Boolean testPass = activeCellClass.equals("fa fa-check") ? true : false;
		if (active) {
			BaseUI.verify_true_AndLog(testPass, MessageFormat.format("Workgroup {0} was active.", workgroupName),
					MessageFormat.format("Workgroup {0} was NOT active.", workgroupName));
		} else {
			BaseUI.verify_false_AndLog(testPass, MessageFormat.format("Workgroup {0} was NOT active.", workgroupName),
					MessageFormat.format("Workgroup {0} was active.", workgroupName));
		}
	}

	public static void verify_AllWorkGroups_ActiveOrInActive_ByEntity(String entityName) throws Exception {
		EntitySelector.selectNode(entityName);
		navigate_to_WorkgroupSettings_Tab();
		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		Pagination workGroupGrid = new Pagination(workGroupSettings_GridID);
		workGroupGrid.show_All();
		workGroupGrid.sort_Column_Ascending("Long Name");
		workGroupGrid.sort_Column_Descending("Long Name");

		rowList.addAll(Locator.lookup_multipleElements("Pagin_list_AllRows", workGroupSettings_GridID, ""));
		for (WebElement row : rowList) {
			WebElement activeCell = row
					.findElement(By.xpath(".//*[5][not(descendant::i)] | .//i[@class='fa fa-check']"));
			String activeCellClass = activeCell.getAttribute("class");
			Boolean testPass = activeCellClass.equals("fa fa-check") ? true : false;
			WebElement workgroupName = row.findElement(By.xpath(".//div[2]"));
			if(testPass == true){
				BaseUI.verify_true_AndLog(testPass,
						MessageFormat.format("Workgroup {0} was active.", workgroupName.getText()),
						MessageFormat.format("Workgroup {0} was NOT active.", workgroupName.getText()));
			}else{
				BaseUI.verify_false_AndLog(testPass,
						MessageFormat.format("Workgroup {0} was NOT active.", workgroupName.getText()),
						MessageFormat.format("Workgroup {0} was active.", workgroupName.getText()));
			}
//			BaseUI.verify_true_AndLog(testPass,
//					MessageFormat.format("Workgroup {0} was active.", workgroupName.getText()),
//					MessageFormat.format("Workgroup {0} was NOT active.", workgroupName.getText()));
		}

	}

	public static void verify_WorkGroup_Assigned_ToEntity(String entityName, String workgroupName) throws Exception {
		EntitySelector.selectNode(entityName);
		navigate_to_EntityAssignment_Tab();
		Pagination workGroupGrid = new Pagination(workGroupAssigned_GridID);
		workGroupGrid.sort_Column_Ascending("Long Name");
		workGroupGrid.sort_Column_Descending("Long Name");
//		WebElement gridRow = workGroupGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workgroupName);

	}

	public static void verify_WorkGroup_NOTActive_ByEntityAndWorkgroup(String entityName, String workgroupName)
			throws Exception {
		EntitySelector.selectNode(entityName);
		navigate_to_WorkgroupSettings_Tab();
		Pagination workGroupGrid = new Pagination(workGroupSettings_GridID);
		// Running into weird issue where order of table cells changes on first
		// navigating to page.
		// Sorting by a column gets the table cells to be rearranged in the
		// correct order.
		workGroupGrid.sort_Column_Ascending("Long Name");
		workGroupGrid.sort_Column_Descending("Long Name");
		WebElement gridRow = workGroupGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workgroupName);
		WebElement activeCell = gridRow
				.findElement(By.xpath(".//*[5][not(descendant::i)] | .//i[@class='fa fa-check']"));
		String activeCellClass = activeCell.getAttribute("class");
		Boolean testPass = activeCellClass.contains("slick-cell") ? true : false;
		BaseUI.verify_true_AndLog(testPass, MessageFormat.format("Workgroup {0} was inactive.", workgroupName),
				MessageFormat.format("Workgroup {0} was active.", workgroupName));
	}

	public static void launch_EditWorkGroupSettings_Modal(String workgroupName) throws Throwable {
		WebElement element = Admin_Workgroups.workGroupSettings_GRID.editButton_ByColumnName_andColumnText("Long Name", workgroupName);
		BaseUI.click(element);
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void launch_EditWorkGroupSettings_Modal_ForGivenWorkgroupID(String workgroupID) throws Throwable {
		BaseUI.click(Admin_Workgroups.workGroupSettings_GRID.editButton_ByColumnName_andColumnText("Workgroup ID", workgroupID));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

}
