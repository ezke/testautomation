package r360.pages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import java.util.HashMap;
import java.util.List;

public class PreDepositExceptionsPage {
    public void searchPreDeposit(String text) {
        WebElement searchBox = Locator.lookupElement("preDepositSearchBox");
        BaseUI.enterText_IntoInputBox(searchBox, text);
    }

    public PreDepositExceptionsBatchDetailPage clickGridItem(int row, String column) throws Exception {
        WebElement cell = Locator.lookupElement("preDepositGrid", Integer.toString(row), column);
        BaseUI.click(cell);
        PreDepositExceptionsBatchDetailPage nextPage = new PreDepositExceptionsBatchDetailPage();
        nextPage.waitForPageToLoad();
        return nextPage;
    }

    public void sortBy(String column, boolean sortAscending) throws Exception {
        WebElement header = Locator.lookupElement("preDepositHeader", column, null);
        String currentSort = header.getAttribute("class").replace("alignRight ", "");
        HashMap<String, Integer> clickTable = new HashMap<String, Integer>(){{
            put("sorting", sortAscending?1:2);
            put("sorting_asc", sortAscending?0:1);
            put("sorting_desc", sortAscending?1:0);
        }};
        for(int clicksLeft = clickTable.get(currentSort); clicksLeft>0; clicksLeft--) {
            BaseUI.click(header);
            Navigation.wait_for_LoadingSpinner_ToDisappear();
        }
    }

    public String[] GetGridHeaderList() {
        List<WebElement> headers = Locator.lookup_multipleElements("preDepositHeaders", null, null);
        return headers.stream().map(item-> BaseUI.get_Attribute_FromField(item, "innerText")).toArray(String[]::new);
    }

    public String getGridItemText(int row, String column) {
        WebElement cell = Locator.lookupElement("preDepositGrid", Integer.toString(row), column);
        return BaseUI.getTextFromField(cell);
    }
}
