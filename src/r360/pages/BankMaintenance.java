package r360.pages;

import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.Pagination;

public class BankMaintenance {

	public static Pagination bankGrid = new Pagination("bankGrid");

	public static void launch_AddBank_Modal() throws Exception {
		BaseUI.click(Locator.lookupElement("BankMain_btn_Add"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("BankAddModal_btn_Cancel", null, null);
	}

	// Pass True to save, False to cancel.
	public static void add_Bank(String entity, String id, String name, Boolean saveOrCancel) throws Exception {
		if (!entity.equals("")) {
			BaseUI.click(Locator.lookupElement("BankAddModal_dropdown_EntityFI"));
			Thread.sleep(200);
			BaseUI.click(Locator.lookupElement("BankAddModal_option_for_EntityFI", entity, null));
		}
		if (!id.equals("")) {
			BaseUI.enterText(Locator.lookupElement("BankAddModal_txt_Id"), id);
		}
		if (!name.equals("")) {
			BaseUI.enterText(Locator.lookupElement("BankAddModal_txt_Name"), name);
			Thread.sleep(200);
		}

		if (saveOrCancel) {
			click_AddBank_Save();
		} else {
			click_AddBank_Cancel();
		}
	}

	public static void click_AddBank_Cancel() throws Exception {
		BaseUI.click(Locator.lookupElement("BankAddModal_btn_Cancel"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_AddBank_Save() throws Exception {
		BaseUI.click(Locator.lookupElement("BankAddModal_btn_Save"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Overload, don't pass in a bool and it won't cancel or save. It will
	// remain on Add Bank modal
	public static void add_Bank(String entity, String id, String name) throws Exception {
		if (!entity.equals("")) {
			BaseUI.click(Locator.lookupElement("BankAddModal_dropdown_EntityFI"));
			Thread.sleep(200);
			BaseUI.click(Locator.lookupElement("BankAddModal_option_for_EntityFI", entity, null));
		}
		if (!id.equals("")) {
			BaseUI.enterText(Locator.lookupElement("BankAddModal_txt_Id"), id);
		}
		if (!name.equals("")) {
			BaseUI.enterText(Locator.lookupElement("BankAddModal_txt_Name"), name);
			Thread.sleep(200);
		}

		Thread.sleep(500);

	}

	public static void sort_ID_Column_Ascending() throws Exception {
		for (int i = 0; i < 4; i++) {
			WebElement header_Icon_Element = Locator.lookupElement("BankMain_header_Id_sort_icon");
			String header_Icon_Class = header_Icon_Element.getAttribute("class");
			if (header_Icon_Class.equals("slick-sort-indicator slick-sort-indicator-asc")) {
				// if it's sorted ascending break out of the loop.
				i = 4;
			} else {
				click_BankMaintenance_Header_Id_Sort_Icon();
			}
		}
		Thread.sleep(400);
	}

	public static void sort_ID_Column_Descending() throws Exception {
		for (int i = 0; i < 4; i++) {
			WebElement header_Icon_Element = Locator.lookupElement("BankMain_header_Id_sort_icon");
			String header_Icon_Class = header_Icon_Element.getAttribute("class");
			if (header_Icon_Class.equals("slick-sort-indicator slick-sort-indicator-desc")) {
				// if it's sorted ascending break out of the loop.
				i = 4;
			} else {
				click_BankMaintenance_Header_Id_Sort_Icon();
			}
		}
		Thread.sleep(400);
	}

	public static void click_BankMaintenance_Header_Id_Sort_Icon() throws Exception {
		BaseUI.click(Locator.lookupElement("BankMain_header_Id_sort_icon"));
		Thread.sleep(150);
	}

	// Does the name appear in the Name list?
	public static Boolean bool_NameInList(String name) {
		ArrayList<WebElement> list_of_names_elements = new ArrayList<WebElement>();
		list_of_names_elements.addAll(Locator.lookup_multipleElements("BankMain_list_of_Names", null, null));

		ArrayList<String> list_of_names_text = new ArrayList<String>();
		for (WebElement name_element : list_of_names_elements) {
			list_of_names_text.add(name_element.getText());
		}

		if (list_of_names_text.size() == 0) {
			Assert.fail("No names were found in list.");
		}

		return list_of_names_text.contains(name);

	}

	// Does the ID appear in the ID List?
	public static Boolean bool_idInList(String id) {
		ArrayList<WebElement> list_of_IDs_elements = new ArrayList<WebElement>();
		list_of_IDs_elements.addAll(Locator.lookup_multipleElements("BankMain_list_of_IDs", null, null));

		ArrayList<String> list_of_IDs_text = new ArrayList<String>();
		for (WebElement id_element : list_of_IDs_elements) {
			list_of_IDs_text.add(id_element.getText());
		}

		if (list_of_IDs_text.size() == 0) {
			Assert.fail("No ID's were found in list.");
		}

		return list_of_IDs_text.contains(id);

	}

	// Does the Entity appear in the entity list?
	public static Boolean bool_entityInList(String entity) {
		ArrayList<WebElement> list_of_entities_elements = new ArrayList<WebElement>();
		list_of_entities_elements.addAll(Locator.lookup_multipleElements("BankMain_list_of_Entities", null, null));

		ArrayList<String> list_of_entities_text = new ArrayList<String>();
		for (WebElement entity_element : list_of_entities_elements) {
			list_of_entities_text.add(entity_element.getText());
		}

		if (list_of_entities_text.size() == 0) {
			Assert.fail("No Entities were found in list.");
		}

		return list_of_entities_text.contains(entity);

	}

	// Verify the ID characters entered were invalid
	public static void AddBank_verifyID_charactersNotValid() throws InterruptedException {
		Thread.sleep(500);
		BaseUI.tabThroughField("BankAddModal_txt_Id");
		Thread.sleep(500);

		WebElement nameErrorLabel = Locator.lookupElement("BankAddModal_error_label_ID");
		BaseUI.verifyElementAppears(nameErrorLabel);
		BaseUI.verifyElementHasExpectedText("BankAddModal_error_label_ID", "ID must be 1-9 digits in length.");
	}

	// Add Bank ----Verify the Name characters entered are invalid.
	public static void AddBank_verifyName_charactersNotValid() throws InterruptedException {
		Thread.sleep(500);
		BaseUI.tabThroughField("BankAddModal_txt_Name");

		WebElement nameErrorLabel = Locator.lookupElement("BankAddModal_error_label_Name");
		BaseUI.verifyElementAppears(nameErrorLabel);
		BaseUI.verifyElementHasExpectedText("BankAddModal_error_label_Name",
				"Invalid Characters - Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.");

	}

	// *******Edit Bank*********//

	// Edit Bank Window. Pass in the ID for it to identify the correct bank to
	// edit.
	public static void launch_Edit_By_ID(String id) throws Throwable {
		BaseUI.click(Locator.lookupElement("BankMain_lnk_Edit_ByID", id, null));
		Thread.sleep(2000);
	}

	// Edit the Bank Name
	public static void editBank_name(String id, String new_name, Boolean saveChanges) throws Throwable {
		launch_Edit_By_ID(id);
		WebElement name_textbox_element = Locator.lookupElement("BankMain_EditBank_txt_Name");

		BaseUI.enterText(name_textbox_element, new_name);

		Thread.sleep(200);
		if (saveChanges) {
			BaseUI.click(Locator.lookupElement("BankMain_EditBank_btn_Save"));
		} else {
			BaseUI.click(Locator.lookupElement("BankMain_EditBank_btn_Cancel"));
		}
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Edit the Bank Name - Overload, doesn't click Save or Cancel.
	public static void editBank_name(String id, String new_name) throws Throwable {
		launch_Edit_By_ID(id);
		WebElement name_textbox_element = Locator.lookupElement("BankMain_EditBank_txt_Name");

		BaseUI.enterText(name_textbox_element, new_name);

		Thread.sleep(300);
	}

	// Verify the Name characters entered are invalid.
	public static void EditBank_verifyName_charactersNotValid() throws InterruptedException {
		Thread.sleep(500);
		BaseUI.tabThroughField("BankMain_EditBank_txt_Name");

		WebElement nameErrorLabel = Locator.lookupElement("BankAddModal_error_label_Name");
		BaseUI.verifyElementAppears(nameErrorLabel);
		BaseUI.verifyElementHasExpectedText("BankMain_EditBank_error_Name",
				"Invalid Characters - Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.");

	}

	// public static void bankgrid_clickNext() throws Throwable {
	// BaseUI.click(Locator.lookupElement("BankMain_btn_gridNavigate_next"));
	// Thread.sleep(1000);
	// }
	//
	// public static void bankgrid_clickFirst() throws Throwable {
	// BaseUI.click(Locator.lookupElement("BankMain_btn_gridNavigate_first"));
	// Thread.sleep(1000);
	// }
	// public static void bankgrid_clickPrevious() throws Throwable {
	// BaseUI.click(Locator.lookupElement("BankMain_btn_gridNavigate_prev"));
	// Thread.sleep(1000);
	// }
	// public static void bankgrid_clickFirst() throws Throwable {
	// BaseUI.click(Locator.lookupElement("BankMain_btn_gridNavigate_first"));
	// Thread.sleep(1000);
	// }

}// end of class
