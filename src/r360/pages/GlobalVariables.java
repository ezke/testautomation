package r360.pages;

import r360.EnvironmentDefinition.Environment;

import java.util.Random;

public class GlobalVariables {
	
	public static Environment environment = null;

	// Global variables. These shouldn't ever really need to be touched
	public static String baseURL = "https://rechubqaweb02.qalabs.nwk/Framework/";
	public static String dbUrl = "jdbc:sqlserver://rechubqadb02.qalabs.nwk;user=sa;password=Wausau#1;databaseName=WFSDB_R360";
	public static String raam_dbUrl = "jdbc:sqlserver://rechubqadb02.qalabs.nwk;user=sa;password=Wausau#1;databaseName=RAAM"; 
	
	public static String entityName = System.getProperty("entityName") == null ? "WFS" : System.getProperty("entityName");
	public static String login = System.getProperty("login") == null ? "DomGia" : System.getProperty("login");
	public static String password = System.getProperty("password") == null ? "Wausau#10"
			: System.getProperty("password");

	public static String entityName_AutomationPermission = "AutomationPermission";
	public static String userName_allPermissions = "AllPermissions";
	public static String passsword_allPermissions = "Wausau#1";

	public static String entityName_viewUserPageDisabled = "AutomationPermission";
	public static String userName_viewUserPageDisabled = "NoViewUsers";
	public static String passsword_viewUserPageDisabled = "Wausau#4";

	public static String entityName_viewEntityPageDisabled = "AutomationPermission";
	public static String userName_viewEntityPageDisabled = "NoViewEntity";
	public static String password_viewEntityPageDisabled = "Wausau#10";

	public static String entityName_viewRolesPageDisabled = "WFS";
	public static String userName_viewRolesPageDisabled = "NoViewRoles";
	public static String password_viewRolesPageDisabled = "Wausau#1";

	public static String entityName_viewGroupsPageDisabled = "WFS";
	public static String userName_viewGroupsPageDisabled = "NoViewGroups";
	public static String password_viewGroupsPageDisabled = "Wausau#1";

	public static String entityName_viewAdminPagesDisabled = "WFS";
	public static String userName_viewAdminPagesDisabled = "NoViewAdmin";
	public static String password_viewAdminPagesDisabled = "Wausau#1";

	public static String entityName_viewSubAdminPagesDisabled = "WFS";
	public static String userName_viewSubAdminPagesDisabled = "NoViewSubAdmin";
	public static String password_viewSubAdminPagesDisabled = "Wausau#1";
	
	public static String entityName_searchMenuPagesDisabled = "WFS";
	public static String userName_searchMenuPagesDisabled = "SearchMenuDisabled";
	public static String password_searchMenuPagesDisabled = "Wausau#1";
	
	public static String entityName_viewPaymentSearchPageDisabled = "WFS";
	public static String userName_viewPaymentSearchPageDisabled = "NoPymtSearch";
	public static String password_viewPaymentSearchPageDisabled = "Wausau#1";
	
	public static String entityName_viewBatchSummaryPageDisabled = "WFS";
	public static String userName_viewBatchSummaryPageDisabled = "NoBatchSummary";
	public static String password_viewBatchSummaryPageDisabled = "Wausau#2";
	
	public static String entityName_viewPaymentSourcePageDisabled = "WFS";
	public static String userName_viewPaymentSourcePageDisabled = "NoViewPaymentSource";
	public static String password_viewPaymentSourcePageDisabled = "Wausau#2";
	
	public static String entityName_ManagePaymentSourcePageDisabled = "WFS";
	public static String userName_ManagePaymentSourcePageDisabled = "NoManagePaymentSource";
	public static String password_ManagePaymentSourcePageDisabled = "Wausau#99";
	
	public static String advncdSearch_Entity = "WFS";
	public static String advncdSearch_User="AutoADVSearch";
	public static String advncdSearch_Password = "Wausau#1";

	public static String autoNoUnlock_Entity = "WFS";
	public static String autoNoUnlock_User="AutoNoUnlockUser";
	public static String autoNoUnlock_Password = "Wausau#1";
	
	public static String reportsPermission_Entity = "WFS";
	public static String reportsPermission_User = "ReportTest";
	public static String reportsPermission_Password = "Wausau#1";

	public static String automationImports_Entity = "LimitedPaymentTypes";
	public static String automationImports_User = "AutoLimitPayType";
	public static String automationImports_Password = "Wausau#1";
	
	
	// Generic Login Name, FistName, LastName and MI
	public static Random rand = new Random();
	public static int randomNum = GlobalVariables.rand.nextInt(1000);
	public static String genericUniqueLoginName = "Auto" + GlobalVariables.randomNum;
	public static String genericLoginName = "AutoTest123";
	public static String genericFirstName = "TestAuto";
	public static String genericLastName = "AutoTest";
	public static String genericMI = "A";

	// Generic Email Address
	public static String genericEmailAddress = "test123@wausaufs.com";

	// Generic Password`
	public static String genericPassword = "Wausau@1";
	public static String genericConfirmPassword = "Wausau@1";

	// Long strings that can be used for verifying 20, 21, 40 and 41 characters.
	// 20 and 40 could be done as substrings but this makes it slightly more
	// clear what the value is for these fields.
	public static String twentyCharacters = "Reginald Trachubuche";
	public static String twentyOneCharacters = "Reginald Trachubuchet";
	public static String twentyFiveCharacters = "North Troombilerviles Cit";
	public static String twentySixCharacters = "North Troombilerviles City";
	public static String thirtyCharacters = "1006573453426 Longish Boulevar";
	public static String thirtyOneChacters = "1006573453426 Longish Boulevard";
	public static String fortyCharacters = "van der Waloonapoolervillepersandenadaso";
	public static String fortyOneCharacters = "van der Waloonapoolervillepersandenadason";
	public static String genericAddUserLongString = "van der Waloonapoolervillepersandenadason van der Waloonapoolervillepersandenadason van der Waloonapoolervillepersandenadason van der Waloonapoolervillepersandenadason van der Waloonapoolervillepersandenadason van der Waloonapoolervillepersandenadason";
	public static String sixtyfiveCharacter = "van der Waloonapoolervillepersandenadason dheydheyahyehdyheitimex";

	// ACH API test variables. We might move these to a separate file eventually ... saz
	public static String searchURI = "https://rechubdevapp02.qalabs.nwk/integratedreceivables/api/batches/search";
	public static String achURI = "https://r36016qafile01.qalabs.nwk:8002/api/ACH";
	public static String goodAchFile = "/src/r360/data/ACH_ValidFile.txt";
	public static String badAchFile = "/src/r360/data/ACH_InvalidFile.txt";

	// R360 search URI
	public static String search2016Url = "https://r36016qaapp01.qalabs.nwk/integratedreceivables/api/batches/search";
	public static String getToken2016Url = "https://R36016QAApp01.qalabs.nwk/IntegratedReceivables/api/identity/token";


	// Accessing shared procs
	public static String databaseUrl = "jdbc:sqlserver://R36016QADB01.qalabs.nwk;user=sa;password=$ecurity_is_Job_1;databaseName=WFSDB_R360";

}
