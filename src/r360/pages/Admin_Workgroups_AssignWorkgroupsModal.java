package r360.pages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.Pagination;

public class Admin_Workgroups_AssignWorkgroupsModal {

	public static final String available_GridID = "availableGrid";
	public static final String selected_GridID = "selectedGrid";
	public static final String availablePager_GridID = "availablePager";
	
	public static Pagination availableGrid = new Pagination(available_GridID);
	public static Pagination selectedGrid = new Pagination(selected_GridID);
	public static Pagination availablePagerGrid = new Pagination(availablePager_GridID);
	
	public static void assign_UnassignedWorkgroup(String unassignedWorkgroup) throws Exception
	{
		selectRow_fromAvailableGrid(unassignedWorkgroup);
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AddSelectedButton"));
		Thread.sleep(100);
	}

	public static void unassign_AssignedWorkgroup(String assignedWorkgroup) throws Exception {
		BaseUI.click(Locator.lookupElement("workgroups_Unassign_UnassignWorkgroupButton", assignedWorkgroup, null));
		BaseUI.waitForElementToBeDisplayed("workgroups_Unassign_ConfigButton", null, null);
		BaseUI.click(Locator.lookupElement("workgroups_Unassign_ConfigButton"));
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}


	public static void selectRow_fromAvailableGrid(String workgroup) throws Exception
	{
	//	availableGrid.show_All();
		showAll_AvailableGrid();
		WebElement rowToSelect = Locator.lookupElement("wrkgrp_Assign_AvailableWorkgroup_ByLongName", workgroup, null);
//		WebElement rowToSelect = availableGrid.get_row_ByColumnName_And_ColumnMatchingText("Long Name", workgroup);
		if(rowToSelect != null) {
			BaseUI.scroll_to_element(rowToSelect);
			BaseUI.click(rowToSelect);
			Thread.sleep(100);
		}
	}
	
	public static void showAll_AvailableGrid() throws Exception
	{
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AvailableGrid_BulbIcon"));
		BaseUI.waitForElementToBeDisplayed("wrkgrp_Assign_AvailableGrid_ShowOption_ByLinkText", "All", null, 5);
		
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AvailableGrid_ShowOption_ByLinkText", "All", null));
		Thread.sleep(500);
		
	}
	
	
	public static void move_fromAvailable_toSelected(String workgroup) throws Exception
	{
		selectRow_fromAvailableGrid(workgroup);
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_AddSelectedButton"));
		Thread.sleep(300);
	}
	
	public static void save_changes() throws Exception
	{
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_SaveButton"));
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void cancel_changes() throws Exception
	{
		BaseUI.click(Locator.lookupElement("wrkgrp_Assign_CancelButton"));
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
}
