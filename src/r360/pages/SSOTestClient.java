package r360.pages;

import utils.BaseUI;
import utils.Locator;

public class SSOTestClient {
    public static void enterEntity(String entity) {
        BaseUI.enterText(Locator.lookupElement("SSOClient_txt_EntityName"), entity);
    }

    public static void enterUser(String user) {
        BaseUI.enterText(Locator.lookupElement("SSOClient_txt_UserName"), user);
    }

    public void clickNext() {
        BaseUI.click(Locator.lookupElement("SSOClient_btn_Next"));
        BaseUI.waitForElementToBeDisplayed_ButDontFail("SSOClient_lgnd_SecondPage", null, null, 30);
    }

    public void clickLogin() {
        BaseUI.click(Locator.lookupElement("SSOClient_btn_Next"));
        BaseUI.waitForElementToBeDisplayed_ButDontFail("dshbrd_Element_To_Wait_For_ForPageLoad", null, null, 30);
    }
}
