package r360.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class ManageQueries {

	public static void delete_Query(String nameOfQueryToDelete) throws Exception {
		BaseUI.click(Locator.lookupElement("mngQueries_DeleteButton_ByQueryName", nameOfQueryToDelete, null));
		Thread.sleep(3000);

		BaseUI.click(Locator.lookupElement("mngQueries_DeleteConfirmModal_DeleteButton"));
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void check_DefaultQuery(String queryName) throws Exception {
		WebElement defaultQueryCheckbox = Locator.lookupElement("mngQueries_SetDefault_Checkbox_ByQueryName", queryName,
				null);
		String defaultQueryCheckbox_Title = defaultQueryCheckbox.getAttribute("title");

		Boolean checkboxChecked = defaultQueryCheckbox_Title.equals("Unset default query");

		if (!checkboxChecked) {
			BaseUI.click(defaultQueryCheckbox);
			Thread.sleep(1500);
			Navigation.wait_for_LoadingSpinner_ToDisappear();

		}
	}

	public static void uncheck_DefaultQuery(String queryName) throws Exception {
		WebElement defaultQueryCheckbox = Locator.lookupElement("mngQueries_SetDefault_Checkbox_ByQueryName", queryName,
				null);
		String defaultQueryCheckbox_Title = defaultQueryCheckbox.getAttribute("title");

		Boolean checkboxChecked = defaultQueryCheckbox_Title.equals("Unset default query");

		if (checkboxChecked) {
			BaseUI.click(defaultQueryCheckbox);
			Thread.sleep(1500);
			Navigation.wait_for_LoadingSpinner_ToDisappear();

		}
	}
	
	
}
