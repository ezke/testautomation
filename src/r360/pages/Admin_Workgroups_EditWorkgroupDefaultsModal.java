package r360.pages;

import utils.BaseUI;
import utils.Locator;

public class Admin_Workgroups_EditWorkgroupDefaultsModal {

	public static void set_Required_Workgroup_Defaults(String viewingDays, String maxSearchDays)
			throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_Defaults_ViewingDaysText"), viewingDays);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_Defaults_MaxSearchDays"), maxSearchDays);
		Thread.sleep(100);
		save_EditWorkGroupsDefaults_Modal();
	}

	public static void save_EditWorkGroupsDefaults_Modal() throws Exception {
		BaseUI.click(Locator.lookupElement("wrkgrp_Defaults_SaveButton"));
		Thread.sleep(5000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
}
