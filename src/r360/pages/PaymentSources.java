package r360.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.Pagination;

public class PaymentSources {

	public static Pagination paymentSourceGrid = new Pagination("psGrid");
	public static Pagination paymentSourcePagerGrid = new Pagination("psPager");
	public static Dropdown system_Dropdown = new Dropdown("s2id_selectSystemType");
	public static Dropdown entityFI_Dropdown = new Dropdown("s2id_selectFI");

	public static void launch_AddPaymentSource_Modal() throws Exception {
		BaseUI.click(Locator.lookupElement("pymtSource_AddButton"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Pass True to check Active Box, False to Uncheck Active Box
	// Pass True to save, False to cancel.
	public static void add_PaymentSource(String shortName, String description, Boolean isActive,
			String system, String entityFI, Boolean saveOrCancel) throws Exception {
		BaseUI.enterText(Locator.lookupElement("pymtSource_ShortName_TextBox"), shortName);
		BaseUI.enterText(Locator.lookupElement("pymtSource_Description_TextBox"), description);
		if (isActive) {
			BaseUI.checkCheckbox("pymtSource_Active_CheckBox");
		} else {
			BaseUI.uncheckCheckbox("pymtSource_Active_CheckBox");
		}

		system_Dropdown.select_EntityValue(system);
		entityFI_Dropdown.select_EntityValue(entityFI);

		if (saveOrCancel) {
			BaseUI.click(Locator.lookupElement("pymtSource_SaveButton"));
		} else {
			BaseUI.click(Locator.lookupElement("pymtSource_CancelButton"));
		}

		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();

	}
	//Saves Payment Source, No cancel
	public static void add_PaymentSource(String shortName, String description, Boolean isActive,
			String system, String entityFI) throws Exception {
		BaseUI.enterText(Locator.lookupElement("pymtSource_ShortName_TextBox"), shortName);
		BaseUI.enterText(Locator.lookupElement("pymtSource_Description_TextBox"), description);
		if (isActive) {
			BaseUI.checkCheckbox("pymtSource_Active_CheckBox");
		} else {
			BaseUI.uncheckCheckbox("pymtSource_Active_CheckBox");
		}

		system_Dropdown.select_EntityValue(system);
		entityFI_Dropdown.select_EntityValue(entityFI);

		save_PaymentSourceModal();

	}
	
	public static void save_PaymentSourceModal() throws Exception{
		BaseUI.click(Locator.lookupElement("pymtSource_SaveButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void cancel_PaymentSourceModal() throws Exception{
		BaseUI.click(Locator.lookupElement("pymtSource_CancelButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	

	public static void launch_EditPaymentSource_ForGivenShortName(String shortName) throws Exception {
		BaseUI.click(PaymentSources.paymentSourceGrid.editButton_ByColumnName_andColumnText("Short Name",
				shortName));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void edit_PaymentSource(String description, Boolean isActive, String system, String entityFI,
			Boolean saveOrCancel) throws Throwable {
		BaseUI.enterText(Locator.lookupElement("pymtSource_Description_TextBox"), description);
		if (isActive) {
			BaseUI.checkCheckbox("pymtSource_Active_CheckBox");
		} else {
			BaseUI.uncheckCheckbox("pymtSource_Active_CheckBox");
		}

		system_Dropdown.select_EntityValue(system);
		entityFI_Dropdown.select_EntityValue(entityFI);

		if (saveOrCancel) {
			BaseUI.click(Locator.lookupElement("pymtSource_SaveButton"));
		} else {
			BaseUI.click(Locator.lookupElement("pymtSource_CancelButton"));
		}

		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void launch_DeletePaymentSource_ForGivenShortName(String shortName) throws Exception{
		BaseUI.click(PaymentSources.paymentSourceGrid.delete_ByColumnName_andColumnText("Short Name", shortName));
		Thread.sleep(2000);
	}
	
	public static void delete_PaymentSource_ForGivenFieldName(String shortName) throws Exception{
		launch_DeletePaymentSource_ForGivenShortName(shortName);
		BaseUI.click(Locator.lookupElement("pymtSource_DeleteButton"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		
	}
	
	public static void cancel_Delete_PaymentSource_Modal() throws Exception{
		BaseUI.click(Locator.lookupElement("pymtSource_Delete_CancelButton"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void verify_DeleteIconLinkList_Appears()throws Throwable {
		ArrayList<WebElement> delete_IconList = new ArrayList<WebElement>();
		delete_IconList = Locator.lookup_multipleElements("pymtSource_DeleteLink_List", null, null);
		
		BaseUI.verify_true_AndLog(delete_IconList.size() > 0, "Delete Icons were present on page.", "Delete Icons were NOT present on page.");
		
		for(WebElement deleteIcon : delete_IconList)
		{
			BaseUI.verifyElementAppears(deleteIcon);
		}
	}
	
	public static void verify_EditIconList_Appears()throws Throwable{
		ArrayList<WebElement> edit_IconList = new ArrayList<WebElement>();
		edit_IconList = Locator.lookup_multipleElements("pymtSource_EditLink_List", null, null);
		
		BaseUI.verify_true_AndLog(edit_IconList.size() > 0, "Edit Icons were present on page.", "Edit Icons were NOT present on page.");
		
		for(WebElement editIcon : edit_IconList)
		{
			BaseUI.verifyElementAppears(editIcon);
		}
	}
	
}
