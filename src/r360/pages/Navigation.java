package r360.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.*;

import java.time.Duration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Navigation {

    public static void navigate_Search_AdvancedSearch() throws Exception {
        navigate_to_page("Search", "Advanced Search");
        // Thread.sleep(4000);
        // wait_for_LoadingSpinner_ToAppear(1);
        waitFor_AdvancedSearchPage();
    }

    public static void navigate_Exceptions_PostDepositExceptions() throws Exception {
        navigate_to_page("Exceptions", "Post-Deposit Exceptions");
        wait_for_LoadingSpinner_ToAppearAndDisappear(15);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void navigate_Exceptions_PreDepositExceptions() throws Exception {
        navigate_to_page("Exceptions", "Pre-Deposit Exceptions");
        Thread.sleep(4000);
        wait_for_LoadingSpinner_ToAppear(1);
    }

    public static void navigate_Exceptions_RPS_Web_Exceptions() throws Exception {
        navigate_to_page_in_another_window("Exceptions", "RPS Web Exceptions", () -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread.sleep(4000);
        wait_for_LoadingSpinner_ToAppear(1);
    }

    public static void waitFor_AdvancedSearchPage() throws Exception {
        BaseUI.waitForElementToBeDisplayed("advancedSearch_Title", null, null, 45);
        // wait_for_LoadingSpinner_ToDisappear();
        BaseUI.waitForElementToBeDisplayed("advancedSearch_workgroupSelector", null, null);
        Thread.sleep(1000);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void navigate_Search_ManageQueries() throws Exception {
        navigate_to_page("Search", "Manage Queries");
        Thread.sleep(1000);
        wait_for_LoadingSpinner_ToAppear(1);
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Search_PaymentSearch() throws Exception {
        navigate_to_page("Search", "Payment Search");
        // Thread.sleep(2000);
        wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_BankMaintenance() throws Exception {
        navigate_to_page("Admin", "Banks");
        // Thread.sleep(5000);
        BaseUI.waitForElementToBeDisplayed("BankMain_BankMaintenance_Title", null, null);
        // wait_for_LoadingSpinner_ToAppear(2);
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_Users() throws Exception {
        navigate_to_page("Admin", "Users");
        BaseUI.waitForElementToBeDisplayed("securityUsers_LoginName_HeaderText", null, null, 20);
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_Extracts_Configuration_Manager() throws Exception {
        navigate_to_page("Admin", "Extract Config Manager");
        BaseUI.waitForElementToBeDisplayed("admin_ExtractConfigManager_AddExtractDefinitionHeader", null, null, 20);
        Thread.sleep(500);
    }

    public static void navigate_Admin_Alerts_Manager() throws Exception {
        navigate_to_page("Admin", "Alert Manager");
        //BaseUI.waitForElementToBeDisplayed("securityUsers_LoginName_HeaderText", null, null, 20);
        Thread.sleep(500);
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_UserGroups() throws Exception {
        navigate_to_page("Admin", "User Groups");
        BaseUI.waitForElementToBeDisplayed("entityTreeNode_SearchBar", null, null);
        wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_UserPreferences() throws Exception {
        navigate_to_page("Admin", "User Preferences");
        //BaseUI.waitForElementToBeDisplayed("entityTreeNode_SearchBar", null, null);
        wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_Branding() throws Exception {
        navigate_to_page("Admin", "Branding");
        // Thread.sleep(3000);
        wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_PaymentSource() throws Exception {
        navigate_to_page("Admin", "Payment Sources");
        wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_Entities() throws Exception {
        navigate_to_page("Admin", "Entities");
        // Thread.sleep(5000);
        // wait_for_LoadingSpinner_ToAppear(5);
        // wait_for_LoadingSpinner_ToDisappear();
        // Thread.sleep(3000);
        // BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("securityEntities_AddButton", null, null);
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_Admin_Roles() throws Exception {
        navigate_to_page("Admin", "Roles");
        // Thread.sleep(4500);
        wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
        Thread.sleep(3000);
    }

    public static void navigate_Admin_Workgroups() throws Exception {
        navigate_to_page("Admin", "Workgroups");
        BaseUI.waitForElementToBeDisplayed("entityTreeNode_ByText", "AutomationTest", null, 30);

        // wait_for_LoadingSpinner_ToAppear();
        wait_for_LoadingSpinner_ToDisappear();
    }

    public static void navigate_DDASummary() throws Exception {
        BaseUI.click_js(Locator.lookupElement("Navigate_toPage_ByNavText", "DDA Summary", null));
        // Thread.sleep(8000);
        // wait_for_LoadingSpinner_ToAppear(3);
        // Thread.sleep(4000);
        BaseUI.waitForElementToBeDisplayed("ddaSum_ChartContainer", null, null);
        wait_for_LoadingSpinner_ToDisappear();
        // BaseUI.wait_forPageToFinishLoading();

        // if (Browser.currentBrowser.equals("firefox")) {
        // if
        // (BaseUI.elementAppears(Locator.lookupElement("ddaSum_TotalAmounts_NotAvailableMessage")))
        // {
        // Navigation.wait_for_LoadingSpinner_ToAppear();
        // Navigation.wait_for_LoadingSpinner_ToDisappear();
        // }
        // }
    }

    public static void navigate_Reports() throws Exception {
        BaseUI.click(BaseUI.waitForElementToBeClickable("Navigate_toPage_ByNavText", "Reports", null, 40));
        BaseUI.waitForElementToBeDisplayed("reports_GroupingHeader_ByText", Reports.receivables360_Online_ReportsText, null, 40);
        Thread.sleep(500);
    }

    public static void navigate_Dashboard() throws Exception {
        BaseUI.click(BaseUI.waitForElementToBeClickable("Navigate_toPage_ByNavText", "Dashboard", null));
        BaseUI.waitForElementToBeDisplayed("dshbrd_Element_To_Wait_For_ForPageLoad", null, null, 45);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
        // Thread.sleep(15000);
        //
        // if (!BaseUI.pageSourceContainsString("paymentTotals")) {
        // Thread.sleep(5000);
        // }
    }

    public static void navigate_BatchSummary() throws Exception {
        BaseUI.click(BaseUI.waitForElementToBeClickable("Navigate_toPage_ByNavText", "Batch Summary", null));
        wait_for_LoadingSpinner_ToDisappear();
        if (!BaseUI.pageSourceContainsString("paymentTotals")) {
            Thread.sleep(500);
        }
    }

    public static void navigate_Notifications() throws Exception {
        Navigation.navigate_openMenu("Notifications");
        //Thread.sleep(4000);
        BaseUI.waitForElementToBeDisplayed("notificationsDetails_pageIdentifier", null, null, 10);
        wait_for_LoadingSpinner_ToDisappear();
        //BaseUI.waitForElementToBeDisplayed("Notifications_AttachmentName_Input", null, null, 10);
        Thread.sleep(500);
    }

    public static void signOut() throws Exception {
        BaseUI.waitForElementToBeDisplayed("Navigate_SignOut", null, null, 10);
        //BaseUI.click(Locator.lookupElement("Navigate_SignOut"));
        BaseUI.click_js(Locator.lookupElement("Navigate_SignOut"));
        Thread.sleep(1000);
    }

    public static void navigate_openMenu(String menuToOpen) throws Exception {
        BaseUI.click(Locator.lookupElement("Navigate_menuByText", menuToOpen, null));
        Thread.sleep(200);
    }

    // Navigates to pages that use Menu and Submenu.
    public static void navigate_to_page(String menu, String subMenu) throws Exception {

        if (Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.click_js(GetToSubMenuElement(menu, subMenu));
        } else {
            BaseUI.click(GetToSubMenuElement(menu, subMenu));
        }
    }

    // Navigates to pages that use Menu and Submenu and appears in another window or tab.
    public static void navigate_to_page_in_another_window(String menu, String subMenu, Runnable delay) throws Exception {
        BaseUI.ClickAndSwitchWindow(GetToSubMenuElement(menu, subMenu), Browser.currentBrowser.equals("internetexplorer"), delay);
    }

    private static WebElement GetToSubMenuElement(String menu, String subMenu) throws Exception {
        if (Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.click_js(Locator.lookupElement("Navigate_menuByText", menu, null));
            // Thread.sleep(1300);
            BaseUI.waitForElementToBeDisplayed("Navigate_menuSubItem", menu, subMenu);
            return Locator.lookupElement("Navigate_menuSubItem", menu, subMenu);
        } else {
            BaseUI.click_js(BaseUI.waitForElementToBeClickable("Navigate_menuByText", menu, null));
            // Thread.sleep(1000);
            BaseUI.waitForElementToBeDisplayed("Navigate_menuSubItem", menu, subMenu);
            return BaseUI.waitForElementToBeClickable("Navigate_menuSubItem", menu, subMenu);
        }
    }

    public static void wait_for_LoadingSpinner_ToAppear() throws Exception {
        BaseUI.wait_for_PageSource_ToContainText("frameworkSpinnerText", 5);
    }

    public static void wait_for_LoadingSpinner_ToAppear(Integer timeToWait) throws Exception {
        BaseUI.wait_for_PageSource_ToContainText("frameworkSpinnerText", timeToWait);
    }

    public static void wait_for_LoadingSpinner_ToAppearAndDisappear(Integer numberOfSecondsToWait) throws Exception {
        wait_for_LoadingSpinner_ToAppearAndDisappear(Duration.ofSeconds(numberOfSecondsToWait));
    }

    public static void wait_for_LoadingSpinner_ToAppearAndDisappear(Duration timeToWait) throws Exception {
        if (BaseUI.wait_for_PageSource_ToContainText("frameworkSpinnerText", timeToWait)) {
            wait_for_LoadingSpinner_ToDisappear();
        }
    }

    public static void wait_for_only_LoadingSpinner_ToDisappear() throws Exception {
        for(int retryCount=0;retryCount<5;retryCount++) {
        BaseUI.wait_for_PageSource_ToNoLongerContainText("frameworkSpinnerText", 75);
            Thread.sleep(500);
            if(!BaseUI.pageSourceContainsString("frameworkSpinnerText")) {
                break; //If I can't find the spinner exiting the loop
            }
            else { // otherwise lets wait again
                BaseUI.log_Status("Spinner flickered");
            }
        }
    }

    public static void wait_for_LoadingSpinner_ToDisappear() throws Exception {
        wait_for_only_LoadingSpinner_ToDisappear();
        Thread.sleep(2500);
        if (BaseUI.pageSourceContainsString("modal fade bootstrap-dialog")) {
            Thread.sleep(4000);
        }
    }

    public static void navigate_Past_CertificateError() throws Exception {
        if (BaseUI.pageSourceContainsString("Continue to this website (not recommended).")) {
            Browser.driver.get("javascript:document.getElementById('overridelink').click();");
            // BaseUI.click(Locator.lookupElement("ie_ContinueToWebsite"));
            Thread.sleep(3500);
        }
    }

    public static void close_ToastError_If_ItAppears() throws Exception {
        if (BaseUI.pageSourceContainsString("toast-item toast-type-error")
                && Locator.lookupElement("error_MessagePopup").isDisplayed()) {

            BaseUI.click(Locator.lookupElement("error_CloseButton"));
            Thread.sleep(500);
        }
    }

    public static void validate_ToastError_Message_If_ItAppears(String message) {
        WebElement webElementMessage;
        webElementMessage = Locator.lookupElement("error_MessagePopup");
        BaseUI.verifyElementAppears(webElementMessage);
        String displayMessage = BaseUI.getTextFromField(webElementMessage);
        assertThat(message, is(displayMessage));
    }

    public static void clickOK_ForErrorModal() throws Exception {
        BaseUI.click(Locator.lookupElement("navigation_InternalServiceError_OKButton"));
        BaseUI.waitForElementToNOTBeDisplayed("navigation_InternalServiceError_OKButton", null, null, 5);
    }



    public static String  SetandValidateUserFullName(String userlogin) {
        String query = "SELECT [Users].LoginID as ULogin,[Persons].FirstName as FN,[Persons].LastName as LN"  + "\n"
               + " FROM [Users]" + "\n"+ "INNER JOIN  [Persons]" + "\n" + "ON  [Users].PersonID=[Persons].PersonID" + "\n"
               + "Where [Users].LoginID=\'" + userlogin + "\'";
        String FullName =null;
        try {
            DatabaseConnection.dbUrl = GlobalVariables.environment.RAAMDBConnection;
            TableData tableData = DatabaseConnection.runSQLServerQuery(query);
            String FirstName = tableData.data.get(0).get("FN");
            String LastName = tableData.data.get(0).get("LN");
            FullName = FirstName + " " + LastName;
            BaseUI.verifyElementHasExpectedText("MyProfileUserName", FullName);

            //} catch (Exception e) {
        }finally{
            BaseUI.verify_true_AndLog(FullName!=null,
                    "Set and validated the Current Users Full Name to generate UAR.",
                    "Unable to Set and validate the Current Users Full Name to generate UAR.");
            return FullName;
        }
    }

    public static void return_Application()
    {
        BaseUI.click(Locator.lookupElement("Navigate_ReturnApplication"));
    }
}// end of class
