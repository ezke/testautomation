package r360.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.BaseUI;
import utils.Browser;
import utils.Is;
import utils.Locator;
import wfsCommon.pages.Pagination;

import java.util.Arrays;
import java.util.List;

public class PostDepositExceptionsTransactionDetailPage {
    @FindBy(xpath = "//span[@class='breadcrumbs']/a[./text()='Post-Deposit Exceptions Summary']")
    private static WebElement exceptionsSummaryBreadCrumb;
    @FindBy(xpath = "//span[@class='header-title']")
    private WebElement pageTitle;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div[1]/div[2]/div[6]/label[1]")
    private WebElement batchIDLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div[1]/div[2]/div[6]/span[1]")
    private WebElement batchIDLabelValue;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[5]/label")
    private WebElement paymentTypeLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[5]/span")
    private WebElement paymentTypeLabelValue;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[4]/label")
    private WebElement paymentSourceLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[4]/span")
    private WebElement paymentSourceLabelValue;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[3]/label")
    private WebElement workgroupLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[3]/span")
    private WebElement workgroupLabelValue;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[2]/label")
    private WebElement entityLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[2]/span")
    private WebElement entityLabelValue;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[1]/label")
    private WebElement depositDateLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div/div[2]/div[1]/span")
    private WebElement depositDateLabelValue;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div[1]/div[2]/div[7]/label[1]")
    private WebElement lockedByLabel;
    @FindBy(xpath = "//*[@id=\"colinfo\"]/div[1]/div[2]/div[7]/span[1]")
    private WebElement lockedByLabelOwner;
    @FindBy(css = "#button-save > i")
    private static WebElement savePostDeposit;
    @FindBy(xpath = "//*[@id=\"stubs-table_wrapper\"]//button[contains(text(), 'Add related item')]")
    private static WebElement addRelatedItem;
    @FindBy(xpath = "//*[@id=\"stubs-table\"]/tbody/tr[5]/td[3]/input")
    private static WebElement addRelatedItemBox1;
    @FindBy(xpath = "//*[@id=\"stubs-table\"]/tbody/tr[5]/td[4]/input")
    private static WebElement addRelatedItemBox2;
    @FindBy(xpath = "//*[@id=\"stubs-table\"]/tbody/tr[5]/td[5]/input")
    private static WebElement addRelatedItemBox3;
    @FindBy(xpath = "//*[@id=\"stubs-table\"]/tbody/tr[5]/td[6]/input")
    private static WebElement addRelatedItemBox4;
    @FindBy(xpath = "//*[@id=\"stubs-table\"]/tbody/tr[5]/td[7]/input")
    private static WebElement addRelatedItemBox5;
    @FindBy(xpath = "//*[@id=\"stubs-table\"]/tbody/tr[5]/td[8]/input")
    private static WebElement addRelatedItemBox6;
    @FindBy(xpath = "//div[@id='stubs-table-container']//div[contains(@class, 'dataTables_scrollBody')]//tbody//td[count(//div[@id='stubs-table-container']//div[contains(@class, 'dataTables_scrollHeadInner')]//thead/tr/th[contains(., 'KFIType')]/preceding-sibling::th)+1]/input")
    private WebElement postDepositKFIType;
    @FindBy(xpath = "//div[@id='stubs-table-container']//div[contains(@class, 'dataTables_scrollBody')]//tbody//td[count(//div[@id='stubs-table-container']//div[contains(@class, 'dataTables_scrollHeadInner')]//thead/tr/th[contains(., '')]/preceding-sibling::th)+1]/button")
    private List<WebElement> deleteTransactionButtons;
    @FindBy(xpath = "//*[@id=\"frameworkModalContent\"]/div[2]/div/a[1]")
    private WebElement confirmDeleteTrans;
    @FindBy(xpath = "//*[@id=\"frameworkModalContent\"]/div[2]/div/a[2]")
    private WebElement confirmCancelTrans;
    @FindBy(xpath = "//div[@id='stubs-table-container']//div[contains(@class, 'dataTables_scrollBody')]//tbody//td[count(//div[@id='stubs-table-container']//div[contains(@class, 'dataTables_scrollHeadInner')]//thead/tr/th[contains(., 'AccountNumber')]/preceding-sibling::th)+1]/input")
    private static WebElement postDepositAccount;
    @FindBy(css = "#stubs-table_filter > label > input")
    private WebElement searchStub;
    @FindBy(id = "button-accept")
    private WebElement acceptButton;
    @FindBy(xpath="//div[@class='modal-body']//h4[text()='Confirm Accept']/..//a[text()='Accept']")
    private WebElement confirmAccept;

    private Pagination stubGrid = new Pagination("stubs-table_wrapper");

    public PostDepositExceptionsTransactionDetailPage() throws Exception {
        Navigation.wait_for_LoadingSpinner_ToAppearAndDisappear(10);
        PageFactory.initElements(Browser.driver, this);
        BaseUI.wait_ForCondition_ToBeMet(()->pageTitle!=null && pageTitle.isDisplayed(), 10);
    }

    public PostDepositExceptionsPage returnToPostDepositExceptionsSummary() throws InterruptedException {
        exceptionsSummaryBreadCrumb.click();
        return new PostDepositExceptionsPage();
	}

    public PostDepositExceptionsTransactionDetailPage savePostDeposit() throws Exception {
        savePostDeposit.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public void addRelatedItem() {
        addRelatedItem.click();
    }

    public void scrollBar(int length) {
        JavascriptExecutor js = (JavascriptExecutor) Browser.driver;
        js.executeScript("window.scrollBy(0," + length + ")");
    }

    public void validateDepositKFITypeInError() {
        validateElementInError(postDepositKFIType);
    }

    public void validateDepositKFITypeIsGood() {
        validateElementInGood(postDepositKFIType);
    }

    public String getLockedByLabel() {
        return lockedByLabel.getText();
    }

    public String getLockedLabelOwner() {
        return lockedByLabelOwner.getText();
    }

    public String getBatchLabel() {
        return batchIDLabel.getText();
    }

    public String getBatchLabelOwner() {
        return batchIDLabelValue.getText();
    }

    public String getPaymentTypeLabel() {
        return paymentTypeLabel.getText();
    }

    public String getPaymentTypeLabelValue() {
        return paymentTypeLabelValue.getText();
    }

    public String getPaymentSourceLabel() {
        return paymentSourceLabel.getText();
    }

    public String getPaymentSourceLabelValue() {
        return paymentSourceLabelValue.getText();
    }

    public String getWorkgroupLabel() {
        return workgroupLabel.getText();
    }

    public String getWorkgroupLabelValue() {
        return workgroupLabelValue.getText();
    }

    public String getEntityLabel() {
        return entityLabel.getText();
    }

    public String getEntityLabelValue() {
        return entityLabelValue.getText();
    }

    public String getDepositDateLabel() {
        return depositDateLabel.getText();
    }

    public String getDepositDateLabelValue() {
        return depositDateLabelValue.getText();
    }

    public PostDepositExceptionsTransactionDetailPage selectaddRelatedItemBox1(String value) throws Exception {
        addRelatedItemBox1.clear();
        addRelatedItemBox1.sendKeys(value);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectaddRelatedItemBox2(String value) throws Exception {
        addRelatedItemBox2.clear();
        addRelatedItemBox2.sendKeys(value);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectaddRelatedItemBox3(String value) throws Exception {
        addRelatedItemBox3.clear();
        addRelatedItemBox3.sendKeys(value);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectaddRelatedItemBox4(String value) throws Exception {
        addRelatedItemBox4.clear();
        addRelatedItemBox4.sendKeys(value);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectaddRelatedItemBox5(String value) throws Exception {
        addRelatedItemBox5.clear();
        addRelatedItemBox5.sendKeys(value);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectaddRelatedItemBox6(String value) throws Exception {
        addRelatedItemBox6.clear();
        addRelatedItemBox6.sendKeys(value);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage clickAccept() throws Exception {
        acceptButton.click();
        Navigation.wait_for_only_LoadingSpinner_ToDisappear();
        confirmAccept.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public String getaddRelatedItemBox1() throws InterruptedException {
        return addRelatedItemBox1.getAttribute("value");
    }

    public String getaddRelatedItemBox2() throws InterruptedException {
        return addRelatedItemBox2.getAttribute("value");
    }

    public String getaddRelatedItemBox3() throws InterruptedException {
        return addRelatedItemBox3.getAttribute("value");
    }

    public String getaddRelatedItemBox4() throws InterruptedException {
        return addRelatedItemBox4.getAttribute("value");
    }

    public String getaddRelatedItemBox5() throws InterruptedException {
        return addRelatedItemBox5.getAttribute("value");
    }

    public String getaddRelatedItemBox6() throws InterruptedException {
        return addRelatedItemBox6.getAttribute("value");
    }

    public boolean ConfirmPageTitle() {
        try {
            return pageTitle.isDisplayed();
        }
        catch(NoSuchElementException e) {
            return false;
        }
    }

    public String getPageTitle() {
        return pageTitle.getText();
    }

    public int getInvoiceCount() {
        return deleteTransactionButtons.size();
    }

    public PostDepositExceptionsTransactionDetailPage deleteInvoice() throws Exception {
        WebElement lastDeleteButton = deleteTransactionButtons.get(deleteTransactionButtons.size()-1);
        BaseUI.click(lastDeleteButton);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage confirmDeleteTransaction() throws Exception {
        confirmDeleteTrans.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage confirmCancelTransaction() throws Exception {
        confirmCancelTrans.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public void validateDepositInError() {
        validateElementInError(postDepositAccount);
    }

    public void validateDepositIsGood() {
        validateElementInGood(postDepositAccount);
    }

    private void validateElementInError(WebElement element) {
        BaseUI.assertThat(element.getAttribute("class"), Is.contains("has-exception"));
    }

    private void validateElementInGood(WebElement element) {
        BaseUI.assertThat(element.getAttribute("class"), Is.not(Is.contains("has-exception")));
    }

    public PostDepositExceptionsTransactionDetailPage searchStub(String search) throws Exception {
        Thread.sleep(1000);
        Actions actions = new Actions(Browser.driver);
        actions.moveToElement(searchStub).click().sendKeys(search).build().perform();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public void typeTextDepositKFIType(String text) {
        BaseUI.enterText(postDepositKFIType, text);
    }

    public String getPaymentTypeLabelValuer() throws InterruptedException {
        return paymentTypeLabelValue.getText();
    }

    public void typeTextPostDepositAccount(String text) {
        BaseUI.enterText(postDepositAccount, text);
    }

    public void waitForAddRelatedItemButton() {
        BaseUI.wait_ForCondition_ToBeMet(()->addRelatedItem.isEnabled(), 5);
    }

    public void populateStub(String column, int row, String value) throws Exception {
        BaseUI.enterText(getCell(column, row), value);
    }

    public String queryStub(String column, int row) {
        return BaseUI.getTextFromInputBox(getCell(column, row));
    }

    public String queryStubBackcolor(String column, int row) {
        return BaseUI.get_CSSAttribute_FromField(getCell(column, row), "background-color");
    }

    private WebElement getCell(String column, int row) {
        return Locator.lookupElement("stubTableCells", Integer.toString(row), column);
    }

    public PostDepositExceptionsTransactionDetailPage clickNextTransaction() throws Exception {
        BaseUI.click(Locator.lookupElement("postDepositExceptionsDetailNextTransactionButton"));
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage clickPreviousTransaction() throws Exception {
        BaseUI.click(Locator.lookupElement("postDepositExceptionsDetailPreviousTransactionButton"));
        return new PostDepositExceptionsTransactionDetailPage();
    }
}







