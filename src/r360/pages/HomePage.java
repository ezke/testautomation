package r360.pages;

import utils.BaseUI;
import utils.Locator;

public class HomePage {

	public static void verifyHeaderLogoAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("headerLogo"));
	}

	public static void verifyHeaderTitleAppears() {
		BaseUI.waitForElementToBeDisplayed("headterTitle", null, null);
		BaseUI.verifyElementAppears(Locator.lookupElement("headterTitle"));
	}

	public static void clickSignOutLink() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("signOutLink"));
		Thread.sleep(1000);
	}

	public static void clickReturnToApplicationLink() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("returnToApplicationLink"));
		Thread.sleep(3000);
	}

}
