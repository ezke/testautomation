package r360.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class LoginPage {

	public static void login(String entityName, String login, String password) throws Exception {
		login(entityName, login, password, 45);

	}

	public static void login(String entityName, String login, String password, Integer implicitWaitTimer)
			throws Exception {
		
		login(entityName, login, password, implicitWaitTimer, true);
	}

	public static void login(String entityName, String login, String password, Integer implicitWaitTimer,
			Boolean checkToSeeIfLoginWasSuccessful) throws Exception {
		// This will allow pages to finish loading before doing things.
		Browser.set_ImplicitWait_AndSaveValue(implicitWaitTimer);

		Thread.sleep(200);
		Navigation.navigate_Past_CertificateError();

		WebElement entityTextBox = Locator.lookupElement("LoginPage_txt_EntityName");
		BaseUI.click(entityTextBox);
		Thread.sleep(200);
		BaseUI.enterText(entityTextBox, entityName);

		WebElement userNameTextBox = Locator.lookupElement("LoginPage_txt_UserName");
		BaseUI.click(userNameTextBox);
		Thread.sleep(200);
		BaseUI.enterText(userNameTextBox, login);
		if (BaseUI.elementAppears(Locator.lookupElement("LoginPage_txt_Password_masked"))) {
			BaseUI.tabThroughField(userNameTextBox);
			WebElement textField = Browser.driver.switchTo().activeElement();
			BaseUI.enterText(textField, password);
		} else {
			WebElement passwordTextBox = Locator.lookupElement("LoginPage_txt_Password");
			BaseUI.click(passwordTextBox);
			Thread.sleep(200);
			BaseUI.enterText(passwordTextBox, password);
		}

		BaseUI.click(Locator.lookupElement("LoginPage_btn_SignIn"));
		if (checkToSeeIfLoginWasSuccessful) {
			BaseUI.waitForElementToBeDisplayed_ButDontFail("dshbrd_Element_To_Wait_For_ForPageLoad", null, null, 30);
		} else {
			Thread.sleep(500);
		}

	}

	public static void clearLoginFields() throws Exception {
		WebElement entityNametextField = Locator.lookupElement("LoginPage_txt_EntityName");
		entityNametextField.sendKeys(Keys.CONTROL + "a");
		entityNametextField.sendKeys(Keys.DELETE);
		BaseUI.tabThroughField(Locator.lookupElement("LoginPage_txt_EntityName"));
		WebElement userNameTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(userNameTextField, "");
		BaseUI.tabThroughField(Locator.lookupElement("LoginPage_txt_UserName"));
		WebElement passwordTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(passwordTextField, "");

	}

	public static void clearPassword() throws Exception {
		BaseUI.tabThroughField(Locator.lookupElement("LoginPage_txt_UserName"));
		WebElement textField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(textField, "");

	}

	public static void verifyLoginPageError() {
		BaseUI.verifyElementAppears(Locator.lookupElement("LoginPageErrorvalidation"));
	}

	public static void verifyLoginPageErrorDoesNotAppear() {
		BaseUI.verifyElementDisabled(Locator.lookupElement("LoginPageErrorvalidation"));
	}

	public static void changePassword(String oldPassword, String newPassword) throws Exception {
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), newPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), newPassword);
		BaseUI.tabThroughField(Locator.lookupElement("loginPageConfirmPasswordText"));
		Thread.sleep(3000);

	}

	public static void clickChangePasswordSaveButton() {
		BaseUI.click(Locator.lookupElement("loginPageChangePasswordSaveButton"));
	}

	public static void clickChangePasswordCancelButton() {
		BaseUI.click(Locator.lookupElement("loginPageChangePasswordCancelButton"));
	}

	public static void verifyLoginPageChangePasswordConfirmMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageChangePasswordConfirmMessage"));
	}

	public static void verifyLoginPageChangePasswordErrorMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageChangePasswordErrorMessage"));
	}

	public static void clickForgotPasswordLink() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click(Locator.lookupElement("loginPageForgotPasswordLink"));
			Thread.sleep(2000);
		} else {
			BaseUI.click(BaseUI.waitForElementToBeClickable("loginPageForgotPasswordLink", null, null));
			Thread.sleep(2000);
		}
	}

	public static void resetForgotPasswordForm(String entity, String userName, String emailSubjectLine)
			throws InterruptedException {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEntityText"), entity);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordLoginNameText"), userName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEmailSubjectLineText"),
				emailSubjectLine);
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordSubmitButton"));
		Thread.sleep(2000);
	}

	public static void verifyForgotPasswordModalAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPasswordModal"));
	}

	public static void enterForgotPasswordEntity(String valueToSet) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEntityText"), valueToSet);
	}

	public static void enterForgotPasswordLoginName(String valueToSet) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordLoginNameText"), valueToSet);
	}

	public static void enterForgotPasswordEmailSubjectLine(String valueToSet) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEmailSubjectLineText"), valueToSet);
	}

	public static void clickForgotPasswordSubmitButton() {
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordSubmitButton"));
	}

	public static void clickForgotPasswordCancelButton() {
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordCancelButton"));
	}

	public static void verifyForgotPasswordEmailConfirmationMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPasswordEmailConfirmationMessage"));
	}

	public static void verifyForgotPasswordErrorMesage(String expectedValue) {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPaswordErrorMessage"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("loginPageForgotPaswordErrorMessage"), expectedValue);
	}

}// end of class
