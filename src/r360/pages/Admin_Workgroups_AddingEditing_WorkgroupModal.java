package r360.pages;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.OptionalInt;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.DatabaseConnection;
import utils.Locator;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.Pagination;

public class Admin_Workgroups_AddingEditing_WorkgroupModal {

	public static final String dda_GridID = "ddaGrid";
	public static Pagination ddaGrid = new Pagination(dda_GridID);
	public static final String field_GridID = "fieldGrid";
	public static Pagination fieldGrid = new Pagination(field_GridID);
	public static Dropdown bankName_Dropdown = new Dropdown("s2id_bankSelect");
	public static Dropdown displayBatchID_Dropdown = new Dropdown("s2id_displayBatchIDSelect");
	public static Dropdown paymentImageDisplay_Dropdown = new Dropdown("s2id_checkImageDisplayModeSelect");
	public static Dropdown documentImageDisplay_Dropdown = new Dropdown("s2id_documentImageDisplayModeSelect");
	public static Dropdown itemType_Dropdown = new Dropdown("s2id_docTypeSelect");
	public static Dropdown dataType_Dropdown = new Dropdown("s2id_dataTypeSelect");
	public static Dropdown paymentSource_Dropdown = new Dropdown("s2id_paymentSourceSelect");
	public static Dropdown paymentSource_DropdownACH = new Dropdown("s2id_paymentSourceACHSelect");
	public static Dropdown template_Dropdown = new Dropdown("s2id_templateSelect");

	public static void set_WorkgroupNames(String longName, String shortName) throws Throwable {
		if(Browser.currentBrowser.equals("chrome")){
			BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"), longName);
			BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"), shortName);
		}else {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_LongNameText"), longName);
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_ShortNameText"), shortName);
		}
	}

	public static void set_WorkgroupStorageSettings(String dataRentention, String imageRetention, String fileGroup)
			throws Throwable {
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("wrkgrp_AddEdit_DataRentionDaysText"), dataRentention);
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("wrkgrp_AddEdit_ImageRentionDaysText"), imageRetention);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_FileGroupText"), fileGroup);
	}

	public static void set_Workgroup_ReportDisplaySettings(String viewingDays, String maxSearchDays,
			String displayBatchID, String paymentImageDisplay, String documentImageDisplay, Boolean isHOA_Checked)
			throws Throwable {
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("wrkgrp_ViewingDaysText"), viewingDays);
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("wrkgrp_MaxSearchDays"), maxSearchDays);
		displayBatchID_Dropdown.select_EntityValue(displayBatchID);
		paymentImageDisplay_Dropdown.select_EntityValue(documentImageDisplay);
		documentImageDisplay_Dropdown.select_EntityValue(documentImageDisplay);
		if (isHOA_Checked) {
			BaseUI.checkCheckbox("wrkgrp_IsHOA_Checkbox");
		} else {
			BaseUI.uncheckCheckbox("wrkgrp_IsHOA_Checkbox");
		}

	}

	public static void set_AddWorkgroupDefaultSettings(String bankName, String workGroupID) throws Exception {
		bankName_Dropdown.select_EntityValue(bankName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_WorkgroupID"), workGroupID);
	}

	public static void reset_Modal(String longName, String shortName, String dataRentention, String imageRetention,
			String fileGroup, String viewingDays, String maxSearchDays, String displayBatchID,
			String paymentImageDisplay, String documentImageDisplay, Boolean isHOA_Checked) throws Throwable {

		set_WorkgroupNames(longName, shortName);
		set_WorkgroupStorageSettings(dataRentention, imageRetention, fileGroup);
		set_Workgroup_ReportDisplaySettings(viewingDays, maxSearchDays, displayBatchID, paymentImageDisplay,
				documentImageDisplay, isHOA_Checked);

		save_AddingEditing_WorkgroupModal();
	}

	public static void add_Workgroup(String bankName, String workGroupID, String longName, String shortName,
			String dataRentention, String imageRetention, String fileGroup, String viewingDays, String maxSearchDays,
			String displayBatchID, String paymentImageDisplay, String documentImageDisplay, Boolean isHOA_Checked)
			throws Throwable {
		set_AddWorkgroupDefaultSettings(bankName, workGroupID);
		set_WorkgroupNames(longName, shortName);
		set_WorkgroupStorageSettings(dataRentention, imageRetention, fileGroup);
		set_Workgroup_ReportDisplaySettings(viewingDays, maxSearchDays, displayBatchID, paymentImageDisplay,
				documentImageDisplay, isHOA_Checked);

		save_AddingEditing_WorkgroupModal();
	}

	public static void cancel_AddingEditing_WorkgroupModal() throws Throwable {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_CancelButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void save_AddingEditing_WorkgroupModal() throws Throwable {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_SaveButton"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void isWorkGroupPresent() throws Throwable {
		WebElement fieldName = Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName_Error");
		String fieldNameError = fieldName.getText();
		String expectedError = "The field name entered already exists.";
		if(fieldNameError.equals(expectedError))
		{
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_DataEntryFieldsModal();
			Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
		}
	}

	public static void verify_Textbox_Value(String textBoxIdentifier, String expectedValue) {
		String textBoxValue = BaseUI.getTextFromInputBox(Locator.lookupElement(textBoxIdentifier));
		BaseUI.verify_true_AndLog(textBoxValue.equals(expectedValue),
				MessageFormat.format("Textbox value {0} was a match", textBoxValue),
				MessageFormat.format("Expected value: {0}, Seeing value: {1}.", expectedValue, textBoxValue));
	}

	public static void delete_EditWorkGroup_RT_And_DDA(String columnName, String columnText) throws Throwable {
		WebElement element = Admin_Workgroups_AddingEditing_WorkgroupModal.ddaGrid
				.delete_ByColumnName_andColumnText(columnName, columnText);
		BaseUI.click(element);
		Thread.sleep(1000);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launch_DataEntryFields_AddButton() throws Exception {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_AddButton"));
		Thread.sleep(500);
	}

	public static void save_DataEntryFieldsModal() throws Exception {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_SaveButton"));
		Thread.sleep(500);
	}

	public static void cancel_DataEntryFieldsModal() throws Exception {
		BaseUI.click_js(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_CancelButton"));
		Thread.sleep(500);
	}
	
//	adds data entry fields for Blank Template 
	public static void add_DataEntryFields(String template, Boolean isActive_Checked, String fieldName, String UILabel, String itemType, 
			String displayOrder, String dataType, String paymentSource) throws Exception{
		template_Dropdown.select_EntityValue(template);
		if(isActive_Checked){
			BaseUI.checkCheckbox("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox");
		} else {
			BaseUI.uncheckCheckbox("wrkgrp_AddEdit_DataEntryFields_Active_CheckBox");
		}
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_FieldName"), fieldName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_UILabel"), UILabel);
		itemType_Dropdown.select_EntityValue(itemType);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_DisplayOrder"), displayOrder);
		dataType_Dropdown.select_EntityValue(dataType);
		paymentSource_Dropdown.select_EntityValue(paymentSource);
	}
	
//	adds data entry fields for ACH or Standard Wire
	public static void add_DataEntryFields_NonBlankTemplate(String template, String paymentSource) throws Exception{
		template_Dropdown.select_EntityValue(template);
		paymentSource_DropdownACH.select_EntityValue(paymentSource);
	}
	
	//since the grid need to be sorted to select the right workgroup
	public static void launch_SortClickWorkgroup_NavigateTo_DataEntryFieldsTab(String workGroup) throws Throwable{
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Descending("Long Name");
		Admin_Workgroups.workGroupSettings_GRID.sort_Column_Ascending("Long Name");
		Admin_Workgroups.launch_EditWorkGroupSettings_Modal(workGroup);
		Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DataEntryFields_Tab();
	}
	
	public static void launch_EditDataEntryFieldsSettings_ForGivenFieldName(String fieldName) throws Throwable {
		BaseUI.click(Admin_Workgroups_AddingEditing_WorkgroupModal.fieldGrid.editButton_ByColumnName_andColumnText("Name", fieldName));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void navigate_to_General_Tab() throws Exception {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_General_Tab"));
		Thread.sleep(500);
	}

	public static void navigate_to_DataEntryFields_Tab() throws Exception {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_Tab"));
		Thread.sleep(500);
	}

	public static void navigate_to_DDAAssociation_Tab() throws Exception {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_Tab"));
		Thread.sleep(500);
	}

	public static void click_AssociateButton() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_Associate_Button"));
		Thread.sleep(300);
	}

	public static void set_RT_And_DDA_RequiredFields(String rtNumber, String ddaNumber) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_R/TText"), rtNumber);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("wrkgrp_AddEdit_DDAAssociation_DDAText"), ddaNumber);
		click_AssociateButton();
		Thread.sleep(1000);
	}

	public static void associate_RT_And_DDA_ToWorkgroup(String rtNumber, String ddaNumber) throws Throwable {
		navigate_to_DDAAssociation_Tab();
		set_RT_And_DDA_RequiredFields(rtNumber, ddaNumber);
		Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void delete_WorkGroup_From_RAAM_Database(String externalReferenceID)throws ClassNotFoundException, SQLException{
		/* Query for deleting wokgroup from rechubqadb02.RAAM database */
		String query = "Declare @Resourceid INT" + "\n" + "Declare @ExternalReferenceID CHAR"
				+ "\n" + "Set @ExternalReferenceID = '" + externalReferenceID + "'"
				+ "\n" + "Select @Resourceid = ResourceID from Resources WHERE ExternalReferenceID = @ExternalReferenceID;"
				+ "\n" + "Select @Resourceid"
				+ "\n" + "EXEC usp_AuthorizationPolicies_DeleteByResource @Resourceid"
				+ "\n" + "EXEC usp_Resources_Delete @Resourceid" ;
				
		DatabaseConnection.dbUrl = GlobalVariables.raam_dbUrl;
		DatabaseConnection.runSQLServerCommand(query);
	}

	public static void delete_Workgroup_From_Database(String workGroupID, String bankID, String siteCodeID)
			throws ClassNotFoundException, SQLException {
		delete_All_Data_for_Workgroup(siteCodeID, workGroupID, bankID);
		/* Query for deleting wokgroup from rechubqadb02.WFSDB.R360 database */
		String query = "declare @ClientAccountID int" + "\n" + "declare @BankID int" + "\n" + "declare @SiteCodeID int" 
				+ "\n" + "set @ClientAccountID    = '" + workGroupID + "'" + "\n" + "set @BankID = '" + bankID + "'"
				+ "\n" + "set @SiteCodeID     = '" + siteCodeID + "'" + "\n"
				+ "\n" + "IF EXISTS (SELECT 1 FROM [RecHubUser].[SessionClientAccountEntitlements]  WHERE SiteClientAccountID = @ClientAccountID AND SiteBankID = @BankID)"
				+ "\n" + "BEGIN" + "\n" + "DELETE [RecHubUser].[SessionClientAccountEntitlements]" + "\n" + "WHERE"
				+ "\n" + "SiteClientAccountID = @ClientAccountID" + "\n" + "AND SiteBankID = @BankID;" + "\n" + "END"
				+ "\n"
				+ "IF EXISTS (SELECT 1 FROM RecHubAudit.factExtractAudits "
				+ "\n" + "INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubAudit.factExtractAudits.ClientAccountKey"
				+ "\n" + "WHERE SiteClientAccountID = @ClientAccountID AND SiteBankID = @BankID AND SiteCOdeID = @SiteCodeID)"
				+ "\n" + "BEGIN" + "\n" + "DELETE RecHubAudit.factExtractAudits"
				+ "\n" + "WHERE  RecHubAudit.factExtractAudits.ClientAccountKey IN"
				+ "\n" + "(SELECT RecHubData.dimClientAccounts.ClientAccountKey FROM RecHubData.dimClientAccounts"
				+ "\n" + "WHERE SiteClientAccountID = @ClientAccountID AND SiteBankID = @BankID AND SiteCOdeID = @SiteCodeID);"
				+ "\n" + "END" + "\n"
				+ "IF EXISTS (SELECT 1 FROM [RecHubData].[dimClientAccounts] WHERE SiteClientAccountID = @ClientAccountID AND SiteBankID = @BankID AND SiteCOdeID = @SiteCodeID)"
				+ "\n" + "BEGIN" + "\n" + "DELETE [RecHubData].[dimClientAccounts]" + "\n" + "WHERE" + "\n"
				+ "SiteClientAccountID = @ClientAccountID" + "\n" + "AND SiteBankID = @BankID" + "\n"
				+ "AND SiteCodeID = @SiteCodeID;" + "\n"
				+ "END" + "\n"
				+ "IF EXISTS (SELECT 1 FROM [RecHubUser].[OLWorkgroups] WHERE SiteClientAccountID = @ClientAccountID AND SiteBankID = @BankID)"
				+ "\n" + "BEGIN" + "\n"
				+ "DELETE [RecHubUser].[OLWorkgroups]"
				+ "\n" + "WHERE" + "\n"
				+ "SiteClientAccountID = @ClientAccountID" + "\n"
				+ "AND SiteBankID = @BankID;" + "\n"
				+ "END";
		DatabaseConnection.dbUrl = GlobalVariables.dbUrl;
		DatabaseConnection.runSQLServerCommand(query);

	}

	public static void delete_All_Data_for_Workgroup(String siteCode, String workgroupID, String bankID) throws SQLException, ClassNotFoundException {
		String[] tablesToClean_by_WorkgroupKey = {
			"RecHubData.ClientAccountBatchPaymentTypes",
			"RecHubData.ClientAccountBatchSources",
			"RecHubData.factBatchData",
			"RecHubData.factBatchExtracts",
			"RecHubData.factBatchSummary",
			"RecHubData.factCheckImages",
			"RecHubData.factChecks",
			"RecHubData.factDataEntryDetails",
			"RecHubData.factDocumentImages",
			"RecHubData.factDocuments",
			"RecHubData.factExtractTraces",
			"RecHubData.factItemData",
			"RecHubData.factNotificationFiles",
			"RecHubData.factNotifications",
			"RecHubData.factRawPaymentData",
			"RecHubData.factStubs",
			"RecHubData.factTransactionSummary",
			"RecHubUser.SessionClientAccountEntitlements",
			"RecHubAlert.EventLog",
			"RecHubAudit.factExtractAudits",
			"RecHubData.dimClientAccounts"
		};
		String[] tablesToClean_by_WorkgroupID = {
			"RecHubData.dimImageRPSAliasMappings",
			"RecHubData.dimWorkgroupBusinessRules",
			"RecHubData.dimWorkgroupDataEntryColumns"
		};

		OptionalInt workgroupKey = GetWorkgroupKey(siteCode, workgroupID, bankID);

		if(workgroupKey.isPresent()) {
			for (String table : tablesToClean_by_WorkgroupKey) {
				delete_All_Rows_With_WorkgroupKey(table, String.valueOf(workgroupKey.getAsInt()));
			}
		}

		for (String table : tablesToClean_by_WorkgroupID) {
			delete_All_Rows_With_WorkgroupID(table, bankID, workgroupID);
		}
	}

	private static OptionalInt GetWorkgroupKey(String siteCode, String workgroupID, String bankID) {
		DatabaseConnection.dbUrl = GlobalVariables.dbUrl;
		String query=
				"SELECT Top 1 ClientAccountKey\n" +
				"  FROM RecHubData.dimClientAccounts\n" +
				"  WHERE SiteBankID = " + bankID + "\n" +
				"\tAND SiteClientAccountID = " + workgroupID + "\n" +
				"\tAND SiteCodeID = " + siteCode;
		try {
			String workgroupKey = DatabaseConnection.runSQLServerQueryString(query);
			return OptionalInt.of(Integer.parseInt(workgroupKey));
		}
		catch (SQLException e) {
			//This is here in case the workgroup is not in the system
			//In which case the return value should be empty
			//This is not really an error so need not be recorded
			return OptionalInt.empty();
		}
	}

	private static void delete_All_Rows_With_WorkgroupID(String table, String bankID, String workgroupID) throws SQLException, ClassNotFoundException {
		DatabaseConnection.dbUrl = GlobalVariables.dbUrl;
		String query=
				"Delete WFSDB_R360." + table + "\n" +
				"	WHERE SiteBankID = " + bankID + "\n" +
				"		AND SiteClientAccountID = " + workgroupID;
		DatabaseConnection.runSQLServerCommand(query);
	}

	public static void delete_All_Rows_With_WorkgroupKey(String table, String workgroupKey) throws SQLException, ClassNotFoundException {
		DatabaseConnection.dbUrl = GlobalVariables.dbUrl;
		String query=
				"Delete WFSDB_R360." + table + "\n" +
				"  WHERE ClientAccountKey = " + workgroupKey;
		DatabaseConnection.runSQLServerCommand(query);
	}

	public static void delete_DataEntry_FromDatabase(String workgroupID, String bankID, String fieldName) throws ClassNotFoundException, SQLException{
		/* Query for deleting single data entry field from rechubqadb02.WFSDB.R360 database */
		String query = "declare @ClientAccountID int" + "\n" + "declare @BankID int" + "\n"  + "declare @FieldName varchar(99)" + "\n"
				+ "set @ClientAccountID    = '" + workgroupID +"'" + "\n" + "set @BankID = '" + bankID + "'" + "\n"
				+ "set @FieldName     = '" + fieldName + "'" + "\n" 
				+ "IF EXISTS (SELECT 1 FROM [RecHubData].[dimWorkgroupDataEntryColumns]  WHERE SiteClientAccountID = @ClientAccountID AND SiteBankID = @BankID AND FieldName = @FieldName)" + "\n"
				+ "BEGIN" + "\n"
				+ "\n" + "DELETE [RecHubData].[dimWorkgroupDataEntryColumns]"
				+ "\n" + "WHERE" + "\n"
				+ "SiteClientAccountID = @ClientAccountID"
				+ "\n" + "AND SiteBankID = @BankID" + "\n"
				+ "AND FieldName = @FieldName;" + "\n"
				+ "END" ;
		
		DatabaseConnection.dbUrl = GlobalVariables.dbUrl;
		DatabaseConnection.runSQLServerCommand(query);
	}
	
	public static void delete_DataEntry_FromDataBase_ForNonBlankTemplate(String workgroupID)throws ClassNotFoundException, SQLException{
		/* Query for deleting all data entry fields for Non-BlankTemplate from rechubqadb02.WFSDB.R360 database */
		String query = "declare @ClientAccountID int" + "\n"
				+ "set @ClientAccountID    = '" + workgroupID +"'" + "\n" 
				+ "IF EXISTS (SELECT 1 FROM [RecHubData].[dimWorkgroupDataEntryColumns]  WHERE SiteClientAccountID = @ClientAccountID )" + "\n"
				+ "BEGIN" + "\n"
				+ "\n" + "DELETE [RecHubData].[dimWorkgroupDataEntryColumns]"
				+ "\n" + "WHERE" + "\n"
				+ "SiteClientAccountID = @ClientAccountID" +"\n"
				+ "END" ;
		
		DatabaseConnection.dbUrl = GlobalVariables.dbUrl;
		DatabaseConnection.runSQLServerCommand(query);
	}

}
