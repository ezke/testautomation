package r360.pages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class PreDepositExceptionsTransactionDetailPage {
    public void waitForPageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("preDepositTransactionDetailPageTitle", null, null);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
    }

    public void rejectTransaction() throws Exception {
        WebElement rejectButton = Locator.lookupElement("preDepositTransactionDetailRejectButton");
        BaseUI.click(rejectButton);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
    }

    public void saveTransaction() throws Exception {
        WebElement saveButton = Locator.lookupElement("preDepositTransactionDetailSaveButton");
        BaseUI.click(saveButton);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
    }

    public PreDepositExceptionsBatchDetailPage returnToBatchDetail() throws Exception {
        WebElement breadCrumb = Locator.lookupElement("preDepositTransactionDetailBatchBreadCrumb");
        BaseUI.click(breadCrumb);
        PreDepositExceptionsBatchDetailPage nextPage = new PreDepositExceptionsBatchDetailPage();
        nextPage.waitForPageToLoad();
        return nextPage;
    }
}
