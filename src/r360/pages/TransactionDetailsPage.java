package r360.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.BaseUI;
import utils.Browser;

public class TransactionDetailsPage  {
    public TransactionDetailsPage() {
        PageFactory.initElements(Browser.driver, this);
    }

    // Transaction Details Page Header
    @FindBy(xpath = "//SPAN[@_ngcontent-c18='']")
    private WebElement TransactionDetailPageHeader;
    @FindBy(xpath = "//A[text()='Dashboard']")
    private WebElement DashboardInHeaderPathLink;
    @FindBy(xpath = "//A[@_ngcontent-c8=''][text()='Batch Summary']")
    private WebElement BatchSummaryInHeaderPathLink;
    @FindBy(xpath = "//A[@_ngcontent-c8=''][text()='Batch Detail']")
    private WebElement BatchDetailInHeaderPathLink;
    @FindBy(xpath = "//I[@id='printPageButton']")
    private WebElement PrintIconInheader;
    @FindBy(xpath = "//I[@id='refreshPageButton']")
    private WebElement RefreshIconInHeader;
    public boolean isTransactionDetailPageHeaderVisible() {return BaseUI.elementAppears(TransactionDetailPageHeader);}
    public boolean isDashboardInHeaderPathLinkVisible() {return BaseUI.elementAppears(DashboardInHeaderPathLink);}
    public boolean isBatchSummaryInHeaderPathLinkVisible() {return BaseUI.elementAppears(BatchSummaryInHeaderPathLink);}
    public boolean isBatchDetailInHeaderPathLinkVisible() {return BaseUI.elementAppears(BatchDetailInHeaderPathLink);}
    public boolean isPrintIconInheaderVisible() {return BaseUI.elementAppears(PrintIconInheader);}
    public boolean isRefreshIconInHeaderVisible() {return BaseUI.elementAppears(RefreshIconInHeader);}


    // Transaction Details Page Transaction Details
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Transaction Details']")
    private WebElement TransactionDetailsLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Bank: ']")
    private WebElement BankLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Workgroup: ']")
    private WebElement WorkgroupLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Deposit Date: ']")
    private WebElement DepositDateLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Batch: ']")
    private WebElement BatchLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Account Site Code: ']")
    private WebElement AccountSiteCodeLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Batch Site Code: ']")
    private WebElement BatchSiteCodeLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Batch Cue ID: ']")
    private WebElement BatchCueIDLabel;
    @FindBy(xpath = "//STRONG[@_ngcontent-c18=''][text()='Transaction: ']")
    private WebElement TransactionLabel;
    public boolean isTransactionDetailsLabelVisible() {return BaseUI.elementAppears(TransactionDetailsLabel);}
    public boolean isBankLabelVisible() {return BaseUI.elementAppears(BankLabel);}
    public boolean isWorkgroupLabelVisible() {return BaseUI.elementAppears(WorkgroupLabel);}
    public boolean isDepositDateLabelVisible() {return BaseUI.elementAppears(DepositDateLabel);}
    public boolean isBatchLabelVisible() {return BaseUI.elementAppears(BatchLabel);}
    public boolean isAccountSiteCodeLabelVisible() {return BaseUI.elementAppears(AccountSiteCodeLabel);}
    public boolean isBatchSiteCodeLabelVisible() {return BaseUI.elementAppears(BatchSiteCodeLabel);}
    public boolean isBatchCueIDLabelVisible() {return BaseUI.elementAppears(BatchCueIDLabel);}
    public boolean isTransactionLabelVisible() {return BaseUI.elementAppears(TransactionLabel);}

    // Transaction Details Page Payment Items
    @FindBy(xpath = "//STRONG[@_ngcontent-c19=''][text()='Payment Items']")
    private WebElement PaymentItemsheader;
    @FindBy(xpath = "//SELECT[@name='paymentgrid-grid_length']")
    private WebElement PaymentItemsShowEntries;
    @FindBy(xpath = "//SELECT[@name='paymentgrid-grid_length']/option[@value='1000']")
    private WebElement PaymentItemsShow1000Entries;
    @FindBy(xpath = "//SELECT[@name='paymentgrid-grid_length']/option[@value='100']")
    private WebElement PaymentItemsShow100Entries;
    @FindBy(xpath = "//SELECT[@name='paymentgrid-grid_length']/option[@value='50']")
    private WebElement PaymentItemsShow50Entries;
    @FindBy(xpath = "//SELECT[@name='paymentgrid-grid_length']/option[@value='25']")
    private WebElement PaymentItemsShow25Entries;
    @FindBy(xpath = "//SELECT[@name='paymentgrid-grid_length']/option[@value='10']")
    private WebElement PaymentItemsShow10Entries;
    @FindBy(xpath = "(//LABEL)[2]//INPUT[@type='search']")
    private WebElement PaymentItemsSearchField;
    public boolean isPaymentItemsheaderVisible() {return BaseUI.elementAppears(PaymentItemsheader);}
    public boolean isPaymentItemsShowEntriesVisible() {return BaseUI.elementAppears(PaymentItemsShowEntries);}
    public boolean isPaymentItemsShow1000EntriesVisible() {return BaseUI.elementAppears(PaymentItemsShow1000Entries);}
    public boolean isPaymentItemsShow100EntriesVisible() {return BaseUI.elementAppears(PaymentItemsShow100Entries);}
    public boolean isPaymentItemsShow50EntriesVisible() {return BaseUI.elementAppears(PaymentItemsShow50Entries);}
    public boolean isPaymentItemsShow25EntriesVisible() {return BaseUI.elementAppears(PaymentItemsShow25Entries);}
    public boolean isPaymentItemsShow10EntriesVisible() {return BaseUI.elementAppears(PaymentItemsShow10Entries);}
    public boolean isPaymentItemsSearchFieldVisible() {return BaseUI.elementAppears(PaymentItemsSearchField);}

    // Transaction Details Page Related Items

    @FindBy(xpath = "//STRONG[@_ngcontent-c20=''][text()='Related Items']")
    private WebElement RelatedItemsheader;
    @FindBy(xpath = "//SELECT[@name='relatedgrid-grid_length']")
    private WebElement RelatedItemsShowEntries;
    @FindBy(xpath = "//SELECT[@name='relatedgrid-grid_length']/option[@value='1000']")
    private WebElement RelatedItemsShow1000Entries;
    @FindBy(xpath = "//SELECT[@name='relatedgrid-grid_length']/option[@value='100']")
    private WebElement RelatedItemsShow100Entries;
    @FindBy(xpath = "//SELECT[@name='relatedgrid-grid_length']/option[@value='50']")
    private WebElement RelatedItemsShow50Entries;
    @FindBy(xpath = "//SELECT[@name='relatedgrid-grid_length']/option[@value='25']")
    private WebElement  RelatedItemsShow25Entries;
    @FindBy(xpath = "//SELECT[@name='relatedgrid-grid_length']/option[@value='10']")
    private WebElement RelatedItemsShow10Entries;
    @FindBy(xpath = "(//LABEL)[4]//INPUT[@type='search']")
    private WebElement RelatedItemsSearchField;
    public boolean isRelatedItemsheaderVisible() {return BaseUI.elementAppears(RelatedItemsheader);}
    public boolean isRelatedItemsShowEntriesVisible() {return BaseUI.elementAppears(RelatedItemsShowEntries);}
    public boolean isRelatedItemsShow1000EntriesVisible() {return BaseUI.elementAppears(RelatedItemsShow1000Entries);}
    public boolean isRelatedItemsShow100EntriesVisible() {return BaseUI.elementAppears(RelatedItemsShow100Entries);}
    public boolean isRelatedItemsShow50EntriesVisible() {return BaseUI.elementAppears(RelatedItemsShow50Entries);}
    public boolean isRelatedItemsShow25EntriesVisible() {return BaseUI.elementAppears(RelatedItemsShow25Entries);}
    public boolean isRelatedItemsShow10EntriesVisible() {return BaseUI.elementAppears(RelatedItemsShow10Entries);}
    public boolean isRelatedItemsSearchFieldVisible() {return BaseUI.elementAppears(RelatedItemsSearchField);}

    // Transaction Details Page  Documents

    @FindBy(xpath = "//STRONG[@_ngcontent-c21=''][text()='Documents']")
    private WebElement Documentsheader;
    @FindBy(xpath = "//SELECT[@name='documentgrid-grid_length']")
    private WebElement DocumentsShowEntries;
    @FindBy(xpath = "//SELECT[@name='documentgrid-grid_length']/option[@value='1000']")
    private WebElement DocumentsShow1000Entries;
    @FindBy(xpath = "//SELECT[@name='documentgrid-grid_length']/option[@value='100']")
    private WebElement DocumentsShow100Entries;
    @FindBy(xpath = "//SELECT[@name='documentgrid-grid_length']/option[@value='50']")
    private WebElement DocumentsShow50Entries;
    @FindBy(xpath = "//SELECT[@name='documentgrid-grid_length']/option[@value='25']")
    private WebElement DocumentsShow25Entries;
    @FindBy(xpath = "//SELECT[@name='documentgrid-grid_length']/option[@value='10']")
    private WebElement DocumentsShow10Entries;
    @FindBy(xpath = "(//LABEL)[6]//INPUT[@type='search']")
    private WebElement DocumentsSearchField;
    public boolean isDocumentsheaderVisible() {return  BaseUI.elementAppears(Documentsheader);}
    public boolean isDocumentsShowEntriesVisible() {return BaseUI.elementAppears(DocumentsShowEntries);}
    public boolean isDocumentsShow1000EntriesVisible() {return BaseUI.elementAppears(DocumentsShow1000Entries);}
    public boolean isDocumentsShow100EntriesVisible() {return BaseUI.elementAppears(DocumentsShow100Entries);}
    public boolean isDocumentsShow50EntriesVisible() {return BaseUI.elementAppears(DocumentsShow50Entries);}
    public boolean isDocumentsShow25EntriesVisible() {return BaseUI.elementAppears(DocumentsShow25Entries);}
    public boolean isDocumentsShow10EntriesVisible() {return BaseUI.elementAppears(DocumentsShow10Entries);}
    public boolean isDocumentsSearchFieldVisible() {return BaseUI.elementAppears(DocumentsSearchField);}
}
