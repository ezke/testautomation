package r360.pages;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.DataBuilder;
import utils.DatabaseConnection;
import utils.Locator;
import utils.TableData;
import wfsCommon.pages.DatePicker;
import wfsCommon.pages.PieChart;

public class DDA_Summary {

	public static DatePicker depositDate = new DatePicker("ddaSum_DepositDate_Textbox", null, null);
	public static PieChart TotalAmounts = new PieChart("paymentTotals");
	public static PieChart PaymentCounts = new PieChart("paymentCounts");

	public static TableData extract_DDASummary_TitleLines() {
		TableData tableData = new TableData();

		HashMap<String, String> tableExtractorMap = new HashMap<String, String>();
		tableExtractorMap.put("entity", "./div[1]//span[@class='slick-group-title']/span");
		tableExtractorMap.put("payment_count", "./div[3]");
		tableExtractorMap.put("total", "./div[4]");

		tableData.data = BaseUI.tableExtractor("ddaSum_TableDDARows", tableExtractorMap);
		tableData.remove_Character("DDA: ", "entity");
		return tableData;

	}

	public static TableData extract_DDASummary_nonTitleLines() {
		TableData tableData = new TableData();
		// target all the rows of our table
		ArrayList<WebElement> ddaSum_AllTableRows = Locator.lookup_multipleElements("ddaSum_AllTableRows", null, null);
		String DDA_Entity = "";

		// cycle through eacth of our table rows
		//
		for (WebElement tableRow : ddaSum_AllTableRows) {
			HashMap<String, String> newRow = new HashMap<String, String>();

			if (tableRow.getAttribute("class").contains("slick-group")) {
				DDA_Entity = tableRow.findElement(By.xpath("./div[1]//span[@class='slick-group-title']/span")).getText()
						.replace("DDA: ", "");
			} else {
				newRow.put("dda_entity", DDA_Entity);
				// Our query could only return the very last part of this
				// entity. So we'll compare that.
				String entity = tableRow.findElement(By.xpath("./div[2]")).getText();
				entity = entity.substring(entity.lastIndexOf("\\") + 1, entity.length());
				newRow.put("entity", entity);
				newRow.put("workgroup", tableRow.findElement(By.xpath("./div[3]")).getText());
				newRow.put("payment_source", tableRow.findElement(By.xpath("./div[4]")).getText());
				newRow.put("payment_type", tableRow.findElement(By.xpath("./div[5]")).getText());
				newRow.put("payment_count", tableRow.findElement(By.xpath("./div[6]")).getText());
				newRow.put("total", tableRow.findElement(By.xpath("./div[7]")).getText().replace(",", ""));
			}
			if (newRow.size() > 0) {
				tableData.data.add(newRow);
			}
		}

		// Was unable to recreate order in database call, had to change order so
		// that they match when it comes time to compare them.
		tableData.sort_ByColumn_Ascending("payment_type");
		tableData.sort_ByColumn_Ascending("workgroup");
		tableData.sort_ByColumn_numeric_Ascending("dda_entity");
		return tableData;
	}

	// pass in date in format yyyyMMdd
	// public static TableData return_DataBase_Data_For_DDASummary(String date)
	// throws ClassNotFoundException, SQLException {
	//
	// String query = "declare @Date int" + "\n" + "set @Date = '" + date + "'"
	// + "\n" + "SELECT" + "\n"
	// + "b.DDA as 'dda_entity', " + "\n" + "d.EntityName as 'entity'," + "\n"
	// + "CONCAT(c.SiteClientAccountID, ' - ', c.LongName) as 'workgroup'," +
	// "\n"
	// + "e.LongName as 'payment_source', " + "\n"
	// + "f.LongName as 'payment_type', count(*) as 'payment_count'" + "\n" +
	// ",SUM(Amount) 'total'" + "\n"
	// + "FROM [WFSDB_R360].[RecHubData].[factChecks] a" + "\n"
	// + " inner join WFSDB_R360.RecHubData.dimDDAs b on a.DDAKey = b.DDAKey" +
	// "\n"
	// + " inner join WFSDB_R360.rechubdata.dimBatchSources e on
	// e.BatchSourceKey = a.BatchSourceKey" + "\n"
	// + " inner join WFSDB_R360.RecHubData.dimBatchPaymentTypes f on
	// f.BatchPaymentTypeKey = a.BatchPaymentTypeKey"
	// + "\n"
	// + " inner join WFSDB_R360.RecHubData.dimClientAccounts c on
	// a.ClientAccountKey = c.ClientAccountKey"
	// + "\n" + " inner join raam.dbo.Entities d on d.EntityID = e.EntityID" +
	// "\n" + " where" + "\n"
	// + " IsDeleted = 0" + "\n" + " and DepositDateKey = @Date" + "\n"
	// + " group by b.DDA, d.EntityName, e.LongName, f.LongName,
	// c.SiteClientAccountID, c.LongName" + "\n"
	// + " order by b.DDA desc;";
	//
	// TableData tableData = DatabaseConnection.runSQLServerQuery(query);
	// // tableData.remove_DuplicateRows_ForPassedInColumnNames("dda_entity",
	// // "workgroup");
	// tableData.set_decimal_Precision(2);
	// tableData.sort_ByColumn_numeric_Ascending("dda_entity");
	// return tableData;
	// }

	// pass in date in format yyyyMMdd
	public static TableData return_DataBase_Data_For_DDASummary(String date) throws Exception {

		// String query = "Declare @date int" + "\n" + "Set @date = '" + date +
		// "'" + "\n" + " SELECT " + "\n"
		// + " h.DDA as 'dda_entity'" + "\n" + " ,d.EntityName as 'entity'" +
		// "\n"
		// + " ,concat (b.SiteClientAccountID, ' - ',b.LongName) as 'workgroup'"
		// + "\n"
		// + " --c.SiteBankID as 'Bank'" + "\n"
		// + " ,e.LongName as 'payment_source'" + "\n" + " ,f.LongName as
		// 'payment_type'" + "\n"
		// + " ,SUM(a.CheckCount) as 'payment_count'" + "\n"
		// + " ,SUM(a.CheckTotal) over (partition by h.dda, concat
		// (b.SiteClientAccountID, ' - ',b.LongName), f.LongName ) as 'total' "
		// + "\n"
		// + " FROM [WFSDB_R360].[RecHubData].[factTransactionSummary] a" + "\n"
		// + " inner join WFSDB_R360.RecHubData.dimClientAccounts b on
		// a.ClientAccountkey = b.ClientAccountkey"
		// + "\n"
		// + " inner join WFSDB_R360.RecHubData.factChecks i on
		// i.ClientAccountKey = a.ClientAccountKey and i.BankKey = a.BankKey and
		// /*i.BatchID = a.BatchID and i.BatchNumber = a.BatchNumber and
		// i.TransactionID = a.TransactionID and*/ i.BatchPaymentTypeKey =
		// a.BatchPaymentTypeKey and i.BatchSourceKey = a.BatchSourceKey and
		// i.CreationDate = a.CreationDate and i.ModificationDate =
		// a.ModificationDate"
		// + "\n" + " inner join WFSDB_R360.RecHubData.dimDDAs h on h.DDAKey =
		// i.DDAKey" + "\n"
		// + " inner join WFSDB_R360.RecHubData.dimBanks c on c.BankKey =
		// a.BankKey" + "\n"
		// + " inner join WFSDB_R360.rechubdata.dimBatchSources e on
		// e.BatchSourceKey = a.BatchSourceKey" + "\n"
		// + " inner join WFSDB_R360.RecHubData.dimBatchPaymentTypes f on
		// f.BatchPaymentTypeKey = a.BatchPaymentTypeKey"
		// + "\n"
		// + " inner join WFSDB_R360.RecHubUser.OLWorkgroups g on
		// g.SiteClientAccountID = b.SiteClientAccountID and g.SiteBankID =
		// c.SiteBankID"
		// + "\n" + " inner join raam.dbo.Entities d on d.EntityID = g.EntityID"
		// + "\n" + " where " + "\n"
		// + " a.IsDeleted = 0 and i.IsDeleted =0" + "\n" + "and
		// a.DepositDateKey = @date" + "\n"
		// + "group by h.DDA ,a.DepositDateKey ,d.EntityName ,e.LongName
		// ,f.LongName ,b.SiteClientAccountID ,b.LongName ,a.CheckCount,
		// a.CheckTotal"
		// + "\n" + "order by h.DDA desc";

		// 20170208
		String query = DataBuilder.Get_FileData_AsString("\\src\\r360\\data\\Automation DDA Summary query.sql");
		String command = DataBuilder
				.Get_FileData_AsString("\\src\\r360\\data\\Automation DDA Summary query CREATE Temp table.sql");
		query = query.replace("20170208", date);
		command = command.replace("20170208", date);

		// DatabaseConnection.runSQLServerCommand(command);
		TableData tableData = DatabaseConnection.runSQLServerCommand_AndThenQuery(command, query);
//		TableData TableWithoutExtras = new TableData();
//		TableWithoutExtras.data.add(tableData.data.get(0));
//		for (int i = 1; i < tableData.data.size(); i++) {
//			if (!tableData.data.get(i).equals(tableData.data.get(i - 1))) {
//				TableWithoutExtras.data.add(tableData.data.get(i));
//			}
//		}

		// tableData.remove_DuplicateRows_ForPassedInColumnNames("dda_entity",
		// "workgroup");
		tableData.set_decimal_Precision(2);
		tableData.sort_ByColumn_Ascending("payment_type");
		tableData.sort_ByColumn_numeric_Descending("total");
//		TableWithoutExtras.sort_ByColumn_numeric_Descending("payment_count");
		tableData.sort_ByColumn_Ascending("workgroup");
		tableData.sort_ByColumn_numeric_Ascending("dda_entity");
		return tableData;
	}

	public static String format_EntityName_ForLegend(String entityName) {
		entityName = entityName.replace("DDA: ", "");
		if (entityName.length() > 15) {
			entityName = entityName.substring(0, 6) + "..."
					+ entityName.substring(entityName.length() - 6, entityName.length());
		}

		return entityName;
	}

	public static void sort_Descending_ByColumnName(String columnName) throws Exception {
		for (int i = 0; i < 4; i++) {
			String sortElementClass = BaseUI.get_Attribute_FromField(Locator.lookupElement("ddaSum_SortIndicator_ByColumnHeader", columnName, null), "class");
			{
				if (sortElementClass.contains("desc")) {
					BaseUI.log_Status("Column " + columnName + " was sorted Descending");
					break;
				} else {
					BaseUI.click(Locator.lookupElement("ddaSum_Header_ByText", columnName, null));
					Thread.sleep(500);
				}

			}
		}
	}

	public static void sort_Ascending_ByColumnName(String columnName) throws Exception {
		for (int i = 0; i < 4; i++) {
			String sortElementClass = BaseUI.get_Attribute_FromField(Locator.lookupElement("ddaSum_SortIndicator_ByColumnHeader", columnName, null), "class");
			{
				if (sortElementClass.contains("asc")) {
					BaseUI.log_Status("Column " + columnName + " was sorted Ascending");
					break;
				} else {
					BaseUI.click(Locator.lookupElement("ddaSum_Header_ByText", columnName, null));
					Thread.sleep(500);
				}

			}
		}
	}

	public static void verify_PieChart_Contains_CorrectEntries(Integer entryCount, PieChart pieChart) {
		TableData tableData = extract_DDASummary_TitleLines();
		BaseUI.verify_true_AndLog(tableData.data.size() == entryCount,
				MessageFormat.format("List of Summaries was equal to {0}", tableData.data.size()), MessageFormat
						.format("List of Summaries was equal to {0}, Expected {1}", tableData.data.size(), entryCount));

		ArrayList<String> legendKeys = pieChart.list_of_LegendKeys();

		BaseUI.verify_true_AndLog(legendKeys.size() == entryCount,
				MessageFormat.format("List of Legend Keys was equal to {0}", legendKeys.size()), MessageFormat
						.format("List of Legend Keys was equal to {0}, Expected {1}", legendKeys.size(), entryCount));

		ArrayList<WebElement> pieSlices = pieChart.list_of_PieSlices();

		BaseUI.verify_true_AndLog(pieSlices.size() == entryCount,
				MessageFormat.format("List of Pie Slices was equal to {0}", pieSlices.size()), MessageFormat
						.format("List of Pie Slices was equal to {0}, Expected {1}", pieSlices.size(), entryCount));

		for (HashMap<String, String> tableRow : tableData.data) {
			String entity = format_EntityName_ForLegend(tableRow.get("entity"));
			BaseUI.verify_true_AndLog(legendKeys.contains(entity),
					MessageFormat.format("Entity {0} was found in Pie Chart legend.", entity),
					MessageFormat.format("Entity {0} was NOT found in Pie Chart legend.", entity));
		}

	}

	/// Total Amounts Pie Chart validations and methods.
	///
	///

	// unable to see sliced accurately under 0.5. Needed to remove them.
	public static void VerifyDDASummary_Against_TotalAmounts_PieChart() throws Exception {
		TableData tableData = extract_DDASummary_TitleLines();
		tableData.remove_Rows_UnderThreshold_ForColumn(2.0, "total");

		TableData tableData_SmallSlices = extract_DDASummary_TitleLines();
		tableData_SmallSlices.remove_Rows_OverThreshold_ForColumn(1.9, "total");
		// Still cannot check the very very small slices.
		tableData_SmallSlices.remove_Rows_UnderThreshold_ForColumn(2.0, "total");

		BaseUI.verify_true_AndLog(tableData.data.size() > 0, "Data size was Greater than 0.", "Data was empty.");

		check_TotalAmounts_PieChartSlices(tableData);

		// uncheck all of the large percentage slices
		for (HashMap<String, String> tableRow : tableData.data) {
			String entityName = tableRow.get("entity");
			entityName = format_EntityName_ForLegend(entityName);

			TotalAmounts.uncheck_legendBox(entityName);
			Thread.sleep(100);
		}
		Thread.sleep(750);

		check_TotalAmounts_PieChartSlices(tableData_SmallSlices);

	}

	public static void verify_TotalAmounts_PieChart() throws Exception {
		TableData tableData = extract_DDASummary_TitleLines();
		ArrayList<String> piechart_LegendKeys = TotalAmounts.list_of_LegendKeys();
		BaseUI.verify_true_AndLog(tableData.data.size() == piechart_LegendKeys.size(),
				"DDA Summaries list matches keys size in Payment Totals pie chart.",
				"DDA Summary List's size did not match the number of keys in Payment Totals pie chart.");
		BaseUI.verify_true_AndLog(tableData.data.size() > 0, "DDA Summary list contained values.",
				"DDA Summary list was empty.");

		verify_PieChartData(TotalAmounts, tableData, "Amount");
	}

	public static void verify_TotalPayments_PieChart() throws Exception {
		TableData tableData = extract_DDASummary_TitleLines();
		ArrayList<String> piechart_LegendKeys = TotalAmounts.list_of_LegendKeys();
		BaseUI.verify_true_AndLog(tableData.data.size() == piechart_LegendKeys.size(),
				"DDA Summaries list matches keys size in Total Payments pie chart.",
				"DDA Summary List's size did not match the number of keys in Total Payments pie chart.");
		BaseUI.verify_true_AndLog(tableData.data.size() > 0, "DDA Summary list contained values.",
				"DDA Summary list was empty.");

		verify_PieChartData(PaymentCounts, tableData, "Count");
	}

	private static void verify_PieChartData(PieChart pieChartToVerify, TableData tableDataToVerify,
			String countORAmount) throws Exception {
		pieChartToVerify.uncheck_ALL_legendBoxes();

		for (HashMap<String, String> tableRow : tableDataToVerify.data) {
			String entity = format_EntityName_ForLegend(tableRow.get("entity"));
			pieChartToVerify.check_legendBox(entity);

			// Need to add logic for longer entities
			String toolTip = "";

			if (entity.length() > 15) {
				int index = pieChartToVerify.return_Index_OfLegendItem(entity);
				toolTip = pieChartToVerify.get_ToolTip_ByIndex(index);
			} else {
				toolTip = pieChartToVerify.get_ToolTip_ByText(entity);
			}

			String toolTipEntity = toolTip.split("\\n")[0];
			String toolTipAmount = "";
			try {
				toolTipAmount = toolTip.split("\\n")[1];
			} catch (Exception e) {
				Thread.sleep(100);
			}

			toolTipAmount = toolTipAmount.substring(0, toolTipAmount.indexOf('('));
			toolTipAmount = toolTipAmount.replace("$", "").trim();
			Thread.sleep(100);

			toolTipEntity = toolTipEntity.replace("DDA: ", "");

			BaseUI.baseStringCompare("Entity", tableRow.get("entity").trim(), toolTipEntity.trim());
			if (countORAmount.equals("Amount")) {
				BaseUI.baseStringCompare("Amount", tableRow.get("total"), toolTipAmount);
			} else {
				BaseUI.baseStringCompare("Count", tableRow.get("payment_count"), toolTipAmount);
			}
			Thread.sleep(3000);
			pieChartToVerify.uncheck_ALL_legendBoxes();
		}

	}

	public static void verify_TotalAmounts_legendkey_Matches_DDASummaryList() {
		TableData tableData = extract_DDASummary_TitleLines();
		ArrayList<String> piechart_LegendKeys = TotalAmounts.list_of_LegendKeys();

		BaseUI.verify_true_AndLog(tableData.data.size() == piechart_LegendKeys.size(),
				"DDA Summaries list matches keys size in Payment Totals pie chart.",
				"DDA Summary List's size did not match the number of keys in Payment Totals pie chart.");

		BaseUI.verify_true_AndLog(tableData.data.size() > 0, "DDA Summary list contained values.",
				"DDA Summary list was empty.");

		for (HashMap<String, String> tableRow : tableData.data) {
			String entity = format_EntityName_ForLegend(tableRow.get("entity"));
			BaseUI.verify_true_AndLog(piechart_LegendKeys.contains(entity),
					MessageFormat.format("Entity {0} was found in Pie Chart legend.", entity),
					MessageFormat.format("Entity {0} was NOT found in Pie Chart legend.", entity));
		}
	}

	public static void check_TotalAmounts_PieChartSlices(TableData tableData) throws Exception {
		for (HashMap<String, String> tableRow : tableData.data) {
			String toolTip = "";

			// Need to add logic for longer entities
			String entityName = tableRow.get("entity").replace("DDA: ", "");
			if (entityName.length() > 15) {
				entityName = format_EntityName_ForLegend(entityName);
				int index = TotalAmounts.return_Index_OfLegendItem(entityName);
				toolTip = TotalAmounts.get_ToolTip_ByIndex(index);
			} else {
				toolTip = TotalAmounts.get_ToolTip_ByText(entityName);
			}

			String toolTipEntity = toolTip.split("\\n")[0];
			String toolTipAmount = "";
			try {
				toolTipAmount = toolTip.split("\\n")[1];
			} catch (Exception e) {
				Thread.sleep(100);
			}

			toolTipAmount = toolTipAmount.substring(0, toolTipAmount.indexOf('('));
			toolTipAmount = toolTipAmount.replace("$", "").trim();
			Thread.sleep(100);

			BaseUI.baseStringCompare("Entity", tableRow.get("entity"), toolTipEntity);
			BaseUI.baseStringCompare("Amount", tableRow.get("total"), toolTipAmount);
			Thread.sleep(8000);
		}
	}

	public static void verify_TotalAmounts_TotalAmount() {
		String title = TotalAmounts.return_PieChart_Title();

		BaseUI.verify_true_AndLog(title.contains("Total Amount"), "Total Amount label present.",
				"Could not find Total Amount label.");

		String amount = title.replace("Total Amount ", "");
		amount = amount.replace("$", "").replace(",", "").trim();

		TableData tableData = extract_DDASummary_TitleLines();
		BigDecimal tableTotal = tableData.return_Sum_ofColumn("total");
		String str_tableTotal = tableTotal.toString();

		BaseUI.verify_true_AndLog(str_tableTotal.equals(amount), "Total Amounts matched.", MessageFormat.format(
				"DDA Summary Table showing: {0}, Payment Totals Pie Chart showing: {1}", str_tableTotal, amount));

	}

	///
	/// Payment Counts validation and methods.
	///
	///

	public static void check_PaymentCounts_PieChartSlices(TableData tableData) throws Exception {
		for (HashMap<String, String> tableRow : tableData.data) {
			String toolTip = "";

			// Need to add logic for longer entities
			String entityName = tableRow.get("entity").replace("DDA: ", "");
			if (entityName.length() > 15) {
				entityName = format_EntityName_ForLegend(entityName);
				int index = PaymentCounts.return_Index_OfLegendItem(entityName);
				toolTip = PaymentCounts.get_ToolTip_ByIndex(index);
			} else {
				toolTip = PaymentCounts.get_ToolTip_ByText(entityName);
			}

			String toolTipEntity = toolTip.split("\\n")[0];
			String toolTipAmount = toolTip.split("\\n")[1].substring(0, toolTip.split("\\n")[1].indexOf('(')).trim();
			Thread.sleep(100);

			BaseUI.baseStringCompare("Entity", tableRow.get("entity"), toolTipEntity);
			BaseUI.baseStringCompare("Count", tableRow.get("payment_count"), toolTipAmount);
		}
	}

	public static void verify_PaymentCounts_TotalPayments() {
		String title = PaymentCounts.return_PieChart_Title();

		BaseUI.verify_true_AndLog(title.contains("Total Payments"), "Total Payments label present.",
				"Could not find Total Payments label.");

		String amount = title.replace("Total Payments ", "");

		TableData tableData = extract_DDASummary_TitleLines();
		BigDecimal tableTotal = tableData.return_Sum_ofColumn("payment_count");
		String str_tableTotal = tableTotal.toString();

		BaseUI.verify_true_AndLog(str_tableTotal.equals(amount), "Total Payments matched.", MessageFormat.format(
				"DDA Summary Table showing: {0}, Payment Counts Pie Chart showing: {1}", str_tableTotal, amount));

	}

	// unable to see sliced accurately under 0.5. Needed to remove them.
	public static void VerifyDDASummary_Against_TotalPayments_PieChart() throws Exception {
		TableData tableData = extract_DDASummary_TitleLines();
		tableData.remove_Rows_UnderThreshold_ForColumn(5.0, "payment_count");

		TableData tableData_SmallSlices = extract_DDASummary_TitleLines();
		tableData_SmallSlices.remove_Rows_OverThreshold_ForColumn(4.9, "payment_count");
		// Still cannot check the very very small slices.
		tableData_SmallSlices.remove_Rows_UnderThreshold_ForColumn(5.0, "payment_count");

		BaseUI.verify_true_AndLog(tableData.data.size() > 0, "Data size was Greater than 0.", "Data was empty.");

		check_PaymentCounts_PieChartSlices(tableData);

		// uncheck all of the large percentage slices
		for (HashMap<String, String> tableRow : tableData.data) {
			String entityName = tableRow.get("entity");
			entityName = format_EntityName_ForLegend(entityName);

			PaymentCounts.uncheck_legendBox(entityName);
			Thread.sleep(100);
		}
		Thread.sleep(750);

		check_PaymentCounts_PieChartSlices(tableData_SmallSlices);

	}

	public static void verify_TotalCounts_legendkey_Matches_DDASummaryList() {
		TableData tableData = extract_DDASummary_TitleLines();
		ArrayList<String> piechart_LegendKeys = PaymentCounts.list_of_LegendKeys();

		BaseUI.verify_true_AndLog(tableData.data.size() == piechart_LegendKeys.size(),
				"DDA Summaries list matches keys size in Payment Counts pie chart.",
				"DDA Summary List's size did not match the number of keys in Payment Counts pie chart.");

		BaseUI.verify_true_AndLog(tableData.data.size() > 0, "DDA Summary list contained values.",
				"DDA Summary list was empty.");

		for (HashMap<String, String> tableRow : tableData.data) {
			String entity = format_EntityName_ForLegend(tableRow.get("entity"));
			BaseUI.verify_true_AndLog(piechart_LegendKeys.contains(entity),
					MessageFormat.format("Entity {0} was found in Pie Chart legend.", entity),
					MessageFormat.format("Entity {0} was NOT found in Pie Chart legend.", entity));
		}
	}

}
