package r360.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import r360.data.Config_DataRetrieval;
import utils.*;
import wfsCommon.pages.DatePicker;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.FilterExpander;

public class Reports {

	public static String reportCreated_TimeFormat = "MM/dd/yyyy hh:mm a";
	public static String database_DateFormat = "yyyyMMdd";
	public static String userActivityReport_CSVFileName = "User-Activity-Report.CSV";

	public static String receivables360_Online_ReportsText = "Receivables360 Online\u2122 Reports";
	public static Dropdown generateReportAs_dropdown = new Dropdown("s2id_GT");
	public static Dropdown paymentSource_dropdown = new Dropdown("s2id_PS");
	public static Dropdown eventType_dropdown = new Dropdown("reports_EventsType_Dropdown_InitialElement", null, null);
	public static DatePicker startDate_DatePicker = new DatePicker("reports_StartDateDatePicker", null, null);
	public static DatePicker endDate_DatePicker = new DatePicker("reports_EndDateDatePicker", null, null);
	public static FilterExpander userEntityExpanderDropdown = new FilterExpander("filterExpander");

	public static void select_UserActivityReport_Values(String generateReportAs, String startDate, String endDate,
			String user, String eventType) throws Exception {
		if (generateReportAs != null) {
			generateReportAs_dropdown.select_EntityValue(generateReportAs);
		}
		if (startDate != null) {
			startDate_DatePicker.enter_Text(startDate);
		}
		if (endDate != null) {
			endDate_DatePicker.enter_Text(endDate);
		}
		if (user != null) {
			userEntityExpanderDropdown.search_forText(user);
		}
		if (eventType != null) {
			eventType_dropdown.select_EntityValue(eventType);
		}

	}

	public static void select_ImportReconciliationReport_Values(String generateReportAs, String startDate,
			String endDate, String paymentSource) throws Exception {
		if (generateReportAs != null) {
			generateReportAs_dropdown.select_EntityValue(generateReportAs);
		}
		if (startDate != null) {
			startDate_DatePicker.enter_Text(startDate);
		}
		if (endDate != null) {
			endDate_DatePicker.enter_Text(endDate);
		}
		if (paymentSource != null) {
			paymentSource_dropdown.select_EntityValue(paymentSource);
		}

	}

	public static HashMap<String, String> return_UserActivityReport_RowMatchingCriteria(TableData csvTable,
			String entity, String dateRange1, String dateRange2, String user, String usersRequested, String eventType,
			String timeRequested) throws Exception {
		String expectedDateRange = "Date Range: " + dateRange1 + " - " + dateRange2;

		String expectedSelectedUsers = "Selected User: " + usersRequested;
		String expectedSelectedEventType = "Selected Event Type: " + eventType;

		HashMap<String, String> expectedValues = new HashMap<String, String>();
		expectedValues.put("Entity", entity);
		expectedValues.put("DateRange", expectedDateRange);
		expectedValues.put("User", user);
		expectedValues.put("SelectedUsers", expectedSelectedUsers);

		expectedValues.put("SelectedEventType", expectedSelectedEventType);

		HashMap<String, String> returnedRow = csvTable.return_Row_BasedOn_AnyNumberOfMatchingFields(expectedValues);

		return returnedRow;

	}

	// Time that we record might be a couple minutes off from time listed in csv, so
	// I need to verify it falls within a range.
	public static void verify_Row_CreationTime_AndByUser_Valid(HashMap<String, String> rowToCheck, String expectedTime,
			String expectedUser) throws Exception {
		String reportCreated = rowToCheck.get("ReportCreated");
		String timeToGet = reportCreated.substring(15, reportCreated.indexOf(" by"));
		String dateRange1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(expectedTime,
				reportCreated_TimeFormat, reportCreated_TimeFormat, -1);
		String dateRange2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(expectedTime,
				reportCreated_TimeFormat, reportCreated_TimeFormat, 1);
		BaseUI.verify_Date_IsBetween_DateRange(dateRange1, dateRange2, timeToGet, reportCreated_TimeFormat);
		BaseUI.verify_true_AndLog(reportCreated.endsWith("by " + expectedUser),
				"Report Created ends with expected user.",
				"Report Created did not end with expected user.  Seing " + reportCreated);
		// reportCreated_TimeFormat

	}

	// Validates DateRange, SelectedUsers, SelectedEventType, and ReportCreated
	// fields.
	public static void verify_Rows_MatcheExpectedValues(TableData csvTable, String entity, String dateRange1,
			String dateRange2, String user, String usersRequested, String eventType, String timeRequested)
			throws Exception {
		BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found CSV Rows.", "Did NOT find CSV Rows.");

		String expectedDateRange = "Date Range: " + dateRange1 + " - " + dateRange2;
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("DateRange", expectedDateRange, csvTable.data);

		String expectedSelectedUsers = "Selected User: " + usersRequested;
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("SelectedUsers", expectedSelectedUsers, csvTable.data);

		String expectedSelectedEventType = "Selected Event Type: " + eventType;
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("SelectedEventType", expectedSelectedEventType,
				csvTable.data);

		// Make sure the actual time displayed is within an acceptable range of what we
		// wanted then we'll use the time we pull from the csv to compare the
		// ReportCreated field
		String reportCreated_FirstCell = csvTable.data.get(0).get("ReportCreated");
		String timeToGet = reportCreated_FirstCell.substring(15, reportCreated_FirstCell.indexOf(" by"));
		String reportCreated_Range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(timeRequested,
				reportCreated_TimeFormat, reportCreated_TimeFormat, -1);
		String reportCreated_Range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(timeRequested,
				reportCreated_TimeFormat, reportCreated_TimeFormat, 1);
		BaseUI.verify_Date_IsBetween_DateRange(reportCreated_Range1, reportCreated_Range2, timeToGet,
				reportCreated_TimeFormat);

		String expectedReportCreated = "Report created " + timeToGet + " by " + user;
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("ReportCreated", expectedReportCreated, csvTable.data);
	}

	// Has weaker validations since I didn't know which data would be there going
	// in.
	public static void verify_PDF_FirstRow_HasValues(String startDate, String endDate, String entity, String eventType,
			String user) throws Exception {
		String firstRow_AuditDateTime = BaseUI
				.getTextFromField(Locator.lookupElement("reportsPDF_UserActivityReport_FirstRow_AuditDateTime"));
		firstRow_AuditDateTime = BaseUI.return_Date_AsDifferentFormat(firstRow_AuditDateTime, "MM/dd/yy h:mm:ss a",
				"MM/dd/yyyy h:mm:ss a");
		startDate = BaseUI.return_Date_AsDifferentFormat(startDate, "MM/dd/yyyy", "MM/dd/yyyy h:mm:ss a");
		endDate = BaseUI.return_Date_AsDifferentFormat(endDate, "MM/dd/yyyy", "MM/dd/yyyy h:mm:ss a");
		BaseUI.verify_Date_IsBetween_DateRange(startDate, endDate, firstRow_AuditDateTime, "MM/dd/yyyy h:mm:ss a");

		BaseUI.verifyElementHasExpectedText("reportsPDF_UserActivityReport_FirstRow_Entity", entity);

		if (user == null) {
			String userOnPage = BaseUI
					.getTextFromField(Locator.lookupElement("reportsPDF_UserActivityReport_FirstRow_User"));
			BaseUI.verify_String_Not_EmptyOrNull(userOnPage.trim());
		} else {
			String userOnPage = BaseUI
					.getTextFromField(Locator.lookupElement("reportsPDF_UserActivityReport_FirstRow_User"));
			BaseUI.baseStringCompare("User", user, userOnPage);
		}

		if (eventType == null) {
			String eventTypeOnPage = BaseUI
					.getTextFromField(Locator.lookupElement("reportsPDF_UserActivityReport_FirstRow_EventType"));
			BaseUI.verify_String_Not_EmptyOrNull(eventTypeOnPage);
		} else {
			String eventTypeOnPage = BaseUI
					.getTextFromField(Locator.lookupElement("reportsPDF_UserActivityReport_FirstRow_EventType"));
			BaseUI.baseStringCompare("Event Type", eventType, eventTypeOnPage);
		}

		String auditMessageOnPage = BaseUI
				.getTextFromField(Locator.lookupElement("reportsPDF_UserActivityReport_FirstRow_AuditMessage"));
		BaseUI.verify_String_Not_EmptyOrNull(auditMessageOnPage);

	}

	public static void verify_PDF_UserActivityReport_FiltersMatchExpected(String user, String usersRequested,
			String timeRequested, String dateRange1, String dateRange2, String eventType) throws Exception {

		String expectedDateRange = "Date Range: " + dateRange1 + " - " + dateRange2;
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("reportsPDF_UserActivityReport_DateRange"),
				expectedDateRange);

		String expectedSelectedUsers = "Selected User: " + usersRequested;
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("reportsPDF_UserActivityReport_SelectedUser"),
				expectedSelectedUsers);

		String expectedSelectedEventType = "Selected Event Type: " + eventType;
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("reportsPDF_UserActivityReport_SelectedEventType"),
				expectedSelectedEventType);

		// Make sure the actual time displayed is within an acceptable range of what we
		// wanted then we'll use the time we pull from the csv to compare the
		// Report Created field
		WebElement reportCreatedElement = Locator.lookupElement("reportsPDF_UserActivityReport_ReportCreated");
		String reportCreatedText = BaseUI.getTextFromField(reportCreatedElement);
		String timeToGet = reportCreatedText.substring(15, reportCreatedText.indexOf(" by"));
		String reportCreated_Range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(timeRequested,
				reportCreated_TimeFormat, reportCreated_TimeFormat, -1);
		String reportCreated_Range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(timeRequested,
				reportCreated_TimeFormat, reportCreated_TimeFormat, 1);
		BaseUI.verify_Date_IsBetween_DateRange(reportCreated_Range1, reportCreated_Range2, timeToGet,
				reportCreated_TimeFormat);
		String expectedReportCreated = "Report created " + timeToGet + " by " + user;
		BaseUI.baseStringCompare("Report Created", expectedReportCreated, reportCreatedText);
	}

	public static TableData returnCSV_Results(String fileName) throws Exception {
		TableData tableData = new TableData();

		Config_DataRetrieval.retrieve_DataSheet(Browser.downloadLocation + "\\", fileName);

		tableData = Config_DataRetrieval.tableData;
		tableData.sort_ByColumn_Descending("ReportCreated");
		tableData.remove_Character("\"");
		tableData.replace_Character("  ", " ", "AuditDateTime");
		
		return tableData;
	}


	public static void generate_UAR_VerifyEventTypeExist(String report,String rep_username,String eventType,String AuditDateTime,String CSVeventype, String AuditMessage )throws Exception {
		final String previousDate = BaseUI.getDateAsString_InRelationToTodaysDate(-1,"MM/dd/yyyy");
		final String laterDate = BaseUI.getDateAsString_InRelationToTodaysDate(1, "MM/dd/yyyy");
		DataBuilder.delete_Files_WithExtention_FromLocation(".CSV", Browser.downloadLocation);
		String mainHandle = Browser.driver.getWindowHandle();
		Navigation.navigate_Reports();
		click_groupingHeader(Reports.receivables360_Online_ReportsText);
		click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, report);
		select_UserActivityReport_Values("CSV", previousDate, laterDate, rep_username,eventType);
		click_GenerateReport_Button();
		FileOperations.wait_For_File_To_Exist(Browser.downloadLocation + "\\" + Reports.userActivityReport_CSVFileName);
		TableData csvResults = Reports.returnCSV_Results(Reports.userActivityReport_CSVFileName);
		BaseUI.verify_TableColumn_hasValue_MatchString("AuditDateTime", AuditDateTime,"EventType", CSVeventype,"AuditMessage", AuditMessage,csvResults.data);
		//Close extra windows
		BaseUI.close_ExtraWindows(mainHandle);
	}

	public static void verify_Reports_Title_Appears() {
		WebElement title = Locator.lookupElement("reports_Reports_Header");
		BaseUI.verifyElementAppears(title);
		BaseUI.verifyElementHasExpectedText(title, "Reports");
	}

	public static void verify_GroupingAppears_ByGroupingText(String groupingText) {
		WebElement groupingHeader = Locator.lookupElement("reports_GroupingHeader_ByText", groupingText, null);
		BaseUI.verifyElementAppears(groupingHeader);
		BaseUI.verifyElementHasExpectedText(groupingHeader, groupingText);
	}

	public static void verify_Group_ExpandedOrCollapsed(String groupingText, Boolean groupExpanded) {
		String partialTextToMatch = "ui-state-active";
		WebElement groupingHeader = Locator.lookupElement("reports_GroupingHeader_ByText", groupingText, null);
		if (groupExpanded) {
			BaseUI.verifyElementHasExpectedPartialAttributeValue(groupingHeader, "class", partialTextToMatch);
		} else {
			BaseUI.verifyElementDoesNotHavePartialAttributeValue(groupingHeader, "class", partialTextToMatch);
		}
	}

	public static void click_GenerateReport_Button() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("reports_GenerateReportButton"), false, 
				()->{
					try {
						Navigation.wait_for_LoadingSpinner_ToAppear();
						Navigation.wait_for_LoadingSpinner_ToDisappear();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void verify_PDF_Title_MatchesExpected(String expectedTitle) {
		WebElement pdfTitle = Locator.lookupElement("reportsPDF_Title");
		BaseUI.verifyElementAppears(pdfTitle);
		BaseUI.verifyElementHasExpectedText(pdfTitle, expectedTitle);
	}

	public static void click_groupingHeader(String groupingText) throws Exception {
		BaseUI.click(Locator.lookupElement("reports_GroupingHeader_ByText", groupingText, null));
		BaseUI.waitForElementToContain_PartialAttributeMatch("reports_GroupingHeader_ByText", groupingText, null,
				"class", "ui-state-active");
		Thread.sleep(200);
	}

	public static void expand_groupingHeader(String groupingText) throws Exception {
		WebElement groupHeaderElement = Locator.lookupElement("reports_GroupingHeader_ByText", groupingText, null);
		String groupHeaderClass = BaseUI.get_Attribute_FromField(groupHeaderElement, "class");
		if (groupHeaderClass.contains("ui-state-active")) {
			click_groupingHeader(groupingText);
		}

	}

	public static void click_ReportHeader_ByGroupingText_And_ByReportText(String groupingText, String reportText)
			throws Exception {
		BaseUI.click(Locator.lookupElement("reports_Grouping_Link_ByGroupingHeaderText_AndReportText", groupingText,
				reportText));
		Thread.sleep(5000);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.wait_for_PageSource_ToNoLongerContainText("Loading...", 10);
	}

	public static void verify_Grouping_Displayed_Matches_expected(String groupingText, String[] expectedEntries) {
		ArrayList<WebElement> entryList = Locator
				.lookup_multipleElements("reports_Grouping_EntryList_ByGroupingHeaderText", groupingText, null);

		BaseUI.verify_true_AndLog(entryList.size() == expectedEntries.length, "Number of Reports accurate.",
				"Number of reports didn't match expected number.");

		for (int i = 0; i < expectedEntries.length; i++) {
			BaseUI.verifyElementAppears(entryList.get(i));
			BaseUI.verifyElementHasExpectedText(entryList.get(i), expectedEntries[i]);
		}
	}

	// Use date format database_DateFormat
	public static TableData return_auditInfo_ForDates(String date1, String date2, String eventType) throws Exception {
		TableData table = new TableData();
		DatabaseConnection.dbUrl = GlobalVariables.environment.DBConnection;

		String query = "SELECT \r\n" + " d.CreationDate\r\n" + "  ,e.LogonName as 'User'\r\n" + "  ,b.EventType\r\n"
				+ "  ,d.AuditMessage\r\n" + " FROM [RecHubAudit].[factEventAuditSummary] a\r\n"
				+ "  inner  join RecHubAudit.dimAuditEvents b on b.AuditEventKey  = a.AuditEventKey\r\n"
				+ "  inner  join RecHubAudit.factEventAuditMessages d on a.AuditKey  = d.AuditKey\r\n"
				+ "  inner  join [RecHubUser].[Users] e on a.UserID  = e.UserID\r\n"
				+ " where a.IsDeleted  = 0 and a.AuditDateKey  >= '{0}' and a.AuditDateKey  <= '{1}' and b.EventType = '{2}'"
				+ " order by b.EventType , d.creationdate";

		query = query.replace("'", "''");
		query = MessageFormat.format(query, date1, date2, eventType);

		table = DatabaseConnection.runSQLServerQuery(query);
		table.remove_Rows_WithColumnName_WhereValues_Match("User", "Undefined");
		table.remove_Character("\"");
		table.replace_DateFormats("CreationDate", "MM-dd-yyyy-HH-mm-ss", "MM/dd/yy h:mm:ss a");

		TableData tableFormatted = new TableData();
		tableFormatted.data.add(table.data.get(0));

		for (int i = 1; i < table.data.size(); i++) {
			String creationDateTimePriorLine = table.data.get(i - 1).get("CreationDate");
			String creationDateTimeCurrentLine = table.data.get(i).get("CreationDate");

			String userPriorLine = table.data.get(i - 1).get("User");
			String userCurrentLine = table.data.get(i).get("User");

			if (creationDateTimePriorLine.equals(creationDateTimeCurrentLine) && userPriorLine.equals(userCurrentLine)) {
				tableFormatted.data.get(tableFormatted.data.size() - 1).put("AuditMessage",
						table.data.get(i - 1).get("AuditMessage") + table.data.get(i).get("AuditMessage"));
				BaseUI.log_Status("Filtered out line " + table.data.get(i));
			} else {
				tableFormatted.data.add(table.data.get(i));
			}

		}

		return tableFormatted;
	}

	public static void getToPage() throws Exception {
		getToPage(Browser.selectProperBrowser());
	}

	public static void getToPage(String browserToUse) throws Exception {
		Browser.openBrowser(GlobalVariables.environment.BaseURL,  browserToUse);

		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Reports();
	}

	public static HashMap <Integer, List<String>> extractDataFromListtoHashmap(Iterator<WebElement> itr2, String auditTimeColumn, String eventTypeColumn, String auditMessageColumn){
		WebElement test;
		String fieldText;
		boolean  auditTimeFound = false, eventTypeFound = false, auditMessageTitleFound = false;
		HashMap <Integer, List<String>> selectedData = new HashMap<>();
		List<String> extractedData= new ArrayList<String>();
		int count = 1;
		while(itr2.hasNext()) {
			try {
				test = itr2.next();
				fieldText = test.getText();

				if ((test.getAttribute("style").substring(0, 15)).equals(auditTimeColumn)  && (auditMessageTitleFound)) {
					extractedData.add(fieldText);
					auditTimeFound = true;
				} else if ((test.getAttribute("style").substring(0, 15)).equals(eventTypeColumn) && (auditMessageTitleFound)) {
					extractedData.add(fieldText);
					eventTypeFound = true;
				} else if ((test.getAttribute("style").substring(0, 15)).equals(auditMessageColumn) && (auditMessageTitleFound) && (eventTypeFound) && (auditTimeFound)) {
					extractedData.add(fieldText);
					auditTimeFound = false;
					eventTypeFound = false;
					selectedData.put(count, extractedData);
					count++;
					extractedData = new ArrayList<String>();
				} else {
					//BaseUI.log_Status("----------------");//("****Condition didnt match*****");: " + test.getAttribute("style").substring(0, 15) + " " + test.getText());
				}

				if (fieldText.equals("Audit Message")){
					auditMessageTitleFound = true;
				}else {}
			}catch(NullPointerException e1){
				BaseUI.log_Status("NULL Pointer Exception!!" + e1.getMessage());
			}catch(Exception e2){
				BaseUI.log_Status(e2.getMessage());
			}
		}
		return selectedData;
	}


	public static boolean verifyEventEntriesInUserActivityReport(HashMap <Integer, List<String>> selectedData){
		List<String> auditEntries;
		Boolean foundDashboard = false, foundAlertManager = false, foundReports = false, foundBatchSummary =  false, error = false;
		//Iterate through the hash map to identify if the Page View events are listed in the report.
		BaseUI.log_Status("error = " + error);
		for(HashMap.Entry<Integer, List<String>> entry : selectedData.entrySet()){
			BaseUI.log_Status(entry.getKey() + ":" + entry.getValue());
			auditEntries = entry.getValue();
			if (auditEntries.get(1).equals("Viewed Page")){
				if(auditEntries.get(2).contains("Dashboard")) {
					foundDashboard = true;
					foundAlertManager = false; foundReports = false; foundBatchSummary =  false;
					BaseUI.log_Status ("found Viewed Page - Dashboard");
				} else if (auditEntries.get(2).contains("BatchSummary")) {
					foundBatchSummary = true;
					foundDashboard = false; foundAlertManager = false; foundReports = false;
					BaseUI.log_Status ("found Viewed Page - BatchSummary");
				} else if (auditEntries.get(2).contains("Reports")) {
					foundReports = true;
					foundBatchSummary = false; foundDashboard = false; foundAlertManager = false;
					BaseUI.log_Status ("found Viewed Page - Reports");
				}else if (auditEntries.get(2).contains("AlertManager")) {
					foundAlertManager = true; foundReports = false; foundBatchSummary = false; foundDashboard = false;
					BaseUI.log_Status("found Viewed Page - AlertManager");
				}else{
					foundAlertManager = false; foundReports = false; foundBatchSummary = false; foundDashboard = false;
					BaseUI.log_Status("found Viewed Page for event other than Dashboard, Reports, Alert Manager or Batch Summary");
				}
			}
			else{// check if additional events related to a page view event are listed in the report.
				if(foundDashboard && auditEntries.get(1).equals("Submitted / Retrieved Data") && auditEntries.get(2).contains("ReceivablesSummary")){
					BaseUI.log_Status("Verified additional events associated with page load - Dashboard");
				}else if (foundBatchSummary && (auditEntries.get(1).equals("Submitted / Retrieved Data") || auditEntries.get(1).equals("Viewed Data")) && auditEntries.get(2).contains("BatchSummary")) {
					BaseUI.log_Status("Verified additional events associated with page load - Batch Summary");
				}else if (foundReports && (auditEntries.get(1).equals("Submitted / Retrieved Data") || auditEntries.get(1).equals("Viewed Data")) && auditEntries.get(2).contains("ReceivablesSummary")) {
					BaseUI.log_Status("Verified additional events associated with page load - Reports");
				}else if (foundAlertManager && auditEntries.get(1).equals("Submitted / Retrieved Data") && auditEntries.get(2).contains("Alerts")){
					BaseUI.log_Status("Verified additional events associated with page load - Alert Manager");
				}else if (!foundAlertManager && !foundBatchSummary && !foundReports && !foundDashboard){
					BaseUI.log_Status("found event type other than focus of the test.");
				}
				else {
					error = true;
				}
			}
			BaseUI.log_Status("error = " + error);
		}
		BaseUI.log_Status("error = " + error);
		return error;
	}

	public static void generateAndNavigateToPDFReport(String startDate, String endDate, String eventType, String report) throws Exception {
		Reports.click_ReportHeader_ByGroupingText_And_ByReportText(Reports.receivables360_Online_ReportsText, report);
		Reports.select_UserActivityReport_Values("PDF", startDate, endDate, null, eventType);
		Reports.click_GenerateReport_Button();
		BaseUI.waitForElementToBeDisplayed("reportsPDF_Title", "", "", 5);
	}

	@NotNull
	public static WebElement getSecondLastPage(int totalPages1) throws Exception {
		String pageID;
		for (int i = 0; i < (totalPages1 - 2); i++) {//second last page of the report
			Browser.driver.findElement(By.id("next")).click();
			Navigation.wait_for_LoadingSpinner_ToDisappear();
		}
		pageID = "pageContainer" + (totalPages1 - 1);
		WebElement secondLastPage = Browser.driver.findElement(By.id(pageID));

		while (!((secondLastPage.getAttribute("data-loaded")).equals("true"))) {
			BaseUI.log_Status("Current Page: " + secondLastPage.getAttribute("data-page-number") + " Still loading!");
		}
		BaseUI.log_Status("Current Page: " + secondLastPage.getAttribute("data-page-number"));
		return secondLastPage;
	}
}// End of Class
