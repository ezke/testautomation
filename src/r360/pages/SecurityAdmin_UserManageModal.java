package r360.pages;

import r360.data.UserData;
import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.Pagination;

public class SecurityAdmin_UserManageModal {
	public static Pagination availableGroupsGrid = new Pagination("userPageEntityGroupsGrid");
	public static Pagination associatedGroupsGrid = new Pagination("userPageUserGroupsGrid");
	public static Pagination userManagementGrid = new Pagination ("userGrid");
	
	public static void wait_for_modal_to_be_ready() throws Exception
	{
		//waits for spinner to no longer exist.
		for(int i = 0; i < 5; i++)
		{
			if(!BaseUI.elementExists("Global_LoadingSpinner", null, null))
			{
				break;
			}
			Thread.sleep(1000);	
		}
		Thread.sleep(200);
	}
	
	public static void navigate_to_Group_ManagementTab() throws Exception
	{
//		Navigation.wait_for_LoadingSpinner_ToDisappear();
//		Thread.sleep(500);
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityUserEditGroupManage_TabLink", null, null));
		Thread.sleep(2000);
	}
	
	public static void navigate_to_User_InformationTab() throws Exception
	{
		BaseUI.click(Locator.lookupElement("entityUserEditUserInfo_TabLink"));
		Thread.sleep(2000);
	}
	
	public static void move_selected_FromAvailableGrid() throws Exception
	{
		BaseUI.click(Locator.lookupElement("entityUserEditGroup_MoveSelectedButton"));
		Thread.sleep(500);
	}
	
	public static void remove_selected_FromAssociatedGrid() throws Exception
	{
		BaseUI.click(Locator.lookupElement("entityUserEditGroup_RemoveSelectedButton"));
		Thread.sleep(500);
	}
	
	public static void click_cancelButton() throws Exception
	{
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityUserEdit_CancelButton", null, null));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void click_saveButton() throws Exception
	{
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityUserEdit_SaveButton", null, null));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	public static void clickAddGroupButton() throws Exception{
		BaseUI.click(Locator.lookupElement("securityAddGroupButton"));
		Thread.sleep(2000);
	}
	
	public static void enterGroupNameText(String valueToSet) throws Exception{
		BaseUI.enterText(Locator.lookupElement("securityGroupNameText"), valueToSet);
		Thread.sleep(2000);
	}
	public static void tabThroughGroupNameText() throws Exception{
		BaseUI.tabThroughField(Locator.lookupElement("securityGroupNameText"));
	}
	
	public static void navigateToGroupsTab() throws Exception{
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityAdmin_GroupsTab", null, null));
		Thread.sleep(2000);
	}
	public static void clickGroupSavebutton() throws Exception{
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityGroupSaveButton", null, null));
		Thread.sleep(2000);
	}
	
	public static void clickGroupDeletebutton() throws Exception{
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityGroupDeleteButton", null, null));
		Thread.sleep(2000);
	}
	public static void clickGroupConfirmDeletebutton() throws Exception{
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityGroupConfirmDeleteButton", null, null));
		Thread.sleep(2000);
	}

	public static void addUser(UserData user) throws Exception {
		addUser(user.getLoginName(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getEmail());
	}
	
	public static void addUser(String userName, String userPassword, String firstName, String lastName, String email) throws Exception
	{
		BaseUI.enterText(Locator.lookupElement("entityUserInfoLoginName"), userName);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoFirstName"), firstName);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoLastName"), lastName);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoEmail"), email);
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoEmail"));
		Thread.sleep(500);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoPasswordTextbox"), userPassword);
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoPasswordTextbox"));
		BaseUI.enterText(Locator.lookupElement("entityUserInfoConfirmPasswordTextbox"), userPassword);
		//BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoConfirmPasswordTextbox"));
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoLoginName"));
	
		click_saveButton();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	public static void enterUsersForErrorValidation(String loginName, String firstName, String lastName, String email) throws Exception{
		BaseUI.enterText(Locator.lookupElement("entityUserInfoLoginName"), loginName);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoFirstName"), firstName);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoLastName"), lastName);
		BaseUI.enterText(Locator.lookupElement("entityUserInfoEmail"), email);
		BaseUI.tabThroughField(Locator.lookupElement("entityUserInfoEmail"));
		Thread.sleep(1000);
	}
	
	public static void click_EditButton_ByColumnName_andColumnText(String columnName, String columnText) throws Exception{
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText(columnName, columnText));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void findByLoginName(String loginName) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("admin_Users_Filter_LoginName"), loginName);
	}
}
