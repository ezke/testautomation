package r360.pages;

import java.util.ArrayList;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import wfsCommon.pages.EntitySelector;
import wfsCommon.pages.Pagination;

public class SecurityAdministration {

	public static final String roleGridID = "roleGrid";
	public static Pagination roleGrid = new Pagination(roleGridID);
	public static Pagination groupsGrid = new Pagination("groupGrid");
	public static Pagination userManagementGrid = new Pagination("userGrid");

	public static void expandNode(String nodeToExpand) throws Exception {
		// get the Node Arrow element by the passed in text.
		WebElement nodeArrow = Locator.lookupElement("entityTreeNode_Arrow_ByText", nodeToExpand, null);
		if (nodeArrow != null) {
			String nodeArrowState = nodeArrow.getAttribute("class");

			// if node is collapsed
			if (nodeArrowState.contains("jqx-tree-item-arrow-collapse")) {
				BaseUI.click_js(Locator.lookupElement("entityTreeNode_Arrow_ByText", nodeToExpand, null));
				Navigation.wait_for_LoadingSpinner_ToAppearAndDisappear(10);
			}

		}
	}

	public static void CreateUser_GivenEntity(String entityName, String userName, String userPassword, String firstName,
			String lastName, String email) throws Exception {
		selectNode(entityName);
		navigate_toUserTab();
		launchAddUserModal();
		SecurityAdmin_UserManageModal.addUser(userName, userPassword, firstName, lastName, email);
	}

	public static void selectNode(String nodeToExpand) throws Exception {
		Thread.sleep(1000);
		BaseUI.scroll_to_element(Locator.lookupElement("entityTreeNode_ByText", nodeToExpand, null));
		WebElement node = Locator.lookupElement("entityTreeNode_ByText", nodeToExpand, null);
		BaseUI.click_js(node);
		Thread.sleep(2200);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Starts on Administration page on the Groups tab and checks that there are
	// Users before launching Groups Modal.
	public static void remove_AllUsers_FromGivenGroup(String groupName) throws Exception {
		String numberUsersInGroup = groupsGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Users", groupName);

		if (!BaseUI.stringEmpty(numberUsersInGroup)) {
			launchEditGroupModal_forGivenGroup(groupName);
			SecurityAdmin_GroupsManagementModal.remove_AllUsers();
		}
	}

	// Starts on Administration page on the Groups tab and checks that there are
	// Roles before launching Groups Modal.
	public static void remove_AllRoles_FromGivenGroup(String groupName) throws Exception {
		String numberRolesInGroup = groupsGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Roles", groupName);

		if (!BaseUI.stringEmpty(numberRolesInGroup)) {
			launchEditGroupModal_forGivenGroup(groupName);
			SecurityAdmin_GroupsManagementModal.remove_AllRoles();
		}
	}

	// Starts on Administration page on the Roles tab and checks that there are
	// groups before launching Roles Modal.
	public static void remove_AllGroups_FromGivenRole(String roleName) throws Exception {
		roleGrid.filter_textbox_SendKeys("Name", roleName);

		String numberGroupsInRole = roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Groups", roleName);

		if (!BaseUI.stringEmpty(numberGroupsInRole)) {
			launchEditRoleModal_forGivenRole(roleName);
			SecurityAdmin_RolesManagementModal.remove_AllGroups();
		}

		roleGrid.filter_textbox_ClearFilter("Name");
	}

	// Starts on Administration page on the Roles tab and checks that there are
	// policies before launching Roles Modal.
	public static void remove_AllPolicies_FromGivenRole(String roleName) throws Exception {
		roleGrid.filter_textbox_SendKeys("Name", roleName);

		String numberPoliciesInRole = roleGrid.get_CellText_ByHeaderName_AndRowIdentifyingText("# of Policies",
				roleName);

		if (!BaseUI.stringEmpty(numberPoliciesInRole)) {
			launchEditRoleModal_forGivenRole(roleName);
			SecurityAdmin_RolesManagementModal.remove_AllPolicies();
		}

		roleGrid.filter_textbox_ClearFilter("Name");
	}

	public static void launchAddUserModal() throws Exception {
		BaseUI.click(Locator.lookupElement("entityUserAddButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launchAddEntityModal() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityEntities_AddButton", null, null));
		// Thread.sleep(1500);
		BaseUI.waitForElementToBeDisplayed("securityAdmin_GenericModal", null, null, 20);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launchAddRoleModal() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_Roles_AddButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launchAddGroupsModal() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_Groups_AddButton"));
		BaseUI.waitForElementToBeDisplayed("securityAdmin_GenericModal", null, null, 20);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		// if (!Locator.lookupElement("grpsManage_Modal").isDisplayed()) {
		// BaseUI.verify_true_AndLog(false, "", "Groups Management Modal Never
		// appeared.");
		// }
	}

	public static void launchEditUserModal_forGivenUser(String userName) throws Exception {
		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", userName));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launchEditRoleModal_forGivenRole(String roleName) throws Exception {

		BaseUI.click(roleGrid.editButton_ByColumnName_andColumnText("Name", roleName));
		Thread.sleep(750);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launchEditGroupModal_forGivenGroup(String groupName) throws Exception {
		BaseUI.click(groupsGrid.editButton_ByColumnName_andColumnText("Name", groupName));
		Thread.sleep(750);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launchEditEntityModal_forGivenEntity(String entityName) throws Exception {
		SecurityAdministration.selectNode(entityName);
		BaseUI.click_js(Locator.lookupElement("securityEntities_EditButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Be VERY careful which entities you delete.
	public static void deleteEntity_LaunchDeleteModal(String nameOfEntity) throws Exception {
		// BaseUI.click_js(Locator.lookupElement("entityTreeNode_ByText",
		// nameOfEntity, null));
		// Thread.sleep(1000);

		EntitySelector.selectNode(nameOfEntity);

		if (!Locator.lookupElement("securityEntities_DeleteButton").isDisplayed()) {
			navigate_toEntityTab();
		}
		BaseUI.click_js(Locator.lookupElement("securityEntities_DeleteButton"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void launch_DeleteRoleModal(String roleToDelete) throws Exception {
		BaseUI.checkCheckbox(roleGrid.check_box_for_GivenColumnAndText("Name", roleToDelete));
		click_DeleteRolesButton();
	}

	public static void click_DeleteRoleModal_CancelButton() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_Roles_DeleteModal_CancelButton"));
		Thread.sleep(2000);

	}

	public static void verify_UnableToDelete_Role(String roleToDelete) throws Exception {
		launch_DeleteRoleModal(roleToDelete);
		BaseUI.verifyElementAppears(Locator.lookupElement("scrtyAdmin_Roles_DeleteModal_UnableToDelete_SubHeader"));
		BaseUI.verifyElementDoesNotAppearByString("scrtyAdmin_Roles_DeleteModal_AbleToDelete_SubHeader");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("scrtyAdmin_Roles_DeleteModal_roleName_CannotDelete"),
				roleToDelete);
		BaseUI.verifyElementDisabled(Locator.lookupElement("scrtyAdmin_Roles_DeleteModal_DeleteButton"));
	}

	// Be VERY careful which entities you delete.
	public static void deleteEntity(String nameOfEntity) throws Exception {
		if (!Locator.lookupElement("securityEntities_DeleteButton").isDisplayed()) {
			navigate_toEntityTab();
		}

		selectNode(nameOfEntity);
		// BaseUI.click_js(Locator.lookupElement("entityTreeNode_ByText",
		// nameOfEntity, null));
		// Thread.sleep(1000);
		// Navigation.wait_for_LoadingSpinner_ToDisappear();

		BaseUI.click(BaseUI.waitForElementToBeClickable("securityEntities_DeleteButton", null, null));
		Thread.sleep(1500);
		BaseUI.click(Locator.lookupElement("securityEntitiesDeleteModal_DeleteButton"));
		Thread.sleep(2500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void cancel_deleteEntity() throws Exception {
		BaseUI.click(Locator.lookupElement("securityEntitiesDeleteModal_CancelButton"));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void deleteEntity_withGroups(String nameOfEntity) throws Exception {
		SecurityAdministration.navigate_toGroupsTab();
		SecurityAdministration.selectNode(nameOfEntity);
		groupsGrid.show_All();
		groupsGrid.check_all_checkbox();

		deleteGroups();

		SecurityAdministration.navigate_toEntityTab();
		SecurityAdministration.selectNode(nameOfEntity);
		deleteEntity(nameOfEntity);

	}

	// Be VERY careful which entities you delete.
	public static void deleteEntity_withRoles(String nameOfEntity) throws Exception {
		SecurityAdministration.navigate_toRolesTab();
		SecurityAdministration.selectNode(nameOfEntity);
		roleGrid.show_All();
		Thread.sleep(500);
		roleGrid.check_all_checkbox();
		Thread.sleep(200);

		deleteRoles();

		// if(BaseUI.pageSourceContainsString("Getting the role summaries
		// failed."))
		// {
		// ArrayList<WebElement> errorModal_Boxes_CloseButtons =
		// Locator.lookup_multipleElements("error_CloseButton", null, null);
		// for(int i = 0; i < errorModal_Boxes_CloseButtons.size(); i++)
		// {
		// BaseUI.click(Locator.lookupElement("error_CloseButton"));
		// Thread.sleep(500);
		// }
		//
		// deleteRoles();
		// }

		Thread.sleep(1500);
		if (BaseUI.pageSourceContainsString("Getting the role summaries failed")) {
			BaseUI.click(BaseUI.waitForElementToBeClickable("error_CloseButton", null, null));
			Thread.sleep(500);
		}

		Thread.sleep(1000);
		SecurityAdministration.navigate_toEntityTab();
		SecurityAdministration.selectNode(nameOfEntity);
		deleteEntity(nameOfEntity);
	}

	public static void deleteGroups() throws Exception {
		WebElement rolesDeleteButton = Locator.lookupElement("scrtyAdmin_Groups_DeleteButton");
		if (rolesDeleteButton.isEnabled()) {
			click_DeleteGroupsButton();

			BaseUI.click(BaseUI.waitForElementToBeClickable("scrtyAdmin_Groups_DeleteModal_DeleteButton", null, null));
			Navigation.wait_for_LoadingSpinner_ToAppear();
			Navigation.wait_for_LoadingSpinner_ToDisappear();

			if (BaseUI.elementExists("error_CloseButton", null, null)
					&& Locator.lookupElement("error_CloseButton").isDisplayed()) {
				BaseUI.click(Locator.lookupElement("error_CloseButton"));
				Thread.sleep(500);
			}
			if (Browser.currentBrowser.equals("internetexplorer")) {
				Thread.sleep(1000);
			}
		}
	}

	public static void deleteGroups_ForGivenEntity(String nameOfEntity) throws Exception {
		selectNode(nameOfEntity);
		navigate_toGroupsTab();
		Pagination groupGrid = new Pagination("groupGrid");
		if (groupGrid.columns_Elements_List("Name").size() > 0) {
			groupGrid.check_all_checkbox();
			BaseUI.click(Locator.lookupElement("securityGroupDeleteButton"));
			Thread.sleep(500);
			BaseUI.click(BaseUI.waitForElementToBeClickable("securityGroupConfirmDeleteButton", null, null));
			Thread.sleep(4000);
			Navigation.wait_for_LoadingSpinner_ToDisappear();
		}
	}

	public static void deleteGroup_ForGivenGroupName(String groupName) throws Exception {
		WebElement checkbox = groupsGrid.check_box_for_GivenColumnAndText("Name", groupName);
		BaseUI.checkCheckbox(checkbox);
		Thread.sleep(100);
		deleteGroups();
	}

	public static void deleteRoles() throws Exception {
		WebElement rolesDeleteButton = Locator.lookupElement("scrtyAdmin_Roles_DeleteButton");
		if (rolesDeleteButton.isEnabled()) {
			click_DeleteRolesButton();

			BaseUI.click(BaseUI.waitForElementToBeClickable("scrtyAdmin_Roles_DeleteModal_DeleteButton", null, null));
			// Thread.sleep(1200);
			Navigation.wait_for_LoadingSpinner_ToAppear(2);
			Navigation.wait_for_LoadingSpinner_ToDisappear();
		}
	}

	public static void deleteRole_ForGivenRoleName(String roleName) throws Exception {
		roleGrid.filter_textbox_SendKeys("Name", roleName);

		WebElement checkbox = roleGrid.check_box_for_GivenColumnAndText("Name", roleName);
		BaseUI.checkCheckbox(checkbox);
		Thread.sleep(100);
		deleteRoles();

		roleGrid.filter_textbox_ClearFilter("Name");
	}

	public static void deleteRole_ForGivenRoleName_WithGroupsAndPolicies(String roleName) throws Exception {
		SecurityAdministration.remove_AllGroups_FromGivenRole(roleName);
		SecurityAdministration.remove_AllPolicies_FromGivenRole(roleName);

		WebElement checkbox = roleGrid.check_box_for_GivenColumnAndText("Name", roleName);
		BaseUI.checkCheckbox(checkbox);
		Thread.sleep(100);
		deleteRoles();
	}

	public static void cancel_DeleteRolesModal() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_Roles_DeleteModal_CancelButton"));
		Thread.sleep(500);
	}

	public static void cancel_DeleteGroupsModal() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_Groups_DeleteModal_CancelButton"));
		Thread.sleep(500);
	}

	public static void click_DeleteRolesButton() throws Exception {
		WebElement rolesDeleteButton = Locator.lookupElement("scrtyAdmin_Roles_DeleteButton");
		BaseUI.click(rolesDeleteButton);
		Thread.sleep(500);
	}

	public static void click_DeleteGroupsButton() throws Exception {
		WebElement rolesDeleteButton = Locator.lookupElement("scrtyAdmin_Groups_DeleteButton");
		BaseUI.click(rolesDeleteButton);
		Thread.sleep(500);
	}

	// Be VERY careful which entities you delete.
	public static void deleteEntity_WithChildEntities(String nameOfEntity) throws Exception {
		SecurityAdministration.expandNode(nameOfEntity);

		ArrayList<WebElement> childNodes = new ArrayList<WebElement>();
		childNodes.addAll(Locator.lookup_multipleElements("entityTreeNode_ByText_ChildNodes", nameOfEntity, null));

		for (int i = 0; i < childNodes.size(); i++) {
			SecurityAdministration.expandNode(nameOfEntity);
			WebElement childNode = Locator.lookupElement("entityTreeNode_ByText_ChildNodes", nameOfEntity, null);
			SecurityAdministration.deleteEntity(childNode.getText());
		}

		SecurityAdministration.selectNode(nameOfEntity);
		SecurityAdministration.deleteEntity(nameOfEntity);
	}

	public static void deleteUsers_ForGivenEntity(String nameOfEntity) throws Exception {
		selectNode(nameOfEntity);
		navigate_toUserTab();
		Pagination userGrid = new Pagination("userGrid");
		if (userGrid.columns_Elements_List("Login Name").size() > 0) {
			userGrid.check_all_checkbox();
			BaseUI.click(Locator.lookupElement("entityUserDeleteButton"));
			Thread.sleep(500);
			BaseUI.click(Locator.lookupElement("securityUserDeleteModal_DeleteButton"));
			Thread.sleep(4000);
		}
	}

	public static void deleteUser(String columnName, String columnText) throws Exception {

		Pagination userManagementGrid = new Pagination("userGrid");
		userManagementGrid.check_box_for_GivenColumnAndText(columnName, columnText);
		WebElement element = userManagementGrid.check_box_for_GivenColumnAndText(columnName, columnText);
		BaseUI.click(element);
		Thread.sleep(3000);
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserDeleteButton"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("entityUserDeleteButton"));
		Thread.sleep(2000);
		SecurityAdminAddUser.clickDeleteButton();
		Thread.sleep(1000);
		SecurityAdminAddUser.clickConfirmDeleteButton();
		// Thread.sleep(1000);
		BaseUI.waitForElementToNOTBeDisplayed("securityUsers_UserGridElement_ByText", columnText, null, 20);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void navigate_toUserTab() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityAdmin_UsersTab", null, null));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void navigate_toEntityTab() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityAdmin_EntitiesTab", null, null));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void navigate_toGroupsTab() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityAdmin_GroupsTab", null, null));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void navigate_toRolesTab() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("securityAdmin_RolesTab", null, null));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// public static void deleteModal_TableText_ByPassedInHeader(String
	// headerText) {
	//
	// }

	public static void deleteEntity_With_Workgroups(String testEntity, String entityToExpand) throws Exception {
		if (BaseUI.elementExists("entityTreeNode_ByText", testEntity, null)) {
			Navigation.navigate_Admin_Workgroups();
			if (entityToExpand != null) {
				EntitySelector.expandNode(entityToExpand);
			}
			EntitySelector.selectNode(testEntity);
			Admin_Workgroups.remove_AllWorkgroupsFromEntity();

			Navigation.navigate_Admin_Entities();

			if (entityToExpand != null) {
				EntitySelector.expandNode(entityToExpand);
			}
			SecurityAdministration.deleteEntity(testEntity);
		}
	}
}
