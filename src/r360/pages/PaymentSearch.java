package r360.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import r360.data.ImportData;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.FilterExpander;
import wfsCommon.pages.TextBox;

public class PaymentSearch {

	public static FilterExpander workGroupSelector = new FilterExpander("filterExpander");
	public static Dropdown paymentTypeDropdown = new Dropdown("s2id_paymentType");
	public static Dropdown paymentSourceDropdown = new Dropdown("s2id_paymentSource");
	public static Dropdown sortByDropdown = new Dropdown("s2id_payment-sort");

	public static void enter_Date(WebElement dateField, String date) {
		BaseUI.click(dateField);
		dateField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		dateField.sendKeys(date);
	}

	public static void setDateRange(String dateFrom, String dateTo) {
		enter_Date(Locator.lookupElement("paymentSearch_fromDate"), dateFrom);
		enter_Date(Locator.lookupElement("paymentSearch_toDate"), dateTo);
	}

	public static void click_Search() throws Exception {
		BaseUI.click(Locator.lookupElement("paymentSearch_SearchButton"));
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_AddCriteria() throws Exception {
		BaseUI.click(Locator.lookupElement("paymentSearch_AddSearchCriteria_Button"));
		Thread.sleep(300);
	}

	// Row index starts at 1.
	public static void add_Search_Criteria(Integer rowIndex, String field, String operator, String value)
			throws Exception {
		click_AddCriteria();

		Dropdown fieldDropdown = new Dropdown("paymentSearch_QueryBuilder_Field_ByRowNumber", rowIndex.toString(),
				null);
		Dropdown operatorDropdown = new Dropdown("paymentSearch_QueryBuilder_Operator_ByRowNumber", rowIndex.toString(),
				null);
		TextBox valueTextBox = new TextBox("paymentSearch_QueryBuilder_Value_ByRowNumber", rowIndex.toString(), null);

		if (field != null) {
			fieldDropdown.select_EntityValue(field);
		}
		if (operator != null) {
			operatorDropdown.select_EntityValue(operator);
		}
		if (value != null) {
			valueTextBox.enterTextIntoField_AndTab(value);
		}

	}

	public static void click_ClearSearchButton() throws Exception {
		BaseUI.click(Locator.lookupElement("paymentSearch_ClearSearchButton"));
		Thread.sleep(1500);
	}

	public static void click_PaymentSearch_Breadcrumb() throws Exception {
		BaseUI.click(Locator.lookupElement("paymentSearch_Results_BreadcrumbHeaderLink_PaymentSearch"));
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		Thread.sleep(2000);
	}

	public static void verify_Breadcrumb(String[] crumbArray) {

		// ArrayList<WebElement> breadcrumbElements=
		// Locator.lookup_multipleElements("advancedSearch_Breadcrumb_List",
		// null, null);
		String breadCrumb = BaseUI.getTextFromField(Locator.lookupElement("paymentSearch_Breadcrumb_CurrentPage"));
		String[] breadCrumbSplit = breadCrumb.split(">");

		BaseUI.verify_true_AndLog(crumbArray.length == breadCrumbSplit.length, "Bread Crumb had expected length.",
				"Bread Crumb length did NOT match.");

		for (int i = 0; i < crumbArray.length; i++) {
			// String elementText =
			// BaseUI.getTextFromField(breadcrumbElements.get(i));
			BaseUI.baseStringCompare("Breadcrumb Text", crumbArray[i], breadCrumbSplit[i].trim());
		}

		// BaseUI.baseStringCompare("Current Page in Breadcrumb",
		// crumbArray[crumbArray.length], currentPage);

	}

	public static void click_SearchResults_Breadcrumb() throws Exception {
		BaseUI.click(Locator.lookupElement("paymentSearch_Results_BreadcrumbHeaderLink_SearchResults"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Verify Default Values on Payment Search page.
	public static void verify_DefaultValues_forPaymentSearch() throws Exception {

		String workgroupSelectorValue = workGroupSelector.return_SelectedText();

		String date1 = BaseUI.getTextFromInputBox(Locator.lookupElement("paymentSearch_fromDate"));
		String date2 = BaseUI.getTextFromInputBox(Locator.lookupElement("paymentSearch_toDate"));
		String todaysDate = ImportData.return_TodaysDate_FormattedForWebsite();

		BaseUI.baseStringCompare("From Date", todaysDate, date1);
		BaseUI.baseStringCompare("To Date", todaysDate, date2);
		BaseUI.baseStringCompare("Work Group Selector Text", "Select an Entity or Workgroup", workgroupSelectorValue);

		paymentTypeDropdown.verify_SelectedDropdownValue("-- All --");
		paymentSourceDropdown.verify_SelectedDropdownValue("-- All --");
		sortByDropdown.verify_SelectedDropdownValue("Select one...");

		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("paymentSearch_QueryBuilder_Field_ByRowNumber", "1", null));
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("paymentSearch_QueryBuilder_Operator_ByRowNumber", "1", null));
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("paymentSearch_QueryBuilder_Value_ByRowNumber", "1", null));

		sortByDropdown.verify_SelectedDropdownValue("Select one...");
	}

	public static void select_ShowRowsDropdownValue(Integer numberToShow) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("paymentSearch_ShowResultNumberDropdown"),
				numberToShow.toString());
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static TableData return_SearchResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = available_Columns();

		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(Locator.lookup_multipleElements("paymentSearch_ResultRows", null, null));
		for (Integer i = 1; i <= rowList.size(); i++) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			ArrayList<WebElement> cellList = new ArrayList<WebElement>();
			cellList.addAll(Locator.lookup_multipleElements("paymentSearch_ResultRowByIndex", i.toString(), null));

			for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++) {
				if (cellList.size() == 14) {
					String cellText = cellList.get(headerIndex + 1).getAttribute("innerText");

					dataRow.put(headers.get(headerIndex), cellText);
				} else {
					String cellText = cellList.get(headerIndex).getAttribute("innerText");

					dataRow.put(headers.get(headerIndex), cellText);
				}
			}
			// if (dataRow.containsKey("Batch Id")) {
			// dataRow.put("Batch ID", dataRow.get("Batch Id"));
			// dataRow.remove("Batch Id");
			// }
			tableData.data.add(dataRow);
		}

		tableData.replace_Null_With_Empty();
		// tableData.replace_DateFormat_shortened();
		// tableData.remove_TrailingZeros_fromNumbers();
		return tableData;
	}

	public static ArrayList<String> available_Columns() {
		ArrayList<WebElement> headerElements = Locator.lookup_multipleElements("paymentSearch_TableHeaders", null,
				null);
		ArrayList<String> headers = new ArrayList<String>();
		for (WebElement header : headerElements) {
			String headerText = header.getAttribute("innerText");
			headers.add(headerText);
		}

		return headers;
	}

	public static void click_Header_ByText(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		BaseUI.scroll_to_element(headerElement);
		BaseUI.click(headerElement);
		Thread.sleep(1500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void sort_ByHeader_Ascending(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		if (headerElement.getAttribute("aria-sort") == null
				|| !BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "ascending")) {
			click_Header_ByText(headerText);

			if (!BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "ascending")) {
				click_Header_ByText(headerText);
			}

		}

	}

	public static void sort_ByHeader_Descending(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("paymentSearch_TableHeader_ByText", headerText, null);
		if (headerElement.getAttribute("aria-sort") == null
				|| !BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "descending")) {
			click_Header_ByText(headerText);

			if (!BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "descending")) {
				click_Header_ByText(headerText);
			}
		}
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedAscending(String dropdown_Value, String columnName)
			throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		TableData sorted_Results = return_SearchResults();
		sorted_Results.sort_ByColumn_Ascending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedDescending(String dropdown_Value, String columnName)
			throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		TableData sorted_Results = return_SearchResults();
		sorted_Results.sort_ByColumn_Descending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(String dropdown_Value,
			String columnName) throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		Results.remove_Character("$");
		Results.remove_Character(",");
		Results.remove_Character("/");

		TableData sorted_Results = return_SearchResults();
		sorted_Results.remove_Character("$");
		sorted_Results.remove_Character(",");
		sorted_Results.remove_Character("/");
		sorted_Results.sort_ByColumn_numeric_Ascending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(String dropdown_Value,
			String columnName) throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		Results.remove_Character("$");
		Results.remove_Character(",");
		Results.remove_Character("/");

		TableData sorted_Results = return_SearchResults();
		sorted_Results.remove_Character("$");
		sorted_Results.remove_Character(",");
		sorted_Results.remove_Character("/");
		sorted_Results.sort_ByColumn_numeric_Descending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static void verify_Field_Equals_Value(Integer indexNumber, String field, String value) throws Exception {
		String operator = "Equals";

		verify_Field_Operator_Value(indexNumber, field, operator, value);
	}

	public static void verify_Field_Operator_Value(Integer indexNumber, String field, String operator, String value)
			throws Exception {
		add_Search_Criteria(indexNumber, field, operator, value);
		click_Search();
		select_ShowRowsDropdownValue(100);
		TableData results = return_SearchResults();

		verify_ResultsPage_ResultsEqualExpected(results, field, value);

	}

	public static void verify_ResultsPage_ResultsEqualExpected(TableData results, String field, String value)
			throws Exception {

		results.remove_Character("$");
		results.remove_Character(",");
		results.remove_Character("/");
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			BaseUI.baseStringCompare(field, value, tableRow.get(field));
		}
	}

	public static void verify_Field_Contains_Value(Integer indexNumber, String field, String value) throws Exception {

		String operator = "Contains";

		add_Search_Criteria(indexNumber, field, operator, value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData results = return_SearchResults();

		verify_ResultsPage_Contains_Value(results, field, value);
	}

	public static void verify_ResultsPage_Contains_Value(TableData results, String field, String value)
			throws Exception {

		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			BaseUI.baseStringPartialCompare(field, value, tableRow.get(field));
		}
	}

	public static void verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(TableData results, String field,
			String operator, String value) throws Exception {

		results.remove_Character("$");
		results.remove_Character(",");
		results.remove_Character("/");
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			Double fieldValue = Double.parseDouble(tableRow.get(field));

			if (operator.equals("Is Less Than Or Equal To")) {

				// Integer fieldValue = Integer.parseInt(tableRow.get(field));
				BaseUI.verify_true_AndLog(fieldValue <= Double.parseDouble(value),
						MessageFormat.format("{0} is Less Than or equal to {1}", tableRow.get(field), value),
						MessageFormat.format("{0} is NOT Less Than or equal to {1}", tableRow.get(field), value));
			} else {

				BaseUI.verify_true_AndLog(fieldValue >= Double.parseDouble(value),
						MessageFormat.format("{0} is Greater Than or equal to {1}", tableRow.get(field), value),
						MessageFormat.format("{0} is NOT Greater Than or equal to {1}", tableRow.get(field), value));
			}
		}
	}

	public static void verify_QueryBuilder_ReturnsNoResults(Integer indexNumber, String field, String operator,
			String value) throws Exception {
		add_Search_Criteria(indexNumber, field, operator, value);
		click_Search();

		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_NoSearchResults"));
	}

	public static void verify_QueryBuilder_MissingRequiredField(Integer indexNumber, String field, String operator,
			String value) throws Exception {
		verify_QueryBuilder_ErrorMessage(indexNumber, field, operator, value, "Please fill in all required criteria.");

	}

	public static void verify_QueryBuilder_NonNumericChars_Error(Integer indexNumber, String field, String operator,
			String value) throws Exception {
		verify_QueryBuilder_ErrorMessage(indexNumber, field, operator, value,
				"Numeric Columns must have Numeric Values");

	}

	public static void verify_QueryBuilder_ErrorMessage(Integer indexNumber, String field, String operator,
			String value, String errorMessage) throws Exception {
		add_Search_Criteria(indexNumber, field, operator, value);
		click_Search();

		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserErrorPopup_ErrorMessage"));
		BaseUI.verifyElementHasExpectedText("entityUserErrorPopup_ErrorMessage", errorMessage);
	}

	public static String return_Text_FromPDF_Field(String field) {
		WebElement pdfTextElement = Locator.lookupElement("paymentSearch_BatchPDF_FindValue_ByText", field, null);
		String pdfText = BaseUI.getTextFromField(pdfTextElement).trim();

		return pdfText;
	}

	// rowNumber starts at 1.
	public static void click_Cell_Link(String columnName, Integer rowNumber) throws Exception {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("paymentSearch_ResultRowByIndex", rowNumber.toString(), null));

		BaseUI.click(cellList.get(headerIndex));
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();

	}

	// Index for transactionLineToClick starts at 1.
	public static void verify_ClickingTransaction_TakesYouToTransactionDetails_Page(Integer transactionLineToClick)
			throws Exception {

		TableData tableRowData = PaymentSearch.return_SearchResults();

		PaymentSearch.click_Cell_Link("Transaction", transactionLineToClick);
		Thread.sleep(100);

		String transactionDetail_DepositDate = BaseUI.getTextFromField(Locator.lookupElement(
				"paymentSearch_TransactionDetail_Header_TransactionDetail_ByText", "Deposit Date:", null));

		String transactionDetail_BatchNumber = BaseUI.getTextFromField(Locator
				.lookupElement("paymentSearch_TransactionDetail_Header_TransactionDetail_ByText", "Batch:", null));

		String transactionDetail_BatchID = BaseUI.getTextFromField(Locator
				.lookupElement("paymentSearch_TransactionDetail_Header_TransactionDetail_ByText", "Batch ID:", null));

		String transactionDetail_Transaction = BaseUI.getTextFromField(Locator.lookupElement(
				"paymentSearch_TransactionDetail_Header_TransactionDetail_ByText", "Transaction:", null));

		BaseUI.baseStringCompare("Deposit Date", tableRowData.data.get(transactionLineToClick - 1).get("Deposit Date"),
				transactionDetail_DepositDate);

		BaseUI.baseStringCompare("Batch ID", tableRowData.data.get(transactionLineToClick - 1).get("Batch ID"),
				transactionDetail_BatchID);

		BaseUI.baseStringCompare("Batch Number", tableRowData.data.get(transactionLineToClick - 1).get("Batch"),
				transactionDetail_BatchNumber);

		BaseUI.baseStringCompare("Transaction", tableRowData.data.get(transactionLineToClick - 1).get("Transaction"),
				transactionDetail_Transaction);

		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_TransactionDetail_Header"));
	}

	// Index for batchToClick starts at 1.
	public static void verify_ClickingBatch_TakesYouToBatchDetails_Page(Integer batchLineToClick) throws Exception {

		TableData tableRowData = PaymentSearch.return_SearchResults();

		PaymentSearch.click_Cell_Link("Batch", batchLineToClick);
		Thread.sleep(100);

		String batchDetail_DepositDate = BaseUI.getTextFromField(
				Locator.lookupElement("paymentSearch_BatchDetail_Header_BatchDetail_ByText", "Deposit Date:", null));

		String batchDetail_BatchNumber = BaseUI.getTextFromField(
				Locator.lookupElement("paymentSearch_BatchDetail_Header_BatchDetail_ByText", "Batch:", null));

		String batchDetail_BatchID = BaseUI.getTextFromField(
				Locator.lookupElement("paymentSearch_BatchDetail_Header_BatchDetail_ByText", "Batch ID:", null));

		BaseUI.baseStringCompare("Deposit Date", tableRowData.data.get(batchLineToClick - 1).get("Deposit Date"),
				batchDetail_DepositDate);

		BaseUI.baseStringCompare("Batch ID", tableRowData.data.get(batchLineToClick - 1).get("Batch ID"),
				batchDetail_BatchID);

		BaseUI.baseStringCompare("Batch Number", tableRowData.data.get(batchLineToClick - 1).get("Batch"),
				batchDetail_BatchNumber);

		BaseUI.verifyElementAppears(Locator.lookupElement("paymentSearch_BatchDetail_Header"));
	}

	public static WebElement return_Link_ByColumnName_AndColumnRow(String columnName, Integer rowNumber) {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("paymentSearch_ResultRowByIndex", rowNumber.toString(), null));

		return cellList.get(headerIndex);
	}

	// pass in deposit date in format MM/dd/yyyy
	public static void verify_Results_HaveCorrect_DepositDate(String depositDate) throws Exception {
		TableData searchResults = return_SearchResults();

		BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Search Results found.", "Search Results not found.");

		for (HashMap<String, String> tableRow : searchResults.data) {
			BaseUI.baseStringCompare("Deposit Date", depositDate, tableRow.get("Deposit Date"));
		}
	}
	
	public static void click_PreviousButton() throws Exception{
		BaseUI.click(Locator.lookupElement("paymentSearch_Pagin_PreviousButton"));
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void click_NextButton() throws Exception{
		BaseUI.click(Locator.lookupElement("paymentSearch_Pagin_NextButton"));
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void click_Page_ByPageNumber(Integer pageNumberToClick) throws Exception{
		BaseUI.click(Locator.lookupElement("paymentSearch_Pagin_ByPageNumber", pageNumberToClick.toString(), null));
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
}