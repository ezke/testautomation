package r360.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.Pagination;
import wfsCommon.pages.TextBox;

public class SecurityAdmin_RolesManagementModal {

	public static final String roleInfo_ApplicationDropdown = "s2id_selRolePageApplications";
	public static Dropdown appDropdwn = new Dropdown(roleInfo_ApplicationDropdown);
	public static Pagination groupManage_AvailableGroups = new Pagination("rolePageEntityGroupsGrid");
	public static Pagination groupManage_AssociatedGroups = new Pagination("rolePageRoleGroupsGrid");
	public static Pagination policyManage_AvailablePolicies = new Pagination("rolePageEntityApplicationPoliciesGrid");
	public static Pagination policyManage_AssociatedPolicies = new Pagination("rolePageRolePoliciesGrid");
	public static TextBox roleName = new TextBox("scrtyAdmin_RolesMdl_RoleNameTxt", null, null);
	public static TextBox roleDescription = new TextBox("scrtyAdmin_RolesMdl_RoleDescTxt", null, null);

	public static void navigate_GroupManagementTab() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("scrtyAdmin_RolesMdl_Nav_GroupsTab", null, null));
		Thread.sleep(500);
	}

	public static void navigate_PolicyManagementTab() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("scrtyAdmin_RolesMdl_Nav_PolicyTab", null, null));
		Thread.sleep(500);
	}

	public static void move_selectedGroups_FromAvailableGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_RolesMdl_GrpManage_MoveButton"));
		Thread.sleep(500);

	}

	public static void remove_selectedGroups_FromAssociatedGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_RolesMdl_GrpManage_RemoveButton"));
		Thread.sleep(500);
	}

	public static void move_selectedPolicies_FromAvailableGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_RolesMdl_PolicyManage_MoveButton"));
		Thread.sleep(500);
	}

	public static void remove_selectedPolicies_FromAssociatedGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_RolesMdl_PolicyManage_RemoveButton"));
		Thread.sleep(500);
	}

	public static void select_Application(String value) throws Exception {
		appDropdwn.select_EntityValue(value);
	}

	public static void enter_required_RoleFields(String roleName, String roleDescription, String roleApplication)
			throws Exception {
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleNameTxt"), roleName);
		BaseUI.enterText(Locator.lookupElement("scrtyAdmin_RolesMdl_RoleDescTxt"), roleDescription);
		select_Application(roleApplication);
	}

	public static void click_Cancel() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_RolesMdl_CancelButton"));
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_Save() throws Exception {
		BaseUI.click(Locator.lookupElement("scrtyAdmin_RolesMdl_SaveButton"));
		Navigation.wait_for_LoadingSpinner_ToAppear(1);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void addRole(String roleName, String roleDescription, String roleApplication) throws Exception {
		enter_required_RoleFields(roleName, roleDescription, roleApplication);
		click_Save();
	}

	public static void check_AvailableGroupCheckbox(String groupName) throws Exception {
		groupManage_AvailableGroups.show_All();
		groupManage_AvailableGroups.filter_textbox_SendKeys("Name", groupName);
		WebElement groupCheckbox = groupManage_AvailableGroups.check_box_for_GivenColumnAndText("Name", groupName);
		BaseUI.checkCheckbox(groupCheckbox);
		groupManage_AvailableGroups.filter_textbox_ClearFilter("Name");
	}
	
	public static void check_AssociatedGroupCheckbox(String groupName) throws Exception {
		WebElement groupCheckbox = groupManage_AssociatedGroups.check_box_for_GivenColumnAndText("Name", groupName);
		BaseUI.checkCheckbox(groupCheckbox);
	}
	
	public static void check_AvailablePolicyCheckbox(String policyName) throws Exception {
		WebElement policyCheckbox = policyManage_AvailablePolicies
				.check_box_for_GivenColumnAndText("Description", policyName);
		BaseUI.checkCheckbox(policyCheckbox);
	}

	public static void check_AssociatedPolicyCheckbox(String policyName) throws Exception {
		WebElement policyCheckbox = policyManage_AssociatedPolicies
				.check_box_for_GivenColumnAndText("Description", policyName);
		BaseUI.checkCheckbox(policyCheckbox);
	}
	
	public static void remove_AllGroups() throws Exception {
		navigate_GroupManagementTab();
		groupManage_AssociatedGroups.check_all_checkbox();
		remove_selectedGroups_FromAssociatedGrid();
		click_Save();
	}

	public static void remove_AllPolicies() throws Exception {
		navigate_PolicyManagementTab();
		policyManage_AssociatedPolicies.check_all_checkbox();
		remove_selectedPolicies_FromAssociatedGrid();
		click_Save();
	}

	public static void verifyErrorMessage_afterEnteringtext_ForGivenField(String fieldLocator, String textToEnter,
			String expectedErrorMessage) throws Exception {
		BaseUI.enterText(Locator.lookupElement(fieldLocator), textToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(fieldLocator));
		verifyErrorMessage_ForGivenField(fieldLocator, expectedErrorMessage);
	}

	public static void verifyErrorMessage_ForGivenField(String fieldLocator, String expectedErrorMessage)
			throws Exception {
		String errorText = Locator.lookupElement(fieldLocator)
				.findElement(By.xpath("./following-sibling::span[@class='validationMessage']")).getText();
		BaseUI.baseStringCompare(fieldLocator, expectedErrorMessage, errorText);
	}
	
	public static void verifyErrorMessage_ForGivenField(WebElement fieldElement, String expectedErrorMessage)
			throws Exception {
		String errorText = fieldElement
				.findElement(By.xpath("./following-sibling::span[@class='validationMessage']")).getText();
		BaseUI.baseStringCompare("Field To Check", expectedErrorMessage, errorText);
	}


	public static void verifyNO_ErrorMessage_ForGivenField(String fieldLocator, String textToEnter, String expectedText)
			throws Exception {
		BaseUI.enterText(Locator.lookupElement(fieldLocator), textToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(fieldLocator));
		String fieldText = BaseUI.getTextFromInputBox(Locator.lookupElement(fieldLocator));
		BaseUI.baseStringCompare(fieldLocator, expectedText, fieldText);
		WebElement errorElement = Locator.lookupElement(fieldLocator)
				.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		BaseUI.verifyElementDoesNotAppear(errorElement);
	}

}
