package r360.pages;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class SecurityAdminAddUser {
	
	//verify and click Add button
	
	public static void verifyAddUsersButtonAppears() throws InterruptedException{
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddButton"));
		Thread.sleep(2000);
	}
	public static void clickAddUserButton() throws Exception{
//		BaseUI.click_js(BaseUI.waitForElementToBeClickable("entityUserAddButton", null, null));
		BaseUI.click(Locator.lookupElement("entityUserAddButton"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("entityUserAddUserInfoCancelButton", null, null, 20);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	//click save button
	public static void clickSaveButton() throws Exception{
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityUserAddUserInfoSaveButton", null, null));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	public static void verifyPartialAddUserSaveButton(String expectedValue) {
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoSaveButton",expectedValue);
	}
	public static void verifySaveButtonEnabled(){
		BaseUI.verifyElementEnabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
	}
	
	public static void verifySaveButtonDisabled(){
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
	}
	
	//click cancel button
	public static void clickCancelButton() throws Exception{
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityUserAddUserInfoCancelButton", null, null));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	public static void verifyPartialTextAddUserCancelButton(String expectedValue) {
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoCancelButton",expectedValue);
	}
	// Click Cancel to close out the EntityManagementModal
	public static void cancelEntityUserUpdatemodal() throws Exception {
			BaseUI.click(Locator.lookupElement("entityUserAddUserInfoCancelButton"));
			Thread.sleep(2000);
	}
	//click delete button to delete user
	public static void clickDeleteButton() throws Exception{
		BaseUI.click(Locator.lookupElement("entityUserDeleteButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	//click confirm delete button to delete user permanently
	public static void clickConfirmDeleteButton(){
		BaseUI.click(Locator.lookupElement("entityUserConfirmDeleteButton"));
	}
	public static void clickConfirmCancelButton(){
		BaseUI.click(Locator.lookupElement("entityUserConfirmCancelButton"));
	}
	public static void verifyUserManagementLabelAppears(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserManagementLabel"));
	}
	//login name 
	public static void verifyLoginNameLabelAppears(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLoginNameLabel"));
	}
	public static void tabThroughLoginNameAndVerifyErrorMessageAppears() throws InterruptedException {
		BaseUI.tabThroughField("entityUserAddUserInfoLoginNameText");
		BaseUI.verifyElementAppearsByString("entityUserAddUserInfoLoginNameTextErrorBox");
	}
	public static void tabThroughLoginName() throws Exception{
		BaseUI.tabThroughField(Locator.lookupElement("entityUserAddUserInfoLoginNameText"));
	}
	public static void verifyLoginNameErrorBoxAppears(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLoginNameTextErrorBox"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoLoginNameTextErrorBox", "This field is required");
	}
	public static void verifyLoginNameErrorBoxDisplayedForSpecialCharacter(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLoginNameTextErrorBoxSpecialCharacter"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoLoginNameTextErrorBoxSpecialCharacter", "Only Letters, digits, and @ . _ are allowed");
	}
	public static void verifyLoginNameErrorBoxDisplayedForMinLengthAppears(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLoginNameTextErrorBoxForMinLength"));
		//BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoLoginNameTextErrorBoxForMinLength", "Login Name must have minimum length");
	}
	public static void verifyLoginNameErrorIsNotDisplayed(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoLoginNameTextErrorBox"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoLoginNameTextErrorBoxSpecialCharacter"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoLoginNameTextErrorBoxForMinLength"));
	}
	public static void verifyUniqueLoginNameErrorAppears(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserUniqueLoginNameError"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserUniqueLoginNameError", "Login Name must be unique");
	}
	public static void verifyUniqueLoginNameErrorDoesNotAppear(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserUniqueLoginNameError"));	
	}
//	public static void runLoginNameValidation() throws InterruptedException{
//		SecurityAdminAddUser.tabThroughLoginNameAndVerifyErrorMessageAppears();
//		SecurityAdminAddUser.enterLoginName("aetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshwu");
//		SecurityAdminAddUser.tabThroughLoginName();
//		Thread.sleep(3000);
//		SecurityAdminAddUser.enterLoginName("aetewye156845wgagwahsgtea152364heoiyte@uyeshwu.");
//		SecurityAdminAddUser.tabThroughLoginName();
//		Thread.sleep(3000);
//		SecurityAdminAddUser.enterLoginName(GlobalVariables.genericAddUserLongString);
//		String LoginnameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("entityUserAddUserInfoLoginNameText"));
//		Assert.assertTrue(LoginnameInputText.length() <= 255, "Name length was not less than or equal to 255.");
//		SecurityAdminAddUser.tabThroughLoginName();
//		SecurityAdminAddUser.enterLoginName("abc");
//		SecurityAdminAddUser.tabThroughLoginName();
//		Thread.sleep(3000);
//		SecurityAdminAddUser.verifyLoginNameErrorBoxDisplayedForMinLengthAppears();
//		SecurityAdminAddUser.enterLoginName("abc1");
//		SecurityAdminAddUser.tabThroughLoginName();
//		Thread.sleep(3000);
//		SecurityAdminAddUser.verifyLoginNameErrorBoxDisplayedForMinLengthAppears();
//		SecurityAdminAddUser.enterLoginName("abcd{[[**");
//		SecurityAdminAddUser.verifyLoginNameErrorBoxDisplayedForSpecialCharacter();
//		SecurityAdminAddUser.enterLoginName("JohnS12");
//		SecurityAdminAddUser.verifyLoginName("JohnS12");
//		SecurityAdminAddUser.verifyLoginNameErrorIsNotDisplayed();
//	}
	public static void enterLoginName(String valueToSet){
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("entityUserAddUserInfoLoginNameText"), valueToSet);
	}
	public static void verifyLoginName(String valueToVerify){
		BaseUI.verifyValueOfField("entityUserAddUserInfoLoginNameText", valueToVerify);
	}
	//First Name 
	public static void enterFirstName(String valueToSet) throws Exception{
		if(Browser.currentBrowser.equals("chrome")){
			BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityUserAddUserInfoFirstNameText"), valueToSet);
		}else
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("entityUserAddUserInfoFirstNameText"), valueToSet);
	}
	public static void verifyFirstName(String valueToSet){
		BaseUI.verifyValueOfField("entityUserAddUserInfoFirstNameText", valueToSet);
	}
	public static void tabThroughFirstName() throws InterruptedException{
		BaseUI.tabThroughField("entityUserAddUserInfoFirstNameText");
	}
	public static void verifyFirstNameErrorBoxDisplayedForSpecialCharacter(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoFirstNameTextErrorBoxForSpecialCharacter"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoFirstNameTextErrorBoxForSpecialCharacter", "Only Letters, digits, spaces and & , . :");
	}
	public static void verifyFirstNameErrorBoxDisplayedForMinLength(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoFirstNameTextErrorBoxForMinLength"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoFirstNameTextErrorBoxForMinLength", "Please enter at least 2 characters");
	}
	public static void verifyFirstNameErrorIsNotDisplayed(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoFirstNameTextErrorBoxForSpecialCharacter"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoFirstNameTextErrorBoxForMinLength"));
	}
//	public static void runFirstNameValidation() throws InterruptedException{
//		SecurityAdminAddUser.enterFirstName("aaetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshwu");
//		SecurityAdminAddUser.tabThroughFirstName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterFirstName("aaetewyetywet156489gagwahsgtea784123569iytesuyesh63");
//		SecurityAdminAddUser.tabThroughFirstName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterFirstName("abd123 ,.&()_-: ");
//		SecurityAdminAddUser.tabThroughFirstName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterFirstName("abd123 ,.&()_-:[[\\**");
//		SecurityAdminAddUser.tabThroughFirstName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.verifyFirstNameErrorBoxDisplayedForSpecialCharacter();
//		SecurityAdminAddUser.enterFirstName(GlobalVariables.sixtyfiveCharacter);
//		String FirstnameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("entityUserAddUserInfoFirstNameText"));
//		Assert.assertTrue(FirstnameInputText.length() <= 50, "Name length was not less than or equal to 50.");
//		SecurityAdminAddUser.tabThroughFirstName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterFirstName("John");
//		SecurityAdminAddUser.tabThroughFirstName();
//		SecurityAdminAddUser.verifyFirstName("John");
//		SecurityAdminAddUser.verifyFirstNameErrorIsNotDisplayed();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterFirstName("J");
//		SecurityAdminAddUser.tabThroughFirstName();
//		SecurityAdminAddUser.verifyFirstNameErrorBoxDisplayedForMinLength();
//	}
	//Middle Initial
	public static void enterMiddleInitial(String valueToSet){
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("entityUserAddUserInfoM.I.Text"), valueToSet);
	}
	public static void verifyMiddleInitial(String valueToSet){
		BaseUI.verifyValueOfField("entityUserAddUserInfoM.I.Text", valueToSet);
	}
	public static void tabThroughMiddleInitial() throws InterruptedException{
		BaseUI.tabThroughField("entityUserAddUserInfoM.I.Text");
	}
	public static void verifyMiddleInitialErrorBoxDisplayed(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoM.I.TextErrorBox"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoM.I.TextErrorBox", "A single letter only");
	}
	public static void verifyMiddleInitialErrorIsNotDisplayed(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoM.I.TextErrorBox"));
	}
//	public static void runMiddleInitialValidation() throws InterruptedException{
//		SecurityAdminAddUser.enterMiddleInitial("A");
//		SecurityAdminAddUser.verifyMiddleInitial("A");
//		SecurityAdminAddUser.tabThroughMiddleInitial();
//		Thread.sleep(3000);
//		SecurityAdminAddUser.enterMiddleInitial("1");
//		SecurityAdminAddUser.tabThroughMiddleInitial();
//		Thread.sleep(3000);
//		SecurityAdminAddUser.verifyMiddleInitialErrorBoxDisplayed();
//		SecurityAdminAddUser.enterMiddleInitial("*");
//		SecurityAdminAddUser.tabThroughMiddleInitial();
//		SecurityAdminAddUser.verifyMiddleInitialErrorBoxDisplayed();
//		
//	}
	//Last Name
	public static void enterLastName(String valueToSet) throws Exception{
		if(Browser.currentBrowser.equals("chrome")){
			BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityUserAddUserInfoLastNameText"), valueToSet);
		}else{
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("entityUserAddUserInfoLastNameText"), valueToSet);
		}
	}
	public static void verifyLastName(String valueToSet){
		BaseUI.verifyValueOfField("entityUserAddUserInfoLastNameText", valueToSet);
	}
	public static void tabThroughLastName() throws InterruptedException{
		BaseUI.tabThroughField("entityUserAddUserInfoLastNameText");
	}
	public static void verifyLastNameErrorBoxDisplays(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLastNameTextValidation"));
	}
	public static void verifyLastNameErrorBoxDisplayedForMinLength(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLastNameTextErrorBoxForMinLength"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoLastNameTextErrorBoxForMinLength", "Please enter at least 2 characters");
	}
	public static void verifyLastNameErrorBoxDisplayedForSpecialCharacter(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoLastNameTextErrorBoxForSpecialCharacter"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoLastNameTextErrorBoxForSpecialCharacter", "Only Letters, digits, spaces");
	}
	public static void verifyLastNameErrorIsNotDisplayed(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoLastNameTextErrorBoxForMinLength"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoLastNameTextErrorBoxForSpecialCharacter"));
	}
//	public static void runLastNameValidation() throws InterruptedException{
//		SecurityAdminAddUser.enterLastName("aaetewyetywetwysdhwgagwahsgteagsgtgaheoiytesuyeshwu");
//		SecurityAdminAddUser.tabThroughLastName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterLastName("aaetewyetywet156489gagwahsgtea784123569iytesuyesh63");
//		SecurityAdminAddUser.tabThroughLastName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterLastName("abd123 ,.&()_-:");
//		SecurityAdminAddUser.tabThroughLastName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterLastName("abd123 ,.&()_-:[[\\**");
//		SecurityAdminAddUser.tabThroughLastName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.verifyLastNameErrorBoxDisplayedForSpecialCharacter();
//		SecurityAdminAddUser.enterLastName(GlobalVariables.sixtyfiveCharacter);
//		String LastnameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("entityUserAddUserInfoLastNameText"));
//		Assert.assertTrue(LastnameInputText.length() <= 50, "Name length was not less than or equal to 50.");
//		SecurityAdminAddUser.tabThroughLastName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.enterLastName("Barett");
//		SecurityAdminAddUser.tabThroughLastName();
//		SecurityAdminAddUser.verifyLastName("Barett");
//		SecurityAdminAddUser.verifyLastNameErrorIsNotDisplayed();
//		SecurityAdminAddUser.enterLastName("B");
//		SecurityAdminAddUser.tabThroughLastName();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.verifyLastNameErrorBoxDisplays();	
//	}
	//Email Address
	public static void enterEmailAddress(String valueToEnter) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("entityUserAddUserInfoEmailText"), valueToEnter);
	}
	public static void verifyEmailAddress(String valueToVerify){
		BaseUI.verifyValueOfField("entityUserAddUserInfoEmailText", valueToVerify);
	}
	public static void tabThroughEmailAddress() throws Exception{
		BaseUI.tabThroughField(Locator.lookupElement("entityUserAddUserInfoEmailText"));
	}
	public static void verifyEmailAddressErrorBoxDisplayed(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoEmailErrorBox"));
		BaseUI.verifyElementHasExpectedPartialText("entityUserAddUserInfoEmailErrorBox", "Please enter a valid email address");
	}
	public static void verifyEmailAddressErrorIsNotDisplayed(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserAddUserInfoEmailErrorBox"));
	}
//	public static void runEmailAddressValidation() throws InterruptedException{
//		SecurityAdminAddUser.enterEmailAddress("abc.gmail.com");
//		SecurityAdminAddUser.tabThroughEmailAddress();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.verifyEmailAddressErrorBoxDisplayed();
//		SecurityAdminAddUser.enterEmailAddress("abc@gmailcom");
//		SecurityAdminAddUser.tabThroughEmailAddress();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.verifyEmailAddressErrorBoxDisplayed();
//		SecurityAdminAddUser.enterEmailAddress("abc@gmail.");
//		SecurityAdminAddUser.tabThroughEmailAddress();
//		Thread.sleep(2000);
//		SecurityAdminAddUser.verifyEmailAddressErrorBoxDisplayed();
//		SecurityAdminAddUser.enterEmailAddress(GlobalVariables.genericEmailAddress);
//		SecurityAdminAddUser.tabThroughEmailAddress();
//		SecurityAdminAddUser.verifyEmailAddress(GlobalVariables.genericEmailAddress);
//		SecurityAdminAddUser.verifyEmailAddressErrorIsNotDisplayed();
//	}
	//Update Password Button
	public static void clickUpdatePasswordButton(){
		BaseUI.click(Locator.lookupElement("entityUserUpdatePasswordButton"));
	}
	public static void updatePassword(String password) throws Exception{
		BaseUI.click(Locator.lookupElement("entityUserUpdatePasswordButton"));
		BaseUI.enterText(Locator.lookupElement("entityUserAddUserInfoPasswordText"), password);
		BaseUI.enterText(Locator.lookupElement("entityUserAddUserInfoConfirmPasswordText"), password);
		BaseUI.tabThroughField(Locator.lookupElement("entityUserAddUserInfoConfirmPasswordText"));
	}
	public static void verifyUpdatePasswordButtonAppears(){
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserUpdatePasswordButton"));
	}
	public static void verifyUpdatePasswordButtonDoesNotAppear(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("entityUserUpdatePasswordButton"));
	}	
	//Password
	public static void verifyPasswordTextBoxDisabled(){
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoPasswordText"));
	}
	public static void enterPassword(String valueToSet){
		BaseUI.enterText(Locator.lookupElement("entityUserAddUserInfoPasswordText"), valueToSet);
	}
	public static void verifyPassword(String valueToSet){
		BaseUI.verifyValueOfField("entityUserAddUserInfoPasswordText", valueToSet);
	}
	public static void tabThroughPasswordText() throws Exception{
		BaseUI.tabThroughField(Locator.lookupElement("entityUserAddUserInfoPasswordText"));
	}
	
	public static void verifyPasswordErrorMessageForMinLengthCharAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoPasswordErrorForMinLength"));
	}
	public static void verifyPasswordErrorMessageForMinSpecialCharAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoPasswordErrorForSpecialChar"));
	}
	public static void verifyPasswordErrorMessageForMinUppercaseCharAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoPasswordErrorForMinUpperCaseChar"));
	}
	public static void verifyPasswordErrorMessageForMinLowercaseCharAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoPasswordErrorForMinLowerCaseChar"));
	}
	public static void verifyPasswordErrorMessageForMinNumericCharAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserAddUserInfoPasswordErrorForMinNumericChar"));
	}
	public static void verifyPasswordConfigForValidation(String Entity, String texboxIdentifier, 
			String expectedValue) throws Exception{
		SecurityAdministration.launchEditEntityModal_forGivenEntity(Entity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
		SecurityAdmin_EntityManageModal.verify_Textbox_Value(texboxIdentifier, expectedValue);
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		SecurityAdministration.navigate_toUserTab();
//		SecurityAdminAddUser.clickAddUserButton();
//		Thread.sleep(2000);
//		SecurityAdmin_UserManageModal.enterUsersForPasswordValidation(loginName, firstName, lastName, email);
//		Thread.sleep(1000);
	}
	
	
	public static void verifyPasswordError(String textBoxIdentifier, String textBoxErrorValidationIdentifier,
			String errorMessage, String valueToEnter) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement(textBoxIdentifier), valueToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxIdentifier));
		Thread.sleep(3000);
		BaseUI.verifyElementAppears(Locator.lookupElement(textBoxErrorValidationIdentifier));
		BaseUI.verifyElementHasExpectedText(textBoxErrorValidationIdentifier, errorMessage);
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
	}
	
	public static void verifyError(String textBoxIdentifier, String textBoxErrorValidationIdentifier,
			String errorMessage, String valueToEnter) throws Exception {
		BaseUI.enterText(Locator.lookupElement(textBoxIdentifier), valueToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxIdentifier));
		Thread.sleep(1000);
		BaseUI.verifyElementAppears(Locator.lookupElement(textBoxErrorValidationIdentifier));
		BaseUI.verifyElementHasExpectedText(textBoxErrorValidationIdentifier, errorMessage);
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
	}
	public static void verifyErrorIsNotDisplayed(String textBoxIdentifier, String textBoxErrorValidationIdentifier,
			String errorMessage, String valueToEnter) throws Exception {
		BaseUI.enterText(Locator.lookupElement(textBoxIdentifier), valueToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxIdentifier));
		Thread.sleep(1000);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement(textBoxErrorValidationIdentifier));
		BaseUI.verifyElementDoesNotHaveExpectedText(textBoxErrorValidationIdentifier, errorMessage);
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoSaveButton"));
	}
	
	
	//Confirm password
	public static void enterConfirmPassword(String valueToSet){
		BaseUI.enterText(Locator.lookupElement("entityUserAddUserInfoConfirmPasswordText"), valueToSet);
	}
	public static void verifyConfirmPassword(String valueToSet){
		BaseUI.verifyValueOfField("entityUserAddUserInfoConfirmPasswordText", valueToSet);
	}
	public static void tabThroughConfirmPassword() throws InterruptedException {
		BaseUI.tabThroughField("entityUserAddUserInfoConfirmPasswordText");
	}	
	public static void tabThroughConfirmPasswordAndVerifyErrorMessageAppears() throws InterruptedException {
		BaseUI.tabThroughField("entityUserAddUserInfoPasswordText");
		BaseUI.verifyElementAppearsByString("entityUserAddUserInfoConfirmPasswordDoNotMatchErrorBox");
	}
	//Active CheckBox
	public static void checkActiveBox(){
		BaseUI.checkCheckbox("entityUserAddUserInfoActiveCheckbox");
	}
	public static void verifyActiveBoxChecked(){
		BaseUI.verifyCheckboxStatus("entityUserAddUserInfoActiveCheckbox", "checked");
	}
	public static void verifyActiveBoxUnchecked(){
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityUserAddUserInfoActiveCheckbox"), false);
	}
	public static void uncheckActiveBox(){
		BaseUI.uncheckCheckbox("entityUserAddUserInfoActiveCheckbox");
	}
	//Account Locked
	public static void checkAccountIsLockedBox(){
		BaseUI.checkCheckbox("entityUserAddUserInfoAccountIsLockedCheckbox");
	}
	public static void uncheckAccountIsLockedBox(){
		BaseUI.uncheckCheckbox("entityUserAddUserInfoAccountIsLockedCheckbox");
	}
	public static void verifyAccountIsLockedBoxChecked(){
		BaseUI.verifyCheckboxStatus("entityUserAddUserInfoAccountIsLockedCheckbox", "checked");
	}

	public static void verifyAccountIsLockedBoxUnchecked(){
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityUserAddUserInfoAccountIsLockedCheckbox"), false);
	}
	
	//Change Password
	
	public static void checkChangePasswordAtnextLogin() throws InterruptedException{
		BaseUI.checkCheckbox("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox");
		Thread.sleep(500);
	}
	public static void unCheckChangePasswordAtnextLogin() throws InterruptedException{
		BaseUI.uncheckCheckbox("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox");
		Thread.sleep(500);
	}
	public static void verifyChangePasswordAtNextLoginDisabled(){
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox"));
	}
	public static void verifyChangePasswordAtNextLoginCheckBox() throws InterruptedException{
		BaseUI.verifyCheckboxStatus("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox", "checked");
		Thread.sleep(500);
	}
	public static void verifyChangePasswordAtNextLoginUncheckBox() throws InterruptedException{
		BaseUI.verifyCheckboxStatus("entityUserAddUserInfoChangePasswordAtNextLoginCheckBox", "unchecked");
		Thread.sleep(500);
	}	
}
	
