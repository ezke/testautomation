package r360.pages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import wfsCommon.pages.Pagination;
import wfsCommon.pages.TextBox;

public class SecurityAdmin_GroupsManagementModal {

	public static TextBox groupDescriptionTxt = new TextBox("grpsManage_GroupDescriptiontxt", null, null);
	public static TextBox groupNameTxt = new TextBox("grpsManage_GroupNameTxt", null, null);
	public static Pagination usersAvailableGrid = new Pagination("groupPageEntityUsersGrid");
	public static Pagination usersAssociatedGrid = new Pagination("groupPageGroupUsersGrid");
	public static Pagination rolesAvailableGrid = new Pagination("groupPageEntityRolesGrid");
	public static Pagination rolesAssociatedGrid = new Pagination("groupPageGroupRolesGrid");

	public static void click_Cancel() throws Exception {
		BaseUI.click(Locator.lookupElement("grpsManage_CancelButton"));
		Thread.sleep(500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_Save() throws Exception {
		BaseUI.click(Locator.lookupElement("grpsManage_SaveButton"));
		Thread.sleep(200);
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void addGroup(String groupName) throws Exception {
		groupNameTxt.enterTextIntoField_AndTab(groupName);
		click_Save();
	}

	public static void navigate_UserManagementTab() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			Thread.sleep(1000);
			BaseUI.click(Locator.lookupElement("grpsManage_Nav_UsersTab", null, null));
			Thread.sleep(500);
		} else {
			BaseUI.click(BaseUI.waitForElementToBeClickable("grpsManage_Nav_UsersTab", null, null));
			Thread.sleep(500);
		}
	}

	public static void navigate_RoleManagementTab() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			Thread.sleep(1000);
			BaseUI.click(Locator.lookupElement("grpsManage_Nav_RolesTab", null, null));
			Thread.sleep(500);
		} else {
			BaseUI.click(BaseUI.waitForElementToBeClickable("grpsManage_Nav_RolesTab", null, null));
			Thread.sleep(500);
		}

	}

	public static void remove_AllUsers() throws Exception {
		navigate_UserManagementTab();
		usersAssociatedGrid.check_all_checkbox();
		remove_selectedUsers_FromAssociatedGrid();
		click_Save();
	}

	public static void remove_selectedUsers_FromAssociatedGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("grpsManage_UserManage_RemoveButton"));
		Thread.sleep(500);
	}

	public static void remove_AllRoles() throws Exception {
		navigate_RoleManagementTab();
		rolesAssociatedGrid.check_all_checkbox();
		remove_selectedRoles_FromAssociatedGrid();
		click_Save();
	}

	public static void remove_selectedRoles_FromAssociatedGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("grpsManage_RoleManage_RemoveButton"));
		Thread.sleep(500);
	}

	public static void check_AvailableUserCheckbox(String userName) throws Exception {
		WebElement userCheckbox = usersAvailableGrid.check_box_for_GivenColumnAndText("Name", userName);
		BaseUI.checkCheckbox(userCheckbox);
	}

	public static void check_AvailableRoleCheckbox(String roleName) throws Exception {
		WebElement roleCheckbox = rolesAvailableGrid.check_box_for_GivenColumnAndText("Name", roleName);
		BaseUI.checkCheckbox(roleCheckbox);
	}

	public static void move_selectedUsers_FromAvailableGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("grpsManage_UserManage_MoveButton"));
		Thread.sleep(500);
	}

	public static void move_selectedRoles_FromAvailableGrid() throws Exception {
		BaseUI.click(Locator.lookupElement("grpsManage_RoleManage_MoveButton"));
		Thread.sleep(500);
	}
}
