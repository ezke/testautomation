package r360.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.BaseUI;
import utils.Browser;
import utils.DatabaseConnection;
import utils.TableData;

public class MyProfilePage {
    public MyProfilePage() {
        PageFactory.initElements(Browser.driver, this);
    }

    @FindBy(xpath = "//B[@_ngcontent-c3='']")
    private WebElement UserProfileCaretSignAngular;
    @FindBy(xpath = "(//B[@class='caret'])[4]")
    private WebElement UserProfileCaretSignNonAngular;
    @FindBy(xpath = "//A[@_ngcontent-c3=''][text()='My Profile']")
    private WebElement MyProfileAngular;
    @FindBy(xpath = "//A[@class='tabElement'][text()='My Profile']")
    private WebElement MyProfileNonAngular;
    @FindBy(xpath = "//SPAN[@_ngcontent-c0=''][text()='My Profile']")
    private WebElement MyProfileheaderlabel;
    @FindBy(xpath = "//LABEL[@_ngcontent-c0=''][text()='Login Name: ']")
    private WebElement LoginNameLabel;
    @FindBy(xpath = "//LABEL[@_ngcontent-c0=''][text()='Name: ']")
    private WebElement NameLabel;
    @FindBy(xpath = "//LABEL[@_ngcontent-c0=''][text()='Email: ']")
    private WebElement EmailLabel;
    @FindBy(xpath = "//LABEL[@_ngcontent-c0=''][text()='Password: ']")
    private WebElement PasswordLabel;
    @FindBy(xpath = "//BUTTON[@_ngcontent-c0=''][text()='Change']")
    private WebElement changePasswordButton;
    @FindBy(xpath = "(//INPUT[@_ngcontent-c0=''])[1]")
    private WebElement currentPasswordTextBox;
    @FindBy(xpath = "(//INPUT[@_ngcontent-c0=''])[2]")
    private WebElement newPasswordTextBox;
    @FindBy(xpath = "(//INPUT[@_ngcontent-c0=''])[3]")
    private WebElement confirmNewPasswordTextBox;
    @FindBy(xpath = "//BUTTON[@_ngcontent-c0=''][text()='Cancel']")
    private WebElement cancelButton;
    @FindBy(xpath = "//BUTTON[@id='password_modal_save']")
    private WebElement saveButton;
    @FindBy(xpath = "//DIV[@_ngcontent-c0=''][text()=' Password does not match current password. ']")
    private WebElement CurPwdDoNotMatch;
    @FindBy(xpath = "//DIV[@_ngcontent-c0=''][text()=' New Passwords do not match ']")
    private WebElement NewPwdDoNotMatch;
    @FindBy(xpath = "//DIV[@_ngcontent-c0=''][text()=' Passwords cannot be reused in less than 180 days. ']")
    private WebElement NoReusePasswordLessThan180days;
    @FindBy(xpath = "//DIV[@_ngcontent-c0=''][text()=' Password has been changed ']")
    private WebElement PasswordHasChanged;
    public boolean isUserProfileCaretSignAngularVisible() {return BaseUI.elementAppears(UserProfileCaretSignAngular);}
    public boolean isUserProfileCaretSignNonAngularVisible() {return BaseUI.elementAppears(UserProfileCaretSignNonAngular);}
    public boolean isMyProfileAngularVisible() {return BaseUI.elementAppears(MyProfileAngular);}
    public boolean isMyProfileNonAngularVisible() {return BaseUI.elementAppears(MyProfileNonAngular);}
    public boolean isMyProfileheaderlabelVisible() {return BaseUI.elementAppears(MyProfileheaderlabel);}
    public boolean isLoginNameLabelVisible() {return BaseUI.elementAppears(LoginNameLabel);}
    public boolean isNameLabelVisible() {return BaseUI.elementAppears(NameLabel);}
    public boolean isEmailLabelVisible() {return BaseUI.elementAppears(EmailLabel);}
    public boolean isPasswordLabelVisible() {return BaseUI.elementAppears(PasswordLabel);}
    public boolean isCurPwdDoNotMatchVisible() {return BaseUI.elementAppears(CurPwdDoNotMatch);}
    public boolean isNewPwdDoNotMatchVisible() {return BaseUI.elementAppears(NewPwdDoNotMatch);}
    public boolean isNoReusePasswordLessThan180daysVisible() {return BaseUI.elementAppears(NoReusePasswordLessThan180days);}
    public boolean isPasswordHasChangedVisible() {return BaseUI.elementAppears(PasswordHasChanged);}
    public WebElement getUserProfileCaretSignAngularXpath(){
        return  UserProfileCaretSignAngular;
    }
	public WebElement getUserProfileCaretSignNonAngularXpath(){
        return  UserProfileCaretSignNonAngular;
    }
	public WebElement getMyProfileAngularXpath(){
        return  MyProfileAngular;
    }
	public WebElement getMyProfileNonAngularXpath(){
        return  MyProfileNonAngular;
    }
    public WebElement getChangePasswordButtonXpath(){
        return changePasswordButton;
    }
    public WebElement getCurrentPasswordTextBoxXpath(){
        return currentPasswordTextBox;
    }
    public WebElement getNewPasswordTextBoxXpath(){
        return newPasswordTextBox;
    }
    public WebElement getConfirmNewPasswordTextBoxXpath(){
        return confirmNewPasswordTextBox;
    }
    public WebElement getCancelButtonXpath(){return cancelButton;}
    public WebElement getSaveButtonXpath(){
        return saveButton;
    }

    public boolean CheckforNoReuse180orChangeComplete() {
        if(isNoReusePasswordLessThan180daysVisible()){
            return true;
        }else if (isPasswordHasChangedVisible()){
            return true;
        }else{
            return false;
        }
    }

    public boolean getmyprofiledata_validate(String userlogin) {
        String query = "SELECT [Users].LoginID as ULogin,[Users].PersonID,[Users].Email as Email,"
                + "[Users].EncryptedPassword as PassWrd,[Persons].FirstName as FN,[Persons].LastName as LN,"
                + "[Persons].PersonID,[Users].LastPasswordChangedDate as LPCD"
                + "\n"
                + " FROM [Users]" + "\n"
                + "INNER JOIN  [Persons]" + "\n"
                + "ON  [Users].PersonID=[Persons].PersonID" + "\n"
                + "Where [Users].LoginID=\'" + userlogin + "\'";
        try {
            DatabaseConnection.dbUrl= GlobalVariables.environment.RAAMDBConnection;
            TableData tableData = DatabaseConnection.runSQLServerQuery(query);
            String Fullname=tableData.data.get(0).get("FN")+" "+tableData.data.get(0).get("LN");
            String email=tableData.data.get(0).get("Email");
            String user=tableData.data.get(0).get("ULogin");
            BaseUI.verifyElementHasExpectedText("MyProfileFullName",Fullname);
            BaseUI.verifyElementHasExpectedText("MyProfileEmail",email);
            BaseUI.verifyElementHasExpectedText("MyProfileLoginName",user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
