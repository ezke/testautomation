package r360.pages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class PreDepositExceptionsBatchDetailPage {
    public void waitForPageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("preDepositBatchDetailPageTitle", null, null);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
    }

    public void lockBatch() throws Exception {
        WebElement lockSwitch = Locator.lookupElement("preDepositBatchDetailBatchLock");
        BaseUI.click(lockSwitch);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
    }

    public PreDepositExceptionsTransactionDetailPage clickGridItem(int row, String column) throws Exception {
        WebElement cell = Locator.lookupElement("preDepositBatchDetailGrid", Integer.toString(row), column);
        BaseUI.click(cell);
        PreDepositExceptionsTransactionDetailPage nextPage = new PreDepositExceptionsTransactionDetailPage();
        nextPage.waitForPageToLoad();
        return nextPage;
    }

    public String getGridItemText(int row, String column) {
        WebElement cell = Locator.lookupElement("preDepositBatchDetailGrid", Integer.toString(row), column);
        return BaseUI.getTextFromField(cell);
    }

    public String getGridHoverMessage() {
        WebElement firstRow = Locator.lookupElement("preDepositBatchDetailGridFirstRow");
        return BaseUI.get_Attribute_FromField(firstRow, "title");
    }
}
