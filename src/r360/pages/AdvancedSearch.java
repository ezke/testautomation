package r360.pages;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.File;
import java.nio.file.Files;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import r360.data.Config_DataRetrieval;
import r360.data.ImportData;
import utils.*;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.FilterExpander;
import wfsCommon.pages.TextBox;

public class AdvancedSearch {

	public static FilterExpander workGroupSelector = new FilterExpander("filterExpander");
	public static Dropdown paymentTypeDropdown = new Dropdown("s2id_paymentType");
	public static Dropdown paymentSourceDropdown = new Dropdown("s2id_paymentSource");
	public static Dropdown sortByDropdown = new Dropdown("s2id_query-sort");
	public static Dropdown savedQueryDropdown = new Dropdown("s2id_query-selector");






	public static void select_SavedQuery(String advancedSearch_QueryName) throws Exception {
		AdvancedSearch.savedQueryDropdown.select_EntityValue(advancedSearch_QueryName);
		Navigation.waitFor_AdvancedSearchPage();
	}

	public static void setDateRange(String dateFrom, String dateTo) {
		enter_Date(Locator.lookupElement("advancedSearch_fromDate"), dateFrom);
		enter_Date(Locator.lookupElement("advancedSearch_toDate"), dateTo);
	}



	// Verify values on Advanced Search. This method assumes there is only one
	// Query Builder row.
	// Also assumes there it is only set to display 1 field.
	public static void verify_Values_forAdvancedSearch(String workgroup, String paymentType, String paymentSource,
			String sortBy, String searchCrit_field, String searchCrit_Operator, String searchCrit_Value,
			String displayField, String queryName, String queryDescription) throws Exception {

		Dropdown fieldDropdown = new Dropdown("advancedSearch_QueryBuilder_Field_ByRowNumber", "1", null);
		Dropdown operatorDropdown = new Dropdown("advancedSearch_QueryBuilder_Operator_ByRowNumber", "1", null);
		// TextBox valueTextBox = new
		// TextBox("advancedSearch_QueryBuilder_Value_ByRowNumber", "1", null);

		String workgroupSelectorValue = workGroupSelector.return_SelectedText();

		String date1 = BaseUI.getTextFromInputBox(Locator.lookupElement("advancedSearch_fromDate"));
		String date2 = BaseUI.getTextFromInputBox(Locator.lookupElement("advancedSearch_toDate"));
		String todaysDate = ImportData.return_TodaysDate_FormattedForWebsite();
		String searchCrit_valueTextBox = BaseUI
				.getTextFromInputBox(Locator.lookupElement("advancedSearch_QueryBuilder_Value_ByRowNumber", "1", null));

		BaseUI.baseStringCompare("From Date", todaysDate, date1);
		BaseUI.baseStringCompare("To Date", todaysDate, date2);
		BaseUI.baseStringCompare("Work Group Selector Text", "Selected: " + workgroup, workgroupSelectorValue);

		paymentTypeDropdown.verify_SelectedDropdownValue(paymentType);
		paymentSourceDropdown.verify_SelectedDropdownValue(paymentSource);
		sortByDropdown.verify_SelectedDropdownValue(sortBy);
		savedQueryDropdown.verify_SelectedDropdownValue(queryName);
		fieldDropdown.verify_SelectedDropdownValue(searchCrit_field);
		operatorDropdown.verify_SelectedDropdownValue(searchCrit_Operator);
		BaseUI.baseStringCompare("Search Criteria Value field.", searchCrit_Value, searchCrit_valueTextBox);

		ArrayList<String> selectedFields = returnSelectedFieldsList();

		BaseUI.verify_true_AndLog(selectedFields.size() == 1, "Selected Fields count was correct.",
				"Selected Fields count was NOT correct.");
		BaseUI.verify_true_AndLog(selectedFields.get(0).equals(displayField), "Found correct Display field.",
				"Display field NOT correct.");
	}

	public static String return_Text_FromPDF_Field(String field) {
		WebElement pdfTextElement = Locator.lookupElement("advancedSearch_BatchPDF_FindValue_ByText", field, null);
		String pdfText = BaseUI.getTextFromField(pdfTextElement).trim();

		return pdfText;
	}

	// Verify Default Values on Advanced Search page.
	public static void verify_DefaultValues_forAdvancedSearch() throws Exception {

		String workgroupSelectorValue = workGroupSelector.return_SelectedText();

		String date1 = BaseUI.getTextFromInputBox(Locator.lookupElement("advancedSearch_fromDate"));
		String date2 = BaseUI.getTextFromInputBox(Locator.lookupElement("advancedSearch_toDate"));
		String todaysDate = ImportData.return_TodaysDate_FormattedForWebsite();

		BaseUI.baseStringCompare("From Date", todaysDate, date1);
		BaseUI.baseStringCompare("To Date", todaysDate, date2);
		BaseUI.baseStringCompare("Work Group Selector Text", "Select Workgroup", workgroupSelectorValue);

		paymentTypeDropdown.verify_SelectedDropdownValue("-- All --");
		paymentSourceDropdown.verify_SelectedDropdownValue("-- All --");

		savedQueryDropdown.verify_SelectedDropdownValue("Select Query...");
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("advancedSearch_QueryBuilder_Field_ByRowNumber", "1", null));
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("advancedSearch_QueryBuilder_Operator_ByRowNumber", "1", null));
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("advancedSearch_QueryBuilder_Value_ByRowNumber", "1", null));

		ArrayList<String> selectedFields = returnSelectedFieldsList();

		BaseUI.assertThat("selectedFields", selectedFields, Is.emptyCollection());

		sortByDropdown.verify_SelectedDropdownValue("Select one...");
	}

	public static void addDisplayField(String displayFieldToAdd) throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_AvailableListByText", displayFieldToAdd, null));
		Thread.sleep(100);
		click_MoveSelectedFromAvailableList();
	}

	public static void add_All_AvailableFields() throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_MoveAllAvailbleFieldsButton"));
		Thread.sleep(400);
	}

	public static ArrayList<String> return_AvailableFieldsList() {
		ArrayList<WebElement> availableFields_List = Locator.lookup_multipleElements("advancedSearch_AvailableList",
				null, null);
		ArrayList<String> availableFields_Text = new ArrayList<String>();

		for (WebElement availableField : availableFields_List) {
			availableFields_Text.add(BaseUI.getTextFromField(availableField));
		}

		return availableFields_Text;
	}

	public static ArrayList<String> returnSelectedFieldsList() {
		ArrayList<WebElement> selectedFields_List = Locator.lookup_multipleElements("advancedSearch_SelectedList", null,
				null);
		ArrayList<String> selectedFields_Text = new ArrayList<String>();

		for (WebElement selectedField : selectedFields_List) {
			selectedFields_Text.add(BaseUI.getTextFromField(selectedField));
		}

		return selectedFields_Text;
	}

	public static void save_Query(String queryName, String queryDescription) throws Exception {
		click_SaveQuery();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("advancedSearch_AddQuery_NameTextbox"), queryName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("advancedSearch_AddQuery_Description"), queryDescription);
		BaseUI.click(Locator.lookupElement("advancedSearch_AddQuery_Save"));

		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_SaveQuery() throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_SaveQueryButton"));
		Thread.sleep(5000);
	}

	public static void click_MoveSelectedFromAvailableList() throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_MoveToSelectedFieldsList"));
		Thread.sleep(300);
	}

	public static void click_RemoveAll_SelectedFields() throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_RemoveAllSelectedFieldsButton"));
		Thread.sleep(300);
	}

	public static void click_AdvancedSearch_Breadcrumb() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupElement("advancedSearch_Results_BreadcrumbHeaderLink_AdvancedSearch"));
		} else {
			BaseUI.click(Locator.lookupElement("advancedSearch_Results_BreadcrumbHeaderLink_AdvancedSearch"));
		}
		Navigation.waitFor_AdvancedSearchPage();
		// Navigation.wait_for_LoadingSpinner_ToAppear(4);
		// Navigation.wait_for_LoadingSpinner_ToDisappear();
		// Thread.sleep(1500);
	}

	public static void click_SearchResults_Breadcrumb() throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_Results_BreadcrumbHeaderLink_SearchResults"));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void click_ClearSearchButton() throws Exception {
		WebElement clearSearchButton = Locator.lookupElement("advancedSearch_ClearSearchButton");
		BaseUI.scroll_to_element(clearSearchButton);
		BaseUI.click_js(clearSearchButton);
		Navigation.wait_for_LoadingSpinner_ToAppear(1);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		// Thread.sleep(1000);
	}

	// Takes significantly longer than just using click_ClearSearchButton()
	public static void clear_Form_Manually() throws Exception {
		if (BaseUI.elementExists("advancedSearch_Results_BreadcrumbHeaderLink_AdvancedSearch", null, null)) {
			click_AdvancedSearch_Breadcrumb();
		}

		if (!AdvancedSearch.paymentTypeDropdown.return_EntityValue().contains("All")) {
			paymentTypeDropdown.reset_Dropdown();
		}

		if (!AdvancedSearch.paymentSourceDropdown.return_EntityValue().contains("All")) {
			paymentSourceDropdown.reset_Dropdown();
		}

		if (!AdvancedSearch.sortByDropdown.return_EntityValue().equals("Select one...")) {
			sortByDropdown.reset_Dropdown();
		}

		ArrayList<WebElement> QueryBuilder_ClearLinks = Locator
				.lookup_multipleElements("advancedSearch_QueryBuilder_ListOf_ClearLinks", null, null);
		if (QueryBuilder_ClearLinks.size() > 0) {
			for (int i = 0; i < QueryBuilder_ClearLinks.size(); i++) {
				BaseUI.click(Locator.lookupElement("advancedSearch_QueryBuilder_ListOf_ClearLinks"));
			}
		}

		click_RemoveAll_SelectedFields();
	}

	// pass in deposit date in format MM/dd/yyyy
	public static void verify_Results_HaveCorrect_DepositDate(String depositDate) throws Exception {
		TableData searchResults = return_SearchResults();

		BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Search Results found.", "Search Results not found.");

		for (HashMap<String, String> tableRow : searchResults.data) {
			BaseUI.baseStringCompare("Deposit Date", depositDate, tableRow.get("Deposit Date"));
		}
	}

	public static void click_Search() throws Exception {
		BaseUI.click(Locator.lookupElement("advancedSearch_SearchButton"));
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void enter_Date(WebElement dateField, String date) {
		BaseUI.click(dateField);
		dateField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		dateField.sendKeys(date);

	}

	public static void click_AddCriteria() throws Exception {

		BaseUI.click(Locator.lookupElement("advancedSearch_AddSearchCriteria_Button"));
		Thread.sleep(300);
	}

	// Row index starts at 1.
	public static void add_Search_Criteria(Integer rowIndex, String field, String operator, String value)
			throws Exception {
		click_AddCriteria();

		Dropdown fieldDropdown = new Dropdown("advancedSearch_QueryBuilder_Field_ByRowNumber", rowIndex.toString(),
				null);
		Dropdown operatorDropdown = new Dropdown("advancedSearch_QueryBuilder_Operator_ByRowNumber",
				rowIndex.toString(), null);
		TextBox valueTextBox = new TextBox("advancedSearch_QueryBuilder_Value_ByRowNumber", rowIndex.toString(), null);

		if (field != null) {
			fieldDropdown.enterText_IntoSearchTextbox(field);

			BaseUI.click(Locator.lookupElement("dropdwn_ListItem_AfterFilter_ByText", field, null));
			Thread.sleep(300);
			// fieldDropdown.select_EntityValue(field);
			// BaseUI.click(Locator.lookupElement("dropdwn_ListItem_ByText",
			// field, null));
			// Thread.sleep(400);
		}
		if (operator != null) {
			operatorDropdown.select_EntityValue(operator);
		}
		if (value != null) {
			valueTextBox.enterTextIntoField_AndTab(value);
		}

	}

	// Create a simple saved query by passing in all of the variables listed.
	public static void create_SavedQuery(String workgroup, String date1, String date2, String paymentType,
			String paymentSource, String sortBy, String searchCrit_field, String searchCrit_Operator,
			String searchCrit_Value, String displayField, String queryName, String queryDescription) throws Exception {
		workGroupSelector.search_forText(workgroup);
		setDateRange(date1, date2);
		paymentTypeDropdown.select_EntityValue(paymentType);
		paymentSourceDropdown.select_EntityValue(paymentSource);
		sortByDropdown.select_EntityValue(sortBy);
		add_Search_Criteria(1, searchCrit_field, searchCrit_Operator, searchCrit_Value);
		addDisplayField(displayField);

		save_Query(queryName, queryDescription);
		Thread.sleep(500);
	}

	public static void verify_Field_Equals_Value(Integer indexNumber, String field, String value) throws Exception {
		String operator = "Equals";

		verify_Field_Operator_Value(indexNumber, field, operator, value);
	}

	public static void verify_Field_IsLessThan_Value(Integer indexNumber, String field, String value) throws Exception {
		String operator = "Is Less Than";

		verify_Field_GreaterThan_OrLessThan_Value(indexNumber, field, operator, value);
	}

	public static void verify_Field_IsGreaterThan_Value(Integer indexNumber, String field, String value)
			throws Exception {
		String operator = "Is Greater Than";

		verify_Field_GreaterThan_OrLessThan_Value(indexNumber, field, operator, value);
	}

	public static void verify_Field_BeginsWith_Value(Integer indexNumber, String field, String value) throws Exception {

		String operator = "Begins With";

		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData results = return_SearchResults();

		verify_ResultsPage_BeginsWith_Value(results, field, value);
	}

	public static void verify_ResultsPage_BeginsWith_Value(TableData results, String field, String value)
			throws Exception {

		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			String tableValue = tableRow.get(field);
			String shortenedValue = tableValue.substring(0, value.length());

			BaseUI.baseStringCompare(field, value, shortenedValue);
		}
	}

	public static void verify_Field_Contains_Value(Integer indexNumber, String field, String value) throws Exception {

		String operator = "Contains";

		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData results = return_SearchResults();

		verify_ResultsPage_Contains_Value(results, field, value);
	}

	public static void verify_ResultsPage_Contains_Value(TableData results, String field, String value)
			throws Exception {

		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			BaseUI.baseStringPartialCompare(field, value, tableRow.get(field));
		}
	}

	public static void verify_Field_EndsWith_Value(Integer indexNumber, String field, String value) throws Exception {

		String operator = "Ends With";

		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData results = return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			String fieldEndsWith = tableRow.get(field);
			fieldEndsWith = fieldEndsWith.substring(fieldEndsWith.length() - value.length(), fieldEndsWith.length());

			BaseUI.baseStringCompare(field, value, fieldEndsWith);
		}
	}

	public static void verify_QueryBuilder_ReturnsNoResults(Integer indexNumber, String field, String operator,
			String value) throws Exception {
		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();

		BaseUI.verifyElementAppears(Locator.lookupElement("advancedSearch_NoSearchResults"));
	}

	public static void verify_QueryBuilder_MissingRequiredField(Integer indexNumber, String field, String operator,
			String value) throws Exception {
		verify_QueryBuilder_ErrorMessage(indexNumber, field, operator, value, "Please fill in all required criteria.");

	}

	public static void verify_QueryBuilder_NonNumericChars_Error(Integer indexNumber, String field, String operator,
			String value) throws Exception {
		verify_QueryBuilder_ErrorMessage(indexNumber, field, operator, value,
				"The Value for the field you selected must be numeric.");

	}

	public static void verify_QueryBuilder_ErrorMessage(Integer indexNumber, String field, String operator,
			String value, String errorMessage) throws Exception {
		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();

		BaseUI.verifyElementAppears(Locator.lookupElement("entityUserErrorPopup_ErrorMessage"));
		BaseUI.verifyElementHasExpectedText("entityUserErrorPopup_ErrorMessage", errorMessage);
	}

	public static void add_DisplayFields(String[] displayFields) throws Exception {
		for (String field : displayFields) {
			addDisplayField(field);
		}
	}

	public static void verify_Correct_DisplayFields(String[] displayFields) throws Exception {

		ArrayList<WebElement> headerList = Locator.lookup_multipleElements("advancedSearch_TableHeaders", null, null);
		ArrayList<String> headerTextList = new ArrayList<String>();
		for (WebElement header : headerList) {
			headerTextList.add(BaseUI.getTextFromField(header));
		}

		BaseUI.verify_true_AndLog(displayFields.length == headerTextList.size(), "Lists matched in size.",
				"Lists did NOT match.");

		for (String displayField : displayFields) {
			BaseUI.verify_true_AndLog(headerTextList.contains(displayField),

					MessageFormat.format("Found Display Field: {0} on Results page.", displayField),
					MessageFormat.format("Did NOT find Display Field: {0} on Results page.", displayField));
		}
	}

	public static void verify_Field_Operator_Value(Integer indexNumber, String field, String operator, String value)
			throws Exception {
		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();
		select_ShowRowsDropdownValue(100);
		TableData results = return_SearchResults();

		verify_ResultsPage_ResultsEqualExpected(results, field, value);

	}

	public static void verify_ResultsPage_ResultsEqualExpected(TableData results, String field, String value)
			throws Exception {

		results.remove_Character("$");
		results.remove_Character(",");
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			BaseUI.baseStringCompare(field, value, tableRow.get(field));
		}
	}

	public static void verify_Field_GreaterThan_OrLessThan_Value(Integer indexNumber, String field, String operator,
			String value) throws Exception {

		add_Search_Criteria(indexNumber, field, operator, value);
		add_All_AvailableFields();
		click_Search();
		select_ShowRowsDropdownValue(100);
		TableData results = return_SearchResults();

		verify_ResultsPage_GreaterThan_Or_LessThan(results, field, operator, value);
	}

	public static void verify_ResultsPage_GreaterThan_Or_LessThan(TableData results, String field, String operator,
			String value) throws Exception {

		results.remove_Character("$");
		results.remove_Character(",");
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			Double fieldValue = Double.parseDouble(tableRow.get(field));

			if (operator.equals("Is Less Than")) {

				// Integer fieldValue = Integer.parseInt(tableRow.get(field));
				BaseUI.verify_true_AndLog(fieldValue < Double.parseDouble(value),
						MessageFormat.format("{0} is Less Than {1}", tableRow.get(field), value),
						MessageFormat.format("{0} is NOT Less Than {1}", tableRow.get(field), value));
			} else {

				BaseUI.verify_true_AndLog(fieldValue > Double.parseDouble(value),
						MessageFormat.format("{0} is Greater Than {1}", tableRow.get(field), value),
						MessageFormat.format("{0} is NOT Greater Than {1}", tableRow.get(field), value));
			}
		}
	}

	public static void verify_ResultsPage_GreaterThanOrEqualTo_Or_LessThanOrEqualTo(TableData results, String field,
			String operator, String value) throws Exception {

		results.remove_Character("$");
		results.remove_Character(",");
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			Double fieldValue = Double.parseDouble(tableRow.get(field));

			if (operator.equals("Is Less Than or Equal To")) {

				// Integer fieldValue = Integer.parseInt(tableRow.get(field));
				BaseUI.verify_true_AndLog(fieldValue <= Double.parseDouble(value),
						MessageFormat.format("{0} is Less Than or equal to {1}", tableRow.get(field), value),
						MessageFormat.format("{0} is NOT Less Than or equal to {1}", tableRow.get(field), value));
			} else {

				BaseUI.verify_true_AndLog(fieldValue >= Double.parseDouble(value),
						MessageFormat.format("{0} is Greater Than or equal to {1}", tableRow.get(field), value),
						MessageFormat.format("{0} is NOT Greater Than or equal to {1}", tableRow.get(field), value));
			}
		}
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedAscending(String dropdown_Value, String columnName)
			throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		TableData sorted_Results = return_SearchResults();
		sorted_Results.sort_ByColumn_Ascending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedDescending(String dropdown_Value, String columnName)
			throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		TableData sorted_Results = return_SearchResults();
		sorted_Results.sort_ByColumn_Descending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static WebElement return_Link_ByColumnName_AndColumnRow(String columnName, Integer rowNumber) {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("advancedSearch_ResultRowByIndex", rowNumber.toString(), null));

		return cellList.get(headerIndex);
	}

	// Cannot get this to work. None of the hover methods are causing the tool
	// tip to appear.
	public static void return_Tooltip_Text(String columnName, Integer rowNumber) throws Exception {
//		Actions ToolTip1 = new Actions(Browser.driver);

//		ArrayList<String> headers = available_Columns();
//		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("advancedSearch_ResultRowByIndex", rowNumber.toString(), null));

		// BaseUI.click(cellList.get(headerIndex));

		// BaseUI.elementHover(cellList.get(headerIndex));
		//
		//
		// ToolTip1.clickAndHold(cellList.get(headerIndex)).perform();
		// Thread.sleep(3000);
		// Navigation.wait_for_LoadingSpinner_ToDisappear();
		//
		// WebElement googleLogo =
		// Browser.driver.findElement(By.xpath("//div[@id='hplogo']"));
		//
		// Thread.sleep(2000);
		//
		// ToolTip1.clickAndHold(googleLogo).perform();

	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedAscending_Numeric(String dropdown_Value,
			String columnName) throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		Results.remove_Character("$");
		Results.remove_Character(",");

		TableData sorted_Results = return_SearchResults();
		sorted_Results.remove_Character("$");
		sorted_Results.remove_Character(",");
		sorted_Results.sort_ByColumn_numeric_Ascending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	public static void verify_SortBy_Dropdown_SearchResults_SortedDescending_Numeric(String dropdown_Value,
			String columnName) throws Exception {
		sortByDropdown.select_EntityValue(dropdown_Value);
		click_Search();
		select_ShowRowsDropdownValue(100);

		TableData Results = return_SearchResults();
		Results.remove_Character("$");
		Results.remove_Character(",");

		TableData sorted_Results = return_SearchResults();
		sorted_Results.remove_Character("$");
		sorted_Results.remove_Character(",");
		sorted_Results.sort_ByColumn_numeric_Descending(columnName);

		BaseUI.verify_TableColumns_Match(columnName, sorted_Results.data, Results.data);
	}

	// List of Transaction numbers in results list.
	public static ArrayList<String> available_Transactions() {
		ArrayList<WebElement> transactionsElementList = Locator
				.lookup_multipleElements("advancedSearch_TransactionsList", null, null);
		ArrayList<String> transactions = new ArrayList<String>();
		for (WebElement transaction : transactionsElementList) {
			transactions.add(transaction.getText());
		}

		return transactions;

	}

	public static ArrayList<String> available_Columns() {
		ArrayList<WebElement> headerElements = Locator.lookup_multipleElements("advancedSearch_TableHeaders", null,
				null);
		ArrayList<String> headers = new ArrayList<String>();
		for (WebElement header : headerElements) {
			String headerText = header.getAttribute("innerText");
			headers.add(headerText);
		}

		return headers;
	}

	public static Integer headerCount() {
		ArrayList<WebElement> headers = Locator.lookup_multipleElements("advancedSearch_TableHeaders", null, null);

		return headers.size();
	}

	public static Integer row_Count() {
		ArrayList<WebElement> rows = Locator.lookup_multipleElements("advancedSearch_Rows", null, null);

		return rows.size();
	}

	public static void select_ShowRowsDropdownValue(Integer numberToShow) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("advancedSearch_ShowResultNumberDropdown"),
				numberToShow.toString());
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// rowNumber starts at 1.
	public static void click_Cell_Link(String columnName, Integer rowNumber) throws Exception {
		ArrayList<String> headers = available_Columns();
		Integer headerIndex = headers.indexOf(columnName);

		ArrayList<WebElement> cellList = new ArrayList<WebElement>();
		cellList.addAll(Locator.lookup_multipleElements("advancedSearch_ResultRowByIndex", rowNumber.toString(), null));

		BaseUI.click(cellList.get(headerIndex));
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();

	}

	public static void verify_Breadcrumb(String[] crumbArray) {

		// ArrayList<WebElement> breadcrumbElements=
		// Locator.lookup_multipleElements("advancedSearch_Breadcrumb_List",
		// null, null);
		String breadCrumb = BaseUI.getTextFromField(Locator.lookupElement("advancedSearch_Breadcrumb_CurrentPage"));
		String[] breadCrumbSplit = breadCrumb.split(">");

		BaseUI.verify_true_AndLog(crumbArray.length == breadCrumbSplit.length, "Bread Crumb had expected length.",
				"Bread Crumb length did NOT match.");

		for (int i = 0; i < crumbArray.length; i++) {
			// String elementText =
			// BaseUI.getTextFromField(breadcrumbElements.get(i));
			BaseUI.baseStringCompare("Breadcrumb Text", crumbArray[i], breadCrumbSplit[i].trim());
		}

		// BaseUI.baseStringCompare("Current Page in Breadcrumb",
		// crumbArray[crumbArray.length], currentPage);

	}

	public static TableData return_SearchResults() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = available_Columns();

		if (Browser.currentBrowser.equals("chrome")) {

			//
			// ArrayList<String> newHeaders = new ArrayList<String>();
			// newHeaders.add("EmptyBeginningCell");
			// newHeaders.addAll(headers);

			tableData = BaseUI.tableExtractorV2("advancedSearch_ResultsTable_Body", headers.toArray(new String[0]));
			// tableData.replace_Null_With_Empty();

		} else {
			tableData = BaseUI.tableExtractor_ByCell("advancedSearch_ResultCells_Version2",
					headers.toArray(new String[0]));

			// tableData = return_SearchResults_old();
		}
		if (tableData.data.get(0).containsKey("Batch Id")) {
			tableData.replaceKey_ForGivenKey("Batch Id", "Batch ID");
		}

		return tableData;
	}

	public static TableData return_SearchResults_old() throws Exception {
		TableData tableData = new TableData();
		ArrayList<String> headers = available_Columns();

		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(Locator.lookup_multipleElements("advancedSearch_ResultRows", null, null));
		for (Integer i = 1; i <= rowList.size(); i++) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			ArrayList<WebElement> cellList = new ArrayList<WebElement>();
			cellList.addAll(Locator.lookup_multipleElements("advancedSearch_ResultRowByIndex", i.toString(), null));

			for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++) {
				String cellText = cellList.get(headerIndex).getAttribute("innerText");

				dataRow.put(headers.get(headerIndex), cellText);

			}
			if (dataRow.containsKey("Batch Id")) {
				dataRow.put("Batch ID", dataRow.get("Batch Id"));
				dataRow.remove("Batch Id");
			}
			tableData.data.add(dataRow);
		}

		tableData.replace_Null_With_Empty();
		// tableData.replace_DateFormat_shortened();
		// tableData.remove_TrailingZeros_fromNumbers();
		return tableData;
	}

	public static TableData returnCSV_Results() throws Exception {
		TableData tableData = new TableData();
		String downloadDirectory = "c:\\DAF\\downloads\\";
		String fileName = "search_results.csv";

		// If our Download Directory doesn't work we'll add it.
		File directory = new File(String.valueOf(downloadDirectory));
		if (!directory.exists()) {
			directory.mkdir();
		}

		// Delete previous csv files before we download our new one.
		File file = new File(downloadDirectory + fileName);
		try {
			Files.deleteIfExists(file.toPath());
		} catch (Exception e) {
			BaseUI.verify_true_AndLog(false, "", "Unable to delete search_results.csv file.");
		}

		BaseUI.click(Locator.lookupElement("advancedSearch_DownloadAsText_Button"));
		Thread.sleep(2000);
		wait_For_CSV_To_Download(downloadDirectory + fileName);

		Config_DataRetrieval.retrieve_DataSheet(downloadDirectory, fileName);

		tableData = Config_DataRetrieval.tableData;

		return tableData;
	}

	public static void wait_For_CSV_To_Download(String fileString) throws Exception {

		File file = new File(fileString);
		for (int i = 0; i < 5; i++) {
			if (file.exists()) {
				break;
			} else {
				Thread.sleep(500);
			}

		}

	}

	public static String getText(WebElement element) {
		return (String) ((JavascriptExecutor) Browser.driver).executeScript("return jQuery(arguments[0]).text();",
				element);
	}

	public static Boolean isValidDate(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (Exception pe) {
			return false;
		}
		return true;
	}

	public static void sort_ByHeader_Ascending(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("advancedSearch_TableHeader_ByText", headerText, null);
		if (headerElement.getAttribute("aria-sort") == null
				|| !BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "ascending")) {
			click_Header_ByText(headerText);

			if (!BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "ascending")) {
				click_Header_ByText(headerText);
			}

		}

	}

	public static void sort_ByHeader_Descending(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("advancedSearch_TableHeader_ByText", headerText, null);
		if (headerElement.getAttribute("aria-sort") == null
				|| !BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "descending")) {
			click_Header_ByText(headerText);

			if (!BaseUI.elementHasExpectedAttribute(headerElement, "aria-sort", "descending")) {
				click_Header_ByText(headerText);
			}
		}
	}

	public static void click_Header_ByText(String headerText) throws Exception {
		WebElement headerElement = Locator.lookupElement("advancedSearch_TableHeader_ByText", headerText, null);
		BaseUI.scroll_to_element(headerElement);
		BaseUI.click(headerElement);
		Thread.sleep(1500);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	// Date must be in format yyyyMMdd
	public static TableData return_StubData(String date, String workgroup) throws Exception, SQLException {
		/***** Query for Stub Data *****/

		String query = "declare @entity varchar(99)" + "\n" + "declare @date int" + "\n" + "declare @Workgroup int"
				+ "\n" + "declare @batch int" + "\n" + "declare @trans int" + "\n"
				+ "set @entity    = 'AutomationImports'" + "\n" + "set @Workgroup = '" + workgroup + "'" + "\n"
				+ "set @date      = '" + date + "'" + "\n" + "set @batch     = '5917'" + "\n" + "set @trans     = '1'"
				+ "\n" + "SELECT" + "\n" + "f.EntityName as 'Entity'" + "\n"
				+ ",CONCAT(b.SiteClientAccountID,' - ', (select top 1 LongName from [WFSDB_R360].RecHubData.dimClientAccounts z where z.MostRecent = 1 and b.SiteClientAccountID = z.SiteClientAccountID and b.sitebankid = z.sitebankid)) as 'Workgroup'"
				+ "\n" + ",e.SiteBankID as 'Bank ID'" + "\n" + ",e.BankName as 'Bank'" + "\n"
				+ ",c.LongName as 'Payment Type'" + "\n" + ",d.LongName as 'Payment Source'" + "\n"
				+ ",a.[DepositDateKey] as 'Deposit Date'" + "\n" + ",a.[SourceBatchID] as 'Batch ID'" + "\n"
				+ ",a.[BatchNumber] as 'Batch Number'" + "\n" + ",a.[TransactionID] as 'Transaction'" + "\n"
				+ ",a.[Amount] as 'Payment Amount'" + "\n" + ",a.BatchSequence    as 'Invoice Batch Sequence'" + "\n"
				+ ",i.IsCheck" + "\n" + ",i.UILabel" + "\n" + ",h.DataEntryValue" + "\n"
				+ "FROM [WFSDB_R360].[RecHubData].[factstubs] a" + "\n"
				+ "inner join WFSDB_R360.RecHubData.dimClientAccounts b on a.clientaccountkey = b.ClientAccountKey"
				+ "\n"
				+ "inner join [WFSDB_R360].[RecHubData].[dimBatchPaymentTypes] c on a.BatchPaymentTypeKey = c.BatchPaymentTypeKey"
				+ "\n" + "inner join [WFSDB_R360].RecHubData.dimBatchSources d on a.BatchSourceKey = d.BatchSourceKey"
				+ "\n" + "inner join WFSDB_R360.RecHubData.dimBanks e on a.BankKey = e.BankKey" + "\n"
				+ "inner join [WFSDB_R360].[RecHubuser].[OLWorkgroups] g on g.SiteClientAccountID = b.SiteClientAccountID and g.SiteBankID = e.SiteBankID"
				+ "\n" + "inner join RAAM.dbo.Entities f on f.EntityID = g.EntityID" + "\n"
				+ "left join WFSDB_R360.RecHubData.factDataEntryDetails h on a.ClientAccountKey = h.ClientAccountKey and a.batchsequence = h.batchsequence and a.TransactionID = h.TransactionID and a.SourceBatchID = h.SourceBatchID and h.DepositDateKey = a.DepositDateKey"
				+ "\n"
				+ "left join wfsdb_r360.RecHubData.dimWorkgroupDataEntryColumns i on h.WorkgroupDataEntryColumnKey = i.WorkgroupDataEntryColumnKey"
				+ "\n" + "where a.IsDeleted = 0" + "\n" + "and a.DepositDateKey = @date" + "\n"
				// --and f.entityname = @entity
				+ "and b.SiteClientAccountID = @workgroup" + "\n"
				// --and a.SourceBatchID = @batch
				// --and a.TransactionID = @trans
				+ "group by i.WorkgroupDataEntryColumnKey, f.EntityName, e.SiteBankID ,e.BankName ,c.LongName ,d.LongName,b.SiteClientAccountID ,e.SiteBankID, b.sitebankid, a.DepositDateKey, a.[SourceBatchID], a.[BatchNumber],a.[TransactionID],a.[Amount],i.IsCheck ,i.UILabel, h.DataEntryValue,  a.BatchSequence"
				+ "\n" + "order by a.TransactionID";

		TableData data = DatabaseConnection.runSQLServerQuery(query);
		data = format_Data(data);
		return data;
	}

	// Date must be in format yyyyMMdd
	public static TableData return_PaymentData(String date, String workgroup) throws Exception, SQLException {
		/***** Query for Payment Data *****/

		String query = "declare @entity varchar(99)" + "\n" + "declare @date int" + "\n" + "declare @Workgroup int"
				+ "\n" + "declare @batch int" + "\n" + "declare @trans int" + "\n"

				+ "set @entity    = 'AutomationImports'" + "\n" + "set @Workgroup = '" + workgroup + "'" + "\n"
				+ "set @date      = '" + date + "'" + "\n" + "set @batch     = '5917'" + "\n" + "set @trans     = '1'"
				+ "\n"

				+ "SELECT " + "\n" + "f.EntityName       as 'Entity'" + "\n"
				+ ",CONCAT(b.SiteClientAccountID,' - ', (select top 1 LongName from [WFSDB_R360].RecHubData.dimClientAccounts z where z.MostRecent = 1 and b.SiteClientAccountID = z.SiteClientAccountID and b.sitebankid = z.sitebankid)) as 'Workgroup'"
				+ "\n" + ",e.SiteBankID       as 'Bank ID'" + "\n" + ",e.BankName         as 'Bank'" + "\n"
				+ ",c.LongName         as 'Payment Type'" + "\n" + " ,d.LongName         as 'Payment Source'" + "\n"
				+ " ,a.[DepositDateKey] as 'Deposit Date'" + "\n" + " ,a.[SourceBatchID]  as 'Batch ID'" + "\n"
				+ " ,a.[BatchNumber]    as 'Batch Number'" + "\n" + " ,a.[TransactionID]  as 'Transaction'" + "\n"
				+ " ,a.[Amount]         as 'Payment Amount'" + "\n" + " ,[RoutingNumber]    as 'R/T'" + "\n"
				+ " ,[Account]          as 'Account Number'" + "\n" + " ,[Serial]           as 'Check/Trace/Ref Number'"
				+ "\n" + " ,[RemitterName]     as 'Payer'" + "\n" + " ,a.CheckSequence    as 'Payment Sequence'" + "\n"
				+ ",a.BatchSequence    as 'Payment Batch Sequence'" + "\n" + ",j.DDA" + "\n" + " ,i.UILabel" + "\n"
				+ " ,h.DataEntryValue" + "\n" + "FROM [WFSDB_R360].[RecHubData].[factChecks] a" + "\n"
				+ "inner join WFSDB_R360.RecHubData.dimClientAccounts b on a.clientaccountkey = b.ClientAccountKey"
				+ "\n"
				+ "inner join [WFSDB_R360].[RecHubData].[dimBatchPaymentTypes] c on a.BatchPaymentTypeKey = c.BatchPaymentTypeKey"
				+ "\n" + "inner join [WFSDB_R360].RecHubData.dimBatchSources d on a.BatchSourceKey = d.BatchSourceKey"
				+ "\n" + " inner join WFSDB_R360.RecHubData.dimBanks e on a.BankKey = e.BankKey" + "\n"
				+ "inner join [WFSDB_R360].[RecHubuser].[OLWorkgroups] g on g.SiteClientAccountID = b.SiteClientAccountID and g.SiteBankID = e.SiteBankID"
				+ "\n" + " inner join RAAM.dbo.Entities f on f.EntityID = g.EntityID" + "\n"
				+ " inner join WFSDB_R360.RecHubData.dimDDAs j on j.DDAKey = a.DDAKey" + "\n"
				+ " left join WFSDB_R360.RecHubData.factDataEntryDetails h on a.ClientAccountKey = h.ClientAccountKey and a.batchsequence = h.batchsequence and a.TransactionID = h.TransactionID and a.SourceBatchID = h.SourceBatchID-- or k.batchsequence = h.batchsequence and k.TransactionID = h.TransactionID and k.SourceBatchID = h.SourceBatchID"
				+ "\n"
				+ " left join wfsdb_r360.RecHubData.dimWorkgroupDataEntryColumns i on h.WorkgroupDataEntryColumnKey = i.WorkgroupDataEntryColumnKey"
				+ "\n"

				+ "where a.IsDeleted = 0" + "\n" + "and a.DepositDateKey = @date " + "\n"
				+ "--and f.entityname = @entity" + "\n" + "and b.SiteClientAccountID = @workgroup " + "\n"
				+ "--and a.SourceBatchID = @batch" + "\n" + "--and a.TransactionID = @trans" + "\n"

				+ "group by i.WorkgroupDataEntryColumnKey ,f.EntityName ,b.SiteClientAccountID,b.sitebankid ,e.SiteBankID ,e.BankName ,c.LongName ,d.LongName ,a.[DepositDateKey] ,a.[SourceBatchID] ,a.[BatchNumber],a.[TransactionID] ,a.[Amount] ,[RoutingNumber],[Account] ,[Serial],[RemitterName],j.DDA,i.IsCheck ,i.UILabel,h.DataEntryValue ,a.BatchSequence ,a.CheckSequence"
				+ "\n"

				+ "order by a.SourceBatchID,a.TransactionID";

		TableData data = DatabaseConnection.runSQLServerQuery(query);
		data = format_Data(data);
		return data;
	}

	public static void verify_PaymentType_Filter(String paymentType) throws Exception {
		String field = "Payment Type";

		TableData results;

		paymentTypeDropdown.select_EntityValue(paymentType);
		click_Search();
		select_ShowRowsDropdownValue(100);
		results = AdvancedSearch.return_SearchResults();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Search Results Found.", "Search Results NOT found.");

		for (HashMap<String, String> tableRow : results.data) {
			BaseUI.baseStringCompare(field, paymentType, tableRow.get(field));
		}

	}

	// Validates the Sort Ascending alpha-numeric by the passed in header text.
	public static void verify_SearchResults_SortAscending_ByColumnHeader(String headerText) throws Exception {
		TableData results = return_SearchResults();
		results.sort_ByColumn_Ascending(headerText);

		sort_ByHeader_Ascending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	// Validates the Sort Descending alpha-numeric by the passed in header text.
	public static void verify_SearchResults_SortDescending_ByColumnHeader(String headerText) throws Exception {
		TableData results = return_SearchResults();
		results.sort_ByColumn_Descending(headerText);

		sort_ByHeader_Descending(headerText);
		TableData newTableData = AdvancedSearch.return_SearchResults();

		BaseUI.verify_TableColumns_Match(headerText, results.data, newTableData.data);
	}

	private static TableData format_Data(TableData tableData) {

		tableData.replace_Null_With_Empty();

		TableData newTableData = new TableData();

		for (int i = 0; i < tableData.data.size(); i++) {
			String uiLabel = tableData.data.get(i).get("UILabel");
			String dataEntryValue = tableData.data.get(i).get("DataEntryValue");
			tableData.data.get(i).remove("UILabel");
			tableData.data.get(i).remove("DataEntryValue");
			Boolean tableRowMatchFound = false;

			if (newTableData.data.size() > 0) {
				for (int newI = 0; newI < newTableData.data.size(); newI++) {
					Boolean tableRowMatches = true;

					for (String key : tableData.data.get(i).keySet()) {
						String oldTableValue = tableData.data.get(i).get(key);
						String newTableValue = newTableData.data.get(newI).get(key);
						if (!key.equals("DataEntryValue") && !key.equals("UILabel")
								&& !oldTableValue.equals(newTableValue)) {
							tableRowMatches = false;
							break;
						}
					}
					if (tableRowMatches) {
						newTableData.data.get(newI).put(uiLabel, dataEntryValue);
						tableRowMatchFound = true;
						break;
					}
				}
				if (!tableRowMatchFound) {
					tableData.data.get(i).put(uiLabel, dataEntryValue);
					newTableData.data.add(tableData.data.get(i));

				}

			} else {
				tableData.data.get(i).put(uiLabel, dataEntryValue);
				newTableData.data.add(tableData.data.get(i));
			}

		}

		return newTableData;
	}

	public static TableData return_PDF_Data() throws Exception {
		TableData pdfData = new TableData();

		WebElement pdfContainer = Locator.lookupElement("advancedSearch_PDFContainer");
		pdfContainer.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		Thread.sleep(200);
		pdfContainer.sendKeys(Keys.chord(Keys.CONTROL, "c"));
		Thread.sleep(200);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		String result = (String) clipboard.getData(DataFlavor.stringFlavor);
		result = result.substring(0, 1).equals("\n") ? result.substring(1, result.length()) : result;

		String[] resultsArray = result.split("\\r?\\n");
		HashMap<String, String> tableRow = new HashMap<String, String>();
		for (int i = 0; i < resultsArray.length; i++) {
			tableRow.put(resultsArray[i].replace(":", "").trim(), resultsArray[i + 1]);
			i++;

		}
		pdfData.data.add(tableRow);

		return pdfData;
	}

	// Index for transactionLineToClick starts at 1.
	public static void verify_ClickingTransaction_TakesYouToTransactionDetails_Page(Integer transactionLineToClick)
			throws Exception {

		TableData tableRowData = AdvancedSearch.return_SearchResults();
		String workgroup = BaseUI.getTextFromField(Locator.lookupElement("advancedSearch_WorkGroupValue"));

		AdvancedSearch.click_Cell_Link("Transaction", transactionLineToClick);
		Thread.sleep(100);

		String transactionDetail_Workgroup = BaseUI.getTextFromField(Locator
				.lookupElement("advancedSearch_TransactionDetail_Header_transactionDetail_ByText", "Workgroup:", null));

		String transactionDetail_DepositDate = BaseUI.getTextFromField(Locator.lookupElement(
				"advancedSearch_TransactionDetail_Header_transactionDetail_ByText", "Deposit Date:", null));

		String transactionDetail_BatchNumber = BaseUI.getTextFromField(Locator
				.lookupElement("advancedSearch_TransactionDetail_Header_transactionDetail_ByText", "Batch:", null));

		String transactionDetail_BatchID = BaseUI.getTextFromField(Locator
				.lookupElement("advancedSearch_TransactionDetail_Header_transactionDetail_ByText", "Batch ID:", null));

		String transactionDetail_Transaction = BaseUI.getTextFromField(Locator.lookupElement(
				"advancedSearch_TransactionDetail_Header_transactionDetail_ByText", "Transaction:", null));

		BaseUI.baseStringCompare("Deposit Date", tableRowData.data.get(transactionLineToClick - 1).get("Deposit Date"),
				transactionDetail_DepositDate);

		BaseUI.baseStringCompare("Batch ID", tableRowData.data.get(transactionLineToClick - 1).get("Batch ID"),
				transactionDetail_BatchID);

		BaseUI.baseStringCompare("Batch Number", tableRowData.data.get(transactionLineToClick - 1).get("Batch Number"),
				transactionDetail_BatchNumber);

		BaseUI.baseStringCompare("Transaction", tableRowData.data.get(transactionLineToClick - 1).get("Transaction"),
				transactionDetail_Transaction);

		BaseUI.baseStringCompare("Workgroup", workgroup, transactionDetail_Workgroup);

		BaseUI.verifyElementAppears(Locator.lookupElement("advancedSearch_TransactionDetail_Header"));
	}

	// rowNumber numbers start at 1.
	public static WebElement return_BatchDetails_Cell_ByPassedInHeader_AndRowNumber(String columnName,
			Integer rowNumber) {
		ArrayList<WebElement> headerList = Locator
				.lookup_multipleElements("advancedSearch_BatchDetails_ListOfTableHeaders", null, null);

		Integer headerNumber = 0;
		for (int i = 0; i < headerList.size(); i++) {
			String headerText = BaseUI.getTextFromField(headerList.get(i));
			if (headerText.equals(columnName)) {
				headerNumber = headerNumber + 1;
			}
		}

		WebElement tableRow = Locator.lookupElement("advancedSearch_BatchDetails_TableRow_ByPassedInIndex",
				rowNumber.toString(), null);

		WebElement correctCell = tableRow.findElement(By.xpath("./td[" + headerNumber.toString() + "]"));

		return correctCell;
	}

	public static void verify_BatchDetailsCell_Title_ByHeaderText_AndRowNumber(String columnName, Integer rowNumber,
			String expectedText) throws Exception {
		WebElement elementToCheck = return_BatchDetails_Cell_ByPassedInHeader_AndRowNumber(columnName, rowNumber);

		WebElement titleElement = elementToCheck.findElement(By.xpath("./parent::tr[1]"));

		BaseUI.verifyElementHasExpectedAttributeValue(titleElement, "title", expectedText);

	}

	public static void click_BatchDetailsCell_ByHeaderText_AndRowNumber(String columnName, Integer rowNumber)
			throws Exception {
		WebElement elementToClick = return_BatchDetails_Cell_ByPassedInHeader_AndRowNumber(columnName, rowNumber);

		BaseUI.click(elementToClick);
		Thread.sleep(3000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void removeFirstFilterIfItExists() {
		WebElement firstFilterClear = Locator.lookupElement("advancedSearch_FirstCriteria_X");
		if(firstFilterClear!=null) {
			BaseUI.click(firstFilterClear);
		}
	}

	public static boolean verify_ResultsPage_Has_Selective_Print_Mode_Button() {
		return BaseUI.elementAppears(getSelectivePrintButton());
	}

	private static WebElement getSelectivePrintButton() {
		return Locator.lookupElement("advancedSearch_SelectivePrintButton");
	}

	public static void click_ResultsPage_Selective_Print_Mode_Button() {
		WebElement selectivePrintButton = getSelectivePrintButton();
		BaseUI.click(selectivePrintButton);
	}

	public static void verify_ResultsPage_Has_PDFView_Button() {
		WebElement pdfViewButton = Locator.lookupElement("advancedSearch_PDFViewButton");
		BaseUI.assertThat(BaseUI.elementAppears(pdfViewButton), Is.equalTo(true));
	}

	public static void verify_ResultsPage_Has_DownloadTest_Button() {
		WebElement downloadTestButton = Locator.lookupElement("advancedSearch_DownloadTestButton");
		BaseUI.assertThat(BaseUI.elementAppears(downloadTestButton), Is.equalTo(true));
	}

	public static void verify_ResultsPage_Has_DownloadResults_Button() {
		WebElement downloadResultsButton = Locator.lookupElement("advancedSearch_DownloadResults");
		BaseUI.assertThat(BaseUI.elementAppears(downloadResultsButton), Is.equalTo(true));
	}

	public static String return_ResultsPage_Selective_Print_Mode_Button_Text() {
		WebElement selectivePrintButton = getSelectivePrintButton();
		return BaseUI.getTextFromField(selectivePrintButton);
	}

	public static void verify_ResultsPage_Has_Inclusion_Checkboxes() {
		WebElement firstCheckbox = Locator.lookupElement("advancedSearch_SelectivePrint_FirstRow_Checkbox");
		BaseUI.assertThat(BaseUI.elementAppears(firstCheckbox), Is.equalTo(true));
	}

	public static void verify_ResultsPage_UnderSelectivePrint_Has_InvoicePayment_Dropdown() {
		WebElement invoicePayment_dropdown = Locator.lookupElement("advancedSearch_SelectivePrint_InvoicePayment_Dropdown");
		BaseUI.assertThat(BaseUI.elementAppears(invoicePayment_dropdown), Is.equalTo(true));
	}

	public static void verify_ResultsPage_Has_ViewSelectedButton() {
		WebElement viewSelectedButton = Locator.lookupElement("advancedSearch_SelectivePrint_ViewSelectedButton");
		BaseUI.assertThat(BaseUI.elementAppears(viewSelectedButton), Is.equalTo(true));
	}
}
