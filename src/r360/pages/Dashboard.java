package r360.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.DatabaseConnection;
import utils.Locator;
import utils.TableData;
import wfsCommon.pages.DatePicker;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.PieChart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dashboard {

    public static DatePicker receivables_depositDate = new DatePicker("dshbrd_ReceivablesSummary_DepositDate_Input",
            null, null);
    public static DatePicker dashboard_depositDate = new DatePicker("dshbrd_Dash_DepositDate", null, null);
    public static PieChart TotalAmount = new PieChart("paymentTotals");
    public static PieChart TotalTransactions = new PieChart("paymentCounts");
    public static Dropdown receivables_GroupBy_dropdown = new Dropdown("dshbrd_RecievablesSummary_GroupBy_Dropdown",
            null, null);
    private static final String AMOUNTS_PIE_CHART_LABEL="Total Amount";
    private static final String PAYMENTS_PIE_CHART_LABEL="Total Payments";

    public static TableData extract_ReceivablesSummary_Tables() {
        TableData tableData = new TableData();
        // target all the rows of our table
        ArrayList<WebElement> receivableSummaryRows = Locator
                .lookup_multipleElements("dshbrd_ReceivablesSummary_tableRows", null, null);
        String grouping = "";
        String groupType = "";

        // cycle through eacth of our table rows
        //
        for (WebElement tableRow : receivableSummaryRows) {
            HashMap<String, String> newRow = new HashMap<String, String>();

            if (tableRow.getAttribute("class").contains("group")) {
                grouping = tableRow.findElement(By.xpath(".//strong")).getText();
                groupType = grouping.substring(0, grouping.indexOf(":")).trim();
                grouping = grouping.substring(grouping.indexOf(":") + 1, grouping.length()).trim();
                if (groupType.equals("Entity")) {
                    grouping = grouping.substring(grouping.lastIndexOf("\\") + 1, grouping.length()).trim();
                }
            } else {
                newRow.put(groupType, grouping);
                // Our query could only return the very last part of this
                // entity. So we'll compare that.
                // String entity =
                // tableRow.findElement(By.xpath("./td[2]")).getText();
                // entity = entity.substring(entity.lastIndexOf("\\") +1,
                // entity.length());
                // newRow.put("entity", entity);
                newRow.put("workgroup", tableRow.findElement(By.xpath("./td[1]")).getText());
                newRow.put("payment_source", tableRow.findElement(By.xpath("./td[2]")).getText());
                newRow.put("payment_type", tableRow.findElement(By.xpath("./td[3]")).getText());
                newRow.put("transaction_count", tableRow.findElement(By.xpath("./td[4]")).getText());
                newRow.put("total",
                        tableRow.findElement(By.xpath("./td[5]")).getText().replace(",", "").replace("$", ""));
            }
            if (newRow.size() > 0) {
                tableData.data.add(newRow);
            }
        }

        return tableData;
    }

    public static boolean arePieChartsVisible() {
        WebElement[] pieCharts = Locator.lookup_multipleElements("dshbrd_PieCharts", "", "").toArray(new WebElement[0]);
        return Arrays.stream(pieCharts).allMatch(pie -> BaseUI.elementAppears(pie)) && pieCharts.length == 2;
    }

    public static void WaitForPageToLoad() {
        BaseUI.waitForElementToBeDisplayed_ButDontFail("dshbrd_Element_To_Wait_For_ForPageLoad", null, null, 30);
    }

    // Format date as 20150822 or yyyyMMdd
    public static TableData return_DataBase_Data_For_ReceivablesSummary(String date)
            throws Exception {

        String query = "declare @date int" + "\n" + "set @date = " + date + "\n" + "SELECT " + "\n"
                + "d.EntityName as 'Entity'" + "\n"
                + ",CONCAT(a.SiteClientAccountID,' - ', (select top 1 LongName from [WFSDB_R360].RecHubData.dimClientAccounts z where z.MostRecent = 1 and a.SiteClientAccountID = z.SiteClientAccountID and a.SiteBankID = z.SiteBankID)) as 'workgroup'"
                + "\n" + ",SUM(checktotal) as 'total'" + "\n" + ",e.LongName as 'payment_source'" + "\n"
                + ",c.LongName as 'payment_type'" + "\n" + ",SUM(TransactionCount) as 'transaction_count'" + "\n"
                + "FROM [WFSDB_R360].[RecHubData].[factBatchSummary] b" + "\n"
                + "INNER JOIN [WFSDB_R360].RecHubData.dimClientAccounts a on b.ClientAccountKey = a.ClientAccountKey"
                + "\n"
                + "inner join [WFSDB_R360].[RecHubData].[dimBatchPaymentTypes] c on b.BatchPaymentTypeKey = c.BatchPaymentTypeKey"
                + "\n" + "inner join [WFSDB_R360].RecHubData.dimBatchSources e on b.BatchSourceKey = e.BatchSourceKey"
                + "\n" + "inner join [WFSDB_R360].[RecHubData].dimBanks f on b.BankKey = f.BankKey" + "\n"
                + "inner join [WFSDB_R360].[RecHubuser].[OLWorkgroups] g on g.SiteClientAccountID = a.SiteClientAccountID and g.SiteBankID = f.SiteBankID"
                + "\n" + "inner join [raam].dbo.Entities d on d.EntityID = g.EntityID" + "\n" + "\n"
                + "where IsDeleted = 0" + "\n" + "and b.DepositDateKey = @date" + "\n"
                + "group by d.EntityName, a.SiteClientAccountID, a.LongName, e.LongName, c.LongName, a.SiteBankID"
                + "\n" + "order by d.EntityName, 'Workgroup'";

        TableData tableData = DatabaseConnection.runSQLServerQuery(query);
        tableData.set_decimal_Precision(2);
        // tableData.sort_ByColumn_numeric("dda_entity");
        return tableData;
    }

    // Format date as 20150822 or yyyyMMdd
    public static TableData return_DataBase_Data_For_PieCharts(String date)
            throws Exception {

        String query = "declare @date int" + "\n" + "set @date      =" + date + "\n" + "SELECT" + "\n"
                + "SUM(checktotal) as 'total'" + "\n" + ",c.LongName as 'payment_type'" + "\n"
                + ",SUM(TransactionCount) as 'transaction_count'" + "\n"
                + "FROM [WFSDB_R360].[RecHubData].[factBatchSummary] b" + "\n"
                + "inner JOIN [WFSDB_R360].RecHubData.dimClientAccounts a on b.ClientAccountKey = a.ClientAccountKey"
                + "\n"
                + "inner join [WFSDB_R360].[RecHubData].[dimBatchPaymentTypes] c on b.BatchPaymentTypeKey = c.BatchPaymentTypeKey"
                + "\n" + "inner join [WFSDB_R360].RecHubData.dimBatchSources e on b.BatchSourceKey = e.BatchSourceKey"
                + "\n" + "inner join [WFSDB_R360].[RecHubData].dimBanks f on b.BankKey = f.BankKey" + "\n"
                + "inner join [WFSDB_R360].[RecHubuser].[OLWorkgroups] g on g.SiteClientAccountID = a.SiteClientAccountID and g.SiteBankID = f.SiteBankID"
                + "\n" + "inner join [raam].dbo.Entities d on d.EntityID = g.EntityID" + "\n" + "\n"
                + "where IsDeleted = 0" + "\n" + "and b.DepositDateKey = @date" + "\n" + "\n" + "group by c.LongName";

        TableData tableData = DatabaseConnection.runSQLServerQuery(query);
        tableData.set_decimal_Precision(2);
        // tableData.sort_ByColumn_numeric("dda_entity");
        return tableData;
    }

    public static void sort_ByColumn_Descending(String columnName) throws Exception {

        for (int i = 0; i < 2; i++) {
            WebElement columnHeaderElement = Locator.lookupElement("dshbrd_ColumnHeader_ByText", columnName, null);
            if (!BaseUI.elementHasExpectedAttribute(columnHeaderElement, "class", "sorting_desc")) {
                BaseUI.click(columnHeaderElement);
                Thread.sleep(500);
            }
        }
    }

    public static void sort_ByColumn_Ascending(String columnName) throws Exception {

        for (int i = 0; i < 2; i++) {
            WebElement columnHeaderElement = Locator.lookupElement("dshbrd_ColumnHeader_ByText", columnName, null);
            if (!BaseUI.elementHasExpectedAttribute(columnHeaderElement, "class", "sorting_asc")) {
                BaseUI.click(columnHeaderElement);
                Thread.sleep(500);
            }
        }
    }

    public static void click_OnFirstRowReceivablesTable() throws Exception {

        WebElement cellElement = Locator.lookupElement("dshbrd_ReceivablesSummary_firstRow", null, null);
        BaseUI.click(cellElement);
        Thread.sleep(500);
    }

    private static ArrayList<LegendItem> return_PieChartLegendList(String pieTitle) {
        ArrayList<WebElement> legendElements = Locator.lookup_multipleElements("dshbrd_Dash_PieChart_LegendList", pieTitle,
                null);
        ArrayList<LegendItem> legendKeyText = new ArrayList<LegendItem>();

        for (WebElement legend : legendElements) {
            String legendText = BaseUI.get_Attribute_FromField(legend, "innerText");
            if (!legendText.equals("")) {
                legendKeyText.add(new LegendItem(legendText));
            }
        }

        return legendKeyText;
    }

    private static BigDecimal return_DisplayedTotal(String total_amount) {
        Pattern pattern = Pattern.compile("^\\s*[^:]+:\\s*\\$?(?<total>[\\d\\.,]+).*");
        WebElement pieTotalLabel = Locator.lookupElement("dshbrd_PieTotalLabel", total_amount, null);
        String totalText = BaseUI.get_Attribute_FromField(pieTotalLabel, "innerText");
        Matcher matcher = pattern.matcher(totalText);
        matcher.find();
        return new BigDecimal(matcher.group("total").replace(",", ""));
    }

    public static TableData return_totalAmount_PieChartData() {
        ArrayList<WebElement> legendElements = Locator.lookup_multipleElements("dshbrd_Dash_PieChart_LegendList", null,
                null);

        TableData legendData = new TableData();

        for (WebElement legend : legendElements) {

            HashMap<String, String> legendRow = new HashMap<String, String>();

            String legendText = BaseUI.getTextFromField(legend);
            String paymentType = legendText.split("\\:")[0];
            String amount = legendText.split("\\:")[1];
            amount = amount.substring(0, amount.lastIndexOf("(") - 1).trim();

            legendRow.put("payment_type", paymentType);
            legendRow.put("amount", amount);

            legendData.data.add(legendRow);
        }
        legendData.remove_Character("$");
        legendData.remove_Character(",");

        return legendData;
    }

    public static boolean isReceivablesSummaryVisible() {
        WebElement recSummaryGrid = Locator.lookupElement("dshbrd_ReceivablesSummary");
        return BaseUI.elementAppears(recSummaryGrid);
    }

    public static boolean areCollapseButtonsVisible() {
        WebElement[] collapseButtons = {
                getAmountsAndCountsCollapseButton(),
                getReceivablesSummaryCollapseButton()
        };
        return Arrays.stream(collapseButtons).allMatch(button -> button != null && BaseUI.elementAppears(button));
    }

    public static boolean areExpandButtonsVisible() {
        WebElement[] expandButtons = {
                getAmountsAndCountsExpandButton(),
                getReceivablesSummaryExpandButton()
        };
        return Arrays.stream(expandButtons).allMatch(button -> button != null && BaseUI.elementAppears(button));
    }

    private static WebElement getAmountsAndCountsCollapseButton() {
        return Locator.lookupElement("dshbrd_CollapseExpandButtons", "Summary Amounts and Counts", "-");
    }

    private static WebElement getReceivablesSummaryCollapseButton() {
        return Locator.lookupElement("dshbrd_CollapseExpandButtons", "Receivables Summary", "-");
    }

    private static WebElement getAmountsAndCountsExpandButton() {
        return Locator.lookupElement("dshbrd_CollapseExpandButtons", "Summary Amounts and Counts", "+");
    }

    private static WebElement getReceivablesSummaryExpandButton() {
        return Locator.lookupElement("dshbrd_CollapseExpandButtons", "Receivables Summary", "+");
    }

    public static void collapseAmountsAndCounts() {
        WebElement button = getAmountsAndCountsCollapseButton();

        BaseUI.click(button);
    }

    public static void collapseReceivablesSummary() {
        WebElement button = getReceivablesSummaryCollapseButton();

        BaseUI.click(button);
    }

    public static void expandAmountsAndCounts() {
        WebElement button = getAmountsAndCountsExpandButton();

        BaseUI.click(button);
    }

    public static void expandReceivablesSummary() {
        WebElement button = getReceivablesSummaryExpandButton();

        BaseUI.click(button);
    }

    public static boolean isAmountsAndCountsCollapseButtonVisible() {
        return BaseUI.elementAppears(getAmountsAndCountsExpandButton());
    }

    public static ArrayList<LegendItem> return_AmountsPieChartLegendList() {
        return return_PieChartLegendList(AMOUNTS_PIE_CHART_LABEL);
    }

    public static ArrayList<LegendItem> return_PaymentsPieChartLegendList() {
        return return_PieChartLegendList(PAYMENTS_PIE_CHART_LABEL);
    }

    public static BigDecimal return_AmountsDisplayedTotal() {
        return return_DisplayedTotal(AMOUNTS_PIE_CHART_LABEL);
    }

    public static BigDecimal return_PaymentsDisplayedTotal() {
        return return_DisplayedTotal(PAYMENTS_PIE_CHART_LABEL);
    }

    public static boolean isDDAGroupingVisible() {
        WebElement ddaGrouping = Locator.lookupElement("dshbrd_GroupingSelector");
        if(ddaGrouping!=null && BaseUI.elementAppears(ddaGrouping)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void clickRefresh() {
        WebElement refreshButton = Locator.lookupElement("dshbrd_RefreshButton");
        BaseUI.click(refreshButton);
    }

    public static class LegendItem {
        private String _name;
        private BigDecimal _amount;
        private BigDecimal _percent;

        public LegendItem(String text) {
            Pattern pattern = Pattern.compile("^\\s*(?<name>[^:]+):\\s*\\$?(?<amount>[\\d\\.,]+)\\s*\\((?<percent>[\\d\\.]+)\\s*%\\s*\\).*");
            Matcher matcher = pattern.matcher(text);
            matcher.find();
            _name = matcher.group("name");
            _amount = new BigDecimal(matcher.group("amount").replace(",", ""));
            _percent = new BigDecimal(matcher.group("percent"));
        }

        public LegendItem(String name, BigDecimal amount, BigDecimal percent) {
            _name = name;
            _amount = amount;
            _percent = percent;
        }

        public String getName() {
            return _name;
        }

        public BigDecimal getAmount() {
            return _amount;
        }

        public BigDecimal getPercent() {
            return _percent;
        }
    }
}
