package r360.pages;

import java.text.MessageFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import wfsCommon.pages.Dropdown;
import wfsCommon.pages.TextBox;

public class SecurityAdmin_EntityManageModal {

	public static Dropdown entityTypeSelector = new Dropdown("s2id_selEntityPageEntityTypes");
	public static TextBox SignOutRedirectURL = new TextBox("entityEntityInfoSignOutRedirectURL_textbox", null, null);
	
	// Clicks Cancel to close out the EntityManagementModal
	public static void cancel_EntityManagementModal() throws Exception {
		BaseUI.click(Locator.lookupElement("entityEntityInfoCancelButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void save_EntityManagementModal() throws Exception {
		BaseUI.click(Locator.lookupElement("entityPwdConfigSaveButton"));
		Thread.sleep(7000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void select_EntityValue(String valueToSelect) throws Exception {
		
		//entityTypeSelector.select_EntityValue(valueToSelect);
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoNameText"));
		Thread.sleep(300);
		WebElement dropdownElement = Browser.driver.switchTo().activeElement();
		//BaseUI.enterKey(Locator.lookupElement("entityEntityInfoEntityTypeDropdown"), Keys.RETURN);
		BaseUI.enterKey(dropdownElement, Keys.RETURN);
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("entityEntityInfoEntityTypeDropdown_ListItem_ByText", valueToSelect, null));
		Thread.sleep(200);

	}

	// enter the text and save entity.  Only goes over required fields.
	public static void add_Entity(String entityName, String entityType) throws Exception {
		enter_EntityRequiredInfo(entityName, entityType);
		save_EntityManagementModal();
//		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
//		Thread.sleep(2000);
//		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	// enter the text and save entity. Overload goes through every value.
	public static void add_Entity(String entityName, String entityType, 
			String entityDescription, String entityExternalID, Boolean subscribedApp1,
			Boolean authTypeStandardPWord, Boolean authTypeSingleSignOn,
			Boolean activateEntityActive) throws Exception {
		
		enter_EntityRequiredInfo(entityName, entityType);
		
		if(entityDescription != null)
		{
			BaseUI.enterText(Locator.lookupElement("entityEntityInfoDescriptionText"), entityDescription);
		}
		if(entityExternalID != null)
		{
			BaseUI.enterText(Locator.lookupElement("entityEntityInfoExtEntityIdText"), entityExternalID);
		}
		if(subscribedApp1 != null)
		{
			if (subscribedApp1) {
				BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoSubAppR360Checkbox"));
			} else {
				BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoSubAppR360Checkbox"));
			}
		}
		if(authTypeStandardPWord != null)
		{
			if (authTypeStandardPWord) {
				BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
			} else {
				BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoStdPasswordCheckbox"));
			}
		}
		
		if(authTypeSingleSignOn != null)
		{
			if (authTypeStandardPWord) {
				BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
			} else {
				BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoSingleSignOnCheckbox"));
			}
		}
		
		if(activateEntityActive != null)
		{
			if (activateEntityActive) {
				BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoActiveCheckbox"));
			} else {
				BaseUI.uncheckCheckbox(Locator.lookupElement("entityEntityInfoActiveCheckbox"));
			}
		}
		
		
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(3000);
	}

	// enter the text and save entity. Overload goes through all textbox fields
	public static void add_Entity(String entityName, String entityType, String entityDescription,
			String entityExternalID) throws Exception {

		enter_EntityRequiredInfo(entityName, entityType);

		if (entityDescription != null) {
			BaseUI.enterText(Locator.lookupElement("entityEntityInfoDescriptionText"), entityDescription);
		}
		if (entityExternalID != null) {
			enter_ExternalEntityID(entityExternalID);
		}

		click_save();
	}
	
	public static void click_save() throws Exception
	{
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(1000);
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void enter_ExternalEntityID(String entityExternalID)
	{
		BaseUI.enterText(Locator.lookupElement("entityEntityInfoExtEntityIdText"), entityExternalID);
	}
	
	public static void setPasswordConfigurationUsersNameRulesToTrue() throws Exception{
		BaseUI.checkCheckbox(Locator.lookupElement("entityPwdConfigCanContainUsernameCheckBox"));
		BaseUI.checkCheckbox(Locator.lookupElement("entityPwdConfigCanContainReversedUsernameCheckBox"));
		BaseUI.checkCheckbox(Locator.lookupElement("entityPwdConfigCanContainUsernameFirstnameCheckBox"));
		BaseUI.checkCheckbox(Locator.lookupElement("entityPwdConfigCanContainUsernameLastnameCheckBox"));
		Thread.sleep(2000);
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(2000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
		
	}
	
	public static void setPasswordConfigurationUsersNameRulesToFalse() throws Exception{
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityPwdConfigCanContainUsernameCheckBox"));
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityPwdConfigCanContainReversedUsernameCheckBox"));
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityPwdConfigCanContainUsernameFirstnameCheckBox"));
		BaseUI.uncheckCheckbox(Locator.lookupElement("entityPwdConfigCanContainUsernameLastnameCheckBox"));
		Thread.sleep(2000);
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(2000);	
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}
	
	public static void verifyPasswordConfigurationUsersNameRulesSetToFalse() throws InterruptedException{
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityPwdConfigCanContainUsernameCheckBox"), false);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityPwdConfigCanContainReversedUsernameCheckBox"), false);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityPwdConfigCanContainUsernameFirstnameCheckBox"), false);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("entityPwdConfigCanContainUsernameLastnameCheckBox"), false);
		Thread.sleep(2000);
		BaseUI.click(Locator.lookupElement("entityEntityInfoCancelButton"));
		Thread.sleep(2000);
	}
	public static void verifyPasswordConfigurationCharacterRulesSettingValue(String minLength, String maxRepeatingChars, String minNumChars, 
			String minSpecialChars, String minUppercaseChars, String minLowercaseChars) throws Exception{
		verify_Textbox_Value("entityPwdConfigCharacterRulesMinLengthText", minLength);
		verify_Textbox_Value("entityPwdConfigCharacterRulesMaxRepeatinCharText", maxRepeatingChars);
		verify_Textbox_Value("entityPwdConfigCharacterRulesMinNumCharText", minNumChars);
		verify_Textbox_Value("entityPwdConfigCharacterRulesMinSpecialCharText", minSpecialChars);
		verify_Textbox_Value("entityPwdConfigCharacterRulesMinUppercaseCharText", minUppercaseChars);
		verify_Textbox_Value("entityPwdConfigCharacterRulesMinLowercaseCharText", minLowercaseChars);
		cancel_EntityManagementModal();
	}
	
	public static void set_LoginConfiguration_Settings(String inactiveDays, String maxLogins, String minLength,
			String minNumericChars, String minAlphaChars, String minSpecialChars) throws Exception{
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityLoginConfigInactiveDaysText"), inactiveDays);
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityLoginConfigMaxLoginAttemptText"), maxLogins);		
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityLoginConfigCharacterRulesMinLengthText"), minLength);	
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityLoginConfigCharacterRulesMinAlphaCharText"), minAlphaChars);
		BaseUI.tabThroughField("entityLoginConfigCharacterRulesMinAlphaCharText");
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityLoginConfigCharacterRulesMinNumCharText"), minNumericChars);	
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityLoginConfigCharacterRulesMinSpecialCharText"), minSpecialChars);
	}
	public static void verifyLoginConfigurationSettingsValue(String inactiveDays, String maxLogins, String minLength,
			String minAlphaChars, String minNumericChars, String minSpecialChars){
		verify_Textbox_Value("entityLoginConfigInactiveDaysText", inactiveDays);
		verify_Textbox_Value("entityLoginConfigMaxLoginAttemptText",maxLogins);		
		verify_Textbox_Value("entityLoginConfigCharacterRulesMinLengthText", minLength);	
		verify_Textbox_Value("entityLoginConfigCharacterRulesMinAlphaCharText", minAlphaChars);	
		verify_Textbox_Value("entityLoginConfigCharacterRulesMinNumCharText", minNumericChars);	
		verify_Textbox_Value("entityLoginConfigCharacterRulesMinSpecialCharText", minSpecialChars);
	}
	
	
	// enter the text
	public static void enter_EntityRequiredInfo(String entityName, String entityType) throws Exception {
		BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("entityEntityInfoNameText"), entityName);
		select_EntityValue(entityType);
	}

	// enter the text
	public static void enter_Description(String textToEnter) throws Exception {
		BaseUI.enterText(BaseUI.waitForElementToBeClickable("entityEntityInfoDescriptionText", null, null), textToEnter);
		Thread.sleep(100);
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoDescriptionText", null, null));
	}

	
	// Overload also sets the Application checkbox. Needed if multiple
	// applications listed.
	public static void add_Entity(String entityName, String entityType, String entityApplication) throws Exception {
		BaseUI.enterText(Locator.lookupElement("entityEntityInfoNameText"), entityName);
		select_EntityValue(entityType);
		BaseUI.checkCheckbox(Locator.lookupElement("entityEntityInfoSubApp_ByApplication", entityType, null));
		Thread.sleep(200);
		BaseUI.click(Locator.lookupElement("entityEntityInfoSaveButton"));
		Thread.sleep(2000);
	}

	public static void navigate_to_PasswordConfigurationTab() throws Exception {
		BaseUI.click(Locator.lookupElement("entityPasswordConfigTab"));
		Thread.sleep(1000);

	}

	public static void navigate_to_LoginConfigurationTab() throws Exception {
		BaseUI.click(Locator.lookupElement("entityLoginConfig"));
		Thread.sleep(1000);
	}
	
	public static void navigate_to_EntityInfoTab() throws Exception {
		BaseUI.click(Locator.lookupElement("entityInfoTab"));
		Thread.sleep(1000);
	}
	
	
	public static void verify_Textbox_Value(String textBoxIdentifier, String expectedValue) {
		String textBoxValue = BaseUI.getTextFromInputBox(Locator.lookupElement(textBoxIdentifier));
		BaseUI.verify_true_AndLog(textBoxValue.equals(expectedValue),
				MessageFormat.format("Textbox value {0} was a match", textBoxValue),
				MessageFormat.format("Expected value: {0}, Seeing value: {1}.", expectedValue, textBoxValue));
	}

	public static void verify_Textbox_Value_isNOT(String textBoxIdentifier, String not_expectedValue) {
		String textBoxValue = BaseUI.getTextFromInputBox(Locator.lookupElement(textBoxIdentifier));
		BaseUI.verify_false_AndLog(textBoxValue.equals(not_expectedValue),
				MessageFormat.format("Textbox value {0} was NOT a match", textBoxValue),
				MessageFormat.format("Value was NOT supposed to be {0}.", not_expectedValue));
	}

	public static void verifyPasswordError(String textBoxIdentifier, String textBoxErrorValidationIdentifier,
			String errorMessage, String valueToEnter) throws Exception {
		BaseUI.enterText(Locator.lookupElement(textBoxIdentifier), valueToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxIdentifier));
		BaseUI.verifyElementAppears(Locator.lookupElement(textBoxErrorValidationIdentifier));
		BaseUI.verifyElementHasExpectedText(textBoxErrorValidationIdentifier, errorMessage);
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityPwdConfigSaveButton"));

	}
	
	public static void verifyTextBoxError(String textBoxIdentifier, String errorMessage, String valueToEnter) throws Exception {
		WebElement textBox = Locator.lookupElement(textBoxIdentifier);
		BaseUI.enterText(textBox, valueToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxIdentifier));
		WebElement errorMessageElement = textBox.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		BaseUI.verifyElementAppears(errorMessageElement);
		BaseUI.verifyElementHasExpectedText(errorMessageElement, errorMessage);
		BaseUI.verifyElementDisabled(Locator.lookupElement("entityPwdConfigSaveButton"));

	}

	public static void reset_Modal_PasswordTab(String testEntity) throws Exception {
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();
	}

	public static void reset_Modal_LoginTab(String testEntity) throws Exception {
		SecurityAdmin_EntityManageModal.cancel_EntityManagementModal();
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();
	}

	
	
	
	public static void verifyTextBoxCharsInvalid(String[] specialChars, String fieldToCheck,
			String fieldValidationErrorMessage) throws Exception {
		for (String character : specialChars) {
			WebElement textBox = Locator.lookupElement(fieldToCheck);
			BaseUI.enterText(textBox, character);
			BaseUI.tabThroughField(textBox);

			WebElement errorElement = textBox.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
			String errorMessageText = BaseUI.getTextFromField(errorElement);
			BaseUI.verify_true_AndLog(errorMessageText.equals(fieldValidationErrorMessage),
					MessageFormat.format("Error message was correct for: {0}", character),
					MessageFormat.format("Error message was NOT correct for: {0}", character));
		}

	}
	public static void verifyTextBoxStringInvalid_AfterSave(String textToEnter, String fieldToCheck,
			String fieldValidationErrorMessage) throws Exception {
		
		WebElement textBox = Locator.lookupElement(fieldToCheck);
		BaseUI.enterText(textBox, textToEnter);
		BaseUI.tabThroughField(textBox);
		
		BaseUI.click(Locator.lookupElement("entityPwdConfigSaveButton"));
		Thread.sleep(5000);
		WebElement errorElement = textBox.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		
		BaseUI.verifyElementHasExpectedText(errorElement, fieldValidationErrorMessage);
	}
	
	public static void verifyTextBoxStringValid(String textToEnter, String fieldToCheck) throws Exception {
	
			WebElement textBox = Locator.lookupElement(fieldToCheck);
			BaseUI.enterText(textBox, textToEnter);
			BaseUI.tabThroughField(textBox);

			WebElement errorElement = textBox.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		
			BaseUI.verifyElementDoesNotAppear(errorElement);
	}

	public static void verifyChange_toGivenPasswordField(String testEntity, String textBoxLocator, String textBoxValue)
			throws Exception {
		reset_Modal_PasswordTab(testEntity);
		BaseUI.enterText(Locator.lookupElement(textBoxLocator), textBoxValue);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxLocator));
		verify_Textbox_Value(textBoxLocator, textBoxValue);
		save_EntityManagementModal();

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		navigate_to_PasswordConfigurationTab();

		SecurityAdmin_EntityManageModal.verify_Textbox_Value(textBoxLocator, textBoxValue);
	}
	
	public static void verifyChange_toGivenLoginField(String testEntity, String textBoxLocator, String textBoxValue)
			throws Exception {
		reset_Modal_LoginTab(testEntity);
		BaseUI.enterText(Locator.lookupElement(textBoxLocator), textBoxValue);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxLocator));
		verify_Textbox_Value(textBoxLocator, textBoxValue);
		save_EntityManagementModal();

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		navigate_to_LoginConfigurationTab();

		SecurityAdmin_EntityManageModal.verify_Textbox_Value(textBoxLocator, textBoxValue);
	}

	public static void verifyChangeCancelled_toGivenPasswordField(String testEntity, String textBoxLocator, String textBoxValue)
			throws Exception {
		reset_Modal_PasswordTab(testEntity);
		String originalTextBoxValue = BaseUI.getTextFromInputBox(Locator.lookupElement(textBoxLocator));
		BaseUI.enterText(Locator.lookupElement(textBoxLocator), textBoxValue);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxLocator));
		verify_Textbox_Value(textBoxLocator, textBoxValue);
		reset_Modal_PasswordTab(testEntity);

		verify_Textbox_Value_isNOT(textBoxLocator, textBoxValue);
		verify_Textbox_Value(textBoxLocator, originalTextBoxValue);
	}
	
	public static void verifyChangeCancelled_toGivenLoginField(String testEntity, String textBoxLocator, String textBoxValue)
			throws Exception {
		reset_Modal_LoginTab(testEntity);
		String originalTextBoxValue = BaseUI.getTextFromInputBox(Locator.lookupElement(textBoxLocator));
		BaseUI.enterText(Locator.lookupElement(textBoxLocator), textBoxValue);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxLocator));
		verify_Textbox_Value(textBoxLocator, textBoxValue);
		reset_Modal_LoginTab(testEntity);

		verify_Textbox_Value_isNOT(textBoxLocator, textBoxValue);
		verify_Textbox_Value(textBoxLocator, originalTextBoxValue);
	}

	public static void verifyChange_toGivenPasswordField_BlankComesBackAs0(String testEntity, String textBoxLocator,
			String textBoxLocator_Validation) throws Exception {
		reset_Modal_PasswordTab(testEntity);
		String value = "";
		BaseUI.enterText(Locator.lookupElement(textBoxLocator), value);
		BaseUI.tabThroughField(Locator.lookupElement(textBoxLocator));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement(textBoxLocator_Validation));
		Thread.sleep(500);
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_PasswordConfigurationTab();

		SecurityAdmin_EntityManageModal.verify_Textbox_Value(textBoxLocator, "0");
	}
	
	

	public static void verify_CannotPasteNegativeValue(String fieldIdentifier, String valueToPaste, String errorMessage, String testEntity) throws Exception
	{
		reset_Modal_LoginTab(testEntity);
		BaseUI.pasteText(Locator.lookupElement(fieldIdentifier), valueToPaste);
		BaseUI.tabThroughField(Locator.lookupElement(fieldIdentifier));
		Thread.sleep(500);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement(fieldIdentifier), "");
		//BaseUI.click(Locator.lookupElement("entityLoginConfigSaveButton"));
		//BaseUI.verifyElementDisabled(Locator.lookupElement("entityLoginConfigSaveButton"));
//		WebElement errorTextElement = Locator.lookupElement(fieldIdentifier).findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
//		BaseUI.verifyElementAppears(errorTextElement);
//		BaseUI.verifyElementHasExpectedText(errorTextElement, errorMessage);
	}
	
	
	public static void verifyChange_toGivenLoginField_BlankComesBackAs0(String testEntity, String textBoxLocator) throws Exception {
		reset_Modal_LoginTab(testEntity);
		String value = "";
		WebElement textBoxElement = Locator.lookupElement(textBoxLocator);
		BaseUI.enterText(textBoxElement, value);
		BaseUI.tabThroughField(textBoxElement);
		BaseUI.verifyElementDoesNotAppear(textBoxElement.findElement(By.xpath("./following-sibling::span[@class='validationMessage']")));
		Thread.sleep(500);
		SecurityAdmin_EntityManageModal.save_EntityManagementModal();

		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		SecurityAdmin_EntityManageModal.navigate_to_LoginConfigurationTab();

		SecurityAdmin_EntityManageModal.verify_Textbox_Value(textBoxLocator, "0");
	}
	
	// Pass true to save, false to cancel.
	public static void check_checkBox_And_SaveOrCancel(String checkBoxIdentifier, Boolean save) throws Exception {
		BaseUI.checkCheckbox(checkBoxIdentifier);
		Thread.sleep(100);
		if (save) {
			save_EntityManagementModal();
		} else {
			cancel_EntityManagementModal();
		}
	}
	
	public static void unCheckCheckBoxAndSaveOrCancel(String checkBoxIdentifier, Boolean save) throws Exception {
		BaseUI.uncheckCheckbox(checkBoxIdentifier);
		Thread.sleep(100);
		if (save) {
			save_EntityManagementModal();
		} else {
			cancel_EntityManagementModal();
		}
	}

	public static void verify_checkingCheckboxAndClickingCancel_doesntSave(String testEntity, String checkBoxIdentifier)
			throws Exception {
		check_checkBox_And_SaveOrCancel(checkBoxIdentifier, false);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		navigate_to_PasswordConfigurationTab();

		BaseUI.verifyCheckboxStatus(Locator.lookupElement(checkBoxIdentifier), false);
	}

	public static void verify_checkingCheckboxAndClickingSave_Saves(String testEntity, String checkBoxIdentifier)
			throws Exception {
		check_checkBox_And_SaveOrCancel(checkBoxIdentifier, true);
		SecurityAdministration.launchEditEntityModal_forGivenEntity(testEntity);
		navigate_to_PasswordConfigurationTab();

		BaseUI.verifyCheckboxStatus(Locator.lookupElement(checkBoxIdentifier), true);
	}
	
	public static void verify_LoginValidation(String rule_Locator, String rule_ValueToSet, String login_ValueToSet, String login_ErrorMessage) throws Exception
	{
		set_LoginRule(rule_Locator, rule_ValueToSet);
		SecurityAdministration.navigate_toUserTab();
		SecurityAdministration.launchAddUserModal();
		WebElement textField = Locator.lookupElement("entityUserInfoLoginName");
		BaseUI.enterText(textField, login_ValueToSet);
		BaseUI.tabThroughField(textField);
		Thread.sleep(5000);
		WebElement textFieldValidation = textField.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		BaseUI.verifyElementAppears(textFieldValidation);
		BaseUI.verifyElementHasExpectedText(textFieldValidation, login_ErrorMessage);
	}
	
	public static void set_LoginRule(String rule_Locator, String rule_ValueToSet) throws Exception
	{
		WebElement textBoxToSet = Locator.lookupElement(rule_Locator);
		BaseUI.enterText(textBoxToSet, rule_ValueToSet);
		BaseUI.tabThroughField(textBoxToSet);
		Thread.sleep(200);
		save_EntityManagementModal();
	}
	
	public static void click_EntityTypeDropdown() throws Exception {
		BaseUI.click(Locator.lookupElement("entityEntityInfoEntityTypeDropdown"));
		Thread.sleep(300);
	}

	public static void click_EntityTypeDropdown_Close() throws Exception {
		BaseUI.click(Locator.lookupElement("entityEntityInfoEntityTypeDropdown_masked"));
		Thread.sleep(300);
	}
	

	public static void verify_EntityTypeDropdownAccurate(String selectedEntityType) {
		BaseUI.verifyElementHasExpectedText("entityEntityInfoEntityTypeDropdown_ChosenType", selectedEntityType);
		BaseUI.verifyElementAppearsByString("entityEntityInfoEntityTypeDropdown_ChosenType");

	}


	public static String select_EntityValue_ByIndex(Integer valueToSelect) throws Exception {
		BaseUI.tabThroughField(Locator.lookupElement("entityEntityInfoNameText"));
		
		WebElement dropdownFocusedElement = Browser.driver.switchTo().activeElement();
		BaseUI.enterKey(dropdownFocusedElement, Keys.RETURN);
		Thread.sleep(300);
		String elementText = BaseUI.getTextFromField(Locator.lookupElement(
				"entityEntityInfoEntityTypeDropdown_ListItemText_ByIndex", valueToSelect.toString(), null));
		BaseUI.click(Locator.lookupElement("entityEntityInfoEntityTypeDropdown_ListItem_ByIndex",
				valueToSelect.toString(), null));
		Thread.sleep(200);

		return elementText;

	}
	
	public static void clear_selectedEntityType() throws Exception {
		BaseUI.click(Locator.lookupElement("entityEntityInfoEntityTypeDropdown_ClearSelectedType"));
		Thread.sleep(300);
	}
}
