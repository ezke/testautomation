package r360.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PostDepositExceptionsPage {

    private static final String POSTDEPOSITTABLE = "//table[@id='postDepositTable']";
    @FindBy(xpath = POSTDEPOSITTABLE + "//*[contains(@class, 'unlock-button text-primary')]")
    private static WebElement firstWorkGroupFirstCell;
    @FindBy(xpath = "//input[@type='search']")
    private WebElement postDepositSearch;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Deposit Date']")
    private WebElement depositDateColumnHeading;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Amount']")
    private static WebElement amountColumnHeading;
    @FindBy(xpath = "//div[contains(@class, 'dataTables_scrollBody')]//tbody//td[count(//div[contains(@class, 'dataTables_scrollHeadInner')]//thead/tr/th[contains(., 'Transaction ID')]/preceding-sibling::th)+1][text()='2']")
    private static WebElement distinctElement;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Entity']")
    private static WebElement entityColumnHeading;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Workgroup']")
    private static WebElement workgroupColumnHeading;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Payment Source']")
    private static WebElement paymentSourceColumnHeading;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Payment Type']")
    private static WebElement paymentTypeColumnHeading;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Batch ID']")
    private static WebElement batchIDColumnHeading;
    @FindBy(xpath = "//*[@id='postDepositTable_wrapper']//th[./text()='Transaction ID']")
    private static WebElement transactionIDColumnHeading;
    @FindBy(xpath = "//table[@id='postDepositTable']/tbody/tr/td[1]")
    private static WebElement firstTransactionFirstCell;
    @FindBy(xpath = "//*[@id=\"postDepositTable\"]/tbody[1]/tr[2]")
    private static WebElement selectTransactionTwo;
    @FindBy(xpath = "//div[@class=\"modal-dialog\"]//a[contains(@class, \"btn\") and ./text()=\"Unlock\"]")
    private static WebElement unLockButton;
    @FindBy(xpath = "//span[@class='page-title' and ./text()='Post-Deposit Exceptions Summary']")
    private static WebElement pageTitle;
    @FindBy(xpath = "//div[@class='dataTables_scrollHead']//table/thead/tr/th[1]/following-sibling::th")
    private List<WebElement> tableHeaders;
    @FindBy(xpath = "//table[@id='postDepositTable']/tbody/tr/td[1]/following-sibling::td")
    private List<WebElement> tableCells;
    @FindBy(xpath = POSTDEPOSITTABLE + "//td/a[contains(@class, \"unlock-button\") and not(contains(@class, \"text-primary\"))]")
    private List<WebElement> listLockedTransactionElements;
    @FindAll({@FindBy(xpath = "//tr[@title='Click here to view Exceptions Transaction Detail']/td[not(text()) and not(node())]/following-sibling::td[1]")})
    private List<WebElement> listUnlockedTransactionElements;
    @FindBy(linkText = "Unlock")
    private WebElement unlockButton;
    @FindBy(linkText = "Cancel")
    private WebElement cancelButton;
    @FindBy(linkText = "Post-Deposit Exceptions Summary")
    private WebElement postDepositExceptionsSummary;

    public PostDepositExceptionsPage() {
        waitTimeForPage(Duration.ofSeconds(10));
        PageFactory.initElements(Browser.driver, this);
    }

    public void waitTimeForPage(Duration timeToWait) {
        try {
            Navigation.wait_for_LoadingSpinner_ToAppearAndDisappear(timeToWait);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PostDepositExceptionsPage sortByDepositDateHeading() throws InterruptedException {
        return sortByDepositDateHeading(true);
    }

    public PostDepositExceptionsPage sortByDepositDateHeading(boolean ascending) throws InterruptedException {
        String currentSort = depositDateColumnHeading.getAttribute("class");
        HashMap<String, Integer> clickTable = new HashMap<String, Integer>(){{
                 put("sorting", ascending?1:2);
                 put("sorting_asc", ascending?0:1);
                 put("sorting_desc", ascending?1:0);
                }};
        for(int clicksLeft = clickTable.get(currentSort); clicksLeft>0; clicksLeft--) {
            depositDateColumnHeading.click();
            waitTimeForPage(Duration.ofSeconds(10));
        }
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsPage sortByAmountHeading() throws InterruptedException {
        amountColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsPage sortByEntityHeading() {
        entityColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsPage sortByWorkGroupHeading() {
        workgroupColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsPage sortByPaymentSourceColumnHeading() {
        paymentSourceColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsPage sortByPaymentTypeColumnHeading() {
        paymentTypeColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsPage sortBybatchIDColumnHeading() {
        batchIDColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public void clickUnlockModalCancelButton() {
        BaseUI.click(cancelButton);
    }

    public PostDepositExceptionsPage sortByTransactionIDColumnHeading() throws InterruptedException {
        transactionIDColumnHeading.click();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectFirstTransaction() throws Exception {
        firstTransactionFirstCell.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    private String[] getHeaders() {
        ArrayList<String> result = new ArrayList<String>();
        for (WebElement headerElement : tableHeaders) {
            String headerText = BaseUI.get_Attribute_FromField(headerElement, "innerText");
            result.add(headerText.trim());
        }
        return result.toArray(new String[0]);
    }

    public void selectUnLock() {
        unLockButton.click();
    }

    public PostDepositExceptionsTransactionDetailPage selectTransactionTwo() throws Exception {
        selectTransactionTwo.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public String getRowTitleAttribute() {
        String popUpText = selectTransactionTwo.getAttribute("Title");
        return popUpText;
    }

    public boolean isDepositDateHeadingVisible() {
        return BaseUI.elementAppears(depositDateColumnHeading);
    }

    public boolean isAmountHeadingVisible() {
        return BaseUI.elementAppears(amountColumnHeading);
    }

    public boolean isEntityHeadingVisible() {
        return BaseUI.elementAppears(entityColumnHeading);
    }

    public boolean isWorkgroupHeadingVisible() {
        return BaseUI.elementAppears(workgroupColumnHeading);
    }

    public boolean isPaymentSourceHeadingVisible() {
        return BaseUI.elementAppears(paymentSourceColumnHeading);
    }

    public boolean isPaymentTypeHeadingVisible() {
        return BaseUI.elementAppears(paymentTypeColumnHeading);
    }

    public boolean isBatchIDHeadingVisible() {
        return BaseUI.elementAppears(batchIDColumnHeading);
    }

    public boolean isTransactionIDHeadingVisible() {
        return BaseUI.elementAppears(transactionIDColumnHeading);
    }

    public TableData getPageTable() {
        return BaseUI.tableExtractor_ByCell(new ArrayList<>(tableCells), getHeaders());
    }

    public String getTransactionIDFromRow(int row) {
        TableData table = getPageTable();
        return table.dataRow(row).get("Transaction ID");
    }

    public PostDepositExceptionsPage searchPostDeposit(String searchText) {
        Actions actions = new Actions(Browser.driver);
        BaseUI.clearText_Textbox(postDepositSearch);
        actions.moveToElement(postDepositSearch).click().sendKeys(searchText).sendKeys(Keys.RETURN).build().perform();
        return new PostDepositExceptionsPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectDestinctTransaction() throws Exception {
        Navigation.wait_for_LoadingSpinner_ToDisappear();
        distinctElement.click();
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectFirstWorkGroup() throws Exception {
        Actions actions = new Actions(Browser.driver);
        WebElement cell = Locator.lookupElement("postDepositExceptionsSummaryGrid", "1", "Workgroup");
        BaseUI.click(cell);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public PostDepositExceptionsTransactionDetailPage selectFirstUnlockedTransactionElement() throws Exception {
        clickFirstInList(listUnlockedTransactionElements);
        return new PostDepositExceptionsTransactionDetailPage();
    }

    public void selectFirstLockedTransactionElement() throws Exception {
        clickFirstInList(listLockedTransactionElements);
    }

    public void clickLockOfRow(int row) {
        WebElement lock = Locator.lookupElement("postDepositExceptionsLocks", Integer.toString(row), null);
        BaseUI.click(lock);
    }

    private void clickFirstInList(List<WebElement> list) {
        if(list.size()>0) {
            Actions actions = new Actions(Browser.driver);
            actions.moveToElement(list.get(0)).click().build().perform();
        }
        else {
            throw new IllegalArgumentException("The list is empty");
        }
    }

    public boolean isLockButtonEnabled() {
        return isButtonEnabled(unLockButton);
    }

    public boolean isButtonEnabled(WebElement button) {
        String disabledAttribute = button.getAttribute("disabled");
        return disabledAttribute == null || !disabledAttribute.equals("true");
    }

    public void selectCancel() {
        cancelButton.click();
    }

    public boolean isRowLocked(int row) {
        WebElement lock = Locator.lookupElement("postDepositExceptionsLocks", Integer.toString(row), null);
        return lock!=null;
    }

    public void clickUnlockModalUnlockButton() {
        BaseUI.click(unlockButton);
    }
}







