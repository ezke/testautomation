package r360.data;

import r360.pages.GlobalVariables;
import utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class IntegraPayDatabaseAccess {
    private static void prepConnection() {
        DatabaseConnection.dbUrl = GlobalVariables.environment.IntegraPayDBConnection;
    }

    private static void executeLockboxBasedQuery(String query, int bankID, int lockboxID) throws SQLException {
        prepConnection();
        PreparedStatement prepQuery = DatabaseConnection.getPreparedStatement(query);
        prepQuery.setInt(1, bankID);
        prepQuery.setInt(2, lockboxID);
        prepQuery.execute();
    }

    public static void deleteLockbox(int bankID, int lockboxID) throws SQLException {
        executeLockboxBasedQuery(
                "DELETE Lockbox\n" +
                        "\tFROM dbo.Lockbox\n" +
                        "\tWHERE BankID = ?\n" +
                        "\t\tAND LockboxID = ?",
                bankID,
                lockboxID
        );
    }

    public static void deleteBank(int bankID) throws SQLException {
        prepConnection();
        PreparedStatement deleteQuery = DatabaseConnection.getPreparedStatement(
                "DELETE Bank\n" +
                        "\tFROM dbo.Bank\n" +
                        "\tWHERE BankID = ?"
        );
        deleteQuery.setInt(1, bankID);
        deleteQuery.execute();
    }

    public static void deleteDESetup(int bankID, int lockboxID) throws SQLException {
        executeLockboxBasedQuery(
                "DECLARE @bankID INT,\n" +
                        "\t@lockboxID INT\n" +
                        "SET @bankID = ?\n" +
                        "SET @lockboxID = ?\n" +
                        "DELETE DESetup\n" +
                        "\tFROM dbo.DESetup\n" +
                        "\tWHERE DESetupID in\n" +
                        "\t(SELECT TOP(100) DESetupID\n" +
                        "\t\tFROM dbo.BatchTypes\n" +
                        "\t\tWHERE BankID = @bankID\n" +
                        "\t\t\tAND LockboxID = @lockboxID\n" +
                        "\tUnion \n" +
                        "\tSELECT TOP(100) DESetupID\n" +
                        "\t\tFROM dbo.Lockbox\n" +
                        "\t\tWHERE BankID = @bankID\n" +
                        "\t\t\tAND LockboxID = @lockboxID)",
                bankID,
                lockboxID
        );
    }

    public static void deleteLockboxChildren(String[] tables, int bankID, int lockboxID) throws SQLException {
        for(String table : tables) {
            deleteLockboxChild(table, bankID, lockboxID);
        }
    }

    public static void deleteLockboxChild(String table, int bankID, int lockboxID) throws SQLException {
        executeLockboxBasedQuery(
                "DELETE dbo." + table + "\n" +
                        "\tWHERE BankID = ?\n" +
                        "\t\tAND LockboxID = ?",
                bankID,
                lockboxID
        );
    }

    public static void deleteLockboxDecisioningReasons(int bankID, int lockboxID) throws SQLException {
        executeLockboxBasedQuery(
                "DELETE LockboxDecisioningReasons\n" +
                        "\tFROM dbo.LockboxDecisioningReasons\n" +
                        "\t\tINNER JOIN dbo.Lockbox\n" +
                        "\t\t\tON LockboxDecisioningReasons.LockboxKey = Lockbox.LockboxKey\n" +
                        "\tWHERE Lockbox.BankID = ?\n" +
                        "\t\tAND Lockbox.LockboxID = ?",
                bankID,
                lockboxID
        );
    }

    public static void deleteBatchChildren(String[] tables, int bankID, int lockboxID) throws SQLException {
        for(String table : tables) {
            deleteBatchChild(table, bankID, lockboxID);
        }
    }

    public static void deleteBatchChild(String table, int bankID, int lockboxID) throws SQLException {
        executeLockboxBasedQuery(
                "DELETE " + table + "\n" +
                        "\tFROM dbo." + table + "\n" +
                        "\t\tINNER JOIN dbo.Batch\n" +
                        "\t\t\tON " + table + ".GlobalBatchID = Batch.GlobalBatchID\n" +
                        "\tWHERE Batch.BankID = ?\n" +
                        "\t\tAND Batch.LockboxID = ?",
                bankID,
                lockboxID
        );
    }

    public static void deleteDEItemFieldData(int bankID, int lockboxID) throws SQLException {
        executeLockboxBasedQuery(
                "DELETE DEItemFieldData\n" +
                        "\tFROM dbo.DEItemFieldData\n" +
                        "\t\tINNER JOIN dbo.DEItemRowData\n" +
                        "\t\t\tON DEItemFieldData.DEItemRowDataID = DEItemRowData.DEItemRowDataID\n" +
                        "\t\tINNER JOIN dbo.Batch\n" +
                        "\t\t\tON DEItemRowData.GlobalBatchID = Batch.GlobalBatchID\n" +
                        "\tWHERE Batch.BankID = ?\n" +
                        "\t\tAND Batch.LockboxID = ?",
                bankID,
                lockboxID
        );
    }
}
