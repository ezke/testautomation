package r360.data;

import utils.BaseUI;

import java.util.Date;

public class DataImportQueue {
    public static void waitForImport(String clientProcessCode, int queueType, Date startDate) {
        BaseUI.wait_ForCondition_ToBeMet(
                ()->checkFileSuccessfullyImported(clientProcessCode, queueType, startDate),
                180,
                15000);
    }

    public static boolean checkFileSuccessfullyImported(String clientProcessCode, int queueType, Date startDate) throws Exception {
        return R360DatabaseAccess.inDataImportQueue(clientProcessCode, queueType,0, startDate);
    }

    public static boolean checkDataImportQueue(String clientProcessCode, int queueType, Date startDate) throws Exception {
        try {
            waitForImport(clientProcessCode, queueType, startDate);
            return true;
        }
        catch(Exception ex) {
            return false;
        }
    }
}
