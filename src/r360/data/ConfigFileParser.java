package r360.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import r360.pages.GlobalVariables;

public class ConfigFileParser {
	public static HashMap<String, String> getFitInputPathes(String configFilename) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		return getImportInputPathes(configFilename, "//FileImportConfigSections/add");
	}
	
	public static HashMap<String, String> getDitInputPathes(String configFilename) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		return getImportInputPathes(configFilename, "//DataImportConfigSections/add");
	}
	
	private static HashMap<String, String> getImportInputPathes(String configFilename, String settingSection) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		HashMap<String, String> paths = new HashMap<String, String>();
		File xmlFile = new File(configFilename);
		Document xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
		xmlDoc.getDocumentElement().normalize();
		XPath xpath = XPathFactory.newInstance().newXPath();
		
		NodeList nodes = (NodeList)xpath.evaluate(settingSection, xmlDoc.getDocumentElement(), XPathConstants.NODESET);
		for(int index=0; index < nodes.getLength(); index++) {
			Element setting = (Element)nodes.item(index);
			paths.put(
				setting.getAttribute("ClientProcessCode"), 
				pathToUnc(setting.getAttribute("InputFolder"))
			);
		}
		return paths;
	}
		
	public static String pathToUnc(String path) {
		if(path.matches("^[a-zA-Z]\\:.*")) {
			return GlobalVariables.environment.ClientDriveUNC + path.replaceAll("^[a-zA-Z]:", "");
		}
		else {
			return path;
		}
	}
}
