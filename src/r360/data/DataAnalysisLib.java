package r360.data;

import java.util.HashMap;
import utils.BaseUI;

public class DataAnalysisLib {
	public static HashMap<String, String> stringsToHashMap(String[] items, String delimiter) {
		HashMap<String, String> result = new HashMap<String, String>();
		for(String item:items) {
			String[] keyValue = item.split(delimiter);
			result.put(keyValue[0], keyValue[1]);
		}
		return result;
	}
	
	public static HashMap<String, String> stringsToHashMap(String[] items) {
		return stringsToHashMap(items, "\\:\\s*");
	}
	
	public static void validateRowContent(String[] expectedValues, HashMap<String, String> row) {
		BaseUI.verify_TableRow_Matches_ExpectedFields(
				"0", 
				stringsToHashMap(expectedValues), 
				row
		);
	}
}
