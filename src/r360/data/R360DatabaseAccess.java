package r360.data;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import r360.pages.GlobalVariables;
import utils.DatabaseConnection;
import utils.TableData;

public class R360DatabaseAccess {
	private static final HashMap<String, String> _functionTable = new HashMap<String, String>() {{
		put("{NEWGUID}", "NEWID()");
		put("{GETDATE}", "GETDATE()");
	}};

	public static boolean inDataImportQueue(String clientProcessCode, int queueType, int responseStatus, Date startDate) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		String sqlQuery = 
				"DECLARE @clientprocess_code VARCHAR(99)\n" +
				"DECLARE @startdate DATETIME\n" +
				"SET @startdate = '" + formatter.format(DateUtils.addMinutes(startDate, -2)) + "'\n" +
				"SET @clientprocess_code = '" + clientProcessCode + "'\n" + 
				"SELECT TOP 1\n" + 
				"		[DataImportQueueID]   --Unique Import ID\n" + 
				"	FROM [WFSDB_R360].[RecHubSystem].[DataImportQueue]\n" + 
				"	WHERE ClientProcessCode = @clientprocess_code\n" +
				"		AND CreationDate > @startdate\n" +
				"		AND QueueType = " + String.valueOf(queueType) +
				"		AND QueueStatus = 150" +
				"		AND ResponseStatus = " + String.valueOf(responseStatus);
		return query(sqlQuery).data.size() > 0;
	}
	
	private static void prepConnection() {
		DatabaseConnection.dbUrl = GlobalVariables.environment.DBConnection;
	}
	
	public static TableData query(String sqlQuery) throws Exception {
		prepConnection();
		return DatabaseConnection.runSQLServerQuery(sqlQuery);
	}

	public static int scalarInt(String sqlQuery) throws Exception {
		prepConnection();
		return DatabaseConnection.runSQLServerScalarQueryInt(sqlQuery);
	}

	private HashMap<String, String> insertFunctionValues(HashMap<String, String> row) {
		HashMap<String, String> result= new HashMap<String, String>(row.entrySet().stream().collect(
				Collectors.toMap(Map.Entry::getKey, item-> translateSqlFunction(item.getValue()))
		));
		return result;
	}

	private static String translateSqlFunction(String value) {
		String result = "'"+value+"'";

		if(_functionTable.containsKey(value)) {
			result=_functionTable.get(value);
		}
		return result;
	}

	public static void insert(String table, HashMap<String, String> row) throws SQLException, ClassNotFoundException {
		prepConnection();
		String fieldList=StringUtils.join( row.keySet(), ",\n\t\t");
		String[] fieldAssignments=row.entrySet().stream().map((item)->item.getKey()+"="+ translateSqlFunction(item.getValue())).toArray(String[]::new);
		String assignments = StringUtils.join(fieldAssignments, ",\n\t\t");
		String sql=
				"INSERT INTO " + table + " (\n" +
				fieldList +
				"\t)\n" +
				"\tSELECT\n" +
				assignments +
				"\n";
		DatabaseConnection.runSQLServerCommand(sql);
	}

    public static void delete(String table, HashMap<String, String> row, String[] filterFields) throws SQLException, ClassNotFoundException {
        prepConnection();
        List<String> filters= Arrays.asList(filterFields);
        String[] filterValues=row.entrySet().stream().filter(entry->filters.contains(entry.getKey())).map(entry->entry.getKey()+"='"+entry.getValue()+"'").toArray(String[]::new);
        String sql=
                "Delete "+table+"\n" +
                        "\tWhere "+StringUtils.join(filterValues, "\n\tAnd ");
        DatabaseConnection.runSQLServerCommand(sql);
    }

	public static String scalarString(String sqlQuery) throws Exception {
        prepConnection();
        return DatabaseConnection.runSQLServerQueryString(sqlQuery);
    }

    public static void PrepPostDepExcReimport(String workgroupID, Date depositDate, String batchID) throws SQLException, ClassNotFoundException {
		prepConnection();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String clearExceptionBatches =
				"Delete [RecHubException].[PostDepositBatchExceptions]\n" +
				"  From [RecHubException].[PostDepositBatchExceptions] ExceptBatches\n" +
				"\tJOIN [RecHubData].[factBatchSummary] BatchSum\n" +
				"\t\tON BatchSum.BatchID = ExceptBatches.BatchID\n" +
				"\tJOIN [RecHubData].[dimClientAccounts] ClientAcc\n" +
				"\t\tON BatchSum.ClientAccountKey = ClientAcc.ClientAccountKey\n" +
				"  WHERE BatchSum.SourceBatchID = " + batchID + "\n" +
				"\tAND ClientAcc.SiteClientAccountID = " + workgroupID + "\n" +
				"\tAND BatchSum.DepositDateKey = " + formatter.format(depositDate);
		String clearLocksOnBatches =
				"DELETE locks\n" +
				"  FROM [RecHubData].[factBatchSummary] batchSummary\n" +
				"	JOIN [RecHubException].[PostDepositTransactionExceptions] locks\n" +
				"		ON locks.BatchID = batchSummary.BatchID AND locks.DepositDateKey = batchSummary.DepositDateKey\n" +
				"  WHERE batchSummary.SourceBatchID = " + batchID + "\n" +
				"	AND batchSummary.DepositDateKey = " + formatter.format(depositDate);
		DatabaseConnection.runSQLServerCommand(clearExceptionBatches);
		DatabaseConnection.runSQLServerCommand(clearLocksOnBatches);
	}
}
