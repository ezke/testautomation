package r360.data;

public class WorkgroupData {
    private final String _importFile;
    private final String _siteCode;
    private final String _entityPath;
    private final String _bankName;
    private final String _bankId;
    private final String _workgroupId;
    private final String _workgroupName;
    private final String[] _requiredFields;

    public WorkgroupData(String siteCode, String entityPath, String bankId, String bankName, String workgroupId, String workgroupName, String importFile, String[] requiredFields) {
        _siteCode=siteCode;
        _entityPath=entityPath;
        _bankId=bankId;
        _bankName=bankName;
        _workgroupId=workgroupId;
        _workgroupName=workgroupName;
        _importFile=importFile;
        _requiredFields = requiredFields;
    }

    public String getSiteCode() {
        return _siteCode;
    }
    
    public String getEntityPath() {
		return _entityPath;
	}

    public String getBankName() {
		return _bankName;
	}

    public String getBankId() {
		return _bankId;
	}

    public String getWorkgroupId() {
		return _workgroupId;
	}

    public String getWorkgroupName() {
		return _workgroupName;
	}

	public String getImportFile() {
        return _importFile;
    }

    public String[] getRequiredFields() {
        return _requiredFields;
    }
}
