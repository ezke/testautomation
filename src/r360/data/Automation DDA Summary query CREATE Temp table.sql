DECLARE @DateKey INT = 20170208 

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
       DROP TABLE #tmpResults;

CREATE TABLE #tmpResults
(
       Entity NVARCHAR(128),
       SiteBankID INT,
       SiteClientAccountID INT,
       DisplayLabel VARCHAR(80),
       BatchSourceKey INT,
       BatchPaymentTypeKey INT,
       DDAKey INT,
       PaymentCount INT,
       PaymentTotal MONEY
);
       
/* Use a cte to get the batch IDs that the user can see and a cte to get the check data "rolled" up for display */
;WITH factBatchSummary_cte AS
(
       SELECT
              RecHubData.factBatchSummary.BatchID,
              RecHubData.dimClientAccounts.SiteBankID,
              RecHubData.dimClientAccounts.SiteClientAccountID,
              RAAM.dbo.Entities.EntityName AS 'Entity'
       FROM
              RecHubData.factBatchSummary
              INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
              INNER JOIN RecHubUser.OLWorkgroups /*Join through OLWorkgroups to ensure WG association still exist*/
                     ON RecHubData.dimClientAccounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
                           AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
              INNER JOIN RAAM.dbo.Entities ON RAAM.dbo.Entities.EntityID = RecHubUser.OLWorkgroups.EntityID
       WHERE
              RecHubData.factBatchSummary.DepositDateKey = @DateKey
              AND RecHubData.factBatchSummary.DepositStatus >= 850/*Deposit Complete*/
              AND RecHubData.factBatchSummary.IsDeleted = 0
),
factCheckData_cte AS
(
       SELECT
              factBatchSummary_cte.Entity,
              factBatchSummary_cte.SiteBankID,
              factBatchSummary_cte.SiteClientAccountID,
              RecHubData.factChecks.BatchSourceKey,
              RecHubData.factChecks.BatchPaymentTypeKey,
              RecHubData.factChecks.DDAKey,
              COUNT(RecHubData.factChecks.ClientAccountKey) AS PaymentCount,
              SUM(RecHubData.factChecks.Amount) AS PaymentTotal
       FROM
              factBatchSummary_cte
              INNER JOIN RecHubData.factChecks ON RecHubData.factChecks.BatchID = factBatchSummary_cte.BatchID
       WHERE 
              RecHubData.factChecks.IsDeleted = 0
              AND RecHubData.factChecks.DepositDateKey = @DateKey
       GROUP BY
              factBatchSummary_cte.Entity,
              factBatchSummary_cte.SiteBankID,
              factBatchSummary_cte.SiteClientAccountID,
              RecHubData.factChecks.BatchSourceKey,
              RecHubData.factChecks.BatchPaymentTypeKey,
              RecHubData.factChecks.DDAKey
)
INSERT INTO #tmpResults
(
       Entity,
       SiteBankID,
       SiteClientAccountID,
       BatchSourceKey,
       BatchPaymentTypeKey,
       DDAKey,
       PaymentCount,
       PaymentTotal
)
SELECT
       Entity,
       SiteBankID,
       SiteClientAccountID,
       BatchSourceKey,
       BatchPaymentTypeKey,
       DDAKey,
       PaymentCount,
       PaymentTotal
FROM factCheckData_cte;

/* Select the final results joining the friendly names and values for the UI */



