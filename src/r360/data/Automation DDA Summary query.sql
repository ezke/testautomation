DECLARE @DateKey INT = 20170208 

SELECT
       RecHubData.dimDDAs.DDA AS dda_entity,
       @DateKey AS DepositDateKey,
       #tmpResults.Entity as entity,
       CONCAT(RecHubData.dimClientAccountsView.SiteClientAccountID, ' - ', RecHubData.dimClientAccountsView.LongName) AS 'workgroup',
       #tmpResults.SiteBankID,
       RecHubData.dimBatchSources.LongName AS payment_source,
       RecHubData.dimBatchPaymentTypes.LongName AS payment_type,
       #tmpResults.PaymentCount AS payment_count,
       #tmpResults.PaymentTotal AS total
FROM 
       #tmpResults
       INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = #tmpResults.BatchSourceKey
       INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = #tmpResults.BatchPaymentTypeKey
       INNER JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = #tmpResults.DDAKey
       INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = #tmpResults.SiteBankID
              AND RecHubData.dimClientAccountsView.SiteClientAccountID = #tmpResults.SiteClientAccountID
              
 IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
       DROP TABLE #tmpResults;
 