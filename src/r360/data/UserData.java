package r360.data;

public class UserData {

    private final String _entityPath;
    private final String _loginName;
    private final String _password;
    private final String _firstName;
    private final String _lastName;
    private final String _email;

    public UserData(String entityPath, String loginName, String password, String firstName, String lastName, String email) {
        _entityPath = entityPath;
        _loginName = loginName;
        _password = password;
        _firstName = firstName;
        _lastName = lastName;
        _email = email;
    }

    public String getEntityPath() { return _entityPath; }

    public String getLoginName() { return _loginName; }

    public String getPassword() { return _password; }

    public String getFirstName() { return _firstName; }

    public String getLastName() { return _lastName; }

    public String getEmail() { return _email; }
}
