﻿function Get-BatchSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @BankID INT,
		@CustomerID INT,
		@LockboxID INT,
		@BatchID INT,
		@DepositDate DATETIME,
		@ProcessingDate DATETIME,
		@BatchTypeCode INT,
		@BatchTypeSubCode INT,
		@DepositStatus INT = 240,
		@DataEntryStatus INT,
		@DEStatusName VARCHAR(40),
		@DESetupID INT,
		@SiteCodeID INT,
		@GlobalBatchID INT = -1,
		@RetryCount INT = 10,
		@xmlData XML;

SET @xmlData = '{0}';

SELECT
	@BankID = Batch.att.value('@BankID','INT'),
	@CustomerID = Batch.att.value('@CustomerID','INT'),
	@LockboxID = Batch.att.value('@LockboxID','INT'),
	@BatchID = Batch.att.value('@BatchID','INT'),
	@DepositDate = CASE 
		WHEN PATINDEX('GETDATE()%',Batch.att.value('@DepositDate','VARCHAR(25)')) > 0 THEN  CONVERT(DATE, GETDATE())
		ELSE Batch.att.value('@DepositDate','DATETIME')
	END,
	@ProcessingDate = CASE 
		WHEN PATINDEX('GETDATE()%',Batch.att.value('@ProcessingDate','VARCHAR(25)')) > 0 THEN CONVERT(DATE, GETDATE())
		ELSE Batch.att.value('@ProcessingDate','DATETIME')
	END,
	@BatchTypeCode = Batch.att.value('@BatchTypeCode','INT'),
	@BatchTypeSubCode = Batch.att.value('@BatchTypeSubCode','INT'),
	@DEStatusName = Batch.att.value('@DEStatus','VARCHAR(40)')
FROM
	@xmlData.nodes('Batch') Batch(att);

SET NOCOUNT OFF;
IF NOT EXISTS( SELECT 1 FROM dbo.Batch WHERE BankID = @BankID AND LockboxID = @LockboxID AND BatchID = @BatchID AND ProcessingDate = @ProcessingDate )
BEGIN
	SELECT 
		@DESetupID = COALESCE(dbo.BatchTypes.DESetupID,dbo.Lockbox.DESetupID),
		@SiteCodeID = SiteCode
	FROM
		dbo.Lockbox
		LEFT JOIN dbo.BatchTypes ON dbo.BatchTypes.BankID = dbo.Lockbox.BankID
			AND dbo.BatchTypes.CustomerID = dbo.Lockbox.CustomerID
			AND dbo.BatchTypes.LockboxID = dbo.Lockbox.LockboxID
	WHERE
		dbo.Lockbox.BankID = @BankID
		AND dbo.Lockbox.CustomerID = @CustomerID
		AND dbo.Lockbox.LockboxID = @LockboxID
		AND dbo.BatchTypes.Code = @BatchTypeCode
		AND dbo.BatchTYpes.SubCode = @BatchTypeSubCode;

	WHILE( @RetryCount > 0 )
	BEGIN
		SELECT @GlobalBatchID = ABS(CHECKSUM(NEWID()))
		IF NOT EXISTS( SELECT 1 FROM dbo.Batch WHERE GlobalBatchID = @GlobalBatchID)
		BEGIN
			SELECT 
				@DataEntryStatus = COALESCE(StatusValue,4)
			FROM 
				dbo.StatusTypes
			WHERE 
				StatusType = 'DataEntry' AND StatusDisplayName = @DEStatusName;

			INSERT INTO dbo.Batch(GlobalBatchID,BankID,LockboxID,BatchID,DepositDate,ProcessingDate,BatchTypeCode,BatchTypeSubCode,DepositStatus,DESetupID,DEStatus,SiteCode)
			VALUES (@GlobalBatchID,@BankID,@LockboxID,@BatchID,@DepositDate,@ProcessingDate,@BatchTypeCode,@BatchTypeSubCode,@DepositStatus,@DESetupID,@DataEntryStatus,@SiteCodeID)
			BREAK;
		END
		ELSE 
		BEGIN
			SET @RetryCount =- 1;
			SET @GlobalBatchID = 0;
		END
	END
END
SELECT @GlobalBatchID AS GlobalBatchID;";
}

function Get-CheckSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @DESetupID INT,
		@DEItemRowDataID UNIQUEIDENTIFIER,
		@GlobalBatchID INT,
		@GlobalCheckID INT,
		@Loop INT = 1,
		@RetryCount INT,
		@RowNo INT,
		@TransactionID INT,
		@xmlData XML

SET @xmlData = '{0}';

DECLARE @Checks TABLE
(
	GlobalBatchID INT,
	TransactionID INT,
	TransactionSequence INT,
	BatchSequence INT,
	CheckSequence INT,
	Amount MONEY,
	RT VARCHAR(30),
	Account VARCHAR(30),
	Serial VARCHAR(30),
	TransactionCode VARCHAR(30),
	LockboxDecisioningReasonID UNIQUEIDENTIFIER
);

;WITH xmlData AS
(
	SELECT 
		CheckElement.att.value('@BankID','INT') AS BankID,
		CheckElement.att.value('@CustomerID','INT') AS CustomerID,
		CheckElement.att.value('@LockboxID','INT') AS LockboxID,
		CheckElement.att.value('@GlobalBatchID','INT') AS GlobalBatchID,
		CheckElement.att.value('@TransactionID','INT') AS TransactionID,
		CheckElement.att.value('@TransactionSequence','INT') AS TransactionSequence,
		CheckElement.att.value('@BatchSequence','INT') AS BatchSequence,
		CheckElement.att.value('@CheckSequence','INT') AS CheckSequence,
		CheckElement.att.value('@Amount','MONEY') AS Amount,
		CheckElement.att.value('@RT','VARCHAR(30)') AS RT,
		CheckElement.att.value('@Account','VARCHAR(30)') AS Account,
		CheckElement.att.value('@Serial','VARCHAR(30)') AS Serial,
		CheckElement.att.value('@TransactionCode','VARCHAR(30)') AS TransactionCode,
		CheckElement.att.value('@DecisioningReasonCode','VARCHAR(25)') AS DecisioningReasonCode
	FROM
		@xmlData.nodes('/Check') CheckElement(att)
)
INSERT INTO @Checks
(
	GlobalBatchID,
	TransactionID,
	TransactionSequence,
	BatchSequence,
	CheckSequence,
	Amount,
	RT,
	Account,
	Serial,
	TransactionCode,
	LockboxDecisioningReasonID
)
SELECT 
	xmlData.GlobalBatchID,
	xmlData.TransactionID,
	xmlData.TransactionSequence,
	xmlData.BatchSequence,
	xmlData.CheckSequence,
	xmlData.Amount,
	xmlData.RT,
	xmlData.Account,
	xmlData.Serial,
	xmlData.TransactionCode,
	dbo.LockboxDecisioningReasons.LockboxDecisioningReasonID
FROM
	xmlData
	INNER JOIN dbo.Lockbox ON dbo.Lockbox.BankID = xmlData.BankID
		AND dbo.Lockbox.CustomerID = xmlData.CustomerID
		AND dbo.Lockbox.LockboxID = xmlData.LockboxID
	LEFT JOIN dbo.DecisioningReasons ON dbo.DecisioningReasons.DecisioningReasonCode = xmlData.DecisioningReasonCode
	LEFT JOIN dbo.LockboxDecisioningReasons ON dbo.LockboxDecisioningReasons.LockboxKey = dbo.Lockbox.LockboxKey
		AND dbo.LockboxDecisioningReasons.DecisioningReasonID = dbo.DecisioningReasons.DecisioningReasonID;

SELECT @GlobalBatchID=GlobalBatchID,@TransactionID=TransactionID FROM @Checks;
SELECT @DESetupID=DESetupID FROM dbo.Batch WHERE GlobalBatchID = @GlobalBatchID;

SET NOCOUNT OFF;
SET @RetryCount = 10;
WHILE( @RetryCount > 0 )
BEGIN
	SELECT @GlobalCheckID = ABS(CHECKSUM(NEWID()))
	IF NOT EXISTS( SELECT 1 FROM dbo.Checks WHERE GlobalCheckID = @GlobalCheckID )
	BEGIN
		INSERT INTO dbo.Checks
		(
			GlobalBatchID,
			GlobalCheckID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			CheckSequence,
			Amount,
			RT,
			Account,
			Serial,
			TransactionCode,
			LockboxDecisioningReasonID
		)
		SELECT
			GlobalBatchID,
			@GlobalCheckID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			CheckSequence,
			Amount,
			RT,
			Account,
			Serial,
			TransactionCode,
			LockboxDecisioningReasonID
		FROM
			@Checks

		SELECT 
			@DEItemRowDataID = NEWID(),
			@RowNo = COALESCE(MAX(RowNo)+1,1)
		FROM 
			dbo.DEItemRowData 
		WHERE 
			GlobalBatchID=@GlobalBatchID 
			AND TransactionID=@TransactionID;

		INSERT INTO dbo.DEItemRowData
		(
			DEItemRowDataID,
			GlobalBatchID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			ImageSequence,
			DocumentType,
			RowNo,
			GlobalCheckID,
			CheckSequence,
			DESetupID
		)
		SELECT
			@DEItemRowDataID,
			GlobalBatchID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			0,
			0,
			@RowNo,
			@GlobalCheckID,
			CheckSequence,
			@DESetupID
		FROM 
			@Checks;
		BREAK;
	END
	ELSE
	BEGIN
		SET @RetryCount =- 1;
		SET @GlobalCheckID = -1;
	END
END

SELECT @GlobalCheckID,@DEItemRowDataID;";
}

function Get-DEItemFieldData()
{
return [string] "SET NOCOUNT ON;
DEClARE @xmlData XML;

SET @xmlData = '{0}';

DECLARE @DataEntryFields TABLE
(
	DEItemRowDataID UNIQUEIDENTIFIER,
	GlobalBatchID INT,
	TableName VARCHAR(32),
	FieldName VARCHAR(36),
	Value VARCHAR(255),
	DecisioningReasonCode VARCHAR(255),
	DecisioningStatus VARCHAR(255)
)
INSERT INTO @DataEntryFields
(
	DEItemRowDataID,
	GlobalBatchID,
	TableName,
	FieldName,
	Value,
	DecisioningReasonCode,
	DecisioningStatus
)
SELECT 
	DataEntryField.att.value('../@DEItemRowDataID','UNIQUEIDENTIFIER') AS DEItemRowDataID,
	DataEntryField.att.value('../@GlobalBatchID','INT') AS GlobalBatchID,
	DataEntryField.att.value('@TableName','VARCHAR(32)') AS TableName,
	DataEntryField.att.value('@FieldName','VARCHAR(36)') AS FieldName,
	DataEntryField.att.value('@Value','VARCHAR(255)') AS Value,
	DataEntryField.att.value('@DecisioningReasonCode','VARCHAR(255)') AS DecisioningReasonCode,
	DataEntryField.att.value('@DecisioningStatus','VARCHAR(255)') AS DecisioningStatus
FROM
	@xmlData.nodes('/DataEntry/DataEntryField') DataEntryField(att);	

SET NOCOUNT OFF;
INSERT INTO dbo.DEItemFieldData
(
	DEItemRowDataID,
	OriginalValue,
	ModifiedValue,
	DESetupFieldID,
	LockboxDecisioningReasonID,
	FieldDecisionStatus
)
SELECT 
	DEItemRowDataID,
	Value,
	Value,
	dbo.DESetupFields.DESetupFieldID,
	dbo.LockboxDecisioningReasons.LockboxDecisioningReasonID,
	CASE UPPER(DecisioningStatus)
		WHEN 'NOTREQUIRED' THEN 0
		WHEN 'PENDING' THEN 1
		WHEN 'ACCEPTED' THEN 2
		WHEN 'REJECTED' THEN 3
		ELSE 0
	END
FROM 
	@DataEntryFields DEF
	INNER JOIN dbo.Batch ON dbo.Batch.GlobalBatchID = DEF.GlobalBatchID
	INNER JOIN dbo.Lockbox ON dbo.Lockbox.BankID = dbo.Batch.BankID AND dbo.Lockbox.LockboxID = dbo.Batch.LockboxID 
	INNER JOIN dbo.DESetupFields ON dbo.DESetupFields.DESetupID = dbo.Batch.DESetupID
		AND DEF.TableName = dbo.DESetupFields.TableName
		AND DEF.FieldName = dbo.DESetupFields.FldName
		AND dbo.DESetupFields.DocumentType = {1}
	LEFT JOIN dbo.DecisioningReasons ON dbo.DecisioningReasons.DecisioningReasonCode = DEF.DecisioningReasonCode 
	LEFT JOIN dbo.LockboxDecisioningReasons ON dbo.LockboxDecisioningReasons.LockboxKey = dbo.Lockbox.LockboxKey
		AND dbo.LockboxDecisioningReasons.DecisioningReasonID = dbo.DecisioningReasons.DecisioningReasonID;"
}

function Get-DocumentSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @GlobalDocumentID INT,
		@Loop INT = 1,
		@RetryCount INT = 10,
		@xmlData XML

SET @xmlData = '{0}';

SET NOCOUNT OFF;
WHILE( @RetryCount > 0 )
BEGIN
	SELECT @GlobalDocumentID = ABS(CHECKSUM(NEWID()))
	IF NOT EXISTS( SELECT 1 FROM dbo.Documents WHERE GlobalDocumentID = @GlobalDocumentID )
	BEGIN
		INSERT INTO dbo.Documents
		(
			GlobalBatchID,
			GlobalDocumentID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			DocumentSequence,
			FileDescriptor
		)
		SELECT 
			DocumentElement.att.value('@GlobalBatchID','INT') AS GlobalBatchID,
			@GlobalDocumentID,
			DocumentElement.att.value('@TransactionID','INT') AS TransactionID,
			DocumentElement.att.value('@TransactionSequence','INT') AS TransactionSequence,
			DocumentElement.att.value('@BatchSequence','INT') AS BatchSequence,
			DocumentElement.att.value('@DocumentSequence','INT') AS DocumentSequence,
			DocumentElement.att.value('@FileDescriptor','VARCHAR(30)') AS FileDescriptor
		FROM
			@xmlData.nodes('/Document') DocumentElement(att);	
		BREAK;
	END
	ELSE
	BEGIN
		SET @RetryCount =- 1;
		SET @GlobalDocumentID = -1;
	END
END
SELECT @GlobalDocumentID";
}

function Get-StubSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @DESetupID INT,
		@GlobalBatchID INT,
		@GlobalStubID INT,
		@DEItemRowDataID UNIQUEIDENTIFIER,
		@Loop INT = 1,
		@RetryCount INT = 10,
		@RowNo INT,
		@TransactionID INT,
		@xmlData XML

SET @xmlData = '{0}';

DECLARE @Stub TABLE
(
	GlobalBatchID INT,
	TransactionID INT,
	TransactionSequence INT,
	BatchSequence INT,
	StubSequence INT,
	Amount MONEY,
	Account VARCHAR(80)
);

INSERT INTO @Stub
(
	GlobalBatchID,
	TransactionID,
	TransactionSequence,
	BatchSequence,
	StubSequence,
	Amount,
	Account
)
SELECT 
	StubElement.att.value('@GlobalBatchID','INT') AS GlobalBatchID,
	StubElement.att.value('@TransactionID','INT') AS TransactionID,
	StubElement.att.value('@TransactionSequence','INT') AS TransactionSequence,
	StubElement.att.value('@BatchSequence','INT') AS BatchSequence,
	StubElement.att.value('@StubSequence','INT') AS StubSequence,
	StubElement.att.value('@Amount','MONEY') AS Amount,
	StubElement.att.value('@Account','VARCHAR(80)') AS Account
FROM
	@xmlData.nodes('/DataEntry') StubElement(att);	

SELECT @GlobalBatchID=GlobalBatchID,@TransactionID=TransactionID FROM @Stub;
SELECT @DESetupID=DESetupID FROM dbo.Batch WHERE GlobalBatchID = @GlobalBatchID;

SET NOCOUNT OFF;
WHILE( @RetryCount > 0 )
BEGIN
	SELECT @GlobalStubID = ABS(CHECKSUM(NEWID()))
	IF NOT EXISTS( SELECT 1 FROM dbo.Stubs WHERE GlobalStubID = @GlobalStubID )
	BEGIN
		INSERT INTO dbo.Stubs
		(
			GlobalBatchID,
			GlobalStubID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			StubSequence,
			Amount,
			AccountNumber
		)
		SELECT 
			GlobalBatchID,
			@GlobalStubID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			StubSequence,
			Amount,
			Account
		FROM
			@Stub;

		SELECT 
			@DEItemRowDataID = NEWID(),
			@RowNo = COALESCE(MAX(RowNo)+1,1)
		FROM 
			dbo.DEItemRowData 
		WHERE 
			GlobalBatchID=@GlobalBatchID 
			AND TransactionID=@TransactionID;

		INSERT INTO dbo.DEItemRowData
		(
			DEItemRowDataID,
			GlobalBatchID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			ImageSequence,
			DocumentType,
			RowNo,
			GlobalStubID,
			StubSequence,
			DESetupID
		)
		SELECT
			@DEItemRowDataID,
			GlobalBatchID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			0,
			1,
			@RowNo,
			@GlobalStubID,
			StubSequence,
			@DESetupID
		FROM 
			@Stub;
		BREAK;
	END
	ELSE
	BEGIN
		SET @RetryCount =- 1;
		SET @GlobalStubID = -1;
	END
END
SELECT @GlobalStubID,@DEItemRowDataID;";
}

function Get-TransactionSQL()
{
return [string] "DECLARE @GlobalBatchID INT = {0},
		@TransactionID INT = {1},
		@DecisionSource VARCHAR(25) = '{2}';

INSERT INTO dbo.Transactions(GlobalBatchID,TransactionID,[Sequence],DecisionSourceStatus)
SELECT
	@GlobalBatchID,
	@TransactionID,
	@TransactionID,
	CASE UPPER(@DecisionSource)
		WHEN 'NOTREQUIRED' THEN 0
		WHEN 'BATCHLOGON' THEN 1
		WHEN 'DATAENTRY' THEN 2
		WHEN 'ACCEPTED' THEN 3
		WHEN 'REJECTED' THEN 4
	END ;";
}

Export-ModuleMember Get-BatchSQL;
Export-ModuleMember Get-CheckSQL;
Export-ModuleMember Get-DEItemFieldData;
Export-ModuleMember Get-DocumentSQL;
Export-ModuleMember Get-StubSQL;
Export-ModuleMember Get-TransactionSQL;
