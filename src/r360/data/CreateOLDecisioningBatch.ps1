﻿param
(
	[parameter(Mandatory = $true)][string] $DBServer,
	[parameter(Mandatory = $true)][string] $DBName,
	[parameter(Mandatory = $true)][string] $ImportFile,
	[string] $UserID = $null,
	[string] $Password = $null
)

################################################################################
################################################################################
#
#
# NOT TO BE RELEASED OUTSIDE OF R360 Dev/QA.
#
#
################################################################################
################################################################################

################################################################################
# 01/20/2017 PT #136378841 JPB 0.00	Created.
# 02/15/2017 PT #133886021 JPB 1.01 Support older version of integraPAY, addtional logging.
#							Support for DE Required/Mask In.
#							Support for current date on Processing/DepositDate
# 03/01/2017 PT #138739823 Added SiteCode so current processing date match
################################################################################

################################################################################
# Ideally the SQL code in this script would be SPs. However, this is a test data
# generator and will moved from system to system in Dev/QA. Keeping the SQL code
# here will make this easier. 
#
# The longer SQL statements were tested/debugged in SSMS and simply copied. This
# was done to make it easier to read/troubleshoot/modify.
#################################################################################
## cls
Import-Module -Name .\CreateOLDecisioningBatch_BatchSQL -Force;
Import-Module -Name .\CreateOLDecisioningBatch_SetupSQL -Force;

$ScriptName = "CreateOLDecisioningBatch";
$ScriptVerison = "1.02";

################################################################################
# Write message to log file.
################################################################################
function Write-Log([String] $local:LogInformation)
{
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($LogFile) -eq $null )
		{
			$LogFile = Get-Location + "\" + $LogFile;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $LogFile -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $local:LogInformation | Out-File -FilePath $LogFile -Encoding unicode -Append;
	}
}

################################################################################
# Open the database connection
################################################################################
function Create-DatabaseConnection([ref] $local:dbConnection,[string] $local:DBServer,[string] $local:DBName,[string] $local:UserID,[string] $local:Password)
{
	if( $local:UserID.Length -gt 0 )
	{
		$local:dbConnectionOptions = [string]::Format("Data Source={0}; Initial Catalog={1};UId={2};Pwd={3}",$local:DBServer,$local:DBName,$local:UserID,$local:Password);
	}
	else
	{
		$local:dbConnectionOptions = [string]::Format("Data Source={0}; Initial Catalog={1};Integrated Security=SSPI",$local:DBServer,$local:DBName);
	}
	$local:dbConnection.value = New-Object System.Data.SqlClient.SqlConnection($local:dbConnectionOptions);

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		Write-Log $event.Message 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$Results = $false;
		}
	};
	$local:dbConnection.value.add_InfoMessage($handler);
	$local:dbConnection.value.FireInfoMessageEventOnUserErrors = $true;
}

################################################################################
# Open the database connection
################################################################################
function Open-DatabaseConnection([System.Data.SqlClient.SqlConnection] $local:dbConnection)
{
	try
	{
		$local:dbConnection.Open();
		$Results = $true;
	}
	catch
	{
		Write-Log "Error Opening Database Connection";
		Write-Log $_.Exception.Message;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# ExecuteSQLStatement
################################################################################
function ExecuteSQLStatement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[string] $local:SQL,[ref] $local:SQLResults)
{
	$local:Results = $true;
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$local:dbCommand.CommandType = [System.Data.CommandType]::Text;
	$local:dbCommand.CommandText = $local:SQL;
	try
	{
		$local:SQLResults.value = $local:dbCommand.ExecuteNonQuery();
	}
	catch
	{
		$local:Results = $false;
		Write-Log "Error executing SQL Command";
		Write-Log $_.Exception.Message;
	}
	return $local:Results;
}

################################################################################
# ExecuteSQLReader
################################################################################
function ExecuteSQLReader([System.Data.SqlClient.SqlConnection] $local:dbConnection,[ref] $local:SQLReader,[string] $local:SQL)
{
	$local:Results = $true;
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$local:dbCommand.CommandType = [System.Data.CommandType]::Text;
	$local:dbCommand.CommandText = $local:SQL;
	try
	{
		$local:SQLReader.value = $local:dbCommand.ExecuteReader();
	}
	catch
	{
	}
	return $local:Results; 
}

################################################################################
# ProcessBankElement
################################################################################
function ProcessBankElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Bank)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	
	$local:SQL = [string]::Format((Get-BankSQL),$local:Bank.OuterXml);

	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq -1 )
		{ 
			Write-Log "BankID already existed";
		}
		elseif( $local:SQLResults -eq 0 )
		{ 
			Write-Log "BankID already existed";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log "BankID successfully added";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert BankID";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessCustomerElement
################################################################################
function ProcessCustomerElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Customer,[int]$local:BankID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	$local:SQL = New-Object System.Text.StringBuilder;
	$local:SQL.AppendFormat("IF NOT EXISTS(SELECT 1 FROM dbo.Customer WHERE BankID = {0} AND CustomerID = {1})",$local:BankID,$local:Customer.CustomerID) | Out-Null;
	$local:SQL.AppendLine() | Out-Null;
    $local:SQL.AppendLine("BEGIN") | Out-Null;
	$local:SQL.Append("INSERT INTO dbo.Customer(BankID,CustomerID,Name) ") | Out-Null;
	$local:SQL.AppendFormat("VALUES ({0},{1},'{2}');",$local:BankID,$local:Customer.CustomerID,$local:Customer.Name) | Out-Null;
    $local:SQL.AppendLine() | Out-Null;
    $local:SQL.AppendLine("END") | Out-Null;
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL.ToString() ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq -1 )
		{ 
			Write-Log "CustomerID already existed";
		}
		elseif( $local:SQLResults -eq 0 )
		{ 
			Write-Log "CustomerID already existed";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log "CustomerID successfully added";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert CustomerID";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessLockboxElement
################################################################################
function ProcessLockboxElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Lockbox,[int]$local:BankID,[int]$local:CustomerID,[int]$local:SiteCode)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;

	$local:SQL = [string]::Format((Get-LockboxSQL),$local:BankID,$local:CustomerID,$local:SiteCode,$local:Lockbox.OuterXml);
	
	try
	{
		if( (ExecuteSQLStatement $local:dbConnection $local:SQL.ToString() ([ref]$local:SQLResults)) )
		{
			if( $local:SQLResults -eq -1 )
			{ 
				Write-Log "LockboxID already existed";
			}
			elseif( $local:SQLResults -eq 0 )
			{ 
				Write-Log "LockboxID already existed";
			}
			elseif( $local:SQLResults -gt 0 )
			{
				Write-Log "LockboxID successfully added";
			}
			else
			{
				Write-Log "Unknown error executing SQL Command to insert LockboxID";
				$local:Results = $false;
			}
		}
	}
	catch
	{
		Write-Host $_.Exception.Message;
	}
	return $local:Results;
}

################################################################################
# ProcessBatchTypesElement
################################################################################
function ProcessBatchTypesElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:BatchTypes,[int] $local:BankID,[int] $local:CustomerID,[int] $local:LockboxID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	
	$local:SQL = [string]::Format((Get-BatchTypesSQL),$local:BankID,$local:CustomerID,$local:LockboxID,$local:BatchTypes.OuterXml);
	
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq -1 )
		{ 
			Write-Log "BatchType already existed";
		}
		elseif( $local:SQLResults -eq 0 )
		{ 
			Write-Log "BatchType already existed";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log "BatchType successfully added";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert BatchType";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessDataEntryElement
################################################################################
function CreateDataEntryColumns([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:DataEntrySetups)
{
	[int] $local:SQLResults = 0;
	[System.Data.SqlClient.SqlDataReader] $local:SQLReader = $null;
	$local:Results = $true;
	$local:SQL = New-Object System.Text.StringBuilder;
	foreach( $local:DataEntryField in $local:DataEntrySetups.DataEntry.DataEntryField)
	{
		$local:SQL.Clear();
		$local:SQL.AppendFormat("IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{0}' AND COLUMN_NAME = '{1}' ) ",
			$local:DataEntryField.TableName,$local:DataEntryField.Name) | Out-Null;
		$local:SQL.AppendFormat("ALTER TABLE dbo.{0} ADD {1} ",$local:DataEntryField.TableName,$local:DataEntryField.Name) | Out-Null;
		switch( $local:DataEntryField.DataType.ToUpper() )
		{
			"ALPHANUMERIC"
				{
					$local:SQL.AppendFormat("VARCHAR({0});",$local:DataEntryField.Length);
				}
			"DATE"
				{
					$local:SQL.AppendLine("DATETIME;");
				}
			default
				{
					$local:SQL.AppendFormat("{0};",$local:DataEntryField.DataType.ToUpper());
				}
		}
		if( (ExecuteSQLStatement $local:dbConnection $local:SQL.ToString()([ref]$local:SQLResults)) )
		{
			if( $local:SQLResults -ne -1 )
			{
				Write-Log ([string]::Format("Unknown error executing SQL Command to create data entry column {0}.{0}",$local:DataEntryField.TableName,$local:DataEntryField.Name));
				$local:Results = $false;
			}
			else
			{
				Write-Log ([string]::Format("Successfully processed data entry column {0}.{1}",$local:DataEntryField.TableName,$local:DataEntryField.Name));
			}
		}
	}
	return $local:Results
}

################################################################################
# ProcessDataEntryElement
################################################################################
function ProcessDataEntryElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:DataEntrySetups)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	
	$local:SQL = [string]::Format((Get-DataEntrySetupSQL),$local:DataEntrySetups.OuterXml);
	
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq -1 -or $local:SQLResults -eq 0 )
		{ 
			Write-Log "DESetup already existed";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log "DESetup successfully added";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert DESetup";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessDecisioningReasonsElement
################################################################################
function ProcessDecisioningReasonsElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:DecisioningReasons,[int] $local:BankID,[int] $local:CustomerID,[int] $local:LockboxID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;

	$local:SQL = [string]::Format((Get-DecisioningReasonsSQL),$local:BankID,$local:CustomerID,$local:LockboxID,$local:DecisioningReasons.OuterXml);
	
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq 0 )
		{ 
			Write-Log "DecisioningReasonCode already existed";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log "DecisioningReasonCode successfully added";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert DecisioningReasonCode";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessSchedulesElement
################################################################################
function ProcessSchedulesElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Schedules,[int] $local:BankID,[int] $local:CustomerID,[int] $local:LockboxID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;

	
	$local:SQL = [string]::Format((Get-SchedulesSQL),$local:BankID,$local:CustomerID,$local:LockboxID,$local:Schedules.OuterXml);
	
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq 0 )
		{ 
			Write-Log "Schedule already existed";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log "Schedule successfully added";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert Schedule";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessSiteCode
################################################################################
function ProcessSiteCode([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:SiteCode)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;

	
	$local:SQL = [string]::Format((Get-SiteCodeSQL),$local:SiteCode.OuterXml);
	
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -gt 0 )
		{
			Write-Log "SiteCode successfully added or updated";
		}
		else
		{
			Write-Log "Unknown error executing SQL Command for SiteCode";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# InsertTransaction
################################################################################
function InsertTransaction([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Transaction,[Int] $local:GlobalBatchID,[Int] $local:TransactionID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	
	$local:SQL = [string]::Format((Get-TransactionSQL),$local:GlobalBatchID,$local:TransactionID,$local:Transaction.DecisionSource);
	
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -eq 0 )
		{ 
			Write-Log "Transaction not added";
		}
		elseif( $local:SQLResults -gt 0 )
		{
			Write-Log ([string]::Format("Successfully added transaction {0}",$local:TransactionID));
		}
		else
		{
			Write-Log "Unknown error executing SQL Command to insert Transaction";
			$local:Results = $false;
		}
	}
	return $local:Results;
}

################################################################################
# ProcessCheckDataEntryElements
################################################################################
function ProcessCheckDataEntryElements([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:DataEntry,[Int] $local:GlobalCheckID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	$local:SQL = New-Object System.Text.StringBuilder;
	foreach( $local:DataEntryField in $local:DataEntry.DataEntryField )
	{
		$local:SQL.Clear() | Out-Null;
		$local:SQL.AppendFormat("UPDATE dbo.{0} SET {1}='{2}' WHERE GlobalCheckID = {3}",
			$local:DataEntryField.TableName,
			$local:DataEntryField.FieldName,
			$local:DataEntryField.Value,
			$local:GlobalCheckID) | Out-Null;
		if( (ExecuteSQLStatement $local:dbConnection $local:SQL.ToString() ([ref]$local:SQLResults)) )
		{
			if( $local:SQLResults -eq 0 )
			{ 
				Write-Log "DataEntryField not updated";
			}
			elseif( $local:SQLResults -gt 0 )
			{
				Write-Log ([string]::Format("Successfully updated data entry eield {0}.{1}",$local:DataEntryField.TableName,$local:DataEntryField.FieldName));
			}
			else
			{
				Write-Log "";
				$local:Results = $false;
			}
		}
	}
}

################################################################################
# ProcessCheckElement
################################################################################
function ProcessCheckElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,	[System.Xml.XmlElement] $local:Check)
{
	[int] $local:SQLResults = 0;
	[System.Data.SqlClient.SqlDataReader] $local:SQLReader = $null;
	$local:Results = $true;
	$local:SQL = [string]::Format((Get-CheckSQL),$local:Check.OuterXml);
	
	if( (ExecuteSQLReader $local:dbConnection ([ref]$local:SQLReader) $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLReader.HasRows )
		{
			$local:SQLReader.Read() | Out-Null;
			$local:GlobalCheckID = $local:SQLReader.Item(0);
			$local:DEItemRowDataID = $local:SQLReader.Item(1);
			$local:SQLReader.Close();
			if( $local:GlobalCheckID -eq -1 )
			{
				Write-Log "Could not get a GlobalCheckID in 10 tries";
			}
			else
			{
				Write-Log ([string]::Format("Successfully added check, check sequence {0}, batch sequence {1}, amount {2}",$local:Check.CheckSequence,$local:Check.BatchSequence,$local:Check.Amount));
				if( $local:Check.SelectSingleNode("DataEntry") -ne $null )
				{
					ProcessCheckDataEntryElements $local:dbConnection $local:Check.DataEntry $local:GlobalCheckID;

					$local:Check.DataEntry.SetAttribute("DEItemRowDataID", $local:DEItemRowDataID.ToString());
					$local:Check.DataEntry.SetAttribute("GlobalBatchID", $local:Check.GlobalBatchID.ToString());

					InsertDEItemFieldData $local:dbConnection $local:Check.DataEntry 0;
				}
			}
		}
	}
}

################################################################################
# ProcessChecksElement
################################################################################
function ProcessChecksElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Checks,[ref] $local:BatchSequence)
{
	[int] $local:CheckSequence = 1;
	[int] $local:TransactionSequence = 1;
	foreach( $local:Check in $local:Checks.Check )
	{
		$local:Check.SetAttribute("BankID", $local:Checks.BankID.ToString());
		$local:Check.SetAttribute("CustomerID", $local:Checks.CustomerID.ToString());
		$local:Check.SetAttribute("LockboxID", $local:Checks.LockboxID.ToString());
		$local:Check.SetAttribute("GlobalBatchID", $local:Checks.GlobalBatchID.ToString());
		$local:Check.SetAttribute("TransactionID", $local:Checks.TransactionID.ToString());
		$local:Check.SetAttribute("TransactionSequence", $local:TransactionSequence.ToString());
		$local:Check.SetAttribute("CheckSequence", $local:CheckSequence.ToString());
		$local:Check.SetAttribute("BatchSequence", $local:BatchSequence.value.ToString());
		
		ProcessCheckElement $local:dbConnection $local:Check $local:Checks.BankID $local:Checks.CustomerID $local:Checks.LockboxID $local:Checks.GlobalBatchID $local:Checks.TransactionID $local:TransactionSequence $local:BatchSequence $local:CheckSequence;
		$local:BatchSequence.value++;
		$local:CheckSequence++;
		$local:TransactionSequence++;
	}
}


################################################################################
# InsertStubSource
################################################################################
function InsertStubSource([System.Data.SqlClient.SqlConnection] $local:dbConnection,[Int] $local:GlobalStubID,[Int]$local:GlobalDocumentID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	$SQL = [string]::Format("INSERT INTO dbo.StubSource(GlobalStubID,GlobalDocumentID) VALUES ({0},{1});",$local:GlobalStubID,$local:GlobalDocumentID);
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -gt 0 )
		{
			Write-Log "Successfully added stub source";
		}
	}
}

################################################################################
# InsertDEItemFieldData
################################################################################
function InsertDEItemFieldData([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement]$local:DataEntry,[Int]$local:DocumentType)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	$SQL = [string]::Format((Get-DEItemFieldData),$local:DataEntry.OuterXml,$local:DocumentType);
	if( (ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLResults -gt 0 )
		{
			switch( $local:DocumentType )
			{
				0 { $local:Message = "Successfully added check DEItemFieldData" }
				1 { $local:Message = "Successfully added stub DEItemFieldData" }
			}			
			Write-Log $local:Message;
		}
	}
}

################################################################################
# UpdateStubsDataEntry
################################################################################
function UpdateStubsDataEntry([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement]$local:DataEntry,[Int] $local:GlobalStubID)
{
	[int] $local:SQLResults = 0;
	$local:Results = $true;
	$local:SQL = New-Object System.Text.StringBuilder;
	foreach( $local:DataEntryField in $local:DataEntry.DataEntryField )
	{
		$local:SQL.Clear() | Out-Null;
		if( $local:DataEntryField.TableName.ToUpper() -eq "STUBSDATAENTRY" )
		{
			$local:SQL.AppendFormat("UPDATE dbo.{0} SET {1}='{2}' WHERE GlobalStubID = {3}",
				$local:DataEntryField.TableName,
				$local:DataEntryField.FieldName,
				$local:DataEntryField.Value,
				$local:GlobalStubID) | Out-Null;
			if( (ExecuteSQLStatement $local:dbConnection $local:SQL.ToString() ([ref]$local:SQLResults)) )
			{
				if( $local:SQLResults -eq 0 )
				{ 
					Write-Log "DataEntryField not updated";
				}
				elseif( $local:SQLResults -gt 0 )
				{
					Write-Log ([string]::Format("Successfully updated data entry field {0}.{1}",$local:DataEntryField.TableName,$local:DataEntryField.FieldName));
				}
				else
				{
					Write-Log "Unknown error executing SQL Command to update data entry field";
					$local:Results = $false;
				}
			}
		}
	}
}

################################################################################
# InsertStub
################################################################################
function InsertStub([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Stub)
{
	[int] $local:SQLResults = 0;
	[System.Data.SqlClient.SqlDataReader] $local:SQLReader = $null;
	$local:Results = $true;
	$local:SQL = [string]::Format((Get-StubSQL),$local:Stub.OuterXml);
	
	if( (ExecuteSQLReader $local:dbConnection ([ref]$local:SQLReader) $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLReader.HasRows )
		{
			$local:SQLReader.Read() | Out-Null;
			$local:GlobalStubID = $local:SQLReader.Item(0);
			$local:DEItemRowDataID = $local:SQLReader.Item(1);
			$local:SQLReader.Close();
			if( $local:GlobalStubID -eq -1 )
			{
				Write-Log "Could not get a GlobalStubID in 10 tries";
			}
			else
			{
				Write-Log ([string]::Format("Successfully added stub, stub sequence {0}, batch sequence {1}",$local:Stub.StubSequence,$local:Stub.BatchSequence));
				InsertStubSource $local:dbConnection $local:GlobalStubID $local:Stub.GlobalDocumentID;
				$local:Stub.SetAttribute("DEItemRowDataID", $local:DEItemRowDataID.ToString());
				
				UpdateStubsDataEntry $local:dbConnection $local:Stub $local:GlobalStubID;
				InsertDEItemFieldData $local:dbConnection $local:Stub 1;
			}
		}
	}
}

################################################################################
# ProcessDocumentDataEntryElements
# This function creates the stub records. It is named after the XML, not what table in this case.
################################################################################
function ProcessDocumentDataEntryElements([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Document,[ref] $local:BatchSequence,[ref] $local:StubSequence)
{
	[int] $local:TransactionSequence = 1;	
	foreach( $local:Stub in $local:Document.DataEntry )
	{
		$local:StubAmount = $local:Stub.SelectSingleNode("DataEntryField[@TableName='Stubs' and @FieldName='Amount']");
		$local:StubAccount = $local:Stub.SelectSingleNode("DataEntryField[@TableName='Stubs' and @FieldName='AccountNumber']");

		$local:Stub.SetAttribute("GlobalBatchID", $local:Document.GlobalBatchID.ToString());
		$local:Stub.SetAttribute("GlobalDocumentID", $local:Document.GlobalDocumentID.ToString());
		$local:Stub.SetAttribute("TransactionID", $local:Document.TransactionID.ToString());
		$local:Stub.SetAttribute("TransactionSequence", $local:TransactionSequence.ToString());
		$local:Stub.SetAttribute("BatchSequence", $local:BatchSequence.value.ToString());
		$local:Stub.SetAttribute("StubSequence", $local:StubSequence.value.ToString());
		if( $local:StubAmount -ne $null )
		{
			$local:Stub.SetAttribute("Amount", $local:StubAmount.Value.ToString());
		}
		if( $local:StubAccount -ne $null )
		{
			$local:Stub.SetAttribute("Account", $local:StubAccount.Value.ToString());
		}
		
		InsertStub $local:dbConnection $local:Stub;
		
        $local:BatchSequence.value++;
		$local:StubSequence.value++;
		$local:TransactionSequence++;
	}
}

################################################################################
# ProcessDocumentElement
################################################################################
function ProcessDocumentElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Document,[ref] $local:BatchSequence,[ref] $local:StubSequence)
{
	[int] $local:SQLResults = 0;
	[System.Data.SqlClient.SqlDataReader] $local:SQLReader = $null;
	$local:Results = $true;
		
	$local:Document.SetAttribute("BatchSequence", $local:BatchSequence.value.ToString());
	$local:SQL = [string]::Format((Get-DocumentSQL),$local:Document.OuterXml);

	if( (ExecuteSQLReader $local:dbConnection ([ref]$local:SQLReader) $local:SQL([ref]$local:SQLResults)) )
	{
		if( $local:SQLReader.HasRows )
		{
		    $local:SQLReader.Read() | Out-Null;
			$local:GlobalDocumentID = $local:SQLReader.Item(0);
			$local:SQLReader.Close();
			if( $local:GlobalDocumentID -eq -1 )
			{
				Write-Log "Could not get a GlobalDocumentID in 10 tries";
			}
			else
			{
			    Write-Log ([string]::Format("Successfully added document, document sequence {0}, batch sequence {1}",$local:Document.DocumentSequence,$local:Document.BatchSequence));
				if( $local:Document.SelectSingleNode("DataEntry") -ne $null )
				{
					$local:BatchSequence.value++;
					$local:Document.SetAttribute("GlobalDocumentID", $local:GlobalDocumentID.ToString());
					ProcessDocumentDataEntryElements $local:dbConnection $local:Document $local:BatchSequence $local:StubSequence;
				}
			}
		}
	}
}

################################################################################
# ProcessDocumentsElement
################################################################################
function ProcessDocumentsElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Documents,[ref] $local:BatchSequence)
{
	[int] $local:DocumentSequence = 1;
	[int] $local:TransactionSequence = 1;
	[int] $local:StubSequence = 1;
	foreach( $local:Document in $local:Documents.SelectNodes("Document") )
	{
		$local:Document.SetAttribute("GlobalBatchID", $local:Documents.GlobalBatchID.ToString());
		$local:Document.SetAttribute("TransactionID", $local:Documents.TransactionID.ToString());
		$local:Document.SetAttribute("TransactionSequence", $local:TransactionSequence.ToString());
		$local:Document.SetAttribute("DocumentSequence", $local:DocumentSequence.ToString());

		ProcessDocumentElement $local:dbConnection $local:Document $local:BatchSequence ([ref] $local:StubSequence);
		
		$local:DocumentSequence++;
		$local:TransactionSequence++;
	}
}

################################################################################
# ProcessTransactionElements
################################################################################
function ProcessTransactionElements([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Transactions)
{
	$local:TransactionID = 1;
	[int] $local:BatchSequence = 1;
	foreach( $local:Transaction in $local:Transactions.Transaction )
	{
		if( (InsertTransaction $local:dbConnection $local:Transaction $local:Transactions.GlobalBatchID $local:TransactionID) )
		{
		    if( $local:Transaction.SelectSingleNode("Checks") -ne $null )
			{
				$local:Transaction.Checks.SetAttribute("BankID", $local:Transactions.BankID.ToString());
				$local:Transaction.Checks.SetAttribute("CustomerID", $local:Transactions.CustomerID.ToString());
				$local:Transaction.Checks.SetAttribute("LockboxID", $local:Transactions.LockboxID.ToString());
				$local:Transaction.Checks.SetAttribute("GlobalBatchID", $local:Transactions.GlobalBatchID.ToString());
				$local:Transaction.Checks.SetAttribute("TransactionID", $local:TransactionID.ToString());
				
				ProcessChecksElement $local:dbConnection $local:Transaction.Checks ([ref]$local:BatchSequence);
			}
			if( $local:Transaction.SelectSingleNode("Documents") -ne $null )
			{
			    $local:Transaction.Documents.SetAttribute("GlobalBatchID", $local:Transactions.GlobalBatchID.ToString());
				$local:Transaction.Documents.SetAttribute("TransactionID", $local:TransactionID.ToString());

				ProcessDocumentsElement $local:dbConnection $local:Transaction.Documents ([ref]$local:BatchSequence);
			}
			
			$local:TransactionID++;
		}
	}
}

################################################################################
# ProcessBatchElement
################################################################################
function ProcessBatchElement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Batch)
{
	[int] $local:SQLResults = 0;
	[System.Data.SqlClient.SqlDataReader] $local:SQLReader = $null;
	$local:Results = $true;
	
	$local:SQL = [string]::Format((Get-BatchSQL),$local:Batch.OuterXml);
	if( (ExecuteSQLReader $local:dbConnection ([ref]$local:SQLReader) $local:SQL ([ref]$local:SQLResults)) )
	{
		if( $local:SQLReader.HasRows )
		{
			$local:SQLReader.Read() | Out-Null;
			$local:GlobalBatchID = $local:SQLReader.Item(0);
			$local:SQLReader.Close();
			if( $local:GlobalBatchID -eq -1 )
			{
				if( $local:Batch.ProcessingDate.ToUpper().Contains("GETDATE") )
				{
					Write-Log ([string]::Format("BatchID {0} for ProcessingDate {1} already exists",$local:Batch.BatchID,(Get-Date –f "MM/dd/yyyy")));
					Write-Host ([string]::Format("BatchID {0} for ProcessingDate {1} already exists",$local:Batch.BatchID,(Get-Date –f "MM/dd/yyyy")));
				}
				else
				{
					Write-Log ([string]::Format("BatchID {0} for ProcessingDate {1} already exists",$local:Batch.BatchID,$local:Batch.ProcessingDate));
					Write-Host ([string]::Format("BatchID {0} for ProcessingDate {1} already exists",$local:Batch.BatchID,$local:Batch.ProcessingDate));
				}
			}
			else
			{
				if( $local:Batch.ProcessingDate.ToUpper().Contains("GETDATE") )
				{
					Write-Log ([string]::Format("Successfully added Batch {0} for ProcessingDate {1}",$local:Batch.BatchID,(Get-Date –f "MM/dd/yyyy")));
					Write-Host ([string]::Format("Successfully added Batch {0} for ProcessingDate {1}",$local:Batch.BatchID,(Get-Date –f "MM/dd/yyyy")));
				}
				else
				{
					Write-Log ([string]::Format("Successfully added Batch {0} for ProcessingDate {1}",$local:Batch.BatchID,$local:Batch.ProcessingDate));
					Write-Host ([string]::Format("Successfully added Batch {0} for ProcessingDate {1}",$local:Batch.BatchID,$local:Batch.ProcessingDate));
				}
				$local:Batch.Transactions.SetAttribute("BankID", $local:Batch.BankID.ToString());
                $local:Batch.Transactions.SetAttribute("CustomerID", $local:Batch.CustomerID.ToString());
                $local:Batch.Transactions.SetAttribute("LockboxID", $local:Batch.LockboxID.ToString());
                $local:Batch.Transactions.SetAttribute("GlobalBatchID", $local:GlobalBatchID.ToString());

				ProcessTransactionElements $local:dbConnection $local:Batch.Transactions;
			}
		}
	}
}

################################################################################
# ProcessSetup
################################################################################
function ProcessSetup([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Setup)
{
	$local:Results = $false;
	if( ($local:Results = ProcessSiteCode $local:dbConnection $local:Setup.SiteCode) )
	{
		if( ($local:Results = ProcessBankElement $local:dbConnection $local:Setup.Bank) )
		{
			if( ($local:Results = ProcessCustomerElement $local:dbConnection $local:Setup.Customer $local:Setup.Bank.BankID) )
			{
				if( ($local:Results = ProcessLockboxElement $local:dbConnection $local:Setup.Lockbox $local:Setup.Bank.BankID $local:Setup.Customer.CustomerID $local:Setup.SiteCode.SiteCode) )
				{
					if( ($local:Results = ProcessBatchTypesElement $local:dbConnection $local:Setup.BatchTypes $local:Setup.Bank.BankID $local:Setup.Customer.CustomerID $local:Setup.Lockbox.LockboxID) )
					{
						if( ($local:Results = CreateDataEntryColumns $local:dbConnection $local:Setup.DataEntrySetups) )
						{
							if( ($local:Results = ProcessDataEntryElement $local:dbConnection $local:Setup.DataEntrySetups) )
							{
								if( ($local:Results = ProcessDecisioningReasonsElement $local:dbConnection $local:Setup.DecisioningReasons $local:Setup.Bank.BankID $local:Setup.Customer.CustomerID $local:Setup.Lockbox.LockboxID) )
								{
									$local:Results = ProcessSchedulesElement $local:dbConnection $local:Setup.Schedules $local:Setup.Bank.BankID $local:Setup.Customer.CustomerID $local:Setup.Lockbox.LockboxID;
								}
							}
						}
					}
				}
			}
		}
	}
	return $local:Results;
}
################################################################################
# ProcessBatches
################################################################################
function ProcessBatches([System.Data.SqlClient.SqlConnection] $local:dbConnection,[System.Xml.XmlElement] $local:Batches,[int] $local:BankID,[int] $local:CustomerID,[int] $local:LockboxID)
{
	foreach( $local:Batch in $local:Batches.Batch )
	{
		#Add bank/customer/lockox info to element
		$local:Batch.SetAttribute("BankID", $local:BankID.ToString());
		$local:Batch.SetAttribute("CustomerID", $local:CustomerID.ToString());
		$local:Batch.SetAttribute("LockboxID", $local:LockboxID.ToString());
		ProcessBatchElement $local:dbConnection $local:Batch;
	}
}	

################################################################################
# Main
################################################################################
[System.Data.SqlClient.SqlConnection] $dbConnection = $null;

$ScriptPath = [System.IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Definition);
$LogFile = Join-Path -Path $ScriptPath -ChildPath ([string]::Format("{0}_{1}.log",[System.IO.Path]::GetFilenameWithoutExtension($MyInvocation.MyCommand.Definition),(Get-Date –f yyyyMMddHHmmss)));

Write-Log ([string]::Format("{0} - Version {1}",$ScriptName,$ScriptVerison) );

if( [System.IO.Path]::GetDirectoryName($ImportFile).Length -eq 0 )
{
	$ImportFile = Join-Path $ScriptPath -ChildPath $ImportFile;
}

if( (Test-Path $ImportFile) -ne $true )
{
	Write-Host $ImportFile "not found, aborting.....";
	return;
}
else
{
	Write-Host ([string]::Format("Using import file {0}",$ImportFile));;
	Write-Log ([string]::Format("Using import file {0}",$ImportFile));
}
$xml = New-Object "System.Xml.XmlDocument";
$xml.load($ImportFile);

$StopWatch = [System.Diagnostics.Stopwatch]::StartNew();
Create-DatabaseConnection ([ref]$dbConnection) $DBServer $DBName $UserID $Password;
#Open connection for setup processing
if( (Open-DatabaseConnection $local:dbConnection) )
{
	Write-Log ([string]::Format("Successfully connected to {0}.{1}",$DBServer,$DBName));
	Write-Host ([string]::Format("Successfully connected to {0}.{1}",$DBServer,$DBName));
	Write-Log "Processing Setup";
	Write-Host "Processing Setup..." -NoNewline;
	if( ProcessSetup $dbConnection $xml.ExceptionBatches.Setup )
	{
		Write-Log "Successfully processed setup";	
		Write-Host "Successfully processed setup";	
		ProcessBatches $dbConnection $xml.ExceptionBatches.Batches $xml.ExceptionBatches.Setup.Bank.BankID $xml.ExceptionBatches.Setup.Customer.CustomerID $xml.ExceptionBatches.Setup.Lockbox.LockboxID;
		##process batch here
	}
	$dbConnection.Close();
	$StopWatch.Stop();
	$TimeSpan = [TimeSpan]::FromMilliseconds($StopWatch.ElapsedMilliseconds);
	Write-Host ([string]::Format("Completed in {0:00}:{1:00}:{2:00}.{3:000}",$TimeSpan.Hours,$TimeSpan.Minutes,$TimeSpan.Seconds,$TimeSpan.Milliseconds));
}
else
{
	Write-Host ([string]::Format("Count not open connection to {0}.{1}",$DBServer,$DBName));
}
