package r360.data;

import java.util.HashMap;
import utils.DataBuilder;
import utils.TableData;

public class Config_DataRetrieval {

	public static HashMap<String, String> config_Data = null;
	
	public static String importData = null;
	
	public static TableData tableData = new TableData();

	// public static void set_TestData() throws Exception {
	// config_Data =
	// DataBuilder.GetConfig_DataFromFile("C:\\Selenium\\Eber\\Config.txt");
	// clientName = config_Data.get("clientName");
	// Browser.defaultBrowser = config_Data.get("Browser");
	// userName = config_Data.get("UserName");
	// password = config_Data.get("password");
	//
	//
	//
	// }

	// pulls values from xml. If values are null, it relies on Config file as
	// backup.
	public static void get_ConfigData() throws Exception {
		//config_Data = DataBuilder.GetConfig_DataFromFile("C:\\Selenium\\Eber\\Config.txt");
		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\r360\\data\\Config.txt");
		
		Thread.sleep(100);
	}
	
	public static void get_importData(String file) throws Exception {
		importData = DataBuilder.Get_FileData_AsString(file);
	}
	
	public static void retrieve_DataSheet(String nameOfDataSheet) throws Exception
	{
		String dataFilePath = "\\src\\r360\\data\\" + nameOfDataSheet;
		tableData = DataBuilder.returnTableData_ForComparison(dataFilePath, ",", true);
	}
	
	//Overload doesn't use standard Data file Path.  Path has to be passed in.
	public static void retrieve_DataSheet(String path, String nameOfDataSheet) throws Exception
	{
		tableData = DataBuilder.returnTableData_ForComparison(path + nameOfDataSheet, ",", false);
	}

	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}

		return value;
	}

//	 public static void pdfExtractor() {
//	        PDFTextStripper pdfStripper = null;
//	        PDDocument pdDoc = null;
//	        COSDocument cosDoc = null;
//	        File file = new File("C:/my.pdf");
//	        try {
//	            PDFParser parser = new PDFParser((RandomAccessRead) file);
//	            parser.parse();
//	            cosDoc = parser.getDocument();
//	            pdfStripper = new PDFTextStripper();
//	            pdDoc = new PDDocument(cosDoc);
//	            pdfStripper.setStartPage(1);
//	            pdfStripper.setEndPage(5);
//	            String parsedText = pdfStripper.getText(pdDoc);
//	            System.out.println(parsedText);
//	        } catch (IOException e) {
//	            // TODO Auto-generated catch block
//	            e.printStackTrace();
//	        } 
//	    }

}
