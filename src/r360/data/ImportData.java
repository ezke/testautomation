package r360.data;

import r360.EnvironmentDefinition.Environment;
import r360.pages.GlobalVariables;
import utils.*;
import wfsCommon.pages.DatePicker;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImportData {
	private static TableData availableEnvironments;
	private static HashMap<String, String> config_Data;
	private static HashMap<String, String> selectedEnvironment;
	
	public static void retrieve_EnvironmentData() throws Exception {
		HashMap<String, String> environmentDetail;
		availableEnvironments = DataBuilder
				.returnTableData_ForComparison("\\src\\r360\\data\\environmentData.csv", "\\|", true);

		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\r360\\data\\Config.txt");

		//loads the user config data
		String environmentName = getValue("environment");
		//GlobalVariables.userName = getValue("userName");
		//GlobalVariables.password = getValue("password");
		Browser.currentBrowser  = getValue("browser");
		
		//loads our environment data.
		selectedEnvironment = availableEnvironments.return_Row_BasedOn_1MatchingField("environment",
				environmentName);
		environmentDetail = DataBuilder.GetConfig_DataFromFile("\\src\\r360\\data\\" + selectedEnvironment.get("detail_file"));
		GlobalVariables.environment = new Environment(environmentName, environmentDetail);
		GlobalVariables.dbUrl = GlobalVariables.environment.DBConnection;
	}

	public static void import_PreDepositExceptions_Data() throws IOException, SQLException, InterruptedException {
		ConnectionStringParser connectionParser = new ConnectionStringParser(GlobalVariables.environment.IntegraPayDBConnection);
		removeIntegraPayLockboxData(1159511, 7890001);
		String script=".\\CreateOLDecisioningBatch.ps1";
		CommandLine.execute(standardMessage->System.out.println(standardMessage),
				errorMessage->System.out.println("Error: " + errorMessage),
				script,
				"-DBServer", connectionParser.getServer(),
				"-DBName", connectionParser.getDatabase(),
				"-ImportFile", "AutoIntegraPayBatchTemplate.xml",
				"-UserID", connectionParser.getUser(),
				"-Password", connectionParser.getPassword());
	}

	public static void removeIntegraPayLockboxData(int bankID, int lockboxID) throws SQLException {
		String[] lockboxChildTables = {
				"Batch",
				"BatchTypes",
				"Schedule"
		};
		String[] batchChildTables = {
				"Checks",
				"DEItemRowData",
				"Stubs",
				"Transactions",
				"StubsDataEntry",
				"ChecksDataEntry"
		};
		IntegraPayDatabaseAccess.deleteDEItemFieldData(bankID, lockboxID);
		IntegraPayDatabaseAccess.deleteBatchChildren(batchChildTables, bankID, lockboxID);
		IntegraPayDatabaseAccess.deleteLockboxDecisioningReasons(bankID, lockboxID);
		IntegraPayDatabaseAccess.deleteDESetup(bankID, lockboxID);
		IntegraPayDatabaseAccess.deleteLockboxChildren(lockboxChildTables, bankID, lockboxID);
	}

	// Gets the specified value from first the System Property. If that is null it
	// reverts to config file.
	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}
		if (value != null) {
			System.setProperty(valueToGet, value);
		}
		return value;
	}
	
	// Pass in the location of the file. It will return the file as a String
	// array after
	// updating "Date:" lines with today's date.
	public static String[] update_datFile_withTodaysDate(String datFile) throws Exception {
		String todaysDate = return_TodaysDate_FormattedForWebsite();

		Config_DataRetrieval.get_importData(datFile);
		String importData = Config_DataRetrieval.importData;

		String lines[] = importData.split("\\r?\\n");

		for (int i = 0; i < lines.length; i++) {
			if (lines[i].contains("Date:")) {
				lines[i] = lines[i].substring(0, lines[i].indexOf(":") + 1) + todaysDate;
			}
		}

		return lines;

	}

	// Need to use different logic for WIRE file.
	public static String[] update_WIRE_datFile_withTodaysDate(String datFile) throws Exception {
		String transactionNumbersLocation = "\\\\qaautotest_7b.qalabs.nwk\\C$\\daf\\ach_code\\codeForWire_TransactionNumber.txt";
		String todaysDate = return_TodaysDate_FormattedForWIRE_Import();

		Config_DataRetrieval.get_importData(datFile);
		String importData = Config_DataRetrieval.importData;

		String lines[] = importData.split("\\r?\\n");

		// Update first line with today's date.
		String[] line1 = lines[0].split(",");
		line1[3] = todaysDate;


		
		//Gets our new Transaction number.  Increments by 2 and then subtracts by 1 to et the first one.
		Integer transactionNumber = return_NewFileDuplicateCode(transactionNumbersLocation, 2);
		
		
		String identifier =  "0" + transactionNumber.toString().substring(1, 4);
		//String identifier = identifierInt.toString();

		line1[5] = identifier;


		// Recompile Line 0;
		lines[0] = recompile_ArrayIntoString(line1);
		
		
		//Update lines at indexes 3 and 4.
		String[] lineIndex3 = lines[3].split(",");
		String[] lineIndex4 = lines[4].split(",");
		
	
		//String identifier = identifierInt.toString();
		lineIndex3[5] = "00" + ((Integer)(transactionNumber - 1)).toString();
		lineIndex4[5] = "00" + transactionNumber.toString();
		lines[3] = recompile_ArrayIntoString(lineIndex3);
		lines[4] = recompile_ArrayIntoString(lineIndex4);

		// Update second line with today's date.
		String[] line2 = lines[1].split(",");
		line2[4] = todaysDate;
		lines[1] = recompile_ArrayIntoString(line2);

		return lines;

	}
	
	private static String recompile_ArrayIntoString(String[] line)
	{
		String newLine0 = "";
		for (String segment : line) {
			if (!newLine0.equals("")) {
				newLine0 = newLine0 + "," + segment;
			} else {
				newLine0 = segment;
			}
		}
		
		return newLine0;
		
	}
	

	// Need to use different logic for ACH file.
	public static String[] update_ACH_CIE_datFile_withTodaysDate(String datFile) throws Exception {

		Config_DataRetrieval.get_importData(datFile);
		String importData = Config_DataRetrieval.importData;

		String lines[] = importData.split("\\r?\\n");

		String todaysDateFormated = return_TodaysDate_FormattedForACH_CIE_Import();

		String line1Date = lines[1].substring(69, 78);
		lines[1] = lines[1].replace(line1Date, todaysDateFormated);

		lines[4] = lines[4].replace(line1Date, todaysDateFormated);

		String importFileLocation = "\\\\qaautotest_7b.qalabs.nwk\\C$\\daf\\ach_code\\codeForACH_CIE.txt";

		Integer identifierInt = return_NewFileDuplicateCode(importFileLocation, 1);
		String identifier = identifierInt.toString();

		lines[10] = lines[10].substring(0, 9) + identifier + lines[10].substring(13, lines[10].length());
		lines[2] = lines[2].substring(0, 85) + identifier + lines[2].substring(89, lines[2].length());
		lines[5] = lines[5].substring(0, 85) + identifier + lines[5].substring(89, lines[5].length());
		lines[7] = lines[7].substring(0, 85) + identifier + lines[7].substring(89, lines[7].length());

		return lines;

	}
	
	//Update ACH CTX file.
	public static String[] update_ACH_CTX_datFile_withTodaysDate(String datFile) throws Exception {
		Config_DataRetrieval.get_importData(datFile);
		String importData = Config_DataRetrieval.importData;

		String lines[] = importData.split("\\r?\\n");
		String todaysDateFormated = return_TodaysDate_FormattedForACH_CIE_Import();
		
		String line1Date = lines[1].substring(69, 78);
		
		Integer lastIndex = lines.length - 1;
		
		String oldUniqueCode =  lines[lastIndex].substring(9, 13);
		String importFileLocation = "\\\\qaautotest_7b.qalabs.nwk\\C$\\daf\\ach_code\\codeForACH_CTX.txt";

		Integer identifierInt = return_NewFileDuplicateCode(importFileLocation, 1);
		String newCode = identifierInt.toString();
		
		
		//lines[1] = lines[1].replace(line1Date, todaysDateFormated);
		for(Integer i = 0; i < lines.length; i++)
		{
			lines[i] = lines[i].replace(line1Date, todaysDateFormated);
			lines[i] = lines[i].replace(oldUniqueCode, newCode);
		}
		
		
		return lines;
	}
	

	public static Integer return_NewFileDuplicateCode(String importFileLocation, Integer incrementAmount) throws Exception {
		// Update unique identifier for dat file so we don't end up with
		// duplicate dat file errors.
		//
		Config_DataRetrieval.get_importData(importFileLocation);
		String identifier = Config_DataRetrieval.importData;
		identifier = identifier.substring(0, 4);

		Integer identifierInt = Integer.parseInt(identifier);
		if (identifierInt < 9997) {
			identifierInt = identifierInt + incrementAmount;
		} else {
			identifierInt = 1000;
		}
		
		identifier = identifierInt.toString();

		// Update our file with the new identifier so we can properly run
		// the test next time.
		Path file = Paths.get(importFileLocation);
		Files.write(file, new ArrayList<String>(Arrays.asList(identifier)), Charset.forName("UTF-8"));
		
		
		return identifierInt;

	}

	public static String return_TodaysDate_FormattedForWebsite() {
		DatePicker datePicker = new DatePicker(null, null, null);
		String todaysDate = datePicker.getTodaysDate_AsString();

		return todaysDate;
	}

	public static String return_YesterdaysDate_FormattedForWebsite() {
		DatePicker datePicker = new DatePicker(null, null, null);
		String yesterdaysDate = datePicker.getYesterdaysDate_AsString();

		return yesterdaysDate;
	}

	public static String return_PreviousDate90Day_FormattedForWebsite()
	{
		DatePicker datePicker = new DatePicker(null,null,null);
		String previousDate = datePicker.getPrevious90DayDate_AsString();
		return previousDate;
	}

	public static String return_TodaysDate_FormattedForWIRE_Import() {
		DatePicker datePicker = new DatePicker(null, null, null);
		String todaysDate = datePicker.getTodaysDate_AsString_WithFormat("yyMMdd");

		return todaysDate;
	}

	public static String return_TodaysDate_FormattedForACH_CIE_Import() {

		String todaysDate = return_TodaysDate_FormattedForWIRE_Import();

		// Add Julian Date to end of Today's Date.
		DateFormat d = new SimpleDateFormat("D");
		Date date = new Date();
		String todaysDate_DayOfYear = d.format(date);
		if(todaysDate_DayOfYear.length() == 1)
		{
			todaysDate_DayOfYear = "00" + todaysDate_DayOfYear;
		}
		else if(todaysDate_DayOfYear.length() == 2)
		{
			todaysDate_DayOfYear = "0" + todaysDate_DayOfYear;
		}

		todaysDate = todaysDate + todaysDate_DayOfYear;

		return todaysDate;
	}

	public static String return_TodaysDate_FormattedForDatabase() {
		DatePicker datePicker = new DatePicker(null, null, null);
		String todaysDate = datePicker.getTodaysDate_AsString_WithFormat("yyyyMMdd");

		return todaysDate;
	}

	public static void import_Workgroup_1123211_Data_WithTodaysDate() throws Exception {
		// import_ImageRPS_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_1123211.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationImageRPS\\00_Input\\Automation_ImageRPS_Import.dat",
		// "AutomationImageRPS", batchPaymentType.Check);
		import_Data("AdvancedSearch_ImportFile_workgroup_1123211.dat", batchPaymentType.Check, GlobalVariables.environment);
	}

	public static void import_Workgroup_33338888_Data_WithTodaysDate() throws Exception {
		
		import_Data("AdvancedSearch_ImportFile_workgroup_33338888_ACH_CTX.dat", batchPaymentType.ACH_CTX, GlobalVariables.environment);
		
	}
	
	
	public static void import_Workgroup_2234322_Data_WithTodaysDate() throws Exception {
		Object[][] documents_ToImport = {
				{ "AdvancedSearch_ImportFile_workgroup_2234322_Cash.dat", batchPaymentType.Cash, "" },
				{ "AdvancedSearch_ImportFile_workgroup_2234322_Check.dat", batchPaymentType.Check, "" },
				{ "AdvancedSearch_ImportFile_workgroup_2234322_CreditCard.dat", batchPaymentType.Card, "" },
				{ "AdvancedSearch_ImportFile_workgroup_2234322_SWIFT.dat", batchPaymentType.SWIFT, "" },
				{ "AdvancedSearch_ImportFile_workgroup_2234322_WIRE.dat", batchPaymentType.Wire, "" },
				{ "AdvancedSearch_ImportFile_workgroup_2234322_ACH_CIE.dat", batchPaymentType.ACH, "" } };

		for (int i = 0; i < documents_ToImport.length; i++) {
			documents_ToImport[i][2] = import_Data((String) documents_ToImport[i][0],
					(batchPaymentType) documents_ToImport[i][1], GlobalVariables.environment);
		}
		
		//Uncomment this line to test to see if Import is going through successfully.
//		 for(int i = 0; i < documents_ToImport.length; i++)
//		 {
//			 wait_ForDataImport_ToFinishProcessing((String)documents_ToImport[i][2],
//					 (batchPaymentType)documents_ToImport[i][1]);
//		 }

		// import_ImageRPS_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_2234322_Cash.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationImageRPS\\00_Input\\Automation_ImageRPS_2234322_Cash.dat",
		// "AutomationImageRPS", batchPaymentType.Cash);
		//
		// import_ImageRPS_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_2234322_Check.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationImageRPS\\00_Input\\Automation_ImageRPS_2234322_Check.dat",
		// "AutomationImageRPS", batchPaymentType.Check);
		//
		// import_ImageRPS_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_2234322_CreditCard.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationImageRPS\\00_Input\\Automation_ImageRPS_2234322_CreditCard.dat",
		// "AutomationImageRPS", batchPaymentType.Credit_Card);
		//
		// import_ImageRPS_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_2234322_SWIFT.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationImageRPS\\00_Input\\Automation_ImageRPS_2234322_SWIFT.dat",
		// "AutomationImageRPS", batchPaymentType.SWIFT);
		//
		// //Import Wire Data.
		// import_WIRE_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_2234322_WIRE.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationWIRE\\00_Input\\AdvancedSearch_ImportFile_workgroup_2234322_WIRE.dat",
		// "AutomationWIRE", batchPaymentType.Wire);
		//
		// // Import ACH-CIE Data.
		// import_ACH_CIE_Data("\\src\\r360\\data\\AdvancedSearch_ImportFile_workgroup_2234322_ACH_CIE.dat",
		// "\\\\rechubqaclnt02.qalabs.nwk\\e$\\WFSApps\\RecHub\\Data\\DataImport\\AutomationACH\\00_Input\\AdvancedSearch_ImportFile_workgroup_2234322_ACH_CIE.dat",
		// "AutomationACH", batchPaymentType.ACH);

	}

    public static void import_Workgroup_7890001_Data_WithTodaysDate() throws Exception {
        Object[][] documents_ToImport = {
                { "PostDepositExceptions_ImportFile_workgroup_7890001.dat", batchPaymentType.Cash, "" }
        };
        for (Object[] document : documents_ToImport) {
            document[2] = import_Data(
                    (String)document[0],
                    (batchPaymentType)document[1],
                    GlobalVariables.environment);
        }
    }

	public static void import_Workgroup_7890003_Data_WithTodaysDate() throws Exception {
		Object[][] documents_ToImport = {
				{ "Dashboard_ImportFile_workgroup_7890003.dat", batchPaymentType.Cash, "" }
		};
		for (Object[] document : documents_ToImport) {
			document[2] = import_Data(
					(String)document[0],
					(batchPaymentType)document[1],
					GlobalVariables.environment);
		}
	}

	public static String import_Data(String fileName, batchPaymentType paymentType) throws Exception {
		return import_Data(fileName, paymentType, Environment.createDefaultEnvironment());
	}

	public static String import_Data(String fileName, batchPaymentType paymentType, Environment environment) throws Exception {
		String fileProcessCode = "";
		String datFileLocation = "\\src\\r360\\data\\" + fileName;
		String import_FolderLocation = "";
		String[] importFileData = new String[] {};

		switch (paymentType) {
		case Cash:
			fileProcessCode = "AutomationImageRPS";
			importFileData = update_datFile_withTodaysDate(datFileLocation);
			break;
		case Check:
			fileProcessCode = "AutomationImageRPS";
			importFileData = update_datFile_withTodaysDate(datFileLocation);
			break;
		case Card:
			fileProcessCode = "AutomationImageRPS";
			importFileData = update_datFile_withTodaysDate(datFileLocation);
			break;
		case DIT:
			break;
		case SWIFT:
			fileProcessCode = "AutomationImageRPS";
			importFileData = update_datFile_withTodaysDate(datFileLocation);
			break;
		case Wire:
			fileProcessCode = "AutomationWIRE";
			importFileData = update_WIRE_datFile_withTodaysDate(datFileLocation);
			break;
		case ACH:
			fileProcessCode = "AutomationACH";
			importFileData = update_ACH_CIE_datFile_withTodaysDate(datFileLocation);
			break;
		case ACH_CTX:
			fileProcessCode = "AutomationACH";
			importFileData = update_ACH_CTX_datFile_withTodaysDate(datFileLocation);
			break;
		default:
			BaseUI.verify_true_AndLog(false, "", "Could not find File Process Code.");
			break;
		}

		import_FolderLocation = environment.ClientDriveUNC + "\\WFSApps\\RecHub\\Data\\DataImport\\"
				+ fileProcessCode + "\\00_Input\\" + fileName;
		
		if(paymentType.equals(batchPaymentType.DIT)){
			import_FolderLocation = environment.ClientDriveUNC + "\\WFSApps\\RecHub\\Data\\DataImport\\AutomationWIRE2\\00_Input\\" + fileName;
		}
		
		String dailyImportsDirectory = environment.ClientDriveUNC + "\\DailyAutomationImports\\" + fileName;

		BaseUI.log_Status("Writing file to " + import_FolderLocation);

		Path file = Paths.get(import_FolderLocation);
		Files.write(file, Arrays.asList(importFileData), Charset.forName("UTF-8"));
		
		//Justin requested that we write this file in a place where he could then use the file.
		Path backupFile = Paths.get(dailyImportsDirectory);
		Files.write(backupFile, Arrays.asList(importFileData), Charset.forName("UTF-8"));

		return fileProcessCode;
	}

	public static void import_ACH_CIE_Data(String datFileLocation, String newFileImportLocation, String fileProcessCode,
			batchPaymentType paymentType) throws Exception {
		String[] importFileData = update_ACH_CIE_datFile_withTodaysDate(datFileLocation);

		List<String> lines = Arrays.asList(importFileData);

		// Path originalFileToUpdate = Paths.get(System.getProperty("user.dir")
		// + datFileLocation);
		// Files.write(originalFileToUpdate, lines, Charset.forName("UTF-8"));

		Path file = Paths.get(newFileImportLocation);
		Files.write(file, lines, Charset.forName("UTF-8"));

		wait_ForDataImport_ToFinishProcessing(fileProcessCode, paymentType);

		Thread.sleep(100);
	}

	public static void import_WIRE_Data(String datFileLocation, String newFileImportLocation, String fileProcessCode,
			batchPaymentType paymentType) throws Exception {
		String[] importFileData = update_WIRE_datFile_withTodaysDate(datFileLocation);

		List<String> lines = Arrays.asList(importFileData);
		Path file = Paths.get(newFileImportLocation);
		Files.write(file, lines, Charset.forName("UTF-8"));

		wait_ForDataImport_ToFinishProcessing(fileProcessCode, paymentType);

		Thread.sleep(100);
	}



	// We'll update our data with Today's Date, then write it to the passed in
	// file
	// and then run our wait method to query the database and find out when the
	// import is done.
	public static void import_ImageRPS_Data(String datFileLocation, String newFileImportLocation,
			String fileProcessCode, batchPaymentType paymentType) throws Exception {
		String[] importFileData = update_datFile_withTodaysDate(datFileLocation);

		List<String> lines = Arrays.asList(importFileData);
		Path file = Paths.get(newFileImportLocation);
		Files.write(file, lines, Charset.forName("UTF-8"));

		wait_ForDataImport_ToFinishProcessing(fileProcessCode, paymentType);

		Thread.sleep(100);

	}

	public static void wait_ForDataImport_ToFinishProcessing(String clientProcessCode, batchPaymentType paymentType)
			throws Exception, SQLException {

		String query = "declare @clientprocess_code varchar(99)" + "\n" + "set @clientprocess_code      = '"
				+ clientProcessCode + "'" + "\n" + "SELECT top 1" + "\n" + "[DataImportQueueID]     --Unique Import ID"
				+ "\n" + ",[QueueStatus]          --Status of 150 means complete" + "\n"
				+ ",[ResponseStatus]       --Response of 0 is good, 1 is duplicate file, 2 is duplicate transaction"
				+ "\n"
				+ ",[CreationDate]         --Timestamp of when database entry was created for file import. Example: 2016-11-09 09:50:46.273"
				+ "\n"
				+ ",[RetryCount]           --If there was an error and import is attempted again this is the count"
				+ "\n"
				+ ",[ClientProcessCode]    --This matches up with the data import header name in the DIT config - AutomationImageRPS"
				+ "\n" + ",[XMLDataDocument]      --File Details" + "\n"
				+ ",[XMLResponseDocument]  --This will give any errors that happened or a Sucess message" + "\n"
				+ "FROM [WFSDB_R360].[RecHubSystem].[DataImportQueue]" + "\n"
				+ "where ClientProcessCode = @clientprocess_code" + "\n" + "order by DataImportQueueID DESC";

		Boolean responseSuccessful = false;

		// Using the date time to identify the correct XML Response from our
		// query.
		Date todaysDate = new Date();

		// loop through and run query repeatedly to see if the import is
		// complete.
		for (int i = 0; i < 24; i++) {
			TableData tableData = DatabaseConnection.runSQLServerQuery(query);
			if (tableData.data.size() > 0 && valid_XMLResponseCode(tableData.data.get(0), todaysDate, paymentType)) {
				responseSuccessful = true;
				break;
			} else {
				Thread.sleep(15000);
			}
		}

		BaseUI.verify_true_AndLog(responseSuccessful, "Data Import call was successful.",
				"Data Import call was unsuccessful.");

	}

	// Checks for a valid response status, queue status, payment type, and also
	// makes sure the
	// time matches up within 2 minutes.
	// Once these conditions are met it means that the import is complete and
	// was successful.
	public static Boolean valid_XMLResponseCode(HashMap<String, String> queryResponse, Date queryDate_ApproximateTime,
			batchPaymentType paymentType) throws Exception {

		String queueStatus = queryResponse.get("QueueStatus");
		String responseStatus = queryResponse.get("ResponseStatus");

		// get the query timestamp
		DateFormat format = new SimpleDateFormat("MM-dd-yyyy-HH-mm-ss");
		Date queryDate = format.parse(queryResponse.get("CreationDate"));

		// make sure the timestamp is within 5 minutes of the expected time that
		// was passed in.
		int minutesDifference = 2;
		Boolean timeDifferenceMatch;
		if (queryDate_ApproximateTime.getTime() - queryDate.getTime() <= minutesDifference * 60 * 1000) {
			timeDifferenceMatch = true;
		} else {
			timeDifferenceMatch = false;
		}

		// This is the type of payment that we're matching.
		String paymentIndex = Integer.toString(paymentType.ordinal());
		String batchPayment = MessageFormat.format("BatchPaymentTypeKey=\"{0}\"", paymentIndex);

		// response status of 0 and queue status of 150 means it was successful.
		// Also checking to make sure the creation time matches.
		if (responseStatus != null && queueStatus != null && responseStatus.equals("0") && queueStatus.equals("150")
				&& timeDifferenceMatch && queryResponse.get("XMLResponseDocument").contains(batchPayment)) {
			return true;
		} else {
			return false;
		}
	}

	public static void fit_SimpleFileDrop(String userFileName, Date startDate, String message) throws Exception {
		Config_DataRetrieval.get_importData("\\src\\r360\\data\\FileImportTemplate.xml");
		String fileContent = Config_DataRetrieval.importData;
		List<String> newFileContent = new ArrayList<String>();
		
		Path destinationFile = Paths.get(getFitInputFolder("Auto_SimpleFileDrop_00") + "\\R360_XMLAuto_Drop_0.xml");
		SimpleDateFormat filenameFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		newFileContent.add( 
				fileContent.replace("{DATE}", filenameFormatter.format(startDate))
					.replace("{MESSAGE}", message)
					.replace("{USERFILENAME}", userFileName)
					.replace("{AttachmentFolder}", GlobalVariables.environment.FileImportAttachmentFolder)
				);
		try{
			Files.write(destinationFile,newFileContent, Charset.forName("UTF-8"));
			BaseUI.log_Status("XML has been moved to the input folder Successful");
		}
		
		catch(Exception e){
			BaseUI.log_Status(e.getMessage());
		}
	}
	
	public static void dit_FileDrop(String userFileName, Date startDate, String message) throws Exception {
		Config_DataRetrieval.get_importData("\\src\\r360\\data\\Dit_WireFile_Template.dat");
		String fileContent = Config_DataRetrieval.importData;
		List<String> newFileContent = new ArrayList<String>();
		
		Path destinationFile = Paths.get(getDitInputFolder("SimpleFileDrop_00") + "\\R360_XMLAuto_Drop_0.xml");
		SimpleDateFormat filenameFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		newFileContent.add( 
				fileContent.replace("{DATE}", filenameFormatter.format(startDate))
					.replace("{MESSAGE}", message)
					.replace("{USERFILENAME}", userFileName)
					.replace("{AttachmentFolder}", GlobalVariables.environment.FileImportAttachmentFolder)
				);
		try{
			Files.write(destinationFile,newFileContent, Charset.forName("UTF-8"));
			BaseUI.log_Status("XML has been moved to the input folder Successful");
		}
		
		catch(Exception e){
			BaseUI.log_Status(e.getMessage());
		}
	}


	public static void fit_ImageRPSDrop(String attachmentFullPath, Date startDate) throws Exception {
		Config_DataRetrieval.get_importData("\\src\\r360\\data\\FileImportImageRPSTemplate.dat");
		String fileContent = Config_DataRetrieval.importData;
		List<String> newFileContent = new ArrayList<String>();
		
		Path destinationFile = Paths.get(getFitInputFolder("Auto_ImageRPS_00") + "\\R360_XMLAuto_Drop_0.dat");
		SimpleDateFormat filenameFormatter = new SimpleDateFormat("MM/dd/yy");
		newFileContent.add( 
				fileContent.replace("{DATE}", filenameFormatter.format(startDate))
					.replace("{ATTACHMENT}", attachmentFullPath)
		);
		try{
			Files.write(destinationFile,newFileContent, Charset.forName("UTF-8"));
			BaseUI.log_Status("DAT file has been moved to the input folder Successfully");

		}
		catch(Exception e){
			BaseUI.log_Status(e.getMessage());
		}
	}

	private static String getFileContent(String path) throws Exception {
		Config_DataRetrieval.get_importData(path);
		return Config_DataRetrieval.importData;
	}

	private static String buildClientSetupFilename(
			String workgroupID,
			String workgroupLongName,
			int SiteId,
			Date processDate) {
		SimpleDateFormat filenameFormatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return "Client" + workgroupID + "_" +
				workgroupLongName + "_" +
				SiteId + "_" +
				filenameFormatter.format(processDate) +
				".xml";
	}

	public static void dit_ImageRPSClientSetupDrop(String workgroupID, String workgroupLongName, String sourceFileName) throws Exception {
		String fileContent = getFileContent("\\src\\r360\\data\\" + sourceFileName);
		String destinationFile = buildClientSetupFilename(workgroupID, workgroupLongName, 1, new Date());
		Path destinationFilePath = Paths.get(getDitInputFolder("AutomationClient") + "\\" + destinationFile);
		List<String> newFileContent = new ArrayList<String>();
		newFileContent.add(fileContent);
		try {
			Files.write(destinationFilePath, newFileContent, Charset.forName("UTF-8"));
			BaseUI.log_Status("Client Setup file for Workgroup " + workgroupID + "has been moved to the input folder Successfully");
		}
		catch(Exception ex){
			BaseUI.log_Status(ex.getMessage());
		}
	}

	public static void dit_ImageRPSDrop(String attachmentFullPath, Date startDate) throws Exception {
		String fileContent = getFileContent("\\src\\r360\\data\\DataImportImageRPSTemplate.dat");
		List<String> newFileContent = new ArrayList<String>();
		
		Path destinationFile = Paths.get(getDitInputFolder("AutomationImageRPS") + "\\R360_DATAuto_Drop_0.dat");
		SimpleDateFormat filenameFormatter = new SimpleDateFormat("MM/dd/yy");
		newFileContent.add( 
				fileContent.replace("{DATE}", filenameFormatter.format(startDate))
					.replace("{ATTACHMENT}", attachmentFullPath)
		);
		try{
			Files.write(destinationFile,newFileContent, Charset.forName("UTF-8"));
			BaseUI.log_Status("DAT has been moved to the input folder Successful");

		}
		catch(Exception e){
			BaseUI.log_Status(e.getMessage());
		}
	}
	
	public static void attachment_Drop(String attachmentFile, String attachmentFullPath) throws Exception {
		String source = DataBuilder.Get_FileData_Path("\\src\\r360\\data\\Attachments\\" + attachmentFile);
		
		Files.copy(Paths.get(source), Paths.get(ConfigFileParser.pathToUnc(attachmentFullPath)), StandardCopyOption.REPLACE_EXISTING);
	}
	
	private static String getFitInputFolder(String clientProcessCode) throws Exception {
		HashMap<String, String> paths=null;
		
		try {
			paths = ConfigFileParser.getFitInputPathes(GlobalVariables.environment.ClientDriveUNC + "\\WFSApps\\RecHub\\bin\\FileImportClientSvc.exe.config");
		}
		catch(Exception ex) {
			throw new Exception("Unable to read FIT settings from the config file.");
		}
		if(paths != null && paths.containsKey(clientProcessCode)) {
			return paths.get(clientProcessCode);
		}
		else {
			throw new Exception("Could not find the Client Process Code.");
		}
	}
	

	private static String getDitInputFolder(String clientProcessCode) throws Exception {
		HashMap<String, String> paths=null;
		
		try {
			paths = ConfigFileParser.getDitInputPathes(GlobalVariables.environment.ClientDriveUNC + "\\WFSApps\\RecHub\\bin\\DataImportClientSvc.exe.config");
		}
		catch(Exception ex) {
			throw new Exception("Unable to read FIT settings from the config file.");
		}
		if(paths != null && paths.containsKey(clientProcessCode)) {
			return paths.get(clientProcessCode);
		}
		else {
			throw new Exception("Could not find the Client Process Code.");
		}
	}

	public static void import_Workgroup_7890004_Data_WithTodaysDate() throws Exception {
		import_Data(
			"PDE_ImportFile_WG_7890004_SixDataEntryFields.dat",
			batchPaymentType.Cash,
			GlobalVariables.environment);
	}

	// used to identify BatchPaymentTypeKey from database. Leave these values in
	// this order and the keys will match up.
	public enum batchPaymentType {
		Check, ACH, Wire, Card, SWIFT, Cash, Correspondence_Only, ACH_CTX, DIT
	}

}
