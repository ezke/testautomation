package r360.data;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.time.DateUtils;
import utils.BaseUI;
import utils.DataBuilder;

public class LogFile_Scan {
	public static String[] getRecentLogLines(String logFilePath, Date startDate) throws Exception {
		startDate = DateUtils.addMinutes(startDate, -1);
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		SimpleDateFormat filenameFormatter = new SimpleDateFormat("yyyyMMdd");
		String logFile = logFilePath + "\\" + filenameFormatter.format(startDate) + "_FITClientLog.txt";
		String content = DataBuilder.Get_FileData_AsString(logFile);
		String[] lines = content.split("[\r\n]+");
		Pattern pattern = Pattern.compile("^\\w+\\s+(\\d{2}/\\d{2}/\\d{4}\\s\\d{2}:\\d{2}:\\d{2}\\s\\w{2})\\s+.*$");
		ArrayList<String> results = new ArrayList<String>();
		
		try {
			for(String line:lines) {
				Matcher matcher = pattern.matcher(line);
				if(matcher.find()) {
					String lineDateText = matcher.replaceAll("$1");
					Date lineDate = formatter.parse(lineDateText);
					if(lineDate.compareTo(startDate)>=0) {
						results.add(line);
					}
				}
			}
		}
		catch(Exception ex) {
			BaseUI.log_Warning(ex.getMessage());
		}
		return results.toArray(new String[results.size()]);
	}
}
