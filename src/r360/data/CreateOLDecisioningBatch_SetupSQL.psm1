﻿function Get-BankSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @BankID INT,
		@BankName VARCHAR(25),
		@xmlData XML;

SET @xmlData = '{0}';

SELECT
	@BankID = Bank.att.value('@BankID','INT'),
	@BankName = Bank.att.value('@Name','VARCHAR(25)')
FROM
	@xmlData.nodes('Bank') Bank(att);

SET NOCOUNT OFF;
IF NOT EXISTS( SELECT 1 FROM dbo.Bank WHERE BankID = @BankID )
	INSERT INTO dbo.Bank(BankID,BankName) VALUES (@BankID,@BankName);";
}

function Get-BatchTypesSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @BankID INT = {0},
		@CustomerID INT = {1},
		@LockboxID INT = {2},
		@xmlData XML; 

SET @xmlData = '{3}';

DECLARE @BatchTypes TABLE
(
	BankID INT,
	CustomerID INT,
	LockboxID INT,
	Code INT,
	SubCode INT,
	[Description] VARCHAR(255),
	DESetupID INT
);

INSERT INTO @BatchTypes
(
	BankID,
	CustomerID,
	LockboxID,
	Code,
	SubCode,
	[Description],
	DESetupID
)
SELECT
	@BankID,
	@CustomerID,
	@LockboxID,
	BatchType.att.value('@Code','INT'),
	BatchType.att.value('@SubCode','INT'),
	BatchType.att.value('@Description','VARCHAR(255)'),
	BatchType.att.value('@DESetupID','INT')
FROM 
	@xmlData.nodes('/BatchTypes/BatchType') BatchType(att)

SET NOCOUNT OFF;
;WITH BatchTypes_CTE AS
(
	SELECT
		BankID,
		CustomerID,
		LockboxID,
		Code,
		SubCode
	FROM 
		@BatchTypes
	EXCEPT
	SELECT
		BankID,
		CustomerID,
		LockboxID,
		Code,
		SubCode
	FROM 
		dbo.BatchTypes
	WHERE
		BankID = @BankID
		AND CustomerID = @CustomerID
		AND LockboxID = @LockboxID
)
INSERT INTO dbo.BatchTypes
(
	BankID,
	CustomerID,
	LockboxID,
	Code,
	SubCode,
	[Description],
	DESetupID
)
SELECT 
	BT.BankID,
	BT.CustomerID,
	BT.LockboxID,
	BT.Code,
	BT.SubCode,
	BT.[Description],
	BT.DESetupID
FROM 
	@BatchTypes BT
	INNER JOIN BatchTypes_CTE ON BatchTypes_CTE.BankID = BT.BankID
		AND BatchTypes_CTE.CustomerID = BT.CustomerID
		AND BatchTypes_CTE.LockboxID = BT.LockboxID
		AND BatchTypes_CTE.Code = BT.Code
		AND BatchTypes_CTE.SubCode = BT.SubCode;";
}

function Get-DataEntrySetupSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @xmlData XML,
		@Loop INT = 1,
		@SequenceNumber INT,
		@DESetupID INT,
		@CurrentDESetupID INT = -1;

SET @xmlData = '{0}';

DECLARE @DataEntryFields TABLE
(
	RowID INT IDENTITY(1,1),
	DESetupID INT,
	TableName VARCHAR(36),
	FldName VARCHAR(32),
	TypeRequired BIT,
	DocumentType INT,
	ScreenOrder TINYINT,
	FldLength TINYINT,
	FldDataTypeEnum SMALLINT,
	FldPrompt VARCHAR(32),
	MaskIn VARCHAR(128)
);

INSERT INTO @DataEntryFields
(
	DESetupID,
	TableName,
	FldName,
	DocumentType,
	ScreenOrder,
	FldLength,
	FldDataTypeEnum,
	FldPrompt,
	TypeRequired,
	MaskIn
)
SELECT
	DataEntryField.att.value('../@DESetupID','INT') AS DESetupID,
	DataEntryField.att.value('@TableName','VARCHAR(36)') AS TableName,
	DataEntryField.att.value('@Name','VARCHAR(32)') AS FldName,
	CASE UPPER(DataEntryField.att.value('@TableName','VARCHAR(36)'))
		WHEN 'CHECKS' THEN 0
		WHEN 'CHECKSDATAENTRY' THEN 0
		WHEN 'STUBS' THEN 1
		WHEN 'STUBSDATAENTRY' THEN 1
	END AS DocumentType,
	DataEntryField.att.value('@ScreenOrder','TINYINT') AS ScreenOrder,
	DataEntryField.att.value('@Length','TINYINT') AS FldLength,
	CASE UPPER(DataEntryField.att.value('@DataType','VARCHAR(15)'))
		WHEN 'ALPHANUMERIC' THEN 1
		WHEN 'FLOAT' THEN 6
		WHEN 'MONEY' THEN 7
		WHEN 'DATE' THEN 11
		ELSE 1
	END AS FldDataTypeEnum,
	DataEntryField.att.value('@Prompt','VARCHAR(32)') AS FldPrompt,
	COALESCE(DataEntryField.att.value('@IsRequired','BIT'),0) AS TypeRequired,
	DataEntryField.att.value('@InputMask','VARCHAR(128)') AS MaskIn
FROM
	@xmlData.nodes('/DataEntrySetups/DataEntry/DataEntryField') DataEntryField(att);

SET NOCOUNT OFF;

;WITH DESetupIDS AS
(
	SELECT DISTINCT DESetupID FROM @DataEntryFields
	EXCEPT
	SELECT DESetupID FROM dbo.DESetup
)	
INSERT INTO dbo.DESetup(DESetupID)
SELECT DESetupID FROM DESetupIDS;

WHILE( @Loop <= (SELECT MAX(RowID) FROM @DataEntryFields) )
BEGIN
	IF NOT EXISTS(SELECT 1 FROM dbo.DESetupFields INNER JOIN @DataEntryFields DEF ON DEF.DocumentType = dbo.DESetupFields.DocumentType AND DEF.TableName = dbo.DESetupFields.TableName AND DEF.FldName = dbo.DESetupFields.FldName WHERE dbo.DESetupFields.DESetupID =  DEF.DESetupID AND DEF.RowID = @Loop)
	BEGIN
		SELECT @DESetupID=DESetupID FROM @DataEntryFields WHERE RowID = @Loop;

		IF( @DESetupID <> @CurrentDESetupID )
		BEGIN
			SELECT 
				@SequenceNumber = COALESCE(MAX(SequenceNumber),0)+1
			FROM 
				dbo.DESetupFields 
			WHERE 
				DESetupID = @DESetupID;

			SET @CurrentDESetupID = @DESetupID;
		END

		INSERT INTO dbo.DESetupFields
		(
			DESetupID,
			TableName,
			FldName,
			DocumentType,
			ScreenOrder,
			FldLength,
			FldDataTypeEnum,
			FldPrompt,
			SequenceNumber,
			TypeRequired,
			MaskIn
		)
		SELECT
			DESetupID,
			TableName,
			FldName,
			DocumentType,
			ScreenOrder,
			FldLength,
			FldDataTypeEnum,
			FldPrompt,
			@SequenceNumber,
			TypeRequired,
			MaskIn
		FROM 
			@DataEntryFields
		WHERE 
			RowID = @Loop;

		SET @SequenceNumber += 1;		
	END
	SET @Loop += 1;
END"
}

function Get-DecisioningReasonsSQL()
{
return [string]"SET NOCOUNT ON;
DECLARE @BankID INT = {0},
		@CustomerID INT = {1},
		@LockboxID INT = {2},
		@LockboxKey UNIQUEIDENTIFIER, 
		@xmlData XML; 

SET @xmlData = '{3}'; 

DECLARE @DecisioningReasons TABLE
(
	DecisioningReasonCode VARCHAR(25),
	DecisioningReasonDesc VARCHAR(100)
);

INSERT INTO @DecisioningReasons
(
	DecisioningReasonCode,
	DecisioningReasonDesc
) 
SELECT 
	DecisioningReason.att.value('@Code','VARCHAR(25)'),
	DecisioningReason.att.value('@Description','VARCHAR(100)') 
FROM 
	@xmlData.nodes('/DecisioningReasons/DecisioningReason') DecisioningReason(att);

SELECT 
	@LockboxKey = LockboxKey 
FROM 
	dbo.Lockbox 
WHERE 
	BankID = @BankID 
	AND CustomerID = @CustomerID
	AND LockboxID = @LockboxID;

;WITH DecisioningReasonCode_CTE AS
(
	SELECT DecisioningReasonCode FROM @DecisioningReasons 
	EXCEPT 
	SELECT DecisioningReasonCode FROM dbo.DecisioningReasons 
) 
INSERT INTO dbo.DecisioningReasons 
( 
	DecisioningReasonCode,
	DecisioningReasonDesc
) 
SELECT 
	DR.DecisioningReasonCode,
	DR.DecisioningReasonDesc 
FROM 
	@DecisioningReasons DR
	INNER JOIN DecisioningReasonCode_CTE ON DecisioningReasonCode_CTE.DecisioningReasonCode = DR.DecisioningReasonCode;

SET NOCOUNT OFF;

;WITH LockboxDecisioningReasons_CTE AS
(
	SELECT 
		@LockboxKey AS LockboxKey,
		DecisioningReasonID 
	FROM 
		dbo.DecisioningReasons 
		INNER JOIN @DecisioningReasons DR ON DR.DecisioningReasonCode = dbo.DecisioningReasons.DecisioningReasonCode 
	EXCEPT 
	SELECT 
		dbo.LockboxDecisioningReasons.LockboxKey,
		dbo.LockboxDecisioningReasons.DecisioningReasonID 
	FROM 
		dbo.LockboxDecisioningReasons 
		INNER JOIN dbo.DecisioningReasons ON dbo.DecisioningReasons.DecisioningReasonID = dbo.LockboxDecisioningReasons.DecisioningReasonID 
		INNER JOIN @DecisioningReasons DR ON DR.DecisioningReasonCode = dbo.DecisioningReasons.DecisioningReasonCode 
	WHERE 
		LockboxKey = @LockboxKey
) 
INSERT INTO dbo.LockboxDecisioningReasons
(
	LockboxKey,
	DecisioningReasonID
) 
SELECT 
	LockboxKey,
	DecisioningReasonID 
FROM 
	LockboxDecisioningReasons_CTE;";
}

function Get-LockboxSQL()
{
	return [string] "SET NOCOUNT ON;
DECLARE @BankID INT={0},
		@CustomerID INT={1},
		@SiteCodeID INT={2},
		@LockboxID INT,
		@LockboxName VARCHAR(11),
		@DESetupID INT,
		@xmlData XML;

SET @xmlData = '{3}';

SELECT
	@LockboxID = Lockbox.att.value('@LockboxID','INT'),
	@LockboxName = Lockbox.att.value('@Name','VARCHAR(11)'),
	@DESetupID = Lockbox.att.value('@DESetupID','INT')
	
FROM
	@xmlData.nodes('Lockbox') Lockbox(att);

SET NOCOUNT OFF;
BEGIN TRY
IF NOT EXISTS(SELECT 1 FROM dbo.Lockbox WHERE BankID = @BankID AND CustomerID = @CustomerID AND LockboxID = @LockboxID )
BEGIN
	INSERT INTO dbo.Lockbox(BankID,CustomerID,LockboxID,ShortName,DESetupID,SiteCode) 
	VALUES (@BankID,@CustomerID,@LockboxID,@LockboxName,@DESetupID,@SiteCodeID);
END
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH";
}

function Get-SchedulesSQL()
{
return [string] "SET NOCOUNT ON;
DECLARE @BankID INT = {0},
		@CustomerID INT = {1},
		@LockboxID INT = {2},
		@LockboxKey UNIQUEIDENTIFIER,
		@xmlData XML; 

SET @xmlData = '{3}'; 

DECLARE @Schedule TABLE
(
	BankID INT,
	CustomerID INT,
	LockboxID INT,
	[DayOfWeek] SMALLINT,
	ScheduleTime DATETIME,
	ActivityID INT
);

INSERT INTO @Schedule
(
	BankID,
	CustomerID,
	LockboxID,
	[DayOfWeek],
	ScheduleTime,
	ActivityID
)
SELECT  
	@BankID,
	@CustomerID,
	@LockboxID,
	CASE UPPER(Schedule.att.value('@Day','VARCHAR(10)'))
		WHEN 'SUNDAY' THEN 0
		WHEN 'MONDAY' THEN 1
		WHEN 'TUESDAY' THEN 2
		WHEN 'WEDNESDAY' THEN 3
		WHEN 'THURSDAY' THEN 4
		WHEN 'FRIDAY' THEN 5
		WHEN 'SATURDAY' THEN 6
	END AS [DayOfWeek],
	Schedule.att.value('@Time','DATETIME'),
	dbo.ScheduleActivities.ActivityID
FROM @xmlData.nodes('/Schedules/Schedule') Schedule(att)
INNER JOIN dbo.ScheduleActivities ON dbo.ScheduleActivities.[Description] = Schedule.att.value('@Activity','VARCHAR(128)');

SET NOCOUNT OFF;
;WITH Schedule_CTE AS
(
	SELECT BankID,CustomerID,LockboxID,[DayOfWeek],ActivityID
	FROM @Schedule
	EXCEPT
	SELECT BankID,CustomerID,LockboxID,[DayOfWeek],ActivityID
	FROM dbo.Schedule
)
INSERT INTO dbo.Schedule
(
	BankID,
	CustomerID,
	LockboxID,
	[DayOfWeek],
	ScheduleTime,
	ActivityID
)
SELECT
	S.BankID,
	S.CustomerID,
	S.LockboxID,
	S.[DayOfWeek],
	S.ScheduleTime,
	S.ActivityID
FROM 
	@Schedule S
	INNER JOIN Schedule_CTE ON Schedule_CTE.BankID = S.BankID
		AND Schedule_CTE.CustomerID = S.CustomerID
		AND Schedule_CTE.LockboxID = S.LockboxID
		AND Schedule_CTE.[DayOfWeek] = S.[DayOfWeek]
		AND Schedule_CTE.ActivityID = S.ActivityID;";
}

function Get-SiteCodeSQL
{
return [string] "SET NOCOUNT ON;
DECLARE @SiteCodeID INT,
		@ShortName VARCHAR(30),
		@LongName VARCHAR(128),
		@CurrentProcessingDate DATETIME,
		@LocalTimeZoneBias INT,
		@xmlData XML;

SET @xmlData = '{0}';

SELECT
	@SiteCodeID = SiteCode.att.value('@SiteCode','INT'),
	@ShortName = SiteCode.att.value('@ShortName','VARCHAR(30)'),
	@LongName = SiteCode.att.value('@LongName','VARCHAR(128)'),
	@CurrentProcessingDate = CASE 
		WHEN PATINDEX('GETDATE()%',SiteCode.att.value('@CurrentProcessingDate','VARCHAR(25)')) > 0 THEN  CONVERT(DATE, GETDATE())
		ELSE SiteCode.att.value('@CurrentProcessingDate','DATETIME')
	END,
	@LocalTimeZoneBias = SiteCode.att.value('@LocalTimeZoneBias','INT')
FROM
	@xmlData.nodes('SiteCode') SiteCode(att);

SET NOCOUNT OFF;

IF NOT EXISTS( SELECT 1 FROM dbo.SiteCodes WHERE SiteCodeID = @SiteCodeID )
	INSERT INTO dbo.SiteCodes(SiteCodeID,SiteShortName,SiteLongName,CurrentProcessingDate,LocalTimeZoneBias,CreationDate,ModificationDate,CreatedBy,ModifiedBy,CWDBActiveSite)
	SELECT 
		@SiteCodeID,
		@ShortName,
		@LongName,
		@CurrentProcessingDate,
		@LocalTimeZoneBias,
		GETDATE(),
		GETDATE(),
		SUSER_NAME(),
		SUSER_NAME(),
		0;
	ELSE
		UPDATE dbo.SiteCodes SET CurrentProcessingDate = @CurrentProcessingDate WHERE SiteCodeID = @SiteCodeID;"
}
Export-ModuleMember Get-BankSQL;
Export-ModuleMember Get-BatchTypesSQL;
Export-ModuleMember Get-DataEntrySetupSQL;
Export-ModuleMember Get-DecisioningReasonsSQL;
Export-ModuleMember Get-LockboxSQL;
Export-ModuleMember Get-SchedulesSQL;
Export-ModuleMember Get-SiteCodeSQL;

