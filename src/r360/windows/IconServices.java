package r360.windows;

import r360.pages.GlobalVariables;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.net.MalformedURLException;

public class IconServices extends WindowBase {
    private static final String ICON_CLIENT_PATH = "D:\\Test\\ICONServiceTestClient\\TestICONWebService.exe";

    public IconServices() throws MalformedURLException {
        _application = Application.launchAndAttach(
                GlobalVariables.environment.ClientURL + ":9995/Seed?wsdl",
                ICON_CLIENT_PATH);
    }

    public void setRequestXML(String xml) {
        _application.putElementText(xml, ElementFilter.byAutomationID("txtXml"));
    }

    public void makeRequest() {
        _application.clickElement(ElementFilter.byText("Go!"), ElementFilter.byType("Button"));
    }

    public String readResults() {
        String[] windows=_application.getWindowList();
        return _application.getElementText(ElementFilter.byType("text"));
    }
}
