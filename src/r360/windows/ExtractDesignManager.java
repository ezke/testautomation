package r360.windows;

import r360.pages.GlobalVariables;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.net.MalformedURLException;

public class ExtractDesignManager extends WindowBase {

    private static final String EDM_EXE_PATH = "D:\\WFSApps\\RecHub\\ExtractDesignManager\\bin2\\ExtractDesignManager.exe";

    public ExtractDesignManager() throws MalformedURLException {
        _application = Application.launchAndAttach(
                GlobalVariables.environment.ClientURL + ":9995/Seed?wsdl",
                EDM_EXE_PATH);
    }

    public void logIn() {
        _application.putElementText("WFS", ElementFilter.byAutomationID("txtEntity"));
        _application.putElementText("cjohnson", ElementFilter.byAutomationID("txtLogonName"));
        _application.putElementText("Wausau#1", ElementFilter.byAutomationID("txtPassword"));
        _application.clickElement(ElementFilter.byAutomationID("btnOk"));
    }

    public void openDefinition(String definitionFilename) {
        _application.setCurrentWindow(ElementFilter.byAutomationID("frmMain"));
        _application.typeOnKeyboardWithSpecialKeys("<alt>f</alt>o");
        _application.typeOnKeyboard(definitionFilename);
        _application.clickElement(ElementFilter.byText("Open"), ElementFilter.byType("button"));
    }

    public void runDefinition() {
        _application.setCurrentWindow(ElementFilter.byAutomationID("frmMain"));
        _application.clickElement(ElementFilter.byText("Run"));
        _application.clickElement(ElementFilter.byText("Go"), ElementFilter.byType("button"));
    }
}
