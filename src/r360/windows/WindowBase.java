package r360.windows;

import utils.WhiteSeedWrapper.Application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class WindowBase {
    protected Application _application;

    public void saveScreenShot(String filename) {
        try {
            File screenshotFile = new File(filename);
            FileOutputStream screenshotStream = new FileOutputStream(screenshotFile);
            screenshotStream.write(this.getScreenShot());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] getScreenShot() {
        return _application.getScreenShot();
    }

    public void close() {
        _application.close();
    }
}
