package r360.windows;

import r360.pages.GlobalVariables;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.net.MalformedURLException;


public class ExtractRun extends WindowBase {
    private static final String EDM_EXE_PATH = "D:\\WFSApps\\RecHub\\ExtractDesignManager\\bin2\\ExtractRun.exe";

    private ExtractRun(Application application) {
        _application = application;
    }

    public void logIn() {
        _application.setCurrentWindow(ElementFilter.byAutomationID("frmLogon"));
        _application.putElementText("WFS", ElementFilter.byAutomationID("txtEntity"));
        _application.putElementText("cjohnson", ElementFilter.byAutomationID("txtLogonName"));
        _application.putElementText("Wausau#1", ElementFilter.byAutomationID("txtPassword"));
        _application.clickElement(ElementFilter.byAutomationID("btnOk"));
        _application.setCurrentWindow(ElementFilter.byAutomationID("frmExtractRun"));
    }

    public void openDefinition(String definitionFile) {
        _application.clickElement(ElementFilter.byAutomationID("btnSelectExtractScriptDir"));
        _application.typeOnKeyboard(definitionFile + "\n");
    }

    public void run() {
        _application.clickElement(ElementFilter.byAutomationID("btnGo"));
    }

    public static ExtractRun OpenExtractRun() throws MalformedURLException {
        return new ExtractRun(Application.launchAndAttach(
                GlobalVariables.environment.ClientURL + ":9995/Seed?wsdl",
                EDM_EXE_PATH));
    }
}
