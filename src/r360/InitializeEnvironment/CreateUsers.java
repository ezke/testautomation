package r360.InitializeEnvironment;

import org.testng.ITestResult;
import org.testng.annotations.*;
import r360.data.UserData;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import wfsCommon.pages.EntitySelector;

public class CreateUsers extends BaseTest {
    private final UserData adminUser = new UserData(
            GlobalVariables.entityName,
            GlobalVariables.login,
            GlobalVariables.password,
            "Dominic",
            "Giallombardo",
            "dgiallombardo@wausaufs.com"
    );

    private final UserData autoLimitPayTypeUser = new UserData(
            "WFS/AutomationTest/LimitedPaymentTypes",
            GlobalVariables.automationImports_User,
            GlobalVariables.automationImports_Password,
            "Auto",
            "Limit",
            "charlie.johnson@Deluxe.com"
    );


    @BeforeClass
    public void setup_method() throws Throwable {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

        Navigation.navigate_Admin_Users();
    }

    @Test(dataProvider = "Users", groups= {"all_Test"}, priority = 1)
    public void ensureUsers(UserData user) throws Exception {
        EntitySelector.selectNodePath(user.getEntityPath());
        if(!hasUser(user)) {
            SecurityAdministration.launchAddUserModal();
            SecurityAdmin_UserManageModal.addUser(user);
        }
    }

    private boolean hasUser(UserData user) {
        SecurityAdmin_UserManageModal.findByLoginName(user.getLoginName());
        return BaseUI.elementAppears(Locator.lookupElement("admin_Users_Cell_Contains", user.getLoginName(), null));
    }

    private void ensureUser(UserData user) {

    }

    @DataProvider(name = "Users")
    private Object[] getUsers() {
        return new UserData[] {
                adminUser,
                autoLimitPayTypeUser
        };
    }

    @AfterMethod(alwaysRun = true)
    public void testTeardown(ITestResult result) throws Throwable {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void fixtureTearDown() throws Throwable {

        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}
