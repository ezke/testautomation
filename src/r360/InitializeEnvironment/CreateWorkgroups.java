package r360.InitializeEnvironment;

import org.testng.ITestResult;
import org.testng.annotations.*;

import r360.data.DataImportQueue;
import r360.data.ImportData;
import r360.data.WorkgroupData;
import r360.pages.*;
import r360.tests.BaseTest;
import utils.*;
import wfsCommon.pages.EntitySelector;

import java.util.Date;
import java.util.stream.Stream;

public class CreateWorkgroups extends BaseTest {
    private final WorkgroupData firstWorkgroup = new WorkgroupData(
            "1",
            "AutomationTest/AutomationImports",
            "1159511",
            "1159511 - Automation Imports Bank",
            "7890001",
            "Post Deposit Test Workgroup",
            "PostDepositTestWorkgroupBasic.xml",
            new String[] {"MyFirstField"}
    );

    private final WorkgroupData requiredCurrencyFieldWorkgroup = new WorkgroupData(
            "1",
            "AutomationTest/AutomationImports",
            "1159511",
            "1159511 - Automation Imports Bank",
            "7890002",
            "PDE Test Workgroup Required Currency",
            "PostDepositTestWorkgroupRequiredCurrency.xml",
            new String[] {"Amount"}
    );

    private final WorkgroupData limitedPaymentTypesWorkgroup = new WorkgroupData(
            "1",
            "AutomationTest/LimitedPaymentTypes",
            "1159511",
            "1159511 - Automation Imports Bank",
            "7890003",
            "Checks Only Workgroup",
            "ChecksOnlyWorkgroup.xml",
            new String[] {}
    );

    private final WorkgroupData sixFieldsWorkgroup = new WorkgroupData(
            "1",
            "AutomationTest/AutomationImports",
            "1159511",
            "1159511 - Automation Imports Bank",
            "7890004",
            "Six Field Workgroup",
            "SixFieldsWorkgroup.xml",
            new String[] {"BPRAccountNumber"}
    );

    @BeforeClass
    public void setup_method() throws Throwable {
        Browser.openBrowser(GlobalVariables.environment.BaseURL);
        LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);

        Navigation.navigate_Admin_Workgroups();
        for(WorkgroupData workgroup : getWorkgroups()) {
            RemoveWorkgroup(workgroup);
        }
    }

    private void RemoveWorkgroup(WorkgroupData workgroup) throws Exception {
        EntitySelector.selectNodePath(workgroup.getEntityPath());
        Admin_Workgroups.navigate_to_EntityAssignment_Tab();
        if(BaseUI.elementExists("workgroups_Unassign_UnassignWorkgroupButton", workgroup.getWorkgroupName(), null)) {
            Admin_Workgroups_AssignWorkgroupsModal.unassign_AssignedWorkgroup(workgroup.getWorkgroupName());
        }
        Admin_Workgroups_AddingEditing_WorkgroupModal.delete_Workgroup_From_Database(workgroup.getWorkgroupId(), workgroup.getBankId(), workgroup.getSiteCode());
    }

    public WorkgroupData[] getWorkgroups() {
        return new WorkgroupData[] {
                firstWorkgroup,
                requiredCurrencyFieldWorkgroup,
                limitedPaymentTypesWorkgroup,
                sixFieldsWorkgroup
        };
    }

    @DataProvider(name = "Workgroups")
    public Object[] getWorkgroupsForTests() {
        return getWorkgroups();
    }

    @Test(dataProvider = "Workgroups", groups = { "all_Tests" }, priority = 1)
    public void Remove_And_Recreate_PostDepositExceptions_Workgroup(WorkgroupData workgroup) throws Throwable {
        Date startTime = new Date();
        EntitySelector.selectNodePath(workgroup.getEntityPath());
        ImportData.dit_ImageRPSClientSetupDrop(workgroup.getWorkgroupId(), workgroup.getWorkgroupName(), workgroup.getImportFile());
        BaseUI.assertThat("Post Deposit Exceptions workgroup loaded", DataImportQueue.checkDataImportQueue("AutomationClient", 0, startTime), Is.equalTo(true));
        Thread.sleep(2000);
        Admin_Workgroups.launch_AssignWorkgroupsmodal();
        Admin_Workgroups_AssignWorkgroupsModal.assign_UnassignedWorkgroup(workgroup.getWorkgroupName());
        Admin_Workgroups_AssignWorkgroupsModal.save_changes();
        BaseUI.assertThat(workgroup.getWorkgroupName(), BaseUI.elementExists("workgroups_Unassign_UnassignWorkgroupButton", workgroup.getWorkgroupName(), null), Is.equalTo(true));
        Stream.of(workgroup.getRequiredFields()).forEach(field-> {
            try {
                Set_DataEntry_Field_As_Required(workgroup, field);
            }
            catch (Throwable throwable) {
                throw new RuntimeException("Failed to mark field (" + field + ") as required", throwable);
            }
        });
    }

    public void Set_DataEntry_Field_As_Required(WorkgroupData workgroup, String requiredField) throws Throwable {
        Admin_Workgroups.navigate_to_WorkgroupSettings_Tab();
        Admin_Workgroups.launch_EditWorkGroupSettings_Modal(workgroup.getWorkgroupName());
        Admin_Workgroups_AddingEditing_WorkgroupModal.navigate_to_DataEntryFields_Tab();
        Admin_Workgroups_AddingEditing_WorkgroupModal.launch_EditDataEntryFieldsSettings_ForGivenFieldName(requiredField);
        BaseUI.checkCheckbox(Locator.lookupElement("wrkgrp_AddEdit_DataEntryFields_PostDepositRules_CheckBox"));
        Admin_Workgroups_AddingEditing_WorkgroupModal.save_DataEntryFieldsModal();
        Admin_Workgroups_AddingEditing_WorkgroupModal.save_AddingEditing_WorkgroupModal();
    }

    @AfterMethod(alwaysRun = true)
    public void testTeardown(ITestResult result) throws Throwable {
        ResultWriter.checkForFailureAndScreenshot(result);

        if(BaseUI.elementExists("wrkgrp_AddEdit_ModalWindow", null, null)&& BaseUI.elementAppears(Locator.lookupElement("wrkgrp_AddEdit_ModalWindow"))) {
            Admin_Workgroups_AddingEditing_WorkgroupModal.cancel_AddingEditing_WorkgroupModal();
        }
    }

    @AfterClass(alwaysRun = true)
    public void fixtureTearDown() throws Throwable {

        try {
            Navigation.signOut();
        } finally {
            Browser.closeBrowser();
        }
    }
}