***Object Repository	***ObjectRepo
***Add key (will be used by automation code) in first column	*** Add identifier xpath, id, name, classname, etc to second column
*** Separate columns with a tab, not a space	*** Organize keys based on page object appears on


***Pagination ****Grid ****Table
Pagin_btn_gridNavigate_first	//div[@id='{0}']/parent::*[1]//span[@class='slick-pager-nav']//span[contains(@class,'ui-icon-seek-first')]
Pagin_btn_gridNavigate_prev	//div[@id='{0}']/parent::*[1]//span[@class='slick-pager-nav']//span[contains(@class,'ui-icon-seek-prev')]
Pagin_btn_gridNavigate_next	//div[@id='{0}']/parent::*[1]//span[@class='slick-pager-nav']//span[contains(@class,'ui-icon-seek-next')]
Pagin_btn_gridNavigate_last	//div[@id='{0}']/parent::*[1]//span[@class='slick-pager-nav']//span[contains(@class,'ui-icon-seek-end')]
Pagin_header_byText	//div[@id='{0}']//div[@class='slick-header-columns']/div/span[@class='slick-column-name'][./text()='{1}']
Pagin_header_sort_icon	//div[@id='{0}']//div[./span[@class='slick-column-name'][./text()='{1}']]/span[2]	
Pagin_headers_list	//div[@id='{0}']//div[@class='slick-header-columns']/div
Pagin_list_Values_forColumn	//div[@id='{0}']//div[contains(@class,'ui-widget-content slick-row')]/div[{1}]
Pagin_last_available_Value	(//div[@id='{0}']//div[contains(@class,'ui-widget-content slick-row')]/div[{1}])[last()]
Pagin_first_available_Value	(//div[@id='{0}']//div[contains(@class,'ui-widget-content slick-row')]/div[{1}])[1]
Pagin_Grid_ToScrollThrough	//div[@id='{0}']//div[@class='slick-viewport']
Pagin_lightBulb_icon	//div[@id='{0}']//span[@class='ui-icon ui-icon-lightbulb']
Pagin_show_All_link	//div[@id='{0}']//span[@class='slick-pager-settings-expanded']/a[contains(text(),'All')]
Pagin_show_Auto_link	//div[@id='{0}']//span[@class='slick-pager-settings-expanded']/a[contains(text(),'Auto')]
Pagin_show_10_link	//div[@id='{0}']//span[@class='slick-pager-settings-expanded']/a[./text()='10']
Pagin_show_25_link	//div[@id='{0}']//span[@class='slick-pager-settings-expanded']/a[./text()='25']
Pagin_show_50_link	//div[@id='{0}']//span[@class='slick-pager-settings-expanded']/a[./text()='50']
Pagin_show_100_link	//div[@id='{0}']//span[@class='slick-pager-settings-expanded']/a[./text()='100']
Pagin_txt_By_columnName	(//div[@id='{0}']//div[@class='slick-headerrow-columns']/div)[{1}]//input[@type='text']
Pagin_header_checkbox	//div[@id='{0}']//div[@class='slick-header-columns']//input[@type='checkbox']
Pagin_all_checkboxes	//div[@id='{0}']//div[contains(@class, 'ui-widget-content slick-row')]//input[@type='checkbox']
Pagin_last_checkbox	(//div[@id='{0}']//div[contains(@class, 'ui-widget-content slick-row')]//input[@type='checkbox'])[last()]
Pagin_firstTable_checkbox	(//div[@id='{0}']//div[contains(@class, 'ui-widget-content slick-row')]//input[@type='checkbox'])[1]
Pagin_firstTable_editButton	(//div[@id='{0}']//div[contains(@class, 'ui-widget-content slick-row')]//a[./*[@class='fa fa-edit']])[1]
Pagin_list_AllRows	//div[@id='{0}']//div[contains(@class, 'ui-widget-content slick-row')]
Pagin_slick_Status	//div[@id='{0}']//span[@class='slick-pager-status']



***Entity Selector ***Tree Node elements ***Usually found on Admin pages.
entityTreeNode_ByText	//div[contains(@class, 'jqx-rc-all')][./text()='{0}']
entityTreeNode_ByText_ChildNodes	//div[contains(@class, 'jqx-rc-all')][./text()='{0}']/following-sibling::ul//div[contains(@class,'jqx-rc-all jqx-tree-item')]
entityTree_AllAvailableEntities	//ul[@class='jqx-tree-dropdown-root']//div[contains(@class, 'jqx-rc-all')][not(contains(text(), 'Loading...'))]
entityTreeNode_Arrow_ByText	//div[contains(@class, 'jqx-rc-all')][contains(text(),'{0}')]/preceding-sibling::span[1]
entityTreeNode_SearchBar	//div[contains(@id, 's2id_autogen')]//a[contains(@class,'select2-choice')]
entityTreeNode_FirstNode	(//div[contains(@class, 'jqx-rc-all')])[1]
entityTreeNode_AllNodesWithoutDownArrow	//ul[@class='jqx-tree-dropdown-root']//div[contains(@class,'jqx-rc-all')][not(preceding-sibling::span[@class='jqx-tree-item-arrow-expand jqx-icon-arrow-down'])]
entityTree	//div[@id='entityTree']
entityTree_SearchTextBox	//*[@id='select2-drop']/div/input
entityTree_ResultsResetButton	//*[@id='filterClear']
entityTree_Results	//ul[@class='select2-results']//td/b
entityTree_Results_NOMatches	//li[@class='select2-no-results'][contains(text(), 'No matches found')]
entityTree_Results_ByText	//ul[@class='select2-results']//td/b[contains(text(),'{0}')]
entityTree_SearchArrow	//div[@id='entityTree']//span[@class='select2-arrow']/b
entityTree_CloseSearchDropdownElement	id=select2-drop-mask
***entityTree_ListOfEntity_AndChildNodes	//li[contains(@id, 'entityTree-id')][./div[./text()='{0}']]/div[./text()='{0}'] | //li[contains(@id, 'entityTree-id')][./div[./text()='{0}']]//li/div
entityTree_ListOfEntity_AndChildNodes	//li[contains(@id, 'entityTree-id')][./div[./text()='{0}']]//div

***Dropdown elements
dropdwn_dropdown	//div[@id='{0}']
dropdwn_selectedValue	//div[@id='{0}']//span[@class='select2-chosen']
dropdwn_Dropdown	//div[@id='{0}']//a[1]
dropdwn_Dropdown_ByID //select[@id="{0}"]
dropdwn_ListItem_ByText	//div[@id='select2-drop']//li[./div[contains(text(),'{0}')]]
dropdwn_ListItem_AfterFilter_ByText	//div[@id='select2-drop']//li[./div/span[contains(text(),'{0}')]]
dropdwn_Dropdown_ClearButton	//div[@id='{0}']//abbr[@class='select2-search-choice-close']
dropdwn_ResultsList	//ul[@class='select2-results']/li
dropdwn_CloseDropdown	//*[@id='select2-drop-mask']
dropdwn_SearchBox	//div[@id='select2-drop']//input[contains(@class,'select2-input')]
dropdwn_SearchBox_NoResults	//li[@class='select2-no-results'][./text()='No matches found']

***Filter Expander ***Advanced Search page control
fltr_expander_Open	//div[@id='{0}']
fltr_expander_Mask	id=select2-drop-mask
fltr_expander_OpenSearch	//div[@id='{0}']//a[contains(@class,'select2-choice')]
fltr_expander_searchTxtBox	//*[@id ='select2-drop']//input[contains(@class,'select2-input')]
fltr_expander_searchResult	//li[contains(@class, 'select2-results-dept-0 select2-result select2-result-selectable')]
fltr_expander_Text	//div[@id='{0}']//div[@id='filterTitle']
fltr_expander_ResetButton	//a[@id='filterClear']
fltr_expander_NoResults	//li[@class='select2-no-results'][./text()='No matches found']

****putsbox elements*****
putsbox_Username	//*[@id='user_email']
putsbox_Password	//*[@id='user_password']
putsbox_Signin_Button	//input[@class='btn btn-primary'][contains(@value,'Sign in')]
putsbox_ClearHistory_Button	//a[contains(@class,'btn btn-default')][contains(text(),'Clear History')]
putsbox_Email_ByEmailName	//div[contains(@class,'history')]/ul/li/a[contains(text(),'{0}')]
***putsbox_HTML_LinkList	//tbody/tr/td[4]/a[2]
putsbox_HTML_LinkList	//a[./text()='HTML']
putsbox_Subject_ByText	//tbody/tr/td[2][contains(text(), '{0}')]
putsbox_EmailAddress_Validation	//*[@id='putsbox-token-input']
putsbox_Subject_List	//tbody/tr/td[2]
putsbox_Inspect_Link	//a[./text()='Inspect']

****email r360 ResetPasswordForm elements****
email_ResetPasswordForm_Content	//html/body
email_ResetPasswordForm_Link	//a[contains(text(),'link')]
email_ResetPasswordForm_Modal	//*[@id='resetForm']
email_ResetPasswordForm_Link_ErrorValidation	//*[@id='errorMessage']
email_ResetPasswordForm_EnityText	id=Entity
email_ResetPasswordForm_LoginNameText	//*[@id='UserName']
email_ResetPasswordForm_NewPasswordText	//*[@id='NewPassword']
email_ResetPasswordForm_ConfirmNewPasswordText	//*[@id='ConfirmNewPassword']
email_ResetPasswordForm_Submit_Button	//*[@id='submitReset']
email_ResetPasswordForm_ErrorMessage	//*[@id='Errors']/ul/li
email_ResetPasswordForm_ErrorMessageByText	//*[@id='Errors']//li[contains(text(),'{0}')]


***Date Picker
datePick_MonthDropdown	//select[@class='ui-datepicker-month']
datePick_YearDropdown	//select[@class='ui-datepicker-year']
datePick_day_ByText	//table[@class='ui-datepicker-calendar']//a[./text()='{0}']

***Pie Chart
pieChart_pieSlice_ByIndex	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][last()]/*[local-name()='path'][{1}]
pieChart_ListOfSlices	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][last()]/*[local-name()='path']
testPie2	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][1]/following-sibling::*[local-name() = 'g'][last()]
pieChart_testToolText	//div[@class='jqx-rc-all jqx-button'][contains(@style, 'display: block')]//span[@class='jqx-chart-tooltip-text']//b
pieChart_testToolText2	(//span[@class='jqx-chart-tooltip-text']//b)[1]
pieChart_testToolText3	(//span[@class='jqx-chart-tooltip-text']//b)[3]
pieChart_testToolTip_ByText	//span[@class='jqx-chart-tooltip-text']//b[contains(text(), '{0}')]
pieChart_testToolTips	//span[@class='jqx-chart-tooltip-text']//b
pieChart_chartContainer	//div[@id='{0}']//*[@id='svgChart']/*[local-name() = 'g']/*[local-name() = 'rect']
pieChart_legendList	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][1]/following-sibling::*[local-name() = 'g'][position() < last()]/*[local-name() ='text']
pieChart_legendList_Checked	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][1]/following-sibling::*[local-name() = 'g'][position() < last()]/*[local-name() ='text'][./preceding-sibling::*[local-name()='rect'][@fill-opacity='1']]
pieChart_legendList_ByText	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][1]/following-sibling::*[local-name() = 'g'][position() < last()]/*[local-name() ='text'][./text()='{1}']
pieChart_legendCheckBox_ByText	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g']/*[local-name() = 'g'][1]/following-sibling::*[local-name() = 'g'][position() < last()]/*[local-name() ='text'][./text()='{1}']/preceding-sibling::*[local-name() = 'rect']
pieChart_Title	//div[@id='{0}']//*[local-name() = 'svg']/*[local-name() = 'g'][1]/*[local-name() = 'g'][1]/*[local-name() = 'text']
panel_Header	//div[@class='panel-heading']

***Toast Error
toast_CloseButton	//*[contains(@class,'toast-item-close')]
