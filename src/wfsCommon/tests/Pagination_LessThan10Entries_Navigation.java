package wfsCommon.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import r360.tests.BaseTest;
import wfsCommon.pages.Pagination;
import r360.pages.GlobalVariables;
import r360.pages.LoginPage;
import r360.pages.Navigation;
import r360.pages.SecurityAdmin_UserManageModal;
import r360.pages.SecurityAdministration;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class Pagination_LessThan10Entries_Navigation extends BaseTest {

	Pagination userManagementGrid;
	Pagination testGrid;
	public String gridID;

	public String nodeText = "AutomationTest";
	public String userText = "TestUser1";
	
	@BeforeClass
	public void pre_setup() throws Exception {
		//Override these methods to use Base Class for other testing.
		set_test_Variables();
		setupSteps_toOverwrite();
		page_specificSteps_toOverwrite();
		

		// declare our Available Groups grid that we'll use later.
		testGrid = new Pagination(gridID);
	}
	
	public void set_test_Variables()
	{
		
	}
	
	public void page_specificSteps_toOverwrite() throws Exception
	{
		userManagementGrid = new Pagination("userGrid");
		SecurityAdministration.selectNode(nodeText);

		BaseUI.click(userManagementGrid.editButton_ByColumnName_andColumnText("Login Name", userText));
		Thread.sleep(3000);
		SecurityAdmin_UserManageModal.navigate_to_Group_ManagementTab();
	}
	
	public void setupSteps_toOverwrite() throws Exception
	{
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entityName, GlobalVariables.login, GlobalVariables.password);
		Navigation.navigate_Admin_Users();
	}

	@Test
	public void First_Button_Disabled() throws Throwable {
		testGrid.verifyFirstButtonDisabled();
	}
	
	@Test
	public void Previous_Button_Disabled() throws Throwable {
		testGrid.verifyPreviousButtonDisabled();
	}

	
	@Test
	public void Next_Button_Disabled() throws Throwable {
		testGrid.verifyNextButtonDisabled();
	}

	@Test
	public void Last_Button_Disabled() throws Throwable {
		testGrid.verifyLastButtonDisabled();
	}
	

	
	@AfterMethod(alwaysRun = true)
	public void writeResult(ITestResult result) throws Exception {

		testGrid.click_first();
		
		Thread.sleep(1000);
	}

	@AfterClass(alwaysRun = true)
	public void TearDown() throws Exception {

		try {
			if(Locator.lookupElement("entityUserEdit_Modal").isDisplayed())
			{
				SecurityAdmin_UserManageModal.click_cancelButton();
			}
			Navigation.signOut();
		}
		finally {
			Browser.closeBrowser();
		}
	}
}
