package wfsCommon.tests;

import java.util.ArrayList;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import r360.data.ClassObject_ForFieldAndValue;
import r360.tests.BaseTest;
import utils.BaseUI;
import utils.Locator;
import wfsCommon.pages.Pagination;

public class Pagin_GridDisplay_FilterTests_Code extends BaseTest {

	public Pagination gridToTest;
	String filterName;
	public String gridID;

	public ArrayList<ClassObject_ForFieldAndValue> filter_ByColumn = new ArrayList<ClassObject_ForFieldAndValue>();
	public ArrayList<ClassObject_ForFieldAndValue> filter_ByColumn_NoResults = new ArrayList<ClassObject_ForFieldAndValue>();
	public ArrayList<ClassObject_ForFieldAndValue> filter_ByColumn_ClearFilter = new ArrayList<ClassObject_ForFieldAndValue>();
	public ArrayList<ClassObject_ForFieldAndValue> filter_ByColumn_ClearFilter_AndLaunchModal_ThenClose = new ArrayList<ClassObject_ForFieldAndValue>();
	public ArrayList<ClassObject_ForFieldAndValue> sort_ByColumn_AscendingAndDescending = new ArrayList<ClassObject_ForFieldAndValue>();

	// pass in the Test Case Name, "single" or "multiple" and if multiple also
	// pass in the column name that gets sorted ascending when navigating.
	// This will cause the Multiple page navigation tests to be generated and
	// run.
	// Example: navigation_testGenerator.add(new
	// ClassObject_ForFieldAndValue("PT6321","multiple", "Description"));
	// Example2: navigation_testGenerator.add(new
	// ClassObject_ForFieldAndValue("PT6321","single", ""));
	// public String[] navigation_multiple_pages = new String[1];
	public ArrayList<ClassObject_ForFieldAndValue> navigation_testGenerator = new ArrayList<ClassObject_ForFieldAndValue>();

	public ArrayList<ClassObject_ForFieldAndValue> ShowNumber_testGenerator = new ArrayList<ClassObject_ForFieldAndValue>();

	// Set the Object repository identifier for the cancel button for modal
	// window.
	public String cancelButton_Identifier;

	String nodeText = "AutomationTest";

	@DataProvider(name = "filter_Field")
	public Object[][] createData() {
		return create_Field_Value_Object(filter_ByColumn);
	}

	@DataProvider(name = "filter_Field_NoResults")
	public Object[][] createData_FilterNoResults() {
		return create_Field_Value_Object(filter_ByColumn_NoResults);
	}

	@DataProvider(name = "filter_Field_ClearFilter_AndLaunchModal")
	public Object[][] createData_ClearFilterTests_WithModalLaunch() {
		return create_Field_Value_Object(filter_ByColumn_ClearFilter_AndLaunchModal_ThenClose);
	}

	@DataProvider(name = "filter_Field_ClearFilter")
	public Object[][] createData_ClearFilterTests() {
		return create_Field_Value_Object(filter_ByColumn_ClearFilter);
	}

	@DataProvider(name = "Sort")
	public Object[][] createData_SortTests() {
		return create_Field_Value_Object(sort_ByColumn_AscendingAndDescending);
	}

	public void TC_Sort_By_ColumnName_Ascending(String testCase, String filterName, String filterText)
			throws Throwable {
		gridToTest.sort_Column_Ascending(filterName);

		if (filterText.equals("Alphabetical")) {
			gridToTest.verifyColumnAscending_alphabetical(filterName);
		} else {
			gridToTest.verifyColumnAscending_numeric(filterName);
		}
	}

	public void TC_Sort_By_ColumnName_Descending(String testCase, String filterName, String filterText)
			throws Throwable {
		gridToTest.sort_Column_Descending(filterName);

		if (filterText.equals("Alphabetical")) {
			gridToTest.verifyColumnDescending_alphabetical(filterName);
		} else {
			gridToTest.verifyColumnDescending_numeric(filterName);
		}
	}

	public void TC_Filter_By_ColumnName_WithGivenText(String testCase, String filterName, String filterText)
			throws Throwable {
		gridToTest.filter_textbox_SendKeys(filterName, filterText);
		gridToTest.verifyColumn_TextInList_ALLValues_ContainText(filterName, filterText);
	}

	public void TC_Filter_By_ColumnName_WithGivenText_NoResults(String testCase, String filterName, String filterText)
			throws Throwable {
		gridToTest.filter_textbox_SendKeys(filterName, filterText);
		gridToTest.verifyColumn_NoResultsFound(filterName);
	}

	// Some TC's call for you to set a filter, launch a modal, close the modal
	// and then clear the filter.
	// You'll need to set the cancelButton_Identifier variable for these to
	// work. That will be the cancel button identifier
	// to close the modal window.
	public void TC_FilterAnd_LaunchModal_ThenCloseModal_ClearFilter_AllPreviousEntriesAppear(String testCase,
			String fieldName, String fieldText) throws Throwable {
		// get first list of names
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(fieldName)));

		gridToTest.filter_textbox_SendKeys(fieldName, fieldText);
		BaseUI.click(gridToTest.editButton_ByColumnName_andColumnText(fieldName, fieldText));
		BaseUI.waitForElementToBeClickable(cancelButton_Identifier, null, null);
		BaseUI.click(Locator.lookupElement(cancelButton_Identifier));
		Thread.sleep(2000);
		gridToTest.verifyColumn_TextInList_ALLValues_ContainText(fieldName, fieldText);
		gridToTest.filter_textbox_ClearFilter(fieldName);

		// get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(fieldName)));

		// verify our 2 lists match.
		gridToTest.verifyPagesListsMatch(list_of_names, current_list_of_names);
	}


	public void TC_FilterAnd_ClearFilter_AllPreviousEntriesAppear(String testCase, String fieldName, String fieldText)
			throws Throwable {
		// get first list of names
		ArrayList<String> list_of_names = new ArrayList<String>();
		list_of_names.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(fieldName)));

		gridToTest.filter_textbox_SendKeys(fieldName, fieldText);
		gridToTest.filter_textbox_ClearFilter(fieldName);

		// get another list of names after we clear the filter.
		ArrayList<String> current_list_of_names = new ArrayList<String>();
		current_list_of_names.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(fieldName)));

		// verify our 2 lists match.
		gridToTest.verifyPagesListsMatch(list_of_names, current_list_of_names);
	}


	public Object[][] create_Field_Value_Object(ArrayList<ClassObject_ForFieldAndValue> dataSet) {
		Object[][] testObject = new Object[dataSet.size()][3];

		int count = 0;
		for (ClassObject_ForFieldAndValue entry : dataSet) {
			testObject[count][0] = entry.testCase;
			testObject[count][1] = entry.fieldName;
			testObject[count][2] = entry.valueName;
			count++;
		}
		return testObject;
	}

	/// Tests for Navigation across multiple pagination pages
	///
	///
	///

	@DataProvider(name = "navigation_multiple_pages")
	public Object[][] create_multipageNavigation_Tests() {

		if (navigation_testGenerator.size() > 0 && navigation_testGenerator.get(0).fieldName.equals("multiple")) {
			return create_Field_Value_Object(navigation_testGenerator);
		} else {
			// if the value is not set to multiple we'll return an empty object.
			// That will cause this suite of tests to not be generated.
			Object[][] emptyObject = new Object[0][3];
			return emptyObject;
		}
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_ListsAreDifferent(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		ArrayList<String> previous_pages_texts = new ArrayList<String>();
		previous_pages_texts.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(columnToCheck)));
		gridToTest.click_next();
		gridToTest.verifyColumnAscending_alphabetical(columnToCheck);

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		current_pages_texts.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(columnToCheck)));

		gridToTest.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.verifyFirstButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.verifyPreviousButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.verifyNextButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.verifyLastButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_PreviousPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();

		// Click Next.
		gridToTest.click_next();
		previous_pages_texts.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(columnToCheck)));
		Thread.sleep(1000);
		gridToTest.verifyColumnAscending_alphabetical(columnToCheck);

		// Click Previous
		gridToTest.click_previous();
		current_pages_texts.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(columnToCheck)));
		gridToTest.verifyColumnAscending_alphabetical(columnToCheck);
		gridToTest.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_PreviousPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_previous();
		gridToTest.verifyFirstButtonDisabled();

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_PreviousPage_PreviousButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_previous();
		gridToTest.verifyPreviousButtonDisabled();

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_PreviousPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_previous();
		gridToTest.verifyNextButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_PreviousPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_previous();
		gridToTest.verifyLastButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {

		ArrayList<String> current_pages_texts = new ArrayList<String>();
		ArrayList<String> previous_pages_texts = new ArrayList<String>();

		gridToTest.click_next();
		previous_pages_texts.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(columnToCheck)));
		Thread.sleep(1000);
		gridToTest.verifyColumnAscending_alphabetical(columnToCheck);

		gridToTest.click_first();
		current_pages_texts.addAll(gridToTest.columns_Texts_List(gridToTest.columns_Elements_List(columnToCheck)));
		gridToTest.verifyColumnAscending_alphabetical(columnToCheck);
		gridToTest.verifyPagesListsDontMatch(previous_pages_texts, current_pages_texts);

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_FirstButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_first();
		gridToTest.verifyFirstButtonDisabled();

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_PreviousButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_first();
		gridToTest.verifyPreviousButtonDisabled();

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_NextButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_first();
		gridToTest.verifyNextButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_LastButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_next();
		gridToTest.click_first();
		gridToTest.verifyLastButtonEnabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_LastPage_AscendingOrder(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_last();
		Thread.sleep(1000);
		gridToTest.verifyColumnAscending_alphabetical(columnToCheck);
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_LastPage_FirstButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_last();
		gridToTest.verifyFirstButtonEnabled();

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_LastPage_PreviousButtonEnabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_last();
		gridToTest.verifyPreviousButtonEnabled();

	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_NextButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_last();
		gridToTest.verifyNextButtonDisabled();
	}

	@Test(dataProvider = "navigation_multiple_pages", alwaysRun = true)
	public void Navigation_MultiPage_NextPage_FirstPage_LastButtonDisabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.click_last();
		gridToTest.verifyLastButtonDisabled();
	}

	/// End of Multiple page navigation tests.
	///
	///
	///

	/// Single page navigation tests.
	///
	///
	
	@DataProvider(name = "navigation_single_page")
	public Object[][] create_singlepageNavigation_Tests() {

		if (navigation_testGenerator.size() > 0 && navigation_testGenerator.get(0).fieldName.equals("single")) {
			return create_Field_Value_Object(navigation_testGenerator);
		} else {
			// if the value is not set to multiple we'll return an empty object.
			// That will cause this suite of tests to not be generated.
			Object[][] emptyObject = new Object[0][3];
			return emptyObject;
		}
	}

	public void Navigation_SinglePage_First_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		gridToTest.verifyFirstButtonDisabled();
	}

	public void Navigation_SinglePage_Previous_Button_Disabled(String testCaseName, String testType,
			String columnToCheck) throws Throwable {
		gridToTest.verifyPreviousButtonDisabled();
	}

	public void Navigation_SinglePage_Next_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		gridToTest.verifyNextButtonDisabled();
	}

	public void Navigation_SinglePage_Last_Button_Disabled(String testCaseName, String testType, String columnToCheck)
			throws Throwable {
		gridToTest.verifyLastButtonDisabled();
	}

	///
	///
	/// End of Single page navigation tests.///

	/// Show Auto/All/100/50/25/10 tests
	///
	///

	@DataProvider(name = "show_Number")
	public Object[][] createData_ShowTests() {
		return create_Field_Value_Object(ShowNumber_testGenerator);
	}

	public void ShowTests_ShowAllOption(String testCase, String columnName, String extraValue) throws Throwable {
		gridToTest.verify_showAllOption(columnName);
	}

	public void ShowTests_ShowAutoOption(String testCase, String columnName, String extraValue) throws Throwable {
		gridToTest.verify_showAutoOption(columnName);
		gridToTest.verify_navigation_lists_DONT_Match(columnName);
	}

	public void ShowTests_Show10Option(String testCase, String columnName, String extraValue) throws Throwable {
		gridToTest.verify_show10Option(columnName);
		gridToTest.verify_navigation_lists_DONT_Match(columnName);
	}

	public void ShowTests_Show25Option(String testCase, String columnName, String extraValue) throws Throwable {
		gridToTest.verify_show25Option(columnName);
		gridToTest.verify_navigation_lists_DONT_Match(columnName);
	}

	public void ShowTests_Show50Option(String testCase, String columnName, String extraValue) throws Throwable {
		gridToTest.verify_show50Option(columnName);
		gridToTest.verify_navigation_lists_DONT_Match(columnName);
	}

	public void ShowTests_Show100Option(String testCase, String columnName, String extraValue) throws Throwable {
		gridToTest.verify_show100Option(columnName);
		gridToTest.verify_navigation_lists_DONT_Match(columnName);
	}

	///
	///
	///
	/// End of Show Number tests


}
