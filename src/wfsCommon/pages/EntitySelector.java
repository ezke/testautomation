package wfsCommon.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class EntitySelector {

	public static void selectNode(String nodeToExpand) throws Exception {
		// WebElement node = Locator.lookupElement("entityTreeNode_ByText",
		// nodeToExpand, null);
		WebElement node = null;
		try {
			node = BaseUI.waitForElementToBeClickable("entityTreeNode_ByText", nodeToExpand, null);
		} catch (Exception E) {
		}
		BaseUI.click(node);
		Thread.sleep(5000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void selectNodePath(String nodePath) throws Exception {
		List<String> pathParts = Arrays.asList(nodePath.split("/"));
		Stream<String> parents = pathParts.stream().limit(pathParts.size()-1);
		parents.forEach(parent-> {
			try {
				expandNode(parent);
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to expand node (" + parent + ")", e);
			}
		});
		selectNode(pathParts.get(pathParts.size()-1));
	}

	public static void selectNode_Scroll(String nodeToExpand) throws Exception {
		WebElement node = Locator.lookupElement("entityTreeNode_ByText", nodeToExpand, null);
		((JavascriptExecutor) Browser.driver).executeScript("arguments[0].scrollIntoView();", node);
		BaseUI.click_js(node);
		Navigation.wait_for_LoadingSpinner_ToAppear();
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public static void expandNode(String nodeToExpand) throws Exception {
		// get the Node Arrow element by the passed in text.
		WebElement nodeArrow = Locator.lookupElement("entityTreeNode_Arrow_ByText", nodeToExpand, null);
		String nodeArrowState = nodeArrow.getAttribute("class");

		// if node is collapsed
		
		if (!nodeArrowState.contains("jqx-tree-item-arrow-expand")) {
		//if (nodeArrowState.contains("jqx-tree-item-arrow-collapse")) {
		if(Browser.currentBrowser.equals("internetexplorer")){
			BaseUI.click_js(nodeArrow);
			Thread.sleep(1000);
		}else{
			BaseUI.click(nodeArrow);
			Thread.sleep(1000);
		}
		}
	}

	public static void search_ByText(String textToEnter) throws InterruptedException {
		if (!BaseUI.elementExists("entityTree_SearchTextBox", null, null)) {
			BaseUI.click(Locator.lookupElement("entityTreeNode_SearchBar"));
			Thread.sleep(300);
		}

		BaseUI.enterText(Locator.lookupElement("entityTree_SearchTextBox"), textToEnter);
		Thread.sleep(1500);

	}

	public static void select_FirstSearchResult() throws InterruptedException {
		ArrayList<WebElement> resultList = new ArrayList<WebElement>();
		resultList.addAll(Locator.lookup_multipleElements("entityTree_Results", null, null));
		BaseUI.click(resultList.get(0));
		Thread.sleep(1000);
	}

	public static void select_SearchResultByText(String resultToSelect) throws Exception {
		BaseUI.click_js(Locator.lookupElement("entityTree_Results_ByText", resultToSelect, null));
		Thread.sleep(1000);
	}

	public static void verify_OnlySpecificEntityPresent(String entityToVerify) {
		ArrayList<WebElement> availableEntitiesWithoutDropdownArrow = new ArrayList<WebElement>();
		availableEntitiesWithoutDropdownArrow
				.addAll(Locator.lookup_multipleElements("entityTreeNode_AllNodesWithoutDownArrow", null, null));
		BaseUI.verify_true_AndLog(availableEntitiesWithoutDropdownArrow.size() == 1, "Only 1 result returned.",
				"Seeing multiple entities.");
		BaseUI.verifyElementHasExpectedText(availableEntitiesWithoutDropdownArrow.get(0), entityToVerify);

	}

	// Took out WFS to the list since that will always be there (as it's the
	// parent of all the other entities.
	public static void verify_Entity_AndChildNodes_Present(ArrayList<String> priorEntityList, String testEntity) {
		ArrayList<String> currentList = new ArrayList<String>();
		currentList.addAll(return_List_OfEntitiesAndChildren_ForGivenEntity(testEntity));
		currentList.remove("WFS");

		for (String entity : currentList) {
			BaseUI.verify_true_AndLog(priorEntityList.contains(entity),
					MessageFormat.format("Entity {0} was found.", entity),
					MessageFormat.format("Could not find entity {0} in prior list.", entity));
		}

		BaseUI.verify_true_AndLog(priorEntityList.size() == currentList.size(), "Lists both matched.",
				"Lists were NOT the same size.");

	}

	public static void verifyPriorEntityList_MatchesCurrentListOfEntities(ArrayList<String> priorList) {
		ArrayList<WebElement> currentList = new ArrayList<WebElement>();
		currentList.addAll(Locator.lookup_multipleElements("entityTree_AllAvailableEntities", null, null));

		BaseUI.verify_true_AndLog(priorList.size() > 0, "Results List was populated.",
				"Could not find matching results.");

		for (int i = 0; i < priorList.size(); i++) {
			BaseUI.baseStringCompare("Entity", priorList.get(i), currentList.get(i).getText());
		}

	}

	public static ArrayList<String> return_List_OfEntities() {
		ArrayList<WebElement> entityElementList = new ArrayList<WebElement>();
		ArrayList<String> entitySelectorList = new ArrayList<String>();
		entityElementList.addAll(Locator.lookup_multipleElements("entityTree_AllAvailableEntities", null, null));
		for (WebElement entity : entityElementList) {
			entitySelectorList.add(entity.getText());
		}

		return entitySelectorList;
	}

	public static ArrayList<String> return_List_OfEntitiesAndChildren_ForGivenEntity(String testEntity) {
		ArrayList<WebElement> entityElementList = new ArrayList<WebElement>();
		ArrayList<String> entitySelectorList = new ArrayList<String>();
		entityElementList
				.addAll(Locator.lookup_multipleElements("entityTree_ListOfEntity_AndChildNodes", testEntity, null));
		for (WebElement entity : entityElementList) {
			entitySelectorList.add(entity.getText());
		}

		return entitySelectorList;
	}

	public static void verifyResultsAllMatch(String textToMatch) {
		ArrayList<WebElement> resultsList = new ArrayList<WebElement>();
		resultsList.addAll(Locator.lookup_multipleElements("entityTree_Results", null, null));

		BaseUI.verify_true_AndLog(resultsList.size() > 0, "Results List was populated.",
				"Could not find matching results.");

		for (WebElement result : resultsList) {
			String resultString = result.getText();
			BaseUI.baseStringPartialCompare_IgnoreCase("SearchResult", textToMatch, resultString);
		}
	}

	public static void verifyNoResults(String textToMatch) {
		BaseUI.verify_false_AndLog(BaseUI.elementExists("entityTree_Results_ByText", textToMatch, null),
				MessageFormat.format("No matching results for {0}", textToMatch),
				MessageFormat.format("Found results for {0}.  Was expecting no results.", textToMatch));
	}

	public static void verifyNoResults() {
		BaseUI.verify_true_AndLog(BaseUI.elementExists("entityTree_Results_NOMatches", null, null),
				"No matching results.", "Found results.");

		BaseUI.verifyElementAppearsByString("entityTree_Results_NOMatches");
	}

	public static void select_Results(String resultToSelect) throws Exception {
		WebElement resultElement = Locator.lookupElement("entityTree_Results_ByText", resultToSelect, null);
		// BaseUI.click_js(resultElement);
		BaseUI.scroll_to_element(resultElement);
		Thread.sleep(500);
		BaseUI.click(resultElement);
		Thread.sleep(4500);
	}

	public static void close_SearchDropdown() throws Exception {
		if (BaseUI.elementExists("entityTree_SearchTextBox", null, null)) {
			BaseUI.click(Locator.lookupElement("entityTree_CloseSearchDropdownElement"));
			Thread.sleep(500);
			BaseUI.wait_forElementToNotExist("Global_LoadingSpinner", null, null);
		}
	}

	public static void reset_SearchResults() throws Exception {
		BaseUI.click(BaseUI.waitForElementToBeClickable("entityTree_ResultsResetButton", null, null));
		Thread.sleep(4000);
		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

}
