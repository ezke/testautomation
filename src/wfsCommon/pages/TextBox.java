package wfsCommon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class TextBox {

	String txtBox_Locator = "";
	String txtBox_Variable1 = "";
	String txtBox_Variable2 = "";

	public TextBox(String locator, String variable1, String variable2) {
		txtBox_Locator = locator;
		txtBox_Variable1 = variable1;
		txtBox_Variable2 = variable2;
	}

	public void enterTextIntoField_AndTab(String textToEnter) throws Exception {
		BaseUI.enterText(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2), textToEnter);
		//BaseUI.tabThroughField(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2));
		//BaseUI.tabThroughField(txtBox_Locator);
		BaseUI.tabThroughField(Browser.driver.switchTo().activeElement());
	}

	public void verifyErrorMessage_afterEnteringtext_ForGivenField(String textToEnter, String expectedErrorMessage)
			throws Exception {
		BaseUI.enterText(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2), textToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2));
		verifyErrorMessage_ForGivenField(expectedErrorMessage);
	}

	public void verifyErrorMessage_ForGivenField(String expectedErrorMessage) throws Exception {
		String errorText = Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2)
				.findElement(By.xpath("./following-sibling::span[@class='validationMessage']")).getText();
		BaseUI.baseStringCompare(txtBox_Locator, expectedErrorMessage, errorText);
	}

	public void verifyNO_ErrorMessage_ForGivenField(String textToEnter, String expectedText) throws Exception {
		BaseUI.enterText(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2), textToEnter);
		BaseUI.tabThroughField(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2));
		String fieldText = BaseUI
				.getTextFromInputBox(Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2));
		BaseUI.baseStringCompare(txtBox_Locator, expectedText, fieldText);
		WebElement errorElement = Locator.lookupElement(txtBox_Locator, txtBox_Variable1, txtBox_Variable2)
				.findElement(By.xpath("./following-sibling::span[@class='validationMessage']"));
		BaseUI.verifyElementDoesNotAppear(errorElement);
	}
}
