package wfsCommon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Locator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePicker {

    public String elementLocator;
    public String elementVariable1;
    public String elementVariable2;

    public DatePicker(String text_locator, String variable1, String variable2) {
        elementLocator = text_locator;
        elementVariable1 = variable1;
        elementVariable2 = variable2;
    }

    public void enter_Date(String date) throws Exception {
        WebElement dateTextBox = Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
        BaseUI.click(dateTextBox);
        dateTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        dateTextBox.sendKeys(date);
        BaseUI.tabThroughField(Locator.lookupElement(elementLocator, elementVariable1, elementVariable2));
        Thread.sleep(1500);
        Navigation.wait_for_LoadingSpinner_ToDisappear();
    }

    public WebElement returnDateBox() throws Exception {
        return Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
    }

    public String get_Text() throws Exception {
        return BaseUI.getTextFromInputBox(returnDateBox());
    }

    public void enter_Text(String textToEnter) throws Exception {
        WebElement dateTextBox = Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
        BaseUI.click(dateTextBox);
        dateTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        dateTextBox.sendKeys(textToEnter);
        BaseUI.tabThroughField(Locator.lookupElement(elementLocator, elementVariable1, elementVariable2));
        Thread.sleep(500);
    }

    public void verify_DateEntered_IsDateViewed(String dateToEnter) throws Exception {
        enter_Date(dateToEnter);
        WebElement dateTextBox = Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
        BaseUI.tabThroughField(dateTextBox);

        String date_Text = BaseUI.getTextFromInputBox(dateTextBox);
        BaseUI.baseStringCompare("Date Field", dateToEnter, date_Text);
    }

    public void verify_DateSelectedFromCalendar_IsDateViewed(String date) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date input_date = dateFormat.parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(input_date);

        pick_Date_FromCalendar(new SimpleDateFormat("MMM").format(cal.getTime()),
                new SimpleDateFormat("d").format(cal.getTime()), new SimpleDateFormat("yyyy").format(cal.getTime()));
        WebElement dateTextBox = Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
        BaseUI.tabThroughField(dateTextBox);

        String date_Text = BaseUI.getTextFromInputBox(dateTextBox);
        BaseUI.baseStringCompare("Date Field", date, date_Text);
    }

    public void verify_DropdownDisplayed() {
        WebElement dateTextBox = Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
        BaseUI.verifyElementAppears(dateTextBox);
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    private Date previousDate90Days() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -90);
        return cal.getTime();
    }

    public String getTodaysDate_AsString_WithFormat(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();

        String todaysDate = dateFormat.format(date);

        return todaysDate;
    }

    public String getYesterdaysDate_AsString_WithFormat(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();

        String yesterdaysDate = dateFormat.format(yesterday());

        return yesterdaysDate;
    }

    public String getPrevious90DayDate_AsString_WithFormat(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        String previousDate = dateFormat.format(previousDate90Days());
        return previousDate;
    }

    public String getTodaysDate_AsString() {
        return getTodaysDate_AsString_WithFormat("MM/dd/yyyy");
    }

    public String getYesterdaysDate_AsString() {
        return getYesterdaysDate_AsString_WithFormat("MM/dd/yyyy");
    }

    public String getPrevious90DayDate_AsString() {
        return getPrevious90DayDate_AsString_WithFormat("MM/dd/yyyy");
    }

    public WebElement datePicker() {
        WebElement date_picker = null;
        WebElement dateTextBox = Locator.lookupElement(elementLocator, elementVariable1, elementVariable2);
        date_picker = dateTextBox.findElement(By.xpath("./following-sibling::span[@class='input-group-btn']"));

        return date_picker;
    }

    public void pick_Date_FromCalendar(String month, String day, String year) throws Exception {
        BaseUI.click(datePicker());
        Thread.sleep(500);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("datePick_MonthDropdown"), month);
        Thread.sleep(100);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("datePick_YearDropdown"), year);
        Thread.sleep(100);
        BaseUI.click(Locator.lookupElement("datePick_day_ByText", day, null));
        Thread.sleep(200);
    }

    // example: passing in -1 would get you yesterday's date.
    public String getDateAsString_InRelationToTodaysDate(Integer numberOfDatesFromToday) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, numberOfDatesFromToday);

        return dateFormat.format(cal.getTime());
    }
}
