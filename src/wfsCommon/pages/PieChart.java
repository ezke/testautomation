package wfsCommon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.internal.Locatable;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

import java.text.MessageFormat;
import java.util.ArrayList;

public class PieChart {

	public String pieChartID;

	public PieChart(String pieID) {
		pieChartID = pieID;
	}

	public String get_ToolTip_ByIndex(Integer index) throws Exception {
		Mouse mouse = ((HasInputDevices) Browser.driver).getMouse();
		
		Thread.sleep(500);
		// Invokes mouseMove method by passing element coordinates as argument
		mouse.mouseMove(((Locatable) Locator.lookupElement("panel_Header", pieChartID, null)).getCoordinates());
		mouse.mouseMove(
				((Locatable) Locator.lookupElement("pieChart_chartContainer", pieChartID, null)).getCoordinates());
		mouse.mouseMove(((Locatable) Locator.lookupElement("pieChart_pieSlice_ByIndex", pieChartID, index.toString()))
				.getCoordinates());
		
//		BaseUI.element_ClickAndHold(Locator.lookupElement("pieChart_pieSlice_ByIndex", pieChartID, index.toString()));
//		
//		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("pieChart_testToolText", null, null);
		String textRetrieved = BaseUI.getTextFromField(Locator.lookupElement("pieChart_testToolText"));


		return textRetrieved;

	}

	public String get_ToolTip_ByText(String legendKey) throws Exception {
		Integer index = return_Index_OfLegendItem(legendKey);

		Mouse mouse = ((HasInputDevices) Browser.driver).getMouse();
		// BaseUI.elementHover(Locator.lookupElement("testPie"));
		Thread.sleep(500);
		// Invokes mouseMove method by passing element coordinates as argument
		mouse.mouseMove(((Locatable) Locator.lookupElement("panel_Header", pieChartID, null)).getCoordinates());
		mouse.mouseMove(
				((Locatable) Locator.lookupElement("pieChart_chartContainer", pieChartID, null)).getCoordinates());
		Thread.sleep(3000);
		

		mouse.mouseMove(((Locatable) Locator.lookupElement("pieChart_pieSlice_ByIndex", pieChartID, index.toString()))
				.getCoordinates());
//		BaseUI.element_ClickAndHold(Locator.lookupElement("pieChart_pieSlice_ByIndex", pieChartID, index.toString()));

//		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("pieChart_testToolText", null, null);
		String textRetrieved = Locator.lookupElement("pieChart_testToolText").getAttribute("innerText");

		return textRetrieved;
	}

	// Getting the ToolTip by matching up the stroke will work better than using
	// the index.
	// public String get_ToolTip_ByText_Stroke(String legendKey) throws
	// Exception {
	// String stroke = return_Stroke_OfLegendElement(legendKey);
	//
	// Mouse mouse = ((HasInputDevices) Browser.driver).getMouse();
	// Thread.sleep(500);
	// // Invokes mouseMove method by passing element coordinates as argument
	// mouse.mouseMove(((Locatable) Locator.lookupElement("panel_Header",
	// pieChartID, null)).getCoordinates());
	// mouse.mouseMove(
	// ((Locatable) Locator.lookupElement("pieChart_chartContainer", pieChartID,
	// null)).getCoordinates());
	// Thread.sleep(3000);
	//
	//
	// mouse.mouseMove(((Locatable)
	// Locator.lookupElement("pieChart_pieSlice_ByIndex", pieChartID,
	// index.toString()))
	// .getCoordinates());
	//
	// Thread.sleep(750);
	// String textRetrieved =
	// BaseUI.getTextFromField(Locator.lookupElement("pieChart_testToolText"));
	//
	//
	// return textRetrieved;
	// }

	public void uncheck_legendBox(String legendKey) throws Exception {

		if (legend_isChecked(legendKey)) {
			//BaseUI.waitForElementToNOTBeDisplayed("pieChart_testToolText", null, null);
			WebElement checkBox = Locator.lookupElement("pieChart_legendCheckBox_ByText", pieChartID, legendKey);
			BaseUI.click_js(checkBox);
			Thread.sleep(500);
		}
	}

	public void check_legendBox(String legendKey) throws InterruptedException {

		if (!legend_isChecked(legendKey)) {
			WebElement checkBox = Locator.lookupElement("pieChart_legendCheckBox_ByText", pieChartID, legendKey);
			BaseUI.click(checkBox);
			Thread.sleep(500);
		}
	}
	
	public void uncheck_ALL_legendBoxes() throws Exception {
		ArrayList<String> legendList = list_of_LegendKeys();

		for (String legend : legendList) {
			uncheck_legendBox(legend);
		}

	}
	

	public void check_ALL_legendBoxes() throws Exception {
		ArrayList<String> legendList = list_of_LegendKeys();

		for (String legend : legendList) {
			check_legendBox(legend);
		}

	}

	public Boolean legend_isChecked(String legendKey) {
		WebElement checkBox = Locator.lookupElement("pieChart_legendCheckBox_ByText", pieChartID, legendKey);
		Boolean checked = Double.parseDouble(checkBox.getAttribute("fill-opacity")) == 1 ? true : false;
		return checked;
	}

	// We can use the stroke to identify the element. Fill would be better, but
	// fill doesn't match up on the pie slice.
	public String return_Stroke_OfLegendElement(String legendKey) {
		WebElement checkBox = Locator.lookupElement("pieChart_legendCheckBox_ByText", pieChartID, legendKey);

		return checkBox.getAttribute("stroke");
	}

	// Returns the Index of the Legend Item. List returned only contains checked
	// checkbox legend keys. Keys that are unchecked won't get counted.
	public int return_Index_OfLegendItem(String legendKey) {
		ArrayList<WebElement> legendElements = Locator.lookup_multipleElements("pieChart_legendList_Checked",
				pieChartID, null);

		for (int i = 1; i <= legendElements.size(); i++) {
			if (legendElements.get(i - 1).getText().equals(legendKey)) {
				return i;
			}
		}

		return 0;
	}

	public String return_PieChart_Title() {
		String title = BaseUI.getTextFromField(Locator.lookupElement("pieChart_Title", pieChartID, null));
		title = title.replace(",", "");
		return title;
	}

	public ArrayList<String> list_of_LegendKeys() {
		ArrayList<WebElement> legendElements = Locator.lookup_multipleElements("pieChart_legendList", pieChartID, null);
		ArrayList<String> list_DDAs = new ArrayList<String>();

		for (WebElement legend : legendElements) {
			list_DDAs.add(legend.getText());
		}

		return list_DDAs;
	}

	public ArrayList<WebElement> list_of_PieSlices() {
		ArrayList<WebElement> pieSlices = Locator.lookup_multipleElements("pieChart_ListOfSlices", pieChartID, null);

		return pieSlices;
	}

	public ArrayList<String> list_of_Visible_LegendKeys() {
		ArrayList<WebElement> legendElements = Locator.lookup_multipleElements("pieChart_legendList", pieChartID, null);
		ArrayList<String> list_DDAs = new ArrayList<String>();

		for (WebElement legend : legendElements) {
			if (legend_isChecked(legend.getText())) {
				list_DDAs.add(legend.getText());
			}
		}

		return list_DDAs;
	}

	public void verify_DDAs_InChart() throws Exception {
		ArrayList<String> ddaList = list_of_Visible_LegendKeys();

		ArrayList<String> chartList = new ArrayList<String>();

		for (Integer i = 1; i <= ddaList.size(); i++) {
			String chartToolTipText = get_ToolTip_ByIndex(i);
			chartToolTipText = chartToolTipText.split("\\n")[0];
			chartToolTipText = chartToolTipText.replace("DDA: ", "");
			String output = "";
			if (!chartToolTipText.equals(ddaList.get(i - 1))) {
				output += MessageFormat.format("Expected {0}, seeing {1}", ddaList.get(i - 1), chartToolTipText);

			}
			BaseUI.verify_true_AndLog(output.equals(""), "", output);

			// BaseUI.verify_true_AndLog(chartToolTipText.contains(ddaList.get(i-1)),
			// "", "");
		}

	}
}
