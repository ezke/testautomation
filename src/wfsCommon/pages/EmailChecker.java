package wfsCommon.pages;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import r360.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class EmailChecker {

	public static void login_ClickForgotPassword_ResetPasswordForm(String entity, String loginName) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("LoginPage_txt_EntityName"), entity);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("LoginPage_txt_UserName"), loginName);
		LoginPage.clickForgotPasswordLink();
		Thread.sleep(1000);
	}

	public static void login_putsbox(String username, String password) throws Exception {
		Browser.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Browser.driver.manage().window().maximize();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("putsbox_Username"), username);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("putsbox_Password"), password);
		BaseUI.click(Locator.lookupElement("putsbox_Signin_Button"));
		Thread.sleep(6000);
	}

	public static void verifyEmailReceived(String username, String password, String emailName, String emailTitle)
			throws Exception {
		login_putsbox(username, password);
		clickOnEmailName(emailName);
		Thread.sleep(8000);
		BaseUI.click(Locator.lookupElement("putsbox_Inspect_Link"));
		Thread.sleep(2000);
		WebElement element = BaseUI.waitForElementToBeClickable("putsbox_Subject_ByText", emailTitle, null);
		BaseUI.verifyElementAppears(element);
		// WebElement tempElement =
		// Locator.findElement(By.xpath("//td[contains(.,'" + emailTitle +
		// "')]"));
		// BaseUI.verifyElementAppears(tempElement);
	}

	public static void verifyEmailNotReceived(String username, String password, String emailName, String emailTitle)
			throws Exception {
		login_putsbox(username, password);
		clickOnEmailName(emailName);
		Thread.sleep(2000);
		WebElement tempElement = Locator.findElement(By.xpath("//td[contains(.,'" + emailTitle + "')]"));
		BaseUI.verifyElementDoesNotAppear(tempElement);
	}

	public static void clickOnEmailName(String emailName) throws Exception {
		WebElement tempElement = Locator.lookupElement("putsbox_Email_ByEmailName", emailName, null);
		// WebElement tempElement =
		// Locator.findElement(By.xpath("//a[contains(.,'" + emailName +
		// "')]/.."));
		BaseUI.click_js(tempElement);
		Thread.sleep(3000);
	}

	public static void deleteAllEmailReceived() throws Exception {
		WebElement tempDeleteElement = Locator.lookupElement("putsbox_ClearHistory_Button");
		BaseUI.click(tempDeleteElement);
		Thread.sleep(2000);
		Alert alert = Browser.driver.switchTo().alert();
		alert.accept();
		Thread.sleep(2000);
	}

	public static void clickOnEmailLink() throws Exception {
		WebElement elementToClick = Locator.lookupElement("email_ResetPasswordForm_Link");
		((JavascriptExecutor) Browser.driver)
				.executeScript("window.scrollTo(0," + elementToClick.getLocation().y + ")");
		elementToClick.click();
		Thread.sleep(5000);
		// BaseUI.click_js(BaseUI.waitForElementToBeClickable("email_ResetPasswordForm_Link",
		// null, null));
	}

	public static void clickOnEmailLink_ResetPassword(String subjectText, String entity, String userName,
			String password) throws Exception {
		// Need to switch to new tab
		String winHandleParent = Browser.driver.getWindowHandle();
		clickOnLinkBySubjectText(subjectText);
		// for (String winHandle : Browser.driver.getWindowHandles()){
		// Browser.driver.switchTo().window(winHandle);
		// }
		Thread.sleep(2000);
		clickOnEmailLink();
		// WebElement tempElement =
		// Locator.lookupElement("email_ResetPasswordForm_Link");
		// tempElement.click();
		Thread.sleep(2000);
		if (BaseUI.pageSourceContainsString("Continue to this website (not recommended).")) {
			BaseUI.click(Locator.lookupElement("ie_ContinueToWebsite"));
			Thread.sleep(3500);
		}
		Thread.sleep(1000);
		enterResetPasswordForm(entity, userName, password);
		switchWindow_Or_NavigateBack(winHandleParent);
		// Browser.driver.switchTo().window(winHandleParent);

	}

	public static void verifyResetPasswordModalAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("email_ResetPasswordForm_Modal"));
	}

	public static void enterResetPasswordForm(String entity, String userName, String password)
			throws InterruptedException {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("email_ResetPasswordForm_EnityText"), entity);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("email_ResetPasswordForm_LoginNameText"), userName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("email_ResetPasswordForm_NewPasswordText"), password);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("email_ResetPasswordForm_ConfirmNewPasswordText"),
				password);
		BaseUI.click(Locator.lookupElement("email_ResetPasswordForm_Submit_Button"));
		Thread.sleep(4000);
	}

	public static void clickOnLinkBySubjectText(String subjectText) throws Exception {
		ArrayList<WebElement> subjectLine = new ArrayList<WebElement>();
		subjectLine.addAll(Locator.lookup_multipleElements("putsbox_Subject_List", null, null));

		ArrayList<WebElement> link = new ArrayList<WebElement>();
		link.addAll(Locator.lookup_multipleElements("putsbox_HTML_LinkList", null, null));

		System.out.println("No. of Rows in the table :" + subjectLine.size());
		System.out.println("No. of Links in the table :" + link.size());

		for (int i = 0; i < subjectLine.size(); i++) {
			String subjectln = subjectLine.get(i).getText();
			System.out.println("subject Values : " + subjectln);

			if (subjectln.equals(subjectText)) {
				// System.out.println("Found");
				if (!Browser.defaultBrowser.equals("internetexplorer")) {
					BaseUI.ClickAndSwitchWindow(link.get(i), true, 2000);
				} else {
					String href = link.get(i).getAttribute("href");
					// Browser.openBrowser("http://putsbox.com/users/sign_in");
					Browser.driver.navigate().to(href);
					;
				}
				// link.get(i).click();
				break;
			}

		}
	}

	public static void verifyLinkErrorMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("email_ResetPasswordForm_Link_ErrorValidation"));
	}

	public static void clearResetPasswordFields() throws Exception {
		WebElement entityNametextField = Locator.lookupElement("email_ResetPasswordForm_EnityText");
		entityNametextField.sendKeys(Keys.CONTROL + "a");
		entityNametextField.sendKeys(Keys.DELETE);
		BaseUI.tabThroughField(Locator.lookupElement("email_ResetPasswordForm_EnityText"));
		WebElement loginNameTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(loginNameTextField, "");
		BaseUI.tabThroughField(Locator.lookupElement("email_ResetPasswordForm_LoginNameText"));
		WebElement newPasswordTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(newPasswordTextField, "");
		BaseUI.tabThroughField(Locator.lookupElement("email_ResetPasswordForm_NewPasswordText"));
		WebElement confirmNewPasswordTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(confirmNewPasswordTextField, "");
	}

	public static void verifyResetPasswordForm_ErrorMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("email_ResetPasswordForm_ErrorMessage"));
	}

	public static void switchWindow_Or_NavigateBack(String windowHandle) throws Exception {
		if (!Browser.defaultBrowser.equals("internetexplorer")) {
			Browser.driver.switchTo().window(windowHandle);
			Thread.sleep(1000);
		} else {
			Browser.clickBrowserBackButton();
			if (BaseUI.pageSourceContainsString("Continue to this website (not recommended).")) {
				Browser.clickBrowserBackButton();
			}
			Browser.clickBrowserBackButton();
		}
	}
}
