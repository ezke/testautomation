package wfsCommon.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Is;
import utils.Locator;

public class Pagination {

	public String gridID;

	public Pagination(String grid_ID) {
		gridID = grid_ID;
	}

	// Returns a list of elements representing all of the values stored for the
	// passed in column name
	public ArrayList<WebElement> columns_Elements_List(String columnName) {
		// Column number to use.
		Integer columnNumber = get_Column_Number_ByColumnName(columnName);

		// Use that column number to reference the correct column and then get a
		// list of the Elements in that column.
		ArrayList<WebElement> column_ValuesList = new ArrayList<WebElement>();
		column_ValuesList.addAll(
				Locator.lookup_multipleElements("Pagin_list_Values_forColumn", gridID, columnNumber.toString()));

		return column_ValuesList;
	}

	public WebElement get_row_ByColumnName_And_ColumnMatchingText(String columnName, String columnMatchingText) {
		Integer columnNumber = get_Column_Number_ByColumnName(columnName);
		// Use that column number to reference the correct column and then get a
		// list of the Elements in that column.
		ArrayList<WebElement> column_ValuesList = new ArrayList<WebElement>();
		column_ValuesList.addAll(
				Locator.lookup_multipleElements("Pagin_list_Values_forColumn", gridID, columnNumber.toString()));

		for (WebElement elementMatch : column_ValuesList) {
			if (elementMatch.getText().equals(columnMatchingText)) {
				return elementMatch
						.findElement(By.xpath(".//parent::div[contains(@class, 'ui-widget-content slick-row')]"));
			}
		}

		return null;

	}

	public Integer get_Column_Number_ByColumnName(String columnName) {
		Integer columnNumber = 0;

		ArrayList<WebElement> columnElementList = new ArrayList<WebElement>();
		columnElementList.addAll(Locator.lookup_multipleElements("Pagin_headers_list", gridID, null));

		// determine the column number that our values will be stored in.
		for (int i = 1; i <= columnElementList.size(); i++) {
			WebElement columnTitle = columnElementList.get(i - 1)
					.findElement(By.xpath("./span[@class='slick-column-name']"));
			if (columnTitle.getText().equals(columnName)) {
				columnNumber = i;
				i = columnElementList.size() + 1;
			}
		}

		return columnNumber;
	}

	// Returns a List of String for the given list of Elements.
	public ArrayList<String> columns_Texts_List(ArrayList<WebElement> elementList) {
		ArrayList<String> textList = new ArrayList<String>();
		for (WebElement element : elementList) {
			textList.add(element.getText());
		}

		return textList;
	}

	// Returns a List of String for the given Column Name. Will work on later.
	// public ArrayList<String> columns_Texts_List(ArrayList<WebElement>
	// elementList)
	// {
	// ArrayList<String> textList = new ArrayList<String>();
	// for(WebElement element: elementList)
	// {
	// textList.add(element.getText());
	// }
	//
	// return textList;
	// }

	// Returns the Filter Textbox for the passed in Column name.
	public WebElement filter_textbox_ByColumnName(String columnName) {
		WebElement filterTextbox = null;

		Integer columnNumber = get_Column_Number_ByColumnName(columnName);

		filterTextbox = Locator.lookupElement("Pagin_txt_By_columnName", gridID, columnNumber.toString());

		return filterTextbox;
	}

	public void filter_textbox_SendKeys(String columnName, String textToEnter) throws Exception {
		WebElement filterTextBox = filter_textbox_ByColumnName(columnName);
		BaseUI.enterText(filterTextBox, textToEnter);
		Thread.sleep(500);
	}

	public void filter_textbox_ClearFilter(String columnName) throws Exception {
		WebElement filterTextBox = filter_textbox_ByColumnName(columnName);
		if (filterTextBox == null) {
			BaseUI.verify_true_AndLog(false, "",
					MessageFormat.format("Was unable to get filter textbox by name {0}.", columnName));

		}
		filterTextBox.clear();
		Thread.sleep(500);
	}

	public WebElement editButton_ByColumnName_andColumnText(String columnName, String columnText) {
		ArrayList<WebElement> columnElements = new ArrayList<WebElement>();
		columnElements.addAll(columns_Elements_List(columnName));

		WebElement editButton = null;

		for (WebElement correctName : columnElements) {
			if (correctName.getText().equals(columnText)) {
				editButton = correctName.findElement(By.xpath(
						"./parent::div[contains(@class, 'ui-widget-content slick-row')][1]//a[./*[@class='fa fa-edit']]"));
				break;
			}
		}

		return editButton;
	}

	public WebElement delete_ByColumnName_andColumnText(String columnName, String columnText) {
		ArrayList<WebElement> columnElements = new ArrayList<WebElement>();
		columnElements.addAll(columns_Elements_List(columnName));

		WebElement deleteButton = null;

		for (WebElement correctName : columnElements) {
			if (correctName.getText().equals(columnText)) {
				deleteButton = correctName.findElement(By.xpath(
						"./parent::div[contains(@class, 'ui-widget-content slick-row')][1]//a[./*[@class='fa fa-times']]"));
				break;
			}
		}

		return deleteButton;
	}

	public void verifyFirstButtonEnabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_first", gridID, null);
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		BaseUI.verifyElementEnabled(paginationDirection);
	}

	public void verifyNextButtonEnabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_next", gridID, null);
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		BaseUI.verifyElementEnabled(paginationDirection);
	}

	public void verifyPreviousButtonEnabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_prev", gridID, null);
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		BaseUI.verifyElementEnabled(paginationDirection);
	}

	public void verifyLastButtonEnabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_last", gridID, null);
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		BaseUI.verifyElementEnabled(paginationDirection);
	}

	public void verifyFirstButtonDisabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_first", gridID, null);
		BaseUI.verifyElementHasExpectedPartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		// BaseUI.verifyElementDisabled(Locator.lookupElement("Pagin_btn_gridNavigate_first",
		// gridID, null));
	}

	public void verifyNextButtonDisabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_next", gridID, null);
		BaseUI.verifyElementHasExpectedPartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		// BaseUI.verifyElementDisabled(Locator.lookupElement("Pagin_btn_gridNavigate_next",
		// gridID, null));
	}

	public void verifyPreviousButtonDisabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_prev", gridID, null);
		BaseUI.verifyElementHasExpectedPartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		// BaseUI.verifyElementDisabled(Locator.lookupElement("Pagin_btn_gridNavigate_prev",
		// gridID, null));
	}

	public void verifyLastButtonDisabled() {
		WebElement paginationDirection = Locator.lookupElement("Pagin_btn_gridNavigate_last", gridID, null);
		BaseUI.verifyElementHasExpectedPartialAttributeValue(paginationDirection, "class", "ui-state-disabled");
		// BaseUI.verifyElementDisabled(Locator.lookupElement("Pagin_btn_gridNavigate_last",
		// gridID, null));
	}

	// Verifies Tabbing worked for all available checkboxes,
	// this Assert will only work if you don't have Edit/Update Buttons.
	public void verifytTabFromFirstCheckboxToEndOfCheckboxes() throws Exception {
		// Set Focus to the firstcheckbox.
		WebElement FirstCheckbox = Locator.lookupElement("Pagin_firstTable_checkbox", gridID, null);
		FirstCheckbox.sendKeys("");

		ArrayList<WebElement> checkboxList = new ArrayList<WebElement>();
		checkboxList.addAll(Locator.lookup_multipleElements("Pagin_all_checkboxes", gridID, null));

		Assert.assertTrue(checkboxList.size() > 0, "Could NOT find checkboxes in list.");

		for (int i = 1; i <= checkboxList.size(); i++) {

			WebElement currentFocusedElement = Browser.driver.switchTo().activeElement();
			String currentElementType = currentFocusedElement.getAttribute("type");
			if (currentElementType != null && currentElementType.equals("checkbox")) {
				currentFocusedElement.sendKeys(Keys.TAB);
			} else {
				BaseUI.verify_true_AndLog(false, "", "Could not tab through all of the checkboxes");
			}
		}
	}

	public WebElement first_Checkbox() {
		return Locator.lookupElement("Pagin_firstTable_checkbox", gridID, null);
	}

	// Verifies Tabbing worked for all available checkboxes,
	// this Assert is set up to also tab through the Edit Links.
	public void verifytTabFromFirstCheckboxToEndOfCheckboxes_withEditUpdateLink() throws Exception {
		// Set Focus to the firstcheckbox.
		WebElement FirstCheckbox = Locator.lookupElement("Pagin_firstTable_checkbox", gridID, null);
		FirstCheckbox.sendKeys("");

		ArrayList<WebElement> checkboxList = new ArrayList<WebElement>();
		checkboxList.addAll(Locator.lookup_multipleElements("Pagin_all_checkboxes", gridID, null));

		Assert.assertTrue(checkboxList.size() > 0, "Could NOT find checkboxes in list.");

		for (int i = 1; i <= checkboxList.size(); i++) {

			WebElement currentFocusedElement = Browser.driver.switchTo().activeElement();
			String currentElementType = currentFocusedElement.getAttribute("type");
			if (currentElementType != null && currentElementType.equals("checkbox")) {
				currentFocusedElement.sendKeys(Keys.TAB);
				// Also need to tab through the second element, which should be
				// the Update link.
				WebElement current2ndElement = Browser.driver.switchTo().activeElement();
				current2ndElement.sendKeys(Keys.TAB);
			} else {
				BaseUI.verify_true_AndLog(false, "", "Could not tab through all of the checkboxes");
				// Assert.fail("Could not tab through all of the checkboxes");
			}
		}
	}

	// verifies that column sort icon is ascending.
	public void verifyColumnAscending(String columnName) {
		BaseUI.verifyElementHasExpectedAttributeValue(
				Locator.lookupElement("Pagin_header_sort_icon", gridID, columnName), "class",
				"slick-sort-indicator slick-sort-indicator-asc");
	}

	// verifies that column sort icon is descending.
	public void verifyColumnDescending(String columnName) {
		BaseUI.verifyElementHasExpectedAttributeValue(
				Locator.lookupElement("Pagin_header_sort_icon", gridID, columnName), "class",
				"slick-sort-indicator slick-sort-indicator-desc");
	}

	// use this to verify numeric columns are in the proper order
	public void verifyColumnAscending_numeric(String columnName) {
		verifyColumnAscending(columnName);

		ArrayList<WebElement> values_list = columns_Elements_List(columnName);
		ArrayList<String> values_string_list = new ArrayList<String>();

		for (WebElement value_element : values_list) {
			String elementText = value_element.getText();
			elementText = elementText.equals("") || elementText == null ? "0" : elementText;
			values_string_list.add(elementText);
		}

		BaseUI.verify_true_AndLog(values_string_list.size() > 0,
				MessageFormat.format("List for column: {0} was populated.", columnName),
				MessageFormat.format("List for column: {0} had no values in it.", columnName));

		// Verify that each value was greater than the last one.
		for (int i = 1; i < values_string_list.size(); i++) {
			// Assert.assertTrue(
			// Integer.parseInt(values_string_list.get(i)) >=
			// Integer.parseInt(values_string_list.get(i - 1)));
			BaseUI.verify_true_AndLog(
					Integer.parseInt(values_string_list.get(i)) >= Integer.parseInt(values_string_list.get(i - 1)),
					"Values were as expected.",
					MessageFormat.format("Column: {0} was not Ascending in numeric order.", columnName));
		}

	}

	// use this to verify numeric columns are in the proper order
	public void verifyColumnDescending_numeric(String columnName) {
		verifyColumnDescending(columnName);

		ArrayList<WebElement> values_list = columns_Elements_List(columnName);
		ArrayList<String> values_string_list = new ArrayList<String>();

		for (WebElement value_element : values_list) {
			String elementText = value_element.getText();
			elementText = elementText.equals("") || elementText == null ? "0" : elementText;
			values_string_list.add(elementText);
		}

		BaseUI.verify_true_AndLog(values_string_list.size() > 0,
				MessageFormat.format("List for column: {0} was populated.", columnName),
				MessageFormat.format("List for column: {0} had no values in it.", columnName));

		// Verify that each value was greater than the last one.
		for (int i = 1; i < values_string_list.size(); i++) {
			BaseUI.verify_true_AndLog(
					Integer.parseInt(values_string_list.get(i)) <= Integer.parseInt(values_string_list.get(i - 1)),
					"Values were as expected.",
					MessageFormat.format("Column: {0} was not Descending in numeric order.", columnName));
		}

	}

	// use this to verify numeric columns are in the proper order
	public void verifyColumnAscending_alphabetical(String columnName) {
		verifyColumnAscending(columnName);

		ArrayList<WebElement> values_list = columns_Elements_List(columnName);
		ArrayList<String> values_string_list = new ArrayList<String>();
		ArrayList<String> values_string_list_sort = new ArrayList<String>();

		for (WebElement value_element : values_list) {
			values_string_list.add(value_element.getText());
			values_string_list_sort.add(value_element.getText());
		}

		Collections.sort(values_string_list_sort, String.CASE_INSENSITIVE_ORDER);

		BaseUI.verify_true_AndLog(values_string_list.size() > 0,
				MessageFormat.format("List for column: {0} was populated.", columnName),
				MessageFormat.format("List for column: {0} had no values in it.", columnName));

		// Verify that the sorted list matches the unsorted list.
		for (int i = 0; i < values_string_list.size(); i++) {
			BaseUI.verify_true_AndLog(values_string_list.get(i).equals(values_string_list_sort.get(i)),
					MessageFormat.format("Value: {0} matched Value: {1}", values_string_list.get(i),
							values_string_list_sort.get(i)),
					MessageFormat.format("Value: {0} did NOT match Value: {1}", values_string_list.get(i),
							values_string_list_sort.get(i)));
		}

	}

	// Pass in your string lists and verify that they don't contain any matching
	// values.
	public void verifyPagesListsDontMatch(ArrayList<String> previousPage, ArrayList<String> currentPage) {
		BaseUI.verify_true_AndLog(currentPage.size() > 0, "Current page contained values.",
				"Current page did NOT contain any values.");
		BaseUI.verify_true_AndLog(previousPage.size() > 0, "Previous page contained values.",
				"Previous page did NOT contain any values.");

		// Make sure the text from the previous page does not appear on this
		// page.
		for (int i = 0; i < currentPage.size(); i++) {
			for (String previousText : previousPage) {
				Assert.assertFalse(currentPage.get(i).equals(previousText),
						MessageFormat.format("Did not expect to see value: {0} on current page.", previousText));

				// Verify that previous text is not contained in the current
				// page.
				// BaseUI.verify_true_AndLog(!currentPage.get(i).contains(previousText),
				// MessageFormat.format("List value {0} was found on current
				// page.", previousText),
				// MessageFormat.format("List value {0} was NOT found on current
				// page.", previousText));
			}

		}

	}

	// Pass in your string lists and verify that they don't contain any matching
	// values.
	public void verifyPagesListsMatch(ArrayList<String> previousPage, ArrayList<String> currentPage) {
		BaseUI.verify_true_AndLog(currentPage.size() > 0, "Current page contained values.",
				"Current page did NOT contain any values.");
		BaseUI.verify_true_AndLog(previousPage.size() > 0, "Previous page contained values.",
				"Previous page did NOT contain any values.");

		for (int i = 0; i < currentPage.size(); i++) {
			Assert.assertTrue(previousPage.get(i).equals(currentPage.get(i)), MessageFormat
					.format("Expected Value: {0}, Current Value: {1}", previousPage.get(i), currentPage.get(i)));

			BaseUI.verify_true_AndLog(previousPage.get(i).equals(currentPage.get(i)),
					MessageFormat.format("Value: {0} matched Value: {1}", previousPage.get(i), currentPage.get(i)),
					MessageFormat.format("Expected Value: {0}, Current Value: {1}", previousPage.get(i),
							currentPage.get(i)));

		}

	}

	// Pass in your string lists and verify that previous list is all in the
	// second list
	public void verifySecondListContains_AllFirstListEntries(ArrayList<String> previousList,
			ArrayList<String> secondList) {
		BaseUI.verify_true_AndLog(previousList.size() > 0, "Current page contained values.",
				"Previous List did NOT contain any values.");
		BaseUI.verify_true_AndLog(secondList.size() > 0, "Previous page contained values.",
				"Second List did NOT contain any values.");

		// Make sure we can find the entries from the previous list in the
		// second list.

		for (String previousText : previousList) {
			// Assert.assertTrue(secondList.contains(previousText),
			// MessageFormat.format("Could not find entry: {0} in second list.",
			// previousText));

			BaseUI.verify_true_AndLog(secondList.contains(previousText),
					MessageFormat.format("Value: {0} was found", previousText),
					MessageFormat.format("Could not find entry: {0} in second list.", previousText));
		}
	}

	public void verifyLessThan10Entries() {
		ArrayList<WebElement> entryList = new ArrayList<WebElement>();
		entryList.addAll(Locator.lookup_multipleElements("Pagin_list_AllRows", gridID, null));

		Assert.assertTrue(entryList.size() < 10, "Expected size to be less than 10.");
		BaseUI.verify_true_AndLog(entryList.size() < 10, "Less than 10 entries.", "Expected size to be less than 10.");
	}

	// Verify that the count of rows is greater than the passed in Integer.
	public void verifyCountOverIntEntries(Integer count) {
		ArrayList<WebElement> entryList = new ArrayList<WebElement>();
		entryList.addAll(Locator.lookup_multipleElements("Pagin_list_AllRows", gridID, null));

		BaseUI.verify_true_AndLog(entryList.size() > count,
				MessageFormat.format("Size was greater than {0} entries", count.toString()),
				MessageFormat.format("Expected size to be greater than {0} entries.", count.toString()));
	}

	// Verify that the count of rows is greater than the passed in Integer.
	// List will be passed in.
	public void verifyCountOverIntEntries(Integer count, ArrayList<String> listToCheck) {

		BaseUI.verify_true_AndLog(listToCheck.size() > count,
				MessageFormat.format("Size was greater than {0} entries", count.toString()),
				MessageFormat.format("Expected size to be greater than {0} entries.", count.toString()));
	}

	// Verify that the count of rows is less than the passed in Integer.
	public void verifyCountLessThan_IntEntries(Integer count) {
		ArrayList<WebElement> entryList = new ArrayList<WebElement>();
		entryList.addAll(Locator.lookup_multipleElements("Pagin_list_AllRows", gridID, null));

		// Assert.assertTrue(entryList.size() < count,
		// MessageFormat.format("Expected size to be greater than {0} entries.",
		// count.toString()));

		BaseUI.verify_true_AndLog(entryList.size() < count,
				MessageFormat.format("Size was Less than {0} entries", count.toString()),
				MessageFormat.format("Expected size to be Less than {0} entries, but it was greater than or equal to.",
						count.toString()));
	}

	// Get the row count displayed in the "Showing all 175 rows" bottom section.
	public Integer get_row_Count() throws Exception {

		show_All();
		WebElement rowCountElement = Locator.lookupElement("Pagin_slick_Status", gridID, null);
		String rowCountText = rowCountElement.getText();
		rowCountText = rowCountText.replace("Showing all ", "");
		rowCountText = rowCountText.replace(" rows", "");

		Integer rowCountInt = Integer.parseInt(rowCountText.trim());
		return rowCountInt;

	}

	public ArrayList<String> getAllEntries(String columnName) throws Exception {
		show_10();
		WebElement nextButton = null;
		ArrayList<String> entryList = new ArrayList<String>();

		int maxCount = 0;
		Boolean continueCycle = true;
		while (continueCycle) {
			nextButton = Locator.lookupElement("Pagin_btn_gridNavigate_next", gridID, null);
			if (BaseUI.elementHasPartialExpectedAttribute(nextButton, "class", "ui-state-disabled")) {
				continueCycle = false;
			}

			ArrayList<WebElement> entryElementList = new ArrayList<WebElement>();
			entryElementList.addAll(columns_Elements_List(columnName));
			entryList.addAll(columns_Texts_List(entryElementList));

			// elementHasExpectedAttribute

			if (continueCycle) {
				BaseUI.click(nextButton);
				Thread.sleep(100);
			}

			maxCount++;
		}

		click_first();

		return entryList;
	}

	// gets the available Entry list.
	public ArrayList<String> getAvailableEntries_viaScroll(String columnName) throws Exception {
		// Set doesn't allow duplicates, so we'll add them into the Set first
		// and then add the set into the ArrayList at the end.
		Set<String> entryList = new LinkedHashSet<>();
		ArrayList<String> entryList_Array = new ArrayList<String>();
		Integer columnNumber = get_Column_Number_ByColumnName(columnName);

		Boolean continueCycle = true;
		WebElement newLastEntry = null;
		while (continueCycle) {
			ArrayList<WebElement> entryElementList = new ArrayList<WebElement>();
			entryElementList.addAll(columns_Elements_List(columnName));
			entryList.addAll(columns_Texts_List(entryElementList));
			WebElement lastAvailableEntry = Locator.lookupElement("Pagin_last_available_Value", gridID,
					columnNumber.toString());
			if (!lastAvailableEntry.equals(newLastEntry)) {
				BaseUI.scroll_to_element(lastAvailableEntry);
			} else {
				break;
			}
			newLastEntry = lastAvailableEntry;

		}
		entryList_Array.addAll(entryList);

		return entryList_Array;
	}

	// scroll to top of list
	public void scroll_ToTopOfList() throws Exception {

		WebElement elementToScrollUp = Locator.lookupElement("Pagin_Grid_ToScrollThrough", gridID, null);
		for (int i = 0; i < 11; i++) {
			elementToScrollUp.sendKeys(Keys.PAGE_UP);
			Thread.sleep(25);
		}

		// BaseUI.scroll_to_element(Locator.lookupElement("Pagin_Grid_ToScrollThrough",
		// gridID, null));

	}

	// Verify Show All option works as expected. Need over 100 entries.
	public void verify_showAllOption(String columnName) throws Exception {
		show_10();
		show_All();
		if (Browser.currentBrowser.equals("internetexplorer")) {
			scroll_ToTopOfList();
		}

		ArrayList<String> allEntries = getAvailableEntries_viaScroll(columnName);
		BaseUI.verify_true_AndLog(allEntries.size() > 100, "Entry list was greater than 100.",
				"Entry list was less than 100.");

		Integer expectedRowCount = get_row_Count();
		BaseUI.verify_true_AndLog(expectedRowCount == allEntries.size(),
				"Row count matched expected value for Show All option.", MessageFormat.format(
						"Row count was {0} did NOT match expected value of {1}.", allEntries.size(), expectedRowCount));

		verifyNextButtonDisabled();
		verifyPreviousButtonDisabled();
		verifyFirstButtonDisabled();
		verifyLastButtonDisabled();
	}

	// Verify Show 10 option works as expected.
	public void verify_show10Option(String columnName) throws Exception {
		show_10();
		ArrayList<String> allEntries = getAvailableEntries_viaScroll(columnName);
		BaseUI.verify_true_AndLog(allEntries.size() == 10, "Entry list has a size of 10.",
				"Entry list size was NOT 10.");

	}

	public void verify_navigation_lists_DONT_Match(String columnName) throws Exception {
		ArrayList<String> initialEntries = getAvailableEntries_viaScroll(columnName);

		click_next();
		ArrayList<String> nextEntryList = getAvailableEntries_viaScroll(columnName);
		verifyPagesListsDontMatch(initialEntries, nextEntryList);

		click_first();
		ArrayList<String> firstEntryList = getAvailableEntries_viaScroll(columnName);
		verifyPagesListsDontMatch(nextEntryList, firstEntryList);

		click_last();
		ArrayList<String> lastEntryList = getAvailableEntries_viaScroll(columnName);
		verifyPagesListsDontMatch(firstEntryList, lastEntryList);

		click_previous();
		ArrayList<String> previousEntryList = getAvailableEntries_viaScroll(columnName);
		verifyPagesListsDontMatch(lastEntryList, previousEntryList);
	}

	// Verify Show Auto option works as expected.
	public void verify_showAutoOption(String columnName) throws Exception {
		show_10();
		ArrayList<String> entriesForShow10 = getAvailableEntries_viaScroll(columnName);

		show_Auto();
		ArrayList<String> allEntries = getAvailableEntries_viaScroll(columnName);
		BaseUI.verify_true_AndLog(allEntries.size() > 10 && allEntries.size() < 25, "Entry list was > 10 and < 25.",
				"Entry list size was NOT between 10-25.");

		BaseUI.verify_true_AndLog(entriesForShow10.size() != allEntries.size(),
				"Entry list from Show 10 does not match Entry list from Show Auto.",
				"Entry list from Show 10 matched Entry list from Show Auto.");
	}

	// Verify Show 25 option works as expected.
	public void verify_show25Option(String columnName) throws Exception {
		show_25();
		if (!Browser.currentBrowser.equals("chrome")) {
			scroll_ToTopOfList();
		}
		ArrayList<String> allEntries = getAvailableEntries_viaScroll(columnName);
		BaseUI.verify_true_AndLog(allEntries.size() == 25, "Entry list has a size of 25.",
				"Entry list size was NOT 25.");
	}

	// Verify Show 50 option works as expected.
	public void verify_show50Option(String columnName) throws Exception {
		show_10();
		show_50();
		if (!Browser.currentBrowser.equals("chrome")) {
			scroll_ToTopOfList();
		}
		ArrayList<String> allEntries = getAvailableEntries_viaScroll(columnName);
		BaseUI.verify_true_AndLog(allEntries.size() == 50, "Entry list has a size of 50.",
				MessageFormat.format("Entry list size was NOT 50. Seeing {0}.", allEntries.size()));
	}

	// Verify Show 100 option works as expected.
	public void verify_show100Option(String columnName) throws Exception {
		show_10();
		show_100();
		if (!Browser.currentBrowser.equals("chrome")) {
			scroll_ToTopOfList();
		}
		ArrayList<String> allEntries = getAvailableEntries_viaScroll(columnName);
		BaseUI.verify_true_AndLog(allEntries.size() == 100, "Entry list has a size of 100.",
				MessageFormat.format("Entry list size was NOT 100. Seeing {0}.", allEntries.size()));
	}

	// Takes the top XX number of entries based on the passed in int.
	public ArrayList<String> getTopEntries(String columnName, Integer numberOfEntries) throws Exception {
		show_10();
		WebElement nextButton = null;
		ArrayList<String> entryList = new ArrayList<String>();

		int maxCount = 0;
		Boolean continueCycle = true;
		while (continueCycle) {
			if (entryList.size() >= numberOfEntries) {
				break;
			}
			nextButton = Locator.lookupElement("Pagin_btn_gridNavigate_next", gridID, null);
			if (BaseUI.elementHasPartialExpectedAttribute(nextButton, "class", "ui-state-disabled")) {
				continueCycle = false;
			}

			ArrayList<WebElement> entryElementList = new ArrayList<WebElement>();
			entryElementList.addAll(columns_Elements_List(columnName));
			entryList.addAll(columns_Texts_List(entryElementList));

			// elementHasExpectedAttribute

			if (continueCycle) {
				BaseUI.click(nextButton);
				Thread.sleep(100);
			}

			maxCount++;
		}

		ArrayList<String> newList = new ArrayList<String>(entryList.subList(0, numberOfEntries));

		click_first();

		return newList;
	}

	public WebElement get_Cell_ByHeaderName_AndRowIdentifyingText(String headerName, String rowIdentifyingText) {
		String xpath = "//div[contains(@class,'ui-widget-content slick-row')][./div[text()='{0}']]/div[count(ancestor::div[@class='slick-viewport']/preceding::div[contains(@class,'slick-header')]//div[./span[text()='{1}']]/preceding::div[contains(@id,'slickgrid')])+1]";
		xpath = xpath.replace("'", "''");
		WebElement table = Browser.driver.findElement(By.id(gridID));
		WebElement cellToReturn = table
				.findElement(By.xpath(MessageFormat.format(xpath, rowIdentifyingText, headerName)));

		return cellToReturn;
	}

	public String get_CellText_ByHeaderName_AndRowIdentifyingText(String headerName, String rowIdentifyingText) {
		WebElement cell = get_Cell_ByHeaderName_AndRowIdentifyingText(headerName, rowIdentifyingText);

		return cell.getText();
	}

	// Verify that the count of rows is greater than the passed in Integer.
	public void verifyCountOfEntries(Integer count) throws Exception {
		BaseUI.verify_true_AndLog(count.equals(get_row_Count()), "Row Count was correct", MessageFormat
				.format("Expected {0} entries, but was {1} entries.", count.toString(), get_row_Count().toString()));
	}

	// Verify that the count of rows is greater than the passed in Integer.
	// Counts the List entries sent over.
	public void verifyCountOfEntries(Integer count, ArrayList<String> entryList) throws Exception {
		BaseUI.verify_true_AndLog(count.equals(entryList.size()), "Row Count was correct", MessageFormat
				.format("Expected {0} entries, but was {1} entries.", count.toString(), get_row_Count().toString()));
	}

	// Verify that the visible count of rows is greater than the passed in
	// Integer.
	public void verifyVisibleCountOfEntries(Integer count) throws Exception {
		ArrayList<WebElement> entryList = new ArrayList<WebElement>();
		entryList.addAll(Locator.lookup_multipleElements("Pagin_list_AllRows", gridID, null));

		BaseUI.verify_true_AndLog(count.equals(entryList.size()), "Row Count was correct",
				MessageFormat.format("Expected {0} entries, but was {1} entries.", count.toString(), entryList.size()));
	}

	public Integer get_visible_entry_Count() {
		ArrayList<WebElement> entryList = new ArrayList<WebElement>();
		entryList.addAll(Locator.lookup_multipleElements("Pagin_list_AllRows", gridID, null));

		return entryList.size();
	}

	public void verifyEntryExists_OR_DoesntExist(String columnName, String columnText, Boolean elementShouldExist) {
		ArrayList<WebElement> columnElements = new ArrayList<WebElement>();
		columnElements.addAll(columns_Elements_List(columnName));

		WebElement editButton = null;
		Boolean elementExists = false;

		for (WebElement correctName : columnElements) {
			if (correctName.getText().equals(columnText)) {
				elementExists = true;
			}
		}

		if (elementShouldExist) {
			BaseUI.verify_true_AndLog(elementExists,
					MessageFormat.format("The entry for Column {0} with text {1} was found.", columnName, columnText),
					MessageFormat.format("The entry for Column {0} with text {1} was NOT found.", columnName,
							columnText));
		} else {
			BaseUI.verify_false_AndLog(elementExists,
					MessageFormat.format("The entry for Column {0} with text {1} was NOT found.", columnName,
							columnText),
					MessageFormat.format("The entry for Column {0} with text {1} was found.", columnName, columnText));
		}

	}

	// use this to verify numeric columns are in the proper order
	public void verifyColumnDescending_alphabetical(String columnName) {
		verifyColumnDescending(columnName);

		ArrayList<WebElement> values_list = columns_Elements_List(columnName);
		ArrayList<String> values_string_list = new ArrayList<String>();
		ArrayList<String> values_string_list_sort = new ArrayList<String>();

		for (WebElement value_element : values_list) {
			values_string_list.add(value_element.getText());
			values_string_list_sort.add(value_element.getText());
		}

		Collections.sort(values_string_list_sort, Collections.reverseOrder(String.CASE_INSENSITIVE_ORDER));

		BaseUI.verify_true_AndLog(values_string_list.size() > 0, "Values found in list.", "No Values found in list.");

		// Verify that the sorted list matches the unsorted list.
		for (int i = 0; i < values_string_list.size(); i++) {
			BaseUI.verify_true_AndLog(values_string_list.get(i).equals(values_string_list_sort.get(i)),
					MessageFormat.format("Value: {0} matched Value: {1}", values_string_list.get(i),
							values_string_list_sort.get(i)),
					MessageFormat.format("Value: {0} did NOT match Value: {1}", values_string_list.get(i),
							values_string_list_sort.get(i)));
		}

	}

	public void verifyColumn_TextInList_ALLValues_ContainText(String columnName, String columnTextToValidate) {
		ArrayList<WebElement> values_list = columns_Elements_List(columnName);

		BaseUI.verify_true_AndLog(values_list.size() > 0,
				MessageFormat.format("Values found in column: {0}", columnName),
				MessageFormat.format("Could not find any text in column: {0}.", columnName));

		for (WebElement text_Element : values_list) {
			BaseUI.verifyElementHasExpectedPartialTextByElementIgnoreCase(text_Element, columnTextToValidate);
		}
	}

	public void verifyColumn_NoResultsFound(String columnName) {
		ArrayList<WebElement> values_list = columns_Elements_List(columnName);

		BaseUI.assertThat("ColumnElementLists", values_list, Is.emptyCollection());
	}

	public void verifyRows_AllCheckboxes_Checked() {
		ArrayList<WebElement> checkboxesList = new ArrayList<WebElement>();
		checkboxesList.addAll(Locator.lookup_multipleElements("Pagin_all_checkboxes", gridID, null));

		BaseUI.verify_true_AndLog(checkboxesList.size() > 0, "Checkbox List found.", "Could not find checkboxes.");

		for (WebElement checkbox : checkboxesList) {
			BaseUI.verifyCheckboxStatus(checkbox, true);
		}
	}

	public void click_sort_ByColumnName(String columnName) throws Exception {
		// WebElement header_byColumn =
		// Locator.lookupElement("Pagin_header_sort_icon", gridID, columnName);
		BaseUI.click_js(Locator.lookupElement("Pagin_header_byText", gridID, columnName));
		Thread.sleep(150);
	}

	public void sort_Column_Ascending(String columnName) throws Exception {
		for (int i = 0; i < 4; i++) {
			WebElement header_Icon_Element = Locator.lookupElement("Pagin_header_sort_icon", gridID, columnName);
			String header_Icon_Class = header_Icon_Element.getAttribute("class");
			if (header_Icon_Class.equals("slick-sort-indicator slick-sort-indicator-asc")) {
				// if it's sorted ascending break out of the loop.
				i = 4;
			} else {
				click_sort_ByColumnName(columnName);
				Thread.sleep(100);
			}
		}
		Thread.sleep(400);
	}

	public void sort_Column_Descending(String columnName) throws Exception {
		for (int i = 0; i < 4; i++) {
			WebElement header_Icon_Element = Locator.lookupElement("Pagin_header_sort_icon", gridID, columnName);
			String header_Icon_Class = header_Icon_Element.getAttribute("class");
			if (header_Icon_Class.equals("slick-sort-indicator slick-sort-indicator-desc")) {
				// if it's sorted descending break out of the loop.
				i = 4;
			} else {
				click_sort_ByColumnName(columnName);
			}
		}
		Thread.sleep(400);
	}

	// clicks the lightbulb and then clicks the All link to show all of the
	// rows.
	public void show_All() throws Exception {
		Navigation.close_ToastError_If_ItAppears();
		if (!Locator.lookupElement("Pagin_show_All_link", gridID, null).isDisplayed()) {
			BaseUI.click(Locator.lookupElement("Pagin_lightBulb_icon", gridID, null));
			Thread.sleep(500);
			if (Browser.currentBrowser.equals("internetexplorer")) {
				Thread.sleep(500);
			}
		}
		BaseUI.click(Locator.lookupElement("Pagin_show_All_link", gridID, null));
		Thread.sleep(1000);
	}

	public void show_Auto() throws Exception {
		Navigation.close_ToastError_If_ItAppears();
		if (!Locator.lookupElement("Pagin_show_All_link", gridID, null).isDisplayed()) {
			BaseUI.click(Locator.lookupElement("Pagin_lightBulb_icon", gridID, null));
			Thread.sleep(500);
		}
		BaseUI.click(Locator.lookupElement("Pagin_show_Auto_link", gridID, null));
		Thread.sleep(1000);
	}

	public void show_10() throws Exception {
		Navigation.close_ToastError_If_ItAppears();
		if (!BaseUI.elementAppears(Locator.lookupElement("Pagin_show_All_link", gridID, null))) {

			BaseUI.click(Locator.lookupElement("Pagin_lightBulb_icon", gridID, null));
			Thread.sleep(500);
		}
		BaseUI.click(Locator.lookupElement("Pagin_show_10_link", gridID, null));
		Thread.sleep(1000);
	}

	public void show_25() throws Exception {
		Navigation.close_ToastError_If_ItAppears();
		if (!Locator.lookupElement("Pagin_show_All_link", gridID, null).isDisplayed()) {
			BaseUI.click(Locator.lookupElement("Pagin_lightBulb_icon", gridID, null));
			Thread.sleep(500);
		}
		BaseUI.click(Locator.lookupElement("Pagin_show_25_link", gridID, null));
		Thread.sleep(1000);
	}

	public void show_50() throws Exception {
		Navigation.close_ToastError_If_ItAppears();
		if (!Locator.lookupElement("Pagin_show_All_link", gridID, null).isDisplayed()) {
			BaseUI.click(Locator.lookupElement("Pagin_lightBulb_icon", gridID, null));
			Thread.sleep(500);
		}
		BaseUI.click(Locator.lookupElement("Pagin_show_50_link", gridID, null));
		Thread.sleep(1000);
	}

	public void show_100() throws Exception {
		Navigation.close_ToastError_If_ItAppears();
		if (!Locator.lookupElement("Pagin_show_All_link", gridID, null).isDisplayed()) {
			BaseUI.click(Locator.lookupElement("Pagin_lightBulb_icon", gridID, null));
			Thread.sleep(500);
		}
		BaseUI.click(Locator.lookupElement("Pagin_show_100_link", gridID, null));
		Thread.sleep(1000);
	}

	public void check_all_checkbox() throws Exception {
		BaseUI.checkCheckbox(Locator.lookupElement("Pagin_header_checkbox", gridID, null));
		// BaseUI.click(Locator.lookupElement("Pagin_header_checkbox", gridID,
		// null));
		Thread.sleep(700);

	}

	public void uncheck_all_checkbox() throws Exception {
		BaseUI.uncheckCheckbox(Locator.lookupElement("Pagin_header_checkbox", gridID, null));
		// BaseUI.click(Locator.lookupElement("Pagin_header_checkbox", gridID,
		// null));
		Thread.sleep(300);

	}

	public WebElement return_AllCheckbox() {
		return Locator.lookupElement("Pagin_header_checkbox", gridID, null);
	}

	public void check_Checkbox_ForColumnName_AndText(String columnName, String columnText) throws Exception {
		WebElement checkBox = check_box_for_GivenColumnAndText(columnName, columnText);
		BaseUI.checkCheckbox(checkBox);
	}

	public void uncheck_Checkbox_ForColumnName_AndText(String columnName, String columnText) throws Exception {
		WebElement checkBox = check_box_for_GivenColumnAndText(columnName, columnText);
		BaseUI.uncheckCheckbox(checkBox);
	}

	public WebElement check_box_for_GivenColumnAndText(String columnName, String columnText) throws Exception {
		ArrayList<WebElement> columnElements = new ArrayList<WebElement>();
		columnElements.addAll(columns_Elements_List(columnName));

		WebElement checkbox = null;

		for (WebElement columnElement : columnElements) {
			String columnElementText = columnElement.getText();
			if (columnElementText.equals(columnText)) {
				checkbox = columnElement.findElement(By.xpath(
						"./parent::div[contains(@class, 'ui-widget-content slick-row')][1]//input[@type='checkbox']"));
				break;
			}
		}

		return checkbox;
	}

	public void click_next() throws Exception {
		BaseUI.click(Locator.lookupElement("Pagin_btn_gridNavigate_next", gridID, null));
		Thread.sleep(1000);
	}

	public void click_previous() throws Exception {
		BaseUI.click(Locator.lookupElement("Pagin_btn_gridNavigate_prev", gridID, null));
		Thread.sleep(1000);
	}

	public void click_first() throws Exception {
		BaseUI.click(Locator.lookupElement("Pagin_btn_gridNavigate_first", gridID, null));
		Thread.sleep(1000);
	}

	public void click_last() throws Exception {
		BaseUI.click(Locator.lookupElement("Pagin_btn_gridNavigate_last", gridID, null));
		Thread.sleep(1000);
	}

	public WebElement firstPageButton() {
		return Locator.lookupElement("Pagin_btn_gridNavigate_first", gridID, null);
	}

}
