package wfsCommon.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import r360.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

//Advanced Search page dropdown.
public class FilterExpander {

	public String filterID = "";

	public FilterExpander(String id) {
		filterID = id;
	}

	public void open_List() throws Exception {
		BaseUI.click(Locator.lookupElement("fltr_expander_Open", filterID, null));
		Thread.sleep(800);
		if (Browser.currentBrowser.equals("internetexplorer")) {
			Thread.sleep(500);
		}
	}

	public void close_List() throws Exception {

		WebElement textbox = Locator.lookupElement("fltr_expander_searchTxtBox");

		if (textbox != null && BaseUI.elementAppears(textbox)) {
			BaseUI.enterKey(Locator.lookupElement("fltr_expander_searchTxtBox"), Keys.ESCAPE);
			Thread.sleep(500);
		}

		BaseUI.click(Locator.lookupElement("fltr_expander_Open", filterID, null));
		Thread.sleep(1250);

		Navigation.close_ToastError_If_ItAppears();
		// if (BaseUI.pageSourceContainsString("toast-item-close")) {
		// BaseUI.click(Locator.lookupElement("toast_CloseButton"));
		// Thread.sleep(1000);
		// }

	}

	public void open_Search() throws Exception {
		if (BaseUI.elementAppears(Locator.lookupElement("fltr_expander_OpenSearch", filterID, null))) {
			BaseUI.click(Locator.lookupElement("fltr_expander_OpenSearch", filterID, null));
			Thread.sleep(300);
			BaseUI.waitForElementToBeDisplayed("fltr_expander_searchTxtBox", null, null);
		}
	}

	public void verify_CurrentText(String expectedText) {
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("fltr_expander_Text", filterID, null), expectedText);
	}

	public void click_Reset() throws Exception {

		open_List();
		BaseUI.click(Locator.lookupElement("fltr_expander_ResetButton"));
		Thread.sleep(500);

	}

	public void search_forText(String textToSearchFor) throws Exception {
		open_List();

		if (BaseUI.elementAppears(Locator.lookupElement("fltr_expander_ResetButton"))) {
			BaseUI.click(Locator.lookupElement("fltr_expander_ResetButton"));
			Thread.sleep(500);
			// open_Search();
		}

		open_Search();
		// if
		// (BaseUI.elementAppears(Locator.lookupElement("fltr_expander_ResetButton")))
		// {
		// BaseUI.click(Locator.lookupElement("fltr_expander_ResetButton"));
		// Thread.sleep(500);
		// }

		BaseUI.enterText(Locator.lookupElement("fltr_expander_searchTxtBox"), textToSearchFor);
		BaseUI.waitForElementToBeDisplayed("fltr_expander_searchResult", null, null);
		Thread.sleep(100);
		BaseUI.scroll_to_element(Locator.lookupElement("fltr_expander_searchResult"));
		BaseUI.click(Locator.lookupElement("fltr_expander_searchResult"));
		Thread.sleep(1000);

		Navigation.wait_for_LoadingSpinner_ToDisappear();
	}

	public void verify_NoResults(String textToSearchFor) throws Exception {
		open_List();
		open_Search();
		BaseUI.enterText(Locator.lookupElement("fltr_expander_searchTxtBox"), textToSearchFor);
		// Thread.sleep(750);
		BaseUI.waitForElementToBeDisplayed("fltr_expander_NoResults", null, null);
		BaseUI.verifyElementAppears(Locator.lookupElement("fltr_expander_NoResults"));

	}

	public String return_SelectedText() throws Exception {

		return BaseUI.getTextFromField(Locator.lookupElement("fltr_expander_Text", filterID, null));
	}

}
