package wfsCommon.pages;

import java.text.MessageFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class Dropdown {

	private String dropdownID;
	private String repositoryIdentifier;
	private String variable1;
	private String variable2;

	public Dropdown(String DropdownID) {
		dropdownID = DropdownID;
	}// div[@id='tabbase']//div[@class='select2-container
		// groupingSelectSummary']

	public Dropdown(String RepositoryIdentifier, String Variable1, String Variable2) {
		repositoryIdentifier = RepositoryIdentifier;
		variable1 = Variable1;
		variable2 = Variable2;
	}

	public WebElement dropdown() {

		if (dropdownID != null) {
			return Locator.lookupElement("dropdwn_dropdown", dropdownID, null);
		} else {
			return Locator.lookupElement(repositoryIdentifier, variable1, variable2);
		}
	}

	public void reset_Dropdown() throws Exception {
		if (dropdownID != null) {
			BaseUI.click(Locator.lookupElement("dropdwn_Dropdown_ClearButton", dropdownID, null));
		} else {
			BaseUI.click(dropdown().findElement(By.xpath(".//abbr[@class='select2-search-choice-close']")));
		}
		Thread.sleep(750);
	}

	public void close_Dropdown() throws Exception {

		BaseUI.click(Locator.lookupElement("dropdwn_CloseDropdown"));
		Thread.sleep(500);
	}

	public void verify_SelectedDropdownValue(String expectedValue) {
		WebElement dropdownSelectedValue = null;
		String elementText_forOutputMessage;
		if (dropdownID != null) {
			dropdownSelectedValue = Locator.lookupElement("dropdwn_selectedValue", dropdownID, null);
			elementText_forOutputMessage = dropdownID;
		} else {
			dropdownSelectedValue = dropdown().findElement(By.xpath(".//span[@class='select2-chosen']"));
			elementText_forOutputMessage = repositoryIdentifier;
		}
		String selectedText = BaseUI.getTextFromField(dropdownSelectedValue);
		BaseUI.baseStringCompare(MessageFormat.format("Dropdown {0} Selected Value", elementText_forOutputMessage), expectedValue, selectedText);
		BaseUI.verifyElementAppears(dropdownSelectedValue);
	}

	public String return_EntityValue() {
		WebElement dropdownSelectedValue = null;
		if (dropdownID != null) {
			dropdownSelectedValue = Locator.lookupElement("dropdwn_selectedValue", dropdownID, null);
		} else {
			dropdownSelectedValue = dropdown().findElement(By.xpath(".//span[@class='select2-chosen']"));
		}
		String selectedText = BaseUI.getTextFromField(dropdownSelectedValue);

		return selectedText;
	}

	public void select_EntityValue(String valueToSelect) throws Exception {
		WebElement dropdownSelectedValue = null;
		if (dropdownID != null) {
			dropdownSelectedValue = Locator.lookupElement("dropdwn_selectedValue", dropdownID, null);
		} else {
			dropdownSelectedValue = dropdown().findElement(By.xpath(".//span[@class='select2-chosen']"));
		}

		BaseUI.click(dropdownSelectedValue);

		Thread.sleep(400);
		BaseUI.click(Locator.lookupElement("dropdwn_ListItem_ByText", valueToSelect, null));
		Thread.sleep(400);

	}

	public void verify_Dropdown_HasFocus() throws Exception {
		WebElement focusedElement = Browser.driver.switchTo().activeElement();

		try {
			BaseUI.enterKey(focusedElement, Keys.RETURN);
			Thread.sleep(300);
			BaseUI.verifyElementAppears(Locator.lookupElement("dropdwn_ResultsList"));
		} finally {

			close_Dropdown();
		}
	}

	public void enterText_IntoSearchTextbox(String textToEnter) throws Exception {
		WebElement dropdownSelectedValue = null;
		if (dropdownID != null) {
			dropdownSelectedValue = Locator.lookupElement("dropdwn_selectedValue", dropdownID, null);
		} else {
			dropdownSelectedValue = dropdown().findElement(By.xpath(".//span[@class='select2-chosen']"));
		}

		BaseUI.click(dropdownSelectedValue);

		Thread.sleep(300);

		WebElement searchBox = Locator.lookupElement("dropdwn_SearchBox");
		BaseUI.enterText_IntoInputBox(searchBox, textToEnter);
		Thread.sleep(200);

	}

	// Enter text into the search box and verify that No Results were found.
	public void verify_NoResultsFound_ForEnteredText(String textToEnter) throws Exception {
		try {
			enterText_IntoSearchTextbox(textToEnter);

			Thread.sleep(500);
			BaseUI.verifyElementAppearsByString("dropdwn_SearchBox_NoResults");
		} finally {
			close_Dropdown();
		}

	}

}
