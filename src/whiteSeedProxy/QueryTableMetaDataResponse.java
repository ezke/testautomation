
package whiteSeedProxy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryTableMetaDataResult" type="{http://schemas.datacontract.org/2004/07/WhiteSeed}FormTableMetaData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryTableMetaDataResult"
})
@XmlRootElement(name = "QueryTableMetaDataResponse")
public class QueryTableMetaDataResponse {

    @XmlElementRef(name = "QueryTableMetaDataResult", namespace = "http://localhost:9995/Seed", type = JAXBElement.class, required = false)
    protected JAXBElement<FormTableMetaData> queryTableMetaDataResult;

    /**
     * Gets the value of the queryTableMetaDataResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FormTableMetaData }{@code >}
     *     
     */
    public JAXBElement<FormTableMetaData> getQueryTableMetaDataResult() {
        return queryTableMetaDataResult;
    }

    /**
     * Sets the value of the queryTableMetaDataResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FormTableMetaData }{@code >}
     *     
     */
    public void setQueryTableMetaDataResult(JAXBElement<FormTableMetaData> value) {
        this.queryTableMetaDataResult = value;
    }

}
