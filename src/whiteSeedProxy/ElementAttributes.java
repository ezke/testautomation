
package whiteSeedProxy;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElementAttributes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ElementAttributes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Text"/>
 *     &lt;enumeration value="AutomationID"/>
 *     &lt;enumeration value="Bounds"/>
 *     &lt;enumeration value="Type"/>
 *     &lt;enumeration value="Value"/>
 *     &lt;enumeration value="WindowHandle"/>
 *     &lt;enumeration value="Enabled"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ElementAttributes", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed")
@XmlEnum
public enum ElementAttributes {

    @XmlEnumValue("Text")
    TEXT("Text"),
    @XmlEnumValue("AutomationID")
    AUTOMATION_ID("AutomationID"),
    @XmlEnumValue("Bounds")
    BOUNDS("Bounds"),
    @XmlEnumValue("Type")
    TYPE("Type"),
    @XmlEnumValue("Value")
    VALUE("Value"),
    @XmlEnumValue("WindowHandle")
    WINDOW_HANDLE("WindowHandle"),
    @XmlEnumValue("Enabled")
    ENABLED("Enabled");
    private final String value;

    ElementAttributes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ElementAttributes fromValue(String v) {
        for (ElementAttributes c: ElementAttributes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
