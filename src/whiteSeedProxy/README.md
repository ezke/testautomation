# Updating whiteSeedProxy

When you've made changes to the WhiteSeed data contracts and need to update
the Java whiteSeedProxy, here's how to do it:

1. Make sure you're running IntelliJ Ultimate. Web Service importing is not
   available in IntelliJ Community edition.
2. Start the newest WhiteSeedDriver and note its URL.
   ![screenshot](ReadmeImages/WhiteSeedUrl.png)
3. Delete the file `whiteSeedProxy/Seed.wsdl`.
4. In the Project window (Alt+1), select the `whiteSeedProxy` folder. (The
   WebServices plug-in is *very* context-sensitive; the Project window has to
   have focus, and you have to have a folder selected, or it won't show the
   right menu items.)
5. Tools > WebServices > Generate Java Code From Wsdl...
   ![screenshot](ReadmeImages/ImportDialog.png)
6. Set the "Web service wsdl url" to point to the running version of
   WhiteSeedDriver, with `?wsdl` appended to the URL. E.g.,
   `http://localhost:9995/Seed?wsdl`
7. Set "Web Service Platform" to the thing that starts "Glassfish / JAX-WS 2.2
   RI ..."
8. Make sure "Output path" is the `src` directory.
9. Make sure "Package prefix" is `whiteSeedProxy`
10. Click OK.
11. Wait for the "WS Import" spinner (in the center of the status bar) to go
    away.