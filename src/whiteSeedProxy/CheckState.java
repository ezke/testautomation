
package whiteSeedProxy;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CheckState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CheckState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unchecked"/>
 *     &lt;enumeration value="Checked"/>
 *     &lt;enumeration value="Indeterminate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CheckState", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed")
@XmlEnum
public enum CheckState {

    @XmlEnumValue("Unchecked")
    UNCHECKED("Unchecked"),
    @XmlEnumValue("Checked")
    CHECKED("Checked"),
    @XmlEnumValue("Indeterminate")
    INDETERMINATE("Indeterminate");
    private final String value;

    CheckState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckState fromValue(String v) {
        for (CheckState c: CheckState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
