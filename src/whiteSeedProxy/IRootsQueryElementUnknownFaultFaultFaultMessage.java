
package whiteSeedProxy;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "UnknownFault", targetNamespace = "http://schemas.datacontract.org/2004/07/WhiteSeed")
public class IRootsQueryElementUnknownFaultFaultFaultMessage
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private UnknownFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public IRootsQueryElementUnknownFaultFaultFaultMessage(String message, UnknownFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public IRootsQueryElementUnknownFaultFaultFaultMessage(String message, UnknownFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: whiteSeedProxy.UnknownFault
     */
    public UnknownFault getFaultInfo() {
        return faultInfo;
    }

}
