
package whiteSeedProxy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElementInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ElementInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckState" type="{http://schemas.datacontract.org/2004/07/WhiteSeed}CheckState" minOccurs="0"/>
 *         &lt;element name="Enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Height" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsOffscreen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScreenX" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="ScreenY" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="UiAutomationClassName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Visible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WhiteClassName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Width" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="WindowHandle" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementInfo", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", propOrder = {
    "checkState",
    "enabled",
    "height",
    "id",
    "isOffscreen",
    "name",
    "screenX",
    "screenY",
    "uiAutomationClassName",
    "visible",
    "whiteClassName",
    "width",
    "windowHandle"
})
public class ElementInfo {

    @XmlElement(name = "CheckState")
    @XmlSchemaType(name = "string")
    protected CheckState checkState;
    @XmlElement(name = "Enabled")
    protected Boolean enabled;
    @XmlElement(name = "Height")
    protected Double height;
    @XmlElementRef(name = "Id", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> id;
    @XmlElement(name = "IsOffscreen")
    protected Boolean isOffscreen;
    @XmlElementRef(name = "Name", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElement(name = "ScreenX")
    protected Double screenX;
    @XmlElement(name = "ScreenY")
    protected Double screenY;
    @XmlElementRef(name = "UiAutomationClassName", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> uiAutomationClassName;
    @XmlElement(name = "Visible")
    protected Boolean visible;
    @XmlElementRef(name = "WhiteClassName", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whiteClassName;
    @XmlElement(name = "Width")
    protected Double width;
    @XmlElement(name = "WindowHandle")
    protected Long windowHandle;

    /**
     * Gets the value of the checkState property.
     * 
     * @return
     *     possible object is
     *     {@link CheckState }
     *     
     */
    public CheckState getCheckState() {
        return checkState;
    }

    /**
     * Sets the value of the checkState property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckState }
     *     
     */
    public void setCheckState(CheckState value) {
        this.checkState = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setHeight(Double value) {
        this.height = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setId(JAXBElement<String> value) {
        this.id = value;
    }

    /**
     * Gets the value of the isOffscreen property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsOffscreen() {
        return isOffscreen;
    }

    /**
     * Sets the value of the isOffscreen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOffscreen(Boolean value) {
        this.isOffscreen = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the screenX property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getScreenX() {
        return screenX;
    }

    /**
     * Sets the value of the screenX property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setScreenX(Double value) {
        this.screenX = value;
    }

    /**
     * Gets the value of the screenY property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getScreenY() {
        return screenY;
    }

    /**
     * Sets the value of the screenY property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setScreenY(Double value) {
        this.screenY = value;
    }

    /**
     * Gets the value of the uiAutomationClassName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUiAutomationClassName() {
        return uiAutomationClassName;
    }

    /**
     * Sets the value of the uiAutomationClassName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUiAutomationClassName(JAXBElement<String> value) {
        this.uiAutomationClassName = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    /**
     * Gets the value of the whiteClassName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhiteClassName() {
        return whiteClassName;
    }

    /**
     * Sets the value of the whiteClassName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhiteClassName(JAXBElement<String> value) {
        this.whiteClassName = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setWidth(Double value) {
        this.width = value;
    }

    /**
     * Gets the value of the windowHandle property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getWindowHandle() {
        return windowHandle;
    }

    /**
     * Sets the value of the windowHandle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setWindowHandle(Long value) {
        this.windowHandle = value;
    }

}
