
package whiteSeedProxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LaunchAndAttachApplicationResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "launchAndAttachApplicationResult"
})
@XmlRootElement(name = "LaunchAndAttachApplicationResponse")
public class LaunchAndAttachApplicationResponse {

    @XmlElement(name = "LaunchAndAttachApplicationResult")
    protected Integer launchAndAttachApplicationResult;

    /**
     * Gets the value of the launchAndAttachApplicationResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLaunchAndAttachApplicationResult() {
        return launchAndAttachApplicationResult;
    }

    /**
     * Sets the value of the launchAndAttachApplicationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLaunchAndAttachApplicationResult(Integer value) {
        this.launchAndAttachApplicationResult = value;
    }

}
