
package whiteSeedProxy;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the whiteSeedProxy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TimeoutFault_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "TimeoutFault");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _UnknownFault_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "UnknownFault");
    private final static QName _ArrayOfElementInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "ArrayOfElementInfo");
    private final static QName _FormTableMetaData_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "FormTableMetaData");
    private final static QName _ArrayOfstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _NotFoundFault_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "NotFoundFault");
    private final static QName _CheckState_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "CheckState");
    private final static QName _FormElementFilter_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "FormElementFilter");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _ElementActions_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "ElementActions");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _ElementInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "ElementInfo");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ArrayOfFormElementFilter_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "ArrayOfFormElementFilter");
    private final static QName _ElementAttributes_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "ElementAttributes");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _GetElementInfoFilters_QNAME = new QName("http://localhost:9995/Seed", "filters");
    private final static QName _FormElementFilterValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "Value");
    private final static QName _QueryTableMetaDataResponseQueryTableMetaDataResult_QNAME = new QName("http://localhost:9995/Seed", "QueryTableMetaDataResult");
    private final static QName _ElementClearFilter_QNAME = new QName("http://localhost:9995/Seed", "filter");
    private final static QName _PingResponsePingResult_QNAME = new QName("http://localhost:9995/Seed", "PingResult");
    private final static QName _QueryElementResponseQueryElementResult_QNAME = new QName("http://localhost:9995/Seed", "QueryElementResult");
    private final static QName _AttachApplicationAppPath_QNAME = new QName("http://localhost:9995/Seed", "appPath");
    private final static QName _TableCellTypeText_QNAME = new QName("http://localhost:9995/Seed", "text");
    private final static QName _TableCellTypeColumnName_QNAME = new QName("http://localhost:9995/Seed", "columnName");
    private final static QName _UnknownFaultMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "Message");
    private final static QName _LaunchApplicationArgs_QNAME = new QName("http://localhost:9995/Seed", "args");
    private final static QName _GetScreenShotResponseGetScreenShotResult_QNAME = new QName("http://localhost:9995/Seed", "GetScreenShotResult");
    private final static QName _GetElementInfoResponseGetElementInfoResult_QNAME = new QName("http://localhost:9995/Seed", "GetElementInfoResult");
    private final static QName _TypeOnKeyboardWithSpecialKeysKeys_QNAME = new QName("http://localhost:9995/Seed", "keys");
    private final static QName _ListWindowsResponseListWindowsResult_QNAME = new QName("http://localhost:9995/Seed", "ListWindowsResult");
    private final static QName _FormTableMetaDataColumns_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "Columns");
    private final static QName _QueryTableCellResponseQueryTableCellResult_QNAME = new QName("http://localhost:9995/Seed", "QueryTableCellResult");
    private final static QName _ElementInfoWhiteClassName_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "WhiteClassName");
    private final static QName _ElementInfoId_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "Id");
    private final static QName _ElementInfoName_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "Name");
    private final static QName _ElementInfoUiAutomationClassName_QNAME = new QName("http://schemas.datacontract.org/2004/07/WhiteSeed", "UiAutomationClassName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: whiteSeedProxy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SetCurrentWindow }
     * 
     */
    public SetCurrentWindow createSetCurrentWindow() {
        return new SetCurrentWindow();
    }

    /**
     * Create an instance of {@link ArrayOfFormElementFilter }
     * 
     */
    public ArrayOfFormElementFilter createArrayOfFormElementFilter() {
        return new ArrayOfFormElementFilter();
    }

    /**
     * Create an instance of {@link TableCellActionResponse }
     * 
     */
    public TableCellActionResponse createTableCellActionResponse() {
        return new TableCellActionResponse();
    }

    /**
     * Create an instance of {@link ElementClear }
     * 
     */
    public ElementClear createElementClear() {
        return new ElementClear();
    }

    /**
     * Create an instance of {@link QueryTableMetaData }
     * 
     */
    public QueryTableMetaData createQueryTableMetaData() {
        return new QueryTableMetaData();
    }

    /**
     * Create an instance of {@link SetFirstWidowToCurrent }
     * 
     */
    public SetFirstWidowToCurrent createSetFirstWidowToCurrent() {
        return new SetFirstWidowToCurrent();
    }

    /**
     * Create an instance of {@link LaunchAndAttachApplicationResponse }
     * 
     */
    public LaunchAndAttachApplicationResponse createLaunchAndAttachApplicationResponse() {
        return new LaunchAndAttachApplicationResponse();
    }

    /**
     * Create an instance of {@link QueryElementResponse }
     * 
     */
    public QueryElementResponse createQueryElementResponse() {
        return new QueryElementResponse();
    }

    /**
     * Create an instance of {@link GetScreenShotResponse }
     * 
     */
    public GetScreenShotResponse createGetScreenShotResponse() {
        return new GetScreenShotResponse();
    }

    /**
     * Create an instance of {@link PingResponse }
     * 
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link QueryElement }
     * 
     */
    public QueryElement createQueryElement() {
        return new QueryElement();
    }

    /**
     * Create an instance of {@link WaitForElement }
     * 
     */
    public WaitForElement createWaitForElement() {
        return new WaitForElement();
    }

    /**
     * Create an instance of {@link TableCellAction }
     * 
     */
    public TableCellAction createTableCellAction() {
        return new TableCellAction();
    }

    /**
     * Create an instance of {@link SetFirstWidowToCurrentResponse }
     * 
     */
    public SetFirstWidowToCurrentResponse createSetFirstWidowToCurrentResponse() {
        return new SetFirstWidowToCurrentResponse();
    }

    /**
     * Create an instance of {@link GetElementInfoResponse }
     * 
     */
    public GetElementInfoResponse createGetElementInfoResponse() {
        return new GetElementInfoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfElementInfo }
     * 
     */
    public ArrayOfElementInfo createArrayOfElementInfo() {
        return new ArrayOfElementInfo();
    }

    /**
     * Create an instance of {@link LaunchApplication }
     * 
     */
    public LaunchApplication createLaunchApplication() {
        return new LaunchApplication();
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link ListWindows }
     * 
     */
    public ListWindows createListWindows() {
        return new ListWindows();
    }

    /**
     * Create an instance of {@link ElementAction }
     * 
     */
    public ElementAction createElementAction() {
        return new ElementAction();
    }

    /**
     * Create an instance of {@link TableCellTypeResponse }
     * 
     */
    public TableCellTypeResponse createTableCellTypeResponse() {
        return new TableCellTypeResponse();
    }

    /**
     * Create an instance of {@link ElementActionResponse }
     * 
     */
    public ElementActionResponse createElementActionResponse() {
        return new ElementActionResponse();
    }

    /**
     * Create an instance of {@link QueryTableCell }
     * 
     */
    public QueryTableCell createQueryTableCell() {
        return new QueryTableCell();
    }

    /**
     * Create an instance of {@link ElementType }
     * 
     */
    public ElementType createElementType() {
        return new ElementType();
    }

    /**
     * Create an instance of {@link GetScreenShot }
     * 
     */
    public GetScreenShot createGetScreenShot() {
        return new GetScreenShot();
    }

    /**
     * Create an instance of {@link ElementTypeWithSpecialKeys }
     * 
     */
    public ElementTypeWithSpecialKeys createElementTypeWithSpecialKeys() {
        return new ElementTypeWithSpecialKeys();
    }

    /**
     * Create an instance of {@link ElementTypeWithSpecialKeysResponse }
     * 
     */
    public ElementTypeWithSpecialKeysResponse createElementTypeWithSpecialKeysResponse() {
        return new ElementTypeWithSpecialKeysResponse();
    }

    /**
     * Create an instance of {@link QueryTableCellResponse }
     * 
     */
    public QueryTableCellResponse createQueryTableCellResponse() {
        return new QueryTableCellResponse();
    }

    /**
     * Create an instance of {@link SetCurrentWindowResponse }
     * 
     */
    public SetCurrentWindowResponse createSetCurrentWindowResponse() {
        return new SetCurrentWindowResponse();
    }

    /**
     * Create an instance of {@link QueryTableMetaDataResponse }
     * 
     */
    public QueryTableMetaDataResponse createQueryTableMetaDataResponse() {
        return new QueryTableMetaDataResponse();
    }

    /**
     * Create an instance of {@link FormTableMetaData }
     * 
     */
    public FormTableMetaData createFormTableMetaData() {
        return new FormTableMetaData();
    }

    /**
     * Create an instance of {@link WaitForElementResponse }
     * 
     */
    public WaitForElementResponse createWaitForElementResponse() {
        return new WaitForElementResponse();
    }

    /**
     * Create an instance of {@link TypeOnKeyboardResponse }
     * 
     */
    public TypeOnKeyboardResponse createTypeOnKeyboardResponse() {
        return new TypeOnKeyboardResponse();
    }

    /**
     * Create an instance of {@link CloseApplicationResponse }
     * 
     */
    public CloseApplicationResponse createCloseApplicationResponse() {
        return new CloseApplicationResponse();
    }

    /**
     * Create an instance of {@link ListWindowsResponse }
     * 
     */
    public ListWindowsResponse createListWindowsResponse() {
        return new ListWindowsResponse();
    }

    /**
     * Create an instance of {@link CloseApplication }
     * 
     */
    public CloseApplication createCloseApplication() {
        return new CloseApplication();
    }

    /**
     * Create an instance of {@link LaunchApplicationResponse }
     * 
     */
    public LaunchApplicationResponse createLaunchApplicationResponse() {
        return new LaunchApplicationResponse();
    }

    /**
     * Create an instance of {@link TypeOnKeyboardWithSpecialKeys }
     * 
     */
    public TypeOnKeyboardWithSpecialKeys createTypeOnKeyboardWithSpecialKeys() {
        return new TypeOnKeyboardWithSpecialKeys();
    }

    /**
     * Create an instance of {@link TableCellType }
     * 
     */
    public TableCellType createTableCellType() {
        return new TableCellType();
    }

    /**
     * Create an instance of {@link AttachApplicationResponse }
     * 
     */
    public AttachApplicationResponse createAttachApplicationResponse() {
        return new AttachApplicationResponse();
    }

    /**
     * Create an instance of {@link LaunchAndAttachApplication }
     * 
     */
    public LaunchAndAttachApplication createLaunchAndAttachApplication() {
        return new LaunchAndAttachApplication();
    }

    /**
     * Create an instance of {@link GetElementInfo }
     * 
     */
    public GetElementInfo createGetElementInfo() {
        return new GetElementInfo();
    }

    /**
     * Create an instance of {@link Ping }
     * 
     */
    public Ping createPing() {
        return new Ping();
    }

    /**
     * Create an instance of {@link AttachApplication }
     * 
     */
    public AttachApplication createAttachApplication() {
        return new AttachApplication();
    }

    /**
     * Create an instance of {@link TypeOnKeyboardWithSpecialKeysResponse }
     * 
     */
    public TypeOnKeyboardWithSpecialKeysResponse createTypeOnKeyboardWithSpecialKeysResponse() {
        return new TypeOnKeyboardWithSpecialKeysResponse();
    }

    /**
     * Create an instance of {@link ElementClearResponse }
     * 
     */
    public ElementClearResponse createElementClearResponse() {
        return new ElementClearResponse();
    }

    /**
     * Create an instance of {@link TypeOnKeyboard }
     * 
     */
    public TypeOnKeyboard createTypeOnKeyboard() {
        return new TypeOnKeyboard();
    }

    /**
     * Create an instance of {@link ElementTypeResponse }
     * 
     */
    public ElementTypeResponse createElementTypeResponse() {
        return new ElementTypeResponse();
    }

    /**
     * Create an instance of {@link TimeoutFault }
     * 
     */
    public TimeoutFault createTimeoutFault() {
        return new TimeoutFault();
    }

    /**
     * Create an instance of {@link ElementInfo }
     * 
     */
    public ElementInfo createElementInfo() {
        return new ElementInfo();
    }

    /**
     * Create an instance of {@link UnknownFault }
     * 
     */
    public UnknownFault createUnknownFault() {
        return new UnknownFault();
    }

    /**
     * Create an instance of {@link FormElementFilter }
     * 
     */
    public FormElementFilter createFormElementFilter() {
        return new FormElementFilter();
    }

    /**
     * Create an instance of {@link NotFoundFault }
     * 
     */
    public NotFoundFault createNotFoundFault() {
        return new NotFoundFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeoutFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "TimeoutFault")
    public JAXBElement<TimeoutFault> createTimeoutFault(TimeoutFault value) {
        return new JAXBElement<TimeoutFault>(_TimeoutFault_QNAME, TimeoutFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnknownFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "UnknownFault")
    public JAXBElement<UnknownFault> createUnknownFault(UnknownFault value) {
        return new JAXBElement<UnknownFault>(_UnknownFault_QNAME, UnknownFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfElementInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "ArrayOfElementInfo")
    public JAXBElement<ArrayOfElementInfo> createArrayOfElementInfo(ArrayOfElementInfo value) {
        return new JAXBElement<ArrayOfElementInfo>(_ArrayOfElementInfo_QNAME, ArrayOfElementInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormTableMetaData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "FormTableMetaData")
    public JAXBElement<FormTableMetaData> createFormTableMetaData(FormTableMetaData value) {
        return new JAXBElement<FormTableMetaData>(_FormTableMetaData_QNAME, FormTableMetaData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotFoundFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "NotFoundFault")
    public JAXBElement<NotFoundFault> createNotFoundFault(NotFoundFault value) {
        return new JAXBElement<NotFoundFault>(_NotFoundFault_QNAME, NotFoundFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "CheckState")
    public JAXBElement<CheckState> createCheckState(CheckState value) {
        return new JAXBElement<CheckState>(_CheckState_QNAME, CheckState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "FormElementFilter")
    public JAXBElement<FormElementFilter> createFormElementFilter(FormElementFilter value) {
        return new JAXBElement<FormElementFilter>(_FormElementFilter_QNAME, FormElementFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElementActions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "ElementActions")
    public JAXBElement<ElementActions> createElementActions(ElementActions value) {
        return new JAXBElement<ElementActions>(_ElementActions_QNAME, ElementActions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElementInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "ElementInfo")
    public JAXBElement<ElementInfo> createElementInfo(ElementInfo value) {
        return new JAXBElement<ElementInfo>(_ElementInfo_QNAME, ElementInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "ArrayOfFormElementFilter")
    public JAXBElement<ArrayOfFormElementFilter> createArrayOfFormElementFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ArrayOfFormElementFilter_QNAME, ArrayOfFormElementFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElementAttributes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "ElementAttributes")
    public JAXBElement<ElementAttributes> createElementAttributes(ElementAttributes value) {
        return new JAXBElement<ElementAttributes>(_ElementAttributes_QNAME, ElementAttributes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filters", scope = GetElementInfo.class)
    public JAXBElement<ArrayOfFormElementFilter> createGetElementInfoFilters(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_GetElementInfoFilters_QNAME, ArrayOfFormElementFilter.class, GetElementInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Value", scope = FormElementFilter.class)
    public JAXBElement<String> createFormElementFilterValue(String value) {
        return new JAXBElement<String>(_FormElementFilterValue_QNAME, String.class, FormElementFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormTableMetaData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "QueryTableMetaDataResult", scope = QueryTableMetaDataResponse.class)
    public JAXBElement<FormTableMetaData> createQueryTableMetaDataResponseQueryTableMetaDataResult(FormTableMetaData value) {
        return new JAXBElement<FormTableMetaData>(_QueryTableMetaDataResponseQueryTableMetaDataResult_QNAME, FormTableMetaData.class, QueryTableMetaDataResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = ElementClear.class)
    public JAXBElement<ArrayOfFormElementFilter> createElementClearFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, ElementClear.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "PingResult", scope = PingResponse.class)
    public JAXBElement<String> createPingResponsePingResult(String value) {
        return new JAXBElement<String>(_PingResponsePingResult_QNAME, String.class, PingResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = QueryElement.class)
    public JAXBElement<ArrayOfFormElementFilter> createQueryElementFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, QueryElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "QueryElementResult", scope = QueryElementResponse.class)
    public JAXBElement<String> createQueryElementResponseQueryElementResult(String value) {
        return new JAXBElement<String>(_QueryElementResponseQueryElementResult_QNAME, String.class, QueryElementResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "appPath", scope = AttachApplication.class)
    public JAXBElement<String> createAttachApplicationAppPath(String value) {
        return new JAXBElement<String>(_AttachApplicationAppPath_QNAME, String.class, AttachApplication.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "text", scope = TableCellType.class)
    public JAXBElement<String> createTableCellTypeText(String value) {
        return new JAXBElement<String>(_TableCellTypeText_QNAME, String.class, TableCellType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "columnName", scope = TableCellType.class)
    public JAXBElement<String> createTableCellTypeColumnName(String value) {
        return new JAXBElement<String>(_TableCellTypeColumnName_QNAME, String.class, TableCellType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = TableCellType.class)
    public JAXBElement<ArrayOfFormElementFilter> createTableCellTypeFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, TableCellType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Message", scope = UnknownFault.class)
    public JAXBElement<String> createUnknownFaultMessage(String value) {
        return new JAXBElement<String>(_UnknownFaultMessage_QNAME, String.class, UnknownFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = ElementAction.class)
    public JAXBElement<ArrayOfFormElementFilter> createElementActionFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, ElementAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filters", scope = WaitForElement.class)
    public JAXBElement<ArrayOfFormElementFilter> createWaitForElementFilters(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_GetElementInfoFilters_QNAME, ArrayOfFormElementFilter.class, WaitForElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "columnName", scope = QueryTableCell.class)
    public JAXBElement<String> createQueryTableCellColumnName(String value) {
        return new JAXBElement<String>(_TableCellTypeColumnName_QNAME, String.class, QueryTableCell.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = QueryTableCell.class)
    public JAXBElement<ArrayOfFormElementFilter> createQueryTableCellFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, QueryTableCell.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = QueryTableMetaData.class)
    public JAXBElement<ArrayOfFormElementFilter> createQueryTableMetaDataFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, QueryTableMetaData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "appPath", scope = LaunchApplication.class)
    public JAXBElement<String> createLaunchApplicationAppPath(String value) {
        return new JAXBElement<String>(_AttachApplicationAppPath_QNAME, String.class, LaunchApplication.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "args", scope = LaunchApplication.class)
    public JAXBElement<ArrayOfstring> createLaunchApplicationArgs(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_LaunchApplicationArgs_QNAME, ArrayOfstring.class, LaunchApplication.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "GetScreenShotResult", scope = GetScreenShotResponse.class)
    public JAXBElement<byte[]> createGetScreenShotResponseGetScreenShotResult(byte[] value) {
        return new JAXBElement<byte[]>(_GetScreenShotResponseGetScreenShotResult_QNAME, byte[].class, GetScreenShotResponse.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Message", scope = TimeoutFault.class)
    public JAXBElement<String> createTimeoutFaultMessage(String value) {
        return new JAXBElement<String>(_UnknownFaultMessage_QNAME, String.class, TimeoutFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfElementInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "GetElementInfoResult", scope = GetElementInfoResponse.class)
    public JAXBElement<ArrayOfElementInfo> createGetElementInfoResponseGetElementInfoResult(ArrayOfElementInfo value) {
        return new JAXBElement<ArrayOfElementInfo>(_GetElementInfoResponseGetElementInfoResult_QNAME, ArrayOfElementInfo.class, GetElementInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "keys", scope = TypeOnKeyboardWithSpecialKeys.class)
    public JAXBElement<String> createTypeOnKeyboardWithSpecialKeysKeys(String value) {
        return new JAXBElement<String>(_TypeOnKeyboardWithSpecialKeysKeys_QNAME, String.class, TypeOnKeyboardWithSpecialKeys.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = SetCurrentWindow.class)
    public JAXBElement<ArrayOfFormElementFilter> createSetCurrentWindowFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, SetCurrentWindow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Message", scope = NotFoundFault.class)
    public JAXBElement<String> createNotFoundFaultMessage(String value) {
        return new JAXBElement<String>(_UnknownFaultMessage_QNAME, String.class, NotFoundFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "text", scope = ElementType.class)
    public JAXBElement<String> createElementTypeText(String value) {
        return new JAXBElement<String>(_TableCellTypeText_QNAME, String.class, ElementType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = ElementType.class)
    public JAXBElement<ArrayOfFormElementFilter> createElementTypeFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, ElementType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "appPath", scope = LaunchAndAttachApplication.class)
    public JAXBElement<String> createLaunchAndAttachApplicationAppPath(String value) {
        return new JAXBElement<String>(_AttachApplicationAppPath_QNAME, String.class, LaunchAndAttachApplication.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "args", scope = LaunchAndAttachApplication.class)
    public JAXBElement<ArrayOfstring> createLaunchAndAttachApplicationArgs(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_LaunchApplicationArgs_QNAME, ArrayOfstring.class, LaunchAndAttachApplication.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "ListWindowsResult", scope = ListWindowsResponse.class)
    public JAXBElement<ArrayOfstring> createListWindowsResponseListWindowsResult(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ListWindowsResponseListWindowsResult_QNAME, ArrayOfstring.class, ListWindowsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Columns", scope = FormTableMetaData.class)
    public JAXBElement<ArrayOfstring> createFormTableMetaDataColumns(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_FormTableMetaDataColumns_QNAME, ArrayOfstring.class, FormTableMetaData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "text", scope = TypeOnKeyboard.class)
    public JAXBElement<String> createTypeOnKeyboardText(String value) {
        return new JAXBElement<String>(_TableCellTypeText_QNAME, String.class, TypeOnKeyboard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "QueryTableCellResult", scope = QueryTableCellResponse.class)
    public JAXBElement<String> createQueryTableCellResponseQueryTableCellResult(String value) {
        return new JAXBElement<String>(_QueryTableCellResponseQueryTableCellResult_QNAME, String.class, QueryTableCellResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "WhiteClassName", scope = ElementInfo.class)
    public JAXBElement<String> createElementInfoWhiteClassName(String value) {
        return new JAXBElement<String>(_ElementInfoWhiteClassName_QNAME, String.class, ElementInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Id", scope = ElementInfo.class)
    public JAXBElement<String> createElementInfoId(String value) {
        return new JAXBElement<String>(_ElementInfoId_QNAME, String.class, ElementInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "Name", scope = ElementInfo.class)
    public JAXBElement<String> createElementInfoName(String value) {
        return new JAXBElement<String>(_ElementInfoName_QNAME, String.class, ElementInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", name = "UiAutomationClassName", scope = ElementInfo.class)
    public JAXBElement<String> createElementInfoUiAutomationClassName(String value) {
        return new JAXBElement<String>(_ElementInfoUiAutomationClassName_QNAME, String.class, ElementInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "columnName", scope = TableCellAction.class)
    public JAXBElement<String> createTableCellActionColumnName(String value) {
        return new JAXBElement<String>(_TableCellTypeColumnName_QNAME, String.class, TableCellAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = TableCellAction.class)
    public JAXBElement<ArrayOfFormElementFilter> createTableCellActionFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, TableCellAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormElementFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "filter", scope = ElementTypeWithSpecialKeys.class)
    public JAXBElement<ArrayOfFormElementFilter> createElementTypeWithSpecialKeysFilter(ArrayOfFormElementFilter value) {
        return new JAXBElement<ArrayOfFormElementFilter>(_ElementClearFilter_QNAME, ArrayOfFormElementFilter.class, ElementTypeWithSpecialKeys.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://localhost:9995/Seed", name = "keys", scope = ElementTypeWithSpecialKeys.class)
    public JAXBElement<String> createElementTypeWithSpecialKeysKeys(String value) {
        return new JAXBElement<String>(_TypeOnKeyboardWithSpecialKeysKeys_QNAME, String.class, ElementTypeWithSpecialKeys.class, value);
    }

}
