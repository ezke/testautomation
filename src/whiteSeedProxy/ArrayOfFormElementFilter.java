
package whiteSeedProxy;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFormElementFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFormElementFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormElementFilter" type="{http://schemas.datacontract.org/2004/07/WhiteSeed}FormElementFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFormElementFilter", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed", propOrder = {
    "formElementFilter"
})
public class ArrayOfFormElementFilter {

    @XmlElement(name = "FormElementFilter", nillable = true)
    protected List<FormElementFilter> formElementFilter;

    /**
     * Gets the value of the formElementFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formElementFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormElementFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormElementFilter }
     * 
     * 
     */
    public List<FormElementFilter> getFormElementFilter() {
        if (formElementFilter == null) {
            formElementFilter = new ArrayList<FormElementFilter>();
        }
        return this.formElementFilter;
    }

}
