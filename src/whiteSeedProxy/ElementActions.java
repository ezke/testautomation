
package whiteSeedProxy;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElementActions.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ElementActions">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Click"/>
 *     &lt;enumeration value="Focus"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ElementActions", namespace = "http://schemas.datacontract.org/2004/07/WhiteSeed")
@XmlEnum
public enum ElementActions {

    @XmlEnumValue("Click")
    CLICK("Click"),
    @XmlEnumValue("Focus")
    FOCUS("Focus");
    private final String value;

    ElementActions(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ElementActions fromValue(String v) {
        for (ElementActions c: ElementActions.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
