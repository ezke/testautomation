
package whiteSeedProxy;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "WhiteSeedService", targetNamespace = "http://tempuri.org/", wsdlLocation = "http://localhost:9995/Seed?wsdl")
public class WhiteSeedService
    extends Service
{

    private final static URL WHITESEEDSERVICE_WSDL_LOCATION;
    private final static WebServiceException WHITESEEDSERVICE_EXCEPTION;
    private final static QName WHITESEEDSERVICE_QNAME = new QName("http://tempuri.org/", "WhiteSeedService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:9995/Seed?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        WHITESEEDSERVICE_WSDL_LOCATION = url;
        WHITESEEDSERVICE_EXCEPTION = e;
    }

    public WhiteSeedService() {
        super(__getWsdlLocation(), WHITESEEDSERVICE_QNAME);
    }

    public WhiteSeedService(WebServiceFeature... features) {
        super(__getWsdlLocation(), WHITESEEDSERVICE_QNAME, features);
    }

    public WhiteSeedService(URL wsdlLocation) {
        super(wsdlLocation, WHITESEEDSERVICE_QNAME);
    }

    public WhiteSeedService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, WHITESEEDSERVICE_QNAME, features);
    }

    public WhiteSeedService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WhiteSeedService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IRoots
     */
    @WebEndpoint(name = "BasicHttpBinding_IRoots")
    public IRoots getBasicHttpBindingIRoots() {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IRoots"), IRoots.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IRoots
     */
    @WebEndpoint(name = "BasicHttpBinding_IRoots")
    public IRoots getBasicHttpBindingIRoots(WebServiceFeature... features) {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IRoots"), IRoots.class, features);
    }

    private static URL __getWsdlLocation() {
        if (WHITESEEDSERVICE_EXCEPTION!= null) {
            throw WHITESEEDSERVICE_EXCEPTION;
        }
        return WHITESEEDSERVICE_WSDL_LOCATION;
    }

}
