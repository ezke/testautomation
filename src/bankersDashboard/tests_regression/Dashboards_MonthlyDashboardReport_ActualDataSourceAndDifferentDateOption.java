package bankersDashboard.tests_regression;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Dashboard_MonthlyDashboardModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.DropDowns;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha
 * 
 *  Test #1445 -
 *	Dashboards: Monthly Dashboard: Data Source - Actual data source and different Date option 
 *  and Reverse Chronological order checkbox checked
 *
 */

public class Dashboards_MonthlyDashboardReport_ActualDataSourceAndDifferentDateOption extends BaseTest {
	
	private static String dataSourceOptionSelected;
	private static String startDateOptionSelected;

	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to Monthly Dashboard Report
		Navigation.navigateToMonthlyDashboard();
		dataSourceOptionSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_Datasource_dropdown");
	}
	
	
	@Test(priority = 1 , groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step1_VerifyDataSourceLabel() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_DatasourceLabel"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboard_DatasourceLabel", "Data Source");
	}
	
	
	@Test(priority = 2, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step1_VerifyStartDateLabel() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_StartDateLabel"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboard_StartDateLabel", "Start Date");
	}
	
	
	@Test(priority = 3, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step1_VerifyReverseChronoLabel() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_ReverseChrono_label"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboard_ReverseChrono_label", "Reverse chronological order");
	}
	
	
	
	@Test(priority = 4, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step1_VerifyReverseChronoCheckboxIsChecked() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_ReverseChronoOrder_checkbox"));
		BaseUI.verifyCheckboxStatus("monthlyDashboard_ReverseChronoOrder_checkbox", "checked");
	}

	
	@Test(priority = 7, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step2To3_VerifyBankLogoOnReport() throws Exception {
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("monthlyDashboard_StartDateOption"), 4);
		startDateOptionSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_StartDateOption");
		Dashboard_MonthlyDashboardModal.clickShowReportBtn();
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 8, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step3_ValidateReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboardReportTitle"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboardReportTitle", "Monthly Dashboard");
	}
	
	
	@Test(priority = 9, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step3_ValidateDataSourceDisplayedOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_dataSourceDisplayedOnReport"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboard_dataSourceDisplayedOnReport", dataSourceOptionSelected);
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "regression_Tests" })
	public static void PT4404_Step3_VerifyDateSelectedDisplayForFirstColumn_ReverseChronoChecked() throws Exception {
		Dashboard_MonthlyDashboardModal.verifyDateSelectedDisplayForFirstColumn_ReverseChronoChecked(startDateOptionSelected);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
   
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	
	  try {
		  StartPage.clickCloseAllTabsButton();
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
