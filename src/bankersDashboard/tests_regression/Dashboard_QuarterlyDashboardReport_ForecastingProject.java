package bankersDashboard.tests_regression;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Dashboard_QuarterlyDashboardModal;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Dashboard_QuarterlyDashboardReport_ForecastingProject extends BaseTest {

	static String forecastingProjName;
	static String newFCProjName;
	static int dateValue = 0;
	static String startDateSelected;

	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Create Forecasting Project
		ForecastingPage.selectExistingFCByIndex(1);
		forecastingProjName = BaseUI.getTextFromField(Locator.lookupElement("forecasting_ExistingForecaseProjName"));
		newFCProjName = ForecastingPage.createCopyOfExistingForecastingProject(forecastingProjName);
		EntitySelector.selectWindow("Banker's Dashboard");
		
		//Navigate to Quarterly Dashboard Report
		Dashboard_QuarterlyDashboardModal.selectDate_DataSourceOption(newFCProjName, dateValue);
		startDateSelected = BaseUI.getSelectedOptionFromDropdown("quarterlyDashboard_StartDateOption");
		Dashboard_QuarterlyDashboardModal.clickShowReportBtn();
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1451_Step3_VerifyBankLogoOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1451_Step3_VerifyReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("quarterlyDashboard_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("quarterlyDashboard_ReportTitle", "Quarterly Dashboard");
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1451_Step3_VerifyDataSourceDisplayedOnReport() throws Exception {
		Dashboard_QuarterlyDashboardModal.verifyDataSourceOnReportPage(newFCProjName);
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1451_Step3_VerifyDateSelectedDisplayForFirstColumn() throws Exception {
		Dashboard_QuarterlyDashboardModal.verifyDateSelectedDisplayForFirstColumn(startDateSelected);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1451_Step3_VerifyLastSixMonthsDateDisplayed_QuarterlyReport_Forecast() throws Exception {
		Dashboard_QuarterlyDashboardModal.verifyDateColumnsDisplayed_MatchesFormat();
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	  
    	try {
    		StartPage.clickCloseAllTabsButton();
    		ForecastingPage.selectExistingFC_SelectFileMenu_DeleteProject(newFCProjName);
	        } finally{
	        	Browser.closeBrowser();
	        	}
	}
}
