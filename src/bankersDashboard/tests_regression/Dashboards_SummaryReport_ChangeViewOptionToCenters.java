package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.SummaryReportPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha
 * 
 *  Test #1445 -
 *	Dashboards: Summary Report: with defaults and View as Centers 
 *
 */

public class Dashboards_SummaryReport_ChangeViewOptionToCenters extends BaseTest {
	
	private static String dateDropdownValue;
	private static String verifyConsolidatedValueDisplayed;
	static String selectedViewOption, profitCenter;

	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to Settings > My Center
		Navigation.navigateToSettings_MyCenters();
		StartPage.clickStartPageTab();
		
		profitCenter = BaseUI.getTextFromField(Locator.lookupElement("summaryWidget_ProfitCenter"));
		
		//Navigate to Summary report
		Navigation.navigateToDashboard_Summary();
		
		//Navigation.navigateToDashboardSummaryReportWithValidation();
		dateDropdownValue = BaseUI.getSelectedOptionFromDropdown("summaryDate_dropdown");

		BaseUI.selectValueFromDropDown(Locator.lookupElement("summaryView_dropdown"), "Centers");
		selectedViewOption = BaseUI.getSelectedOptionFromDropdown("summaryView_dropdown");
		SummaryReportPage.clickRefreshIcon();
	}
	
	@Test
	public static void TC_Step1_VerifyBankLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
		EntitySelector.imageVerification("bankLogo");
	}
	
	
	@Test
	public static void TC_Step1_VerifyReportTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
	
	
	@Test
	public static void TC_Step1_VerifyProfitCenterOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_profitCenterDisplayed"));
		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", profitCenter);
	}
	
	
	@Test
	public static void TC_Step1_VerifyDateOptionSelectedDisplayedOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_dateInColumn1"));
		BaseUI.verifyElementHasExpectedText("summaryReport_dateInColumn1", dateDropdownValue);
	}
	
	
	@Test
	public static void TC_Step2_VerifyFirstCentersOnReport() throws Exception {
		String verifyCenter1ValueDisplayed = BaseUI.getTextFromField(Locator.lookupElement("summaryReport_displayedCenter1"));
		BaseUI.baseStringCompare("summaryReport_displayedCenter1", Navigation.draggedCenter1, verifyCenter1ValueDisplayed);	
	}
	
	
	@Test
	public static void TC_Step3_VerifySecondCenterOnReport() throws Exception {
		String verifyCenter2ValueDisplayed = BaseUI.getTextFromField(Locator.lookupElement("summaryReport_displayedCenter2"));
		BaseUI.scroll_to_element(Locator.lookupElement("summaryReport_displayedCenter2"));
		BaseUI.baseStringCompare("summaryReport_displayedCenter2", Navigation.draggedCenter2, verifyCenter2ValueDisplayed);
	}
	
	
	@Test
	public static void TC_Step4_VerifyDefaultConsolidatedCenterDispalyedOnReport() throws Exception {
	    verifyConsolidatedValueDisplayed = BaseUI.getTextFromField(Locator.lookupElement("summaryReport_ConsolidatedCenter"));
	    BaseUI.baseStringCompare("summaryReport_ConsolidatedCenter", "Consolidated", verifyConsolidatedValueDisplayed);
	}
	
	
	@Test
	public static void TC_Step4_VerifyCentersOptionDisplayedOnReport() throws Exception {
	    BaseUI.verifyElementHasExpectedPartialText("summaryView_dropdown", selectedViewOption);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	 
	  StartPage.clickStartPageTab();
	  StartPage.clickCloseAllTabsButton();
	  
	  try {
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}
