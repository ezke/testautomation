package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Dashboard_QuarterlyDashboardModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Dashboard_QuarterlyDashboardReport_Forecast extends BaseTest {

	private static String datasrc = "Forecast";
	private static int dateValue = 0;
	private static String dataSourceOptionSelected;
	private static String startDateOptionSelected;


	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to Quarterly Dashboard report
		Dashboard_QuarterlyDashboardModal.selectDate_DataSourceOption(datasrc, dateValue);
		dataSourceOptionSelected = BaseUI.getSelectedOptionFromDropdown("quarterlyDashboard_Datasource_dropdown");
		startDateOptionSelected = BaseUI.getSelectedOptionFromDropdown("quarterlyDashboard_StartDateOption");
		Dashboard_QuarterlyDashboardModal.clickShowReportBtn();
	}	

	
	@Test(priority = 20, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1450_Step2_verifyBankLogoOnQuarterlyReport_Forecast() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1450_Step2_ValidateReportTitleOnQuarterlyReport_Forecast() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("quarterlyDashboard_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("quarterlyDashboard_ReportTitle", "Quarterly Dashboard");
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1450_Step2_ValidateDataSourceDisplayedOnQuarterlyReport_Forecast() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("quarterlyDashboard_dataSourceDisplayedOnReport"));
		BaseUI.verifyElementHasExpectedText("quarterlyDashboard_dataSourceDisplayedOnReport", dataSourceOptionSelected);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1450_Step2_VerifyDateSelectedDisplayForFirstColumn() throws Exception {
		Dashboard_QuarterlyDashboardModal.verifyDateSelectedDisplayForFirstColumn(startDateOptionSelected);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1450_Step2_VerifyLastSixMonthsDateDisplayed_QuarterlyReport_Forecast() throws Exception {
		Dashboard_QuarterlyDashboardModal.verifyDateColumnsDisplayed_MatchesFormat();
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
   
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	
    	try {
    		StartPage.clickCloseAllTabsButton();
	        Navigation.navigate_to_signOut();
	        }
    	finally{
    		Browser.closeBrowser();
    		}
    }
}
