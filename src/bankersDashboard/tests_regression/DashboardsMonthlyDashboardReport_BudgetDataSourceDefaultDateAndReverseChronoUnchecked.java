package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Dashboard_MonthlyDashboardModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


/**
 * @author Nisha
 * 
 *  Test #1445 -
 *	Dashboards: Monthly Dashboard: Data Source - Actual and default Date option 
 *  and Reverse Chronological order checkbox unchecked
 *
 */

public class DashboardsMonthlyDashboardReport_BudgetDataSourceDefaultDateAndReverseChronoUnchecked extends BaseTest {

	private static String dataSourceOptionSelected;
	private static String startDateOptionSelected;
	private static String datasrc = "Budget";
	private static int dateValue = 0;



	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		EntitySelector.imageVerification("loginPage_BD_logo");
		
		//Navigate to Monthly Dashboard
		Dashboard_MonthlyDashboardModal.selectDate_DataSourceOption(datasrc, dateValue );
		dataSourceOptionSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_Datasource_dropdown");
		startDateOptionSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_StartDateOption");
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4406_Step2_verifyReverseChronoCheckbox() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_ReverseChronoOrder_checkbox"));
	}
	
	
	@Test (priority = 20, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4406_Step2_verifyReverseChronoCheckboxUnchecked() throws Exception {
		Dashboard_MonthlyDashboardModal.clickReverseChronoCheckbox();
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("monthlyDashboard_ReverseChronoOrder_checkbox"), false);
	}
	
	
	@Test (priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4406_Step3_verifyBankLogoOnReport() throws Exception {
		Dashboard_MonthlyDashboardModal.clickShowReportBtn();
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
		EntitySelector.imageVerification("bankLogo");
	}
	
	
	@Test (priority = 40, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4406_Step4_ValidateReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboardReportTitle"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboardReportTitle", "Monthly Dashboard");
	}
	
	
	@Test (priority = 40, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4406_Step4_ValidateDataSourceDisplayedOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_dataSourceDisplayedOnReport"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboard_dataSourceDisplayedOnReport", dataSourceOptionSelected);
	}
	
	
	@Test (priority = 40, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4406_Step4_VerifyDateSelectedDoesNotDisplayForFirstColumn_ReverseChronoUnchecked() throws Exception {
		Dashboard_MonthlyDashboardModal.verifyDateSelectedDoesNotDisplayForFirstColumn_ReverseChronoUnchecked(startDateOptionSelected);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
   
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	
    	try {
    		StartPage.clickCloseAllTabsButton();
	        Navigation.navigate_to_signOut();
	        } finally {
	        	Browser.closeBrowser();
	        	}
    	}
    }
