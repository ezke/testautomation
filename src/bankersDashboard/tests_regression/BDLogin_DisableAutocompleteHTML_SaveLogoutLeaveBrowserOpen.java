package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.StartPage_User_PreferencesModal;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BDLogin_DisableAutocompleteHTML_SaveLogoutLeaveBrowserOpen extends BaseTest {

	private static String option = "Yes";
	private static String bankEntityDisplayed;


	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);

        bankEntityDisplayed = BaseUI.getTextFromField(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		
		//Navigate to Username > Preferences
		//And set open dashboard into new window option
		StartPage_User_PreferencesModal.userpreferences_OpenDashboard_NewWindow(option);
		
		//After setting up to open start page in new window
		//Logout from current user.
		Navigation.navigate_to_signOut();
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.enterLoginCredentialsWithRememberCheckboxChecked(ClientDataRetrieval.userName, 
				ClientDataRetrieval.password);
	}
	
	@Test(priority = 10, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step2_VerifyStartPageOpensInNewWindow() throws Exception {
		BaseUI.verify_true_AndLog(Browser.driver.getWindowHandles().size() > 1, 
				"Start page open in New window "+ Browser.driver.getWindowHandles().size(), 
				"Start page open in New window "+ Browser.driver.getWindowHandles().size());
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step2_VerifyStartPageOpensInNewWindow_DefaultWindowName() throws Exception {
		BaseUI.verify_true_AndLog(Browser.driver.getTitle().replace("\\'", "").contains("Dashboard from Deluxe - Management Software"), 
				"Default window name MATCH"+ Browser.driver.getTitle(), 
				"Default window name DOES NOT MACTH"+ Browser.driver.getTitle());
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step2_VerifyStartPageOpensInNewWindow_SwitchToNewWindow() throws Exception {
		EntitySelector.selectWindow("Banker's Dashboard");
		EntitySelector.close_ExtraWindows("Banker's Dashboard");
		BaseUI.verify_true_AndLog(Browser.driver.getTitle().contains("Banker's Dashboard"), 
				"New window name MATCH "+ Browser.driver.getTitle(), 
				"New window name DOES NOT MACTH "+ Browser.driver.getTitle());
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step2_VerifyStartPageOpensInNewWindow_BankEntityIsSavedAsPerLastLogin() throws Exception {
		StartPage.clickStartPageTab();
		String bankEntity = BaseUI.getTextFromField(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		BaseUI.verify_true_AndLog(bankEntity.contains(bankEntityDisplayed), 
				"Bank Entity MATCH"+ Browser.driver.getTitle(), 
				"Bank Entity DOES NOT MACTH"+ Browser.driver.getTitle());
	}
	
	
	@Test(priority = 50, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step3_VerifyPreferenceSetBackToNotOpenInNewWindow() throws Exception {
		StartPage_User_PreferencesModal.userpreferences_OpenDashboard_NewWindow("No");
		StartPage.clickStartPageTab();
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
	}
	
	
	@Test(priority = 60, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step4To6_LoginInBack_VerifyStartPageNotOpenedInNewWindow() throws Exception {
		//Browser.closeBrowser();
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.enterLoginCredentialsWithRememberCheckboxChecked(ClientDataRetrieval.userName, 
				ClientDataRetrieval.password);
		
		BaseUI.verify_true_AndLog(Browser.driver.getWindowHandles().size() == 1, 
				"Start page open in New window "+ Browser.driver.getWindowHandles().size(), 
				"Start page open in New window "+ Browser.driver.getWindowHandles().size());
	}
	
	
	@Test(priority = 70, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step4To6_VerifyStartPageWindowTitleMatchBankersDshboard() throws Exception {
		BaseUI.verify_true_AndLog(Browser.driver.getTitle().contains("Banker's Dashboard"), 
				"New window name MATCH"+ Browser.driver.getTitle(), 
				"New window name DOES NOT MACTH"+ Browser.driver.getTitle());
	}
	
	
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step7_LoggedOutSuccessfully() throws Exception {
		Navigation.navigate_to_signOut();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startPage_Logout_option"));
	}
	
	
	@Test(priority = 90, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step8To9_NavigateToLoginPage_VerifyUserNameNotSaved() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		String usernameTxtbox = BaseUI.getTextFromInputBox(Locator.lookupElement("loginPage_Username_textbox"));
		BaseUI.verify_true_AndLog(!usernameTxtbox.isEmpty(), "Username textbox IS NOT EMPTY", "Username textbox IS EMPTY");
		BaseUI.verify_true_AndLog(usernameTxtbox.equals(ClientDataRetrieval.userName), 
				"Username textbox DOES Display username "+ usernameTxtbox, 
				"Username textbox DOES NOT Display username "+ usernameTxtbox);
	}
	
	
	@Test(priority = 100, groups = { "all_Tests", "regression_Tests" })
	public static void PT1124_Step9_VerifyPasswordNotSaved() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		String pwdTxtbox = BaseUI.getTextFromInputBox(Locator.lookupElement("loginPage_Password_textbox"));
		BaseUI.verify_true_AndLog(pwdTxtbox.isEmpty(), "Password textbox IS EMPTY", "Password textbox IS NOT EMPTY");
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
		BaseUI.close_ExtraWindows("Banker's Dashboard");
        Browser.closeBrowser(); 
	}
}
