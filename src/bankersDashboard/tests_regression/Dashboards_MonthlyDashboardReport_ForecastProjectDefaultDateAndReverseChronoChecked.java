package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Dashboard_MonthlyDashboardModal;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Dashboards_MonthlyDashboardReport_ForecastProjectDefaultDateAndReverseChronoChecked extends BaseTest {

	static String dataSourceOptionSelected;
	static String startDateSelected;
 
	static int dateValue = 0;
	static String forecastingProjName;
	static String newFCProjName;
	

	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Create Forecasting Project
		ForecastingPage.selectExistingFCByIndex(1);
		forecastingProjName = BaseUI.getTextFromField(Locator.lookupElement("forecasting_ExistingForecaseProjName"));
		newFCProjName = ForecastingPage.createCopyOfExistingForecastingProject(forecastingProjName);
		EntitySelector.selectWindow("Banker's Dashboard");
		
		//Navigate to Monthly Dashboard Report
		Dashboard_MonthlyDashboardModal.selectDate_DataSourceOption(newFCProjName, dateValue);
		startDateSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_StartDateOption");
		Dashboard_MonthlyDashboardModal.clickShowReportBtn();
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1447_Step3_VerifyBankLogoOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1447_Step3_VerifyReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboardReportTitle"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboardReportTitle", "Monthly Dashboard");
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1447_Step3_VerifyDataSourceDisplayedOnReport() throws Exception {
		Dashboard_MonthlyDashboardModal.verifyDataSourceOnReportPage();
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT1447_Step3_VerifyDateSelectedDisplayForFirstColumn_ReverseChronoChecked() throws Exception {
		Dashboard_MonthlyDashboardModal.verifyDateSelectedDisplayForFirstColumn_ReverseChronoChecked(startDateSelected);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	  
    	try {
    		StartPage.clickCloseAllTabsButton();
    		ForecastingPage.selectExistingFC_SelectFileMenu_DeleteProject(newFCProjName);
	        } finally{
	        	Browser.closeBrowser();
	        	}
	}
}
