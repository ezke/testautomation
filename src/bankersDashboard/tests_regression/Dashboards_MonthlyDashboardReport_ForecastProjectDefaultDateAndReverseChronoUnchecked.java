package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Dashboard_MonthlyDashboardModal;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Dashboards_MonthlyDashboardReport_ForecastProjectDefaultDateAndReverseChronoUnchecked extends BaseTest {

	private static String startDateSelected;
	private static String forecastingProjName;
	private static String newFCProjName;
	private static int dateValue = 0;

	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Create Forecasting Project
		ForecastingPage.selectExistingFCByIndex(1);
		forecastingProjName = BaseUI.getTextFromField(Locator.lookupElement("forecasting_ExistingForecaseProjName"));
		newFCProjName = ForecastingPage.createCopyOfExistingForecastingProject(forecastingProjName);
		EntitySelector.selectWindow("Banker's Dashboard");
		
		//Navigate to Monthly Dashboard Report
		Dashboard_MonthlyDashboardModal.selectDate_DataSourceOption(newFCProjName, dateValue );
		startDateSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_StartDateOption");		
	}
	
	
	@Test (priority = 10, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4410_Step2_verifyReverseChronoCheckbox() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_ReverseChronoOrder_checkbox"));
	}
	
	
	@Test (priority = 20, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4410_Step2_verifyReverseChronoCheckboxUnchecked() throws Exception {
		Dashboard_MonthlyDashboardModal.clickReverseChronoCheckbox();
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("monthlyDashboard_ReverseChronoOrder_checkbox"), false);
	}
	
	
	@Test (priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4410_Step3_VerifyBankLogoOnReport() throws Exception {
		Dashboard_MonthlyDashboardModal.clickShowReportBtn();
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test (priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4410_Step3_VerifyReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboardReportTitle"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboardReportTitle", "Monthly Dashboard");
	}
	
	
	@Test (priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4410_Step3_VerifyDataSourceDisplayedOnReport() throws Exception {
		Dashboard_MonthlyDashboardModal.verifyDataSourceOnReportPage();
	}
	
	
	@Test (priority = 30, groups = { "all_Tests", "regression_Tests", "not_bnc" })
	public static void PT4410_Step3_VerifyDateSelectedDoesNotDisplayForFirstColumn_ReverseChronoUnchecked() throws Exception {
		Dashboard_MonthlyDashboardModal.verifyDateSelectedDoesNotDisplayForFirstColumn_ReverseChronoUnchecked(startDateSelected);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	  
    	try {
    		StartPage.clickCloseAllTabsButton();
    		ForecastingPage.selectExistingFC_SelectFileMenu_DeleteProject(newFCProjName);
	        } finally{
	        	Browser.closeBrowser();
	        	}
	}
}
