package bankersDashboard.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.SummaryReportPage;
import bankersDashboard.pageControls.DropDowns;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


/**
 * @author Nisha
 * 
 *  Test #1445 -
 *	Dashboards: Summary Report: with defaults and different date option 
 *
 */

public class Dashboards_SummaryReport_DifferentDateOption extends BaseTest {
	
public static String dateDropdownValue, getDateValueFromColumn1, verifyDateValueDisplayed;
private static String profitCenter;
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		profitCenter = BaseUI.getTextFromField(Locator.lookupElement("summaryWidget_ProfitCenter"));
		//Navigate to Summary report
		Navigation.navigateToDashboard_Summary();
		
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("summaryDate_dropdown"), 2);
		dateDropdownValue = BaseUI.getSelectedOptionFromDropdown("summaryDate_dropdown");
		SummaryReportPage.clickRefreshIcon();
	}
	
	
	@Test(priority = 0)
	public static void TC_Step1_ValidateBankLogoOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
		EntitySelector.imageVerification("bankLogo");
	}
	
	
	@Test
	public static void TC_Step2_ValidateReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
	
	
	@Test
	public static void TC_Step3_ValidateProfitCenterDisplayedOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_profitCenterDisplayed"));
		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", profitCenter);
	}
	
	
	@Test
	public static void TC_Step4_VerifyDateFieldLabelOnReport() throws Exception {
		BaseUI.verifyElementAppearsByString("summaryReport_date_label");
	}
	
	
	@Test
	public static void TC_Step4_VerifyViewFieldLabelOnReport() throws Exception {
		BaseUI.verifyElementAppearsByString("summaryReport_view_label");
	}
	
	
	@Test
	public static void TC_Step5_VerifyDateDisplayedinFirstColumnMatchesDropDownValueOnReport() throws Exception {	
		//dateDropdownValue = BaseUI.getSelectedOptionFromDropdown("summaryDate_dropdown");
		getDateValueFromColumn1 = BaseUI.getTextFromField(Locator.lookupElement("summaryReport_ConsolidatedCenter"));
		BaseUI.baseStringCompare("summaryReport_ConsolidatedCenter", dateDropdownValue, getDateValueFromColumn1);
	}
	
	
	@Test
	public static void TC_Step6_SelectDifferentDateOptionAndValidateDateOnReport() throws Exception {
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("summaryDate_dropdown"), 5);
		dateDropdownValue = BaseUI.getSelectedOptionFromDropdown("summaryDate_dropdown");
		
		SummaryReportPage.clickRefreshIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
		EntitySelector.imageVerification("bankLogo");
	}
	
	
	@Test
	public static void TC_Step7_ValidateReportTitleOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
	
	
	@Test
	public static void TC_Step7_ValidateProfitCenterDisplayedOnReport() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_profitCenterDisplayed"));
		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", profitCenter);
	}
	
	
	@Test
	public static void TC_Step7_VerifyDateDisplayedinFirstColumnMatchesDropDownValueOnReport() throws Exception {	
		//dateDropdownValue = BaseUI.getSelectedOptionFromDropdown("summaryDate_dropdown");
		getDateValueFromColumn1 = BaseUI.getTextFromField(Locator.lookupElement("summaryReport_ConsolidatedCenter"));
		BaseUI.baseStringCompare("summaryReport_ConsolidatedCenter", dateDropdownValue, getDateValueFromColumn1);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {	
    	try { 
    		StartPage.clickCloseAllTabsButton();
    		Navigation.navigate_to_signOut();
    		}
    	finally{
    		Browser.closeBrowser();
    		}
	}
}
