package bankersDashboard.tests_Critical;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.SummaryReportPage;
import bankersDashboard.pages.UserPreferencesModal;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SummaryReport_ValidateExportToPdf_Excel extends BaseTest {

	private static String region = "East Region";
	static String profitCenter = "Consolidated";
	static String profitCenter_SummaryWidget;
	static String region_SummaryWidget;
	static String dateOn_SummaryWidget;
	static ArrayList<String> summaryWidgetData_Region;
	static ArrayList<String> summaryReportData;
	static ArrayList<String> summaryWidgetData_Consolidated;


	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Username drop down > Preferences
		Navigation.navigate_to_Preferences();
		//UserPreferencesModal.select_UserPreferences_ProfitCenter(region);
		region = UserPreferencesModal.select_UserPreferences_ProfitCenterByIndex(6);
		
		dateOn_SummaryWidget = BaseUI
				.getTextFromField(Locator.lookupElement("summaryWidget_Date"));
		
		summaryWidgetData_Region = new ArrayList<String>(StartPage.return_SummaryWidget_Data());
		BaseUI.verify_true_AndLog(summaryWidgetData_Region.size() > 0, "Found data"+ summaryWidgetData_Region.size(), 
				"Did NOT find data."+ summaryWidgetData_Region.size());
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3437_Step1_VerifyRegion_SummaryWidget() {
		//Validate Selected default profit center is displayed
		//on Summary widget on Start page.
		region_SummaryWidget = BaseUI
				.getTextFromField(Locator.lookupElement("summaryWidget_ProfitCenter"));
		BaseUI.baseStringCompare("Default Profit Center", region, region_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT3437_Step2_Navigate_SummaryReport() throws Exception {
		//Navigate to Summary Report
		Navigation.navigateToDashboard_Summary();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
			
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3437_Step3_VerifyRegion_OnSummaryReport() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_profitCenterDisplayed"));
		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", region_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT3437_Step4_VerifyBankLogo() throws Exception {		
		EntitySelector.imageVerification("summaryReport_BankLogo");
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3437_Step5_VerifyDate_Column1() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_dateInColumn1"));
		BaseUI.verifyElementHasExpectedText("summaryReport_dateInColumn1", dateOn_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT3437_Step6_VerifyDataOnSummaryWidgetForRegionProfitCenter_MatchSummaryReport() throws Exception {
		
		SummaryReportPage.verifyDataOnSummaryWidget_MatchSummaryReport(summaryWidgetData_Region);
	}
	
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 32)
	public void PT3437_Step7_SelectDefaultProfitCenter() throws Exception {
		StartPage.clickCloseAllTabsButton();
		Navigation.navigate_to_Preferences();
		UserPreferencesModal.select_UserPreferences_ProfitCenter(profitCenter);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT3437_Step8_VerifyProfitCenter_SummaryWidget() {
		//Validate Selected default profit center is displayed
		//on Summary widget on Start page.
		profitCenter_SummaryWidget = BaseUI
				.getTextFromField(Locator.lookupElement("summaryWidget_ProfitCenter"));
		BaseUI.baseStringCompare("Default Profit Center", profitCenter, profitCenter_SummaryWidget);
		
		summaryWidgetData_Consolidated = new ArrayList<String>(StartPage.return_SummaryWidget_Data());
		BaseUI.verify_true_AndLog(summaryWidgetData_Consolidated.size() > 0, "Found data"+ summaryWidgetData_Consolidated.size(), 
				"Did NOT find data."+ summaryWidgetData_Consolidated.size());
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT3437_Step9_NavigateTo_SummaryReport() throws Exception {
		
		StartPage.clickCloseAllTabsButton();
		StartPage.clickStartPageTab();
		//Navigate to Summary Report
		Navigation.navigateToDashboard_Summary();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
			
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT3437_Step10_VerifyProfitCenter_OnSummaryReport() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_profitCenterDisplayed"));
		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", profitCenter_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 46)
	public void PT3437_Step11_VerifyClientBankLogo() throws Exception {		
		EntitySelector.imageVerification("summaryReport_BankLogo");
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_BankLogo"));
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT3437_Step12_VerifyDate_MatchesSummaryWidget() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_dateInColumn1"));
		BaseUI.verifyElementHasExpectedText("summaryReport_dateInColumn1", dateOn_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 51)
	public void PT3437_Step13_VerifyDataOnSummaryWidget_MatchSummaryReport_Consolidated() throws Exception {
		
		SummaryReportPage.verifyDataOnSummaryWidget_MatchSummaryReport(summaryWidgetData_Consolidated);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT3437_Step14_VerifyExportToPdfIcon_Enabled() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToPDF_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToPDF_Icon"));
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT3437_Step15_VerifyExportToExcelIcon_Enabled() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToExcel_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToExcel_Icon"));
	}
	
	
//	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 60)
//	public void PT3437_clickExportToExcelIcon() throws Exception {		
//		BaseUI.click(Locator.lookupElement("exportToPDF_Icon"));
//	}
	
		
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 65)
	public void PT3437_Step16_CloseSummaryReport() throws Exception {		
		StartPage.clickCloseAllTabsButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("summaryReport_ReportTitle"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable{
		
	    try {
			StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}
		  
	}
}
