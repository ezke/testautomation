package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.ReportPackages;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * This test case runs the report packages created.
 *
 */
public class RunReportPackagesToScreen_ProfitCenter extends BaseTest {
		
	private static String repPackageName;


	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Set up Report package
		//Create New Report package
		repPackageName = ReportPackages.createNewReportPackageWithReports("no");
		
		//Verify the newly creates favorite report package is displayed
		//under My Favorite Report widget
		//StartPage.verifyMyFavoriteWidget_ReportPackageExists(ReportPackages.storePackageName);
		
		//Navigate to Run Report packages
		Navigation.navigateToReportPackages_RunReportPackage();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1035_Step2_verifyAndscrollToProfitCenterCheckbox() throws Exception {
		ReportPackages.verifyProfitCenter_selectRepPackageAnd(repPackageName, 
				"runReportPackage_ProfitCenter_CheckBox_Region");
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT1035_Step3_SelectTheProfitCenterCheckbox() throws Exception {
		ReportPackages.selectProfitCenterCheckBox_region();
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("runReportPackage_ProfitCenter_CheckBox_Region"), 
				true);
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1035_Step4_clickRunPackageButton() throws Exception {
		ReportPackages.clickRunPackageButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("runReportPackage_ModalTitle"));
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT1035_Step5_verifyReprotPackageRunOnScreen() throws Exception {
		ReportPackages.verifyReportPackagesRunToScreen();
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	  
	  
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {		  
		try {
			 StartPage.clickCloseAllTabsButton();
			 ReportPackages.deleteReportPackageAfterExecuting(repPackageName);
		     Navigation.navigate_to_signOut();
		} finally{
		     Browser.closeBrowser();
		}		  
	}
}

