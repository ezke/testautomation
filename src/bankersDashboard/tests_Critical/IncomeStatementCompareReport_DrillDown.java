package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.IncomeStatementModal;
import bankersDashboard.pages.IncomeStatementReportPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * Smoke Test : This test case validates the Income Statement compare report's 
 * drill down feature
 *
 */
public class IncomeStatementCompareReport_DrillDown extends BaseTest {
		
	private static String profitCenterSelected, selectedSource, selectedDateOption;
	private static String verifyOption1, verifyOption2, verifyOption3;
	private static String periodOption;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to Income statement compare report
		Navigation.navigateToGL_IS_IncomeStatementCompare();
		
		//Store all default options.
		profitCenterSelected = BaseUI.getSelectedOptionFromDropdown("incomeStatementCompare_ProfitCenter_dropdown");
		selectedSource = BaseUI.getSelectedOptionFromDropdown("incomeStatementCompare_Column1DataSource_dropdown");
		selectedDateOption = BaseUI.getSelectedOptionFromDropdown("incomeStatementCompare_DateColumn1_Dropdown");
		periodOption = BaseUI.getSelectedOptionFromDropdown("incomeStatementCompare_PeriodOption");
		
		IncomeStatementModal.clickShowReportButton();
	}	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT2778_Step4_verifyBankLogoIsDisplayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT2778_Step5_verifyIncomeStatementReportTitle() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("incomeStatementCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_ReportTitle", "Income Statement Compare");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 6)
	public void PT2778_Step6_verifySelectedProfitCenterIsDisplayed() throws Exception
	{
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_ProfitCenter", profitCenterSelected);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 6)
	public void PT2778_Step6_verifySelectedDateOptionIsDisplayed() throws Exception
	{
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_Source_DateSelectedIsDisplayed", 
				(periodOption+ "\n" + selectedSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(selectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 7)
	public void PT2778_Step7_verifyInterestIncomeSubTitleHeadings() throws Exception
	{
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_InterestIncomesubtitle", "Interest Income");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 8)
	public void PT2778_Step8_verifySelectedDataSourceIsDisplayed() throws Exception
	{
		BaseUI.verifyElementHasExpectedPartialText("incomeStatementCompare_Source_DateSelectedIsDisplayed", (selectedSource));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT2778_Step10_verifyTitle_GroupDrillDownTitle() throws Exception {
		BaseUI.log_Status("Click on 1st option under assets column to check drill down");
		verifyOption1 = BaseUI.getTextFromField(Locator.lookupElement("incomeStatementCompare_cashAndDuesFromBank_DrillDown"));
		IncomeStatementReportPage.clickToDrilldownReport("incomeStatementCompare_cashAndDuesFromBank_DrillDown");
		//BaseUI.verifyElementAppears(Locator.lookupElement("AnyBankUsa_Logo"));
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_GroupDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupDrillDownTitle", "Group Drill Down");	
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT2778_Step10_verifyGroupDrillDownTitle_ProfitCenter() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtGroupDrillDown", profitCenterSelected);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT2778_Step10_verifyGroupDrillDownTitle_OptionClickedAtAccountDrilldown() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_OptionClickedforDrillDown_presentAtGroupLevel", verifyOption1);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 12)
	public void PT2778_Step12_verifyAccountDrilldownTitle() throws Exception
	{
		verifyOption2 = BaseUI.getTextFromField(Locator.lookupElement("incomeStatementCompare_CommercialAndIndustrial_groupDrillDown"));
		BaseUI.log_Status("Click on 1st option under assets column to check drill down");
		IncomeStatementReportPage.clickToDrilldownReport("incomeStatementCompare_CommercialAndIndustrial_groupDrillDown");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountDrillDownTitle", "Account Drill Down");		
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 13)
	public void PT2778_Step12_verifyProfitCenterDrillDown_OptionClickedAtAccountDrilldown() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetCompare_OptionClickedAtGroupDrillDown_PresentAtAccountLevel", 
				verifyOption2.trim());
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 13)
	public void PT2778_Step12_verifyAccountDrillDown_ProfitCenter() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtAccountDrillDown", profitCenterSelected);
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 14)
	public void PT2778_Step14_verifyDrillDownReportTitle_ProfitCenterTitle() throws Exception {
		verifyOption3 = BaseUI.getTextFromField(Locator.lookupElement("incomeStatementCompare_IntIncComm_AccountDrillDown"));
		BaseUI.log_Status("Click on 1st option under assets column to check drill down");
		IncomeStatementReportPage.clickToDrilldownReport("incomeStatementCompare_IntIncComm_AccountDrillDown");
		//EntitySelector.imageVerification("AnyBankUsa_Logo");
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ProfitCenterDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterDrillDownTitle", "Profit Center Drill Down");				
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 14)
	public void PT2778_Step14_verifyProfitCenterDrillDown_ProfitCenter() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtProfitCenterDrillDown", profitCenterSelected);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 14)
	public void PT2778_Step14_verifyProfitCenterDrillDown_OptionClickedAtAccountDrilldown() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetCompare_OptionClickedAtAccountDrillDown_PresentAtProfitCenterLevel",
				verifyOption3);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT2778_Step15_verifyBackButton_IsEnabled() throws Exception {
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_Back_button_Icon"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 16)
	public void PT2778_Step15_clickBackButtonToGoBack_AccountDrillDownPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 16)
	public void PT2778_Step16_clickBackButtonToGoBack_GroupDrillDownPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_GroupDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupDrillDownTitle", "Group Drill Down");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 18)
	public void PT2778_Step17_clickBackButtonToGoBack_BalanceSheetCompareReportPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("incomeStatementCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_ReportTitle", "Income Statement Compare");
		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	    try {
		  StartPage.clickCloseAllTabsButton();
	      Navigation.navigate_to_signOut();
	    }
	    finally{
	      Browser.closeBrowser();
	    }
	  
    }
}
