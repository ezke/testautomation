package bankersDashboard.tests_Critical;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.SummaryReportPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SummaryWidget_ValidateAdd_Delete extends BaseTest {
	
	private static String dateOn_SummaryWidget;
	private static String profitCenter;
	private static String profitCenter_SummaryWidget;
	private static ArrayList<String> summaryWidgetData_Consolidated;


	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		StartPage.clickStartPageTab();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_SummaryTitle"));
		StartPage.deleteSummaryWidget();
	}
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step2_VerifySummaryWidgetDeleted() throws Exception {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("summaryWidget_SummaryTitle"));
	}


	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step2_VerifySummaryWidgetDeleted_OnlyForThatEntity() throws Exception {
		//Change the bank Entity
		EntitySelector.select_consolidatedBankEntity_fromDropdown(ClientDataRetrieval.consolidatedEntity);
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_SummaryTitle"));
	}


	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step2_VerifyEntityChangedBackToBankEntity() throws Exception {
		//Change back to previous bank Entity and Summary widget is not displayed
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		BaseUI.waitForElementToBeDisplayed("navigation_AL", null, null);
		//BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_AL"));
	}


	@Test(priority = 26, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step2_VerifySummaryWidgetNotDisplayedOnStartPage() throws Exception {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("summaryWidget_SummaryTitle"));
	}



	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step3To4_VerifySummaryWidgetIsAddedBackToStartPage() throws Exception {
		AddWidgets_StartPage.addSummaryWidget_OutOfBoxModule();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_SummaryTitle"));
		profitCenter = BaseUI
				.getTextFromField(Locator.lookupElement("summaryWidget_ProfitCenter"));

		dateOn_SummaryWidget = BaseUI
				.getTextFromField(Locator.lookupElement("summaryWidget_Date"));
	}


	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT3781_Step8_VerifyProfitCenter_SummaryWidget() {
		//Validate Selected default profit center is displayed
		//on Summary widget on Start page.
		profitCenter_SummaryWidget = BaseUI
				.getTextFromField(Locator.lookupElement("summaryWidget_ProfitCenter"));
		BaseUI.baseStringCompare("Default Profit Center", profitCenter, profitCenter_SummaryWidget);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT3781_Step9_VerifySummaryWidgetData_ReturnsValue() {
		summaryWidgetData_Consolidated = new ArrayList<String>(StartPage.return_SummaryWidget_Data());
		BaseUI.verify_true_AndLog(summaryWidgetData_Consolidated.size() > 0, "Found data"+ summaryWidgetData_Consolidated.size(),
				"Did NOT find data."+ summaryWidgetData_Consolidated.size());
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT3781_Step9_NavigateTo_SummaryReport() throws Exception {
		
		StartPage.clickCloseAllTabsButton();
		StartPage.clickStartPageTab();
		//Navigate to Summary Report
		Navigation.navigateToDashboard_Summary();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
			
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT3781_Step10_VerifyProfitCenter_OnSummaryReport() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_profitCenterDisplayed"));
		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", profitCenter_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 46)
	public void PT3781_Step11_VerifyClientBankLogo() throws Exception {		
		EntitySelector.imageVerification("summaryReport_BankLogo");
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_BankLogo"));
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT3781_Step12_VerifyDate_MatchesSummaryWidget() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_dateInColumn1"));
		BaseUI.verifyElementHasExpectedText("summaryReport_dateInColumn1", dateOn_SummaryWidget);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 51)
	public void PT3781_Step13_VerifyDataOnSummaryWidget_MatchSummaryReport_Consolidated() throws Exception {
		
		SummaryReportPage.verifyDataOnSummaryWidget_MatchSummaryReport(summaryWidgetData_Consolidated);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT3781_Step14_VerifyExportToPdfIcon_Enabled() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToPDF_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToPDF_Icon"));
	}
	
	
	@Test (priority = 55)
	public void PT3781_Step15_VerifyExportToExcelIcon_Enabled() throws Exception {		
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToExcel_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToExcel_Icon"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickStartPageTab();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}

}
