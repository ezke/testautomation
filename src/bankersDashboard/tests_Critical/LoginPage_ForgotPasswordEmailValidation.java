package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.ForgotPasswordPage;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.ResultWriter;

public class LoginPage_ForgotPasswordEmailValidation extends BaseTest {
	
	 String email = GlobalVariables.emailAddress;
	 String emailSubject;
	 Email firstEmail;
	 String linkFromEmail = "";
	 String emailCreationDate;
	 String emailBodyContent;
	 String newPassword = "Password"+BaseUI.random_NumberAsString(1, 20)+"!";

	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
		
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.clickOnForgotPasswordLink();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1033_Step1_VerifyForgotPwd_PasswordRetrievalPageHeader() throws Exception {
		BaseUI.verifyElementHasExpectedText("forgotPwd_passwordRetrievalHeader", "Password Retrieval");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1033_Step1_VerifyForgotPwd_PasswordRetrievalSubDescription() throws Exception {
		BaseUI.verifyElementHasExpectedText("forgotPwd_TextOnPassowrdRetrievalPage", 
				"Enter your username. A password reset email will be sent to your account on file.");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT1033_Step1_VerifyForgotPwd_PasswordRetrievalEmailSentMessage() throws Exception {
		ForgotPasswordPage.clickForgotPasswordAndResetPassword(ClientDataRetrieval.userName);
		BaseUI.verifyElementHasExpectedText("forgotPwd_emailSentMessage", 
				"An email has been sent to your account on file. Please click on the link provided in the email to reset your password."); 
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT1033_Step2_VerifyForgotPwd_EmailReceived() throws Exception {			
		Email_API.Wait_ForEmail_ToExist(email, 180);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1033_Step2_VerifyReceivedEmailSubject() throws Exception {
		 emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(emailSubject.equals("Forgot password request"), 
				 "Email subject displays: "+emailSubject, 
				 "Email subject DOES NOT display: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1033_Step2_VerifyReceivedEmail_CreationDate() throws Exception {
		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
				 "Email Creation date MATCHES Alert email sent date "+ String.valueOf(emailCreationDate), 
				 "Email Creation date DOES NOT MATCH Alert email sent date "+ String.valueOf(emailCreationDate));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT1033_Step4_VerifyForgotPwd_ReceivedEmailBodyContent_ResetPwdLink() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		BaseUI.verify_true_AndLog(emailBodyContent.contains("Reset your Password"), 
				"Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+ emailBodyContent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT1033_Step4_VerifyForgotPwd_ReceivedEmailBodyContent_Username() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		BaseUI.verify_true_AndLog(emailBodyContent.contains(ClientDataRetrieval.userName), 
				"Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+ emailBodyContent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT1033_Step4_VerifyForgotPwd_ReceivedEmailLink_PasswordResetModalAppears_AfterNavigatingToLink() throws Exception {
		linkFromEmail = Email_API.return_FirstLink_FromEmail(firstEmail);
		Browser.navigateTo(linkFromEmail);
		ForgotPasswordPage.verifyResetPasswordModalAppears();
	}
	
	
	//Commenting these line for now because when navigated to Reset Your Password hyperlink,
	//it is displaying link has expired message. So Cannot test reseting password at this point.
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT1033_Step4_VerifyForgotPwd_ReceivedEmailLink_PasswordResetModalAppears_SuccessfulLoginWithNewPassword() throws Exception {
		ForgotPasswordPage.enterResetPasswordDetails(ClientDataRetrieval.userName, newPassword);
		//Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, newPassword);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	}
	
    
	@AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	    
	  
    @AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
		try {
			Email_API.delete_Emails_ByRecipient(email);
			Navigation.navigate_to_changePassword(newPassword, ClientDataRetrieval.password);
			Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
