package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * 
 * The Fed Funds Futures Curve displays when logging into a bank.
 * NOTE: This proves that the client service is running.
 *
 */
public class FedFundFuturesCurveWidget extends BaseTest {
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		EntitySelector.imageVerification("loginPage_BD_logo");		
		StartPage.clickStartPageTab();
	}
	
	
	 @Test(groups = { "all_Tests", "critical_Tests" })
	 public static void PT1037_VerifyFedFundFuturesCurveWidget() throws Exception{
		if(!(BaseUI.elementAppears(Locator.lookupElement("startPage_FedFundsFuturesCurve_widget")))) {
			  
			AddWidgets_StartPage.addFedFundCurvesWidget(); 			  
		}
	 }

	 	 
	 @AfterMethod(alwaysRun = true)
	 public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	 }
	  
	  	  
	 @AfterClass(alwaysRun = true)
	 public void tear_down() throws Throwable
	 {
		 try {
		      Navigation.navigate_to_signOut();
		  }
		  finally{
		      Browser.closeBrowser();
		  }
		  
	 }
}

