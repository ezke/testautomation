package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddNewUserPage;
import bankersDashboard.pages.CustomCalculations;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CustomCalculations_ValidatePrivateAccessLevel extends BaseTest {
	
	static String customCalcName;
	static String accessLevel = "Private";
	static String format = "Dollar ($)";
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Create New custom calcualtions
		customCalcName = CustomCalculations.addNewCustomCalculation(format, accessLevel);
		
		//Add New user to test if the Custom Calculations
		//is Private, then it is not available to other users
		AddNewUserPage.addNewUser_LoginAsNewUser();
		Navigation.navigateToTools_CustomCalculations();
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step1_VerifyCustomCalcPage_PageSubText() throws Exception {
	
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_TextUnderCustomCalcHeader"));
		BaseUI.verifyElementHasExpectedText("customCalc_TextUnderCustomCalcHeader", 
				"Use this screen to define and manage custom calculations.");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step2_VerifyCustomCalcPage_AddCalcButton() throws Exception {
	
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_AddCalculation_button"));		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step3_VerifyCustomCalcPage_AllAccessLevel_Btn() throws Exception {
	
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_All_button"));
		BaseUI.verifyElementHasExpectedText("customCalc_All_button", "All");		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step3_VerifyCustomCalcPage_PublicAccessLevel_Btn() throws Exception {
	
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_Public_button"));
		BaseUI.verifyElementHasExpectedText("customCalc_Public_button", "Public");		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step4_VerifyCustomCalcPage_PrivateAccessLevel_Btn() throws Exception {
	
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_Private_button"));
		BaseUI.verifyElementHasExpectedText("customCalc_Private_button", "Private");		
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step5_VerifyCustomCalcPage_NewlyCreatedPrivateCustomCalc_NotAvailableForOtherUsers() throws Exception {
	
		CustomCalculations.searchCustomCalc(customCalcName);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("customCalcSearchPage_CustomCalcName"));
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step5_VerifyCustomCalcPage_NewlyCreatedPrivateCustomCalc_NotAvailableForOtherUser_UnderAllAccess() throws Exception {
	
		CustomCalculations.clickOnAllAccessButton();
		CustomCalculations.verifyCustomCalc_Displays_AtAccessLevel(customCalcName);	
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("customCalcSearchPage_CustomCalcName"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step5_VerifyCustomCalcPage_NewlyCreatedPrivateCustomCalc_NotAvailableForOtherUser_UnderPublicAccess() throws Exception {
	
		CustomCalculations.clickOnPublicAccessButton();
		CustomCalculations.verifyCustomCalc_Displays_AtAccessLevel(customCalcName);	
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("customCalcSearchPage_CustomCalcName"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step5_VerifyCustomCalcPage_NewlyCreatedPrivateCustomCalc_NotAvailableForOtherUser_UnderPrivateAccess() throws Exception {
	
		CustomCalculations.clickOnPrivateAccessButton();
		CustomCalculations.verifyCustomCalc_Displays_AtAccessLevel(customCalcName);	
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("customCalcSearchPage_CustomCalcName"));
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
   
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable
    {  
    	try {
    		Navigation.navigate_to_signOut_WithoutAlert();
    		CustomCalculations.loginAsClientUser_DeleteCustomCalc_NewlyCreatedUser();
	        } finally {
	        	Browser.closeBrowser();
	        	}
    }

 }
