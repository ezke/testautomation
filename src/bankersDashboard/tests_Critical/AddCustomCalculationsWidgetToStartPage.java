/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.CustomCalculations;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class AddCustomCalculationsWidgetToStartPage extends BaseTest {
	
	private static String storeCustomCalc, storeCustomCalcWidgetName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		storeCustomCalc = CustomCalculations.addCustomCalcWithGLAccount();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT1663_Step10_VerifyCustomCalcWidget_AddedToStartPage() throws Exception {
		storeCustomCalcWidgetName = AddWidgets_StartPage.addCustomCalWidgetToStartPage(storeCustomCalc);
		StartPage.clickStartPageTab();
		WebElement customCalcIndicator = Locator.lookupElement("verifyCustomCalcIndicatorAdded_startPage");
		BaseUI.scroll_to_element(customCalcIndicator);
		BaseUI.verifyElementAppears(customCalcIndicator);
		BaseUI.verifyElementHasExpectedText("verifyCustomCalcIndicatorAdded_startPage", storeCustomCalcWidgetName);		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		StartPage.clickStartPageTab();
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable
  {  
	  try {
		  StartPage.deleteWidgetFromStartPage("customCalcWidgetDeleteIcon", storeCustomCalcWidgetName, "deleteCustomCalculationWidget");
		  CustomCalculations.deleteCustomCalculation(storeCustomCalc);
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }
}
