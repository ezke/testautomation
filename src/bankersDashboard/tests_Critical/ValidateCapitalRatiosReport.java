package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CapitalRatioPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.UpdateMemoField;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class ValidateCapitalRatiosReport extends BaseTest {
	
	static String dateOption = "July 2016";
	static String totalRiskBasedRatio;
	static String viewOption = "6";
	static String totalRiskBasedRatio_AfterMemoUpdate;
	static String totalRiskBasedRatioAfter;

	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to GL> Capital Requirements > Capital Ratios report
		CapitalRatioPage.selectDefaultOptions_CapitalRatioReport(dateOption);	
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1490_Step1_VerifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT1490_Step1_verifyCapitalRatio_ReportTitle() throws Exception
	{
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("capRatios_ReportTitle", "Capital Ratios");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT1490_Step2_VerifyCapitalRatioGraphImage() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_CapitalRatioGrapgImg"));
	}
	
	//This verification does not exists on Capital Ratio report
	//So commenting out.
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
//	public void PT1490_Step3_VerifyCapitalRatio_UpdateMemoFieldMsg() throws Exception {
//		BaseUI.scroll_to_element(Locator.lookupElement("capRatios_UpdateMemoFieldMsg"));
//		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_UpdateMemoFieldMsg"));
//	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1490_Step4_EnterValues_UpdateMemoField() throws Exception {
	    totalRiskBasedRatio = BaseUI.
	    		getTextFromField(Locator.lookupElement("capRatios_Table_TotalRiskBasedRatioRaColumn"));
	    
	    StartPage.clickCloseAllTabsButton();
		UpdateMemoField.update_UpdateMemoField(dateOption);
		StartPage.clickCloseAllTabsButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("updtMemo_Header"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT1490_Step5_verifyTotalRiskBasedRatio_CapRatioTableSame() throws Exception {
		StartPage.clickStartPageTab();
		CapitalRatioPage.verifyTotalRiskBasedRatioMatch(viewOption, dateOption, 
				totalRiskBasedRatio);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT1490_Step6_verifyTotalRiskBasedRatio_CapRatioTableDifferent() throws Exception {
		StartPage.clickStartPageTab();
		CapitalRatioPage.verifyTotalRiskBasedRatioDifferent(viewOption, dateOption, 
				totalRiskBasedRatio);
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	UpdateMemoField.resetUpdateMemoFieldValues(dateOption);
	    	StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
