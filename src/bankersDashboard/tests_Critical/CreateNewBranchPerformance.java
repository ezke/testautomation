package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BranchPerformanceModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.ManageExistingViewsModal;
import bankersDashboard.pages.ModifyBranchPerformancePage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CreateNewBranchPerformance extends BaseTest {

	static String setAccessLevel = "Only Me";
	static String balSheetPercent = "50";
	static String incomePercent = "30";
	static String marginPercent = "20";
	static String newViewName;


	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to AL> Historical Monthly Loan Report
		Navigation.navigateToBranchPerformance_CreateNewView();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1857_Step2_VerifyViewNameTextLabel() {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_ViewNameTextboxLabel"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1857_Step2_VerifyViewDescriptionTextLabel() {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_ViewDescriptionTextboxLabel"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1857_Step2_VerifyViewAccessTextLabel() {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_ViewAccessDropdownLabel"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public static void PT1857_Step3_EnterNewViewDetails_VerifyRankingWeightsSubtitle() throws Exception {
		newViewName = BranchPerformanceModal.enterNewViewDetails(setAccessLevel);
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_SetRankingWeights_Subtitle"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1857_Step4_VerifyBalanceSheet_RankingWeights_BeforeCreation() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_BalancePercentage"));
		String balancePercent = BaseUI
				.getTextFromInputBox(Locator.lookupElement("branchPerf_CreateView_BalancePercentage"));
		BaseUI.baseStringCompare("Balance Sheet Percent", "0", balancePercent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1857_Step4_VerifyIncomeStatement_RankingWeights_BeforeCreation() {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_IncomePercentage"));
		String incomePercent = BaseUI
				.getTextFromInputBox(Locator.lookupElement("branchPerf_CreateView_IncomePercentage"));
		BaseUI.baseStringCompare("Income Statement Percent", "0", incomePercent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1857_Step4_VerifyMargin_RankingWeights_BeforeCreation() {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_MarginPercentage"));
		String marginPercent = BaseUI
				.getTextFromInputBox(Locator.lookupElement("branchPerf_CreateView_MarginPercentage"));
		BaseUI.baseStringCompare("Margin Percent", "0", marginPercent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1857_Step4_VerifyTotal_RankingWeights_BeforeCreation() {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_TotWeightPercentage"));
		String totalPercent = BaseUI.get_Attribute_FromField(Locator.lookupElement("branchPerf_CreateView_TotWeightPercentage"),
				"innerText");
		BaseUI.baseStringCompare("Total Percent", "0%", totalPercent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public static void PT1857_Step4_VerifyTotal_RankingWeights_AfterValuesEntered() throws Exception {
		BranchPerformanceModal.addRankingWeights(balSheetPercent, incomePercent, marginPercent);
		String totalPercent = BaseUI.get_Attribute_FromField(Locator.lookupElement("branchPerf_CreateView_TotWeightPercentage"),
				"innerText");
		BaseUI.baseStringCompare("Total Percent Updated", "100%", totalPercent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1857_Step5_VerifyNavigatedToSelectBranchesPage() throws Exception {
		BranchPerformanceModal.clickNextBtn();
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_SelectBranches_Subheader"));
		BaseUI.verifyElementHasExpectedText("branchPerf_CreateView_SelectBranches_Subheader", "Select Branches");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public static void PT1857_Step5_VerifyAvailableCenterTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_AvailCentersLabel"));
		BaseUI.verifyElementHasExpectedText("branchPerf_CreateView_AvailCentersLabel", "Available Centers");
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public static void PT1857_Step5_VerifyMyBranchesTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_CreateView_MyBranchesLabel"));
		BaseUI.verifyElementHasExpectedText("branchPerf_CreateView_MyBranchesLabel", "My Branches");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public static void PT1857_Step6_VerifyAvailableCentersDragged_MyBranchesBox() throws Exception {
		BranchPerformanceModal.dragAndDropCenters();
		BaseUI.verifyElementExists("branchPerf_CreateView_MyBranchesCenter1", null, null);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public static void PT1857_Step7_VerifyNewViewCreatedAndModalClosed() throws Exception {
		BranchPerformanceModal.clickSaveBtn();
		BaseUI.verifyElementDoesNOTExist("branchPerf_CreateView_ModalTitle", null, null);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public static void PT1857_Step8_VerifyNavigatedToModifyViewPage() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_PageHeader"));
		BaseUI.verifyElementHasExpectedText("branchPerf_ModifyView_PageHeader", 
				"Modify Branch Performance View");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public static void PT1857_Step8_VerifyNewViewName() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_NewViewName"));
		BaseUI.verifyElementHasExpectedText("branchPerf_ModifyView_NewViewName", 
				newViewName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public static void PT1857_Step8_VerifyNewViewDescriptionDetail() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_NewViewDescriptionDetail"));
		BaseUI.verifyElementHasExpectedText("branchPerf_ModifyView_NewViewDescriptionDetail", 
				"Testing Creation of New View in Branch Performance" + " " + newViewName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public static void PT1857_Step8_VerifyNewView_BalanceSheetPercent() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_BalancePercent"));
		String balPercent = BaseUI.getTextFromField(Locator.lookupElement("branchPerf_ModifyView_BalancePercent"));
		BaseUI.baseStringCompare("Balance Sheet Percent On Modify View Page", balSheetPercent +"%", balPercent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public static void PT1857_Step8_VerifyNewView_IncomeStatementPercent() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_IncomePercent"));
		String incomePercentDisplayed = BaseUI.getTextFromField(Locator.lookupElement("branchPerf_ModifyView_IncomePercent"));
		BaseUI.baseStringCompare("Income Statement Percent On Modify View Page", incomePercent +"%", incomePercentDisplayed);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public static void PT1857_Step8_VerifyNewView_MarginPercent() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_MarginPercent"));
		String marginPercentDisplayed = BaseUI.getTextFromField(Locator.lookupElement("branchPerf_ModifyView_MarginPercent"));
		BaseUI.baseStringCompare("Margin Percent On Modify View Page", marginPercent +"%", marginPercentDisplayed);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public static void PT1857_Step9_VerifyNewView_DisplaysInExisitngViews() throws Exception {
		StartPage.clickCloseAllTabsButton();
		ManageExistingViewsModal.selectExistingView(newViewName);
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_PageHeader"));
		BaseUI.verifyElementHasExpectedText("branchPerf_ModifyView_NewViewName", 
				newViewName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public static void PT1857_Step10_VerifyNewView_DoesNotDiaplyAfterDeleting() throws Exception {
		ModifyBranchPerformancePage.clickDeleteViewBtn();
    	BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ModifyView_DeleteViewMessage"));
    	BaseUI.verifyElementHasExpectedText("branchPerf_ModifyView_DeleteViewMessage", 
    			"Branch TotemPole Deleted.");
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
