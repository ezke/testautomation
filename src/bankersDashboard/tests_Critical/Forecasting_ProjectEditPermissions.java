package bankersDashboard.tests_Critical;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Forecasting_ProjectEditPermissions extends BaseTest {

	static String fcName = "Auto Test";
	static String toolOption = "Project Edit Permissions...";
	static String allPermissionUser = "AllPC Bob (Bob.AllPC.00000)";
	static String somePermissionUser = "SomePC Bob (Bob.SomePC.00000)";
	static String noPermissionUser = "NoPC Bob (Bob.NoPC.00000)";
	
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Select Existing Forecasting project
		ForecastingPage.selectExistingFCByName(fcName);
		ForecastingPage.clickToolsMenuOptions(toolOption);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1561_VerifyForecasting_ProjecPermission_Title() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_ProjectPermissions_Title"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public static void PT1561_VerifyForecasting_ProjecPermission_HelpText() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_ProjectPermissions_HelpText"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1561_VerifyForecasting_ProjecPermission_AllPermission_User() throws Exception {
		
		ForecastingPage.switchToIframe_ProjectPermission();
		ForecastingPage.verify_ProjectPermission_ActiveUser_ByName(allPermissionUser);
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public static void PT1561_VerifyForecasting_ProjecPermission_AllPermission_User_List() throws Exception {
		
		ForecastingPage.verify_ProjectPermission_ActiveUser_List(allPermissionUser, "ALL");
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public static void PT1561_VerifyForecasting_ProjecPermission_SomePermission_User() throws Exception {
		
		ForecastingPage.verify_ProjectPermission_ActiveUser_ByName(somePermissionUser);
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public static void PT1561_VerifyForecasting_ProjecPermission_SomePermission_User_List() throws Exception {
		
		ForecastingPage.verify_ProjectPermission_ActiveUser_List(somePermissionUser,"1");
		ForecastingPage.verify_ProjectPermission_ActiveUser_List(somePermissionUser, "2");
		ForecastingPage.verify_ProjectPermission_ActiveUser_List(somePermissionUser, "10");
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public static void PT1561_VerifyForecasting_ProjecPermission_NoPermission_User() throws Exception {
		
		ForecastingPage.verify_ProjectPermission_NonActiveUser_ByName(noPermissionUser);
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public static void PT1561_VerifyForecasting_ProjecPermission_NoPermission_User_List() throws Exception {
		
		ForecastingPage.verify_ProjectPermission_NoPermission_UserList(noPermissionUser);
		
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	
	@AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
		 Browser.closeBrowser();
	}
	
}
