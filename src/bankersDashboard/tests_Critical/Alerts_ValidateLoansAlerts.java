package bankersDashboard.tests_Critical;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.Settings_MyAlertsModal;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.StartPage_AlertsModal;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;

public class Alerts_ValidateLoansAlerts extends BaseTest {

	private static String loansAlertsId;
	private static ArrayList<String> loansAlertsDetails;
    static String emailID = "nisha.nayak@deluxe.com";
	
	//Email Validation variables
	static String email = GlobalVariables.emailAddress;
	static String emailSubject;
	static Email firstEmail;
	static String emailCreationDate;
	static String emailBodyContent;
	static String cdDetailsOnEmailModal;


	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
		
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		StartPage.clickMyAlertsBellIcon();
		StartPage_AlertsModal.clickLoansTab();
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_LoansTab_NoAlertsMessage"))) {
			Settings_MyAlertsModal.setLoanThresholds_IfNoAlertMessageDisplayed();			
		} 
		
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_LoansTab_NoAlertsMessage"))) {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("alerts_LoansTab_SendEmailBtn"));
			BaseUI.log_AndFail("Loans Alerts are not Set");
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("alerts_LoansTab_SendEmailBtn"));
			
			//Store 1st Loan Alert Loan ID number
			loansAlertsId = BaseUI.getTextFromField(Locator.lookupElement("alerts_LoansTab_LoansAlerts_FirstLoanId"));
					
			//Get 1st Loan Alert Details
			loansAlertsDetails = StartPage_AlertsModal.return_LoansAlertsDetails();
			BaseUI.verify_true_AndLog(loansAlertsDetails.size() > 0, "CD Alerts Details are Displayed", 
					"CD Alerts Details are NOT Displayed");
		}		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_ModalTitle() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_TodaysAlertsTitle"));
		BaseUI.verifyElementHasExpectedText("alerts_TodaysAlertsTitle", 
				"Today's Alerts");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_AlertsRefreshIcon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_RefreshIcon"));
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_LoansTab() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_LoansTab"));
		BaseUI.verifyElementHasExpectedPartialText("alerts_LoansTab", 
				"Loans");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step2_VerifyAlerts_LoansAlertsExists() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_LoansTab_LoansAlertsNum"));
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step2_VerifyISAlertsTab_SendEmailBtn() throws Exception {
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_LoansTab_LoansAlertsNum"))){
			BaseUI.verifyElementAppears(Locator.lookupElement("alerts_LoansTab_SendEmailBtn"));
		}
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step3_VerifyISAlertsTab_AlertsNum_MatchesAlertsDisplayed() throws Exception {
		StartPage_AlertsModal.verifyLoansAlert_AlertsNum_MatchesAlertsDisplayed();	
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModalDisplaysLoansAlerts() throws Exception {
		StartPage_AlertsModal.clickLoans_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModalTitle"));
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlerts_ToAddressFieldHasText() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText"));
		String toAddressMessage = BaseUI.get_Attribute_FromField(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText"), 
				"placeholder");
		BaseUI.baseStringCompare("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText", 
				"Email(s) - Enter up to 25 emails.", toAddressMessage);
	}
	
	
	@Test(priority = 50, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlerts_SubjectFieldText() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_SubjectField"));
		String subjectFieldMsg = BaseUI.get_NonVisible_TextFromField(Locator
				.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_SubjectField"));
		BaseUI.baseStringCompare("alerts_CDsTab_SendEmail_EmailDialogModal_SubjectField", 
				"Banker's Dashboard Alert - "+ loansAlertsId.toLowerCase(), subjectFieldMsg);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlertDetails_DateMatch() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		StartPage_AlertsModal.verifyAlertsDetails_MatchAlertsEmailDetails_Date(loansAlertsDetails);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlertDetails_ProdNameMatch() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		StartPage_AlertsModal.verifyAlertsDetails_MatchAlertsEmailDetails_ProdName(loansAlertsDetails);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlertDetails_RateAndTermMatch() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		StartPage_AlertsModal.verifyAlertsDetails_MatchAlertsEmailDetails_RateAndTerm(loansAlertsDetails);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlertDetails_AmountMatch() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		StartPage_AlertsModal.verifyAlertsDetails_MatchAlertsEmailDetails_Amount(loansAlertsDetails);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlertDetails_OfficerMatch() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		StartPage_AlertsModal.verifyAlertsDetails_MatchAlertsEmailDetails_Officer(loansAlertsDetails);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlertDetails_BranchMatch() throws Exception {
		//StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		StartPage_AlertsModal.verifyAlertsDetails_MatchAlertsEmailDetails_Branch(loansAlertsDetails);
	}
	
	
	@Test(priority = 45, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyEmailDialogModal_LoansAlerts_EmailBodyFieldText() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_BodyTextarea"));
		String bodyFieldDefaultMsg = BaseUI.get_NonVisible_TextFromField(Locator
				.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_BodyTextarea"));
		BaseUI.baseStringCompare("alerts_CDsTab_SendEmail_EmailDialogModal_BodyTextarea", 
				"Please review the following to ensure that the price is correct for the category.", 
				bodyFieldDefaultMsg);
	}
	
	
	@Test(priority = 55, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step5_VerifyEmailDialogModal_LoansAlerts_SendEmailAndValidate() throws Exception {
		StartPage_AlertsModal.clickEmailModal_CancelBtn();
		StartPage_AlertsModal.enterEmailDetailsForLoansAlerts_SendAlertEmail(emailID);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModalTitle"));
	}
	
	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
//	public static void PT3789_Step5_VerifyRunPackage_EmailReceived() throws Exception {
//		Email_API.Wait_ForEmail_ToExist(email, 120);
//		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
//		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");
//	}
//	
//	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
//	public static void PT3789_Step5_VerifyReceivedEmailSubject() throws Exception {
//		 emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
//		 BaseUI.verify_true_AndLog(emailSubject.equals("Banker's Dashboard Alert - "+ (loansAlertsId).toLowerCase()), 
//				 "Email subject displays Banker's Dashboard Alert - "+emailSubject, 
//				 "Email subject DOES NOT display Banker's Dashboard Alert - "+ emailSubject);
//	}
//	
//	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
//	public static void PT3789_Step5_VerifyReceivedEmail_CreationDate() throws Exception {
//		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
//		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
//				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
//				 "Email Creation date MATCHES Alert email sent date "+ String.valueOf(emailCreationDate), 
//				 "Email Creation date DOES NOT MATCH Alert email sent date "+ String.valueOf(emailCreationDate));
//	}
//	
//	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
//	public static void PT3789_Step5_VerifyEmailContainsMessage() throws Exception {
//		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
//		BaseUI.verify_true_AndLog(emailBodyContent.contains("This email is to test CD alerts email feature. Please ignore!"), 
//				"Email contents FOUND: "+ emailBodyContent, 
//				"Email contents NOT FOUND: "+ emailBodyContent);
//	}
//	
//	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
//	public static void PT3789_Step5_VerifyEmailContainsMessage_LoanAlertsDetails() throws Exception {
//		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
//		BaseUI.verify_true_AndLog(emailBodyContent.contains(cdDetailsOnEmailModal), 
//				"Email contents FOUND displays CD Alerts: "+ emailBodyContent, 
//				"Email contents DOES NOT displays CD Alerts: "+ emailBodyContent);
//	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	Email_API.delete_Emails_ByRecipient(email);
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
