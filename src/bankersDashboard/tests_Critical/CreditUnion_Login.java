package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages_CreditUnion.LoginPage_CreditUnion;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CreditUnion_Login extends BaseTest {
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {		
		//Navigate to Credit Union url
		Browser.openBrowser(GlobalVariables.creditUnion_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		if(!BaseUI.elementAppears(Locator.lookupElement("creditUnion_SiteCertificate"))) {
			BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_Login_Username"));
		} else {
			LoginPage_CreditUnion.click_SiteCertificate();
		}
			
		//Login into Credit Union with Valid logi credentials
		LoginPage_CreditUnion.login(GlobalVariables.userName_CU, GlobalVariables.password_CU);
	}
	
	@Test
	public void PT3784_verify_BDLogo() throws Exception {
		EntitySelector.imageVerification("creditUnion_HomePage_Logo");
		
	}
	
	@Test
	public void PT3784_verify_User_Logged() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_UserDropDown"));
	}
	
	@Test
	public void PT3784_verify_Announcement_header() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_AnnouncementsIcon"));		
	}
	
	@Test
	public void PT3784_verify_Help_header() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_HelpDropDown"));
		BaseUI.verifyElementHasExpectedText("creditUnion_HomePage_HelpDropDown", "Help");	
	}
	
	@Test
	public void PT3784_verify_myAlerts_header() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_AlertsIcon"));		
	}
	
	@Test
	public void PT3784_verify_myFavorites_header() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_MyFavoritesDropdown"));
		BaseUI.verifyElementHasExpectedText("creditUnion_HomePage_MyFavoritesDropdown", "  My Favorite Reports");		
	}
	
	@Test
	public void PT3784_verify_profitCenter_bankEntity() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_EntityDropdown"));
		BaseUI.verifyElementHasExpectedPartialText("creditUnion_HomePage_EntityDropdown", "Credit Union");	
	}
	
	
	@Test
	public void PT3784_verify_Summary_widget() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_SummaryWidget"));
		BaseUI.verifyElementHasExpectedText("creditUnion_HomePage_SummaryWidget", "Summary");
	}
	
	
	@Test
	public void PT3784_verify_AddwidgetIcon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_AddWidgetIcon"));
	}
	
	
	@Test
	public void PT3784_verify_ReportProperties_Icon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_RepPropertiesIcon"));
	}
	
	
	@Test
	public void PT3784_verify_DashboardTab() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_HomePage_DashboardTab"));
		
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	  
	  
	  
	 @AfterClass(alwaysRun = true)
	 public void tear_down() throws Throwable {
		  try {
		      Navigation.navigate_to_signOut();
		  }
		  finally{
		      Browser.closeBrowser();
		  }	  
	 }
}
