package bankersDashboard.tests_Critical;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CapitalRatioPage;
import bankersDashboard.pages.CapitalReqReportPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CapitalRequirementReport_CapRatioValidation extends BaseTest {

	static String dateOption_CapRatio = "July 2017";
	static String newDateOption_CapRatio = "July 2016";
	static String dateOptionCol1_CapReq = "July 2016";
	static String dateOptionCol2_CapReq = "June 2016";
	static ArrayList<String> capRatioList;
	static String capReqDateCol2;
	static String commTier1Ratio;
	static String tier1LevRatio;
	static String tier1RiskBasedRatio;
	static String totalRiskBasedRatio;
	static ArrayList<String> capRatioList_DiffDate;

	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to GL> Capital Requirements > Capital Ratios report
		
		CapitalRatioPage.selectDefaultOptions_CapitalRatioReport(dateOption_CapRatio);
		
		//store capital ratio table last row data
		capRatioList = CapitalRatioPage.return_CapitalRatioTable_LastRowData();
		BaseUI.verify_true_AndLog(capRatioList.size() > 0, "Found data: " + capRatioList.size(), 
				"Did NOT find data: "+ capRatioList.size());
		
		//Click Report Properties Icon and Generate report for new date
		CapitalRatioPage.clickRepPropertiesBtn_SelectDiffDateOption(newDateOption_CapRatio);
		BaseUI.scroll_to_element(Locator.lookupElement("capRatios_Table_PeriodColumnName"));
		
		//store capital ratio table last row data
		capRatioList_DiffDate = CapitalRatioPage.return_CapitalRatioTable_LastRowData();
		BaseUI.verify_true_AndLog(capRatioList.size() > 0, "Found data: " + capRatioList.size(), 
				"Did NOT find data: "+ capRatioList.size());
		StartPage.clickCloseAllTabsButton();
		
		//Navigate to GL> Capital Requirements > Capital Requirement report
		Navigation.navigateToGL_CapitalAndRegulatory_CapitalRequirements();
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("capReq_Modal_Col1DateOption"), dateOption_CapRatio);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("capReq_Modal_Col2DateOption"), dateOption_CapRatio);
		capReqDateCol2 = BaseUI.getSelectedOptionFromDropdown("capReq_Modal_Col2DateOption");
		CapitalReqReportPage.clickShowReportBtn();	
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_ReportTitle() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportTitle", "Capital Requirements");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_Column1Date() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportCol1Date"));
		String expectedDateCol1 = BaseUI.return_Date_AsDifferentFormat(dateOption_CapRatio, "MMMM yyyy", "MMM yyyy").trim();
		BaseUI.verifyElementHasExpectedText("capReq_ReportCol1Date", expectedDateCol1);
	}
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_Column2Date() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportCol2Date"));
		String expectedDateCol2 = BaseUI.return_Date_AsDifferentFormat(capReqDateCol2, "MMMM yyyy", "MMM yyyy").trim();
		BaseUI.verifyElementHasExpectedText("capReq_ReportCol2Date", expectedDateCol2);
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_HeaderPercentChange() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportPercentChange"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportPercentChange", "% Change");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_HeaderDollarChange() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportDollarChange"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportDollarChange", "$ Change");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeaderTier1_Capital() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_Tier1Cap"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_Tier1Cap", "Tier 1 Capital");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeaderTier2_Capital() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_Tier2Cap"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_Tier2Cap", "Tier 2 Capital");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeader_AvgAssets_LevRatio() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_AvgAssets_LevRatio"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_AvgAssets_LevRatio", "Average Assets for Leverage Ratio");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeader_TotRiskWeighed_MonthEndAssets() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd", 
				"Total Risk Weighted Assets/Month-end Assets");
	}
	
	
	@Test(priority = 11, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_CommonTierDataRatio_Match_CapRatioData() throws Exception {
		CapitalReqReportPage.verifyDataOnCapitalReq_MatchCapRatio(capRatioList, dateOption_CapRatio);
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step4_6_VerifyCapitalReqDifferentDate_CommonTierDataRatio_Match_CapRatioData() throws Exception {
		CapitalReqReportPage.clickRepProperties_SelectNewDateOption(dateOptionCol1_CapReq, dateOptionCol2_CapReq);
		CapitalReqReportPage.verifyDataOnCapitalReq_MatchCapRatio(capRatioList_DiffDate, dateOptionCol1_CapReq);
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_Column1Date() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportCol1Date"));
		String expectedDateCol1 = BaseUI.return_Date_AsDifferentFormat(newDateOption_CapRatio, "MMMM yyyy", "MMM yyyy").trim();
		BaseUI.verifyElementHasExpectedText("capReq_ReportCol1Date", expectedDateCol1);
	}
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_Column2Date() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportCol2Date"));
		String expectedDateCol2 = BaseUI.return_Date_AsDifferentFormat(dateOptionCol2_CapReq, "MMMM yyyy", "MMM yyyy").trim();
		BaseUI.verifyElementHasExpectedText("capReq_ReportCol2Date", expectedDateCol2);
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_HeaderPercentChange() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportPercentChange"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportPercentChange", "% Change");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_HeaderDollarChange() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportDollarChange"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportDollarChange", "$ Change");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_GroupHeaderTier1_Capital() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_Tier1Cap"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_Tier1Cap", "Tier 1 Capital");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_GroupHeaderTier2_Capital() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_Tier2Cap"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_Tier2Cap", "Tier 2 Capital");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_GroupHeader_AvgAssets_LevRatio() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_AvgAssets_LevRatio"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_AvgAssets_LevRatio", "Average Assets for Leverage Ratio");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step6_VerifyCapitalReq_Diff_GroupHeader_TotRiskWeighed_MonthEndAssets() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd", 
				"Total Risk Weighted Assets/Month-end Assets");
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
