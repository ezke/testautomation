/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CustomCalculations;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha 02/02/2017
 * Test # 1038
 * SMOKE TEST: Add Custom Calculation
 *
 */
public class AddCustomCalculations_Single extends BaseTest {
	
	static String valueToSelect = "Number (#)";
	static String customCalcName = "TestCC" + BaseUI.random_NumberAsString(1, 4000);
	static String invalidCalcText = "Invalid Calculation!";
	static String validCalcText = "Valid Calculation!";
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		//Open Browser, Navigate to Bankers Dashboard site
		//Login with valid credentials
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		//Navigate to custom calc page
		CustomCalculations.navigateToAddCustomCalcPage();
	}
	
	@Test(priority = 1, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step1_verifySAveButtonIsDisabledWhenCustomCalcNamefieldIsBlank() throws Exception {
		CustomCalculations.checkSaveButton_IsDisabled();
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_InvalidCalculation_text"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("customCalc_InvalidCalculation_text"), invalidCalcText);
	}
	
	
	@Test(priority = 2, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step2_verifyFormatDropdownField() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_Format_dropdownField"));
	}
	
	
	@Test(priority = 4, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step4_verifyCustomCalculationTextbox() throws Exception {
		CustomCalculations.selectValueForFormat(valueToSelect);
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
	}
	
	
	@Test(priority = 6, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step6_verifyAccountGroupsToExpandIsDisplayed() throws Exception {
		BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName);
		customCalcName = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_AssetsGroupExpand"));
	}
	
	
	@Test(priority = 8, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step8_verifyAccountSubGroupsToExpandIsVisible() throws Exception {
		CustomCalculations.clickToExpandGroups("customCalc_AssetsGroupExpand");
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_CashAndDuesGroup"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step10_verifyAccountsSub_SubGroupsToExpandIsVisible() throws Exception {
		CustomCalculations.clickToExpandGroups("customCalc_CashAndDuesGroup");
	    BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_CashGroupExpand"));
	}
		
	
	@Test(priority = 11, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step11_clickOnAccountsSub_SubGroupsToExpand() throws Exception {
		CustomCalculations.clickToExpandGroups("customCalc_CashGroupExpand");
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_GLAccountList_1"));
	}
	
	
	@Test(priority = 13, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step13_verifySaveButton_IsEnabled() throws Exception {
		BaseUI.dragAndDrop("customCalc_GLAccountList_1", "customCalc_DroppableSpace");
		Thread.sleep(500);
		CustomCalculations.checkSaveButton_IsEnabled();
	}
	
	
	@Test(priority = 14, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step14_clickOnPlusIcon() throws Exception {
		CustomCalculations.clickPlusIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_GLAccountList_2"));
	}

	
	@Test(priority = 16, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step16_verifySaveButtonIsEnabled() throws Exception
	{
		BaseUI.dragAndDrop("customCalc_GLAccountList_2", "customCalc_DroppableSpace");
		Thread.sleep(500);
		CustomCalculations.checkSaveButton_IsEnabled();
	}
	
	
	@Test(priority = 17, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step17_VeifyGLAccountListDisplayed() throws Exception {
		CustomCalculations.clickPlusIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_GLAccountList_3"));
	}
	
	
	@Test(priority = 18, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step18_dragAnotherAccountFromListToCustomCalcSpaceAndVerifySaveButtonIsEnabled() throws Exception {
		BaseUI.dragAndDrop("customCalc_GLAccountList_3", "customCalc_DroppableSpace");
		Thread.sleep(500);
		CustomCalculations.checkSaveButton_IsEnabled();
	}
	
	
	@Test(priority = 19, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step19_verifyValidCalculationTextIsDispalyed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_ValidCalculation_text"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("customCalc_ValidCalculation_text"), validCalcText);
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step20_clickSaveButton() throws Exception {
		CustomCalculations.clickOnSaveButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_Search_Textbox"));
	}
	
	
	@Test(priority = 22, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step22_enterCreatedCustomCalcAndVerifyIsDisplayed() throws Exception {
		CustomCalculations.searchCustomCalc(customCalcName);
		CustomCalculations.searchCustomCalcResultDisplayed();
	}
	
	
	@Test(priority = 23, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step23_VerifySwitchedToConsolidatedBankEntity() throws Exception {
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
		EntitySelector.select_consolidatedBankEntity_fromDropdown(ClientDataRetrieval.consolidatedEntity);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("navigation_AL"));
	}
	
	
	@Test(priority = 24, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step13_verifyCreatedCustomCalcIsNotAvailable() throws Exception {
		CustomCalculations.verifyCustomCalcNotDisplayedAtConsolidatedEntity();
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT1038_Step25_VerifyswitchBackToBankEntity() throws Exception {
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_AL"));
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
 
	
	@AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  CustomCalculations.deleteCustomcalcBeforeLogout(customCalcName);
	      Navigation.navigate_to_signOut();
	  } finally{
	      Browser.closeBrowser();
	  }	   
	}
}