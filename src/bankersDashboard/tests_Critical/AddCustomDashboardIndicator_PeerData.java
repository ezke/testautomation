/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class AddCustomDashboardIndicator_PeerData extends BaseTest {
	
	private static String storePeerDataWidgetName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		StartPage.clickAddWidgetIcon_CustomDashboardIndicatorOption();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT1664_Step2_verifyPeerDataOptionIsDisplayed() throws Exception
	{
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("peerDataOption_label"));
		BaseUI.verifyElementHasExpectedText("peerDataOption_label", "Peer data.");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1664_Step4_verifyPeerDataIndicatorTextbox() throws Exception
	{
		AddWidgets_StartPage.clickPeerDataBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("peerDataIndicatorName_textbox"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 7)
	public void PT1664_Step7_verifyIndicatorMeterStyleText() throws Exception
	{
		storePeerDataWidgetName = AddWidgets_StartPage.addPeerDataIndicatorDetails();
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyIndicatorMeterStyleText"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 9)
	public void PT1664_Step9_clickTypeOfMeterStyleForWidget() throws Exception
	{
		StartPage.clickRulerMeterStye();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("Verify_customCalcIndicatorPreviewText"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1664_Step11_verifyCustomCalcIndicatorPreviewText() throws Exception
	{
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("Verify_customCalcIndicatorPreviewText"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 12)
	public void PT1664_Step12_clickSaveButton() throws Exception
	{
		StartPage.customIndicatorModule_clickSaveButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("addWidgetModalTitle"));
	}
	
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 13)
	public void PT1664_Step13_verifyPeerDataWidgetIsDisplayedOnStartpage() throws Exception
	{
		StartPage.clickStartPageTab();
		WebElement peerDataWidget = Locator.lookupElement("verifypeerDataAdded_startPage");
		BaseUI.scroll_to_element(peerDataWidget);
		BaseUI.verifyElementAppears(peerDataWidget);
		BaseUI.verifyElementHasExpectedText("verifypeerDataAdded_startPage", storePeerDataWidgetName);	
	}
	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable
  {	  
	  try {
		  StartPage.clickStartPageTab(); 
		  StartPage.deleteWidgetFromStartPage("peerDataWidgetDeleteIcon", storePeerDataWidgetName, "deleteAnyCustomWidgets");
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}
