package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.MyDashboardIndicator;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SummaryWidget_ValidateEditFeature extends BaseTest {

	static String balanceIndicator;

	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_SummaryTitle"));
		StartPage.clickSummaryWidgetChevronIcon_Settings();
		balanceIndicator = MyDashboardIndicator.addBalanceIndicator_SummaryWidget();
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step2_VerifySummaryWidgetDisplays() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_SummaryTitle"));
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step2_VerifySummaryWidgetDisplays_NewlyAddedBalanceIndicator() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_BalanceIndicator"));
		BaseUI.verifyElementHasExpectedText("summaryWidget_BalanceIndicator", balanceIndicator);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step3_VerifySummaryReport_ReportTitle() throws Exception {
		StartPage.clickCloseAllTabsButton();
		StartPage.clickStartPageTab();
		//Navigate to Summary Report
		Navigation.navigateToDashboard_Summary();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step3_VerifySummaryReportDisplays_NewlyAddedBalanceIndicator() throws Exception {			
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_BalanceIndicator"));
		BaseUI.verifyElementHasExpectedText("summaryReport_BalanceIndicator", balanceIndicator);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public void PT3781_Step4_VerifyNewlyAddedBalanceIndicatorDoesNotDisplayOnSummaryWidget_WhenDeleted() throws Exception {			
		MyDashboardIndicator.deleteCustomIndicator_AddedToSummaryWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("summaryWidget_BalanceIndicator"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
