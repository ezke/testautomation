package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages_Administration.HomePage_Admin;
import bankersDashboard.pages_Administration.LoginPage_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class AdminLogin extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(GlobalVariables.bDashboard_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		BaseUI.wait_forPageToFinishLoading();

		//Switching to Iframes
		BaseUI.switch_ToIframe_ByTagname("Default");
		BaseUI.switch_ToIframe_ByTagname("Header");

		//BaseUI.verifyElementAppears(Locator.lookupElement("admn_HomePage_BankersDashboardLogo"));	
	}
	
	@Test(priority = 20)
	public void PT3783_Step1_verifyAdmin_Homepage_BankersLogo() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("admn_HomePage_BankersDashboardLogo"));
	}
	
	
	@Test(priority = 30)
	public void PT3783_Step1_verifyAdmin_Homepage_AdministrationText() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("admn_HomePage_AdministrationText"));
	}
	
	
	@Test(priority = 40)
	public void PT3783_Step1_verifyAdmin_Homepage_Username() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("admn_HomePage_Username"));
	}
	
	
	@Test(priority = 10)
	public void PT3783_Step1_verifyAdmin_Homepage_DateTime_LoggedIn() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("admn_HomePage_LoggedIn_DateAndTime"));
		String dateTime_LoggedIn = BaseUI.getTextFromField(Locator.lookupElement("admn_HomePage_LoggedIn_DateAndTime"))
				.replaceAll("PM", "").replaceAll("AM", "");
		String dateTime_LoggedIn_formated = BaseUI.getDate_WithFormat_X_Days_FromInitialString(dateTime_LoggedIn, 
				"MM/dd/yyyy hh:mm:ss", "MM/dd/yyyy hh:mm", 0);
		
		String today_DateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy hh:mm");	
		BaseUI.baseStringCompare("User logged on admin site time and date", today_DateTime, dateTime_LoggedIn_formated);	
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable
  {
	  try {
		  HomePage_Admin.click_Logout();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }
}
