/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.DropDowns;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha
 *
 */
public class Forecasting_OpenExistingProject extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		StartPage.clickStartPageTab();
		Navigation.navigateToProjections_Forecasting();
		
		if(BaseUI.elementAppears(Locator.lookupElement("forecasting_AnotherProjectCreationMsg"))) {
    		BaseUI.log_AndFail("Another Forecasting project is in creation and only one project can be created at a time");
    	} else {
    		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("forecastingProjects_dropdown"), 1);
    	}		
	}
	
	
//	@Test (priority = 10)
//	public void verifyforecastingProjectOpened() throws Exception {
//		storeForecastingProjectSelectedFromDropdown = BaseUI.getSelectedOptionFromDropdown("forecastingProjects_dropdown");
//		BaseUI.verify_Value_Selected_InDropdown(Locator.lookupElement("forecastingProjects_dropdown"), 
//				storeForecastingProjectSelectedFromDropdown);
//	}
	
	@Test (priority = 15)
	public void verifyforecastingProject_DisplaysBSTab() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("forecasting_OpenProject_btn"));
		Thread.sleep(5000);
		BaseUI.switch_ToDefaultContent();
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_BSOnOpenForecastingPage"));
	}
	
	@Test (priority = 20)
	public void verifyforecastingProject_DisplaysISTab() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_ISOnOpenForecastingPage"));
	}
	
	@Test (priority = 25)
	public void verifyforecastingProject_DisplaysBSGroup() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_BSGroup"));
	}
		
	@Test (priority = 30)
	public void verifyforecastingProject_DisplaysBSGroupAssets_Subset() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("forecasting_BSGroup_Assets"));
		Thread.sleep(1000);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("forecasting_BSGroup_Assets_Subset"));
	}	
	
	@Test (priority = 40)
	public void verifyForecastingWindowClosed() throws Exception {
		EntitySelector.selectWindow("Banker's Dashboard");
		EntitySelector.close_ExtraWindows("Banker's Dashboard");
		StartPage.clickStartPageTab();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("startPage_Announcements_text"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable {  
	  try {
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
  }
}
