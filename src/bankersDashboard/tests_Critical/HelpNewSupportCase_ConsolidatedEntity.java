package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Help_SupportModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HelpNewSupportCase_ConsolidatedEntity extends BaseTest {

private static String username_loggedIn, newSupportCaseUsernameDisplayed, companyNameDisplayed;
	
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
				
		//Select the Consolidated Entity
		EntitySelector.select_consolidatedBankEntity_fromDropdown(ClientDataRetrieval.consolidatedEntity);
		
		username_loggedIn = BaseUI.getTextFromField(Locator.lookupElement("startPage_Username_dropdown"));
		//BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Help_txt"));
		StartPage.help_dropDown("New Support Case");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT1942_Step1_verifySupportRequestModalTitle() throws Exception
	{		
		BaseUI.verifyElementAppears(Locator.lookupElement("help_SupportRequestModalTitle"));
		BaseUI.verifyElementHasExpectedText("help_SupportRequestModalTitle", "Support Request");
	}
	
	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
//	public void PT1942_Step2_verifyPhoneFieldsAppears() throws Exception
//	{
//		BaseUI.verifyElementAppears(Locator.lookupElement("helpSupReq_PhoneText"));
//	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT1942_Step2_verifySubjectFieldsAppears() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("helpSupReq_SubjectText"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT1942_Step2_verifyDescriptionFieldsAppears() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("helpSupReq_DescriptionText"));
	}
		
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1942_Step3_verifyUsernameAndCompanyNameDisplayedMatchLoggedInUserAndBankEntity() throws Exception
	{
		newSupportCaseUsernameDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("helpSupReq_Name_Textbox"));
		companyNameDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("helpSupReq_Company_Textbox"));
		BaseUI.baseStringCompare("helpSupReq_Name_Textbox", username_loggedIn, newSupportCaseUsernameDisplayed);
		BaseUI.baseStringPartialCompare("helpSupReq_Company_Textbox", ClientDataRetrieval.profitCenter, 
				companyNameDisplayed);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT1942_Step4_verifyOpenCaseButtonEnabled() throws Exception
	{
		Help_SupportModal.enterCaseDetails_SubjectAndDescription();
		BaseUI.verifyElementEnabled(Locator.lookupElement("helpSupReq_NewSupportCase_OpenCaseBtn"));
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 7)
	public void PT1942_Step8_VerifyNewSupportCaseModal_Closed() throws Exception
	{
		Help_SupportModal.clickCancelButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("help_SupportRequestModalTitle"));
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1942_Step9_verifySupportRequestModalTitle() throws Exception
	{	
		StartPage.help_dropDown("New Support Case");
		BaseUI.verifyElementAppears(Locator.lookupElement("help_SupportRequestModalTitle"));
		BaseUI.verifyElementHasExpectedText("help_SupportRequestModalTitle", "Support Request");
	}	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT1942_Step11_verifySubjectFieldsAppears() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("helpSupReq_SubjectText"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT1942_Step12_verifyDescriptionFieldsAppears() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("helpSupReq_DescriptionText"));
	}
		
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT1942_Step13_verifyUsernameAndCompanyNameDisplayedMatchLoggedInUserAndBankEntity() throws Exception
	{
		newSupportCaseUsernameDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("helpSupReq_Name_Textbox"));
		companyNameDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("helpSupReq_Company_Textbox"));
		BaseUI.baseStringCompare("helpSupReq_Name_Textbox", username_loggedIn, newSupportCaseUsernameDisplayed);
		BaseUI.baseStringPartialCompare("helpSupReq_Company_Textbox", ClientDataRetrieval.profitCenter, 
				companyNameDisplayed);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT1942_Step14_verifyOpenCaseButtonEnabled() throws Exception
	{
		Help_SupportModal.enterCaseDetails_SubjectAndDescription();
		BaseUI.verifyElementEnabled(Locator.lookupElement("helpSupReq_NewSupportCase_OpenCaseBtn"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT1942_Step18_VerifyNewSupportCaseModal_Closed() throws Exception
	{
		Help_SupportModal.clickSendButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_startPageTab"));
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
   
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Exception {
	    try {
	        Navigation.navigate_to_signOut();
	        } finally{
	        	Browser.closeBrowser();
	        }
	 }
}
