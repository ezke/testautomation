package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CustomCalculations;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha 01/26/2017
 * SMOKE TEST: Add Custom Calculation
 *
 */
public class AddCustomCalculations_groups extends BaseTest {
	
	private static String valueToSelect = "Number (#)";
	private static String customCalcName = "TestCC"+BaseUI.random_NumberAsString(1, 4000);
	private static String invalidCalcText = "Invalid Calculation!";
	private static String validCalcText = "Valid Calculation!";
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		//Thread.sleep(2000);
		EntitySelector.imageVerification("loginPage_BD_logo");	
		CustomCalculations.navigateToAddCustomCalcPage();
	}
	
		
	@Test(priority = 1, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step1_verifySaveButtonIsDisabled() throws Exception {
		CustomCalculations.checkSaveButton_IsDisabled();
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_InvalidCalculation_text"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("customCalc_InvalidCalculation_text"), invalidCalcText);
	}
	
	
	@Test(priority = 2, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step2_selectFormatOptionFromDropdown() throws Exception {
		CustomCalculations.selectValueForFormat(valueToSelect);
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
	}
	
	
	@Test(priority = 3, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step3_enterCustomCalcName() throws Exception {	
		BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName);
		customCalcName = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_AssetsGroupExpand"));
	}
	
	
	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step5_dragAndDropGroups() throws Exception {
		CustomCalculations.clickToExpandGroups("customCalc_AssetsGroupExpand");
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_CashAndDuesGroup"));
	}
	
	
	@Test(priority = 6, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step6_verifyValidCalculationText() throws Exception {
		BaseUI.dragAndDrop("customCalc_CashAndDuesGroup", "customCalc_DroppableSpace");
		Thread.sleep(500);
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_ValidCalculation_text"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("customCalc_ValidCalculation_text"), validCalcText);
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step7_verifySaveButtonIsEnabled() throws Exception {
		CustomCalculations.checkSaveButton_IsEnabled();
	}
	
	
	@Test(priority = 9, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step9_verifySearchTextboxIsAvailable() throws Exception {
		CustomCalculations.clickOnSaveButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_Search_Textbox"));
		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step10_VerifyNewlyCreatedCustomCalcIsDisplayed() throws Exception
	{
		CustomCalculations.searchCustomCalc(customCalcName);
		CustomCalculations.searchCustomCalcResultDisplayed();
		System.out.println("Custom Calculation is successfully created");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step12_VerifySwitchedToConsolidatedBankEntity() throws Exception
	{
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
		EntitySelector.select_consolidatedBankEntity_fromDropdown(ClientDataRetrieval.consolidatedEntity);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("navigation_AL"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step13_verifyCreatedCustomCalcIsNotAvailable() throws Exception {
		CustomCalculations.verifyCustomCalcNotDisplayedAtConsolidatedEntity();
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT1050_Step12_VerifySwitchedToBankEntity() throws Exception {
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_AL"));
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  
		  //Delete custom calc created before logout.
		  CustomCalculations.deleteCustomcalcBeforeLogout(customCalcName);
	      Navigation.navigate_to_signOut();
	  } finally{
	      Browser.closeBrowser();
	  }	  
    }
}


	
	 