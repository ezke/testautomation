package bankersDashboard.tests_Critical;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CustomCalculations;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.MyDashboardIndicator;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 *  Test #1042 -
	SMOKE TEST: Add Custom Calculation Indicator to My Dashboard 
	and Validate is displayed on Summary Report
 *
 */

public class CustomCalculation_ValidateOnSummaryReport extends BaseTest {

	static String customCalcName;
		
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to custom calc and create a new custom calculation
		customCalcName = CustomCalculations.addNewCustomCalculation();
		
		//Add newly created custom calc to Summary Widget
		MyDashboardIndicator.addCustomCalcToMyDashbordWidget(customCalcName);
		Navigation.navigateToDashboard_Summary();
	}
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void verify_SummaryReportTitle() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("summaryReport_ReportTitle", "Summary");
	}
			
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void verifyCustomCalcInSummaryReport() throws Exception {	
		WebElement customCalcOnSummaryReport = Locator.lookupElement("summaryReport_customCalc");
		BaseUI.scroll_to_element(customCalcOnSummaryReport);
		BaseUI.verifyElementAppears(customCalcOnSummaryReport);
		BaseUI.verifyElementHasExpectedText(customCalcOnSummaryReport, 
				customCalcName.concat("*"));
	}
		
		
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void closeSummaryReport() throws Exception {		
		StartPage.clickCloseAllTabsButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("summaryReport_ReportTitle"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void verifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
		

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	  
		  
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {	  
		try {
			CustomCalculations.deleteCustomCalculation(customCalcName);
			StartPage.verifyCustomCalculation_DeletedFromSummaryReport(customCalcName);
		    Navigation.navigate_to_signOut();
		}
		finally{
		    Browser.closeBrowser();
		}
   }
	  
}

