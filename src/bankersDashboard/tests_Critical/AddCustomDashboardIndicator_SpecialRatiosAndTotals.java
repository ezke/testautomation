/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class AddCustomDashboardIndicator_SpecialRatiosAndTotals extends BaseTest {
	
	private static String storeSpecialRatiosWidgetName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
			
		StartPage.clickAddWidgetIcon_CustomDashboardIndicatorOption();
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1661_Step2_verifySpecialRatiosOptionIsDisplayed() throws Exception
	{
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("specialRatiosOption_label"));
		BaseUI.verifyElementHasExpectedText("specialRatiosOption_label", "Special ratios and totals.");
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1661_Step4_verifyspecialRatiosIndicatorWidgetTextbox() throws Exception
	{
		AddWidgets_StartPage.clickSpecialRatiosAndTotalsRadioBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("specialRatiosIndicatorName_textbox"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 8)
	public void PT1661_Step8_verifyIndicatorMeterStyleText() throws Exception
	{
		storeSpecialRatiosWidgetName = AddWidgets_StartPage.addSpecialRatiosAndTotalsIndicatorDetails();
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyIndicatorMeterStyleText"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT1661_Step11_verify_CustomCalcIndicatorPreviewText() throws Exception
	{
		AddWidgets_StartPage.clickRulerStyleRadioBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("Verify_customCalcIndicatorPreviewText"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 13)
	public void PT1661_Step13_verifyspecialRatiosWidgetIsDisplayedOnStartpage() throws Exception
	{
		StartPage.customIndicatorModule_clickSaveButton();
		StartPage.clickStartPageTab();
		//BaseUI.scroll_to_element(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyspecialRatiosAdded_startPage"));
		BaseUI.verifyElementHasExpectedText("verifyspecialRatiosAdded_startPage", storeSpecialRatiosWidgetName);	
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable
  {	  
	  try {
		  StartPage.clickStartPageTab();
		  StartPage.deleteWidgetFromStartPage("specialRatiosWidgetDeleteIcon", 
				  storeSpecialRatiosWidgetName, "deleteAnyCustomWidgets");
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}
