package bankersDashboard.tests_Critical;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.ReportPackages;
import bankersDashboard.pages.SetUpReportPackage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * This Test case will modify the Report Package 
 * Created in SetUpReportPackageCreate Test case
 *
 */
public class SetUpReportPackages_Modify extends BaseTest {
	
	private static final String packageName = "TestReportPackage123";
	private static final String reportName_monthlyDashboard = "Monthly Dashboard123";
	private static String reportNameDisplayed;

	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Set up Report package
		//Create New Report package
		ReportPackages.createNewReportPackageWithReports("no");
		
		//Navigate to Set up Report package
		Navigation.navigateToReportPackages_SetUpReportPackages();
		ReportPackages.selectFromCurrentPackage(ReportPackages.storePackageName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT4408_Step2_verifyReportPackageNameCanBeModified() throws Exception {
		SetUpReportPackage.editReportPackage(packageName);
		BaseUI.verifyElementEnabled(Locator.lookupElement("setUpReportPackage_SavBtn"));
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT4408_Step3_clickSaveButton() throws Exception {
		ReportPackages.clickSaveButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("setUpReportPackage_PackageNameLabel"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4408_Step5_VerifyReportNameCanBeModified() throws Exception {
		reportNameDisplayed = SetUpReportPackage.editReport(reportName_monthlyDashboard);
	    ReportPackages.clickSaveButton();
	    BaseUI.verifyElementAppears(Locator.lookupElement("setUpReportPackage_CurrentPackageLabel"));
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 6)
	public void PT4408_Step6_verifyReportNameModified() throws Exception {
	     BaseUI.verifyElementHasExpectedText(Locator.lookupElement("setUpReportPackage_MonthlyDashboardReportNameTxt"), 
	    		 reportName_monthlyDashboard);
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 7)
	public void PT4408_Step7_clickEditReportIcon() throws Exception {
	     ReportPackages.clickEditReportIcon();
	     BaseUI.verifyElementAppears(Locator.lookupElement("setUpReportPackage_ReportNameLabel"));
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 8)
	public void PT4408_Step8_verifyReportNameDoesNotMatches() throws Exception {
	     BaseUI.baseStringCompareStringsAreDifferent("setUpReportPackage_MonthlyDashboardReportName", 
	    		 reportNameDisplayed, reportName_monthlyDashboard);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4408_Step10_verifyReportNameUpdatedToInitiallySetReportName() throws Exception {
		BaseUI.enterText(Locator.lookupElement("setUpReportPackage_MonthlyDashboardReportName"), reportNameDisplayed); //"Monthly Dashboard");
		ReportPackages.clickSaveButton();
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("setUpReportPackage_MonthlyDashboardReportNameTxt"), 
				reportNameDisplayed); 
	}
	
	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
//	public void PT4408_Step10_verifyReportNameCanBeDraggedToTop() throws Exception {
//		
//	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT4408_Step11_clickFinishedButton() throws Exception {
		ReportPackages.clickFinishedButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("setUpReportPackage_ModalTitle"));
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	 
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {
	  try {
		  SetUpReportPackage.deleteReportPackage();
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }
	
	

}

