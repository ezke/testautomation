package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.Al_HistoMonthlyLoansPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class AL_HistoricalMonthlyLoanReportValidation extends BaseTest {

	private static String dateOptionDefault;
	private static String viewOptionDefault;
	private static String loansToView_OptionDefault;
	private static String prodTypeOptionDefault;


	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		EntitySelector.imageVerification("loginPage_BD_logo");
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
		
		//Navigate to AL> Historical Monthly Loan Report
		StartPage.clickStartPageTab();
		Navigation.navigateToAL_Loans_HistoricalMonthlyLoans();
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		
		//store all default options selected on Historical Monthly Loan modal
		dateOptionDefault = BaseUI.getSelectedOptionFromDropdown("historicalMonthlyLoans_Modal_DateDropdown")
				.replaceAll(",", "");
		viewOptionDefault = BaseUI.getSelectedOptionFromDropdown("historicalMonthlyLoans_Modal_ViewDropdown");
		loansToView_OptionDefault = BaseUI.getSelectedOptionFromDropdown("historicalMonthlyLoans_Modal_LoansToViewDropdown");
		
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("historicalMonthlyLoans_Modal_ProdTypeDropdown"), 1);
		prodTypeOptionDefault = BaseUI.getSelectedOptionFromDropdown("historicalMonthlyLoans_Modal_ProdTypeDropdown");
		
		//Check Non Accural Loans checkbox is checked by default
		Al_HistoMonthlyLoansPage.verifyNonAccuralLoans_Checkbox_IsChecked();
		Al_HistoMonthlyLoansPage.clickShowReportBtn();		
	}
	
	
	@Test(priority = 10)
	public void PT3825_Step1_VerifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyHistoricalMonthlyReport_ReportTitle() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_ReportTitle", 
				"Historical Monthly Loans - "+ loansToView_OptionDefault.replaceAll("renewed", "Renewed")+ " as of "+ dateOptionDefault);
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyOnReport_SelectedViewOption_GroupHeader() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_View_GroupHeader"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_View_GroupHeader", 
				viewOptionDefault);
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyOnReport_SelectedProductTypeOption() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_ProductType"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_ProductType", 
				prodTypeOptionDefault);
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyOnReport_SelectedDateOption_ColumnHeader() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_Date_ColHeader"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_Date_ColHeader", 
				dateOptionDefault);
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyOnReport_FixedLoans_ColumnHeader() throws Exception
	{
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_FixedRateLoans_ColHeader"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_FixedRateLoans_ColHeader", 
				"Fixed Rate Loans");
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyOnReport_VariableLoans_ColumnHeader() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_VariableRateLoans_ColHeader"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_VariableRateLoans_ColHeader", 
				"Variable Rate Loans");
	}
	
	
	@Test(priority = 15)
	public void PT3825_Step5_verifyOnReport_TotalLoans_ColumnHeader() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("historicalMonthlyLoans_Report_TotalLoans_ColHeader"));
		BaseUI.verifyElementHasExpectedText("historicalMonthlyLoans_Report_TotalLoans_ColHeader", 
				"Total Loans");
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
