/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha
 * Smoke Test : This test case validates the Balance Sheet compare report's 
 * drill down feature
 *
 */
public class BalanceSheetCompareReport_DrillDown extends BaseTest {
		
	private static String profitCenterSelected, selectedSource, selectedDateOption;
	private static String OptionClickedAtReportLevel, optionClickedAtGroupLevel, optionClickedAtAccountLevel;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to Balance Sheet compare report	
		Navigation.navigateToGL_BS_BalanceSheetCompare();
		profitCenterSelected = BaseUI.getSelectedOptionFromDropdown("balanceSheetCompare_Profit_center_dropdown");
		selectedSource = BaseUI.getSelectedOptionFromDropdown("balanceSheetCompare_Column1_dataSource_dropdown");
		selectedDateOption = BaseUI.getSelectedOptionFromDropdown("balanceSheetCompare_Date_column1_dropdown");
		BalanceSheetComparePage.selectDateValueFromDropdown(selectedDateOption);
	}
	
	
	@Test(priority = 4, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyBankLogoIsDisplayed() throws Exception {
		BalanceSheetComparePage.clickShowReportButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
	}
	
	
	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step5_verifyBalanceSheetCompareReportTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
	}
	
	
	@Test(priority = 6, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step6_verifySelectedProfitCenterIsDisplayed() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenter", profitCenterSelected);
	}
	
	
	@Test(priority = 6, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step6_verifySelectedDateOptionIsDisplayed() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportPrimaryDateSelected", 
				(selectedSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(selectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step7_verifySubTitleHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportSubtitleAssets", "Assets");
	}
	
	
	@Test(priority = 8, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifySelectedDataSourceIsDisplayed() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetCompare_ReportPrimaryDateSelected", (selectedSource));
		//BaseUI.verifyElementHasExpectedPartialText("verifySource_DateSelectedIsDisplayed", DatePicker.verifyDateDisplayed(saveSelectedDateOption));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyTitle_AtGroupDrillDownLevel() throws Exception {
		OptionClickedAtReportLevel = BaseUI.getTextFromField(Locator.lookupElement("balanceSheetCompare_cashAndDuesFromBank_DrillDown"));
	    BalanceSheetComparePage.clickToDrilldownReport("balanceSheetCompare_cashAndDuesFromBank_DrillDown");

		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_GroupDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupDrillDownTitle", "Group Drill Down");
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyProfitCenter_AtGroupDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtGroupDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyOptionClicked_AtGroupDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_OptionClickedforDrillDown_presentAtGroupLevel", 
				OptionClickedAtReportLevel);
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_verifyAccountDrilldownTitle_ProfitCenter() throws Exception {	
		optionClickedAtGroupLevel = BaseUI.getTextFromField(Locator.lookupElement("balanceSheetCompare_cashTeller_groupDrillDown"))
				.trim();
		//Click on 1st option under assets column to check drill down"
		BalanceSheetComparePage.clickToDrilldownReport("balanceSheetCompare_cashTeller_groupDrillDown");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountDrillDownTitle", "Account Drill Down");	
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_VerifyProfitCenter_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtAccountDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_VerifyOptionClicked_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetCompare_OptionClickedAtGroupDrillDown_PresentAtAccountLevel", 
				optionClickedAtGroupLevel);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_verifyProfitCenterDrillDownReportTitle() throws Exception {
		optionClickedAtAccountLevel = BaseUI.getTextFromField(Locator.lookupElement("balanceSheetCompare_cashTeller_AccountDrillDown"));
		//Click on 1st option under assets column to check drill down"
		BalanceSheetComparePage.clickToDrilldownReport("balanceSheetCompare_cashTeller_AccountDrillDown");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ProfitCenterDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterDrillDownTitle", "Profit Center Drill Down");		
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyProfitCenter_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtProfitCenterDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyOptionClicked_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetCompare_OptionClickedAtAccountDrillDown_PresentAtProfitCenterLevel", 
				optionClickedAtAccountLevel);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step15_verifyBackButton_IsEnabled() throws Exception
	{
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_Back_button_Icon"));
	}
	
	
	@Test(priority = 45, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step15_clickBackButtonToGoBack_AccountDrillDownPage() throws Exception
	{
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(priority = 50, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step16_clickBackButtonToGoBack_GroupDrillDownPage() throws Exception
	{
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_GroupDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupDrillDownTitle", "Group Drill Down");
	}
	
	
	@Test(priority = 55, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step17_clickBackButtonToGoBack_BalanceSheetCompareReportPage() throws Exception
	{
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
		
	}
	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable
  {  
	  try {
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}
