package bankersDashboard.tests_Critical;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CapitalReqReportPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.UpdateMemoField;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CapitalRequirementReport_MemoFieldValidation extends BaseTest {
	
	static String dateOption_MemoField = "July 2017";
	static String newDateOption_CapRatio = "July 2016";
	static String dateOptionCol1_CapReq = "July 2016";
	static String dateOptionCol2_CapReq = "June 2016";
	static ArrayList<String> capRatioList;
	static String capReqDateCol2;
	static String defTaxAssets;
	static String tier1CapitalCom;
	static String tier2CapitalCom;
	static String disallowedTangAssets;
	static ArrayList<String> capRatioList_DiffDate;

	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Admin> Manage Data > Update Memo Field
		Navigation.navigateToAdmin_UpdateMemoField();
		UpdateMemoField.selectDateFromDropdown(dateOption_MemoField);
		defTaxAssets = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_CapTab_DisallowedDefTax_AssetsTxtbox"));
		tier1CapitalCom = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_OtherTier1CapitalComp_Txtbox"));
		tier2CapitalCom = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_OtherTier2CapitalComp_Txtbox"));
		disallowedTangAssets = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_DisallowedIntangibleAssets_Txtbox"));
		StartPage.clickCloseAllTabsButton();
		
		//Navigate to GL> Capital Requirements > Capital Requirement report
		Navigation.navigateToGL_CapitalAndRegulatory_CapitalRequirements();
		CapitalReqReportPage.selectCol1_DateOption(dateOption_MemoField);
		capReqDateCol2 = BaseUI.getSelectedOptionFromDropdown("capReq_Modal_Col2DateOption");
		CapitalReqReportPage.clickShowReportBtn();		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_ReportTitle() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportTitle", "Capital Requirements");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_Column1Date() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportCol1Date"));
		String expectedDateCol1 = BaseUI.return_Date_AsDifferentFormat(dateOption_MemoField, "MMMM yyyy", "MMM yyyy").trim();
		BaseUI.verifyElementHasExpectedText("capReq_ReportCol1Date", expectedDateCol1);
	}
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_Column2Date() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportCol2Date"));
		String expectedDateCol2 = BaseUI.return_Date_AsDifferentFormat(capReqDateCol2, "MMMM yyyy", "MMM yyyy").trim();
		BaseUI.verifyElementHasExpectedText("capReq_ReportCol2Date", expectedDateCol2);
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_HeaderPercentChange() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportPercentChange"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportPercentChange", "% Change");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_HeaderDollarChange() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportDollarChange"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportDollarChange", "$ Change");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeaderTier1_Capital() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_Tier1Cap"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_Tier1Cap", "Tier 1 Capital");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeaderTier2_Capital() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_Tier2Cap"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_Tier2Cap", "Tier 2 Capital");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeader_AvgAssets_LevRatio() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_AvgAssets_LevRatio"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_AvgAssets_LevRatio", "Average Assets for Leverage Ratio");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_GroupHeader_TotRiskWeighed_MonthEndAssets() {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd"));
		BaseUI.verifyElementHasExpectedText("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd", 
				"Total Risk Weighted Assets/Month-end Assets");
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_DefferesTaxAssetsValue_MatchMemoField() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_Report_DisallowedDefTax_Assets"));
		String capRequ_DisallowedDefTax_Assets = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_DisallowedDefTax_Assets"))
				.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(",", "").trim();
		if(defTaxAssets.equals("0")){
			BaseUI.verifyElementHasExpectedText("capReq_Report_DisallowedDefTax_Assets", "-");
		} else {
			BaseUI.baseStringCompare("capReq_Report_DisallowedDefTax_Assets", 
					defTaxAssets, capRequ_DisallowedDefTax_Assets);
		}		
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_Tier1CapitalComp_MatchMemoField() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_Report_Tier1CapitalComp"));
		String capRequ_Tier1CapitalComp = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_Tier1CapitalComp"))
				.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(",", "").trim();
		
		if(tier1CapitalCom.equals("0")) {
			BaseUI.verifyElementHasExpectedText("capReq_Report_Tier1CapitalComp", "-");
		} else {
			BaseUI.baseStringCompare("capReq_Report_Tier1CapitalComp", 
					tier1CapitalCom, capRequ_Tier1CapitalComp);
		}		
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_Tier2CapitalComp_MatchMemoField() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_Report_Tier2CapitalComp"));
		String capRequ_Tier2CapitalComp = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_Tier2CapitalComp"))
				.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(",", "").trim();
		if(tier2CapitalCom.equals("0")) {
			BaseUI.verifyElementHasExpectedText("capReq_Report_Tier2CapitalComp", "-");
		} else {
			BaseUI.baseStringCompare("capReq_Report_Tier2CapitalComp", 
					tier2CapitalCom, capRequ_Tier2CapitalComp);
		}			
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT1485_Step3_VerifyCapitalReq_DisallowedIntangibleAssets_MatchMemoField() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_Report_DisallowedIntangibleAssets"));
		String capRequ_DisallowedIntangibleAssets = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_DisallowedIntangibleAssets"))
				.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(",", "").trim();
		if(disallowedTangAssets.equals("0")) {
			BaseUI.verifyElementHasExpectedText("capReq_Report_DisallowedIntangibleAssets", "-");
		} else {
			BaseUI.baseStringCompare("capReq_Report_DisallowedIntangibleAssets", 
					disallowedTangAssets, capRequ_DisallowedIntangibleAssets);
		}		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}

}
