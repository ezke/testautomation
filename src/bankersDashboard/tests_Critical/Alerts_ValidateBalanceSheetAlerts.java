package bankersDashboard.tests_Critical;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.Settings_MyAlertsModal;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.StartPage_AlertsModal;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Alerts_ValidateBalanceSheetAlerts extends BaseTest {

	static ArrayList<String> totalBSAlertsList;
	static ArrayList<String> bsAlert_dollarValue;
	static ArrayList<String> bsAlert_PercentValue;
	static String alertChangeValue = "0.20";
	static String resetValue = "5.00";
	
	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		EntitySelector.imageVerification("loginPage_BD_logo");
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
		
		StartPage.clickMyAlertsBellIcon();
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_BalanceSheetTab_NoAlertsMessage"))) {
			StartPage_AlertsModal.clickCloseAlertsModal();
			Settings_MyAlertsModal.updateAlertPercentValue_BalanceSheet(alertChangeValue);
			StartPage.clickMyAlertsBellIcon();
			StartPage_AlertsModal.clickRefreshIcon();
			StartPage_AlertsModal.clickBalanceSheetTab();
			BaseUI.wait_forElementToNotExist("alerts_BalanceSheetTab_NoAlertsMessage", null, null);
		}
		
		
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_BalanceSheetTab_NoAlertsMessage"))) {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("alerts_BalanceSheetTab_ViewBSBtn"));
			BaseUI.log_AndFail("Balance Sheet Alerts are not set");
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("alerts_BalanceSheetTab_ViewBSBtn"));
			//Get Total Balance Sheet alerts displayed
			totalBSAlertsList = StartPage_AlertsModal.return_BalanceSheetAlertsList();
			BaseUI.verify_true_AndLog(totalBSAlertsList.size() > 0, "BS compare Column1 list Displayed", 
					"BS compare Column1 list IS NOT Displayed");
			
			//Get Balance sheet alerts dollar value
			bsAlert_dollarValue = StartPage_AlertsModal.return_BalanceSheetAlertsDollarValueList();
			BaseUI.verify_true_AndLog(bsAlert_dollarValue.size() > 0, "BS compare Dollar value list Displayed", 
					"BS compare Dollar value list IS NOT Displayed");
			
			//Get Balance sheet percentage value displayed
			bsAlert_PercentValue = StartPage_AlertsModal.return_BalanceSheetAlertsPercentValueList();
			BaseUI.verify_true_AndLog(bsAlert_PercentValue.size() > 0, "BS compare percentage value list Displayed", 
					"BS compare percentage value list IS NOT Displayed");
		}		
	}
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_ModalTitle() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_TodaysAlertsTitle"));
		BaseUI.verifyElementHasExpectedText("alerts_TodaysAlertsTitle", 
				"Today's Alerts");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_AlertsRefreshIcon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_RefreshIcon"));
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_BalanceSheetTab() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_BalanceSheetTab"));
		BaseUI.verifyElementHasExpectedPartialText("alerts_BalanceSheetTab", 
				"Balance Sheet");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step2_VerifyAlerts_BalanceSheetAlertsExists() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_BalanceSheetTab_BSAlertsNum"));
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step2_VerifyAlerts_BSAlertsTab_ViewBalanceSheetBtn() throws Exception {
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_BalanceSheetTab_BSAlertsNum"))){
			BaseUI.verifyElementAppears(Locator.lookupElement("alerts_BalanceSheetTab_ViewBSBtn"));
		}
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step3_VerifyAlerts_BSAlertsTab_AlertsNum_MatchesAlertsDisplayed() throws Exception {
		BalanceSheetComparePage.verifyBalanceSheetAlert_AlertsNum_MatchesAlertsDisplayed();	
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyBSAlertsTab_VerifyBSReportDisplays() throws Exception {
		StartPage_AlertsModal.clickBalanceSheetTab();
		StartPage_AlertsModal.clickViewBalanceSheetButton();
		
		BaseUI.waitForElementToBeDisplayed("startPage_startPageTab2", null, null, 1000);
		BaseUI.verifyElementHasExpectedText("startPage_startPageTab2", "BS Compare");
		
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_verifyBankLogoIsDisplayed() throws Exception {
		//validate bank logo is displayed on Report page
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step5_verifyBalanceSheetCompareReportTitle() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step6_verifySelectedProfitCenterIsDisplayed() throws Exception {
		//validate profit center match the selected
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ProfitCenter"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenter", "Consolidated");
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step7_VerifyBSAlertsTab_BSReportDollarValue_MatchesAlertsDollarValueDisplayed() throws Exception {
		BalanceSheetComparePage.verifyBalanceSheet_DollarValue(totalBSAlertsList, 
				bsAlert_dollarValue);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step7_VerifyBSAlertsTab_BSReportPercentValue_MatchesAlertsPercentValueDisplayed() throws Exception {
		BalanceSheetComparePage.verifyBalanceSheet_PercentValue(totalBSAlertsList, 
				bsAlert_PercentValue);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	Settings_MyAlertsModal.updateAlertPercentValue_BalanceSheet(resetValue);
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
