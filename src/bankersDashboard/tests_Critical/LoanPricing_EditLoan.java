package bankersDashboard.tests_Critical;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoanPricing_AddLoanPage;
import bankersDashboard.pages.LoanPricing_RiskWeightingsModal;
import bankersDashboard.pages.LoanTypesModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class LoanPricing_EditLoan extends BaseTest {
	
	static String numberPerPageSelected;
	static ArrayList<String> loanIDsList;
	static String loanIDsPerPage = "25";
	static String numberOfLoansDisplayedMsg;
	static String loanIdentifier = "QAAutomationTest"+BaseUI.random_NumberAsString(1, 30000);
	static String loanIdentifierName = loanIdentifier;
	static String refSource = "Tester";
	static String loanAmount = "100000";
	static String propRate = "3.25";
	static String term = "60";
	static String avgLife = "60";
	static String loanFees = "1000";
	static String loanType = "Home Equity";
	static String riskWeightingOption = "Risk Weighting 3";
	static String appType = "New";
	static String rateType = "Fixed";
	static String loanOfficer;
	static String loanStatusWhenLoanAdded = "In Process";
	static String loanStatusUpdated = "Commitment Accepted";
	static String projectedCloseDateValue = BaseUI.getDateAsString_InRelationToTodaysDate(400);
	static String winProbabilityValue = "50";
	static ArrayList<String> updatedLoanDetails;


	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		StartPage.clickStartPageTab();
		//Add Risk Weighting type before adding Loan Type
		//because Loan Type asks for Risk Weighting type
		LoanPricing_RiskWeightingsModal.newRiskWeighting();
				
		//Validate if Loan Type exists, if yes continue with Adding new Loan
		//else add new loan type first and then add new loan
		LoanTypesModal.newLoanType();
				
		//Navigate to Loan pricing model
		Navigation.navigateToLoanPricing_LoanPricingModel();
		loanOfficer = LoanPricing_AddLoanPage.addLoan(loanIdentifierName, refSource, loanAmount, 
				propRate, term, avgLife, loanFees, loanType, riskWeightingOption, appType, rateType);
		LoanPricing_AddLoanPage.clickAddLoanPage_ReturnToLoanPipelineLink();
	}
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_PageTitle() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_PageTitle"));
		BaseUI.verifyElementHasExpectedText("loanPricing_LoanPricingModel_PageTitle", 
				"Loan Pricing Pipeline");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_BankLogo() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_InactivePipelineHeader() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_InactivePipelineSubHeader"));
		BaseUI.verifyElementHasExpectedPartialText("loanPricing_LoanPricingModel_InactivePipelineSubHeader", 
				"Inactive Pipeline");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanDetails() {
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanDetailsMatchTheEnteredValues(loanIdentifierName, loanAmount, 
				loanType, propRate, refSource, loanOfficer, loanStatusWhenLoanAdded);
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanIdentifier() throws Exception {
		LoanPricing_AddLoanPage.clickLoanEditIcon_UpdateLoanStatus(loanStatusUpdated, projectedCloseDateValue, 
				winProbabilityValue);
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanIDMatchTheEnteredLoanIdentifier(updatedLoanDetails, 
				loanIdentifierName);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_InactivePipelineDoesNotDisplayUpdateLoanStatus() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_UpdatedLoan"));
		BaseUI.verifyElementDoesNotHaveExpectedText("loanPricing_LoanPricingModel_UpdatedLoan", loanIdentifierName);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanOfficer() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanOfficerMatchTheEnteredLoanOfficer(updatedLoanDetails, 
				loanOfficer);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanAmount() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanAmountMatchTheEnteredLoanAmount(updatedLoanDetails, 
				loanAmount);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanType() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanTypeMatchTheEnteredLoanType(updatedLoanDetails,
				loanType);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanProposedRate() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanProposedRateMatchTheEnteredProposedRate(updatedLoanDetails,
				propRate);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanCreatedDate() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanCreatedDateMatchTodaysDate(updatedLoanDetails, 
				BaseUI.getTodaysDate_AsString());
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanWinProbabilityUpdated() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanWinProbabilityMatch(updatedLoanDetails, 
				winProbabilityValue);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanStatusUpdated() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanStatusMatch(updatedLoanDetails, loanStatusUpdated);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanReferralSource() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanReferalMatchEnteredReferalSource(updatedLoanDetails,
				refSource);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NewlyAddedLoanStatusUpdated_LoanProjectedCloseDateUpdated() throws Exception {
		updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanProjectedCloseDateMatch(updatedLoanDetails, 
				projectedCloseDateValue);
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	LoanPricing_AddLoanPage.deleteUpdatedLoan_ClickOk();
	    	StartPage.clickCloseAllTabsButton();
	    	LoanPricing_RiskWeightingsModal.deleteRiskWeighting();
	    	LoanTypesModal.deleteLoanType();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
