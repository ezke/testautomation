package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.ReportPackages;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class RunReportPackagesToScreen_Consolidated extends BaseTest {
	
	static String repPackageName;
		
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Set up Report package
		//Create New Report package
		repPackageName = ReportPackages.createNewReportPackageWithReports("no");
		
		//Navigate to Run Report packages
		Navigation.navigateToReportPackages_RunReportPackage();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1035_Step2_verifyAndscrollToConsolidatedProfitCenterCheckbox() throws Exception {
		ReportPackages.verifyProfitCenter_selectRepPackageAnd(repPackageName,
				"runReportPackage_ProfitCenters_Checkbox_Consolidated");			
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1035_Step4_clickRunPackageButton() throws Exception {
		ReportPackages.selectProfitCenterCheckBox_consolidated();
		ReportPackages.clickRunPackageButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("runReportPackage_ModalTitle"));
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT1035_Step5_verifyReprotPackageRunOnScreen() throws Exception {
		ReportPackages.verifyReportPackagesRunToScreen();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 8)
	public void PT1035_Step8_verifyReportPackageIsDiplayedInFavoriteWedgetBox() throws Exception
	{
		 ReportPackages.verifyReportPackageOnMyFavWidget();
		 /* BaseUI.switch_ToParentWindow();
		    StartPage.ClickStartPageTab();
		    BaseUI.click(Locator.lookupElement("startPageTab2"));
		    Thread.sleep(500);
		    BaseUI.verifyElementHasExpectedText("startPageTab2", ReportPackages.reportNameDisplayed1);
		    BaseUI.verifyElementHasExpectedText("verifyMonthlyDashboardReportTitle", ReportPackages.reportNameDisplayed1);
		    Thread.sleep(1000);
		    BaseUI.click(Locator.lookupElement("startPageTab3"));
		    Thread.sleep(500);
		    BaseUI.verifyElementHasExpectedText("startPageTab3", "Capital Reqs");
		    BaseUI.verifyElementHasExpectedText("verifyCapitalRequirementsReportTitle", ReportPackages.reportNameDisplayed2);
		    Thread.sleep(1000);
		   */
	}
		   	
		 
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		 ResultWriter.checkForFailureAndScreenshot(result);
	}
	  
	    
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
		try {
			 StartPage.clickCloseAllTabsButton();
			 ReportPackages.deleteReportPackageAfterExecuting(repPackageName);
		     Navigation.navigate_to_signOut();
		}finally{
		     Browser.closeBrowser();
		}		  
	}
}
