package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;

public class Forecasting_CreateNewProjectByBank extends BaseTest {
	
	private  String fcName = "QaAutomation_FC"+BaseUI.random_NumberAsString(0, 1000);
	private  String bankOrCenterOption = "Total Bank";
	private  int primProfCenter = 1;
	private  int cashBalAccOption = 1;
	private  int retEarningAccOption = 1;
	private  int begOfYearBalanceAcct = 1;
	private  int isProvAcct = 1;
	private  int bsProvAcct = 1;
	private  int syncThruDate = 4;
	
	//Email Validation Variables
	String email = GlobalVariables.emailAddress;
	Email firstEmail;
	String emailBodyContent;
	String emailSubject;

	private  String expEmailSubject = " Your Forecast project is ready";
	private  String emailCreationDate;
	private  String expEmailBodyContent = "The forecast project named '"+fcName+"' is ready to be edited.";


	@BeforeClass(alwaysRun = true)
	public  void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
	
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		StartPage.clickStartPageTab();
		ForecastingPage.addNewFCProjectDetails(fcName, bankOrCenterOption, primProfCenter, cashBalAccOption, retEarningAccOption, 
				begOfYearBalanceAcct, isProvAcct, bsProvAcct, syncThruDate);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfoPage_FCName"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfoPage_FCName", fcName);
    }
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_CashBalAcc() {
		ForecastingPage.verifyNewCreatedForecastingProject_CashBalAcc();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_RetainedEarningAcc() {
		ForecastingPage.verifyNewCreatedForecastingProject_RetainedEarningAcc();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_BegOfYearBalanceAcct() {
		ForecastingPage.verifyNewCreatedForecastingProject_BegOfYearBalanceAcct();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_BSProvisionAcct() {
		ForecastingPage.verifyNewCreatedForecastingProject_BSProvisionAcct();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_ISProvisionAcct() {
		ForecastingPage.verifyNewCreatedForecastingProject_ISProvisionAcct();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_SyncThruDate() {
		ForecastingPage.verifyNewCreatedForecastingProject_SyncThruDate();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1562_Step2_VerifyNewCreatedForecastingProject_PrimProfitCenter() {
		ForecastingPage.verifyNewCreatedForecastingProject_PrimaryProfitCenter();
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public  void PT1562_Step3To4_VerifyNewCreatedForecastingProject_SmartFillPage() throws Exception {
		ForecastingPage.enterSmartFillData();
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ForecastingProjReadyMsg"));
		BaseUI.verifyElementHasExpectedPartialText("forecasting_CreateNewFC_ForecastingProjReadyMsg", 
				"Preparing Project");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public  void PT1562_Step3To4_VerifyForecastingProject_NewFCProject_EmailReceived() throws Exception {
		ForecastingPage.clickCloseForecastingProjectReadyModal();
    	Email_API.Wait_ForEmail_ToExist(email, 900);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");		
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public  void PT1562_Step3To4_VerifyForecastingProject_NewFCProject_EmailSubject() throws Exception {
		emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		BaseUI.verify_true_AndLog(emailSubject.equals(expEmailSubject), 
				"Email subject displays Forecast message: "+ emailSubject, 
				 "Email subject DOES NOT display Forecast message: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public  void PT1562_Step3To4_VerifyForecastingProject_NewFCProject_EmailCreationDate() throws Exception {
		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
				 "Email Creation date MATCHES System date: "+ String.valueOf(emailCreationDate), 
				 "Email Creation date DOES NOT MATCHES System date: "+ String.valueOf(emailCreationDate));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public  void PT1562_Step3To4_VerifyForecastingProject_NewFCProject_EmailContent() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		BaseUI.verify_true_AndLog(emailBodyContent.contains(expEmailBodyContent), "Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+emailBodyContent);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  Email_API.delete_Emails_ByRecipient(email);
		  ForecastingPage.selectExistingFC_SelectFileMenu_DeleteProject(fcName);
	      //Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
