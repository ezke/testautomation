package bankersDashboard.tests_Critical;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoanPricing_AddLoanPage;
import bankersDashboard.pages.LoanPricing_RiskWeightingsModal;
import bankersDashboard.pages.LoanTypesModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class LoanPricing_AddLoan extends BaseTest {

	static String numberPerPageSelected;
	static ArrayList<String> loanIDsList;
	static String loanIDsPerPage = "25";
	static String numberOfLoansDisplayedMsg;
	static String loanIdentifier = "QAAutomationTest"+BaseUI.random_NumberAsString(1, 30000);
	static String refSource = "Tester";
	static String loanAmount = "100000";
	static String propRate = "3.25";
	static String term = "60";
	static String avgLife = "60";
	static String loanFees = "1000";
	static String loanType = "Home Equity";
	static String riskWeightingOption = "Risk Weighting 3";
	static String appType = "New";
	static String rateType = "Fixed";
	static String loanStatus =  "In Process";
	private static String selectNumOfLoanIdsPerPage = "10";
	static String loanOfficer;
	static String winProbability = "0";
	static String projectedCloseDate = "";
	static ArrayList<String> newLoanDetails;


	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		StartPage.clickStartPageTab();
		
		//Add Risk Weighting type before adding Loan Type
		//because Loan Type asks for Risk Weighting type
		LoanPricing_RiskWeightingsModal.newRiskWeighting();
		
		//Validate if Loan Type exists, if yes continue with Adding new Loan
		//else add new loan type first and then add new loan
		LoanTypesModal.newLoanType();
		
		//Navigate to Loan pricing model
		Navigation.navigateToLoanPricing_LoanPricingModel();
		loanOfficer = LoanPricing_AddLoanPage.addLoan(loanIdentifier, refSource, loanAmount, 
				propRate, term, avgLife, loanFees, loanType, riskWeightingOption, appType, rateType);
		LoanPricing_AddLoanPage.clickAddLoanPage_ReturnToLoanPipelineLink();
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_PageTitle() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_PageTitle"));
		BaseUI.verifyElementHasExpectedText("loanPricing_LoanPricingModel_PageTitle", 
				"Loan Pricing Pipeline");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_BankLogo() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_InactivePipelineHeader() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_InactivePipelineSubHeader"));
		BaseUI.verifyElementHasExpectedPartialText("loanPricing_LoanPricingModel_InactivePipelineSubHeader", 
				"Inactive Pipeline");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_DateMonthDisplayed() throws Exception {
		//commented these lines for now, will update later.
		//if(BaseUI.elementExists("loanPricing_LoanPricingModel_Month_NumberOfLoansSubHeader", null, null)) {
			BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_Month_NumberOfLoansSubHeader"));
		//}
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_TotalLoanAmountDisplayed() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("loanPricing_LoanPricingModel_TotalLoanAmount"));
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_TotalLoanAmount"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NumberOfLoanIDDisplayedMessage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("loanPricing_LoanPricingModel_NumberOfItemsPerPageText"));
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_NumberOfItemsPerPageText"));
		numberOfLoansDisplayedMsg = BaseUI.getTextFromField(Locator.lookupElement("loanPricing_LoanPricingModel_NumberOfItemsPerPageText"));
		numberPerPageSelected = BaseUI.getSelectedOptionFromDropdown("loanPricing_LoanPricingModel_NumberOfItemsPerPageDropdown");
		BaseUI.baseStringPartialCompare("loanPricing_LoanPricingModel_NumberOfItemsPerPageText", 
				"Showing 1 to "+ numberPerPageSelected, numberOfLoansDisplayedMsg);
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NumberOfLoanIDDisplayed_SelectedPerPageOption() throws Exception {	
		LoanPricing_AddLoanPage.selectNumberOfLoanIDs_EntriesPerPage(selectNumOfLoanIdsPerPage);
		LoanPricing_AddLoanPage.verifyLoansPerPageMatchSelectedToDisplay();
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricing_NumberOfLoanIDDisplayed_DifferentSelectedPerPageOption() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("loanPricing_LoanPricingModel_NumberOfItemsPerPageDropdown"));
		LoanPricing_AddLoanPage.selectNumberOfLoanIDs_EntriesPerPage(loanIDsPerPage);
		LoanPricing_AddLoanPage.verifyLoansPerPageMatchSelectedToDisplay();
	}

	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanIdentifier() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanIDMatchTheEnteredLoanIdentifier(newLoanDetails, loanIdentifier);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanOfficer() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanOfficerMatchTheEnteredLoanOfficer(newLoanDetails, loanOfficer);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanAmount() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanAmountMatchTheEnteredLoanAmount(newLoanDetails, loanAmount);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanType() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanTypeMatchTheEnteredLoanType(newLoanDetails, loanType);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanProposedRate() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanProposedRateMatchTheEnteredProposedRate(newLoanDetails, propRate);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanCreatedDate() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanCreatedDateMatchTodaysDate(newLoanDetails, BaseUI.getTodaysDate_AsString());
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanWinProbability() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanWinProbabilityMatch(newLoanDetails, winProbability);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanStatus() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanStatusMatch(newLoanDetails, loanStatus);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanReferralSource() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanReferalMatchEnteredReferalSource(newLoanDetails, refSource);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_NewlyAddedLoan_LoanProjectedCloseDate() {
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanProjectedCloseDateMatch(newLoanDetails, projectedCloseDate);
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT3782_Step2_VerifyLoanPricingModal_LoanNotDeleted_OnCancel() throws Exception {
		LoanPricing_AddLoanPage.deleteLoan_ClickCancel();
		newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		LoanPricing_AddLoanPage.verifyNewlyAddedLoanIDMatchTheEnteredLoanIdentifier(newLoanDetails, loanIdentifier);
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	LoanPricing_AddLoanPage.deleteLoan_ClickOk();
	    	StartPage.clickCloseAllTabsButton();
	    	LoanPricing_RiskWeightingsModal.deleteRiskWeighting();
	    	LoanTypesModal.deleteLoanType();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
