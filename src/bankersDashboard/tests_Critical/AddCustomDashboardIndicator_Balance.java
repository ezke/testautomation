/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class AddCustomDashboardIndicator_Balance extends BaseTest {
	
	private static String storeBalancesWidgetName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		StartPage.clickAddWidgetIcon_CustomDashboardIndicatorOption();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1660_Step2_verifyBalancesOptionIsDisplayed() throws Exception {
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("balancesOption_label"));
		BaseUI.verifyElementHasExpectedText("balancesOption_label", "Balances.");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1660_Step4_verifyBalancesIndicatorWidgetTextbox() throws Exception {
		AddWidgets_StartPage.clickBalanceRadioBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("balancesIndicatorName_textbox"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 8)
	public void PT1660_Step8_verifyIndicatorMeterStyleText() throws Exception {
		storeBalancesWidgetName = AddWidgets_StartPage.addBalanceIndicatorDetails();
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyIndicatorMeterStyleText"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT1660_Step11_verifyCustomCalcIndicatorPreviewText() throws Exception {
		AddWidgets_StartPage.clickBetaMeterStyleRadioBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("Verify_customCalcIndicatorPreviewText"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 13)
	public void PT1660_Step13_verifySupplementalWidgetIsDisplayedOnStartpage() throws Exception {
		StartPage.customIndicatorModule_clickSaveButton();
		StartPage.clickStartPageTab();
		//BaseUI.scroll_to_element(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
		BaseUI.verifyElementHasExpectedText("verifyBalancesIndicatorAdded_startPage", storeBalancesWidgetName);		
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable {  
	  try {
		  StartPage.clickStartPageTab();
		  StartPage.deleteWidgetFromStartPage("balancesWidgetDeleteIcon", storeBalancesWidgetName, "deleteAnyCustomWidgets");
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}
