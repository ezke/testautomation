package bankersDashboard.tests_Critical;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mailosaur.model.Attachment;
import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.ReportPackages;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;


public class RunReportPackagesToEmail extends BaseTest {
		
	WebElement emailRadioBtn, pdfRadioBtn;
	private String email = GlobalVariables.emailAddress;
	private Attachment[] emailAttachments;
	private String emailSubject;
	private Email firstEmail;
	private int numOfEmailAttachments;
	private String emailCreationDate;
	private String emailBodyContent;
	String repPackageName;
	private String todayDate;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
		
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Set up Report package
		//Create New Report package
		repPackageName = ReportPackages.createNewReportPackageWithReports("no");
		
		//Navigate to Run Report packages
		Navigation.navigateToReportPackages_RunReportPackage();
		//ReportPackages.verify_RunReportPackage_ModalTitle();
		ReportPackages.selectRunReportPackage(repPackageName);
	}
	

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1607_Step2_verifyAndScrollToElement() throws Exception
	{
		BaseUI.scroll_to_element(Locator.lookupElement("runReportPackage_ProfitCenter_CheckBox_Region"));
	    BaseUI.verifyElementAppears(Locator.lookupElement("runReportPackage_ProfitCenter_CheckBox_Region"));
	}
	
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT1607_Step3_VerifyProfitCenterCheckboxDiaplayed() throws Exception
	{
		 ReportPackages.selectProfitCenterCheckBox_region();
		 BaseUI.verifyCheckboxStatus(Locator.lookupElement("runReportPackage_ProfitCenter_CheckBox_Region"), 
				 true);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1607_Step4_verifyEmailRadioButton() throws Exception
	{
		 BaseUI.waitForElementToBeDisplayed("runReportPackage_Format_ScreenOption_RadioButton", null, null);
		 emailRadioBtn = Locator.lookupElement("runReportPackage_Format_ScreenOption_RadioButton");
		 BaseUI.verifyElementAppears(emailRadioBtn);
		 BaseUI.verifyElementEnabled(emailRadioBtn);
	}
	
		
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT1607_Step5_clickEmailRadioButton() throws Exception
	{
		ReportPackages.clickEmailRadioBtn();
		BaseUI.verifyElementAppears(Locator.lookupElement("runReportPackage_FileTypeExcel_RadioButton"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 7)
	public void PT1607_Step7_verifyPdfRadioButton() throws Exception {
		ReportPackages.clickExcelFileType_RadioBtn();
		pdfRadioBtn = Locator.lookupElement("runReportPackage_FileTypePDF_RadioButton");
		BaseUI.verifyElementAppears(pdfRadioBtn);
		BaseUI.verifyElementEnabled(pdfRadioBtn);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 9)
	public void PT1607_Step9_VerifyRunPackage_EmailReceived() throws Exception
	{
		ReportPackages.clickPdfFileType_RadioBtn();
		ReportPackages.clickRunPackageButton();
		Email_API.Wait_ForEmail_ToExist(email, 180);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT1607_Step9_VerifyEmailContainsReportPackageAttachment() throws Exception {
		emailAttachments = Email_API.return_EmailAttachments_FromEmail(firstEmail);
		todayDate = BaseUI.getTodaysDate_AsString();		
		if(todayDate.startsWith("0")){
			todayDate = BaseUI.return_Date_AsDifferentFormat(todayDate, "M/d/yyyy", "M-dd-yyyy");
		} else {
			todayDate = BaseUI.return_Date_AsDifferentFormat(todayDate, "MM/d/yyyy", "MM-dd-yyyy");
		}
		BaseUI.verify_true_AndLog(emailAttachments[0].fileName.contains(repPackageName+"-"+todayDate+".pdf"), 
				"Attachments FOUND and MATCHES Report Package Name: "+emailAttachments[0].fileName, 
				"Attachments are NOT FOUND: "+emailAttachments[0].fileName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1607_Step9_VerifyEmailContains_OneReportPackageAttachment() throws Exception {
		numOfEmailAttachments = Email_API.return_EmailNumOfAttachments_FromEmail(firstEmail);
		System.out.println("emailAttachments: " +numOfEmailAttachments);
		BaseUI.verify_true_AndLog(numOfEmailAttachments > 0, "Attchments FOUND: "+ numOfEmailAttachments, 
				"Attachments are NOT FOUND: "+ numOfEmailAttachments);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1607_Step9_VerifyReceivedEmailSubject() throws Exception {
		 emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(emailSubject.equals("PDF Attached"), "Email subject displays PDF attached message: "+emailSubject, 
				 "Email subject DOES NOT display PDF attached message: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1607_Step9_VerifyReceivedEmail_CreationDate() throws Exception {
		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
				 "Email Creation date MATCHES System date: "+ String.valueOf(emailCreationDate), 
				 "Email Creation date DOES NOT MATCHES System date: "+ String.valueOf(emailCreationDate));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1607_Step9_VerifyEmailContainsMessage_DocAttached() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		System.out.println("emailBodyContent: " +emailBodyContent);
		BaseUI.verify_true_AndLog(emailBodyContent.contains("The document you requested is attached"), 
				"Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+ emailBodyContent);
	}
		
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1607_Step10_verifyEmailReceiveMessage() throws Exception {
		 StartPage.clickCloseAllTabsButton();
		 BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		 BaseUI.verifyElementHasExpectedText("runReportPackage_EmailReceiveMessage", 
				 GlobalVariables.runReportEmailMessage);
	}
	
		 
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		 ResultWriter.checkForFailureAndScreenshot(result);

	}
	  
	  	  
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
		try {
			  Email_API.delete_Emails_ByRecipient(email);
			  BaseUI.switch_ToDefaultContent();
			  ReportPackages.clickCloseModalIconToCloseRunReportPackageModal();
			  ReportPackages.deleteReportPackageAfterExecuting(repPackageName);
		      Navigation.navigate_to_signOut();
		} finally{
		      Browser.closeBrowser();
		}
	}
}
