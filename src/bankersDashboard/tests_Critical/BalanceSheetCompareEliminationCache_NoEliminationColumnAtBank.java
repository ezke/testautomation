/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BalanceSheetCompareEliminationCache_NoEliminationColumnAtBank extends BaseTest {

	static String customCalcName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		Navigation.navigateToSettings_MyCenters();
		
		//Navigate to Balance Sheet Compare report
		Navigation.navigateToGL_BS_BalanceSheetCompare();
		//BalanceSheetComparePage.verifyBalanceSheetCompareModalTitle();		
	}
	
	
	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public void PT1461_Step3_VerifyExpandColumnsCheckboxEnabled() throws Exception {
		BalanceSheetComparePage.verifyExpandColumnCheckboxIsEnabled();
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT1461_Step4_VerifyBankLogoIsDisplayed() throws Exception {
		BalanceSheetComparePage.clickExpandColumnsCheckbox();
		BalanceSheetComparePage.clickShowReportButton();
		EntitySelector.imageVerification("bankLogo");
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	
	@Test (priority = 15, groups = { "all_Tests", "critical_Tests" })
	public void PT1461_Step4_VerifyReportTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
	}
	
	
	@Test (priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT1461_Step4_VerifyTrackTireIcon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_TireTracksIcon"));
	}
	
	
	@Test (priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT1461_Step5_verifyNoEliminateColumn_AtBankLevel() throws Exception {
		BalanceSheetComparePage.clickTireTrackIconToExpand();
		BaseUI.verifyElementDoesNOTExist("balanceSheetCompare_EliminationsColumn", "Eliminations", null);
	}
	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable { 
	  try {
		  StartPage.clickCloseAllTabsButton();
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
  }  
}

