package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DocumentVaultValidation extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to Access document vault modal	
		Navigation.navigateToTools_DocumentVault();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1704_Step2_verifyDocumentVault_ModalTitle() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("docVault_ModalTitle"));
		BaseUI.verifyElementHasExpectedText("docVault_ModalTitle", "Document Vault");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1704_Step3_verifyDocumentVault_DeleteFolderIcon() throws Exception
	{
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		Thread.sleep(200);
		BaseUI.verifyElementAppears(Locator.lookupElement("docVault_DeleteFolderIcon"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1704_Step3_verifyDocumentVault_RenameIcon() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("docVault_RenameFolderIcon"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1704_Step3_verifyDocumentVault_UploadIcon() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("docVault_UploadIcon"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1704_Step3_verifyDocumentVault_DeleteIcon() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("docVault_DeleteIcon"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1704_Step3_verifyDocumentVault_MoveFileTo_Dropdown() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("docVault_MoveToFile_Dropdown"));
	}
	
	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
//	public static void PT1704_Step3_verifyDocumentVault_MoveFileToFolder_FolderNameExists() throws Exception
//	{
//		String expectedFolderName = BaseUI.get_Attribute_FromField(Locator.lookupElement("docVault_FolderName"), "innerText");
//		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("docVault_MoveToFile_Dropdown"), 
//				2);
//		String folderNameSelected = BaseUI.getSelectedOptionFromDropdown("docVault_MoveToFile_Dropdown");
//		
//		BaseUI.verify_true_AndLog(folderNameSelected.equals(expectedFolderName), "Folder Name selected to move file EXISTS",
//				"Folder Name selected to move file DOES NOT Exists");
//		
//	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1704_Step3_verifyDocumentVaultModalClosed() throws Exception
	{
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("docVault_ModalCloseBtn"));
		Thread.sleep(500);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("docVault_ModalTitle"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable
    {  
	    try {
	    	StartPage.clickCloseAllTabsButton();
	        Navigation.navigate_to_signOut();
	    }
	    finally{
	        Browser.closeBrowser();
	    }	  
    }
}
