/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.CustomCalculations;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.MyDashboardIndicator;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class AddCustomCalculationToSummaryWidget extends BaseTest {

	static String customCalcName;
	static String customCalcIndicatorName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		customCalcName = CustomCalculations.addNewCustomCalculation();
	}
	
	@Test (priority = 1)
	public void PT1042_Step2_Navigate_MyDashboardIndicator() throws Exception {
		Navigation.navigateToSettings_MyDashboard();
		BaseUI.verifyElementAppears(Locator.lookupElement("myDashboardIndicator_ModalTitle"));
		BaseUI.verifyElementHasExpectedText("myDashboardIndicator_ModalTitle", "My Dashboard Indicators");
	}
	
	
	
	@Test (priority = 2)
	public void PT1042_Step3_VerifyCustomCalcRadioBtn_InMyDashboardReport() throws Exception
	{		
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		MyDashboardIndicator.click_AddIndicatorButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("myDashboardIndicator_CustomCalc_RadioBtn"));
	}
	
	
	@Test (priority = 3)
	public void PT1042_Step4_ClickAndVerifyCustomCalculationIsDisplayed() throws Exception
	{		
		MyDashboardIndicator.clickRadioButton("Custom Calc");
		BaseUI.verifyElementAppears(Locator.lookupElement("myDashboardIndicator_SelectCustomCalcFromList"));
	}
	
	
	@Test (priority = 5)
	public void PT1042_Step5_ClickNextAndVerifyMtdChangeCheckbox() throws Exception
	{
		MyDashboardIndicator.selectCustomCalc(customCalcName);
		customCalcIndicatorName = BaseUI.
				getTextFromInputBox(Locator.lookupElement("myDashboardIndicator_customCalcIndicatorName_txtBox"));
		MyDashboardIndicator.click_NextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("myDashboardIndicator_MtdChange_Checkbox"));
	}
	
	
	@Test (priority = 6)
	public void PT1042_Step6_ClickFinishAndVerifyAddIndicatorButtonDisplayed() throws Exception
	{
		MyDashboardIndicator.click_FinishedButton();
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.verifyElementAppears(Locator.lookupElement("myDashboardIndicator_AddIndicatorBtn"));
	}
	
	
	@Test (priority = 7)
	public void PT1042_Step6_VerifyMyIndicatorModalClosed() throws Exception
	{	
		MyDashboardIndicator.click_FinishedButton();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("myDashboardIndicator_ModalTitle"));
	}
	
	
	@Test (priority = 8)
	public void PT1042_Step6_VerifyCustomCalculation_DisplayedOnSummaryWidget() throws Exception {
		StartPage.clickStartPageTab();
		WebElement customCalcIndicator = Locator.lookupElement("myDashboardIndicator_CustomCalcIndicatorOnMyDashboard", 
				customCalcIndicatorName, null);
		String actualCustomIndicator = BaseUI.getTextFromField(customCalcIndicator);
		BaseUI.verifyElementAppears(customCalcIndicator);
		BaseUI.baseStringCompare("myDashboardIndicator_CustomCalcIndicatorOnMyDashboard", 
				customCalcIndicatorName+"*", actualCustomIndicator);
	}
	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  

    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  CustomCalculations.deleteCustomCalculation(customCalcName);
		  StartPage.verifyCustomCalculation_DeletedFromSummaryReport(customCalcName);
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
  }  
}
