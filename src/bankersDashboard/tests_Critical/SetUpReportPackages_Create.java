package bankersDashboard.tests_Critical;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.ReportPackages;
import bankersDashboard.pages.SetUpReportPackage;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SetUpReportPackages_Create extends BaseTest {
	
	private static String storePackageName;

	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Set up Report package
		//Create New Report package
		storePackageName = ReportPackages.createNewReportPackage();
	}
	
	
	@Test(priority = 1)
	public void PT1603_Step1_verifyFavoriteReportPackageText() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("setUpReportPackage_FavoriteReportPackage"));	
	}
	
		
	@Test(priority = 2)
	public void PT1603_Step2_VerifyRemoveFromFavoriteReportPackageIcon() throws Exception
	{
		ReportPackages.clickFavoriteReportPackageIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("setUpReportPackage_RemoveFromFavoritesText"));
	}
	
		
	@Test(priority = 3)
	public void PT1603_Step3_selectReportsFromAddReportDropdown() throws Exception
	{
		SetUpReportPackage.addAllReportsToReportPackage();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("setUpReportPackage_ModalTitle"));
	}


	@Test(priority = 4)
	public void PT1603_Step4_VerifyReportPackage_AddedToMyFavoriteWidget() throws Exception {
		StartPage.clickStartPageTab();
		BaseUI.scroll_to_element(Locator.lookupElement("startpage_MyFavReportWidgetTitle"));
		StartPage.clickRepPackageOnMyFavReportWidget(storePackageName);
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_startPageTab2"));
		BaseUI.verifyElementHasExpectedText("startPage_startPageTab2", "Monthly Dashboard");		
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	 
  
   @AfterClass(alwaysRun = true)
   public void tear_down() throws Throwable
    {
	  try {
		  StartPage.clickCloseAllTabsButton();
		  SetUpReportPackage.deleteReportPackage();
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}

