package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class StartPage_DeleteWidget extends BaseTest {
	
	static String storeBalancesWidgetName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		storeBalancesWidgetName = StartPage.addBalanceWidgetToStartPage();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1659_Step3_verifyBalancesWidgetIsDisplayedOnStartpage() throws Exception {
		StartPage.clickStartPageTab();
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
		BaseUI.verifyElementHasExpectedText("verifyBalancesIndicatorAdded_startPage", storeBalancesWidgetName);		
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT1659_Step5_ClickDelete_VerifyRemoveWidgetModalAppearsOnStartpage() throws Exception {
		 StartPage.clickDeleteWidgetIcon("deleteAnyCustomWidgets");
		 BaseUI.verifyElementAppears(Locator.lookupElement("removeModuleTitle"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT1659_Step6_ClickDelete_CancelBtn_VerifyBalancesWidgetExistsOnStartpage() throws Exception {
		 StartPage.clickRemoveWidget_CancelBtn();
		 BaseUI.verifyElementAppears(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1659_Step7_ClickDelete_OkBtn_VerifyBalancesWidgetDeletedFromStartpage() throws Exception {
		StartPage.deleteWidgetFromStartPage("balancesWidgetDeleteIcon", storeBalancesWidgetName, "deleteAnyCustomWidgets");
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
    	try {
	        Navigation.navigate_to_signOut();
	        } finally {
	        	Browser.closeBrowser();
	        	}
	 }
}
