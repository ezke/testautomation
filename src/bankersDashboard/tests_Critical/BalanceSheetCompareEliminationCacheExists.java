/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;

import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * Smoke Test: Validate Balance Sheet Compare Report has 
 * Elimination column when Consolidated bank entity is selected
 * @author Nisha 
 */
public class BalanceSheetCompareEliminationCacheExists extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		StartPage.clickStartPageTab();
		EntitySelector.select_consolidatedBankEntity_fromDropdown(ClientDataRetrieval.consolidatedEntity);

		// Navigate to Balance sheet compare report
		Navigation.navigateToGL_BS_BalanceSheetCompare();
	}

	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public static void PT1044_Step4_verifyExpandColumnsCheckboxEnabled() throws Exception {
		// Check expand column checkbox.
		BalanceSheetComparePage.verifyExpandColumnCheckboxIsEnabled();
	}

	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT1044_Step5_verifyReportTitle() throws Exception {
		// Leave default settings and click Show Report button
		BalanceSheetComparePage.clickExpandColumnsCheckbox();
		BalanceSheetComparePage.clickShowReportButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
	}

	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT1044_Step5_verifyBankLogoIsDisplayed() throws Exception {
		// EntitySelector.imageVerification("bankLogo");
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}

	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT1044_Step5_verifyTireTrackIcon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_TireTracksIcon"));
	}

	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT1044_Step5To6_verifyEliminationColumnExistsAtConsolidatedBankLevel() throws Exception {
		BalanceSheetComparePage.clickTireTrackIconToExpand();
		BaseUI.verifyElementExists("balanceSheetCompare_EliminationsColumn", "Eliminations", null);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
		try {
			StartPage.clickCloseAllTabsButton();
			Navigation.navigate_to_signOut();
		} finally {
			Browser.closeBrowser();
		}

	}

}
