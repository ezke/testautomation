package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.Settings_MyCentersModal;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BalanceSheetCompare_MTDValidation extends BaseTest {

	static String view = "Summary";
	static String profitCenter = "Consolidated";
	static String dataSourcePrimary = "Actual"; 
	static String avgPrimary = "MTD";
	static String datePrimary = "July 2016";
	static String dataSourceSecCol = "Actual";
	static String avgSecCol = "MTD";
	static String dateSecCol = "July 2016";
	static String OptionClick_AtGroupDrillDown;
	static String expectedDrillDown_PrimDateSource = (avgPrimary + " " + "Average" + "\n" + dataSourcePrimary);
	private static String OptionClick_AtAccountDrillDown;
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to My Centers and add centers
		StartPage.clickStartPageTab();
		Settings_MyCentersModal.addCenters_MyCenters();
		
		//Navigate to Balance Sheet compare report
		Navigation.navigateToGL_BS_BalanceSheetCompare();
		//BalanceSheetComparePage.verifyBalanceSheetCompareModalTitle();
		BalanceSheetComparePage.enterMtdSearchCriteria(view, profitCenter, dataSourcePrimary, avgPrimary, 
				datePrimary, dataSourceSecCol, avgSecCol, dateSecCol);
		BalanceSheetComparePage.clickShowReportButton();
	}
	
	
	@Test(priority = 10)
	public static void PT3827_Step3_verifyBankLogoIsDisplayed() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 10)
	public static void PT3827_Step3_verifyBalanceSheetCompare_ReportTitle() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
	}
	
	
	@Test(priority = 10)
	public static void PT3827_Step3_verifySelected_ProfitCenterIsDisplayed() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ProfitCenter"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenter", profitCenter);
	}
	
	
	@Test(priority = 15)
	public static void PT3827_Step4_verifySubTitle_Assets() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportSubtitleAssets"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportSubtitleAssets", "Assets");
	}
	
	
	@Test(priority = 15)
	public static void PT3827_Step4_verifySubTitle_Liabilities() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportSubtitleLiabilities"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportSubtitleLiabilities", "Liabilities");
	}
	
	
	@Test(priority = 15)
	public static void PT3827_Step4_verifySubTitle_Equity() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportSubtitleEquity"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportSubtitleEquity", "Equity");
	}
	
	
	@Test(priority = 20)
	public static void PT3827_Step5_verifyPrimaryColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportPrimaryDateSelected"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportPrimaryDateSelected", 
				(expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(datePrimary, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 20)
	public static void PT3827_Step5_verifySecondColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportSecColDateSelected"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportSecColDateSelected", 
				(expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(dateSecCol, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 25)
	public static void PT3827_Step6_verifySecondColumn_PrimaryColumn_DataMatch() throws Exception
	{
		BalanceSheetComparePage.verifyPrimarySecond_ColumnData();
	}
	
	
	@Test(priority = 30)
	public static void PT3827_Step7_verifyTireTrackIsDisplayed() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_TireTracksIcon"));
	}
	
	
	@Test(priority = 30)
	public static void PT3827_Step7_verifyCenterAvailable_OnTireTrackClick() throws Exception
	{
		BalanceSheetComparePage.clickTireTrackIconToExpand();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_TireTrack_Center1"));
	}
	
	
	@Test(priority = 35)
	public static void PT3827_Step8_verifyGroupDrillDownLevel() throws Exception
	{
		OptionClick_AtGroupDrillDown = BaseUI
				.getTextFromField(Locator.lookupElement("balanceSheetCompare_cashAndDuesFromBank_DrillDown")).trim();
		BalanceSheetComparePage.clickGroupDrillDown();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_GroupDrillDownTitle"));
	  	BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupDrillDownTitle", "Group Drill Down");
	}
	
	
	@Test(priority = 40)
	public static void PT3827_Step8_verifyProfitCenter_GroupDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtGroupDrillDown", 
				profitCenter);
	}
	
	
	@Test(priority = 40)
	public static void PT3827_Step8_verifyOptionClicked_ToGroupDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_OptionClickedforDrillDown_presentAtGroupLevel", 
				OptionClick_AtGroupDrillDown);
	}
	
	
	@Test(priority = 40)
	public static void PT3827_Step8_verifyMtd_PrimAvgData_AtGroupDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupLevel_PrimCol_DataSourceDate", 
				(expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(datePrimary, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 40)
	public static void PT3827_Step8_verifyMtd_SecColumnAvgData_AtGroupDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_GroupLevel_SecCol_DataSourceDate", 
				(expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(dateSecCol, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 41)
	public static void PT3827_Step8_verifyAccountDrillDownLevel() throws Exception
	{
		OptionClick_AtAccountDrillDown = BaseUI
				.getTextFromField(Locator.lookupElement("balanceSheetCompare_cashTeller_groupDrillDown")).trim();
		BalanceSheetComparePage.clickAccountDrillDown();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_AccountDrillDownTitle"));
	  	BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(priority = 45)
	public static void PT3827_Step8_verifyProfitCenter_AccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtAccountDrillDown", 
				profitCenter);
	}
	
	
	@Test(priority = 45)
	public static void PT3827_Step8_verifyOptionClicked_ToAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_OptionClickedAtGroupDrillDown_PresentAtAccountLevel", 
				OptionClick_AtAccountDrillDown);
	}
	
	
	@Test(priority = 45)
	public static void PT3827_Step8_verifyMtd_PrimAvgData_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountLevel_PrimCol_DataSourceDate", 
				(expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(datePrimary, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 45)
	public static void PT3827_Step8_verifyMtd_SecColumnAvgData_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_AccountLevel_SecCol_DataSourceDate", 
				(expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(dateSecCol, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 51)
	public static void PT3827_Step8_verifyProfitCenterDrillDownLevel() throws Exception
	{
		OptionClick_AtAccountDrillDown = BaseUI
				.getTextFromField(Locator.lookupElement("balanceSheetCompare_cashTeller_AccountDrillDown")).trim();
		BalanceSheetComparePage.clickProfitCenterDrillDown();
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ProfitCenterDrillDownTitle"));
	  	BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterDrillDownTitle", "Profit Center Drill Down");
	}
	
	
	@Test(priority = 55)
	public static void PT3827_Step8_verifyProfitCenter_ProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenterAtProfitCenterDrillDown", 
				profitCenter);
	}
	
	
	@Test(priority = 55)
	public static void PT3827_Step8_verifyOptionClicked_ToProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetCompare_OptionClickedAtAccountDrillDown_PresentAtProfitCenterLevel", 
				OptionClick_AtAccountDrillDown);
	}
	
	
	@Test(priority = 55)
	public static void PT3827_Step8_verifyMtd_PrimAvgData_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenLevel_PrimCol_DataSourceDate", 
				expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(datePrimary, "MMMM yyyy", "MMM yyyy"));
	}
	
	
	@Test(priority = 55)
	public static void PT3827_Step8_verifyMtd_SecColumnAvgData_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenLevel_SecCol_DataSourceDate", 
				expectedDrillDown_PrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(dateSecCol, "MMMM yyyy", "MMM yyyy"));
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable
    {  
	    try {
	    	StartPage.clickCloseAllTabsButton();
	    	Settings_MyCentersModal.removeMyCenters();
	        Navigation.navigate_to_signOut();
	    }
	    finally{
	        Browser.closeBrowser();
	    }	  
    }
}
