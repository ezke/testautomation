/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddNewUserPage;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;


public class AddNewUser extends BaseTest {
	
	private static String newUserName;
	
	//Email Validation variables
	static String email = GlobalVariables.emailAddress;
	static String emailSubject;
	static Email firstEmail;
	static String emailCreationDate;
	static String emailBodyContent;
	static String cdDetailsOnEmailModal;

	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
		
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		newUserName = AddNewUserPage.addNewUser_CopyDefaultFromExistingUser();
		Navigation.navigateToAdmin_Users_ManageUsers();
		StartPage.switchToIframe();
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1040_Step15_VerifyManageUser_UserCreated() {
		BaseUI.verifyElementAppears(Locator.lookupElement("manageUsers_NewlyUserID"));
		BaseUI.verifyElementHasExpectedText("manageUsers_NewlyUserID", newUserName);
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT1040_Step15_VerifyManageUser_ModalClosed() throws Exception {
		AddNewUserPage.clickFinishedBtn();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("manageUsers_ModalTitle"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT1040_Step15_VerifyNewUserCreated_EmailReceived() throws Exception {
		Email_API.Wait_ForEmail_ToExist(email, 120);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");
	}
	
	
	@Test (groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT1040_Step18To19_VerifyNewUser_LoginSuccessfully() throws Exception {	
		StartPage.clickCloseAllTabsButton();		
		Navigation.navigate_to_signOut();
		
		//Open the new browser session and login with newly created user		
		LoginPage.loginAsNewlyCreatedUserAndLogout(newUserName);	
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1040_Step15_VerifyReceivedEmailSubject() throws Exception {
		 emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(emailSubject.equals("Welcome"), 
				 "Email subject displays: "+emailSubject, 
				 "Email subject DOES NOT display: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1040_Step15_VerifyReceivedEmail_CreationDate() throws Exception {
		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
				 "Email Creation date MATCHES Alert email sent date "+ String.valueOf(emailCreationDate), 
				 "Email Creation date DOES NOT MATCH Alert email sent date "+ String.valueOf(emailCreationDate));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT1040_Step15_VerifyEmailContainsMessage() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		BaseUI.verify_true_AndLog(emailBodyContent.contains(newUserName), 
				"Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+ emailBodyContent);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  Email_API.delete_Emails_ByRecipient(email);
		  LoginPage.loginAsClientUser();
		  AddNewUserPage.click_VerifyNewUser_Deleted();
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
   }

}
