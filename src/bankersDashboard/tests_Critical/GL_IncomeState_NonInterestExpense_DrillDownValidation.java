package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.IS_NonInterestExpenseModal_Page;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class GL_IncomeState_NonInterestExpense_DrillDownValidation extends BaseTest {

	static String profitCenterSelected, saveSelectedSource, saveSelectedDateOption;
	static String OptionClickedAtReportLevel, optionClickedAtAccountLevel;
	static String expectedPrimDateSource;
	private static String dataOfOptionClickedAtAccountLevel;
	private static String dataOfOptionClickedAtReportLevel;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to GL > Income Statement > Non Interest Expense report	
		Navigation.navigateToGL_IS_NonInterestExpense();
		profitCenterSelected = BaseUI.getSelectedOptionFromDropdown("nonInterestExpense_ProfitCenterDropdown");
		saveSelectedSource = BaseUI.getSelectedOptionFromDropdown("nonInterestExpense_Col1DataSourceDropdown");
		saveSelectedDateOption = BaseUI.getSelectedOptionFromDropdown("nonInterestExpense_DateCol1Dropdown");
		IS_NonInterestExpenseModal_Page.selectSecColDateValue(saveSelectedDateOption);
		expectedPrimDateSource = "MTD"+"\n"+saveSelectedSource;
		IS_NonInterestExpenseModal_Page.clickShowReportButton();
	}
	
	
	@Test(priority = 4, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyBankLogoIsDisplayed_NonInterestExpense() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_BankLogo"));
	}
	
	
	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyNonInterestExpense_ReportTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ReportTitle", "Non Interest Expense");
	}
	
	
	@Test(priority = 6, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyNonInterestExpense_SelectedProfitCenterIsDisplayed() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenter", profitCenterSelected);
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestExpense_1stGroupHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ReportGroupHeader_Salaries_EmployeeBenefits", 
				"Salaries and employee benefits");
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestExpense_2ndGroupHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("nonInterestExpense_ReportGroupHeader_Occupancy", 
				"Occupancy");
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestExpense_3rdGroupHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ReportGroupHeader_OtherNonIE", 
				"Other noninterest expense");
	}
	
	
	@Test(priority = 8)
	public void PT2778_Step8_VerifyNonInterestExpense_PrimaryColumn_MtdAvgDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ReportPrimaryDateSelected"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ReportPrimaryDateSelected", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	@Test(priority = 8)
	public void PT2778_Step8_VerifyNonInterestExpense_SecColumn_MtdAvgDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ReportSecColDateSelected"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ReportSecColDateSelected", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyNonInterestExpense_PrimarySecondColDataMatch() throws Exception {
		IS_NonInterestExpenseModal_Page.verifyNonInterestExpense_PrimarySecond_ColumnData("nonInterestExpense_ReportPrimaryColData", 
				"nonInterestExpense_ReportSecColData");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_verifyNonInterestExpense_AccountDrilldownTitle() throws Exception {
		OptionClickedAtReportLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestExpense_OfficerSalariesOptionForDrillDown"));
		dataOfOptionClickedAtReportLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestExpense_ReportPrimaryCol_1stOptionData"));
		IS_NonInterestExpenseModal_Page.clickToDrilldownReport("nonInterestExpense_OfficerSalariesOptionForDrillDown");

		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_VerifyNonInterestExpense_ProfitCenter_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenterAtAccountDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_VerifyNonInterestExpense_OptionClicked_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("nonInterestExpense_OptionClickedForDrillDown_PresentAtAccountLevel", 
				OptionClickedAtReportLevel.trim());
	}
	
	
	@Test(priority = 25)
	public void PT2778_Step12_VerifyNonInterestExpenseAtAccountDrillDownLevel_PrimaryColumn_MtdAvgDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_AccountDrillDownLevel_PrimaryColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_AccountDrillDownLevel_PrimaryColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	@Test(priority = 25)
	public void PT2778_Step12_VerifyNonInterestExpenseAtAccountDrillDownLevel_SecColumn_MtdAvgDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_AccountDrillDownLevel_SecColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_AccountDrillDownLevel_SecColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 25)
	public void PT2778_Step12_VerifyNonInterestExpenseAtAccountDrillDownLevel_PrimaryColTotalMatchReportLevelOptionValue() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_AccountDrillDownLevel_PrimaryColTotalValue"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_AccountDrillDownLevel_PrimaryColTotalValue", dataOfOptionClickedAtReportLevel);
				
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyNonInterestExpense_PrimarySecondColDataMatch_AtAccountDrilldownLevel() throws Exception {
		IS_NonInterestExpenseModal_Page.verifyNonInterestExpense_PrimarySecond_ColumnData("nonInterestExpense_AccountDrillDownLevel_PrimaryColData", 
				"nonInterestExpense_AccountDrillDownLevel_SecColData");
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestExpense_ProfitCenterDrillDownReportTitle() throws Exception {
		optionClickedAtAccountLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestExpense_1stOptionAtAccountDrillDown"));
		dataOfOptionClickedAtAccountLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestExpense_AccountDrillDownLevel_PrimaryCol_1stOptionData"));
		//Click on 1st option under assets column to check drill down"
		IS_NonInterestExpenseModal_Page.clickToDrilldownReport("nonInterestExpense_1stOptionAtAccountDrillDown");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ProfitCenterDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenterDrillDownTitle", "Profit Center Drill Down");		
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestExpense_ProfitCenter_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenterAtProfitCenterDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestExpense_OptionClicked_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("nonInterestExpense_OptionClickedAtAccountDrillDown_PresentAtProfitCenterLevel", 
				optionClickedAtAccountLevel);
	}
	
	
	@Test(priority = 35)
	public void PT2778_Step14_VerifyNonInterestExpenseAtProfitCenterDrillDownLevel_PrimaryColumn_MtdAvgDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ProfitCenterDrillDownLevel_PrimaryColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenterDrillDownLevel_PrimaryColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	@Test(priority = 35)
	public void PT2778_Step14_VerifyNonInterestExpenseAtProfitCenterDrillDownLevel_SecColumn_MtdAvgDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ProfitCenterDrillDownLevel_SecColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenterDrillDownLevel_SecColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 35)
	public void PT2778_Step12_VerifyNonInterestExpenseAtProfitCenterDrillDownLevel_PrimaryColTotalMatchAccountDrilldownOptionValue() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ProfitCenterDrillDownLevel_PrimaryColTotalValue"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ProfitCenterDrillDownLevel_PrimaryColTotalValue", 
				dataOfOptionClickedAtAccountLevel);
				
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestExpense_PrimarySecondColDataMatch_AtProfitCenterDrilldownLevel() throws Exception {
		IS_NonInterestExpenseModal_Page.verifyNonInterestExpense_PrimarySecond_ColumnData("nonInterestExpense_ProfitCenterDrillDownLevel_PrimaryColData", 
				"nonInterestExpense_ProfitCenterDrillDownLevel_SecColData");
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step15_VerifyNonInterestExpense_BackButton_IsEnabled() throws Exception {
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_Back_button_Icon"));
	}
	
	
	@Test(priority = 45, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step15_VerifyNonInterestExpense_BackButtonToGoBack_AccountDrillDownPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(priority = 55, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step17_VerifyNonInterestExpense_BackButtonToGoBack_BalanceSheetCompareReportPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestExpense_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestExpense_ReportTitle", "Non Interest Expense");		
	}

		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
   
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
