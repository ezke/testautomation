package bankersDashboard.tests_Critical;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.IncomeStatementReportPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.Settings_MyAlertsModal;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pages.StartPage_AlertsModal;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Alerts_ValidateIncomeStatementAlerts extends BaseTest {

	static ArrayList<String> totalISAlertsList;
	static ArrayList<String> isAlert_dollarValue;
	static ArrayList<String> isAlert_PercentValue;
	
	//Value to enter for %Alert textbox, if no IS alert exists
	static String alertChangeValue = "0.20";
	static String resetValue = "5.00";
	
	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		StartPage.clickMyAlertsBellIcon();
		StartPage_AlertsModal.clickIncomeStatementTab();
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_IncomeStatementTab_NoAlertsMessage"))) {
			StartPage_AlertsModal.clickCloseAlertsModal();
			Settings_MyAlertsModal.updateAlertPercentValue_IncomeStatement(alertChangeValue);
			StartPage.clickMyAlertsBellIcon();
			StartPage_AlertsModal.clickRefreshIcon();
			StartPage_AlertsModal.clickIncomeStatementTab();
			BaseUI.wait_forElementToNotExist("alerts_IncomeStatementTab_NoAlertsMessage", null, null);
		}
		
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_IncomeStatementTab_NoAlertsMessage"))) {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("alerts_IncomeStateTab_ViewISBtn"));
			BaseUI.log_AndFail("Income Statement Alerts are not set");
		} else {
			BaseUI.waitForElementToBeDisplayed("alerts_IncomeStateTab_ViewISBtn", null, null);
			
			//Get Total Balance Sheet alerts displayed
			totalISAlertsList = StartPage_AlertsModal.return_IncomeStatementAlertsList();
			BaseUI.verify_true_AndLog(totalISAlertsList.size() > 0, "BS compare Column1 list Displayed", 
					"BS compare Column1 list IS NOT Displayed");
			
			//Get Balance sheet alerts dollar value
			isAlert_dollarValue = StartPage_AlertsModal.return_IncomeStatementAlertsDollarValueList();
			BaseUI.verify_true_AndLog(isAlert_dollarValue.size() > 0, "BS compare Dollar value list Displayed", 
					"BS compare Dollar value list IS NOT Displayed");
			
			//Get Balance sheet percentage value displayed
			isAlert_PercentValue = StartPage_AlertsModal.return_IncomeStatementAlertsPercentValueList();
			BaseUI.verify_true_AndLog(isAlert_PercentValue.size() > 0, "BS compare percentage value list Displayed", 
					"BS compare percentage value list IS NOT Displayed");
		}		
	}
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_ModalTitle() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_TodaysAlertsTitle"));
		BaseUI.verifyElementHasExpectedText("alerts_TodaysAlertsTitle", 
				"Today's Alerts");
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_AlertsRefreshIcon() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_RefreshIcon"));
	}
	
	
	@Test(priority = 15, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step1_VerifyAlerts_IncomeStatementTab() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_IncomeStateTab"));
		BaseUI.verifyElementHasExpectedPartialText("alerts_IncomeStateTab", 
				"Income Statement");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step2_VerifyAlerts_IncomeStatementAlertsExists() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_IncomeStateTab_ISAlertsNum"));
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step2_VerifyISAlertsTab_ViewIncomeStatementBtn() throws Exception {
		if(BaseUI.elementAppears(Locator.lookupElement("alerts_IncomeStateTab_ISAlertsNum"))){
			BaseUI.verifyElementAppears(Locator.lookupElement("alerts_IncomeStateTab_ViewISBtn"));
		}
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step3_VerifyISAlertsTab_AlertsNum_MatchesAlertsDisplayed() throws Exception {
		IncomeStatementReportPage.verifyIncomeStatementAlert_AlertsNum_MatchesAlertsDisplayed();	
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_VerifyISAlertsTab_VerifyISReportDisplays() throws Exception {
		StartPage_AlertsModal.clickIncomeStatementTab();
		StartPage_AlertsModal.clickViewIncomeStatementButton();
		
		BaseUI.waitForElementToBeDisplayed("startPage_startPageTab2", null, null, 1000);
		BaseUI.verifyElementHasExpectedText("startPage_startPageTab2", "IS Compare");
		
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step4_verifyBankLogoIsDisplayed() throws Exception {
		//validate bank logo is displayed on Report page
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step5_verifyIncomeStatementCompareReportTitle() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("incomeStatementCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_ReportTitle", "Income Statement Compare");
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step6_verifySelectedProfitCenterIsDisplayed() throws Exception {
		//validate profit center match the selected
		BaseUI.verifyElementAppears(Locator.lookupElement("incomeStatementCompare_ProfitCenter"));
		BaseUI.verifyElementHasExpectedText("incomeStatementCompare_ProfitCenter", "Consolidated");
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step7_VerifyISAlertsTab_ISReportDollarValue_MatchesAlertsDollarValueDisplayed() throws Exception {
		IncomeStatementReportPage.verify_IncomeStatement_DollarValue(totalISAlertsList, 
				isAlert_dollarValue);
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public static void PT3789_Step7_VerifyISAlertsTab_ISReportPercentValue_MatchesAlertsPercentValueDisplayed() throws Exception {
		IncomeStatementReportPage.verify_IncomeStatement_PercentValue(totalISAlertsList, 
				isAlert_PercentValue);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickStartPageTab();
	    	Settings_MyAlertsModal.updateAlertPercentValue_IncomeStatement(resetValue);
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
