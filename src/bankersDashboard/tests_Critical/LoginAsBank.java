package bankersDashboard.tests_Critical;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class LoginAsBank extends BaseTest {
	
	private static String bankEntity;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT1031_Step1_VerifySaveBankEntity() throws Exception {
		StartPage.verifyBankEntityRemainsSame_AfterLoggingBackIn(bankEntity);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT1031_Step1_VerifyBankersDashboardLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_BD_logo"));
	}
	
    
	@AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	    
	  
    @AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
		try {
		     Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}

}

