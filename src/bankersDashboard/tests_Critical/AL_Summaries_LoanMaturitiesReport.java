package bankersDashboard.tests_Critical;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AL_SummariesMaturitiesModal;
import bankersDashboard.pages.AL_SummariesMaturitiesReportPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class AL_Summaries_LoanMaturitiesReport extends BaseTest {
		
	static String dateOptionDefault;
	static String viewOptionDefault;
	static String regBranch_OptionDefault;
	static String officersOptionDefault;
	static ArrayList<String> dateColList;
	static String viewOption = "6 Months";
	static int officersOption = 2;
	static int regBranchOption = 3;
	static String dateOption = "Apr, 2017";
	private static String regBranch_OffSelected;
	
	@BeforeClass(alwaysRun = true)
	public void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to AL> Historical Monthly Loan Report
		StartPage.clickStartPageTab();
		Navigation.navigateToAL_Summaries_Maturities();
		
		//store all default options selected on Historical Monthly Loan modal
		dateOptionDefault = BaseUI.getSelectedOptionFromDropdown("alSum_MaturitiesModal_Date_Dropdown")
				.replaceAll(",", "");
		viewOptionDefault = BaseUI.getSelectedOptionFromDropdown("alSum_MaturitiesModal_View_Dropdown")
				.replaceAll(" Months", "");
		regBranch_OptionDefault = BaseUI.getSelectedOptionFromDropdown("alSum_MaturitiesModal_Reg_Branch_Dropdown");
		officersOptionDefault = BaseUI.getSelectedOptionFromDropdown("alSum_MaturitiesModal_Officers_Dropdown");
		
		//Check Non Accural Loans checkbox is checked by default
		AL_SummariesMaturitiesModal.verifyNonAccuralLoans_Checkbox_IsChecked();
		AL_SummariesMaturitiesModal.clickShowReportBtn();		
	}
	
	
	@Test(priority = 10)
	public void PT3066_Step1_VerifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 15)
	public void PT3066_Step5_VerifyAlSumMaturitiesReport_ReportTitle() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("alSum_MaturitiesReport_ReportTitle", 
				"Maturities Summary");
	}
	
	
	@Test(priority = 15)
	public void PT3066_Step5_VerifyAlSumMaturitiesReport_MaturitiesGraph() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_GraphDisplayed"));
	}
	
	
	@Test(priority = 15)
	public void PT3066_Step5_verifyOnReport_SelectedViewOption() throws Exception
	{
		dateColList = AL_SummariesMaturitiesReportPage.return_SumMaturitiesRepTable_DateColumnData();
		int dateColSize = dateColList.size();
		BaseUI.verify_true_AndLog(dateColList.size() > 0 , "Renewal Dates column displays date " + dateColList.size(), 
				"Renewal Dates column DOES NOT displays date " + dateColList.size());
		BaseUI.verify_true_AndLog((dateColSize - 1) == Integer.parseInt(viewOptionDefault), 
				"Selected View option Match Displayed "+ ((dateColSize - 1)), 
				"Selected View option DOES NOT Match Displayed "+ (dateColSize - 1));
	}
	
	
	@Test(priority = 15)
	public void PT3066_Step5_verifyOnReport_SelectedDateOptionDisplayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_DateDisplayed"));
		BaseUI.verifyElementHasExpectedText("alSum_MaturitiesReport_DateDisplayed", 
				dateOptionDefault);
	}
	
	
	@Test(priority = 20)
	public void PT3066_Step5_verifyOnReport_ExportToPDFIcon_Enabled() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToPDF_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToPDF_Icon"));
	}
	
	
	@Test(priority = 20)
	public void PT3066_Step5_verifyOnReport_ExportToExcelIcon_Enabled() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToExcel_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToExcel_Icon"));
	}
	
	
	@Test(priority = 25)
	public void PT3066_Step6_VerifyAlSumMaturitiesReport_ReportTitle_DifferentOptions() throws Exception {
		regBranch_OffSelected = AL_SummariesMaturitiesReportPage.clickRepProperties_SelectNewDateOption(viewOption, 
				officersOption, regBranchOption, dateOption);
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("alSum_MaturitiesReport_ReportTitle", 
				"Maturities Summary");
	}
	
	
	@Test(priority = 30)
	public void PT3066_Step7_VerifyBankLogoDisplayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 30)
	public void PT3066_Step7_VerifyAlSumMaturitiesReport_RegBranch_OfficerS_SubTitle() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_RegionOfficer_Subheader"));
		BaseUI.verifyElementHasExpectedText("alSum_MaturitiesReport_RegionOfficer_Subheader", 
				regBranch_OffSelected);
	}
	
	
	@Test(priority = 35)
	public void PT3066_Step7_VerifyAlSumMaturitiesReport_MaturitiesGraph_DiffOptions() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_GraphDisplayed"));
	}
	
	
	@Test(priority = 35)
	public void PT3066_Step7_verifyOnReport_SelectedViewOption_DiffOption() throws Exception
	{
		dateColList = AL_SummariesMaturitiesReportPage.return_SumMaturitiesRepTable_DateColumnData();
		int dateColSize = dateColList.size();
		BaseUI.scroll_to_element(Locator.lookupElement("alSum_MaturitiesReport_DateDisplayed"));
		BaseUI.verify_true_AndLog(dateColList.size() > 0 , "Renewal Dates column displays date " + dateColList.size(), 
				"Renewal Dates column DOES NOT displays date " + dateColList.size());
		BaseUI.verify_true_AndLog((dateColSize - 1) == Integer.parseInt(viewOption.replaceAll(" Months", "")), 
				"Selected View option Match Displayed "+ ((dateColSize - 1)), 
				"Selected View option DOES NOT Match Displayed "+ (dateColSize - 1));
	}
	
	
	@Test(priority = 35)
	public void PT3066_Step7_verifyOnReport_SelectedDateOptionDisplayed_DiffOption() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("alSum_MaturitiesReport_DateDisplayed"));
		BaseUI.verifyElementAppears(Locator.lookupElement("alSum_MaturitiesReport_DateDisplayed"));
		BaseUI.verifyElementHasExpectedText("alSum_MaturitiesReport_DateDisplayed", 
				dateOption.replace(",", ""));
	}
	
	
	@Test(priority = 40)
	public void PT3066_Step8_verifyOnReport_ExportToPDFIcon_Enabled_DiffOption() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToPDF_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToPDF_Icon"));
	}
	
	
	@Test(priority = 40)
	public void PT3066_Step8_verifyOnReport_ExportToExcelIcon_Enabled_DiffOption() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_exportToExcel_Icon"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_exportToExcel_Icon"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickCloseAllTabsButton();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}

}
