/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

/**
 *  Test #1046 -
	SMOKE TEST: Redirect to HTTPS
 *
 */
public class RedirectToHttps extends BaseTest {
	
	static String https_url;
	static String http_url = "http://clients-qa.bankersdashboard.com/";
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT1046_VerifyRedirectToHttps()
	{
		https_url = Browser.driver.getCurrentUrl();
		BaseUI.baseStringCompareStringsAreDifferent("", https_url, http_url);
		//System.out.println("The webapp redirects to https even after initiating with http and successfully login user");
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable {
	  try {
		  BaseUI.switch_ToDefaultContent();
		  StartPage.clickCloseAllTabsButton();
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
  }

}
