package bankersDashboard.tests_Critical;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;

public class Forecasting_ReconcileProject extends BaseTest {

	 String fcName = "QaAutomation_FC"+BaseUI.random_NumberAsString(0, 1000);
	 String bankOrCenterOption = "Total Bank";
	 int primProfCenter = 1;
	 int cashBalAccOption = 1;
	 int retEarningAccOption = 1;
	 int begOfYearBalanceAcct = 1;
	 int isProvAcct = 1;
	 int bsProvAcct = 1;
	 int syncThruDate = 4;
	
	//Email Validations
	 String email = GlobalVariables.emailAddress;
	 Email firstEmail;
	 String emailBodyContent;
	 String emailSubject;
	 String expEmailSubject = "Forecast project is ready";
	 long emailCreationDate;
	 String expEmailBodyContent = "The forecast project named '"+fcName+"' is ready to be edited.";
	
	 String storeMonthHeaderFortheColValueEntered;
	 String avg = "No";
	 String primDataSource = "Actual";
	 String syncDateSelected;
	 String dateValue = "July 2017";
	 String view = "Summary";
	 String profitCenter = "Consolidated";
	 String formattedDateValue;
	 String expOutOfBalValue = "5450000";
	 String actualOutOfBalValue;


	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		StartPage.clickStartPageTab();
		
		//Create New forecasting Project
		syncDateSelected = ForecastingPage.createForecastingProject(fcName, bankOrCenterOption, primProfCenter, cashBalAccOption, retEarningAccOption, 
				begOfYearBalanceAcct, isProvAcct, bsProvAcct, syncThruDate);
		
		//Wait for the forecasting project ready email
    	Email_API.Wait_ForEmail_ToExist(email, 900);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");	
		emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
//		BaseUI.verify_true_AndLog(emailSubject.equals(expEmailSubject), 
//				"Email subject displays Forecast message: "+ emailSubject, 
//				 "Email subject DOES NOT display Forecast message: "+ emailSubject);
		
		//Select Existing Forecasting project
		ForecastingPage.selectExistingFCByName(fcName);
		formattedDateValue = BaseUI.return_Date_AsDifferentFormat(dateValue, "MMM yyyy", "MMMM yyyy").trim();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSAssetsSubset() throws Exception {
		BaseUI.click_js(Locator.lookupElement("forecasting_BSGroup_Assets"));
		Thread.sleep(300);
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_BSGroup_Assets_Subset"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCashTellerSubset() throws Exception {
		BaseUI.click_js(Locator.lookupElement("forecasting_BSGroup_Assets_CashAndDuesExpander"));
		Thread.sleep(500);
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_BSGroup_Assets_Subset_CashTellerOption"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCashTellerSubset_AccTextbox() throws Exception {
		BaseUI.click(Locator.lookupElement("forecasting_BSGroup_Assets_Subset_CashTellerOption"));
		Thread.sleep(2000);
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_BSGroup_Assets_Subset_CashTellerOption_textbox"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCashTellerSubset_EnterAccTextbox() throws Exception {
		ForecastingPage.enterOutOfBalancevalue_Reconcile(expOutOfBalValue);
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCompareReport_OutOfBalCol() throws Exception {
		//Navigate to Balance Sheet compare report
		BalanceSheetComparePage.openBSCompareReport_WithForecastingProject(view, profitCenter, primDataSource, avg, 
				formattedDateValue, fcName, avg, formattedDateValue);
		WebElement fc_OutOfBal = Locator.lookupElement("balanceSheetRep_ForecastingProjName_OutOfBalCol");
		BaseUI.scroll_to_element(fc_OutOfBal);
		BaseUI.verifyElementAppears(fc_OutOfBal);
		BaseUI.verifyElementHasExpectedPartialText("balanceSheetRep_ForecastingProjName_OutOfBalCol", 
				"Out of Balance");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCompareReport_OutOfBalColValue() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("balanceSheetRep_ForecastingProjName_OutOfBalColValue"));
		actualOutOfBalValue = BaseUI.getTextFromField(Locator.lookupElement("balanceSheetRep_ForecastingProjName_OutOfBalColValue"))
				.replace(",", "").trim();
		BaseUI.baseStringCompare("balanceSheetRep_ForecastingProjName_OutOfBalColValue", expOutOfBalValue, actualOutOfBalValue);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCompareReport_FCName() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("balanceSheetRep_ForecastingProjName"));
		BaseUI.verifyElementHasExpectedText("balanceSheetRep_ForecastingProjName", "* Forecast 1 = "+fcName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProjModalTitle() throws Exception {
		//Select Existing Forecasting project
		ForecastingPage.selectExistingFCByName(fcName);
		ForecastingPage.clickToolsMenuOptions("Reconcile Project");
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_ReconcileProject_ModalTitle"));
		BaseUI.verifyElementHasExpectedPartialText("forecasting_ReconcileProject_ModalTitle", 
				"Forecasting");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProjPreparingMsg() throws Exception {
		ForecastingPage.switchToModalIframe();
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_ReconcileProject_ForecastingProjReadyMsg"));
		BaseUI.verifyElementHasExpectedPartialText("forecasting_ReconcileProject_ForecastingProjReadyMsg", 
				"Preparing Project");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProjMsg() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_ReconcileProject_ReconcileMsg"));
		BaseUI.verifyElementHasExpectedPartialText("forecasting_ReconcileProject_ReconcileMsg", 
				"Your project is now being processed. An e-mail will be sent to");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCompareReport_OutOfBalColDoesNotApper() throws Exception {
		ForecastingPage.closeReconcileModalWindow();
		//Navigate to Balance Sheet compare report
		BalanceSheetComparePage.openBSCompareReport_WithForecastingProject(view, profitCenter, primDataSource, avg, 
				formattedDateValue, fcName, avg, formattedDateValue);
		BaseUI.scroll_to_element(Locator.lookupElement("balanceSheetRep_ForecastingProjName"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("balanceSheetRep_ForecastingProjName_OutOfBalCol"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 75)
	public void PT1563_Step3To4_VerifyForecastingProject_ReconcileProj_BSCompareReport_FCNameExists() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("balanceSheetRep_ForecastingProjName"));
		BaseUI.verifyElementHasExpectedText("balanceSheetRep_ForecastingProjName", "* Forecast 1 = "+fcName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
	public void PT1563_Step3To4_VerifyForecastingProject_NewFCProject_VerifyFCProjectDeleted() throws Exception {
		ForecastingPage.selectExistingFC_SelectFileMenu_DeleteProject(fcName);
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Logout_option"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  Email_API.delete_Emails_ByRecipient(email);
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
