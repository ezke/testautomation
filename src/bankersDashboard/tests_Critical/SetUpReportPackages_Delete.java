package bankersDashboard.tests_Critical;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.ReportPackages;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha
 * This class will delete the report package
 * Created in SetUpReportPackageCreate class
 *
 */
public class SetUpReportPackages_Delete extends BaseTest {
	
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		
		//Navigate to Set up Report package
		//Create New Report package
		ReportPackages.createNewReportPackageWithReports("no");
		
		//Navigate to Set up Report Packages
		Navigation.navigateToReportPackages_SetUpReportPackages();  
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1599_Step2_verifyReportPackageIsDeleted() throws Exception {
		ReportPackages.selectFromCurrentPackage(ReportPackages.storePackageName);
		ReportPackages.clickDeleteReportIcon();
		ReportPackages.clickDeleteReportPackageIcon();
		BaseUI.verifySelectedItemInDropdown(Locator.lookupElement("setUpReportPackage_CurrentPackage_DropDown"), "Select a Package...");
	}
		
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT1599_Step3_clickFinishedButton() throws Exception {
		 ReportPackages.clickFinishedButton();
		 BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("setUpReportPackage_ModalTitle"));
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	 
  
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {
	  try {
	      Navigation.navigate_to_signOut();
	  } finally{
	      Browser.closeBrowser();
	  }	  
    }
}
