package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;

public class Forecasting_CopyToForecast extends BaseTest {

	 String email = GlobalVariables.emailAddress;
	 Email firstEmail;
	 String emailBodyContent;
	 String emailSubject;
	 String fcName = "TestAutomationFC2017";
	 String expEmailBodyContent = "The forecast project named "+ fcName +" has been copied to the data source Forecast.";
	 String expEmailSubject = "Forecast is Ready";
	 String emailCreationDate;
	 String view = "Summary";
	 String profitCenter = "Consolidated";
	 String primDataSource = "TestAutomationFC2017";
	 String primAvg = "No";
	 String primDate = "April 2015";
	 String secDataSource = "Forecast";
	 String secAvg = "No";
	 String secDate = "April 2015";
	
	@BeforeClass(alwaysRun = true)
	public  void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
		
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		//Select Existing Forecasting project and 
		//Choose Copy to Forecast option under Tools menu
		ForecastingPage.selectExistingFC_SelectToolsOption(fcName, "Copy to Forecast");		
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public  void PT1569_Step3_VerifyNewCreatedForecastingProject_CopyToForecastModalTitle() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CopyToForecast_ModalTitle"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CopyToForecast_ModalTitle", "Forecasting");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public  void PT1569_Step3_VerifyNewCreatedForecastingProject_CopytoForecastReadyMsg() throws Exception {
		StartPage.switchToIframe();
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CopyToForecast_FCCopyToForecastMsg"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CopyToForecast_FCCopyToForecastMsg", "Copying Project");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public  void PT1569_Step3_VerifyForecastingProject_CopyToForecast_EmailReceived() throws Exception {
//		StartPage.switchToIframe();
//		BaseUI.click(Locator.lookupElement("forecasting_CopyToBudget_CloseBtn"));
//		Thread.sleep(1000);
		BaseUI.switch_ToDefaultContent();
    	Email_API.Wait_ForEmail_ToExist(email, 300);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");		
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public  void PT1569_Step3_VerifyForecastingProject_CopyToForecast_EmailSubject() throws Exception {
		emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		BaseUI.verify_true_AndLog(emailSubject.equals(expEmailSubject), 
				"Email subject displays copy to Forecast message: "+ emailSubject, 
				 "Email subject DOES NOT display copy to Forecast message: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public  void PT1569_Step3_VerifyForecastingProject_CopyToForecast_EmailCreationDate() throws Exception {
		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
				 "Email Creation date MATCHES System date: "+ String.valueOf(emailCreationDate), 
				 "Email Creation date DOES NOT MATCHES System date: "+ String.valueOf(emailCreationDate));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public  void PT1569_Step3_VerifyForecastingProject_CopyToForecast_EmailContent() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		BaseUI.verify_true_AndLog(emailBodyContent.equals(expEmailBodyContent), "Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+emailBodyContent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public  void PT1569_Step4To5_VerifyForecastingProject_CopyToForecast_NavigateToBSCompareReport() throws Exception {
		BaseUI.switch_ToDefaultContent();
		EntitySelector.selectWindow("Banker's Dashboard");
		EntitySelector.close_ExtraWindows("Banker's Dashboard");
		BalanceSheetComparePage.openBSCompareReport_WithForecastingProject(view, profitCenter, primDataSource, primAvg,
				primDate, secDataSource, secAvg, secDate);
		BalanceSheetComparePage.verifyBalanceSheet_PrimColData_WithSecColData();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public  void PT1569_Step4To5_VerifyForecastingProject_CopyToForecast_VerifyFCProjName_BSCompareReport() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("balanceSheetRep_ForecastingProjName"));
		BaseUI.verifyElementHasExpectedText("balanceSheetRep_ForecastingProjName", "* Forecast 1 = "+fcName);
	}

	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  Email_API.delete_Emails_ByRecipient(email);	 
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
