package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.IS_NonInterestIncomeModal;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class GL_IncomeState_NonInterestIncome_DrillDownValidation extends BaseTest {

	static String profitCenterSelected, saveSelectedSource, saveSelectedDateOption;
	static String OptionClickedAtReportLevel, optionClickedAtAccountLevel;
	static String expectedPrimDateSource;
	private static String dataOfOptionClickedAtReportLevel;
	private static String dataOfOptionClickedAtAccountLevel;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Navigate to GL > Income Statement > Non Interest Income report	
		Navigation.navigateToGL_IS_NonInterestIncome();
		profitCenterSelected = BaseUI.getSelectedOptionFromDropdown("nonInterestIncome_Profit_center_dropdown");
		saveSelectedSource = BaseUI.getSelectedOptionFromDropdown("nonInterestIncome_Column1_dataSource_dropdown");
		saveSelectedDateOption = BaseUI.getSelectedOptionFromDropdown("nonInterestIncome_Date_column1_dropdown");
		IS_NonInterestIncomeModal.selectSecColDateValue(saveSelectedDateOption);
		expectedPrimDateSource = "MTD"+"\n"+saveSelectedSource;
		IS_NonInterestIncomeModal.clickShowReportButton();
	}
	
	
	@Test(priority = 4, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyBankLogoIsDisplayed_NonInterestIncome() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_BankLogo"));
	}
	
	
	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyNonInterestIncome_ReportTitle() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportTitle", "Non Interest Income");
	}
	
	
	@Test(priority = 6, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step4_verifyNonInterestIncome_SelectedProfitCenterIsDisplayed() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenter", profitCenterSelected);
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestIncome_GroupHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportGroupHeader_ServChargesAndFees", 
				"Service charges and fees");
	}
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestExpense_2ndGroupHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportGroupHeader_MortLoanAndRelFees", 
				"Mortgage loan and related fees");
	}
	
	
	@Test(priority = 7, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestExpense_3rdGroupHeadings() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportGroupHeader_OtherNonII", 
				"Other noninterest income");
	}
	
	
	@Test(priority = 8, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step8_verifyNonInterestIncome_SelectedDataSourceIsDisplayed() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("nonInterestIncome_ReportPrimaryDateSelected", (saveSelectedSource));
		//BaseUI.verifyElementHasExpectedPartialText("verifySource_DateSelectedIsDisplayed", DatePicker.verifyDateDisplayed(saveSelectedDateOption));
	}
	
	
	@Test(priority = 8)
	public void PT2778_Step8_VerifyNonInterestIncome_PrimaryColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ReportPrimaryDateSelected"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportPrimaryDateSelected", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	@Test(priority = 8)
	public void PT2778_Step8_VerifyNonInterestIncome_SecColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ReportSecColDateSelected"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportSecColDateSelected", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyNonInterestIncome_PrimarySecondColDataMatch() throws Exception {
		IS_NonInterestIncomeModal.verifyPrimarySecond_ColumnData("nonInterestIncome_ReportPrimaryColData", "nonInterestIncome_ReportSecColData");
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_verifyNonInterestIncome_AccountDrilldownTitle() throws Exception {
		OptionClickedAtReportLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestIncome_DDA_NOWIncomeConsumer_DrillDown"));
		dataOfOptionClickedAtReportLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestIncome_ReportPrimaryCol_1stOptionData"));
		IS_NonInterestIncomeModal.clickToDrilldownReport("nonInterestIncome_DDA_NOWIncomeConsumer_DrillDown");

		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_VerifyNonInterestIncome_ProfitCenter_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenterAtAccountDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step12_VerifyNonInterestIncome_OptionClicked_AtAccountDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("nonInterestIncome_OptionClickedForDrillDown_PresentAtAccountLevel", 
				OptionClickedAtReportLevel.trim());
	}
	
	
	@Test(priority = 25)
	public void PT2778_Step12_VerifyNonInterestIncomeAtAccountDrillDownLevel_PrimaryColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_AccountDrillDownLevel_PrimaryColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_AccountDrillDownLevel_PrimaryColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	@Test(priority = 25)
	public void PT2778_Step12_VerifyNonInterestIncomeAtAccountDrillDownLevel_SecColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_AccountDrillDownLevel_SecColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_AccountDrillDownLevel_SecColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 25)
	public void PT2778_Step12_VerifyNonInterestIncomeAtAccountDrillDownLevel_PrimaryColTotalMatchReportLevelOptionValue() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_AccountDrillDownLevel_PrimaryColTotalValue"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_AccountDrillDownLevel_PrimaryColTotalValue", dataOfOptionClickedAtReportLevel);
				
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step10_VerifyNonInterestIncome_PrimarySecondColDataMatch_AtAccountDrilldownLevel() throws Exception {
		IS_NonInterestIncomeModal.verifyPrimarySecond_ColumnData("nonInterestIncome_AccountDrillDownLevel_PrimaryColData", 
				"nonInterestIncome_AccountDrillDownLevel_SecColData");
	}
	
	
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestIncome_ProfitCenterDrillDownReportTitle() throws Exception {
		optionClickedAtAccountLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestIncome_cashTeller_AccountDrillDown"));
		dataOfOptionClickedAtAccountLevel = BaseUI.getTextFromField(Locator.lookupElement("nonInterestIncome_AccountDrillDownLevel_PrimaryCol_1stOptionData"));
		//Click on 1st option under assets column to check drill down"
		IS_NonInterestIncomeModal.clickToDrilldownReport("nonInterestIncome_cashTeller_AccountDrillDown");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ProfitCenterDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenterDrillDownTitle", "Profit Center Drill Down");		
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestIncome_ProfitCenter_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenterAtProfitCenterDrillDown", profitCenterSelected);
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestIncome_OptionClicked_AtProfitCenterDrillDownLevel() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("nonInterestIncome_OptionClickedAtAccountDrillDown_PresentAtProfitCenterLevel", 
				optionClickedAtAccountLevel);
	}
	
	
	@Test(priority = 35)
	public void PT2778_Step14_VerifyNonInterestIncomeAtProfitCenterDrillDownLevel_PrimaryColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ProfitCenterDrillDownLevel_PrimaryColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenterDrillDownLevel_PrimaryColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	@Test(priority = 35)
	public void PT2778_Step14_VerifyNonInterestIncomeAtProfitCenterDrillDownLevel_SecColumn_MtdAvgDate() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ProfitCenterDrillDownLevel_SecColDate_DataSrc_Avg"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenterDrillDownLevel_SecColDate_DataSrc_Avg", 
				(expectedPrimDateSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@Test(priority = 35)
	public void PT2778_Step12_VerifyNonInterestIncomeAtProfitCenterDrillDownLevel_PrimaryColTotalMatchAccountDrilldownOptionValue() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ProfitCenterDrillDownLevel_PrimaryColTotalValue"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ProfitCenterDrillDownLevel_PrimaryColTotalValue", dataOfOptionClickedAtAccountLevel);
				
	}
	
	
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step14_VerifyNonInterestIncome_PrimarySecondColDataMatch_AtProfitCenterDrilldownLevel() throws Exception {
		IS_NonInterestIncomeModal.verifyPrimarySecond_ColumnData("nonInterestIncome_ProfitCenterDrillDownLevel_PrimaryColData", 
				"nonInterestIncome_ProfitCenterDrillDownLevel_SecColData");
	}
	
	
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step15_VerifyNonInterestIncome_BackButton_IsEnabled() throws Exception {
		BaseUI.verifyElementEnabled(Locator.lookupElement("startPage_Back_button_Icon"));
	}
	
	
	@Test(priority = 45, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step15_VerifyNonInterestIncome_BackButtonToGoBack_AccountDrillDownPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_AccountDrillDownTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_AccountDrillDownTitle", "Account Drill Down");
	}
	
	
	@Test(priority = 55, groups = { "all_Tests", "critical_Tests" })
	public void PT2778_Step17_VerifyNonInterestIncome_BackButtonToGoBack_BalanceSheetCompareReportPage() throws Exception {
		StartPage.clickBackButtonIcon();
		BaseUI.verifyElementAppears(Locator.lookupElement("nonInterestIncome_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("nonInterestIncome_ReportTitle", "Non Interest Income");	
	}

	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
   
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }  
    }
}
