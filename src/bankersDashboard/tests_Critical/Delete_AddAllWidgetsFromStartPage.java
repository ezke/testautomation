package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Delete_AddAllWidgetsFromStartPage extends BaseTest {

	
	@BeforeClass(alwaysRun = true)
	public static void set_up() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	}
	
	
	@Test(priority = 5, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifySummaryWidgetDeleted() throws Exception {	
		StartPage.deleteSummaryWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("summaryWidget_SummaryTitle"));		
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyMyFavReportsWidgetDeleted() throws Exception {	
		StartPage.deleteMyFavReportsWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_MyFavReportWidgetTitle"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyFedFundCurveWidgetDeleted() throws Exception {	
		StartPage.deleteFedFundCurveWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_FedFundCurveWidgetTitle"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyNew_RenewedLoans_CdsWidgetDeleted() throws Exception {	
		StartPage.deleteNew_RenewedLoans_CdsWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_NewRenewedLoans_CDsWidgetTitle"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyMyMarginWidgetDeleted() throws Exception {	
		StartPage.deleteMyMarginWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_MyMarginWidgetTitle"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyRecentLoanWinsWidgetDeleted() throws Exception {	
		StartPage.deleteRecentLoanWinsWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_RecentLoanWinsWidgetTitle"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyRecentLoanLossesWidgetDeleted() throws Exception {	
		StartPage.deleteRecentLoanLossesWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_RecentLoanLossesWidgetTitle"));
	}
	
	
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step1_VerifyExpectedLoanClosingsWidgetDeleted() throws Exception {	
		StartPage.deleteExpectedLoanClosingsWidget();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("startpage_ExpectedLoanClosingsWidgetTitle"));
	}
	
	
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyExpectedLoanClosingsWidgetAddedToStartPage() throws Exception {	
		AddWidgets_StartPage.addAllWidgets_OutOfBoxModule();
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_ExpectedLoanClosingsWidgetTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyRecentLoanLossesWidgetAddedToStartPage() throws Exception {	
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_RecentLoanLossesWidgetTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyNewRenewed_LoansCDsWidgetAddedToStartPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("startpage_NewRenewedLoans_CDsWidgetTitle"));
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_NewRenewedLoans_CDsWidgetTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyRecentLoanWinsWidgetAddedToStartPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("startpage_RecentLoanWinsWidgetTitle"));
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_RecentLoanWinsWidgetTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyMyMarginWidgetAddedToStartPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("startpage_MyMarginWidgetTitle"));
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_MyMarginWidgetTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyFedFundCurveWidgetAddedToStartPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("startpage_FedFundCurveWidgetTitle"));
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_FedFundCurveWidgetTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifySummaryWidgetAddedToStartPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("summaryWidget_SummaryTitle"));
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_SummaryTitle"));
	}
	
	
	@Test(priority = 25, groups = { "all_Tests", "critical_Tests" })
	public static void PT3711_Step2_VerifyMyFavReportsWidgetAddedToStartPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("startpage_MyFavReportWidgetTitle"));
		BaseUI.verifyElementAppears(Locator.lookupElement("startpage_MyFavReportWidgetTitle"));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void tear_down() throws Throwable {
	    try {
	    	StartPage.clickStartPageTab();
		    Navigation.navigate_to_signOut();
		}
		finally{
		     Browser.closeBrowser();
		}	  
	}
}
