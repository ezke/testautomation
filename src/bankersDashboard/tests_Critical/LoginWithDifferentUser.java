package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pageControls.EntitySelector;
import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class LoginWithDifferentUser extends BaseTest {
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {		
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3828_Step1_VerifyBankLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT3828_Step1_VerifyAnnouncementLogo() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Announcements_text"));		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3828_Step1_Verify_Help_header() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Help_txt"));
		BaseUI.verifyElementHasExpectedText("startPage_Help_txt", GlobalVariables.help_header_txt);	
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT3828_Step1_VerifyMyAlertsLogo() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_MyAlerts_text"));		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3828_Step1_Verify_myFavorites_header() throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_MyFavReports_dropdown"));
		BaseUI.verifyElementHasExpectedText("startPage_MyFavReports_dropdown", GlobalVariables.myFavorties_header_txt);		
	}
	
    
	@AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception{
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	  
	  	  
	 @AfterClass(alwaysRun = true)
	 public void tear_down() throws Throwable {
		  try {
		      Navigation.navigate_to_signOut();
		  }
		  finally{
		      Browser.closeBrowser();
		  }	  
	 }
}
