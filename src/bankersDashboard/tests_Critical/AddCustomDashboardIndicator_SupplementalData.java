/**
 * 
 */
package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.AddWidgets_StartPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

/**
 * @author Nisha
 * Smoke Test : Add Supplemental Data Custom Indicator to Start Page:
 *
 */
public class AddCustomDashboardIndicator_SupplementalData extends BaseTest {
	
	private static String storeSupplementalDataWidgetName;
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
		
		//Click Add Widget + Icon on start page header tab
		StartPage.clickAddWidgetIcon_CustomDashboardIndicatorOption();
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT1662_Step2_verifySupplementalDataOptionIsDisplayed() throws Exception {
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("supplementalDataOption_label"));
		BaseUI.verifyElementHasExpectedText("supplementalDataOption_label", "Supplemental data.");
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT1662_Step4_verifySupplementalDataWidgetTextbox() throws Exception {
		AddWidgets_StartPage.clickSupplementalDataRadioBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("supplementalDataIndicatorName_textbox"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 8)
	public void PT1662_Step8_verifyIndicatorMeterStyleText() throws Exception {
		storeSupplementalDataWidgetName = AddWidgets_StartPage.addSupplementalDataIndicatorDetails();
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyIndicatorMeterStyleText"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT1662_Step11_verify_CustomCalcIndicatorPreviewText() throws Exception {
		AddWidgets_StartPage.clickRulerStyleRadioBtn();
		StartPage.customIndicatorModule_clickNextButton();
		BaseUI.verifyElementAppears(Locator.lookupElement("Verify_customCalcIndicatorPreviewText"));
	}

	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 13)
	public void PT1662_Step13_verifySupplementalWidgetIsDisplayedOnStartpage() throws Exception {
		StartPage.customIndicatorModule_clickSaveButton();
		StartPage.clickStartPageTab();
		//BaseUI.scroll_to_element(Locator.lookupElement("verifyBalancesIndicatorAdded_startPage"));
		BaseUI.verifyElementAppears(Locator.lookupElement("verifysupplementalDataAdded_startPage"));
		BaseUI.verifyElementHasExpectedText("verifysupplementalDataAdded_startPage", storeSupplementalDataWidgetName);		
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
  
  
  @AfterClass(alwaysRun = true)
  public void tear_down() throws Throwable
  {  
	  try {
		  StartPage.clickStartPageTab();
		  StartPage.deleteWidgetFromStartPage("supplementalDataWidgetDeleteIcon", storeSupplementalDataWidgetName, 
				  "deleteAnyCustomWidgets");
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }
	  
  }

}
