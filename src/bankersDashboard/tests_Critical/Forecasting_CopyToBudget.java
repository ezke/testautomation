package bankersDashboard.tests_Critical;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;

public class Forecasting_CopyToBudget extends BaseTest {
		
	static String email = GlobalVariables.emailAddress;
	static Email firstEmail;
	static String emailBodyContent;
	static String emailSubject;
	static String fcName = "TestAutomationFC2017";
	static String expEmailBodyContent = "The forecast project named "+ fcName +" has been copied to the data source Budget.";
	static String expEmailSubject = "Forecast is Ready";
	static String emailCreationDate;
	static String view = "Summary";
	static String profitCenter = "Consolidated";
	static String primDataSource = "TestAutomationFC2017";
	static String primAvg = "No";
	static String primDate = "April 2013";
	static String secDataSource = "Budget";
	static String secAvg = "No";
	static String secDate = "April 2013";
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
	
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		//Select Existing Forecasting project and 
		//Choose Copy to Budget option under Tools menu
		ForecastingPage.selectExistingFC_SelectToolsOption(fcName, "Copy to Budget");
		ForecastingPage.switchToModalIframe();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1570_Step3_VerifyNewCreatedForecastingProject() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CopyToBudget_CopyBtn"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public static void PT1570_Step3_VerifyNewCreatedForecastingProject_CopyToBudgetMsg() throws Exception {
		ForecastingPage.clickCopyToBudgetModal_CopyBtn();
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CopyToBudget_FCCopyToBudgetMsg"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CopyToBudget_FCCopyToBudgetMsg", "Copying Project to Budget");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public static void PT1570_Step3_VerifyForecastingProject_CopyToBudget_EmailReceived() throws Exception {
//		StartPage.switchToIframe();
//		BaseUI.click(Locator.lookupElement("forecasting_CopyToBudget_CloseBtn"));
//		Thread.sleep(1000);
		BaseUI.switch_ToDefaultContent();
    	Email_API.Wait_ForEmail_ToExist(email, 300);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");		
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1570_Step3_VerifyForecastingProject_CopyToBudget_EmailSubject() throws Exception {
		emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		BaseUI.verify_true_AndLog(emailSubject.equals(expEmailSubject), 
				"Email subject displays copy to Forecast message: "+ emailSubject, 
				 "Email subject DOES NOT display copy to Forecast message: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1570_Step3_VerifyForecastingProject_CopyToBudget_EmailCreationDate() throws Exception {
		 emailCreationDate = Email_API.return_EmailCreationDate_FromEmail(firstEmail);
		 BaseUI.verify_true_AndLog(String.valueOf(emailCreationDate)
				 .contains(BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/d/yyyy", "yyyy-MM-dd")), 
				 "Email Creation date MATCHES System date: "+ String.valueOf(emailCreationDate), 
				 "Email Creation date DOES NOT MATCHES System date: "+ String.valueOf(emailCreationDate));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1570_Step3_VerifyForecastingProject_CopyToBudget_EmailContent() throws Exception {
		emailBodyContent = Email_API.return_EmailData_FromEmailBody(firstEmail);
		BaseUI.verify_true_AndLog(emailBodyContent.contains(expEmailBodyContent), "Email contents FOUND: "+ emailBodyContent, 
				"Email contents NOT FOUND: "+emailBodyContent);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1570_Step4To5_VerifyForecastingProject_CopyToBudget_NavigateToBSCompareReport() throws Exception {
		BaseUI.switch_ToDefaultContent();
		EntitySelector.selectWindow("Banker's Dashboard");
		EntitySelector.close_ExtraWindows("Banker's Dashboard");
		BalanceSheetComparePage.openBSCompareReport_WithForecastingProject(view, profitCenter, primDataSource, primAvg,
				primDate, secDataSource, secAvg, secDate);
		BalanceSheetComparePage.verifyBalanceSheet_PrimColData_WithSecColData();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1570_Step4To5_VerifyForecastingProject_CopyToBudget_VerifyFCProjName_BSCompareReport() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("balanceSheetRep_ForecastingProjName"));
		BaseUI.verifyElementHasExpectedText("balanceSheetRep_ForecastingProjName", "* Forecast 1 = "+fcName);
	}

	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  Email_API.delete_Emails_ByRecipient(email);	 
	      Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
