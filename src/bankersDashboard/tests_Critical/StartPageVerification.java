package bankersDashboard.tests_Critical;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class StartPageVerification extends BaseTest {
	
	@BeforeClass(alwaysRun = true)
	public void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);	
	}
	
	@Test
	public void PT3828_Step1_PT3828_Step4_Verify_BDLogo() throws Exception
	{
		EntitySelector.imageVerification("loginPage_BD_logo");
		
	}
	
	@Test
	public void PT3828_Step4_verify_User_Logged() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Username_dropdown"));
		//BaseUI.verifyElementHasExpectedText("startPage_Username_dropdown", GlobalVariables.userLogged);
	}
	
	@Test
	public void PT3828_Step4_verify_Announcement_header() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Announcements_text"));
		//BaseUI.verifyElementHasExpectedText("verify_Announcements_text", GlobalVariables.announcement_txt);
		
	}
	
	@Test
	public void PT3828_Step4_verify_Help_header() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Help_txt"));
		BaseUI.verifyElementHasExpectedText("startPage_Help_txt", GlobalVariables.help_header_txt);	
	}
	
	@Test
	public void PT3828_Step4_verify_myAlerts_header() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_MyAlerts_text"));
		//BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupElement("Verify_MyAlerts_text"), GlobalVariables.myAlerts_header_txt);		
	}
	
	@Test
	public void PT3828_Step4_verify_myFavorites_header() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_MyFavReports_dropdown"));
		BaseUI.verifyElementHasExpectedText("startPage_MyFavReports_dropdown", GlobalVariables.myFavorties_header_txt);		
	}
	
	@Test
	public void PT3828_Step1_verify_profitCenter_bankEntity() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		BaseUI.verifyElementHasExpectedText("startPage_ProfitCenterEntity_displayed", ClientDataRetrieval.profitCenter);	
	}
	
	
	@Test
	public void PT3828_Step4_verify_Summary_widget() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Summary_widget"));
		BaseUI.verifyElementHasExpectedText("startPage_Summary_widget", GlobalVariables.summary_widget_txt);
	}
	
	
	@Test
	public void PT3828_Step4_verify_AddwidgetIcon() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_AddWidget_Icon"));
	}
	
	
//	@Test
//	public void PT3828_Step4_verify_ExportToPDF_Icon() throws Exception
//	{
//		BaseUI.elementAppears(Locator.lookupElement("ExportToPDF"));
//		//BaseUI.verifyElementAppears(Locator.lookupElement("ExportToPDF"));
//	}
//	
//	
//	@Test
//	public void PT3828_Step4_verify_ExportToExcel_Icon() throws Exception
//	{
//		BaseUI.verifyElementAppears(Locator.lookupElement("ExportToExcel"));
//	}
//	
//	
//	@Test
//	public void PT3828_Step4_verify_RefreshIcon() throws Exception
//	{
//		BaseUI.verifyElementAppears(Locator.lookupElement("Refresh_button"));
//	}
	
	
	@Test
	public void PT3828_Step4_verify_ReportProperties_Icon() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Report_properties_Icon"));
	}
	
	
	@Test
	public void PT3828_Step4_verify_DashboardTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Dashboards"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_GLTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_GL"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_ALTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_AL"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_ProjectionsTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Projections"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_ReportPackagesTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_ReportPackages"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_BranchPerformanceTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_BranchPerformance"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_LoanPricingTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_LoanPricing"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_ToolsTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Tools"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_SettingsTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Settings"));
		
	}
	
	
	@Test
	public void PT3828_Step4_verify_AdminTab() throws Exception
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Admin"));
		
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
	
	 @AfterClass(alwaysRun = true)
	  public void tear_down() throws Throwable
	  {
		  try {
		      Navigation.navigate_to_signOut();
		  }
		  finally{
		      Browser.closeBrowser();
		  }		  
	  }

}
