package bankersDashboard.tests_ignored;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mailosaur.model.Email;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.ForecastingPage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Email_API;
import utils.Locator;
import utils.ResultWriter;

public class Forecasting_SynchronizeHistorical extends BaseTest {

	static Email firstEmail;
	static String emailBodyContent;
	static String emailSubject;
	static String email = "qaautomation" + Email_API.email_Ending;
	static String expEmailSubject = "Forecast project is ready";
	static long emailCreationDate;
	//static String expEmailBodyContent = "The forecast project named '"+fcName+"' is ready to be edited.";
	private static int fcName = 3;
	private static String existingFCProj;
//	private static String existingFCProj_SyncDate;
	private static String copiedFCProj_FirstMonthYearHeader;
	private static String SyncThruDate;
	private static String synchronizedFCProj_FirstMonthYearHeader;
	private static String syncThruYear;
	private static String newFCName;
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Email_API.delete_Emails_ByRecipient(email);
		
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
	
		StartPage.clickStartPageTab();
		
		//Select existing forecasting Project
		ForecastingPage.selectExistingFCByIndex(fcName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public static void PT1564_Step4_VerifyForecastingProject_SynchronizeHistProj_ExistingProjectName() throws Exception {
		WebElement exisFCProjName = Locator.lookupElement("forecasting_ExistingForecaseProjName");
		existingFCProj = BaseUI.getTextFromField(exisFCProjName);
		BaseUI.verifyElementAppears(exisFCProjName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public static void PT1564_Step5To6_VerifyForecastingProject_SynchronizeHistProj_CreateCopy_ExistingFCProject() throws Exception {
		newFCName = ForecastingPage.createCopyOfExistingForecastingProject(existingFCProj);
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateFromExistingFCProj_FCName"));
		BaseUI.verifyElementHasExpectedText("forecasting_CreateFromExistingFCProj_FCName", newFCName);	
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1564_Step6_VerifyForecastingProject_SynchronizeHistProj_ExistingProjectNameDisplaysOnConfirmationPage() throws Exception {
		WebElement exisFCProjName_ConfirmationPage = Locator.lookupElement("forecasting_CreateFromExistingFCProj_ExistingFCName");
		BaseUI.verifyElementAppears(exisFCProjName_ConfirmationPage);
		BaseUI.verifyElementHasExpectedText("forecasting_CreateFromExistingFCProj_ExistingFCName", existingFCProj);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public static void PT1564_Step6_VerifyForecastingProject_SynchronizeHistProj_SyncDateDisplaysOnConfirmationPage() throws Exception {
		WebElement syncDate_ConfirmationPage = Locator.lookupElement("forecasting_CreateFromExistingFCProj_ExistingFCName");
//		existingFCProj_SyncDate = BaseUI.getTextFromField(syncDate_ConfirmationPage);
		BaseUI.verifyElementAppears(syncDate_ConfirmationPage);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public static void PT1564_Step7_VerifyForecastingProject_SynchronizeHistProj_CopiedProjectNameDisplayed() throws Exception {
		ForecastingPage.clickSaveButton();
		WebElement copiedFCProjName = Locator.lookupElement("forecasting_ExistingForecaseProjName");
		BaseUI.verifyElementAppears(copiedFCProjName);
		BaseUI.verifyElementHasExpectedText("forecasting_ExistingForecaseProjName", 
				newFCName);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public static void PT1564_Step7_VerifyForecastingProject_SynchronizeHistProj_CopiedProjectName_FirstMonthYearColumnHeader() throws Exception {
		WebElement copiedFCProj_MonthYearHeader = Locator.lookupElement("forecasting_1stMonthYearColumnHeader");
		copiedFCProj_FirstMonthYearHeader = BaseUI.getTextFromField(copiedFCProj_MonthYearHeader);
		SyncThruDate = BaseUI.getTextFromField(copiedFCProj_MonthYearHeader).substring(3).trim();
		syncThruYear = ForecastingPage.convertSynDate(SyncThruDate);
		BaseUI.verifyElementAppears(copiedFCProj_MonthYearHeader);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public static void PT1564_Step8_VerifyForecastingProject_SynchronizeHistProj_CopiedProjectName_SynchronizeProject() throws Exception {
		ForecastingPage.synchronizeHistorical_ForecastingProject(syncThruYear);
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_SyncHistoricalProj_SyncProjectProgressModalTitle"));
		BaseUI.verifyElementHasExpectedText("forecasting_SyncHistoricalProj_SyncProjectProgressModalTitle", "Synchronize in Progress");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public static void PT1564_Step9_VerifyForecastingProject_SynchronizeHistProj_CopiedProjectName_SynchronizeProjectMsg() throws Exception {
		ForecastingPage.switchToModalIframe();
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_SyncHistoricalProj_SyncProjectProgressMsg"));
		BaseUI.verifyElementHasExpectedText("forecasting_SyncHistoricalProj_SyncProjectProgressMsg", "Preparing Project");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
	public static void PT1564_Step9_VerifyForecastingProject_SynchronizeHistProj_CopiedProjectName_SynchronizeProjectDetailedMsg() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_SyncHistoricalProj_SyncProjectProgress_ResyncMsg"));
		BaseUI.verifyElementHasExpectedPartialText("forecasting_SyncHistoricalProj_SyncProjectProgress_ResyncMsg", 
				"Your project is now being resynchronized. It will be unavailable for use until the process completes. An e-mail will be sent to ");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 90)
	public static void PT1564_Step9_VerifyForecastingProject_SynchronizeHistProj_CopiedProjectName_CloseSynchronizeProjectModal() throws Exception {
		ForecastingPage.clickSynchronizeHistoricalCloseBtn();
		BaseUI.switch_ToDefaultContent();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("forecasting_SyncHistoricalProj_SyncProjectProgressModalTitle"));
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 100)
	public static void PT1564_Step10_VerifyForecastingProject_SynchronizeHistProj_EmailReceived() throws Exception {
		EntitySelector.selectWindow("Banker's Dashboard");
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_startPageTab"));
		//Wait for the forecasting project ready email
    	Email_API.Wait_ForEmail_ToExist(email, 600);
		firstEmail = Email_API.return_FirstEmail_ByRecipient(email);
		BaseUI.verify_true_AndLog(firstEmail != null, "Found Email", "DID NOT FIND Email");	
		emailSubject = Email_API.return_EmailSubject_FromEmail(firstEmail);
		BaseUI.verify_true_AndLog(emailSubject.equals(expEmailSubject), 
				"Email subject displays Forecast message: "+ emailSubject, 
				 "Email subject DOES NOT display Forecast message: "+ emailSubject);
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 110)
	public static void PT1564_Step11_VerifyForecastingProject_SynchronizeHistProj_NavigateAndOpenSynchronizedProject() throws Exception {
        StartPage.clickStartPageTab();
		
		//Select existing forecasting Project
		ForecastingPage.selectExistingFCByName(newFCName);
		WebElement copiedFCProj_MonthYearHeader = Locator.lookupElement("forecasting_1stMonthYearColumnHeader");
		BaseUI.verifyElementAppears(copiedFCProj_MonthYearHeader);
		synchronizedFCProj_FirstMonthYearHeader = BaseUI.getTextFromField(copiedFCProj_MonthYearHeader);
		BaseUI.baseStringCompareStringsAreDifferent("forecasting_1stMonthYearColumnHeader", copiedFCProj_FirstMonthYearHeader, 
				synchronizedFCProj_FirstMonthYearHeader);
	}
	
	
//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 120)
//	public static void PT1564_Step12_VerifyForecastingProject_SynchronizeHistProj_DeleteSynchronizedProject() throws Exception {
//		
//		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_Logout_option"));
//	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	  try {
		  Email_API.delete_Emails_ByRecipient(email);
		  ForecastingPage.deleteForecastingProjectWhenOnFCPage_ForecastingProjectDisplayed();
	      //Navigation.navigate_to_signOut();
	  }
	  finally{
	      Browser.closeBrowser();
	  }	  
    }
}
