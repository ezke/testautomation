/**
 * 
 */
package bankersDashboard.pages;

import bankersDashboard.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Locator;

/**
 * @author Nisha
 *
 */
public class AddNewUserPage {
	
	public static void enterNewUserDetails() throws Exception
	{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_UserName_Textbox"), GlobalVariables.newUserName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_FirstName_Textbox"), GlobalVariables.firstName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_LastName_Textbox"), GlobalVariables.lastName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_CompanyName_Textbox"), GlobalVariables.companyName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Location_Textbox"), GlobalVariables.location);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Email_Textbox"), GlobalVariables.emailAddress);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Phone_Textbox"), GlobalVariables.phoneNumber);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Password_Textbox"), GlobalVariables.PasswordNew);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_ConfirmPassword_Textbox"), GlobalVariables.confirmPassword);
		Thread.sleep(1000);
		BaseUI.verifyElementHasExpectedText("manageUsers_PasswordStrongText", "Password strength: Strong");
		//BaseUI.selectValueFromDropDownByPartialText(Locator.lookupElement("manageUsers_CopyDefaultsFrom_Dropdown"), "Nisha");
	}
	
	
	public static void enterNewUserDetails_CopyDefaultsFromExistingUser() throws Exception
	{
		enterNewUserDetails();
		BaseUI.selectValueFromDropDownByPartialText(Locator.lookupElement("manageUsers_CopyDefaultsFrom_Dropdown"), "Nisha");
		Thread.sleep(500);
	}
	
	
	public static void updateExistingUserDetails() throws Exception
	{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_UserName_Textbox"), GlobalVariables.newUserName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_FirstName_Textbox"), GlobalVariables.firstName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_LastName_Textbox"), GlobalVariables.lastName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_CompanyName_Textbox"), GlobalVariables.companyName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Location_Textbox"), GlobalVariables.location);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Email_Textbox"), GlobalVariables.emailAddress);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Phone_Textbox"), GlobalVariables.phoneNumber);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_Password_Textbox"), GlobalVariables.PasswordNew);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("manageUsers_ConfirmPassword_Textbox"), GlobalVariables.confirmPassword);
		Thread.sleep(1000);
		BaseUI.verifyElementHasExpectedText("manageUsers_PasswordStrongText", "Password strength: Strong");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("manageUsers_CopyDefaultsFrom_Dropdown"), "Nayak, Nisha");
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("manageUsers_ResendWelcomeEmail_RadioBtn_YesOption"));
		BaseUI.click(Locator.lookupElement("manageUsers_ResendWelcomeEmail_RadioBtn_NoOption"));
	}
	
	
	
	public static void setAccessForNewUser() throws Exception
	{
		BaseUI.click(Locator.lookupElement("manageUsers_UserAdmin_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_DocumentVaultAdmin_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_AllowTabletAppAccess_Checkbox"));
		//Thread.sleep(1000);
		BaseUI.click(Locator.lookupElement("manageUsers_EntityAccessEnable_RadioBtn"));
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("manageUsers_AllProfitCenterOption_Checkbox"));
		Thread.sleep(200);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("manageUsers_EntityAccess_Dropdown"), 
				ClientDataRetrieval.profitCenter);
		BaseUI.click(Locator.lookupElement("manageUsers_EntityAccessEnable_RadioBtn"));
		BaseUI.click(Locator.lookupElement("manageUsers_AllProfitCenterOption_Checkbox"));
		//Thread.sleep(1000);
		BaseUI.click(Locator.lookupElement("manageUsers_GLAccess_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_ALAccess_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_ParametersAdmin_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_DesignBoardPackage_Checkbox"));
		//Thread.sleep(1000);
		BaseUI.click(Locator.lookupElement("manageUsers_ProjectionsAdmin_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_MappingAdmin_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_AccessDataExport_Checkbox"));
		BaseUI.click(Locator.lookupElement("manageUsers_UpdateMemoFields_Checkbox"));
		//click_ManageUserSetAccess_Cancelbutton();	
		clickFinishedBtn();
		StartPage.switchToIframe();
		BaseUI.waitForElementToBeDisplayed("manageUsers_AddUserBtn", null, null);
	}

	
	public static void verifyCheckbox_Checked_SetAccessForNewUser() throws Exception
	{
		BaseUI.verifyCheckboxStatus("manageUsers_UserAdmin_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_UserAdmin_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_DocumentVaultAdmin_Checkbox", "checked");
		//BaseUI.verifyCheckboxStatus("allowTabletAppAccess_Checkbox", "unchecked");
		//Thread.sleep(1000);
		select_EntityAndSetAccess(ClientDataRetrieval.profitCenter);
		BaseUI.verifyElementEnabled(Locator.lookupElement("manageUsers_EntityAccessEnable_RadioBtn"));
		//BaseUI.verifyCheckboxStatus("entityAccessEnable_RadioBtn"));
		BaseUI.verifyCheckboxStatus("manageUsers_AllProfitCenterOption_Checkbox", "checked");
		//Thread.sleep(1000);
		BaseUI.verifyCheckboxStatus("manageUsers_GLAccess_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_ALAccess_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_ParametersAdmin_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_DesignBoardPackage_Checkbox", "checked");
		//Thread.sleep(1000);
		BaseUI.verifyCheckboxStatus("manageUsers_ProjectionsAdmin_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_MappingAdmin_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_AccessDataExport_Checkbox", "checked");
		BaseUI.verifyCheckboxStatus("manageUsers_UpdateMemoFields_Checkbox", "checked");
		click_ManageUserSetAccess_Cancelbutton();			
	}
	
	
	public static void click_ManageUserSetAccess_Cancelbutton() throws InterruptedException {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("manageUsers_CancelBtn"));
		Thread.sleep(500);
	}
	
	
	public static void select_EntityAndSetAccess(String entity) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("manageUsers_EntityAccess_Dropdown"), entity);
		Thread.sleep(500);
	}
	
	
	public static void click_ManageUser_DeleteUser() throws Exception {
		StartPage.switchToIframe();
		BaseUI.click(Locator.lookupElement("manageUsers_DeleteNewlyCreatedUserIcon"));
		Thread.sleep(500);
	}
	
	
	public static void click_VerifyNewUser_Deleted() throws Exception {
		StartPage.clickCloseAllTabsButton();
		Navigation.navigateToAdmin_Users_ManageUsers();
		click_ManageUser_DeleteUser();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("manageUsers_DeleteNewlyCreatedUserIcon"));
		clickFinishedBtn();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("manageUsers_ModalTitle"));
	}
	
	
	public static void clickFinishedBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click_js(Locator.lookupElement("manageUsers_FinishedBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	
	public static void clickAddUserBtn() throws Exception {
		StartPage.switchToIframe();
		BaseUI.click(Locator.lookupElement("manageUsers_AddUserBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	public static void checkAllProfitCenter_CheckboxChecked() {
		BaseUI.verifyCheckboxStatus("manageUsers_AllProfitCenterOption_Checkbox", "checked");
	}
	
	
	public static String addNewUser_CopyDefaultFromExistingUser() throws Exception {
		Navigation.navigateToAdmin_Users_ManageUsers();
		AddNewUserPage.clickAddUserBtn();
		BaseUI.waitForElementToBeDisplayed("manageUsers_UserNameLabelDisplayed", null, null);
		AddNewUserPage.enterNewUserDetails_CopyDefaultsFromExistingUser();
		AddNewUserPage.clickFinishedBtn();
		StartPage.switchToIframe();
		BaseUI.waitForElementToBeDisplayed("manageUsers_GetUsernameOfNewCreatedUser", null, null, 1000);
		String storeNewUserName = BaseUI.getTextFromField(Locator.lookupElement("manageUsers_GetUsernameOfNewCreatedUser"));
		
		AddNewUserPage.checkAllProfitCenter_CheckboxChecked();
		AddNewUserPage.clickFinishedBtn();

		StartPage.switchToIframe();
		BaseUI.waitForElementToBeDisplayed("manageUsers_AddUserBtn", null, null);
		AddNewUserPage.clickFinishedBtn();		
		BaseUI.wait_forElementToNotExist("manageUsers_ModalTitle", null, null);
		
		StartPage.clickCloseAllTabsButton();		
		return storeNewUserName;
	}
	
	
	public static void clickCloseManageUserModal() throws Exception {
		//BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("manageUsers_CloseManageUserModal"));
		Thread.sleep(1000);
	}
	
	
	public static void addNewUserCopyFromExistingUser_LoginAsNewUser() throws Exception {
		StartPage.clickCloseAllTabsButton();
		StartPage.clickStartPageTab();
		
		String newUser = AddNewUserPage.addNewUser_CopyDefaultFromExistingUser();
		Navigation.navigate_to_signOut();
		
		//Login into Browser as new user
		LoginPage.loginAsNewlyCreatedUser(newUser);
	}
	
	
	public static void addNewUser_LoginAsNewUser() throws Exception {
		StartPage.clickCloseAllTabsButton();
		StartPage.clickStartPageTab();
		
		String newUser = addNewUser();
		Navigation.navigate_to_signOut();
		
		//Login into Browser as new user
		LoginPage.loginAsNewlyCreatedUser(newUser);
	}
	
	
	public static String addNewUser() throws Exception {
		Navigation.navigateToAdmin_Users_ManageUsers();
		AddNewUserPage.clickAddUserBtn();
		BaseUI.waitForElementToBeDisplayed("manageUsers_UserNameLabelDisplayed", null, null);
		AddNewUserPage.enterNewUserDetails();
		AddNewUserPage.clickFinishedBtn();
		switchToIframe();
		String storeNewUserName = BaseUI.getTextFromField(Locator.lookupElement("manageUsers_GetUsernameOfNewCreatedUser"));
		
		clickAllProfitCenterCheckbox();
		setAccessForNewUser();
				
		AddNewUserPage.clickFinishedBtn();	
		
		StartPage.clickCloseAllTabsButton();		
		return storeNewUserName;
	}
	
	
	public static void clickAllProfitCenterCheckbox() throws Exception {
		BaseUI.click(Locator.lookupElement("manageUsers_AllProfitCenterOption_Checkbox"));
		Thread.sleep(500);
	}
	
	
	public static void switchToIframe() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		Thread.sleep(500);
	}
}
