package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class UserPreferencesModal {
	
	public static void select_DefaultProfitCenter(String profitCenter) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("userPreferences_DefaultProfitCenter"), 
				profitCenter);
		Thread.sleep(500);
	}
	
	public static void click_FinishedButton() throws Exception {
	    BaseUI.switch_ToDefaultContent();
		BaseUI.click_js(Locator.lookupElement("userPreferences_FinishedBtn"));
		Thread.sleep(1000);
		BaseUI.wait_forElementToNotExist("userPreferences_FinishedBtn", null, null);
	}
	
	public static void select_UserPreferences_ProfitCenter(String profitCenter) throws Exception {
		select_DefaultProfitCenter(profitCenter);
		click_FinishedButton();
		BaseUI.wait_forPageToFinishLoading();
		StartPage.clickStartPageTab();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_ProfitCenter"));
	}
	
	
	public static void select_ProfitCenter(int profitCenter) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("userPreferences_DefaultProfitCenter"), 
				profitCenter);
		Thread.sleep(500);
	}
	
	
	public static String select_UserPreferences_ProfitCenterByIndex(int profitCenter) throws Exception {
		select_ProfitCenter(profitCenter);
		String profitCenterSelected = BaseUI.getSelectedOptionFromDropdown("userPreferences_DefaultProfitCenter");
		click_FinishedButton();
		BaseUI.wait_forPageToFinishLoading();
		StartPage.clickStartPageTab();
		BaseUI.verifyElementAppears(Locator.lookupElement("summaryWidget_ProfitCenter"));
		return profitCenterSelected;
	}

}
