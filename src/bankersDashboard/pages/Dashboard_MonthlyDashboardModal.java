package bankersDashboard.pages;

import bankersDashboard.pageControls.DatePicker;
import utils.BaseUI;
import utils.Locator;

public class Dashboard_MonthlyDashboardModal {
	
	static String dataSourceOptionSelected;
	static String startDateSelected; 
	
	public static void selectDate_DataSourceOption(String datasrc, int dateValue) throws Exception {
		Navigation.navigateToMonthlyDashboard();
		BaseUI.selectValueFromDropDown(Locator.lookupElement("monthlyDashboard_Datasource_dropdown"), datasrc);
		Thread.sleep(200);
		dataSourceOptionSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_Datasource_dropdown");
		Thread.sleep(300);
		startDateSelected = BaseUI.getSelectedOptionFromDropdown("monthlyDashboard_StartDateOption");
		//clickShowReportBtn();
	}
	
	
	public static void clickShowReportBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("monthlyDashboard_ShowRepBtn"));
		Thread.sleep(3000);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null); 
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("monthlyDashboardReportTitle", null, "60");
	}
	
	
	public static void verifyDataSourceOnReportPage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("monthlyDashboard_dataSourceDisplayedOnReport"));
		BaseUI.verifyElementHasExpectedText("monthlyDashboard_dataSourceDisplayedOnReport", dataSourceOptionSelected);
	}
	
	
    public static void verifyDateValueOnReportPage() {
    	String datedisplayedOnReport = BaseUI.getTextFromField(Locator.lookupElement("monthlyDashboard_datedisplayedOnReport"));
    	BaseUI.baseStringCompare("monthlyDashboard_datedisplayedOnReport", DatePicker.verifyDateDisplayed(startDateSelected), 
    					datedisplayedOnReport);
	}
    
    
    public static void clickReverseChronoCheckbox() throws Exception {
    	BaseUI.click(Locator.lookupElement("monthlyDashboard_ReverseChronoOrder_checkbox"));
		Thread.sleep(100);
    }
    
    public static void verifyDateSelectedDisplayForFirstColumn_ReverseChronoChecked(String startDateOptionSelected) throws Exception {
    	String datedisplayedOnReport = BaseUI.getTextFromField(Locator.lookupElement("monthlyDashboard_datedisplayedOnReport"));
		BaseUI.baseStringCompare("monthlyDashboard_datedisplayedOnReport", 
				BaseUI.return_Date_AsDifferentFormat(startDateOptionSelected, "MMMM yyyy", "MMM yyyy"), datedisplayedOnReport);
    }
    
    public static void verifyDateSelectedDoesNotDisplayForFirstColumn_ReverseChronoUnchecked(String startDateOptionSelected) throws Exception {
    	String datedisplayedOnReport = BaseUI.getTextFromField(Locator.lookupElement("monthlyDashboard_datedisplayedOnReport"));
    	BaseUI.baseStringCompareStringsAreDifferent("monthlyDashboard_datedisplayedOnReport", 
    			BaseUI.return_Date_AsDifferentFormat(startDateOptionSelected, "MMMM yyyy", "MMM yyyy"), datedisplayedOnReport);
    }
}
