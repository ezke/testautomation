package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class IncomeStatementModal {
	
	public static void clickShowReportButton() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("balanceSheet_showReportBtn"));
		Thread.sleep(2500);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.waitForElementToBeDisplayed("incomeStatementCompare_ReportTitle", null, null);
	}

}
