package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class AL_SummariesMaturitiesReportPage {

	public static ArrayList<String> return_SumMaturitiesRepTable_DateColumnData() {
		ArrayList<WebElement> dateColumnData = Locator.lookup_multipleElements("alSum_MaturitiesReport_Date_ColumnList", null,
				null);

		ArrayList<String> dateColList = new ArrayList<String>();

		for (WebElement date : dateColumnData) {
			String dateValue = BaseUI.getTextFromField(date);
			dateColList.add(dateValue);
		}
		
		return dateColList;
	}
	
	
	public static void clickReportPropertiesIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_Report_properties_Icon"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("alSum_MaturitiesModal_ModalTitle", null, null, 60);
	}
	
	
	public static String clickRepProperties_SelectNewDateOption(String viewOption, 
			int officersOption, int regBranchOption, String dateOption) throws Exception {
		clickReportPropertiesIcon();
		AL_SummariesMaturitiesModal.selectViewOption(viewOption);
		AL_SummariesMaturitiesModal.selectOfficersOption(officersOption);
		AL_SummariesMaturitiesModal.selectRegion_BranchOption(regBranchOption);
		AL_SummariesMaturitiesModal.selectDateOption(dateOption);
		String regOptionSelected = BaseUI.getSelectedOptionFromDropdown("alSum_MaturitiesModal_Reg_Branch_Dropdown");
		String officersOptionSelected = BaseUI.getSelectedOptionFromDropdown("alSum_MaturitiesModal_Officers_Dropdown");
		String reg_OfficerSelected = (regOptionSelected+" - "+officersOptionSelected).trim();
		AL_SummariesMaturitiesModal.clickShowReportBtn();
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("alSum_MaturitiesReport_ReportTitle", 
				null, null);
		
		return reg_OfficerSelected;
	}
}
