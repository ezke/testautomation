package bankersDashboard.pages;

import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
public class ReportPackages {
	
	
	private static String randomNum = BaseUI.random_NumberAsString(1, 500);
	private static final String packageName = "TestReportPackage";
	public static String storePackageName;
	public static String reportNameDisplayed1, reportNameDisplayed2;
		
	
	public static void verify_RunReportPackage_ModalTitle(){
		
		BaseUI.verifyElementAppears(Locator.lookupElement("runReportPackage_ModalTitle"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("runReportPackage_ModalTitle"), "Run Report Package");
	}
	
	
	public static void selectFromCurrentPackage(String packageName) throws Exception
	{
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUpReportPackage_CurrentPackage_DropDown"), packageName);
		Thread.sleep(2000);
	}
	
	
	public static void clickAddNewReportPackageIcon() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_AddNewReportPackage_PlusIcon"));
		Thread.sleep(2000);
	}
	
	
	public static void clickEditReportPackageIcon() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_EditReportPackage_GearIcon"));
		Thread.sleep(2000);
	}

	
	
	public static void clickDeleteReportPackageIcon() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_DeleteReportPackage_CrossIcon"));
		BaseUI.switchToAlert_Accept();
		Thread.sleep(2000);
	}
	
	
	public static void clickFavoriteReportPackageIcon() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_FavoriteReportPackage"));
		Thread.sleep(2000);
	}
	
	
	public static void selectReportFromList(String reportName) throws Exception
	{
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("setUpReportPackage_AddNewReport_DropDown"), reportName);
		Thread.sleep(2000);
	}
	
	
	public static void clickEditReportIcon() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_EditReportFromList"));		
		Thread.sleep(2000);
	}
	
	
	
	public static void clickDeleteReportIcon() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_DeleteReportFromList"));
		BaseUI.switchToAlert_Accept();
		Thread.sleep(2000);
	}
	
	
	
	public static void enterPackageName(String packageName) throws Exception
	{
		BaseUI.enterText(Locator.lookupElement("setUpReportPackage_PackageName_Txtbox"), packageName);
		Thread.sleep(2000);
	}
	
	
	
	public static void clickBoardPackage(String option) throws Exception
	{
		if(option=="yes")
		{
		   BaseUI.click(Locator.lookupElement("setUpReportPackage_BoardPackage_RadioBtn_YesOption"));
		   Thread.sleep(2000);
		}
		
		else
		{
		   BaseUI.click(Locator.lookupElement("setUpReportPackage_BoardPackage_RadioBtn_NoOption"));
		   Thread.sleep(2000);
		}
	}
	
	
	public static void clickSaveButton() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_SavBtn"));
		Thread.sleep(2000);
	}
	
	
	
	public static void clickCancelButton() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_Cancel_Btn"));
		Thread.sleep(2000);
	}
	
	
	
	public static void clickFinishedButton() throws Exception
	{
		BaseUI.click(Locator.lookupElement("setUpReportPackage_FinishedBtn"));
		Thread.sleep(2000);
	}
	
	
	public static void verifyReportPackageOnMyFavWidget()
	
	{
		//List<WebElement> numberOfReportPackage = 
	}
	
	
	public static void selectRunReportPackage(String reportPackageName) throws Exception
	{
		BaseUI.selectValueFromDropDown(Locator.lookupElement("runReportPackage_ReportPackage_dropdown"), reportPackageName);
		Thread.sleep(2000);
	}
	
	

    public static void selectProfitCenterCheckBox(String profitCenter){
         //Get the count of all available check boxes
    	
        ArrayList<WebElement> checkboxs = Locator.lookup_multipleElements("profitCenters_checkboxs", null, null);
    	int checkboxSize = checkboxs.size();
    	System.out.println(checkboxSize);
    	BaseUI.click(Locator.lookupElement("runReportPackage_ProfitCenters_Checkbox_Consolidated"));
    	
    	for (int i=0;i<checkboxSize; i++)
    	{
    		 // Store the checkbox name to the string variable, using 'Value' attribute
    		 
    		 String sValue = checkboxs.get(i).getAttribute("value");
    	 
    		 // Select the checkbox it the value of the checkbox is same what you are looking for
    	 
    		 if (sValue.equalsIgnoreCase(profitCenter)){
    	 
    			System.out.println(checkboxs.get(i));
    			BaseUI.click(checkboxs.get(i));
    	        
    			// This will take the execution out of for loop
    	        break;
    	        } 	 
    		}
    	}
     
    
    public static void selectProfitCenterCheckBox_consolidated() throws Exception
    {
       
      	BaseUI.click(Locator.lookupElement("runReportPackage_ProfitCenters_Checkbox_Consolidated"));
      	Thread.sleep(1000);
    }
	
	
    public static void selectProfitCenterCheckBox_region() throws Exception{
    	
    	BaseUI.checkCheckbox(Locator.lookupElement("runReportPackage_ProfitCenter_CheckBox_Region"));
       
   	    //BaseUI.click(Locator.lookupElement("profitCenters_checkbox_region"));
   	    Thread.sleep(2000);
    }
    
    
    public static String createNewReportPackageWithReports(String boardPkgOption) throws Exception
    {
    	Navigation.navigateToReportPackages_SetUpReportPackages();
		ReportPackages.clickAddNewReportPackageIcon();
		ReportPackages.enterPackageName(packageName+randomNum);
		storePackageName = BaseUI.getTextFromInputBox(Locator.lookupElement("setUpReportPackage_PackageName_Txtbox"));
		ReportPackages.clickBoardPackage(boardPkgOption);
		//BaseUI.verifyElementEnabled(Locator.lookupElement("setUpReportPackage_SavBtn"));
		ReportPackages.clickSaveButton();
		
		BaseUI.waitForElementToBeDisplayed("setUpReportPackage_FavoriteReportPackage", null, null);
		ReportPackages.clickFavoriteReportPackageIcon();
		ReportPackages.selectReportFromList("Monthly Dashboard");
		BaseUI.waitForElementToBeDisplayed("setUpReportPackage_MonthlyDashboardReportName", null, null, 500);
		reportNameDisplayed1 = BaseUI.getTextFromInputBox(Locator.lookupElement("setUpReportPackage_MonthlyDashboardReportName"));
		ReportPackages.clickSaveButton();
		ReportPackages.selectReportFromList("Capital Requirements");
		BaseUI.waitForElementToBeDisplayed("setUpReportPackage_CapitalRequirementsReportName", null, null, 500);
		reportNameDisplayed2 = BaseUI.getTextFromInputBox(Locator.lookupElement("setUpReportPackage_CapitalRequirementsReportName"));
		ReportPackages.clickSaveButton();
		ReportPackages.clickFinishedButton();
		BaseUI.wait_forElementToNotExist("setUpReportPackage_ModalTitle", null, null);
		return storePackageName;
    }
    
    
    public static String createNewReportPackage() throws Exception
    {
    	Navigation.navigateToReportPackages_SetUpReportPackages();
		//BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		Thread.sleep(1000);
		ReportPackages.clickAddNewReportPackageIcon();
		ReportPackages.enterPackageName(packageName+randomNum);
		storePackageName = BaseUI.getTextFromInputBox(Locator.lookupElement("setUpReportPackage_PackageName_Txtbox"));
		Thread.sleep(1000);
		ReportPackages.clickBoardPackage("no");
		//BaseUI.verifyElementEnabled(Locator.lookupElement("setUpReportPackage_SavBtn"));
		ReportPackages.clickSaveButton();
		Thread.sleep(1000);
		return storePackageName;
    
    }
    
    
    public static void deleteReportPackageAfterExecuting(String repPackageName) throws Exception
    {
    	Navigation.navigateToReportPackages_SetUpReportPackages();
		ReportPackages.selectFromCurrentPackage(repPackageName);
		ReportPackages.clickDeleteReportPackageIcon();
		ReportPackages.clickFinishedButton();
		BaseUI.switch_ToDefaultContent();
		StartPage.clickStartPageTab();
    }
    
    
    public static void verifyReportPackagesRunToScreen() throws Exception
    {
    	BaseUI.switch_ToDefaultContent();
	    StartPage.clickStartPageTab();
	    StartPage.clickStartPage_Tab2();
	    
	    //Validate the 1st Report title
	    BaseUI.verifyElementHasExpectedText("startPage_startPageTab2", ReportPackages.reportNameDisplayed1);
	    BaseUI.verifyElementHasExpectedText("monthlyDashboardReportTitle", ReportPackages.reportNameDisplayed1);
	    
	    StartPage.clickStartPageTab();
	    StartPage.clickStartPage_Tab3();
	    
	    //Validate the 2nd Report title
	    BaseUI.verifyElementHasExpectedText("startPage_startPageTab3", "Capital Reqs");
	    BaseUI.verifyElementHasExpectedText("capReq_ReportTitle", ReportPackages.reportNameDisplayed2);
	    
	    StartPage.clickCloseAllTabsButton();
	    StartPage.clickStartPageTab();
    }
    
    
    
    public static void clickRunPackageButton() throws Exception
	{
		 BaseUI.switch_ToDefaultContent();
		 BaseUI.click(Locator.lookupElement("runReportPackage_RunPackage_Btn"));
		 Thread.sleep(3000);
		 BaseUI.wait_forElementToNotExist("runReportPackage_RunPackage_Btn", null, null);
	}
    
    
    public static void clickCloseModalIconToCloseRunReportPackageModal() throws Exception
    {
         BaseUI.click(Locator.lookupElement("runReportPackage_CloseModal"));
         Thread.sleep(300);
    }
    
    
    public static void verifyProfitCenter_selectRepPackageAnd(String reportPackageName, String profitCenter) throws Exception {
    	ReportPackages.selectRunReportPackage(reportPackageName);
		WebElement regionProfitCenter = Locator.lookupElement(profitCenter);
		BaseUI.scroll_to_element(regionProfitCenter);
		BaseUI.verifyElementAppears(regionProfitCenter);
    }
    
    public static void clickExcelFileType_RadioBtn() throws Exception {
    	BaseUI.click(Locator.lookupElement("runReportPackage_FileTypeExcel_RadioButton"));
    	Thread.sleep(200);
    }
    
    
    public static void clickPdfFileType_RadioBtn() throws Exception {
    	BaseUI.click(Locator.lookupElement("runReportPackage_FileTypePDF_RadioButton"));
    	Thread.sleep(200);
    }
    
    public static void clickEmailRadioBtn() throws Exception {
    	BaseUI.click(Locator.lookupElement("runReportPackage_Format_ScreenOption_RadioButton"));
		Thread.sleep(200);
    }
}
