package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class BranchPerformanceModal {

	public static String enterNewViewDetails(String setAccessLevel) throws Exception {
		
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("branchPerf_CreateView_NameOrView"), 
				"TestView" + BaseUI.random_NumberAsString(0, 1000));
		String viewName = BaseUI.getTextFromInputBox(Locator.lookupElement("branchPerf_CreateView_NameOrView"));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("branchPerf_CreateView_ViewDescription"), 
				"Testing Creation of New View in Branch Performance" + " " + viewName);
		selectWhoHasAccess_FromDropdown(setAccessLevel);
		clickNextBtn();
		return viewName;
	}
	
	
	public static void addRankingWeights(String balSheetPercent, String incomePercent, String marginPercent) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("branchPerf_CreateView_BalancePercentage"), 
				balSheetPercent);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("branchPerf_CreateView_IncomePercentage"), 
				incomePercent);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("branchPerf_CreateView_MarginPercentage"), 
				marginPercent);
		BaseUI.click(Locator.lookupElement("branchPerf_CreateView_TotWeightPercentage"));
	}
	
	
	public static void selectWhoHasAccess_FromDropdown(String setAccessLevel) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("branchPerf_CreateView_PermissionDropdown"), 
				setAccessLevel);
		Thread.sleep(300);
	}
	
	
	public static void clickNextBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("branchPerf_CreateView_NextBtn"));
		Thread.sleep(1000);
	}
	
	
	public static void clickPrevioustBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("branchPerf_CreateView_PreviousBtn"));
		Thread.sleep(1000);
	}
	
	
	public static void clickSaveBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("branchPerf_CreateView_SaveBtn"));
		Thread.sleep(1000);
	}
	
	
	public static void dragAndDropCenters() throws Exception {
		//BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.dragAndDrop("myCenters_DragFromAvailableCentersBox_1", "myCenters_DropIntoMyCentersBox");
		Thread.sleep(300);
		BaseUI.dragAndDrop("myCenters_DragFromAvailableCentersBox_2", "myCenters_DropIntoMyCentersBox");
		Thread.sleep(300);
	}
}
