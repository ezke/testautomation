package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class ManageExistingViewsModal {
	
	public static void selectExistingView_FromDropdown(String selectView) throws Exception {
		StartPage.switchToIframe();
		BaseUI.selectValueFromDropDown(Locator.lookupElement("branchPerf_ManageExistView_AllViews"), 
				selectView);
		Thread.sleep(300);
	}
	
	public static void clickDeleteViewBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("branchPerf_ManageExistView_SelectBtn"));
		Thread.sleep(1000);
	}
	
	
	public static void selectExistingView(String viewName) throws Exception {
		Navigation.navigateToBranchPerformance_ManageExistingView();
		BaseUI.verifyElementAppears(Locator.lookupElement("branchPerf_ManageExistView_ModalTitle"));
		ManageExistingViewsModal.selectExistingView_FromDropdown(viewName);
		ManageExistingViewsModal.clickDeleteViewBtn();
	}

}
