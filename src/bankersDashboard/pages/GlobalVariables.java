package bankersDashboard.pages;

import bankersDashboard.data.ClientDataRetrieval;
import utils.BaseUI;

public class GlobalVariables {
	
	public static String baseURL_RC = "https://clients-rc.bankersdashboard.com/";
	public static String baseURL_QA = "https://clients-qa.bankersdashboard.com/";
	public static String bd_logo = "/public/images/banner/dlx-bd-logo.png";
	public static String browserTitle = "Banker's Dashboard";
	
	public static String userName = "nisha.nonpesv.00000";
	public static String password = "Password1*";
	public static String newPassword = "Password1!";
	public static String userLogged = "Deluxe Automation";
	public static String summary_widget_txt = "Summary";
	
	public static String announcement_txt = "Announcements";
	public static String help_header_txt = "Help";
	public static String myAlerts_header_txt = "My Alerts";
	public static String myFavorties_header_txt = "  My Favorite Reports";
	public static String profitCenter_entity = "Any Bank USA";
	public static String consolidated_bankEntity = "AnyBank USA, Inc & Subsidiaries";
	
	public static String reportPackageName = "TestReportPackage";
	public static String runReportPackageName = "My Favorite Reports";
	public static String runReportEmailMessage = "Your report package is currently being processed and will be emailed to you shortly. You may now close this window and continue working.";

	public static String newUserName = "qa.automation" + BaseUI.random_NumberAsString(0, 10000);
	public static String firstName = "Automation"+BaseUI.random_NumberAsString(0, 1000);
	public static String lastName = "Test" + BaseUI.random_NumberAsString(0, 1000);
	public static String companyName = "Deluxe";
	public static String location = "Atlanta";
	public static String emailAddress = "qaautomation2.j4pxicxw@mailosaur.io";
	public static String phoneNumber = "123456789";
	public static String PasswordNew = "Password1!";
	public static String confirmPassword = "Password1!";
	
	
	//Admin login details
	public static String bDashboard_Admin_URL = "";
	public static String bDashboard_Admin_Password = "Password1*";
	public static String bDashboard_Admin_Username = "nisha.nayak";
	
	
	//Credit Union Login Details
	public static String creditUnion_URL = "";
	
	public static String userName_CU = "daf.automation.C16030";
	public static String password_CU = "Password1*";
}

