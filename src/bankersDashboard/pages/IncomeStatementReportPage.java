package bankersDashboard.pages;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class IncomeStatementReportPage {
	
	 public static ArrayList<String> return_IncomeStatementRep_Column1List() {
		ArrayList<WebElement> isRep_Col1 = Locator.lookup_multipleElements("incomeStatementRep_SubheaderColumnList", null, 
				 null);

		ArrayList<String> isRep_Col1List = new ArrayList<String>();

		for (WebElement data : isRep_Col1) {
			String isValue = BaseUI.getTextFromField(data);
			isRep_Col1List.add(isValue);
		}
			
		return isRep_Col1List;
	}
	    
	    
	public static ArrayList<String> return_IncomeStatementRep_DollarChangeColList() {
		ArrayList<WebElement> isRep_$ChangeCol1 = Locator.lookup_multipleElements("incomeStatementRep_DollarChangeColumnList", null,
				null);

		ArrayList<String> isRep_$ChangeCol1List = new ArrayList<String>();

		for (WebElement data : isRep_$ChangeCol1) {
			String isValue = BaseUI.getTextFromField(data);
			isRep_$ChangeCol1List.add(isValue);
		}
			
		return isRep_$ChangeCol1List;
	}
	    
	    
	public static ArrayList<String> return_IncomeStatementRep_PercentChangeColList() {
		ArrayList<WebElement> isRep_PerChangeCol1 = Locator.lookup_multipleElements("incomeStatementRep_PercentChangeColumnList", null,
				null);

		ArrayList<String> isRep_PerChangeCol1List = new ArrayList<String>();

		for (WebElement data : isRep_PerChangeCol1) {
			String isValue = BaseUI.getTextFromField(data);
			isRep_PerChangeCol1List.add(isValue);
		}
			
		return isRep_PerChangeCol1List;
	}

    public static void verify_IncomeStatement_DollarValue(ArrayList<String> totalISAlertsList, 
	    	ArrayList<String> isAlert_dollarValue) {
	    ArrayList<String> isRep_Col1List = IncomeStatementReportPage.return_IncomeStatementRep_Column1List();
		BaseUI.verify_true_AndLog(isRep_Col1List.size() > 0, "IS compare Column1 list Displayed", 
				"IS compare Column1 list IS NOT Displayed");
			
		ArrayList<String> isRep_DollarChangeList = IncomeStatementReportPage.return_IncomeStatementRep_DollarChangeColList();
		BaseUI.verify_true_AndLog(isRep_DollarChangeList.size() > 0, 
				"IS compare $change column list Displayed "+isRep_DollarChangeList.size(), 
				"IS compare $change column list IS NOT Displayed "+isRep_DollarChangeList.size());
			
		for (int i=0; i<totalISAlertsList.size(); i++) {
			for (int j=0; j<isRep_Col1List.size(); j++) {
				if(isRep_Col1List.get(j).equals(totalISAlertsList.get(i))) {
					BaseUI.verify_true_AndLog((isRep_DollarChangeList.get(j).replace("-", "").replaceAll("\\(", "").replaceAll("\\)", "").trim())
							.contains((isAlert_dollarValue.get(i).replace("$", "").replace("-", "").trim())), 
							isRep_DollarChangeList.get(j) + " IS compare report $change value MATCH displayed on IS Alerts " + isAlert_dollarValue.get(i).replaceAll("$", ""), 
							isRep_DollarChangeList.get(j) + " IS compare report $change value DOES NOT MATCH displayed on IS Alerts " + isAlert_dollarValue.get(i).replaceAll("$", ""));
				} 
			}
		}
	}
	    
	    
	public static void verify_IncomeStatement_PercentValue(ArrayList<String> totalISAlertsList, 
			ArrayList<String> isAlert_PercentValue) {
		ArrayList<String> isRep_Col1List = IncomeStatementReportPage.return_IncomeStatementRep_Column1List();
		BaseUI.verify_true_AndLog(isRep_Col1List.size() > 0, "IS compare Column1 list Displayed", 
				"IS compare Column1 list IS NOT Displayed");
		
		ArrayList<String> isRep_PercentageChangeList = IncomeStatementReportPage.return_IncomeStatementRep_PercentChangeColList();
		BaseUI.verify_true_AndLog(isRep_PercentageChangeList.size() > 0, "IS compare %change column list Displayed", 
				"IS compare %change column list IS NOT Displayed");
		
		for (int i=0; i<totalISAlertsList.size(); i++) {
			for (int j=0; j<isRep_Col1List.size(); j++) {
				if(isRep_Col1List.get(j).equals(totalISAlertsList.get(i))) {
					double percentValue = Double.parseDouble(isAlert_PercentValue.get(i)
							.replaceAll("%", "").replaceAll("\\(", "").replaceAll("\\)", "").trim());
                    DecimalFormat value = new DecimalFormat("#.#");
					
					//double expected_PercentValue = 
					String isAlerts_PercentValue = value.format(percentValue);//String.valueOf(Math.round(Double.valueOf(percentValue)));
//					BigDecimal myBigDecimal = new BigDecimal(percentValue);		
//					String isAlerts_PercentValue = myBigDecimal.setScale(1, RoundingMode.HALF_UP).toString();
					
					BaseUI.verify_true_AndLog((isRep_PercentageChangeList.get(j).replace("-", "").trim()).contains(isAlerts_PercentValue), 
							isRep_PercentageChangeList.get(j) + " IS compare report %change value MATCH displayed on IS Alerts " + isAlerts_PercentValue, 
							isRep_PercentageChangeList.get(j) + " IS compare report %change value DOES NOT MATCH displayed on IS Alerts " + isAlerts_PercentValue);
				} else {
					BaseUI.verify_true_AndLog(!(isRep_Col1List.get(j).equals(totalISAlertsList.get(i))), 
							"Report column list DOES NOT MATCH Alert List", "Report column list MATCH Alert List");
				}
			}
		}
	}
	
	
	public static void verifyIncomeStatementAlert_AlertsNum_MatchesAlertsDisplayed() {
		String totalISAlerts = BaseUI.
				getTextFromField(Locator.lookupElement("alerts_IncomeStateTab_ISAlertsNum"));
		ArrayList<String> totalISAlertsList = StartPage_AlertsModal.return_IncomeStatementAlertsList();
		
		BaseUI.verify_true_AndLog(totalISAlerts.equals(String.valueOf(totalISAlertsList.size())), 
				"Income statement Alerts Number and displayed List Match "+ totalISAlertsList.size(), 
				"Income statement Alerts Number and displayed List DOES NOT Match "+ totalISAlertsList.size());
	}
	
	
	public static void clickToDrilldownReport(String elementToClick) throws Exception
	{
		BaseUI.click(Locator.lookupElement(elementToClick));
		Thread.sleep(2000);
	}
}
