package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class LoanPricing_RiskWeightingsModal {
		
	public static void click_AddNewRiskWeightingBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_RiskWeightings_AddNewRiskWeightingBtn"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("loanPricing_RiskWeightings_DescriptionTxtbx", null, null);
	}
	
	
	public static void switchToIframe() throws Exception
	{
		BaseUI.switch_ToIframe(Locator.lookupElement("loanPricing_RiskWeightings_FrameWindow"));
		Thread.sleep(500);
	}
	
	
	public static void addNewRiskWeighting() throws Exception {
		switchToIframe();
		click_AddNewRiskWeightingBtn();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_RiskWeightings_DescriptionTxtbx"), 
				"Risk Weighting 3");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_RiskWeightings_CapitalWeightingTxtbx"), 
				"200");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_RiskWeightings_AllowanceWeightingTxtbx"), 
				"100");
		click_UpdateBtn();
		switchToIframe();
		BaseUI.waitForElementToBeDisplayed("loanPricing_RiskWeightings_AddNewRiskWeightingBtn", null, null);
		click_UpdateBtn();
	}
	
	
	public static void newRiskWeighting() throws Exception {
		Navigation.navigateToLoanPricing_RiskWeightings();
		addNewRiskWeighting();
	}
	

	public static void deleteRiskWeighting() throws Exception {
		StartPage.clickStartPageTab();
		Navigation.navigateToLoanPricing_RiskWeightings();
		clickRiskWeightingDeleteIcon();
		click_UpdateBtn();
	}


	public static void clickRiskWeightingDeleteIcon() throws Exception {
		switchToIframe();
		BaseUI.click(Locator.lookupElement("loanPricing_RiskWeightings_RiskWeightings3DeleteIcon"));
		Thread.sleep(500);
		BaseUI.switchToAlert_Accept();
	}
	
	
	public static void click_UpdateBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("loanPricing_RiskWeightings_UpdateBtn"));
		Thread.sleep(500);
	}

}
