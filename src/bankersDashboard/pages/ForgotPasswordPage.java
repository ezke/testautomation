package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class ForgotPasswordPage {

	public static void enterForgotPasswordDetails(String username) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("forgotPwd_UsernameTextbox"), username);
	}
	
	
	public static void clickResetPasswordBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("forgotPwd_resetPassword_button"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("forgotPwd_emailSentMessage", null, null);
	}
	
	
	public static void clickForgotPasswordAndResetPassword(String username) throws Exception {
		enterForgotPasswordDetails(username);
		clickResetPasswordBtn();
	}
	
	
	public static void verifyResetPasswordModalAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("forgotPwd_ResetPwd_ResetPwdHeader"));
		BaseUI.verifyElementHasExpectedText("forgotPwd_ResetPwd_ResetPwdHeader", "Reset Password");
	}

	public static void enterResetPasswordDetails(String username, String password)
			throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("forgotPwd_ResetPwd_UsernameTxtbox"), username);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("forgotPwd_ResetPwd_PwdTxtbox"), password);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("forgotPwd_ResetPwd_ConfirmPwdTxtbox"),
				password);
		BaseUI.click(Locator.lookupElement("forgotPwd_ResetPwd_ChangePwdBtn"));
		Thread.sleep(4000);
		BaseUI.waitForElementToBeDisplayed("loginPage_Username_textbox", null, null);
	}
}
