package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class Al_HistoMonthlyLoansPage {
	
	public static void selectViewOption(String viewOption) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("historicalMonthlyLoans_Modal_ViewDropdown"), 
				viewOption);
		Thread.sleep(300);
	}
	
	
	public static void selectDateOption(String dateOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("historicalMonthlyLoans_Modal_DateDropdown"), 
				dateOption);
		Thread.sleep(300);
	}
	
	
	public static void selectProdTypeOption(String prodTypeOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("historicalMonthlyLoans_Modal_ViewDropdown"), 
				prodTypeOption);
		Thread.sleep(300);
	}
	
	
	public static void selectLoanToViewOption(String loansToViewOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("historicalMonthlyLoans_Modal_DateDropdown"), 
				loansToViewOption);
		Thread.sleep(300);
	}
	
	
	public static void verifyNonAccuralLoans_Checkbox_IsChecked() {
		BaseUI.verifyCheckboxStatus("historicalMonthlyLoans_Modal_NonAccrual", "checked");
	}
	
	
	public static void clickShowReportBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("historicalMonthlyLoans_Modal_ShowReportBtn"));
		Thread.sleep(2000);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, "60");
		BaseUI.wait_forPageToFinishLoading();
	}

}
