package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class Dashboard_QuarterlyDashboardModal {

	//private static String dataSourceOptionSelected;
	private static String expStartDate;

	public static void selectDate_DataSourceOption(String datasrc, int dateValue) throws Exception {
		Navigation.navigateToQuarterlyDashboard();
		BaseUI.selectValueFromDropDown(Locator.lookupElement("quarterlyDashboard_Datasource_dropdown"), datasrc);
		Thread.sleep(200);
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("quarterlyDashboard_StartDateOption"), dateValue);
		Thread.sleep(200);
	}
	
	
	public static void clickShowReportBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("quarterlyDashboard_ShowRepBtn"));
		Thread.sleep(3000);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null); 
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("quarterlyDashboard_ReportTitle", null, "180");
	}
	
	
	public static void verifyDataSourceOnReportPage(String dataSourceOptionSelected) {
		BaseUI.verifyElementAppears(Locator.lookupElement("quarterlyDashboard_dataSourceDisplayedOnReport"));
		BaseUI.verifyElementHasExpectedText("quarterlyDashboard_dataSourceDisplayedOnReport", dataSourceOptionSelected);
	}
	
    
    public static void verifyDateSelectedDisplayForFirstColumn(String startDateOptionSelected) throws Exception {
    	String datedisplayedOnReport = BaseUI.getTextFromField(Locator.lookupElement("quarterlyDashboard_datedisplayedOnReport"))
    			.trim();
    	if(startDateOptionSelected.contains("QTR 1")) {
    		expStartDate = "Mar"+startDateOptionSelected.substring(5);
    	} else if (startDateOptionSelected.contains("QTR 2")) {
    		expStartDate = "Jun"+startDateOptionSelected.substring(5);
    	} else if (startDateOptionSelected.contains("QTR 3")) {
    		expStartDate = "Sep"+startDateOptionSelected.substring(5);
    	} else {
    		expStartDate = "Dec"+startDateOptionSelected.substring(5);
    	}
		BaseUI.baseStringCompare("quarterlyDashboard_datedisplayedOnReport", 
				expStartDate, datedisplayedOnReport);
    }
    
    
    public static ArrayList<String> return_QuarterlyDashboardReport_DateValue() {
		ArrayList<WebElement> dateColumn = Locator.lookup_multipleElements("quarterlyDashboard_dateColumnList", null,
				null);

		ArrayList<String> dateColumnList = new ArrayList<String>();

		for (WebElement data : dateColumn) {
			String capReqValue = BaseUI.getTextFromField(data);
			dateColumnList.add(capReqValue);
		}
		
		return dateColumnList;
	} 
    
    
    public static void verifyDateColumnsDisplayed_MatchesFormat() {
    	ArrayList<String> dateList = Dashboard_QuarterlyDashboardModal.return_QuarterlyDashboardReport_DateValue();
		BaseUI.verify_true_AndLog(dateList.size() == 6, "Last 6 months date columns ARE DISPLAYED", 
				"Last 6 months date columns ARE NOT DISPLAYED");
//		BaseUI.verify_true_AndLog(!dateList.get(0).trim().contains(startDateOptionSelected), 
//				"First Column DATE VALUE DOES NOT DISPLAY QTR", 
//				"First Column DATE VALUE DOES DISPLAY QTR");
		for(String date : dateList) {
			BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(date, "MMM yyyy"), "Date Displayed MATCHES FORMAT", 
					"Date Displayed DOES NOT MATCH FORMAT");
		}
    }
}
