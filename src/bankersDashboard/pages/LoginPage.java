
package bankersDashboard.pages;

/**
 * @author Selenium
 *
 */

import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pageControls.EntitySelector;
//import bankersDashboard.data.ClientDataRetrieval;
//import bankersDashboard.tests.JavascriptExecutor;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class LoginPage {
	
	public static void login(String userName, String password) throws Exception {
		//if (ClientDataRetrieval.clientName.startsWith("Any Bank")) {
		//BaseUI.verifyElementAppears(Locator.isElementPresent(By.id("logo")));
		BaseUI.waitForElementToBeDisplayed("loginPage_LoginPage_text", null, null, 30);
		BaseUI.click(Locator.lookupElement("loginPage_Username_textbox"));
		BaseUI.enterText(Locator.lookupElement("loginPage_Username_textbox"), userName);
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("loginPage_Password_textbox"));
		BaseUI.enterText(Locator.lookupElement("loginPage_Password_textbox"), password);
		BaseUI.checkCheckbox(Locator.lookupElement("loginPage_Checkbox_rememberMyUsername"));
		Thread.sleep(500);
		BaseUI.click_js(Locator.lookupRequiredElement("loginPage_Login_button"));
		Thread.sleep(3000);
		BaseUI.wait_forElementToNotExist("loginPage_Login_button", null, null);
		if(Browser.driver.getWindowHandles().size() > 1) {
			//BaseUI.close_ExtraWindows("Banker's Dashboard from Deluxe");
			EntitySelector.selectWindow("Banker's Dashboard");
			EntitySelector.close_ExtraWindows("Banker's Dashboard");
			StartPage_User_PreferencesModal.userpreferences_OpenDashboard_NewWindow("No");
		}
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed_ButDontFail("startPage_ProfitCenterEntity_displayed", 
				null, null, 300);
	}
	
	
	public static void loginAsNewlyCreatedUserAndLogout(String newUserName) throws Exception {
		//Open the new browser session and login with newly created user
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(newUserName, GlobalVariables.PasswordNew);
				
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
				
		BaseUI.waitForElementToBeDisplayed("loginPage_BD_logo", null, null);
		BaseUI.waitForElementToBeDisplayed("startPage_Announcements_text", null, null);
		
		try {
			Navigation.navigate_to_signOut_WithoutAlert();
			EntitySelector.selectWindow("Banker's Dashboard from Deluxe - Management Software");
	        } finally {
	        	Browser.closeBrowser();
	        	}
	}
	
	
	public static void loginAsNewlyCreatedUser(String newUserName) throws Exception {
		//Open the new browser session and login with newly created user
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(newUserName, GlobalVariables.PasswordNew);
				
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
				
		BaseUI.waitForElementToBeDisplayed("loginPage_BD_logo", null, null);
		BaseUI.waitForElementToBeDisplayed("startPage_Announcements_text", null, null);
	}
	
	
	public static void loginAsClientUser() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		BaseUI.waitForElementToBeDisplayed("loginPage_BD_logo", null, null);		
	}
	
	
	public static void clickOnForgotPasswordLink() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("loginPage_forgotPassword_Linktext"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("forgotPwd_passwordRetrievalHeader", null, null);
		BaseUI.waitForElementToBeDisplayed("forgotPwd_TextOnPassowrdRetrievalPage", null, null);
	}
	
	
	public static void enterLoginCredentialsWithRememberCheckboxChecked(String userName, String password) throws Exception {
		BaseUI.enterText(Locator.lookupElement("loginPage_Username_textbox"), userName);
		BaseUI.enterText(Locator.lookupElement("loginPage_Password_textbox"), password);
		BaseUI.checkCheckbox(Locator.lookupElement("loginPage_Checkbox_rememberMyUsername"));
			
		clickLoginBtn();
	}
	
	
    public static void enterLoginCredentialsWithRememberCheckboxUnchecked(String userName, String password) throws Exception {
		BaseUI.enterText(Locator.lookupElement("loginPage_Username_textbox"), ClientDataRetrieval.userName);
		BaseUI.enterText(Locator.lookupElement("loginPage_Password_textbox"), ClientDataRetrieval.password);
		BaseUI.uncheckCheckbox(Locator.lookupElement("loginPage_Checkbox_rememberMyUsername"));
			
		clickLoginBtn();
	}
    
    
    public static void clickLoginBtn() throws Exception {
    	BaseUI.click(Locator.lookupElement("loginPage_Login_button"));
		Thread.sleep(3000);
    }
    
    
}

