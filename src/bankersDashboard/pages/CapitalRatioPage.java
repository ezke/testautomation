package bankersDashboard.pages;

import java.util.ArrayList;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;


public class CapitalRatioPage {
	
	public static void selectDefaultOptions_CapitalRatioReport(String dateOption) throws Exception {
		Navigation.navigateToGL_CapitalAndRegulatory_CapitalRatios();
		
		selectDateOption(dateOption);
		BaseUI.switch_ToDefaultContent();
		clickShowReportBtn();
		
		//Wait for report to load
		BaseUI.waitForElementToBeDisplayed_ButDontFail("capRatios_ReportTitle", 
				null, null, 1000);
		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_ReportTitle"));
	}
	
	
	public static void selectOption_CapitalRatioReport(String viewOption, String dateOption) throws Exception {
		Navigation.navigateToGL_CapitalAndRegulatory_CapitalRatios();
		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_ModalTitle"));
		
		//selectViewOption(viewOption);
		selectDateOption(dateOption);
		BaseUI.switch_ToDefaultContent();
		clickShowReportBtn();
		
		//Wait for report to load
		//BaseUI.wait_forElementToNotExist("capRatios_ReportLoadingText", null, null);
		BaseUI.waitForElementToBeDisplayed_ButDontFail("capRatios_ReportTitle", 
				null, null, 1000);
		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_ReportTitle"));
	}
	
	
	public static void selectViewOption(String viewOption) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("capRatios_ViewDropdown"), 
				viewOption);
		Thread.sleep(300);
	}
	
	
	public static void selectDateOption(String dateOption) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("capRatios_DateDropdown"), 
				dateOption);
		Thread.sleep(300);
	}
	
	
	public static void clickShowReportBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("capRatios_ShowReportBtn"));
		Thread.sleep(5000);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.wait_forElementToNotExist("capRatios_ReportLoadingText", null, null);
	}
	
	
	public static void openCapitalRatioReport(String viewOption, String dateOption) throws Exception {
		StartPage.clickCloseAllTabsButton();
		CapitalRatioPage.selectOption_CapitalRatioReport(viewOption, dateOption);
	}
	
	
	public static void verifyTotalRiskBasedRatioMatch(String viewOption, String dateOption, 
			String totalRiskBasedRatio) throws Exception {
		CapitalRatioPage.openCapitalRatioReport(viewOption, dateOption);
		String totalRiskBasedRatioAfter = BaseUI.
	    		getTextFromField(Locator.lookupElement("capRatios_Table_TotalRiskBasedRatioRaColumn"));
		
		BaseUI.scroll_to_element(Locator.lookupElement("capRatios_Table_TotalRiskBasedRatioRaColumn"));
		BaseUI.verify_true_AndLog((totalRiskBasedRatioAfter.equals(totalRiskBasedRatio) && (!totalRiskBasedRatioAfter.equals("N/A"))), 
				"Total Risk Based Ratio match "+ totalRiskBasedRatioAfter, 
				"Total Risk Based Ratio DOES NOT match "+ totalRiskBasedRatioAfter);
	}
	
	
	public static void verifyTotalRiskBasedRatioDifferent(String viewOption, String dateOption, 
			String totalRiskBasedRatio) throws Exception {
		UpdateMemoField.editUpdateMemoField_CapitalValues(dateOption);
		CapitalRatioPage.openCapitalRatioReport(viewOption, dateOption);
		String totalRiskBasedRatio_AfterMemoUpdate = BaseUI.
				getTextFromField(Locator.lookupElement("capRatios_Table_TotalRiskBasedRatioRaColumn"));
		
		BaseUI.scroll_to_element(Locator.lookupElement("capRatios_Table_TotalRiskBasedRatioRaColumn"));
		BaseUI.verify_true_AndLog(!(totalRiskBasedRatio_AfterMemoUpdate.equals(totalRiskBasedRatio) 
				&& totalRiskBasedRatio_AfterMemoUpdate.equals("N/A")), 
				"Total Risk Based Ratio DOES NOT match "+ totalRiskBasedRatio_AfterMemoUpdate, 
				"Total Risk Based Ratio match "+ totalRiskBasedRatio_AfterMemoUpdate);
	}
	
	
	public static ArrayList<String> return_CapitalRatioTable_LastRowData() {
		ArrayList<WebElement> capRatioData = Locator.lookup_multipleElements("capRatios_Table_LastRowList", null,
				null);

		ArrayList<String> dataList = new ArrayList<String>();

		for (WebElement data : capRatioData) {
			String capRatioValue = BaseUI.getTextFromField(data);
			dataList.add(capRatioValue);
		}
		
		return dataList;
	}

	
	public static void clickRepPropertiesBtn_SelectDiffDateOption(String dateOption) throws Exception {
		clickRepPropertiesBtn();
		
		//selectViewOption(viewOption);
		selectDateOption(dateOption);
		BaseUI.switch_ToDefaultContent();
		clickShowReportBtn();
				
		//Wait for report to load
		BaseUI.wait_forElementToNotExist("capRatios_ReportLoadingText", null, null);
//		BaseUI.waitForElementToBeDisplayed_ButDontFail("capRatios_ReportTitle", 
//				null, null, 3000);
		BaseUI.wait_forElementToNotExist("reportLoading_Phrase", null, null);
		BaseUI.waitForElementToBeDisplayed("capRatios_ReportTitle", null, null);
		BaseUI.wait_ForElement_ToContainText("capRatios_ReportTitle", 100, "Capital Ratios");
		BaseUI.wait_ForElement_ToContainText("capRatios_Table_PeriodColumnName", 50, "Period");	
	}
	
	
	public static void clickRepPropertiesBtn() throws Exception {
		BaseUI.click_js(Locator.lookupElement("startPage_Report_properties_Icon"));
		Thread.sleep(2000);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("capRatios_ModalTitle", null, null);
	}
		
	
}
