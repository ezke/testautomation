package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class StartPage_User_PreferencesModal {

	public static void userpreferences_OpenDashboard_NewWindow(String option) throws Exception {
		Navigation.navigate_to_Preferences();
		selectOpenDashboardNewWindowOption(option);
		clickFinishedBtn();
	}
	
	public static void selectOpenDashboardNewWindowOption(String option) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("userPreferences_OpenDashboardNewWindow_dropdown"), 
				option);
		Thread.sleep(200);
	}
	
	public static void clickFinishedBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click_js(Locator.lookupElement("userPreferences_FinishedBtn"));
		Thread.sleep(2000);
		BaseUI.waitForElementToNOTBeDisplayed("userPreference_ModalTitle", null, null);
	}
}
