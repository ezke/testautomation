package bankersDashboard.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class MyDashboardIndicator {
	
	public static void addCustomCalcToMyDashbordWidget(String customCalcName) throws Exception {
		Navigation.navigateToSettings_MyDashboard();
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_ModalTitle", null, null);
		//BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		click_AddIndicatorButton();
		clickRadioButton("Custom Calc");
		selectCustomCalc(customCalcName);
//		String customCalcIndicatorName = BaseUI.
//				getTextFromInputBox(Locator.lookupElement("myDashboardIndicator_customCalcIndicatorName_txtBox"));
		
		click_NextButton();
		BaseUI.switch_ToDefaultContent();
		click_FinishedButton();
		click_FinishedButton();
		StartPage.clickStartPageTab();
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_CustomCalcIndicatorOnMyDashboard",
				customCalcName, null);
	}
	
	
	public static void click_NextButton() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("myDashboardIndicator_NextBtn"));
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_GreenComparisonText", null, null);
	}
	
	
	public static void click_FinishedButton() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click_js(Locator.lookupElement("myDashboardIndicator_FinishedBtn"));
		Thread.sleep(2000);
		BaseUI.wait_forElementToNotExist("myDashboardIndicator_ModalTitle", null, null);
	}
	
	
	public static void click_AddIndicatorButton() throws InterruptedException {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("myDashboardIndicator_AddIndicatorBtn"));
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_Balance_RadioBtn", null, null);
	}
	
	
	public static void clickRadioButton(String type_radio) throws InterruptedException {
		
		if(type_radio.equals("Balance")) {
			BaseUI.click(Locator.lookupElement("myDashboardIndicator_Balance_RadioBtn"));
			Thread.sleep(100);
		} else if (type_radio.equals("Special Ratios")){
			BaseUI.click(Locator.lookupElement("myDashboardIndicator_SpecialRatios_RadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("myDashboardIndicator_CustomCalc_RadioBtn"));
			Thread.sleep(100);
		}	
	}
	
	
	public static void selectCustomCalc(String customCalcName) throws Exception {
		List<WebElement> allCustomCalcs = Locator.lookup_multipleElements("myDashboardIndicator_SelectCustomCalcFromList", null, null);		
		
		for (WebElement customCalc : allCustomCalcs ) {
			if(customCalc.getText().equals(customCalcName)) {
				customCalc.click();
				break;
			}
		}
	}
	
	
	public static String addBalanceIndicator_SummaryWidget() throws Exception {
		click_AddIndicatorButton();
		clickRadioButton("Balance");
		BaseUI.click(Locator.lookupElement("myDashboardIndicator_BalanceOption_AssetsIndicatorValue"));
		Thread.sleep(200);
		BaseUI.click(Locator.lookupElement("myDashboardIndicator_BalanceOption_Assets_CashAndDuesIndicatorValue"));
		String balanceIndicator = BaseUI.getTextFromInputBox(Locator.lookupElement("myDashboardIndicator_BalanceOption_IndicatorName_Textbox"));
		click_NextButton();
		click_FinishedButton();
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_IndicatorNameDisplays", null, null);
		click_FinishedButton();
		return balanceIndicator;		
	}
	
	
	public static void clickCustomIndicator_DeleteIcon() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("myDashboardIndicator_DeleteCustomIndicator"));
		Thread.sleep(200);
		BaseUI.switchToAlert_Accept();
		BaseUI.wait_forElementToNotExist("myDashboardIndicator_IndicatorNameDisplays", null, null);
	}
	
	
	public static void deleteCustomIndicator_AddedToSummaryWidget() throws Exception {
		StartPage.clickCloseAllTabsButton();
    	StartPage.clickStartPageTab();
		Navigation.navigateToSettings_MyDashboard();
		clickCustomIndicator_DeleteIcon();
		click_FinishedButton();		
		StartPage.clickStartPageTab();
	}
}
