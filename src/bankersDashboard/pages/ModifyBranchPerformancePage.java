package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class ModifyBranchPerformancePage {

	
	public static void clickDeleteViewBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("branchPerf_ModifyView_DeleteView"));
		Thread.sleep(1000);
		BaseUI.switchToAlert_Accept();
	}
}
