package bankersDashboard.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import bankersDashboard.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class StartPage {
	
	public static void clickAddWidgetIcon() throws Exception {
		BaseUI.click_js(Locator.lookupElement("startPage_AddWidget_Icon"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("addWidgetModalTitle", null, null);
//		String expectedValue = "My Dashboard Modules";
//		BaseUI.verifyElementHasExpectedText("addWidgetModalTitle", expectedValue );
//		Reporter.log("My Dashboard Modules window is displayed");
	}
	
	
	public static void deleteWidgetFromStartPage(String widgetToBeDelete, String widgetName, String option) throws Exception {
		//BaseUI.click(Locator.lookupElement("startPageTab"));
		StartPage.clickDeleteWidgetIcon(option);
		//BaseUI.click(Locator.lookupElement(widgetToBeDelete));//customCalcWidgetDeleteIcon
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
		BaseUI.click(Locator.lookupElement("clickOkButton"));
		Thread.sleep(1000);
		BaseUI.wait_forElementToNotExist("verifyCustomCalcIndicatorAdded_startPage", widgetName, null);//storeCustomCalcWidgetName
	}
	
	
	public static void clickDeleteWidgetIcon(String option) throws Exception {
		clickStartPageTab();
		BaseUI.click(Locator.lookupElement("editAnyCustomWidgets"));
		Thread.sleep(200);
		switch (option) {
		case "deleteAnyCustomWidgets" :
			BaseUI.click(Locator.lookupElement("deleteAnyCustomWidgets"));
			Thread.sleep(500);
			break;
			
		case "deleteCustomCalculationWidget" :
			BaseUI.click(Locator.lookupElement("deleteCustomCalculationWidget"));
			Thread.sleep(500);
			break;
			
		case "editWidgets_Settings" :
			BaseUI.click(Locator.lookupElement("editWidgets_Settings"));
			Thread.sleep(500);
			break;
			
		default:
			break;
			
		}
	}
	
		
	public static void clickReportPropertiesIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_Report_properties_Icon"));
		Thread.sleep(2000);
	}
	
		
	public static void clickExpotToExcelIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_exportToExcel_Icon"));
		Thread.sleep(2000);
	}
	
		
	public static void clickExpotToPdfIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_exportToPDF_Icon"));
//		BaseUI.wait_forElementToNotExist("reportLoadingText", "3000", null);
//		BaseUI.wait_forElementToNotExist("reportLoadingText", "3000", null);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(5000);
	}
	
	
	public static void clickAnnouncementsHeader() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_Announcements_text"));
		Thread.sleep(2000);
	}
		
	
	public static String getDisplayedUsername() throws Exception {
		String userName = BaseUI.getTextFromField(Locator.lookupElement("startPage_Username_dropdown"));
		//Thread.sleep(2000);
		return userName;
	}
	
		
	public static String getDisplayedBankEntity() throws Exception {
		String bankEntity = BaseUI.getTextFromField(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		//Thread.sleep(2000);
		return bankEntity;	
	}
	
		
	public static void clickMyAlertsBellIcon() throws Exception {
		StartPage.clickStartPageTab();
		BaseUI.click(Locator.lookupElement("startPage_MyAlerts_dropdown"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("alerts_TodaysAlertsTitle", null, null, 500);
		BaseUI.waitForElementToBeDisplayed("alerts_RefreshIcon", null, null, 1500);
	}
	
	
	public static void clickMyFavoriteReports() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_MyFavReports_dropdown"));
		Thread.sleep(2000);
	}
	
	
	public static void clickBackButtonIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_Back_button_Icon"));
		Thread.sleep(2000);
		BaseUI.wait_forPageToFinishLoading();
	}
	
	
	
	public static void clickLeftMenuCollapseButton() throws Exception
	{
		BaseUI.click(Locator.lookupElement("startPage_LeftHandMenuToggle"));
		Thread.sleep(2000);
	}
	

	
	public static void clickCloseAllTabsButton() throws Exception
	{
		StartPage.clickStartPageTab();
		BaseUI.click_js(Locator.lookupElement("startPage_CloseAllTabs_btn"));
		Thread.sleep(2000);
	}
	
	
	public static void clickStartPageTab() throws Exception
	{
		BaseUI.switch_ToDefaultContent();
		BaseUI.click_js(Locator.lookupElement("startPage_startPageTab"));
		Thread.sleep(2000);
	}
	
	
	public static void switchToIframe() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		Thread.sleep(500);
	}
	
	
	public static void help_dropDown(String helpOptionToBeSelected) throws Exception
	{
		BaseUI.click(Locator.lookupElement("startPage_Help_txt"));
		Thread.sleep(500);
		List<WebElement> all = Locator.lookup_multipleElements("startPage_Help_dropdown", null, null);
		Thread.sleep(5000);
		for (WebElement helpOption : all )
		{
			if(helpOption.getText().equals(helpOptionToBeSelected))
			{
				helpOption.click();
				Thread.sleep(1000);
				System.out.println("Option is selected " + helpOptionToBeSelected );
				break;
			}
		}
	}
	
	
	public static void clickAddWidgetIcon_CustomDashboardIndicatorOption() throws Exception {
		StartPage.clickStartPageTab();
		BaseUI.click_js(Locator.lookupElement("startPage_AddWidget_Icon"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("addWidgetModalTitle", null, null);
		StartPage.clickCustomIndicatorRadioButton();
	} 
	
	
	public static void customIndicatorModule_clickNextButton() throws Exception {
		BaseUI.click_js(Locator.lookupElement("nextButton"));
		Thread.sleep(500);
	}
	
	
	public static void customIndicatorModule_clickSaveButton() throws Exception
	{
	    BaseUI.click_js(Locator.lookupElement("saveButton"));
	    Thread.sleep(1000);
	}
	
	
	public static void verifyAddWidgetModalTitle()
	{
		BaseUI.verifyElementAppears(Locator.lookupElement("addWidgetModalTitle"));
		String expectedValue = "My Dashboard Modules";
		BaseUI.verifyElementHasExpectedText("addWidgetModalTitle", expectedValue );
		Reporter.log("My Dashboard Modules window is displayed");
	}
	
	
	public static void clickCustomIndicatorRadioButton() throws Exception{
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("customDashboardIndicator_radioBtn"));
		Thread.sleep(200);
	}
	
	
	public static void verifyCustomCalculation_DeletedFromSummaryReport(String customCalcName) throws Exception {
		 Browser.driver.navigate().refresh();
		 // Thread.sleep(10000);
		 BaseUI.wait_forPageToFinishLoading();
		 BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("myDashboardIndicator_CustomCalcIndicatorOnMyDashboard", 
				 customCalcName, null));
	}
	
	
	public static ArrayList<String> return_SummaryWidget_Data() {
		ArrayList<WebElement> summaryWidgetData = Locator.lookup_multipleElements("summaryWidgetData_WithoutDailyData", null,
				null);

		ArrayList<String> dataList = new ArrayList<String>();

		for (WebElement data : summaryWidgetData) {
			String summaryDataValue = BaseUI.getTextFromField(data);
			dataList.add(summaryDataValue);
		}
		
		return dataList;
	}
	

	public static void clickMyFavoriteReportsPackages(String reportPackage) throws Exception {
		clickMyFavoriteReports();
		List<WebElement> myFavWidget_RepPackages = Locator.lookup_multipleElements("startPage_MyFavReports_dropdownList", null,
				null);
		
		for (WebElement repPackage : myFavWidget_RepPackages)
		{
//			String reportPackageDisplayed = BaseUI.getTextFromField(repPackage);
            if((repPackage.getText().replaceAll("...", "").trim()).equals(reportPackage.trim())) {
            	BaseUI.click(repPackage);
            	Thread.sleep(3000);
            	BaseUI.wait_forPageToFinishLoading();
            	BaseUI.waitForElementToBeDisplayed("startPage_startPageTab2", null, null);
            	//clickCloseAllTabsButton();
            }			
		}
		
//		ArrayList<WebElement> myFavReports = Locator.lookup_multipleElements("myFavoriteWidget_AllReportPackages", null,
//				null);
//
//		ArrayList<String> myFavReportsList = new ArrayList<String>();
//
//		for (WebElement data : myFavReports) {
//			String reports = BaseUI.getTextFromField(data);
//			myFavReportsList.add(reports);
//		}
//		
//		return myFavReportsList;
	}
	
	
	public static ArrayList<String> return_MyFavReportsList() throws Exception{
		clickMyFavoriteReports();
		ArrayList<WebElement> myFavReports = Locator.lookup_multipleElements("startPage_MyFavReports_dropdownList", null,
				null);

		ArrayList<String> myFavReportsList = new ArrayList<String>();

		for (WebElement data : myFavReports) {
			String reports = BaseUI.getTextFromField(data);
			myFavReportsList.add(reports);
		}
		
		return myFavReportsList;
	}
	
	
	public static void verifyBankEntityRemainsSame_AfterLoggingBackIn(String bankEntity) throws Exception {
		Navigation.navigate_to_signOut();
		System.out.println("Successfully logged out");
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		bankEntity = BaseUI.getTextFromField(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		BaseUI.verify_true_AndLog(bankEntity.equals(ClientDataRetrieval.profitCenter), 
				"Bank Entity displayed matches the bank entity displayed before logging out " + bankEntity, 
				"Bank Entity displayed DOES NOT match the bank entity displayed before logging out " + bankEntity);
	}
	
	
	public static void clickStartPage_Tab2() throws Exception {
		BaseUI.click_js(Locator.lookupElement("startPage_startPageTab2"));
		Thread.sleep(500);
	}
	
	
	public static void clickStartPage_Tab3() throws Exception {
		BaseUI.click_js(Locator.lookupElement("startPage_startPageTab3"));
		Thread.sleep(500);
	}
	
	
	public static void clickRulerMeterStye() throws Exception {
		BaseUI.click(Locator.lookupElement("rulerMeterStyle"));
		Thread.sleep(300);
	}
	
	
	public static void clickSummaryWidgetChevronIcon() throws Exception {
		StartPage.clickStartPageTab();
		BaseUI.click(Locator.lookupElement("summaryWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("summaryWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickSummaryWidgetChevronIcon_Delete() throws Exception {
		clickSummaryWidgetChevronIcon();
		BaseUI.click(Locator.lookupElement("summaryWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void clickRemoveWidget_OkBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("clickOkButton"));
		Thread.sleep(500);
		//BaseUI.wait_forElementToNotExist("removeModuleTitle", null, null);
	}
	
	
	public static void clickRemoveWidget_CancelBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("clickCancelButton"));
		Thread.sleep(500);
		BaseUI.waitForElementToNOTBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteSummaryWidget() throws Exception {
		clickSummaryWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void deleteMyFavReportsWidget() throws Exception {
		clickMyFavReportsWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickMyFavReportsWidgetChevronIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("myFavReportsWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("myFavReportsWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickMyFavReportsWidgetChevronIcon_Delete() throws Exception {
		clickMyFavReportsWidgetChevronIcon();
		BaseUI.click(Locator.lookupElement("myFavReportsWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteFedFundCurveWidget() throws Exception {
		clickFedFundCurveWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickFedFundCurveChevronIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("FedFundCurveWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("FedFundCurveWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickFedFundCurveWidgetChevronIcon_Delete() throws Exception {
		clickFedFundCurveChevronIcon();
		BaseUI.click(Locator.lookupElement("FedFundCurveWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteMyMarginWidget() throws Exception {
		clickMyMarginWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickMyMarginChevronIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("myMarginWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("myMarginWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickMyMarginWidgetChevronIcon_Delete() throws Exception {
		clickMyMarginChevronIcon();
		BaseUI.click(Locator.lookupElement("myMarginWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteNew_RenewedLoans_CdsWidget() throws Exception {
		clickNew_RenewedLoans_CdsWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickNew_RenewedLoans_CdsChevronIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("new_RenewedLoans_CdsWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("new_RenewedLoans_CdsWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickNew_RenewedLoans_CdsWidgetChevronIcon_Delete() throws Exception {
		clickNew_RenewedLoans_CdsChevronIcon();
		BaseUI.click(Locator.lookupElement("new_RenewedLoans_CdsWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteExpectedLoanClosingsWidget() throws Exception {
		clickExpectedLoanClosingsWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickExpectedLoanClosingsChevronIcon() throws Exception {
		if(BaseUI.elementAppears(Locator.lookupElement("expectedLoanClosingsWidget_ChevronIcon"))) {
			BaseUI.click(Locator.lookupElement("expectedLoanClosingsWidget_ChevronIcon"));
			Thread.sleep(200);
			BaseUI.waitForElementToBeDisplayed("expectedLoanClosingsWidget_ChevronIcon_Delete", null, null);
		}
	}
	
	
	public static void clickExpectedLoanClosingsWidgetChevronIcon_Delete() throws Exception {
		clickExpectedLoanClosingsChevronIcon();
		BaseUI.click(Locator.lookupElement("expectedLoanClosingsWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteRecentLoanLossesWidget() throws Exception {
		clickRecentLoanLossesWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickRecentLoanLossesChevronIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("recentLoanLossesWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("recentLoanLossesWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickRecentLoanLossesWidgetChevronIcon_Delete() throws Exception {
		clickRecentLoanLossesChevronIcon();
		BaseUI.click(Locator.lookupElement("recentLoanLossesWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	
	public static void deleteRecentLoanWinsWidget() throws Exception {
		clickRecentLoanWinsWidgetChevronIcon_Delete();
		clickRemoveWidget_OkBtn();
	}
	
	
	public static void clickRecentLoanWinsChevronIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("recentLoanWinsWidget_ChevronIcon"));
		Thread.sleep(200);
		BaseUI.waitForElementToBeDisplayed("recentLoanWinsWidget_ChevronIcon_Delete", null, null);
	}
	
	
	public static void clickRecentLoanWinsWidgetChevronIcon_Delete() throws Exception {
		clickRecentLoanWinsChevronIcon();
		BaseUI.click(Locator.lookupElement("recentLoanWinsWidget_ChevronIcon_Delete"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("removeModuleTitle", null, null);
	}
	
	public static void clickSummaryWidgetChevronIcon_Settings() throws Exception {
		clickSummaryWidgetChevronIcon();
		BaseUI.click(Locator.lookupElement("summaryWidget_ChevronIcon_Settings"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_ModalTitle", null, null);
	}
	
	
	public static void clickRepPackageOnMyFavReportWidget(String repPackageName) throws Exception {
        List<WebElement> myFavRepPackages = Locator.lookup_multipleElements("startPage_MyFavoriteWidget_AllReportPackages", null, null);		
        
		for (WebElement repPackage : myFavRepPackages ) {
			if(BaseUI.getTextFromField(repPackage).contains(repPackageName)) {
				BaseUI.click_js(repPackage);
				//Report packages have multiple reports and takes longer to load
				//So added more wait time here
				//Thread.sleep(30000);
				BaseUI.wait_forElementToNotExist("reportLoadingText", "3000", null);
				BaseUI.wait_forPageToFinishLoading();
				BaseUI.waitForElementToBeDisplayed("startPage_startPageTab2", null, null);
				BaseUI.waitForElementToBeDisplayed("startPage_startPageTab3", null, null);
				BaseUI.wait_ForElement_ToContainText("startPage_startPageTab2", 1000, "Monthly Dashboard");
				break;
			}
		}
	}
	
	
	public static String addBalanceWidgetToStartPage() throws Exception {
		clickAddWidgetIcon_CustomDashboardIndicatorOption();
		customIndicatorModule_clickNextButton();
		BaseUI.waitForElementToBeDisplayed("balancesOption_label", null, null);
		AddWidgets_StartPage.clickBalanceRadioBtn();
		customIndicatorModule_clickNextButton();
		BaseUI.waitForElementToBeDisplayed("balancesIndicatorName_textbox", null, null);
		String storeBalancesWidgetName = AddWidgets_StartPage.addBalanceIndicatorDetails();
		AddWidgets_StartPage.clickBetaMeterStyleRadioBtn();
		customIndicatorModule_clickNextButton();
		BaseUI.waitForElementToBeDisplayed("Verify_customCalcIndicatorPreviewText", null, null);
		customIndicatorModule_clickSaveButton();
		
		return storeBalancesWidgetName;
	}
}
