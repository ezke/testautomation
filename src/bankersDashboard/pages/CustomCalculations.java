package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

/**
 * @author Nisha 01/26/2017
 * 
 * This page has all the page objects of Custom Calculations
 * page
 *
 */
public class CustomCalculations {
	
	
	private static String verifyHeaderText = "Custom Calculations";
	private static String verifyHeaderText1 = "Use this screen to define and manage custom calculations.";

	private static String valueToSelect = "Number (#)";
	private static String customCalcName = "TestCC"+BaseUI.random_NumberAsString(1, 10000);
	//private static String invalidCalcText = "Invalid Calculation!";
	//private static String validCalcText = "Valid Calculation!";
	private static String storeCustomCalc;//, storeCustomCalcWidgetName;
	
	
	public static void navigateToAddCustomCalcPage() throws Exception {
		Navigation.navigateToTools_CustomCalculations();
		
		//Commenting these lines because some of the tests failing after this point in jenkins
		CustomCalculations.searchCustomCalc("TestCC");
		
		if(!(BaseUI.elementAppears(Locator.lookupElement("customCalc_NoMatchingRecordText")))) {
			CustomCalculations.deleteCustomCalc();
		}	
		BaseUI.waitForElementToBeDisplayed("customCalc_AddCalculation_button", null, null, 30);
		StartPage.clickCloseAllTabsButton();
		Navigation.navigateToTools_CustomCalculations();
		CustomCalculations.clickOnAddCalcButton();
	}
	
	
	public static void deleteCustomCalculation(String customCalc) throws Exception
	{
		Navigation.navigateToTools_CustomCalculations();
		/*if(Locator.lookupElement("customCal_InactiveTab").isDisplayed())
		{
			BaseUI.click(Locator.lookupElement("customCalc_closeInactive_CustomCalTab"));
			Thread.sleep(200);
		}
		else
		{
			System.out.println("Inactive custom calc tab is not displayed");
		}*/
		CustomCalculations.searchCustomCalc(customCalc);
		CustomCalculations.deleteCustomCalc();
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
	}

	public static void switchToCustomCalcFrame() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("Custom_Calc_frame"));
		Thread.sleep(300);
		//BaseUI.waitForElementToBeDisplayed("customCalc_pageHeader", null, null);
	}
	
	public static void verifyCustomCalcPageHeader() throws Exception {
		BaseUI.verifyElementHasExpectedPartialText("customCalc_pageHeader", verifyHeaderText);
		BaseUI.verifyElementHasExpectedText("customCalc_TextUnderCustomCalcHeader", verifyHeaderText1);
	}
	
	public static void clickOnAddCalcButton() throws Exception
	{
		BaseUI.click_js(Locator.lookupElement("customCalc_AddCalculation_button"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("CustomCalc_Save_Button", null, null, 30);
		Thread.sleep(2000);
	}
	
	
	public static void checkSaveButton_IsDisabled() throws Exception {
		BaseUI.verifyElementDisabled(Locator.lookupElement("CustomCalc_Save_Button"));
	}
	
	
	public static void checkSaveButton_IsEnabled() throws Exception {
		BaseUI.verifyElementEnabled(Locator.lookupElement("CustomCalc_Save_Button"));
	}
	
	
	public static void selectValueForFormat(String valueToSelect) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("customCalc_Format_dropdownField"), valueToSelect);
		Thread.sleep(2000);
	}
	
	
	public static void selectValueForAccessLevel(String accessLevel) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("customCalc_AccessLevelDropdown"), accessLevel);
		Thread.sleep(2000);
	}
	
	
	
	public static void enterCustomCalculationName() throws Exception {
		//final String customCalcName = "TestCC123";
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName);
	}
	
	
	public static void clickToExpandGroups(String element) throws Exception {
		BaseUI.click(Locator.lookupElement(element));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
		
	public static void searchCustomCalc(String ccName) throws Exception {
		BaseUI.enterText((Locator.lookupElement("customCalc_Search_Textbox")), ccName);
		Thread.sleep(2000);
	}
	
	
	public static void deleteCustomCalc() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_Added"));
		Thread.sleep(200);
		BaseUI.click_js(Locator.lookupElement("customCalc_Delete"));
		Thread.sleep(3000);
		//BaseUI.switch_ToDefaultContent();
		BaseUI.switchToAlert_Accept();
		BaseUI.switch_ToIframe(Locator.lookupElement("Custom_Calc_frame"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("customCalcSearchPage_CustomCalcNoEntriesMsg", null, null);
	}
	
	
	public static void searchCustomCalcResultDisplayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_OnlyOneCustomCalcSearchResultDisplay"));
		String expectedValue = "Showing 1 to 1 of 1 entries";
		BaseUI.verifyElementHasExpectedPartialText("customCalc_OnlyOneCustomCalcSearchResultDisplay", expectedValue);
	}
	
	
	public static void searchCustomCalcResultNotDisplayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_OnlyOneCustomCalcSearchResultDisplay"));
		String expectedValue = "Showing 0 to 0 of 0 entries";
		BaseUI.verifyElementHasExpectedPartialText("customCalc_OnlyOneCustomCalcSearchResultDisplay", expectedValue );
		BaseUI.log_Status("Custom Calculations are tied to the UserId and the entity, so Custom Calculations defined by the user will only display in the entity with which it was created.");
	}
	
	
	public static String addNewCustomCalculation() throws Exception {
		CustomCalculations.navigateToAddCustomCalcPage();
		BaseUI.waitForElementToBeDisplayed("CustomCalc_Save_Button", null, null);
		CustomCalculations.checkSaveButton_IsDisabled();
		BaseUI.waitForElementToBeDisplayed("customCalc_InvalidCalculation_text", null, null);
		BaseUI.waitForElementToBeDisplayed("customCalc_Format_dropdownField", null, null);
		CustomCalculations.selectValueForFormat(valueToSelect);
		//BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName);
		Thread.sleep(500);
		String storeCustomCalc = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		BaseUI.waitForElementToBeDisplayed("customCalc_AssetsGroupExpand", null, null);
		CustomCalculations.clickToExpandGroups("customCalc_AssetsGroupExpand");
		
		//Click on GL Accounts groups to expand
		BaseUI.waitForElementToBeDisplayed("customCalc_CashAndDuesGroup", null, null);
		clickOnCashAndDuesGroup();
		clickOnCashGroupExpand();
		
		//Drag and drop the 1st item under expanded group
		BaseUI.dragAndDrop("customCalc_GLAccountList_1", "customCalc_DroppableSpace");
		CustomCalculations.checkSaveButton_IsEnabled();
		Thread.sleep(100);
		
		//Click plus icon to add more items to calculation
		clickPlusIcon();
		
		//Drag and drop the next item under expanded group
		BaseUI.dragAndDrop("customCalc_GLAccountList_2", "customCalc_DroppableSpace");
		CustomCalculations.checkSaveButton_IsEnabled();
		
		//Click plus icon to add more items to calculation
		clickPlusIcon();
		BaseUI.dragAndDrop("customCalc_GLAccountList_3", "customCalc_DroppableSpace");
		CustomCalculations.checkSaveButton_IsEnabled();
		BaseUI.waitForElementToBeDisplayed("customCalc_ValidCalculation_text", null, null);
		//BaseUI.verifyElementHasExpectedText(Locator.lookupElement("customCalc_ValidCalculation_text"), validCalcText);
		
		//Click Save button
		clickOnSaveButton();
		
		//Wait for Search box to appear.
		BaseUI.waitForElementToBeDisplayed("customCalc_Search_Textbox", null, null);
		CustomCalculations.searchCustomCalc(storeCustomCalc);
		CustomCalculations.searchCustomCalcResultDisplayed();
		
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
		//Thread.sleep(1000);
		return storeCustomCalc;
	}
	
	
	public static void clickPlusIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_PlusIcon"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(500);
	}
	
	
	public static void clickOnSaveButton() throws Exception {
	    BaseUI.click_js(Locator.lookupElement("CustomCalc_Save_Button"));
	    BaseUI.wait_forPageToFinishLoading();
	    Thread.sleep(2000);
//	    if (DropDowns.isAlertPresent()) {
//			BaseUI.switchToAlert_Accept();
//			BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName+BaseUI.random_NumberAsString(1, 4000) );
//			Thread.sleep(500);
//			storeCustomCalc = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
//			clickOnSaveButton();
//		}
	}
	
	
	public static void clickOnCashAndDuesGroup() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_CashAndDuesGroup"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}
	
	
	public static void clickOnCashGroupExpand() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_CashGroupExpand"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(500);
	}
	
	public static void deleteCustomcalcBeforeLogout(String customCalcName) throws Exception {
		Navigation.navigateToTools_CustomCalculations();
		//CustomCalculations.switchToCustomCalcFrame();
		BaseUI.waitForElementToBeDisplayed("customCalc_Search_Textbox", null, null);
		CustomCalculations.searchCustomCalc(customCalcName);
		//CustomCalculations.deleteCustomCalc();
		//Thread.sleep(1000);
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
	}
	
	
	public static void loginAsClientUser_DeleteCustomCalc_NewlyCreatedUser() throws Exception {
	    LoginPage.loginAsClientUser();
		
		CustomCalculations.deleteCustomcalcBeforeLogout(customCalcName);
		AddNewUserPage.click_VerifyNewUser_Deleted();
		StartPage.clickCloseAllTabsButton();
	    Navigation.navigate_to_signOut();
	}
	
	
	public static String addNewCustomCalculation(String valueToSelect, String accessLevel) throws Exception {
		CustomCalculations.navigateToAddCustomCalcPage();
		CustomCalculations.checkSaveButton_IsDisabled();
		CustomCalculations.selectValueForFormat(valueToSelect);
		//BaseUI.verifyElementAppears(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		enterCustomCalculationName();
		String storeCustomCalc = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		
		selectValueForAccessLevel(accessLevel);
		selectValueForFormat(valueToSelect);
		CustomCalculations.clickToExpandGroups("customCalc_AssetsGroupExpand");
		
		//Click on GL Accounts groups to expand
		BaseUI.waitForElementToBeDisplayed("customCalc_CashAndDuesGroup", null, null);
		clickOnCashAndDuesGroup();
		clickOnCashGroupExpand();
		
		//Drag and drop the 1st item under expanded group
		BaseUI.dragAndDrop("customCalc_GLAccountList_1", "customCalc_DroppableSpace");
		Thread.sleep(500);
		CustomCalculations.checkSaveButton_IsEnabled();
		
		
		//Click plus icon to add more items to calculation
		clickPlusIcon();
		
		//Drag and drop the next item under expanded group
		BaseUI.dragAndDrop("customCalc_GLAccountList_2", "customCalc_DroppableSpace");
		Thread.sleep(500);
		CustomCalculations.checkSaveButton_IsEnabled();
		
		//Click plus icon to add more items to calculation
		clickPlusIcon();
		BaseUI.dragAndDrop("customCalc_GLAccountList_3", "customCalc_DroppableSpace");
		CustomCalculations.checkSaveButton_IsEnabled();
		BaseUI.waitForElementToBeDisplayed("customCalc_ValidCalculation_text", null, null);
		//BaseUI.verifyElementHasExpectedText(Locator.lookupElement("customCalc_ValidCalculation_text"), validCalcText);
		
		//Click Save button
		clickOnSaveButton();
//		if (DropDowns.isAlertPresent()) {
//		    BaseUI.switchToAlert_Accept();
//			BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName);
//			Thread.sleep(500);
//			storeCustomCalc = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
//			clickOnSaveButton();
//		} 
		
		//Wait for Search box to appear.
		BaseUI.waitForElementToBeDisplayed("customCalc_Search_Textbox", null, null);
		CustomCalculations.searchCustomCalc(storeCustomCalc);
		CustomCalculations.searchCustomCalcResultDisplayed();
		
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
		
		return storeCustomCalc;
	}
	
	
	public static void clickOnAllAccessButton() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_All_button"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}
	
	
	public static void clickOnPrivateAccessButton() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_Private_button"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}
	
	
	public static void clickOnPublicAccessButton() throws Exception {
		BaseUI.click(Locator.lookupElement("customCalc_Public_button"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}
	
	
	public static void verifyCustomCalc_Displays_AtAccessLevel(String customCalcName) throws Exception {
		CustomCalculations.searchCustomCalc(customCalcName);
		if(BaseUI.elementAppears(Locator.lookupElement("customCalcSearchPage_CustomCalcName"))) {
			String customCalcDisplayed = BaseUI.getTextFromField(Locator.lookupElement("customCalcSearchPage_CustomCalcName"));
			BaseUI.baseStringCompare("customCalcSearchPage_CustomCalcName", customCalcName, customCalcDisplayed);
		}
		//BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("customCalcSearchPage_CustomCalcName"));
	}
	
	
	public static String addCustomCalcWithGLAccount() throws Exception {
		navigateToAddCustomCalcPage();
		BaseUI.verifyElementAppears(Locator.lookupElement("CustomCalc_Save_Button"));
		checkSaveButton_IsDisabled();
		BaseUI.waitForElementToBeDisplayed("customCalc_InvalidCalculation_text", null, null);
		BaseUI.waitForElementToBeDisplayed("customCalc_Format_dropdownField", null, null);
		selectValueForFormat(valueToSelect);
		BaseUI.enterText(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"), customCalcName);//+CCName
		Thread.sleep(500);
		storeCustomCalc = BaseUI.getTextFromInputBox(Locator.lookupElement("customCalc_NameCustomCalculation_txtbox"));
		BaseUI.waitForElementToBeDisplayed("customCalc_AssetsGroupExpand", null, null);
		clickToExpandGroups("customCalc_AssetsGroupExpand");
		BaseUI.waitForElementToBeDisplayed("customCalc_CashAndDuesGroup", null, null);
		BaseUI.click(Locator.lookupElement("customCalc_CashAndDuesGroup"));
		Thread.sleep(1000);
		BaseUI.click(Locator.lookupElement("customCalc_CashGroupExpand"));
		Thread.sleep(500);
		BaseUI.dragAndDrop("customCalc_GLAccountList_1", "customCalc_DroppableSpace");
		checkSaveButton_IsEnabled();
		Thread.sleep(200);
		clickPlusIcon();
		BaseUI.dragAndDrop("customCalc_GLAccountList_2", "customCalc_DroppableSpace");
		checkSaveButton_IsEnabled();
		clickPlusIcon();
		BaseUI.dragAndDrop("customCalc_GLAccountList_3", "customCalc_DroppableSpace");
		checkSaveButton_IsEnabled();
		BaseUI.waitForElementToBeDisplayed("customCalc_ValidCalculation_text", null, null);
		clickOnSaveButton();
		BaseUI.waitForElementToBeDisplayed("customCalc_Search_Textbox", null, null);
		searchCustomCalc(storeCustomCalc);
		searchCustomCalcResultDisplayed();
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
		return customCalcName;
	}
	
	public static void verifyCustomCalcNotDisplayedAtConsolidatedEntity() throws Exception {
		Navigation.navigateToTools_CustomCalculations();
		CustomCalculations.switchToCustomCalcFrame();
		CustomCalculations.searchCustomCalc(customCalcName);
		CustomCalculations.searchCustomCalcResultNotDisplayed();
		BaseUI.switch_ToDefaultContent();
		StartPage.clickCloseAllTabsButton();
	}
}
