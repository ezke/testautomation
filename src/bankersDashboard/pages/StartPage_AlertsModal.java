package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class StartPage_AlertsModal {
	
	public static ArrayList<String> return_BalanceSheetAlertsList() {
		ArrayList<WebElement> bsAlerts = Locator.lookup_multipleElements("alerts_BalanceSheetTab_BSAlertsList", null,
				null);

		ArrayList<String> bsAlertsList = new ArrayList<String>();

		for (WebElement data : bsAlerts) {
			String capRatioValue = BaseUI.getTextFromField(data);
			bsAlertsList.add(capRatioValue);
		}
		
		return bsAlertsList;
	}
	
	
	public static ArrayList<String> return_IncomeStatementAlertsList() {
		ArrayList<WebElement> isAlerts = Locator.lookup_multipleElements("alerts_IncomeStateTab_ISAlertsList", null,
				null);

		ArrayList<String> isAlertsList = new ArrayList<String>();

		for (WebElement data : isAlerts) {
			String isValue = BaseUI.getTextFromField(data);
			isAlertsList.add(isValue);
		}
		
		return isAlertsList;
	}
	
	
	public static void clickViewBalanceSheetButton() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_BalanceSheetTab_ViewBSBtn"));
		Thread.sleep(2000);
	}
	
	
	public static void clickViewIncomeStatementButton() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_IncomeStateTab_ViewISBtn"));
		Thread.sleep(2000);
	}
	
	
	public static void clickLoans_SendEmailButton() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_LoansTab_SendEmailBtn"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("alerts_CDsTab_SendEmail_EmailDialogModalTitle", null, null);
	}
	
	
	public static void clickCds_SendEmailButton() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_CDsTab_SendEmailBtn"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("alerts_CDsTab_SendEmail_EmailDialogModalTitle", null, null);
	}
	
	
	public static void clickBalanceSheetTab() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_BalanceSheetTab"));
		Thread.sleep(1000);
	}
	
	
	public static void clickIncomeStatementTab() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_IncomeStateTab"));
		Thread.sleep(1000);
	}
	
	
	public static void clickLoansTab() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_LoansTab"));
		Thread.sleep(1000);
		//BaseUI.waitForElementToBeDisplayed("alerts_LoansTab_SendEmailBtn", null, null, 200);
		//BaseUI.wait_forElementToNotExist("alerts_LoansTab_NoAlertsMessage", null, null);
	}
	
	
	public static void clickCdsTab() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_CDsTab"));
		Thread.sleep(1000);
		//BaseUI.waitForElementToBeDisplayed("alerts_CDsTab_SendEmailBtn", null, null, 200);
		//BaseUI.wait_forElementToNotExist("alerts_CDsTab_NoAlertsMessage", null, null);
	}
	
	public static void clickRefreshIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_RefreshIcon"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("alerts_RefreshIcon", null, null, 1500);
	}
	
	
	public static void clickCloseAlertsModal() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_CloseAlertsModal"));
		Thread.sleep(1000);
	}
	

	public static ArrayList<String> return_BalanceSheetAlertsDollarValueList() {
		ArrayList<WebElement> bsAlerts$Values = Locator.lookup_multipleElements("alerts_BalanceSheetTab_Alerts_DollarValueList", null,
				null);

		ArrayList<String> bsAlerts$VaulesList = new ArrayList<String>();

		for (WebElement data : bsAlerts$Values) {
			String capRatioValue = BaseUI.getTextFromField(data);
			bsAlerts$VaulesList.add(capRatioValue);
		}
		
		return bsAlerts$VaulesList;
	}
	
		
	public static ArrayList<String> return_BalanceSheetAlertsPercentValueList() {
		ArrayList<WebElement> bsAlertsPerValues = Locator.lookup_multipleElements("alerts_BalanceSheetTab_Alerts_PercentValueList", null,
				null);

		ArrayList<String> bsAlertsPerVaulesList = new ArrayList<String>();

		for (WebElement data : bsAlertsPerValues) {
			String capRatioValue = BaseUI.getTextFromField(data);
			bsAlertsPerVaulesList.add(capRatioValue);
		}
		
		return bsAlertsPerVaulesList;
	}
	
	
	public static ArrayList<String> return_IncomeStatementAlertsDollarValueList() {
		ArrayList<WebElement> isAlerts$Values = Locator.lookup_multipleElements("alerts_IncomeStatementTab_Alerts_DollarValueList", null,
				null);

		ArrayList<String> isAlerts$VaulesList = new ArrayList<String>();

		for (WebElement data : isAlerts$Values) {
			String isValue = BaseUI.getTextFromField(data);
			isAlerts$VaulesList.add(isValue);
		}
		
		return isAlerts$VaulesList;
	}
	
		
	public static ArrayList<String> return_IncomeStatementAlertsPercentValueList() {
		ArrayList<WebElement> isAlertsPerValues = Locator.lookup_multipleElements("alerts_IncomeStatementTab_Alerts_PercentValueList", null,
				null);

		ArrayList<String> isAlertsPerVaulesList = new ArrayList<String>();

		for (WebElement data : isAlertsPerValues) {
			String isValue = BaseUI.getTextFromField(data);
			isAlertsPerVaulesList.add(isValue);
		}
		
		return isAlertsPerVaulesList;
	}
	
	
	public static ArrayList<String> return_TotalLoansAlertsList() {
		ArrayList<WebElement> loansAlerts = Locator.lookup_multipleElements("alerts_LoansTab_LoansAlertsList", null,
				null);

		ArrayList<String> loansAlertsList = new ArrayList<String>();

		for (WebElement data : loansAlerts) {
			String loansAlertsValue = BaseUI.getTextFromField(data);
			loansAlertsList.add(loansAlertsValue);
		}
		
		return loansAlertsList;
	}
	
	
	public static ArrayList<String> return_TotalCDsAlertsList() {
		ArrayList<WebElement> cdsAlerts = Locator.lookup_multipleElements("alerts_CDsTab_CDAlertsList", null,
				null);

		ArrayList<String> cdsAlertsList = new ArrayList<String>();

		for (WebElement data : cdsAlerts) {
			String cdsAlertsValue = BaseUI.getTextFromField(data);
			cdsAlertsList.add(cdsAlertsValue);
		}
		
		return cdsAlertsList;
	}
	
	
	public static void verifyLoansAlert_AlertsNum_MatchesAlertsDisplayed() {
		String totalLoansAlerts = BaseUI.
				getTextFromField(Locator.lookupElement("alerts_LoansTab_LoansAlertsNum"));
		ArrayList<String> totalLoansAlertsList = StartPage_AlertsModal.return_TotalLoansAlertsList();
		
		BaseUI.verify_true_AndLog(totalLoansAlerts.equals(String.valueOf(totalLoansAlertsList.size())), 
				"Loans Alerts Number and displayed List Match "+ totalLoansAlertsList.size(), 
				"Loans Alerts Number and displayed List DOES NOT Match "+ totalLoansAlertsList.size());
	}
	
	
	public static void verifyCDsAlert_AlertsNum_MatchesAlertsDisplayed() {
		String totalCDsAlerts = BaseUI.
				getTextFromField(Locator.lookupElement("alerts_CDsTab_CDAlertsNum"));
		ArrayList<String> totalCDsAlertsList = StartPage_AlertsModal.return_TotalCDsAlertsList();
		
		BaseUI.verify_true_AndLog(totalCDsAlerts.equals(String.valueOf(totalCDsAlertsList.size())), 
				"CDs Alerts Number and displayed List Match "+ totalCDsAlertsList.size(), 
				"CDs Alerts Number and displayed List DOES NOT Match "+ totalCDsAlertsList.size());
	}
	
	
	public static void clickEmailModal_CancelBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CancelBtn"));
		Thread.sleep(1000);
		BaseUI.wait_forElementToNotExist("alerts_CDsTab_SendEmail_EmailDialogModalTitle", null, null);
	}
	
	
	public static void clickEmailModal_SendBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_SendBtn"));
		Thread.sleep(1000);
		BaseUI.wait_forElementToNotExist("alerts_CDsTab_SendEmail_EmailDialogModalTitle", null, null);
	}
	
	
	public static ArrayList<String> return_CDsAlertsDetails() {
		ArrayList<WebElement> cdsAlertsDetails = Locator.lookup_multipleElements("alerts_CDsTab_CDAlertsDetails", null,
				null);

		ArrayList<String> cdsAlertsInfo = new ArrayList<String>();

		for (WebElement data : cdsAlertsDetails) {
			String cdsAlertsValue = BaseUI.getTextFromField(data);
			cdsAlertsInfo.add(cdsAlertsValue);
		}
		
		return cdsAlertsInfo;
	}
	
	
	public static void verifyAlertsDetails_MatchAlertsDetailsOnEmailModal(ArrayList<String> AlertsDetails) {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		for (int i=0;i<AlertsDetails.size();i++) {
			BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
					AlertsDetails.get(i), cdDetailsOnEmailModal);
		}	
	}
	
	
	public static ArrayList<String> return_LoansAlertsDetails() {
		ArrayList<WebElement> loansAlertsDetails = Locator.lookup_multipleElements("alerts_LoansTab_LoansAlertsDetails", null,
				null);

		ArrayList<String> loansAlertsInfo = new ArrayList<String>();

		for (WebElement data : loansAlertsDetails) {
			String loansAlertsValue = BaseUI.getTextFromField(data);
			loansAlertsInfo.add(loansAlertsValue);
		}
		
		return loansAlertsInfo;
	}
	
	
	public static void enterEmailDetailsForCDAlerts_SendAlertEmail(String emailID) throws Exception {
		StartPage.clickStartPageTab();
		StartPage.clickMyAlertsBellIcon();
		StartPage_AlertsModal.clickCdsTab();
		
		StartPage_AlertsModal.clickCds_SendEmailButton();
		BaseUI.click(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText"));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText"), 
				emailID);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_BodyTextarea"), 
				"This email is to test CD alerts email feature. Please ignore!");
		clickEmailModal_SendBtn();
	}
	
	
	public static void enterEmailDetailsForLoansAlerts_SendAlertEmail(String emailID) throws Exception {
		StartPage.clickStartPageTab();
		StartPage.clickMyAlertsBellIcon();
		
		StartPage_AlertsModal.clickLoansTab();		
		StartPage_AlertsModal.clickLoans_SendEmailButton();
		BaseUI.click(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText"));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_ToAddressText"), 
				emailID);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_BodyTextarea"), 
				"This email is to test Loans alerts email feature. Please ignore!");
		clickEmailModal_SendBtn();		
	}
	
	public static void verifyAlertsDetails_MatchAlertsEmailDetails_ProdName(ArrayList<String> AlertsDetails) {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		//for (int i=0;i<AlertsDetails.size();i++) {
			BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
					AlertsDetails.get(0), cdDetailsOnEmailModal);
		//}	
	}
	
	public static void verifyAlertsDetails_MatchAlertsEmailDetails_RateAndTerm(ArrayList<String> AlertsDetails) {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		String rateAmount = AlertsDetails.get(1).substring(0, AlertsDetails.get(1).indexOf("%"));
		String exprateAmount = String.valueOf(Math.round(Double.valueOf(rateAmount)));
			BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
					exprateAmount, cdDetailsOnEmailModal);
	}
	
	public static void verifyAlertsDetails_MatchAlertsEmailDetails_Date(ArrayList<String> AlertsDetails) throws Exception {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
	
		String amountText = AlertsDetails.get(2).substring(AlertsDetails.get(2).indexOf(" ")).trim();
		String date = BaseUI.return_Date_AsDifferentFormat(amountText, "MMM dd, yyyy", "MM/dd/yyyy");
		
		if(date.startsWith("0")){
			date = BaseUI.return_Date_AsDifferentFormat(amountText, "MMM dd, yyyy", "M/dd/yyyy");
		}
		
		BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
				"Date: "+date, cdDetailsOnEmailModal.replaceAll("[^\\p{ASCII}]", ""));
	}
	
	
	public static void verifyAlertsDetails_MatchAlertsEmailDetails_Amount(ArrayList<String> AlertsDetails) throws Exception {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails"));
		String amountText = AlertsDetails.get(2).substring(0, AlertsDetails.get(2).indexOf(",", 2));
		BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
				amountText, cdDetailsOnEmailModal);
	}
	
	
	public static void verifyAlertsDetails_MatchAlertsEmailDetails_Officer(ArrayList<String> AlertsDetails) {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails")).trim();
		String officerName = AlertsDetails.get(3).substring(0, AlertsDetails.get(3).indexOf(",")).trim();
		String expected_OffBranch = "Officer: "+officerName;
		BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
				expected_OffBranch, cdDetailsOnEmailModal);	
	}
	
	
	public static void verifyAlertsDetails_MatchAlertsEmailDetails_Branch(ArrayList<String> AlertsDetails) {	
		String cdDetailsOnEmailModal = BaseUI.get_NonVisible_TextFromField(Locator
						.lookupElement("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails")).trim();
		String branchName = AlertsDetails.get(3).replaceAll(".*,", "").trim();
		String expected_OffBranch = "Branch: "+branchName;
		BaseUI.baseStringPartialCompare("alerts_CDsTab_SendEmail_EmailDialogModal_CDAlertDetails", 
				expected_OffBranch, cdDetailsOnEmailModal);	
	}
	
}
