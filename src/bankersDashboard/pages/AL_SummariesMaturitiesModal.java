package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class AL_SummariesMaturitiesModal {

	public static void selectViewOption(String viewOption) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("alSum_MaturitiesModal_View_Dropdown"), 
				viewOption);
		Thread.sleep(300);
	}
	
	
	public static void selectDateOption(String dateOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("alSum_MaturitiesModal_Date_Dropdown"), 
				dateOption);
		Thread.sleep(300);
	}
	
	
	public static void selectRegion_BranchOption(int regBranchOption) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("alSum_MaturitiesModal_Reg_Branch_Dropdown"), 
				regBranchOption);
		Thread.sleep(300);
	}
	
	
	public static void selectOfficersOption(int officersOption) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("alSum_MaturitiesModal_Officers_Dropdown"), 
				officersOption);
		Thread.sleep(300);
	}
	
	
	public static void verifyNonAccuralLoans_Checkbox_IsChecked() {
		BaseUI.verifyCheckboxStatus("alSum_MaturitiesModal_NonAccrualLoans_Checkbox", "checked");
	}
	
	
	public static void clickShowReportBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("alSum_MaturitiesModal_ShowRepBtn"));
		Thread.sleep(2000);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
	}
}
