package bankersDashboard.pages;

import bankersDashboard.pageControls.DropDowns;
import utils.BaseUI;
import utils.Locator;

public class Help_SupportModal {
	
	public static void clickCancelButton() throws Exception {
		BaseUI.click(Locator.lookupElement("helpSupReq_NewSupportCaseCancel_Btn"));
		Thread.sleep(1000);
	}
	
	
	public static void clickSendButton() throws Exception {
		BaseUI.click(Locator.lookupElement("helpSupReq_NewSupportCase_OpenCaseBtn"));
		Thread.sleep(3000);
	}
	
	
	public static void enterCaseDetails_SubjectAndDescription() throws Exception {
		if(BaseUI.elementAppears(Locator.lookupElement("helpSupReq_PhoneText"))){
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("helpSupReq_PhoneNumber_Textbox"), "1234567890");
		}
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("helpSupReq_Subject_Textbox"), 
				"Automation Testing New Support Case webapp feature");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("helpSupReq_Description_Textbox"), 
				"Automation Testing New Support Case webapp feature...Please ignore this case");
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("helpSupReq_Category_Dropdown"), 2);
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("helpSupReq_Category_Dropdown"), 0);
	}
}
