package bankersDashboard.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Settings_MyAlertsModal {
	
	public static ArrayList<String> return_PercentAlert() {
		ArrayList<WebElement> alertPercentData = Locator.lookup_multipleElements("setting_MyAlerts_PercentAlertTextbox", null,
				null);

		ArrayList<String> alertPercentList = new ArrayList<String>();

		for (WebElement data : alertPercentData) {
			String capRatioValue = BaseUI.getTextFromField(data);
			alertPercentList.add(capRatioValue);
		}
		
		return alertPercentList;	
	}
	
	
	public static void enterIncomeStatement_PercentAlertValue(String setValue) throws Exception {
		List<WebElement> alertPercent = Locator.lookup_multipleElements("setting_MyAlerts_IncomeStatementTab_PercentAlertTextbox", 
				null, null);
		BaseUI.verify_true_AndLog(alertPercent.size() > 0 , "Percent Alert text exists",
				"Percent Alert DOES NOT text exists");
		
		for (WebElement alertPercentTxtBx : alertPercent) {
			BaseUI.click(alertPercentTxtBx);
			BaseUI.enterText_IntoInputBox(alertPercentTxtBx, setValue);
		}
	}
	
	
	public static void enterBalSheet_PercentAlertValue(String setValue) throws Exception {
		List<WebElement> alertPercent = Locator.lookup_multipleElements("setting_MyAlerts_BalanceSheetTab_PercentAlertTextbox", 
				null, null);
		BaseUI.verify_true_AndLog(alertPercent.size() > 0 , "Percent Alert text exists",
				"Percent Alert DOES NOT text exists");
		
		for (WebElement alertPercentTxtBx : alertPercent) {
			BaseUI.click(alertPercentTxtBx);
			BaseUI.enterText_IntoInputBox(alertPercentTxtBx, setValue);
		}
	}
	
	
	public static void updateAlertPercentValue_BalanceSheet(String setValue) throws Exception {
		StartPage.clickStartPageTab();
		Navigation.navigateToSetting_MyAlerts();
		clickBalanceSheetTab();
		enterBalSheet_PercentAlertValue(setValue);
		clickSaveButton();		
	}
		
	public static void updateAlertPercentValue_IncomeStatement(String setValue) throws Exception {
		StartPage.clickStartPageTab();
		Navigation.navigateToSetting_MyAlerts();
		clickIncomeStatementTab();
		enterIncomeStatement_PercentAlertValue(setValue);
		clickSaveButton();
	}
	
	public static void clickBalanceSheetTab() throws InterruptedException {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("setting_MyAlerts_BalanceSheetTab"));
		Thread.sleep(300);
	}
	
	
	public static void clickIncomeStatementTab() throws InterruptedException {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("setting_MyAlerts_IncomeStatementTab"));
		Thread.sleep(300);
	}
	
	
	public static void clickOtherTab() throws InterruptedException {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("setting_MyAlerts_OtherTab"));
		Thread.sleep(300);
		BaseUI.waitForElementToBeDisplayed("setting_MyAlerts_OthersTab_DefaultBranchPerformanceSubHeader", null, null);
	}
	
	
	public static void clickSaveButton() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("setting_MyAlerts_SaveButton"));
		Thread.sleep(1300);
		BaseUI.wait_forElementToNotExist("setting_MyAlerts_ModalTitle", null, null);
	}
	
	
	public static void clickCancelButton() throws InterruptedException {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("setting_MyAlerts_CancelButton"));
		Thread.sleep(1300);
	}
	
	
	public static void selectLoansThreshold1() throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setting_MyAlerts_OthersTab_LoansThreshold1Dropdown"), 
				"3 Std Dev");
		Thread.sleep(300);
	}
	
	
	public static void selectLoansThreshold2() throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setting_MyAlerts_OthersTab_LoansThreshold2Dropdown"), 
				"45 Days");
		Thread.sleep(300);
	}
	
	
	public static void selectCdsThreshold1() throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setting_MyAlerts_OthersTab_CdsThreshold1Dropdown"), 
				"3 Std Dev");
		Thread.sleep(300);
	}
	
	
	public static void selectCdsThreshold2() throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setting_MyAlerts_OthersTab_CdsThreshold2Dropdown"), 
				"45 Days");
		Thread.sleep(300);
	}
	
	
	public static void updateLoansThresholds() throws Exception {
		Navigation.navigateToSetting_MyAlerts();
		clickOtherTab();
		selectLoansThreshold1();
		selectLoansThreshold2();
		clickSaveButton();		
	}
	
	
	public static void updateCDsThresholds() throws Exception {
		Navigation.navigateToSetting_MyAlerts();
		clickOtherTab();	
		selectCdsThreshold1();
		selectCdsThreshold2();
		clickSaveButton();
	}
	
	
	public static void setLoanThresholds_IfNoAlertMessageDisplayed() throws Exception {
		StartPage_AlertsModal.clickCloseAlertsModal();
		Settings_MyAlertsModal.updateLoansThresholds();
		StartPage.clickStartPageTab();
		StartPage.clickMyAlertsBellIcon();
		StartPage_AlertsModal.clickRefreshIcon();
		StartPage_AlertsModal.clickLoansTab();
	}
	
	
	public static void setCdsThresholds_IfNoAlertMessageDisplayed() throws Exception {
		StartPage_AlertsModal.clickCloseAlertsModal();
		Settings_MyAlertsModal.updateCDsThresholds();
		StartPage.clickStartPageTab();
		StartPage.clickMyAlertsBellIcon();
		StartPage_AlertsModal.clickRefreshIcon();
		StartPage_AlertsModal.clickCdsTab();	
	}

}
