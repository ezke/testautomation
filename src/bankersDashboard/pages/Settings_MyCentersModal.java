package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class Settings_MyCentersModal {
	
	public static void clickRemoveAllCentersBtn() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("myCenters_RemoveBtn"));
		Thread.sleep(500);
		BaseUI.wait_forElementToNotExist("myCenters_DragFromMyCentersBox_1", null, null);
	}
	
	
	public static void clickAddAllCentersBtn() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("myCenters_AddBtn"));
		Thread.sleep(500);
	}
	
	
	public static void dragAndDropCenters() throws Exception {
		BaseUI.click(Locator.lookupElement("myCenters_DragFromAvailableCentersBox_1"));
		BaseUI.dragAndDrop("myCenters_DragFromAvailableCentersBox_1", "myCenters_DropIntoMyCentersBox");
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("myCenters_DragFromAvailableCentersBox_2"));
		BaseUI.dragAndDrop("myCenters_DragFromAvailableCentersBox_2", "myCenters_DropIntoMyCentersBox");
		Thread.sleep(1000);
	}
	
	
	public static void clickMyCenters_FinishedBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("myCenters_FinishedBtn"));
		Thread.sleep(1000);
	}
	
	
	public static void addCenters_MyCenters() throws Exception {
		Navigation.navigateToSetting_MyCenters();
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.waitForElementToBeDisplayed("myCenters_AvailableCentersText", null, null);
		BaseUI.waitForElementToBeDisplayed("myCenters_RemoveCentersText", null, null);
//		BaseUI.verifyElementHasExpectedText("myCenters_AvailableCentersText", "Available Centers");
//		BaseUI.verifyElementHasExpectedText("myCenters_RemoveCentersText", "My Centers");
		BaseUI.switch_ToDefaultContent();
		clickRemoveAllCentersBtn();
		
		dragAndDropCenters();
//		String draggedCenter1 = BaseUI.getTextFromField(Locator.lookupElement("myCenters_DragFromMyCentersBox_1"));
//		String draggedCenter2 = BaseUI.getTextFromField(Locator.lookupElement("myCenters_DragFromMyCentersBox_2"));
		clickMyCenters_FinishedBtn();
	}
	
	
	public static void removeMyCenters() throws Exception {
		Navigation.navigateToSetting_MyCenters();
		clickRemoveAllCentersBtn();
		clickMyCenters_FinishedBtn();
        BaseUI.waitForElementToNOTBeDisplayed("myCenters_ModalTitle", null, null);
	}

}
