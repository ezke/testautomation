package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class IS_NonInterestIncomeModal {
	
	public static void clickShowReportButton() throws Exception
	{
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("balanceSheet_showReportBtn"));
		Thread.sleep(2500);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("nonInterestIncome_ReportTitle", null, null);
	}

	
	public static void clickToDrilldownReport(String elementToClick) throws Exception
	{
		BaseUI.click(Locator.lookupElement(elementToClick));
		Thread.sleep(2000);
	}
	
	
	public static void selectSecColDateValue(String date) throws Exception {
		BaseUI.click(Locator.lookupElement("nonInterestIncome_Date_column2_dropdown"));
		Thread.sleep(200);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("nonInterestIncome_Date_column2_dropdown"), date);
		Thread.sleep(500);
	}
	
	
	public static ArrayList<String> return_PrimaryCol_Data(String reportPrimaryColData) {
		ArrayList<WebElement> primaryColData = Locator.lookup_multipleElements(reportPrimaryColData, null,
				null);

		ArrayList<String> primaryDataList = new ArrayList<String>();

		for (WebElement data : primaryColData) {
			String colValue = BaseUI.getTextFromField(data).trim();
			primaryDataList.add(colValue);
		}
			
		return primaryDataList;
	}
	    
	    
	public static ArrayList<String> return_SecondColumn_data(String reportSecColData) {
	    ArrayList<WebElement> secondColData = Locator.lookup_multipleElements(reportSecColData, null,
				null);

		ArrayList<String> secDataList = new ArrayList<String>();

		for (WebElement data : secondColData) {
			String colValue = BaseUI.getTextFromField(data).trim();
			secDataList.add(colValue);
			System.out.println(colValue);
		}
			
		return secDataList;
	}
	    
	    
	public static void verifyPrimarySecond_ColumnData(String reportPrimaryColData, String reportSecColData) {
	    ArrayList<String> primaryDataList = return_PrimaryCol_Data(reportPrimaryColData);
	    BaseUI.verify_true_AndLog(primaryDataList.size() > 0, "Array list is NOT empty", "Array List is empty");
	    
	    ArrayList<String> secDataList = return_SecondColumn_data(reportSecColData);
	    BaseUI.verify_true_AndLog(primaryDataList.size()== secDataList.size(), "Array list Match", 
	    		"Array List DOES NOT Match");
	    	
	    for(int i=0; i<secDataList.size(); i++) {
	    	BaseUI.verify_true_AndLog(secDataList.get(i).contains(primaryDataList.get(i)), 
	    			secDataList.get(i) + " Second column data MATCH Primary column "+ primaryDataList.get(i), 
	                secDataList.get(i) + " Second column data DOES NOT MATCH Primary column "+ primaryDataList.get(i));
	    }
	}
}
