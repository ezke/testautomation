package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class CapitalReqReportPage {
	
	
	public static ArrayList<String> return_CapitalReq_CommonTierData() {
		ArrayList<WebElement> capReqData = Locator.lookup_multipleElements("capReq_RatioList", null,
				null);

		ArrayList<String> dataList = new ArrayList<String>();

		for (WebElement data : capReqData) {
			String capReqValue = BaseUI.getTextFromField(data);
			dataList.add(capReqValue);
		}
		
		return dataList;
	}
	
	
	public static void verifyDataOnCapitalReq_MatchCapRatio(ArrayList<String> capRatioData, String dateOption) throws Exception {
		
		BaseUI.scroll_to_element(Locator.lookupElement("capReq_ReportGroupHeading_TotRiskWeightedAssets_MonthEnd"));
		String commTier1Ratio = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_CommTier1Ratio"));
		String tier1LevRatio = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_Tier1LevRatio"));
		String tier1RiskBasedRatio = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_Tier1RiskBasedRatio"));
		String totalRiskBasedRatio = BaseUI.getTextFromField(Locator.lookupElement("capReq_Report_TotalRiskBasedRatio"));
		String expectedDateCol1 = BaseUI.return_Date_AsDifferentFormat(dateOption, "MMMM yyyy", "MMM yyyy").trim();
		
		int capRatioSize = capRatioData.size()-1;
		for(int i = capRatioSize; i>=0; i--) {
			if(!(i == capRatioData.size()-2)) {
				BaseUI.verify_true_AndLog((capRatioData.get(i).trim()).contains(commTier1Ratio.trim()) ||
						(capRatioData.get(i).trim()).contains(tier1LevRatio.trim()) ||
						(capRatioData.get(i).trim()).contains(tier1RiskBasedRatio.trim()) ||
						(capRatioData.get(i).trim()).contains(totalRiskBasedRatio.trim()) || 
						(capRatioData.get(i).trim()).contains(expectedDateCol1.trim()), 
						"Capital Requirement Data MATCH " + capRatioData.get(i), 
						"Capital Requirement Data DOES NOT MATCH " + capRatioData.get(i));
			}
		}
	}
	
	
	public static void selectCol1_DateOption(String dateOption) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("capReq_Modal_Col1DateOption"), 
				dateOption);
		Thread.sleep(300);
	}
	
	
	public static void selectCol2_DateOption(String dateOption) throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("capReq_Modal_Col2DateOption"), 
				dateOption);
		Thread.sleep(300);
	}
	
	
	public static void clickShowReportBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("capReq_ShowRepBtn"));
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("capReq_ReportTitle", "Capital Requirements", null);
	}
	
	
	public static void selectDateOptions_CapReqReport(String dateOptionCol1, 
			String dateOptionCol2) throws Exception {
		selectCol1_DateOption(dateOptionCol1);
		selectCol2_DateOption(dateOptionCol2);
		clickShowReportBtn();		
	}
	
	
	public static void clickRepPropertiesBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_Report_properties_Icon"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("capReq_ModalTitle", null, null);
	}
	
	
	public static void clickRepProperties_SelectNewDateOption(String dateOptionCol1, 
			String dateOptionCol2) throws Exception {
		clickRepPropertiesBtn();
		selectDateOptions_CapReqReport(dateOptionCol1, dateOptionCol2);
		BaseUI.verifyElementAppears(Locator.lookupElement("capReq_ReportTitle"));
	}

}
