package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class LoanTypesModal {
	
	public static ArrayList<String> return_LoanTypesDescriptionList() {
		ArrayList<WebElement> loanTypesDescriptionData = Locator.lookup_multipleElements("loanPricing_LoanTypes_DescriptionColList", null,
				null);

		ArrayList<String> loanTypesDescriptionList = new ArrayList<String>();

		for (WebElement data : loanTypesDescriptionData) {
			String loanTypesDescriptionValue = BaseUI.getTextFromInputBox(data);
			loanTypesDescriptionList.add(loanTypesDescriptionValue);
		}
		
		return loanTypesDescriptionList;
	}
	
	
//	public static void verifyLoanTypeExists_ElseAddNew() throws Exception {
//		ArrayList<String> loanTypesList = return_LoanTypesDescriptionList();
//    	BaseUI.verify_true_AndLog(loanTypesList.size() > 0, "Loan types description column is NOT empty", 
//    			"Loan types description column is empty");
//    	
//    	for(int i=0; i<loanTypesList.size();) {
//    		if(loanTypesList.get(i).trim().equals("Home Equity")) {
//    			BaseUI.switch_ToDefaultContent();
//    			click_UpdateBtn();			
//    		} else {
//    			System.out.println(loanTypesList.get(i));
//    			i++;
//    		}
//    	}	
//    	if(BaseUI.elementAppears(Locator.lookupElement("loanPricing_LoanTypes_UpdateBtn"))){
//    		addNewLoanType();
//    	}
//	}
	
	
	public static void click_UpdateBtn() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("loanPricing_LoanTypes_UpdateBtn"));
		Thread.sleep(500);
	}
	
	
	public static void click_AddNewLoanTypeBtn() throws Exception {
		BaseUI.click_js(Locator.lookupElement("loanPricing_LoanTypes_AddNewLoanTypeBtn"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanTypes_RiskWeighting_Dropdown", null, null);
	}
	
	
	public static void addNewLoanType() throws Exception {
		StartPage.switchToIframe();
		click_AddNewLoanTypeBtn();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanTypes_DescriptionTxtbx"), 
				"Home Equity");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanTypes_RiskWeighting_Dropdown"), 
				"Risk Weighting 3");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanTypes_OrgCostTxtbx"), 
				"200");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanTypes_AnnServCostTxtbx"), 
				"100");
		click_UpdateBtn();
		click_UpdateBtn();
	}
	
	public static void newLoanType() throws Exception {
		Navigation.navigateToLoanPricing_LoanTypes();
		addNewLoanType();
	}

	public static void deleteLoanType() throws Exception {
		StartPage.clickStartPageTab();
		Navigation.navigateToLoanPricing_LoanTypes();
		clickLoanTypeDeleteIcon();
		click_UpdateBtn();
	}


	public static void clickLoanTypeDeleteIcon() throws Exception {
		StartPage.switchToIframe();
		BaseUI.click(Locator.lookupElement("loanPricing_LoanTypes_HomeEquityDeleteIcon"));
		Thread.sleep(500);
		BaseUI.switchToAlert_Accept();
	}
}
