/**
 * 
 */
package bankersDashboard.pages;

import bankersDashboard.pageControls.EntitySelector;
import bankersDashboard.pageControls.MouseHover;
import utils.BaseUI;
import utils.Locator;

public class Navigation {
	
	public static String draggedCenter1, draggedCenter2;
	
	public static void navigate_to_userDropdown() throws Exception 
	{
		BaseUI.click(Locator.lookupRequiredElement("startPage_Username_dropdown"));
		BaseUI.waitForElementToBeDisplayed("startPage_Logout_option", null, null, 10);
		Thread.sleep(2000);
	}
	
	public static void navigate_to_signOut() throws Exception {
		StartPage.clickStartPageTab();
		navigate_to_userDropdown();
		BaseUI.click_js(Locator.lookupRequiredElement("startPage_Logout_option"));
		BaseUI.waitForAlertToAppear(30);
		//Thread.sleep(1500);
		BaseUI.switchToAlert_Accept();
	}
	
	public static void navigate_to_changePassword(String oldPwd, String newPwd) throws Exception
	{
		navigate_to_userDropdown();
		BaseUI.click(Locator.lookupElement("changePassword_dropdown"));
		Thread.sleep(3000);
		navigate_toIframe_window();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("changePassword_OldPassword_txtbox"), oldPwd);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("changePassword_NewPassword_txtbox"), newPwd);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("changePassword_ConfirmNewPassword_txtbox"), newPwd);
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("changePassword_ChangeButton"));
		Thread.sleep(500);
	}
	
	public static void navigate_to_Preferences() throws Exception
	{
		navigate_to_userDropdown();
		BaseUI.click(Locator.lookupElement("userPreferences_dropdown"));
		Thread.sleep(2000);
		navigate_toIframe_window();
		
	}
	
	
	public static void navigate_toIframe_window() throws InterruptedException
	{
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		Thread.sleep(2000);
	}
	
	
   //Dashboard left menu
	public static void navigateToDashboard_Summary() throws Exception
	  {
		   MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Dashboards", "navigation_Dashboards_Summary");
		   BaseUI.wait_forElementToNotExist("reportLoadingText", "3000", null); 
		   BaseUI.wait_forElementToNotExist("capRatios_ReportLoadingText", "2000", null);
		   BaseUI.wait_forElementToNotExist("reportLoading_Phrase", "3000", null);
		   BaseUI.wait_forPageToFinishLoading();
		   Thread.sleep(2000);
		   BaseUI.waitForElementToBeDisplayed("summaryReport_ReportTitle", null, null, 60);
	  }
	
	
	public static void navigateToQuarterlyDashboard() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Dashboards", 
				"navigation_Dashboards_QuarterlyDashboard");
		BaseUI.waitForElementToBeDisplayed("quarterlyDashboard_ModalTitle",null, null);
		StartPage.switchToIframe();
	}
	
	
	public static void navigateToMonthlyDashboard() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Dashboards", 
				"navigation_Dashboards_MonthlyDashboard");
		BaseUI.waitForElementToBeDisplayed("monthlyDashboard_modal_title",null, null);
		StartPage.switchToIframe();
	}
	
	
	//GL>Summaries Left Menu Navigation
	public static void navigateToGL_ExecutiveSummaries() throws Exception {
		 MouseHover.secondSubMenuOption1("navigation_GL", "navigation_GLSummaries", 
				  "arrow_toNavigate_Summaries_submenu", "navigation_GLSummaries_ExecutiveSummary");
    }
	
	
	public static void navigateToGL_ExecutiveSummariesCompare() throws Exception {		  
		 MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_Summaries_submenu", 
				  "navigation_GLSummaries_ExecutiveSummaryCompare");
		  //Thread.sleep(2000);
	}
	 
	
	public static void navigateToGL_ExecutiveSummaries_12mos() throws Exception{
		 MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_Summaries_submenu",
				  "navigation_GLSummaries_ExecutiveSummary_12mos");
		  //Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_RatioAnalysis() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_Summaries_submenu", 
				  "navigation_GLSummaries_RatioAnalysis");
		//Thread.sleep(2000);
	}
	
	
	
	//GL>Balance Sheet Left Menu Navigation
	public static void navigateToGL_BS_BalanceSheetCompare() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_BS_submenu", 
				  "navigation_GLBalanceSheet_BalanceSheetCompare");
		
		BaseUI.waitForElementToBeDisplayed("balanceSheetCompare_ModalTitle", null, "60");
		StartPage.switchToIframe();
	}
	
	
	public static void navigateToGL_BS_BalanceSheet() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_BS_submenu", 
				  "navigation_GLBalanceSheet_BalanceSheet");
	}
	
	
	public static void navigateToGL_BS_EarningAssetMix() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_BS_submenu", 
				  "navigation_GLBalanceSheet_EarningAssetMix");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_BS_FundingMix() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_BS_submenu", 
				  "navigation_GLBalanceSheet_FundingMix");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_BS_NonEarningAssetMix() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_BS_submenu", 
				  "navigation_GLBalanceSheet_NonEarningAssetMix");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_BS_LoanConcentration() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_BS_submenu", 
				  "navigation_GLBalanceSheet_LoanConcentration");
	    //Thread.sleep(2000);
	}
	
	
	//GL>Income Statement Left Menu Navigation
	public static void navigateToGL_IS_IncomeStatementCompare() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_IS_submenu", 
				  "navigation_GLIncomeStatement_IncomeStatementCompare");
		BaseUI.waitForElementToBeDisplayed("incomeStatementCompare_modalTitle", null, null);
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
	}
	
	
	public static void navigateToGL_IS_IncomeStatement() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_IS_submenu", 
				  "navigation_GLIncomeStatement_IncomeStatement");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_IS_NonInterestIncome() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_IS_submenu", 
				  "navigation_GLIncomeStatement_NonInterestIncome");
		BaseUI.waitForElementToBeDisplayed("nonInterestIncome_ModalTitle", null, null);
		StartPage.switchToIframe();
	}
	
	
	public static void navigateToGL_IS_NonInterestExpense() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_IS_submenu",
				  "navigation_GLIncomeStatement_NonInterestExpense");
		BaseUI.waitForElementToBeDisplayed("nonInterestExpense_ModalTitle", null, null);
		StartPage.switchToIframe();
	}
	
	//GL>Margin Analysis Left Menu Navigation
	public static void navigateToGL_MarginAnalysis_MarginAnalysisCompare() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_MarginAnalysis_submenu", 
				  "navigation_GLMarginAnalysis_MarginAnalysisCompare");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_MarginAnalysis_MarginAnalysis() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_MarginAnalysis_submenu",
				  "navigation_GLMarginAnalysis_MarginAnalysis");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_MarginAnalysis_MarginGraph() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_MarginAnalysis_submenu", 
				  "navigation_GLMarginAnalysis_MarginGraph");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_MarginAnalysis_MarginComponentGraph() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_MarginAnalysis_submenu",
				  "navigation_GLMarginAnalysis_MarginComponentsGraph");
		//Thread.sleep(2000);
	}
	
	
	//GL>Capital & Regulatory Left Menu Navigation
	public static void navigateToGL_CapitalAndRegulatory_LiquidityReport() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_CapitalAndRegulatory_submenu",
				  "navigation_GLCapitalAndRegulatory_LiquidityReport");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_CapitalAndRegulatory_AllowanceForLoanLossAdequacy() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_CapitalAndRegulatory_submenu", 
				  "navigation_GLCapitalAndRegulatory_AllowanceForLoanLossAdequacy");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_CapitalAndRegulatory_CapitalRequirements() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_CapitalAndRegulatory_submenu", 
				  "navigation_GLCapitalAndRegulatory_CapitalRequirements");
		BaseUI.waitForElementToBeDisplayed("capReq_ModalTitle", null, null);
	}
	
	
	public static void navigateToGL_CapitalAndRegulatory_CapitalRatios() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_CapitalAndRegulatory_submenu", 
				  "navigation_GLCapitalAndRegulatory_CapitalRatios");
		BaseUI.waitForElementToBeDisplayed("capRatios_ModalTitle", null, null);
		BaseUI.verifyElementAppears(Locator.lookupElement("capRatios_ModalTitle"));
    }
	
	
	public static void navigateToGL_CapitalAndRegulatory_RiskWeightedAssetsWorksheet() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_CapitalAndRegulatory_submenu", 
				  "navigation_GLCapitalAndRegulatory_RiskWeightedAssetsWorksheet");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToGL_CapitalAndRegulatory_TexasRatio() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_GL", "arrow_toNavigate_CapitalAndRegulatory_submenu", 
				  "navigation_GLCapitalAndRegulatory_TexasRatio");
		//Thread.sleep(2000);
	}
	
    //Al>Summaries Left Menu Navigation
	public static void navigateToAL_Summaries_NewAndRenewed() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Summaries_submenu", 
				  "navigation_ALSummaries_NewAndRenewed");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_Summaries_Maturities() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Summaries_submenu", 
				  "navigation_ALSummaries_Maturities");
		//Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("alSum_MaturitiesModal_ModalTitle", null, null);
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
	}
	
	
	//Al>Loans Left Menu Navigation
	public static void navigateToAL_Loans_CurrentPortfolio() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Loans_submenu", 
				  "navigation_ALLoans_CurrentPortfolio");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_Loans_HistoricalMonthlyLoans() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Loans_submenu", 
				  "navigation_ALLoans_HistoricalMonthlyLoans");
		//Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("historicalMonthlyLoans_ModalTitle", null, null);
	}
	
	
	public static void navigateToAL_Loans_LoanMaturities() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Loans_submenu",
				  "navigation_ALLoans_LoanMaturities");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_Loans_LoanRepricing() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Loans_submenu", 
				  "navigation_ALLoans_LoanRepricing");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_Loans_PrimeRateShock() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_Loans_submenu", 
				  "navigation_ALLoans_PrimeRateShock");
		//Thread.sleep(2000);
	}
	
	
	//Al>CDs Left Menu Navigation
	public static void navigateToAL_CDs_CurrentPortfolio() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_CDs_submenu", 
				  "navigation_ALCDs_CurrentPortfolio");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_CDs_HistoricalMonthlyCDs() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_CDs_submenu", 
				  "navigation_ALCDs_HistoricalMonthlyCDs");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_CDs_Maturities() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_CDs_submenu", 
				  "navigation_ALCDs_Maturities");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToAL_CDs_MaturityRateChange() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_AL", "arrow_toNavigate_AL_CDs_submenu", 
				  "navigation_ALCDs_MaturityRateChange");
		//Thread.sleep(2000);
	}
	
	
	//Projections
	public static void navigateToProjections_CreateNewProjection() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Projections", 
				   "navigation_Projections_CreateNewProjections");
		//Thread.sleep(2000);
	}
	
	
	public static void navigateToProjections_ManageProjections() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Projections", 
				   "navigation_Projections_ManageProjections");
		//Thread.sleep(2000);
	}
	
	
    public static void navigateToProjections_Forecasting() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Projections", 
				   "navigation_Projections_Forecasting");
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(3000);
		EntitySelector.selectWindow("Forecasting");
		//Thread.sleep(5000);
		BaseUI.waitForElementToBeDisplayed("forecastingModalTitle", null, null, 60);
		StartPage.switchToIframe();
	}
	
	
	public static void navigateToProjections_UploadTemplate() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Projections", 
				   "navigation_Projections_UploadTemplate");	         
	}
	
	//Report Packages
	public static void navigateToReportPackages_RunReportPackage() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_ReportPackages", 
				"navigation_ReportPackages_RunReportPackage");
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("runReportPackage_ModalTitle", null, null, 60);
		StartPage.switchToIframe();
	}

	
	public static void navigateToReportPackages_SetUpReportPackages() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_ReportPackages", 
				"navigation_ReportPackages_SetUpReportPackages");
		BaseUI.waitForElementToBeDisplayed("setUpReportPackage_ModalTitle", null, null, 80);
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));	         
	}
	
	//Branch Performance Left Menu
	public static void navigateToBranchPerformance_ShowBranchPerformance() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_BranchPerformance", 
				"navigation_BranchPerformance_ShowBranchPerformance");
	}
	
	
    public static void navigateToBranchPerformance_CreateNewView() throws Exception {
    	StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_BranchPerformance", 
				"navigation_BranchPerformance_CreateNewView");
		//Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("branchPerf_CreateView_ModalTitle", null, null);
		BaseUI.switch_ToIframe(Locator.lookupElement("capRatios_Iframe"));
		BaseUI.waitForElementToBeDisplayed("branchPerf_CreateView_GeneralInfo_SubTitle", null, null);
	}
	
	
	public static void navigateToBranchPerformance_ManageExistingView() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_BranchPerformance", 
				"navigation_BranchPerformance_ManageExistingView");
		//Thread.sleep(2000);
	}
	
	
	//Tools > Custom Calculations left menu
	public static void navigateToTools_CustomCalculations() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Tools", "navigation_Tools_CustomCalculations");
		CustomCalculations.switchToCustomCalcFrame();
		BaseUI.waitForElementToBeDisplayed("customCalc_pageHeader", null, null, 60);
		BaseUI.waitForElementToBeDisplayed("customCalc_Search_Textbox", null, null);		
	}
	
	//Settings > My Dashboard left menu
	public static void navigateToSettings_MyDashboard() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Settings", "navigation_Settings_MyDashboard");
		//Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("myDashboardIndicator_ModalTitle", null, null);
    }
		
    //Settings > My Centers left menu
	public static void navigateToSettings_MyCenters() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Settings", "navigation_Settings_MyCenters");
	    //Thread.sleep(2000);  
		BaseUI.waitForElementToBeDisplayed("myCenters_ModalTitle", null, null);
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.waitForElementToBeDisplayed("myCenters_AvailableCentersText", null, null);
		BaseUI.waitForElementToBeDisplayed("myCenters_RemoveCentersText", null, null);
//		BaseUI.verifyElementHasExpectedText("myCenters_AvailableCentersText", "Available Centers");
//		BaseUI.verifyElementHasExpectedText("myCenters_RemoveCentersText", "My Centers");
	
		Settings_MyCentersModal.clickRemoveAllCentersBtn();
		Settings_MyCentersModal.dragAndDropCenters();
		draggedCenter1 = BaseUI.getTextFromField(Locator.lookupElement("myCenters_DragFromMyCentersBox_1"));
		draggedCenter2 = BaseUI.getTextFromField(Locator.lookupElement("myCenters_DragFromMyCentersBox_2"));
		Settings_MyCentersModal.clickMyCenters_FinishedBtn();
	}
	
	
	public static void navigateToAdmin_Users_ManageUsers() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_Admin", "arrow_toNavigate_Users_submenu", 
				  "navigation_AdminUsers_ManageUsers");
		//Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("manageUsers_ModalTitle", null, null, 60);
	}
	
	
//	public static void navigateToDashboardSummaryReportWithValidation() throws Exception {
//		EntitySelector.imageVerification("demoBankLogoOnReports");
//		BaseUI.verifyElementAppears(Locator.lookupElement("myDashboardReportTitle"));
//		BaseUI.verifyElementHasExpectedText("myDashboardReportTitle", "Summary");
//		Thread.sleep(200);
//		BaseUI.verifyElementHasExpectedText("summaryReport_profitCenterDisplayed", "Consolidated");
//	}
	
	
	public static void navigateToAdmin_UpdateMemoField() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu2_option("navigation_Admin", "arrow_toNavigate_ManageData_submenu", 
				"navigation_AdminManageData_UpdateMemoFields");
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.wait_ForElement_ToContainText("updtMemo_CapitalTab", 100, "Capital");
	}
	
	
	public static void navigateToTools_DocumentVault() throws Exception {
		StartPage.clickStartPageTab();
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Tools", "navigation_Tools_DocumentVault");
		Thread.sleep(1500);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.wait_ForElement_ToContainText("docVault_ModalTitle", 100, "Document Vault");
	}
	
	public static void navigateToSetting_MyAlerts() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Settings", "navigation_Settings_MyAlerts");
	    //Thread.sleep(2000);  
	    BaseUI.waitForElementToBeDisplayed("setting_MyAlerts_ModalTitle", null, null);
	}
	
	
	public static void navigateToSetting_MyCenters() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_Settings", "navigation_Settings_MyCenters");
	    //Thread.sleep(2000);  
	    BaseUI.waitForElementToBeDisplayed("myCenters_ModalTitle", null, null);
	}
	
	
	public static void navigate_to_signOut_WithoutAlert() throws Exception {
		StartPage.clickCloseAllTabsButton();
		StartPage.clickStartPageTab();
		navigate_to_userDropdown();
		BaseUI.click(Locator.lookupElement("startPage_Logout_option"));
		Thread.sleep(1000);
		//EntitySelector.selectWindow_CloseWindow("Banker's Dashboard from Deluxe - Management Software");
	}
	
	
	public static void navigateToLoanPricing_LoanPricingModel() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_LoanPricing", "navigation_LoanPricing_LoanPricingModel");  
	    BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_PageTitle", null, null, 1500);
	}
	
	
	public static void navigateToLoanPricing_LoanTypes() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_LoanPricing", "navigation_LoanPricing_LoanTypes");  
	    BaseUI.waitForElementToBeDisplayed("loanPricing_LoanTypes_ModalTitle", null, null);
	}
	
	
	public static void navigateToForecasting_Tools_SynchronizeHistorical() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("forecasting_ToolsMenuLink", "forecasting_SyncHistoricalProj_Toolsmenu_SyncHisSubmenu");  
	    BaseUI.waitForElementToBeDisplayed("forecasting_SyncHistoricalProj", null, null);
	}
	
	
	public static void navigateToLoanPricing_RiskWeightings() throws Exception {
		MouseHover.clickItemFromLeftMenu_subMenu1_option1("navigation_LoanPricing", "navigation_LoanPricing_RiskWeightings"); 
	    BaseUI.waitForElementToBeDisplayed("loanPricing_RiskWeightings_ModalTitle", null, null);
	}
}

