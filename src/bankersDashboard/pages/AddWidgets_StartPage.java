package bankersDashboard.pages;

import bankersDashboard.pageControls.DropDowns;
import utils.BaseUI;
import utils.Locator;

public class AddWidgets_StartPage {
	
	public static void addFedFundCurvesWidget() throws Exception {
		BaseUI.click(Locator.lookupElement("startPage_AddWidget_Icon"));
		Thread.sleep(1000);
		BaseUI.verifyElementAppears(Locator.lookupElement("addWidgetModalTitle"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("addWidgetModalTitle"), "My Dashboard Modules" );
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("outOfBox_radioBtn"));
		BaseUI.click(Locator.lookupElement("nextButton"));
		Thread.sleep(2000);
		BaseUI.verifyElementAppears(Locator.lookupElement("verifyChooseModulesToAdd_txt"));
		BaseUI.verifyElementHasExpectedText("verifyChooseModulesToAdd_txt", "Choose from these modules to add to your Dashboard:");
		Thread.sleep(1000);
		BaseUI.checkCheckbox(Locator.lookupElement("fedFundFuturesCurve_checkbox"));
		BaseUI.click(Locator.lookupElement("saveButton"));
		Thread.sleep(2000);
		BaseUI.switch_ToDefaultContent();
		BaseUI.verifyElementAppears(Locator.lookupElement("startPage_FedFundsFuturesCurve_widget"));
		System.out.println(BaseUI.elementExists("startPage_FedFundsFuturesCurve_widget", null, null));
	    System.out.println("Fed Fund Futures Curve is Displayed on Start Page");
	}
	
	
	public static void clickOutOfBoxModule_RadioBtn() throws Exception {
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("outOfBox_radioBtn"));
		Thread.sleep(100);
	}
	
	
	public static void clickOutOfBoxModule_NextBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("nextButton"));
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("verifyChooseModulesToAdd_txt", null, null);
	}
	
	
	public static void clickOutOfBoxModule_SaveBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("saveButton"));
		Thread.sleep(1000);
		BaseUI.waitForElementToNOTBeDisplayed("saveButton", null, null);
		BaseUI.switch_ToDefaultContent();
		BaseUI.wait_forElementToNotExist("addWidgetModalTitle", null, null);
	}
	
	
	public static void addSummaryWidget_OutOfBoxModule() throws Exception{
		addOutOfBoxModule_Widgets();
		BaseUI.checkCheckbox(Locator.lookupElement("addWidget_Summary_Checkbox"));
		clickOutOfBoxModule_SaveBtn();
	}
	
	
	public static void addOutOfBoxModule_Widgets() throws Exception {
		StartPage.clickStartPageTab();
		StartPage.clickAddWidgetIcon();
		clickOutOfBoxModule_RadioBtn();
		clickOutOfBoxModule_NextBtn();
	}
	
	
	public static void addAllWidgets_OutOfBoxModule() throws Exception{
		addOutOfBoxModule_Widgets();
		BaseUI.checkCheckbox(Locator.lookupElement("addWidget_Summary_Checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("fedFundsCurve_Checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("myfavreport_Checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("recentLoanLosses_Checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("recentLoanWins_Checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("expectedLoanClosings_checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("newRenewedLoandCD_Checkbox"));
		BaseUI.checkCheckbox(Locator.lookupElement("myMarginGraph_Checkbox"));
		clickOutOfBoxModule_SaveBtn();
	}
	
	public static void customIndicatorModule_clickNextButton() throws Exception {
		BaseUI.click_js(Locator.lookupElement("nextButton"));
		Thread.sleep(500);
	}
	
	
	public static void customIndicatorModule_clickSaveButton() throws Exception {
	    BaseUI.click_js(Locator.lookupElement("saveButton"));
	    Thread.sleep(1000);
	}

	
	public static String addCustomCalWidgetToStartPage(String customCalName) throws Exception {
		StartPage.clickStartPageTab();
		BaseUI.waitForElementToBeDisplayed("startPage_AddWidget_Icon", null, null);
		StartPage.clickAddWidgetIcon();
		BaseUI.switch_ToIframe(Locator.lookupElement("My_dashboard_frame"));
		BaseUI.click(Locator.lookupElement("customDashboardIndicator_radioBtn"));
		customIndicatorModule_clickNextButton();
		BaseUI.waitForElementToBeDisplayed("customCalcOption_label", null, null);
		BaseUI.click(Locator.lookupElement("customCalcOption_radioBtn"));
		customIndicatorModule_clickNextButton();
		BaseUI.waitForElementToBeDisplayed("customCalcIndicatorName_textbox", null, null);
		BaseUI.enterText(Locator.lookupElement("customCalcIndicatorName_textbox"), 
				("TestAddCustomCalcWidget" + BaseUI.random_NumberAsString(0, 5000)));
		String storeCustomCalcWidgetName = BaseUI
				.getTextFromInputBox(Locator.lookupElement("customCalcIndicatorName_textbox"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("customCalcPickCenter_dropdown"), "Accounting");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("customCalcName_dropdown"), customCalName);
		customIndicatorModule_clickNextButton();
		BaseUI.waitForElementToBeDisplayed("Verify_customCalcIndicatorPreviewText", null, null);
		customIndicatorModule_clickSaveButton();
		return storeCustomCalcWidgetName;	
	}
	
	
	public static String addBalanceIndicatorDetails() throws Exception {
		BaseUI.enterText(Locator.lookupElement("balancesIndicatorName_textbox"), 
				"TestAddBalancesWidgetStartPage" +BaseUI.random_NumberAsString(0, 1000));
		String storeBalancesWidgetName = BaseUI.getTextFromInputBox(Locator.lookupElement("balancesIndicatorName_textbox"));
		
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balancesTrackAgainst_textbox"), "Target");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balancesPickCenter_dropdown"), "Accounting");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balancesGroup_dropdown"), "100000000 - Cash and due from banks");
		StartPage.customIndicatorModule_clickNextButton();
		
		return storeBalancesWidgetName;
	}
	
	
	public static String addSpecialRatiosAndTotalsIndicatorDetails() throws Exception {
		BaseUI.enterText(Locator.lookupElement("specialRatiosIndicatorName_textbox"), 
				"TestSpecialRatiosWidget"+BaseUI.random_NumberAsString(0, 1000));
		String storeSpecialRatiosWidgetName = BaseUI.getTextFromInputBox(Locator.lookupElement("specialRatiosIndicatorName_textbox"));
		
		BaseUI.selectValueFromDropDown(Locator.lookupElement("specialRatiosTrackAgainst_textbox"), "Budget");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("specialRatiosPickCenter_dropdown"), "Accounting");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("specialRatiosRatio_dropdown"), "Loans / Deposits");
		StartPage.customIndicatorModule_clickNextButton();
		
		return storeSpecialRatiosWidgetName;		
	}
	
	
	public static String addSupplementalDataIndicatorDetails() throws Exception {
		BaseUI.enterText(Locator.lookupElement("supplementalDataIndicatorName_textbox"), 
				"TestSupplementalDataWidget"+BaseUI.random_NumberAsString(0, 1000));
		String storeSupplementalDataWidgetName = BaseUI.getTextFromInputBox(Locator.lookupElement("supplementalDataIndicatorName_textbox"));
		BaseUI.selectValueFromDropDown(Locator.lookupElement("supplementalDataTrackAgainst_textbox"), "Budget");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("supplementalDataPickCenter_dropdown"), "Accounting");
		BaseUI.selectValueFromDropDown(Locator.lookupElement("supplementalDataGroup_dropdown"), "999910103 - # Accts: Comml & ind");
		StartPage.customIndicatorModule_clickNextButton();
		
		return storeSupplementalDataWidgetName;
	}
	
	
	public static void clickRulerStyleRadioBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("rulerMeterStyle"));
		Thread.sleep(200);
	}
	
	
	public static void clickBetaMeterStyleRadioBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("betaMeterStyle"));
		Thread.sleep(200);
	}
	
	
	public static void clickSupplementalDataRadioBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("supplementalDataOption_radioBtn"));
		Thread.sleep(200);
	}
	
	
	public static void clickSpecialRatiosAndTotalsRadioBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("specialRatiosOption_radioBtn"));
		Thread.sleep(200);
	}
	
	
	public static void clickBalanceRadioBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("balancesOption_radioBtn"));
		Thread.sleep(200);
	}
	
	
	public static String addPeerDataIndicatorDetails() throws Exception {
		BaseUI.enterText(Locator.lookupElement("peerDataIndicatorName_textbox"), 
				"TestPeerDataWidgetStartPage"+BaseUI.random_NumberAsString(0, 1000));
		String storePeerDataWidgetName = BaseUI.getTextFromInputBox(Locator.lookupElement("peerDataIndicatorName_textbox"));
		
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("peerDataTrackAgainst_dropdown"), 2);
		DropDowns.selectFromDropDownByIndex(Locator.lookupElement("peerDataBenchmark_dropdown"), 2);
		StartPage.customIndicatorModule_clickNextButton();
		return storePeerDataWidgetName;
	}
	
	
	public static void clickPeerDataBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("peerDataOption_radioBtn"));
		Thread.sleep(200);
	}
}
