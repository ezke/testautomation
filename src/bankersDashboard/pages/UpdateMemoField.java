package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class UpdateMemoField {
	
	static String totalRiskAssets;
	static Object disallowedDeferresTax;
	static String otherBorrowingDues;
	static String internetCD;
	static String lessCoreCD;
	static String federalReserveReq;
	static String liabilitiesSecured;
	static String accumulatedOtherCom;
	static String lessDeferredTax;
	static String lessNetUnrealized;
	static String lessDeductionApplied;


	public static void update_UpdateMemoField(String dateOption) throws Exception {
		Navigation.navigateToAdmin_UpdateMemoField();
		BaseUI.verifyElementAppears(Locator.lookupElement("updtMemo_Header"));
		
		//Select July 2016 from date dropdown option
		selectDateFromDropdown(dateOption);
		
		//Click Capital tab 
		clickCapitalTab();
		
		//Enter values for these 2 options under Capital tab
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_CapTab_TotRisk_AssetsTxtbox"), "223610862");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_CapTab_DisallowedDefTax_AssetsTxtbox"), "4294785");
		
		//Click Regulatory tab 
		clickRegulatoryTab();
				
		//Enter values for these 2 options under Regulatory tab
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_RegulTab_OtherBorrowing_DuesTxtbox"), "7000000");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_RegulTab_InternetCD_Txtbox"), "53327000");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_RegulTab_LessCoreCD_Txtbox"), "817800");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_RegulTab_FederalReserveReq_Txtbox"), "1027000");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_RegulTab_LiabilitiesSecured_Txtbox"), "3488468");
		
		//Click Basel III tab 
		clickBaselTab();
						
		//Enter values for these 2 options under Basel III tab
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_BaselTab_AccumulatedOtherComP_Txtbox"), "43386");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_BaselTab_LessDeferredTax_Txtbox"), "2576871");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_BaselTab_LessNetUnrealized_Txtbox"), "43386");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_BaselTab_LessDeductionApplied_Txtbox"), "1717914");
		
		clickSaveButton();
	}
	
	
	public static void selectDateFromDropdown(String dateOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("updtMemo_DateDropdown"), dateOption);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}

	
	public static void editUpdateMemoField_CapitalValues(String dateOption) throws Exception {
		Navigation.navigateToAdmin_UpdateMemoField();
		BaseUI.verifyElementAppears(Locator.lookupElement("updtMemo_Header"));
		
		//Select July 2016 from date dropdown option
		selectDateFromDropdown(dateOption);
		
		//Click Capital tab 
		clickCapitalTab();
		
		//Enter values for these 2 options under Capital tab
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_CapTab_TotRisk_AssetsTxtbox"), "253610862");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("updtMemo_CapTab_DisallowedDefTax_AssetsTxtbox"), "5294785");
		
		clickSaveButton();
	}
	
	
	public static void clickSaveButton() throws Exception {
		BaseUI.click(Locator.lookupElement("updtMemo_SaveBtn"));
		Thread.sleep(1500);
	}
	
	
	public static void getValues_UpdateMemoField(String dateOption) throws Exception {
		Navigation.navigateToAdmin_UpdateMemoField();
		BaseUI.verifyElementAppears(Locator.lookupElement("updtMemo_Header"));
		
		//Select July 2016 from date dropdown option
		selectDateFromDropdown(dateOption);
		
		//Click Capital tab 
		clickCapitalTab();
		
		//Enter values for these 2 options under Capital tab
		totalRiskAssets = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_CapTab_TotRisk_AssetsTxtbox"));
		disallowedDeferresTax = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_CapTab_DisallowedDefTax_AssetsTxtbox"));
		
		//Click Regulatory tab 
		clickRegulatoryTab();
				
		//Enter values for these 2 options under Regulatory tab
		otherBorrowingDues = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_RegulTab_OtherBorrowing_DuesTxtbox"));
		internetCD = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_RegulTab_InternetCD_Txtbox"));
		lessCoreCD = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_RegulTab_LessCoreCD_Txtbox"));
		federalReserveReq = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_RegulTab_FederalReserveReq_Txtbox"));
		liabilitiesSecured = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_RegulTab_LiabilitiesSecured_Txtbox"));
		
		//Click Basel III tab 
		clickBaselTab();
						
		//Enter values for these 2 options under Basel III tab
		accumulatedOtherCom = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_AccumulatedOtherComP_Txtbox"));
		lessDeferredTax = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_LessDeferredTax_Txtbox"));
		lessNetUnrealized = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_LessNetUnrealized_Txtbox"));
		lessDeductionApplied = BaseUI.getTextFromInputBox(Locator.lookupElement("updtMemo_BaselTab_LessDeductionApplied_Txtbox"));
		
		clickSaveButton();
	}
	
	
	public static void resetUpdateMemoFieldValues(String dateOption) throws Exception {
		StartPage.clickStartPageTab();
		StartPage.clickCloseAllTabsButton();
		UpdateMemoField.update_UpdateMemoField(dateOption);		
	}
	
	
	public static void clickCapitalTab() throws Exception {
		BaseUI.click(Locator.lookupElement("updtMemo_CapitalTab"));
		Thread.sleep(100);
	}
	
	
	public static void clickRegulatoryTab() throws Exception {
		BaseUI.click(Locator.lookupElement("updtMemo_RegulatoryTab"));
		Thread.sleep(100);
	}
	
	
	public static void clickBaselTab() throws Exception {
		BaseUI.click(Locator.lookupElement("updtMemo_BaselTab"));
		Thread.sleep(100);
	}
}
