package bankersDashboard.pages;

import utils.BaseUI;
import utils.Locator;

public class SetUpReportPackage {
	
	public static void deleteReportPackage() throws Exception {
		BaseUI.switch_ToDefaultContent();
		Navigation.navigateToReportPackages_SetUpReportPackages();
		ReportPackages.selectFromCurrentPackage(ReportPackages.storePackageName);
		ReportPackages.clickDeleteReportPackageIcon();
		ReportPackages.clickFinishedButton();
		BaseUI.switch_ToDefaultContent();
	}
	
	
	public static void addMultipleReports_ReportPackage() throws Exception {
		ReportPackages.selectReportFromList("Monthly Dashboard");
		ReportPackages.clickSaveButton();
		ReportPackages.selectReportFromList("Capital Requirements");
		ReportPackages.clickSaveButton();
		//ReportPackages.clickFinishedButton();
	}
	
    
	public static void editReportPackage(String packageName) throws Exception {
		ReportPackages.clickEditReportPackageIcon();
		String reportPackageName = BaseUI.getTextFromField(Locator.lookupElement("setUpReportPackage_PackageName_Txtbox"));
		System.out.println(reportPackageName);
		ReportPackages.enterPackageName(packageName);
		ReportPackages.enterPackageName(ReportPackages.storePackageName);
		ReportPackages.clickBoardPackage("no");
	}
	
	public static String editReport(String displayedReportName) throws Exception {
		ReportPackages.clickEditReportIcon();
		//reportName = BaseUI.getTextFromField(Locator.lookupElement("MonthlyDashboardReportName"));
		String reportNameDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("setUpReportPackage_MonthlyDashboardReportName"));
		
		System.out.println(reportNameDisplayed);
	    BaseUI.enterText(Locator.lookupElement("setUpReportPackage_MonthlyDashboardReportName"), displayedReportName);
	    
	    return reportNameDisplayed;
	}
	
	
	public static void addExecSummaryReport(int pcIndex, int primaryDataIndex, 
			int compateToIndex, String period) throws Exception {
		ReportPackages.selectReportFromList("Executive Summary");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_ExecSummary_ReportName", null, null);
		selectExecSummary_ProfitCenter(pcIndex);
		selectExecSummary_DataSrc_PrimaryData(primaryDataIndex);
		selectExecSummary_DataSrc_CompareTo(compateToIndex);
		selectExecSummary_Period(period);
		clickRep_SaveButton();
	}
	
	
	public static void selectExecSummary_DataSrc_PrimaryData(int index) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("setUp_RepPackage_ExecSummary_DataSource_PrimaryDropdown"), 
				index);
		Thread.sleep(200);
	}
	
	
	public static void selectExecSummary_ProfitCenter(int index) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("setUp_RepPackage_ExecSummary_ProfitCenterDropdown"), 
				index);
		Thread.sleep(200);
	}
	
	
	public static void selectExecSummary_DataSrc_CompareTo(int index) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("setUp_RepPackage_ExecSummary_DataSource_ComPareToDropdown"), 
				index);
		Thread.sleep(200);
	}
	
	
	public static void select_DataSrc_PrimaryData(String primDataSrc) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_ExecSummary_DataSource_PrimaryDropdown"), 
				primDataSrc);
		Thread.sleep(200);
	}
	
	
	public static void select_DataSrc_CompareTo(String compToDataSrc) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_ExecSummary_DataSource_ComPareToDropdown"), 
				compToDataSrc);
		Thread.sleep(200);
	}
	
	
	public static void selectExecSummary_Period(String period) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_ExecSummary_PeriodDropdown"), period);
		Thread.sleep(200);
	}
	
	
	public static void clickRep_SaveButton() throws Exception {
		BaseUI.click(Locator.lookupElement("setUp_RepPackage_ExecSummary_SaveBtn"));
		Thread.sleep(1000);
	}
	
	
	public static void clickExecSumm12Mons_RevChronoOrderCheckbox() throws Exception {
		BaseUI.click(Locator.lookupElement("setUp_RepPackage_ExecSummary12Mons_RevChronoOrderCheckbox"));
		Thread.sleep(1000);
	}

	
	public static void addExecSummary12MonthsReport(int pcIndex, int primaryDataIndex, 
			 String period) throws Exception {
		ReportPackages.selectReportFromList("Executive Summary (12mos.)");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_ExecSummary12Mons_ReportName", null, null);
		selectExecSummary_ProfitCenter(pcIndex);
		selectExecSummary_DataSrc_PrimaryData(primaryDataIndex);
		selectExecSummary_Period(period);
		clickExecSumm12Mons_RevChronoOrderCheckbox();
		clickRep_SaveButton();
	}
	
	
	public static void addRatioAnalysisReport(int primaryDataIndex, int compateToIndex) throws Exception {
		ReportPackages.selectReportFromList("Ratio Analysis");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_RatioAnalysis_ReportName", null, null);
		selectExecSummary_DataSrc_PrimaryData(primaryDataIndex);
		selectExecSummary_DataSrc_CompareTo(compateToIndex);
		clickExecSumm12Mons_RevChronoOrderCheckbox();
		clickRep_SaveButton();
	}
	
	
	public static void addBalanceSheetReport(String viewOption, String primaryData, String rangeOption, 
			int profCenterOption, String avgOption) throws Exception {
		ReportPackages.selectReportFromList("Balance Sheet");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_BS_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		selectBalSheet_RangeOption(rangeOption);
		selectBalSheet_ProfitCenter(profCenterOption);
		selectBalSheet_DataSrc_PrimaryData(primaryData);
		selectBalSheet_AvgOption(avgOption);
		clickBalSheet_RevChronoOrderCheckbox();
		verifyBalSheet_SuppressZeroCheckbox_Checked();
		clickRep_SaveButton();
	}
	
	
	public static void selectBalSheet_ViewOption(String viewOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BS_ViewDropdown"), viewOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBalSheet_RangeOption(String rangeOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BS_RangeDropdown"), rangeOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBalSheet_ProfitCenter(int index) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("setUp_RepPackage_BS_PCDropdown"), 
				index);
		Thread.sleep(200);
	}
	
	
	public static void selectBalSheet_DataSrc_PrimaryData(String dataSourceOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BS_PrimDataSrc_Dropdown"), 
				dataSourceOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBalSheet_AvgOption(String avgOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BS_AvgDropdown"), 
				avgOption);
		Thread.sleep(200);
	}
	
	
	public static void clickBalSheet_RevChronoOrderCheckbox() throws Exception {
		BaseUI.click(Locator.lookupElement("setUp_RepPackage_BS_RevChronoCheckbox"));
		Thread.sleep(1000);
	}
	
	
	public static void verifyBalSheet_SuppressZeroCheckbox_Checked() {
		BaseUI.verifyCheckboxStatus("setUp_RepPackage_BS_SuppressZeroCheckbox", "checked");
	}
	
	public static void addReportsToRepPackages(String reportName, String reportNameDisplayed, 
			int profCenterOption) throws Exception {
		ReportPackages.selectReportFromList(reportName);
		BaseUI.waitForElementToBeDisplayed(reportNameDisplayed, null, null);
		selectBalSheet_ProfitCenter(profCenterOption);
		clickRep_SaveButton();
	}
	
	
	public static void addEarningAssetsMixReport(String reportName, String reportNameDisplayed, 
			int profCenterOption) throws Exception {
		addReportsToRepPackages(reportName, reportNameDisplayed, profCenterOption);
	}
	
	
	public static void addFundingMixReport(String reportName, String reportNameDisplayed, 
			int profCenterOption) throws Exception {
		addReportsToRepPackages(reportName, reportNameDisplayed, profCenterOption);
	}
	
	
	public static void addNonEarningAssetsReport(String reportName, String reportNameDisplayed, 
			int profCenterOption) throws Exception {
		addReportsToRepPackages(reportName, reportNameDisplayed, profCenterOption);
	}
	
	
	public static void addLoanConcentrationReport(int profCenterOption, String primDataSrcOption, String compToDataSrcOption,
			String periodOption) throws Exception {
		ReportPackages.selectReportFromList("Loan Concentration");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_LoanConcentration_ReportName", null, null);
		selectBalSheet_ProfitCenter(profCenterOption);
		select_DataSrc_PrimaryData(primDataSrcOption);
		select_DataSrc_CompareTo(compToDataSrcOption);
		selectExecSummary_Period(periodOption);
		clickRep_SaveButton();
	}
	
	
	public static void addIncomeStatementReport(String viewOption, String primaryData, String rangeOption, 
			int profCenterOption, String avgOption) throws Exception {
		ReportPackages.selectReportFromList("Income Statement");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_IS_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		selectBalSheet_RangeOption(rangeOption);
		selectBalSheet_ProfitCenter(profCenterOption);
		selectBalSheet_DataSrc_PrimaryData(primaryData);
		selectBalSheet_AvgOption(avgOption);
		clickBalSheet_RevChronoOrderCheckbox();
		verifyBalSheet_SuppressZeroCheckbox_Checked();
		clickRep_SaveButton();
	}
	
	
	public static void addMarginAnalysisReport(String rangeOption, int profCenterOption, 
			String primaryData) throws Exception {
		ReportPackages.selectReportFromList("Margin Analysis");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_MarginAnaly_ReportName", null, null);
		selectBalSheet_RangeOption(rangeOption);
		selectBalSheet_ProfitCenter(profCenterOption);
		selectBalSheet_DataSrc_PrimaryData(primaryData);
		clickBalSheet_RevChronoOrderCheckbox();
		clickRep_SaveButton();
	}
	
	
	public static void selectMarginAnalyComp_PrimDataPeriod(String primperiod) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_MarginAnalyCompare_PrimDataPeriodDropdown"), 
				primperiod);
		Thread.sleep(200);
	}
	
	
	public static void selectMarginAnalyComp_CompToPeriod(String compToperiod) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_MarginAnalyCompare_CompToPeriodDropdown"), 
				compToperiod);
		Thread.sleep(200);
	}
	
	
	public static void addMarginAnalyCompareReport(String viewOption, int profCenterOption, String primDataSrcOption, 
			String compToDataSrcOption, String primPeriodOption, String compToPeriodOption) throws Exception {
		ReportPackages.selectReportFromList("Margin Analysis Compare");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_MarginAnalyCompare_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		selectBalSheet_ProfitCenter(profCenterOption);
		select_DataSrc_PrimaryData(primDataSrcOption);
		select_DataSrc_CompareTo(compToDataSrcOption);
		selectMarginAnalyComp_PrimDataPeriod(primPeriodOption);
		selectMarginAnalyComp_CompToPeriod(compToPeriodOption);
		clickRep_SaveButton();
	}
	
	
	public static void addMarginGraphReport(String viewOption, int profCenterOption, 
			String primDataSrcOption) throws Exception {
		ReportPackages.selectReportFromList("Margin Graph");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_MarginGraph_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		selectBalSheet_ProfitCenter(profCenterOption);
		selectBalSheet_DataSrc_PrimaryData(primDataSrcOption);
		clickRep_SaveButton();
	}
	
	
	public static void addMarginCompoGraphReport(int profCenterOption) throws Exception {
		ReportPackages.selectReportFromList("Margin Components Graph");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_MarginCompoGraph_ReportName", null, null);
		selectBalSheet_ProfitCenter(profCenterOption);
		clickRep_SaveButton();
	}
	
	
	public static void selectPeriod(String periodOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_CapitalReq_PeriodDropdown"), 
				periodOption);
		Thread.sleep(200);
	}
	
	
	public static void addMarginCompoGraphReport(String periodOption) throws Exception {
		ReportPackages.selectReportFromList("Capital Requirements");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_CapitalReq_ReportName", null, null);
		selectPeriod(periodOption);
		clickRep_SaveButton();
	}
	
	
	public static void addLiquidityReport(String viewOption, String periodOption) throws Exception {
		ReportPackages.selectReportFromList("Liquidity Report");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_LiquidityRep_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		selectPeriod(periodOption);
		clickRep_SaveButton();
	}
	
	
	public static void addLoanLossAdequacyAllowanceReport(int profCenterOption, String periodOption) throws Exception {
		ReportPackages.selectReportFromList("Loan Loss Adequacy Allowance");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_LoanLossAdequacyAllowance_ReportName", null, null);
		selectBalSheet_ProfitCenter(profCenterOption);
		selectPeriod(periodOption);
		clickRep_SaveButton();
	}
	
	
	public static void addTexasRatioReport(String viewOption, String primDataSrcOption) throws Exception {
		ReportPackages.selectReportFromList("Texas Ratio");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_TexasRatio_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		selectBalSheet_DataSrc_PrimaryData(primDataSrcOption);
		clickRep_SaveButton();
	}
	
	
	public static void addHistoricalCapitalRatiosReport(String viewOption, String primDataSrcOption) throws Exception {
		ReportPackages.selectReportFromList("Historical Capital Ratios");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_HisCapRatios_ReportName", null, null);
		selectBalSheet_ViewOption(viewOption);
		clickRep_SaveButton();
	}
	
	
	public static void selectBSCompare_ReportView(String reportViewOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_ReportViewDropdown"), 
				reportViewOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBSCompare_ProfitCenter(int profitCenterOption) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("setUp_RepPackage_BSCompare_ProfitCenterDropdown"), 
				profitCenterOption);
		Thread.sleep(200);
	}
	
	
	public static void verifyBSCompare_SuppressZeroCheckbox_Checked() {
		BaseUI.verifyCheckboxStatus("setUp_RepPackage_BSCompare_SuppressZeroCheckbox", "checked");
	}
	
	
	public static void selectBSCompare_PrimSource(String primSourceOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_PrimarySourceDropdown"), 
				primSourceOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBSCompare_PrimAvg(String primAvgOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_PrimaryAvgDropdown"), 
				primAvgOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBSCompare_Col2Source(String col2SourceOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_Col2SourceDropdown"), 
				col2SourceOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBSCompare_Col2Avg(String col2AvgOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_Col2AvgDropdown"), 
				col2AvgOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBSCompare_Col2PeriodOrDate(String col2DateOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_Col2PeriodDropdown"), 
				col2DateOption);
		Thread.sleep(200);
	}
	
	
	public static void selectBSCompare_Col2CompTo(String col2CompToOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("setUp_RepPackage_BSCompare_Col2CompareToDropdown"), 
				col2CompToOption);
		Thread.sleep(200);
	}
	
	
	public static void clickBSCompare_Col2ShowPercentCheckbx() throws Exception {
		BaseUI.click(Locator.lookupElement("setUp_RepPackage_BSCompare_Col2_ShowPercentCheckbox"));
		Thread.sleep(200);
	}
	
	
	public static void clickBSCompare_Col2ShowDollarCheckbx() throws Exception {
		BaseUI.click(Locator.lookupElement("setUp_RepPackage_BSCompare_Col2_ShowDollarCheckbox"));
		Thread.sleep(200);
	}
	
	
	public static void selectOption_ForBS_IS_NII_NIECompareReports(String viewOption, String col2CompToOption, 
			String col2DateOption, String col2AvgOption, String col2SourceOption, String primAvgOption, 
			String primSourceOption, int profCenterOption) throws Exception {
		selectBSCompare_ReportView(viewOption);
		selectBSCompare_ProfitCenter(profCenterOption);
		selectBSCompare_PrimSource(primSourceOption);
		selectBSCompare_PrimAvg(primAvgOption);
		selectBSCompare_Col2Source(col2SourceOption);
		selectBSCompare_Col2Avg(col2AvgOption);
		selectBSCompare_Col2PeriodOrDate(col2DateOption);
		selectBSCompare_Col2CompTo(col2CompToOption);
		clickBSCompare_Col2ShowDollarCheckbx();
		clickBSCompare_Col2ShowDollarCheckbx();
	}
	
	
	public static void addBSCompareReport(String viewOption, String col2CompToOption, 
			String col2DateOption, String col2AvgOption, String col2SourceOption, String primAvgOption, 
			String primSourceOption, int profCenterOption) throws Exception {
		ReportPackages.selectReportFromList("Balance Sheet Compare");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_BSCompare_ReportName", null, null);
		selectOption_ForBS_IS_NII_NIECompareReports(viewOption, col2CompToOption, 
				col2DateOption, col2AvgOption, col2SourceOption, primAvgOption, primSourceOption, profCenterOption);
		clickRep_SaveButton();
	}
	
	
	public static void addISCompareReport(String viewOption, String col2CompToOption, 
			String col2DateOption, String col2AvgOption, String col2SourceOption, String primAvgOption, 
			String primSourceOption, int profCenterOption) throws Exception {
		ReportPackages.selectReportFromList("Income Statement Compare");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_ISCompare_ReportName", null, null);
		selectOption_ForBS_IS_NII_NIECompareReports(viewOption, col2CompToOption, 
				col2DateOption, col2AvgOption, col2SourceOption, primAvgOption, primSourceOption, profCenterOption);
		clickRep_SaveButton();
	}
	
	
	public static void addNonInterestIncomeReport(String viewOption, String col2CompToOption, 
			String col2DateOption, String col2AvgOption, String col2SourceOption, String primAvgOption, 
			String primSourceOption, int profCenterOption) throws Exception {
		ReportPackages.selectReportFromList("Non Interest Income");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_NonInterestIncome_ReportName", null, null);
		selectOption_ForBS_IS_NII_NIECompareReports(viewOption, col2CompToOption, 
				col2DateOption, col2AvgOption, col2SourceOption, primAvgOption, primSourceOption, profCenterOption);
		clickRep_SaveButton();
	}
	
	
	public static void addNonInterestExpenseReport(String viewOption, String col2CompToOption, 
			String col2DateOption, String col2AvgOption, String col2SourceOption, String primAvgOption, 
			String primSourceOption, int profCenterOption) throws Exception {
		ReportPackages.selectReportFromList("Non Interest Expense");
		BaseUI.waitForElementToBeDisplayed("setUp_RepPackage_NonInterestExpense_ReportName", null, null);
		selectOption_ForBS_IS_NII_NIECompareReports(viewOption, col2CompToOption, 
				col2DateOption, col2AvgOption, col2SourceOption, primAvgOption, primSourceOption, profCenterOption);
		clickRep_SaveButton();
	}
	
	
	public static void addAllReportsToReportPackage() throws Exception {
		addMultipleReports_ReportPackage();
		addExecSummary12MonthsReport(2, 1, "YTD");
		addExecSummaryReport(2,1,1, "QTD");
		addRatioAnalysisReport(1, 2);
	    addBalanceSheetReport("Detailed", "Actual", "Calendar", 3, "MTD");
	    addReportsToRepPackages("Earning Assets Mix", "setUp_RepPackage_EarningAssetsMix_ReportName", 3);
	    addReportsToRepPackages("Non Earning Assets", "setUp_RepPackage_NonEarningAssets_ReportName", 4);
	    addReportsToRepPackages("Funding Mix", "setUp_RepPackage_FundingMix_ReportName", 5);
		addLoanConcentrationReport(6, "Actual", "Forecast", "Same Period");
	    addMarginAnalyCompareReport("Detailed", 1, "Actual", "Budget", "QTD", "Same Period");
        addMarginAnalysisReport("Calendar", 5, "Actual");
	//  SetUpReportPackage.addIncomeStatementReport(viewOption, primaryData, rangeOption, profCenterOption, avgOption);
		addMarginGraphReport("6 Months", 5, "Actual");
	    addMarginCompoGraphReport(3);
	    addMarginCompoGraphReport("Prior Period");
		addLiquidityReport("Detailed", "Prior Year");
//	    addBSCompareReport("Detailed", "Primary", "Prior Period", "MTD", "Budget", "MTD", 
//		"Actual", 3);
//	    addISCompareReport("Detailed", "Primary", "Prior Period", "MTD", "Budget", "MTD", 
//		"Actual", 3);
	    ReportPackages.clickFinishedButton();
	}
}
