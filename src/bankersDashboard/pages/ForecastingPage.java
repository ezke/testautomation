package bankersDashboard.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Locator;

public class ForecastingPage {
	
	private static String primProfCenterSelected;
	private static String cashBalAccOptionSelected;
	private static String retEarningAccOptionSelected;
	private static String begOfYearBalanceAcctSelected;
	private static String bsProvAcctSelected;
	private static String isProvAcctSelected;
	private static String syncThruDateSelected;


	public static void clickCreateNewProject() throws Exception {
		BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_CreateNewProjectBtn"));
		Thread.sleep(3000);
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("forecastingModalTitle"));
		Thread.sleep(200);
//		BaseUI.waitForElementToBeDisplayed("forecastingModalTitle", null, null);
		StartPage.switchToIframe();
		BaseUI.wait_forElementToNotExist("forecasting_CreateNewFC_CreateNewProjectBtn", null, null);
	    BaseUI.waitForElementToBeDisplayed("forecasting_CreateNewFC_StartLabel", null, null, 60);
	}
	
	
	public static void enterForecastingProjectName(String fcName) throws Exception {
		//BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ProjNameTxtbox"));
		BaseUI.switch_ToDefaultContent();
		StartPage.switchToIframe();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("forecasting_CreateNewFC_ProjNameTxtbox"), 
				fcName);
	}
	
	
	public static void selectCurrentFCProject(String fcProjectName) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("forecasting_CreateNewFC_CurrentProjectDropdown"), 
				fcProjectName);
	}
	
	
	public static void clickNextIcon() throws Exception {
		BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_NextIcon"));
		Thread.sleep(3000);
		//BaseUI.waitForElementToBeDisplayed("forecasting_CreateNewFC_NextPageLabel", null, null);
	}
	
	
    public static void clickByCenter_TotalRadioButton(String type_radio) throws InterruptedException {		
		if(type_radio.equals("Total Bank")) {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_GeneralPage_TotalBankRadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_GeneralPage_ByCenterRadioBtn"));
			Thread.sleep(100);
		}	
	}

    
    public static void selectGeneralPage_PrimaryProfitCenter(int profCenter) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_GeneralPage_PrimProfitCenter"), 
				profCenter);
		Thread.sleep(300);
	}
    
    
    public static void selectCashPage_CashBalAccDropdown(int profCenter) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_CashPage_CashBalAccDropdown"), 
				profCenter);
		Thread.sleep(300);
	}
    
    
    public static void selectCashPage_RetainedEarningAcc(int profCenter) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_CashPage_RetainedEarningAccDropdown"), 
				profCenter);
		Thread.sleep(300);
	}
    
    
    public static void selectLoanLossAllowancePage_BegOfYearBalanceAcct(int BegOfYearBalanceAcct) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_LoanLossAllowancePage_BegOfYearBalanceAcctDropdown"), 
				BegOfYearBalanceAcct);
		Thread.sleep(300);
	}
    
    
    public static void selectLoanLossAllowancePage_BalanceSheetProvisionAcct(int bsProvAcct) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_LoanLossAllowancePage_BalanceSheetProvisionAcctDropdown"), 
				bsProvAcct);
		Thread.sleep(300);
	}
    
    
    public static void selectLoanLossAllowancePage_IncomeStatementProvisionAcct(int isProvAcct) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_LoanLossAllowancePage_IncomeStatementProvisionAcctDropdown"), 
				isProvAcct);
		Thread.sleep(300);
	}
    
    
    public static void selectSyncHisDataPage_SyncThruDate(int SyncThruDate) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecasting_CreateNewFC_SyncHisDataPage_SyncThruDateDropdown"), 
				SyncThruDate);
		Thread.sleep(300);
	}
    
    
    public static void addNewFCProjectDetails(String fcName, String bankOrCenterOption, int primProfCenter, int cashBalAccOption,
    		int retEarningAccOption, int begOfYearBalanceAcct, int isProvAcct, int bsProvAcct, int syncThruDate) throws Exception {
    	Navigation.navigateToProjections_Forecasting();
    	if(BaseUI.elementAppears(Locator.lookupElement("forecasting_AnotherProjectCreationMsg"))) {
    		BaseUI.log_AndFail("Another Forecasting project is in creation and only one project can be created at a time");
    	} else {
    		clickCreateNewProject();
        	//StartPage.switchToIframe();
        	enterForecastingProjectName(fcName);
        	clickNextIcon();
        	clickByCenter_TotalRadioButton(bankOrCenterOption);
        	selectGeneralPage_PrimaryProfitCenter(primProfCenter);
        	primProfCenterSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_GeneralPage_PrimProfitCenter");
        	clickNextIcon();
        	selectCashPage_CashBalAccDropdown(cashBalAccOption);
        	cashBalAccOptionSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_CashPage_CashBalAccDropdown");
        	selectCashPage_RetainedEarningAcc(retEarningAccOption);
        	retEarningAccOptionSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_CashPage_RetainedEarningAccDropdown");
        	clickNextIcon();
        	selectLoanLossAllowancePage_BegOfYearBalanceAcct(begOfYearBalanceAcct);
        	begOfYearBalanceAcctSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_LoanLossAllowancePage_BegOfYearBalanceAcctDropdown");
        	selectLoanLossAllowancePage_BalanceSheetProvisionAcct(bsProvAcct);
        	bsProvAcctSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_LoanLossAllowancePage_BalanceSheetProvisionAcctDropdown");
        	selectLoanLossAllowancePage_IncomeStatementProvisionAcct(isProvAcct);
        	isProvAcctSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_LoanLossAllowancePage_IncomeStatementProvisionAcctDropdown");
        	clickNextIcon();
        	selectSyncHisDataPage_SyncThruDate(syncThruDate);
        	syncThruDateSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_SyncHisDataPage_SyncThruDateDropdown");
        	clickNextIcon();
    	}   	
    }
    
    
    public static void verifyNewCreatedForecastingProject_PrimaryProfitCenter() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_PrimProfitCenter"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_PrimProfitCenter", primProfCenterSelected);
    }
    
    
    public static void verifyNewCreatedForecastingProject_CashBalAcc() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_CashAccount"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_CashAccount", cashBalAccOptionSelected);
    }
    
    
    public static void verifyNewCreatedForecastingProject_RetainedEarningAcc() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_RetainedEarningsAcct"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_RetainedEarningsAcct", retEarningAccOptionSelected);
    }
    
    
    public static void verifyNewCreatedForecastingProject_BegOfYearBalanceAcct() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_ALLBeginningOfYearBalance"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_ALLBeginningOfYearBalance", 
    			begOfYearBalanceAcctSelected);
    }
    
    
    public static void verifyNewCreatedForecastingProject_BSProvisionAcct() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_ALLBSAccount"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_ALLBSAccount", bsProvAcctSelected);
    }
    
    
    public static void verifyNewCreatedForecastingProject_ISProvisionAcct() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_ALLISAccount"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_ALLISAccount", isProvAcctSelected);
    }
    
    
    public static void verifyNewCreatedForecastingProject_SyncThruDate() {
    	BaseUI.verifyElementAppears(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_SyncThruDate"));
    	BaseUI.verifyElementHasExpectedText("forecasting_CreateNewFC_ConfirmInfo_SyncThruDate", syncThruDateSelected);
    }
    
    public static void clickOpenForecastingProjectBtn() throws Exception {
    	BaseUI.click_js(Locator.lookupElement("forecasting_OpenProject_btn"));
		Thread.sleep(5000);
		BaseUI.switch_ToDefaultContent();
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("forecasting_BSOnOpenForecastingPage", null, null,120);
    }
    
    
    public static void clickToolsMenuOptions(String menuOption) throws Exception {
        List<WebElement> toolsMenuOptions = Locator.lookup_multipleElements("forecasting_ToolsMenuOptions", null, null);		
        
        BaseUI.click_js(Locator.lookupElement("forecasting_ToolsMenuLink"));
		Thread.sleep(500);
		for (WebElement toolsOption : toolsMenuOptions ) {
			System.out.println(BaseUI.getTextFromField(toolsOption));
			if(BaseUI.getTextFromField(toolsOption).contains(menuOption)) {
				BaseUI.click_js(toolsOption);
				Thread.sleep(3000);
				BaseUI.wait_forPageToFinishLoading();
//				BaseUI.waitForElementToBeDisplayed("forecasting_ToolsOptionModal", null, null);
				break;
			}
		}
	}
    
    
    public static void selectExistingFC_SelectToolsOption(String fcName, String toolsOption) throws Exception {
    	StartPage.clickStartPageTab();
    	Navigation.navigateToProjections_Forecasting();
    	
    	if(BaseUI.elementAppears(Locator.lookupElement("forecasting_AnotherProjectCreationMsg"))) {
    		BaseUI.log_AndFail("Another Forecasting project is in creation and only one project can be created at a time");
    	} else {
    		BaseUI.selectValueFromDropDown(Locator.lookupElement("forecastingProjects_dropdown"), 
    				fcName);
    		Thread.sleep(200);
    		ForecastingPage.clickOpenForecastingProjectBtn();
    		ForecastingPage.clickToolsMenuOptions(toolsOption);
    	}		
    }
    
    
    public static void clickSaveButton() throws Exception {
    	BaseUI.click_js(Locator.lookupElement("forecasting_CreateNewFC_ConfirmInfo_saveButton"));
		Thread.sleep(2000);
		BaseUI.waitForElementToNOTBeDisplayed("forecasting_CreateNewFC_ConfirmInfo_saveButton", null, "60");
		BaseUI.switch_ToDefaultContent();
		//BaseUI.waitForElementToBeDisplayed("forecasting_CreateNewFC_SmartFillPage_NextButton", null, null);
    }
    
    
    public static void clickSmartFillNextButton() throws Exception {
    	WebElement smartFillNextBtn = Locator.lookupElement("forecasting_CreateNewFC_SmartFillPage_NextButton");
    	//BaseUI.scroll_to_element(smartFillNextBtn);
    	BaseUI.click_js(smartFillNextBtn);
		Thread.sleep(3000);
    }
    
    
    public static void clickCopyToBudgetModal_CopyBtn() throws Exception {
    	BaseUI.click(Locator.lookupElement("forecasting_CopyToBudget_CopyBtn"));
		Thread.sleep(3000);
		BaseUI.switch_ToDefaultContent();
		StartPage.switchToIframe();
    }
    
    public static void switchToIframe_ProjectPermission() throws Exception{
    	BaseUI.switch_ToIframe(Locator.lookupElement("forecasting_ProjectPermissions_Iframe"));
    	Thread.sleep(500);
    }
    
    public static void click_ProjectPermission_User_ByName(String name) throws Exception{
    	ArrayList<WebElement> userLists = Locator.lookup_multipleElements("forecasting_ProjectPermissions_Users", null, null);
    	
    	BaseUI.verify_true_AndLog(userLists.size()>0, "Found Users", "Did Not Find Users");
    	for (Integer i = 0; i < userLists.size(); i++) {
    		if(BaseUI.getTextFromField(userLists.get(i)).contains(name)){
    			BaseUI.click(userLists.get(i));
    			BaseUI.wait_forPageToFinishLoading();
    			Thread.sleep((long) (0.75*1000*60));
    	    	break;
    		}
    	}    	
    }
    
    public static void close_ProjectPermission_Modal() throws Exception{
    	BaseUI.click(Locator.lookupElement("forecasting_ProjectPermissions_CloseModal"));
    	Thread.sleep(1000);
    }
    
    //Verifies Profit Center List by User Name
    public static void verify_ProjectPermission_ActiveUser_List(String name, String listName) throws Exception{
    	click_ProjectPermission_User_ByName(name);
    	ArrayList<WebElement> profitCenterLists = Locator.lookup_multipleElements("forecasting_ProjectPermissions_Lists", null, null);
    	
    	BaseUI.verify_true_AndLog(profitCenterLists.size()>0, "Found Users", "Did Not Find Users");
    	for (WebElement profitCenterList : profitCenterLists ) {
    		if(BaseUI.getTextFromField(profitCenterList).contains(listName)){
    			String attributeValue= BaseUI.get_CSSAttribute_FromField(profitCenterList, "color");
    			String expectedColor ="rgba(255, 255, 255, 1)";
    			BaseUI.baseStringCompare("User Selected", expectedColor, attributeValue);
    			break;
    		}
    	}
    }
    
    
    //Verifies User Active List by User Name
    //Note: Active user is in Blue
    public static void verify_ProjectPermission_ActiveUser_ByName(String name){
    	ArrayList<WebElement> userLists = Locator.lookup_multipleElements("forecasting_ProjectPermissions_Users", null, null);
    	
    	BaseUI.verify_true_AndLog(userLists.size()>0, "Found Users", "Did Not Find Users");
    	for (WebElement userList : userLists ) {
    		if(BaseUI.getTextFromField(userList).contains(name)){
    			String attributeValue= BaseUI.get_Attribute_FromField(userList, "style");
    			String expectedvalue = "color: blue;";
    			BaseUI.baseStringCompare("User Selected", expectedvalue, attributeValue);
    			break;
    		}
    	}
    }
    
    //Verifies User Non Active List by User Name
    //Note: Non Active user is in Black
    public static void verify_ProjectPermission_NonActiveUser_ByName(String name){
    	ArrayList<WebElement> userLists = Locator.lookup_multipleElements("forecasting_ProjectPermissions_Users", null, null);
    	
    	BaseUI.verify_true_AndLog(userLists.size()>0, "Found Users", "Did Not Find Users");
    	for (WebElement userList : userLists ) {
    		if(BaseUI.getTextFromField(userList).contains(name)){
    			String attributeValue= BaseUI.get_Attribute_FromField(userList, "style");
    			String expectedvalue = "color: black;";
    			BaseUI.baseStringCompare("User Selected", expectedvalue, attributeValue);
    			break;
    		}
    	}
    }
    
    //Verifies Profit center Non Active List by User Name
    //Note: Non Active user is in Black
    public static void verify_ProjectPermission_NoPermission_UserList(String name) throws Exception{
    	click_ProjectPermission_User_ByName(name);
    	ArrayList<WebElement> profitCenterLists = Locator.lookup_multipleElements("forecasting_ProjectPermissions_Lists", null, null);
    	
    	BaseUI.verify_true_AndLog(profitCenterLists.size()>0, "Found list", "Did Not Find List");
    	
    	for (WebElement profitCenterList : profitCenterLists ) {
    		String attributeValue= BaseUI.get_CSSAttribute_FromField(profitCenterList, "color");
    		String expectedColor ="rgba(33, 33, 33, 1)";
    		BaseUI.baseStringCompare("User Not Selected Color", expectedColor, attributeValue);
    		}		
    }
    
    
    public static void clickFileMenuOptions(String menuOption) throws Exception {
        List<WebElement> fileMenuOptions = Locator.lookup_multipleElements("forecasting_FileMenuOptions", null, null);		
        
        BaseUI.click_js(Locator.lookupElement("forecasting_FileMenuLink"));
		Thread.sleep(200);
		for (WebElement fileOption : fileMenuOptions ) {
			if(BaseUI.getTextFromField(fileOption).contains(menuOption)) {
				BaseUI.click(fileOption);
				Thread.sleep(3000);
				BaseUI.wait_forPageToFinishLoading();
				BaseUI.waitForElementToBeDisplayed("forecasting_DeleteFCProject_ModalTitle", null, null);
				break;
			}
		}
	}
    
    
    public static void selectExistingFC_SelectFileMenu_DeleteProject(String fcName) throws Exception {
    	StartPage.clickStartPageTab();
    	Navigation.navigateToProjections_Forecasting();
		BaseUI.selectValueFromDropDown(Locator.lookupElement("forecastingProjects_dropdown"), 
				fcName);
		Thread.sleep(200);
		ForecastingPage.clickOpenForecastingProjectBtn();
		ForecastingPage.clickFileMenuOptions("Delete Project");
		StartPage.switchToIframe();
		clickDeleteProjModal_DeleteBtn();
		BaseUI.wait_forElementToNotExist("forecasting_DeleteFCProject_FCProjName", null, null);
		//BaseUI.switch_ToDefaultContent();
		EntitySelector.selectWindow("Banker's Dashboard");
		//EntitySelector.close_ExtraWindows("Banker's Dashboard");
    }
    
    
    public static void clickDeleteProjModal_DeleteBtn() throws Exception {
    	BaseUI.click(Locator.lookupElement("forecasting_DeleteFCProject_DeleteBtn"));
		Thread.sleep(3000);
		BaseUI.switch_ToDefaultContent();		
    }
    
    
    public static void enterSmartFillData() throws Exception {
    	ForecastingPage.clickSaveButton();
		ForecastingPage.clickSmartFillNextButton();
		ForecastingPage.clickSmartFillNextButton();
		StartPage.switchToIframe();
    }
    
    
    public static void clickCloseForecastingProjectReadyModal() throws InterruptedException {
    	BaseUI.click(Locator.lookupElement("forecasting_CopyToBudget_CloseBtn"));
		Thread.sleep(1000);
		BaseUI.switch_ToDefaultContent();
		EntitySelector.selectWindow("Banker's Dashboard");
    }
    
    
    public static void addNewFCProjectDetails_ByCenter(String fcName, String bankOrCenterOption, int primProfCenter, int cashBalAccOption,
    		int retEarningAccOption, String loanLossAll_Option, int begOfYearBalanceAcct, int isProvAcct, int bsProvAcct, 
    		String incomeTaxes_Option, int syncThruDate) throws Exception {
    	Navigation.navigateToProjections_Forecasting();
    	
    	if(BaseUI.elementAppears(Locator.lookupElement("forecasting_AnotherProjectCreationMsg"))) {
    		BaseUI.log_AndFail("Another Forecasting project is in creation and only one project can be created at a time");
    	} else {
    		clickCreateNewProject();
        	//StartPage.switchToIframe();
        	enterForecastingProjectName(fcName);
        	clickNextIcon();
        	clickByCenter_TotalRadioButton(bankOrCenterOption);
        	selectGeneralPage_PrimaryProfitCenter(primProfCenter);
        	primProfCenterSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_GeneralPage_PrimProfitCenter");
        	clickNextIcon();
        	clickByCenterCashPage_BalCashPCRadioButton("Yes");
        	selectCashPage_CashBalAccDropdown(cashBalAccOption);
        	cashBalAccOptionSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_CashPage_CashBalAccDropdown");
        	selectCashPage_RetainedEarningAcc(retEarningAccOption);
        	retEarningAccOptionSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_CashPage_RetainedEarningAccDropdown");
        	clickNextIcon();
        	clickByCenterLoanLossAllowPage_ConsolOrPCRadioButton(loanLossAll_Option);
        	selectLoanLossAllowancePage_BegOfYearBalanceAcct(begOfYearBalanceAcct);
        	begOfYearBalanceAcctSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_LoanLossAllowancePage_BegOfYearBalanceAcctDropdown");
        	selectLoanLossAllowancePage_BalanceSheetProvisionAcct(bsProvAcct);
        	bsProvAcctSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_LoanLossAllowancePage_BalanceSheetProvisionAcctDropdown");
        	selectLoanLossAllowancePage_IncomeStatementProvisionAcct(isProvAcct);
        	isProvAcctSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_LoanLossAllowancePage_IncomeStatementProvisionAcctDropdown");
        	clickNextIcon();
        	clickByCenterIncomeTaxesPage_ConsolOrPCRadioButton(incomeTaxes_Option);
        	clickNextIcon();
        	selectSyncHisDataPage_SyncThruDate(syncThruDate);
        	syncThruDateSelected = BaseUI.getSelectedOptionFromDropdown("forecasting_CreateNewFC_SyncHisDataPage_SyncThruDateDropdown");
        	clickNextIcon();
    	}   	
    }
    
    
    public static void clickByCenterIncomeTaxesPage_ConsolOrPCRadioButton(String type_radio) throws InterruptedException {		
		if(type_radio.equals("Consolidated")) {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ByCenter_IncomeTaxesPage_ConsolRadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ByCenter_IncomeTaxesPage_ProfCenterRadioBtn"));
			Thread.sleep(100);
		}	
	}
    
    
    public static void clickByCenterLoanLossAllowPage_ConsolOrPCRadioButton(String type_radio) throws InterruptedException {		
		if(type_radio.equals("Consolidated")) {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ByCenter_LoanLossAllowancePage_ConsolRadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ByCenter_LoanLossAllowancePage_ProfCenterRadioBtn"));
			Thread.sleep(100);
		}	
	}
    
    
    public static void clickByCenterCashPage_BalCashPCRadioButton(String type_radio) throws InterruptedException {		
		if(type_radio.equals("Yes")) {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ByCenter_CashPage_BalCashProfCenter_YesRadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("forecasting_CreateNewFC_ByCenter_CashPage_BalCashProfCenter_NoRadioBtn"));
			Thread.sleep(100);
		}	
	}
    
    
    public static void switchToModalIframe() throws Exception
	{
		BaseUI.switch_ToIframe(Locator.lookupElement("forecasting_modaliframe"));
		Thread.sleep(500);
	}
    
    
    public static void selectExistingFCByName(String fcName) throws Exception {
    	StartPage.clickCloseAllTabsButton();
//    	StartPage.clickStartPageTab();
    	Navigation.navigateToProjections_Forecasting();
		BaseUI.selectValueFromDropDown(Locator.lookupElement("forecastingProjects_dropdown"), 
				fcName);
		Thread.sleep(200);
		ForecastingPage.clickOpenForecastingProjectBtn();
    }
    
    
    public static String createForecastingProject(String fcName, String bankOrCenterOption, int primProfCenter, int cashBalAccOption,
    		int retEarningAccOption, int begOfYearBalanceAcct, int isProvAcct, int bsProvAcct, int syncThruDate) throws Exception {
    	addNewFCProjectDetails(fcName, bankOrCenterOption, primProfCenter, cashBalAccOption, 
    			retEarningAccOption, begOfYearBalanceAcct, isProvAcct, bsProvAcct, syncThruDate);
    	ForecastingPage.enterSmartFillData();
		BaseUI.waitForElementToBeDisplayed("forecasting_CreateNewFC_ForecastingProjReadyMsg", null ,null);
		ForecastingPage.clickCloseForecastingProjectReadyModal();
		return syncThruDateSelected;
    }
    
    
    public static void enterOutOfBalancevalue_Reconcile(String expOutOfBalValue) throws Exception {
    	BaseUI.enterText_IntoInputBox(Locator.lookupElement("forecasting_BSGroup_Assets_Subset_CashTellerOption_textbox"), 
				expOutOfBalValue);
		BaseUI.click(Locator.lookupElement("forecasting_BSGroup_Assets_Subset_CashTellerOption_2ndtextbox"));
		Thread.sleep(1000);
//		String storeMonthHeaderFortheColValueEntered = BaseUI
//				.getTextFromField(Locator.lookupElement("forecasting_BSGroup_Assets_Subset_CashTellerOption_MonthHeader"));
		EntitySelector.selectWindow("Banker's Dashboard");
		EntitySelector.close_ExtraWindows("Banker's Dashboard");
    }
    
    
    public static void closeReconcileModalWindow() throws Exception {
    	BaseUI.click(Locator.lookupElement("forecasting_ReconcileProject_CloseBtn"));
		Thread.sleep(1000);
		//BaseUI.wait_forElementToNotExist("forecasting_ReconcileProject_ModalTitle", null, null);
		//Browser.close_CurrentBrowserWindow();
		EntitySelector.selectWindow("Banker's Dashboard");
		EntitySelector.close_ExtraWindows("Banker's Dashboard");
    }
    
    
    public static void selectExistingFCByIndex(int fcName) throws Exception {
    	StartPage.clickCloseAllTabsButton();
    	StartPage.clickStartPageTab();
    	Navigation.navigateToProjections_Forecasting();
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("forecastingProjects_dropdown"), 
				fcName);
		Thread.sleep(200);
		ForecastingPage.clickOpenForecastingProjectBtn();
    }
    
    
    public static void clickBSOrISOption_ForSynchronizeHistoricalProject(String option) throws Exception {
    	if(option.contains("BS")) {
    		BaseUI.click_js(Locator.lookupElement("forecasting_SyncHistoricalProj_BSRadipBtn"));
    		Thread.sleep(200);
    	} else {
    		BaseUI.click_js(Locator.lookupElement("forecasting_SyncHistoricalProj_ISRadipBtn"));
    		Thread.sleep(200);
    	}
    }
    
    
    public static void selectSyncHisDataPage_SyncThruDateByName(String SyncThruDate) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("forecasting_SyncHistoricalProj_SyncDateDropdown"), 
				SyncThruDate);
		Thread.sleep(2000);
	}
    
    
    public static void clickSynchronizeHistoricalBtn() throws Exception {
    	BaseUI.click_js(Locator.lookupElement("forecasting_SyncHistoricalProj_SyncBtn"));
    	Thread.sleep(2000);
    }
    
    
    public static void clickSynchronizeHistoricalCloseBtn() throws Exception {
    	BaseUI.click_js(Locator.lookupElement("forecasting_SyncHistoricalProj_CloseBtn"));
    	Thread.sleep(2000);
    }
    
    public static void clickToolsSyncHistoricalOptions() throws Exception {
    	BaseUI.click(Locator.lookupElement("forecasting_ToolsMenuLink"));
		Thread.sleep(500);
    
    	BaseUI.click_js(Locator.lookupElement("forecasting_SyncHistoricalProj_Toolsmenu_SyncHisSubmenu"));
		Thread.sleep(1000);
    }
    
    public static String convertSynDate(String date) {
    	int syncYear = Integer.parseInt(date);
    	String expSyncYear = String.valueOf(syncYear+1);
		return expSyncYear; 		
    }
    
    
    public static String createCopyOfExistingForecastingProject(String existingFCProj) throws Exception {
    	clickFileMenuOptions("New Project");
		switchToModalIframe();
		enterForecastingProjectName(existingFCProj+"_Copy"+BaseUI.random_NumberAsString(0, 100));
		String newFCName = BaseUI.getTextFromInputBox(Locator.lookupElement("forecasting_CreateNewFC_ProjNameTxtbox"));
		selectCurrentFCProject(existingFCProj);
		clickNextIcon();
		clickSaveButton();
		BaseUI.waitForElementToBeDisplayed("forecasting_ExistingForecaseProjName", null, null);
		
		return newFCName;
    }
    
    
    public static void synchronizeHistorical_ForecastingProject(String syncThruYear) throws Exception {
    	ForecastingPage.clickToolsMenuOptions("Synchronize Historical...");
    	//Navigation.navigateToForecasting_Tools_SynchronizeHistorical();
    	switchToModalIframe();
    	selectSyncHisDataPage_SyncThruDateByName("December "+syncThruYear);
    	clickBSOrISOption_ForSynchronizeHistoricalProject("BS");
    	clickSynchronizeHistoricalBtn();
    	BaseUI.switch_ToDefaultContent();
    	Thread.sleep(500);
    }
    
    
    public static void deleteForecastingProjectWhenOnFCPage_ForecastingProjectDisplayed() throws Exception {
    	clickFileMenuOptions("Delete Project");
		StartPage.switchToIframe();
		ForecastingPage.clickDeleteProjModal_DeleteBtn();
		BaseUI.wait_forElementToNotExist("forecasting_DeleteFCProject_FCProjName", null, null);
		EntitySelector.selectWindow("Banker's Dashboard");
		//EntitySelector.close_ExtraWindows("Banker's Dashboard");
    }
  
}
