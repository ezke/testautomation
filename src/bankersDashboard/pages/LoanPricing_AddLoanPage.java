package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import bankersDashboard.pageControls.DatePicker;
import utils.BaseUI;
import utils.Locator;

public class LoanPricing_AddLoanPage {
	
	private static int indexOfNewlyAddedLoan;


	public static ArrayList<String> return_LoanIDPerPageList() {
		ArrayList<WebElement> loanIDs = Locator.lookup_multipleElements("loanPricing_LoanPricingModel_LoanIDList", null,
				null);

		ArrayList<String> loanIDsList = new ArrayList<String>();

		for (WebElement data : loanIDs) {
			String loanIDsValue = BaseUI.getTextFromField(data);
			loanIDsList.add(loanIDsValue);
		}
		
		return loanIDsList;
	}
	
	
	public static void selectNumberOfLoanIDs_EntriesPerPage(String valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_NumberOfItemsPerPageDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void selectOfficersByValue(String valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_OfficersDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void selectOfficersByIndexValue(int valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("loanPricing_LoanPricingModel_OfficersDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void selectLoanTypes(String valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_LoanTypesDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void selectLoanStatues(String valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_LoanStatuesDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void selectMonthsDropdown(String valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_MonthsDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void clickAddLoanBtn() throws Exception {
		BaseUI.click_js(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanBtn"));
		Thread.sleep(2000);
		BaseUI.switch_ToIframe(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_Iframe"));
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanPage_LoanDetailsTitle", null, null);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanPage_LoanPricingAnalysisTitle", null, null);
	}
	
	
	public static void selectLoanOfficer(int valueToBeSelected) throws Exception {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_LoanOfficerDropdown"), 
				valueToBeSelected);
		Thread.sleep(200);
	}
	
	
	public static void chooseApplicationType(String applicationType) throws Exception {
		if(applicationType.equals("New")) {
			BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_AppType_NewRadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_AppType_RenewRadioBtn"));
			Thread.sleep(100);
		}
	}
	
	
	public static void chooseRateType(String rateType) throws Exception {
		if(rateType.equals("Fixed")) {
			BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_RateType_FixedRadioBtn"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_RateType_VariableRadioBtn"));
			Thread.sleep(100);
		}
	}
	
	
	public static void selectLoanType(String loanType) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_LoanTypeDropdown"), 
				loanType);
		Thread.sleep(200);
	}
	
	
	public static String addLoan(String loanIdentifier, String refSource, String loanAmount, String propRate, String term,
			String avgLife, String loanFees, String loanType, String riskWeightingOption, String appType, String rateType) throws Exception {
		clickAddLoanBtn();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_LoanIdentifierTextbox"), 
				loanIdentifier);
		selectLoanOfficer(1);
		String loanOfficer = BaseUI.getSelectedOptionFromDropdown("loanPricing_LoanPricingModel_AddLoanPage_LoanOfficerDropdown");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_ReferralSourceTextbox"), 
				refSource);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_LoanAmountTextbox"), 
				loanAmount);
		//Choose New or Renewed Application type
		chooseApplicationType(appType);
		
		//Choose Fixed or Variable Rate type
		chooseRateType(rateType);
		
		//Enter Proposed Rate, Term, AvgLife, LoanFees
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_ProposedRateTextbox"), 
				propRate);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_TermTextbox"), 
				term);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_AvgLifeTextbox"), 
				avgLife);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_LoanFeesTextbox"), 
				loanFees);
		
		selectLoanType(loanType);
		selectRiskWeighting(riskWeightingOption);
		
		//Enter Account, Balance and Rate details
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_AccTextbox"), 
				"1234567890123456");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_AvgBalanceTextbox"), 
				"10000");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_RateTextbox"), 
				"3");
		clickSaveLoanButton();
		return loanOfficer;		
	}
	
	public static void clickSaveLoanButton() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_SaveLoanBtn"));
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_SaveLoanBtn"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanPage_LoanSaveMsg", null, null);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanPage_ReturnToLoanPipelineLink", null, null);
	}
	
	
	public static void clickAddLoanPage_ReturnToLoanPipelineLink() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_ReturnToLoanPipelineLink"));
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_ReturnToLoanPipelineLink"));
		Thread.sleep(3000);
		BaseUI.switch_ToDefaultContent();
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanBtn", null, null);
	}
	
	
	public static ArrayList<String> return_NewlyAddedLoanDetails() {
		ArrayList<WebElement> newLoanDetails = Locator.lookup_multipleElements("loanPricing_LoanPricingModel_TopRowLoanDetails", null,
				null);

		ArrayList<String> newloanDetailsList = new ArrayList<String>();

		for (WebElement data : newLoanDetails) {
			String loanDetailValue = BaseUI.getTextFromField(data);
			newloanDetailsList.add(loanDetailValue);
		}
		
		return newloanDetailsList;
	}
	
	
	public static void verifyLoansPerPageMatchSelectedToDisplay() {
		String numberPerPageSelected = BaseUI.getSelectedOptionFromDropdown("loanPricing_LoanPricingModel_NumberOfItemsPerPageDropdown");
		ArrayList<String> loanIDsList = LoanPricing_AddLoanPage.return_LoanIDPerPageList();
		
		BaseUI.verify_true_AndLog(loanIDsList.size() > 0, "Loan IDs Displayed", "Loan IDs NOT Displayed");
		BaseUI.verify_true_AndLog(numberPerPageSelected.contains(String.valueOf(loanIDsList.size())), 
				"Loan IDs Displayed MATCH Number per page Entry "+ loanIDsList.size(), 
				"Loan IDs Displayed DOES NOT MATCH Number per page Entry " + loanIDsList.size());
	}
	
	
	public static void verifyNewlyAddedLoanDetailsMatchTheEnteredValues(String loanIdentifier, String loanAmount, String loanType, String propRate,
			String refSource, String loanOfficer, String loanStatus) {
		ArrayList<String> newLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedLoanDetails();
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		for (int i=0; i<newLoanDetails.size()-2; i++) {
			if(!(i == newLoanDetails.size()-2) || i == newLoanDetails.size()-10) {
			BaseUI.verify_true_AndLog((newLoanDetails.get(i).trim()).contains(loanIdentifier.trim()) || (newLoanDetails.get(i).trim()).contains(loanAmount.trim()) ||
					(newLoanDetails.get(i).trim()).contains(loanType) || (newLoanDetails.get(i).trim()).contains(propRate) ||
					(newLoanDetails.get(i).trim()).contains(BaseUI.getTodaysDate_AsString().trim()) || (newLoanDetails.get(i).trim()).contains("0") ||
					(newLoanDetails.get(i).trim()).contains(loanStatus) || (newLoanDetails.get(i).trim()).contains(refSource) || 
					(newLoanDetails.get(i).trim()).contains(loanOfficer.trim()), 
					"Newly added loan details MATCH the ones entered "+ newLoanDetails.get(i), 
					"Newly added loan details DOES NOT MATCH the ones entered "+ newLoanDetails.get(i));
			}
		}
	}
	
	
	public static void verifyNewlyAddedLoanIDMatchTheEnteredLoanIdentifier(ArrayList<String> newLoanDetails, String loanIdentifier) {
		
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 0;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(loanIdentifier.trim()), 
				"Newly added loan ID MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added loan ID DOES NOT MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan));
	}
	
	
	public static void verifyNewlyAddedLoanOfficerMatchTheEnteredLoanOfficer(ArrayList<String> newLoanDetails, String loanOfficer) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 1;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(loanOfficer.trim()), 
				"Newly added loan Officer MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added loan Officer DOES NOT MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan));	
	}
	
	
	public static void verifyNewlyAddedLoanAmountMatchTheEnteredLoanAmount(ArrayList<String> newLoanDetails, String loanAmount) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 2;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).replaceAll(",", "").trim()).contains(loanAmount.trim()), 
				"Newly added loan Amount MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added loan Amount DOES NOT MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan));	
	}
	
	
	public static void verifyNewlyAddedLoanTypeMatchTheEnteredLoanType(ArrayList<String> newLoanDetails, String loanType) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 3;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(loanType.trim()), 
				"Newly added loan Type MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added loan Type DOES NOT MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void verifyNewlyAddedLoanProposedRateMatchTheEnteredProposedRate(ArrayList<String> newLoanDetails, String propRate) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 4;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(propRate.trim()), 
				"Newly added Loan Proposed Rate MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added Loan Proposed Rate DOES NOT MATCH the one entered "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void verifyNewlyAddedLoanCreatedDateMatchTodaysDate(ArrayList<String> newLoanDetails, String propRate) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 5;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(BaseUI.getTodaysDate_AsString().trim()), 
				"Newly added Loan Date MATCH the Loan creation day date "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added Loan Date DOES NOT MATCH the Loan creation day date "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void verifyNewlyAddedLoanWinProbabilityMatch(ArrayList<String> newLoanDetails, String winProbability) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 6;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains("0"), 
				"Newly added Loan Win Probability MATCH  "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added Loan Win Probability DOES NOT MATCH "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void verifyNewlyAddedLoanStatusMatch(ArrayList<String> newLoanDetails, String loanStatus) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 7;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(loanStatus), 
				"Newly added Loan Status MATCH  "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added Loan Status DOES NOT MATCH "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void verifyNewlyAddedLoanReferalMatchEnteredReferalSource(ArrayList<String> newLoanDetails, String refSource) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 8;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(refSource), 
				"Newly added Loan Referal Source MATCH  "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added Loan Referal Source DOES NOT MATCH "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void verifyNewlyAddedLoanProjectedCloseDateMatch(ArrayList<String> newLoanDetails, String projectedCloseDate) {
		BaseUI.verify_true_AndLog(newLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+newLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+newLoanDetails.size());
		indexOfNewlyAddedLoan = 9;
		BaseUI.verify_true_AndLog((newLoanDetails.get(indexOfNewlyAddedLoan).trim()).contains(projectedCloseDate), 
				"Newly added Loan projected Close Date MATCH  "+ newLoanDetails.get(indexOfNewlyAddedLoan), 
				"Newly added Loan projected Close Date DOES NOT MATCH "+ newLoanDetails.get(indexOfNewlyAddedLoan));		
	}
	
	
	public static void deleteLoan_ClickOk() throws Exception {
		clickDeleteLoanIcon();
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_DeleteLoanPricingDataModal_ModalTitle", 
				null, null);
		clickDeleteLoanPricing_OkButton();
	}
	
	
	public static void deleteLoan_ClickCancel() throws Exception {
		clickDeleteLoanIcon();
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_DeleteLoanPricingDataModal_ModalTitle", 
				null, null);
		clickDeleteLoanPricing_CancelButton();
	}
	
	
	public static void clickDeleteLoanPricing_OkButton() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_DeleteLoanPricingDataModal_OkBtn"));
		Thread.sleep(500);
	}
	
	
	public static void clickDeleteLoanPricing_CancelButton() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_DeleteLoanPricingDataModal_CancelBtn"));
		Thread.sleep(500);
	}
	
	
	public static void clickDeleteLoanIcon() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("loanPricing_LoanPricingModel_LoanDeleteIcon"));
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_LoanDeleteIcon"));
		Thread.sleep(500);
	}
	
	
	public static void clickEditLoanIcon() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("loanPricing_LoanPricingModel_LoanEditIcon"));
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_LoanEditIcon"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_ModalTitle", 
				null, null);
	}
	
	
	// Select pick up date
	public static void select_PickUpDate(String pickUpDate) throws Exception {
		BaseUI.click_js(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePickerIcon"));
		Thread.sleep(200);
		DatePicker.selectTheDate(pickUpDate);
	}
	
	
	public static void selectLoanStatus_UpdateStatusModal(String loanStatus) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_StatusDropdown"), 
				loanStatus);
		Thread.sleep(300);
	}
	
	public static void enterWinProbability(String winProbabilityValue){
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_WinProbabilityTextbox"), 
				winProbabilityValue);
	}
	
	
	public static void clickUpdateLoanStatus_CancelButton() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_CancelBtn"));
		Thread.sleep(2500);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanBtn", null, null);
	}
	
	
	public static void clickUpdateLoanStatus_UpdateButton() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_UpdateBtn"));
		Thread.sleep(2500);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_AddLoanBtn", null, null, 1000);
	}
	
	
	public static void clickLoanEditIcon_UpdateLoanStatus(String loanStatus, String dateValue,
			String winProbabilityValue) throws Exception {
		clickEditLoanIcon();
//		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_EnterDate"), 
//				dateValue);
		select_PickUpDate(dateValue);
		enterWinProbability(winProbabilityValue);
		selectLoanStatus_UpdateStatusModal(loanStatus);
		clickUpdateLoanStatus_UpdateButton();
	}
	
	
	public static void clickLoanEditIcon_DoNotUpdateLoanStatus(String loanStatus, String dateValue, 
			String winProbabilityValue) throws Exception {
		clickEditLoanIcon();
		selectLoanStatus_UpdateStatusModal(loanStatus);
		select_PickUpDate(dateValue);
		enterWinProbability(winProbabilityValue);
		clickUpdateLoanStatus_CancelButton();
	}
	
	
	public static ArrayList<String> return_NewlyAddedUpdatedLoanStatus() {
		ArrayList<WebElement> updatedLoanDetails = Locator.lookup_multipleElements("loanPricing_LoanPricingModel_UpdatedLoanDetails", null,
				null);

		ArrayList<String> updatedloanDetailsList = new ArrayList<String>();

		for (WebElement data : updatedLoanDetails) {
			String loanDetailValue = BaseUI.getTextFromField(data);
			updatedloanDetailsList.add(loanDetailValue);
		}
		
		return updatedloanDetailsList;
	}
	
	public static void verifyNewlyAddedLoanStatusUpdated(String loanIdentifier, String loanAmount, String loanType, String propRate,
			String refSource, String loanOfficer, String loanStatusUpdated, String winProbability, String closingDate) {
		ArrayList<String> updatedLoanDetails = LoanPricing_AddLoanPage.return_NewlyAddedUpdatedLoanStatus();
		BaseUI.verify_true_AndLog(updatedLoanDetails.size() > 0, "New Loan Details are Displayed in loan pipeline "+updatedLoanDetails.size(), 
				"New Loan Details are NOT Displayed in loan pipeline "+updatedLoanDetails.size());
		for (int i=1; i<updatedLoanDetails.size()-2; i++) {
			if(!(i == updatedLoanDetails.size()-2)) {
			BaseUI.verify_true_AndLog((updatedLoanDetails.get(i).trim()).contains(loanIdentifier.trim()) || (updatedLoanDetails.get(i).trim()).contains(loanAmount.trim()) ||
					(updatedLoanDetails.get(i).trim()).contains(loanType) || (updatedLoanDetails.get(i).trim()).contains(propRate) ||
					(updatedLoanDetails.get(i).trim()).contains(BaseUI.getTodaysDate_AsString().trim()) || (updatedLoanDetails.get(i).trim()).contains("0") ||
					(updatedLoanDetails.get(i).trim()).contains(loanStatusUpdated) || (updatedLoanDetails.get(i).trim()).contains(refSource) || 
					(updatedLoanDetails.get(i).trim()).contains(loanOfficer.trim()) || (updatedLoanDetails.get(i).trim()).contains(winProbability.trim()) || 
					(updatedLoanDetails.get(i).trim()).contains(closingDate), 
					"Newly added loan Status updated MATCH the ones entered "+ updatedLoanDetails.get(i), 
					"Newly added loan Status updated DOES NOT MATCH the ones entered "+ updatedLoanDetails.get(i));
			}
		}
	}
	
	
	public static void deleteUpdatedLoan_ClickOk() throws Exception {
		WebElement loanDeleteIcon = Locator.lookupElement("loanPricing_LoanPricingModel_UpdatedLoan_DeleteIcon");
		BaseUI.scroll_to_element(loanDeleteIcon);
		BaseUI.click(loanDeleteIcon);
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("loanPricing_LoanPricingModel_DeleteLoanPricingDataModal_ModalTitle", 
				null, null);
		clickDeleteLoanPricing_OkButton();
	}
	
	
	public static void selectRiskWeighting(String riskWeightingType) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("loanPricing_LoanPricingModel_AddLoanPage_RiskWeightingDropdown"), 
				riskWeightingType);
		Thread.sleep(200);
	}

}
