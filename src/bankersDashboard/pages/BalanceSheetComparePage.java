/**
 * 
 */
package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

/**
 * @author Selenium
 *
 */
public class BalanceSheetComparePage {

	public static void verifyBalanceSheetCompareModalTitle() {
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ModalTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ModalTitle", "Balance Sheet Compare");
	}
	
	
	public static void clickShowReportButton() throws Exception {
		BaseUI.switch_ToDefaultContent();
		BaseUI.click(Locator.lookupElement("balanceSheet_showReportBtn"));
		Thread.sleep(1000);
		BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("balanceSheetCompare_ReportTitle", null, null, 50);
	}


	public static void clickExpandColumnsCheckbox() throws Exception {
		BaseUI.click(Locator.lookupElement("balanceSheetCompare_Expand_columns_checkbox"));
		Thread.sleep(500);
	}
	

	public static void verifyExpandColumnCheckboxIsEnabled() {
		BaseUI.verifyElementEnabled(Locator.lookupElement("balanceSheetCompare_Expand_columns_checkbox"));
	}
	

	public static void clickTireTrackIconToExpand() throws Exception {
		BaseUI.click(Locator.lookupElement("balanceSheetCompare_TireTracksIcon"));
		Thread.sleep(1000);
	}
	
	
	public static void clickToDrilldownReport(String elementToClick) throws Exception {
		BaseUI.click(Locator.lookupElement(elementToClick));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	
	public static void selectViewOption(String view) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_ViewDropdown"), view);
		Thread.sleep(500);
	}
	
	
	public static void selectProfitCenterOption(String profitCenter) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Profit_center_dropdown"), profitCenter);
		Thread.sleep(500);
	}
	
	
	public static void enterSearchCriteria_PrimaryRow(String dataSource, String avg, String date) throws Exception {
		
		//Select all the details for Primary row		
		BaseUI.click(Locator.lookupElement("balanceSheetCompare_Expand_columns_checkbox"));
		Thread.sleep(200);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Column1_dataSource_dropdown"), dataSource);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Avg_column1_dropdown"), avg);
		BaseUI.click(Locator.lookupElement("balanceSheetCompare_Date_column2_dropdown"));
		Thread.sleep(200);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Date_column1_dropdown"), date);
		Thread.sleep(1500);
	}
	
	
    public static void enterSearchCriteria_SecColumn(String dataSource, String avg, String date) throws Exception {
		
		//Select all the details for 2nd column
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Column2DataSourceDropdown"), dataSource);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Avg_column2_dropdown"), avg);
		BaseUI.click(Locator.lookupElement("balanceSheetCompare_Date_column2_dropdown"));
		Thread.sleep(200);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Date_column2_dropdown"), date);
		Thread.sleep(2000);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Column2CompareDropdown"), "Primary");
		Thread.sleep(1000);
	}
    
    
    public static void enterMtdSearchCriteria(String view, String profitCenter, String dataSourcePrimary, String avgPrimary, String datePrimary,
    		String dataSourceSecCol, String avgSecCol, String dateSecCol) throws Exception {
    	selectViewOption(view);
    	selectProfitCenterOption(profitCenter);
    	enterSearchCriteria_PrimaryRow(dataSourcePrimary, avgPrimary, datePrimary);
    	enterSearchCriteria_SecColumn(dataSourceSecCol, avgSecCol, datePrimary);  	  	
    }
    
    
    public static ArrayList<String> return_PrimaryCol_Data() {
		ArrayList<WebElement> primaryColData = Locator.lookup_multipleElements("balanceSheetCompare_ReportPrimaryColData", null,
				null);

		ArrayList<String> primaryDataList = new ArrayList<String>();

		for (WebElement data : primaryColData) {
			String colValue = BaseUI.getTextFromField(data).trim();
			primaryDataList.add(colValue);
		}
		
		return primaryDataList;
	}
    
    
    public static ArrayList<String> return_SecondColumn_data() {
    	ArrayList<WebElement> secondColData = Locator.lookup_multipleElements("balanceSheetCompare_ReportSecColData", null,
				null);

		ArrayList<String> secDataList = new ArrayList<String>();

		for (WebElement data : secondColData) {
			String colValue = BaseUI.getTextFromField(data).trim();
			secDataList.add(colValue);
			System.out.println(colValue);
		}
		
		return secDataList;
    }
    
    
    public static void verifyPrimarySecond_ColumnData() {
    	ArrayList<String> primaryDataList = return_PrimaryCol_Data();
    	BaseUI.verify_true_AndLog(primaryDataList.size() > 0, "Array list is NOT empty", "Array List is empty");
    	
    	ArrayList<String> secDataList = return_SecondColumn_data();
    	BaseUI.verify_true_AndLog(secDataList.size() > 0, "Array list is NOT empty", "Array List is empty");
    	
    	for(int i=0; i<secDataList.size(); i++) {
    		BaseUI.verify_true_AndLog(secDataList.get(i).contains(primaryDataList.get(i)), 
    				secDataList.get(i) + " Second column data MATCH Primary column "+ primaryDataList.get(i), 
    				secDataList.get(i) + " Second column data DOES NOT MATCH Primary column "+ primaryDataList.get(i));
    		
    	}
    }
	

    public static void clickGroupDrillDown() throws Exception {
	    BalanceSheetComparePage.clickToDrilldownReport("balanceSheetCompare_cashAndDuesFromBank_DrillDown");
	    BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
    }
    
    
    public static void clickAccountDrillDown() throws Exception {
    	BalanceSheetComparePage.clickToDrilldownReport("balanceSheetCompare_cashTeller_groupDrillDown");
	    BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
    }
    
    
    public static void clickProfitCenterDrillDown() throws Exception {
    	BalanceSheetComparePage.clickToDrilldownReport("balanceSheetCompare_cashTeller_AccountDrillDown");
	    BaseUI.wait_forElementToNotExist("reportLoadingText", null, null);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
    }
	
    
    public static ArrayList<String> return_BalanceSheetRep_Column1List() {
		ArrayList<WebElement> bsRep_Col1 = Locator.lookup_multipleElements("balanceSheetRep_SubheaderColumnList", null,
				null);

		ArrayList<String> bsRep_Col1List = new ArrayList<String>();

		for (WebElement data : bsRep_Col1) {
			String capRatioValue = BaseUI.getTextFromField(data);
			bsRep_Col1List.add(capRatioValue);
		}
		
		return bsRep_Col1List;
	}
    
    
    public static ArrayList<String> return_BalanceSheetRep_DollarChangeColList() {
		ArrayList<WebElement> bsRep_$ChangeCol1 = Locator.lookup_multipleElements("balanceSheetRep_DollarChangeColumnList", null,
				null);

		ArrayList<String> bsRep_$ChangeCol1List = new ArrayList<String>();

		for (WebElement data : bsRep_$ChangeCol1) {
			String capRatioValue = BaseUI.getTextFromField(data);
			bsRep_$ChangeCol1List.add(capRatioValue);
		}
		
		return bsRep_$ChangeCol1List;
	}
    
    
    public static ArrayList<String> return_BalanceSheetRep_PercentChangeColList() {
		ArrayList<WebElement> bsRep_PerChangeCol1 = Locator.lookup_multipleElements("balanceSheetRep_PercentChangeColumnList", null,
				null);

		ArrayList<String> bsRep_PerChangeCol1List = new ArrayList<String>();

		for (WebElement data : bsRep_PerChangeCol1) {
			String capRatioValue = BaseUI.getTextFromField(data);
			bsRep_PerChangeCol1List.add(capRatioValue);
		}
		
		return bsRep_PerChangeCol1List;
	}
    
    
    public static void verifyBalanceSheet_DollarValue(ArrayList<String> totalBSAlertsList, 
	    	ArrayList<String> bsAlert_dollarValue) {
    	ArrayList<String> bsRep_Col1List = BalanceSheetComparePage.return_BalanceSheetRep_Column1List();
		BaseUI.verify_true_AndLog(bsRep_Col1List.size() > 0, "BS compare Column1 list Displayed", 
				"BS compare Column1 list IS NOT Displayed");
		
		ArrayList<String> bsRep_DollarChangeList = BalanceSheetComparePage.return_BalanceSheetRep_DollarChangeColList();
		BaseUI.verify_true_AndLog(bsRep_DollarChangeList.size() > 0, "BS compare $change column list Displayed", 
				"BS compare $change column list IS NOT Displayed");
		
		for (int i=0; i<totalBSAlertsList.size(); i++) {
			for (int j=0; j<bsRep_Col1List.size(); j++) {
				if(bsRep_Col1List.get(j).equals(totalBSAlertsList.get(i))) {
					BaseUI.verify_true_AndLog((bsRep_DollarChangeList.get(j).replace("-", "").replaceAll("\\(", "").replaceAll("\\)", "").trim())
							.contains((bsAlert_dollarValue.get(i).replace("$", "").replace("-", "").trim())), 
							bsRep_DollarChangeList.get(j) + "BS compare report $change value MATCH displayed on BS Alerts" + bsAlert_dollarValue.get(i).replaceAll("$", ""), 
							bsRep_DollarChangeList.get(j) + "BS compare report $change value DOES NOT MATCH displayed on BS Alerts" + bsAlert_dollarValue.get(i).replaceAll("$", ""));
				}
			}
		}
    }
    
    
    public static void verifyBalanceSheet_PercentValue(ArrayList<String> totalBSAlertsList, 
	    	ArrayList<String> bsAlert_PercentValue) {
    	ArrayList<String> bsRep_Col1List = BalanceSheetComparePage.return_BalanceSheetRep_Column1List();
		BaseUI.verify_true_AndLog(bsRep_Col1List.size() > 0, "BS compare Column1 list Displayed", 
				"BS compare Column1 list IS NOT Displayed");
		
		ArrayList<String> bsRep_PercentageChangeList = BalanceSheetComparePage.return_BalanceSheetRep_PercentChangeColList();
		BaseUI.verify_true_AndLog(bsRep_PercentageChangeList.size() > 0, "BS compare %change column list Displayed", 
				"BS compare %change column list IS NOT Displayed");
		
		for (int i=0; i<totalBSAlertsList.size(); i++) {
			for (int j=0; j<bsRep_Col1List.size(); j++) {
				if(bsRep_Col1List.get(j).equals(totalBSAlertsList.get(i))) {
					double percentValue = Double.parseDouble(bsAlert_PercentValue.get(i)
							.replaceAll("%", "").replaceAll("\\(", "").replaceAll("\\)", "").trim());
					String bsAlerts_PercentValue = String.valueOf(Math.round(Double.valueOf(percentValue)));
//					BigDecimal myBigDecimal = new BigDecimal(percentValue);		
//					String bsAlerts_PercentValue = myBigDecimal.setScale(1, RoundingMode.HALF_UP).toString();
					BaseUI.verify_true_AndLog((bsRep_PercentageChangeList.get(j).replace("-", "").trim()).contains(bsAlerts_PercentValue), 
							bsRep_PercentageChangeList.get(j) + "BS compare report %change value MATCH displayed on BS Alerts" + bsAlerts_PercentValue, 
							bsRep_PercentageChangeList.get(j) + "BS compare report %change value DOES NOT MATCH displayed on BS Alerts" + bsAlerts_PercentValue);
				} else {
					BaseUI.verify_true_AndLog(!(bsRep_Col1List.get(j).equals(totalBSAlertsList.get(i))), 
							"Report column list DOES NOT MATCH Alert List", "Report column list MATCH Alert List");
				}
			}
		}
    }
    
    
    public static void verifyBalanceSheetAlert_AlertsNum_MatchesAlertsDisplayed() {
    	String totalBSAlerts = BaseUI.
				getTextFromField(Locator.lookupElement("alerts_BalanceSheetTab_BSAlertsNum"));
		System.out.println("totalBSAlerts:" + totalBSAlerts);
		ArrayList<String> totalBSAlertsList = StartPage_AlertsModal.return_BalanceSheetAlertsList();
		
		BaseUI.verify_true_AndLog(totalBSAlerts.equals(String.valueOf(totalBSAlertsList.size())), 
				"Balance Sheet Alerts Number and displayed List Match "+ totalBSAlertsList.size(), 
				"Balance Sheet Alerts Number and displayed List DOES NOT Match "+ totalBSAlertsList.size());
    }
    
    
    public static void openBSCompareReport_WithForecastingProject(String view, String profitCenter, String primDataSource, String primAvg, String primDate, 
    		String secDataSource, String secAvg, String secDate) throws Exception {
    	Navigation.navigateToGL_BS_BalanceSheetCompare();
		selectViewOption(view);
		selectProfitCenterOption(profitCenter);
		enterSearchCriteria_PrimaryRow(primDataSource, primAvg, primDate);
		clickExpandColumnCheckbox();
		enterSearchCriteria_SecColumn(secDataSource, secAvg, secDate);
		clickShowReportButton();
    }
    
    
    public static ArrayList<String> return_BalanceSheetRep_PrimColumn1List() {
		ArrayList<WebElement> bsRep_PrimColList = Locator.lookup_multipleElements("balanceSheetRep_PrimaryColumnList", null,
				null);

		ArrayList<String> bsRep_PrimCol = new ArrayList<String>();

		for (WebElement data : bsRep_PrimColList) {
			String primColValue = BaseUI.getTextFromField(data);
			bsRep_PrimCol.add(primColValue);
		}
		
		return bsRep_PrimCol;
	}
    
    
    public static ArrayList<String> return_BalanceSheetRep_SecColumn1List() {
		ArrayList<WebElement> bsRep_SecColList = Locator.lookup_multipleElements("balanceSheetRep_SecColumnList", null,
				null);

		ArrayList<String> bsRep_SecCol = new ArrayList<String>();

		for (WebElement data : bsRep_SecColList) {
			String secColValue = BaseUI.getTextFromField(data);
			bsRep_SecCol.add(secColValue);
		}
		
		return bsRep_SecCol;
	}
    
    
    public static void verifyBalanceSheet_PrimColData_WithSecColData() {
    	ArrayList<String> bsRep_PrimCol1List = BalanceSheetComparePage.return_BalanceSheetRep_PrimColumn1List();
		BaseUI.verify_true_AndLog(bsRep_PrimCol1List.size() > 0, "BS compare Primary Column list Displayed", 
				"BS compare Primary Column list IS NOT Displayed");
		
		ArrayList<String> bsRep_secCol1List = BalanceSheetComparePage.return_BalanceSheetRep_SecColumn1List();
		BaseUI.verify_true_AndLog(bsRep_secCol1List.size() > 0, "BS compare Second column list Displayed", 
				"BS compare Second column list IS NOT Displayed");
		
		for (int i=0; i<bsRep_PrimCol1List.size(); i++) {
			BaseUI.verify_true_AndLog((bsRep_PrimCol1List.get(i).trim()).equals(bsRep_secCol1List.get(i)), 
					bsRep_PrimCol1List.get(i) + "BS compare report Primary Col value MATCH Second Column" + bsRep_secCol1List.get(i), 
					bsRep_PrimCol1List.get(i) + "BS compare report Primary Col value DOES NOT MATCH Second Column" + bsRep_secCol1List.get(i));
		}
    }
    
    
    public static void clickExpandColumnCheckbox() throws Exception {
    	BaseUI.click(Locator.lookupElement("balanceSheetCompare_Expand_columns_checkbox"));
		Thread.sleep(200);
    }

    public static void selectDateValueFromDropdown(String dateValue) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("balanceSheetCompare_Date_column2_dropdown"), dateValue);
		Thread.sleep(500);
	}
}
