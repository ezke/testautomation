package bankersDashboard.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class SummaryReportPage {
	
	public static ArrayList<String> return_SummaryReport_Data() {
		ArrayList<WebElement> summaryReportData = Locator.lookup_multipleElements("summaryReportData", null,
				null);

		ArrayList<String> dataList = new ArrayList<String>();

		for (WebElement data : summaryReportData) {
			String summaryRepValue = BaseUI.getTextFromField(data);
			dataList.add(summaryRepValue);
		}
		
		return dataList;
	}

	
	
    public static void verifyDataOnSummaryWidget_MatchSummaryReport(ArrayList<String> widgetData) throws Exception {
		
    	ArrayList<String> summaryReportData = new ArrayList<String>(SummaryReportPage.return_SummaryReport_Data());
		BaseUI.verify_true_AndLog(summaryReportData.size() > 0, "Found data" + summaryReportData.size(), 
				"Did NOT data links."+ summaryReportData.size());
		
		BaseUI.verify_true_AndLog(summaryReportData.size() == widgetData.size(), "Array List Match", 
				"Array List DOES NOT Match");
		
		for(int i = 0; i<summaryReportData.size(); i++) {
			BaseUI.verify_true_AndLog((summaryReportData.get(i).trim()).equals(widgetData.get(i).trim()), 
					summaryReportData.get(i) + " Data Match " + widgetData.get(i), 
					summaryReportData.get(i) +" Data DOES NOT Match " + widgetData.get(i));
		}
	}
    
    
    public static void clickRefreshIcon() throws Exception {
    	BaseUI.click(Locator.lookupElement("summaryReport_RefreshButton"));
		Thread.sleep(2000);
		BaseUI.wait_forElementToNotExist("reportLoadingText", "3000", null); 
		BaseUI.wait_forElementToNotExist("capRatios_ReportLoadingText", "2000", null);
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("summaryReport_profitCenterDisplayed", null, "300");
    }
}
