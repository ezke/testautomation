package bankersDashboard.tests;

import bankersDashboard.data.BaseTest;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
//import library.Utility;
import utils.ResultWriter;

public class DemoExtentReport extends BaseTest {
	
	ExtentReports report;
	ExtentTest logger; 
	WebDriver driver;
	
	 
	@Test(priority = 1)
	public void verifyBlogTitle() throws Exception
	{
		
	report=new ExtentReports("C:\\Report\\TestExtentReport.html");
	 
	logger=report.startTest("VerifyBlogTitle");
	 
	//driver=new FirefoxDriver();
	 
	//driver.manage().window().maximize();
	 
	logger.log(LogStatus.INFO, "Browser started ");
	 
	Browser.openBrowser("http://www.google.com");
	 
	logger.log(LogStatus.INFO, "Application is up and running");
	 
	String title=Browser.driver.getTitle();
	 
	Assert.assertTrue(title.contains("Google")); 
	 
	logger.log(LogStatus.PASS, "Title verified");
	
	Thread.sleep(1000);
	
	
	logger.log(LogStatus.INFO, "Clicking on search box");
	BaseUI.click(Locator.lookupElement("googleSearchBox"));
	Thread.sleep(500);
	
	String text = BaseUI.getTextFromField(Locator.lookupElement("googleSearchBox"));
	
	logger.log(LogStatus.PASS, "Successfully clicked on search box");
	
	System.out.println(text);
	
	}
	
	
	@Test(priority = 2)
	public void enterTextInSearchBox() throws Exception
	{
		logger.log(LogStatus.INFO, "Entering text in search box");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("enterText_SearchBox"), "Selenium");
		Thread.sleep(500);
		
		String text = BaseUI.getTextFromField(Locator.lookupElement("enterText_SearchBox"));
		
		logger.log(LogStatus.PASS, "Successfully entered text in search box");
		
		System.out.println(text);
		
		Thread.sleep(1000);
	}
	 
	@Test(priority = 3)
	public void clickOnSearchBox() throws Exception
	{
		
	logger.log(LogStatus.INFO, "Clicking on search box");
	BaseUI.click(Locator.lookupElement("searchButton"));
	Thread.sleep(500);
	
	//String text = BaseUI.getTextFromField(Locator.lookupElement("searchButton"));
	
	logger.log(LogStatus.PASS, "Successfully clicked on search box");
	
	//System.out.println(text);
	}
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception
	{
		
		
	if(result.getStatus()==ITestResult.FAILURE)
	{
		ResultWriter.checkForFailureAndScreenshot(result);
	 
	//String screenshot_path=Utility.captureScreenshot(driver, result.getName());
	//String image= logger.addScreenCapture(screenshot_path);
	logger.log(LogStatus.FAIL, "Title verification");
	 
	 
	}
	}
	
	@AfterClass
	public void teardown()
	{
	report.endTest(logger);
	report.flush();
	 
	Browser.driver.get("C:\\Report\\TestExtentReport.html");
	}
	 

}
