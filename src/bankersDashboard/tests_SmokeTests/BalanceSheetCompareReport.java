package bankersDashboard.tests_SmokeTests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankersDashboard.data.BaseTest;
import bankersDashboard.data.ClientDataRetrieval;
import bankersDashboard.pages.BalanceSheetComparePage;
import bankersDashboard.pages.LoginPage;
import bankersDashboard.pages.Navigation;
import bankersDashboard.pages.StartPage;
import bankersDashboard.pageControls.EntitySelector;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class BalanceSheetCompareReport extends BaseTest {

	private static String profitCenterSelected, saveSelectedSource, saveSelectedDateOption;
	
	@BeforeClass(alwaysRun = true)
	public static void set_up () throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		//Select the bank Entity
		EntitySelector.select_bankEntity_fromDropdown(ClientDataRetrieval.profitCenter);
		EntitySelector.imageVerification("loginPage_BD_logo");	
		
		//Navigate to Balance Sheet compare report
		StartPage.clickStartPageTab();	
		Navigation.navigateToGL_BS_BalanceSheetCompare();
		profitCenterSelected = BaseUI.getSelectedOptionFromDropdown("balanceSheetCompare_Profit_center_dropdown");
		saveSelectedSource = BaseUI.getSelectedOptionFromDropdown("balanceSheetCompare_Column1_dataSource_dropdown");
		saveSelectedDateOption = BaseUI.getSelectedOptionFromDropdown("balanceSheetCompare_Date_column1_dropdown");
	}
	
	
	@Test(priority = 4, groups = { "smoke_tests", "critical_Tests" })
	public static void PT1475_Step5_verifyBankLogoIsDisplayed() throws Exception {
		BalanceSheetComparePage.clickShowReportButton();
		//validate bank logo is displayed on Report page
		BaseUI.verifyElementAppears(Locator.lookupElement("bankLogo"));
	}
	
	
	@Test(priority = 5, groups = { "smoke_tests", "critical_Tests" })
	public static void PT1475_Step6_verifyBalanceSheetCompareReportTitle() throws Exception {
		//validate Report title
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportTitle"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportTitle", "Balance Sheet Compare");
	}
	
	
	@Test(priority = 6, groups = { "smoke_tests", "critical_Tests" })
	public static void PT1475_Step6_verifySelectedProfitCenterIsDisplayed() throws Exception {
		//validate profit center match the selected
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ProfitCenter"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ProfitCenter", profitCenterSelected);
	}
	
	@Test(priority = 7, groups = { "smoke_tests", "critical_Tests" })
	public static void PT1475_Step6_verifyPrimaryColumn_DataSourceAndDate() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("balanceSheetCompare_ReportPrimaryDateSelected"));
		BaseUI.verifyElementHasExpectedText("balanceSheetCompare_ReportPrimaryDateSelected", 
				(saveSelectedSource+ "\n" + BaseUI.return_Date_AsDifferentFormat(saveSelectedDateOption, "MMMM yyyy", "MMM yyyy")).trim());
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
  
    
    @AfterClass(alwaysRun = true)
    public void tear_down() throws Throwable {  
	    try {
		  //Close all tabs and click on start page tab
		  StartPage.clickCloseAllTabsButton();
		  
		  //click signout button to logout.
	      Navigation.navigate_to_signOut();
	   } finally{
	      Browser.closeBrowser();
	   }	  
   }
}
