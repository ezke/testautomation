package bankersDashboard.pages_CreditUnion;

import org.openqa.selenium.By;

import utils.BaseUI;
import utils.Locator;

public class LoginPage_CreditUnion {
	
		
	public static void login(String userName, String password) throws Exception {
		
		verifyCreditUnion_Logo();
		verifyCreditUnion_ClientLogin();
		
		//Enter Username and Password
		BaseUI.click(Locator.lookupElement("creditUnion_Login_Username"));
		BaseUI.enterText(Locator.lookupElement("creditUnion_Login_Username"), userName);
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("creditUnion_Login_Password"));
		BaseUI.enterText(Locator.lookupElement("creditUnion_Login_Password"), password);
		
		//Click Login button
		click_LoginButton();
		BaseUI.wait_forPageToFinishLoading();
	}
	
	public static void verifyCreditUnion_Logo() {
		BaseUI.verifyElementAppears(Locator.findElement(By.id("logo")));
	}

	public static void verifyCreditUnion_ClientLogin() {
		BaseUI.verifyElementAppears(Locator.lookupElement("creditUnion_Login_ClientLogin_Txt"));
	}
	
	public static void click_LoginButton() throws Exception {
		BaseUI.click(Locator.lookupElement("creditUnion_Login_LoginBtn"));
		Thread.sleep(2000);	
	}
	
	public static void click_SiteCertificate() throws Exception {
		BaseUI.click(Locator.lookupElement("creditUnion_SiteCertificate"));
		Thread.sleep(500);
	}
}
