package bankersDashboard.pageControls;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.testng.Reporter;
import utils.BaseUI;
import utils.Locator;

public class DatePicker {

	public static void dateValidate(String dateString) throws ParseException
	{
		Reporter.log("Date is being formated as needed");
		DateFormat format = new SimpleDateFormat("MMM YYYY", Locale.ENGLISH);
		Date date = null;
		try
		{
		  Reporter.log("Date is parsed");
		  date = format.parse(dateString);
		  System.out.println(date);
		  //SimpleDateFormat format = new SimpleDateFormat("MMM YYYY");
		  Reporter.log("Displaying the formatted date");
		  System.out.println("Displayed Date on report is : " + format.format(date));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	
	public static String verifyDateDisplayed(String dateToVerify) 
	{
		Reporter.log("Date is being formated as needed");
		DateFormat formatting = new SimpleDateFormat("MMM YYYY", Locale.ENGLISH);
		Date date = null;
		try
		{
		  date = formatting.parse(dateToVerify);
		  System.out.println(date);
		  //SimpleDateFormat format = new SimpleDateFormat("MMM YYYY");
		  Reporter.log("Displaying the formatted date");
		  System.out.println("Displayed Date on report is : " + formatting.format(date));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return(formatting.format(date));
	}
	
	
	// date String must be in format MM/dd/yyyy
	public static void selectTheDate(String date) throws Exception {
			
//		DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
//		Date dateTime = formatDate.parse(date);
		Calendar dateWeWant = getCalendarValue(date);

		//extract the year and month out of the web element.
		String month_And_Year = BaseUI.getTextFromField(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_MonthYearHeader"));
		String month = month_And_Year.split("\\s+")[0];
		Integer monthInteger = convertMonth_Name_To_Number(month);
		String year = month_And_Year.split("\\s+")[1];
		Integer yearInteger = Integer.parseInt(year);

		navigate_ToCorrectYear(dateWeWant, yearInteger);
		navigate_ToCorrectMonth(dateWeWant, monthInteger);
		click_On_Date(String.valueOf(dateWeWant.get(Calendar.DAY_OF_MONTH)));
	}
		
		
	public static Calendar getCalendarValue(String date) {
		final DateFormat formattedDate = new SimpleDateFormat("MM/dd/yyyy");
	    final Calendar calendarDate = Calendar.getInstance();
	    try {
	    	calendarDate.setTime(formattedDate.parse(date));
	        System.out.println("Year = " + calendarDate.get(Calendar.YEAR));
	        System.out.println("Month = " + (calendarDate.get(Calendar.MONTH)));
	        System.out.println("Day = " + calendarDate.get(Calendar.DAY_OF_MONTH));
	        } catch (ParseException e) {
	        	e.printStackTrace();
	        	}
			
		return calendarDate;
	}
		
		
	public static void click_On_Date(String dateToPick) throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_SelectDate",
				dateToPick, null));
		Thread.sleep(500);
	}

	public static void navigate_ToCorrectYear(Calendar dateTimeWeWant, Integer currentYearSet) throws Exception {
		Integer yearOfDateWeWant =  dateTimeWeWant.get(Calendar.YEAR);
		Integer yearDifference = yearOfDateWeWant - currentYearSet;
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_MonthName"));
		Thread.sleep(500);
		
		if (yearDifference > 0) {
			for (int i = 0; i < yearDifference; i++) {
				click_NextYear_Button();
			}
		}
		if (yearDifference < 0) {
			for (int i = 0; i > yearDifference; i--) {
				click_PreviousYear_Button();
			}
		}
	}

	public static void navigate_ToCorrectMonth(Calendar dateTimeWeWant, Integer currentMonthSet) throws Exception {
		Integer monthDifference = dateTimeWeWant.get(Calendar.MONTH) - currentMonthSet;

		if (monthDifference > 0) {
			for (int i = 0; i < monthDifference; i++) {
				click_NextMonth_Button();
			}
	    } else if (monthDifference < 0) {
			for (int i = 0; i > monthDifference; i--) {
				click_PreviousMonth_Button();
			}
		}
	}

	public static void click_PreviousMonth_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_CurrentMonth"));
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_PreviousMonth"));
		Thread.sleep(200);
	}

	
	public static void click_NextMonth_Button() throws Exception {
		BaseUI.click_js(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_CurrentMonth"));
		Thread.sleep(500);
		BaseUI.click_js(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_NextMonth"));
		Thread.sleep(500);
	}

	public static void click_NextYear_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_NextYearBtn"));
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_NextYear_MonthOption"));
		Thread.sleep(500);
	}
		
	public static void click_PreviousYear_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_PreviousYearBtn"));
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("loanPricing_LoanPricingModel_EditLoanPricingStatusModal_DatePicker_NextYear_MonthOption"));
		Thread.sleep(500);
	}


	public static Integer convertMonth_Name_To_Number(String monthName) throws Exception {
		Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(monthName);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer month = cal.get(Calendar.MONTH);

		return month;
	}
	
}
