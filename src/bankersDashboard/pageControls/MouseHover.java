/**
 * 
 */
package bankersDashboard.pageControls;

/**
 * @author Nisha
 *
 */
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class MouseHover {
	
    /**Nisha- 01/19/2017
     * MouseHover action performed on Left Menu Navigation
     * Option from sub-menu 1 is selected
     * 
     * @param menuID
     * @param itemToClick
     */
    public static void ClickItemFromLeftMenu_subMenu1_option(String menuID, String itemToClick)
    {			
		Actions action = new Actions(Browser.driver);
		BaseUI.click(Locator.lookupElement(menuID));
		//WebElement Option2 = Locator.lookupElement(option2);
	    action.moveToElement(Locator.lookupElement(itemToClick))
	          .click().build().perform();
	    try{
	         Thread.sleep(3000);
	    }catch(InterruptedException e)
	    {
	    	//Do nothing
	    }
		System.out.println("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
		Reporter.log("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
	}
    
    
    
    /**MouseHover action is performed on Left Menu tab
     * Option from sub-menu 2 is selected
     * 
     * @param menuID
     * @param subMenu2
     * @param itemToClick
     * @throws Exception 
     */
    public static void clickItemFromLeftMenu_subMenu2_option(String menuID, String subMenu2, String itemToClick) throws Exception
    {			
		Actions action = new Actions(Browser.driver);
		BaseUI.elementHover(Locator.lookupElement(menuID));
		BaseUI.click_js(Locator.lookupElement(menuID));
		WebElement subMenuOption = Locator.lookupElement(subMenu2);
		BaseUI.click_js(subMenuOption);
		action.moveToElement(subMenuOption).click();		
	    action.moveToElement(Locator.lookupElement(itemToClick))
	          .click().build().perform();
	    BaseUI.click_js(Locator.lookupElement(itemToClick));
	    Thread.sleep(3000);
//	    try{
//	         Thread.sleep(3000);
//	    }catch(InterruptedException e)
//	    {
//	    	//Do nothing
//	    }
		System.out.println("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
		Reporter.log("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
	}
    
    
	
    
    /**Nisha- 01/18/2017
     * MouseHover action performed on Left Menu Navigation
     * Option from sub-menu 1 is selected
     * 
     * @param menuID
     * @param itemToClick
     */
	public static void clickItemFromLeftMenu_subMenu1_option1(String menuID, String itemToClick) throws Exception
	{			
		// Actions action = new Actions(Browser.driver);
		// BaseUI.click(Locator.lookupElement(menuID));
		// action.moveToElement(Locator.lookupElement(itemToClick))
		// .click().build().perform();
		BaseUI.elementHover_js(Locator.lookupElement(menuID));
		BaseUI.click_js(Locator.lookupElement(menuID));
		Thread.sleep(2000);
		BaseUI.elementHover_js(Locator.lookupElement(itemToClick));
		BaseUI.click_js(Locator.lookupElement(itemToClick));
		Thread.sleep(3000);
		// BaseUI.click_js(Locator.lookupElement(itemToClick));
		Reporter.log("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
		}
	
	
	
	/**Nisha- 01/18/2017
     * MouseHover action is performed on Left Menu tab
     * Option from sub-menu 1 is selected
	 * 
	 * @param menuID
	 * @param option1
	 * @param itemToClick
	 */	
	public static void clickItemFromLeftMenu_subMenu1_option2(String menuID, String option1, String itemToClick)
	{			
			Actions action = new Actions(Browser.driver);
			BaseUI.click(Locator.lookupElement(menuID));   
		    action.moveToElement(Locator.lookupElement(option1)).click();
		    action.moveToElement(Locator.lookupElement(itemToClick))
		          .click().perform();
		    try{
		         Thread.sleep(3000);
		    }catch(InterruptedException e)
		    {
		    	//Do nothing
		    }
			System.out.println("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
			Reporter.log("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
		}
	
	
	/**Nisha- 01/18/2017
     * MouseHover action performed on Left Menu Navigation
     * Option from sub-menu 1 is selected
	 * 
	 * @param menuID
	 * @param option1
	 * @param option2
	 * @param itemToClick
	 * @throws Exception 
	 */
    public static void clickItemFromLeftMenu_subMenu1_option3(String menuID, String option1, String option2, String itemToClick) throws Exception
     {			
		Actions action = new Actions(Browser.driver);
		BaseUI.click(Locator.lookupElement(menuID));
		WebElement Option2 = Locator.lookupElement(option2);
	    action.moveToElement(Locator.lookupElement(option1))
	          .moveToElement(Option2)
	          .moveToElement(Locator.lookupElement(itemToClick))
	          .click().build().perform();
	    try{
	         Thread.sleep(3000);
	    }catch(InterruptedException e)
	    {
	    	//Do nothing
	    }
		System.out.println("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
		Reporter.log("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
	}
    
    
    /**Nisha- 01/18/2017
     * MouseHover action performed on Left Menu Navigation
     * Option from sub-menu 2 is selected
     * 
     * @param leftNavigation1
     * @param firstSubMenu
     * @param arrow
     * @param firstOption
     * @throws InterruptedException
     */
    
  //Selecting from sub-sub menu options
    public static void secondSubMenuOption1(String leftNavigation1, String firstSubMenu, 
    		String arrow, String firstOption) throws InterruptedException
      {
        //mouse hover action is performed here
        Actions action = new Actions(Browser.driver);

        BaseUI.click(Locator.lookupElement(leftNavigation1));
        action.moveToElement(Locator.lookupElement(firstSubMenu)).click();
        action.moveToElement(Locator.lookupElement(arrow)).click();
        action.moveToElement(Locator.lookupElement(firstOption)).click().perform();
       
        Thread.sleep(3000);

        
        
      }
}

