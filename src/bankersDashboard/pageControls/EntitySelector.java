/**
 * 
 */
package bankersDashboard.pageControls;

/**
 * @author Nisha
 *
 */
import java.util.List;
import java.util.Set;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import bankersDashboard.data.ClientDataRetrieval;
import org.openqa.selenium.JavascriptExecutor;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class EntitySelector {
	
	public static void select_bankEntity_fromDropdown(String bankEntity) throws Exception
	{
	
		String profitCenter_BankEntity_actual = BaseUI.getTextFromField(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		
		if(!(profitCenter_BankEntity_actual.equals(ClientDataRetrieval.profitCenter)))
		{
		
		    System.out.println("Displayed bank entity is " + profitCenter_BankEntity_actual);
		    Reporter.log("Displayed bank entity is " + profitCenter_BankEntity_actual);
			BaseUI.click(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
			Thread.sleep(15000);
			List<WebElement> allBankEntity = Locator.lookup_multipleElements("startPage_SelectBankEntity_dropdown", null, null);
			
			for (WebElement profitCenter : allBankEntity )
			{
				if(profitCenter.getText().equals(bankEntity)) //GlobalVariables.profitCenter_entity
				{
					profitCenter.click();
					BaseUI.wait_forPageToFinishLoading();
					BaseUI.waitForElementToBeDisplayed_ButDontFail("startPage_ProfitCenterEntity_displayed", 
							ClientDataRetrieval.profitCenter, null, 5000);
					break;
				}
			}
			EntitySelector.imageVerification("loginPage_BD_logo");
			BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
	   }
	}

	
	public static void select_consolidatedBankEntity_fromDropdown(String consolidated_bankEntity) throws InterruptedException
	{
		String profitCenter_BankEntity_actual = BaseUI.getTextFromField(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
		if(!(profitCenter_BankEntity_actual.equals(ClientDataRetrieval.consolidatedEntity)))	
		{
			System.out.println("Displayed bank entity is " + profitCenter_BankEntity_actual);
			Reporter.log("Displayed bank entity is " + profitCenter_BankEntity_actual);
			BaseUI.click(Locator.lookupElement("startPage_ProfitCenterEntity_displayed"));
			Thread.sleep(12000);
			List<WebElement> allBankEntity = Locator.lookup_multipleElements("startPage_SelectBankEntity_dropdown", null, null);
			
			for (WebElement profitCenter : allBankEntity )
			{
				if(profitCenter.getText().equals(consolidated_bankEntity))
				{
					profitCenter.click();
					BaseUI.wait_forPageToFinishLoading();
					BaseUI.waitForElementToBeDisplayed_ButDontFail("startPage_ProfitCenterEntity_displayed", 
							ClientDataRetrieval.consolidatedEntity, null, 5000);
					break;
				}
			}
			EntitySelector.imageVerification("loginPage_BD_logo");
			BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_BD_logo"));
		}
	}
	
	public static void imageVerification(String element)
	{
		WebElement imageFile = Locator.lookupElement(element);
		Boolean imagePresent = (Boolean) ((JavascriptExecutor)Browser.driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", imageFile);
		if(!imagePresent){
		System.out.println("Image is not displayed");
		Reporter.log("Image is not displayed" + element);
		}else{
		System.out.println("Image is displayed");
		Reporter.log("Image is not displayed" + element);
		}
	}
	
	
	public static void close_ExtraWindows(String primaryWindowHandle) throws Exception {
		// Checks for extra windows and closes any that are not the main window.
		primaryWindowHandle = Browser.driver.getWindowHandle();
		System.out.println(primaryWindowHandle);
		//System.out.println(BaseUI.getTextFromField(Locator.lookupElement(primaryWindowHandle)));
		if (Browser.driver.getWindowHandles().size() > 1) {
			for (String s : Browser.driver.getWindowHandles()) {
				BaseUI.switch_ToWindow(s);
				Reporter.log("Successfully switched to newly open window " + s);
				Browser.driver.switchTo().window(s);

				if (!s.equals(primaryWindowHandle)) {
					BaseUI.switch_ToWindow(s);
					Reporter.log("Successfully switched to primary window " + s);
					Browser.driver.close();
					Reporter.log("Successfully closed primary window " + s);
				}
			}
			// Switch back to primary window handle after we've closed other
			// windows.
			BaseUI.switch_ToWindow(primaryWindowHandle);
			Thread.sleep(100);
			String parentWindowTitle = Browser.driver.getTitle();
			Reporter.log("Successfully switched back to main window " + parentWindowTitle);
		}
	}
	
	
	public static void selectWindow(String windowTitle)
	{
		WebDriver popUp = null;
		Reporter.log("Get all the window handles");
		Set<String> windowHandles = Browser.driver.getWindowHandles();
		for(int i=0;i<windowHandles.size();i++)
		{
			popUp = Browser.driver.switchTo().window(windowHandles.toArray()[i].toString());
			Reporter.log("Switch to windowTitle passed");
			System.out.println("Window Title : "+ popUp.getTitle());
			if(popUp.getTitle().equals(windowTitle))
			{
				Reporter.log("If windowTitle matches the required window title break the loop");
				break;
			}
		}
	}
	
	
	//This method is not closing the selected window I want. Need to work on this.
	//Commenting out these lines for now
//	public static void selectWindow_CloseWindow(String windowTitle)
//	{
//		WebDriver popUp = null;
//		Reporter.log("Get all the window handles");
//		Set<String> windowHandles = Browser.driver.getWindowHandles();
//		for(int i=0;i<windowHandles.size();i++)
//		{
//			popUp = Browser.driver.switchTo().window(windowHandles.toArray()[i].toString());
//			Reporter.log("Switch to windowTitle passed");
//			System.out.println("Window Title : "+ popUp.getTitle());
//			if(popUp.getTitle().equals(windowTitle))
//			{
//				Reporter.log("If windowTitle matches the required window title break the loop");
//				Browser.closeBrowser();
//				//break;
//			}
//		}
//	}
}

