package bankersDashboard.pageControls;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class DropDowns {
	
	public static void dragAndDrop(WebElement source, WebElement destination)
	{
		//WebElement From = driver.findElement(By.xpath(".//*[@id='treebox1']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span"));
		BaseUI.verifyElementAppears(Locator.lookupElement("source"));
		//WebElement To = driver.findElement(By.xpath(".//*[@id='treebox2']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span"));
		BaseUI.verifyElementAppears(Locator.lookupElement("destination")); 
		Actions builder = new Actions(Browser.driver);
		 
		Action dragAndDrop = builder.clickAndHold(source)
		 
		.moveToElement(destination)
		 
		.release(destination)
		 
		.build();
		 
		dragAndDrop.perform();
	}
	
	
	public static void selectFromDropDownByIndex(WebElement elementToSelect, int optionToBeSelected) throws Exception
	{
		System.out.println("Selecting option " + optionToBeSelected + " from WebElement " + elementToSelect.getTagName());
		Reporter.log("Selecting option " + optionToBeSelected + " from WebElement " + elementToSelect.getTagName());
		new Select(elementToSelect).selectByIndex(optionToBeSelected);
		Thread.sleep(500);
	}
	
	
	public static boolean isAlertPresent() {
        try {
        	WebDriverWait wait = new WebDriverWait(Browser.driver, 2);
    		wait.until(ExpectedConditions.alertIsPresent());
            Browser.driver.switchTo().alert();
            Reporter.log("Successfully switched to Alert and accepted it");
            return true;
        } catch (NoAlertPresentException Ex) {
        	Reporter.log("Alert is not present");
            return false;
        }   
    }   

}
