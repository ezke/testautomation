package bankersDashboard.tests_prerelease;

import bankersDashboard.data.BaseTest;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import bankersDashboard.pages.GlobalVariables;
import bankersDashboard.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


/**
 * @author Nisha
 * 
 *   Test #1139 -
 *   BD-LOGIN: Reset Password: User Account Locks After 3 Consecutive Failed Login Attempts  
 */

public class BDLogin_ResetPasswordUserAccountLocksAfter3FailedAttempt extends BaseTest {

//		private static String username = "automation.test.00000";
		private static String password = "Password1&";
		private static String invalidLoginCredentialsMsg = "Your username and/or password is incorrect.";
		private static String consecutiveFailedAttemptsMsg = "4 failed login attempts. Account disabled, please contact your account administrator!";
		private static String errorMessage;
		private static String validatePasswordRetrievalText = "Enter your username. A password reset email will be sent to your account on file.";
		private static String emailSentMessage = "An email has been sent to your account on file. Please click on the link provided in the email to reset your password.";
		
		@BeforeClass(alwaysRun = true)
		public static void set_up () throws Exception
		{
			Browser.openBrowser(GlobalVariables.baseURL_RC);
		}
		
		
		
		@Test(priority = 0)
		public static void TC1139_validateUserAccountLocksAfter4ConsecutiveFailedAttempts() throws Exception
		{
			for (int i=0;i<3;i++)
			{
			    LoginPage.login(GlobalVariables.userName, password);
				BaseUI.elementAppears(Locator.lookupElement("Verify_message_for_wrong_login_credentials")); 
				Boolean condition = BaseUI.elementAppears(Locator.lookupElement("Verify_message_for_wrong_login_credentials"));
			    System.out.println(condition);
			    if(condition = true)
			    {
			      BaseUI.verifyElementAppears(Locator.lookupElement("Verify_message_for_wrong_login_credentials"));
			      errorMessage = BaseUI.getTextFromField(Locator.lookupElement("Verify_message_for_wrong_login_credentials"));
			      BaseUI.baseStringCompare("Verify_message_for_wrong_login_credentials", invalidLoginCredentialsMsg, errorMessage);
			    }
			} 
			
			 LoginPage.login(GlobalVariables.userName, password);
			 BaseUI.verifyElementAppears(Locator.lookupElement("verifyMessageFor4WrongLoginCredentials"));
			 errorMessage = BaseUI.getTextFromField(Locator.lookupElement("verifyMessageFor4WrongLoginCredentials"));
			 BaseUI.baseStringCompare("verifyMessageFor4WrongLoginCredentials", consecutiveFailedAttemptsMsg, errorMessage);
			 Thread.sleep(1000);
		}
		
		
		@Test(priority = 1)
		public static void TC1139_validateForgotPasswordLink() throws Exception
		{
			BaseUI.click(Locator.lookupElement("forgotPassword_Linktext"));
			Thread.sleep(500);
			BaseUI.verifyElementAppears(Locator.lookupElement("passwordRetrievalHeader"));
			BaseUI.verifyElementAppears(Locator.lookupElement("verifyTextOnPassowrdRetrievalPage"));
			BaseUI.verifyElementHasExpectedText("verifyTextOnPassowrdRetrievalPage", validatePasswordRetrievalText);
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("forgotPasswordPage_UsernameTextbox"), GlobalVariables.userName);
			BaseUI.click(Locator.lookupElement("resetPassword_button"));
			Thread.sleep(500);
			BaseUI.verifyElementAppears(Locator.lookupElement("emailSentMessage"));
			BaseUI.verifyElementHasExpectedText("emailSentMessage", emailSentMessage);
			
			
		}
		
		
		@AfterMethod(alwaysRun = true)
		public void testCleanup(ITestResult result) throws Exception {
			ResultWriter.checkForFailureAndScreenshot(result);

		}
	  
	  
	  
	  @AfterClass(alwaysRun = true)
	  public void tear_down() throws Throwable
	  {	  		  
		      Browser.closeBrowser();		  
	  }
}
