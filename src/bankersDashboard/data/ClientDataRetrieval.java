package bankersDashboard.data;

import java.util.ArrayList;
import java.util.HashMap;

import bankersDashboard.pages.GlobalVariables;
import org.testng.Reporter;
import utils.Browser;
import utils.DataBuilder;

public class ClientDataRetrieval {

	public static HashMap<String, String> config_Data = null;
	public static String clientName;
	public static String userName;
	public static String password;
	public static String environment;
	public static Object[][] environment_Data = null;
	public static String objectRepositoryPath;
	public static String url;
	public static String profitCenter;
	public static String consolidatedEntity;
	
	public static client_Designation client_Name;


	// pulls values from xml. If values are null, it relies on Config file as
	// backup.
	public static void set_TestData_XML() throws Exception {
		
		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\bankersDashboard\\data\\Config.txt");
		
		environment_Data = DataBuilder.getTableData_FromFile("\\src\\bankersDashboard\\data\\ClientData.txt");

		clientName = getValue("clientName");
		userName = getValue("userName");
		password = getValue("password");
		environment = getValue("environment");
		profitCenter = getValue("profitCenter");
		consolidatedEntity = getValue("consolidatedEntity");
		GlobalVariables.bDashboard_Admin_URL = "http://admin-" + environment.toLowerCase() + ".bankersdashboard.com/login.asp";
		GlobalVariables.creditUnion_URL = "https://" + environment.toLowerCase() + ".deluxeperformancedashboard.com/login.aspx";
		Browser.defaultBrowser = getValue("browser");
		
		objectRepositoryPath = returnValue_ByPassedInText("Repo", "Environment", clientName, environment);
		url = returnValue_ByPassedInText("URL", "Environment", clientName, environment);
		set_Client();
	}


	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}

		if (value != null) {
			System.setProperty(valueToGet, value);
		}
		return value;
	}

	// looks for the value for the columnHeader variable. Matches the row based
		// on the rowIdentifier and the columnHeader2/columnHeader2_Value.
		public static String returnValue_ByPassedInText(String columnHeader, String columnHeader2, String rowIdentifier,
				String columnHeader2_CellValue) {
			return returnValue_ByPassedInText(environment_Data, columnHeader, columnHeader2, rowIdentifier,
					columnHeader2_CellValue);
		}

		public static String returnValue_ByPassedInText(Object[][] dataToUse, String columnHeader, String columnHeader2,
				String rowIdentifier, String columnHeader2_CellValue) {
			String value = null;

			ArrayList<String> headerList = new ArrayList<String>();
			for (int i = 0; i < dataToUse[0].length; i++) {
				headerList.add(dataToUse[0][i].toString());
			}

			for (int i = 0; i < dataToUse.length; i++) {
				if (rowIdentifier.equals(dataToUse[i][0].toString())
						&& dataToUse[i][headerList.indexOf(columnHeader2)].equals(columnHeader2_CellValue)) {

					value = dataToUse[i][headerList.indexOf(columnHeader)].toString();
					break;
				}
			}

			return value;
		}

		public static String returnValue_ByPassedInText(String columnHeader, String rowIdentifier) {
			String value = null;

			ArrayList<String> headerList = new ArrayList<String>();
			for (int i = 0; i <= environment_Data.length; i++) {
				headerList.add(environment_Data[0][i].toString());
			}

			for (int i = 0; i <= environment_Data.length; i++) {
				if (rowIdentifier.equals(environment_Data[i][0].toString())) {

					value = environment_Data[i][headerList.indexOf(columnHeader)].toString();
					break;
				}
			}

			return value;
		}

		// Return true if the client you passed in matches the client that is
		// active.
		public static Boolean client_Matches(client_Designation clientToMatch) {
			Boolean clientNameMatches = false;

			clientNameMatches = client_Name.equals(clientToMatch) ? true : false;

			return clientNameMatches;
		}

		private static void set_Client() {
			switch (clientName) {
			case "Any Bank USA":
				client_Name = client_Designation.AnyBankUsa;
				profitCenter = "Any Bank USA";
				consolidatedEntity = "AnyBank USA, Inc & Subsidaries";
				break;
			case "Oak Bank":
				client_Name = client_Designation.OakBank;
				profitCenter = "Oak National Bank";
				consolidatedEntity = "Oak Consolidated";
				break;
			case "Simmons Bank":
				client_Name = client_Designation.Simmons;
				profitCenter = "Simmons Bank";
				consolidatedEntity = "Simmons First National Corp (SFNC)";
				break;
			case "Bank of North Carolina":
				client_Name = client_Designation.BNC;
				profitCenter = "Bank of North Carolina";
				consolidatedEntity = "BNC BanCorp & Subsidiaries";
				break;
			case "CB&S Bank":
				client_Name = client_Designation.CBS;
				profitCenter = "CB&S Bank";
				consolidatedEntity = "NA";
				break;
			case "United Bank":
				client_Name = client_Designation.United;
				profitCenter = "United Bank";
				consolidatedEntity = "Consolidated Bank";
				break;
			case "First Bank":
				client_Name = client_Designation.FirstBank;
				profitCenter = "First Bank";
				consolidatedEntity = "NA";
				break;
			default:
				String message = "Unable to find client.";
				System.out.println(message);
				Reporter.log(message);

				break;
			}

		}

		public static enum client_Designation {
			AnyBankUsa("Any Bank USA"), OakBank("Oak National Bank"), Simmons("Simmons Bank"), BNC("Bank of North Carolina"),
			 CBS("CB&S Bank"), United("United Bank"), FirstBank("First Bank"),  ;

			private String value;

			private client_Designation(final String val) {
				value = val;
			}

			public String getValue() {
				return value;
			}

			@Override
			public String toString() {
				return getValue();
			}
		}
}
