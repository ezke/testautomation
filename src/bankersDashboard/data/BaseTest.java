package bankersDashboard.data;

import org.testng.annotations.BeforeSuite;

import utils.Locator;
import utils.Selenium;

public abstract class BaseTest {
	
	@BeforeSuite(alwaysRun=true)
	public void config_setup_method() throws Exception {
		Selenium.verifySeleniumIsRunning();
		ClientDataRetrieval.set_TestData_XML();
		Locator.loadObjectRepository(
				// Load our common repository
				"\\src\\bankersDashboard\\bDashboardObjectRepo.txt",
				// Load our client specific repository
				ClientDataRetrieval.objectRepositoryPath);
	}
}
