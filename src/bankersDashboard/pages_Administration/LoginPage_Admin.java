package bankersDashboard.pages_Administration;

import bankersDashboard.pages.GlobalVariables;
import utils.BaseUI;
import utils.Locator;

public class LoginPage_Admin {

	public static void login_Admin() throws Exception {
		BaseUI.click(Locator.lookupElement("admin_login_UserID"));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("admin_login_UserID"),
				GlobalVariables.bDashboard_Admin_Username);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("admin_login_password"),
				GlobalVariables.bDashboard_Admin_Password);
		click_Submit();
	}

	public static void click_Submit() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("admin_login_submit"));
		Thread.sleep(3000);
	}
}
