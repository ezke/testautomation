package bankersDashboard.pages_Administration;

import utils.BaseUI;
import utils.Locator;

public class HomePage_Admin {
	
	public static void click_Logout() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("admn_HomePage_LogoutLink"));
		Thread.sleep(1000);
	}

}
