package utils;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.regex.Pattern;

public final class RemoteWindowsService implements WindowsService {
    @NotNull
    private final String _serverName;
    @NotNull
    private final String _windowsServiceName;
    @NotNull
    private final Duration _startTimeout;
    @NotNull
    private final Duration _stopTimeout;

    public RemoteWindowsService(@NotNull String serverName, @NotNull String windowsServiceName,
                                @NotNull Duration startTimeout, @NotNull Duration stopTimeout) {
        Parameter.named("computerNameOrIp").withValue(serverName).mustBeNonNull();
        Parameter.named("serviceName").withValue(windowsServiceName).mustBeNonNull();
        Parameter.named("startTimeout").withValue(startTimeout).mustBeNonNull();
        Parameter.named("stopTimeout").withValue(stopTimeout).mustBeNonNull();

        _serverName = serverName;
        _windowsServiceName = windowsServiceName;
        _startTimeout = startTimeout;
        _stopTimeout = stopTimeout;
    }

    private String executeCommand(@NotNull String action) throws WindowsServiceException {
        Parameter.named("action").withValue(action).mustBeNonNull();

        final int SUCCESS = 0;
        final int ALREADY_RUNNING = 1056; // service is running, try to start -> "An instance of the service is already running."
        final int NOT_STARTED = 1062; // service is stopped, try to stop -> "The service has not been started."

        String serverArgument = _serverName.startsWith("\\\\") ? _serverName : "\\\\" + _serverName;
        String[] command = {"cmd.exe", "/c", "sc", serverArgument, action, _windowsServiceName};
        StringBuilder output = new StringBuilder();

        try {
            Process process = new ProcessBuilder(command).start();
            InputStream inputStream = process.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                output.append(line).append("\n");
            }

            int exitCode = process.exitValue();
            if (exitCode != SUCCESS && exitCode != ALREADY_RUNNING && exitCode != NOT_STARTED) {
                String exceptionMessage = "Error running Windows service command.\n\n";

                // Can we provide troubleshooting advice?
                switch (exitCode) {
                    case 1722:
                        // "The RPC server is unavailable."
                        exceptionMessage += "** POSSIBLE CAUSE: Is the server name (" + _serverName + ") misspelled?\n\n";
                }

                exceptionMessage += "Command output:\n" + output;
                throw new WindowsServiceException(exceptionMessage);
            }

            return output.toString();
        } catch (IOException e) {
            throw new WindowsServiceException(e.getMessage(), e);
        }
    }

    public static WindowsServiceState parseStateFromQueryOutput(String queryOutput) {
        Pattern pattern = Pattern.compile("\\bSTATE\\s*:\\s*(\\d+)");
        java.util.regex.Matcher matcher = pattern.matcher(queryOutput);
        if (!matcher.find()) {
            BaseUI.log_Warning("Could not determine Windows service state. Query output was:\n" + queryOutput);
            return WindowsServiceState.UNKNOWN;
        }

        String stateNumber = matcher.group(1);
        switch (stateNumber) {
            case "1":
                return WindowsServiceState.STOPPED;
            case "2":
                return WindowsServiceState.STARTING;
            case "3":
                return WindowsServiceState.STOPPING;
            case "4":
                return WindowsServiceState.RUNNING;
            case "5":
                return WindowsServiceState.RESUMING;
            case "6":
                return WindowsServiceState.PAUSING;
            case "7":
                return WindowsServiceState.PAUSED;
            default:
                BaseUI.log_Warning("Unrecognized Windows service state: " + stateNumber);
                return WindowsServiceState.UNKNOWN;
        }
    }

    @NotNull
    private String getDescription() {
        return "Windows service '" + _windowsServiceName + "' on " + _serverName;
    }

    public WindowsServiceState getState() throws WindowsServiceException {
        String output = executeCommand("query");
        WindowsServiceState result = parseStateFromQueryOutput(output);
        BaseUI.log_Status(getDescription() + " is " + result);
        return result;
    }

    public void makeRunning() throws WindowsServiceException {
        WindowsServiceState startingState = getState();

        switch(startingState){
            case RUNNING:
                //do nothing
                break;
            case STOPPED:
                start();
                break;
            case PAUSED:
                resume();
                break;
            case STARTING:
                waitForState(WindowsServiceState.RUNNING, _startTimeout);
                break;
            case STOPPING:
                waitForState(WindowsServiceState.STOPPED, _startTimeout);
                start();
                break;
            case PAUSING:
                waitForState(WindowsServiceState.PAUSED, _startTimeout);
                resume();
                break;
            case RESUMING:
                waitForState(WindowsServiceState.RUNNING, _startTimeout);
                break;
            case UNKNOWN:
                throw new WindowsServiceException("Unknown State, needs logic added.");
        }
    }

    public void makeStopped() throws WindowsServiceException {
        WindowsServiceState startingState = getState();

        switch(startingState){
            case RUNNING:
                stop();
                break;
            case STOPPED:
                //do nothing
                break;
            case PAUSED:
                resume();
                stop();
                break;
            case STARTING:
                waitForState(WindowsServiceState.RUNNING, _startTimeout);
                stop();
                break;
            case STOPPING:
                waitForState(WindowsServiceState.STOPPED, _startTimeout);
                break;
            case PAUSING:
                waitForState(WindowsServiceState.PAUSED, _startTimeout);
                resume();
                stop();
                break;
            case RESUMING:
                waitForState(WindowsServiceState.RUNNING, _startTimeout);
                stop();
                break;
            case UNKNOWN:
                throw new WindowsServiceException("Unknown State, needs logic added.");
        }
    }

    public void makePaused() throws WindowsServiceException {
        WindowsServiceState startingState = getState();

        switch(startingState){
            case RUNNING:
                pause();
                break;
            case STOPPED:
                start();
                pause();
                break;
            case PAUSED:
                //do nothing
                break;
            case STARTING:
                waitForState(WindowsServiceState.RUNNING, _startTimeout);
                pause();
                break;
            case STOPPING:
                waitForState(WindowsServiceState.STOPPED, _startTimeout);
                start();
                pause();
                break;
            case PAUSING:
                waitForState(WindowsServiceState.PAUSED, _startTimeout);
                break;
            case RESUMING:
                waitForState(WindowsServiceState.RUNNING, _startTimeout);
                pause();
                break;
            case UNKNOWN:
                throw new WindowsServiceException("Unknown State, needs logic added.");
        }
    }


    public void start() throws WindowsServiceException {
        BaseUI.log_Status("Starting " + getDescription());
        executeCommand("start");
        waitForState(WindowsServiceState.RUNNING, _startTimeout);
    }

    public void stop() throws WindowsServiceException {
        BaseUI.log_Status("Stopping " + getDescription());
        executeCommand("stop");
        waitForState(WindowsServiceState.STOPPED, _stopTimeout);
    }

    public void resume() throws WindowsServiceException {
        BaseUI.log_Status("Resuming " + getDescription());
        executeCommand("continue");
        waitForState(WindowsServiceState.RUNNING, _startTimeout);
    }


    public void pause() throws WindowsServiceException {
        BaseUI.log_Status("Pausing " + getDescription());
        executeCommand("pause");
        waitForState(WindowsServiceState.PAUSED, _startTimeout);
    }



    private void waitForState(WindowsServiceState expectedState, Duration timeout) {
        Wait.atMost(timeout).until(
                "State of " + getDescription(),
                this::getState,
                Is.equalTo(expectedState));
    }

    public void verifyState(org.hamcrest.Matcher<WindowsServiceState> condition) throws WindowsServiceException {
        WindowsServiceState actualState = getState();
        BaseUI.assertThat("State of " + getDescription(), actualState, condition);
    }

    public void validateWindowServiceState_MatchesExpected(WindowsServiceState expectedState) throws Exception {
        verifyState(Is.equalTo(expectedState));
    }
}
