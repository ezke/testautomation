package utils;

import org.jetbrains.annotations.NotNull;

public class Parameter {
    private Parameter() {
    }

    public static ParameterName named(@NotNull String name) {
        return new ParameterName(name);
    }

    public static class ParameterName {
        @NotNull
        private String name;

        ParameterName(@NotNull String name) {
            this.name = name;
        }

        public <T> ParameterWithValue<T> withValue(T value) {
            return new ParameterWithValue<>(name, value);
        }
    }

    public static class ParameterWithValue<T> {
        @NotNull
        private String name;
        private T value;

        ParameterWithValue(@NotNull String name, T value) {
            this.name = name;
            this.value = value;
        }

        public void mustBeNonNull() {
            if (value == null) {
                throw new IllegalArgumentException("Parameter '" + name + "' requires a non-null value, but was null");
            }
        }
    }
}
