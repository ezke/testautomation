package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class ResultWriter {
	private static int resultNumber = 0;

	public static void checkForFailureAndScreenshot(ITestResult result) throws Exception {
		if (result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) {
			saveScreenshotOnError(result, result.getName(), resultNumber);
		}
		resultNumber++;
	}

	public static void checkForFailureAndScreenshot(ITestResult result, Consumer<String> getScreenshot)
			throws Exception {
		if (result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) {
			saveScreenshotOnError(result, result.getName(), resultNumber, getScreenshot);
		}
		resultNumber++;
	}

	public static String saveScreenshotOnError(ITestResult result, String testNamePlaceholderForFileName,
			int resultNumber) throws Exception {
		return saveScreenshotOnError(result, testNamePlaceholderForFileName, resultNumber, (filePath) -> {
			if (Browser.driver == null) {
				BaseUI.log_Warning("Driver was null.");
			} else {
				File destination = new File(filePath);
				try {

					try {
						if (Browser.driver.equals("winium")) {
							File scrFile = ((TakesScreenshot) Browser.driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile, destination);
						} else {
							ScreenShot.makeFullScreenshot(Browser.driver, destination.getAbsolutePath());
						}
					} catch (InterruptedException e) {
						System.out.println("Encountered error while taking screenshot: " + e.getMessage());
					} catch (org.openqa.selenium.UnhandledAlertException | org.openqa.selenium.JavascriptException e2) {
						// Seeing issues with screenshots around Unhandled alerts with the
						// makeFullScreenshot option. Attempting to fix that here by reverting to non-full screenshot option.
						File scrFile = ((TakesScreenshot) Browser.driver).getScreenshotAs(OutputType.FILE);
						try {
							FileUtils.copyFile(scrFile, destination);
						} catch (IOException e3) {
							// TODO Auto-generated catch block
							e3.printStackTrace();
						}
					}
					// File scrFile = ((TakesScreenshot)
					// Browser.driver).getScreenshotAs(OutputType.FILE);
					// FileUtils.copyFile(scrFile, destination);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

	}

	public static String saveScreenshotOnError(ITestResult result, String testNamePlaceholderForFileName,
			int resultNumber, Consumer<String> getScreenshot) throws Exception {
		String className = result.getTestClass().getName();
		String testStartTime = SystemVariables.testStartTime;

		Date todaysDate = Calendar.getInstance().getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		String strTodayDate = dateFormat.format(todaysDate);

		String path = "C:\\DAF\\screenshots\\" + strTodayDate + "\\";

		path = path + className + "\\";
		File file = new File(path);
		file.mkdirs();

		String fullScreenshotPath = path + testNamePlaceholderForFileName + "_" + testStartTime + "_SS_" + resultNumber
				+ ".jpg";

		// Augmenter augmenter = new Augmenter();
		// TakesScreenshot ts = (TakesScreenshot) augmenter.augment(Browser.driver);
		// screenshot = ts.getScreenshotAs(OutputType.BYTES);
		// FileUtils.writeByteArrayToFile(file, screenshot);
		//

		getScreenshot.accept(fullScreenshotPath);

		String message = "Screenshot: " + "path=" + fullScreenshotPath + "; " + "class=" + className + "; " + "method="
				+ result.getName() + describeParameterList(result.getParameters());

		System.out.println(message);
		Reporter.log(message);

		ScreenshotCleanup("C:\\DAF\\screenshots\\");

		return fullScreenshotPath;
	}

	private static String describeParameterList(Object[] parameters) {
		if (parameters == null) {
			return "()";
		}

		final Stream<CharSequence> strings = Arrays.stream(parameters).map(p -> describeParameter(p));
		return "(" + String.join(", ", strings::iterator) + ")";
	}

	private static String describeParameter(Object parameter) {
		return parameter == null ? "null" : quote(parameter.toString());
	}

	private static String quote(String value) {
		final String backslash = "\\";
		final String quote = "\"";

		final String escapedContent = value.toString().replace(backslash, backslash + backslash).replace(quote,
				backslash + quote);
		return quote + escapedContent + quote;
	}

	public static String saveScreenshot(String screenshotName, String screenshotPath) throws IOException {
		Date todaysDate = Calendar.getInstance().getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yy_hh-mm");
		String timestamp = dateFormat.format(todaysDate);
		screenshotName = screenshotName + "-" + timestamp;
		screenshotPath += screenshotName + ".png";

		File destination = new File(screenshotPath);

		try {

			if(Browser.driver != null && Browser.currentBrowser.equals("winium")){
				File scrFile = ((TakesScreenshot) Browser.driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, destination);
			}else if(Browser.driver != null) {
				ScreenShot.makeFullScreenshot(Browser.driver, destination.getAbsolutePath());
			}
		} catch (InterruptedException e) {
			System.out.println("Encountered error while taking screenshot: " + e.getMessage());
		}

		return screenshotPath;
	}

	// Cleans up Screenshot folder after so many days. Currently set to 7 days.
	private static void ScreenshotCleanup(String ScreenshotFolder) throws Exception {
		// Code to cleanup all folders over 7 days old. Will eventually do this from
		// SharedFolderLocation.
		ArrayList<String> directories = new ArrayList<String>();

		File[] directoriesFromPath = new File(ScreenshotFolder).listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory();
			}
		});

		for (File directory : directoriesFromPath) {
			directories.add(directory.toString());
		}

		for (String directory : directories) {
			File folder = new File(directory);

			// How many days back the folder was modified from today's date.
			long dateDifference = new Date().getTime() - folder.lastModified();

			// After these days are passed we will delete the folder.
			int daysPassed = 7;
			int daysToDeleteBy = daysPassed * 24 * 60 * 60 * 1000;
			if (dateDifference > daysToDeleteBy) {
				FileUtils.deleteDirectory(new File(directory));
			}

		}
	}

}
