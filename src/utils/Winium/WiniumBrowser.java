package utils.Winium;

public class WiniumBrowser {



    private String _serverName;
    private int _port;
    private String _applicationPath;
    private String _applicationArguments;


    public WiniumBrowser(String serverName, int port, String applicationPath, String applicationArguments)
    {
        _serverName = serverName;
        _port = port;
        _applicationPath = applicationPath;
        _applicationArguments = applicationArguments;

    }

    public String get_serverName() {
        return _serverName;
    }

    public int get_port() {
        return _port;
    }

    public String get_applicationPath() {
        return _applicationPath;
    }

    public String get_applicationArguments() {
        return _applicationArguments;
    }

    public String getWiniumServerString(){
        return "http://" + _serverName + ":" + String.valueOf(_port);
        //"http://rps602auto1sql.qalabs.nwk:9999"
    }


}
