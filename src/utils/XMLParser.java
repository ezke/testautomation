package utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.File;


public class XMLParser {

	private Document xml;
	private String filePath;

	public XMLParser(String filePathAndName) throws Exception {
		xml = createXML(filePathAndName);
		filePath = filePathAndName;
	}

	public static XMLParser getXMLParserWithContent(String filePathAndName, Document xml) {
		return new XMLParser(filePathAndName, xml);
	}

	private XMLParser(String filePathAndName, Document xml) {
		filePath = filePathAndName;
		this.xml = xml;
	}

	private Document createXML(String xmlFileName) throws Exception {

		File xmlFile = new File(xmlFileName);
		Document xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
		xmlDoc.getDocumentElement().normalize();

		return xmlDoc;
	}

	public void append_CDataNode(Node nodeToAppendTo, String textToAppend) {
		Node cdata = xml.createCDATASection(textToAppend);
		nodeToAppendTo.appendChild(cdata);
	}

	public NodeList findNodes(String xpathToEvaluate) throws Exception {
		XPath newXpath = XPathFactory.newInstance().newXPath();
		NodeList nodes = (NodeList) newXpath.evaluate(xpathToEvaluate, xml.getDocumentElement(),
				XPathConstants.NODESET);
		return nodes;
	}

	public Node findNode(String xpathToEvaluate) throws Exception {
		XPath newXpath = XPathFactory.newInstance().newXPath();
		Node node = (Node) newXpath.evaluate(xpathToEvaluate, xml.getDocumentElement(), XPathConstants.NODE);
		return node;
	}

	public void createNode(Node nodeToAppendTo, String nodeType) {
		Element newNode = xml.createElement(nodeType);
		nodeToAppendTo.appendChild(newNode);
	}

	public void applyUpdates_ToXML() throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(xml);
		StreamResult result = new StreamResult(new File(filePath));
		transformer.transform(source, result);
	}

	public String getAttribute(Node nodeToCheck, String attributeToGet) {
		Element elementToSet = (Element) nodeToCheck;
		String attribute = elementToSet.getAttribute(attributeToGet);

		return attribute;
	}

	public void setAttribute(Node nodeToUpdate, String attributeToUpdate, String valueToSet) {
		Element elementToSet = (Element) nodeToUpdate;
		elementToSet.setAttribute(attributeToUpdate, valueToSet);

	}

	public NodeList getElementsByTagName(String tagName){

		return xml.getElementsByTagName(tagName);
	}

	// private HashMap<String, String> getImportInputPathes(String configFilename,
	// String settingSection)
	// throws ParserConfigurationException, SAXException, IOException,
	// XPathExpressionException {
	// HashMap<String, String> paths = new HashMap<String, String>();
	//
	// XPath xpath = XPathFactory.newInstance().newXPath();
	//
	// NodeList nodes = (NodeList) xpath.evaluate(settingSection,
	// xmlDoc.getDocumentElement(), XPathConstants.NODESET);
	// // for(int index=0; index < nodes.getLength(); index++) {
	// // Element setting = (Element)nodes.item(index);
	// // paths.put(
	// // setting.getAttribute("ClientProcessCode"),
	// // pathToUnc(setting.getAttribute("InputFolder"))
	// // );
	// // }
	// return paths;
	// }

}
