//// LET 12/14/15: Browser.java
//// Purpose: Contains actions for dealing with opening/closing browsers. The open action specifies a default browser to use
//// but can be overridden by setting a value in testng or ant files.
//// The warning messages in this file can be ignored. 
package utils;

import java.io.File;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.Reporter;

import utils.Winium.WiniumBrowser;

//import shopDeluxe.pages.GlobalVariables;

public class Browser {

    public static WebDriver driver;
    public static WebDriverWait wait;
    public static int defaultWaitTime = 10;
    public static int defaultImplicitWaitTime = 30;
    public static String typeOfBrowser = "desktop";
    //public static String defaultBrowser = "internetexplorer";
    public static String defaultBrowser = "chrome";
    // public static String currentBrowser = System.getProperty("browser") !=
    // null ? System.getProperty("browser") : defaultBrowser;
    public static String currentBrowser = "";

    public static String downloadLocation = "c:\\DAF\\downloads";



    public static WebDriver openWiniumDriver(WiniumBrowser winBrowser) throws Exception {
        currentBrowser = "winium";

        DesktopOptions deskOption = new DesktopOptions();
        deskOption.setApplicationPath(winBrowser.get_applicationPath());
        String arguments = winBrowser.get_applicationArguments();
        if(arguments != null) {
            deskOption.setArguments(arguments);
        }
        deskOption.setDebugConnectToRunningApp(false);
        deskOption.setLaunchDelay(2);

        return driver = new WiniumDriver(new URL(winBrowser.getWiniumServerString()), deskOption);
    }

    // Open a browser to a particular URL. Defaults to the value set in the
    // string above unless overridden.
    // Note that this uses RemoteWebDriver and depends on Selenium-Standalone
    // being up and running.
    // The value here can also be passed in as a variable via Ant/Jenkins
    public static WebDriver openBrowser(String url, String browserToUse) throws Exception {
        currentBrowser = browserToUse;

        try {
            String bundleNumber = System.getProperty("bundle");
            if (bundleNumber != null) {
                // if (url.contains("bags")) {
                // bagsAndBows.pages.GlobalVariables.mobileBundleID =
                // bundleNumber;
                // bagsAndBows.pages.GlobalVariables.mobileURLSuffix =
                // bagsAndBows.pages.GlobalVariables.mobileURLSuffix
                // .replaceFirst("\\d\\d\\d", bundleNumber);
                // bagsAndBows.pages.GlobalVariables.mobilePreview =
                // bagsAndBows.pages.GlobalVariables.mobilePreview
                // .replaceFirst("\\d\\d\\d", bundleNumber);
                // System.out.println("Bundle is set to " +
                // bagsAndBows.pages.GlobalVariables.mobileBundleID);
                // System.out.println("Suffix is " +
                // bagsAndBows.pages.GlobalVariables.mobileURLSuffix);
                // System.out.println("Preview is " +
                // bagsAndBows.pages.GlobalVariables.mobilePreview);
                // } else {
                // GlobalVariables.mobileBundleID = bundleNumber;
                // GlobalVariables.mobileURLSuffix =
                // GlobalVariables.mobileURLSuffix.replaceFirst("\\d\\d\\d",
                // bundleNumber);
                // GlobalVariables.mobilePreview =
                // GlobalVariables.mobilePreview.replaceFirst("\\d\\d\\d",
                // bundleNumber);
                // System.out.println("Bundle is set to " +
                // GlobalVariables.mobileBundleID);
                // System.out.println("Suffix is " +
                // GlobalVariables.mobileURLSuffix);
                // System.out.println("Preview is " +
                // GlobalVariables.mobilePreview);
                // }

            }
        } catch (NullPointerException e) {
            Reporter.log("No mobile bundle set");
        }

        BaseUI.log_Status("Launching " + browserToUse + " browser.");
        DesiredCapabilities capa = new DesiredCapabilities();
        switch (browserToUse) {
            case "firefox":
                // WebDriver driver = new FirefoxDriver();
                System.setProperty("webdriver.gecko.driver", "c:\\Selenium\\geckodriver.exe");
                capa = DesiredCapabilities.firefox();

                if (url.contains("rechubqaweb02.qalabs.nwk") || url.contains("rpsqaaut3.qalabs.nwk")
                        || url.contains("nextgenblack3.qalabs.nwk") || url.contains("rps602auto1web.qalabs.nwk/Framework")) {
                    capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                    capa.setCapability("marionette", false);

                    // ProfilesIni profile = new ProfilesIni();

                    // FirefoxProfile firefoxProfile = profile.getProfile("default");
                    FirefoxProfile firefoxProfile = new FirefoxProfile();
                    firefoxProfile.setPreference("browser.download.folderList", 2);
                    firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
                    firefoxProfile.setPreference("browser.download.dir", downloadLocation);
                    firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");
                    firefoxProfile.setAcceptUntrustedCertificates(true);
                    firefoxProfile.setAssumeUntrustedCertificateIssuer(true);

                    // Code to clear out Firefox cache
                    // firefoxProfile.setPreference("browser.cache.disk.enable", false);
                    // firefoxProfile.setPreference("browser.cache.memory.enable",
                    // false);
                    // firefoxProfile.setPreference("browser.cache.offline.enable",
                    // false);
                    // firefoxProfile.setPreference("network.http.use-cache", false);

                    capa.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
                }

                // Do not uncomment this line
                // driver = new FirefoxDriver(capa);
                break;
            case "chrome":
                // Need to disable flash for testing the configurator
                System.setProperty("webdriver.chrome.driver", "c:\\Selenium\\chromedriver.exe");
                capa = DesiredCapabilities.chrome();
                ChromeOptions options = new ChromeOptions();
                Map<String, Object> preferences = new Hashtable<String, Object>();
                // disable flash and the PDF viewer
                preferences.put("plugins.plugins_disabled", new String[]{"Adobe Flash Player"});
                preferences.put("download.default_directory", downloadLocation);

                options.setExperimentalOption("prefs", preferences);
                String client = System.getProperty("clientName");
//                if (client != null && client.startsWith("AARP")) {
//                    options.addExtensions(new File("c:\\Selenium\\AdBlock_v3.16.0.crx"));
//                }
                options.addArguments("--disable-plugins");
                options.addArguments("--disable-bundled-ppapi-flash");
                options.addArguments("--disable-internal-flash");
                options.addArguments("--disable-plugins-discovery");
                options.addArguments("--start-maximized");
                capa.setCapability(ChromeOptions.CAPABILITY, options);

                if(url.contains("ie-dev.deluxe.com") || url.contains("datamyx.com")) {
                    driver = new ChromeDriver(capa);
                }
                // Do not uncomment this line
                // driver = new ChromeDriver(capa);
                break;
            case "internetexplorer":
                System.setProperty("webdriver.ie.driver", "c:\\Selenium\\IEDriverServer.exe");
                //System.setProperty("webdriver.ie.driver.extractpath", "c:\\IEDriverInfo\\");
                //System.setProperty("webdriver.ie.logfile", "c:\\IEDriverInfo\\IEDriverServer.log");

                // LoggingPreferences logs = new LoggingPreferences();
                // logs.enable(LogType.BROWSER, Level.ALL);
                // logs.enable(LogType.CLIENT, Level.ALL);
                // logs.enable(LogType.DRIVER, Level.ALL);
                // logs.enable(LogType.PERFORMANCE, Level.ALL);
                // logs.enable(LogType.PROFILER, Level.ALL);
                // logs.enable(LogType.SERVER, Level.ALL);
                //
                // InternetExplorerDriverService.Builder service =
                // new InternetExplorerDriverService.Builder();
                // service =
                // service.withLogLevel(InternetExplorerDriverLogLevel.TRACE);
                // service = service.withLogFile(new
                // File("c:\\logs\\selenium.log"));

                capa = DesiredCapabilities.internetExplorer();
                // capa.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                // true);

                // capa.setCapability("ignoreZoomSetting", true);
                // capa.setCapability("ie.ensureCleanSession", true);
                if (url.contains("rpsqaaut3.qalabs.nwk") || url.contains("rechubqaweb02.qalabs.nwk") || url.contains("nextgenblack3.qalabs.nwk")) {
                    capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                    capa.setCapability("enablePersistentHover", false);

                    // This will hopefully fix issue where typing doesn't work.
                    capa.setCapability("requireWindowFocus", false);

                    // Setting to false fixed issue where browser would get stuck on
                    // starting page.
                    capa.setCapability("ie.ensureCleanSession", true);
                    // capa.setCapability("initialBrowserUrl",
                    // "https://www.google.com/");
                    // capa.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,
                    // "https://www.google.com/");
                    capa.setCapability("browserstack.ie.noFlash", "true");
                    capa.setCapability("browserstack.bfcache", "0");
                    // capa.setCapability(CapabilityType.LOGGING_PREFS, logs);
                } else if (url.contains("bankersdashboard.com")) {
                    //capa.setCapability("enablePersistentHover", false);
                    //capa.setCapability("requireWindowFocus", true);
                    //capa.setCapability("browserstack.bfcache", "0");
                    //capa.setCapability("ie.ensureCleanSession", true);
                    BaseUI.log_Status("Setting browser settings for Bankers Dashboard");
                    capa.setCapability("ignoreZoomSetting", true);
                    capa.setCapability("browserstack.ie.noFlash", true);
                    capa.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                    capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                } else {
                    capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                    capa.setCapability("enablePersistentHover", false);
                    capa.setCapability("requireWindowFocus", false);
                    capa.setCapability("ie.ensureCleanSession", true);
                    capa.setCapability("browserstack.ie.noFlash", true);
                    capa.setCapability("browserstack.bfcache", "0");
                }
                // capa.setCapability(CapabilityType.LOGGING_PREFS, logs);

                // Do not uncomment this line
                // driver = new InternetExplorerDriver(capa);

                // capa = DesiredCapabilities.internetExplorer();
                // System.setProperty("webdriver.ie.driver",
                // "c:\\Selenium\\IEDriverServer.exe");
                // capa.setCapability("nativeEvents", false);
                // capa.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                // true);
                // capa.setCapability(InternetExplorerDriver.IE_SWITCHES,
                // "-extoff");
                // capa.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS,
                // true);

                // capability.setCapability("ignoreZoomSetting", true);

                //
                // capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                // true);
                //
                // capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

                break;
            case "android":
                // Android uses the chrome driver but is really a chrome package
                // with a different option
                System.setProperty("webdriver.chrome.driver", "c:\\Selenium\\chromedriver.exe");
                capa = DesiredCapabilities.chrome();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("androidPackage", "com.android.chrome");
                capa.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                // Set the mobile type flag. We use this in tests to determine which
                // version of an object to use
                typeOfBrowser = "mobile";
                // Do not uncomment this line
                // driver = new ChromeDriver(capa);
                break;
            // It shouldn't ever make it here, but just in case, launch Firefox
            case "iphonesimulated":
                // Need to disable flash for testing the configurator
                System.setProperty("webdriver.chrome.driver", "c:\\Selenium\\chromedriver.exe");

                capa = DesiredCapabilities.chrome();
                ChromeOptions optionsSimulated = new ChromeOptions();
                optionsSimulated.addArguments("--disable-plugins");
                optionsSimulated.addArguments("--disable-bundled-ppapi-flash");
                optionsSimulated.addArguments("--disable-internal-flash");
                optionsSimulated.addArguments("--disable-plugins-discovery");
                optionsSimulated.addArguments("--start-maximized");
                optionsSimulated.addArguments(
                        "--user-agent=Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5");
                capa.setCapability(ChromeOptions.CAPABILITY, optionsSimulated);
                typeOfBrowser = "mobile";
                // Do not uncomment this line
                // driver = new ChromeDriver(capa);
                break;
            case "chromeHeadless":
                System.setProperty("webdriver.chrome.driver", "c:\\Selenium\\chromedriver.exe");
                options = new ChromeOptions();

                options.addArguments("--headless");
                options.addArguments("window-size=1920,1080");
                options.addArguments("--disable-plugins");
                options.addArguments("--disable-bundled-ppapi-flash");
                options.addArguments("--disable-internal-flash");
                options.addArguments("--disable-plugins-discovery");
                options.addArguments("--start-maximized");

                options.addArguments("--proxy-server='direct://'");
                options.addArguments("--proxy-bypass-list=*");

                ChromeDriverService driverService = ChromeDriverService.createDefaultService();
                RemoteWebDriver chromeDriver = new ChromeDriver(driverService, options);

                driver = chromeDriver;

                Map<String, Object> commandParams = new HashMap<>();
                commandParams.put("cmd", "Page.setDownloadBehavior");
                Map<String, String> params = new HashMap<>();
                params.put("behavior", "allow");
                params.put("downloadPath", downloadLocation);
                commandParams.put("params", params);
                ObjectMapper objectMapper = new ObjectMapper();
                HttpClient httpClient = HttpClientBuilder.create().build();
                String command = objectMapper.writeValueAsString(commandParams);
                String u = driverService.getUrl().toString() + "/session/" + chromeDriver.getSessionId() + "/chromium/send_command";
                HttpPost request = new HttpPost(u);
                request.addHeader("content-type", "application/json");
                request.setEntity(new StringEntity(command));
                httpClient.execute(request);
                break;
            default:
                capa = DesiredCapabilities.firefox();
                // Do not uncomment this line
                // driver = new FirefoxDriver(capa);
        }
        // Use below line for grid support
        // driver = new RemoteWebDriver(new
        // URL("http://168.135.43.181:4444/wd/hub"), capa);
        if((url.contains("ie-dev.deluxe.com")
                || (url.contains("datamyx.com")) && currentBrowser.equals("chrome"))
                || currentBrowser.equals("chromeHeadless")
                ) {
            //Setting this in Chrome section under these conditions to not use RemoteWebDriver.
        }
        else {
            driver = new RemoteWebDriver(capa);
        }

        driver.manage().timeouts().implicitlyWait(defaultImplicitWaitTime, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, defaultWaitTime);

        if (typeOfBrowser == "mobile") {
            // if (url.contains("bags")) {
            // driver.get(bagsAndBows.pages.GlobalVariables.mobilePreview);
            // } else {
            // driver.get(GlobalVariables.mobilePreview);
            // }
            // Thread.sleep(4000);
            // BaseUI.clickMobifyPreviewButton();
        } else {
            driver.get(url);
        }

        Thread.sleep(6000);
        // Set cookies to avoid popup for SD only
        if (url.contains("sf-stage.deluxe.com") && typeOfBrowser == "desktop") {
            Cookie newCookie = new Cookie("QSI_SI_e9i7rRec1VN9AnH_intercept", "true", "/");
            driver.manage().addCookie(newCookie);
            newCookie = new Cookie("QSI_SI_6PrLmNvnEeCtAnb_intercept", "true", "/");
            driver.manage().addCookie(newCookie);
            driver.manage().window().maximize();
            Thread.sleep(1000);
            driver.get(url);
            Thread.sleep(6000);
        }
        // else if (defaultBrowser.equals("internetexplorer") &&
        // url.equals("https://rechubqaweb02.qalabs.nwk/Framework/")) {
        // BaseUI.click_js(Locator.lookupElement("ie_ContinueToWebsite"));
        // Thread.sleep(1500);
        //
        // }
        defaultBrowser = browserToUse;

        driver.switchTo().window(driver.getWindowHandle());

        Thread.sleep(500);
        BaseUI.log_Status("Opened " + url + " with " + defaultBrowser);

        return driver;
    }

    public static void changeDriver(WebDriver newDriver, String browser) {
        driver = newDriver;
        currentBrowser = browser;
    }

    public static String selectProperBrowser() {
        String browserToUse = defaultBrowser;
        try {
            browserToUse = System.getProperty("browser");
            if (browserToUse == null) {
                browserToUse = defaultBrowser;
            }
        }
        catch (NullPointerException e) {
            BaseUI.log_Status("Browser not set, using default");
        }
        return browserToUse;
    }

    public static WebDriver openBrowser(String url) throws Exception {
        return openBrowser(url, selectProperBrowser());
    }

    public static void closeBrowser() {
        if (driver != null) {
            driver.quit();
            driver = null;
            BaseUI.log_Status("Closed browser");
        }
    }

    public static void close_CurrentBrowserWindow() {
        driver.close();
        driver = null;
        BaseUI.log_Status("Closed current browser window");
    }

    // Clicks the back button on every browser type.
    public static void clickBrowserBackButton() {
        driver.navigate().back();
        BaseUI.log_Status("Clicking back button on browser");
    }

    // Navigate to a new page in the current session.
    public static void navigateTo(String urlValue) {
        driver.navigate().to(urlValue);
        BaseUI.log_Status("Navigating to " + urlValue);
    }

    public static void set_ImplicitWait_AndSaveValue(Integer implicitWaitTimeInSeconds) {
        defaultImplicitWaitTime = implicitWaitTimeInSeconds;
        BaseUI.log_Status("Set Implicit Wait time to " + implicitWaitTimeInSeconds.toString());
        driver.manage().timeouts().implicitlyWait(implicitWaitTimeInSeconds, TimeUnit.SECONDS);
    }

    //Do NOT use this method if you plan to run multiple jobs on the same vm via Jenkins using multiple executors/nodes.
    public static void kill_ExcessProcesses() throws Exception {

        // Process process = Runtime.getRuntime().exec("tasklist.exe");
        Process process = Runtime.getRuntime().exec("tasklist.exe");
        Scanner scanner = new Scanner(new InputStreamReader(process.getInputStream()));
        while (scanner.hasNext()) {
            String scannerText = scanner.nextLine();
            System.out.println(scannerText);

            if (scannerText != null) {

                if (scannerText.startsWith("chrome.exe")) {
                    Runtime.getRuntime().exec("taskkill /F /T /IM chrome.exe");
                }
                if (scannerText.startsWith("chromedriver.exe")) {
                    Runtime.getRuntime().exec("taskkill /F /T /IM chromedriver.exe");
                }
                if (scannerText.startsWith("iexplore.exe")) {
                    Runtime.getRuntime().exec("taskkill /F /T /IM iexplore.exe");
                }
                if (scannerText.startsWith("IEDriverServer.exe")) {
                    Runtime.getRuntime().exec("taskkill /F /T /IM IEDriverServer.exe");
                }

                if (scannerText.startsWith("firefox.exe")) {
                    Runtime.getRuntime().exec("taskkill /F /T /IM firefox.exe");
                }

                // taskkill /IM "process_name" /T /F
            }
        }
        scanner.close();
    }
}// End of Class
