package utils;

import org.testng.annotations.Test;

public class DiskCleanup {

	@Test
	public void run_DiskCleanup() throws Exception {
		FileOperations.cleanup_dafTestUser();
		FileOperations.cleanup_TempFolder();
	}

}
