package utils;

public class BustedTestError extends Error {
    public BustedTestError(String message) {
        super(message);
    }
}
