package utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.text.WordUtils;
import org.jetbrains.annotations.Nullable;

public class TableData {

	public ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

	public TableData(ArrayList<HashMap<String, String>> newData) {
		data = newData;
	}

	public TableData() {
	}

	public Integer columnCount() {
		Integer count = data.get(0).size();

		return count;
	}

	public Integer rowCount() {
		Integer count = data.size();

		return count;
	}

	public Integer countOf_Matches_ThatHave_ColumnName_AndCellValue(String columnName, String expectedValue) {
		Integer count_OfMatches = 0;

		for (Integer i = 0; i < rowCount(); i++) {
			if (data.get(i).get(columnName).equals(expectedValue)) {
				count_OfMatches++;
			}
		}

		return count_OfMatches;
	}

	public Integer countOf_Matches_ThatHave_matchingHashMapValues(HashMap<String, String> matchingValues) {
		Integer count_OfMatches = 0;

		for (Integer i = 0; i < rowCount(); i++) {
			boolean rowMatchesExpectedValues = true;
			for (String key : matchingValues.keySet()) {
				if (!data.get(i).get(key).equals(matchingValues.get(key))) {
					rowMatchesExpectedValues = false;
				}
			}
			if (rowMatchesExpectedValues) {
				count_OfMatches++;
			}
		}

		return count_OfMatches;
	}

	public void logTable() {
		for (HashMap<String, String> row : data) {
			BaseUI.log_Status("Table row: " + row.toString());

		}
	}

	public void verify_TableSize(Integer expectedSize) {
		BaseUI.verify_true_AndLog(data.size() == expectedSize, "Rows matched count of " + expectedSize.toString(),
				"Expecting " + expectedSize.toString() + " rows. But seeing " + String.valueOf(data.size()) + " rows.");

	}

	// Pass in the column name and the expected count. Will check that column to see
	// how many unique values are in that column.
	public void verify_CountOfUniqueColumnValues(int expectedCount, String columnToCheck) {
		Set<String> columnValues = new HashSet<String>();

		for (HashMap<String, String> row : data) {
			columnValues.add(row.get(columnToCheck));
		}

		BaseUI.verify_true_AndLog(columnValues.size() == expectedCount,
				"Unique column value count for column " + columnToCheck + " matched expected count of "
						+ String.valueOf(expectedCount),
				"Unique column value count for column " + columnToCheck + " did NOT match expected count of "
						+ String.valueOf(expectedCount) + " seeing " + String.valueOf(columnValues.size()));

	}

	public Integer first_IndexOf(String columnName, String expectedValue) {
		Integer count_OfMatches = null;

		for (Integer i = 0; i < rowCount(); i++) {
			if (data.get(i).get(columnName).equals(expectedValue)) {
				return i;
			}
		}

		return count_OfMatches;
	}

	public Integer first_IndexOf_DoubleMatch(String columnName, String expectedValue, String columnName2,
			String expectedValue2) {
		Integer count_OfMatches = null;

		for (Integer i = 0; i < rowCount(); i++) {
			if (data.get(i).get(columnName).equals(expectedValue)
					&& data.get(i).get(columnName2).equals(expectedValue2)) {
				return i;
			}
		}

		return count_OfMatches;
	}

	public HashMap<String, String> dataRow(int i) {

		return data.get(i);
	}

	public BigDecimal return_Sum_ofColumn(String columnName) {
		BigDecimal total_amount = new BigDecimal(0.0);

		for (HashMap<String, String> dataRow : data) {
			String formatedColumn = dataRow.get(columnName).replace(",", "");
			formatedColumn = formatedColumn.replace("$", "").replace(",", "");
			BigDecimal total = new BigDecimal(formatedColumn);
			total_amount = total_amount.add(total);
		}

		return total_amount;

	}

	@Nullable
	public HashMap<String, String> return_Row_BasedOn_1MatchingField(String key1, String expectedValue1) {
		for (HashMap<String, String> row : data) {
			String value1 = row.get(key1);

			if (value1 != null && value1.equals(expectedValue1)) {
				return row;
			}
		}

		return null;
	}

	public HashMap<String, String> return_ClonedRow(HashMap<String, String> rowToClone) {

		return BaseUI.cloneHashMap(rowToClone);
	}

	public HashMap<String, String> return_Row_BasedOn_2MatchingFields(String key1, String expectedValue1, String key2,
			String expectedValue2) {
		for (HashMap<String, String> row : data) {
			String value1 = row.get(key1);
			String value2 = row.get(key2);

			if (value1 != null && value2 != null && value1.equals(expectedValue1) && value2.equals(expectedValue2)) {
				return row;
			}
		}

		return null;
	}

	public HashMap<String, String> return_Row_BasedOn_AnyNumberOfMatchingFields(HashMap<String, String> fieldsToMatch)
			throws Exception {
		for (HashMap<String, String> row : data) {

			boolean match = true;

			for (String key : fieldsToMatch.keySet()) {
				String rowValue = row.get(key);

				if (rowValue == null || !rowValue.equals(fieldsToMatch.get(key))) {
					match = false;
				}
			}

			if (match) {
				return row;
			}
		}

		return null;
	}

	public TableData return_Table_BasedOn_AnyNumberOfMatchingFields(HashMap<String, String> fieldsToMatch)
			throws Exception {

		TableData newTable = new TableData();

		for (HashMap<String, String> row : data) {

			boolean match = true;

			for (String key : fieldsToMatch.keySet()) {
				String rowValue = row.get(key);

				if (rowValue == null || !rowValue.equals(fieldsToMatch.get(key))) {
					match = false;
				}
			}

			if (match) {
				newTable.data.add(row);
			}
		}

		return newTable;
	}

	public Integer return_RowIndex_BasedOn_2MatchingFields(String key1, String expectedValue1, String key2,
			String expectedValue2) {
		Integer rowIndex = null;

		for (Integer i = 0; i < data.size(); i++) {
			if (data.get(i).get(key1).equals(expectedValue1) && data.get(i).get(key2).equals(expectedValue2)) {
				rowIndex = i;
			}
		}

		if (rowIndex!= null){
			return rowIndex;
		}else{
			throw new IllegalStateException("No match found");
		}
	}

	public int return_RowIndex_BasedOn_1MatchingFields(String key1, String expectedValue1) {
		Integer rowIndex = null;

		for (Integer i = 0; i < data.size(); i++) {
			if (data.get(i).get(key1).equals(expectedValue1)) {
				rowIndex = i;
			}
		}
		if (rowIndex!= null){
			return rowIndex;
		}else{
			throw new IllegalStateException("No match found");
		}
	}

	public int return_RowIndex_BasedOn_AnyNumberOfMatchingFields(Map<String, String> fieldsToMatch) {

		Integer rowIndex = null;

		for (int i = 0; i<data.size(); i++) {

			boolean match = true;
			HashMap<String, String> row = dataRow(i);

			for (String key : fieldsToMatch.keySet()) {
				String rowValue = row.get(key);

				if (rowValue == null || !rowValue.equals(fieldsToMatch.get(key))) {
					match = false;
				}
			}

			if (match) {
				rowIndex = i;
				break;
			}

		}
		if (rowIndex!= null){
			return rowIndex.intValue();
		}else{
			throw new IllegalStateException("No match found");
		}
	}

	// Removes duplicate rows for given key
	public void remove_DuplicateRows_ForPassedInColumnName(String columnName) {
		ArrayList<String> uniqueKeyValues = new ArrayList<String>();

		for (Iterator<HashMap<String, String>> iterator = data.iterator(); iterator.hasNext();) {
			HashMap<String, String> tableRow = iterator.next();
			if (uniqueKeyValues.contains(tableRow.get(columnName))) {
				iterator.remove();
			} else {
				uniqueKeyValues.add(tableRow.get(columnName));
			}

		}
	}

	// Removes duplicate rows for given keys. Pass in 2 columns and where there
	// is already a match we remove it.
	public void remove_DuplicateRows_ForPassedInColumnNames(String columnName, String columnName2) {

		ArrayList<String> uniqueValues = new ArrayList<String>();

		for (Iterator<HashMap<String, String>> iterator = data.iterator(); iterator.hasNext();) {
			HashMap<String, String> tableRow = iterator.next();
			if (uniqueValues.contains(tableRow.get(columnName) + tableRow.get(columnName2))) {
				iterator.remove();
			} else {
				uniqueValues.add(tableRow.get(columnName) + tableRow.get(columnName2));
			}

		}
	}

	// Removes rows that are under a certain threshold based on the sum of all
	// the values in that column.
	// So if one cell had a value that was less than .01 percent of the sum of
	// that column, that row will be deleted.
	public void remove_Rows_UnderThreshold_ForColumn(double threshold, String columnName) {
		double total_amount = return_Sum_ofColumn(columnName).doubleValue();

		for (Iterator<HashMap<String, String>> iterator = data.iterator(); iterator.hasNext();) {
			HashMap<String, String> tableRow = iterator.next();
			double row_total = Double.parseDouble(tableRow.get(columnName).replace(",", ""));
			if (((row_total * 100) / total_amount) < threshold) {
				iterator.remove();
				// DDA_Summary.PaymentTotals.uncheck_legendBox(tableRow.get("entity").replace("DDA:
				// ", ""));
			}
		}
	}

	// Remove all rows that are over a certain threshold based on the sum of all
	// the values in that column.
	// So if one cell had a value that was Greater than .01 percent of the sum
	// of that column, that row will be deleted.
	public void remove_Rows_OverThreshold_ForColumn(double threshold, String columnName) {
		double total_amount = return_Sum_ofColumn(columnName).doubleValue();

		for (Iterator<HashMap<String, String>> iterator = data.iterator(); iterator.hasNext();) {
			HashMap<String, String> tableRow = iterator.next();
			double row_total = Double.parseDouble(tableRow.get(columnName).replace(",", ""));
			if (((row_total * 100) / total_amount) > threshold) {
				iterator.remove();
			}
		}
	}

	// Remove all rows that do not have the specified column/value.
	public void remove_Rows_WithColumnName_WhereValues_DoNOTMatch(String columnName, String columnValue) {

		for (Iterator<HashMap<String, String>> iterator = data.iterator(); iterator.hasNext();) {
			HashMap<String, String> tableRow = iterator.next();
			if (!tableRow.get(columnName).equals(columnValue)) {
				iterator.remove();
			}
		}
	}

	// Remove all rows that do not have the specified column/value.
	public void remove_Rows_WithColumnName_WhereValues_Match(String columnName, String columnValue) {

		for (Iterator<HashMap<String, String>> iterator = data.iterator(); iterator.hasNext();) {
			HashMap<String, String> tableRow = iterator.next();
			if (tableRow.get(columnName).equals(columnValue)) {
				iterator.remove();
			}
		}
	}

	// Converts from MM/dd/yyyy format to M/d/yyyy format (Example 09/06/2016
	// becomes 9/6/2016)
	public void replace_DateFormat_shortened() throws ParseException {
		replace_DateFormats("MM/dd/yyyy", "M/d/yyyy");
	}

	// Replaces Old Date Format with New Date Format.
	public void replace_DateFormats(String oldFormat, String newFormat) throws ParseException {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				if (key.toLowerCase().contains("date") && !dataRow.get(key).equals("")) {
					DateFormat fromFormat = new SimpleDateFormat(oldFormat);
					fromFormat.setLenient(false);
					DateFormat toFormat = new SimpleDateFormat(newFormat);
					toFormat.setLenient(false);
					Date date = fromFormat.parse(dataRow.get(key));
					// toFormat.format(date);
					dataRow.put(key, toFormat.format(date));
				}
			}
		}
	}

	// Replaces Old Date Format with New Date Format. This Overload only
	// performs the formatting
	// to a specific passed in column
	public void replace_DateFormats(String columnToFormat, String oldFormat, String newFormat) throws ParseException {
		for (HashMap<String, String> dataRow : data) {
			DateFormat fromFormat = new SimpleDateFormat(oldFormat);
			fromFormat.setLenient(false);
			DateFormat toFormat = new SimpleDateFormat(newFormat);
			toFormat.setLenient(false);
			Date date = fromFormat.parse(dataRow.get(columnToFormat));
			// toFormat.format(date);
			dataRow.put(columnToFormat, toFormat.format(date));
		}
	}

	public void remove_LeadingZeros_fromNumbers() throws ParseException {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				String cellText = dataRow.get(key);
				if (cellText != null && cellText.matches("\\d+")) {
					cellText = cellText.replaceAll("^0+", "");
					dataRow.put(key, cellText);
				}
			}
		}
	}

	public void remove_TrailingZeros_fromNumbers() {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				String cellText = dataRow.get(key);
				if (cellText != null) {
					cellText = cellText.indexOf(".") < 0 ? cellText
							: cellText.replaceAll("0*$", "").replaceAll("\\.$", "");
					dataRow.put(key, cellText);
				}
			}
		}
	}

	public void remove_Character(String characterToRemove) {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				String cellText = dataRow.get(key);
				if (cellText != null) {
					cellText = cellText.replace(characterToRemove, "");
					dataRow.put(key, cellText);
				}
			}
		}
	}

	public TableData cloneTable() {
		TableData newTable = new TableData();
		for (HashMap<String, String> row : data) {
			newTable.data.add(new HashMap<>(row));
		}

		return newTable;
	}

	public void remove_Character_FromKeys(String characterToRemove) {

		ArrayList<String> keyValues = new ArrayList<String>();

		for (ListIterator<HashMap<String, String>> iterator = data.listIterator(); iterator.hasNext();) {

			HashMap<String, String> tableRow = iterator.next();
			HashMap<String, String> updatedTableRow = new HashMap<String, String>();

			for (String key : tableRow.keySet()) {
				updatedTableRow.put(key.replace(characterToRemove, ""), tableRow.get(key));

			}

			iterator.set(updatedTableRow);

		}

	}

	// Overload only removes the character from the column name that is passed
	// in.
	public void remove_Character(String characterToRemove, String columnToRemoveCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToRemoveCharFrom);
			if (cellText != null) {
				cellText = cellText.replace(characterToRemove, "");
				dataRow.put(columnToRemoveCharFrom, cellText);
			}
		}
	}

	public void trim_column(String columnToRemoveCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToRemoveCharFrom);
			if (cellText != null) {
				cellText = cellText.trim();
				dataRow.put(columnToRemoveCharFrom, cellText);
			}
		}
	}

	// This will split line and get the first line
	public void split_column(String columnToSplit) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToSplit);
			if (cellText != null) {
				cellText = cellText.split("\n")[0];
				dataRow.put(columnToSplit, cellText);
			}
		}
	}

	public void split_column(String columnToSplit, String regexToSplit, int intToTake) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToSplit);
			if (cellText != null) {
				cellText = cellText.split(regexToSplit)[intToTake];
				dataRow.put(columnToSplit, cellText);
			}
		}
	}

	public void replace_Character(String characterToReplace, String valueToReplaceWith,
			String columnToReplaceCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToReplaceCharFrom);
			if (cellText != null) {
				cellText = cellText.replaceAll(characterToReplace, valueToReplaceWith);
				dataRow.put(columnToReplaceCharFrom, cellText);
			}
		}
	}

	// Changes formatting so only the first letter of each word is capitalized
	public void capitalize_FirstLetter_ForColumn(String columnToReplaceCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToReplaceCharFrom);
			if (cellText != null) {
				cellText = WordUtils.capitalizeFully(cellText);
				dataRow.put(columnToReplaceCharFrom, cellText);
			}
		}
	}

	public void replace_Characters_FromBeginning(String characterToReplace, String valueToReplaceWith,
			String columnToReplaceCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToReplaceCharFrom);
			if (cellText != null && cellText.startsWith(characterToReplace)) {
				cellText = cellText.substring(characterToReplace.length(), cellText.length());
				cellText = valueToReplaceWith + cellText;
				dataRow.put(columnToReplaceCharFrom, cellText);
			}
		}
	}

	public void remove_ExtraSpaces(String columnToReplaceCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToReplaceCharFrom);
			if (cellText != null) {
				cellText = BaseUI.remove_ExtraSpaces(cellText);
				dataRow.put(columnToReplaceCharFrom, cellText);
			}
		}
	}

	// Need to be very careful while using this, can strip character as well
	public void remove_NonStandardSpaces(String columnToReplaceCharFrom) {
		for (HashMap<String, String> dataRow : data) {
			String cellText = dataRow.get(columnToReplaceCharFrom);
			if (cellText != null) {
				cellText = BaseUI.remove_NonStandardSpaces(cellText);
				dataRow.put(columnToReplaceCharFrom, cellText);
			}
		}
	}

	public void set_decimal_Precision(int digitsToKeep) {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				String cellText = dataRow.get(key);
				if (cellText != null) {
					cellText = cellText.indexOf(".") < 0 ? cellText
							: cellText.substring(0, cellText.indexOf(".") + digitsToKeep + 1);
					dataRow.put(key, cellText);
				}
			}
		}
	}

	public void set_decimal_Precision(int digitsToKeep, String columnToFormat) {
		for (HashMap<String, String> dataRow : data) {

			String cellText = dataRow.get(columnToFormat);
			if (cellText != null) {
				cellText = cellText.indexOf(".") < 0 ? cellText
						: cellText.substring(0, cellText.indexOf(".") + digitsToKeep + 1);
				dataRow.put(columnToFormat, cellText);
			}

		}
	}

	public void format_Number_forCurrency(String columnToFormat) {
		for (HashMap<String, String> dataRow : data) {

			String cellText = dataRow.get(columnToFormat);
			if (cellText == null) {
				cellText = "0.00";
			} else if (cellText.indexOf(".") < 0) {
				cellText += ".00";
			} else {
				cellText = cellText.substring(0, cellText.indexOf(".") + 3);
			}

			dataRow.put(columnToFormat, cellText);
		}

	}

	public void sort_ByColumn_Ascending(final String columnName) {
		Collections.sort(data, new Comparator<HashMap<String, String>>() {
			public int compare(final HashMap<String, String> o1, final HashMap<String, String> o2) {
				return o1.get(columnName).compareToIgnoreCase(o2.get(columnName));
			}
		});

	}

	public void sort_ByColumn_Descending(final String columnName) {
		Collections.sort(data, new Comparator<HashMap<String, String>>() {
			public int compare(final HashMap<String, String> o1, final HashMap<String, String> o2) {
				return o2.get(columnName).compareToIgnoreCase(o1.get(columnName));
			}
		});

	}

	public void sort_ByColumn_numeric_Ascending(final String columnName) {
		// Double newDouble = Double.parseDouble(data.get(1).get(columnName));

		Collections.sort(data, new Comparator<HashMap<String, String>>() {
			public int compare(final HashMap<String, String> o1, final HashMap<String, String> o2) {
				double firstDouble = o1.get(columnName).equals("") ? 0.0 : Double.parseDouble(o1.get(columnName));
				double secondDouble = o2.get(columnName).equals("") ? 0.0 : Double.parseDouble(o2.get(columnName));
				return Double.compare(firstDouble, secondDouble);
				// return Double.compare(Double.parseDouble(o1.get(columnName)),
				// (Double.parseDouble(o2.get(columnName))));
			}
		});
	}

	private void verify_Column_Sorted(String columnName, BiPredicate<String, String> areValuesInOrder) {
		for (int i = 1; i < data.size(); i++) {
			String previousValue = data.get(i - 1).get(columnName);
			String currentValue = data.get(i).get(columnName);

			// "Row 32 (Boo) and row 33 (Foo) are [NOT] in the expected order."
			String messagePrefix = MessageFormat.format("Row {0} ({1}) and row {2} ({3}) are ", i - 1, previousValue, i,
					currentValue);
			String messageSuffix = "in the expected order";

			BaseUI.verify_true_AndLog(areValuesInOrder.test(previousValue, currentValue), messagePrefix + messageSuffix,
					messagePrefix + "NOT " + messageSuffix);
		}
	}

	public void verify_Column_NotEmptyOrNull(String columnName) {
		String output = "";
		for (int i = 0; i < data.size(); i++) {
			if (BaseUI.stringEmpty(data.get(i).get(columnName).trim())) {
				output += "\nRow " + String.valueOf(i) + " Column " + columnName + " WAS Empty or null.";
			}
		}
		BaseUI.verify_true_AndLog(output.equals(""),
				"None of the values in column " + columnName + " were null or empty.", output);

	}

	public void verify_Column_Sorted_Descending(String columnName) {
		verify_Column_Sorted(columnName, (previousValue, currentValue) -> {
			int comparison = currentValue.compareToIgnoreCase(previousValue);
			return comparison <= 0;
		});
	}

	public void verify_Column_Sorted_Ascending(String columnName) {
		verify_Column_Sorted(columnName, (previousValue, currentValue) -> {
			int comparison = currentValue.compareToIgnoreCase(previousValue);
			return comparison >= 0;
		});
	}

	public void verify_MatchingRow_Found(HashMap<String, String> rowToFind) throws Exception {
		HashMap<String, String> matchingRow = return_Row_BasedOn_AnyNumberOfMatchingFields(rowToFind);
		BaseUI.verify_true_AndLog(matchingRow != null, "Found matching row for passed in data.",
				"Did NOT find matching row for passed in data. \n" + rowToFind);
	}

	public void verify_Value_NOT_InColumn(String columnName, String value) {
		boolean valueFound = false;

		for (HashMap<String, String> tableRow : data) {
			if (tableRow.get(columnName).equals(value)) {
				valueFound = true;
			}
		}

		BaseUI.verify_false_AndLog(valueFound, "Column: " + columnName + " did NOT contain value: " + value,
				"Column: " + columnName + " DID contain value: " + value);

	}

	public void verify_Value_InColumn(String columnName, String value) {
		boolean valueFound = false;

		for (HashMap<String, String> tableRow : data) {
			if (tableRow.get(columnName).equals(value)) {
				valueFound = true;
			}
		}

		BaseUI.verify_true_AndLog(valueFound, "Column: " + columnName + " did contain value: " + value,
				"Column: " + columnName + " did NOT contain value: " + value);

	}

	public void verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(TableData searchResults, String columnName,
			String expectedValue, int expectedCount) {
		int countofMatches = searchResults.countOf_Matches_ThatHave_ColumnName_AndCellValue(columnName, expectedValue);
		BaseUI.verify_true_AndLog(countofMatches == expectedCount,
				columnName + " with expectedValue " + expectedValue + " Row Count " + countofMatches + " match " + expectedCount
						+ " Row count",
				columnName + " with expectedValue " + expectedValue + " Row Count " + countofMatches + " does NOT match " + expectedCount
						+ " Row count");
	}

	public void sort_ByColumn_numeric_Descending(final String columnName) {
		// Double newDouble = Double.parseDouble(data.get(1).get(columnName));

		Collections.sort(data, new Comparator<HashMap<String, String>>() {
			public int compare(final HashMap<String, String> o1, final HashMap<String, String> o2) {
				double firstDouble = o1.get(columnName).equals("") ? 0.0 : Double.parseDouble(o1.get(columnName));
				double secondDouble = o2.get(columnName).equals("") ? 0.0 : Double.parseDouble(o2.get(columnName));
				return Double.compare(secondDouble, firstDouble);
				// return Double.compare(Double.parseDouble(o2.get(columnName)),
				// (Double.parseDouble(o1.get(columnName))));
			}
		});
	}

	public void verify_Column_Sorted_Numeric_Descending(String columnName) {
		verify_Column_Sorted(columnName, (previousValue, currentValue) -> {
			Double previousDouble = Double.parseDouble(previousValue);
			Double currentDouble = Double.parseDouble(currentValue);

			return previousDouble >= currentDouble;
		});
	}

	public void verify_Column_Sorted_Numeric_Ascending(String columnName) {
		verify_Column_Sorted(columnName, (previousValue, currentValue) -> {
			Double previousDouble = Double.parseDouble(previousValue);
			Double currentDouble = Double.parseDouble(currentValue);

			return previousDouble <= currentDouble;
		});
	}

	public void replace_Null_With_Empty() {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				String cellText = dataRow.get(key);
				if (cellText == null) {
					cellText = "";
					dataRow.put(key, cellText);
				}
			}
		}

	}

	public void replace_Empty_WithEmpty() {
		for (HashMap<String, String> dataRow : data) {
			for (String key : dataRow.keySet()) {
				String cellText = dataRow.get(key);
				if (cellText == null || cellText.equals(" ") || cellText.equals(" ")) {
					cellText = "";
					dataRow.put(key, cellText);
				}
			}
		}

	}

	public void replace_Null_With_Value_ForColumn(String columnName, String value) {
		for (HashMap<String, String> dataRow : data) {
			if (dataRow.get(columnName) == null) {
				dataRow.put(columnName, value);
			}
		}

	}

	// Not currently working
	// public Boolean keyExists(String columnName) {
	// Boolean columnExists = false;
	//
	// for (HashMap<String, String> dataRow : data) {
	// for (String key : dataRow.keySet()) {
	// if (key.equals(columnName)) {
	// columnExists = true;
	// }
	// }
	// }
	//
	// return columnExists;
	// }

	public void updateColumn_WithReplacementValues(String columnToUpdate, Set<String> valuesToReplace,
			String valueToReplaceWith) {
		for (HashMap<String, String> row : data) {
			String actualValue = row.get(columnToUpdate).trim();
			if (valuesToReplace.contains(actualValue)) {
				row.put(columnToUpdate, valueToReplaceWith);
			}
		}
	}

	// Use this method to set any values that don't fall into the
	// valuesThatShouldMatch to the new value.
	public void updateColumn_WhereDoesNOTMatchSet_WithReplacementValue(String columnToUpdate,
			Set<String> valuesThatShouldMatch, String valueToReplaceWith) {
		for (HashMap<String, String> row : data) {
			String actualValue = row.get(columnToUpdate).trim();
			if (!valuesThatShouldMatch.contains(actualValue)) {
				row.put(columnToUpdate, valueToReplaceWith);
			}
		}
	}

	public void add_Column_IfItDoesntExist(String columnName) {

		for (HashMap<String, String> dataRow : data) {
			if (!dataRow.containsKey(columnName)) {
				dataRow.put(columnName, "");
			}
		}
	}

	public void verify_Column_MatchesValue_WhenOtherColumnPopulated_elseEmpty(String columnToCheck,
			String valueForColumn, String columnThatNeedsToBePopulated) {

		for (int i = 0; i < data.size(); i++) {

			boolean columnShouldHaveValue = BaseUI.stringEmpty(data.get(i).get(columnThatNeedsToBePopulated)) ? false
					: true;

			String actualValue = data.get(i).get(columnToCheck);

			if (columnShouldHaveValue) {
				BaseUI.baseStringCompare(columnToCheck, valueForColumn, actualValue);
			} else {
				BaseUI.verify_true_AndLog(BaseUI.stringEmpty(actualValue),
						"Value for column " + columnToCheck + " was empty due to column " + columnThatNeedsToBePopulated
								+ " being empty.",
						"Seeing value " + actualValue + " for column " + columnToCheck
								+ " but it was supposed to be empty since column " + columnThatNeedsToBePopulated
								+ " was empty.");
			}

		}

	}

	public void replaceKey_ForGivenKey(String originalKey, String newKey) {
		for (HashMap<String, String> dataRow : data) {
			dataRow.put(newKey, dataRow.get(originalKey));
			dataRow.remove(originalKey);
		}
	}

	public void replaceValue_ForColumn_WhenEqualTo(String columnName, String oldValue, String newValue) {
		for (HashMap<String, String> dataRow : data) {
			if (dataRow.get(columnName).equals(oldValue)) {
				dataRow.put(columnName, newValue);
			}
		}
	}

	// Pass in lambda expression to apply to each row.
	public void forEach_Row(Consumer<HashMap<String, String>> lambdaExpression) {
		data.forEach(lambdaExpression);
	}

	public HashMap<String, String> dataRow(String keyToMatch, String expectedValue) {
		HashMap<String, String> row = new HashMap<String, String>();

		for (HashMap<String, String> potentialRow : data) {
			if (potentialRow.get(keyToMatch).equals(expectedValue)) {
				row = potentialRow;
			}
		}

		return row;
	}

	public void removeIntChars_FromBeginningOfAll_Values_InColumn(String columnName, int startIndex) {
		for (HashMap<String, String> row : data) {
			String columnValue = row.get(columnName);
			columnValue = columnValue.substring(startIndex, columnValue.length());

			row.put(columnName, columnValue);
		}
	}

    @Override
    public String toString() {
        return data.stream().map(row -> row.keySet().stream()
                .sorted(String::compareToIgnoreCase)
                .map(key -> key + "=" + row.get(key))
                .collect(Collectors.joining("\t"))
        ).collect(Collectors.joining("\n"));
    }
}
