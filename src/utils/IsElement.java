package utils;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.WebElement;

import java.util.function.Predicate;

public final class IsElement {
    private IsElement() {
    }

    public static Matcher<WebElement> withText(String text) {
        return new ElementTextMatcher(s -> s.equals(text), "element with text", text);
    }

    public static Matcher<WebElement> withTextContaining(String substring) {
        return new ElementTextMatcher(s -> s.contains(substring), "element containing text", substring);
    }

    public static Matcher<WebElement> withTextMatchingRegex(String regex) {
        return new ElementTextMatcher(s -> s.matches(regex), "element with text matching regex", regex);
    }

    // Matcher classes

    private static class ElementTextMatcher extends TypeSafeMatcher<WebElement> {
        private final Predicate<String> _isMatch;
        private final String _summary;
        private final String _expected;

        private ElementTextMatcher(Predicate<String> isMatch, String summary, String expected) {
            _isMatch = isMatch;
            _summary = summary;
            _expected = expected;
        }

        @Override
        protected void describeMismatchSafely(WebElement item, Description mismatchDescription) {
            mismatchDescription
                    .appendText("was element with text <")
                    .appendText(item.getText())
                    .appendText(">");
        }

        @Override
        protected boolean matchesSafely(WebElement webElement) {
            return _isMatch.test(webElement.getText());
        }

        @Override
        public void describeTo(Description description) {
            description
                    .appendText(_summary)
                    .appendText(" <")
                    .appendText(_expected)
                    .appendText(">");
        }
    }
}
