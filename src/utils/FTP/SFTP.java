package utils.FTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import utils.BaseUI;

public class SFTP {

    private static Session loginToServer(FTP_ClientInfo clientInfo) throws Exception {
        JSch jsch = new JSch();
        Session session = null;
        session = jsch.getSession(clientInfo._user, clientInfo._server, clientInfo._port);
        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword(clientInfo._password);
        session.connect();

        return session;
    }

    public static void downloadFile(FTP_ClientInfo clientInfo, File fileToDownload, String downloadLocation) throws Exception {
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        try {
            session = loginToServer(clientInfo);
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;


            String pathToDownloadFrom = fileToDownload.getPath().replace("\\", "/");
            String finalFile = downloadLocation + "\\" + fileToDownload.getName();

            sftpChannel.get(pathToDownloadFrom, finalFile);
        } finally {
            sftpChannel.exit();
            session.disconnect();
        }


    }

    public static void uploadFiles(FTP_ClientInfo clientInfo, File[] filesToUpload, String newDirectory)
            throws Exception {
        Session session = loginToServer(clientInfo);

        Channel channel = session.openChannel("sftp");
        channel.connect();
        ChannelSftp sftpChannel = (ChannelSftp) channel;

        if (newDirectory != null) {
            newDirectory = newDirectory.endsWith("/") ? newDirectory : newDirectory + "/";
        }

        try {

            for (File file : filesToUpload) {
                BaseUI.log_Status("Attempting to upload file " + file.toString());
                String fileToUpload = file.getName();
                if (newDirectory != null) {
                    fileToUpload = newDirectory + fileToUpload;
                }

                try {
                    ((ChannelSftp) channel).mkdir(newDirectory);
                } catch (Exception ee) {
                }
                sftpChannel.put(new FileInputStream(file), fileToUpload);
            }

        } finally {
            channel.disconnect();
            session.disconnect();
        }
    }


    public static void delete_Files(FTP_ClientInfo clientInfo, String[] fileNames, String directoryToRemoveFrom) throws Exception {
        Session session = loginToServer(clientInfo);

        Channel channel = session.openChannel("sftp");
        channel.connect();
        ChannelSftp sftpChannel = (ChannelSftp) channel;

        if (directoryToRemoveFrom != null) {
            directoryToRemoveFrom = directoryToRemoveFrom.endsWith("/") ? directoryToRemoveFrom : directoryToRemoveFrom + "/";
        }

        try {
            for (String fileName : fileNames) {

                String fileToDelete = fileName;
                if (directoryToRemoveFrom != null) {
                    fileToDelete = directoryToRemoveFrom + fileToDelete;
                } else {
                    fileToDelete = "." + fileToDelete;
                }

                try {
                    delete_File(sftpChannel, fileToDelete);
                } catch (IOException e) {
                    BaseUI.log_Warning(
                            "Encountered error while attempting to delete file " + fileName + "\n" + e.getMessage());
                } catch (SftpException e) {
                    BaseUI.log_Warning(
                            "Encountered error while attempting to delete file " + fileName + "\n" + e.getMessage());
                }


            }
        } finally {
            channel.disconnect();
            session.disconnect();
        }
    }


    private static void delete_File(ChannelSftp sftpClient, String fileName) throws IOException, SftpException {
        BaseUI.log_Status("Attempting to delete file " + fileName);
        sftpClient.rm(fileName);
    }

}// End of Class
