package utils.FTP;

public class FTP_ClientInfo {

	public String _server = "";
	public int _port = -1;
	public String _user = "";
	public String _password = "";

	public FTP_ClientInfo(String server, int port, String user, String password) {
		_server = server;
		_port = port;
		_user = user;
		_password = password;
	}

}
