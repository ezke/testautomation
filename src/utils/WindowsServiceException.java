package utils;

public class WindowsServiceException extends Exception {
    public WindowsServiceException(String message) {
        super(message);
    }

    public WindowsServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
