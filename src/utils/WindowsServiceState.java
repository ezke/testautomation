package utils;

public enum WindowsServiceState {
    UNKNOWN("Unknown"),
    RUNNING("Running"),
    STOPPING("Stopping"),
    STOPPED("Stopped"),
    STARTING("Starting"),
    PAUSED("Paused"),
    PAUSING("Pausing"),
    RESUMING("Resuming");

    private String value;

    WindowsServiceState(final String val) {
        value = val;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getValue();
    }
}
