package utils;

public class TestFailedDueToEarlierTestFailure extends Error {
    public TestFailedDueToEarlierTestFailure(String message) {
        super(message);
    }
}
