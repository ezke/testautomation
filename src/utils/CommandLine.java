package utils;

import r360.pages.GlobalVariables;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CommandLine {
    public static void execute(Consumer<String> standardLogger, Consumer<String> errorLogger, String... arguments) throws IOException, InterruptedException {
        Stream<String> preArgsList = Stream.of("powershell.exe", "-Executionpolicy", "Unrestricted");
        Stream<String> completeArgsList = Stream.concat(preArgsList, Arrays.stream(arguments));
        ProcessBuilder builder = new ProcessBuilder(
                completeArgsList.toArray(String[]::new));
        builder.directory(new File(".\\src\\r360\\data"));
        Process powershellCommand = builder.start();
        StreamGobbler errorGobbler = new StreamGobbler(powershellCommand.getErrorStream(), errorLogger);
        StreamGobbler standardGobbler = new StreamGobbler(powershellCommand.getInputStream(), standardLogger);
        errorGobbler.start();
        standardGobbler.start();
        powershellCommand.waitFor();
    }
}
