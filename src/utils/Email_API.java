package utils;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.mailosaur.MailboxApi;
import com.mailosaur.exception.MailosaurException;
import com.mailosaur.model.Attachment;
import com.mailosaur.model.Email;

public class Email_API {

	static String apiKey = "v87IhGSmN3ohRJy";
	static String mailosaurbox = "j4pxicxw";
	public static String email_Ending = ".j4pxicxw@mailosaur.io";

	static MailboxApi mailbox = new MailboxApi(mailosaurbox, apiKey);

	// public static void initialize_APICall() {
	// mailbox = new MailboxApi(mailosaurbox, apiKey);
	// }

	public static void Wait_ForEmail_ToExist(String recipient, Integer secondsToWait) throws Exception {
		BaseUI.log_Status("Waiting up to " + secondsToWait.toString() + " seconds for email to appear in inbox.");
		for (Integer i = 0; i < secondsToWait; i++) {
			Email[] emails = mailbox.getEmailsByRecipient(recipient);
			if (emails.length > 0) {
				break;
			} else {
				Thread.sleep(1000);
			}
		}

	}

	public static void verify_EmailCount(Email[] emailsReturned, Integer expectedCount) {
		BaseUI.verify_true_AndLog(emailsReturned.length == expectedCount,
				"Found correct number of emails " + expectedCount.toString(),
				"Expected " + expectedCount + " emails, but seeing " + String.valueOf(emailsReturned.length));

	}

	public static Email return_FirstEmail_ByRecipient(String recipient) throws Exception {

		try {
			Email firstEmail = return_AllEmailsByRecipient(recipient)[0];
			BaseUI.log_Status("Returning email for recipient " + recipient);
			return firstEmail;
		} catch (Exception E) {
			BaseUI.log_Warning("Unable to find email by recipient " + recipient);
			return null;
		}
	}

	public static Email[] return_AllEmailsByRecipient(String recipient) throws Exception {
		Email[] emails = mailbox.getEmailsByRecipient(recipient);
		BaseUI.log_Status("Returning all emails for " + recipient);

		if (emails.length == 0) {
			BaseUI.log_Status("No emails were found.");
		}

		return emails;
	}

	// dateWeWant must be in format M/d/yyyy
	public static Email return_EarliestEmail_ForRecipient(String recipient) throws Exception {
		Email[] allEmails = return_AllEmailsByRecipient(recipient);
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy hh:mm:ss Z");

		Email emailWeWant = null;
		Date dateOfEmailWeWant = null;

		for (Email email : allEmails) {
			String date = email.headers.get("Date").toString();
			Date currentDate = sdf.parse(date);
			if (emailWeWant == null) {
				emailWeWant = email;
				dateOfEmailWeWant = currentDate;
			} else {
				if (currentDate.before(dateOfEmailWeWant)) {
					emailWeWant = email;
					dateOfEmailWeWant = currentDate;
				}
			}
		}

		return emailWeWant;

	}

	public static void open_EmailHTML_In_NewBrowserWindow(Email email) throws Exception {
		String html = email.html.toString();

		Path newFilePath = Paths.get(Browser.downloadLocation + "\\" + "email.html");
		Files.write(newFilePath, new ArrayList<String>(Arrays.asList(html)), Charset.forName("UTF-8"));

		BaseUI.open_NewWindow_And_SwitchToIt("file:///" + newFilePath.toString().replace("\\", "/"));

	}

	public static void delete_Emails_ByRecipient(String recipient) throws Exception {
		Email[] emails = return_AllEmailsByRecipient(recipient);

		BaseUI.log_Status("Deleting all emails for " + recipient);

		for (Email email : emails) {
			String emailID = email.id;
			if (email.to[0].address.equals(recipient)) {
				try {
					mailbox.deleteEmail(emailID);
				} catch (Exception e) {
					BaseUI.log_Warning("Encountered error while attempting to delete email: " + e.getMessage());
				}
			}
		}
	}

	public static void delete_Email_ByEmailID(String id) throws Exception {
		mailbox.deleteEmail(id);
	}

	// will need new method if there is more than one link.
	public static String return_FirstLink_FromEmail(Email email) {
		BaseUI.log_Status("Attempting to return first link from email.");
		return email.html.links[0].href;
	}

	public static Attachment[] return_EmailAttachments_FromEmail(Email email) {
		BaseUI.log_Status("Attempting to return attachments from email.");
		return email.attachments;
	}

	public static int return_EmailNumOfAttachments_FromEmail(Email email) {
		BaseUI.log_Status("Attempting to return number of attachments from email.");
		return email.attachments.length;
	}

	public static String return_EmailSubject_FromEmail(Email email) {
		BaseUI.log_Status("Attempting to return Subject from email.");
		return email.subject;
	}

	public static String return_EmailCreationDate_FromEmail(Email email) {
		BaseUI.log_Status("Attempting to return email creation date from email.");
		return email.creationdate.toString();
	}

	public static String return_EmailData_FromEmailBody(Email email) {
		BaseUI.log_Status("Attempting to return email content from email.");
		return email.html.body;
	}

}// End of Class
