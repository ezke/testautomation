//// LET 12/14/15: Browser.java
//// Purpose: Contains actions for dealing with opening/closing browsers. The open action specifies a default browser to use
//// but can be overridden by setting a value in testng or ant files.
//// The warning messages in this file can be ignored. 
package utils;

import java.math.BigDecimal;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.function.Function;

import net.sourceforge.jtds.jdbc.ClobImpl;

public class DatabaseConnection {
	// CMP database connection string
	// static String dbUrl =
	// "jdbc:sqlserver://DOMUWD04\\DT01;databaseName=POMSCMP;integratedSecurity=true;";
	// to use jtds driver will need jtds jar. Connection string will look like
	// below. Allows for Windows Credentials
	// jdbc:jtds:sqlserver://[servername]:[port]/[DBName];domain=[DomainName];user=[username];password=[password]
	public static String dbUrl = "jdbc:sqlserver://rechubqadb02.qalabs.nwk;user=sa;password=Wausau#1;databaseName=WFSDB_R360";
	public static String db2016Url = "jdbc:sqlserver://R36016QADB01.qalabs.nwk;user=sa;password=$ecurity_is_Job_1;databaseName=WFSDB_R360";
	// CI database information
	static String oracleURL = "jdbc:oracle:thin:@Domdld02-vip:1521:BL02";
	// static String username = "t445187";
	// static String password = "jonah4wail$$del";
	static String username = "sa";
	static String password = "Wausau#1";

	// Function to connect to SQL Server - and run a query
	public static TableData runSQLServerQuery(String query) throws Exception {
		// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection con = DriverManager.getConnection(dbUrl);
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(query);

		TableData tableData = new TableData();
		// ArrayList<Object> dataList = new ArrayList<Object>();
		// This gets tricky - we need to know the return type of each field that we're
		// pulling back. Yikes.
		while (rs.next()) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			// Used to get Column Header
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();

			// The column count starts from 1
			for (int i = 1; i <= columnCount; i++) {
				// Column header
				String key = rsmd.getColumnName(i);

				Object cell = (Object) rs.getObject(i);
				String value = returnValue(cell);

				dataRow.put(key, value);

			}

			tableData.data.add(dataRow);
		}

		BaseUI.log_Status("Successfully ran query.  Returning Data.");

		return tableData;
	}

	// Function to connect to SQL Server - and run a query
	public static TableData runSQLServerStoredProcQueryFourArgs(String url, String arg1, String arg2, String arg3, String arg4) throws Exception {

		Connection con = DriverManager.getConnection(url);

		//PreparedStatement statement = con.prepareCall("{call RecHubAPI.usp_factBatchSummary_Get(?, ?, ?, ?)}");
		CallableStatement statement = con.prepareCall("{call RecHubAPI.usp_factBatchSummary_Get(?, ?, ?, ?)}");
		statement.setString(1, arg1);
		statement.setString(2, arg2);
		statement.setString(3, arg3);
		statement.setString(4, arg4);

		boolean hadResults = statement.execute();

		System.out.println("Deposit Date | Processesing Date | Batch ID | Source Date ID | Bank ID | Workgroup ID | Batch Number | Check Count | Stub Count | Bstch Source Key | Document Count | Scanned Check Count | Batch Payment Source | Batch Payment Type");

			ResultSet resultSet = statement.getResultSet();

			TableData tableData = new TableData();

			// process result set
			while (resultSet.next()) {
				HashMap<String, String> dataRow = new HashMap<String, String>();
				// Used to get Column Header
				ResultSetMetaData rsmd = resultSet.getMetaData();
				int columnCount = rsmd.getColumnCount();

				// The column count starts from 1
				for (int i = 1; i <= columnCount; i++) {
					// Column header
					String key = rsmd.getColumnName(i);

					Object cell = (Object) resultSet.getObject(i);
					String value = returnValue(cell);

					dataRow.put(key, value);

				}

				tableData.data.add(dataRow);
			}

		statement.close();

		return tableData;
	}

	@FunctionalInterface
    public interface FunctionWithSQLException<T, R> {
	    R apply(T t) throws SQLException;
    }

	private static <T> T runSQLServerScalarHelper(String query, FunctionWithSQLException<ResultSet, T> extractValue) throws SQLException {
        Connection con = DriverManager.getConnection(dbUrl);
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();

        int columnCount = rsmd.getColumnCount();
        if(columnCount != 1) {
            throw new SQLException("Scalar query should return a resultset of a single column; This one resulted in " + columnCount + " columns.");
        }

        if(rs.next()) {
            T result = extractValue.apply(rs);
            if(rs.next()) {
                throw new SQLException("Scalar query should return a resultset of a single row; This one resulted returned more than one row.");
            }
            rs.close();
            stmt.close();
            con.close();
            return result;
        }
        else {
            throw new SQLException("Scalar query should return a resultset of a single row; This one resulted returned no rows.");
        }
    }

	public static int runSQLServerScalarQueryInt(String query) throws SQLException {
	    return runSQLServerScalarHelper(query, rs->rs.getInt(1));
    }

    public static String runSQLServerQueryString(String query) throws SQLException {
	    return runSQLServerScalarHelper(query, rs->rs.getString(1));
    }

	// Function to connect to SQL Server - and run a query as a command without
	// returning any datasets
	public static void runSQLServerCommand(String query) throws SQLException, ClassNotFoundException {
		// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection con = DriverManager.getConnection(dbUrl);
		Statement stmt = con.createStatement();
		stmt.execute(query);
	}

	// Function to connect to SQL Server - and run a query as a command without
	// returning any datasets
	public static void runSQLServerUpdate(String query) throws SQLException, ClassNotFoundException {
		// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection con = DriverManager.getConnection(dbUrl);
		Statement stmt = con.createStatement();
		stmt.executeUpdate(query);
	}

	// Function to connect to SQL Server - and run a query
	public static TableData runSQLServerCommand_AndThenQuery(String command, String query) throws Exception {
		// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection con = DriverManager.getConnection(dbUrl);

		Statement cmnd = con.createStatement();
		cmnd.executeUpdate(command);

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(query);

		TableData tableData = new TableData();
		// ArrayList<Object> dataList = new ArrayList<Object>();
		// This gets tricky - we need to know the return type of each field that we're
		// pulling back. Yikes.
		while (rs.next()) {

			HashMap<String, String> dataRow = new HashMap<String, String>();
			// Used to get Column Header
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();

			// The column count starts from 1
			for (int i = 1; i <= columnCount; i++) {
				// Column header
				String key = rsmd.getColumnName(i);

				Object cell = (Object) rs.getObject(i);
				String value = returnValue(cell);

				dataRow.put(key, value);

			}

			tableData.data.add(dataRow);
		}

		BaseUI.log_Status("Successfully ran query.  Returning Data.");

		return tableData;
	}

	public static String returnValue(Object cell) throws Exception {
		String str = null;

		if (cell instanceof String) {
			// do you work here for string like below
			str = (String) cell;
		} else if (cell instanceof Integer) {
			str = Integer.toString((Integer) cell);
			// do cell work for Integer
		} else if (cell instanceof Double) {
			str = Double.toString((Double) cell);
		} else if (cell instanceof BigDecimal) {
			str = ((BigDecimal) cell).toString();
		} else if (cell instanceof Long) {
			str = Long.toString((Long) cell);
		} else if (cell instanceof Short) {
			str = Short.toString((Short) cell);
		} else if (cell instanceof Timestamp) {
			str = new SimpleDateFormat("MM-dd-yyyy-HH-mm-ss").format((Timestamp) cell);
		} else if (cell instanceof Boolean) {
			str = (Boolean) cell == true ? "1" : "0";
		} else if (cell instanceof ClobImpl) {
			ClobImpl newClob = (ClobImpl) cell;
			if (newClob.length() >= 1) {
				str = newClob.getSubString(1, (int) newClob.length());
			} 
		}

		return str;
	}

	// Function to connect to Oracle - and run a query
	public static void runOracleQuery(String query) throws SQLException, ClassNotFoundException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection(oracleURL, username, password);
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		// This gets tricky - we need to know the return type of each field that we're
		// pulling back. Yikes.
		while (rs.next()) {
			BigDecimal ordernumber = rs.getBigDecimal("order_number");
			String statusValue = rs.getString("grouping_status_code");
			BaseUI.log_Status("Order number is " + ordernumber + " and status is " + statusValue);
		}
		BaseUI.log_Status("After result set");
	}

	public static PreparedStatement getPreparedStatement(String query) throws SQLException {
		Connection con = DriverManager.getConnection(dbUrl);
		return con.prepareStatement(query);
	}
}
