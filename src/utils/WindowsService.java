package utils;

import org.hamcrest.Matcher;

public interface WindowsService {
    WindowsServiceState getState() throws WindowsServiceException;

    void start() throws WindowsServiceException;

    void stop() throws WindowsServiceException;

    void resume() throws WindowsServiceException;

    void pause() throws WindowsServiceException;

    void makeRunning() throws WindowsServiceException;

    void makeStopped() throws WindowsServiceException;

    void makePaused() throws WindowsServiceException;

    void verifyState(Matcher<WindowsServiceState> condition) throws WindowsServiceException;

    void validateWindowServiceState_MatchesExpected(WindowsServiceState expectedState) throws Exception;
}
