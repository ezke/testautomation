package utils;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.VerRsrc.VS_FIXEDFILEINFO;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.io.Zip;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Callable;

public class FileOperations {
	public static void copyFiles(String sourceDirectory, String destinationDirectory) {

		BaseUI.log_Status("Copying files from " + sourceDirectory + " to " + destinationDirectory);

		File source = new File(sourceDirectory);
		File dest = new File(destinationDirectory);
		try {
			FileUtils.copyDirectory(source, dest);
		} catch (IOException e) {
			BaseUI.log_Warning(e.getMessage());
		}
	}

	public static void copyFile(String sourceDirectory, String destinationDirectory) {

		BaseUI.log_Status("Copying file from " + sourceDirectory + " to " + destinationDirectory);

		File source = new File(sourceDirectory);
		File dest = new File(destinationDirectory);
		try {
			FileUtils.copyFile(source, dest);
		} catch (IOException e) {
			BaseUI.log_Warning(e.getMessage());
		}
	}

	// fileName_Prefix will be the start of the file name that we'll
	// search for.
	public static String[] getLatestFileExtract_FromFolder(String folderLocation, String fileName_Prefix)
			throws Exception {
		File folder = new File(folderLocation);
		File[] listOfFilesAndFolders = return_NonHiddenFiles(folder);

		// ArrayList<File> filesThatMatch = new ArrayList<File>();
		File fileToPick = null;
		long lastModified_ClosestToCurrent = 0;

		for (File file : listOfFilesAndFolders) {
			if (file.isFile() && file.getName().startsWith(fileName_Prefix)) {
				// filesThatMatch.add(file);
				long lastModified = file.lastModified();
				if (lastModified > lastModified_ClosestToCurrent) {
					lastModified_ClosestToCurrent = lastModified;
					fileToPick = file;
				}
			}
		}

		String fileExtract = DataBuilder.Get_FileData_AsString(fileToPick);

		return fileExtract.split("\\r");
	}

	// Verifies that there are no files remaining. If there are folders it will
	// still pass. Ignores Hidden files.
	public static void verify_NoFiles_Remaining(String folderLocation) {
		File folder = new File(folderLocation);
		File[] listOfFilesAndFolders = return_NonHiddenFiles(folder);

		String output = "";
		for (File file : listOfFilesAndFolders) {
			if (file.isFile()) {
				output += "\n" + "Seeing file " + file.getName();
			}
		}

		BaseUI.verify_true_AndLog(output.equals(""), "No Files Found.", output);

	}

	public static void wait_For_File_To_Exist(String fileString) throws Exception {

		BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
			public Boolean call() throws Exception {
				File file = new File(fileString);
				return file.exists();
			}
		}, 30);

	}

	// Returns non-hidden files. Doesn't include folders.
	public static File[] return_NonHiddenFiles(File directory) {
		File[] files = directory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				Boolean notHiddenAndIsFile = true;
				if (file.isHidden() || !file.isFile()) {
					notHiddenAndIsFile = false;
				}

				return notHiddenAndIsFile;
			}
		});

		return files;
	}

	// Pass in the extension minus the period. So blue.dat would be "dat"
	public static File[] return_Files_ThatEndInExtension(String directoryPath, String extension) throws Exception {
		File[] initialArray = return_NonHidden_Files_AndSubFiles(directoryPath);
		ArrayList<File> newList = new ArrayList<File>();

		for (File file : initialArray) {
			if (file.getName().endsWith("." + extension)) {
				newList.add(file);
			}

		}

		return newList.toArray(new File[newList.size()]);
	}

	public static File[] return_NonHidden_Files_AndSubFiles(String directoryPath) throws Exception {
		ArrayList<File> completeList = new ArrayList<File>();
		find_SubFiles(directoryPath, completeList);
		// get all the files from a directory
		return completeList.toArray(new File[completeList.size()]);
	}

	public static File[] return_NonHidden_Files_AndSubFiles_excludeDirectories(String directoryPath) throws Exception {
		ArrayList<File> completeList = new ArrayList<File>();
		find_SubFiles(directoryPath, completeList);

		Iterator<File> fileIterator = completeList.iterator();
		while (fileIterator.hasNext()) {
			File nextFile = fileIterator.next();
			if (nextFile.isDirectory())
				fileIterator.remove();
		}

		// get all the files from a directory
		return completeList.toArray(new File[completeList.size()]);
	}

	// Updates the ArrayList we pass in with files found in subdirectories.
	// Ignore Thumbs.db as that was throwing off my tests.
	private static void find_SubFiles(String directoryPath, ArrayList<File> completeList) throws Exception {
		File directory = new File(directoryPath);
		File[] fileList = directory.listFiles();
		for (File file : fileList) {
			if (!file.isHidden() && !file.getName().contains("Thumbs.db")) {
				completeList.add(file);
				if (file.isDirectory()) {

					find_SubFiles(file.getAbsolutePath(), completeList);
				}
				// else {
				// completeList.add(file);
				// }
			}
		}
	}

	public static void verify_FileNames_EndsWithExtension(String folderName, String extensionFilesShouldHave)
			throws Exception {
		File[] availableFiles = return_NonHidden_Files_AndSubFiles(folderName);

		BaseUI.verify_true_AndLog(availableFiles.length > 0, "Found Files.", "Did NOT find files.");
		for (File file : availableFiles) {
			String actualExtension = FilenameUtils.getExtension(file.getName());
			BaseUI.baseStringCompare("File Extension", extensionFilesShouldHave, actualExtension);
		}

	}

	public static void wait_ForFileCount_ToMatch(String folderToGet, Integer expectedCount, Integer timeToWaitInSeconds)
			throws Exception {

		BaseUI.log_Status(
				"Attempting to wait for file count for directory " + folderToGet + " to reach " + expectedCount);
		// Integer fileCount = 0;

		// for (int i = 0; i < timeToWaitInSeconds; i++) {
		// File expectedFolder = new File(folderToGet);
		// File[] listOfExpectedFiles = return_NonHiddenFiles(expectedFolder);
		// fileCount = listOfExpectedFiles.length;
		// if (listOfExpectedFiles.length == expectedCount) {
		//
		// break;
		// } else {
		// Thread.sleep(1000);
		// }
		// }
		try {
			BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
				public Boolean call() throws Exception {
					// File expectedFolder = new File(folderToGet);
					return return_NonHidden_Files_AndSubFiles(folderToGet).length == expectedCount;
				}
			}, timeToWaitInSeconds);
		} finally {

			File[] allFiles = return_NonHidden_Files_AndSubFiles(folderToGet);
			String output = "";
			if (allFiles.length > 0) {
				for (File file : allFiles) {
					output += "\nFound File " + file.getAbsolutePath();
				}
				BaseUI.log_Status(output);
			}
			BaseUI.log_Status("File count was " + allFiles.length);
		}
	}

	public static String getVersionInfo(String path) throws IOException {
		try {
			IntByReference dwDummy = new IntByReference();
			dwDummy.setValue(0);

			int versionlength = com.sun.jna.platform.win32.Version.INSTANCE.GetFileVersionInfoSize(path, dwDummy);

			byte[] bufferarray = new byte[versionlength];
			Pointer lpData = new Memory(bufferarray.length);
			PointerByReference lplpBuffer = new PointerByReference();
			IntByReference puLen = new IntByReference();
			boolean fileInfoResult = com.sun.jna.platform.win32.Version.INSTANCE.GetFileVersionInfo(path, 0,
					versionlength, lpData);
			boolean verQueryVal = com.sun.jna.platform.win32.Version.INSTANCE.VerQueryValue(lpData, "\\", lplpBuffer,
					puLen);

			VS_FIXEDFILEINFO lplpBufStructure = new VS_FIXEDFILEINFO(lplpBuffer.getValue());
			lplpBufStructure.read();

			int v1 = (lplpBufStructure.dwFileVersionMS).intValue() >> 16;
			int v2 = (lplpBufStructure.dwFileVersionMS).intValue() & 0xffff;
			int v3 = (lplpBufStructure.dwFileVersionLS).intValue() >> 16;
			int v4 = (lplpBufStructure.dwFileVersionLS).intValue() & 0xffff;
			System.out.println("Version: " + v1 + "." + v2 + "." + v3 + "." + v4);
			return String.valueOf(v1) + "." + String.valueOf(v2) + "." + String.valueOf(v3) + "." + String.valueOf(v4);
		} catch (Exception ex) {
			String errorMessage = "Error reading version info for path '" + path + "': " + ex.getMessage();
			throw new IOException(errorMessage, ex);
		}
	}

	public static void wait_ForFile_WithExtension_ToExist(String extension, String folderLocation,
			Integer timeToWaitInSeconds) {
		String newExtension = extension.startsWith(".") ? extension.substring(1, extension.length()) : extension;
		BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
			public Boolean call() throws Exception {
				File[] listOfExpectedFiles = return_NonHiddenFiles(new File(folderLocation));
				Boolean fileHasExtension = false;
				for (File file : listOfExpectedFiles) {
					if (file.isFile()) {
						String fileExtension = FilenameUtils.getExtension(file.toString());
						if (fileExtension.equals(newExtension)) {
							fileHasExtension = true;
							break;
						}

						// if (file.getName().endsWith(extension)) {
						// fileHasExtension = true;
						// break;
						// }
					}
				}
				return fileHasExtension;
			}
		}, timeToWaitInSeconds);

	}

	public static void wait_ForFile_WithExtension_ToNOTExist(String extension, String folderlocation,
			Integer timeToWaitInSeconds) {
		String newextension = extension.startsWith(".") ? extension.substring(1, extension.length()) : extension;
		BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
			public Boolean call() throws Exception {
				File[] listOfExpectedFiles = return_NonHiddenFiles(new File(folderlocation));
				File fileWithExtension = null;
				Boolean fileHasExtension = true;
				for (File file : listOfExpectedFiles) {
					if (file.isFile()) {
						String fileExtension = FilenameUtils.getExtension(file.toString());
						if (fileExtension.equals(newextension)) {
							fileWithExtension = file;
							break;
						}

						// if (file.getName().endsWith(extension)) {
						// fileHasExtension = true;
						// break;
						// }
					}
				}
				return fileWithExtension == null;
			}
		}, timeToWaitInSeconds);

	}

	public static void verify_AllFiles_FromFoldersMatch(String expectedFolderLocation, String actualFolderLocation)
			throws Exception {
		File[] listOfExpectedFiles = return_NonHidden_Files_AndSubFiles(expectedFolderLocation);
		File[] listOfActualFiles = return_NonHidden_Files_AndSubFiles(actualFolderLocation);

		BaseUI.verify_true_AndLog(listOfExpectedFiles.length > 0, "Found files.", "Did NOT find files.");
		BaseUI.verify_true_AndLog(listOfExpectedFiles.length == listOfActualFiles.length,
				"Number of Files matched between expected and actual file arrays.  Count was "
						+ String.valueOf(listOfExpectedFiles.length),
				"Expected " + String.valueOf(listOfExpectedFiles.length) + " files, but seeing "
						+ String.valueOf(listOfActualFiles.length) + " files.");

		String output = "";

		for (int i = 0; i < listOfExpectedFiles.length; i++) {
			Boolean filesMatched = FileUtils.contentEquals(listOfExpectedFiles[i], listOfActualFiles[i]);
			if (!filesMatched) {
				output += "\n" + "File " + listOfExpectedFiles[i].getPath() + " did not match "
						+ listOfActualFiles[i].getPath();
			}
		}

		BaseUI.verify_true_AndLog(output.equals(""),
				"Files from " + expectedFolderLocation + " matched files from " + actualFolderLocation, output);

	}

	public static void verify_AllFiles_FromFolder_MatchesExpectedFilesAndFileName_WithoutExtension(File[] expectedFiles,
			String actualFolderLocation) throws Exception {
		// File actualFolder = new File(actualFolderLocation);

		File[] listOfActualFiles = return_NonHidden_Files_AndSubFiles(actualFolderLocation);

		BaseUI.verify_true_AndLog(expectedFiles.length > 0, "Found files.", "Did NOT find files.");
		BaseUI.verify_true_AndLog(expectedFiles.length == listOfActualFiles.length,
				"Number of Files matched between expected and actual file arrays.  Count was "
						+ String.valueOf(expectedFiles.length),
				"Expected " + String.valueOf(expectedFiles.length) + " files, but seeing "
						+ String.valueOf(listOfActualFiles.length) + " files.");

		String output = "";

		for (int i = 0; i < expectedFiles.length; i++) {
			Boolean filesMatched = FileUtils.contentEquals(expectedFiles[i], listOfActualFiles[i]);
			if (!filesMatched) {
				output += "\n" + "File " + expectedFiles[i].getPath() + " did not match "
						+ listOfActualFiles[i].getPath();
			} else {
				// don't need pass condition here, is handled later on.
			}
			String expected_FileNameWithoutExtension = expectedFiles[i].getName();
			expected_FileNameWithoutExtension = expected_FileNameWithoutExtension.split("\\.")[0];
			String actual_FileNameWithoutExtension = listOfActualFiles[i].getName();
			;
			actual_FileNameWithoutExtension = actual_FileNameWithoutExtension.split("\\.")[0];
			if (!expected_FileNameWithoutExtension.equals(actual_FileNameWithoutExtension)) {
				output += "\n" + "File name expected was " + expected_FileNameWithoutExtension + " did not match "
						+ actual_FileNameWithoutExtension;
			} else {
				// pass condition handled later on.
			}

		}

		BaseUI.verify_true_AndLog(output.equals(""), "Files matched files from " + actualFolderLocation, output);

	}

	// Checks folder and subfolders to make sure that the passed in file name cannot
	// be found.
	public static void verify_FileName_NotFound_InFolderOrSubFolders(String folderToCheck, String fileName)
			throws Exception {
		File[] listOfFiles = return_NonHidden_Files_AndSubFiles(folderToCheck);

		Boolean testPass = true;
		for (File file : listOfFiles) {
			// If we find the file we'll fail the test.
			if (file.getName().equals(fileName)) {
				testPass = false;
				BaseUI.verify_true_AndLog(testPass, "",
						"Found file " + fileName + " and file should not have been there.");
			}

		}
		// Above we check if the file does not exist. So if we get to this point that
		// means the file did not exist (if it did this method would have thrown an
		// error and not continued)
		BaseUI.verify_true_AndLog(testPass, "Did Not find file " + fileName, "Found the file " + fileName);

	}

	// Checks folder and subfolders to make sure that the passed in file name cannot
	// be found.
	public static void verify_FileName_NotFound(String folderToCheck, String fileName) throws Exception {
		File[] listOfFiles = return_NonHiddenFiles(new File(folderToCheck));

		Boolean testPass = true;
		for (File file : listOfFiles) {
			// If we find the file we'll fail the test.
			if (file.getName().equals(fileName)) {
				testPass = false;
				BaseUI.verify_true_AndLog(testPass, "",
						"Found file " + fileName + " and file should not have been there.");
			}

		}
		// Above we check if the file does not exist. So if we get to this point that
		// means the file did not exist (if it did this method would have thrown an
		// error and not continued)
		BaseUI.verify_true_AndLog(testPass, "Did Not find file " + fileName, "Found the file " + fileName);

	}

	// Checks folder top level folder for the specified file.
	public static void verify_FileFound(String folderToCheck, String fileName) throws Exception {

		File directory = new File(folderToCheck);
		File[] listOfFiles = FileOperations.return_NonHiddenFiles(directory);

		Boolean foundFile = false;
		for (File file : listOfFiles) {
			// If we find the file we'll fail the test.
			if (file.getName().equals(fileName)) {
				foundFile = true;
			}

		}

		BaseUI.verify_true_AndLog(foundFile, "Found the file " + fileName, "Did NOT find the file " + fileName);

	}

	public static void verify_FileCount_Correct(String fileLocation, Integer expectedCount) throws Exception {
		File[] listOfExpectedFiles = return_NonHidden_Files_AndSubFiles(fileLocation);

		BaseUI.verify_true_AndLog(listOfExpectedFiles.length == expectedCount,
				"Number of Files matched between expected and actual file arrays.",
				"Expected " + expectedCount.toString() + " files, but seeing "
						+ String.valueOf(listOfExpectedFiles.length) + " files.");
	}

	public static void verify_Files_Match(String expectedFilePath, String actualFilePath) throws Exception {
		File expectedFile = new File(expectedFilePath);
		File actualFile = new File(actualFilePath);

		BaseUI.verify_true_AndLog(FileUtils.contentEquals(expectedFile, actualFile),
				"File " + expectedFile + " matched " + actualFile,
				"File " + expectedFile + " did NOT match " + actualFile);
	}

	// Validates Files, sub directories, and sub files to verify that they matched
	// after copy. Does not perform copy.
	public static void verify_Files_CopiedCorrectly(String source_Directory, String end_Directory,
			Integer expectedCountOfFilesAndDirectories) throws Exception {
		// File expectedFile = new File(source_Directory);
		File[] expectedFilesAndDirectories = return_NonHidden_Files_AndSubFiles(source_Directory);

		// File actualFile = new File(end_Directory);
		File[] actualFilesAndDirectories = return_NonHidden_Files_AndSubFiles(end_Directory);

		BaseUI.verify_true_AndLog(expectedFilesAndDirectories.length == expectedCountOfFilesAndDirectories,
				"File and Directory count matched " + expectedCountOfFilesAndDirectories.toString(),
				"Actual File and Directory count was " + String.valueOf(expectedFilesAndDirectories.length)
						+ " but was expecting " + expectedCountOfFilesAndDirectories.toString());
		BaseUI.verify_true_AndLog(expectedFilesAndDirectories.length == actualFilesAndDirectories.length,
				"Expected and actual files matched in size of " + String.valueOf(expectedFilesAndDirectories.length),
				"Expected file size of " + String.valueOf(expectedFilesAndDirectories.length) + " but seeing "
						+ String.valueOf(actualFilesAndDirectories.length));

		String output = "";
		for (int i = 0; i < expectedFilesAndDirectories.length; i++) {
			if (expectedFilesAndDirectories[i].isFile()) {

				if (!FileUtils.contentEquals(expectedFilesAndDirectories[i], actualFilesAndDirectories[i])) {
					output += "\n File " + expectedFilesAndDirectories[i].toString() + " Did not match "
							+ actualFilesAndDirectories[i].toString();
				} else {
					// Don't need anything in here as if they do equal that's our pass condition.
				}
			} // Directory Validation section. Empty since Directory and Files will have same
				// validation below.
			else {
			}

			// removes source directories from name so that we can compare the rest of the
			// file name.
			String expected_directoryShortened = expectedFilesAndDirectories[i].toString().replace(source_Directory,
					"");
			String actual_directoryShortened = actualFilesAndDirectories[i].toString().replace(end_Directory, "");
			if (!expected_directoryShortened.equals(actual_directoryShortened)) {
				output += "\n File " + expectedFilesAndDirectories[i].toString() + " Did not match "
						+ actualFilesAndDirectories[i].toString();
			} else {
				// don't need anything here as pass condition is handled outside of for loop.
			}
		}
		BaseUI.verify_true_AndLog(output.equals(""), "Files and Directories from " + source_Directory
				+ " matched Files and Directories from " + end_Directory, output);
	}

	public static void verify_File_Matches(String source_Directory_AndFile, String end_Directory_AndFile)
			throws Exception {
		String[] sourceExtract = DataBuilder.Get_FileData_AsString(source_Directory_AndFile).split("\\n");
		String[] endFileExtract = DataBuilder.Get_FileData_AsString(end_Directory_AndFile).split("\\n");

		String output = "";
		BaseUI.verify_true_AndLog(sourceExtract.length == endFileExtract.length, "Files matched in line count.",
				"Files did NOT match in line count.");
		for (int i = 0; i < sourceExtract.length; i++) {
			if (!sourceExtract[i].equals(endFileExtract[i])) {
				output += "\n" + "Source File had line:\n" + sourceExtract[i] + "\n" + "But End file had line:\n"
						+ endFileExtract[i];
			}

		}

		BaseUI.verify_true_AndLog(output.equals(""),
				"File " + source_Directory_AndFile + " matched file " + end_Directory_AndFile, output);
	}

	public static void cleanup_TempFolder() throws Exception {
		String tempDirectory = System.getProperty("java.io.tmpdir");
		cleanup_PriorFiles(tempDirectory);
	}

	public static void cleanup_dafTestUser() throws Exception {
		String tempDirectory = "C:\\Users\\daftestuser\\AppData\\Local\\Temp\\";
		cleanup_PriorFiles(tempDirectory);
	}

	// Clean up prior files based on passed in folder.
	// will delete files that are in folders nested in that folder.
	// also delete folders
	public static void cleanup_PriorFiles(String folderLocation) throws Exception {

		File path = new File(folderLocation);
		BaseUI.log_Status("Cleaning up directory " + folderLocation);

		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					String fileLocation = files[i].toString();
					cleanup_PriorFiles(fileLocation);

					// Check if there are any remaining files (might be some if they were locked).
					// Delete if empty.
					File[] listOfFilesAndFolders = files[i].listFiles();
					if (listOfFilesAndFolders.length == 0) {
						files[i].delete();
					}

				} else {
					boolean fileIsNotLocked = files[i].canWrite();

					if (fileIsNotLocked) {
						// BaseUI.log_Status(files[i].toString());
						files[i].delete();
					}
				}
			}
		}
	}

	// Will only delete the files in the folder that we pass in. Will not delete
	// directories
	public static void deleteFiles_TopFolder(String folderToClear) {
		File folder = new File(folderToClear);
		File[] listOfFilesAndFolders = folder.listFiles();
		BaseUI.log_Status("Deleting files in " + folderToClear);

		for (File file : listOfFilesAndFolders) {
			if (file.isFile()) {
				file.delete();
			}
		}
	}

	public static void create_Folder(String folderLocation) throws IOException {
		BaseUI.log_Status("Creating folder in " + folderLocation);
		Path path = Paths.get(folderLocation);
		if(!Files.exists(path)){
			try{
				Files.createDirectories(path);
			}catch(IOException e){
				e.printStackTrace();
			}

		}
	}

	public static void verify_FolderNotFound(String folderLocation){

		BaseUI.log_Status("Verifying Folder does not exist " + folderLocation);
		Path path = Paths.get(folderLocation);

		boolean directoryNotexists = Files.notExists(path);
		BaseUI.verify_true_AndLog(directoryNotexists, "Did Not Find the folder ", "Found the folder");

	}

	public static void delete_SpecificFile(String fullNameWithPath) {
		BaseUI.log_Status("Deleting file " + fullNameWithPath);

		File fileToDelete = new File(fullNameWithPath);

		fileToDelete.delete();

	}

	// Clean up a single prior file
	public static void cleanup_PriorFile(String folderLocation) throws Exception {

		File path = new File(folderLocation);

		if (path.exists()) {

			boolean fileIsNotLocked = path.canWrite();

			if (fileIsNotLocked) {
				// BaseUI.log_Status(files[i].toString());
				path.delete();
			}

		}
	}

	public static void verify_FileMetadata_BitSize(String[] fileName, int imageBitSize)throws Exception{

		int length = fileName.length;
		for ( int i = 0; i < length; i++ ){
			File file = new File( fileName[i] );
			final BufferedImage bufferImage = ImageIO.read(file);

			BaseUI.verify_true_AndLog(bufferImage.getColorModel().getPixelSize()== imageBitSize,
					"File Bit size " + bufferImage.getColorModel().getPixelSize() + " matches expected Bit size " + imageBitSize ,
					"File Bit size " + bufferImage.getColorModel().getPixelSize() + " did Not match expected Bit size " + imageBitSize);
		}

	}

	public static void unZipFiles(String zipFileWithPath, String unZipFilePath) throws Exception{
		FileInputStream fileInputStream = new FileInputStream(zipFileWithPath);
		Zip.unzip(fileInputStream, new File(unZipFilePath));
	}
}// End of Class
