package utils;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DefectTracker {

	private static String knownIssuesFile = "";
	// private static String knownIssuesFile = "\\src\\inteliData\\data\\KnownIssues.csv";
	private static TableData knownIssues;
	private static String testngLocation = System.getProperty("user.dir");
	static XMLParser parser;

	private static String currentClient = "";

	private static String initialXpath = "//*[local-name()='class'][@name='{0}']//*[local-name()='test-method'][@name='{1}']";

	public static void main(String[] args) throws Exception {
//		System.setProperty("environment", "RC");
//		System.setProperty("clientName", "Citi-9485");
//		knownIssuesFile = "\\src\\dRewards\\data\\KnownIssues.csv";

		knownIssuesFile = args[0];

		currentClient = System.getProperty("clientName");

		knownIssues = DataBuilder.returnTableData_ForComparison(knownIssuesFile, "\\,", true);
		knownIssues.remove_Character("\"", "Environments");
		knownIssues.remove_Character("\"", "Parameters");

		testngLocation += "\\test-output\\testng-results.xml";

		parser = new XMLParser(testngLocation);

		for (HashMap<String, String> issue : knownIssues.data) {

			String[] environments = Arrays.stream(issue.get("Environments").split(",")).map(String::trim).toArray(String[]::new);

			if (!issue.get("Expected Message").trim().isEmpty()
					&& Arrays.asList(environments).contains(System.getProperty("environment"))
					&& clientMatch(currentClient, issue.get("Clients"))) {
				String failTestXpath = initialXpath;

				if (!issue.get("Parameters").trim().isEmpty()) {
					String[] parameters = issue.get("Parameters").split("\\,");
					for (int i = 0; i < parameters.length; i++) {
						failTestXpath += "[./params/param[@index='" + String.valueOf(i) + "']/value[contains(.,'"
								+ parameters[i].replace("\"", "").trim() + "')]]";
					}

				}

				String testMethodXPATH = MessageFormat.format(failTestXpath.replace("'", "''"), issue.get("Test Class"),
						issue.get("Test Method"));
				String messageXPATH = MessageFormat.format(testMethodXPATH.replace("'", "''"), issue.get("Test Class"),
						issue.get("Test Method")) + "/exception/message";
				String reporterXpath = testMethodXPATH + "/reporter-output";
				String lastReporterLineXpath = "(" + reporterXpath + "/line)[last()]";

				Node test_Node = parser.findNode(testMethodXPATH);
				Node reporterNode = parser.findNode(reporterXpath);
				Node messageNode = parser.findNode(messageXPATH);

				if (test_Node == null) {
					System.out.println("Unable to find element by xpath " + testMethodXPATH);
				} else {
					String testStatus = parser.getAttribute(test_Node, "status");
					String issueFrequency = issue.get("Issue Frequency");

					if (testStatus.equals("PASS") && issueFrequency.equals("always")) {
						// Run logic to update test to FAIL with the reason stating that we need to
						// update CSV to fixed and evaluate whether attached bugs can be closed.
						add_XMLInfo_ToTest(reporterNode, lastReporterLineXpath, issue);
						set_TestMightBeFixed(test_Node, testMethodXPATH);

					} else if ((testStatus.equals("FAIL") || testStatus.equals("SKIP"))
							&& issueFrequency.equals("always")) {
						// Check if message matches. If Message Matches we'll update to "KNOWN_ISSUE_
						if (message_Matches_Issue(messageNode, issue.get("Expected Message"))) {
							if (isBlocker(issue)) {
								updateFollowing_FailedTests_ToBlocked(testMethodXPATH, issue);
							}

							add_XMLInfo_ToTest(reporterNode, lastReporterLineXpath, issue);
							set_TestIsKnownIssue(test_Node);
						}

					} else if ((testStatus.equals("FAIL") || testStatus.equals("SKIP"))
							&& issueFrequency.equals("sometimes")) {
						if (isBlocker(issue)) {
							updateFollowing_FailedTests_ToBlocked(testMethodXPATH, issue);
						}
						
						
						// Check if message is a match, if it is then we need to mark it as NEEDS_RERUN
						if (message_Matches_Issue(messageNode, issue.get("Expected Message"))) {
							add_XMLInfo_ToTest(reporterNode, lastReporterLineXpath, issue);
							set_TestNeedsRerun(test_Node);
						}
						
						

					} else if ((testStatus.equals("FAIL") || testStatus.equals("SKIP"))
							&& issueFrequency.equals("fixed")) {
						// Check if message is a match, if it is then we need to mark it as
						// ISSUE_REINTRODUCED
						set_Issue_Was_Reintroduced(test_Node, reporterNode, lastReporterLineXpath);
					} else {
						System.out.println("Did NOT find match for issue " + issue.get("Test Class")
								+ issue.get("Test Method") + " " + issue.get("Expected Message"));
					}

				}
			}
		}
	}

	private static boolean isBlocker(HashMap<String, String> issue) {
		String blockerStatus = issue.get("Blocker");
		if (blockerStatus != null && blockerStatus.equals("TRUE")) {
			return true;
		} else {
			return false;
		}
	}

	private static void updateFollowing_FailedTests_ToBlocked(String testMethodXPATH, HashMap<String, String> issue) throws Exception {
		String followingSiblingXpath_FailedOrSkippedTests = testMethodXPATH + "/following-sibling::test-method[@status = 'FAIL' or @status = 'SKIP']";
		
		
		NodeList followingTests = parser.findNodes(followingSiblingXpath_FailedOrSkippedTests);
		for(int i = 0; i < followingTests.getLength(); i++)
		{
			String currentNodeXpath = "(" + followingSiblingXpath_FailedOrSkippedTests + ")[" + String.valueOf(i + 1) + "]";
			String reporterXpath = currentNodeXpath + "/reporter-output";
			String lastReporterLineXPath =  "(" + reporterXpath + "/line)[last()]";
			
			Node reporterNode = parser.findNode(reporterXpath);
			
			append_OutputLine(reporterNode, lastReporterLineXPath, "Blocked due to issue: " + issue.get("Issue Identifier"));
			append_OutputLine(reporterNode, lastReporterLineXPath, "Originating Test Failure at: " + issue.get("Test Class") + "." + issue.get("Test Method"));
			 
			set_TestIsBlocked(followingTests.item(i));
		}
		
		
		
	}

	private static void add_XMLInfo_ToTest(Node reporterNode, String reporterLastLineXpath,
			HashMap<String, String> issue) throws Exception {
		append_OutputLine(reporterNode, reporterLastLineXpath, "Known Issue: " + issue.get("Issue Identifier"));

		append_OutputLine(reporterNode, reporterLastLineXpath, "Issue Frequency: " + issue.get("Issue Frequency"));

		append_OutputLine(reporterNode, reporterLastLineXpath, "Additional Information: " + issue.get("Issue Message"));

	}

	private static boolean clientMatch(String currentClient, String possibleClients) {
		// if there is no client we'll set this to true. Only DRewards has clients, so
		// other projects will have a null value.
		if (currentClient == null) {
			return true;
		}

		possibleClients = possibleClients.replace("\"", "");

		String[] possibleClientArray = Arrays.stream(possibleClients.split(",")).map(String::trim)
				.toArray(String[]::new);
		;

		if (possibleClients != null && Arrays.asList(possibleClientArray).contains(currentClient)) {
			return true;
		} else {
			return false;
		}

	}

	private static void set_TestMightBeFixed(Node testNode, String testNodeXPATH) throws Exception {
		parser.setAttribute(testNode, "status", "FAIL");

		parser.createNode(testNode, "exception");

		Node exceptionNode = parser.findNode(testNodeXPATH + "/exception");
		parser.setAttribute(exceptionNode, "class", "java.lang.AssertionError");

		parser.createNode(exceptionNode, "message");
		parser.createNode(exceptionNode, "full-stacktrace");

		Node messageNode = parser.findNode(testNodeXPATH + "/exception/message");
		parser.append_CDataNode(messageNode,
				"Test is now passing, but issue in csv is marked with a failure rate of 'always'.  Please check that the issue has been fixed and if so, update Issue Frequency to 'fixed'");

		Node stacktraceNode = parser.findNode(testNodeXPATH + "/exception/full-stacktrace");
		parser.append_CDataNode(stacktraceNode, "No Stacktrace available");

		String nameAttribute = parser.getAttribute(testNode, "name");
		parser.setAttribute(testNode, "name", "POSSIBLY_FIXED_" + nameAttribute);

		parser.applyUpdates_ToXML();
	}

	private static void set_TestIsKnownIssue(Node testNode) throws Exception {
		String nameAttribute = parser.getAttribute(testNode, "name");
		parser.setAttribute(testNode, "name", "KNOWN_ISSUE_" + nameAttribute);

		parser.applyUpdates_ToXML();
	}

	private static void set_TestIsBlocked(Node testNode) throws Exception {
		String nameAttribute = parser.getAttribute(testNode, "name");
		parser.setAttribute(testNode, "name", "BLOCKED_" + nameAttribute);

		parser.applyUpdates_ToXML();
	}

	private static void set_Issue_Was_Reintroduced(Node testNode, Node reporterNode, String reporterLastLineXPath)
			throws Exception {
		append_OutputLine(reporterNode, reporterLastLineXPath,
				"Issue was marked as Fixed, but has reoccured, looks like the issue might have been reintroduced.  Please verify.");

		String nameAttribute = parser.getAttribute(testNode, "name");
		parser.setAttribute(testNode, "name", "ISSUE_REINTRODUCED_" + nameAttribute);

		parser.applyUpdates_ToXML();
	}

	private static void set_TestNeedsRerun(Node testNode) throws Exception {
		String nameAttribute = parser.getAttribute(testNode, "name");
		parser.setAttribute(testNode, "name", "RERUN_" + nameAttribute);

		parser.applyUpdates_ToXML();
	}

	private static void append_OutputLine(Node reporterNode, String lastReporterLineXpath, String textToAppend)
			throws Exception {
		parser.createNode(reporterNode, "line");
		Node lastLine = parser.findNode(lastReporterLineXpath);
		parser.append_CDataNode(lastLine, textToAppend);
	}

	// I want to update this at some point to match based on a regex expression,
	// should have some format that allows us to specify parts of the expected
	// message are not going to match exactly.
	private static boolean message_Matches_Issue(Node messageNode, String expectedMessage) {
		if (messageNode == null) {
			return false;
		}

		if (expectedMessage.startsWith("\"") && expectedMessage.endsWith("\"")) {
			expectedMessage = expectedMessage.substring(1, expectedMessage.length() - 1);
		}

		String messageNodeText = messageNode.getTextContent().trim();
		// messageNodeText = "FAIL - Date asdfdsf was NOT between sdfdff and asdfd";

		if (expectedMessage.contains("*")) {
			if (!expectedMessage.startsWith("*")) {
				expectedMessage = "*" + expectedMessage;
			}
			if (!expectedMessage.endsWith("*")) {
				expectedMessage += "*";
			}
			expectedMessage = expectedMessage.replace("\"\"", "\"");

			expectedMessage = wildcardToRegex(expectedMessage);

			boolean messageMatches;
			if(messageNodeText.contains("\n")) {
				Pattern p = Pattern.compile(expectedMessage, Pattern.MULTILINE);
				messageMatches = p.matcher(messageNodeText).find();
			}else{
				Pattern p = Pattern.compile(expectedMessage);
				messageMatches = p.matcher(messageNodeText).find();
			}

			return messageMatches;
		} else {
			return messageNodeText.contains(expectedMessage);
		}

	}

	public static String wildcardToRegex(String wildcard) {
		StringBuffer s = new StringBuffer(wildcard.length());
		s.append('^');
		for (int i = 0, is = wildcard.length(); i < is; i++) {
			char c = wildcard.charAt(i);
			switch (c) {
			case '*':
				s.append(".*");
				break;
			case '?':
				s.append(".");
				break;
			// escape special regexp-characters
			case '(':
			case ')':
			case '[':
			case ']':
			case '$':
			case '^':
			case '.':
			case '{':
			case '}':
			case '|':
			case '\\':
				s.append("\\");
				s.append(c);
				break;
			default:
				s.append(c);
				break;
			}
		}
		s.append('$');
		return (s.toString());
	}

}// End of class
