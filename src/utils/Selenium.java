package utils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Selenium {
    public static void verifySeleniumIsRunning() {
        BaseUI.log_Status("Checking whether Selenium is running...");

        try (final CloseableHttpClient httpClient = HttpClients.createDefault()) {
            // Get the base URL the same way Selenium does
            final String baseUrl = System.getProperty("webdriver.remote.server", "http://localhost:4444/wd/hub");
            final String statusUrl = baseUrl + "/status";
            final HttpGet request = new HttpGet(statusUrl);

            // If Selenium isn't running, the following "execute" call will throw.
            try (final CloseableHttpResponse response = httpClient.execute(request)) {
                BaseUI.log_Status("Selenium is running. Response was: " + response.getStatusLine());

                // Read the entire response so the HTTP stream can be closed gracefully
                final HttpEntity entity = response.getEntity();
                EntityUtils.consume(entity);
            }
        } catch (IOException ioException) {
            BaseUI.log_AndFail("Unable to connect to the Selenium server. Make sure the Selenium server is running.");
        }
    }
}
