package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

public class StreamGobbler extends Thread {
    private InputStream _inputStream;
    private Consumer<String> _logMessage;

    public StreamGobbler(InputStream inputStream, Consumer<String> logMessage) {
        _inputStream = inputStream;
        _logMessage=logMessage;
    }

    public void run() {
        try {
            InputStreamReader reader = new InputStreamReader(_inputStream);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            while((line=bufferedReader.readLine())!=null) {
                _logMessage.accept(line);
            }
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
