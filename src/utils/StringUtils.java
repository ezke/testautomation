package utils;

public final class StringUtils {

    private static final String LEFT_ANGLE_QUOTE = "\u00AB";
    private static final String RIGHT_ANGLE_QUOTE = "\u00BB";

    private StringUtils() {
    }

    public static String angleQuote(String value) {
        if (value == null)
            return "null";

        return LEFT_ANGLE_QUOTE + value + RIGHT_ANGLE_QUOTE;
    }

    public static String getOptions(String valueToSplit, int numberOfIndexesToKeep) {

        String[] splitString = valueToSplit.split("\\,");

        String finalOutput = "";
        for (int i = 0; i < numberOfIndexesToKeep; i++) {
            finalOutput += splitString[i];
            if (numberOfIndexesToKeep - 1 != i) {
                finalOutput += ",";
            }
        }
        return finalOutput;
    }
}
