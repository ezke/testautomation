package utils.winiumXRay;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import utils.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

public class XRayUtils {
    private static final Duration DEFAULT_XRAY_TIMEOUT = Duration.ofSeconds(30);

    private static String _xrayLocation = "";

    private static Document _xmlDocument;

    public static void setXrayLocation(String xrayLocation) {
        _xrayLocation = xrayLocation;
    }

    public static void invalidateXRay() {
        _xmlDocument = null;
    }

    public static String getXrayLocation() {
        return _xrayLocation;
    }

    public static Document getXML() throws Exception {
        if (_xmlDocument == null) {
            takeXray(DEFAULT_XRAY_TIMEOUT);
        }
        return _xmlDocument;
    }

    public static void takeXray(Duration timeToWait) throws Exception {
        FileOperations.delete_SpecificFile(_xrayLocation);

        Actions builder = new Actions(Browser.driver);

        builder.keyDown(Keys.LEFT_ALT);
        builder.keyDown(Keys.LEFT_CONTROL);
        builder.build().perform();

        builder.sendKeys("s").build().perform();

        builder.keyUp(Keys.LEFT_CONTROL);
        builder.keyUp(Keys.LEFT_ALT).build().perform();

        //Adding sleep here, saw some interesting behavior after ctrl+alt+s that could interfere with trying to perform operations directly after.
        Thread.sleep(500);

        _xmlDocument = loadXmlFile(timeToWait);
    }

    private static Document loadXmlFile(Duration timeToWait) throws TimeoutException {
        LocalDateTime endTime = LocalDateTime.now().plus(timeToWait);
        while (LocalDateTime.now().isBefore(endTime)) {
            try {
                return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(_xrayLocation));
            } catch (SAXException | ParserConfigurationException | IOException e) {
                // continue looping
            }
        }

        throw new TimeoutException("Thick client did not respond to screen-scrape request within the specified " +
                BaseUI.durationToString(timeToWait) + " timeout");
    }

    private static String getSubNodeValue(String elementName, String elementVariable1, String elementVariable2, String subNode) throws Exception {

        Document xrayXML = XRayUtils.getXML();

        XMLParser parser = XMLParser.getXMLParserWithContent(_xrayLocation, xrayXML);
        
        String xpath = Locator.get_XR_XPath(elementName, elementVariable1, elementVariable2);

        String valueWeWant = "";
        Node parserNode = parser.findNode(xpath);
        NodeList nodes = parserNode.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) nodes.item(i);
                if (el.getTagName().equals(subNode)) {
                    valueWeWant = el.getTextContent();

                    break;
                }
            }
        }

        return valueWeWant;
    }

    public static String getTooltip(String elementName, String elementVariable1, String elementVariable2) throws Exception {
        invalidateXRay();
        return getSubNodeValue(elementName, elementVariable1, elementVariable2, "WidgetToolTip");
    }


    public static TableData return_Table(String[] columnNames) throws Exception {
        TableData xmlTableData = new TableData();

        Document xrayXML = XRayUtils.getXML();

        XMLParser parser = XMLParser.getXMLParserWithContent(_xrayLocation, xrayXML);

        for (String header : columnNames) {
            NodeList columnCellList = parser.findNodes("//ttbBrowseRowDetail[./WidgetType[./text()='Column (FILL-IN)']][./WidgetLabel[./text()='" + header + "']]/WidgetValue");

            for (int i = 0; i < columnCellList.getLength(); i++) {
                HashMap<String, String> tableRow;

                if (xmlTableData.data.size() == 0 || xmlTableData.data.size() <= i) {
                    tableRow = new HashMap<>();
                    tableRow.put(header, columnCellList.item(i).getTextContent().trim());
                    xmlTableData.data.add(tableRow);
                } else {
                    xmlTableData.data.get(i).put(header, columnCellList.item(i).getTextContent().trim());
                }
            }
        }
        return xmlTableData;
    }

    public static void navigate_ToLocation_InTable(TableData tableToMatchAgainst, String columnName, String columnValue) throws Exception {

        int indexOfItem = tableToMatchAgainst.first_IndexOf(columnName, columnValue);

        for (int i = 0; i < indexOfItem; i++) {
            BaseUI.send_KeyWithActionBuilder(Keys.DOWN);
        }
    }
}
