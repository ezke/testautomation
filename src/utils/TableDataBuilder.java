package utils;

import java.util.ArrayList;
import java.util.HashMap;

public class TableDataBuilder {
    private final ArrayList<HashMap<String, String>> _rows = new ArrayList<>();

    public TableData build() {
        return new TableData(_rows);
    }

    private HashMap<String, String> getRow(int rowIndex) {
        while (_rows.size() <= rowIndex) {
            _rows.add(new HashMap<>());
        }
        return _rows.get(rowIndex);
    }

    public void put(int rowIndex, String key, String value) {
        getRow(rowIndex).put(key, value);
    }
}
