package utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConnectionStringParser {
    private static final Map<String, String> _patterns = new HashMap<String, String>() {{
        put("Server", "(?<=^[\\w:]{0,40}//)[^;]+");
        put("Database", "(?<=;\\s{0,15}databaseName=)[^;]+");
        put("User", "(?<=;\\s{0,15}user=)[^;]+");
        put("Password", "(?<=;\\s{0,15}password=)[^;]+");
    }};
    private Map<String, String> _values = new HashMap<String, String>();

    public ConnectionStringParser(String connectionString) {
        for (Map.Entry<String, String> pattern : _patterns.entrySet()) {
            Matcher matcher = Pattern.compile(pattern.getValue()).matcher(connectionString);
            matcher.find();
            _values.put(pattern.getKey(), matcher.group());
        }
    }

    public String getServer() {
        return _values.get("Server");
    }

    public String getDatabase() {
        return _values.get("Database");
    }

    public String getUser() {
        return _values.get("User");
    }

    public String getPassword() {
        return _values.get("Password");
    }
}
