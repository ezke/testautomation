package utils.unitTests;

import org.testng.annotations.Test;
import utils.BaseUI;
import utils.CommandLine;
import utils.DataBuilder;
import utils.Is;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandLineTest {
    @Test
    public void validateMixOfStandardAndErrorMessages() throws IOException, InterruptedException {
        List<String> standardList = new ArrayList<String>();
        List<String> errorList = new ArrayList<String>();
        CommandLine.execute(standardMessage->standardList.add(standardMessage),
                errorMessage->errorList.add(errorMessage),
                DataBuilder.Get_FileData_Path("\\src\\utils\\unitTests\\unitTestData\\StandardWhileErrorMessages_Powershell.ps1"));
        BaseUI.assertThat(standardList.size(), Is.equalTo(1000));
        BaseUI.assertThat(errorList.size(), Is.equalTo(1000));
    }

    @Test
    public void validateFirstStandardThenErrorMessages() throws IOException, InterruptedException {
        List<String> standardList = new ArrayList<String>();
        List<String> errorList = new ArrayList<String>();
        CommandLine.execute(standardMessage->standardList.add(standardMessage),
                errorMessage->errorList.add(errorMessage),
                DataBuilder.Get_FileData_Path("\\src\\utils\\unitTests\\unitTestData\\StandardMessagesThenErrorMessages_Powershell.ps1"));
        BaseUI.assertThat(standardList.size(), Is.equalTo(1000));
        BaseUI.assertThat(errorList.size(), Is.equalTo(1000));
    }

    @Test
    public void validateFirstErrorThenStandardMessages() throws IOException, InterruptedException {
        List<String> standardList = new ArrayList<String>();
        List<String> errorList = new ArrayList<String>();
        CommandLine.execute(standardMessage->standardList.add(standardMessage),
                errorMessage->errorList.add(errorMessage),
                DataBuilder.Get_FileData_Path("\\src\\utils\\unitTests\\unitTestData\\ErrorMessagesThenStandardMessages_Powershell.ps1"));
        BaseUI.assertThat(standardList.size(), Is.equalTo(1000));
        BaseUI.assertThat(errorList.size(), Is.equalTo(1000));
    }
}
