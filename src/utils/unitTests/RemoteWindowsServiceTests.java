package utils.unitTests;

import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Is;
import utils.RemoteWindowsService;
import utils.WindowsServiceState;

public class RemoteWindowsServiceTests {
    @Test
    public void parseStateFromQueryOutput_Stopped() {
        WindowsServiceState state = RemoteWindowsService.parseStateFromQueryOutput(
                "\n" +
                        "SERVICE_NAME: abc \n" +
                        "        TYPE               : 10  WIN32_OWN_PROCESS  \n" +
                        "        STATE              : 1  STOPPED \n" +
                        "        WIN32_EXIT_CODE    : 0  (0x0)\n" +
                        "        SERVICE_EXIT_CODE  : 0  (0x0)\n" +
                        "        CHECKPOINT         : 0x0\n" +
                        "        WAIT_HINT          : 0x0");
        BaseUI.assertThat(state, Is.equalTo(WindowsServiceState.STOPPED));
    }

    @Test
    public void parseStateFromQueryOutput_Starting() {
        WindowsServiceState state = RemoteWindowsService.parseStateFromQueryOutput(
                "\n" +
                        "SERVICE_NAME: abc \n" +
                        "        TYPE               : 10  WIN32_OWN_PROCESS  \n" +
                        "        STATE              : 2  START_PENDING \n" +
                        "                                (NOT_STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN)\n" +
                        "        WIN32_EXIT_CODE    : 0  (0x0)\n" +
                        "        SERVICE_EXIT_CODE  : 0  (0x0)\n" +
                        "        CHECKPOINT         : 0x0\n" +
                        "        WAIT_HINT          : 0x7d0");
        BaseUI.assertThat(state, Is.equalTo(WindowsServiceState.STARTING));
    }

    @Test
    public void parseStateFromQueryOutput_Stopping() {
        WindowsServiceState state = RemoteWindowsService.parseStateFromQueryOutput(
                "\n" +
                        "SERVICE_NAME: abc \n" +
                        "        TYPE               : 10  WIN32_OWN_PROCESS  \n" +
                        "        STATE              : 3  STOP_PENDING \n" +
                        "                                (STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN)\n" +
                        "        WIN32_EXIT_CODE    : 0  (0x0)\n" +
                        "        SERVICE_EXIT_CODE  : 0  (0x0)\n" +
                        "        CHECKPOINT         : 0x0\n" +
                        "        WAIT_HINT          : 0x0");
        BaseUI.assertThat(state, Is.equalTo(WindowsServiceState.STOPPING));
    }

    @Test
    public void parseStateFromQueryOutput_Running() {
        WindowsServiceState state = RemoteWindowsService.parseStateFromQueryOutput(
                "\n" +
                        "SERVICE_NAME: abc \n" +
                        "        TYPE               : 10  WIN32_OWN_PROCESS  \n" +
                        "        STATE              : 4  RUNNING \n" +
                        "                                (STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN)\n" +
                        "        WIN32_EXIT_CODE    : 0  (0x0)\n" +
                        "        SERVICE_EXIT_CODE  : 0  (0x0)\n" +
                        "        CHECKPOINT         : 0x0\n" +
                        "        WAIT_HINT          : 0x0");
        BaseUI.assertThat(state, Is.equalTo(WindowsServiceState.RUNNING));
    }
}
