package utils.unitTests;

import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Is;

public class UnicodeTests {
    @Test
    public void compilerReadsJavaFilesAsUtf8() {
        BaseUI.assertThat("\u2022", Is.equalTo("•"));
    }
}
