package utils.unitTests;

import utils.BaseUI;
import utils.Is;

public class BaseHamcrestTest {
    protected interface Action {
        void run() throws Exception;
    }

    protected void assertFails(Action action, String expectedExceptionMessage) {

        boolean gotException;
        try {
            action.run();
            gotException = false;
        } catch (AssertionError e) {
            gotException = true;
            BaseUI.assertThat(e.getMessage(), Is.equalTo(expectedExceptionMessage));
        } catch (Exception e) {
            throw new AssertionError("Expected AssertionError but got: " + e);
        }

        BaseUI.assertThat("Was an exception thrown?", gotException, Is.equalTo(true));
    }
}
