package utils.unitTests;

import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Is;
import utils.Wait;

import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class WaitTests {

    private boolean areWeBeingCalledFromAwaitilityPolling() {
        String stackTrace = Arrays.toString(Thread.currentThread().getStackTrace());
        return stackTrace.contains("java.util.concurrent.FutureTask");
    }

    @Test
    public void until_ImmediateSuccess() {
        Queue<String> valuesToReturn = new LinkedList<>(Arrays.asList(
                "Running",
                "<EOL>"));

        Wait.atMost(Duration.ofSeconds(1)).until("Service state",
                valuesToReturn::remove,
                Is.equalTo("Running"));

        BaseUI.assertThat("Next value in queue", valuesToReturn.remove(), Is.equalTo("<EOL>"));
    }

    @Test
    public void until_RetryUntilSuccess() {
        Queue<String> valuesToReturn = new LinkedList<>(Arrays.asList(
                "Stopped",
                "Starting",
                "Starting",
                "Running",
                "<EOL>"));

        Wait.atMost(Duration.ofSeconds(1)).until(
                "Service state",
                valuesToReturn::remove,
                Is.equalTo("Running"));

        BaseUI.assertThat("Next value in queue", valuesToReturn.remove(), Is.equalTo("<EOL>"));
    }

    @Test
    public void until_CallbackNeverMatchesAndTimeoutExpires() {
        boolean didThrow = false;
        try {
            Wait.atMost(Duration.ofSeconds(1)).until("Service state",
                    () -> "Starting",
                    Is.equalTo("Running"));
        } catch (AssertionError e) {
            didThrow = true;
            BaseUI.assertThat(e.getMessage(), Is.equalTo(
                    "Timeout expired (0:01) while waiting for \"Service state\"\n" +
                            "Expected: \"Running\"\n" +
                            "     but: was \"Starting\""));
        }
        BaseUI.assertThat("Was an exception thrown?", didThrow, Is.equalTo(true));
    }

    @Test
    public void until_CallbackThrowsException() {
        boolean didThrow = false;
        try {
            Wait.atMost(Duration.ofSeconds(1)).until("Service state",
                    () -> {
                        throw new Exception("Service not found");
                    },
                    Is.equalTo("Running"));
        } catch (RuntimeException e) {
            didThrow = true;
            BaseUI.assertThat(e.getMessage(), Is.equalTo(
                    "An error occurred while waiting for \"Service state\"\n" +
                            "Service not found"));
        }
        BaseUI.assertThat("Was an exception thrown?", didThrow, Is.equalTo(true));
    }

    @Test
    public void until_TimeoutExpires_ThenCallbackReturnsTheCorrectValue() {
        // Java lambdas can't modify variables - use arrays as a workaround
        boolean[] wasCalledFromAwaitility = {false};
        boolean[] wasCalledNotFromAwaitility = {false};

        Wait.atMost(Duration.ofSeconds(1)).until("Service state",
                () -> {
                    if (areWeBeingCalledFromAwaitilityPolling()) {
                        wasCalledFromAwaitility[0] = true;
                        return "Started";
                    } else {
                        // Awaitility polling is done; Wait is getting the value one last time to
                        // build its failure message. If it matches the expected value this time,
                        // we should ignore the timeout and allow the wait to pass.
                        wasCalledNotFromAwaitility[0] = true;
                        return "Running";
                    }
                },
                Is.equalTo("Running"));

        // Double-check that we went through the code paths we expected
        BaseUI.assertThat("Was the callback called from inside Awaitility's polling loop?",
                wasCalledFromAwaitility[0], Is.equalTo(true));
        BaseUI.assertThat("Was the callback called after Awaitility's polling loop ended?",
                wasCalledNotFromAwaitility[0], Is.equalTo(true));
    }

    @Test
    public void until_TimeoutExpires_ThenCallbackThrowsExceptionWhenBuildingFinalFailureMessage() {
        boolean didThrow = false;
        try {
            Wait.atMost(Duration.ofSeconds(1)).until("Service state",
                    () -> {
                        if (!areWeBeingCalledFromAwaitilityPolling()) {
                            throw new Exception("Service not found");
                        }
                        return "Starting";
                    },
                    Is.equalTo("Running"));
        } catch (RuntimeException e) {
            didThrow = true;
            BaseUI.assertThat(e.getMessage(), Is.equalTo(
                    "An error occurred while waiting for \"Service state\"\n" +
                            "Service not found"));
        }
        BaseUI.assertThat("Was an exception thrown?", didThrow, Is.equalTo(true));
    }

    @Test
    public void until_CallbackTakesLongerThanTimeout() {
        boolean didThrow = false;
        try {
            Wait.atMost(Duration.ofMillis(200)).until("Service state",
                    () -> {
                        Thread.sleep(300);
                        return null;
                    },
                    Is.equalTo("Running"));
        } catch (RuntimeException e) {
            didThrow = true;
            BaseUI.assertThat(e.getMessage(), Is.equalTo(
                    "An error occurred while waiting for \"Service state\"\n" +
                            "java.util.concurrent.TimeoutException"));
        }
        BaseUI.assertThat("Was an exception thrown?", didThrow, Is.equalTo(true));
    }
}
