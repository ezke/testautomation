package utils.unitTests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Is;

import java.time.Duration;
import java.util.HashMap;

public class BaseUITests {
    @DataProvider(name = "durations")
    public Object[][] durationToStringData() {
        return new Object[][]{
                new Object[]{Duration.ofSeconds(1), "0:01"},
                new Object[]{Duration.ofSeconds(60), "1:00"},
                new Object[]{Duration.ofMinutes(15), "15:00"},
        };
    }

    @Test(dataProvider = "durations")
    public void durationToString(Duration duration, String expectedString) {
        String actual = BaseUI.durationToString(duration);
        BaseUI.assertThat(actual, Is.equalTo(expectedString));
    }

    @DataProvider(name = "isNumber")
    public Object[][] isNumberData() {
        return new Object[][]{
                new Object[]{"1", true},
                new Object[]{"1.5", true},
                new Object[]{"-1", true},
                new Object[]{".0", true},
                new Object[]{"-1.234", true},
                new Object[]{"- 1", false},
                new Object[]{"- 0.0", false},
                new Object[]{"", false},
                new Object[]{" ", false},
                new Object[]{"-", false},
                new Object[]{"-a", false},
                new Object[]{"b", false},
                new Object[]{".", false},
        };
    }

    @Test(dataProvider = "isNumber")
    public void isNumber(String stringToCheck, boolean isANumber) {
        BaseUI.assertThat(BaseUI.string_isNumeric(stringToCheck), Is.equalTo(isANumber));
    }

    @Test
    public void cloneHashMap_CopiesData() {
        HashMap<String, String> original = new HashMap<>();
        original.put("A", "1");
        original.put("B", "2");
        HashMap<String, String> clone = BaseUI.cloneHashMap(original);

        BaseUI.assertThat(clone, Is.equalTo(original));
    }

    @Test
    public void cloneHashMap_CreatesNewInstance() {
        HashMap<String, String> original = new HashMap<>();
        original.put("A", "1");
        original.put("B", "2");
        HashMap<String, String> clone = BaseUI.cloneHashMap(original);

        BaseUI.assertThat(clone, Is.differentInstanceThan(original));
    }
}
