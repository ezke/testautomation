package utils.unitTests;

import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Is;
import utils.TableData;

import java.util.HashMap;

public class TableDataTests {

    private TableData createSampleData() {
        TableData tableData = new TableData();

        HashMap<String, String> row1 = new HashMap<>();
        row1.put("Col1", "A");
        row1.put("Col2", "B");
        tableData.data.add(row1);

        HashMap<String, String> row2 = new HashMap<>();
        row2.put("Col1", "C");
        row2.put("Col2", "D");
        tableData.data.add(row2);

        return tableData;
    }

    private TableData createSampleData_MultipleWords(){
        TableData tableData = new TableData();

        HashMap<String, String> row1 = new HashMap<>();
        row1.put("Col1", "a blue sky");
        row1.put("Col2", "BLUE");
        tableData.data.add(row1);

        HashMap<String, String> row2 = new HashMap<>();
        row2.put("Col1", "73 dAYS TO go");
        row2.put("Col2", "hELLO");
        tableData.data.add(row2);

        return tableData;
    }

    @Test
    public void capitalizeFirstLetter_MatchesExpected() {
        TableData tableData = createSampleData_MultipleWords();
        tableData.capitalize_FirstLetter_ForColumn("Col1");
        tableData.capitalize_FirstLetter_ForColumn("Col2");

        BaseUI.assertThat(tableData.data.get(0).get("Col1"), Is.equalTo("A Blue Sky"));
        BaseUI.assertThat(tableData.data.get(0).get("Col2"), Is.equalTo("Blue"));
        BaseUI.assertThat(tableData.data.get(1).get("Col1"), Is.equalTo("73 Days To Go"));
        BaseUI.assertThat(tableData.data.get(1).get("Col2"), Is.equalTo("Hello"));

    }

    @Test
    public void cloneTable_CloneHasSameData() {
        TableData original = createSampleData();
        TableData clone = original.cloneTable();

        BaseUI.assertThat(clone.rowCount(), Is.equalTo(original.rowCount()));
        BaseUI.assertThat(clone.toString(), Is.equalTo(original.toString()));
    }

    @Test
    public void cloneTable_CreatesNewRows() {
        TableData original = createSampleData();
        TableData clone = original.cloneTable();

        HashMap<String, String> originalFirstRow = original.data.get(0);
        HashMap<String, String> cloneFirstRow = clone.data.get(0);
        BaseUI.assertThat("Cloned row", cloneFirstRow, Is.differentInstanceThan(originalFirstRow));
    }
}
