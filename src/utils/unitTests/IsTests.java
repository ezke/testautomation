package utils.unitTests;

import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IsTests extends BaseHamcrestTest {
    @Test
    public void equalTo_Pass() {
        BaseUI.assertThat("message", 10, Is.equalTo(10));
    }

    @Test
    public void equalTo_Fail() {
        assertFails(
                () -> BaseUI.assertThat("message", 10, Is.equalTo(15)),
                "message\nExpected: <15>\n     but: was <10>");
    }

    @Test
    public void notEqualTo_Pass() {
        BaseUI.assertThat("message", 10, Is.not(Is.equalTo(15)));
    }

    @Test
    public void notEqualTo_Fail() {
        assertFails(
                () -> BaseUI.assertThat("message", 10, Is.not(Is.equalTo(10))),
                "message\nExpected: not <10>\n     but: was <10>");
    }

    @Test
    public void isEmptyCollection_Pass() {
        List<String> list = new ArrayList<>();
        BaseUI.assertThat("message", list, Is.emptyCollection());
    }

    @Test
    public void isEmptyCollection_Fail() {
        List<Integer> list = Arrays.asList(5, 2, 8);
        assertFails(
                () -> BaseUI.assertThat("message", list, Is.emptyCollection()),
                "message\nExpected: an empty collection\n     but: <[5, 2, 8]>");
    }

    @Test
    public void isNonEmptyCollection_Pass() {
        List<Integer> list = Arrays.asList(5, 2, 8);
        BaseUI.assertThat("message", list, Is.nonEmptyCollection());
    }

    @Test
    public void isNonEmptyCollection_Fail() {
        List<String> list = new ArrayList<>();
        assertFails(
                () -> BaseUI.assertThat("message", list, Is.nonEmptyCollection()),
                "message\nExpected: not an empty collection\n     but: was <[]>");
    }

    @Test
    public void sameInstanceAs_Pass() {
        List<String> collection = Arrays.asList("abc", "def");
        BaseUI.assertThat("message", collection, Is.sameInstanceAs(collection));
    }

    @Test
    public void sameInstanceAs_Fail() {
        List<String> first = Arrays.asList("abc", "def");
        List<String> second = Arrays.asList("abc", "def");
        assertFails(
                () -> BaseUI.assertThat("message", first, Is.sameInstanceAs(second)),
                "message\nExpected: sameInstance(<[abc, def]>)\n     but: was <[abc, def]>");
    }

    @Test
    public void differentInstanceThan_Pass() {
        List<String> first = Arrays.asList("abc", "def");
        List<String> second = Arrays.asList("abc", "def");
        BaseUI.assertThat("message", first, Is.differentInstanceThan(second));
    }

    @Test
    public void differentInstanceThan_Fail() {
        List<String> collection = Arrays.asList("abc", "def");
        assertFails(
                () -> BaseUI.assertThat("message", collection, Is.differentInstanceThan(collection)),
                "message\nExpected: not sameInstance(<[abc, def]>)\n     but: was <[abc, def]>");
    }

    @Test
    public void contains_String_Pass() {
        BaseUI.assertThat("apple, pear, banana", Is.contains("pear"));
    }

    @Test
    public void contains_String_Fail() {
        assertFails(
                ()->BaseUI.assertThat("message", "apple, kiwi, banana", Is.contains("pear")),
                "message\nExpected: a string containing \"pear\"\n     but: was \"apple, kiwi, banana\""
        );
    }
}
