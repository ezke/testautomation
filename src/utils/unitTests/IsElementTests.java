package utils.unitTests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.IsElement;

import static org.mockito.Mockito.*;

public class IsElementTests extends BaseHamcrestTest {
    private WebElement elementWithText(String text) {
        WebElement element = mock(WebElement.class);

        // Give the element an ugly toString(), so we can make sure it
        // doesn't show up in any failure messages
        when(element.toString()).thenReturn("@@ELEMENT@@");

        when(element.getText()).thenReturn(text);
        return element;
    }

    @Test
    public void withText_Pass() {
        WebElement element = elementWithText("This is my text");
        BaseUI.assertThat("message", element, IsElement.withText("This is my text"));
    }

    @Test
    public void withText_IsNotASubstringMatch() {
        WebElement element = elementWithText("Long string");

        assertFails(
                () -> BaseUI.assertThat("message", element, IsElement.withText("str")),
                "message\n" +
                        "Expected: element with text <str>\n" +
                        "     but: was element with text <Long string>");
    }

    @Test
    public void withTextContaining_Pass() {
        WebElement element = elementWithText("Long string");
        BaseUI.assertThat("message", element, IsElement.withTextContaining("g s"));
    }

    @Test
    public void withTextContaining_Fail() {
        WebElement element = elementWithText("Long string");
        assertFails(
                () -> BaseUI.assertThat("message", element, IsElement.withTextContaining("xyz")),
                "message\n" +
                        "Expected: element containing text <xyz>\n" +
                        "     but: was element with text <Long string>");
    }

    @Test
    public void withTextMatchingRegex_Pass() {
        WebElement element = elementWithText("ABC   123");
        BaseUI.assertThat("message", element, IsElement.withTextMatchingRegex("\\w+\\s+\\d+"));
    }

    @Test
    public void withTextMatchingRegex_IsAnchored() {
        WebElement element = elementWithText("ABC   123");
        assertFails(
                () -> BaseUI.assertThat("message", element, IsElement.withTextMatchingRegex("\\d")),
                "message\n" +
                        "Expected: element with text matching regex <\\d>\n" +
                        "     but: was element with text <ABC   123>");
    }
}
