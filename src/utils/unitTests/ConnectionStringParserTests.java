package utils.unitTests;

import utils.BaseUI;
import utils.ConnectionStringParser;

import org.testng.annotations.Test;
import utils.Is;

public class ConnectionStringParserTests {
    @Test
    public void parseValidJDBCConnectionString() {
        String connectionString = "jdbc:sqlserver://server;user=user;password=password;databaseName=database";
        ConnectionStringParser connection=new ConnectionStringParser(connectionString);
        BaseUI.assertThat(connection.getServer(), Is.equalTo("server"));
        BaseUI.assertThat(connection.getDatabase(), Is.equalTo("database"));
        BaseUI.assertThat(connection.getUser(), Is.equalTo("user"));
        BaseUI.assertThat(connection.getPassword(), Is.equalTo("password"));
    }

    @Test
    public void parseJDBCConnectionStringInDifferentOrder() {
        String connectionString = "jdbc:sqlserver://server;databaseName=database;user=user;password=password";
        ConnectionStringParser connection=new ConnectionStringParser(connectionString);
        BaseUI.assertThat(connection.getServer(), Is.equalTo("server"));
        BaseUI.assertThat(connection.getDatabase(), Is.equalTo("database"));
        BaseUI.assertThat(connection.getUser(), Is.equalTo("user"));
        BaseUI.assertThat(connection.getPassword(), Is.equalTo("password"));
    }
}
