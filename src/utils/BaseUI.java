// LET 12/14/15: BaseUI.java
// Purpose: Calls to Webelement.{action} from pages files. In short, these are the actions that actually interact with webelements/objects.
// This is partially done to provide a single location where the default behavior of webelement.click can be overridden by click(webelement)
// such as adding additional wait statements, logging, or other functionality.
// In general these calls should be made instead of calling the webelement actions. In addition to items available on the right click menu,
// other generic functionality (does the element appear? What is the value of the element? etc) appear in this file.
package utils;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import org.jetbrains.annotations.Nullable;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.asserts.SoftAssert;
import testng_Listeners.ExtentReportListener2;
import static java.lang.System.currentTimeMillis;
import static org.testng.AssertJUnit.fail;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import org.awaitility.Awaitility;

public final class BaseUI {

	private BaseUI() {
	}

	// Generic action to click on a field. Typically used with buttons, links,
	// checkboxes, but can be used by anything
	public static void click(@Nullable WebElement buttonToClick) {
		if (buttonToClick == null) {
			String failLogText = "The element does not exist.";
			log_AndFail(failLogText);
		} else {
			String textToLog = "Clicking on Webelement";
			if(!Browser.currentBrowser.equals("winium")) {
				textToLog += " " + buttonToClick.getText();
			}
			log_Status(textToLog);


			buttonToClick.click();
		}
	}

	public static void double_click(@Nullable WebElement buttonToClick) {
		if (buttonToClick == null) {
			String failLogText = "The element does not exist.";
			log_AndFail(failLogText);
		} else {
			log_Status("Double clicking on WebElement " + buttonToClick.getText());
			Actions action = new Actions(Browser.driver);
			action.doubleClick(buttonToClick).perform();
		}
	}

	public static void open_NewWindow_And_SwitchToIt(String url) {
		ArrayList<String> priorHandles = new ArrayList<String>();
		priorHandles.addAll(Browser.driver.getWindowHandles());

		String javascriptCommand = "$(window.open(''))";
		JavascriptExecutor executor = (JavascriptExecutor) Browser.driver;
		executor.executeScript(javascriptCommand);

		wait_ForNewWindow_ToExist_AndSwitchToIt(priorHandles, Duration.of(20, ChronoUnit.SECONDS));

		Browser.driver.navigate().to(url);

	}

	// Verify that the Audit entries exist in the report for given time.
	public static void verify_TableColumn_hasValue_MatchString(String columnToVerify1, String columnValueNeeded1, String columnToVerify2, String columnValueNeeded2,String columnToVerify3, String columnValueNeeded3,
															   ArrayList<HashMap<String, String>> firstTable) throws Exception {

		BaseUI.verify_true_AndLog(firstTable.size() > 0, "Found rows to test.", "Did not find rows to test.");

		String output = "", newFormat = "", data2 = null, data3 = null;
		int i = 0, fnd = 0;
		for (HashMap<String, String> tableRow : firstTable) {
			newFormat = return_Date_AsDifferentFormat(tableRow.get(columnToVerify1), "MM/dd/yy h:mm:ss a", "MM/dd/yy h:mm a");
			if ((columnValueNeeded1.equals(newFormat)) && (columnValueNeeded2.equals(data2 = tableRow.get(columnToVerify2))) && (columnValueNeeded3.equals(data3 = tableRow.get(columnToVerify3)))) {
				output += "\nIndex(" + i + ") " + columnToVerify1 + ": " + newFormat + ", " + columnToVerify2 + ": " + data2 + ", " + columnToVerify3 + ": " + data3;
				fnd++;
				BaseUI.verify_true_AndLog(fnd >= 0, output, "Did not find: " + columnToVerify1);
			}
			i++;
		}
		BaseUI.verify_true_AndLog(fnd > 0, output, "Did not find: " + columnToVerify1);
	}


	public static String wait_ForNewWindow_ToExist_AndSwitchToIt(ArrayList<String> priorHandles, Duration timeToWait) {
		try {
			WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, timeToWait.getSeconds());
			wait.ignoring(NullPointerException.class).until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {

					boolean newHandleFound = false;

					List<String> newHandles = new ArrayList<String>();
					newHandles.addAll(Browser.driver.getWindowHandles());
					for (String handle : newHandles) {
						if (!priorHandles.contains(handle)) {
							newHandleFound = true;
							Browser.driver.switchTo().window(handle);
							break;
						}
					}

					return newHandleFound;
				}
			});
		} catch (Exception e) {
			log_AndFail(
					MessageFormat.format("Waited for element to be displayed and encountered error {0}", e.toString()));
		}

		return Browser.driver.getWindowHandle();
	}

	// Click with JavaScript. Work around for some of the tricky elements.
	public static void click_js(@Nullable WebElement buttonToClick) throws Exception {
		if (buttonToClick == null) {
			log_AndFail("------ The element does not exist.");
		} else {
			log_Status("Clicking on WebElement " + buttonToClick.getText());
			String javaScript = "var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initMouseEvent(\"click\",true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
					+ "arguments[0].dispatchEvent(evObj);";

			JavascriptExecutor executor = (JavascriptExecutor) Browser.driver;
			executor.executeScript(javaScript, buttonToClick);
			Thread.sleep(100);
		}
	}

	public static Integer return_indexOf_PartialMatchInArray(String[] arrayToSearch, String valueToFind) {
		for (int i = 0; i < arrayToSearch.length; i++) {
			if (arrayToSearch[i].contains(valueToFind)) {
				return i;
			}

		}
		return null;

	}

	public static Boolean string_isNumeric(String stringToCheck) {
		Boolean isNumber = NumberUtils.isCreatable(stringToCheck);

		String logText = "String " + stringToCheck + " was a number? " + isNumber.toString();
		log_Status(logText);

		return isNumber;

	}

	// Similar to above, but won't give a failure if the object doesn't exist.
	// This is good for objects that may or may not exist depending on data.
	public static void clickIfExists(@Nullable WebElement buttonToClick) {
		if (buttonToClick == null) {
			log_Status("No object exists for click if exists. Attempting to continue anyway.");
		} else {
			log_Status("Clicking on WebElement " + buttonToClick.getText());
			buttonToClick.click();
		}
	}

	// Use this method to inject a text node into an element. Useful for determining
	// if a page refresh has occurred as this injected text should disappear from
	// the element after refresh.
	public static void inject_TextNode_IntoElement(@NotNull WebElement elementToInjectTextInto, String textToInject) {
		Parameter.named("elementToInjectTextInto").withValue(elementToInjectTextInto).mustBeNonNull();

		BaseUI.log_Status("Attempting to inject text into element. Text is " + textToInject);
		JavascriptExecutor js = (JavascriptExecutor) Browser.driver;
		js.executeScript("arguments[0].appendChild(document.createTextNode('" + textToInject + "'))",
				elementToInjectTextInto, textToInject);
	}

	public static void send_KeyWithActionBuilder(String keyToEnter){
		log_Status("Entering " + keyToEnter + " into ActionBuilder.");
		Actions builder = new Actions(Browser.driver);
		builder.sendKeys(keyToEnter);
		builder.build().perform();
	}

	public static void send_KeyWithActionBuilder(Keys key) throws Exception {
		log_Status("Entering " + key.toString() + " into ActionBuilder.");
		Actions builder = new Actions(Browser.driver);
		builder.sendKeys(key);
		builder.build().perform();
	}

	// Generic action to enter text into a field. Typically used with text
	// fields
	public static void enterText(@NotNull WebElement elementToSet, String valueToSet) {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		String logText = "Entering " + valueToSet + " into WebElement ";
		if(!Browser.currentBrowser.equals("winium")) {
			logText  += elementToSet.getTagName();
		}
		log_Status("Entering " + valueToSet + " into WebElement " + elementToSet.getTagName());
		elementToSet.clear();
		// For mobile, add in a half second delay
		if (Browser.typeOfBrowser == "mobile") {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// Don't do anything, just wait for the half second
			}
		}
		elementToSet.click();
		elementToSet.sendKeys(valueToSet);
	}

	// Determines if we're at the top of the page. Useful for "Back to top"
	// links.
	public static void verify_WindowPosition_Is_AtTop() {

		Long value = null;

		if (Browser.currentBrowser.equals("internetexplorer")) {
			// If we run into issues with IE we'll switch to this code.
			JavascriptExecutor executor = (JavascriptExecutor) Browser.driver;
			value = (Long) executor.executeScript("return window.pageYOffset;");
		} else {
			JavascriptExecutor executor = (JavascriptExecutor) Browser.driver;
			value = (Long) executor.executeScript("return window.scrollY;");

		}

		log_Status("Checking to see if we're at top of the page.");

		baseStringCompare("Scroll Position", "0", value.toString());
	}

	// needed this method because click in above method was causing issues due
	// to some of these input boxes having
	// a second element in front of it.
	public static void enterText_IntoInputBox(@NotNull WebElement elementToSet, String valueToSet) {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		if(!Browser.currentBrowser.equals("winium")) {
			log_Status("Entering " + valueToSet + " into WebElement " + elementToSet.getTagName());
		}else{
			log_Status("Entering " + valueToSet + " into WebElement");
		}
		String elementText = elementToSet.getAttribute("value");
		if (elementText != null && !elementText.equals("")) {
			elementToSet.clear();
		}
		// For mobile, add in a half second delay
		if (Browser.typeOfBrowser == "mobile") {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// Don't do anything, just wait for the half second
			}
		}
		elementToSet.sendKeys(valueToSet);
	}


	public static void enterText_SendKeys_WithoutClearing(@NotNull WebElement elementToSet, String textToEnter)
			throws Exception {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		String logText = "Entering " + textToEnter + " into WebElement ";
		if(!Browser.currentBrowser.equals("winium")){
			logText += elementToSet.getTagName();
		}
		log_Status(logText);

		elementToSet.sendKeys(textToEnter);
		Thread.sleep(500);

	}

	public static void enterText_IntoInputBox_DontUseClear(@NotNull WebElement elementToSet, String textToEnter)
			throws Exception {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		String logText = "Entering " + textToEnter + " into WebElement ";
		if(!Browser.currentBrowser.equals("winium")){
			logText += elementToSet.getTagName();
		}
		log_Status(logText);

		Actions select = new Actions(Browser.driver);
		select.doubleClick(elementToSet).build().perform();

		Thread.sleep(200);
		elementToSet.sendKeys(textToEnter);

	}

	// Generic action to enter paste text into a field. Loads String into
	// clipboard and then pastes it into given field.
	public static void pasteText(@NotNull WebElement elementToSet, String valueToSet) throws Exception {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		log_Status("Entering " + valueToSet + " into WebElement " + elementToSet.getTagName());
		elementToSet.clear();
		// For mobile, add in a half second delay
		if (Browser.typeOfBrowser == "mobile") {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// Don't do anything, just wait for the half second
			}
		}

		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable transferable = new StringSelection(valueToSet);
		clipboard.setContents(transferable, null);

		elementToSet.click();
		elementToSet.sendKeys(Keys.chord(Keys.CONTROL, "v"), "");
		Thread.sleep(200);
	}

	// Enter key and other system keys. Allows you to send a specific key.
	// Useful for buttons or elements that have some kind of shortcut key.
	public static void enterKey(@NotNull WebElement elementToSet, Keys key) {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		log_Status("Entering " + key.toString() + " into WebElement " + elementToSet.getTagName());
		// For mobile, add in a half second delay
		if (Browser.typeOfBrowser == "mobile") {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// Don't do anything, just wait for the half second
			}
		}
		elementToSet.sendKeys("");
		// elementToSet.click();
		elementToSet.sendKeys(key);
	}

	public static void enterKey_DontUseClear(@NotNull WebElement elementToSet, Keys key) {
		Parameter.named("elementToSet").withValue(elementToSet).mustBeNonNull();

		log_Status("Entering " + key.toString() + " into WebElement " + elementToSet.getTagName());
		// For mobile, add in a half second delay
		if (Browser.typeOfBrowser == "mobile") {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// Don't do anything, just wait for the half second
			}
		}
		elementToSet.sendKeys(key);
	}

	// Generic action to select a value from a dropdown. Only available for
	// dropdown elements
	public static void selectValueFromDropDown(@NotNull WebElement elementToSelect, String valueToSet) {
		Parameter.named("elementToSelect").withValue(elementToSelect).mustBeNonNull();

		log_Status("Selecting " + valueToSet + " from WebElement " + elementToSelect.getTagName());
		new Select(elementToSelect).selectByVisibleText(valueToSet);
	}

	// Generic action to select a value from a dropdown by index
	public static void selectValueFromDropDownByIndex(@NotNull WebElement elementToSelect, int valueToSet) {
		Parameter.named("elementToSelect").withValue(elementToSelect).mustBeNonNull();

		log_Status("Selecting " + valueToSet + " from WebElement " + elementToSelect.getTagName());
		new Select(elementToSelect).selectByIndex(valueToSet);
	}

	public static void verify_Value_Selected_InDropdown(@NotNull WebElement dropdown, String expectedValue) {
		Parameter.named("dropdown").withValue(dropdown).mustBeNonNull();

		Select dropDown = new Select(dropdown);

		String selectedValue = dropDown.getFirstSelectedOption().getText();

		verify_true_AndLog(selectedValue.equals(expectedValue),
				MessageFormat.format("Dropdown value was correct for {0}", expectedValue),
				MessageFormat.format("Expected dropdown value of {0}, but seeing {1}", expectedValue, selectedValue));

	}

	public static void verifyValueOptionsFromDropDown(@Nullable WebElement elementToFind, String valueToVerify) {
		boolean itemFound = false;
		if (elementToFind == null) {
			log_AndFail("The element does not exist.");

		} else {
			List<WebElement> alloption = new Select(elementToFind).getOptions();

			for (WebElement eachOption : alloption) {
				log_Status("This is --" + eachOption.getText() + "--");
				if (eachOption.getText().contains(valueToVerify)) {
					log_Pass("The  " + valueToVerify + " Option has been found.");
					itemFound = true;
				}
			}
			if (itemFound == false) {
				log_AndFail("The  " + valueToVerify + " Option cannot be found in Element " + elementToFind + ".");
			}
		}
	}

	// Generic action to select a value from a dropdown based on the partial
	// value. This is helpful for times that there are leading or trailing
	// spaces.
	// Only available for dropdown elements
	public static void selectValueFromDropDownByPartialText(@NotNull WebElement elementToSelect, String valueToSet) {
		Parameter.named("elementToSelect").withValue(elementToSelect).mustBeNonNull();

		log_Status("Selecting " + valueToSet + " from WebElement " + elementToSelect.getTagName());
		Select tempSelect = new Select(elementToSelect);
		List<WebElement> allOptions = tempSelect.getOptions();
		for (WebElement eachOption : allOptions) {
			if (eachOption.getText().contains(valueToSet)) {
				String optionSelected = eachOption.getText();
				tempSelect.selectByValue(eachOption.getAttribute("value"));
				log_Status("Selecting the " + optionSelected + " option which matches the value of " + valueToSet);
				break;
			}
		}
	}

	// Generic action to make sure a particular field appears on screen. This is
	// needed because the Locator actions don't check
	// visibility - they check whether an object exists or not.
	public static void verifyElementAppears(@Nullable WebElement elementToFind) {
		if (elementToFind == null) {
			log_AndFail("The elementToFind does not exist.");
		} else {
			BaseUI.verify_true_AndLog(elementToFind.isDisplayed(),
					"The " + elementToFind + " is appearing as expected.",
					"The " + elementToFind + " is not appearing as expected.");
		}

	}

	// Return if true if element appears. Return false if it doesn't
	// visibility - they check whether an object exists or not.
	public static boolean elementAppears(@Nullable WebElement elementToFind) {

		boolean element_appears;

		if (elementToFind == null) {
			log_Status("Element " + elementToFind + " was NOT found.");
			return false;
		}

		try {
			if (elementToFind.isDisplayed()) {
				log_Status("Element " + elementToFind + " was displayed.");
				element_appears = true;
			} else {
				log_Status("Element " + elementToFind + " was NOT displayed.");
				element_appears = false;
			}
		} catch (Exception e) {
			if (elementToFind.isDisplayed()) {
				log_Status("Element " + elementToFind + " was displayed.");
				element_appears = true;
			} else {
				log_Status("Element " + elementToFind + " was NOT displayed.");
				element_appears = false;
			}
		}

		return element_appears;

	}

	// Similar to above but using a locator string instead of a webelement
	public static void verifyElementAppearsByString(String elementToFind) {
		try {
			WebElement elementFound = Locator.lookupRequiredElement(elementToFind);
			verify_true_AndLog(elementFound.isDisplayed(),
					"The element " + elementToFind + " is being displayed. This is expected.",
					"The element " + elementToFind + " is NOT being displayed.");

		} catch (Exception e) {
			log_AndFail("The " + elementToFind + " field cannot be found.");
		}
	}

	// Uses locator string instead of web element.
	public static void verifyElementDoesNotAppearByString(String elementToFind) {
		// try {
		WebElement elementFound = Locator.lookupElement(elementToFind);
		if (elementFound == null) {
			log_Pass("The element is not being displayed. This is expected for the element " + elementToFind);
		} else {
			verify_true_AndLog(!elementFound.isDisplayed(),
					"The element exists but is not being displayed. This is expected for the element " + elementToFind,
					"The " + elementToFind + " is appearing when it is not expected.");
		}
		// } catch (Exception e) {
		// System.out.println(
		// "Pass - the element is not being displayed. This is expected for the
		// element " + elementToFind);
		// Reporter.log(
		// "Pass - the element is not being displayed. This is expected for the
		// element " + elementToFind);
		// }
	}

	// Similar to above, this makes sure that an object is not being displayed.
	public static void verifyElementDoesNotAppear(@Nullable WebElement elementToFind) {
		if (elementToFind == null) {
			log_Pass("The element is not being displayed. This is expected.");
		} else {
			verify_true_AndLog(!elementToFind.isDisplayed(), "The element is not being displayed. This is expected.",
					"The " + elementToFind + " is appearing when it is not expected.");
		}
	}

	// Click on a field and then send a tab key - this can be useful for
	// triggering js errors about required fields
	public static void tabThroughField(String lookupValue) throws InterruptedException {
		log_Status("Tabbing through the " + lookupValue + " field");
		WebElement fieldName = Locator.lookupRequiredElement(lookupValue);
		BaseUI.click(fieldName);
		fieldName.sendKeys(Keys.TAB);
		Thread.sleep(200);
	}

	// This overload takes a WebElement. Instead of clicking on the element, it
	// sends "" keys to the element.
	// This causes the element to have focus and avoids issues where the state
	// of the element changes due to the click.
	public static void tabThroughField(@NotNull WebElement elementToTabThrough) throws Exception {
		Parameter.named("elementToTabThrough").withValue(elementToTabThrough).mustBeNonNull();

		log_Status("Tabbing through element.");

		if ("input".equals(elementToTabThrough.getTagName())) {
			elementToTabThrough.sendKeys("");
		} else {
			new Actions(Browser.driver).moveToElement(elementToTabThrough).perform();
		}
		Thread.sleep(100);

		elementToTabThrough.sendKeys(Keys.TAB);
		Thread.sleep(100);
	}

	// This is an exact match on the field and the expected value. See below for
	// a version that does a partial text match
	public static void verifyElementHasExpectedText(String elementKey, String expectedValue) {
		WebElement elementToVerify = Locator.lookupRequiredElement(elementKey);
		verify_true_AndLog(elementToVerify.getText().equals(expectedValue),
				"The value " + expectedValue + " is being displayed as expected for " + elementKey + ".",
				"The value --" + expectedValue + "-- is not being displayed for " + elementKey
						+ ". The value being displayed is --" + elementToVerify.getText() + "--");
	}

	// Overload takes WebElement instead of String.
	public static void verifyElementHasExpectedText(@NotNull WebElement elementToValidate, String expectedValue) {
		Parameter.named("elementToValidate").withValue(elementToValidate).mustBeNonNull();

		verify_true_AndLog(elementToValidate.getText().equals(expectedValue), "Pass - The Value was as expected",
				MessageFormat.format(
						"FAIL - The value -- {0} -- is not being displayed.  The value being displayed is -- {1} --",
						expectedValue, elementToValidate.getText()));
	}

	@Contract("false, _, _ -> fail")
	public static void verify_true_AndLog(Boolean conditionToTest, String passMessage, String failMessage) {
		if (conditionToTest) {
			log_Pass(passMessage);
		} else {
			log_AndFail(failMessage);
		}
	}

	@Contract("true, _, _ -> fail")
	public static void verify_false_AndLog(Boolean conditionToTest, String passMessage, String failMessage) {
		if (!conditionToTest) {
			log_Pass(passMessage);
		} else {
			log_AndFail(failMessage);
		}
	}

	// Verify the string is not empty or null
	public static void verify_String_Not_EmptyOrNull(String stringToVerify) {
		verify_true_AndLog(!stringEmpty(stringToVerify),
				MessageFormat.format("Value {0} was not empty.", stringToVerify),
				MessageFormat.format("Value {0} WAS empty.", stringToVerify));

	}

	// returns a Boolean as to whether the string is empty or not.
	// Returns true if string is empty, false if it has a value.
	public static boolean stringEmpty(String stringToVerify) {
		boolean stringEmpty = false;
		if (stringToVerify == null || stringToVerify.trim().equals("")) {
			stringEmpty = true;
		}

		return stringEmpty;
	}

	public static Boolean element_isEnabled(@NotNull WebElement elementToVerify) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		Boolean elementEnabled = null;

		if (elementToVerify.isEnabled()) {
			String sysOutput = "The element is enabled.";
			log_Status(sysOutput);

			elementEnabled = true;
		} else {
			String sysOutput = "The element is disabled.";
			log_Status(sysOutput);

			elementEnabled = false;
		}

		return elementEnabled;
	}

	// Verify element is enabled.
	public static void verifyElementEnabled(@NotNull WebElement elementToVerify) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();
		verify_true_AndLog(elementToVerify.isEnabled(), "The element is enabled.", "The element is disabled.");
	}

	// Verify element is disabled.
	public static void verifyElementDisabled(@NotNull WebElement elementToVerify) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();
		verify_true_AndLog(!elementToVerify.isEnabled(), "The Element is disabled.", "The element is enabled.");
	}

	// Verify that the element has the expected value for the passed in
	// attribute.
	public static void verifyElementHasExpectedAttributeValue(@NotNull WebElement elementToVerify, String attribute,
			String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String actualAttributeValue = elementToVerify.getAttribute(attribute);
		verify_true_AndLog(actualAttributeValue.equals(expectedValue),
				"The attribute " + attribute + " is correct for" + elementToVerify + ".",
				"The attribute --" + attribute + "-- is not correct for " + elementToVerify + ". The attribute was --"
						+ actualAttributeValue + "--");
	}

	public static Boolean elementHasPartialExpectedAttribute(@NotNull WebElement elementToVerify, String attribute,
			String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String actualAttributeValue = elementToVerify.getAttribute(attribute);
		String output = "";
		if (actualAttributeValue.contains(expectedValue)) {
			output = MessageFormat.format("Found value {0} in Attribute {1} for the WebElement {2}", expectedValue,
					attribute, elementToVerify);
		} else {
			output = MessageFormat.format("Did NOT find value {0} in Attribute {1} for the WebElement {2}",
					expectedValue, attribute, elementToVerify);
		}

		log_Status(output);

		return actualAttributeValue.contains(expectedValue);
	}

	public static Boolean elementHasExpectedAttribute(@NotNull WebElement elementToVerify, String attribute,
			String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String actualAttributeValue = elementToVerify.getAttribute(attribute);
		String output = "";
		if (actualAttributeValue.equals(expectedValue)) {
			output = MessageFormat.format("Value {0} matched Attribute {1} for the WebElement {2}", expectedValue,
					attribute, elementToVerify);
		} else {
			output = MessageFormat.format("Value {0} did NOT match Attribute {1} for the WebElement {2}", expectedValue,
					attribute, elementToVerify);
		}

		log_Status(output);

		return actualAttributeValue.contains(expectedValue);
	}

	// Verify that the element has part of the expected value for the passed in
	// attribute.
	public static void verifyElementHasExpectedPartialAttributeValue(@NotNull WebElement elementToVerify, String attribute,
			String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String actualAttributeValue = elementToVerify.getAttribute(attribute);

		verify_true_AndLog(actualAttributeValue.contains(expectedValue),
				"The attribute " + attribute + " is correct for " + elementToVerify + ".",
				"The attribute --" + attribute + "-- is not correct for " + elementToVerify + ". The attribute was --"
						+ actualAttributeValue + "--");

	}

	public static void verifyElementHasExpected_CSSAttribute(@NotNull WebElement elementToVerify, String cssAttribute,
			String expected_cssValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String cssACTUALValue = elementToVerify.getCssValue(cssAttribute);

		verify_true_AndLog(expected_cssValue.equals(cssACTUALValue),
				"The CSS attribute " + cssAttribute + " is correct for " + elementToVerify + ".",
				"The CSS attribute --" + cssAttribute + "-- is not correct for " + elementToVerify
						+ ". The attribute was --" + cssACTUALValue + "--");

	}

	public static void verifyElementHasExpected_PartialCSSAttribute(@NotNull WebElement elementToVerify, String cssAttribute,
			String expected_cssValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String cssACTUALValue = elementToVerify.getCssValue(cssAttribute);

		verify_true_AndLog(cssACTUALValue.contains(expected_cssValue),
				"The CSS attribute " + cssAttribute + " is correct for " + elementToVerify + ".",
				"The CSS attribute --" + cssAttribute + "-- is not correct for " + elementToVerify
						+ ". The attribute was --" + cssACTUALValue + "--");

	}

	// Verify the given attribute does NOT contain the passed in text.
	public static void verifyElementDoesNotHavePartialAttributeValue(@NotNull WebElement elementToVerify, String attribute,
			String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		String actualAttributeValue = elementToVerify.getAttribute(attribute);
		verify_true_AndLog(!actualAttributeValue.contains(expectedValue),
				"The attribute " + attribute + " is correct for " + elementToVerify + ".",
				"The attribute --" + attribute + "-- is not correct for " + elementToVerify + ". The attribute was --"
						+ actualAttributeValue + "--");
	}

	// Allows for matching of the text in a field based on a partial string.
	// Given that some fields can have a large amount of data, this is helpful
	// for ensuring that we can check only a part of the value.
	public static void verifyElementHasExpectedPartialText(String elementKey, String expectedValue) {
		WebElement elementToVerify = Locator.lookupRequiredElement(elementKey);
		verify_true_AndLog(elementToVerify.getText().contains(expectedValue),
				"The value " + expectedValue + " is being displayed as expected for " + elementKey + ".",
				"The value --" + expectedValue + "-- is not being displayed for " + elementKey
						+ ". The value being displayed is --" + elementToVerify.getText() + "--");
	}

	public static double round_Double_ToPassedInDecimalPlaces(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);

		double finalDouble = (double) tmp / factor;
		log_Status("Returning value rounded to a set amount of decimal spaces." + String.valueOf(finalDouble));

		return finalDouble;
	}

	public static void verifyElementHasExpectedPartialTextByElement(@NotNull WebElement elementToVerify, String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		verify_true_AndLog(elementToVerify.getText().contains(expectedValue),
				"The value " + expectedValue + " is being displayed as expected for " + elementToVerify.getTagName()
						+ ".",
				"The value --" + expectedValue + "-- is not being displayed for " + elementToVerify.getTagName()
						+ ". The value being displayed is --" + elementToVerify.getText() + "--");
	}

	// Allows the case to be ignored as part of the text match.
	public static void verifyElementHasExpectedPartialTextByElementIgnoreCase(@NotNull WebElement elementToVerify,
			String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		verify_true_AndLog(elementToVerify.getText().toUpperCase().contains(expectedValue.toUpperCase()),
				"The value " + expectedValue + " is being displayed as expected for " + elementToVerify.getTagName()
						+ ".",
				"The value --" + expectedValue + "-- is not being displayed for " + elementToVerify.getTagName()
						+ ". The value being displayed is --" + elementToVerify.getText() + "--");
	}

	// Note this is a contains rather than a full .equals.
	public static void verifyElementDoesNotHaveExpectedText(String elementKey, String expectedValue) {
		WebElement elementToVerify = Locator.lookupRequiredElement(elementKey);
		verify_true_AndLog(!elementToVerify.getText().contains(expectedValue),
				"The value " + expectedValue + " is not being displayed for " + elementKey
						+ ". This is expected behavior and the value displayed is " + elementToVerify.getText(),
				"The value --" + expectedValue + "-- is displayed for " + elementKey
						+ ". This should not be displayed but shows " + elementToVerify.getText());
	}

	// Note this is a contains rather than a full .equals.
	public static void verifyElementDoesNotHaveExpectedText(@NotNull WebElement elementToVerify, String expectedValue) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		verify_true_AndLog(!elementToVerify.getText().contains(expectedValue),
				"The value " + expectedValue + " is not being displayed for " + elementToVerify.toString()
						+ ". This is expected behavior and the value displayed is " + elementToVerify.getText(),
				"The value --" + expectedValue + "-- is displayed for " + elementToVerify.toString()
						+ ". This should not be displayed but shows " + elementToVerify.getText());
	}

	// These were being used for validations
	public static void baseStringCompare(String fieldName, String expectedValue, String actualValue) {
		verify_true_AndLog(expectedValue.equals(actualValue),
				"The value displayed for " + fieldName + " on screen " + actualValue + " matches the expected value of "
						+ expectedValue,
				"The value displayed for " + fieldName + " on screen " + actualValue + " does not match the expected value of " + expectedValue);
	}

	public static void baseStringCompareStringsAreDifferent(String fieldName, String expectedValue,
			String actualValue) {
		verify_true_AndLog(!expectedValue.equals(actualValue),
				"The value displayed for " + fieldName + " on screen " + actualValue
						+ " is different than the expected value of " + expectedValue,
				"The value displayed for " + fieldName + " on screen " + actualValue
						+ " is the same as the expected value of " + expectedValue);

	}

	// Compares two strings and returns either true or false.
	public static void baseStringPartialCompare(String fieldName, String expectedValue, String actualValue) {
		verify_true_AndLog(actualValue.contains(expectedValue),
				"The value displayed for " + fieldName + " on screen " + actualValue
						+ " contains the expected value of " + expectedValue,
				"The value displayed for " + fieldName + " on screen " + actualValue
						+ " does not contain the expected value of " + expectedValue);

	}

	public static void baseStringPartialCompare_IgnoreCase(String fieldName, String expectedValue, String actualValue) {
		boolean valuesMatch = actualValue.toLowerCase().contains(expectedValue.toLowerCase());
		BaseUI.verify_true_AndLog(valuesMatch,
				MessageFormat.format("The value displayed for {0} on screen + {1} contains the expected value of {2}",
						fieldName, actualValue, expectedValue),
				MessageFormat.format(
						"The value displayed for {0} on screen + {1} does not contain the expected value of {2}",
						fieldName, actualValue, expectedValue));
	}

	// Activate and deactivate popup windows allow for the "focus" to shift
	// to/from the popup window.
	// The window identifier for the first window is returned so we can get back
	// to it later as part of the deactivate action
	public static String activatePopupWindow() {
		String originalTab = Browser.driver.getWindowHandle();
		// change focus to new tab, get the URL
		ArrayList<String> newTab = new ArrayList<String>(Browser.driver.getWindowHandles());
		newTab.remove(originalTab);
		Browser.driver.switchTo().window(newTab.get(0));
		log_Status("Activated the popup window");
		return originalTab;
	}

	public static void deactivatePopupWindow(String originalTab) {
		Browser.driver.switchTo().window(originalTab);
		log_Status("Deactivated the popup window");
	}

	// Allows verification of the "value" of a field. This is most commonly used
	// by input type fields.
	// If this does not display correctly, use text/partialtext instead.
	public static void verifyValueOfField(String fieldName, String textToVerify) {
		WebElement tempElement = Locator.lookupRequiredElement(fieldName);
		String valueOnScreen = tempElement.getAttribute("value");
		baseStringCompare(fieldName, textToVerify, valueOnScreen);
	}

	public static void verifyPartialValueOfField(String fieldName, String textToVerify) {
		WebElement tempElement = Locator.lookupRequiredElement(fieldName);
		String valueOnScreen = tempElement.getAttribute("value");
		baseStringPartialCompare(fieldName, textToVerify, valueOnScreen);
	}

	// Actions to check and uncheck a checkbox. Note that if the checkbox is
	// already checked, no action is taken.
	public static void checkCheckbox(String locatorValue) {
		WebElement tempElement = Locator.lookupRequiredElement(locatorValue);
		if (!tempElement.isSelected()) {
			tempElement.click();
			log_Status("Checkbox found via " + locatorValue + " has been checked");
		} else {
			log_Status("Checkbox found via " + locatorValue + " is already checked");
		}
	}

	// Actions to check and uncheck a checkbox. Note that if the checkbox is
	// already checked, no action is taken.
	// Overload takes WebElement instead of String.
	public static void checkCheckbox(@NotNull WebElement checkBox) {
		Parameter.named("checkBox").withValue(checkBox).mustBeNonNull();

		if (!checkBox.isSelected()) {
			checkBox.click();
			log_Status("Checkbox has been checked");
		} else {
			log_Status("Checkbox is already checked");
		}
	}

	public static void uncheckCheckbox(String locatorValue) {
		WebElement tempElement = Locator.lookupRequiredElement(locatorValue);
		if (tempElement.isSelected()) {
			tempElement.click();
			log_Status("Checkbox found via " + locatorValue + " has been unchecked");
		} else {
			log_Status("Checkbox found via " + locatorValue + " is already unchecked");
		}
	}

	// Overload takes a WebElement instead of the Locator String.
	public static void uncheckCheckbox(@NotNull WebElement checkbox) {
		Parameter.named("checkbox").withValue(checkbox).mustBeNonNull();

		if (checkbox.isSelected()) {
			checkbox.click();
			log_Status("Checkbox has been unchecked");
		} else {
			log_Status("Checkbox is already unchecked");
		}
	}

	// Verifies that a checkbox is set to a particular value - use "checked" or
	// "unchecked" rather than true and "" (or in some cases " " or null)
	public static void verifyCheckboxStatus(String checkboxName, String expectedStatus) {
		WebElement tempElement = Locator.lookupRequiredElement(checkboxName);
		String onScreen = tempElement.getAttribute("checked");

		verify_true_AndLog(
				(onScreen.equals("true") && expectedStatus.equals("checked"))
						|| (onScreen.equals("") && expectedStatus.equals("unchecked")),
				"The checkbox " + checkboxName + " has the correct value of " + expectedStatus,
				"The checkbox " + checkboxName + " does not have the correct value of " + expectedStatus
						+ ". The value being displayed is " + onScreen);
	}

	// Overload, takes Webelement instead of string. Uses Bool. True means
	// should be checked. False means should be unchecked.
	// Verifies that a checkbox is set to a particular value - use "checked" or
	// "unchecked" rather than true and "" (or in some cases " " or null)
	public static void verifyCheckboxStatus(@NotNull WebElement checkbox, Boolean expectedStatus) {
		Parameter.named("checkbox").withValue(checkbox).mustBeNonNull();

		Boolean checked = checkbox.isSelected();
		verify_true_AndLog((checked && expectedStatus) || (!checked && !expectedStatus),
				"The checkbox has the correct value of " + expectedStatus,
				"The checkbox does not have the correct value of " + expectedStatus + ". The value being displayed is "
						+ checked);
	}

	// Verifies that an element has focus. (Useful for Tab tests).
	public static void verifyElementHasFocus(@NotNull WebElement elementToVerify) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		Boolean hasFocus = elementHasFocus(elementToVerify);
		verify_true_AndLog(hasFocus, "The element has focus.", "The element does NOT have focus.");
	}

	// Determine whether or not the given element has focus (useful for tab
	// tests);
	public static Boolean elementHasFocus(@NotNull WebElement elementToVerify) {
		Parameter.named("elementToVerify").withValue(elementToVerify).mustBeNonNull();

		Boolean elementHasFocus = elementToVerify.equals(Browser.driver.switchTo().activeElement());
		log_Status("Element has focus? " + elementHasFocus.toString());

		return elementHasFocus;
	}

	public static void verifyCheckboxStatusForWebElement(@NotNull WebElement checkbox, String expectedStatus) {
		Parameter.named("checkbox").withValue(checkbox).mustBeNonNull();

		String checkboxName = checkbox.getText();
		String onScreen = checkbox.getAttribute("checked");
		verify_true_AndLog(
				(onScreen.equals("true") && expectedStatus.equals("checked"))
						|| (onScreen.equals("") && expectedStatus.equals("unchecked")),
				"The checkbox " + checkboxName + " has the correct value of " + expectedStatus,
				"The checkbox " + checkboxName + " does not have the correct value of " + expectedStatus
						+ ". The value being displayed is " + onScreen);
	}

	// Verify the content of the first selected item in a dropdown.
	public static void verifySelectedItemInDropdown(@NotNull WebElement selectElement, String valueSelected) {
		Parameter.named("selectElement").withValue(selectElement).mustBeNonNull();

		String selectedItemOnScreen = new Select(selectElement).getFirstSelectedOption().getText();
		verify_true_AndLog(selectedItemOnScreen.contains(valueSelected),
				"The value in the dropdown was expected to be " + valueSelected + " and " + selectedItemOnScreen
						+ " is being displayed",
				"The value in the dropdown was expected to be " + valueSelected + " and " + selectedItemOnScreen
						+ " is being displayed");
	}

	// Returns the full text from a field. This is helpful if we need to take
	// the value from one field and use it in another.
	public static String getTextFromField(@NotNull WebElement fieldToGet) {
		Parameter.named("fieldToGet").withValue(fieldToGet).mustBeNonNull();

		log_Status("Getting the value of the " + fieldToGet.toString() + " field");
		String textValue = fieldToGet.getText();
		return textValue;
	}

	public static String get_NonVisible_TextFromField(@NotNull WebElement fieldToGet) {
		Parameter.named("fieldToGet").withValue(fieldToGet).mustBeNonNull();

		String textToReturn;

		if (fieldToGet.getTagName().equals("input") && fieldToGet.getAttribute("type").equals("text")) {
			textToReturn = get_Attribute_FromField(fieldToGet, "value");
		} else {
			textToReturn = get_Attribute_FromField(fieldToGet, "innerText");
		}

		log_Status("Returning text '" + textToReturn + "' from field.");

		return textToReturn;
	}

	public static Boolean string_IsInteger(String stringToCheck) {
		Boolean isInteger = stringToCheck.matches("^-?\\d+$");

		BaseUI.log_Status(stringToCheck + " was an integer? " + isInteger.toString());

		return isInteger;
	}

	// Use this for time strings in format "hh:mm a" or slight variations on this
	// such as...
	// 01:00 am, 01:00 AM,
	public static Boolean time_MatchesFormat(String timeStringToCompare) {

		Boolean isTimeFormat = timeStringToCompare.matches("(((0[1-9])|(1[0-2])):([0-5])(0|5)\\s(A|P|a|p)(M|m))");
		// Boolean isTimeFormat =
		// timeStringToCompare.matches("/^(0?[1-9]|1[012])(:[0-5]\\d) [APap][mM]$/");

		BaseUI.log_Status(timeStringToCompare + " matched expected time pattern " + isTimeFormat.toString());

		return isTimeFormat;

	}

	public static boolean time_MatchesFormat(String timeStringToCompare, String formatToCheck) {

		SimpleDateFormat sdf = new SimpleDateFormat(formatToCheck);

		boolean timeWasFormat = false;

		BaseUI.log_Status("Attempting to parse " + timeStringToCompare + " with format " + formatToCheck);

		try {
			Date date1 = sdf.parse(timeStringToCompare);
			timeWasFormat = true;
		} catch (Exception e) {
			BaseUI.log_Status("Parsing exception occurred");
		}

		return timeWasFormat;
	}

	// Check to see if string matches sunday, monday, tuesday, wednesday, thursday,
	// friday, or saturday.
	// Case insensitive. Useful as a weak validation if there are no other better
	// validations that could be performed.
	public static boolean string_IsDayOfWeek(String stringToCheck) {
		boolean string_IsDayOfWeek = false;

		if (stringToCheck.toLowerCase().equals("sunday") || stringToCheck.toLowerCase().equals("monday")
				|| stringToCheck.toLowerCase().equals("tuesday") || stringToCheck.toLowerCase().equals("wednesday")
				|| stringToCheck.toLowerCase().equals("thursday") || stringToCheck.toLowerCase().equals("friday")
				|| stringToCheck.toLowerCase().equals("saturday")) {
			string_IsDayOfWeek = true;
		}

		BaseUI.log_Status(stringToCheck + " was a day of the week? " + String.valueOf(string_IsDayOfWeek));

		return string_IsDayOfWeek;

	}

	public static String get_Attribute_FromField(@NotNull WebElement fieldToGet, String attribute) {
		Parameter.named("fieldToGet").withValue(fieldToGet).mustBeNonNull();

		log_Status("Getting the value of the attribute " + attribute + " from " + fieldToGet.toString() + " field");
		String textValue = fieldToGet.getAttribute(attribute);
		return textValue;
	}

	public static String get_CSSAttribute_FromField(@NotNull WebElement fieldToGet, String attribute) {
		Parameter.named("fieldToGet").withValue(fieldToGet).mustBeNonNull();

		log_Status("Getting the value of the attribute " + attribute + " from " + fieldToGet.toString() + " field");
		String textValue = fieldToGet.getCssValue(attribute);
		return textValue;
	}

	// Example of dateFormat: "MM/dd/yyyy"
	public static void verify_FirstDate_IsBefore_SecondDate(String firstDate, String secondDate, String dateFormat)
			throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date date1 = sdf.parse(firstDate);
		Date date2 = sdf.parse(secondDate);

		BaseUI.verify_true_AndLog(date1.before(date2),
				MessageFormat.format("Date {0} was before Date {1}", firstDate, secondDate),
				MessageFormat.format("Date {0} was NOT before Date {1}", firstDate, secondDate));
	}

	// Example of dateFormat: "MM/dd/yyyy"
	// Is inclusive for beforeDate
	// Note: Simply using a format as "hh:mm a" will not work, need to add the
	// date to the format for comparison. LocalDate is finicky that way.
	public static void verify_Date_IsBetween_DateRange(String firstDate, String secondDate, String actualDate,
			String dateFormat) throws Exception {
		DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
		DateTime beforeDate = DateTime.parse(firstDate, DateTimeFormat.forPattern(dateFormat));
		DateTime afterDate = DateTime.parse(secondDate, DateTimeFormat.forPattern(dateFormat));
		DateTime actual_Date = DateTime.parse(actualDate, DateTimeFormat.forPattern(dateFormat));

		Boolean isBefore = (afterDate.isAfter(actual_Date) || afterDate.equals(actual_Date));
		Boolean isAfter = (beforeDate.isBefore(actual_Date) || actual_Date.equals(beforeDate));

		BaseUI.verify_true_AndLog(isBefore && isAfter,
				MessageFormat.format("Date {0} was between {1} and {2}", actualDate, firstDate, secondDate),
				MessageFormat.format("Date {0} was NOT between {1} and {2}", actualDate, firstDate, secondDate));
	}

	public static void verify_Date_IsNotBetween_DateRange(String firstDate, String secondDate, String actualDate,
													   String dateFormat) throws Exception {
		DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
		DateTime beforeDate = DateTime.parse(firstDate, DateTimeFormat.forPattern(dateFormat));
		DateTime afterDate = DateTime.parse(secondDate, DateTimeFormat.forPattern(dateFormat));
		DateTime actual_Date = DateTime.parse(actualDate, DateTimeFormat.forPattern(dateFormat));

		Boolean isBefore = !(afterDate.isAfter(actual_Date) || afterDate.equals(actual_Date));
		Boolean isAfter = !(beforeDate.isBefore(actual_Date) || actual_Date.equals(beforeDate));

		BaseUI.verify_false_AndLog(isBefore && isAfter,
				MessageFormat.format("Date {0} was NOT between {1} and {2}", actualDate, firstDate, secondDate),
				MessageFormat.format("Date {0} was between {1} and {2}", actualDate, firstDate, secondDate));
	}

	// This intermediary method will check that the actual date is between the range
	// passed in.
	public static void verify_Date_IsAcceptable_DateRange(String expectedDate, String actualDate, Integer minutesRange1,
			Integer minutesRange2, String dateFormat) throws Exception {
		String beforeDate = getDate_WithFormat_X_Minutes_FromInitialString(expectedDate, dateFormat, dateFormat,
				minutesRange1);
		String afterDate = getDate_WithFormat_X_Minutes_FromInitialString(expectedDate, dateFormat, dateFormat,
				minutesRange2);

		verify_Date_IsBetween_DateRange(beforeDate, afterDate, actualDate, dateFormat);
	}

	public static void withImplicitWait(long value, TimeUnit timeUnit, Runnable action) {
		Browser.driver.manage().timeouts().implicitlyWait(value, timeUnit);
		try {
			action.run();
		} finally {
			Browser.driver.manage().timeouts().implicitlyWait(Browser.defaultImplicitWaitTime, TimeUnit.SECONDS);
		}
	}

	public static Boolean elementExists(final String elementLocator, final String variable1, final String variable2) {
		ArrayList<WebElement> elementList = new ArrayList<>();
		if(!Browser.currentBrowser.equals("winium")) {
			withImplicitWait(2, TimeUnit.SECONDS,
					() -> elementList.addAll(Locator.lookup_multipleElements(elementLocator, variable1, variable2)));
		}else{
			elementList.addAll(Locator.lookup_multipleElements(elementLocator, variable1, variable2));
		}
		Boolean elementFound = elementList.size() > 0;

		String message = MessageFormat.format(
				"Element found = {3}: Locator of {0}, variable1 = {1}, variable 2 = {2}; ",
				elementLocator,
				variable1 == null ? "empty" : variable1,
				variable2 == null ? "empty" : variable2,
				elementFound.toString());
		log_Status(message);

		return elementFound;

	}

	// Wait 3 times for element to not exist.
	public static void wait_forElementToNotExist(String elementLocator, String variable1, String variable2)
			throws Exception {
		ArrayList<WebElement> elementList = new ArrayList<WebElement>();
		log_Status("Waiting for element " + elementLocator + " to not exist");
		elementList.addAll(Locator.lookup_multipleElements(elementLocator, variable1, variable2));

		for (int i = 0; i < 4; i++) {
			if (!elementExists(elementLocator, null, null)) {
				break;
			}
			Thread.sleep(1000);
		}
		Thread.sleep(200);
	}

	public static void wait_forPageToFinishLoading() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Browser.driver, 60);
		log_Status("Waiting for page to finish loading.");
		Thread.sleep(1000);
		{
			ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
							.equals("complete");
				}
			};

			try {
				wait.until(expectation);
			} catch (Exception e) {
				log_Status("Encountered error waiting for page to finish loading.");
			}
		}
	}

	// Returns the full text from an Input box. Used to get text currently in an
	// Input box for validation.
	public static String getTextFromInputBox(@NotNull WebElement fieldToGet) {
		Parameter.named("fieldToGet").withValue(fieldToGet).mustBeNonNull();

		String textValue = fieldToGet.getAttribute("value");
		return textValue;
	}

	// Similar
	public static void currentURLContainsExpectedURL(String expectedURL) {
		String newPageURL = Browser.driver.getCurrentUrl();
		// Compare URLs
		BaseUI.verify_true_AndLog(newPageURL.toLowerCase().contains(expectedURL.toLowerCase()),
				"The current URL " + newPageURL + " contains the expected value of " + expectedURL,
				"The current URL " + newPageURL + " does not contain the expected value of  " + expectedURL);
	}

	public static void clickMobifyPreviewButton() {
		WebElement tempElement = Locator.findRequiredElement(By.xpath("//button[@id='authorize']"));
		tempElement.click();
	}

	// public static void verifyImageAppearsInPopup(String imageToVerify) {
	// String windowID = BaseUI.activatePopupWindow();
	// Navigation.verifyAnyImageByImageSource("Deluxe_eChecks.png");
	// BaseUI.deactivatePopupWindow(windowID);
	// }

	public static void wait_ForCondition_ToBeMet(Callable<Boolean> conditionToMeet, Integer timeToWaitInSeconds) {

		BaseUI.log_Status("Wait for condition to be met.");
		Awaitility.await().atMost(timeToWaitInSeconds, TimeUnit.SECONDS).until(conditionToMeet);
	}

	// Overload takes a time to wait in milliseconds in between polling.
	public static void wait_ForCondition_ToBeMet(Callable<Boolean> conditionToMeet, Integer timeToWaitInSeconds,
			Integer pollingIntervalInMilliseconds) {

		BaseUI.log_Status("Wait for condition to be met.");
		Awaitility.await().pollInterval(pollingIntervalInMilliseconds, TimeUnit.MILLISECONDS)
				.atMost(timeToWaitInSeconds, TimeUnit.SECONDS).until(conditionToMeet);
	}

	// This overload accepts a Duration so the user can set whatever time unit they
	// want
	// Syntax to create a duration: Duration newDuration = Duration.of(1000,
	// ChronoUnit.MILLIS); or Duration.ofSeconds(25)
	public static void wait_ForCondition_ToBeMet(Callable<Boolean> conditionToMeet, Duration timeToWait,
			Duration pollingInterval) throws Exception {

		BaseUI.log_Status("Wait for condition to be met.");
		Awaitility.await().pollInterval(pollingInterval.toMillis(), TimeUnit.MILLISECONDS)
				.atMost(timeToWait.toMillis(), TimeUnit.MILLISECONDS).until(conditionToMeet);
	}

	//
	// public static void wait_ForCondition_ToBeMet(Predicate lambdaExpression,
	// Integer timeToWaitInSeconds) {
	//
	// BaseUI.log_Status("Wait for condition to be met.");
	// Awaitility.await().atMost(timeToWaitInSeconds,
	// TimeUnit.SECONDS).until(lambdaExpression);
	//
	// }

	public static WebElement waitForElementToBeClickable(String elementLocator, String variable1, String variable2) {
		return waitForElementToBeClickable(elementLocator, variable1, variable2, 25);
	}

	public static WebElement waitForElementToBeClickable(String elementLocator, String variable1, String variable2,
			int timeOutInSeconds) {
		log_Status("Waiting for element " + elementLocator + " to be clickable.");

		WebElement elementToReturn = null;
		try {
			elementToReturn = (new WebDriverWait(Browser.driver, timeOutInSeconds)).ignoring(NullPointerException.class)
					.until(ExpectedConditions
							.elementToBeClickable(Locator.lookupBy(elementLocator, variable1, variable2)));
		} catch (Exception e) {
			log_AndFail(
					MessageFormat.format("Waited for element to be clickable and encountered error {0}", e.toString()));
		}
		return elementToReturn;
	}

	public static WebElement waitForElementToBeDisplayed(String elementLocator, String variable1, String variable2) {
		return waitForElementToBeDisplayed(elementLocator, variable1, variable2, Duration.ofSeconds(25));
	}

	public static WebElement waitForElementToBeDisplayed(String elementLocator, String variable1, String variable2,
			Integer secondsToWait) {
		return waitForElementToBeDisplayed(elementLocator, variable1, variable2,
				Duration.of(secondsToWait, ChronoUnit.SECONDS));
	}

	public static WebElement waitForElementToBeDisplayed(String elementLocator, String variable1, String variable2,
			Duration timeToWait) {
		WebElement elementToReturn = null;

		log_Status("Waiting for element " + elementLocator + " to be displayed.");

		try {
				elementToReturn = (new WebDriverWait(Browser.driver, timeToWait.get(ChronoUnit.SECONDS)))
						.ignoring(NullPointerException.class).until(ExpectedConditions
								.visibilityOfElementLocated(Locator.lookupBy(elementLocator, variable1, variable2)));
		} catch (Exception e) {
			log_AndFail(
					MessageFormat.format("Waited for element to be displayed and encountered error {0}", e.toString()));
		}
		return elementToReturn;
	}

	public static WebElement waitForElementToBeDisplayed_ButDontFail(String elementLocator, String variable1,
			String variable2, Integer secondsToWait) {
		WebElement elementToReturn = null;

		log_Status("Waiting for element " + elementLocator + " to be displayed.");

		try {
			elementToReturn = (new WebDriverWait(Browser.driver, secondsToWait)).ignoring(NullPointerException.class)
					.until(ExpectedConditions
							.visibilityOfElementLocated(Locator.lookupBy(elementLocator, variable1, variable2)));
		} catch (Exception e) {
			log_Warning("Element didn't appear withing the given time span, attempting to continue.");
		}
		return elementToReturn;
	}

	// This overload waits a default of 10 seconds.
	public static void waitForElementToNOTBeDisplayed(String elementLocator, String variable1, String variable2) {
		waitForElementToNOTBeDisplayed(elementLocator, variable1, variable2, 10);

		// log_Status("Waiting for element " + elementLocator + " to NOT be
		// displayed.");

		// try {
		// // Predicate<WebDriver> myPredicate = x -> !x
		// // .findElement(Locator.lookupBy(elementLocator, variable1,
		// // variable2)).isDisplayed();
		//
		// WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, 10)
		// .ignoring(NullPointerException.class);
		// wait.until(new Function<WebDriver, Boolean>() {
		// public Boolean apply(WebDriver driver) {
		// return driver.findElement(Locator.lookupBy(elementLocator, variable1,
		// variable2)).isDisplayed();
		// }
		// });
		// } catch (Exception e) {
		// log_AndFail(MessageFormat.format("Waited for element to NOT be displayed and
		// encountered error {0}",
		// e.toString()));
		// }
	}

	// Overload takes a time to wait. Uses Expected Condition instead of
	// Predicate (Lambda expression).
	public static void waitForElementToNOTBeDisplayed(String elementLocator, String variable1, String variable2,
			Integer timeToWaitInSeconds) {
		log_Status(MessageFormat.format("Waiting for element with locator {0} to not Appear", elementLocator));

		try {
			WebDriverWait wait = new WebDriverWait(Browser.driver, timeToWaitInSeconds);

			wait.ignoring(NullPointerException.class).until(ExpectedConditions
					.invisibilityOfElementLocated(Locator.lookupBy(elementLocator, variable1, variable2)));
		} catch (Exception e) {
			log_AndFail(MessageFormat.format("Waited for element to NOT be displayed and encountered error {0}",
					e.toString()));
		}
	}

	// overload defaults to 10 seconds.
	public static void waitForElementToContain_PartialAttributeMatch(String elementLocator, String variable1,
			String variable2, String attribute, String textToFind) {
		waitForElementToContain_PartialAttributeMatch(elementLocator, variable1, variable2, attribute, textToFind, 10);
	}

	// overload allows you to pass in time to wait.
	public static void waitForElementToContain_PartialAttributeMatch(String elementLocator, String variable1,
			String variable2, String attribute, String textToFind, Integer secondsToWait) {

		log_Status("Waiting for element " + elementLocator + " to be contain expected attribute " + attribute
				+ " to contain value " + textToFind);

		try {
			// Predicate<WebDriver> myPredicate = x -> x
			// .findElement(Locator.lookupBy(elementLocator, variable1,
			// variable2)).getAttribute(attribute)
			// .contains(textToFind);

			WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, secondsToWait);
			wait.ignoring(NullPointerException.class).ignoring(StaleElementReferenceException.class)
					.until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver driver) {
							return driver.findElement(Locator.lookupBy(elementLocator, variable1, variable2))
									.getAttribute(attribute).contains(textToFind);
						}
					});

		} catch (TimeoutException e) {

			log_AndFail(MessageFormat.format("Timed out waiting for an element to appear. Waited for an element "
					+ elementLocator + "  to contain expected attribute " + attribute + " to contain value "
					+ textToFind + "\nError: {0}", e.toString()));
		}
	}

	// Use this method to wait for element to contain a match the the attribute.
	public static void waitForElementToContain_AttributeMatch(String elementLocator, String variable1, String variable2,
			String attribute, String textToFind, Integer secondsToWait) {

		log_Status("Waiting for element " + elementLocator + " to be contain expected attribute " + attribute
				+ " to contain value " + textToFind);

		try {
			WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, secondsToWait);
			wait.ignoring(NullPointerException.class).ignoring(StaleElementReferenceException.class).until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {
					return driver.findElement(Locator.lookupBy(elementLocator, variable1, variable2))
							.getAttribute(attribute).equals(textToFind);
				}
			});

		} catch (TimeoutException e) {

			log_AndFail(MessageFormat.format("Timed out waiting for an element to appear. Waited for an element "
					+ elementLocator + "  to contain expected attribute " + attribute + " to contain value "
					+ textToFind + "\nError: {0}", e.toString()));
		}
	}

	public static void waitForAlertToAppear(Integer secondsToWait) {

		log_Status("Waiting for alert to be present.");

		wait_ForCondition_ToBeMet(new Callable<Boolean>() {
			public Boolean call() throws Exception {
				try {
					Browser.driver.switchTo().alert();
					return true;
				} // try
				catch (NoAlertPresentException Ex) {
					return false;
				} catch (org.openqa.selenium.TimeoutException Ex) {
					return false;
				}
			}
		}, secondsToWait, 1000);
	}

	// Use this method to wait for element to NOT contain a partial attribute match.
	public static void waitForElementToNOTContain_PartialAttributeMatch(String elementLocator, String variable1,
			String variable2, String attribute, String textToFind, Integer secondsToWait) {

		log_Status("Waiting for element " + elementLocator + " to NOT contain expected attribute " + attribute
				+ " with value " + textToFind);

		try {
			WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, secondsToWait);
			wait.ignoring(NullPointerException.class).until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {
					return !driver.findElement(Locator.lookupBy(elementLocator, variable1, variable2))
							.getAttribute(attribute).contains(textToFind);
				}
			});
		} catch (Exception e) {
			log_AndFail(
					MessageFormat.format("Waited for element to be displayed and encountered error {0}", e.toString()));
		}
	}

	// Use this method to wait for element to contain a match the the attribute.
	public static void waitForPageSourceToNotContainValue(String textToFind, Integer secondsToWait) {

		log_Status("Waiting for page source to not contain " + textToFind);

		try {

			WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, secondsToWait);
			wait.ignoring(NullPointerException.class).until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {
					return driver.getPageSource().contains(textToFind);
				}
			});
		} catch (Exception e) {
			log_AndFail(MessageFormat.format(
					"Waited for page source to no longer contain text {0} and encountered error: {1}", textToFind,
					e.getMessage()));
		}
	}

	//useful for dropping files into an choose file control.
	public static void DropFile(File filePath, @NotNull WebElement target, int offsetX, int offsetY) {
		Parameter.named("target").withValue(target).mustBeNonNull();
		if(!filePath.exists())
			throw new WebDriverException("File not found: " + filePath.toString());

		JavascriptExecutor jse = (JavascriptExecutor)Browser.driver;
		WebDriverWait wait = new WebDriverWait(Browser.driver, 30);

		String JS_DROP_FILE =
				"var target = arguments[0]," +
						"    offsetX = arguments[1]," +
						"    offsetY = arguments[2]," +
						"    document = target.ownerDocument || document," +
						"    window = document.defaultView || window;" +
						"" +
						"var input = document.createElement('INPUT');" +
						"input.type = 'file';" +
						"input.style.display = 'none';" +
						"input.onchange = function () {" +
						"  var rect = target.getBoundingClientRect()," +
						"      x = rect.left + (offsetX || (rect.width >> 1))," +
						"      y = rect.top + (offsetY || (rect.height >> 1))," +
						"      dataTransfer = { files: this.files };" +
						"" +
						"  ['dragenter', 'dragover', 'drop'].forEach(function (name) {" +
						"    var evt = document.createEvent('MouseEvent');" +
						"    evt.initMouseEvent(name, !0, !0, window, 0, 0, 0, x, y, !1, !1, !1, !1, 0, null);" +
						"    evt.dataTransfer = dataTransfer;" +
						"    target.dispatchEvent(evt);" +
						"  });" +
						"" +
						"  setTimeout(function () { document.body.removeChild(input); }, 25);" +
						"};" +
						"document.body.appendChild(input);" +
						"return input;";

		WebElement input =  (WebElement)jse.executeScript(JS_DROP_FILE, target, offsetX, offsetY);
		input.sendKeys(filePath.getAbsoluteFile().toString());
		wait.until(ExpectedConditions.stalenessOf(input));
	}

	public static void wait_ForElement_ToContainText(String elementLocator, Integer timeToWaitInSeconds,
			String textToBePresent) {

		log_Status(MessageFormat.format("Waiting for element with locator {0} to contain text {1}", elementLocator,
				textToBePresent));

		try {
			WebDriverWait wait = new WebDriverWait(Browser.driver, timeToWaitInSeconds);
			wait.until(ExpectedConditions.textToBePresentInElementLocated(Locator.lookupBy(elementLocator, null, null),
					textToBePresent));
		} catch (Exception e) {
			log_AndFail(MessageFormat.format("Waited for element to contain text {0} and encountered error {1}.",
					textToBePresent, e.toString()));
		}
	}

	/**
	 * <p>Waits for the element to be present on the page, and to match the given Hamcrest matcher.
	 * (The matcher will probably be something from the ElementHas class.)</p>
	 * <p>The locator string will be included in error messages; use a different overload if you want
	 * to supply a human-readable description for the element.</p>
	 * @param elementLocator Locator for the object repository
	 * @param timeout How long to wait for the condition to be met
	 * @param condition Hamcrest matcher representing the condition to wait for.
	 *                  This will probably be something from the ElementHas class
	 */
	public static void waitForElement(String elementLocator, Duration timeout, Matcher<WebElement> condition) {
		waitForElement(elementLocator, elementLocator, timeout, condition);
	}

	/**
	 * Waits for the element to be present on the page, and to match the given Hamcrest matcher.
	 * (The matcher will probably be something from the ElementHas class.)
	 * @param elementDescription Human-readable description for the element, to appear in error messages
	 * @param elementLocator Locator for the object repository
	 * @param timeout How long to wait for the condition to be met
	 * @param condition Hamcrest matcher representing the condition to wait for.
	 *                  This will probably be something from the ElementHas class
	 */
	public static void waitForElement(String elementDescription, String elementLocator,
									  Duration timeout, Matcher<WebElement> condition) {
		waitForElement(elementDescription, () -> Locator.lookupRequiredElement(elementLocator), timeout, condition);
	}

	/**
	 * Waits for the WebElement to match the given Hamcrest matcher.
	 * (The matcher will probably be something from the ElementHas class.)
	 * @param elementDescription Human-readable description for the element, to appear in error messages
	 * @param element The WebElement
	 * @param timeout How long to wait for the condition to be met
	 * @param condition Hamcrest matcher representing the condition to wait for.
	 *                  This will probably be something from the ElementHas class
	 */
	public static void waitForElement(String elementDescription, @NotNull WebElement element,
									  Duration timeout, Matcher<WebElement> condition) {
		Parameter.named("element").withValue(element).mustBeNonNull();

		waitForElement(elementDescription, () -> element, timeout, condition);
	}

	/**
	 * Waits for the given Supplier to successfully return a WebElement that
	 * matches the given Hamcrest matcher. (The matcher will probably be something
	 * from the ElementHas class.)
	 * @param elementDescription Human-readable description for the element, to appear in error messages
	 * @param getElement A Supplier to be called (repeatedly) to attempt to get an element.
	 *                   This should not return null. Some exceptions (e.g. NoSuchElementException)
	 *                   are ignored and treated as "keep trying".
	 * @param timeout How long to wait for the condition to be met
	 * @param condition Hamcrest matcher representing the condition to wait for.
	 *                  This will probably be something from the ElementHas class
	 */
	private static void waitForElement(String elementDescription, Supplier<WebElement> getElement,
									   Duration timeout, Matcher<WebElement> condition) {
		Description description = new StringDescription();
		condition.describeTo(description);
		log_Status("Waiting for element '" + elementDescription + "' to be " + description);

		try {
			WebDriverWait wait = new WebDriverWait(Browser.driver, timeout.getSeconds());
			// Call getElement.get() inside the lambda, so it's repeated on each retry
			wait.until(driver -> condition.matches(getElement.get()));
		} catch (TimeoutException e) {
			// Run the comparison again and throw an informative failure message. This will evaluate
			// the matcher one more time; if it actually passes this time, then... well, we pass!
			String reason = "Timeout expired (" + durationToString(timeout) + ")\nElement: " + elementDescription;
			// Call Hamcrest's assertThat rather than BaseUI's - we don't need to log on success.
			org.hamcrest.MatcherAssert.assertThat(reason, getElement.get(), condition);
		}
	}

	/// Mouse Hover using JavaScript for those finnicky elements
	/// Dominic Giallombardo 3/11/2016
	public static void elementHover_js(@NotNull WebElement elementToHover) throws Exception {
		Parameter.named("elementToHover").withValue(elementToHover).mustBeNonNull();

		String javaScript = "var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
				+ "arguments[0].dispatchEvent(evObj);";

		JavascriptExecutor executor = (JavascriptExecutor) Browser.driver;
		executor.executeScript(javaScript, elementToHover);
		Thread.sleep(200);
	}

	public static void elementHover(@NotNull WebElement elementToHover) throws Exception {
		Parameter.named("elementToHover").withValue(elementToHover).mustBeNonNull();

		Actions builder = new Actions(Browser.driver);

		try {
			builder.moveToElement(elementToHover).perform();
			log_Pass("Able to hover over element.");
		} catch (Exception e) {
			log_AndFail(MessageFormat.format("Tried to Hover over element and encountered error: {0}", e.toString()));
		}
		Thread.sleep(100);
	}

	public static void element_ClickAndHold(@NotNull WebElement elementToHover) throws Exception {
		Parameter.named("elementToHover").withValue(elementToHover).mustBeNonNull();

		Actions builder = new Actions(Browser.driver);

		// Browser.driver.switchTo().window(Browser.driver.getWindowHandle());
		// Thread.sleep(200);
		try {
			builder.moveToElement(elementToHover).clickAndHold(elementToHover).build().perform();
			log_Pass("Able to Click And Hold the element.");
		} catch (Exception e) {
			log_AndFail(MessageFormat.format("Tried to Click And Hold the element and encountered error: {0}",
					e.toString()));
		}
		Thread.sleep(100);
	}

	public static void verifyElementExists(String elementIdentifier, String elementVariable1, String elementVariable2) {
		BaseUI.verify_true_AndLog(BaseUI.elementExists(elementIdentifier, elementVariable1, elementVariable2),
				MessageFormat.format("Element {0} exists.", elementIdentifier),
				MessageFormat.format("Element {0} does NOT exist.", elementIdentifier));

	}

	public static void verifyElementDoesNOTExist(String elementIdentifier, String elementVariable1,
			String elementVariable2) {
		BaseUI.verify_true_AndLog(!BaseUI.elementExists(elementIdentifier, elementVariable1, elementVariable2),
				MessageFormat.format("Element {0} does NOT exist.", elementIdentifier),
				MessageFormat.format("Element {0} exists.", elementIdentifier));

	}

	// Close any extra windows. Need to pass in the primary window handle for
	// the window you'd like to keep open.
	public static void close_ExtraWindows(String primaryWindowHandle) throws Exception {
		// Checks for extra windows and closes any that are not the main window.
		if (Browser.driver.getWindowHandles().size() > 1) {
			for (String s : Browser.driver.getWindowHandles()) {
				switch_ToWindow(s);
				if (!s.equals(primaryWindowHandle)) {
					Browser.driver.close();
				}
			}
			// Switch back to primary window handle after we've closed other
			// windows.
			switch_ToWindow(primaryWindowHandle);
			Thread.sleep(100);
		}
	}

	public static String random_NumberAsString(int startingInt, int endingInt) {
		Integer randomNumber = ThreadLocalRandom.current().nextInt(startingInt, endingInt + 1);
		log_Status(MessageFormat.format("Random number: {0} was generated.", randomNumber));

		return randomNumber.toString();
	}

	// Pass in the window handle to switch to.
	public static void switch_ToWindow(String windowHandle) throws InterruptedException {
		try {
			Browser.driver.switchTo().window(windowHandle);
			log_Pass(MessageFormat.format("Successfully switched to window {0}", windowHandle));
		} catch (Exception e) {
			log_AndFail(MessageFormat.format("Unable to switch to window {0}", windowHandle) + "/n"
					+ MessageFormat.format("Encountered error: {0}", e.toString()));
		}
		Thread.sleep(100);
	}

	// Dominic - 2/16/2016
	// JavaScript Overload.
	// works regardless of current window count.
	// This overload accepts a bool (either true or false). Causes it to use
	// JavaScript instead of standard click.
	public static String ClickAndSwitchWindow(@NotNull WebElement elementToBeClicked, boolean JavaClick, int timer)
			throws InterruptedException {
		Parameter.named("elementToBeClicked").withValue(elementToBeClicked).mustBeNonNull();

		return ClickAndSwitchWindow(elementToBeClicked, JavaClick, () -> {
			try {
				Thread.sleep(timer);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
	}

	public static String ClickAndSwitchWindow(@NotNull WebElement elementToBeClicked, boolean JavaClick, Runnable delay)
			throws InterruptedException {
		Parameter.named("elementToBeClicked").withValue(elementToBeClicked).mustBeNonNull();

		String handleToReturn = null;
		List<String> previousHandles = new ArrayList<String>();
		List<String> currentHandles = new ArrayList<String>();
		previousHandles.addAll(Browser.driver.getWindowHandles());
		if (!JavaClick) {
			elementToBeClicked.click();
		} else {
			JavascriptExecutor executor = (JavascriptExecutor) Browser.driver;
			executor.executeScript("arguments[0].click();", elementToBeClicked);
		}
		delay.run();
		for (int i = 0; i < 20; i++) {
			currentHandles.clear();
			try {
				currentHandles.addAll(Browser.driver.getWindowHandles());
			} catch (UnhandledAlertException e) {
				BaseUI.closeAlertIfPresent();
				Thread.sleep(100);
			}
			for (String s : previousHandles) {
				if (currentHandles.contains(s)) {
					currentHandles.remove(s);
				}
			}
			if (currentHandles.size() == 1) {
				Browser.driver.switchTo().window(currentHandles.get(0));
				Thread.sleep(100);
				handleToReturn = currentHandles.get(0);
				break;

			} else {
				Thread.sleep(500);
			}
		}
		verify_true_AndLog(handleToReturn != null, MessageFormat.format("Switched to new Window {0}.", handleToReturn),
				"Unable to find new window.");
		return handleToReturn;
	}

	public static String switchToAlert_GetText_AndDismiss() {
		String alertText = "";
		try {
			WebDriverWait wait = new WebDriverWait(Browser.driver, 2);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = Browser.driver.switchTo().alert();
			alertText = alert.getText();
			alert.dismiss();
		} catch (Exception e) {
			// exception handling
		}
		verify_true_AndLog(!alertText.equals(""),
				MessageFormat.format("Switched to new Alert and returned text: {0}", alertText) + "/n"
						+ "Alert was dismissed.",
				"Unable switch to Alert and return the text.");

		return alertText;

	}

	public static void verify_AlertPresent_AndDismiss() {
		try {
			WebDriverWait wait = new WebDriverWait(Browser.driver, 2);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = Browser.driver.switchTo().alert();
			alert.dismiss();
			BaseUI.log_Pass("Found Alert.");
		} catch (Exception e) {
			BaseUI.log_AndFail("Did NOT find alert.");
		}
	}

	public static String switchToAlert_GetText_AndAccept() {
		String alertText = "";
		try {
			WebDriverWait wait = new WebDriverWait(Browser.driver, 2);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = Browser.driver.switchTo().alert();
			alertText = alert.getText();
			alert.accept();
		} catch (Exception e) {
			// exception handling
		}
		verify_true_AndLog(!alertText.equals(""),
				MessageFormat.format("Switched to new Alert and returned text: {0}", alertText) + "/n"
						+ "Alert was dismissed.",
				"Unable switch to Alert and return the text.");

		return alertText;

	}

	public static void closeAlertIfPresent() {
		WebDriverWait wait = new WebDriverWait(Browser.driver, 1 /* timeout in seconds */);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = Browser.driver.switchTo().alert();
			alert.dismiss();
			log_Status("Switched to and dismissed Alert.");
		} catch (Exception e) {
			log_Status("No alert was present.");
		}
	}

	// Returns an ArrayList containing HashMaps.
	// Each entry in the ArrayList represents a row in the table
	// Each entry in the HashMap represents a cell in that specific table. The
	// Key will be the name of the field. The Value will be the extracted value.

	// To use this method, you must pass in the Element Locator that returns a
	// list of the rows in the table.
	// You must also pass in a HashMap with each field name and the xpath for
	// that field name.
	// For Example, one HashMap entry might be the "Credit Card Number" and the
	// xpath to find this will be ".//*[@id='cc_number']".
	// Each HashMap will contain a copy of all of the fields that you passed in
	// and all of the extracted values that it pulled from the table on the
	// website.
	// It will find the individual fields off of the current row.
	public static ArrayList<HashMap<String, String>> tableExtractor(String rowElementLocator,
			HashMap<String, String> field_And_XPath) {
		ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();
		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(Locator.lookup_multipleElements(rowElementLocator, null, null));

		int rowCount = 0;
		for (WebElement row : rowList) {
			HashMap<String, String> rowData_Field_And_Text = new HashMap<String, String>();

			for (String field : field_And_XPath.keySet()) {
				WebElement row_cell = row.findElement(By.xpath(field_And_XPath.get(field)));
				String cellText = "";
				// if it's an input text box, get the value
				if ((row_cell.getTagName().equals("input") && row_cell.getAttribute("type").equals("text"))
						|| (row_cell.getTagName().equals("input") && row_cell.getAttribute("type").equals("tel"))) {
					cellText = row_cell.getAttribute("value");
				}
				// else just get the text.
				else {
					cellText = row_cell.getText();
				}

				rowData_Field_And_Text.put(field, cellText);

				log_Status(MessageFormat.format("Row - {0}: Added field {1} with value of {2}", rowCount, field,
						cellText));
			}
			table_Values.add(rowData_Field_And_Text);
			rowCount++;
		}

		return table_Values;
	}

	// Will only return the first Integer number of rows.

	public static ArrayList<HashMap<String, String>> tableExtractor(Integer numberOfRows_ToGrab,
			String rowElementLocator, HashMap<String, String> field_And_XPath) {
		ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();
		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(Locator.lookup_multipleElements(rowElementLocator, null, null));

		int rowCount = 0;
		for (WebElement row : rowList) {
			if (rowCount == numberOfRows_ToGrab) {
				break;
			}

			HashMap<String, String> rowData_Field_And_Text = new HashMap<String, String>();

			for (String field : field_And_XPath.keySet()) {
				WebElement row_cell = row.findElement(By.xpath(field_And_XPath.get(field)));
				String cellText = "";
				// if it's an input text box, get the value
				if ((row_cell.getTagName().equals("input") && row_cell.getAttribute("type").equals("text"))
						|| (row_cell.getTagName().equals("input") && row_cell.getAttribute("type").equals("tel"))) {
					cellText = row_cell.getAttribute("value");
				}
				// else just get the text.
				else {
					cellText = row_cell.getText();
				}

				rowData_Field_And_Text.put(field, cellText);

				log_Status(MessageFormat.format("Row - {0}: Added field {1} with value of {2}", rowCount, field,
						cellText));
			}
			table_Values.add(rowData_Field_And_Text);
			rowCount++;
		}

		return table_Values;
	}

	// Currently only works in Chrome. IE doesn't pull innerText, so I would
	// need to figure out a work around, but it looks like it might not be
	// possible to run it in IE
	public static TableData tableExtractorV2(String elementIdentifier, String[] headers) {

		return tableExtractorV2(elementIdentifier, null, null, headers);
	}

	// Currently only works in Chrome. IE doesn't pull innerText, so I would
	// need to figure out a work around, but it looks like it might not be
	// possible to run it in IE
	public static TableData tableExtractorV2(String elementIdentifier, String elementVar1, String elementVar2,
			String[] headers) {
		TableData newTable = new TableData();
		String tableSource = "";
		if (Browser.currentBrowser.equals("internetexplorer")) {
			log_AndFail(
					"Unable to use tableExtractorV2 in Internet Explorer due to the way that table text is extracted.  Please use a different method.");
		} else {
			tableSource = BaseUI.get_Attribute_FromField(
					Locator.lookupRequiredElement(elementIdentifier, elementVar1, elementVar2), "innerText");
		}

		if (tableSource == null || tableSource.equals("")) {
			log_AndFail("Unable to find the table text.");
		}

		String delimiter = "\n";

		// Assumes cells are split up by \n
		if (!tableSource.contains("\t")) {
			String[] tableCells = tableSource.split(delimiter);

			int cellCount = tableCells.length;
			int headerCount = headers.length;
			int listSize = cellCount / headerCount;
			// add Hashmaps to our list for each of the rows
			for (int i = 0; i < listSize; i++) {
				newTable.data.add(new HashMap<String, String>());
			}

			int dataIndex = 0;
			for (int i = 0; i < cellCount;) {
				if (dataIndex >= newTable.data.size()) {
					break;
				} else {

				}

				for (int j = 0; j < headerCount; j++) {
					newTable.data.get(dataIndex).put(headers[j], tableCells[i]);

					i++;
				}
				dataIndex++;
			}
			// cells are split up by /t/t/t/t and then /n for line.
		} else {
			String[] tableRows = tableSource.split(delimiter);
			for (String row : tableRows) {
				String[] cells = row.split("\t", -1);

				HashMap<String, String> newRow = new HashMap<String, String>();
				for (int i = 0; i < headers.length; i++) {
					newRow.put(headers[i], cells[i]);
				}
				newTable.data.add(newRow);
			}
		}
		return newTable;
	}

	public static TableData tableExtractor_ByCell(String cells_Element, String[] headers) {

		return tableExtractor_ByCell(cells_Element, null, null, headers);
	}

	// Useful for Rows of data or a panel's worth of data.
	public static HashMap<String, String> cloneHashMap(HashMap<String, String> original) {
		log_Status("Making copy of " + original.toString());
		return new HashMap<String, String>(original);
	}

	public static TableData tableExtractor_ByCell(String cells_Element, String elementVar1, String elementVar2,
												  String[] headers) {
		ArrayList<WebElement> cellElements = Locator.lookup_multipleElements(cells_Element, elementVar1, elementVar2);
		return tableExtractor_ByCell(cellElements, headers);
	}

	public static TableData tableExtractor_ByCell(ArrayList<WebElement> cells, String[] headers) {
		TableData newTable = new TableData();

		ArrayList<String> cellExtracts = new ArrayList<String>();
		for (WebElement cell : cells) {
			if (cell.getTagName().equals("input")) {
				cellExtracts.add(cell.getAttribute("value"));
			} else {
				cellExtracts.add(cell.getAttribute("innerText"));
			}
		}

		int cellCount = cellExtracts.size();
		int headerCount = headers.length;
		int listSize = cellCount / headerCount;
		// add Hashmaps to our list for each of the rows
		for (int i = 0; i < listSize; i++) {
			newTable.data.add(new HashMap<String, String>());
		}

		int dataIndex = 0;
		for (int i = 0; i < cellCount;) {
			if (dataIndex >= newTable.data.size()) {
				break;
			} else {

			}

			for (int j = 0; j < headerCount; j++) {
				newTable.data.get(dataIndex).put(headers[j], cellExtracts.get(i).trim());

				i++;
			}
			dataIndex++;
		}
		// cells are split up by /t/t/t/t and then /n for line.

		return newTable;
	}

	// This overload takes a header array, Strings in this array should match
	// the column names exactly
	// The xpath needs to take the header as a variable and use that to return
	// the entire column as an element list.
	public static ArrayList<HashMap<String, String>> tableExtractor(String[] headerArray,
			String elementLocator_byHeaderText) {
		ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();

		for (String header : headerArray) {
			ArrayList<WebElement> columnCellList = Locator.lookup_multipleElements(elementLocator_byHeaderText, header,
					null);

			for (int i = 0; i < columnCellList.size(); i++) {
				HashMap<String, String> tableRow = null;

				if (table_Values.size() == 0 || table_Values.size() <= i) {
					tableRow = new HashMap<String, String>();
					tableRow.put(header, BaseUI.getTextFromField(columnCellList.get(i)));
					table_Values.add(tableRow);
				} else {
					table_Values.get(i).put(header, BaseUI.getTextFromField(columnCellList.get(i)));
				}
			}
		}
		return table_Values;
	}

	// Tests the passed in column name.
	// Takes 2 ArrayList<HashMap<String,String>> representing the table data.
	// The field is the field that we want to check. Must match the Keys in the
	// HashMaps
	// Tests the columns to make sure they match. Table rows for both tables
	// must
	// be in the same order.
	public static void verify_TableColumns_Match(String columnToVerify, ArrayList<HashMap<String, String>> firstTable,
			ArrayList<HashMap<String, String>> secondTable) {
		verify_TableColumns_Match(columnToVerify, columnToVerify, firstTable, secondTable);
	}

	public static void verify_TableColumns_Match(String firstTableColumn, String secondTableColumn,
			ArrayList<HashMap<String, String>> firstTable, ArrayList<HashMap<String, String>> secondTable) {
		BaseUI.verify_true_AndLog(firstTable.size() == secondTable.size(), "Tables matched in size",
				"Tables did NOT match in size.");

		SoftAssert softAssert = new SoftAssert();
		for (int i = 0; i < firstTable.size(); i++) {
			String firstTableValue = firstTable.get(i).get(firstTableColumn).trim();
			String secondTableValue = secondTable.get(i).get(secondTableColumn).trim();
			softAssert.assertEquals(secondTableValue, firstTableValue, MessageFormat.format("Table Values did NOT match for Row {0} and field {1}", i, firstTableColumn
					+ "\n"
					+ MessageFormat.format("Expected {0}, saw {1}.", firstTableValue, secondTableValue)));
		}
		softAssert.assertAll();
	}

	public static void verify_TableColumns_Match_IgnoreCase(String firstTableColumn, String secondTableColumn,
			ArrayList<HashMap<String, String>> firstTable, ArrayList<HashMap<String, String>> secondTable) {
		BaseUI.verify_true_AndLog(firstTable.size() == secondTable.size(), "Tables matched in size",
				"Tables did NOT match in size.");

		for (int i = 0; i < firstTable.size(); i++) {
			String firstTableValue = firstTable.get(i).get(firstTableColumn).trim();
			String secondTableValue = secondTable.get(i).get(secondTableColumn).trim();
			verify_true_AndLog(firstTableValue.equalsIgnoreCase(secondTableValue),
					MessageFormat.format("Table Values matched for Row {0} and field {1}, Values were: {2}", i,
							firstTableColumn, firstTableValue),
					MessageFormat.format("Table Values did NOT match for Row {0} and field {1}", i, firstTableColumn
							+ "\n"
							+ MessageFormat.format("Expected {0}, saw {1}.", firstTableValue, secondTableValue)));
		}

	}

	// Verify that the given value exists within the table column.
	public static void verify_TableColumn_Contains_GivenValue(String columnToVerify, String columnValueNeeded,
			ArrayList<HashMap<String, String>> firstTable) {
		boolean valueFound = false;

		for (HashMap<String, String> tableRow : firstTable) {
			if (tableRow.get(columnToVerify).equals(columnValueNeeded)) {
				valueFound = true;
				break;
			}
		}

		BaseUI.verify_true_AndLog(valueFound,
				MessageFormat.format("Found value {0} for column {1}", columnValueNeeded, columnToVerify),
				MessageFormat.format("Did NOT find value {0} for column {1}", columnValueNeeded, columnToVerify));

	}

	// Verify that the given value exists within the table column.
	public static void verify_TableColumn_AllColumnValues_MatchString(String columnToVerify, String columnValueNeeded,
			ArrayList<HashMap<String, String>> firstTable) {

		BaseUI.verify_true_AndLog(firstTable.size() > 0, "Found rows to test.", "Did not find rows to test.");

		String output = "";
		int i = 0;
		for (HashMap<String, String> tableRow : firstTable) {
			String actualValue = tableRow.get(columnToVerify);
			if (!columnValueNeeded.equals(actualValue)) {
				output += "\nColumn " + columnToVerify + " at index " + String.valueOf(i) + " was not equal to "
						+ columnValueNeeded + " seeing " + actualValue;
			}
			i++;
		}

		BaseUI.verify_true_AndLog(output.equals(""),
				"All values in column " + columnToVerify + " match " + columnValueNeeded, output);
	}

	// Verify that the given value exists within the table column.
	public static void verify_TableColumn_AllColumnValues_STARTWithString(String columnToVerify,
			String columnValueNeeded, ArrayList<HashMap<String, String>> firstTable) {

		BaseUI.verify_true_AndLog(firstTable.size() > 0, "Found rows to test.", "Did not find rows to test.");

		for (HashMap<String, String> tableRow : firstTable) {
			BaseUI.verify_true_AndLog(tableRow.get(columnToVerify).startsWith(columnValueNeeded),
					"Table value started with " + columnValueNeeded,
					"Table value " + tableRow.get(columnToVerify) + " did NOT start with " + columnValueNeeded);
		}
	}

	// Verifies a single row of data matches the expected row.
	// Pass in both the expected row and the actual row stored in
	// HashMap<String, String>. It will loop
	// through all of the keys and verify the values match.
	// Also checks the key counts to make sure they're the same.
	public static void verify_TableRow_Matches(String rowNumber, HashMap<String, String> expectedRow,
			HashMap<String, String> actualRow) {

		boolean testPass = true;
		String output = "";

		BaseUI.verify_true_AndLog(expectedRow.keySet().size() > 0, "Found keys.", "Did NOT find keys.");

		BaseUI.verify_true_AndLog(expectedRow.keySet().size() == actualRow.keySet().size(), "Keysets matched in count.",
				"Keysets did not match in count.");

		for (String key : expectedRow.keySet()) {
			if (!expectedRow.get(key).equals(actualRow.get(key))) {
				testPass = false;
				output += System.lineSeparator()
						+ MessageFormat.format("For field {0}, expected value {1}, but seeing {2}.", key,
								expectedRow.get(key), actualRow.get(key));
			}
		}

		BaseUI.verify_true_AndLog(testPass, MessageFormat.format("Line {0} passed.", rowNumber), output);
	}

	// Verifies a single row of data matches the expected row.
	// Pass in both the expected row and the actual row stored in
	// HashMap<String, String>. It will loop
	// through all of the keys and verify the values match.
	// Also checks the key counts to make sure they're the same.
	public static void verify_TableRow_Matches_ExpectedFields(String rowNumber, HashMap<String, String> expectedRow,
			HashMap<String, String> actualRow) {

		boolean testPass = true;
		String output = "";

		BaseUI.verify_true_AndLog(expectedRow.keySet().size() > 0, "Found keys.", "Did NOT find keys.");

		for (String key : expectedRow.keySet()) {
			if (actualRow.containsKey(key)) {
				if (!expectedRow.get(key).equals(actualRow.get(key))) {
					testPass = false;
					output += System.lineSeparator()
							+ MessageFormat.format("For field {0}, expected value {1}, but seeing {2}.", key,
									expectedRow.get(key), actualRow.get(key));
				}
			} else {
				testPass = false;
				output += System.lineSeparator()
						+ MessageFormat.format("For expected field {0} was not found in the row.", key);
			}
		}

		BaseUI.verify_true_AndLog(testPass, MessageFormat.format("Line {0} passed.", rowNumber), output);
	}

	public static Boolean pageSourceContainsString(String valueToLookFor) {
		Boolean valueExists = Browser.driver.getPageSource().contains(valueToLookFor);
		if (valueExists) {
			log_Status(MessageFormat.format("Page contains value {0}", valueToLookFor));
		} else {
			log_Status(MessageFormat.format("Page does NOT contain value {0}", valueToLookFor));
		}

		return valueExists;
	}

	public static boolean pageSourceNotContainsString(String valueToLookFor) {
		boolean valueExists = Browser.driver.getPageSource().contains(valueToLookFor);
		if (valueExists) {
			log_Status(MessageFormat.format("Page contains value {0}", valueToLookFor));
		} else {
			log_Status(MessageFormat.format("Page does NOT contain value {0}", valueToLookFor));
		}

		return !valueExists;
	}

	// quick method to wait for an element to not exist on the page. Need to
	// make sure that the text passed in does not match anything else on page.
	public static void wait_for_PageSource_ToNoLongerContainText(String textWeDontWant, Integer numberOfSecondsToWait)
			throws Exception {

		log_Status(MessageFormat.format("Waiting for {0} to no longer exist on page.", textWeDontWant));

		for (int i = 0; i < numberOfSecondsToWait * 2; i++) {
			if (BaseUI.pageSourceContainsString(textWeDontWant)) {
				Thread.sleep(500);
			} else {
				Thread.sleep(100);
				break;
			}
		}
	}

	public static boolean wait_for_PageSource_ToContainText(String textWeWant, Integer numberOfSecondsToWait)
			throws Exception {
		return wait_for_PageSource_ToContainText(textWeWant, Duration.ofSeconds(numberOfSecondsToWait));
	}

	// quick method to wait for an element to not exist on the page. Need to
	// make sure that the text passed in does not match anything else on page.
	public static boolean wait_for_PageSource_ToContainText(String textWeWant, Duration timeToWait)
			throws Exception {
		long deadline = System.currentTimeMillis() + timeToWait.toMillis();
		boolean result = false;
		log_Status(MessageFormat.format("Waiting for {0} to exist on page.", textWeWant));

		while(System.currentTimeMillis()<deadline) {
			if (BaseUI.pageSourceContainsString(textWeWant)) {
				result = true;
				break;
			} else {
				Thread.sleep(50);
			}
		}
		return result;
	}

	// Click with JavaScript. Work around for some of the tricky elements.
	public static void scroll_to_element(@Nullable WebElement elementToScrollTo) throws Exception {
		if (elementToScrollTo == null) {
			log_AndFail("The element has not been found.");
		} else {
			log_Status("Scrolling to WebElement " + elementToScrollTo.getText());
			((JavascriptExecutor) Browser.driver).executeScript("arguments[0].scrollIntoView();", elementToScrollTo);
			Thread.sleep(300);
		}
	}



	public static void scroll_element_to_middleOfScreen(@Nullable WebElement elementToScrollTo) throws Exception {
		if (elementToScrollTo == null) {
			log_AndFail("The element has not been found.");
		} else {
			String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
					+ "var elementTop = arguments[0].getBoundingClientRect().top;"
					+ "window.scrollBy(0, elementTop-(viewPortHeight/2));";

			((JavascriptExecutor) Browser.driver).executeScript(scrollElementIntoMiddle, elementToScrollTo);
			Thread.sleep(300);
		}
	}



	public static void scroll_to_element_NOT_Javascript(@NotNull WebElement elementToScrollTo) throws Exception {
		Parameter.named("elementToScrollTo").withValue(elementToScrollTo).mustBeNonNull();

		log_Status("Attempting to scroll to element.");

		Actions actions = new Actions(Browser.driver);
		actions.moveToElement(elementToScrollTo);
		actions.perform();
	}

	// example: passing in -1 would get you yesterday's date in format
	// MM/dd/yyyy.
	public static String getDateAsString_InRelationToTodaysDate(Integer numberOfDatesFromToday) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, numberOfDatesFromToday);

		String dateFormatted = dateFormat.format(cal.getTime());

		log_Status("Returning date " + dateFormatted);

		return dateFormatted;
	}

	public static String return_Date_AsDifferentFormat(String dateString, String initialFormat, String newFormat)
			throws Exception {
		DateFormat firstFormat = new SimpleDateFormat(initialFormat);
		// cal.setTime(firstFormat.parse(dateString));
		DateFormat secondFormat = new SimpleDateFormat(newFormat);
		Date date = firstFormat.parse(dateString);

		String dateWithNewFormat = secondFormat.format(date);
		log_Status("Returning date " + dateWithNewFormat);

		return dateWithNewFormat;
	}

	// Example of dateFormat: "MM/dd/yyyy"
	public static void verify_FirstDate_IsBefore_OrEQUALto_SecondDate(String firstDate, String secondDate,
			String dateFormat) throws Exception {

		DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
		LocalDate beforeDate = LocalDate.parse(firstDate, format);
		LocalDate afterDate = LocalDate.parse(secondDate, format);

		Boolean isBefore = (beforeDate.isBefore(afterDate) || beforeDate.equals(afterDate));

		BaseUI.verify_true_AndLog(isBefore, MessageFormat.format("Date {0} was before Date {1}", firstDate, secondDate),
				MessageFormat.format("Date {0} was NOT before Date {1}", firstDate, secondDate));
	}

	public static String getDate_WithFormat_X_Days_FromInitialString(String firstDate, String firstFormat,
			String secondFormat, Integer daysDifference) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(firstFormat);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat.parse(firstDate));
		cal.add(Calendar.DATE, daysDifference);

		DateFormat second_DateFormat = new SimpleDateFormat(secondFormat);

		String dateFormatted = second_DateFormat.format(cal.getTime());

		log_Status("Returning date " + dateFormatted);

		return dateFormatted;
	}

	public static String getDate_WithFormat_X_Minutes_FromInitialString(String firstDate, String firstFormat,
			String secondFormat, int minutesDifference) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(firstFormat);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat.parse(firstDate));
		cal.add(Calendar.MINUTE, minutesDifference);

		DateFormat second_DateFormat = new SimpleDateFormat(secondFormat);

		String dateFormatted = second_DateFormat.format(cal.getTime());

		log_Status("Returning date " + dateFormatted);

		return dateFormatted;
	}

	public static String get_SystemTimezone() {
		String timezone = "";

		TimeZone tz = Calendar.getInstance().getTimeZone();
		timezone = tz.getDisplayName();

		log_Status("System time zone is set to " + timezone);

		return timezone;
	}

	// example: passing in -1 would get you yesterday's date in format passed in
	// format.
	// MM/dd/yyyy.
	public static String getDateAsString_InRelationToTodaysDate(Integer numberOfDatesFromToday, String date_format) {

		DateFormat dateFormat = new SimpleDateFormat(date_format);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, numberOfDatesFromToday);

		String dateFormatted = dateFormat.format(cal.getTime());

		log_Status("Returning date " + dateFormatted);

		return dateFormatted;
	}

	// example: passing in 10 would return the time 10 minutes from now.
	// mm:ss.
	public static String getTimeAsString_InRelationToCurrentTime_ByMinutes(Integer numberOfMinutesFromNOW,
			String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, numberOfMinutesFromNOW);

		String dateFormatted = dateFormat.format(cal.getTime());

		log_Status("Returning date " + dateFormatted);

		return dateFormatted;
	}

	/**
	 * This function switches to Alert popUp window gets the text and clicks on OK
	 * button
	 **/
	public static void switchToAlert_Accept() {
		String alertText = null;
		WebDriverWait wait = new WebDriverWait(Browser.driver, 2);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = Browser.driver.switchTo().alert();
		alertText = alert.getText();
		log_Status("Alert message in pop-up window " + alertText);
		alert.accept();
	}

	/**
	 * Pass in the iframe name to switch to.
	 *
	 * @param iframeValue
	 * @throws InterruptedException
	 */
	public static void switch_ToIframe_ByTagname(String iframeValue) throws InterruptedException {
		try {
			log_Status("Switching to iframe window");
			Browser.driver.switchTo().frame(Browser.driver.findElement(By.name(iframeValue)));

			log_Status("Successfully switched to iframe " + iframeValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(100);
	}

	/**
	 * Pass in the iframe value to switch to.
	 *
	 * @param iframeValue
	 * @throws InterruptedException
	 */
	public static void switch_ToIframe(@NotNull WebElement iframeValue) throws InterruptedException {
		Parameter.named("iframeValue").withValue(iframeValue).mustBeNonNull();

		try {
			log_Status("Switching to iframe window");
			Browser.driver.switchTo().frame(iframeValue);

			log_Status("Successfully switched to iframe " + iframeValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(100);
	}

	public static void switch_ToIframe(Integer iframeValue) throws InterruptedException {
		try {
			log_Status("Switching to iframe window");
			Browser.driver.switchTo().frame(iframeValue);

			log_Status("Successfully switched to iframe " + iframeValue.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(100);
	}

	/**
	 * switch to top window or parent window
	 *
	 * @throws InterruptedException
	 */
	public static void switch_ToDefaultContent() throws InterruptedException {
		try {
			log_Status("Switching to default or parent window");
			Browser.driver.switchTo().defaultContent();

			log_Status("Successfully switched to default or parent window");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(100);
	}

	/**
	 * Select an item in a menu based on the menu locator string and the item to be
	 * selected
	 *
	 * @param menuID
	 * @param itemToClick
	 **/
	public static void clickItemFromLeftMenu(String menuID, String itemToClick) {

		Actions action = new Actions(Browser.driver);
		BaseUI.click(Locator.lookupElement(menuID));
		action.moveToElement(Locator.lookupRequiredElement(itemToClick)).click().perform();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// Do nothing
		}
		log_Pass("Pass - Clicked the " + itemToClick + " item in the menu " + menuID);
	}

	/**
	 * This function gets the value selected in dropdown menu and returns the same
	 * value
	 *
	 * @param locator
	 * @return
	 **/
	public static String getSelectedOptionFromDropdown(String locator) {
		return getSelectedOptionFromDropdown(Locator.lookupRequiredElement(locator));
	}
	
	public static String getSelectedOptionFromDropdown(@NotNull WebElement dropdown) {
		Parameter.named("dropdown").withValue(dropdown).mustBeNonNull();

		Select select = new Select(dropdown);
		String selectedValue =  select.getFirstSelectedOption().getText();
		log_Status("Selected Value from dropdown menu " + selectedValue);
		return selectedValue;
	}

	public static String remove_ExtraSpaces(String valueToFormat) {
		log_Status(MessageFormat.format("Removing extra spaces from {0}", valueToFormat));

		valueToFormat = valueToFormat.replaceAll("\\s{2,}", " ");

		return valueToFormat;
	}

	// Need to be careful while using this, can strip character as well
	public static String remove_NonStandardSpaces(String valueToFormat) {
		log_Status(MessageFormat.format("Removing NonStandard Spaces from {0}", valueToFormat));

		valueToFormat = valueToFormat.replaceAll("([^a-zA-Z0-9,-]|\\s)", " ");

		log_Status(MessageFormat.format("Value after removing NonStandard Spaces {0}", valueToFormat));

		return valueToFormat;
	}

	public static Boolean string_IsDouble(String valueToCheck) {
		String decimalPattern = "^-?[0-9]+\\.[0-9]+$";
		Boolean match = Pattern.matches(decimalPattern, valueToCheck);
		log_Status("String was a double? " + match.toString());

		return match;
	}

	public static String convertDouble_ToString_ForCurrency(Double doubleToConvert) {
		// TODO: Change callers to use the overload that takes a primitive double
		Parameter.named("doubleToConvert").withValue(doubleToConvert).mustBeNonNull();
		return convertDouble_ToString_ForCurrency(doubleToConvert.doubleValue());
	}

	public static String convertDouble_ToString_ForCurrency(double doubleToConvert) {
		DecimalFormat formatter = new DecimalFormat("#,###0.00");
		String convertedDouble = formatter.format(doubleToConvert);

		log_Status(MessageFormat.format("Returning Double {0} as String {1}", String.valueOf(doubleToConvert),
				convertedDouble));

		return convertedDouble;
	}

	// This is used only when Double is less than 1 and 0 is absent like .27
	public static String convertDouble_ToString_ForLessThanOne(Double doubleToConvert) {

		DecimalFormat formatter = new DecimalFormat("#.00");
		String convertedDouble = formatter.format(doubleToConvert);

		log_Status(MessageFormat.format("Returning Double {0} as String {1}", doubleToConvert.toString(),
				convertedDouble));

		return convertedDouble;
	}

	public static String convertInteger_ToString_ForCurrency(Integer integerToConvert) {
		DecimalFormat formatter = new DecimalFormat("#,###0.00");
		String convertedInteger = formatter.format(integerToConvert);

		log_Status(MessageFormat.format("Returning Integer {0} as String {1}", integerToConvert.toString(),
				convertedInteger));

		return convertedInteger;
	}

	/**
	 * This function drags an element or text from one location to drop element
	 * location
	 *
	 * @param dragElement
	 * @param dropElement
	 * @throws Exception
	 **/
	public static void dragAndDrop(String dragElement, String dropElement) throws Exception {
		log_Status("Checking if element to be dragged is visible on page" + dragElement);
		BaseUI.verifyElementAppears(Locator.lookupElement(dragElement));
		log_Status("Checking if droppable element is visible on page" + dropElement);
		BaseUI.verifyElementAppears(Locator.lookupElement(dropElement));
		Actions builder = new Actions(Browser.driver);

		log_Status("Dragging the element" + dragElement);
		Action dragAndDrop = builder.clickAndHold(Locator.lookupRequiredElement(dragElement))
				.moveToElement(Locator.lookupRequiredElement(dropElement)).release(Locator.lookupRequiredElement(dropElement)).build();

		log_Status("Dropping the element in the droppable place" + dropElement);
		dragAndDrop.perform();
		Thread.sleep(1000);
	}

	public static String getTodaysDate_AsString() {
		String format = "MM/dd/yyyy";
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String todaysDate = dateFormat.format(date);
		log_Status("Returning date " + todaysDate);

		return todaysDate;
	}

	@Contract(value = "_ -> fail")
	public static void log_AndFail(String failureToLog) {
		String classAndMethod = get_Method_PriorToBaseUI();
		String textToLog = failureToLog;

		if (classAndMethod != null) {
			textToLog = "FAIL - " + get_Method_PriorToBaseUI() + " - " + failureToLog;
		} else {
			textToLog = "FAIL - " + failureToLog;
		}
		
		System.out.println(textToLog);
		Reporter.log(textToLog);
		if (ExtentReportListener2.reporterRunning) {
			ExtentReportListener2.testReporter.log(LogStatus.FAIL, textToLog);
		}
		fail(textToLog);
	}

	public static void log_Warning(String warningToLog) {
		String textToLog =  warningToLog;
		String classAndMethod = get_Method_PriorToBaseUI();

		if (classAndMethod != null) {
			textToLog = "WARNING - " + get_Method_PriorToBaseUI() + " - " + warningToLog;
		} else {
			textToLog = "WARNING - " + warningToLog;
		}
		

		System.out.println(textToLog);
		Reporter.log(textToLog);
		if (ExtentReportListener2.reporterRunning) {
			ExtentReportListener2.testReporter.log(LogStatus.UNKNOWN, warningToLog);
		}
	}

	public static void log_Pass(String passToLog) {
		String textToLog =   passToLog;
		String classAndMethod = get_Method_PriorToBaseUI();

		if (classAndMethod != null) {
			textToLog = "PASS - " + get_Method_PriorToBaseUI() + " - " + passToLog;
		} else {
			textToLog = "PASS - " + passToLog;
		}
		
		
		System.out.println(textToLog);
		Reporter.log(textToLog);
		if (ExtentReportListener2.reporterRunning) {
			ExtentReportListener2.testReporter.log(LogStatus.PASS, textToLog);
		}
	}

	public static void log_Status(String textToLog) {
		textToLog = get_Method_PriorToBaseUI() + " - " + textToLog;
		System.out.println(textToLog);
		Reporter.log(textToLog);
		if (ExtentReportListener2.reporterRunning) {
			ExtentReportListener2.testReporter.log(LogStatus.INFO, textToLog);
		}
	}

	public static void baseStringCompareArray(String[] expectedValue, String[] actualValue) {
		BaseUI.verify_true_AndLog(actualValue.length > 0, "Found Actual Value.", "Could NOT find Actual Value.");

		for (int i = 0; i < expectedValue.length; i++) {

			verify_true_AndLog(expectedValue[i].equals(actualValue[i]),
					"The value displayed for " + " on screen " + actualValue[i] + " matches the expected value of "
							+ expectedValue[i],
					"The value displayed for " + " on screen " + actualValue[i]
							+ " does not match the expected value of " + expectedValue[i]);

		}

	}

	// This method is used to clear text from textbox.
	public static void clearText_Textbox(@NotNull WebElement elementToClear) {
		Parameter.named("elementToClear").withValue(elementToClear).mustBeNonNull();

		log_Status("Clear text from textbox" + elementToClear.getTagName());
		elementToClear.click();

		// this for block is used to clear the input from textbox
		// We can do this by using clear method, but clear method sometime won't work
		// So using this for to clear the text from textbox
		// box.clear();
		String inputValueLength = BaseUI.getTextFromInputBox(elementToClear);

		for (int i = 0; i < inputValueLength.length(); i++) {
			elementToClear.sendKeys(Keys.BACK_SPACE);
		}
	}

	// Return the browser output. Useful for troubleshooting
	public static String return_Browser_Output() {
		log_Status("Attempting to get browser log files.");

		String output = "";
		LogEntries logs = Browser.driver.manage().logs().get("browser");
		if (logs.iterator().hasNext()) {
			for (LogEntry entry : logs) {
				output += entry.getMessage() + "\n";
			}
		}

		return output;
	}

	private static String get_Method_PriorToBaseUI() {
		String fileName = BaseUI.class.getSimpleName() + ".java";
		return getPriorPageObject_AndMethod(fileName);
	}

	private static String getPriorPageObject_AndMethod(String targetFileName) {

		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		int indexWeWant = 0;

		for (int i = 0; i < stackTraceElements.length - 1; i++) {

			String fileName = stackTraceElements[i].getFileName();
			String nextFileName = stackTraceElements[i + 1].getFileName();

			if (fileName != null && nextFileName != null && fileName.equals(targetFileName)
					&& !nextFileName.equals(targetFileName)) {
				indexWeWant = i + 1;
				break;
			}
		}

		String pageObjectAndMethod = stackTraceElements[indexWeWant].getFileName().split("\\.")[0] + "."
				+ stackTraceElements[indexWeWant].getMethodName();

		return pageObjectAndMethod;
	}

	// Get system date and time
	// public static String get_SystemDateAndTime() {
	//
	// DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
	//
	// //get current date time with Date()
	// Date date = new Date();
	//
	// // Now format the date
	// String dateTime = dateFormat.format(date);
	// log_Status("Current date and time is " +dateTime);
	//
	// return dateTime;
	//
	// }

	public static boolean checkBoxStatus(String checkboxName) {
		WebElement tempElement = Locator.lookupRequiredElement(checkboxName);
		boolean checked = tempElement.isSelected();
		String checkedValue = String.valueOf(checked);
		BaseUI.log_Status("Returning checkbox status of " +checkedValue + " for element " +checkboxName);
		return checked;

	}

	private static <T> void logPass(@NotNull String reason, @Nullable T value, @NotNull Matcher<T> matcher) {
		Parameter.named("reason").withValue(reason).mustBeNonNull();
		Parameter.named("matcher").withValue(matcher).mustBeNonNull();

		Description description = new StringDescription();
		if (!reason.isEmpty()) {
			description.appendText(reason)
					.appendText(": ");
		}
		description.appendText("Value ")
				.appendValue(value)
				.appendText(" is ")
				.appendDescriptionOf(matcher);

		log_Pass(description.toString());
	}

	/**
	 * Asserts that the value matches the given condition. Throws on failure, logs on success.
	 *
	 * @param reason  Description of the value
	 * @param actual  The value to check
	 * @param matcher The condition that must be satisfied
	 * @param <T>     Type of the value
	 */
	public static <T> void assertThat(@NotNull String reason, @Nullable T actual, @NotNull Matcher<T> matcher) {
		Parameter.named("reason").withValue(reason).mustBeNonNull();
		Parameter.named("matcher").withValue(matcher).mustBeNonNull();

		org.hamcrest.MatcherAssert.assertThat(reason, actual, matcher);

		// If we got this far, the assertion must have succeeded.
		logPass(reason, actual, matcher);
	}

	/**
	 * Asserts that the value matches the given condition. Throws on failure, logs on success.
	 *
	 * @param actual  The value to check
	 * @param matcher The condition that must be satisfied
	 * @param <T>     Type of the value
	 */
	public static <T> void assertThat(@Nullable T actual, @NotNull Matcher<T> matcher) {
		Parameter.named("matcher").withValue(matcher).mustBeNonNull();

		assertThat("", actual, matcher);
	}

	public static String durationToString(Duration duration) {
		return DurationFormatUtils.formatDuration(duration.toMillis(), "m:ss");
	}
}
