package utils;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.Collection;

public final class Is {
    private Is() {
    }

    public static <T> Matcher<T> equalTo(T expected) {
        return Matchers.equalTo(expected);
    }

    public static <T> Matcher<T> not(Matcher<T> matcher) {
        return Matchers.not(matcher);
    }

    public static <T> Matcher<Collection<?>> emptyCollection() {
        return Matchers.empty();
    }

    public static <T> Matcher<Collection<?>> nonEmptyCollection() {
        return Matchers.not(Matchers.empty());
    }

    public static <T> Matcher<T> sameInstanceAs(T value) {
        return Matchers.sameInstance(value);
    }

    public static <T> Matcher<T> differentInstanceThan(T value) {
        return Matchers.not(sameInstanceAs(value));
    }

    public static Matcher<String> contains(String value) {return Matchers.containsString(value); }
}
