// LET 12/14/15: Locator.java
// Purpose: Builds the object repository based on values stored in a text file containing objectLookupKey, objectValue

package utils;

import org.jetbrains.annotations.Nullable;
import org.testng.annotations.BeforeTest;
import org.w3c.dom.Document;
import utils.winiumXRay.XRayUtils;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

import static utils.StringUtils.angleQuote;

public class Locator {

	private static Map<String, String> map = new HashMap<>();

	// Need to read text file and build up hash map then create the element
	// based on the hashed value
	public static void loadObjectRepository(String... repoTextFiles) throws IOException {
		StackTrace.verifyHasCallerWithAnnotation(Arrays.asList(BeforeTest.class,BeforeSuite.class, BeforeClass.class),
				"The object repository may only be initialized from a {0} method");

		Map<String, String> newMap = new HashMap<>();
		String userDirectory = System.getProperty("user.dir");
		for (String repoTextFile : repoTextFiles) {
			try (BufferedReader br = new BufferedReader(new FileReader(userDirectory + repoTextFile))) {
				String line = null;
				while ((line = br.readLine()) != null) {
					String arr[] = line.split("\\s", 2);
					if (arr.length == 1) {
						if (arr[0].length() == 0 || arr[0].contains("***")) {
							// Empty line is being ignored
						} else {
							BaseUI.log_Warning("The line of -" + arr[0]
									+ "- does not have a key and a value. Please update this object before continuing");
						}
					} else {
						if (!arr[0].contains("***")) {
							newMap.put(arr[0].trim(), arr[1].trim());
						}
					}
				}
			}
		}
		map = newMap;
	}

	@NotNull
	public static WebElement lookupRequiredElement(String objectLookupKey) {
		return lookupRequiredElement(objectLookupKey, null, null);
	}

	@NotNull
	public static WebElement lookupRequiredElement(String objectLookupKey, String variable1, String variable2) {

		if(map.get(objectLookupKey).startsWith("xr")){
			WebElement elementToReturn = findBy_XRay(objectLookupKey, variable1, variable2);
			if(elementToReturn == null){
				throw new NoSuchElementException("Unable to find xrxpath element. If there were any page refreshes, check that Invalidation logic was called.");
			}

			return elementToReturn;
		}else {
			By byLocator = lookupBy(objectLookupKey, variable1, variable2);
			return findRequiredElement(byLocator);
		}
	}

	@Nullable
	private static WebElement findBy_XRay(String objectLookupKey, String variable1, String variable2) {
		return findBy_XrXpath(get_XR_XPath(objectLookupKey, variable1, variable2));
	}

	public static String get_XR_XPath(String objectLookupKey, String variable1, String variable2){
		String objectType = map.get(objectLookupKey).split("=", 2)[0];
		String identifier = map.get(objectLookupKey).split("=", 2)[1];

		String xpath = "//*[./{0}[./text()='{1}']]";
		xpath = xpath.replace("'", "''");

		if (objectType.equals("xrname")) {
			xpath = MessageFormat.format(xpath, "WidgetName", identifier);
		} else if (objectType.equals("xrtype")) {
			xpath = MessageFormat.format(xpath, "WidgetType", identifier);
		} else if (objectType.equals("xrlabel")) {
			xpath = MessageFormat.format(xpath, "WidgetLabel", identifier);
		} else if (objectType.equals("xrvalue")) {
			xpath = MessageFormat.format(xpath, "WidgetValue", identifier);
		} else if (objectType.equals("xrxpath")){
			xpath = insertVariables_IntoPath(identifier, variable1, variable2);
		} else {
			BaseUI.log_AndFail("Unknown identifier type of " + objectType);
		}

		return xpath;
	}

	@NotNull
	public static WebElement findRequiredElement(By by) {
		return Browser.driver.findElement(by);
	}

	// Legacy lookupElement method. Deprecated - use lookupRequiredElement or lookupOptionalElement instead.
	@Nullable
	public static WebElement lookupElement(String objectLookupKey) {
		return lookupOptionalElement(objectLookupKey);
	}

	// Main lookup action - Based on the value found by the key, we set the By
	// locator value
	@Nullable
	public static WebElement lookupOptionalElement(String objectLookupKey) {
		return lookupOptionalElement(objectLookupKey, null, null);
	}

	// Legacy lookupElement method. Deprecated - use lookupRequiredElement or lookupOptionalElement instead.
	@Nullable
	public static WebElement lookupElement(String objectLookupKey, String variable1, String variable2) {
		return lookupOptionalElement(objectLookupKey, variable1, variable2);
	}

	// Main lookup action - Based on the value found by the key, we set the By
	// locator value. Overload accepts up to 2 variables as Strings.
	@Nullable
	public static WebElement lookupOptionalElement(String objectLookupKey, String variable1, String variable2) {
		if(map.get(objectLookupKey).startsWith("xr")){
			return findBy_XRay(objectLookupKey, variable1, variable2);
		}else {
			By byLocator = lookupBy(objectLookupKey, variable1, variable2);
			return findOptionalElement(byLocator);
		}
	}

	@Nullable
	private static WebElement findBy_XrXpath(String xpath) {

		try {
			Document xrayXML = XRayUtils.getXML();

			String tempWinHandle = "";
			XMLParser parser = XMLParser.getXMLParserWithContent(XRayUtils.getXrayLocation(), xrayXML);
			Node parserNode = parser.findNode(xpath);
			NodeList nodes = parserNode.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element el = (Element) nodes.item(i);
					if (el.getTagName().equals("WindowsHandle")) {
						tempWinHandle = el.getTextContent();
						//byLocator = By.xpath("//*[contains(@NativeWindowHandle,'" + tempWinHandle + "')]");
						break;
					}
				}
			}

			Browser.driver.switchTo().window(tempWinHandle);
			return Browser.driver.switchTo().activeElement();

		}catch(Exception e){
			BaseUI.log_AndFail("Encountered exception while trying to locate element using xrxpath: " + e.getMessage());
		}

		return null;
	}

	@NotNull
	public static By lookupBy(String objectLookupKey, String variable1, String variable2) {
		String myPath = map.get(objectLookupKey);

		if (myPath == null) {
			BaseUI.log_AndFail("Could not find '" + objectLookupKey + "' in repository.");
		}

		// Use double angle quotes for clarity, instead of ' or ", because XPath will often contain ' or ".
		BaseUI.log_Status("Path for locator " + angleQuote(objectLookupKey)
				+ " is " + angleQuote(myPath)
				+ (variable1 != null ? " - {0}=" + angleQuote(variable1) : "")
				+ (variable2 != null ? " - {1}=" + angleQuote(variable2) : ""));

		By byLocator;

		if (myPath.startsWith("//") || myPath.startsWith("(")) {
			myPath = insertVariables_IntoPath(myPath, variable1, variable2);
			byLocator = By.xpath(myPath);
		} else if (myPath.startsWith("id=")) {
			byLocator = By.id(myPath.split("id=")[1]);
		} else if (myPath.startsWith("css=")) {
			byLocator = By.cssSelector(myPath.split("css=")[1]);
		} else if (myPath.startsWith("className=")) {
			byLocator = By.className(myPath.split("className=")[1]);
		} else if (myPath.startsWith("name=")) {
			byLocator = By.name(myPath.split("name=")[1]);
		} else if (myPath.startsWith("partialLinkText=")) {
			byLocator = By.partialLinkText(myPath.split("partialLinkText=")[1]);
		} else {
			BaseUI.log_Warning("Identifier " + objectLookupKey + " has a value of " + myPath
					+ " that doesn't match any known format. Attempting to continue as an id.");
			byLocator = By.id(myPath);
		}

		return byLocator;
	}

	@NotNull
	private static String insertVariables_IntoPath(String path, String variable1, String variable2){
		if (path.contains("{1}")) {
			// Escape single quotes since they have special meaning to MessageFormat
			path = path.replace("'", "''");
			path = MessageFormat.format(path, variable1, variable2);
		} else if (path.contains("{0}")) {
			// Escape single quotes since they have special meaning to MessageFormat
			path = path.replace("'", "''");
			path = MessageFormat.format(path, variable1);
		}
		return path;
	}


	// Main lookup action - Based on the value found by the key, we set the By
	// locator value
	@NotNull
	public static ArrayList<WebElement> lookup_multipleElements(String objectLookupKey, String variable1,
																String variable2) {
		ArrayList<WebElement> elementsFound = new ArrayList<WebElement>();

		By byLocator;

		byLocator = lookupBy(objectLookupKey, variable1, variable2);

		elementsFound.addAll(Browser.driver.findElements(byLocator));

		return elementsFound;
	}

	// Legacy findElement method. Deprecated - use findRequiredElement or findOptionalElement instead.
	@Nullable
	public static WebElement findElement(By by) {
		return findOptionalElement(by);
	}

	@Nullable
	public static WebElement findOptionalElement(By by) {
			try {
				List<WebElement> elements = Browser.driver.findElements(by);
				if (elements.size() != 0) {
					return elements.get(0);
				} else {
					BaseUI.log_Warning("Object does not appear to exist. Attempting to continue");
					return null;
				}
			} catch (Exception e) {
				BaseUI.log_Warning("Element could not be found by " + by.toString());
				return null;
			}
//		}
	}
}
