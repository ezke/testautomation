package utils.WhiteSeedWrapper;

import org.jetbrains.annotations.Nullable;
import whiteSeedProxy.*;

import javax.xml.bind.JAXBElement;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static utils.WhiteSeedWrapper.WhiteSeedFault.CreateWhiteSeedFault;

public class Application {
    private final int _applicationID;
    private IRoots _roots;

    public Application(IRoots roots, String appPath, String... appArgs) throws WhiteSeedFault {
        _roots = roots;
        ObjectFactory factory = new ObjectFactory();

        try {
            _applicationID = _roots.launchAndAttachApplication(appPath, packageStringArray(appArgs));
        } catch (IRootsLaunchAndAttachApplicationUnknownFaultFaultFaultMessage iRootsLaunchAndAttachApplicationUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsLaunchAndAttachApplicationUnknownFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsLaunchAndAttachApplicationTimeoutFaultFaultFaultMessage iRootsLaunchAndAttachApplicationTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsLaunchAndAttachApplicationTimeoutFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public Application(IRoots roots, String appPath, double timeoutDurationInSeconds) {
        _roots = roots;
        try {
            _applicationID = _roots.attachApplication(appPath, timeoutDurationInSeconds);
        } catch (IRootsAttachApplicationNotFoundFaultFaultFaultMessage iRootsAttachApplicationNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsAttachApplicationNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsAttachApplicationTimeoutFaultFaultFaultMessage iRootsAttachApplicationTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsAttachApplicationTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsAttachApplicationUnknownFaultFaultFaultMessage iRootsAttachApplicationUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsAttachApplicationUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void close() throws WhiteSeedFault {
        try {
            _roots.closeApplication(_applicationID);
        } catch (IRootsCloseApplicationUnknownFaultFaultFaultMessage iRootsCloseApplicationUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsCloseApplicationUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void waitForElement(Duration timeout, ElementFilter... filterArray) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filterArray);
        try {
            _roots.waitForElement(_applicationID, filters, (int) timeout.getSeconds());
        } catch (IRootsWaitForElementTimeoutFaultFaultFaultMessage iRootsWaitForElementTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsWaitForElementTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsWaitForElementUnknownFaultFaultFaultMessage iRootsWaitForElementUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsWaitForElementUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void clickElement(ElementFilter... filterArray) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filterArray);
        try {
            _roots.elementAction(_applicationID, filters, ElementActions.CLICK);
        } catch (IRootsElementActionNotFoundFaultFaultFaultMessage iRootsElementActionNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementActionNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementActionTimeoutFaultFaultFaultMessage iRootsElementActionTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementActionTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementActionUnknownFaultFaultFaultMessage iRootsElementActionUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementActionUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void focusElement(ElementFilter... filterArray) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filterArray);
        try {
            _roots.elementAction(_applicationID, filters, ElementActions.FOCUS);
        } catch (IRootsElementActionNotFoundFaultFaultFaultMessage iRootsElementActionNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementActionNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementActionTimeoutFaultFaultFaultMessage iRootsElementActionTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementActionTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementActionUnknownFaultFaultFaultMessage iRootsElementActionUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementActionUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void clearElement(ElementFilter... filterArray) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filterArray);
        try {
            _roots.elementClear(_applicationID, filters);
        } catch (IRootsElementClearNotFoundFaultFaultFaultMessage iRootsElementClearNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementClearNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementClearTimeoutFaultFaultFaultMessage iRootsElementClearTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementClearTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementClearUnknownFaultFaultFaultMessage iRootsElementClearUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementClearUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public String getElementText(ElementFilter... filter) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filter);
        try {
            return _roots.queryElement(_applicationID, filters, ElementAttributes.TEXT);
        } catch (IRootsQueryElementNotFoundFaultFaultFaultMessage iRootsQueryElementNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsQueryElementNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsQueryElementUnknownFaultFaultFaultMessage iRootsQueryElementUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsQueryElementUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public String getCellText(String columnName, int rowIndex, ElementFilter... filter) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filter);
        try {
            return _roots.queryTableCell(_applicationID, filters, columnName, rowIndex, ElementAttributes.TEXT);
        } catch (IRootsQueryTableCellNotFoundFaultFaultFaultMessage iRootsQueryTableCellNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsQueryTableCellNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsQueryTableCellTimeoutFaultFaultFaultMessage iRootsQueryTableCellTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsQueryTableCellTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsQueryTableCellUnknownFaultFaultFaultMessage iRootsQueryTableCellUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsQueryTableCellUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void putElementText(String text, ElementFilter... filter) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filter);
        try {
            _roots.elementType(_applicationID, filters, text);
        } catch (IRootsElementTypeNotFoundFaultFaultFaultMessage iRootsElementTypeNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementTypeNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementTypeTimeoutFaultFaultFaultMessage iRootsElementTypeTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementTypeTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementTypeUnknownFaultFaultFaultMessage iRootsElementTypeUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementTypeUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void putElementTextWithSpecialKeys(String text, ElementFilter... filter) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filter);
        try {
            _roots.elementTypeWithSpecialKeys(_applicationID, filters, text);
        } catch (IRootsElementTypeWithSpecialKeysNotFoundFaultFaultFaultMessage iRootsElementTypeWithSpecialKeysNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementTypeWithSpecialKeysNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementTypeWithSpecialKeysTimeoutFaultFaultFaultMessage iRootsElementTypeWithSpecialKeysTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementTypeWithSpecialKeysTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsElementTypeWithSpecialKeysUnknownFaultFaultFaultMessage iRootsElementTypeWithSpecialKeysUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsElementTypeWithSpecialKeysUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public String[] getWindowList() throws WhiteSeedFault {
        List<String> result = null;
        try {
            result = _roots.listWindows(_applicationID).getString();
        } catch (IRootsListWindowsUnknownFaultFaultFaultMessage iRootsListWindowsUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsListWindowsUnknownFaultFaultFaultMessage.getFaultInfo());
        }
        return result.toArray(new String[result.size()]);
    }

    public void setFirstWindowToCurrent() throws WhiteSeedFault {
        try {
            _roots.setFirstWidowToCurrent(_applicationID);
        } catch (IRootsSetFirstWidowToCurrentUnknownFaultFaultFaultMessage iRootsSetFirstWidowToCurrentUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsSetFirstWidowToCurrentUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void setCurrentWindow(ElementFilter... filter) throws WhiteSeedFault {
        ElementFilters filters = new ElementFilters(filter);
        try {
            _roots.setCurrentWindow(_applicationID, filters);
        } catch (IRootsSetCurrentWindowNotFoundFaultFaultFaultMessage iRootsSetCurrentWindowNotFoundFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsSetCurrentWindowNotFoundFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsSetCurrentWindowUnknownFaultFaultFaultMessage iRootsSetCurrentWindowUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsSetCurrentWindowUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public byte[] getScreenShot() throws WhiteSeedFault {
        try {
            return _roots.getScreenShot(_applicationID);
        } catch (IRootsGetScreenShotUnknownFaultFaultFaultMessage iRootsGetScreenShotUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsGetScreenShotUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void typeOnKeyboard(String text) throws WhiteSeedFault {
        try {
            _roots.typeOnKeyboard(_applicationID, text);
        } catch (IRootsTypeOnKeyboardTimeoutFaultFaultFaultMessage iRootsTypeOnKeyboardTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsTypeOnKeyboardTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsTypeOnKeyboardUnknownFaultFaultFaultMessage iRootsTypeOnKeyboardUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsTypeOnKeyboardUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public void typeOnKeyboardWithSpecialKeys(String text) throws WhiteSeedFault {
        try {
            _roots.typeOnKeyboardWithSpecialKeys(_applicationID, text);
        } catch (IRootsTypeOnKeyboardWithSpecialKeysTimeoutFaultFaultFaultMessage iRootsTypeOnKeyboardWithSpecialKeysTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsTypeOnKeyboardWithSpecialKeysTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsTypeOnKeyboardWithSpecialKeysUnknownFaultFaultFaultMessage iRootsTypeOnKeyboardWithSpecialKeysUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsTypeOnKeyboardWithSpecialKeysUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public List<ElementProperties> getElements(ElementFilter... filterArray) {
        try {
            ElementFilters filters = new ElementFilters(filterArray);
            return _roots.getElementInfo(_applicationID, filters).getElementInfo().stream()
                    .map(this::createElementProperties)
                    .collect(Collectors.toList());
        } catch (IRootsGetElementInfoUnknownFaultFaultFaultMessage iRootsGetElementInfoUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsGetElementInfoUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    private ElementProperties createElementProperties(ElementInfo info) {
        return new ElementProperties.Builder()
                .height(info.getHeight())
                .id(getStringValue(info.getId()))
                .initialCheckState(info.getCheckState())
                .initialValue(getStringValue(info.getName()))
                .isEnabled(info.isEnabled())
                .isOffscreen(info.isIsOffscreen())
                .isVisible(info.isVisible())
                .screenX(info.getScreenX())
                .screenY(info.getScreenY())
                .uiAutomationClassName(getStringValue(info.getUiAutomationClassName()))
                .whiteClassName(getStringValue(info.getWhiteClassName()))
                .width(info.getWidth())
                .windowHandle(info.getWindowHandle())
                .build();
    }

    private String getStringValue(@Nullable JAXBElement<String> jaxbElement) {
        return jaxbElement != null
                ? jaxbElement.getValue()
                : null;
    }

    public Optional<ElementProperties> tryGetElement(ElementFilter... filterArray) {
        return getElements(filterArray).stream().findFirst();
    }

    public ElementProperties getElement(ElementFilter... filterArray) {
        return tryGetElement(filterArray).orElseThrow(() -> new WhiteSeedFault("Element not found"));
    }

    public boolean isElementPresent(ElementFilter... filterArray) {
        return tryGetElement(filterArray).isPresent();
    }

    public static Application launchAndAttach(String url, String appPath, String... appArgs) throws MalformedURLException, WhiteSeedFault {
        return new Application(new WhiteSeedService(new URL(url)).getBasicHttpBindingIRoots(), appPath, appArgs);
    }

    public static void launch(String url, String appPath, String... appArgs) throws MalformedURLException {
        WhiteSeedService whiteSeed = new WhiteSeedService(new URL(url));

        try {
            whiteSeed.getBasicHttpBindingIRoots().launchApplication(appPath, packageStringArray(appArgs));
        } catch (IRootsLaunchApplicationTimeoutFaultFaultFaultMessage iRootsLaunchApplicationTimeoutFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsLaunchApplicationTimeoutFaultFaultFaultMessage.getFaultInfo());
        } catch (IRootsLaunchApplicationUnknownFaultFaultFaultMessage iRootsLaunchApplicationUnknownFaultFaultFaultMessage) {
            throw CreateWhiteSeedFault(iRootsLaunchApplicationUnknownFaultFaultFaultMessage.getFaultInfo());
        }
    }

    public static Application attach(String url, String appPath, double timeoutDurationInSeconds) throws MalformedURLException, WhiteSeedFault {
        return new Application(new WhiteSeedService(new URL(url)).getBasicHttpBindingIRoots(), appPath, timeoutDurationInSeconds);
    }

    private static ArrayOfstring packageStringArray(String... strings) {
        ObjectFactory factory = new ObjectFactory();
        ArrayOfstring packagedArgs = factory.createArrayOfstring();
        List<String> args = packagedArgs.getString();

        Collections.addAll(args, strings);
        return packagedArgs;
    }
}
