package utils.WhiteSeedWrapper;

import whiteSeedProxy.ElementAttributes;
import whiteSeedProxy.FormElementFilter;
import whiteSeedProxy.ObjectFactory;

public class ElementFilter extends FormElementFilter {
    public ElementFilter(ElementAttributes field, String value) {
        super();
        ObjectFactory factory = new ObjectFactory();

        super.field=field;
        super.value=factory.createFormElementFilterValue(value);
    }

    public static ElementFilter byAutomationID(String automationID) {
        return new ElementFilter(ElementAttributes.AUTOMATION_ID, automationID);
    }

    public static ElementFilter byText(String text) {
        return new ElementFilter(ElementAttributes.TEXT, text);
    }

    public static ElementFilter byType(String type) {
        return new ElementFilter(ElementAttributes.TYPE, type);
    }

    public static ElementFilter byWindowHandle(long windowHandle) {
        return new ElementFilter(ElementAttributes.WINDOW_HANDLE, String.valueOf(windowHandle));
    }
}
