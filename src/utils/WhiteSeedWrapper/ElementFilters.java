package utils.WhiteSeedWrapper;

import whiteSeedProxy.FormElementFilter;
import whiteSeedProxy.ArrayOfFormElementFilter;

import java.util.Collections;
import java.util.List;

public class ElementFilters extends ArrayOfFormElementFilter{
    public ElementFilters(ElementFilter... filters) {
        super();
        List<FormElementFilter> myList = super.getFormElementFilter();
        Collections.addAll(myList, filters);
    }
}
