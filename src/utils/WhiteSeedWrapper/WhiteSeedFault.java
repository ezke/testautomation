package utils.WhiteSeedWrapper;

import whiteSeedProxy.NotFoundFault;
import whiteSeedProxy.TimeoutFault;
import whiteSeedProxy.UnknownFault;

public class WhiteSeedFault extends Error {
    public WhiteSeedFault(String message) {
        super(message);
    }

    public static WhiteSeedFault CreateWhiteSeedFault(UnknownFault fault) {
        return new WhiteSeedFault(fault.getMessage().getValue());
    }

    public static WhiteSeedFault CreateWhiteSeedFault(NotFoundFault fault) {
        return new WhiteSeedFault(fault.getMessage().getValue());
    }

    public static WhiteSeedFault CreateWhiteSeedFault(TimeoutFault fault) {
        return new WhiteSeedFault(fault.getMessage().getValue());
    }
}