package utils.WhiteSeedWrapper;

import whiteSeedProxy.CheckState;

public class ElementProperties {
    private final double _height;
    private final String _id;
    private final CheckState _initialCheckState;
    private final String _initialValue;
    private final boolean _isEnabled;
    private final boolean _isOffscreen;
    private final boolean _isVisible;
    private final double _screenX;
    private final double _screenY;
    private final String _uiAutomationClassName;
    private final String _whiteClassName;
    private final double _width;
    private final long _windowHandle;

    private ElementProperties(Builder builder) {
        _height = builder._height;
        _id = builder._id;
        _initialCheckState = builder._initialCheckState;
        _initialValue = builder._initialValue;
        _isEnabled = builder._isEnabled;
        _isOffscreen = builder._isOffscreen;
        _isVisible = builder._isVisible;
        _screenX = builder._screenX;
        _screenY = builder._screenY;
        _uiAutomationClassName = builder._uiAutomationClassName;
        _whiteClassName = builder._whiteClassName;
        _width = builder._width;
        _windowHandle = builder._windowHandle;
    }

    public double height() {
        return _height;
    }

    public String id() {
        return _id;
    }

    public CheckState initialCheckState() {
        return _initialCheckState;
    }

    public String initialValue() {
        return _initialValue;
    }

    public boolean isEnabled() {
        return _isEnabled;
    }

    public boolean isOffscreen() {
        return _isOffscreen;
    }

    public boolean isVisible() {
        return _isVisible;
    }

    public double screenX() {
        return _screenX;
    }

    public double screenY() {
        return _screenY;
    }

    public String uiAutomationClassName() {
        return _uiAutomationClassName;
    }

    public String whiteClassName() {
        return _whiteClassName;
    }

    public double width() {
        return _width;
    }

    public long windowHandle() {
        return _windowHandle;
    }

    /**
     * Builder for ElementDescription objects. The methods take nullable values, even though ElementDescription
     * requires non-null values; this is for ease of use with ElementInfo, which is all-nullable, all the time.
     * If null is passed for a property, the builder ignores it and leaves that property at its default value.
     */
    public static class Builder {
        private double _height = 0;
        private String _id = "";
        private CheckState _initialCheckState = CheckState.UNCHECKED;
        private String _initialValue = "";
        private boolean _isEnabled = true;
        private boolean _isOffscreen = false;
        private boolean _isVisible = true;
        private double _screenX = 0;
        private double _screenY = 0;
        private String _uiAutomationClassName = "";
        private String _whiteClassName = "";
        private double _width = 0;
        private long _windowHandle = 0;

        public ElementProperties build() {
            return new ElementProperties(this);
        }

        public Builder height(Double value) {
            if (value != null) {
                _height = value;
            }
            return this;
        }

        public Builder id(String value) {
            if (value != null) {
                _id = value;
            }
            return this;
        }

        public Builder initialCheckState(CheckState value) {
            if (value != null) {
                _initialCheckState = value;
            }
            return this;
        }

        public Builder initialValue(String value) {
            if (value != null) {
                _initialValue = value;
            }
            return this;
        }

        public Builder isEnabled(Boolean value) {
            if (value != null) {
                _isEnabled = value;
            }
            return this;
        }

        public Builder isOffscreen(Boolean value) {
            if (value != null) {
                _isOffscreen = value;
            }
            return this;
        }

        public Builder isVisible(Boolean value) {
            if (value != null) {
                _isVisible = value;
            }
            return this;
        }

        public Builder screenX(Double value) {
            if (value != null) {
                _screenX = value;
            }
            return this;
        }

        public Builder screenY(Double value) {
            if (value != null) {
                _screenY = value;
            }
            return this;
        }

        public Builder uiAutomationClassName(String value) {
            if (value != null) {
                _uiAutomationClassName = value;
            }
            return this;
        }

        public Builder whiteClassName(String value) {
            if (value != null) {
                _whiteClassName = value;
            }
            return this;
        }

        public Builder width(Double value) {
            if (value != null) {
                _width = value;
            }
            return this;
        }

        public Builder windowHandle(Long value) {
            if (value != null) {
                _windowHandle = value;
            }
            return this;
        }
    }
}
