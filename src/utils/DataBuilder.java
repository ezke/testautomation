package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.opencsv.CSVReader;


public class DataBuilder {

	public void addDataLocation(String fileLocation) throws IOException {
		readTestData(fileLocation);
	}

	public static Iterator<Object[]> readTestData(String dataFile) throws IOException {
		ArrayList<Object[]> myEntries = new ArrayList<Object[]>();
		String userDirectory = System.getProperty("user.dir");
		CSVReader myReader = new CSVReader(new FileReader(userDirectory + dataFile), '\t');
		// Grab and throw away the header in line 1;
		myReader.readNext();
		String[] nextLine = null;

		while ((nextLine = myReader.readNext()) != null) {
			myEntries.add(new Object[] { nextLine });
		}
		BaseUI.log_Status("Successfully read data file");
		myReader.close();
		return myEntries.iterator();
	}

	public static Iterator<Object[]> readTestDataForSpecificRows(String dataFile, int specificRowNumber)
			throws IOException {
		ArrayList<Object[]> myEntries = new ArrayList<Object[]>();
		String userDirectory = System.getProperty("user.dir");
		CSVReader myReader = new CSVReader(new FileReader(userDirectory + dataFile), '\t');
		// Grab and throw away the header in line 1;
		myReader.readNext();
		String[] nextLine = null;
		// Ignore the header
		int rowInFile = 0;
		while ((nextLine = myReader.readNext()) != null) {
			rowInFile = rowInFile + 1;
			if (rowInFile == specificRowNumber) {
				BaseUI.log_Status("Using the data from " + nextLine.toString());
				myEntries.add(new Object[] { nextLine });
			}
		}
		BaseUI.log_Status("Successfully read data file");
		myReader.close();
		return myEntries.iterator();
	}

	// Reads a config file in the format of...
	// Name|Someone'sName
	// DOB|01/08/1990

	// returns a HashMap with the key value pairs. Name would be the key,
	// Someone'sName would be the value.
	public static HashMap<String, String> GetConfig_DataFromFile(String txt_path) throws IOException {
		// // Configuration Data
		// String userDirectory = System.getProperty("user.dir");
		// String path = "";
		// path = txt_path;
		// if (txt_path.startsWith("c:") || txt_path.startsWith("C:")) {
		// path = txt_path;
		// } else {
		// path = userDirectory + txt_path;
		// }
		// String text = new Scanner(new File(path)).useDelimiter("\\A").next();
		//
		// // split lines up by row;
		// String lines[] = text.split("\\r?\\n");
		//
		// // String[] headerRow = lines[0].split("\\|");
		//
		// HashMap<String, String> confData = new HashMap<String, String>();
		// for (int i = 0; i < lines.length; i++) {
		// confData.put(lines[i].split("\\|")[0], lines[i].split("\\|")[1]);
		// }
		//
		// return confData;

		return GetConfig_DataFromFile(txt_path, "|");

	}

	// returns a HashMap with the key value pairs. Name would be the key,
	// Someone'sName would be the value.
	public static HashMap<String, String> GetConfig_DataFromFile(String txt_path, String delimiter) throws IOException {
		// Configuration Data

		String path = Get_FileData_Path(txt_path);
		File file = new File(path);
		// If txt_path doesn't exist but txt_path.example does, create txt_path from the
		// example file.
		if (!file.exists()) {
			File exampleFile = new File(file.getAbsolutePath() + ".example");
			if (exampleFile.exists()) {
				FileUtils.copyFile(exampleFile, file);
			}
		}
		String text = Get_FileData_AsString(file);

		// split lines up by row;
		String lines[] = text.split("\\r?\\n");

		// String[] headerRow = lines[0].split("\\|");

		String splitRegex = Pattern.quote(delimiter);

		HashMap<String, String> confData = new HashMap<String, String>();
		for (int i = 0; i < lines.length; i++) {
			final String[] columns = lines[i].split(splitRegex);
			confData.put(columns[0], columns[1]);
		}

		return confData;
	}

	public static String Get_FileData_AsString(String txt_path) throws Exception {
		String path = Get_FileData_Path(txt_path);
		return Get_FileData_AsString(new File(path));
	}

	public static String Get_FileData_AsString(File file) throws FileNotFoundException {
		Scanner readDocument = new Scanner(file);
		String text = readDocument.useDelimiter("\\A").next();
		readDocument.close();

		return text;
	}

	public static String Get_FileData_Path(String filePath) {
		String userDirectory = System.getProperty("user.dir");
		String path = filePath;

		if (!filePath.startsWith("c:") && !filePath.startsWith("C:") && !filePath.startsWith("\\\\")) {
			path = userDirectory + filePath;
		}

		return path;
	}

	public static Object[][] getTableData_FromFile(String txt_tableData) throws FileNotFoundException {
		// Configuration Data
		String userDirectory = System.getProperty("user.dir");
		String path = userDirectory + txt_tableData;
		Scanner newScanner = new Scanner(new File(path));
		String text = newScanner.useDelimiter("\\A").next();
		newScanner.close();
		// split lines up by row;
		String lines[] = text.split("\\r?\\n");

		String[] headerRow = lines[0].split("\\|");

		Object[][] tableData = new Object[lines.length][headerRow.length];

		for (int i = 0; i < lines.length; i++) {
			String[] lineArray = lines[i].split("\\|");
			for (int j = 0; j < headerRow.length; j++) {
				tableData[i][j] = lineArray[j];
			}
		}

		return tableData;

	}

	public static TableData returnTableData_ForComparison(String tableDataPath, String delimiter, Boolean use_UserPath)
			throws FileNotFoundException {
		TableData tabledata = new TableData();
		// Configuration Data
		String userDirectory = System.getProperty("user.dir");
		String path = "";
		if (use_UserPath) {
			path = userDirectory + tableDataPath;
		} else {
			path = tableDataPath;
		}

		Scanner scan = new Scanner(new File(path), "UTF-8");

		String text = scan.useDelimiter("\\A").next();
		scan.close();

		if (text.startsWith("\uFEFF")) {
			text = text.substring(1);
		}
		// split lines up by row;
		String lines[] = text.split("\\r?\\n");

		String[] headerRow;
		if (delimiter.startsWith("\\")) {
			headerRow = lines[0].split(MessageFormat.format("{0}", delimiter));
		} else {
			headerRow = lines[0].split(MessageFormat.format("\\{0}", delimiter));
		}

		// headerRow =
		// lines[0].split(MessageFormat.format("{0}(?=([^\"]*\"[^\"]*\")*[^\"]*$)",
		// delimiter),
		// -1);

		for (int i = 1; i < lines.length; i++) {
			// String[] lineArray = lines[i].split(MessageFormat.format("\\{0}",
			// delimiter));
			// String[] tokens = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)",
			// -1);
			String[] lineArray;
			lineArray = lines[i].split(MessageFormat.format("{0}(?=([^\"]*\"[^\"]*\")*[^\"]*$)", delimiter), -1);

			HashMap<String, String> tableRow = new HashMap<String, String>();
			for (int headerCount = 0; headerCount < headerRow.length; headerCount++) {
				String key = headerRow[headerCount].replaceAll("[\\s\\u00A0]+$", " ").trim();
				String value = null;
				try {
					value = lineArray[headerCount];
				} catch (Exception e) {

				}
				tableRow.put(key, value);
			}
			tabledata.data.add(tableRow);
		}

		scan.close();

		return tabledata;

	}

	public static String return_Downloads_Folder() {
		String home = System.getProperty("user.home");
		String downloads_Location = home + "\\Downloads\\";

		return downloads_Location;
	}

	public static void delete_Files_FromDownloads_withExtension(String extension) {

		delete_Files_WithExtention_FromLocation(extension, return_Downloads_Folder());

	}

	public static void delete_Files_WithExtention_FromLocation(String extension, String folderLocation) {
		extension = extension.startsWith(".") ? extension.substring(1, extension.length()) : extension;
		File folder = new File(folderLocation);
		File folderList[] = folder.listFiles();
		// Searchs .lck
		if (folderList != null) {
			for (int i = 0; i < folderList.length; i++) {
				// String pes = folderList[i].toString();
				// if (pes.endsWith(extension)) {
				// // and deletes
				// boolean success = (new File(folderList[i].toString()).delete());
				// }
				if (folderList[i].isFile()) {
					String fileExtension = FilenameUtils.getExtension(folderList[i].toString());
					if (fileExtension.equals(extension)) {
						boolean success = (new File(folderList[i].toString()).delete());
					}
				}
			}
		}

	}

	public static String return_File_WithExtension(String extension, String folderLocation) {
		File folder = new File(folderLocation);
		File folderList[] = folder.listFiles();
		// Searchs .lck
		for (int i = 0; i < folderList.length; i++) {
			String pes = folderList[i].toString();
			if (pes.endsWith(extension)) {
				// and deletes
				return pes;
			}
		}
		return null;
	}

	// Note: will not preserve original column order.
	public static void update_CSVFile(String filePath, Boolean localFile, String delimiter,
			Consumer<HashMap<String, String>> lambdaToUse) throws Exception {

		ArrayList<String> finalLines = new ArrayList<String>();

		TableData csvData = returnTableData_ForComparison(filePath, delimiter, localFile);
		
		if (csvData == null) {
			BaseUI.log_AndFail("Unable to retrieve CSV data for file " + filePath);
		}
		
		//apply formatting from lambda
		csvData.forEach_Row(lambdaToUse);

		if (delimiter.startsWith("\\")) {
			delimiter = delimiter.substring(1);
		}

		// Assemble the first line and then add to finalLines
		ArrayList<String> keys = new ArrayList<String>(csvData.data.get(0).keySet());
		String firstLine = keys.get(0) + delimiter;
		for (int i = 1; i < keys.size() - 1; i++) {
			firstLine += keys.get(i) + delimiter;
		}
		firstLine += keys.get(keys.size() - 1);
		finalLines.add(firstLine);

		
		
		
		//Reassemble the lines
		for (int i = 0; i < csvData.data.size(); i++) {
			String lineToAdd = "";
		

			for (String key : keys) {
				lineToAdd += csvData.data.get(i).get(key) + delimiter;
			}
			lineToAdd = lineToAdd.substring(0, lineToAdd.lastIndexOf(delimiter));
			finalLines.add(lineToAdd);
		}

		Path newFilePath = Paths.get(filePath);
		Files.write(newFilePath, finalLines, Charset.forName("UTF-8"));

	}
	
	// Note: will not preserve original column order.
	public static void update_CSVFile_OneField(String filePath, Boolean localFile, String delimiter,
			HashMap<String, String> lineMatchingCriteria, String keyToUpdate, String valueToUpdate) throws Exception {

		Consumer<HashMap<String, String>> updateFunction;
		updateFunction = x -> {
			boolean matchesLine = rowMatchesCriteria(x, lineMatchingCriteria);
			
			if (matchesLine) {
				x.put(keyToUpdate, valueToUpdate);
			}
		};
		
		update_CSVFile(filePath, localFile, delimiter, updateFunction);
	}

	private static boolean rowMatchesCriteria(HashMap<String, String> actualRow, HashMap<String, String> matchingCriteria) {
		for (String key : matchingCriteria.keySet()) {
			if (!actualRow.get(key).equals(matchingCriteria.get(key))) {
				return false;
			}
		}

		return true;

	}
	
	public static void wait_ForFile_WithExtension_ToExist(String extension, String folderlocation, int timeToWaitInSeconds) {
		String newextension = extension.startsWith(".") ? extension.substring(1, extension.length()) : extension;
		BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
			public Boolean call() throws Exception {

				File folder = new File(folderlocation);
				File[] listOfExpectedFiles = folder.listFiles();
				Boolean fileHasExtension = false;
				for (File file : listOfExpectedFiles) {
					if (file.isFile()) {
						String fileExtension = FilenameUtils.getExtension(file.toString());
						if (fileExtension.equals(newextension)) {
							fileHasExtension = true;
							break;
						}

					}
				}
				return fileHasExtension;
			}
		}, timeToWaitInSeconds);

	}

}
