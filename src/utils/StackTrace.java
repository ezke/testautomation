package utils;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class StackTrace {

    private StackTrace() {
    }

    private static boolean hasCallerWithAnnotation(Collection<Class<? extends Annotation>> annotations) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        return Arrays.stream(stackTrace).anyMatch(stackTraceElement -> {
            try {
                Class<?> aClass = Class.forName(stackTraceElement.getClassName());
                Method method = aClass.getMethod(stackTraceElement.getMethodName());
                return annotations.stream().anyMatch(a -> method.getDeclaredAnnotation(a) != null);
            } catch (ClassNotFoundException | NoSuchMethodException | NoClassDefFoundError e) {
                return false;
            }
        });
    }

    @NotNull
    private static String describeAnnotations(Collection<Class<? extends Annotation>> permittedAnnotations) {
        List<String> permittedAnnotationNames = permittedAnnotations.stream()
                .map(a -> "@" + a.getSimpleName())
                .collect(Collectors.toList());
        return String.join(" or ", permittedAnnotationNames);
    }

    public static void verifyHasCallerWithAnnotation(Collection<Class<? extends Annotation>> permittedAnnotations,
                                                     String errorMessageTemplate) {
        if (!hasCallerWithAnnotation(permittedAnnotations)) {
            BaseUI.log_AndFail(MessageFormat.format(errorMessageTemplate, describeAnnotations(permittedAnnotations)));
        }
    }
}
