package utils;

import org.awaitility.Awaitility;
import org.awaitility.core.ConditionTimeoutException;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public final class Wait {
    private final Duration _timeout;

    private Wait(Duration timeout) {
        _timeout = timeout;
    }

    public static Wait atMost(Duration timeout) {
        return new Wait(timeout);
    }

    /**
     * Waits for the given Callable to return a value that matches the given Hamcrest matcher.
     * If the timeout expires or the Callable throws an exception, throws an informative failure message.
     *
     * @param valueDescription Human-readable description for the value being supplied, to appear in error messages
     * @param getValue         A Callable to be called (repeatedly) to get a value
     * @param condition        Hamcrest matcher that the value should match
     */
    public <T> void until(String valueDescription, Callable<T> getValue, Matcher<T> condition) {
        Description description = new StringDescription();
        condition.describeTo(description);
        BaseUI.log_Status("Waiting for \"" + valueDescription + "\" to be " + description);

        try {
            Awaitility.await()
                    .atMost(_timeout.toMillis(), TimeUnit.MILLISECONDS)
                    .until(() -> condition.matches(getValue.call()));
        } catch (ConditionTimeoutException e) {
            // Run the comparison again and throw an informative failure message. This will evaluate
            // the matcher one more time; if it actually passes this time, then... well, we pass!
            T finalValue;
            try {
                finalValue = getValue.call();
            } catch (Throwable e1) {
                throw wrapWaitForException(valueDescription, e1);
            }

            String reason = "Timeout expired (" + BaseUI.durationToString(_timeout) + ") while waiting for \"" + valueDescription + "\"";
            // Call Hamcrest's assertThat rather than BaseUI's - we don't need to log on success.
            org.hamcrest.MatcherAssert.assertThat(reason, finalValue, condition);
        } catch (Throwable e) {
            // We land here if getValue threw an exception during the normal "until" wait loop.
            throw wrapWaitForException(valueDescription, e);
        }
    }

    private static RuntimeException wrapWaitForException(String valueDescription, Throwable t) {
        String innerMessage = t.getMessage();
        return new RuntimeException("An error occurred while waiting for \"" + valueDescription + "\"\n" +
                (innerMessage != null ? innerMessage : t.getClass().getName()));
    }
}
