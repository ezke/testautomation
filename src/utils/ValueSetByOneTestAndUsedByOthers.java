package utils;

import org.jetbrains.annotations.NotNull;

/**
 * Represents a non-null value that is initialized by one test, then used by subsequent tests.
 * If the later test tries to read the value, and the first test didn't set it (presumably
 * because the first test failed before it got that far), the getter throws a meaningful error.
 * @param <T> Type of the value.
 */
public class ValueSetByOneTestAndUsedByOthers<T> {
    private String name;
    private T value;

    public ValueSetByOneTestAndUsedByOthers(String name) {
        this.name = name;
    }

    @NotNull
    public T get() {
        if (value == null) {
            throw new TestFailedDueToEarlierTestFailure(
                    "The test could not proceed because it needs the value '" + name +
                            "' from an earlier test, but the earlier test failed.");
        }
        return value;
    }

    public void set(@NotNull T newValue) {
        //noinspection ConstantConditions
        if (newValue == null) {
            throw new BustedTestError("Value '" + name + "' does not allow nulls");
        }
        value = newValue;
    }
}
