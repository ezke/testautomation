package API;

import io.jsonwebtoken.*;

import java.util.Base64;
import java.util.Date;

public class JWT_AuthenticationToken {



	// Sample method to construct a JWT
	//Uses a default JwtBuilder
	public static String create_JWT_AuthenticationToken(String issuer, String key, String roles, String audience, long ttlMillis)
			throws Exception {

		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = base64UrlDecode(key);

		// if it has been specified, let's add the expiration
		long expMillis = nowMillis + ttlMillis;
		Date exp = new Date(expMillis);


		JwtBuilder builder = Jwts.builder().setIssuer(issuer).setAudience(audience).setSubject("U").setIssuedAt(now)
				.setExpiration(exp).claim("name", "U").claim("roles", roles).setHeaderParam("typ", "JWT")
				.signWith(signatureAlgorithm, apiKeySecretBytes);

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	public static byte[] base64UrlDecode(String arg) throws Exception {
		String s = arg;
		s = s.replace("-", "+");
		s = s.replace("_", "/");

		Integer length = s.length() % 4;

		switch (length) {
		case 0:
			break;
		case 2:
			s += "==";
			break;
		case 3:
			s += "=";
			break;
		default:
			throw new Exception("Illegal base64url string!");
		}

//		BASE64Decoder decoder = new BASE64Decoder();
//		byte[] decodedBytes = decoder.decodeBuffer(s);
		
		byte[] decodedBytes = Base64.getDecoder().decode(s);
		
		return decodedBytes;
	}

}
