package API;

import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.testng.IResultMap;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;

import API.REST.REST_Call_Type;
import utils.BaseUI;
import utils.DataBuilder;
import utils.DatabaseConnection;
import utils.TableData;

public class Practitest {

	public static TableData getPractitestProjects() throws Exception {

		String url = "https://api.practitest.com/api/v2/projects.json";

		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
		TableData responseTable = get_responseValues_FromJSON(response, new String[] { "name" });

		return responseTable;
	}
	//
	// public static HashMap<Integer, Integer> return_ActualTestCase_Ids(TableData
	// testCaseTable,
	// ArrayList<Integer> displayIDs) {
	// HashMap<Integer, Integer> actualIDs = new HashMap<Integer, Integer>();
	//
	// for (Integer displayID : displayIDs) {
	// HashMap<String, String> correctRow =
	// testCaseTable.return_Row_BasedOn_1MatchingField("display-id",
	// displayID.toString());
	// actualIDs.add(Integer.parseInt(correctRow.get("id")));
	// }
	//
	// return actualIDs;
	// }

	// Will need to refactor this at some point if I figure out how to go through
	// the results without pulling 3 different collections.
	public static HashMap<Integer, String> return_Unique_PractitestResults(ITestContext testResults) throws Exception {
		HashMap<Integer, String> finalResultList = new HashMap<Integer, String>();

		IResultMap passedTests = testResults.getPassedTests();
		Collection<ITestNGMethod> passedTestsList = passedTests.getAllMethods();
		// We will first populate any passed tests.
		for (ITestNGMethod method : passedTestsList) {
			String testID = method.getMethodName();
			if (testID.startsWith("PT")) {
				testID = testID.substring(2, testID.indexOf("_"));
				if (finalResultList.get(testID) == null) {
					finalResultList.put(Integer.parseInt(testID), "PASS");
				}
			}
		}

		// Then we'll go through skipped tests. Practitest doesn't have a SKIPPED
		// connotation so we'll set these to fail.
		IResultMap skippedTests = testResults.getSkippedTests();
		Collection<ITestNGMethod> skippedTestsList = skippedTests.getAllMethods();
		for (ITestNGMethod method : skippedTestsList) {
			String testID = method.getMethodName();
			if (testID.startsWith("PT")) {
				testID = testID.substring(2, testID.indexOf("_"));
				finalResultList.put(Integer.parseInt(testID), "FAIL");
			}
		}

		// Then we'll go through failed tests.
		IResultMap failedTests = testResults.getFailedTests();
		Collection<ITestNGMethod> failedTestsList = failedTests.getAllMethods();
		for (ITestNGMethod method : failedTestsList) {
			String testID = method.getMethodName();
			if (testID.startsWith("PT")) {
				testID = testID.substring(2, testID.indexOf("_"));
				finalResultList.put(Integer.parseInt(testID), "FAIL");
			}
		}

		return finalResultList;
	}

	// Will need to refactor this at some point if I figure out how to go through
	// the results without pulling 3 different collections.
	public static HashMap<Integer, String> return_Unique_PractitestResults(ArrayList<String> passList,
			ArrayList<String> skipList, ArrayList<String> failList) throws Exception {
		HashMap<Integer, String> finalResultList = new HashMap<Integer, String>();

		// Pass List
		updateResultList(passList, finalResultList, "PASS");

		// Fail List
		updateResultList(failList, finalResultList, "FAIL");

		// Skip List
		updateResultList(skipList, finalResultList, "FAIL");

		// Added logging to help identify where issues are.
		for (Integer key : finalResultList.keySet()) {
			BaseUI.log_Status("Found unique TC " + key.toString());
		}

		return finalResultList;
	}

	private static void updateResultList(ArrayList<String> testList, HashMap<Integer, String> finalResultList,
			String statusToSet) {

		String matchPattern = ".*PT\\d+_.*";

		for (String test : testList) {
			// Handles non-data driven tests that start with PT
			if (test.matches(matchPattern)) {
				Pattern pattern = Pattern.compile("PT(\\d+)_");
				String ptNumber = "";
				Matcher ptNumberFinder = pattern.matcher(test);
				while (ptNumberFinder.find()) {
					ptNumber = ptNumberFinder.group(1);
					finalResultList.put(Integer.parseInt(ptNumber), statusToSet);
				}

			}
		}

	}

	public static void create_Runs(Integer projectID, Integer testSetID, HashMap<Integer, String> finalResults)
			throws Exception {
		// https://api.practitest.com/api/v2/projects/4566/runs.json
		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/runs.json";
		String token = return_Token();

		ArrayList<Integer> testsToGet = new ArrayList<Integer>();
		for (Integer key : finalResults.keySet()) {
			testsToGet.add(key);
		}

		TableData availableInstances = Practitest.get_Instances(projectID, testSetID, testsToGet);
		for (HashMap<String, String> row : availableInstances.data) {
			BaseUI.log_Status("Found Instance " + row.get("id") + " with test id "
					+ row.get("test-id" + " and display id " + row.get("test-display-id")));
		}

		for (Integer key : finalResults.keySet()) {
			HashMap<String, String> correctInstance = availableInstances
					.return_Row_BasedOn_1MatchingField("test-display-id", key.toString());

			String passStatus = finalResults.get(key);
			Integer exitCode = passStatus.equals("PASS") ? 0 : 1;

			String body = "{\"data\": { \"type\": \"instances\", \"attributes\": {\"instance-id\":"
					+ correctInstance.get("id") + ", \"exit-code\":" + exitCode.toString()
					+ ", \"automated-execution-output\": \"THIS IS MY OUTPUT\" }}}";
			String response = REST.http_RESTCall(url, body, REST_Call_Type.Post, token, "Authorization");
			BaseUI.log_Status("Response for Run creation was: " + response);
		}

	}

	public static void get_Instances_ForTests(Integer projectID, Integer setID, ArrayList<Integer> testIDs)
			throws Exception {
		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/instances.json?test-ids=";
		for (int i = 0; i < testIDs.size(); i++) {

			if (i != 0) {
				url += ",";
			}
			url += testIDs.get(i).toString();
		}

		url += "&set-ids=" + setID.toString();

		String token = return_Token();
		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
	}

	private static TableData get_responseValues_FromJSON(String jsonResponse, String[] valuesToGet) throws Exception {
		String response = jsonResponse.substring(jsonResponse.indexOf("[") + 1, jsonResponse.lastIndexOf("]"));
		TableData responseTable = new TableData();

		String[] responseArray = response.split("\\}},");
		for (int i = 0; i < responseArray.length; i++) {
			int countOpeningBrackets = StringUtils.countMatches("responseArray[i]", "{");
			int countClosingBrackets = StringUtils.countMatches("responseArray[i]", "}");
			if (countOpeningBrackets > countClosingBrackets) {
				responseArray[i] = responseArray[i] + "}";
			}
			responseArray[i] = responseArray[i] + "}";
		}

		for (int i = 0; i < responseArray.length; i++) {
			Integer startIndex = responseArray[i].indexOf(":{") + 2;
			String newJSON = responseArray[i].substring(startIndex, responseArray[i].length());
			newJSON = "{" + newJSON;
			if (!newJSON.endsWith("}")) {
				newJSON = newJSON + "}";
			}
			responseArray[i] = responseArray[i] + "}";
			HashMap<String, String> projectRow = new HashMap<String, String>();
			projectRow.put("id", JSON.return_JSON_Value(responseArray[i], "id"));

			for (String value : valuesToGet) {
				projectRow.put(value, JSON.return_JSON_Value(newJSON, value));
			}
			responseTable.data.add(projectRow);
		}

		return responseTable;

	}

	public static Integer get_TestSet_ID_ByProjectID_And_testSetName(Integer projectID, String testSetName)
			throws Exception {
		// testSetName = "DR-8397 - As an unregistered CED desktop user, I would like to
		// receive an error message when I have not properly completed the account
		// activation when redeeming a local offer";
		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/sets.json?name_like="
				+ testSetName;
		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");

		String metaData = response.substring(response.indexOf("\"meta\":") + 7, response.length() - 1);
		int testSetID = -1;
		int totalCount = Integer.parseInt(JSON.return_JSON_Value(metaData, "total-count"));

		if (totalCount != 0) {
			TableData responseTable = get_responseValues_FromJSON(response, new String[] { "name", "display-id" });
			int indexOfTestSetID = responseTable.first_IndexOf("name", testSetName);
			testSetID = Integer.parseInt(responseTable.data.get(indexOfTestSetID).get("id"));
		} 

		return testSetID;
	}

	// currently only returning about 100 TC's, created new solution where we pass
	// in the Display Id's we want to retrieve.
	public static TableData get_Practitest_TestCase_Data(Integer projectID) throws Exception {
		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/tests.json";
		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
		TableData responseTable = get_responseValues_FromJSON(response, new String[] { "name", "display-id" });

		return responseTable;

	}

	public static TableData get_Practitest_TestCase_Data(Integer projectID, ArrayList<Integer> displayIDsToFilterBy)
			throws Exception {

		TableData endTable = new TableData();

		String token = return_Token();
		Integer displayIDCount = 0;

		for (Integer j = 100; j < displayIDsToFilterBy.size() + 100; j += 100) {
			String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString()
					+ "/tests.json?display-ids=";
			while (displayIDCount < j && displayIDCount != displayIDsToFilterBy.size()) {
				url += displayIDsToFilterBy.get(displayIDCount).toString() + ",";
				displayIDCount++;
			}
			// get rid of trailing ","
			url = url.substring(0, url.length() - 1);
			String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
			TableData responseTable = get_responseValues_FromJSON(response, new String[] { "name", "display-id" });
			for (HashMap<String, String> row : responseTable.data) {
				endTable.data.add(row);
			}
			// for (Integer i = 0; i < 100; i++) {
			//
			// if (i != 0 && i != displayIDsToFilterBy.size()) {
			// url += ",";
			// }
			// url += displayIDsToFilterBy.get(i).toString();
			//
			// }
			//
			// String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token,
			// "Authorization");
			// TableData responseTable = get_responseValues_FromJSON(response, new String[]
			// { "name", "display-id" });

		}

		return endTable;

	}

	public static void get_SpecificTestSet(Integer projectID, Integer testSetID) throws Exception {

		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/sets/"
				+ testSetID.toString() + ".json";
		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
	}

	// Pass in the testSetID, we could make an api call again to get the testSetID,
	// but that would be a duplicate call that we'd need to keep making.
	public static void create_TestSet_IfDoesntExist(Integer projectID, Integer testSetID, String suiteName)
			throws Exception {
		// Integer testSetID = get_TestSet_ID_ByProjectID_And_testSetName(projectID,
		// testSetName);
		if (testSetID == -1) {
			String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/sets.json";
			String token = return_Token();

			String body = "{\"data\": { \"type\": \"sets\", \"attributes\": {\"name\": \"" + suiteName
					+ "\", \"priority\": \"highest\"}, \"instances\": {\"test-ids\": [1012726,1013207]} }}";

			String response = REST.http_RESTCall(url, body, REST_Call_Type.Post, token, "Authorization");
		}

	}

	// returns a maximum of 100 Instances
	public static TableData get_Instances(Integer projectID, Integer setID) throws Exception {
		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/instances.json?set-ids="
				+ setID.toString();
		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
		BaseUI.log_Status("Instance Response Body: " + response);
		TableData testData = get_responseValues_FromJSON(response,
				new String[] { "name", "test-id", "test-display-id" });
		return testData;
	}

	public static Integer get_Total_Instance_Count(Integer projectID, Integer setID) throws Exception {
		int totalCount = -1;
		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/instances.json?set-ids="
				+ setID.toString();
		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
		BaseUI.log_Status("Instance Response Body: " + response);

		String metaData = response.substring(response.indexOf("\"meta\":") + 7, response.length() - 1);
		totalCount = Integer.parseInt(JSON.return_JSON_Value(metaData, "total-count"));
		return totalCount;

	}

	// returns a maximum of 100 Instances
	public static TableData get_Instances(Integer projectID, Integer setID, ArrayList<Integer> displayIDs)
			throws Exception {

		TableData endTable = new TableData();

		TableData currentTestData = Practitest.get_Practitest_TestCase_Data(projectID, displayIDs);
		String token = return_Token();
		Integer displayIDCount = 0;

		for (Integer j = 50; j < displayIDs.size() + 50; j += 50) {

			String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString()
					+ "/instances.json?set-ids=" + setID.toString() + "&test-ids=";

			List<String> ids = new ArrayList<>();
			while (displayIDCount < j && displayIDCount < displayIDs.size()) {
				final Integer displayID = displayIDs.get(displayIDCount);
				final HashMap<String, String> row = currentTestData
						.return_Row_BasedOn_1MatchingField("display-id", displayID.toString());
				if (row != null)
					ids.add(row.get("id"));
				else
					BaseUI.log_Warning("Could not find PractiTest ID for display ID " + displayID);
				displayIDCount++;
			}
			url += String.join(",", ids);

			BaseUI.log_Status("Get Instance URL: " + url);

			String response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
			BaseUI.log_Status("Instance Response Body: " + response);
			if (response.contains("\"total-count\":0")) {
				response = REST.http_RESTCall(url, null, REST_Call_Type.Get, token, "Authorization");
			}

			TableData responseTable = get_responseValues_FromJSON(response,
					new String[] { "name", "test-id", "test-display-id" });

			for (HashMap<String, String> row : responseTable.data) {
				endTable.data.add(row);
			}

		}
		return endTable;
	}

	private static String return_Token() {
		// String acctEmail = "joel.caples@wausaufs.com";
		// String acctToken = "0acda02ce1dd6deb0cd1c0ee69e6bb4f43e08670";
		String acctEmail = "dominic.giallombardo@deluxe.com";
		String acctToken = "e842aedaedbf4ded5d1688d845dc140ff226c461";
		byte[] encoding = Base64.getEncoder().encode((acctEmail + ":" + acctToken).getBytes());

		return "Basic " + new String(encoding);
	}

	public static Integer return_ProjectID(ITestContext arg0) throws Exception {
		TableData availableProjects = getPractitestProjects();
		Integer projectID;
		try {
			String localProjectName = arg0.getAllTestMethods()[0].getRealClass().getName().split("\\.")[0];

			HashMap<String, String> projectMappings = DataBuilder
					.GetConfig_DataFromFile("\\src\\testng_Listeners\\practitest_ProjectName_Mappings.csv");

			String projectName = projectMappings.get(localProjectName);

			projectID = Integer
					.parseInt(availableProjects.return_Row_BasedOn_1MatchingField("name", projectName).get("id"));
		} finally {
			availableProjects = null;
		}
		return projectID;
	}

	public static void delete_TestSet(Integer projectID, Integer testSetID) throws Exception {

		String url = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/sets/" + testSetID
				+ ".json";
		String token = return_Token();

		String response = REST.http_RESTCall(url, null, REST_Call_Type.Delete, token, "Authorization");
	}

	// Determine if already existing test set. Then determine if test set matches.
	// Either way we need a test set.
	// Will return the test set id.
	public static Integer create_OR_Update_TestSet(String projectName, Integer projectID, Integer testSetID,
			String suiteName, HashMap<Integer, String> testList) throws Exception {

		if (testSetID != -1) {
			delete_TestSet(projectID, testSetID);
		}
		
		return create_TestSet(projectName, projectID, suiteName, testList);
//		Integer totalInstanceCount = -1;
//		TableData availableInstances = new TableData();
//		if (testSetID == -1) {
//			return create_TestSet(projectName, projectID, suiteName, testList);
//		} else {
//			ArrayList<Integer> testsToGet = new ArrayList<Integer>();
//			for (Integer key : testList.keySet()) {
//				testsToGet.add(key);
//			}
//
//			totalInstanceCount = Practitest.get_Total_Instance_Count(projectID, testSetID);
//
//			// If our available Instances match the Test List in count, we will check to see
//			// if the tests within them match.
//			if (totalInstanceCount == testList.size()) {
//
//				availableInstances = get_Instances(projectID, testSetID, testsToGet);
//				for (HashMap<String, String> row : availableInstances.data) {
//					BaseUI.log_Status("Found Instance " + row.get("id") + " with test id " + row.get("test-id")
//							+ " and display id " + row.get("test-display-id"));
//				}
//
//				Boolean allmatch = true;
//				for (Integer key : testList.keySet()) {
//					if (availableInstances.return_Row_BasedOn_1MatchingField("test-display-id",
//							key.toString()) == null) {
//						allmatch = false;
//						break;
//					}
//				}
//				if (!allmatch) {
//					delete_TestSet(projectID, testSetID);
//					return create_TestSet(projectName, projectID, suiteName, testList);
//				}
//
//			} else {
//				// else we delete the Test Set and recreate it with our test list.
//				delete_TestSet(projectID, testSetID);
//				BaseUI.log_Status("Project ID: " + projectID.toString());
//				BaseUI.log_Status("Suite Name: " + suiteName);
//				return create_TestSet(projectName, projectID, suiteName, testList);
//			}
//		}
//		return testSetID;

	}

	public static Integer create_TestSet(String projectName, Integer projectID, String suiteName,
			HashMap<Integer, String> testSet) throws Exception {
		String updateURL = "https://api.practitest.com/api/v2/projects/" + projectID.toString() + "/sets.json";

		ArrayList<Integer> listOfTCsByDisplayID = new ArrayList<Integer>();
		for (Integer key : testSet.keySet()) {
			listOfTCsByDisplayID.add(key);
		}

		TableData testCaseData = get_Practitest_TestCase_Data(projectID, listOfTCsByDisplayID);

		String token = return_Token();

		String body = "{\"data\": { \"type\": \"sets\", \"attributes\": {\"name\":\"" + suiteName
				+ "\", \"priority\": \"highest\"}, \"instances\": {\"test-ids\": [";
		if (projectName.equals("bankersDashboard") || projectName.equals("dRewards")) {

			body = "{\"data\": { \"type\": \"sets\", \"attributes\": {\"name\":\"" + suiteName
					+ "\", \"priority\": \"highest\",\"version\":\"Sprint Story\", \"custom-fields\": { \"---f-12636\":[\"Story\"], \"---f-12640\":\""
					+ System.getProperty("environment")
					+ "\", \"---f-12634\":[\"All\"]}}, \"instances\": {\"test-ids\": [";

		} else if (projectName.equals("r360")) {

			String versionInfo_ForNotesField = get_R360_VersionInfo();
			
			body = "{\"data\": { \"type\": \"sets\", \"attributes\": {\"name\":\"" + suiteName
					+ "\", \"priority\": \"highest\", \"custom-fields\": { \"---f-25771\":\""
					+ versionInfo_ForNotesField + "\"}}, \"instances\": {\"test-ids\": [";
		}else if(projectName.equals("imageRPS"))
		{
			String verionInfo = imageRPS.pages.GlobalVariables.versionInfo;
			
			body = "{\"data\": { \"type\": \"sets\", \"attributes\": {\"name\":\"" + suiteName
					+ "\", \"priority\": \"highest\", \"custom-fields\": { \"---f-25897\":\""
					+ verionInfo + "\"}}, \"instances\": {\"test-ids\": [";
			
		}

		for (Integer key : testSet.keySet()) {
			HashMap<String, String> rowToCheck = testCaseData.return_Row_BasedOn_1MatchingField("display-id",
					key.toString());
			if (rowToCheck == null) {
				BaseUI.log_Warning("Unable to find id for PT " + key.toString());
			} else {
				String actualID = rowToCheck.get("id");
				body += actualID.toString();
				body += ",";
			}

		}
		// remove trailing ","
		body = body.substring(0, body.length() - 1);

		body += "]} }}";

		BaseUI.log_Status("Creating Test Set. Body: " + body);

		String response = REST.http_RESTCall(updateURL, body, REST_Call_Type.Post, token, "Authorization");
		BaseUI.log_Status("Create Test Set. Response: " + response);
		String idToParse = response.substring(response.indexOf("\"id\":\"") + 6, response.indexOf("\"type\"") - 2);

		return Integer.parseInt(idToParse);

	}

	private static String get_R360_VersionInfo() throws Exception, SQLException {
		String versionInfo_ForNotesField = "";

		TableData r360_VersionData = new TableData();
		String query = "select * from RecHubConfig.Systems";
		DatabaseConnection.dbUrl = r360.pages.GlobalVariables.environment.DBConnection;
		r360_VersionData = DatabaseConnection.runSQLServerQuery(query);

		versionInfo_ForNotesField = "R360 Version Data:";
		for (HashMap<String, String> row : r360_VersionData.data) {
			versionInfo_ForNotesField += "\\n" + row.get("SystemName") + " - version - " + row.get("SystemVersion");

		}

		TableData raam_VersionData = new TableData();
		query = "select * from RaamConfig.Systems";
		DatabaseConnection.dbUrl = r360.pages.GlobalVariables.environment.RAAMDBConnection;
		raam_VersionData = DatabaseConnection.runSQLServerQuery(query);

		versionInfo_ForNotesField += "\\nRAAM Database Version Info:";
		for (HashMap<String, String> row : raam_VersionData.data) {
			versionInfo_ForNotesField += "\\n" + row.get("SystemName") + " - version - " + row.get("SystemVersion");

		}

		TableData framework_VersionData = new TableData();
		query = "SELECT TOP 50 [SystemName]\r\n" + "      ,[DateInstalled]\r\n" + "      ,[SystemVersion]\r\n"
				+ "  FROM [FrameworkConfig].[Systems]";
		DatabaseConnection.dbUrl = r360.pages.GlobalVariables.environment.FrameworkDBConnection;
		framework_VersionData = DatabaseConnection.runSQLServerQuery(query);

		versionInfo_ForNotesField += "\\nFramework Database Version Info:";
		for (HashMap<String, String> row : framework_VersionData.data) {
			versionInfo_ForNotesField += "\\n" + row.get("SystemName") + " - version - " + row.get("SystemVersion");
		}

		return versionInfo_ForNotesField;
	}

}// End of class
