package API;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import utils.TableData;

public class JSON {

	public static TableData return_JSON_Array_AsTableData(String jsonArrayString, String nameOfArray) throws Exception {

		TableData results = new TableData();

		JSONObject initialJSON = new JSONObject(jsonArrayString);
		String result = initialJSON.getString(nameOfArray);

		result = result.substring(1, result.length() - 1);

		List<String> dataReturned = splitUpArray(result);

		for (String row : dataReturned) {
			JSONObject newJSON = new JSONObject(row);
			HashMap<String, String> tableRow = new HashMap<String, String>();

			Iterator<?> keys = newJSON.keys();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				String value = return_JSON_Value(row, key);
				tableRow.put(key, value);
			}
			results.data.add(tableRow);
		}

		return results;
	}

	public static List<String> splitUpArray(String data) {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < data.length(); i++) {
			if ((Character.isDigit(data.charAt(i))) ||
			// Include negative numbers
					(data.charAt(i) == '-') && (i + 1 < data.length() && Character.isDigit(data.charAt(i + 1)))) {
				// Get the number before the comma, unless it's the last number
				int commaIndex = data.indexOf(",", i);
				String number = commaIndex > -1 ? data.substring(i, commaIndex) : data.substring(i);
				list.add(number);
				i += number.length();
			} else if (data.charAt(i) == '{') {
				// Get the group of numbers until you reach the final
				// closing curly brace
				StringBuilder sb = new StringBuilder();
				int openCount = 0;
				int closeCount = 0;
				do {
					if (data.charAt(i) == '{') {
						openCount++;
					} else if (data.charAt(i) == '}') {
						closeCount++;
					}
					sb.append(data.charAt(i));
					i++;
				} while (closeCount < openCount);
				list.add(sb.toString());
			}
		}

		return list;
	}

	public static String return_JSON_Value(String jsonString, String retrieveValue) throws Exception {
		JSONObject jsonMessage = new JSONObject(jsonString);

		Object jsonObject = jsonMessage.get(retrieveValue);

		String jsonValue_ToReturn = "";

		if (jsonObject instanceof String) {
			// do you work here for string like below
			jsonValue_ToReturn = (String) jsonObject;
		} else if (jsonObject instanceof Integer) {
			jsonValue_ToReturn = Integer.toString((Integer) jsonObject);
			// do cell work for Integer
		} else if (jsonObject instanceof Boolean) {
			jsonValue_ToReturn = (Boolean) jsonObject == true ? "true" : "false";
		}

		return jsonValue_ToReturn;
	}
	
}//End of Class
