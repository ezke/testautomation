package API;

import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;
import io.restassured.response.Response;
import io.restassured.http.Method;
import org.bouncycastle.ocsp.Req;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RestAssured {

    public static Response getReponseFromURL() throws Exception {

        RequestSpecification Request = io.restassured.RestAssured.given().relaxedHTTPSValidation();

        Response response = null;
        try {
            response = Request.request(Method.GET);
        } catch (Exception c) {
            String cause = c.getMessage();
            System.out.println("Exception message =>  " + cause);
        }

        return response;
    }

    public static Response postWithFile(File file) throws Exception {

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.

        RequestSpecification Request = io.restassured.RestAssured.given().relaxedHTTPSValidation();

        // Request header
        if (file == null) {
            Request.header(
                    "Content-Type", "multipart/form-data");
            Request.multiPart("ClientProcessorCode", "AutomatedTesterClient");
        } else {
            Request.header(
                    "Content-Type", "multipart/form-data");
            Request.multiPart("Formfile", file).
                    multiPart("ClientProcessorCode", "AutomatedTesterClient");
        }

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        //Response response = Request.request(Method.GET, "/get");
        Response response = null;
        try {
            response = Request.request(Method.POST);
        } catch (Exception c) {
            String cause = c.getMessage();
            System.out.println("Exception message =>  " + cause);
        }

        return response;
    }

    public static Response postSearch(Map<String, String> map, String tokenString, String user, String ArgUser, String pw, String ArgPw, String BankIdString, String ArgBankId, String WorkgroupIdString, String ArgWorkgroupId, String StartDateString, String ArgStartDate, String EndDateString, String ArgEndDate) throws Exception {

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.
        RequestSpecification Request = io.restassured.RestAssured.given().relaxedHTTPSValidation();

        Request.contentType("application/x-www-form-urlencoded; charset=UTF-8");

        // Request header
        Request.header("Authorization", tokenString);

        // These are just hard coded for now.
        Request.formParam(BankIdString, ArgBankId).
                formParam(WorkgroupIdString, ArgWorkgroupId).
                formParam(StartDateString, ArgStartDate).
                formParam(EndDateString, ArgEndDate);

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        Response response = null;
        try {
            response = Request.request(Method.POST);
        } catch (Exception c) {
            String cause = c.getMessage();
            System.out.println("Exception message =>  " + cause);
        }

        return response;
    }

    public static Response getToken(Map<String, String> map, String user, String ArgUser, String pw, String ArgPw) throws Exception {

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.
        RequestSpecification Request = io.restassured.RestAssured.given().relaxedHTTPSValidation();

        // Request header
        Request.header(
                "Content-Type", "application/x-www-form-urlencoded");

        // username and password
        Request.formParam(user, ArgUser).
                formParam(pw, ArgPw);

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        Response response = null;
        try {
            response = Request.request(Method.POST);
        } catch (Exception c) {
            String cause = c.getMessage();
            System.out.println("Exception message =>  " + cause);
        }

        return response;
    }
}// End of Class
