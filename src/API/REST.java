package API;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

@SuppressWarnings("deprecation")
public class REST {

	public enum REST_Call_Type {
		Post("POST"), Delete("DELETE"), Put("PUT"), Get("GET");

		private String value;

		private REST_Call_Type(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	public static String httpPost(String urlStr, String paramName, String token) throws Exception {
		return http_RESTCall(urlStr, paramName, REST_Call_Type.Post, token);
	}

	public static String httpDelete(String urlStr, String paramName, String token) throws Exception {

		return http_RESTCall(urlStr, paramName, REST_Call_Type.Delete, token);
	}

	public static String httpPut(String urlStr, String paramName, String token) throws Exception {

		return http_RESTCall(urlStr, paramName, REST_Call_Type.Put, token);
	}

	public static String httpGet(String urlStr, String paramName, String token) throws Exception {

		return http_RESTCall(urlStr, paramName, REST_Call_Type.Get, token);
	}

	//Defaults to standard authentication token.
	public static String http_RESTCall(String urlStr, String paramName, REST_Call_Type restType,
			String authorizationToken) throws Exception {
		return http_RESTCall(urlStr, paramName, restType, authorizationToken, "Authorization");
		
	}
	
	//paramName would be the body of the rest call to pass.
	public static String http_RESTCall(String urlStr, String paramName, REST_Call_Type restType,
			String authorizationToken, String authenticationType) throws Exception {

		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
		SSLContext.setDefault(ctx);

		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		// String token = JWT_Token.return_JWT_Token();

		conn.setDoOutput(true);
		conn.setRequestMethod(restType.getValue());

		// May need to also pass this in.
		conn.setRequestProperty("Content-Type", "application/json");

		if (authenticationType != null && !authenticationType.equals(""))
			conn.setRequestProperty(authenticationType, authorizationToken);
		// conn.setRequestProperty("X-Rundeck-Auth-Token", authorizationToken);

		// Create the form content
		if (paramName != null) {
			OutputStream out = conn.getOutputStream();
			Writer writer = new OutputStreamWriter(out, "UTF-8");
			writer.write(paramName);

			writer.close();
			out.close();
		}

		BufferedReader rd;

		int responseCode = conn.getResponseCode();
		if (responseCode != 200 && responseCode != 204) {
			
			rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
			// throw new IOException(conn.getResponseMessage());
		} else {
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		}

		// Buffer the result into a string
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		conn.disconnect();
		return sb.toString();
	}
	
	
	private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
	
}// End of Class
