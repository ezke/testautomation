package testng_Listeners;

import com.relevantcodes.extentreports.*;

import utils.Browser;
import utils.ResultWriter;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by andrey.smirnov on 14.06.2016.
 */
public class ExtentReportListener implements IResultListener {

	private ExtentReports reporter = new ExtentReports("build/SimpleReport.html", true, DisplayOrder.NEWEST_FIRST,
			NetworkMode.OFFLINE, Locale.ENGLISH);

	public static ExtentTest testReporter;

	// Needed to add this since every listener in here is activating twice. Only
	// want our code to run once.
	public static Boolean firstRun = true;
	public static Boolean reporterRunning = false;
	ArrayList<String> testList = new ArrayList<String>();
	ArrayList<String> failed_testList = new ArrayList<String>();

	@Override
	public void onTestStart(ITestResult result) {
		// Object[] testParameters = result.getParameters();
		// String testName = result.getMethod().getMethodName();
		// for (Object param : testParameters) {
		// testName = testName + "," + (String) param;
		// }
		//
		// if (!testList.contains(testName)) {
		// testList.add(testName);
		// testReporter = reporter.startTest(testName, "Test Started");
		// testReporter.log(LogStatus.INFO, "Starting test " +
		// result.getMethod().getMethodName());
		// reporterRunning = true;
		// }

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// testReporter.log(LogStatus.PASS, "Test PASSED");
		// reporter.endTest(testReporter);
		// reporter.flush();
		// reporterRunning = false;

	}

	@Override
	public void onFinish(ITestContext context) {
		// reporterRunning = false;
		// reporter.close();
	}

	@Override
	public void onStart(ITestContext arg0) {

		if (firstRun) {
			firstRun = false;
			try {
				Browser.kill_ExcessProcesses();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult result) {

		// Object[] testParameters = result.getParameters();
		// String testName = result.getMethod().getMethodName();
		// for (Object param : testParameters) {
		// testName = testName + "," + (String) param;
		// }
		//
		// if (!failed_testList.contains(testName)) {
		// failed_testList.add(testName);
		// String screenShotPath = "";
		// try {
		// screenShotPath = ResultWriter.saveScreenshot(testName);
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// testReporter.log(LogStatus.FAIL, "Test FAILED");
		// testReporter.log(LogStatus.FAIL, "Snapshot below: " +
		// testReporter.addScreenCapture(screenShotPath));
		//
		// reporter.endTest(testReporter);
		// reporter.flush();
		// reporterRunning = false;
		// }
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// Object[] testParameters = result.getParameters();
		// String testName = result.getMethod().getMethodName();
		// for (Object param : testParameters) {
		// testName = testName + "," + (String) param;
		// }
		//
		// if (!testList.contains(testName)) {
		// testList.add(testName);
		// testReporter = reporter.startTest(result.getMethod().getMethodName(),
		// "Test was SKIPPED.");
		// testReporter.log(LogStatus.SKIP, "Test SKIPPED");
		// reporter.endTest(testReporter);
		// reporter.flush();
		// reporterRunning = false;
		// }
	}

	@Override
	public void onConfigurationFailure(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSkip(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	// Other interface methods
}
