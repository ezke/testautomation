package testng_Listeners;

import com.relevantcodes.extentreports.*;

import API.Practitest;
import API.REST;
import API.REST.REST_Call_Type;
import org.testng.Reporter;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.FileOperations;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.BaseTestMethod;
import org.testng.internal.IResultListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by andrey.smirnov on 14.06.2016.
 */
public class ExtentReportListener2 implements IResultListener {

	// private ExtentReports reporter = new ExtentReports("build/SimpleReport.html",
	// true, DisplayOrder.NEWEST_FIRST,
	// NetworkMode.OFFLINE, Locale.ENGLISH);

	private ExtentReports reporter;
	public static ExtentTest testReporter;
	String folderName = "";
	String path = "";

	public static Boolean reporterRunning = false;
	ArrayList<String> testList = new ArrayList<String>();
	ArrayList<String> testList_Skipped = new ArrayList<String>();
	ArrayList<String> failed_testList = new ArrayList<String>();
	private Boolean setupPass = false;
	private Boolean firstConfig = true;
	private Boolean firstTestClass = true;

	// Hipchat variables
	private Boolean bool_ClearTempFiles = return_TempFilesBool();
	private Boolean bool_send_HipchatNotifications = get_HipChatSetting();
	private final String buildUser = System.getProperty("buildUser");

	// Practitest variables
	private Boolean updatePractitest = update_Practitest();
	private Integer testSetID = -1;
	private HashMap<Integer, String> practitest_FinalResults;
	private String practitestURL = null;
	private Integer projectID;
	private String projectName = "";

	private String startTime;
	private String endTime;
	private String suiteName;
	private String allEncompassing_HipChatToken = "UVvwcNwshmbX8GYnQWXGM4P4pyJDUteEFjvSUffw";

	TableData knownIssues;

	private Boolean return_TempFilesBool() {
		String cleanTemp = System.getProperty("cleanTemp");

		if (cleanTemp != null && cleanTemp.equals("true")) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean update_Practitest() {
		String updatePracti = System.getProperty("updatePractitest");

		if (updatePracti != null && updatePracti.equals("true")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		testReporter.log(LogStatus.PASS, "Test PASSED");
		reporter.endTest(testReporter);
		reporter.flush();
		reporterRunning = false;

	}

	@Override
	public void onFinish(ITestContext context) {
		try {
			if (updatePractitest) {
				practitest_FinalResults = Practitest.return_Unique_PractitestResults(testList, testList_Skipped,
						failed_testList);

				testSetID = Practitest.create_OR_Update_TestSet(projectName, projectID, testSetID, suiteName,
						practitest_FinalResults);

				Practitest.create_Runs(projectID, testSetID, practitest_FinalResults);
				practitestURL = "https://prod.practitest.com/p/" + projectID.toString() + "/sets/"
						+ testSetID.toString() + "/edit";
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Integer passCount = context.getPassedTests().size();
		Integer failCount = context.getFailedTests().size();
		Integer skipCount = context.getSkippedTests().size();

		if (bool_send_HipchatNotifications) {
			try {
				send_HipchatEndNotification(passCount, failCount, skipCount, practitestURL);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (bool_ClearTempFiles) {
			try {
				FileOperations.cleanup_dafTestUser();
				FileOperations.cleanup_TempFolder();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		reporterRunning = false;
		// reporter.close();
	}

	private String get_roomName() {
		String roomName = System.getProperty("roomName");
		if (roomName == null) {
			roomName = "AutoTestRoom";
		}

		return roomName;
	}

	private Boolean get_HipChatSetting() {
		String hipchatNotifications = System.getProperty("sendHipchatNotifications");
		if (hipchatNotifications != null && hipchatNotifications.equals("true")) {
			return true;
		}

		return false;
	}

	@Override
	public void onStart(ITestContext arg0) {

		setupPass = false;
		if (suiteName == null) {
			projectName = arg0.getAllTestMethods()[0].getRealClass().getName().split("\\.")[0];
			suiteName = arg0.getSuite().getName();
		}
		if (firstTestClass) {

			try {
				if (System.getProperty("knownIssueFile") != null) {
					knownIssues = DataBuilder.returnTableData_ForComparison(System.getProperty("knownIssueFile"), "\\,",
							false);
				}
//				knownIssues = DataBuilder.returnTableData_ForComparison(
//						"\\\\plvfile02\\appdev\\upload\\automation\\KnownIssues.csv", "\\,", false);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			if (updatePractitest) {
				try {
					projectID = Practitest.return_ProjectID(arg0);
					testSetID = Practitest.get_TestSet_ID_ByProjectID_And_testSetName(projectID, suiteName);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();

				}
			}

			if (bool_send_HipchatNotifications) {

				send_HipchatStartNotification();
			}

			folderName = "c:/DAF/" + suiteName + "/";

			path = folderName.replace("/", "\\");
			File file = new File(path);
			file.mkdir();
			try {
				FileOperations.cleanup_PriorFiles(path);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			reporter = new ExtentReports(folderName + "SimpleReport.html", true, DisplayOrder.NEWEST_FIRST,
					NetworkMode.OFFLINE, Locale.ENGLISH);

		}

		firstTestClass = false;
	}

	private void send_HipchatStartNotification() {
		startTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy hh:mm:ss a");

		String startMessage = "Started Suite " + suiteName + " at " + startTime;

		String fromUser = "Automation Run";

		if (buildUser != null && !buildUser.equals("$BUILD_USER") && !buildUser.isEmpty()) {
			fromUser = buildUser;
		}

		try {
			String restBody = "{\r\n" + "  \"color\": \"green\", \r\n" + "  \"message\": \"" + startMessage + "\", \r\n"
					+ "  \"notify\": true, \r\n" + "  \"from\": \"" + "Started by " + fromUser + "\", \r\n"
					+ "  \"message_format\": \"text\"\r\n" + "}";

			String url = "https://dlxchat.deluxe.com/v2/room/" + get_roomName() + "/notification";

			// HashMap<String, String> hipChatRoomTokens = DataBuilder
			// .GetConfig_DataFromFile("\\src\\testng_Listeners\\hipChatRoom_Tokens.csv");

			String token = "Bearer " + allEncompassing_HipChatToken;

			String response = REST.http_RESTCall(url, restBody, REST_Call_Type.Post, token, "Authorization");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void send_HipchatEndNotification(Integer passCount, Integer failCount, Integer skipCount,
			String practiTestURL) throws Exception {
		endTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy hh:mm:ss a");

		Double passPercent = 0.0;
		if (passCount != 0) {
			passPercent = 100.0
					- (((double) (skipCount + failCount) * 100.0) / ((double) (passCount + failCount + skipCount)));
		}

		String percentFormatted = BaseUI.convertDouble_ToString_ForCurrency(passPercent);
		percentFormatted += " %";

		String endMessage = "Test Suite " + suiteName + " Ended at " + endTime + "<br>" + "Total Runtime was "
				+ return_DifferenceInTime() + "<br>" + "Tests Passed: " + passCount.toString() + "<br>"
				+ "Tests Failed: " + failCount.toString() + "<br>" + "Tests Skipped: " + skipCount.toString() + "<br>"
				+ "Pass Percentage: " + percentFormatted + "<br>" + "<a href='" + System.getProperty("reportLink")
				+ "'>View More Details</a>";

		if (practiTestURL != null) {
			endMessage += "<br>" + "<a href='" + practiTestURL + "'>Practitest Breakdown</a>";
		}

		String color;
		if (passCount == 0) {
			color = "red";
		} else if (passPercent == 100.0) {
			color = "green";
		} else {
			color = "yellow";
		}

		String restBody = "{\n" + "  \"color\": \"" + color + "\", \n" + "  \"message\": \"" + endMessage + "\", \n"
				+ "  \"notify\": true, \n" + "  \"message_format\": \"html\"\n" + "}";

		String url = "https://dlxchat.deluxe.com/v2/room/" + get_roomName() + "/notification";

		// HashMap<String, String> hipChatRoomTokens = DataBuilder
		// .GetConfig_DataFromFile("\\src\\testng_Listeners\\hipChatRoom_Tokens.csv");

		// String token = "Bearer " + hipChatRoomTokens.get(get_roomName());
		String token = "Bearer " + allEncompassing_HipChatToken;

		String response = REST.http_RESTCall(url, restBody, REST_Call_Type.Post, token, "Authorization");
	}

	private String return_DifferenceInTime() throws Exception {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);

		Date startDate = dateFormat.parse(startTime);
		Date endDate = dateFormat.parse(endTime);
		long diffInMillies = endDate.getTime() - startDate.getTime();

		// TimeUnit minutesTime = TimeUnit.MINUTES;
		// Integer numberOfMinutes = (int) minutesTime.convert(diffInMillies,
		// TimeUnit.MILLISECONDS);
		Integer numberOfSeconds = (int) diffInMillies / 1000;
		Integer numberOfMinutes = numberOfSeconds / 60;
		Integer numberOfHours = numberOfMinutes / 60;
		Integer numberOfMinutesLeft = numberOfMinutes - numberOfHours * 60;
		Integer numberOfSecondsLeft = numberOfSeconds - numberOfMinutesLeft * 60 - numberOfHours * 3600;

		String timeDifference = numberOfHours.toString() + " Hours, " + numberOfMinutesLeft.toString()
				+ " Minutes, and " + numberOfSecondsLeft + " Seconds.";
		return timeDifference;

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestStart(ITestResult result) {

		String testName = returnTestName(result);

		if (!testList.contains(testName)) {
			testList.add(testName);
			testReporter = reporter.startTest(testName, "Test Started");
			testReporter.log(LogStatus.INFO, "Starting test " + result.getMethod().getMethodName());
			reporterRunning = true;
		}

	}

	@Override
	public void onTestFailure(ITestResult result) {

		inject_KnownIssueData(result);
		String testName = returnTestName(result);
		if (!failed_testList.contains(testName)) {

			failed_testList.add(testName);
			takeScreenshot(LogStatus.FAIL, testName);

			testReporter.log(LogStatus.FAIL, "Test FAILED");

			reporter.endTest(testReporter);
			reporter.flush();
			reporterRunning = false;
		}
	}

	private void inject_KnownIssueData(ITestResult result) {
		String testName = result.getMethod().getMethodName();
		if (knownIssues != null) {
			HashMap<String, String> issueData = issueMatchesKnownIssueList(result);

			if (issueData != null) {

				if (!testName.startsWith("KNOWN_ISSUE_")) {
					testName = "KNOWN_ISSUE_" + testName;
					update_TestMethodName(result, testName);
				}

				// result.setAttribute("test-instance-name", testName);
				BaseUI.log_Status("Known Issue: " + issueData.get("Issue Identifier"));
				BaseUI.log_Status("Issue Frequency: " + issueData.get("Issue Frequency"));
				BaseUI.log_Status("Additional Information: " + issueData.get("Issue Message"));
			}
		}

	}

	private void update_TestMethodName(ITestResult result, String textToSet) {
		BaseTestMethod baseTestMethod = (BaseTestMethod) result.getMethod();
		Field f = null;
		try {
			f = baseTestMethod.getClass().getSuperclass().getDeclaredField("m_methodName");
		} catch (NoSuchFieldException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		f.setAccessible(true);
		try {
			f.set(baseTestMethod, textToSet);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private HashMap<String, String> issueMatchesKnownIssueList(ITestResult result) {
		HashMap<String, String> matchingRow = null;

		String resultClass = result.getTestClass().getName();
		String resultMethod = result.getName();
		String failMessage = result.getThrowable().getMessage();

		for (HashMap<String, String> issue : knownIssues.data) {

			//Will also need to add criteria for clients to match (Support DRewards)
			if (matchingParams(result, issue.get("Parameters")) && issue.get("Test Class").equals(resultClass)
					&& issue.get("Test Method").equals(resultMethod)
					&& failMessageMatches(failMessage, issue.get("Expected Message"))
					&& matchingEnvironment(issue.get("Environments"))) {
				return issue;
			}

		}

		return matchingRow;

	}

	private boolean matchingEnvironment(String knownEnvironmentsField) {
		String currentEnvironment = System.getProperty("environment");

		if (currentEnvironment == null) {
			return false;
		}

		String[] environments = knownEnvironmentsField.replace("\"", "").split("\\,");

		return Arrays.asList(environments).contains(currentEnvironment);
	}

	// I want to update this at some point to match based on a regex expression,
	// should have some format that allows us to specify parts of the expected
	// message are not going to match exactly.
	private boolean failMessageMatches(String actualFailMessage, String expectedFailMessage) {
		return actualFailMessage.contains(expectedFailMessage);
	}

	private boolean matchingParams(ITestResult result, String parameterField) {

		parameterField = parameterField.replace("\"", "");
		String[] parameters = new String[0];
		if (!parameterField.trim().isEmpty()) {
			parameters = parameterField.split("\\,");
			for (int i = 0; i < parameters.length; i++) {

			}
		}

		Object[] resultParams = result.getParameters();

		if (parameters.length != resultParams.length) {
			return false;
		} else if (parameters.length > 0) {
			for (int i = 0; i < parameters.length; i++) {
				if (resultParams[i] == null) {
					return false;
				} else {
					String resultParam = resultParams[i].toString();
					if (!resultParam.equals(parameters[i])) {
						return false;
					}
				}
			}
		}

		return true;
	}

	private String returnTestName(ITestResult result) {
		Object[] testParameters = result.getParameters();
		String testName = result.getMethod().getMethodName();
		String testClass = result.getTestClass().getName();
		for (Object param : testParameters) {
			testName = testName + "," + param.toString();
		}

		testName = testClass + "." + testName;

		return testName;
	}

	private void takeScreenshot(LogStatus logStatus, String testName) {
		if (Browser.driver != null) {
			String screenShotPath = "";
			try {
				if(!Browser.currentBrowser.equals("winium")) {
					BaseUI.log_Warning("Current URL: " + Browser.driver.getCurrentUrl());
				}
				screenShotPath = ResultWriter.saveScreenshot(testName, path);
				/*
				This Reporter.log on line 502 is added to:
				Enhance the Extent report listener so that, whenever it takes a screenshot for a failing test,
				it logs the screenshot path to the TestNG log (by calling the Reporter.log method).
				The log line should start with "ScreenshotPath: " so that DafAggregator will be able to recognize it.
				 */
				Reporter.log("ScreenshotPath: " + screenShotPath);
				screenShotPath = ".\\"
						+ screenShotPath.substring(screenShotPath.lastIndexOf("\\"), screenShotPath.length());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException i) {
				BaseUI.log_Warning("Spotted Null reference exception.");
				BaseUI.log_Warning("Test name was: " + testName);
				BaseUI.log_Warning("Path name was: " + path);
			}
			testReporter.log(logStatus, "Snapshot below: " + testReporter.addScreenCapture(screenShotPath));
		}

	}

	@Override
	public void onTestSkipped(ITestResult result) {

		inject_KnownIssueData(result);
		String testName = returnTestName(result);

		if (!testList_Skipped.contains(testName)) {
			testList_Skipped.add(testName);
			// testReporter = reporter.startTest(testName, "Test was SKIPPED.");

			if (!reporterRunning) {
				testReporter = reporter.startTest(testName, "Test was Skipped");
				testReporter.log(LogStatus.INFO, "Test Skipped Name: " + result.getMethod().getMethodName());
				reporterRunning = true;
			}

			takeScreenshot(LogStatus.SKIP, testName);

			testReporter.log(LogStatus.SKIP, "Test SKIPPED");

			reporter.endTest(testReporter);
			reporter.flush();
			reporterRunning = false;
		}
	}

	@Override
	public void onConfigurationFailure(ITestResult arg0) {

		// if(firstConfig)
		// {
		//
		// }

		firstConfig = false;

	}

	@Override
	public void onConfigurationSkip(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSuccess(ITestResult arg0) {
		if (firstConfig) {
			// failed_testList = new ArrayList<String>();
			// testList = new ArrayList<String>();
			setupPass = true;
		}

		firstConfig = false;

	}

}
