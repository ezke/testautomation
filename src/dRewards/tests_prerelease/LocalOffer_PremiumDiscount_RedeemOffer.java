package dRewards.tests_prerelease;

import java.text.MessageFormat;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class LocalOffer_PremiumDiscount_RedeemOffer extends BaseTest {

	// String category= "Dining";
	private String availableOffer = "1";
	private String pointsBefore;
	private String pointsAfter;

	@BeforeClass
	public void setup_method() throws Exception {

		String randomNumber = LocalOffers_LandingPage.return_randomZipCode();
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		Navigation.navigate_LocalOffers_SearchPage();
		String randomDropdown = LocalOffers_LandingPage.return_randomDropdown();
		LocalOffers_LandingPage.localOffer_Search_WithZipcode(randomNumber, randomDropdown);
		Navigation.click_SeeLocalOffer_SearchPageButton();
	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step14_Redeem_PremiumDiscount_Offer() throws Exception {

		pointsBefore = Navigation.returnPoints();
		LocalOffers_LandingPage.redeem_PremiumDiscount_Offer(availableOffer);

		// closing browser because of the print popup
		Browser.closeBrowser();
		Browser.openBrowser(ClientDataRetrieval.url);

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		pointsAfter = Navigation.returnPoints();

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verify_true_AndLog(!pointsBefore.equals(pointsAfter),
					MessageFormat.format("Before points {0} were deducted to {1} after offer redeemed.", pointsBefore,
							pointsAfter),
					MessageFormat.format("Before points {0} were Not deducted to {1} after offer redeemed.",
							pointsBefore, pointsAfter));
		}

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}
