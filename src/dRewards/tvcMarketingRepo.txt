***Login Page ***Launch page
loginPage_easyDeals_UserName	//input[@name='user']
loginPage_easyDeals_Password	//input[@name='password']
loginPage_easyDeals_PasswordMasked	//input[@name='password']
loginPage_easyDeals_SignOnButton	id=submitBtn

***Navigation
nav_SeeAllDepartments_Menu	//a[@class='home']
nav_SeeAll_MenuItem	//a[@id="HL-{0}"]
nav_Logout_Link	id=HL-Logout
nav_AmountOfPoints_Text	//*[@id='header']/p/span
nav_SeeAll_MenuItem	//ul[@id='drop-down-menu']//a[./text() = "{0}"]
nav_SubMenuItem	//ul[@id='drop-down-menu']//li[./h2/a[contains(text(),'{0}')]]/following-sibling::li/a[contains(text(),"{1}")]
nav_Merchandise_DropDown	//select[@id='category']/option[1] | //select[@class='category']/option[1]
nav_GiftCards	id=SL-GiftCards
nav_GiftCards_ViewAllGiftCards	id=SL-GiftCards-ViewAll
nav_WaitElement //ul[@id='drop-down-menu']//a[./text() = "{0}"]

***HomePage LPG sitemedia/widget***
homepage_ClickForDetailsWidget	//img[@class='sidebanner lpg_btn']
homepage_lpgLogo	//img[@class='sidebanner lpg_btn'][@src='/img/Lowest_Price_Guarantee.jpg']
homepage_lpgHeader	//div[@class='lpg_top_content']/h4[./text()='Lowest Price Guarantee']
homepage_lpgMerchandise_Tab	//div[@class='lpg_tab merch active']/h5
***homepage_lpgTravel_TabActive	//div[@class='lpg_tab travel active']
homepage_lpgTravel_TabActive	//div[@class='content travel active']//p
lpgPage_Title	//*[@id='content-main']/div/h4

***HomaPage Site-media Zone***
homePage_SiteMediaIMG_Travel	//div[@id='travel-section']
homePage_SiteMediaSideBanner	//img[@class='sidebanner']

***Local Offer SeachPage***
localOffer_TitleText	//*[@id='contentWrap']//div/h4[contains(text(),'Shop Local. Save Local.')]


***Update Address Book
accnt_addr_DeleteButton	//span[@class='name'][contains(.,'')]/ancestor::div[@class='panel-rounded update-address']//a[@class='btn delete-address']


***Cancel Reservation Page
cancelRes_Points	//tr[contains(@class, 'XtraPts')][./td/div/b[./text()='Rewards Dollars:']]/td[2]
cancelRes_ActualTotal	//td[.//b[./text()='Total Price for Stay after Rewards Dollars:']]/following-sibling::td/b

***Checkout pages
checkout_ErrorMessage_ForDropdown_ByText	//select[@title='{0}*']/following-sibling::label[1]

***homepage_SearchButton	//input[contains(@type,'submit')]

***Merchandise Brand Carousel **** Brand Carousel ***
merch_BrandCarousel_Brand_LandingPage_Header_Title	//*[@id='tabCategoryResults']/h5


***Merchandise Landing Page***
merchandisePage_Header	//div[@id='breadcrumb']/a[contains(text(),'Merchandise')]
merch_merchandise_Category_LandingPage_Header	//div[@id='breadcrumb']/a[@class='selected']

***Gift Cards Result Page***
giftCards_SearchResult_HeaderTitle	//div[@id='tabCategoryResults']/h5
giftCards_SearchResult_link_ByText	//div[@id='tabCategoryResults']/ul/li/a[starts-with(text()," {0}")]
giftCards_CategoryLandingPage_WaitElement	//div[@id='tabCategoryResults']/h5 | //div[@id='tabGiftCards']//a
giftCards_LeftPanelSelectedItem_HeaderText	//div[@id='tabCategoryResults']/h6[contains(text(),"You've Selected:")]
giftCards_SearchResult_linkBoldText_ByText	//div[@id='tabCategoryResults']/ul/li/a/strong[starts-with(text(),"{0}")]


***My Account - Car Rental - View Confirm***
myAccount_carRental_ViewConfirmationPage_ClientLogo	//table[@class="DR-WrapTo100pc"]//tr[1]/td/img[contains(@src,'filogos')]
myAccount_carRental_ViewConfirmationPage_HeaderTitle	//table[@class="DR-WrapTo100pc"]//tr[1]/td/span[contains(text(),'Thank you for your reservation!')]
myAccount_carRental_ViewConfirmationPage_HeaderMessage	//table[@class="DR-WrapTo100pc"]//tr[1]/td/span/following-sibling::p[2]
myAccount_carRental_ViewConfirmationPage_CarRentalCompany_Logo	//td[@class='SplitColumn']/img
myAccount_carRental_ViewConfirmationPage_AvgBaseRate	//td[contains(text(),'Rental Price')]/following-sibling::td
myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars	//td[contains(text(),'Rewards Dollars')]/following-sibling::td
myAccount_carRental_ViewConfirmationPage_TaxesOrFees	//td[contains(text(),'Taxes/Fees')]/following-sibling::td
myAccount_carRental_ViewConfirmationPage_TotalWithTax	//table[@class="DR-WrapTo100pc"]//span[@class='NoMobile']/preceding-sibling::span
myAccount_carRental_ViewConfirmationPage_PickUpDate	(//td[@class="SplitColumn"]//table[@class="DR-WrapTo100pc"]//tr[1]/td[1])[1]
myAccount_carRental_ViewConfirmationPage_ReturnDate	(//td[@class="SplitColumn"]//table[@class="DR-WrapTo100pc"]//tr[1]/td[1])[2]
myAccount_carRental_ViewConfirmationPage_BookingDetail	//td[@class="SplitColumn"]/p
myAccount_carRental_ViewConfirmationPage_SavingsMessage	//td[@class="DR-ReduceFont14a"]
myAccount_carRental_ViewConfirmationPage_PickUpAirportDetails	(//td[@class="SplitColumn"]//table[@class="DR-WrapTo100pc"]//tr[1]/td[1])[1]
myAccount_carRental_ViewConfirmationPage_ReturnAirportDetail	(//td[@class="SplitColumn"]//table[@class="DR-WrapTo100pc"]//tr[1]/td[1])[2]


***Gift Cards Result Page***
giftCards_SearchResult_FeaturedProductDetail_RetailPrice	(//dl[contains(@class,'merchPriceMatrix')]/dd[@class='retail'])[1]
giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars	(//dl[contains(@class,'merchPriceMatrix')]/dd[@class='incentives'])[1]
giftCards_SearchResult_FeaturedProductDetail_YouPay	(//dl[contains(@class,'merchPriceMatrix')]/dd[@class='price'])[1]
giftCards_SearchResult_FeaturedProductDetail_Tooltip	//div[@id='dcs_right']//dl[contains(@class,'merchPriceMatrix')]/dt[3]/a


***Car Rental Search Results Page***
carRentalSelect_GoogleMaps_StreetViewButton //table[@name='mapdisplay']//div[@id='map']//img[contains(@aria-label,'Street View Pegman Control')]

***Homepage Daily Deals***
homepage_DailyDeals_Modal   //ul[@id='dailyDeals']//div[@id='dd-content']
homepage_DailyDeals_SeeMoreDeals_Button //div[@id='dd-content']/center/a[./text()='See Deals']
homepage_DailyDeals_ProductTitle	//div[@class='dd-productname']/a
homepage_DailyDeals_Savings	//ul[@id='dailyDeals']//div[@class='price-matrix']/following-sibling::div
homepage_DailyDeals_Image_SoldOut	//*[@id='dd-content']//a[@class='dailyDealImg']/div[@class='soldOutBanner']
homepage_DailyDeals_EndTime	//span[@class='expires']//span

***Daily Deal Landing Page***
dailyDeals_AllDropdowns //div[@class='selectAtr']
dailyDeals_Dropdown_SelectByIndex   (//div/select)[{0}]