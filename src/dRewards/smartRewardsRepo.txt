***Login Page ***Launch page
loginPage_easyDeals_UserName	id=username
loginPage_easyDeals_Password	id=password
loginPage_easyDeals_PasswordMasked	id=password
loginPage_easyDeals_SignOnButton	//*[@id='submit-btn' or @id='loginBtn'] 


***Navigation
nav_SeeAllDepartments_Menu	//a[@class='home']
***nav_SeeAll_MenuItem	//a[@id="HL-{0}"]
nav_SeeAll_MenuItem	//ul[@id='drop-down-menu']//a[./text() = "{0}"]
nav_SubMenuItem	//ul[@id='drop-down-menu']//li[./h2/a[contains(text(),'{0}')]]/following-sibling::li/a[contains(text(),"{1}")]
Left_Nav_Entertainment	//*[@id='tabGiftCards']/ul/li[2]/a
nav_AmountOfPoints_Text	//*[@id='header']/p/span
nav_Logout_Link	id=HL-Logout
nav_SweepstakesLink	//a[@id='SL-Sweepstakes']
nav_CurrentSweepstakesLink	//a[@id='SL-R-CurrentSweepstakes']
nav_SmartDollars_USD_And_ExtendedPlay	id=HL-R-Rcue
nav_SmartDollars_Only_And_ExtendedPlay	id=HL-R-Rce
nav_Merchandise_DropDown	//select[@id='category']/option[1] | //select[@class='category']/option[1]
nav_GiftCards	id=SL-GiftCards
nav_GiftCards_ViewAllGiftCards	id=SL-GiftCards-ViewAll

***HomePage****
homepage_UserName	//*[@id='header']/p

***HomePage LPG sitemedia/widget***
homepage_LearnMoreLnk	//img[@class='sidebanner lpg_btn']
***homepage_lpgBFooter	//img[@class='sidebanner lpg_btn']
homepage_lpgHeader	//div[@class='lpg_top_content']/h4[./text()='Lowest Price Guarantee']
homepage_lpgMerchandise_Tab	//div[@class='lpg_tab merch active']/h5
homepage_lpgTravel_TabActive	//div[@class='lpg_tab travel active']
homepage_lpgTravel_TabActive	//div[@class='content travel active']//p
lpgPage_Title	//*[@id='content-main']/div/h4

***HomaPage Site-media Zone***
homePage_SiteMediaIMG_Travel	//div[@id='travel-section']
homePage_SiteMediaSideBanner	//div[@class='sideBar']/a[2]/img



***Local Offer SeachPage***
localOffer_TitleText	//*[@id='contentWrap']//div/h4[contains(text(),'Shop Local. Save Local.')]

***Checkout step1
***checkout_ContinueToPaymentInfo	//*[@id='merchContinuePayment']
checkout_ExistingAddress_ShippingAddress1	//span[@class='saved-shippingaddress']
checkout_ExistingAddress_ShippingAddress2	//span[@class='saved-shippingaddress2']
checkout_ExistingAddress_City	//span[@class='saved-shippingcity']
checkout_ExistingAddress_ZipCodeMasked	//span[@class='saved-shippingzipcode-mask']
checkout_ExistingAddress_Country	//span[@class='saved-shippingcountry']
checkout_ExistingAddress_PhoneMasked	//span[@class='saved-shippingphone-mask']
checkout_ExistingAddress_Phone	//span[@class='saved-shippingphone']

checkout_ErrorMessage_ForDropdown_ByText	//select[@title='{0}*']/following-sibling::label[1]

***Cancel Reservation page
cancelRes_Points	//td[.//b[./text()='Smart Dollars:']]/following-sibling::td
cancelRes_ActualTotal	//td[.//b[./text()='Total Price for Stay after Smart Dollars:']]/following-sibling::td/b

***Update Address Book
accnt_addr_DeleteButton	//span[@class='name'][contains(.,'')]/ancestor::div[@class='panel-rounded update-address']//a[@class='btn delete-address']


***Current Sweepstakes Page***
verify_CurrentSweepstakesPageTitle	//a[@id='BC-Sweepstakes']
sweepstakes_Header	//div[@id='featuredSweeps']//h4[1]
verify_SweepstakesNameOnCurrentPage	//div[@id='featuredSweeps']//h4[2]


**My sweeptakes page***
mySweepstakes_CurrentSweepstakes_Name	//*[@id='contest38215']/*[contains(text(),'autotestDAF')]

***Auctions, Order And Shipping Details page
myAuct_OrderAndShip_Header	//*[@id='contentWrap']//div[@class='title']/h4


***Merchandise Brand Carousel **** Brand Carousel ***
merch_BrandCarousel_Brand_LandingPage_Header_Title	//*[@id='tabCategoryResults']/h5

***Merchandise Landing Page***
merchandisePage_Header	//div[@id='breadcrumb']/a[contains(text(),'Merchandise')]
merch_merchandise_Category_LandingPage_Header	//div[@id='breadcrumb']/a[@class='selected']

***Gift Cards Result Page***
giftCards_SearchResult_HeaderTitle	//div[@id='tabCategoryResults']/h5