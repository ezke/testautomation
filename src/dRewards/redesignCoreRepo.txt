***HomePage - Navigation Tabs***
nav_MenuItem	//div[@class='header-nav-program-site-menu']/ul/li/a[contains(text(),'{0}')]
nav_SubMenuItem	//div[@class='header-nav-program-site-menu']/ul/li[a[contains(text(),'{0}')]]//a[contains(text(), "{1}")] | //div[contains(@class,'header-nav-program-site-menu')]/ul/li[a[contains(text(),'{0}')]]//a[contains(text(), "{1}")]

homepage_SearchBox_Displayed //div[@class='input-group']//input[contains(@id,'site-search')]

homePage_ElementToWaitFor   //section[@class='home-page-search']//h1[contains(text(),'Search for Deals Near You')]

***Promo Code Modal***
promoCodeModal_WhereDoIFindTheseLink    //div[@class='Collapsible ']/span[./text()='Where do I find these?']
promoCodeModal_Title    //div[@class='promo-title']
promoCodeModal_EmailIcon    //div[@class='email-icon']/span[@class='email-img']
promoCodeModal_WhereDoIFindThese_Info1  //div[@class='promo-info']/p[contains(text(),'Special Offer Codes can be found in your monthly')]
promoCodeModal_WhereDoIFindThese_Info2  //div[@class='promo-info']/p[contains(text(),"To see if you’re signed up to receive emails")]
promoCodeModal_WhereDoIFindThese_EmailPreferencesLink   //div[@class='promo-info']/p[2]/a
emailPreferencesPage_Header //h2[./text()='Email Preferences']
emailPreferencesPage_LeftNavLinks   //nav[@class='page-navigation left-nav']//li
emailPreferencesPage_SaveChangesBtn //input[@value='Save Changes']
emailPreferencesPage_EmailAddressTextBox    id=emailAddress
emailPreferencesPage_ConfirmEmailAddressTextBox id=confirmEmailAddress

***My Account Page***
account_HeaderText	//h2[./text()='Account Overview'] | //h1[./text()='Account Overview']