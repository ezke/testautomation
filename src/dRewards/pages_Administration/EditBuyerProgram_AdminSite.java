package dRewards.pages_Administration;

import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class EditBuyerProgram_AdminSite {

	public static void UpdateWinLimit_Attribute_13011() throws Exception {

		// Check Auction Win Limit attribute value is greater than 1, if not set
		// greater than 1
		//admin flow is the same setup for all client 
		Navigation_Admin.navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		String attribute = "13011";
		String attributeValue = BaseUI.getTextFromField(
				Locator.lookupElement("clients_ProgramAttributes_Value_ByAttributeID", attribute, null));

		if (Double.parseDouble(attributeValue) < 1000.00) {

			Navigation_Admin.navigate_ExpandCategory("Clients");
			Clients_SearchClients.search_ClientID();
			Clients_SearchClients.click_ClientNumber_FromResult_Option();
			Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
			Clients_SearchClients.update_Attribute(attribute, "1000");

			Clients_SearchClients.search_ClientID_ReloadProgramAttributes();

//			ClientDataRetrieval.set_AttributesReload_URL();
//			Browser.navigateTo(ClientDataRetrieval.attributesReload_URL);
//			BaseUI.verifyElementAppears(Locator.lookupElement("clients_Attributes_Reloaded_Text"));
//
//			Browser.clickBrowserBackButton();
		}
	}

}// End of Class
