package dRewards.pages_Administration;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class ConsumerService {


	public static void click_consumer_Submit_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("consumer_Submit_Button"));
		Thread.sleep(1000);
	}

	public static void search_UserID_AARPRedesign() throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("consumer_EmailScreenName_TextBox"), ClientDataRetrieval.userName);
		click_consumer_Submit_Button();
	}

	public static void search_UserID() throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("consumer_UserIDScreenName_TextBox", ClientDataRetrieval.userName, null), ClientDataRetrieval.userName);
		click_consumer_Submit_Button();
	}
	
	public static void search_UserID(String userName) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("consumer_UserIDScreenName_TextBox", userName, null), userName);
		click_consumer_Submit_Button();
	}
	
	public static void clickUserID_NavigateTo_EditConsumerPage() throws Exception{

		if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			search_UserID_AARPRedesign();
		}else{
			search_UserID();
		}
		BaseUI.click(Locator.lookupElement("consumer_UserID_Link_ByText", ClientDataRetrieval.userName, ClientDataRetrieval.userName.toUpperCase()));
		Thread.sleep(1000);
	}
	
	public static void clickUserID_NavigateTo_EditConsumerPage(String userName) throws Exception{

		 if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			search_UserID_AARPRedesign();
		}else{
			search_UserID(userName);
		}
		BaseUI.click(Locator.lookupElement("consumer_UserID_Link_ByText", userName, userName.toUpperCase()));
		Thread.sleep(1000);
	}
	
	public static void updatePointsOrRewards(String pointsOrRewardsToEnter) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("editConsumer_Points_Textbox"), pointsOrRewardsToEnter);
		click_Consumer_CurrencyInfo_Submit_Button();
	}
	
	public static void click_Consumer_CurrencyInfo_Submit_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("editConsumer_CurrencyInfo_Submit_Button"));
		Thread.sleep(500);
	}
	
	public static void verify_PointsOrRewards_Updated(String expectedPoints){
		String actualPoints = BaseUI.getTextFromInputBox(Locator.lookupElement("editConsumer_Points_Textbox"));
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			actualPoints = actualPoints.split("\\.")[0].replace(",", "");
		}else{
			actualPoints = actualPoints.replace(",", "");
		}
		BaseUI.verifyElementAppears(Locator.lookupElement("editConsumer_Updated_Text"));
		BaseUI.verifyElementHasExpectedText("editConsumer_Updated_Text", "Updated!");
		BaseUI.baseStringCompare("Points", expectedPoints, actualPoints);
	}

	public static void clickLoginAsThisUser_OnEditConsumerPage() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("editConsumer_LoginAsThisUserBtn"), true, 1000);
		BaseUI.waitForElementToBeDisplayed("nav_AccountLink", null, null, 50);
		Thread.sleep(1000);
	}
}
