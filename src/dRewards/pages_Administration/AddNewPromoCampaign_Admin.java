package dRewards.pages_Administration;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class AddNewPromoCampaign_Admin {

	// Adds Generic Promo. Will last from Today's Date to Tomorrow's date.
	public static void add_Promo(String campaignName, String rewardsCashAmount, String transactionDescription,
			String promocode) throws Exception {
		String clientID = Client_Info_Admin.return_ClientId();
		String[] programIDs = Client_Info_Admin.return_ProgramIDs();

		enter_PromoCampaignName(campaignName);
		select_ClientID(clientID);
		for (String program : programIDs) {
			add_programID(program);
		}
		enter_promocode(promocode);
		enter_AmountOfRewardsCash(rewardsCashAmount);
		enter_TransactionDescription(transactionDescription);
		set_PromoCode_For1Day();
		select_MemberStatusValue("Active");

		click_CreatePromo();

	}

	// Adds Generic Promo for free products based on the SKUs we passed in. Will
	// last from Today's Date to Tomorrow's date.
	public static void add_Promo_FreeProducts(String campaignName, String[] productSKUs, String transactionDescription,
			String promocode) throws Exception {
		String clientID = Client_Info_Admin.return_ClientId();
		String[] programIDs = Client_Info_Admin.return_ProgramIDs();

		enter_PromoCampaignName(campaignName);
		select_ClientID(clientID);
		for (String program : programIDs) {
			add_programID(program);
		}

		enter_promocode(promocode);
		enter_Discount_FreeProducts_Offerings(productSKUs);

		enter_TransactionDescription(transactionDescription);
		set_PromoCode_For1Day();
		select_MemberStatusValue("Active");

		click_CreatePromo();
	}
	
	// Adds Generic Promo for free products based on the SKUs we passed in. Will
	// last from Today's Date to Tomorrow's date.
	public static void add_Promo_DiscountProducts(String campaignName, String[] productSKUs, String[] productPrices, String transactionDescription,
			String promocode) throws Exception {
		String clientID = Client_Info_Admin.return_ClientId();
		String[] programIDs = Client_Info_Admin.return_ProgramIDs();

		enter_PromoCampaignName(campaignName);
		select_ClientID(clientID);
		for (String program : programIDs) {
			add_programID(program);
		}

		enter_promocode(promocode);
		enter_Discount_Product_Offerings(productSKUs, productPrices);

		enter_TransactionDescription(transactionDescription);
		set_PromoCode_For1Day();
		select_MemberStatusValue("Active");

		click_CreatePromo();
	}

	public static void verify_AddPromo_Successful() {
		BaseUI.verifyElementAppears(Locator.lookupElement("promo_Message_PromocodeSuccessful"));
	}

	public static void enter_AmountOfRewardsCash(String amount) throws Exception {
		expand_RewardsDeposit();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_AmountOfRewardsCash_Text"), amount);
		Thread.sleep(50);
	}

	public static void enter_Discount_FreeProducts_Offerings(String[] sku_Array) throws Exception {
		expand_DiscountProductOfferings();

		for (Integer i = 1; i <= sku_Array.length; i++) {
			WebElement sku_TextBox = Locator.lookupElement("promo_DiscountProduct_SKU_TextBox_ByIndex", i.toString(),
					null);
			BaseUI.enterText_IntoInputBox(sku_TextBox, sku_Array[i - 1]);
			BaseUI.tabThroughField(sku_TextBox);
			Thread.sleep(2000);

			String amountToDiscount = BaseUI.getTextFromField(
					Locator.lookupElement("promo_DiscountProduct_YouPay_ByIndex", i.toString(), null));
			BaseUI.enterText_IntoInputBox(
					Locator.lookupElement("promo_DiscountProduct_NewDiscount_TextBox_ByIndex", i.toString(), null),
					amountToDiscount);
		}
	}
	
	public static void enter_Discount_Product_Offerings(String[] sku_Array, String[] prices) throws Exception {
		expand_DiscountProductOfferings();

		for (Integer i = 1; i <= sku_Array.length; i++) {
			WebElement sku_TextBox = Locator.lookupElement("promo_DiscountProduct_SKU_TextBox_ByIndex", i.toString(),
					null);
			BaseUI.enterText_IntoInputBox(sku_TextBox, sku_Array[i - 1]);
			BaseUI.tabThroughField(sku_TextBox);
			Thread.sleep(2000);

			BaseUI.enterText_IntoInputBox(
					Locator.lookupElement("promo_DiscountProduct_NewDiscount_TextBox_ByIndex", i.toString(), null),
					prices[i - 1]);
		}
	}
	

	public static void expand_DiscountProductOfferings() throws Exception {
		WebElement DiscountProductOfferings = Locator.lookupElement("promo_DiscountProductOfferings_Expander");
		if (!DiscountProductOfferings.getAttribute("class").equals("section active")) {
			BaseUI.click(DiscountProductOfferings);
			Thread.sleep(1000);
		}
	}

	public static void expand_RewardsDeposit() throws Exception {
		WebElement rewardsDepositExpander = Locator.lookupElement("promo_RewardsDeposit_Expander");
		if (!rewardsDepositExpander.getAttribute("class").equals("section active")) {
			BaseUI.click(rewardsDepositExpander);
			Thread.sleep(1000);
		}
	}
	
	public static void expand_SweepstakesTickets() throws Exception {
		WebElement sweepstakesTicketsExpander = Locator.lookupElement("promo_SweepstakesTickets_Expander");
		if (!sweepstakesTicketsExpander.getAttribute("class").equals("section active")) {
			BaseUI.click(sweepstakesTicketsExpander);
			Thread.sleep(1000);
		}
	}

	public static void click_CreatePromo() throws Exception {
		BaseUI.click(Locator.lookupElement("promo_CreatePromo"));
		BaseUI.waitForElementToBeDisplayed("promo_CreatePromo_ErrorOrSuccessMessage", null, null);
		Thread.sleep(1000);
	}

	public static void select_MemberStatusValue(String valueToSelect) throws Exception {
//		Select dropdown = new Select(Locator.lookupElement("promo_MemberShipStatus_Dropdown"));
//		dropdown.selectByVisibleText(valueToSelect);
//		Thread.sleep(50);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("promo_MemberShipStatus_Dropdown"), valueToSelect);
	}

	public static void set_PromoCode_For1Day() {
		String todayDate = BaseUI.getTodaysDate_AsString();
		String tomorrowDate = BaseUI.getDateAsString_InRelationToTodaysDate(1);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_Promocode_StartDate_Textbox"),
				todayDate);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_Promocode_EndDate_Textbox"),
				tomorrowDate);

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_DateRangeEligibility_From"),
				todayDate);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_DateRangeEligibility_To"),
				tomorrowDate);
	}

	public static void enter_promocode(String promocode) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_Promocode_Textbox"), promocode);
		Thread.sleep(100);
	}

	public static void enter_TransactionDescription(String description) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_TransactionDescription_TextArea"), description);

	}

	public static void add_programID(String programID) throws Exception {
		BaseUI.click(Locator.lookupElement("promo_PromoProductIDs_MultiSelect_OptionByText", programID, null));
		Thread.sleep(100);
		BaseUI.click(Locator.lookupElement("promo_PromoProductIDs_AddIDButton"));
		Thread.sleep(100);

	}

	public static void select_ClientID(String clientID) throws Exception {
		try {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_PromoClientID"), clientID);
			BaseUI.waitForElementToBeDisplayed("promo_PromoClientIDResult_BasedOnTextEntered", clientID, null, 10);
			Thread.sleep(200);
		}catch(Exception e){
			BaseUI.click(Locator.lookupElement("promo_PromoClientID"));
			BaseUI.waitForElementToBeDisplayed("promo_PromoClientIDResult_BasedOnTextEntered", clientID, null, 10);
			Thread.sleep(200);
		}
		BaseUI.click(Locator.lookupElement("promo_PromoClientIDResult_BasedOnTextEntered", clientID, null));
		Thread.sleep(500);
	}

	public static void enter_PromoCampaignName(String campaignName) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_PromoCampaignName_TextBox"), campaignName);
		Thread.sleep(50);
	}
	
	
	// Adds Generic Promo for Sweepstakes we passed in. Will
	// last from Today's Date to Tomorrow's date.
	public static void add_Promo_SweepstakesTickets(String campaignName, String sweepstakesTicketsQuantity, String transactionDescription,
			String promocode, String sweepstakesOption) throws Exception {
		String clientID = Client_Info_Admin.return_ClientId();
		String[] programIDs = Client_Info_Admin.return_ProgramIDs();

		enter_PromoCampaignName(campaignName);
		select_ClientID(clientID);
		for (String program : programIDs) {
			add_programID(program);
		}

		enter_promocode(promocode);
		enter_SweepstakesTicketQuantity(sweepstakesTicketsQuantity);
		add_SweepstakesFromOption(sweepstakesOption);

		enter_TransactionDescription(transactionDescription);
		set_PromoCode_For1Day();
		select_MemberStatusValue("Active");

		click_CreatePromo();
	}
		
		
	public static void enter_SweepstakesTicketQuantity(String ticketQuantity) throws Exception {
		
		expand_SweepstakesTickets();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("promo_SweepstakesTickets_TicketQuantity"), ticketQuantity);
		Thread.sleep(50);
	}
	
	
	public static void add_SweepstakesFromOption(String sweepstakes) throws Exception {
		
		BaseUI.click(Locator.lookupElement("promo_SweepstakesTickets_SelectAvailableSweepstakes", sweepstakes, null));
		Thread.sleep(100);
		
		BaseUI.click(Locator.lookupElement("promo_SweepstakesTickets_AddSweepstakesButton"));
		Thread.sleep(100);

	}	
	
	
	// Adds Generic Promo including Reward Deposit, Discount offerings for both Free products
	// and discounted products and Sweepstakes. Will last from Today's Date to Tomorrow's date.
	public static void add_Promocode_With_MultiOffers(String campaignName, String rewardsCashAmount, String transactionDescription,
			String promocode, String ticketQuantity, String sweepstakes, String[] Sku_value, String[] prices) throws Exception {
		String clientID = Client_Info_Admin.return_ClientId();
		String[] programIDs = Client_Info_Admin.return_ProgramIDs();

		enter_PromoCampaignName(campaignName);
		select_ClientID(clientID);
		for (String program : programIDs) {
			add_programID(program);
		}
		enter_promocode(promocode);
		
		//Rewards Deposit
		enter_AmountOfRewardsCash(rewardsCashAmount);
		
		//Sweepstakes Tickets
		if ((clientID != "215") && (clientID != "630")){
					
			enter_SweepstakesTicketQuantity(ticketQuantity);
			add_SweepstakesFromOption(sweepstakes);
		}
		
		//Discount Offerings- discount product and free product
		enter_Discount_Product_Offerings(Sku_value, prices);
		Thread.sleep(200);
		
		enter_TransactionDescription(transactionDescription);
		set_PromoCode_For1Day();
		select_MemberStatusValue("Active");

		click_CreatePromo();
	}

	// Adds Generic Promo. Will last from Today's Date to Tomorrow's date.
	public static void add_Promo(String campaignName, String rewardsCashAmount, String transactionDescription,
								 String promoCode, String clientID, String programID) throws Exception {
		enter_PromoCampaignName(campaignName);
		select_ClientID(clientID);
		add_programID(programID);

		enter_promocode(promoCode);
		enter_AmountOfRewardsCash(rewardsCashAmount);
		enter_TransactionDescription(transactionDescription);
		set_PromoCode_For1Day();
		select_MemberStatusValue("Active");

		click_CreatePromo();
	}
}
