package dRewards.pages_Administration;

import java.text.MessageFormat;
import org.openqa.selenium.WebElement;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class ProductCatalog_CatalogPage {
	
	public static void search_Product_ByName(String productName) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_Name_TextBox"), productName);
		click_SubmitQuery_Button();
	}
	
	
	public static void search_Product_BySku(String productName) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_ProdCodee_TextBox"), productName);
		click_SubmitQuery_Button();
	}
	
	public static void click_SubmitQuery_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("catalog_SubmitQuery_Button"));
		BaseUI.waitForElementToBeDisplayed("catalog_SubmitQuery_ResultTable", null, null);
		Thread.sleep(500);
	}
	
	public static void click_Update_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("catalog_SubmitQuery_Results_Update_Button"));
		BaseUI.waitForElementToBeDisplayed_ButDontFail("catalog_SubmitQuery_UpdateMessage",null,null, 15);
		Thread.sleep(1000);
	}
	
	public static void click_UpdateByProduct_Button() throws Exception{
		BaseUI.scroll_to_element(Locator.lookupElement("catalog_SubmitQuery_Results_UploadByProduct_Button"));
		BaseUI.click(Locator.lookupElement("catalog_SubmitQuery_Results_UploadByProduct_Button"));
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("catalog_InventoryUpdateSuccessful_WaitElement", null, null, 10);
		//BaseUI.wait_forPageToFinishLoading();
	}

	public static void verify_Textbox_Value(String textBoxIdentifier, String expectedValue) {
		String textBoxValue = BaseUI.getTextFromInputBox(Locator.lookupElement(textBoxIdentifier));
		BaseUI.verify_true_AndLog(textBoxValue.equals(expectedValue),
				MessageFormat.format("Textbox value {0} was a match", textBoxValue),
				MessageFormat.format("Expected value: {0}, Seeing value: {1}.", expectedValue, textBoxValue));
	}
	
	public static void update_Product_Inventory(String quantity) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_SubmitQuery_Results_Inventory_TextBox"), quantity);
		click_Update_Button();
		verify_UpdateCatalog_Text_Appears();
		click_UpdateByProduct_Button();
		verify_CatalogUpload_Text_Appears();
		verify_Textbox_Value("catalog_SubmitQuery_Results_Inventory_TextBox", quantity);
	}
	
	public static void verify_UpdateCatalog_Text_Appears(){
		String pomoCodeText = BaseUI.getTextFromField(Locator.lookupElement("catalog_SubmitQuery_Results_ProductCode_Link"));
		WebElement updateCatalogText = Locator.lookupElement("catalog_SubmitQuery_Results_Text");
		String actualUpdateCatalogText = updateCatalogText.getText();
		String expectedtUpdateCatalogText = ("Successfully updated catalog:")+" "+pomoCodeText;
		BaseUI.baseStringCompare("Product", expectedtUpdateCatalogText, actualUpdateCatalogText);
	}
	
	public static void verify_CatalogUpload_Text_Appears(){
		String pomoCodeText = BaseUI.getTextFromField(Locator.lookupElement("catalog_SubmitQuery_Results_ProductCode_Link"));
		WebElement catalogUploadText = Locator.lookupElement("catalog_SubmitQuery_Results_Text");
		String actualCatalogUploadText = catalogUploadText.getText();
		String expectedtCatalogUploadText = ("CatalogUpload Productcode:")+pomoCodeText+" "+("is Complete");
		BaseUI.baseStringCompare("Product", expectedtCatalogUploadText, actualCatalogUploadText);
	}
	
	public static void click_ViewSpecials_Link() throws Exception{
		BaseUI.click(Locator.lookupElement("catalog_ViewSpecials_Link"));
		Thread.sleep(1000);
	}
	
	//Product Catalog>Catalog page
	public static void select_SuperCategoryID_Dropdown(){
		String superCategoryID = "";
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			superCategoryID = "Citi_DD";
		} else if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){
			superCategoryID = "Allstate_DD";
		} else if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			superCategoryID = "AARP_DD";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			superCategoryID = "DR_DD";
		}
		BaseUI.selectValueFromDropDown(Locator.lookupElement("catalog_ViewSpecials_SubcategoryID_Dropdown"), superCategoryID);
	}
	
	public static void click_BySID_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("catalog_ViewSpecials_BySID_Button"));
		Thread.sleep(1000);
	}
	
	public static void click_ProductSku_ByDailyDealName(String dailyDealName) throws Exception{
		BaseUI.click(Locator.lookupElement("catalog_ViewSpecials_BySID_Results_ProdCode_ByProdName", dailyDealName, null));
		Thread.sleep(1000);
	}
	
	//Product Catalog>Catalog>Edit Product page
	public static void click_SubCategoryID_EditLink() throws Exception{
		
		String superCategoryID = "";
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			superCategoryID = "Citi_DD";
		}else if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){
			superCategoryID = "Allstate_DD";
		} else if (ClientDataRetrieval.isRedesignClient()
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			superCategoryID = "DR_DD";
		}
		
		BaseUI.scroll_to_element(Locator.lookupElement("catalog_EditProduct_BySID",superCategoryID, null ));
		BaseUI.click(Locator.lookupElement("catalog_EditProduct_BySID", superCategoryID, null));
		Thread.sleep(1000);
	}
	
	//Product Catalog>Catalog>Edit Pricing page
	public static void update_SpecialQty(String quantity) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_EditPricing_SpecialQty_TextBox"), quantity);
		Thread.sleep(200);
//		BaseUI.tabThroughField(Locator.lookupElement("catalog_EditPricing_SpecialQty_TextBox"));
//		int quantityInt = Integer.parseInt(quantity);
//
//		if(quantityInt == 1){
//			while (!(BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("catalog_EditPricing_SpecialQtyRemaining_TextBox"), "value").equals("1"))) {
//				BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_EditPricing_SpecialQty_TextBox"),
//						String.valueOf(quantityInt));
//				BaseUI.tabThroughField(Locator.lookupElement("catalog_EditPricing_SpecialQty_TextBox"));
//
//				quantityInt ++;
//			}
//		} else {
//			BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_EditPricing_SpecialQty_TextBox"), quantity);
//		}
	}
	
	
	//Product Catalog>Catalog>Edit Pricing page
	public static void select_Category_Dropdown(String category) throws InterruptedException{
		BaseUI.selectValueFromDropDown(Locator.lookupElement("catalog_EditPricing_Category_Dropdown"), category);
		Thread.sleep(200);
	}
	
	// Product Catalog>Catalog>Edit Pricing page
	public static void select_SubCategoryGroup_Dropdown(String subCategoryGroup) throws InterruptedException {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("catalog_EditPricing_SubcategoryGroup_Dropdown"), subCategoryGroup);
		Thread.sleep(200);
	}
	
	// Product Catalog>Catalog>Edit Pricing page
	public static void select_subCategory_Dropdown(String subCategory) throws InterruptedException {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("catalog_EditPricing_Subcategory_Dropdown"), subCategory);
		Thread.sleep(200);
	}
	
	// Product Catalog>Catalog>Edit Pricing page
	public static void click_EditPricing_Submit_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("catalog_EditPricing_SubmitButton"));
		Thread.sleep(1000);
	}
	
	public static void update_DailyDeal_ProductSku_Quantity(String dailyDealName, String quantity) throws Exception{ //, String inventoryCount
		// Product Catalog>Catalog Page
		Navigation_Admin.navigate_ProductCatalog_Catalog();
		click_ViewSpecials_Link();

		// Product Catalog>ViewSpecial Page
		select_SuperCategoryID_Dropdown();
		click_BySID_Button();
		click_ProductSku_ByDailyDealName(dailyDealName);
		String productSku = BaseUI.getTextFromField(Locator.lookupElement("catalog_EditProduct_Title")).split("\\-")[0];
		
		//Product Catalog>Catalog>Edit Product Page
//		click_SubCategoryID_EditLink();
//
//		// Product Catalog>Catalog>Edit Pricing Page
		//Do not need to update/change the special quantity value
		//for updating daily deal status from Available to In Cart to
		//Sold out. Hence commenting out the lines 189-195.
//		update_SpecialQty(quantity);
//
//		select_Category_Dropdown("Daily Deal");
//		select_SubCategoryGroup_Dropdown("Daily_Deal");
//		select_subCategory_Dropdown("Other");
//		click_EditPricing_Submit_Button();
//		BaseUI.switchToAlert_Accept();

		Navigation_Admin.navigate_ProductCatalog_Catalog();
		search_Product_BySku(productSku);
		// Product Catalog>Catalog Page
		enterInventoryCount(quantity);
		clickUploadButton_ForProductUpload();
		clickCatalogInventoryUpdateCheck_Checkbox();
		click_UpdateByProduct_Button();
		
		// Product Catalog Page
		//Got confirmation from DR team that we do not need to do MiniCatalog upload and MemcacheFlush,
		//When Updating the product Inventory upload. Hence commenting these 2 lines. Tested it and
		//works without these 2 steps.
		//CatalogUpload_And_MemcacheFlush.launch_MiniCatalogUpload();
		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
	}

	public static void clickUploadButton_ForProductUpload() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("catalog_UpdateButton_ForProductInventoryUpdate"));
		BaseUI.waitForElementToBeDisplayed("catalog_InventoryUpdateSuccessful_WaitElement", null, null, 20);
		Thread.sleep(1000);
	}

	public static void clickCatalogInventoryUpdateCheck_Checkbox() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("catalog_UpdateCheckCheckbox_ForProductInventoryUpdate"));
		Thread.sleep(1000);
	}

	public static void enterInventoryCount(String quantity) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("catalog_InventoryTextbox_ForProductInventoryUpdate"), quantity);
		Thread.sleep(200);
	}

	public static String returnInventoryCountValue_ForSelectedProductSku(String productSku) throws Exception {
		Navigation_Admin.navigate_ProductCatalog_Catalog();
		search_Product_BySku(productSku);

		String inventoryCountValue = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("catalog_InventoryTextbox_ForProductInventoryUpdate"));

		return inventoryCountValue;
	}

	public static String returnProductSku_ForSelectedDailyDeal(String dailyDealName) throws Exception {
		Navigation_Admin.navigate_ProductCatalog_Catalog();
		click_ViewSpecials_Link();

		// Product Catalog>ViewSpecial Page
		select_SuperCategoryID_Dropdown();
		click_BySID_Button();
		click_ProductSku_ByDailyDealName(dailyDealName);
		String productSku = BaseUI.getTextFromField(Locator.lookupElement("catalog_EditProduct_Title")).split("\\-")[0];

		return productSku;
	}
}
