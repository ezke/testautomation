package dRewards.pages_Administration;

import utils.BaseUI;
import utils.Locator;

public class Auction_SearchSubAuction_Admin {

	public static void click_Search() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSubAuction_Search_Button"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("searchSubAuction_Row1_ViewLink", null, null, 20);
		Thread.sleep(500);
	}

	public static void click_firstViewLink() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("searchSubAuction_Row1_ViewLink"));
		BaseUI.waitForElementToBeDisplayed("auctionViewDetail_Reload_CoreAuctionsManager_Button", null, null, 20);
		Thread.sleep(500);
	}

	// Searches with default values and clicks View for first result.
	public static void search_WithDefaults_AndClickView() throws Exception {
		BaseUI.checkCheckbox("searchSubAuction_ShowTestDemoAuction_Checkbox");
		click_Search();
		click_firstViewLink();
	}
	
	public static void search_WithAuctionName(String auctionName) throws Exception{
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("searchSubAuction_AuctionName_Textbox"), auctionName);
		click_Search();
	}
	
	public static void click_ViewLink_ByAuctionName(String auctionName) throws Exception{
		BaseUI.click(Locator.lookupElement("searchSubAuction_ViewLink_ByAuctionName", auctionName, null));
		Thread.sleep(500);
	}

	public static void clickSubAuctionsStatus(String status) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("searchSubAuction_SubAuctionStatus"), status);
		BaseUI.waitForElementToBeDisplayed("searchSubAuction_SubAuctionStatus", status, null, 30);
	}

	public static void enterAndSelect_ClientID(String pid) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("searchSubAuction_ClientNameTextBox"), pid);
		Thread.sleep(200);
		BaseUI.click(Locator.lookupRequiredElement("searchSweepstakes_ClientName_FirstOption"));
		BaseUI.waitForElementToBeDisplayed("searchSweepstakes_ClientNameTextBox", pid, null, 30);
	}

	//Redesign only
	public static void searchActiveAuctionsForGivenProgramID(String status, String clientName) throws Exception {
		Navigation_Admin.navigate_Auction_SearchSubAuctions();
		clickSubAuctionsStatus(status);
		enterAndSelect_ClientID(clientName);
		BaseUI.checkCheckbox("searchSubAuction_ShowTestDemoAuction_Checkbox");
		clickSearch();
	}

	//Redesign only
	public static void clickSearch() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSubAuction_Search_Button"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("searchSubAuction_WaitElement", null, null, 20);
		Thread.sleep(500);
	}
}// End of class
