package dRewards.pages_Administration;

import utils.BaseUI;
import utils.Locator;

public class EditSweepstakesProgram_Admin {

    public static void update_EndDate(String dateToEnter) throws Exception {
        enter_EndDate(dateToEnter);
        click_SaveProgramButton();
    }

    public static void enter_EndDate(String dateToEnter) {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_EditSweepstakesEndDate_Textbox"), dateToEnter);
    }

    public static void click_SaveProgramButton() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("sweepstakes_EditSweepstakes_SaveProgram_Button"));
        BaseUI.waitForElementToBeDisplayed("sweepstakes_EditSweepstakes_ErrorOrConfirmMessage", null, null);
        Thread.sleep(500);
    }
}//End of class
