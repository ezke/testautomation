package dRewards.pages_Administration;

import dRewards.pages.GlobalVariables;
import utils.BaseUI;
import utils.Locator;

public class LoginPage_Admin {

	public static void login_Admin() throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("admin_login_UserID"), GlobalVariables.dRewards_Admin_Login);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("admin_login_password"),
				GlobalVariables.dRewards_Admin_Password);
		click_Submit();
	}

	public static void click_Submit() throws Exception {
		BaseUI.click_js(Locator.lookupElement("admin_login_submit"));
		BaseUI.waitForElementToBeDisplayed("admin_nav_NavigationLink_Logo", null, null, 20);
		Thread.sleep(500);
	}

}
