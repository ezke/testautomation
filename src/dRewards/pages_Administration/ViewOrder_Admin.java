package dRewards.pages_Administration;

import utils.BaseUI;
import utils.Locator;

public class ViewOrder_Admin {

	public static void cancelOrder() throws Exception {
		BaseUI.click(Locator.lookupElement("admin_viewOrder_CancelRefundReOrder_Button"));
		Thread.sleep(1000);
	}

}
