package dRewards.pages_Administration;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class CancelRefundReOrder_Admin {

	public static void cancel_Order() throws Exception {
		//WebElement cancelCheckbox = Locator.lookupElement("admin_cancel_Checkbox");

		ArrayList<WebElement> cancelCheckboxes = Locator.lookup_multipleElements("admin_cancel_AllCheckboxes", null,
				null);
		for (WebElement checkbox : cancelCheckboxes) {
			BaseUI.checkCheckbox(checkbox);
		}
		
		select_RefundReason_TestOrder();
		click_CancelRefundButton();
		BaseUI.switchToAlert_GetText_AndAccept();
		Thread.sleep(1000);
	}

	public static void click_CancelRefundButton() throws Exception {
		BaseUI.click(Locator.lookupElement("admin_cancel_CancelRefundButton"));
		Thread.sleep(500);
	}

	public static void select_RefundReason_TestOrder() {
		ArrayList<WebElement> refundReasonDropdowns = Locator
				.lookup_multipleElements("admin_cancel_AllRefundReasonDropdowns", null, null);
		for (WebElement dropdown : refundReasonDropdowns) {
			BaseUI.selectValueFromDropDownByPartialText(dropdown, "Test Order");
		}
	}

}
