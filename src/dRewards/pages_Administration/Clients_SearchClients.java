package dRewards.pages_Administration;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class Clients_SearchClients {

	public static void click_SubmitQuery() throws Exception{
		BaseUI.click(Locator.lookupElement("clients_SubmitQuery_Button"));
		BaseUI.waitForElementToBeDisplayed("clients_ClientNumber_Results_TableIsPresent",
				null,null,30);
		Thread.sleep(500);
	}
	
	public static void click_ReloadProgramAttributes_PredictedSearchClient() throws Exception{
		BaseUI.click(Locator.lookupElement("reloadProgramAttributes_PredictedSearchClient"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(500);
	}

	public static void search_ClientID() throws Exception{
		String clientName_ID = Client_Info_Admin.return_ClientId();
		BaseUI.enterText(Locator.lookupElement("clients_ClientID_TextBox"), clientName_ID);
		Thread.sleep(200);
		click_SubmitQuery();
	}

	//If using this method you need to switch back to original browser instance,
	//so use originalDriver = Browser.driver; statement before calling this method and
	//Browser.driver = originalDriver; statement after this method call to switch back to original
	//browser instance.
	/*Example:
	originalDriver = Browser.driver;
	search_ClientID_ReloadProgramAttributes();
	Browser.driver = originalDriver;
	*/
	public static void search_ClientID_ReloadProgramAttributes() throws Exception {
		Browser.openBrowser(GlobalVariables.reloadProgramAttributes);
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.selectValueFromDropDown(Locator.lookupElement("reloadProgramAttributes_SystemSelect"), "Redesign");
		} else {
			BaseUI.selectValueFromDropDown(Locator.lookupElement("reloadProgramAttributes_SystemSelect"), "Legacy");
		}
		BaseUI.selectValueFromDropDown(Locator.lookupElement("reloadProgramAttributes_EnvironmentSelect"), ClientDataRetrieval.environment);
		String clientName_ID = Client_Info_Admin.return_ClientId();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("reloadProgramAttributes_EnterClientId"), clientName_ID);
		Thread.sleep(200);
		String clientID_Selected = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("reloadProgramAttributes_EnterClientId"));
		//if(!BaseUI.elementAppears(Locator.lookupElement("reloadProgramAttributes_SubmitBtn", clientName_ID, null))) {
		if (!clientID_Selected.contains(clientName_ID)) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("reloadProgramAttributes_EnterClientId"), clientName_ID);
			BaseUI.waitForElementToBeDisplayed("reloadProgramAttributes_SubmitBtn", clientName_ID, null, 30);
			Thread.sleep(200);
		} else {
			BaseUI.waitForElementToBeDisplayed("reloadProgramAttributes_SubmitBtn", clientName_ID, null, 30);
		}
		click_ReloadProgramAttributes_PredictedSearchClient();
		BaseUI.click(Locator.lookupElement("reloadProgramAttributes_SubmitBtn"));
		BaseUI.waitForElementToBeDisplayed("reloadProgramAttributes_MsgPresent", null, null, 30);
		if (ClientDataRetrieval.isRedesignClient()){ String environmentLowerCase = ClientDataRetrieval.environment.toLowerCase();
			String text = BaseUI.getTextFromField(Locator.lookupElement("reloadProgramAttributes_MsgPresent"));
			BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupElement("reloadProgramAttributes_MsgPresent"),
				"Program Attributes are reloaded successfully for " + clientName_ID + " in " + environmentLowerCase + " environment.");

	}
		Browser.close_CurrentBrowserWindow();
	}

	public static void click_ClientNumber_FromResult_Option() throws Exception{
		String clientName_ID = Client_Info_Admin.return_ClientId();
		BaseUI.click(Locator.lookupElement("clients_ClientNumber_Results_ByText", clientName_ID, null));
		BaseUI.waitForElementToBeDisplayed("clients_EditBuyer_ProgramName_ElementIsPresent", null, null, 20);	
		Thread.sleep(500);
	}
	
	public static void click_ProgramID(String[]programIDs) throws Exception{
		if(ClientDataRetrieval.client_Name.equals(client_Designation.Allstate)){
			click_ProgramName_ByProgramID("9419");
		} else if(ClientDataRetrieval.client_Name.equals(client_Designation.AllstateCash)){
			click_ProgramName_ByProgramID("9414");
		} else if(ClientDataRetrieval.client_Name.equals(client_Designation.RedesignCore)) {
			click_ProgramName_ByProgramID("9474");
		} else if(ClientDataRetrieval.client_Name.equals(client_Designation.McAfee)) {
			click_ProgramName_ByProgramID("9604");
		} else{
			click_ProgramName_ByProgramID(programIDs[0]);
		}
	}
	
	public static void click_ProgramName_ByProgramID(String programID) throws Exception {
		BaseUI.click(Locator.lookupElement("clients_EditBuyer_ProgramName_ByText", programID, null));
		BaseUI.waitForElementToNOTBeDisplayed("clients_EditBuyer_ProgramName_ByText",null,null,10);
		Thread.sleep(200);
	}
	
	public static void click_EditAllProgramAttributes() throws Exception {
		BaseUI.click(Locator.lookupElement("clients_EditAllProgramAttributes_Button"));
		BaseUI.waitForElementToBeDisplayed("clients_EditAllProgramAttributes_ExistingAttributes", null, null, 15);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	public static void click_EditAllProgramAttributes_UpdateButton() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("clients_EditAllProgram_Update_Button"));
		BaseUI.click_js(Locator.lookupRequiredElement("clients_EditAllProgram_Update_Button"));
		BaseUI.waitForElementToBeDisplayed("clients_EditAllProgram_UpdateSuccessful_Text",
				null, null, 30);
		Thread.sleep(200);
	}
	
	public static void update_Attribute(String attribute, String valueToEnter) throws Exception {
		click_EditAllProgramAttributes();
		BaseUI.scroll_to_element(Locator.lookupElement("clients_EditAllProgram_AttributesTextBox_ByAttributeID",
				attribute, null));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("clients_EditAllProgram_AttributesTextBox_ByAttributeID",
				attribute, null), valueToEnter);
		click_EditAllProgramAttributes_UpdateButton();
	}
}
