package dRewards.pages_Administration;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class Auction_ViewDetail_Admin {

	// Reloads all of the Auction stuff for the first row.
	public static void click_ReloadAuction_Managers() throws Exception {

		// if(Navigation_Admin.navigate_ClickNavLink("Create Auction");
		// (Browser.openBrowser(GlobalVariables.dRewards_Admin_ReloadAuctionQA_URL));
		
		
		BaseUI.click(Locator.lookupElement("auctionViewDetail_Reload_CoreAuctionsManager_Button"));
		BaseUI.waitForElementToBeDisplayed("auctionViewDetails_After_Reload_PageIsPresent", null, null, 20);
		Thread.sleep(500);
		Browser.clickBrowserBackButton();

		BaseUI.click(Locator.lookupElement("auctionViewDetail_Reload_CitiAndAARP_AuctionManager_Button"));
		BaseUI.waitForElementToBeDisplayed("auctionViewDetails_After_Reload_PageIsPresent", null, null, 20);
		Thread.sleep(500);
		Browser.clickBrowserBackButton();

		BaseUI.click(Locator.lookupElement("auctionViewDetail_Row1_Reload_Subauction_Link"));
		BaseUI.waitForElementToBeDisplayed("auctionViewDetails_After_Reload_PageIsPresent", null, null, 20);
		Thread.sleep(500);
		Browser.clickBrowserBackButton();
	}

}// End of class
