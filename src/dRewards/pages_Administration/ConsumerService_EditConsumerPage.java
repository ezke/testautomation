package dRewards.pages_Administration;

import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class ConsumerService_EditConsumerPage {

    public static void enterAddress_IntoAddress1Textbox(String address) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("editConsumer_Address_TextBox"), address);
        Thread.sleep(500);
    }

    public static void selectStateFromDropdownOption(String state) throws Exception {
        BaseUI.selectValueFromDropDown(Locator.lookupElement("editConsumer_State_Dropdown"), state);
        Thread.sleep(500);
    }

    public static void enterZipCode(String zipCode) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("editConsumer_ZipCode_TextBox"), zipCode);
        Thread.sleep(500);
    }

    public static void enterCityName(String city) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("editConsumer_City_TextBox"), city);
        Thread.sleep(500);
    }

    public static void selectCountryFromDropdownOption(String country) throws Exception {
        BaseUI.selectValueFromDropDown(Locator.lookupElement("editConsumer_Country_Dropdown"), country);
        Thread.sleep(500);
    }

    public static void enterUserAddressDetails(String address, String city,
                                               String state, String zipcode, String country) throws Exception {
        enterAddress_IntoAddress1Textbox(address);
        enterCityName(city);
        enterZipCode(zipcode);
        selectStateFromDropdownOption(state);
        selectCountryFromDropdownOption(country);
    }

    public static void click_EditConsumer_Submit_Button() throws Exception{
        BaseUI.click(Locator.lookupElement("editConsumer_SubmitButton"));
        BaseUI.waitForElementToBeDisplayed("editConsumer_Updated_StatusText", null, null, 30);
        Thread.sleep(1000);
    }

    public static void updateUser_AddressDetails(String address, String city,
                                                 String state, String zipcode, String country) throws Exception {
        enterUserAddressDetails(address, city, state, zipcode, country);
        click_EditConsumer_Submit_Button();
    }

    public static void updateUserAddress_ForAARPRedesign(String username, String address, String city,
                                                         String state, String zipcode, String country) throws Exception {
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
        if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AARPRedesign) &&
                BaseUI.elementExists("loginPage_NucaptchaIsPresent", null, null)) {
            //Navigate to Admin site and login with valid credentials
            ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();

            //Click on Consumer Service option in left navigation panel
            //Search user details by either entering username/user email
            Navigation_Admin.navigate_ExpandCategory("Consumer Service");
            ConsumerService.search_UserID(username);
            ConsumerService.clickUserID_NavigateTo_EditConsumerPage(username);
        } else {
            //Navigate to Admin site and login with valid credentials
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();

            //Click on Consumer Service option in left navigation panel
            //Search user details by either entering username/user email
            Navigation_Admin.navigate_ExpandCategory("Consumer Service");
            if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AARPRedesign)) {
                ConsumerService.search_UserID_AARPRedesign();
            } else {
                ConsumerService.search_UserID();
            }
            ConsumerService.clickUserID_NavigateTo_EditConsumerPage();
        }

        //Enter User's valid address details
        ConsumerService_EditConsumerPage.updateUser_AddressDetails(address, city, state, zipcode, country);
    }
}
