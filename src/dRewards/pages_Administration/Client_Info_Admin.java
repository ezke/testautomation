package dRewards.pages_Administration;

import java.util.ArrayList;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;

//Return info based on the current client.
public class Client_Info_Admin {
	public static String return_ClientId() {
		String clientID = "";

		if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			clientID = "611";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			clientID = "626";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			clientID = "435";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			clientID = "215";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			clientID = "630";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)){
			clientID = "633";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)){
			clientID = "632";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)){
			clientID = "635";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.McAfee)){
			clientID = "739";
	}
		return clientID;
	}

	// Returns String array since multiple program ID's can be selected.
	public static String[] return_ProgramIDs() {
		ArrayList<String> programIDList = new ArrayList<String>();

		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			programIDList.add("9362");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate)) {
			programIDList.add("9419");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			programIDList.add("9414");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			programIDList.add("9255");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			programIDList.add("9485");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			programIDList.add("9452");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			programIDList.add("9461");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			programIDList.add("9457");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			programIDList.add("9474");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			programIDList.add("9363");
		}else if (ClientDataRetrieval.client_Matches(client_Designation.McAfee)) {
			programIDList.add("9604");
		}

		String[] programIDs = new String[programIDList.size()];

		return programIDList.toArray(programIDs);
	}

}// End of Class
