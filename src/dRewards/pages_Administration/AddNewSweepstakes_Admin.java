package dRewards.pages_Administration;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class AddNewSweepstakes_Admin {

    public static String entered_sweepstakesName;
    private static String randomNum = BaseUI.random_NumberAsString(1, 50);

    public static void Add_NewSweepstakes(String sweepstakesName, String sweepstakesDescription,
            /* SweepstakesTypes sweepstakesType, */ String[] programIDs, Boolean isFeaturedSweepstakes,
                                          String startDate, String endDate, String drawingDate, String displayEndDate, String ticketCost,
                                          String cumulativeCap, String docID, String productSku, String position, String dCash, String quantity,
                                          String numDays_Claim, String ticketQty, String ticketLimit, String option) throws Exception {

        String clientName_ID = Client_Info_Admin.return_ClientId();
        // programIDs = Client_Info_Admin.return_ProgramIDs();

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"), (sweepstakesName));
        entered_sweepstakesName = BaseUI
                .getTextFromInputBox(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"));

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDescription_Textbox"), sweepstakesDescription);
        // BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesType_Dropdown"),
        // sweepstakesType.getValue());
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_FeaturedSweepstakes_Dropdown"),
                isFeaturedSweepstakes == true ? "True" : "False");

        enter_AndSelect_ClientName(clientName_ID);

        for (String programID : programIDs) {
            add_clientID_ProgramID(programID);
        }
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesStartDate_Textbox"), startDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Textbox"), endDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDrawingDate_Textbox"), drawingDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDisplayEndDate_Textbox"), displayEndDate);

        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTicketCost_Textbox"), ticketCost);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesCumulativeCap_Textbox"),
                cumulativeCap);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTermDoc_Textbox"), docID);

        add_AddPrizeRecord_Details(productSku, position, dCash, quantity, numDays_Claim);
        click_AddPrizeRecordDialog_CloseButton();

        add_EditActionRecord_Details(ticketQty, ticketLimit, option);
        click_EditActionRecordDialog_CloseButton();
        click_CreateProgram();
        // Thread.sleep(500);
    }

    public static void add_CurrentSweepstakes(String sweepstakesName, String sweepstakesDescription,
            /* SweepstakesTypes sweepstakesType, */ String[] programIDs, Boolean isFeaturedSweepstakes,
                                              String startDate, String endDate, String drawingDate, String displayEndDate, String ticketCost,
                                              String cumulativeCap, String docID, String productSku, String position, String dCash, String quantity,
                                              String numDays_Claim, String ticketQty, String ticketLimit, String option) throws Exception {

        String clientName_ID = Client_Info_Admin.return_ClientId();
        // programIDs = Client_Info_Admin.return_ProgramIDs();

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"), (sweepstakesName));
        entered_sweepstakesName = BaseUI
                .getTextFromInputBox(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"));

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDescription_Textbox"), sweepstakesDescription);
        // BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesType_Dropdown"),
        // sweepstakesType.getValue());
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_FeaturedSweepstakes_Dropdown"),
                isFeaturedSweepstakes == false ? "False" : "True");

        enter_AndSelect_ClientName(clientName_ID);


        for (String programID : programIDs) {
            add_clientID_ProgramID(programID);
        }
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesStartDate_Textbox"), startDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Textbox"), endDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDrawingDate_Textbox"), drawingDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDisplayEndDate_Textbox"), displayEndDate);

        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTicketCost_Textbox"), ticketCost);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesCumulativeCap_Textbox"),
                cumulativeCap);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTermDoc_Textbox"), docID);

        add_AddPrizeRecord_Details(productSku, position, dCash, quantity, numDays_Claim);
        click_AddPrizeRecordDialog_CloseButton();

        add_EditActionRecord_Details(ticketQty, ticketLimit, option);
        click_EditActionRecordDialog_CloseButton();
        click_CreateProgram();
    }

    public static void enter_AndSelect_ClientName(String clientName_ID) throws Exception {
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesClientName_Textbox"), clientName_ID);
        try {
            BaseUI.waitForElementToBeDisplayed("sweepstakes_SweepstakesClientName_ResultOption", clientName_ID, null, 10);
            Thread.sleep(200);
            //Add Retry logic in case the control didn't work the first time.
        } catch (Exception e) {
            BaseUI.click(Locator.lookupElement("sweepstakes_SweepstakesClientName_Textbox"));
            BaseUI.waitForElementToBeDisplayed("sweepstakes_SweepstakesClientName_ResultOption", clientName_ID, null, 10);
            Thread.sleep(200);
        }

        BaseUI.click(Locator.lookupElement("sweepstakes_SweepstakesClientName_ResultOption", clientName_ID, null));
        BaseUI.waitForElementToNOTBeDisplayed("sweepstakes_SweepstakesClientName_ResultOption", clientName_ID, null);
        Thread.sleep(500);

    }

    public static void add_CurrentSweepstakes_WithLogin_And_Ticket_Option(String sweepstakesName,
                                                                          String sweepstakesDescription,
            /* SweepstakesTypes sweepstakesType, */ String[] programIDs, Boolean isFeaturedSweepstakes,
                                                                          String startDate, String endDate, String drawingDate, String displayEndDate, String ticketCost,
                                                                          String cumulativeCap, String docID, String productSku, String position, String dCash, String quantity,
                                                                          String numDays_Claim, String ticketQty, String ticketLimit, String ticketOption, String loginOption)
            throws Exception {

        String clientName_ID = Client_Info_Admin.return_ClientId();
        // programIDs = Client_Info_Admin.return_ProgramIDs();

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"), (sweepstakesName));
        entered_sweepstakesName = BaseUI
                .getTextFromInputBox(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"));

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDescription_Textbox"), sweepstakesDescription);
        // BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesType_Dropdown"),
        // sweepstakesType.getValue());
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_FeaturedSweepstakes_Dropdown"),
                isFeaturedSweepstakes == false ? "False" : "True");

        enter_AndSelect_ClientName(clientName_ID);

        for (String programID : programIDs) {
            add_clientID_ProgramID(programID);
        }
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesStartDate_Textbox"), startDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Textbox"), endDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDrawingDate_Textbox"), drawingDate);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDisplayEndDate_Textbox"), displayEndDate);

        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTicketCost_Textbox"), ticketCost);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesCumulativeCap_Textbox"),
                cumulativeCap);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTermDoc_Textbox"), docID);

        add_AddPrizeRecord_Details(productSku, position, dCash, quantity, numDays_Claim);
        click_AddPrizeRecordDialog_CloseButton();

        add_EditActionRecord_Details(ticketQty, ticketLimit, ticketOption);
        click_EditActionRecordDialog_CloseButton();
        add_EditActionRecord_Details(ticketQty, ticketLimit, loginOption);
        click_EditActionRecordDialog_CloseButton();
        click_CreateProgram();
    }

    public static void add_UpcomingSweepstakes(Integer hourToStart, Integer minuteToStart, Integer hourToLast, Integer minutesToLast, String sweepstakesName,
                                               String sweepstakesDescription, String[] programIDs, Boolean isFeaturedSweepstakes, String startDate,
                                               String endDate, String drawingDate, String displayEndDate, String ticketCost, String cumulativeCap,
                                               String docID, String productSku, String position, String dCash, String quantity, String numDays_Claim,
                                               String ticketQty, String ticketLimit, String option) throws Exception {

        String clientName_ID = Client_Info_Admin.return_ClientId();

        String startHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(hourToStart, "h:00 a");
        String startMinute = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minuteToStart, "m");
        String endHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(hourToLast, "h:00 a");
        String endMinutes = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "m");
        String drawingDateHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(hourToLast, "h:00 a");
        String drawingDateMinute = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "m");
        String displayEndHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(hourToLast, "h:00 a");
        String displayEndMinutes = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "m");

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"), (sweepstakesName));
        entered_sweepstakesName = BaseUI
                .getTextFromInputBox(Locator.lookupElement("sweepstakes_SweepstakesName_Textbox"));

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDescription_Textbox"), sweepstakesDescription);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_FeaturedSweepstakes_Dropdown"),
                isFeaturedSweepstakes == false ? "False" : "True");

        enter_AndSelect_ClientName(clientName_ID);

        for (String programID : programIDs) {
            add_clientID_ProgramID(programID);
        }
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesStartDate_Textbox"), startDate);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesStartDate_Hour_Dropdown"), startHour);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesStartDate_Minute_TextBox"), startMinute);

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Textbox"), endDate);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Hour_Dropdown"), endHour);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Minute_TextBox"), endMinutes);

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDrawingDate_Textbox"), drawingDate);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesDrawingDate_Hour_Dropdown"), drawingDateHour);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDrawingDate_Minute_Textbox"), drawingDateMinute);

        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDisplayEndDate_Textbox"), displayEndDate);
        BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesDisplayEndDate_Hour_Dropdown"), displayEndHour);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesDisplayEndDate_Minute_Textbox"), displayEndMinutes);

        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTicketCost_Textbox"), ticketCost);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesCumulativeCap_Textbox"), cumulativeCap);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_SweepstakesTermDoc_Textbox"), docID);

        add_AddPrizeRecord_Details(productSku, position, dCash, quantity, numDays_Claim);
        click_AddPrizeRecordDialog_CloseButton();

        add_EditActionRecord_Details(ticketQty, ticketLimit, option);
        click_EditActionRecordDialog_CloseButton();
        click_CreateProgram();
    }

    public static void edit_Sweepstakes_ForPastSweepstakes(Integer hourToLast, Integer minutesToLast, String sweepstakesName, String endDate) throws Exception {

        String endHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(hourToLast, "h:00 a");
        String endMinutes = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "m");

        String[] programIDs = Client_Info_Admin.return_ProgramIDs();

        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .verify_Sweepstakes_ResultDisplays_ByName(sweepstakesName);

        ArrayList<WebElement> editButtonsMatchingCriteria = Locator.lookup_multipleElements(
                "searchSweepstakes_EditSweepstakes_Button_BySweepstakesName_AndPID", sweepstakesName, programIDs[0]);

        for (int i = 0; i < editButtonsMatchingCriteria.size(); i++) {

            if (!BaseUI.elementAppears(Locator.lookupElement("searchSweepstakes_Header"))) {
                Navigation_Admin.navigate_Sweepstakes_SearchProgram();
                Sweepstakes_SearchSweepstakesProgramSearch_Admin
                        .verify_Sweepstakes_ResultDisplays_ByName(sweepstakesName);
            }

            Sweepstakes_SearchSweepstakesProgramSearch_Admin.click_EditButton_BySweepstakesName_AndPID(entered_sweepstakesName, programIDs[0], i);

            BaseUI.enterText(Locator.lookupElement("sweepstakes_EditSweepstakesEndDate_Textbox"), endDate);
            BaseUI.selectValueFromDropDown(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Hour_Dropdown"), endHour);
            BaseUI.enterText(Locator.lookupElement("sweepstakes_SweepstakesEndDate_Minute_TextBox"), endMinutes);

            click_SaveProgram();

            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Sweepstakes_ResultDisplays_ByName(sweepstakesName);
            Sweepstakes_SearchSweepstakesProgramSearch_Admin
                    .verify_Sweepstakes_Status_AwardsPending_BySweepstakesName(entered_sweepstakesName);

            // launch memcache flush to update sweepstakes
            CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
        }
    }



    public static void edit_Sweepstakes_ExpireTheSweepstake(String sweepstakesName) throws Exception {

        String[] programIDs = Client_Info_Admin.return_ProgramIDs();

        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .verify_Sweepstakes_ResultDisplays_ByName(sweepstakesName);

        ArrayList<WebElement> editButtonsMatchingCriteria = Locator.lookup_multipleElements(
                "searchSweepstakes_EditSweepstakes_Button_BySweepstakesName_AndPID", sweepstakesName, programIDs[0]);

        for (int i = 0; i < editButtonsMatchingCriteria.size(); i++) {

            if (!BaseUI.elementAppears(Locator.lookupElement("searchSweepstakes_Header"))) {
                Navigation_Admin.navigate_Sweepstakes_SearchProgram();
                Sweepstakes_SearchSweepstakesProgramSearch_Admin
                        .verify_Sweepstakes_ResultDisplays_ByName(sweepstakesName);
            }

            Sweepstakes_SearchSweepstakesProgramSearch_Admin.click_EditButton_BySweepstakesName_AndPID(entered_sweepstakesName, programIDs[0], i);

            String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(-2, "MM/dd/yyyy");

            String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(-1, "MM/dd/yyyy");

            BaseUI.enterText(Locator.lookupRequiredElement("sweepstakes_EditSweepstakesStartDate_Textbox"), startDate);
            BaseUI.enterText(Locator.lookupRequiredElement("sweepstakes_EditSweepstakesEndDate_Textbox"), endDate);
            BaseUI.enterText(Locator.lookupRequiredElement("sweepstakes_EditSweepstakesDrawingDate_Textbox"), endDate);
            BaseUI.enterText(Locator.lookupRequiredElement("sweepstakes_EditSweepstakesDisplayEndDate_Textbox"), endDate);


            click_SaveProgram();

            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Sweepstakes_ResultDisplays_ByName(sweepstakesName);
            Sweepstakes_SearchSweepstakesProgramSearch_Admin
                    .verify_Sweepstakes_Status_AwardsPending_BySweepstakesName(entered_sweepstakesName);

            // launch memcache flush to update sweepstakes
            CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
        }
    }




    public enum SweepstakesTypes {
        Standard("1 - Standard"), Sports("2 - Sports");

        private String value;

        private SweepstakesTypes(final String val) {
            value = val;
        }

        public String getValue() {
            return value;
        }
    }

    public static void add_clientID_ProgramID(String programID) throws Exception {
        BaseUI.scroll_to_element(
                Locator.lookupElement("sweepstakes_SweepstakesProgramID_ProgramListOption", programID, null));
        BaseUI.click(Locator.lookupElement("sweepstakes_SweepstakesProgramID_ProgramListOption", programID, null));
        Thread.sleep(200);
        click_Add_ProgramID_Button(programID);
    }

    public static void click_Add_ProgramID_Button(String programID) throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_Sweepstakes_SelectProgramID_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("sweepstakes_SweepstakesProgramID_ProgramListOption", programID, null);
        Thread.sleep(500);
    }

    public static void click_CreateProgram() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_CreateProgram_button"));
        Thread.sleep(2000);
    }

    public static void click_SaveProgram() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_SaveProgram_Button"));
        Thread.sleep(2000);
    }

    public static void click_AddPrize_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_AddPrize_Button"));
        BaseUI.waitForElementToBeDisplayed("sweepstakes_AddPrize_SKU_Textbox", null, null);
        Thread.sleep(1500);
    }

    public static void add_AddPrizeRecord_Details(String productSku, String position, String dCash, String quantity,
                                                  String numDays_Claim) throws Exception {

        click_AddPrize_Button();
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_AddPrize_SKU_Textbox"), productSku);
        Thread.sleep(300);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_AddPrize_Position_Textbox"), position);
        Thread.sleep(300);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_AddPrize_DCashAmount_Textbox"), dCash);
        Thread.sleep(300);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_AddPrize_Quantity_Textbox"), quantity);
        Thread.sleep(300);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("sweepstakes_AddPrize_NumberOfDays_Claim_Textbox"), numDays_Claim);
        Thread.sleep(300);

        click_AddPrizeRecordDialog_SubmitButton();
    }

    public static void click_AddPrizeRecordDialog_SubmitButton() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_AddPrize_Submit_button"));
        BaseUI.waitForElementToContain_AttributeMatch("sweepstakes_AddPrize_SKU_Textbox", null, null, "value", "", 5);
        Thread.sleep(500);
    }

    public static void click_AddPrizeRecordDialog_CancelButton() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_AddPrize_Cancel_button"));
        Thread.sleep(2000);
    }

    public static void click_AddPrizeRecordDialog_CloseButton() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_AddPrize_Close_button"));
        BaseUI.waitForElementToNOTBeDisplayed("sweepstakes_AddPrize_SKU_Textbox", null, null);
        Thread.sleep(500);
    }

    public static void click_EditSelected_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_EditSelected_button"));
        BaseUI.waitForElementToBeDisplayed("sweepstakes_EditSelected_TicketLimit", null, null);
        Thread.sleep(500);
    }

    public static void click_DeletedSelected_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_DeleteSelected_button"));
        Thread.sleep(2000);
    }

    public static void click_TicketsGivenForTransaction_Option(String option) throws Exception {

        List<WebElement> allTicketsTransactionOption = Locator
                .lookup_multipleElements("sweepstakes_EditTicketsGivenForTransaction_option", null, null);

        for (WebElement TicketsGivenForTransactionOption : allTicketsTransactionOption) {
            if (TicketsGivenForTransactionOption.getText().equals(option)) {
                TicketsGivenForTransactionOption.click();
                Thread.sleep(200);
                break;
            }
        }
    }

    public static void add_EditActionRecord_Details(String ticketQty, String ticketLimit, String option)
            throws Exception {

        click_TicketsGivenForTransaction_Option(option);
        click_EditSelected_Button();
        BaseUI.enterText(Locator.lookupElement("sweepstakes_EditSelected_TicketQuantity"), ticketQty);
        BaseUI.enterText(Locator.lookupElement("sweepstakes_EditSelected_TicketLimit"), ticketLimit);
        Thread.sleep(200);

        click_EditActionRecordDialog_SubmitButton();
    }

    public static void click_EditActionRecordDialog_SubmitButton() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_EditActionRecord_Submit_button"));
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void click_EditActionRecordDialog_CancelButton() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_EditActionRecord_Cancel_button"));
        Thread.sleep(2000);
    }

    public static void click_EditActionRecordDialog_CloseButton() throws Exception {
        BaseUI.click(Locator.lookupElement("sweepstakes_EditActionRecord_Close_button"));
        Thread.sleep(2000);
    }
}
