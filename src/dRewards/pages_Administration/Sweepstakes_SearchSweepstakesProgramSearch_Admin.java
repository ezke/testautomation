package dRewards.pages_Administration;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

import java.util.ArrayList;

public class Sweepstakes_SearchSweepstakesProgramSearch_Admin {
	
	public static void click_SearchButton() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_SearchButton"));
		BaseUI.wait_forPageToFinishLoading();
		//BaseUI.waitForElementToBeDisplayed("searchSweepstakes_SearchResult_DrawingDate_HeaderElement", null, null);
		Thread.sleep(1000);
	}
	

	public static void search_For_AndThen_UpdateEndTime(String sweepstakesName, String endDate) throws Exception {
		enter_SweepstakesName_Textbox(sweepstakesName);
		click_SearchButton();
		String[] programIDs = Client_Info_Admin.return_ProgramIDs();

		Sweepstakes_SearchSweepstakesProgramSearch_Admin.click_EditButton_BySweepstakesName_AndPID(sweepstakesName, programIDs[0], 0);
		EditSweepstakesProgram_Admin.update_EndDate(endDate);
	}

	public static void click_FirstEditLink_FromResultsTable() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("searchSweepstakes_SearchResults_FirstEditLink", null, null));
		BaseUI.waitForElementToBeDisplayed("sweepstakes_EditSweepstakes_Title", null, null);
		Thread.sleep(500);
	}


	public static void click_ResetButton() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_ResetButton"));
		Thread.sleep(500);
	}

	
	public static void enter_SweepstakesName_Textbox(String sweepstakesName) throws Exception {
		BaseUI.enterText(Locator.lookupElement("searchSweepstakes_SweepstakesName"), sweepstakesName);
		Thread.sleep(200);
	}
	
	
	public static void verify_Navigated_ToSearchSweepstakes_Page() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_SearchSweepstakes_Title"));
		BaseUI.verifyElementHasExpectedText("searchSweepstakes_SearchSweepstakes_Title", "Sweepstakes Program Search");
	}
	
	
	public static void verify_SweepstakesName_SearchResult(String sweepstakesName) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_SearchResult_BySweepstakesName", sweepstakesName, null));
		String entered_SweepstakesName = BaseUI.getTextFromField(Locator.lookupElement("searchSweepstakes_SearchResult_BySweepstakesName", sweepstakesName, null));
		BaseUI.baseStringCompare("searchSweepstakes_verify_SweepstakesName_displays_SearchResult", sweepstakesName, entered_SweepstakesName);
	}
	
	
	public static void verify_NewSweepstakes_Status_Inactive() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_verify_NewSweepstakes_Status_Inactive"));
		BaseUI.verifyElementHasExpectedPartialText("searchSweepstakes_verify_NewSweepstakes_Status_Inactive", "Inactive");
	}
	
	public static void verify_NewSweepstakes_Status_Inactive_BySweepstakesName(String sweepstakes) throws Exception {
		BaseUI.verifyElementAppears(Locator
				.lookupElement("searchSweepstakes_Sweepstakes_Status_Inactive_BySweepstakesName", sweepstakes, null));
		BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupElement(
				"searchSweepstakes_Sweepstakes_Status_Inactive_BySweepstakesName", sweepstakes, null), "Inactive");
	}
	
	
	public static void verify_NewSweepstakes_Status_Active() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_verify_NewSweepstakes_Status_Active"));
		BaseUI.verifyElementHasExpectedPartialText("searchSweepstakes_verify_NewSweepstakes_Status_Active", "Active");
	}
	
	public static void verify_NewSweepstakes_Status_Active_BySweepstakesName(String sweepstakes) throws Exception {
		BaseUI.verifyElementAppears(Locator
				.lookupElement("searchSweepstakes_Sweepstakes_Status_Active_BySweepstakesName", sweepstakes, null));
		BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupElement(
				"searchSweepstakes_Sweepstakes_Status_Active_BySweepstakesName", sweepstakes, null), "Active");
	}
	
	public static void verify_NewSweepstakes_Status_PendingStart() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_verify_NewSweepstakes_Status_PendingStart"));
		BaseUI.verifyElementHasExpectedPartialText("searchSweepstakes_verify_NewSweepstakes_Status_PendingStart", "Pending Start");
	}
	
	public static void verify_NewSweepstakes_Status_PendingStart_BySweepstakesName(String sweepstakes)
			throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_Sweepstakes_Status_PendingStart_BySweepstakesName", 
				sweepstakes, null));
		BaseUI.verifyElementHasExpectedPartialTextByElement(
				Locator.lookupElement("searchSweepstakes_Sweepstakes_Status_PendingStart_BySweepstakesName",
						sweepstakes, null),"Pending Start");
	}
	
	
	public static void verify_Sweepstakes_Status_AwardsPending() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("searchSweepstakes_verify_Sweepstakes_Status_AwardsPending"));
		BaseUI.verifyElementHasExpectedPartialText("searchSweepstakes_verify_Sweepstakes_Status_AwardsPending", "Awards Pending");
	}
	
	public static void verify_Sweepstakes_Status_AwardsPending_BySweepstakesName(String sweepstakes) throws Exception {
		BaseUI.verifyElementAppears(
				Locator.lookupElement("searchSweepstakes_verify_Sweepstakes_Status_AwardsPending", sweepstakes, null));
		BaseUI.verifyElementHasExpectedPartialTextByElement(
				Locator.lookupElement("searchSweepstakes_verify_Sweepstakes_Status_AwardsPending", sweepstakes, null),
				"Awards Pending");
	}
	
	public static void click_ActivateButton() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_click_ActivateSweepstakes_button"));
		Thread.sleep(500);
	}
	
	public static void click_ActivateButton_BySweepstakesName(String sweepstakes) throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_ActivateSweepstakes_Button_BySweepstakesName", sweepstakes, null));
		BaseUI.waitForElementToNOTBeDisplayed("searchSweepstakes_ActivateSweepstakes_Button_BySweepstakesName",sweepstakes,null);
		Thread.sleep(500);
	}

	
	public static void click_ViewButton() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_click_ViewSweepstakes_button"));
		Thread.sleep(500);
	}
	
	
	public static void click_EditButton() throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_click_EditSweepstakes_button"));
		Thread.sleep(500);
	}
	
	public static void click_EditButton_BySweepstakesName(String sweepstakes) throws Exception {
		BaseUI.click(Locator.lookupElement("searchSweepstakes_EditSweepstakes_Button_BySweepstakesName", sweepstakes, null));
		Thread.sleep(500);
	}

	public static void click_EditButton_BySweepstakesName_AndPID(String sweepstakes, String pid, int index) throws Exception {
		ArrayList<WebElement> editButtonsMatchingCriteria = Locator.lookup_multipleElements(
				"searchSweepstakes_EditSweepstakes_Button_BySweepstakesName_AndPID", sweepstakes, pid);

		BaseUI.click(editButtonsMatchingCriteria.get(index));
		BaseUI.waitForElementToBeDisplayed("sweepstakes_EditSweepstakes_Title",null,null);
		Thread.sleep(500);
	}
	
	public static void validate_Sweepstakes_IsActivated() throws Exception {
		enter_SweepstakesName_Textbox(AddNewSweepstakes_Admin.entered_sweepstakesName);
		click_SearchButton();
		verify_SweepstakesName_SearchResult(AddNewSweepstakes_Admin.entered_sweepstakesName);
		verify_NewSweepstakes_Status_Active_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
	}
	
	
	public static void validate_NewSweepstakes_IsDisplayed_Result() throws Exception {
		enter_SweepstakesName_Textbox(AddNewSweepstakes_Admin.entered_sweepstakesName);
		click_SearchButton();
		verify_SweepstakesName_SearchResult(AddNewSweepstakes_Admin.entered_sweepstakesName);
		verify_NewSweepstakes_Status_Inactive_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
	}
	
	public static void validate_UpcomingSweepstakes_IsPendingStart() throws Exception {
		enter_SweepstakesName_Textbox(AddNewSweepstakes_Admin.entered_sweepstakesName);
		click_SearchButton();
		verify_SweepstakesName_SearchResult(AddNewSweepstakes_Admin.entered_sweepstakesName);
		verify_NewSweepstakes_Status_PendingStart_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
	}
	
	public static void verify_Sweepstakes_ResultDisplays_ByName(String sweepstakesName) throws Exception{
		enter_SweepstakesName_Textbox(sweepstakesName);
		click_SearchButton();
		verify_SweepstakesName_SearchResult(sweepstakesName);
	}

	public static void selectSweepstakesStatus_dropDown(String status) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("searchSweepstakes_SweepstakesStatusDropDown"), status);
		BaseUI.waitForElementToBeDisplayed("searchSweepstakes_SweepstakesStatusDropDown", status, null, 30);
	}

	public static void enterAndSelect_ClientID(String clientName) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("searchSweepstakes_ClientNameTextBox"), clientName);
		Thread.sleep(200);
		BaseUI.click(Locator.lookupRequiredElement("searchSweepstakes_ClientName_FirstOption"));
		BaseUI.waitForElementToBeDisplayed("searchSweepstakes_ClientNameTextBox", clientName, null, 30);
	}

	public static void searchActiveSweepstakesForGivenProgramID(String status, String clientName, String[] programIDs) throws Exception {
		Navigation_Admin.navigate_Sweepstakes_SearchProgram();
		selectSweepstakesStatus_dropDown(status);
		enterAndSelect_ClientID(clientName);

		for (String programID : programIDs) {
			selectProgramID(programID);
		}
		click_SearchButton();
	}

	public static void selectProgramID(String pid) throws Exception {
		BaseUI.selectValueFromDropDownByPartialText(Locator.lookupRequiredElement("searchSweepstakes_ProgramIDOption"), pid);
		Thread.sleep(300);
	}
}
