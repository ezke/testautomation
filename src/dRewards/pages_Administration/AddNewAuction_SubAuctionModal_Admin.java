package dRewards.pages_Administration;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class AddNewAuction_SubAuctionModal_Admin {

	public static void add_subauction(String[] programIDs, String startDate, String endDate, String endHour,
			String endMinutes) throws Exception {
		AddNewAuction_Admin.click_Add_subauction();

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_StartDate_TextBox"), startDate);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_StartDate_Time_Dropdown"), "12 AM");
		Thread.sleep(300);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_DisplayStartDate_TextBox"), startDate);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_EndDate_Textbox"), endDate);
		Thread.sleep(300);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_EndHour_Dropdown"), endHour);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_EndMinutes_Dropdown"), endMinutes);
		Thread.sleep(300);

		for (String programID : programIDs) {
			add_programID(programID);
		}

		click_Add_subauction();
		click_Close_Subauction();
	}

	public static void add_subauction(String startDate, String endDate, String endHour, String endMinutes)
			throws Exception {
		add_subauction(Client_Info_Admin.return_ProgramIDs(), startDate, endDate, endHour, endMinutes);
	}

	public static void add_subauction(Integer minutesToLast) throws Exception {
		String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "hh:00 a");
		endHour = endHour == "12:00 AM" ? "12 AM" : endHour;

		String endMinutes = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "mm");

		add_subauction(Client_Info_Admin.return_ProgramIDs(), startDate, endDate, endHour, endMinutes);
	}
	
	public static void add_Subauction_WithMinAndMax_Points(String[] programIDs, String startDate, String endDate, String endHour,
			String endMinutes, String minPoints, String maxPoints) throws Exception{
		
		AddNewAuction_Admin.click_Add_subauction();

		BaseUI.enterText(Locator.lookupElement("subAuction_StartDate_TextBox"), startDate);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_StartDate_Time_Dropdown"), "12 AM");
		BaseUI.enterText(Locator.lookupElement("subAuction_EndDate_Textbox"), endDate);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_MinPoints_TextBox"), minPoints);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_MaxPoints_TextBox"), maxPoints);
		BaseUI.enterText(Locator.lookupElement("subAuction_DisplayStartDate_TextBox"), startDate);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_EndHour_Dropdown"), endHour);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_EndMinutes_Dropdown"), endMinutes);

		for (String programID : programIDs) {
			add_programID(programID);
		}

		click_Add_subauction();
		click_Close_Subauction();
	}
	
	public static void add_Subauction_WithMinAndMax_Points(Integer minutesToLast, String minPoints, String maxPoints) throws Exception{
		String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "hh:00 a");
		endHour = endHour == "12:00 AM" ? "12 AM" : endHour;

		String endMinutes = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "mm");
		add_Subauction_WithMinAndMax_Points(Client_Info_Admin.return_ProgramIDs(), startDate, endDate, endHour, endMinutes, minPoints, maxPoints);
	}

	public static void add_programID(String programID) throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("subAuction_ProgramID_AllProgramID_Option", programID, null));
		BaseUI.click(Locator.lookupElement("subAuction_ProgramID_AllProgramID_Option", programID, null));
		//no need for element to wait as is just selecting an element
		Thread.sleep(1000);
		click_Add_ProgramID_Button();
	}

	public static void click_Add_ProgramID_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("subAuction_add_Program_Button"));
		//no need for wait logic as is just clicking to add a pid bu nothing change in page or redirection
		Thread.sleep(300);
	}

	public static void click_Close_Subauction() throws Exception {
		if (BaseUI.elementAppears(Locator.lookupElement("subAuction_Close_Button"))) {
			BaseUI.click(Locator.lookupElement("subAuction_Close_Button"));
			BaseUI.waitForElementToNOTBeDisplayed("subAuction_ProgramID_AllProgramID_Option",null,null,10);
			Thread.sleep(1000);
		}
	}

	public static void click_Add_subauction() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupElement("subAuction_add_Subauction_SubmitButton"));
			BaseUI.waitForElementToNOTBeDisplayed("subAuction_ProgramID_AllProgramID_Option",null,null,10);
			BaseUI.wait_forPageToFinishLoading();
		} else {
			BaseUI.click(Locator.lookupRequiredElement("subAuction_add_Subauction_SubmitButton"));
			BaseUI.wait_forPageToFinishLoading();
			BaseUI.waitForElementToNOTBeDisplayed("subAuction_ProgramID_AllProgramID_Option",null,null,30);
			Thread.sleep(2000);
		}
	}

	public static void add_subauction(String[] programIDs, String startDate, String endDate, String startDate_Feature,
			String endDate_Feature, String endHour, String endMinutes) throws Exception {
		// AddNewAuction_Admin.click_Add_subauction();

		BaseUI.getDateAsString_InRelationToTodaysDate(0);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_Feature_DisplayStartDate_TextBox"), startDate_Feature);
		BaseUI.getDateAsString_InRelationToTodaysDate(0);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_Feature_EndDate_Textbox"), endDate_Feature);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_StartDate_TextBox"), startDate);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_DisplayStartDate_TextBox"), startDate);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_StartDate_Time_Dropdown"), "12 AM");
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("subAuction_EndDate_Textbox"), endDate);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_EndHour_Dropdown"), endHour);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("subAuction_EndMinutes_Dropdown"), endMinutes);

		for (String programID : programIDs) {
			add_programID(programID);
		}

		click_Add_subauction();
		click_Close_Subauction();
	}

	public static void add_subauction(String startDate1, String endDate1, String startDate, String endDate,
			String endHour, String endMinutes) throws Exception {
		add_subauction(Client_Info_Admin.return_ProgramIDs(), startDate1, endDate1, startDate, endDate, endHour,
				endMinutes);
	}

	public static void add_subauction_WithFeatured_StartAndEndDates(Integer minutesToLast) throws Exception {
		String startDate1 = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endDate1 = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endHour = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "hh:00 a");
		endHour = endHour == "12:00 AM" ? "12 AM" : endHour;

		String endMinutes = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesToLast, "mm");

		// String startDate1 = null;
		// String endDate1 = null;
		add_subauction(Client_Info_Admin.return_ProgramIDs(), startDate1, startDate, endDate1, endDate, endHour,
				endMinutes);
	}
}
