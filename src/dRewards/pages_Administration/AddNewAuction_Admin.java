package dRewards.pages_Administration;

import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class AddNewAuction_Admin {

	public static void Add_New_Auction(String auctionName, RedemptionTypes redempType, Double startingBid,
			Integer extendTime, String productSku, AuctionTypes auctionType, Boolean isDemo) throws Exception {

		String clientName_ID = Client_Info_Admin.return_ClientId();

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_AuctionName_TextBox"), auctionName);
		Thread.sleep(200);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_ClientName_TextBox"), clientName_ID);
		String clientID_Selected = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("auction_ClientName_TextBox"));
		if(!(clientID_Selected.contains(clientName_ID))) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_ClientName_TextBox"), clientName_ID);
			BaseUI.waitForElementToBeDisplayed("auction_ClientName_ResultOption", clientName_ID, null, 20);
			Thread.sleep(300);
		} else {
			BaseUI.waitForElementToBeDisplayed("auction_ClientName_ResultOption", clientName_ID, null, 20);
			Thread.sleep(300);
		}
		BaseUI.click(Locator.lookupElement("auction_ClientName_ResultOption", clientName_ID, null));
		BaseUI.waitForElementToNOTBeDisplayed("auction_ClientName_ResultOption", clientName_ID, null, 20);
		//BaseUI.waitForElementToBeDisplayed("auction_RedemptionType_Dropdown", null, null, 20);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("auction_RedemptionType_Dropdown"), redempType.getValue());
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_StartingBid_TextBox"), startingBid.toString());
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_ExtendedTime_TextBox"), extendTime.toString());
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_ProductSku_TextBox"), productSku);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("auction_AuctionType_Dropdown"), auctionType.getValue());
		BaseUI.selectValueFromDropDown(Locator.lookupElement("auction_IsDemoAuction_Dropdown"),
				isDemo == true ? "True" : "False");
	}

	public static void Add_New_Auction_DualCurrency(String auctionName, RedemptionTypes redempType, Double startingBid,
			Double usd_Amount, Integer extendTime, String productSku, AuctionTypes auctionType, Boolean isDemo)
			throws Exception {
		Add_New_Auction(auctionName, redempType, startingBid, extendTime, productSku, auctionType, isDemo);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("auction_StartingBid_USDAmount_Textbox"), usd_Amount.toString());
	}

	public enum RedemptionTypes {
		DualCurrency("Dual Currency"), FullRedemption("Full Redemption");

		private String value;
		private RedemptionTypes(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	public enum AuctionTypes {
		Standard("1 - Standard"), Sports("2 - Sports");

		private String value;

		private AuctionTypes(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}
	}

	// Not sufficient to load auction
	public static void click_ReloadAuction_Manager() throws Exception {
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		BaseUI.click(Locator.lookupElement("auction_Reload_CoreAuctionsManager_Button"));
		Thread.sleep(500);
		Browser.clickBrowserBackButton();

		BaseUI.click(Locator.lookupElement("auction_Reload_CitiAndAARP_AuctionManager_Button"));
		Thread.sleep(500);
		Browser.clickBrowserBackButton();
	}

	public static void click_Add_subauction() throws Exception {
		BaseUI.click(Locator.lookupElement("auction_Add_subauction_Button"));
		BaseUI.waitForElementToBeDisplayed("subAuction_StartDate_TextBox", null, null, 20);
		Thread.sleep(2000);
	}

	public static void click_CreateProgram() throws Exception {
		BaseUI.click(Locator.lookupElement("auction_CreateProgramButton"));
		BaseUI.waitForElementToNOTBeDisplayed("auction_CreateProgramButton", null, null, 30);
		Thread.sleep(2000);
	}

	public static void initialize_Auction() throws Exception {
		click_CreateProgram();
		Auction_SearchSubAuction_Admin.search_WithDefaults_AndClickView();

		Auction_ViewDetail_Admin.click_ReloadAuction_Managers();
	}
}// End of Class
