package dRewards.pages_Administration;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class CatalogUpload_And_MemcacheFlush {
	
	public static void launch_CatalogUpload() throws Exception{
		Navigation_Admin.navigate_ExpandCategory("Product Catalog");
		click_CatalogUpload_Button();
		BaseUI.switchToAlert_Accept();
		Thread.sleep(10* 1000 * 60);
		BaseUI.switchToAlert_Accept();
		BaseUI.switchToAlert_Accept();
		
	}
	
	public static void click_CatalogUpload_Button() throws Exception{
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP) || 
				ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.click_js(Locator.lookupElement("catalog_UploadButton"));
			Thread.sleep(300);
		} else {
			BaseUI.click(Locator.lookupElement("catalog_UploadButton"));
			Thread.sleep(300);
		}
	}
	
	public static void launch_MemcacheFlush() throws Exception{
		BaseUI.scroll_to_element(Locator.lookupElement("admin_nav_NavigationLinkPrimary","Memcache Flush", null ));
		Navigation_Admin.navigate_ExpandCategory("Memcache Flush");
		click_MemcacheFlush_Button();
		BaseUI.switchToAlert_Accept();
		BaseUI.waitForElementToBeDisplayed("memcacheFlush_Successful", null, null, 30);
	}
	
	public static void click_MemcacheFlush_Button() throws Exception{
		BaseUI.click(Locator.lookupElement("memcacheFlush_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	public static void click_MiniCatalogUpload_Button() throws Exception {

		BaseUI.click(Locator.lookupElement("catalog_MiniCatalogUpload_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	public static void launch_MiniCatalogUpload() throws Exception {
		Navigation_Admin.navigate_ExpandCategory("Product Catalog");
		click_MiniCatalogUpload_Button();
		BaseUI.switchToAlert_Accept();
		Thread.sleep(3 * 1000 * 60);
		BaseUI.switchToAlert_Accept();
		BaseUI.switchToAlert_Accept();
	}
}
