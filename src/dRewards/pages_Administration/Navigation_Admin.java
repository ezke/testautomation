package dRewards.pages_Administration;

import org.testng.SkipException;
import utils.BaseUI;
import utils.Locator;

public class Navigation_Admin {

	public static void navigate_ExpandCategory(String categoryName) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("admin_nav_NavigationLinkPrimary", categoryName, null));
		BaseUI.waitForElementToNOTBeDisplayed("admin_nav_NavigationLinkPrimary", null, null, 20);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(500);
	}

	public static void navigate_ClickNavLink(String subCategoryName) throws Exception {
		BaseUI.click(Locator.lookupElement("admin_nav_NavigationLink_SubLink", subCategoryName, null));
		BaseUI.waitForElementToNOTBeDisplayed("admin_nav_NavigationLink_SubLink", null, null, 20);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(500);
	}

	public static void navigate_CreatePromoCodes() throws Exception {
		navigate_ClickNavLink("Create Promo Codes");
		BaseUI.waitForElementToBeDisplayed("promo_PromoCampaignName_TextBox", null, null);
		Thread.sleep(500);
	}

	// Also known as Consumer Service > Shopping
	public static void navigate_OrderManagement() throws Exception {
		navigate_ExpandCategory("Consumer Service");
		navigate_ClickNavLink("Shopping");
	}
	
	public static void navigate_Shopping_SearchOrders() throws Exception
	{
		navigate_OrderManagement();	
		BaseUI.click(Locator.lookupElement("admin_OrderManage_OrdersLink"));
		Thread.sleep(500);
	}
	
	// Navigate to Product Catalog > Catalog
	public static void navigate_ProductCatalog_Catalog() throws Exception{
		navigate_ExpandCategory("Product Catalog");
		BaseUI.waitForElementToContain_AttributeMatch("admin_nav_NavigationLink_SubLink_FollowingCategoryItem",
				"Product Catalog", null, "class", "subNav", 10);
		Thread.sleep(500);
		navigate_ClickNavLink("Catalog");
		BaseUI.waitForElementToBeDisplayed("catalog_Name_TextBox", null, null);
		Thread.sleep(500);
	}
	
	
	//Navigate to Admin > Sweepstakes > Create Program in Left hand menu
	public static void navigate_Sweepstakes_CreateProgram() throws Exception {
		navigate_ExpandCategory("Sweepstakes");
		navigate_ClickNavLink("Create Program");
		BaseUI.waitForElementToBeDisplayed("sweepstakes_SweepstakesName_Textbox", null, null);
		Thread.sleep(300);
	}
	
	
	//Navigate to Admin > Sweepstakes > Search Program in Left hand menu
	public static void navigate_Sweepstakes_SearchProgram() throws Exception {
		navigate_ExpandCategory("Sweepstakes");
		navigate_ClickNavLink("Search Program");
		BaseUI.waitForElementToBeDisplayed("searchSweepstakes_SweepstakesName", null, null, 10);
		Thread.sleep(500);
	}

	public static void navigateExpandCategory_SelectClientProgramId() throws Exception {
		navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		if (BaseUI.pageSourceContainsString("14014")) {
			String attributeValue = BaseUI.getTextFromField(
					Locator.lookupElement("clients_ProgramAttributes_Value_ByAttributeID", "14014", null));
			if (attributeValue.equals("True")) {
				// if the client have attribute True it will skip all the test
				throw new SkipException(
						"Skipping all the tests as this attribute value 14014 is set to True, this client will not see Daily Category Carousel in Homepage");
			}
		}
	}

	public static void navigateExpandCategory_SelectClientProgramId_CheckAttributeSetToTrue(
			String attribute, String valueToCheck) throws Exception {
		navigate_ClickNavLink("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		if (BaseUI.pageSourceContainsString(attribute)) {
			String attributeValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("clients_ProgramAttributes_Value_ByAttributeID", attribute, null), "innerText");
			if (!attributeValue.equals(valueToCheck)) {
				// if the client have attribute value FALSE it will skip all the tests
				throw new SkipException(
						"Skipping all the tests as this attribute value " + attribute +" is set to FALSE, "+
								"this client will not see Daily Category Carousel in Homepage");
			} else {
				BaseUI.log_Status("Attribute " + attribute +" is set to TRUE so DAILY CATEGORY SPECIALS CAROUSEL IS NOT DISPLAYED ON HOME PAGE");
			}
		} else {
			BaseUI.pageSourceNotContainsString(attribute);
			BaseUI.log_Status("Daily Category Specials Carousel is displayed on Home page");
		}
	}

	//Navigate to Admin > Sweepstakes > Search Program in Left hand menu
	public static void navigate_Auction_SearchSubAuctions() throws Exception {
		navigate_ExpandCategory("Auctions");
		navigate_ClickNavLink("Search SubAuction");
		BaseUI.waitForElementToBeDisplayed("searchSubAuction_HeaderTitle", null, null, 30);
		Thread.sleep(500);
	}
}
