package dRewards.pages_Administration;

import utils.BaseUI;
import utils.Locator;

public class Shopping_SearchOrders_Admin {

	public static void search_Order_ByOrderID(String orderID) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("admin_ShopSearchOrders_OrderIDText"), orderID);
		click_SearchOrders();
	}

	public static void click_SearchOrders() throws Exception {
		BaseUI.click(Locator.lookupElement("admin_ShopSearchOrders_SearchOrdersButton"));
		Thread.sleep(500);
	}

	public static void clickOrderLink_ByOrderID(String orderID) throws Exception {
		BaseUI.click(Locator.lookupElement("admin_ShopSearchOrders_OrderLink_ByOrderID", orderID, null));
		Thread.sleep(1000);
	}
}
