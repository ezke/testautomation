package dRewards.pageControls;

import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class HomePage_LPGWidget {


    public static void click_LearnMore_ExpandWidget() throws Exception {
        if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.JeepwaveRewards)
                || ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.TVC_Marketing)) {
            Homepage.clickLPG_LpgWidget();
        } else {
            Homepage.clickLPG_LearnMoreLink();
        }
    }

    public static void click_Current_SeeTermsAndConditionsLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_lpg_current_TermsAndConditions"));
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)) {
            BaseUI.waitForElementToBeDisplayed("lpgPage_BreadCrumb", null, null);
        } else {
            BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
        }
        Thread.sleep(500);
    }

    public static void click_Travel_Tab() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_lpgTravel_Tab"));
        Thread.sleep(500);
    }


}
