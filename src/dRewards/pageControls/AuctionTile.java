package dRewards.pageControls;

public interface AuctionTile {
    static AuctionTile empty() {
        return new UninitializedAuctionTile();
    }

    void click_DetailsButton() throws Exception;
    
    void click_AuctionsImage() throws Exception;

    void verify_AuctionDetails_Info(String productDescription, Double currentBid, Boolean isDualCurrency);

    void verify_MyAuctions_CurrentAuctionDetails_Info(String expectedPoints, String productDescription, Double currentBid);
}
