package dRewards.pageControls;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import utils.BaseUI;
import utils.Locator;

public class DatePicker {

	// date String must be in format MM/dd/yyyy
	public void selectTheDate(String date) throws Exception {
		
		Calendar dateWeWant = getCalendarValue(date);

		//extract the year
		Integer yearInteger = getYearInPicker();

		navigate_ToCorrectYear(dateWeWant, yearInteger);

		//re extract the text and get the month out of it.
		Integer monthInteger = getMonthInPicker();

		navigate_ToCorrectMonth(dateWeWant, monthInteger);
		click_On_Date(String.valueOf(dateWeWant.get(Calendar.DAY_OF_MONTH)));

	}

	protected Integer getYearInPicker(){
		String month_And_Year = BaseUI.getTextFromField(Locator.lookupRequiredElement("datePick_MonthAndYearHeader"));
		String year = month_And_Year.split("\\s+")[1];
		return Integer.parseInt(year);
	}

	protected Integer getMonthInPicker() throws Exception {
		String month_And_Year = BaseUI.getTextFromField(Locator.lookupRequiredElement("datePick_MonthAndYearHeader"));
		String month = month_And_Year.split("\\s+")[0];
		return convertMonth_Name_To_Number(month);
	}



	public Calendar getCalendarValue(String date)
	{
		 final DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
         final Calendar c = Calendar.getInstance();
         try {
             c.setTime(df.parse(date));
             System.out.println("Year = " + c.get(Calendar.YEAR));
             System.out.println("Month = " + (c.get(Calendar.MONTH)));
             System.out.println("Day = " + c.get(Calendar.DAY_OF_MONTH));
         } 
         catch (ParseException e) {
             e.printStackTrace();
         }
		
		return c;
	}
	
	
	public void click_On_Date(String dateToPick) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("datePick_dateCell_ByText", dateToPick, null));
		Thread.sleep(500);
	}

	protected void navigate_ToCorrectYear(Calendar dateTimeWeWant, Integer currentYearSet) throws Exception {
		Integer yearOfDateWeWant =  dateTimeWeWant.get(Calendar.YEAR);
		Integer yearDifference = yearOfDateWeWant - currentYearSet;
		if (yearDifference > 0) {
			for (int i = 0; i < yearDifference; i++) {
				click_NextYear_Button();
			}
		}
		else if(yearDifference < 0){
			for (int i = 0; i > yearDifference; i--) {
				click_PreviousYear_Button();
			}
		}
	}

	public void navigate_ToCorrectMonth(Calendar dateTimeWeWant, Integer currentMonthSet) throws Exception {
		Integer monthDifference = dateTimeWeWant.get(Calendar.MONTH) - currentMonthSet;

		if (monthDifference > 0) {
			for (int i = 0; i < monthDifference; i++) {
				click_NextMonth_Button();
			}
		} else if (monthDifference < 0) {
			for (int i = 0; i > monthDifference; i--) {
				click_PreviousMonth_Button();
			}

		}

	}

	public void click_PreviousMonth_Button() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("datePick_PreviousMonthButton"));
		Thread.sleep(200);
	}

	public void click_NextMonth_Button() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("datePick_NextMonthButton"));
		Thread.sleep(200);
	}

	public void click_NextYear_Button() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("datePick_NextYearButton"));
		Thread.sleep(200);
	}
	
	public void click_PreviousYear_Button() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("datePick_PreviousYearButton"));
		Thread.sleep(200);
	}


	public Integer convertMonth_Name_To_Number(String monthName) throws Exception {

		Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(monthName);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer month = cal.get(Calendar.MONTH);

		return month;
	}

}
