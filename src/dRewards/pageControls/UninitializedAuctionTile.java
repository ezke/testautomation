package dRewards.pageControls;

import utils.TestFailedDueToEarlierTestFailure;

public class UninitializedAuctionTile implements AuctionTile {
    private void fail(String longAction, String shortAction) {
        throw new TestFailedDueToEarlierTestFailure(
                "The test tried to " + longAction + ", but did not specify which auction to " + shortAction + ". " +
                        "This is often caused by an earlier test failing.");
    }

    @Override
    public void click_DetailsButton() {
        fail("click an auction's Details button", "click");
    }
    
    @Override
    public void click_AuctionsImage() {
        fail("click an auction's Image", "click");
    }

    @Override
    public void verify_AuctionDetails_Info(String productDescription, Double currentBid, Boolean isDualCurrency) {
        fail("verify auction information", "verify");
    }

    @Override
    public void verify_MyAuctions_CurrentAuctionDetails_Info(String expectedPoints, String productDescription, Double currentBid) {
        fail("verify 'My Auctions' auction information", "verify");
    }
}
