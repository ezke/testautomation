package dRewards.pageControls;

import java.util.Calendar;

public class DatePicker_Citi extends DatePicker {

    @Override
    protected void navigate_ToCorrectYear(Calendar dateTimeWeWant, Integer currentYearSet) throws Exception {
        Integer yearOfDateWeWant = dateTimeWeWant.get(Calendar.YEAR);
        Integer yearDifference = yearOfDateWeWant - currentYearSet;
        if (yearDifference > 0) {
            int count = 0;
            while (!getYearInPicker().equals(yearOfDateWeWant)) {
                click_NextMonth_Button();

                count++;
                if(count > 24){
                    break;
                }
            }

        } else if (yearDifference < 0) {
            int count = 0;
            while (!getYearInPicker().equals(yearOfDateWeWant)) {
                click_PreviousMonth_Button();

                count++;
                if(count > 24){
                    break;
                }
            }

        }
    }
}
