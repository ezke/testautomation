package dRewards.pageControls;

import org.openqa.selenium.WebElement;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

//Found on Shipping and Billing pages.
public class PriceSummary {

	public static void verify_PriceSummary_Info_SalesTaxPresent(PricingDetails pricingDetails) {
		verify_PriceSummary_Info(pricingDetails.points, pricingDetails.retailPrice, pricingDetails.priceAfterPoints,
				pricingDetails.savings_Percent, pricingDetails.shipping_Cost, true, pricingDetails.order_Total,
				pricingDetails.allstate_Cash);

	}

	public static void verify_PriceSummary_Info_SalesTaxNOTPresent(PricingDetails pricingDetails) {
		verify_PriceSummary_Info(pricingDetails.points, pricingDetails.retailPrice, pricingDetails.priceAfterPoints,
				pricingDetails.savings_Percent, pricingDetails.shipping_Cost, false, pricingDetails.order_Total,
				pricingDetails.allstate_Cash);

	}
	
	public static void verify_PriceSummary_Info_SalesTaxPresent_GiftCard(PricingDetails pricingDetails) {
		verify_PriceSummary_Info_GiftCard_Only(pricingDetails.points, pricingDetails.retailPrice, pricingDetails.priceAfterPoints,
				pricingDetails.savings_Percent, pricingDetails.shipping_Cost, true, pricingDetails.order_Total,
				pricingDetails.allstate_Cash);

	}

	// Use this Overload method when we don't already know what the sales tax
	// should be.
	public static void verify_PriceSummary_Info(String currencyAmount, String retailPrice, String youPay,
			String percentSavings, String shippingCost, Boolean salesTaxPresent, String orderTotal,
			String allstate_Cash) {

		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("priceSum_priceSummaryTitle", "Order Summary");
		} else {
			BaseUI.verifyElementHasExpectedText("priceSum_priceSummaryTitle", "Price Summary");
		}

		verify_SalesTax_Present(salesTaxPresent);

		// if (salesTaxPresent) {
		// String newOrderTotal = orderTotal.replace("$", "").replace(",", "");
		// String salesTaxOnPage =
		// BaseUI.getTextFromField(Locator.lookupRequiredElement("priceSum_estimated_SalesTax"));
		// salesTaxOnPage = salesTaxOnPage.replace("$", "").replace(",", "");
		//
		// Double newOrderTotal_Double = Double.parseDouble(newOrderTotal);
		// Double salesTax_Double = Double.parseDouble(salesTaxOnPage);
		//
		// newOrderTotal_Double += salesTax_Double;
		//
		// orderTotal = "$" +
		// BaseUI.convertDouble_ToString_ForCurrency(newOrderTotal_Double);
		// }

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			if (allstate_Cash != null) {
				if (!allstate_Cash.equals("$0.00")) {
					allstate_Cash = "-" + allstate_Cash;
				}
				BaseUI.verifyElementHasExpectedText("priceSum_AllstateCash_Amount", allstate_Cash);
			} else {
				BaseUI.verifyElementHasExpectedText("priceSum_AllstateCash_Amount", "---");
			}
		}

		verify_Amounts_ExceptSalesTax(currencyAmount, retailPrice, youPay, percentSavings, shippingCost, orderTotal);
	}

	public static void verify_PriceSummary_Info_GiftCard_Only(String currencyAmount, String retailPrice, String youPay,
			String percentSavings, String shippingCost, Boolean salesTaxPresent, String orderTotal,
			String allstate_Cash) {

		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("priceSum_priceSummaryTitle", "Order Summary");
		} else {
			BaseUI.verifyElementHasExpectedText("priceSum_priceSummaryTitle", "Price Summary");
		}

		verify_SalesTax_Present_GiftCard_Only(salesTaxPresent);

		// if (salesTaxPresent) {
		// String newOrderTotal = orderTotal.replace("$", "").replace(",", "");
		// String salesTaxOnPage =
		// BaseUI.getTextFromField(Locator.lookupRequiredElement("priceSum_estimated_SalesTax"));
		// salesTaxOnPage = salesTaxOnPage.replace("$", "").replace(",", "");
		//
		// Double newOrderTotal_Double = Double.parseDouble(newOrderTotal);
		// Double salesTax_Double = Double.parseDouble(salesTaxOnPage);
		//
		// newOrderTotal_Double += salesTax_Double;
		//
		// orderTotal = "$" +
		// BaseUI.convertDouble_ToString_ForCurrency(newOrderTotal_Double);
		// }

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			if (allstate_Cash != null) {
				if (!allstate_Cash.equals("$0.00")) {
					allstate_Cash = "-" + allstate_Cash;
				}
				BaseUI.verifyElementHasExpectedText("priceSum_AllstateCash_Amount", allstate_Cash);
			} else {
				BaseUI.verifyElementHasExpectedText("priceSum_AllstateCash_Amount", "---");
			}
		}

		verify_Amounts_ExceptSalesTax(currencyAmount, retailPrice, youPay, percentSavings, shippingCost, orderTotal);
	}
	public static void verify_PriceSummary_Info_TaxAmountKnown(PricingDetails pricingDetails) {
		verify_PriceSummary_Info_TaxAmountKnown(pricingDetails.points, pricingDetails.retailPrice,
				pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
				pricingDetails.tax_Cost, pricingDetails.order_Total, pricingDetails.allstate_Cash);

	}

	// Use this Overload method when we DO know what the sales tax should be.
	public static void verify_PriceSummary_Info_TaxAmountKnown(String currencyAmount, String retailPrice, String youPay,
			String percentSavings, String shippingCost, String salesTax, String orderTotal, String allstate_Cash) {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			if (allstate_Cash != null) {
				if (!allstate_Cash.equals("$0.00")) {
					allstate_Cash = "-" + allstate_Cash;
				}

				BaseUI.verifyElementHasExpectedText("priceSum_AllstateCash_Amount", allstate_Cash);
			} else {
				BaseUI.verifyElementHasExpectedText("priceSum_AllstateCash_Amount", "---");
			}
		}
		
		if (ClientDataRetrieval.isRedesignClient()) {
			if(!orderTotal.equals("$0.00")) {
				BaseUI.verifyElementHasExpectedText("priceSum_priceSummaryTitle", "Order Summary");
			}		
		} else {
			BaseUI.verifyElementHasExpectedText("priceSum_priceSummaryTitle", "Price Summary");
		}		

		verify_SalesTax_Amount(salesTax);

		verify_Amounts_ExceptSalesTax(currencyAmount, retailPrice, youPay, percentSavings, shippingCost, orderTotal);
	}

	public static void verify_SalesTax_Amount(String salesTaxAmount) {
		String salesTaxOnPage = return_SalesTaxAsString();
		BaseUI.baseStringCompare("Sales Tax Amount", salesTaxAmount, salesTaxOnPage);
	}

	// Verifies all the amounts except for Sales Tax
	public static void verify_Amounts_ExceptSalesTax(String currencyAmount, String retailPrice, String youPay,
			String percentSavings, String shippingCost, String orderTotal) {
		BaseUI.verifyElementHasExpectedText("priceSum_retailPrice", retailPrice);

		String currency = "Points";
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			currency = "Smart Dollars";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			currency = "Rewards Dollars";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			currency = "-(Member Savings)";
		}else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			currency = "Savings Dollars";
		}
		
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedText("priceSum_currency", currency);
	//	}
		//if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
		//	BaseUI.verifyElementHasExpectedText("priceSum_currency", currency+ ":");
				
		} else {
			BaseUI.verifyElementHasExpectedText("priceSum_currency", currency + ":");
		}

		// if (!ClientDataRetrieval.clientName.startsWith("Citi-9485")) {
		String actual_currency = BaseUI.getTextFromField(Locator.lookupRequiredElement("priceSum_currency"));
		BaseUI.baseStringPartialCompare("currency", currency, actual_currency);
		// BaseUI.verifyElementHasExpectedText("priceSum_currency", currency +
		// ":");
		// }
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedPartialText("priceSum_currencyAmount", currencyAmount);
		}
		BaseUI.verifyElementHasExpectedText("priceSum_youPay", youPay);
		BaseUI.verifyElementHasExpectedText("priceSum_percentSavings", percentSavings);
		BaseUI.verifyElementHasExpectedText("priceSum_shippingCost", shippingCost);

		BaseUI.verifyElementHasExpectedText("priceSum_orderTotal", orderTotal);
	}

	// Assumes state is Florida in Zip 33487. Otherwise we'll need to create an
	// overload to handle other tax rates.
	public static void verify_SalesTax_Present(Boolean salesTaxPresent) {
		if (salesTaxPresent) {
			// retailPrice = retailPrice.replace("$", "").replace(",", "");
			// Double taxRate = .07;
			// Double retailPriceDouble = Double.parseDouble(retailPrice);
			//
			// Double expectedSalesTax = retailPriceDouble * taxRate;
			// BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("priceSum_estimated_SalesTax"),
			// "$" +
			// BaseUI.convertDouble_ToString_ForCurrency(expectedSalesTax));

			String salesTaxOnPage = return_SalesTaxAsString();
			salesTaxOnPage = salesTaxOnPage.replace("$", "").replace("$", "");
			Double salesTaxDouble = Double.parseDouble(salesTaxOnPage);

			if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
					|| ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
					|| ClientDataRetrieval.client_Matches(client_Designation.McAfee)) {
				BaseUI.verify_true_AndLog(salesTaxDouble >= 0, "Sales Tax was found and is equal to " + salesTaxOnPage,
						"Seeing " + salesTaxOnPage + " on page.");
			} else {
				BaseUI.verify_true_AndLog(salesTaxDouble > 0, "Sales Tax was found and greater than 0.",
						"Seeing " + salesTaxOnPage + " on page.");
			}

		} else {
			WebElement salesTaxNotPresentYetElement = Locator.lookupRequiredElement("priceSum_salesTaxCalc_AtCheckout");
			BaseUI.verifyElementAppears(salesTaxNotPresentYetElement);
			BaseUI.verifyElementHasExpectedText(salesTaxNotPresentYetElement,
					"Sales tax will be calculated at checkout");
		}
	}
	
	public static void verify_SalesTax_Present_GiftCard_Only(Boolean salesTaxPresent) {
		if (salesTaxPresent) {

			String salesTaxOnPage = return_SalesTaxAsString();
			salesTaxOnPage = salesTaxOnPage.replace("$", "").replace("$", "");
			Double salesTaxDouble = Double.parseDouble(salesTaxOnPage);
			
			BaseUI.verify_true_AndLog(salesTaxDouble >= 0, "Sales Tax was found: "+ salesTaxDouble,
					"Sales Tax is NOT Displayed");				
		} else {
			WebElement salesTaxNotPresentYetElement = Locator.lookupRequiredElement("priceSum_salesTaxCalc_AtCheckout");
			BaseUI.verifyElementAppears(salesTaxNotPresentYetElement);
			BaseUI.verifyElementHasExpectedText(salesTaxNotPresentYetElement,
					"Sales tax will be calculated at checkout");
		}
	}

	public static String return_SalesTaxAsString() {
		String salesTaxOnPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("priceSum_estimated_SalesTax"));

		return salesTaxOnPage;
	}

	// Use if Shipping Cost was not created yet.
	// public static void verify_PriceSummary_Info(String currencyAmount, String
	// retailPrice, String youPay,
	// String percentSavings, Boolean salesTaxPresent, String orderTotal) {
	// verify_PriceSummary_Info(currencyAmount, retailPrice, youPay,
	// percentSavings, "$0.00", salesTaxPresent,
	// orderTotal);
	//
	// }

}// End of Class
