package dRewards.pageControls;

import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Navigation;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class WebAuctionTile implements AuctionTile {
    private String oneBasedAuctionIndexForXPath;

    public WebAuctionTile(int oneBasedAuctionIndexForXPath) {
        this.oneBasedAuctionIndexForXPath = String.valueOf(oneBasedAuctionIndexForXPath);
    }

    @Override
    public void click_DetailsButton() throws Exception {
        WebElement elementToClick = Locator.lookupRequiredElement("auction_DetailsLink_ByIndex", oneBasedAuctionIndexForXPath,
                null);
        BaseUI.click(elementToClick);
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("auctionBid_WaitElement", null, null, 30);
        Thread.sleep(2000);
    }
    
    
    @Override
    public void click_AuctionsImage() throws Exception {
        WebElement elementToClick = Locator.lookupRequiredElement("auction_AuctionsImage_ByIndex", oneBasedAuctionIndexForXPath,
                null);
        BaseUI.click(elementToClick);
        Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
    }

    @Override
    public void verify_AuctionDetails_Info(String productDescription, Double currentBid, Boolean isDualCurrency) {
        String timeText = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("auction_TimeLeft_ByIndex", oneBasedAuctionIndexForXPath, null));
        if (ClientDataRetrieval.isRedesignClient()) {
        	BaseUI.verify_true_AndLog(timeText.matches("^(\\d{2}m\\s\\d{2}s)$") || timeText.matches("^(\\d{2}h\\s\\d{2}m)$"), 
            		"Matched pattern 00m 00s or 00h 00m",
                    "Text did not look right, seeing value " + timeText);
        } else {
        	BaseUI.verify_true_AndLog(timeText.matches("^(\\d{2}h\\s\\d{2}m\\s\\d{2}s)$"), "Matched pattern 00h 00m 00s",
                    "Text did not look right, seeing value " + timeText);
        }
        
        BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("auction_Title_ByIndex", oneBasedAuctionIndexForXPath, null),
                productDescription);
        String expectedBid = BaseUI.convertDouble_ToString_ForCurrency(currentBid);
        if (!ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.SmartRewards)) {
            expectedBid = expectedBid.split("\\.")[0];
        }
        
        String currencyType = "Points";
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.SmartRewards)) {
            currencyType = "Smart Dollars";
        }
        
        if (ClientDataRetrieval.isRedesignClient()) {
        	BaseUI.verifyElementHasExpectedText(
                    Locator.lookupRequiredElement("auction_currentBid_ByIndex", oneBasedAuctionIndexForXPath, null), expectedBid+" Points");
		} else {
			BaseUI.verifyElementHasExpectedText(
	                Locator.lookupRequiredElement("auction_currentBid_ByIndex", oneBasedAuctionIndexForXPath, null), expectedBid);
	        BaseUI.verifyElementHasExpectedText(
	                Locator.lookupRequiredElement("auction_currencyType_ByIndex", oneBasedAuctionIndexForXPath, null), currencyType);
	        
	        WebElement typeOfAuction = Locator.lookupRequiredElement("auction_AuctionType", oneBasedAuctionIndexForXPath, null);
	        if (isDualCurrency) {
	            if (BaseUI.elementAppears(Locator.lookupOptionalElement("auction_BidExceeds_Text", oneBasedAuctionIndexForXPath, null))) {
	                BaseUI.verifyElementHasExpectedText(
	                        Locator.lookupRequiredElement("auction_BidExceeds_Text", oneBasedAuctionIndexForXPath, null), "Minimum bid exceeds your balance");
	            } else {
	                BaseUI.verifyElementHasExpectedText(typeOfAuction, currencyType + " + USD");
	            }
	        } else {
	            if (BaseUI.elementAppears(Locator.lookupOptionalElement("auction_BidExceeds_Text", oneBasedAuctionIndexForXPath, null))) {
	                BaseUI.verifyElementHasExpectedText(
	                        Locator.lookupRequiredElement("auction_BidExceeds_Text", oneBasedAuctionIndexForXPath, null), "Minimum bid exceeds your balance");
	            } else {
	                BaseUI.verifyElementHasExpectedText(typeOfAuction, currencyType + " Only");
	            }
	        }
		}      
    }

    @Override
    public void verify_MyAuctions_CurrentAuctionDetails_Info(String expectedPoints, String productDescription, Double currentBid) {

        String actualPoints = Navigation.returnPoints();
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.SmartRewards)) {
            actualPoints = actualPoints.split("\\.")[0];
        }
        String timeText = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("myAuct_TimeLeft_ByIndex", oneBasedAuctionIndexForXPath, null));
        BaseUI.verify_true_AndLog(timeText.matches("^(\\d{2}h\\s\\d{2}m\\s\\d{2}s)$"), "Matched pattern 00h 00m 00s",
                "Text did not look right, seeing value " + timeText);

        BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("myAuct_Title_ByIndex", oneBasedAuctionIndexForXPath, null),
                productDescription);
        String expectedBid = BaseUI.convertDouble_ToString_ForCurrency(currentBid);
        if (!ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.SmartRewards)) {
            expectedBid = expectedBid.split("\\.")[0];
        }
        BaseUI.baseStringCompare("Points", expectedPoints, actualPoints);
        BaseUI.verifyElementHasExpectedText(
                Locator.lookupRequiredElement("myAuct_currentBid_ByIndex", oneBasedAuctionIndexForXPath, null), expectedBid);
        BaseUI.verifyElementHasExpectedText(
                Locator.lookupRequiredElement("myAuct_currencyType_ByIndex", oneBasedAuctionIndexForXPath, null),
                ClientDataRetrieval.client_Currency);
    }
}
