package dRewards.data;

import java.util.ArrayList;
import java.util.HashMap;

import dRewards.pageControls.DatePicker;
import dRewards.pageControls.DatePicker_Citi;
import org.testng.Reporter;

import dRewards.pages.GlobalVariables;
import utils.Browser;
import utils.DataBuilder;

public class ClientDataRetrieval {

	public static HashMap<String, String> config_Data = null;
	public static String clientName;
	public static String userName;
	public static String password;
	public static String environment;
	public static String citiDomain;
	public static Object[][] environment_Data = null;
	public static String objectRepositoryPath;
	public static String url;
	public static String buyerID;
	public static String secondaryUser_AARP_Bpass;
	public static String secondaryUser_AARP_BpassAuction;
	public static String secondaryPassword_AARP_Bpass;
	public static String secondaryPassword_AARP_BpassAuction;
	public static String secondary_AARP_Bpass_URL;
	// secondaryUser will be used for things like auctions.
	public static String secondaryUser;
	public static String secondaryPassword;
	public static String secondaryUser_NoPoints;
	public static String secondaryPassword_NoPoints;
	public static String secondaryUser_OnePoints;
	public static String secondaryPassword_OnePoints;
	public static String secondaryUser_1499Points;
	public static String secondaryPassword_1499Points;
	public static String secondaryUser_1500Points;
	public static String secondaryPassword_1500Points;
	public static String secondaryUser_30001Points;
	public static String secondaryPassword_30001Points;
	public static String secondaryUser_31000Points;
	public static String secondaryPassword_31000Points;
	public static String attributesReload_URL;

	//For AARP Redesign Bypass url secondary user and password for micro auctions
	public static String secondaryUser_AARP_Bpass_NoPoints;
	public static String secondaryPassword_AARP_Bpass_NoPoints;
	public static String secondaryUser_AARP_Bpass_OnePoints;
	public static String secondaryPassword_AARP_Bpass_OnePoints;
	public static String secondaryUser_AARP_Bpass_1499Points;
	public static String secondaryPassword_AARP_Bpass_1499Points;
	public static String secondaryUser_AARP_Bpass_1500Points;
	public static String secondaryPassword_AARP_Bpass_1500Points;
	public static String secondaryUser_AARP_Bpass_30001Points;
	public static String secondaryPassword_AARP_Bpass_30001Points;
	public static String secondaryUser_AARP_Bpass_31000Points;
	public static String secondaryPassword_AARP_Bpass_31000Points;
	
	

	public static client_Designation client_Name;
	public static String client_Currency;

	// public static void set_TestData() throws Exception {
	// config_Data =
	// DataBuilder.GetConfig_DataFromFile("C:\\Selenium\\Eber\\Config.txt");
	// clientName = config_Data.get("clientName");
	// Browser.defaultBrowser = config_Data.get("Browser");
	// userName = config_Data.get("UserName");
	// password = config_Data.get("password");
	//
	//
	//
	// }

	// pulls values from xml. If values are null, it relies on Config file as
	// backup.
	public static void set_TestData_XML() throws Exception {
		// config_Data =
		// DataBuilder.GetConfig_DataFromFile("C:\\Selenium\\Eber\\Config.txt");
		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\dRewards\\data\\Config.txt");
		// config_Data =
		// DataBuilder.GetConfig_DataFromFile("\\src\\dRewards\\data\\/Config-smartrewards.txt");
		environment_Data = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\ClientData.txt");

		clientName = getValue("clientName");
		userName = getValue("userName");
		password = getValue("password");
		environment = getValue("environment");
		buyerID = getValue("buyerID");
		citiDomain = getValue("Citidomain");
		GlobalVariables.dRewards_Admin_URL = "https://dradmin.drw" + environment.toLowerCase() + ".com/login.jsp";
		GlobalVariables.dRewards_Admin_Login = environment.equals("RC") ? "qa5" : "qa2";
		Browser.defaultBrowser = getValue("browser");

		objectRepositoryPath = returnValue_ByPassedInText("Repo", "Environment", clientName, environment);
		if (objectRepositoryPath == null) {
			throw new RuntimeException("Could not find object repository path for client=" + clientName + " and environment=" + environment);
		}

		url = returnValue_ByPassedInText("URL", "Environment", clientName, environment);
		set_Client();
		set_SecondaryUser_NoPoints();
		set_SecondaryUser_OnePoints();
		set_SecondaryUser_1499Points();
		set_SecondaryUser_1500Points();
		set_SecondaryUser_30001Points();
		set_SecondaryUser_31000Points();
	}

	public static void set_secondaryUser_Info_AARP_BpassAuction(String browser) throws Exception {
		Object[][] secondary_userData_AARPRedesign = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_BpassAuction.txt");

		secondaryUser_AARP_BpassAuction = returnValue_ByPassedInText(secondary_userData_AARPRedesign, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_BpassAuction = returnValue_ByPassedInText(secondary_userData_AARPRedesign, "Password", "Browser", clientName, browser);
		//secondary_AARP_Bpass_URL = returnValue_ByPassedInText(secondary_userData_AARPRedesing, "URL", "Environment", clientName,environment);
	}

	public static void set_secondaryUser_Info_AARP_Bpass(String browser) throws Exception {
		Object[][] secondary_userData_AARPRedesign = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_Bpass.txt");

		secondaryUser_AARP_Bpass = returnValue_ByPassedInText(secondary_userData_AARPRedesign, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass = returnValue_ByPassedInText(secondary_userData_AARPRedesign, "Password", "Browser", clientName, browser);
		//secondary_AARP_Bpass_URL = returnValue_ByPassedInText(secondary_userData_AARPRedesing, "URL", "Environment", clientName,environment);
	}

	public static void set_secondary_AARP_Bpass_URL() throws Exception{
		Object[][] secondary_AARP_Bpass_URLData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\secondary_AARP_Bpass_URL.txt");

		secondary_AARP_Bpass_URL = returnValue_ByPassedInText(secondary_AARP_Bpass_URLData, "URL", "Environment", clientName, environment);
	}

	public static void set_secondaryUser_Info(String browser) throws Exception {
		Object[][] secondary_userData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users.txt");

		secondaryUser = returnValue_ByPassedInText(secondary_userData, "Username", "Browser", clientName, browser);
		secondaryPassword = returnValue_ByPassedInText(secondary_userData, "Password", "Browser", clientName, browser);
	}
	
	public static void set_AttributesReload_URL() throws Exception{
		Object[][] attributesReload_URLData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\AttributesReload_URL.txt");

		attributesReload_URL = returnValue_ByPassedInText(attributesReload_URLData, "URL", "Environment", clientName, environment);
	}
	
	public static void set_SecondaryUser_NoPoints() throws Exception{
		Object[][] secondaryUser_NoPointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_NoPoints.txt");

		secondaryUser_NoPoints = returnValue_ByPassedInText(secondaryUser_NoPointsData, "Username", "Environment", clientName, environment);
		secondaryPassword_NoPoints = returnValue_ByPassedInText(secondaryUser_NoPointsData, "Password", "Environment", clientName, environment);
	}
	
	public static void set_SecondaryUser_OnePoints() throws Exception{
		Object[][] secondaryUser_1PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_1Points.txt");

		secondaryUser_OnePoints = returnValue_ByPassedInText(secondaryUser_1PointsData, "Username", "Environment", clientName, environment);
		secondaryPassword_OnePoints = returnValue_ByPassedInText(secondaryUser_1PointsData, "Password", "Environment", clientName, environment);
	}
	
	public static void set_SecondaryUser_1499Points() throws Exception{
		Object[][] secondaryUser_1499PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_1499Points.txt");

		secondaryUser_1499Points = returnValue_ByPassedInText(secondaryUser_1499PointsData, "Username", "Environment", clientName, environment);
		secondaryPassword_1499Points = returnValue_ByPassedInText(secondaryUser_1499PointsData, "Password", "Environment", clientName, environment);
	}
	
	public static void set_SecondaryUser_1500Points() throws Exception{
		Object[][] secondaryUser_1500PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_1500Points.txt");

		secondaryUser_1500Points = returnValue_ByPassedInText(secondaryUser_1500PointsData, "Username", "Environment", clientName, environment);
		secondaryPassword_1500Points = returnValue_ByPassedInText(secondaryUser_1500PointsData, "Password", "Environment", clientName, environment);
	}
	
	public static void set_SecondaryUser_30001Points() throws Exception{
		Object[][] secondaryUser_30001PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_30001Points.txt");

		secondaryUser_30001Points = returnValue_ByPassedInText(secondaryUser_30001PointsData, "Username", "Environment", clientName, environment);
		secondaryPassword_30001Points = returnValue_ByPassedInText(secondaryUser_30001PointsData, "Password", "Environment", clientName, environment);
	}
	
	public static void set_SecondaryUser_31000Points() throws Exception{
		Object[][] secondaryUser_31000PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_31000Points.txt");

		secondaryUser_31000Points = returnValue_ByPassedInText(secondaryUser_31000PointsData, "Username", "Environment", clientName, environment);
		secondaryPassword_31000Points = returnValue_ByPassedInText(secondaryUser_31000PointsData, "Password", "Environment", clientName, environment);
	}

	public static void set_SecondaryUser_AARP_ByPass_NoPoints(String browser) throws Exception{
		Object[][] secondaryUser_NoPointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_ByPass_NoPoints.txt");

		secondaryUser_AARP_Bpass_NoPoints = returnValue_ByPassedInText(secondaryUser_NoPointsData, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass_NoPoints = returnValue_ByPassedInText(secondaryUser_NoPointsData, "Password", "Browser", clientName, browser);
	}

	public static void set_SecondaryUser_AARP_ByPass_OnePoints(String browser) throws Exception{
		Object[][] secondaryUser_1PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_ByPass_1Points.txt");

		secondaryUser_AARP_Bpass_OnePoints = returnValue_ByPassedInText(secondaryUser_1PointsData, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass_OnePoints = returnValue_ByPassedInText(secondaryUser_1PointsData, "Password", "Browser", clientName, browser);
	}

	public static void set_SecondaryUser_AARP_ByPass_1499Points(String browser) throws Exception{
		Object[][] secondaryUser_1499PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_ByPass_1499Points.txt");

		secondaryUser_AARP_Bpass_1499Points = returnValue_ByPassedInText(secondaryUser_1499PointsData, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass_1499Points = returnValue_ByPassedInText(secondaryUser_1499PointsData, "Password", "Browser", clientName, browser);
	}

	public static void set_SecondaryUser_AARP_ByPass_1500Points(String browser) throws Exception{
		Object[][] secondaryUser_1500PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_ByPass_1500Points.txt");

		secondaryUser_AARP_Bpass_1500Points = returnValue_ByPassedInText(secondaryUser_1500PointsData, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass_1500Points = returnValue_ByPassedInText(secondaryUser_1500PointsData, "Password", "Browser", clientName, browser);
	}

	public static void set_SecondaryUser_AARP_ByPass_30001Points(String browser) throws Exception{
		Object[][] secondaryUser_30001PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_ByPass_30001Points.txt");

		secondaryUser_AARP_Bpass_30001Points = returnValue_ByPassedInText(secondaryUser_30001PointsData, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass_30001Points = returnValue_ByPassedInText(secondaryUser_30001PointsData, "Password", "Browser", clientName, browser);
	}

	public static void set_SecondaryUser_AARP_ByPass_31000Points(String browser) throws Exception{
		Object[][] secondaryUser_31000PointsData = DataBuilder.getTableData_FromFile("\\src\\dRewards\\data\\Secondary_Users_AARP_ByPass_31000Points.txt");

		secondaryUser_AARP_Bpass_31000Points = returnValue_ByPassedInText(secondaryUser_31000PointsData, "Username", "Browser", clientName, browser);
		secondaryPassword_AARP_Bpass_31000Points = returnValue_ByPassedInText(secondaryUser_31000PointsData, "Password", "Browser", clientName, browser);
	}

	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}

		if (value != null) {
			System.setProperty(valueToGet, value);
		}
		return value;
	}

	// looks for the value for the columnHeader variable. Matches the row based
	// on the rowIdentifier and the columnHeader2/columnHeader2_Value.
	public static String returnValue_ByPassedInText(String columnHeader, String columnHeader2, String rowIdentifier,
			String columnHeader2_CellValue) {
		return returnValue_ByPassedInText(environment_Data, columnHeader, columnHeader2, rowIdentifier,
				columnHeader2_CellValue);
	}

	public static String returnValue_ByPassedInText(Object[][] dataToUse, String columnHeader, String columnHeader2,
			String rowIdentifier, String columnHeader2_CellValue) {
		String value = null;

		ArrayList<String> headerList = new ArrayList<String>();
		for (int i = 0; i < dataToUse[0].length; i++) {
			headerList.add(dataToUse[0][i].toString());
		}

		for (int i = 0; i < dataToUse.length; i++) {
			if (rowIdentifier.equals(dataToUse[i][0].toString())
					&& dataToUse[i][headerList.indexOf(columnHeader2)].equals(columnHeader2_CellValue)) {

				value = dataToUse[i][headerList.indexOf(columnHeader)].toString();
				break;
			}
		}

		return value;
	}

	public static String returnValue_ByPassedInText(String columnHeader, String rowIdentifier) {
		String value = null;

		ArrayList<String> headerList = new ArrayList<String>();
		for (int i = 0; i <= environment_Data.length; i++) {
			headerList.add(environment_Data[0][i].toString());
		}

		for (int i = 0; i <= environment_Data.length; i++) {
			if (rowIdentifier.equals(environment_Data[i][0].toString())) {

				value = environment_Data[i][headerList.indexOf(columnHeader)].toString();
				break;
			}
		}

		return value;
	}

	// Return true if the client you passed in matches the client that is
	// active.
	public static Boolean client_Matches(client_Designation clientToMatch) {
		Boolean clientNameMatches = false;

		if (clientToMatch.equals(client_Designation.Allstate_both)) {
			if (client_Name.equals(client_Designation.Allstate)
					|| client_Name.equals(client_Designation.AllstateCash)) {
				clientNameMatches = true;
			}
		} else {
			clientNameMatches = client_Name.equals(clientToMatch) ? true : false;
		}

		return clientNameMatches;
	}

	public static DatePicker getDatePicker(){
		if(client_Matches(client_Designation.Citi)) {
			return new DatePicker_Citi();
		}else{
			return new DatePicker();
		}

	}

	private static void set_Client() {
		switch (clientName) {
		case "Smart rewards-9255":
			client_Name = client_Designation.SmartRewards;
			client_Currency = "Smart Dollars";
			break;
		case "Citi-9485":
			client_Name = client_Designation.Citi;
			break;
		case "Extracashatt-9486":
			client_Name = client_Designation.Extracashatt;
			break;
		case "Allstate Rewards-9419":
			client_Name = client_Designation.Allstate;
			client_Currency = "Points";
			break;
		case "Allstate CashRewards-9414":
			client_Name = client_Designation.AllstateCash;
			client_Currency = "Points";
			break;
		case "TVC Marketing-9452":
			client_Name = client_Designation.TVC_Marketing;
			client_Currency = "Rewards Dollars";
			break;
		case "AARP-9362":
			client_Name = client_Designation.AARP;
			client_Currency = "Points";
			break;
		case "RedesignCore":
				client_Name = client_Designation.RedesignCore;
				client_Currency = "Points";
				break;
			case "McAfee-9604":
				client_Name = client_Designation.McAfee;
				client_Currency = "Points";
				break;
		case "Streetwise Rewards-9461":
			client_Name = client_Designation.StreetwiseRewards;
			client_Currency = "Points";
			break;
		case "Jeepwave Rewards-9457":
			client_Name = client_Designation.JeepwaveRewards;
			client_Currency = "Savings Dollars";
			break;
		case "AARPRedesign-9476":
			client_Name = client_Designation.AARPRedesign;
			client_Currency = "Points";
			break;
		default:
			String message = "Unable to find client.";
			System.out.println(message);
			Reporter.log(message);

			break;
		}

	}

	public static boolean isLegacyClient() {
		return !isRedesignClient();
	}

	public static boolean isRedesignClient() {
		return client_Matches(client_Designation.AARPRedesign)
				|| client_Matches(client_Designation.RedesignCore)
				|| client_Matches(client_Designation.McAfee);
	}

	public static enum client_Designation {
		SmartRewards("Smart rewards-9255"), Citi("Citi-9485"), Extracashatt("-9486"), Citi_both(
				"Citi"), Allstate("Allstate Rewards-9419"), AllstateCash(
				"Allstate CashRewards-9414"), Allstate_both(
						"Allstate"), TVC_Marketing("TVC Marketing-9452"), AARP("AARP-9362"), RedesignCore("RedesignCore"),McAfee("McAfee-9604"),
		StreetwiseRewards("Streetwise Rewards-9461"),JeepwaveRewards("Jeepwave Rewards-9457"), AARPRedesign("AARPRedesign-9476");

		private String value;

		private client_Designation(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

}
