package dRewards.data;

import org.testng.annotations.BeforeSuite;

import utils.Locator;
import utils.Selenium;

public abstract class BaseTest {

	@BeforeSuite(alwaysRun=true)
	public void config_setup_method() throws Exception {
		Selenium.verifySeleniumIsRunning();

		ClientDataRetrieval.set_TestData_XML();

		Locator.loadObjectRepository(
				// Load our common repository
				ClientDataRetrieval.isRedesignClient()
						? "\\src\\dRewards\\redesignCommonRepo.txt"
						: "\\src\\dRewards\\commonRepo.txt",
				// Load our client specific repository
				ClientDataRetrieval.objectRepositoryPath);
	}
}
