package dRewards.tests_ignored;

import java.util.concurrent.ThreadLocalRandom;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;


public class MerchCheckoutProcess_Workflow_PT3235 extends BaseTest {

	private TableData productList_ShoppingCart = new TableData();
	private TableData productList_ReviewOrder = new TableData();
	private TableData productList_OrderConfirmation = new TableData();
	
	private PricingDetails pricingDetails_ShoppingCart = new PricingDetails();
	private PricingDetails pricingDetails_ReviewOrder = new PricingDetails();
	private PricingDetails pricingDetails_OrderConfirmation = new PricingDetails();
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		///LoginPage.login(GlobalVariables.citiEasyDeals_UserName, GlobalVariables.citiEasyDeals_Password);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Home_viaLogo();
		Navigation.navigate_Merchandise_Sunglasses();

		// add 3 products to cart.
		Merchandise.navigate_ToPage_ByPageNumber("1");
		Integer randomProduct1 = ThreadLocalRandom.current().nextInt(1, 48);
		Merchandise.add_To_Cart_ByNumber(randomProduct1.toString());
		
		Navigation.navigate_Merchandise_Tech();
		Merchandise.navigate_ToPage_ByPageNumber("2");
		Integer randomProduct2 = ThreadLocalRandom.current().nextInt(2, 48);
		Merchandise.add_To_Cart_ByNumber(randomProduct2.toString());
		
		Navigation.navigate_Merchandise_Beauty();
		Merchandise.navigate_ToPage_ByPageNumber("1");
		Integer randomProduct3 = ThreadLocalRandom.current().nextInt(1, 48);
		Merchandise.add_To_Cart_ByNumber(randomProduct3.toString());

		productList_ShoppingCart.data = ShoppingCart.get_Table_Data().data;
		pricingDetails_ShoppingCart = ShoppingCart.retrieve_PricingDetails();
		ShoppingCart.click_ProceedToCheckout();
		CheckoutPage_ShippingInfo.navigate_To_NextPage();
		CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		
		productList_ReviewOrder.data = CheckoutPage_ReviewOrder.get_Table_Data().data;
		pricingDetails_ReviewOrder = CheckoutPage_ReviewOrder.retrieve_PricingDetails();
		CheckoutPage_ReviewOrder.get_Table_Data();
		
		
		CheckoutPage_ReviewOrder.click_PlaceYourOrder();
		CheckoutPage_ReviewOrder.submit_Order();
		
		productList_OrderConfirmation.data = CheckoutPage_ReviewOrder.get_Table_Data().data;
		pricingDetails_OrderConfirmation = CheckoutPage_ReviewOrder.retrieve_PricingDetails();
		CheckoutPage_ReviewOrder.get_Table_Data();
		
		
		
		//Before looping through lists, it is important to make sure that there are values in the lists.
		//This will prevent false positives from occurring.
		BaseUI.verify_true_AndLog(productList_ShoppingCart.data.size() > 0, "Table was greater than 0", "Table was empty.");
		BaseUI.verify_true_AndLog(productList_ShoppingCart.data.size() == productList_ReviewOrder.data.size(), "Table sizes matched.", "Tables did not match in size.");
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_PricingDetails_RetailPrice() throws Exception {
			BaseUI.baseStringCompare("Retail Price", pricingDetails_ShoppingCart.retailPrice,
					pricingDetails_ReviewOrder.retailPrice);
	}
	
	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_PricingDetails_Points() throws Exception {
			BaseUI.baseStringCompare("Points", pricingDetails_ShoppingCart.points,
					pricingDetails_ReviewOrder.points);
	}
	
	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_PricingDetails_PriceAfterPoints() throws Exception {
			BaseUI.baseStringCompare("Price After Points", pricingDetails_ShoppingCart.priceAfterPoints,
					pricingDetails_ReviewOrder.priceAfterPoints);
	}
	
	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_PricingDetails_Savings() throws Exception {
			BaseUI.baseStringCompare("Savings", pricingDetails_ShoppingCart.savings_Percent,
					pricingDetails_ReviewOrder.savings_Percent);
	}
	
	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_PricingDetails_ShippingCost() throws Exception {
			BaseUI.baseStringCompare("Shipping Cost", pricingDetails_ShoppingCart.shipping_Cost,
					pricingDetails_ReviewOrder.shipping_Cost);
	}
	
	//Order total is different on Review Order page because the tax amount is figured out then.
	//Will need to add the tax amount to the Order Total that we got from Shopping Cart page and then compare.
	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_PricingDetails_OrderTotal() throws Exception {
		
		pricingDetails_ShoppingCart.order_Total = pricingDetails_ShoppingCart.order_Total.replace("$", "");
		pricingDetails_ReviewOrder.order_Total = pricingDetails_ReviewOrder.order_Total.replace("$", "");
		pricingDetails_ReviewOrder.tax_Cost = pricingDetails_ReviewOrder.tax_Cost.replace("$", "");
		
		String newShoppingCartTotal = Double.toString(Double.parseDouble(pricingDetails_ShoppingCart.order_Total) + 
				Double.parseDouble(pricingDetails_ReviewOrder.tax_Cost));
		
			BaseUI.baseStringCompare("Shipping Cost", newShoppingCartTotal,
					pricingDetails_ReviewOrder.order_Total);
	}
	
	
	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_productName_Matches() throws Exception {
		BaseUI.verify_TableColumns_Match("Product Name", productList_ShoppingCart.data, productList_ReviewOrder.data);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_productQuantity_Matches() throws Exception {
		BaseUI.verify_TableColumns_Match("quantity", productList_ShoppingCart.data, productList_ReviewOrder.data);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_productPricingRetail_Matches() throws Exception {
		BaseUI.verify_TableColumns_Match("pricing_Retail", productList_ShoppingCart.data, productList_ReviewOrder.data);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_productPricingPoints_Matches() throws Exception {
		BaseUI.verify_TableColumns_Match("pricing_Points", productList_ShoppingCart.data, productList_ReviewOrder.data);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_productPricingYouPay_Matches() throws Exception {
		BaseUI.verify_TableColumns_Match("pricing_YouPay", productList_ShoppingCart.data, productList_ReviewOrder.data);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step16_productPricingYouPay_MatchesOrderConfirmation() throws Exception {
		BaseUI.verify_TableColumns_Match("pricing_YouPay", productList_ShoppingCart.data, productList_OrderConfirmation.data);
	}
	
	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}
}
