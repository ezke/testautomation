package dRewards.tests_ignored;

import java.util.HashMap;

import dRewards.pageControls.AuctionTile;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account_OrderAndShippingDetails_Page;
import dRewards.pages.Auction_Details;
import dRewards.pages.Auctions;
import dRewards.pages.Auctions_Checkout_OrderComplete;
import dRewards.pages.Auctions_Checkout_OrderReview;
import dRewards.pages.Auctions_Checkout_Shipping;
import dRewards.pages.Auctions_MyAuctions;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewAuction_Admin;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import dRewards.pages_Administration.AddNewAuction_SubAuctionModal_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Auction_BackAndForth_PointsBid_ClaimNow extends BaseTest {

	private WebDriver originalDriver;
	private WebDriver secondBidderDriver;
	private String original_Browser;

	private String auctionName = "AutomationTest";
	private Double startingBid = 500.00;
	private Double currentBid = 500.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;
	// String productSku = "aucdysonDC44demo";
	private String productSku = "auc46samsungsmarthdtvDEMO";
	// String expectedProductName = "DC44 Animal Digital Slim Cordless Vacuum
	// with Handheld Tool Kit - DEMO ONLY";
	private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
	private Boolean isDemo = true;

	private String expectedEndTime = "";
	// Auction time in Minutes
	private Integer auctionTimeToLast = 4;
	private HashMap<String, String> shippingInfo;
	private HashMap<String, String> auctionEndInfo;
	private String retailPrice;

	private String user1_previousPoints;
	private String user2_previousPoints;
	private String orderNumber;
	private TableData bidHistory;

	@BeforeClass
	public void setup_method() throws Exception {

		bidHistory = new TableData();
		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		auctionName += randomNumber;

//		DRewards_Utilities.catalog_upload();
//		DRewards_Utilities.catalog_upload_AndWait();
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		original_Browser = Browser.currentBrowser;

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			auctionTimeToLast += 1;
		}

		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction(auctionName, RedemptionTypes.FullRedemption, startingBid, extendTime,
				productSku, AuctionTypes.Standard, isDemo);
		expectedEndTime = Auctions.return_Auction_TimeLeftFormat_forCheckout(auctionTimeToLast);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Auctions_AllAuctions();
		user1_previousPoints = Navigation.returnPoints();
	}

	// validation for this is weak.
	@Test(priority = 9, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_StartBidding() throws Exception {
		AuctionTile auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.click_DetailsButton();
		Auction_Details.verify_StartBidding();
		Auction_Details.verify_TimeLeft_ContainsCorrectFormat();
	}

	@Test(priority = 10, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_1stBidSuccessful() throws Exception {
		currentBid +=1;
		retailPrice = Auction_Details.return_RetailPrice();
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.click_place_Bid();
		Auction_Details.verify_WinningAuction();
		Navigation.verify_pointsMatchExpected(Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
	}

	@Test(priority = 20, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_2ndBidSuccessful_SecondaryUser() throws Exception {
		

		// Set up Second Bidder
		originalDriver = Browser.driver;
		ClientDataRetrieval.set_secondaryUser_Info(original_Browser);
		Browser.openBrowser(ClientDataRetrieval.url, "chrome");
		secondBidderDriver = Browser.driver;
		LoginPage.login(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
		Navigation.navigate_Auctions_AllAuctions();
		AuctionTile auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, currentBid);
		currentBid += 1;
		
		user2_previousPoints = Navigation.returnPoints();
		auctionTile.click_DetailsButton();
		Auction_Details.click_place_Bid();
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(false, String.valueOf(currentBid)));
		Auction_Details.verify_WinningAuction();
		Navigation.verify_pointsMatchExpected(Navigation.return_ExpectedPoints_AsString(user2_previousPoints, currentBid.toString()));
	}

	@Test(priority = 30, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_1stBidder_WasOutbid() throws Exception {
		Browser.driver = originalDriver;
		Auction_Details.verify_Outbid(user1_previousPoints);
		BaseUI.baseStringCompare("Points match previous count.", user1_previousPoints, Navigation.returnPoints());
	}

	@Test(priority = 40, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_1stBidder_CounterBidSuccessful() throws Exception {
		currentBid += 1;
		Browser.driver = originalDriver;
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.click_place_Bid();
		Auction_Details.verify_WinningAuction();
		Navigation.verify_pointsMatchExpected(Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
	}

	@Test(priority = 50, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_2ndBidder_WasOutbid() throws Exception {
		Browser.driver = secondBidderDriver;
		Auction_Details.verify_Outbid(user2_previousPoints);
	}

	@Test(priority = 60, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_ReviewBidHistory() throws Exception {
		Integer numberOfBidders = 2;
		Integer numberOfBids = 3;

		Browser.driver = originalDriver;
		BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", numberOfBids.toString());
		Auction_Details.click_View_BidHistory();
		Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
	}

	@Test(priority = 70, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Close_BidHistory_Window() throws Exception {
		Auction_Details.click_Close_BidHistoryWindow();
		BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
	}

	@Test(priority = 80, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_PopupAppears() throws Exception {
		//currentBid += 1;
		Browser.driver = originalDriver;
		Auction_Details.wait_For_End_OfAuction(auctionTimeToLast * 60);

		Auction_Details.wait_For_ClaimPopup_ToAppear();
		Auction_Details.verify_AuctionWon_PopupAppears();
	}

	@Test(priority = 90, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_WonBannerAppears() throws Exception {
		Auction_Details.verify_AuctionWon_Banner_Appears();
	}

	@Test(priority = 100, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_ClaimAuction_ShippingPage() throws Exception {
		Auction_Details.click_ClaimNow_InWinningPopup();
		if (BaseUI.elementExists("auctShip_1stAddress_FirstName", null, null)) {
			shippingInfo = Auctions_Checkout_Shipping.return_ShippingInfo();
		} else {
			shippingInfo = CheckoutPage_ShippingInfo.return_DefaultShippingInfo();
		}
		auctionEndInfo = Auctions_Checkout_Shipping.return_AuctionEndInfo();
		Auctions_Checkout_Shipping.verify_AuctionInfo(expectedProductName, currentBid.toString(), expectedEndTime);
	}

	@Test(priority = 110, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_ClaimAuction_Checkout_OrderReview() throws Exception {
		Auctions_Checkout_Shipping.navigate_To_NextPage();
		// Auctions_Checkout_Shipping.click_Use_FirstShippingAddress();
		// Auctions_Checkout_Shipping.click_ReviewYourOrder_Button();
		Auctions_Checkout_OrderReview.verify_OrderReview_Info(shippingInfo, expectedProductName, retailPrice,
				expectedEndTime, currentBid.toString());
	}

	@Test(priority = 120, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_ClaimAuction_Checkout_OrderComplete() throws Exception {
		Auctions_Checkout_OrderReview.click_AcceptConditions_AndCompleteOrder();
		orderNumber = Auctions_Checkout_OrderComplete.return_OrderNumber();
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
		Auctions_Checkout_OrderComplete.verify_Order_Info(shippingInfo, expectedProductName, retailPrice,
				expectedEndTime, currentBid.toString());
	}

	@Test(priority = 130, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Step28_Verify_OrderComplete_PrintReceiptVisible() throws Exception {
		Auctions_Checkout_OrderComplete.verify_PrintYourReceiptButton_VisibleAndEnabled();
	}

	// Test for non-allstateCash
	@Test(priority = 130, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_ClaimAuction_Checkout_OrderComplete_PointsCorrect()
			throws Exception {
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
	}

	@Test(priority = 140, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_Validate_MyAuction() throws Exception {
		Navigation.navigate_Auctions_MyAuctions();
		Auctions_MyAuctions.verify_AuctionWon_AndAlreadyClaimed(expectedProductName, currentBid,
				auctionEndInfo.get("auctionEnded"));
	}

	@Test(priority = 150, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Navigate_AuctionEnds_FirstUserWins_Validate_MyAuction_ViewDetails() throws Exception {
		Auctions_MyAuctions.click_LastAuction_ViewDetails();
	}

	@Test(priority = 160, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_History_OrderAndShippingDetails_PaymentInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_Auction_FullRedemption_PaymentInformation(orderNumber,
				BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM-dd-yyyy"),
				BaseUI.convertDouble_ToString_ForCurrency(currentBid));
	}

	@Test(priority = 170, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_History_OrderAndShippingDetails_ShippingInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_OrderDetails_ShippingInfo_NotShipped();
	}

	@Test(priority = 170, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_History_OrderAndShippingDetails_ProductInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_ProductInfo(expectedProductName, 1,
				Double.parseDouble(retailPrice.replace("$", "").replace(",", "")));
	}

	@Test(priority = 170, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_History_OrderAndShippingDetails_CustomerAddressInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(shippingInfo.get("clientName"),
				shippingInfo.get("address"), shippingInfo.get("city"), shippingInfo.get("state"),
				shippingInfo.get("country"), shippingInfo.get("zip"));
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondBidderDriver != null) {
			Browser.driver = secondBidderDriver;
			Browser.closeBrowser();
		}
	}

}
