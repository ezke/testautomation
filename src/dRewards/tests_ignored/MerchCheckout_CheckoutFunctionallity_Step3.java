package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class MerchCheckout_CheckoutFunctionallity_Step3 extends BaseTest {
	
	private String orderID;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Home_viaLogo();
		Navigation.navigate_Merchandise_Tech();

		// add 3 products to cart.
		Merchandise.add_To_Cart_ByNumber("1");
		Navigation.navigate_Merchandise_Tech();
		Merchandise.add_To_Cart_ByNumber("2");

		ShoppingCart.click_ProceedToCheckout();
		CheckoutPage_ShippingInfo.navigate_To_NextPage();
		CheckoutPage_PaymentInfo.navigate_toReviewPayment();
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step17_Click_EditShippingInformation_TakesUser_toShippingInfoPage() throws Exception {
		CheckoutPage_ReviewOrder.click_EditShipping();
		BaseUI.verifyElementExists("checkout_ShippingPage_Title", null, null);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step17_Click_EditPaymentInformation_TakesUser_toPaymentInfoPage() throws Exception {
		CheckoutPage_ReviewOrder.click_EditBilling();
		BaseUI.verifyElementExists("checkoutPay_PaymentInfoTitle", null, null);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (!BaseUI.elementExists("checkout_ReviewOrder_Title", null, null)
				|| !BaseUI.elementAppears(Locator.lookupElement("checkout_ReviewOrder_Title"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.click_ProceedToCheckout();
			CheckoutPage_ShippingInfo.navigate_To_NextPage();
			CheckoutPage_PaymentInfo.navigate_toReviewPayment();
			CheckoutPage_ReviewOrder.submit_Order();
			BaseUI.verifyElementAppearsByString("oederConfNumber");
			//String ConfirmationOrderId = BaseUI.getTextFromField(Locator.lookupRequiredElement("oederConfNumber")) ;
			
			  orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
				OrderConfirmationPage.click_ContinueShopping();
				///BaseUI.verifyElementDoesNotAppear(Locator.lookupRequiredElement("orderConf_Title"));
				Navigation.navigate_Account();
				Thread.sleep(50);
				
				//String myAccountOrderId = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderIdNum")) ;
				//BaseUI.baseStringCompare("orderIdNum", ConfirmationOrderId, myAccountOrderId);
				
				
		} 
	}
			
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}
}
