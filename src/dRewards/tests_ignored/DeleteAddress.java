package dRewards.tests_ignored;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.Browser;

public class DeleteAddress extends BaseTest {

	@BeforeClass
	public void setup_method()throws Exception{
		Browser.openBrowser(ClientDataRetrieval.url);
		//LaunchPage.click_sign_On();
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}
		
		
	@Test (groups = { "regression_Tests", "all_tests" })
	public void Delete_Address() throws Exception {
		Navigation.navigate_Account_UpdateAddressBook();
		Account_UpdateAddressBook.delete_Address();
		Browser.closeBrowser();		
	}
}
