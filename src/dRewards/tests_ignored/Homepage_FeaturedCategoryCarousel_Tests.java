package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Homepage_FeaturedCategoryCarousel_Tests extends BaseTest {
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		
		Navigation_Admin.navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		if (BaseUI.pageSourceContainsString("14014")) {
			String attributeValue = BaseUI.getTextFromField(
					Locator.lookupElement("clients_ProgramAttributes_Value_ByAttributeID", "14014", null));
			if (attributeValue.equals("True")) {
				// if the client have attribute True it will skip all the test
				throw new SkipException(
						"Skipping all the tests as this attribute value 14014 is set to True, this client will not see Daily Category Carousel in Homepage");
			}
		}
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" })
	public void TC3214_HomePage_Step19_Verify_FeaturedCategoriesCarousel_Header_Appears() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_Header"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_Header"));
		BaseUI.verifyElementHasExpectedPartialText("homepage_FeaturedCatCarousel_Header", "Featured Categories");
	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" })
	public void TC3214_HomePage_Step19_Verify_FeaturedCategories_RightNavigationArrow_Appears() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_Header"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_RightNavigation"));

	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" })
	public void TC3214_HomePage_Step19_Verify_FeaturedCategories_LeftNavigationArrow_Appears() throws Exception {

		Homepage.click_FeaturedCategories_Carousel_RightNavigation_Arrow();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_LeftNavigation"));

	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" })
	public void TC3214_HomePage_Step19_Verify_FeaturedCategories_Clicks_Entire_LeftNavigationArrow() throws Exception {

		Homepage.click_FeaturedCategories_Carousel_RightNavigation_Arrow();
		Homepage.click_NavigateFeaturedCategoryCarouselArrow_ToFarLeft();

	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" })
	public void TC3214_HomePage_Step19_Verify_FeaturedCategories_Clicks_Entire_RightNavigationArrow() throws Exception {

		Homepage.click_NavigateFeaturedCategoryCarouselArrow_ToFarRight();

	}

	@Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" })
	public void TC3214_HomePage_Step19_Verify_ClickingFirstItem_NavigatesTo_LandingPage() throws Exception {

		Homepage.verify_Clicking_FeaturedCatCarousel_FirstItem_NavigatesTo_LandingPage();

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}
