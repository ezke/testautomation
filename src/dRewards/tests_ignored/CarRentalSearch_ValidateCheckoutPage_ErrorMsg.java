package dRewards.tests_ignored;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.CarRentalSearch_Checkout;
import dRewards.pages.CarRental_ModifySearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class CarRentalSearch_ValidateCheckoutPage_ErrorMsg extends BaseTest {

	private CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();
	private TableData carRentalData;
	private String cityNameText;
	private String airportLocation;
	private String avgBaseRate;
	private String points_OrSmartDollars;
	private String youPay;
	private String totalWithTax;

	@BeforeClass
	public void setup_method() throws Exception {

		searchCriteria.airportCode_CityName = "PBI";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(30);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(32);
		searchCriteria.carClassOptionDisplayed = "Full Size";
		searchCriteria.pickUpTimeDisplayed = "11:00 AM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";

		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_CarRental_Page();
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
		carRentalData = CarRental_ModifySearchResults.get_Table_Data_Avis();

		cityNameText = BaseUI
				.get_Attribute_FromField(Locator.lookupRequiredElement("carRental_ModifySearchResults_Location"), "innerText")
				.replaceAll("Location:", "").trim();
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		CarRental_ModifySearchResults.click_List_Btn();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_MapTab"));

		airportLocation = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Avis_AirportAddress"))
				.trim();

		HashMap<String, String> carRentalRow = carRentalData.data.get(0);

		avgBaseRate = carRentalRow.get("avgBaseRate").replace("$", "");
		points_OrSmartDollars = carRentalRow.get("points_Or_SmartDollars").replace("%", "");
		youPay = carRentalRow.get("youPay").replace("$", "");
		totalWithTax = carRentalRow.get("totalWithTax").replace("$", "");

	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_LandingPage_VerifyModifySearchResults_Header() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ModifySearchResults_Header"));
		BaseUI.verifyElementHasExpectedPartialText("carRental_ModifySearchResults_Header", "Modify Search Results");
	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_LandingPage_ClickReserveLink() throws Exception {

		if (BaseUI.elementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_AvisLogo"))) {

			CarRental_ModifySearchResults.click_ReserveLink("carRentalSearchResult_ListViewResult_Avis_ReserveBtn");
		} else if (BaseUI.elementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_BudgetLogo"))) {

			CarRental_ModifySearchResults.click_ReserveLink("carRentalSearchResult_ListViewResult_budget_ReserveBtn");
		} else {
			System.out.println("Search Results does not have List of Car Rental avaialble");
		}

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_HeaderTitle"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_HeaderTitle",
				"Car Rentals | Additional Information");
	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_LandingPage_VerifyCarRentalTitleHeader() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle"));
		String headerTitle = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle"));

		if (BaseUI.elementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle"))
				&& headerTitle.contains("Avis")) {

			// BaseUI.verify_true_AndLog(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle").isDisplayed(),
			// "Header title " + headerTitle + " is Displayed", "Header title "
			// + headerTitle + " is not Displayed");
			BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_YourCarRentalTitle",
					"Your Avis Rental Car");
		} else if (BaseUI.elementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle"))
				&& headerTitle.contains("Budget")) {

			// BaseUI.verify_true_AndLog(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle").isDisplayed(),
			// "Header title " + headerTitle + " is Displayed", "Header title "
			// + headerTitle + " is not Displayed");
			BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_YourCarRentalTitle",
					"Your Budget Rental Car");
		} else {

			BaseUI.verify_true_AndLog(
					Locator.lookupElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle").isDisplayed(),
					"Header title is Displayed", "Header title is not Displayed");
		}
	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyCarClass() throws Exception {

		String carClassDisplayed_CheckoutPage = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarClass"))
				.replaceAll("Car Class: ", "").trim();
		BaseUI.verify_true_AndLog(carClassDisplayed_CheckoutPage.contains(searchCriteria.carClassOptionDisplayed),
				"Car Class displayed on checkout page matches " + searchCriteria.carClassOptionDisplayed,
				"Car Class displayed on checkout page does not match " + searchCriteria.carClassOptionDisplayed);
	}

	@Test(priority = 33, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyCarImageDisplayed() throws Exception {

		String carNameDetail = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarNameDetail"));
		String carSpecificationDetails = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarModalDetail"));
		BaseUI.verify_true_AndLog(
				(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarNameDetail").isDisplayed()
						&& (Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarModalDetail").isDisplayed())),
				"Car Details " + carNameDetail + " " + carSpecificationDetails + " " + "are displayed",
				"Car Details " + carNameDetail + " " + carSpecificationDetails + " " + "are not displayed");
	}

	@Test(priority = 34, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyCarDetails() throws Exception {

		BaseUI.verify_true_AndLog((Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarImage").isDisplayed()),
				"Car Rental image is displayed", "Car Rental image is not displayed");
	}

	@Test(priority = 35, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyCarRentalCompanyLogo() throws Exception {

		BaseUI.verify_true_AndLog(
				(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarRentalCompanyLogo").isDisplayed()),
				"Car Rental Company logo is displayed", "Car Rental Company logo is not displayed");
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyPickUpDateAndTime() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PickUpDateAndTime"));
		String pickUpDate_Time = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PickUpDateAndTime"));
		String pickUpDate = BaseUI.return_Date_AsDifferentFormat(searchCriteria.pickUpDateDisplayed, "MM/dd/yyyy",
				"MMMM dd, yyyy");

		BaseUI.verify_true_AndLog(pickUpDate_Time.contains(pickUpDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page match "
						+ (pickUpDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page does not match "
						+ (pickUpDate + "-" + searchCriteria.pickUpTimeDisplayed));
	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyReturnDateAndTime() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_ReturnDateAndTime"));
		String returnDate_Time = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_ReturnDateAndTime"));
		String returnDate = BaseUI.return_Date_AsDifferentFormat(searchCriteria.returnDateDisplayed, "MM/dd/yyyy",
				"MMMM dd, yyyy");

		BaseUI.verify_true_AndLog(returnDate_Time.contains(returnDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page match "
						+ (returnDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page does not match "
						+ (returnDate + "-" + searchCriteria.pickUpTimeDisplayed));
	}

	@Test(priority = 55, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyAirplaneIcon() throws Exception {

		BaseUI.verifyElementAppears(
				Locator.lookupElement("carRentalSearchResult_CheckoutPage_AirplaneIcon_PickUpLocation"));
		BaseUI.verifyElementAppears(
				Locator.lookupElement("carRentalSearchResult_CheckoutPage_AirplaneIcon_ReturnLocation"));
	}

	@Test(priority = 60, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyAirport_PickUpAddress() throws Exception {

		BaseUI.verifyElementHasExpectedPartialText("carRentalSearchResult_CheckoutPage_PickUpLocation",
				airportLocation);
	}

	@Test(priority = 70, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyAirport_ReturnAddress() throws Exception {

		BaseUI.verifyElementHasExpectedPartialText("carRentalSearchResult_CheckoutPage_ReturnLocation",
				airportLocation);
	}

	@Test(priority = 75, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyYourRates_Header() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourRates_Header"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_YourRates_Header", "Your Rates");
	}

	@Test(priority = 80, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyPriceDetails() throws Exception {

		CarRentalSearch_Checkout.verify_AvisCarRentalCheckout_PriceDetails(avgBaseRate, points_OrSmartDollars, youPay,
				totalWithTax);
	}

	@Test(priority = 85, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_ClickTaxes_OrFeesLink() throws Exception {

		CarRentalSearch_Checkout.clickTaxes_FeesLink();

		BaseUI.verifyElementAppears(
				Locator.lookupElement("carRentalSearchResult_CheckoutPage_TaxAndFees_AirportConcFee"));
		CarRentalSearch_Checkout.verify_CarRental_TaxesOrFees_Subcategory();
	}

	@Test(priority = 86, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyDriverInfo_Header() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_Header"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_DriverInfo_Header",
				"Driver Information");
	}

	@Test(priority = 87, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyDriverInfo_Note() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_Note"));
		BaseUI.verify_true_AndLog(
				(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_Note").isDisplayed()),
				"Driver information note " + Locator.lookupElement("carRentalSearchResult_CheckoutPage_DriverInfo_Note")
						.getAttribute("innerText") + " is displayed",
				"Driver information note " + Locator.lookupElement("carRentalSearchResult_CheckoutPage_DriverInfo_Note")
						.getAttribute("innerText") + " is not displayed");
	}

	@Test(priority = 88, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyFlightInfo_Header() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FlightInfo_Header"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_FlightInfo_Header",
				"Flight Information (Recommended)");
	}

	@Test(priority = 89, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyFlightInfo_Note() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FlightInfo_Note"));
		BaseUI.verify_true_AndLog(
				(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FlightInfo_Note").isDisplayed()),
				"Flight Information note " + Locator.lookupElement("carRentalSearchResult_CheckoutPage_FlightInfo_Note")
						.getAttribute("innerText") + " is displayed",
				"Flight Information note " + Locator.lookupElement("carRentalSearchResult_CheckoutPage_FlightInfo_Note")
						.getAttribute("innerText") + " is not displayed");
	}

	@Test(priority = 90, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyAdditionalOptions_Header() throws Exception {

		BaseUI.verifyElementAppears(
				Locator.lookupElement("carRentalSearchResult_CheckoutPage_AdditionalOptions_Header"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_AdditionalOptions_Header",
				"Additional Options");
	}

	@Test(priority = 92, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step28_CarRentalSearchResult_CheckoutPage_VerifyAdditionalOptions_Note() throws Exception {

		CarRentalSearch_Checkout.clickAdditionalOptionsPlus_ToExpand();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AdditionalOptions_Note"));
		BaseUI.verify_true_AndLog(
				(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AdditionalOptions_Note").isDisplayed()),
				"Additional Options note "
						+ Locator.lookupElement("carRentalSearchResult_CheckoutPage_AdditionalOptions_Note")
								.getAttribute("innerText")
						+ " is displayed",
				"Additional Options note "
						+ Locator.lookupElement("carRentalSearchResult_CheckoutPage_AdditionalOptions_Note")
								.getAttribute("innerText")
						+ " is not displayed");
	}

	@Test(priority = 93, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_CheckoutPage_VerifyErrorMessaging() throws Exception {

		CarRentalSearch_Checkout.clickCarRentalSearchResult_CheckoutPage_ReserveCarBtn();
		CarRentalSearch_Checkout.verifyCarRentalSearchResult_CheckoutPage_ErrorMessaging();

	}

	@Test(priority = 94, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_CheckoutPage_VerifyPoliciesLnk() throws Exception {

		String currentWindow = Browser.driver.getWindowHandle();
		CarRentalSearch_Checkout.verifyCarRentalSearchResult_CheckoutPage_VerifyPoliciesLnk();
		BaseUI.close_ExtraWindows(currentWindow);

	}

	@Test(priority = 95, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_CheckoutPage_VerifyFaqsLnk() throws Exception {

		String currentWindow = Browser.driver.getWindowHandle();
		CarRentalSearch_Checkout.verifyCarRentalSearchResult_CheckoutPage_VerifyFaqsLnk();
		BaseUI.close_ExtraWindows(currentWindow);

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}
