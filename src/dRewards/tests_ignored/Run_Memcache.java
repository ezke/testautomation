package dRewards.tests_ignored;

import org.testng.annotations.Test;

import dRewards.data.ClientDataRetrieval;
import dRewards.pages.DRewards_Utilities;

public class Run_Memcache {

	@Test
	public void runMemCache() throws Exception{
		ClientDataRetrieval.set_TestData_XML();
		DRewards_Utilities.flush_Memcache();
	}	
}
