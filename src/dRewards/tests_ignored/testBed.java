package dRewards.tests_ignored;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.Account_UpdatePaymentOptions;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ShippingInfo;

import dRewards.pages.LaunchPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;

import utils.Locator;
import utils.ResultWriter;


public class testBed extends BaseTest {


	@BeforeClass
	public void setup_method() throws Exception {
		// dataForTest.get("stuff");
		// Browser.openBrowser(GlobalVariables.url_cityEasyDeals_QA);
		Browser.openBrowser(ClientDataRetrieval.url);
		LaunchPage.click_sign_On();

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Home_viaLogo();
		Navigation.navigate_Merchandise_Tech();

		// add 2 products to cart.
		Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 48));
		// ShoppingCart.click_ProceedToCheckout();
	}

	@Test
	public void Add_PaymentOption() throws Exception {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.click_ProceedToCheckout();
			CheckoutPage_ShippingInfo.navigate_To_NextPage();
			CheckoutPage_PaymentInfo.add_NewPaymentOption("MasterCard", "5105105105105100", "12", "22", "211",
					"qa automation", "1225 broken sound", "United States", "Boca Raton", "FL", "33481");
			CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
			Thread.sleep(100);

	}

	@Test
	public void remove_AllPaymentOptions() throws Exception {
		Navigation.navigate_Account();
		Account.clear_PaymentHistory();
	}
	
	@Test
	public void remove_DeleteSpecificPaymentOption() throws Exception {
		String cardholder = "QA Automation";
		Navigation.navigate_Account_PaymentOptions();
		Account_UpdatePaymentOptions.delete_PaymentOption_ByCardholder(cardholder);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		// Navigation.navigate_ShoppingCart();
		// ShoppingCart.click_ProceedToCheckout();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			// Make sure to delete the address I added for this test if it still
			// exists.
			Navigation.navigate_Account_UpdateAddressBook();
			if (BaseUI.elementExists("accnt_addr_DeleteButton_ByPassedInText", "Giallombardo", null)) {
				Account_UpdateAddressBook.delete_Address_ByPassedInName("Giallombardo");
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
