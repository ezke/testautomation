package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.HotelsAndCondos_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.TravelCarousel_ResultPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class HotelSearch2 extends BaseTest {

	// testchange
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		// LaunchPage.click_sign_On();
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// Navigation.navigate_Hotel_Landingpage();
	}

	// AARP, AllState, AllState Cash
	//Not Smart Rewards, Citi, TVC
	// Steps: 1, 2
	@Test(groups = {"not_citi", "not_tvc", "not_smart", "regression_Tests", "all_tests"} )
	public void TC3135_Step1_Navigate_Homepage_SearchForHotel() throws Exception {
		String location = "Orlando, FL";
		String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
		String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
		String numberOfAdults = "2";
		String numberOfChildren = "2";
		String ageOfChildren = "5";

		Homepage.verify_HotelSearch_ElementsPresent();
		Homepage.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren, ageOfChildren);
		HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
				numberOfChildren, ageOfChildren);
	}

	@Test(groups = {"regression_Tests", "all_tests"} )
	public void TC3135_Step6_Navigate_Travel_HomePage_SearchForHotel() throws Exception {
		Navigation.navigate_Hotel_Landingpage();
		HotelsAndCondos.verify_HotelSearch_ElementsPresent();
	}
	
	
	// Smart Rewards, TVC
	@Test(groups = {"not_citi", "not_allstate", "not_allstateCash", "regression_Tests", "all_tests"} )
	public void TC3135_Navigate_Homepage_SmokeTest_SearchForHotel_HasTravelThingy() throws Exception {
		String location = "Orlando, FL";
		String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
		String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
		String numberOfAdults = "2";
		String numberOfChildren = "2";
		String ageOfChildren = "5";

		Homepage.verify_TravelSearch_ElementsPresent();
		Homepage.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren, ageOfChildren);
		HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
				numberOfChildren, ageOfChildren);
	}

	// Not citi deals
	@Test(groups = {"not_citi", "regression_Tests", "all_tests"} )
	public void TC3135_Step4_Homepage_Search_WithoutSelecting_Criteria() throws Exception {
		Homepage.verify_DestinationRequired_Error();
	}

	// Citi deals.  Citi only has the carousel.
	@Test(groups = {"not_tvc", "not_aarp", "not_allstate", "not_allstateCash", "regression_Tests", "all_tests"} )
	public void TC3135_Step3_Homepage_Carousel() throws Exception {
		String carouselIndex = "3";
		String selectedCarouselText = BaseUI.get_NonVisible_TextFromField(Locator.lookupRequiredElement("ttd_ProductName_ByIndex",carouselIndex, null));
		String selectedCarouselImageAlt = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("ttd_ProductImage_ByIndex", carouselIndex, null), "alt");
		BaseUI.baseStringCompare("Carousel Text matches image alt.", selectedCarouselText, selectedCarouselImageAlt);
		
		Homepage.navigate_TTD_right_Carousel();
		Homepage.navigate_TTD_right_Carousel();
		Homepage.navigate_TTD_right_Carousel();
		Homepage.navigate_TTD_left_Carousel();
		//Should be on the third Carousel item.
		BaseUI.click(Locator.lookupRequiredElement("ttd_ViewDealLink_ByIndex", carouselIndex, null));
		TravelCarousel_ResultPage.validate_TravelCarousel_ResultPage(selectedCarouselText);
		
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}
