package dRewards.tests_ignored;

import dRewards.pageControls.AuctionTile;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Auction_Details;
import dRewards.pages.Auctions;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewAuction_Admin;
import dRewards.pages_Administration.AddNewAuction_SubAuctionModal_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Auction_NegativeTests extends BaseTest {

	private WebDriver originalDriver;
	private WebDriver secondBidderDriver;
	private String original_Browser;
	private String second_Browser = "chrome";

	private String auctionName = "AutomationTest";
	private Double startingBid = 500.00;
	private Double usdAmount = 3.99;
	private Double currentBid = 500.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;

	private String productSku = "auc46samsungsmarthdtvDEMO";
	private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
	private Boolean isDemo = true;
	private Double pointsIncrease = 1.0;
	private Integer auctionTimeToLast = 4;
	private AuctionTile auctionTile = AuctionTile.empty();

	private String user1_previousPoints;
	private String user2_previousPoints;
	private TableData bidHistory;

	@BeforeClass
	public void setup_method() throws Exception {
		//MemcacheFlush.flush_Memcache();
		
		bidHistory = new TableData();
		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		auctionName += randomNumber;
//		DRewards_Utilities.flush_Memcache();

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			auctionTimeToLast += 1;
		}

		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction_DualCurrency(auctionName, RedemptionTypes.DualCurrency, startingBid,
				usdAmount, extendTime, productSku, AuctionTypes.Standard, isDemo);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Auctions_AllAuctions();
		user1_previousPoints = Navigation.returnPoints();

	}

	@Test(priority = 10, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionFoundInList_AndAccurate() throws Exception {
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.verify_AuctionDetails_Info(expectedProductName, currentBid, true);
	}

	@Test(priority = 11, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_StartBidding() throws Exception {
		auctionTile.click_DetailsButton();
		Auction_Details.verify_StartBidding();
	}

	@Test(priority = 20, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_PlaceBid_Launches_DualCurrency_Modal() throws Exception {
		currentBid += pointsIncrease;
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.click_place_Bid();
		Auction_Details.verify_DualCurrency_AcceptModal(usdAmount);
	}

	@Test(priority = 30, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_DualCurrency_AcceptModal_ClickIAgree() throws Exception {
		Auction_Details.click_IAgree_DualCurrencyModal();
		Auction_Details.verify_DualCurrency_AcceptModal_NotVisible();
	}

	@Test(priority = 40, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_BidSuccessful() throws Exception {
		
		Auction_Details.verify_WinningAuction();
		Auction_Details.verify_BidButtonDisabled();
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
	}

	@Test(priority = 50, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_SecondBidder_AuctionFoundAndAccurate() throws Exception {
		// Set up Second Bidder
		originalDriver = Browser.driver;
		original_Browser = Browser.currentBrowser;
		ClientDataRetrieval.set_secondaryUser_Info(original_Browser);
		Browser.openBrowser(ClientDataRetrieval.url, second_Browser);
		// This will save the driver for switching back and forth.
		secondBidderDriver = Browser.driver;

		LoginPage.login(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
		Navigation.navigate_Auctions_AllAuctions();
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, currentBid);

		user2_previousPoints = Navigation.returnPoints();
		auctionTile.verify_AuctionDetails_Info(expectedProductName, currentBid, true);

		// bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(false,
		// String.valueOf(currentBid + 1)));
	}

	@Test(priority = 51, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_SecondBidder_AuctionDetails() throws Exception {
		currentBid += pointsIncrease;
		Browser.changeDriver(secondBidderDriver, second_Browser);
		auctionTile.click_DetailsButton();
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(false, String.valueOf(currentBid)));
		Auction_Details.click_place_Bid();
		Auction_Details.click_IAgree_DualCurrencyModal();

		Auction_Details.verify_WinningAuction();
		Auction_Details.verify_BidButtonDisabled();
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user2_previousPoints, currentBid.toString()));
	}
	
	@Test(priority = 60, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_1stBidder_WasOutbid() throws Exception {
		Browser.changeDriver(originalDriver, original_Browser);
		Auction_Details.verify_Outbid(user1_previousPoints);
		BaseUI.baseStringCompare("Points match previous count.", user1_previousPoints, Navigation.returnPoints());
	}

	@Test(priority = 70, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_1stBidder_CounterBidSuccessful() throws Exception {
		currentBid += 1;
		Browser.changeDriver(originalDriver, original_Browser);
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.click_place_Bid();
		Auction_Details.verify_WinningAuction();
		Navigation.verify_pointsMatchExpected(Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
	}

	@Test(priority = 80, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_2ndBidder_WasOutbid() throws Exception {
		Browser.changeDriver(secondBidderDriver, second_Browser);
		Auction_Details.verify_Outbid(user2_previousPoints);
	}

	@Test(priority = 90, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_ReviewBidHistory() throws Exception {
		Integer numberOfBidders = 2;
		Integer numberOfBids = 3;

		Browser.changeDriver(originalDriver, original_Browser);
		BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", numberOfBids.toString());
		Auction_Details.click_View_BidHistory();
		Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
	}

	@Test(priority = 100, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Close_BidHistory_Window() throws Exception {
		Auction_Details.click_Close_BidHistoryWindow();
		BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
	}

	@Test(priority = 110, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_PopupAppears() throws Exception {
		//currentBid += 1;
		Browser.changeDriver(originalDriver, original_Browser);
		Auction_Details.wait_For_End_OfAuction(auctionTimeToLast * 60);

		Auction_Details.wait_For_ClaimPopup_ToAppear();
		Auction_Details.verify_AuctionWon_PopupAppears();
	}

	@Test(priority = 120, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_AuctionEnds_FirstUserWins_ClickRemindMeLater() throws Exception {
		Auction_Details.click_RemindMeLater_InWinningPopup();
		Auction_Details.verify_AuctionWon_NotVisible();
	}
	
	@Test(priority = 130, alwaysRun = true, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
	public void TC3249_Verify_SignOut_SignIn_AuctionMessage() throws Exception {
		Navigation.navigate_Logout();
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Thread.sleep(100);
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondBidderDriver != null) {
			Browser.driver = secondBidderDriver;
			Browser.closeBrowser();
		}
	}

}// End of Class
