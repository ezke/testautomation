package dRewards.tests_ignored;

import dRewards.data.BaseTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;

public class CarRentalSearch extends BaseTest {
    //String firstDate = "Fri";

    private String firstDate = "";
    private String friday = "Fi";

    @BeforeMethod(alwaysRun = true)
    public void setup_method() throws Exception {

        //String fri = "fri";
        //	BaseUI.getDateAsString_InRelationToTodaysDate(0, "EE"); //Returning date 01/Fri/2018

        //firstDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(firstDate, "MM/dd/yyyy", "MM/dd/yyyy", 1);

        if (BaseUI.getDateAsString_InRelationToTodaysDate(0, "EE").equals(friday)) {

            firstDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
            firstDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(firstDate, "MM/dd/yyyy", "MM/dd/yyyy", 2);

        } else
            firstDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
        firstDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(firstDate, "MM/dd/yyyy", "MM/dd/yyyy", 1);


        ClientDataRetrieval.set_TestData_XML();
        Browser.openBrowser(ClientDataRetrieval.url);
        //LaunchPage.click_sign_On();
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test
    public void Navigate_CarRental_page() throws Exception {
        Navigation.navigate_CarRental_Page();
        Browser.closeBrowser();
    }
}
