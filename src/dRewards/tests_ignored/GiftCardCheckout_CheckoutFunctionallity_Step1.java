package dRewards.tests_ignored;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

//Tests Step 7 of the Test Case PDF Eber sent me.
public class GiftCardCheckout_CheckoutFunctionallity_Step1 extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		// dataForTest.get("stuff");
		// Browser.openBrowser(GlobalVariables.url_cityEasyDeals_QA);
		Browser.openBrowser(ClientDataRetrieval.url);		
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// LoginPage.login_citiEasyDeals(GlobalVariables.citiEasyDeals_UserName,
		// GlobalVariables.citiEasyDeals_Password);

		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Home_viaLogo();
		Navigation.navigate_GiftCard_All();
		

		// add 2 products to cart.
		Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 4));
		Navigation.navigate_Merchandise_Tech();
		Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 4));
		ShoppingCart.click_ProceedToCheckout();
	}

	// @Test (groups = { "regression_Tests", "all_tests" })
	// public void
	// Step8_NegativeTest_ClickContinue_WithoutEnteringAnything_ShippingError()
	// throws Exception {
	// CheckoutPage_ShippingInfo.click_ContinueToPaymentInfo();
	// CheckoutPage_ShippingInfo.verify_TopErrorDisplayed("Please Select A Ship
	// To Address");
	// }	
	
	
	@Test (groups = { "regression_Tests", "all_tests" }) 
	public void TC3235_Step8_NegativeTest_AddNewDeliveryAddress_ErrorMessage_FirstName() throws Exception {
		if (BaseUI.elementExists("checkout_AddNewShippingAddress", null, null)) {
			CheckoutPage_ShippingInfo.continue_To_NextPage();
			CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Recipient's First Name",
					"Please enter your first name");

		}
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Recipient's First Name", "Please enter your first name");

	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step8_NegativeTest_AddNewDeliveryAddress_ErrorMessage_LastName() throws Exception {
		CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Recipient's Last Name", "Please enter your last name");
	}

	@Test (groups = { "regression_Tests", "all_tests" })// (priority = 2)
	public void TC3235_Step8_NegativeTest_AddNewDeliveryAddress_ErrorMessage_AddressLine1() throws Exception {
		CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Address Line 1", "Shipping address is required");
	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step8_NegativeTest_AddNewDeliveryAddress_ErrorMessage_City() throws Exception {
		CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("City", "Shipping city is required");
	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step8_NegativeTest_AddNewDeliveryAddress_ErrorMessage_ZipCode() throws Exception {
		CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Zip/Postal Code", "Please enter your zip code");
	}

	@Test (groups = { "regression_Tests", "all_tests" })// (priority = 2)
	public void TC3235_Step8_NegativeTest_AddNewDeliveryAddress_ErrorMessage_Phone() throws Exception {
		CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Phone", "Please enter a valid phone number");
	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_shippingAddress1() throws Exception {
		String address1 = "1225 broken sound";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct(
				"checkout_ExistingAddress_ShippingAddress1", address1, true);
	}

	@Test (groups = { "regression_Tests", "all_tests" })//(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_shippingAddress2() throws Exception {
		String address2 = "";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct(
				"checkout_ExistingAddress_ShippingAddress2", address2, false);
	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_city() throws Exception {
		String city = "Boca Raton";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_ExistingAddress_City", city,
				true);
	}

	@Test (groups = { "regression_Tests", "all_tests" })//(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_state() throws Exception {
		String state = "FL";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_ExistingAddress_State",
				state, true);
	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_zipCode() throws Exception {
		String zipCode = "33487";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_ExistingAddress_ZipCode",
				zipCode, false);
		WebElement zipCodeMasked = Locator.lookupElement("checkout_ExistingAddress_ZipCodeMasked", "QaAuto", null);
		BaseUI.verifyElementAppears(zipCodeMasked);
		BaseUI.verifyElementHasExpectedText(zipCodeMasked, "*****");

	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_country() throws Exception {
		String country = "US";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_ExistingAddress_Country",
				country, true);
	}

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step9_Validate_ExistingAddress_phoneNumber() throws Exception {
		String phoneNumber = "9544561212";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_ExistingAddress_Phone",
				phoneNumber, false);
		WebElement phoneNumberMasked = Locator.lookupElement("checkout_ExistingAddress_PhoneMasked", "QAauto", null);
		BaseUI.verifyElementAppears(phoneNumberMasked);
		BaseUI.verifyElementHasExpectedText(phoneNumberMasked, "******1212");
	}

	/*@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 2)
	public void TC3235_Step10_User_AbleToSelect_ExistingAddress_andContinueToPaymentInfo() throws Exception {
		if (BaseUI.elementExists("checkout_ShipToExistingAddress_Radio", null, null)){
		CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio();
		CheckoutPage_ShippingInfo.click_ContinueToPaymentInfo();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkoutPay_PaymentInfoTitle"));
	
	}	
		CheckoutPage_ShippingInfo.click_ContinueToPaymentInfo();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkoutPay_PaymentInfoTitle"));
	
    }  */

	@Test (groups = { "regression_Tests", "all_tests" }) //(priority = 1)
	public void TC3235_Step10_User_AbleToSelect_NewShippingAddress_andContinueToPaymentInfo() throws Exception {
		String firstName = "QAauto";
		String lastName = "Auto";
		String address1 = "1225 broken sound";
		String city = "Boca Raton";
		String state = "FL";
		String zip = "33487";
		String phone = "9544561212";

		CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, state, zip, phone);
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkoutPay_PaymentInfoTitle"));
		Navigation.navigate_Account_UpdateAddressBook();
		//Account_UpdateAddressBook.delete_Address_ByPassedInName("QAauto");
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_ShoppingCart();
		ShoppingCart.click_ProceedToCheckout();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			// Make sure to delete the address I added for this test if it still
			// exists.
			Navigation.navigate_Account_UpdateAddressBook();
			if (BaseUI.elementExists("accnt_addr_DeleteButton_ByPassedInText", "QAauto", null)) {
				Account_UpdateAddressBook.delete_Address_ByPassedInName("QAauto");
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
