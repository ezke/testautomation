package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


/**
 * Add Sweepstakes from Admin site.
 * 
 * Note: At this time we do not have access to Rundeck site.
 * So cannot flush the newly created sweepstakes. And due to 
 * this Sweepstakes will not be available on webapp.
 *
 */
public class HomePage_VerifySweepstakes_Tests extends BaseTest {
		

	@BeforeClass
	public void setup_method() throws Exception {

		String randomNum = BaseUI.random_NumberAsString(1, 50);
		String sweepstakesName = "autotestDAF"+randomNum;
		String sweepstakesDescription = sweepstakesName;
		//SweepstakesTypes sweepstakesType = "Standard";
		Boolean isFeaturedSweepstakes = true;
		String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(30);
		String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(30);
		String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(30);
		String ticketCost = "2.0";
		String cumulativeCap = "1000000";
		String docID = "183";
		String productSku = "ssdemochevycamerored";
		String position = "1";
		String dCash = "0";
		String quantity = "0";
		String numDays_Claim = "10";
		String ticketQty = "1";
		String ticketLimit = "1000";
		String option = "Ticket Purchase";

		//Open DR Admin site
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		
		//Expand Sweepstakes menu and click on Create Program sub menu
		Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
		Navigation_Admin.navigate_Sweepstakes_CreateProgram();
		
		//Add New Sweepstakes from Admin site
		AddNewSweepstakes_Admin.Add_NewSweepstakes(sweepstakesName, sweepstakesDescription, Client_Info_Admin.return_ProgramIDs(), 
		                      isFeaturedSweepstakes, startDate, endDate, drawingDate, displayEndDate, ticketCost, cumulativeCap, docID, productSku, position,
		                      dCash, quantity, numDays_Claim, ticketQty, ticketLimit, option);
		
		//Verify once New Sweepstakes is created we are navigated to Search Sweepstakes Program search page
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();
		
		//Click Activate button to activate newly created Sweepstakes
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.click_ActivateButton();
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_Sweepstakes_IsActivated();
		
		//Navigate to DR web app
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}
	
	
	
	@Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_tvc", "not_citi" })
	public void TC3214_HomePage_Step8_VerifySweepstakesTitleOnHomePage() throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.SmartRewards))
		{
		    Navigation.navigate_SweepstakesLink_currentSweepstakes();
		    BaseUI.verifyElementAppears(Locator.lookupRequiredElement("verify_CurrentSweepstakesPageTitle"));
		    BaseUI.verifyElementHasExpectedText("verify_CurrentSweepstakesPageTitle", "Current Sweepstakes");		    
		}
		else{
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("verify_Sweepstakes_IMG"));
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("verify_Sweepstakes_headerOnHomePage"));
		    BaseUI.verifyElementHasExpectedText("verify_Sweepstakes_headerOnHomePage", "Sweepstakes");
		}
	}
	

	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {

		} finally {
			Browser.closeBrowser();
		}
	}
}
