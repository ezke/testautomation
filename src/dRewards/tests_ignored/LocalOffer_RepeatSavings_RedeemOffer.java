package dRewards.tests_ignored;

import java.text.MessageFormat;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account_History;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class LocalOffer_RepeatSavings_RedeemOffer extends BaseTest {

	private String pointsBefore;
	private String pointsAfter;
	private String transactionDescription;
	private String category = "Dining";
	private String zipcode = "33130";
	private String randomNumber;
	private TableData localOfferData;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		Navigation.navigate_LocalOffers_SearchPage();
		LocalOffers_LandingPage.localOffer_Search_WithZipcode(zipcode, category);
		Navigation.click_SeeLocalOffer_SearchPageButton();
	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step_15and16_Redeem_RepeatSavings_Offer() throws Exception {

		pointsBefore = Navigation.returnPoints();

		LocalOffers_LandingPage.redeem_RepeatSavings_Offer();

		Browser.closeBrowser();
		Browser.openBrowser(ClientDataRetrieval.url);

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		pointsAfter = Navigation.returnPoints();

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verify_true_AndLog(!pointsBefore.equals(pointsAfter),
					MessageFormat.format("Before points {0} were deducted to {1} after offer redeemed.", pointsBefore,
							pointsAfter),
					MessageFormat.format("Before points {0} were Not deducted to {1} after offer redeemed.",
							pointsBefore, pointsAfter));
		}
	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step_15and16_Redeem_RepeatSavings_AccountPage_VerifyPoints() throws Exception {

		Navigation.navigate_Account();
		transactionDescription = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionDescription"));
		String pointsRedeemed = Navigation.return_ExpectedPoints_AsString(pointsBefore, pointsAfter);
		Account_History.verify_LocalOffer_PointsRedeemed(pointsRedeemed);
	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step_15and16_Redeem_RepeatSavings_RedeemSameOffer_SetToLocalOfferPage()
			throws Exception {
		
		// setting up to redeem same offer again
		Navigation.navigate_LocalOffers_SearchPage();
		LocalOffers_LandingPage.localOffer_Search_WithZipcode(zipcode, category);
		Navigation.click_SeeLocalOffer_SearchPageButton();
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step_15and16_Redeem_RepeatSavings_RedeemSameOffer_Again() throws Exception {
		
		pointsBefore = Navigation.returnPoints();
		LocalOffers_LandingPage.redeem_RepeatSaving_OfferAgain(transactionDescription, category);

		Browser.closeBrowser();
		Browser.openBrowser(ClientDataRetrieval.url);

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		pointsAfter = Navigation.returnPoints();

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verify_true_AndLog(!pointsBefore.equals(pointsAfter),
					MessageFormat.format("Before points {0} were deducted to {1} after offer redeemed.", pointsBefore,
							pointsAfter),
					MessageFormat.format("Before points {0} were Not deducted to {1} after offer redeemed.",
							pointsBefore, pointsAfter));
		}
	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step_15and16_Redeem_RepeatSavings_RedeemSameOffer_Again_AccountPage_VerifyPoints()
			throws Exception {

		Navigation.navigate_Account();
		String pointsRedeemed = Navigation.return_ExpectedPoints_AsString(pointsBefore, pointsAfter);
		Account_History.verify_LocalOffer_PointsRedeemed(pointsRedeemed);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}
}
