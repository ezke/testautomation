package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Merchandise_BrandCarousel_Tests extends BaseTest {
	
	private String brandName_Legacy = "Vera Bradley";

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		Navigation.navigate_Merchandise();	
	}
	
	@Test(priority = 10, groups = { "regression_Tests", "all_tests",})
	public void TC3243_Merchandise_BrandCarousel_Verify_LeftNavigation_Arrow(){
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_BrandCarousel_LeftNavigation_Arrow"));
	}
	
	@Test(priority = 20, groups = { "regression_Tests", "all_tests",})
	public void TC3243_Merchandise_BrandCarousel_Verify_RightNavigation_Arrow(){
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_BrandCarousel_RightNavigation_Arrow"));
	}
	
	@Test(priority = 30, groups = { "regression_Tests", "all_tests",})
	public void TC3243_Merchandise_BrandCarousel_Verify_Clicking_RightArrow_Navigates_ToFarRight() throws Exception{
		Merchandise.click_Merchandise_BrandCarousel_RightArrow_Navigates_ToFarRight();
	}
	
	@Test(priority = 40, groups = { "regression_Tests", "all_tests",})
	public void TC3243_Merchandise_BrandCarousel_Verify_Clicking_LeftArrow_Navigates_ToFarLeft() throws Exception{
		Merchandise.click_Merchandise_BrandCarousel_LeftArrow_Navigates_ToFarLeft();
	}
	
	@Test(priority = 50, groups = { "regression_Tests", "all_tests",})
	public void TC3243_Merchandise_BrandCarousel_Verify_Clicking_Brand_Navigates_ToLandingPage() throws Exception{
		Merchandise
		.clickAndVerify_Merchandise_BrandCarousel_Displayed_NavigatesTo_LandingPage("merch_BrandCarousel_BrandImage", brandName_Legacy );
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();;
		Navigation.navigate_Merchandise();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
	
	
}
