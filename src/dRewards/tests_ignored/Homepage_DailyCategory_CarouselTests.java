package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Homepage_DailyCategory_CarouselTests extends BaseTest {

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		
		Navigation_Admin.navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		if (BaseUI.pageSourceContainsString("14014")) {
			String attributeValue = BaseUI.getTextFromField(
					Locator.lookupElement("clients_ProgramAttributes_Value_ByAttributeID", "14014", null));
			if (attributeValue.equals("True")) {
				// if the client have attribute True it will skip all the test
				throw new SkipException(
						"Skipping all the tests as this attribute value 14014 is set to True, this client will not see Daily Category Carousel in Homepage");
			}
		}

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_Verify_DailyCategorySpecial_Header_Appears() throws Exception {
		Homepage.verify_DailyCarousel_Header_Appears();

	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_Verify_DailyCategorySpecial_SeeMoreLink_Appears() throws Exception {
		Homepage.verify_DailyCarousel_SeeMoreLink_Appears();

	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_Verify_DailyCategorySpecial_RightNavigationArrow_Appears() throws Exception {
		Homepage.verify_DailyCarousel_RightNavigationArrow_Appears();
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_Verify_DailyCategorySpecial_LeftNavigationArrow_Appears() throws Exception {
		Homepage.verify_DailyCarousel_LeftNavigationArrow_Appears();
	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_DailyCategorySpecial_Carousel_Click_Entire_RightNavigationArrow()
			throws Exception {
		Homepage.verify_DailyCategoryCarousel_Clicks_EntireRightNavigation_Arrow();

	}

	@Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_DailyCategorySpecial_Carousel_Click_Entire_LeftNavigationArrow()
			throws Exception {
		Homepage.verify_DailyCategoryCarousel_Clicks_EntireLeftNavigation_Arrow();
	}

	@Test(priority = 70, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc" })
	public void TC3214_HomePage_Step19_Verify_DailyCategorySpecial_ClickFirstItem_NavigatesTo_LandingPage()
			throws Exception {
		Homepage.verify_DailyCategoryCarousel_ClickingFirstItem_NavigatesTo_LandingPage();
	}

	@Test(priority = 80, groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_citi" })
	public void TC3214_HomePage_Step19_Verify_DailyCategorySpecial_CategoryList_Exists_LandingPage() throws Exception {
		if (BaseUI.elementExists("homepage_DailyCatCarousel_Modal", null, null)) {
			Homepage.verify_CategoryDisplayedInDailyCategoryCarousel_Exists_In_SeeMorePage();
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
}
