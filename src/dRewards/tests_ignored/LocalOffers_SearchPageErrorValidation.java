package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.Browser;
import utils.ResultWriter;

public class LocalOffers_SearchPageErrorValidation extends BaseTest {

	//String adminUserAddressText;
	//String adminUserCityText;
	//String adminUserState;

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		Navigation.navigate_LocalOffers_SearchPage();
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step21_Verify_LocalOffer_SearchPagerTitle() throws Exception {

		LocalOffers_LandingPage.verify_LocalOffer_Title_Appears();
	}

	@Test(priority = 2, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step21_Verify_LocalOffer_SearchPagerErrorValidation() throws Exception {

		LocalOffers_LandingPage.verify_LocalOffer_SearchPageError();

	}

	@Test(priority = 3, groups = { "regression_Tests", "all_tests" })
	public void TC3247_LocalOffer_Step5_Verify_LocalOffer_SearchPagerResult() throws Exception {
		//String textToContain = "Local Offers";
		String state = "FL";
		String city = "Fort Lauderdale";
		String category = "All Offers";

		LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state, city, category);
		Navigation.click_SeeLocalOffer_SearchPageButton();
		//LocalOffers_LandingPage.verify_Breadcrumb_ContainsText(textToContain);
		String[] breadcrumbArray = {"Home", "Local Offers"};
		Navigation.verify_BreadcrumbList(breadcrumbArray);

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_LocalOffers_SearchPage();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}
}