package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.Account;
import dRewards.pages.Account_History;
import dRewards.pages.Account_OrderAndShippingDetails_Page;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class GiftCard_Checkout_Different_BillingAndShippingAddress_Test extends BaseTest {

	// Variables for Billing Info
	private String firstName = "QA";
	private String lastName = "Auto";
	private String billingAddress1 = "1225 Broken Sound";
	private String billingCity = "Boca Raton";
	private String billingState = "FL";
	private String billingZip = "33487";
	private String billingPhone = "9544151992";

	// Variables for Shipping Info
	private String shippingAddress1 = "1225 Broken Sound";
	private String shippingCity = "Boca Raton";
	private String shippingState = "FL";
	private String shippingZip = "33487";
	private String shippingPhone = "7152456542";

	private String ccType = "MasterCard";
	private String ccNumber = "5105105105105100";
	private String cvvCode = "211";
	private String expire_month = "12";
	private String expire_year = "22";
	private String country = "United States";

	// Variables for product info/pay info
	private TableData shoppingCartData;
	private PricingDetails pricingDetails;
	private String points_Before_Checkout;
	private String points_After_Checkout;
	private String orderID;

	private String secondRun_allstate_Cash = "$5.50";
	private String salesTaxCost;

	// Variable for Points available and Allstate Cash?

	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all items from Shopping Cart.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}

		// Logic to remove all Shipping Info
		Navigation.navigate_Account_UpdateAddressBook();
		Account_UpdateAddressBook.delete_Address();

		// Logic to remove all Billing Info.
		Navigation.navigate_Account();
		Account.clear_PaymentHistory();

		// Add 2 products to cart.
		Navigation.navigate_GiftCard_Dining();
		Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(1, 1));
		Merchandise.add_To_Cart_GiftCard(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));
		/// Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1,
		/// Merchandise.get_ItemCount_OnPage()));
		Navigation.navigate_GiftCard_Shopping();
		Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(1, 1));
		Merchandise.add_To_Cart_GiftCard(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));

		// Add logic here to pull product info from Shopping Cart page
		pricingDetails = ShoppingCart.retrieve_PricingDetails();
		pricingDetails.set_SavingsAmount_Citi();
		shoppingCartData = ShoppingCart.get_Table_Data();
		points_Before_Checkout = Navigation.returnPoints();

		ShoppingCart.click_ProceedToCheckout();

	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_ShippingPage_PriceInfo() throws Exception {
		PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Continue_ToPage_AfterShippingInfo() throws Exception {
		if (CheckoutPage_ShippingInfo.newShippingAddress_Radio_Exists()) {
			CheckoutPage_ShippingInfo.click_AddNewShippingAddress_Radio();
		}

		CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, shippingAddress1, shippingCity,
				shippingState, shippingZip, shippingPhone);
		CheckoutPage_ShippingInfo.check_SaveForLater_Checkbox();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();

		pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
		pricingDetails.add_Taxes_To_OrderTotal();

	}

	@Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
			"not_aarp" })
	public void PT3235_Validate_AllStateCash_PriceInfo() throws Exception {
		PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);
	}

	@Test(priority = 26, groups = { "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
			"not_aarp" })
	public void PT3235_AllStateCash_Continue_ToBillingPage() throws Exception {
		// Allstate Cash was not used for this run through.
		pricingDetails.allstate_Cash = "$0.00";
		CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
		CheckoutPage_AllStateCash.click_Continue_ToNextPage();

	}

	@Test(priority = 27, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_BillingPage_PricingInfo() throws Exception {

		// Verify some stuff
		PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
		}
	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_aarp" })
	public void PT3235_Validate_OrderReviewPage_ProductDetails() throws Exception {
		// Update Payment Info. Save for later use.

		CheckoutPage_PaymentInfo.add_NewPaymentOption(ccType, ccNumber, expire_month, expire_year, cvvCode,
				firstName + " " + lastName, billingAddress1, country, billingCity, billingState, billingZip);
		CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
		CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

		CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 31, groups = { "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
			"not_allstateCash" })
	// ****this TC will failed till this issue get fix --
	// https://www.pivotaltracker.com/n/projects/1268844 --***********
	public void PT3235_Validate_OrderReviewPage_ProductDetails_Aarp() throws Exception {
		// Update Payment Info. Save for later use.

		CheckoutPage_PaymentInfo.add_NewPaymentOption_Redesign(ccType, ccNumber, expire_month, expire_year, cvvCode,
				firstName + " " + lastName);
		CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
		CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

		CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderReviewPage_PriceSummary() throws Exception {
		PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderReviewPage_PaymentInformation() throws Exception {

		CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, billingAddress1, billingCity,
				billingState, billingZip, ccType, ccNumber, expire_month + "/" + expire_year);

		// Add Logic here for Allstate Cash
		CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);

		// CheckoutPage_ReviewOrder.retrieve_PricingDetails();
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderReviewPage_ShippingInformation() throws Exception {
		CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, shippingAddress1, shippingCity,
				shippingState, shippingZip, country, shippingPhone);
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderReviewPage_PricingDetails() throws Exception {

		CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
				pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
				pricingDetails.order_Total, pricingDetails.tax_Cost);

	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderReviewPage_IAgreeInfo() throws Exception {
		CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {
		CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
		CheckoutPage_ReviewOrder.click_PlaceYourOrder();
		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, shippingAddress1, shippingCity,
				shippingState, "*****", country, shippingPhone);
	}

	@Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_citi" })
	public void PT3235_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
		points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
				pricingDetails.points);
		Navigation.verify_pointsMatchExpected(points_After_Checkout);
	}

	@Test(priority = 51, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
		OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, billingAddress1, billingCity,
				billingState, ccType, ccNumber, billingZip);
	}

	@Test(priority = 51, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
		// ****this TC will failed till this issue get fix --
		// https://www.pivotaltracker.com/n/projects/1268844 --***********
		OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 51, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
		PricingDetails pricingDetails_OrderConfirmPage;
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		} else {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
		}
		OrderConfirmationPage.verify_PricingDetails(pricingDetails);
	}

	@Test(priority = 60, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_AccountHistory_Page() throws Exception {
		Navigation.navigate_Account();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase(orderID, pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase(orderID, pricingDetails.points);
		}
	}

	@Test(priority = 70, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, shippingAddress1,
				shippingCity, shippingState, country, shippingZip);
	}

	@Test(priority = 71, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_CancelOrder_Page_ProductInfo() throws Exception {
		// ****this TC will failed till this issue get fix --
		// https://www.pivotaltracker.com/n/projects/1268844 --***********
		Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
	}

	@Test(priority = 72, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_CancelOrder_Page_PaymentInfo() throws Exception {

		String orderDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM-dd-yyyy");

		Double orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
				- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
				- Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));

		Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID, orderDate,
				orderSubtotal, Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", "")),
				pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", "")), 
				pricingDetails.points, ccNumber);
	}

	@Test(priority = 80, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_Cancelled() throws Exception {
		Account.cancel_Order(orderID);
		Navigation.navigate_Logout();
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Account();
		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_Cancelled(pricingDetails, salesTaxCost);
	}

	@Test(priority = 81, groups = { "regression_Tests", "all_tests", "not_citi" })
	public void PT3235_Validate_Points_Reimbursed() throws Exception {
		Navigation.verify_pointsMatchExpected(points_Before_Checkout);
	}

	@Test(priority = 82, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Validate_AccountHistory_OrderCancelled() throws Exception {
		Navigation.navigate_Account();
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase_Cancelled(pricingDetails.points);
		}
	}

	// // These tests are for our second run through the Gift Cards workflow.
	// // This run will be using the shipping and billing information that we
	// used
	// // for the first run through.
	//
	@Test(priority = 100, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info() throws Exception {
		Navigation.navigate_GiftCard_Travel();
		Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(1, 1));
		/// Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1,
		/// Merchandise.get_ItemCount_OnPage()));
		Merchandise.add_To_Cart_GiftCard(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));
		Navigation.navigate_GiftCard_Entertaiment();
		Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(1, 1));
		/// Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1,
		/// Merchandise.get_ItemCount_OnPage()));
		Merchandise.add_To_Cart_GiftCard(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));

		// Add logic here to pull product info from Shopping Cart page
		pricingDetails = ShoppingCart.retrieve_PricingDetails();
		pricingDetails.set_SavingsAmount_Citi();
		shoppingCartData = ShoppingCart.get_Table_Data();
		points_Before_Checkout = Navigation.returnPoints();

		ShoppingCart.click_ProceedToCheckout();
	}

	@Test(priority = 110, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_ShippingPage_PriceInfo() throws Exception {
		PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
	}

	@Test(priority = 120, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_ExistingAddress() throws Exception {

		CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio();
		String expectedAdress = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress1"));
		BaseUI.baseStringCompare("checkout_ExistingAddress_ShippingAddress1", expectedAdress, billingAddress1);
	}

	@Test(priority = 121, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Continue_ExistingPayment() throws Exception {

		CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
		pricingDetails.add_Taxes_To_OrderTotal();

	}

	@Test(priority = 125, groups = { "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
			"not_aarp" })
	public void PT3235_2ndRun_Validate_AllStateCash_PriceInfo() throws Exception {
		PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);

	}

	@Test(priority = 126, groups = { "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
			"not_aarp" })
	public void PT3235_2ndRun_AllStateCash_Continue_ToBillingPage() throws Exception {
		// Allstate Cash was not used for this run through.
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			pricingDetails.allstate_Cash = secondRun_allstate_Cash;
		}
		pricingDetails.subtract_AllstateCash_From_OrderTotal();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_AllStateCash
					.apply_Other_Amount(Double.parseDouble(secondRun_allstate_Cash.replace("$", "").replace(",", "")));
			CheckoutPage_AllStateCash.click_Continue_ToNextPage();
		}

	}

	@Test(priority = 127, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_BillingPage_PricingInfo() throws Exception {
		PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
		}
	}

	@Test(priority = 130, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderReviewPage_ProductDetails() throws Exception {
		// ****this TC will failed till this issue get fix --
		// https://www.pivotaltracker.com/n/projects/1268844 --***********
		// CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
		CheckoutPage_PaymentInfo.click_ExistingPayment_Radio();
		CheckoutPage_PaymentInfo.enter_CVV_Code(cvvCode);
		// Add logic to verify Existing Payment Radio.

		CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

		CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 140, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderReviewPage_PriceSummary() throws Exception {
		PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
	}

	@Test(priority = 140, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderReviewPage_PaymentInformation() throws Exception {
		CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, billingAddress1, billingCity,
				billingState, billingZip, ccType, ccNumber, expire_month + "/" + expire_year);

		// Add Logic here for Allstate Cash
		CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
	}

	@Test(priority = 140, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderReviewPage_ShippingInformation() throws Exception {
		CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, shippingAddress1, shippingCity,
				shippingState, shippingZip, country, shippingPhone);
	}

	@Test(priority = 140, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderReviewPage_PricingDetails() throws Exception {
		CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
				pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
				pricingDetails.order_Total, pricingDetails.tax_Cost);

	}

	@Test(priority = 140, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderReviewPage_IAgreeInfo() throws Exception {
		CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
	}

	@Test(priority = 150, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {
		CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
		CheckoutPage_ReviewOrder.click_PlaceYourOrder();
		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, shippingAddress1, shippingCity,
				shippingState, "*****", country, shippingPhone);
	}

	@Test(priority = 151, groups = { "regression_Tests", "all_tests", "not_citi" })
	public void PT3235_2ndRun_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
		points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
				pricingDetails.points);
		Navigation.verify_pointsMatchExpected(points_After_Checkout);
	}

	@Test(priority = 151, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
		OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, billingAddress1, billingCity,
				billingState, ccType, ccNumber, billingZip);
	}

	@Test(priority = 151, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
		// ****this TC will failed till this issue get fix --
		// https://www.pivotaltracker.com/n/projects/1268844 --***********
		OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 151, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
		PricingDetails pricingDetails_OrderConfirmPage;
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		} else {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		}
		OrderConfirmationPage.verify_PricingDetails(pricingDetails);
	}

	@Test(priority = 160, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_AccountHistory_Page() throws Exception {
		Navigation.navigate_Account();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase(orderID, pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase(orderID, pricingDetails.points);
		}
	}

	@Test(priority = 170, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, shippingAddress1,
				shippingCity, shippingState, country, shippingZip);
	}

	@Test(priority = 171, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_CancelOrder_Page_ProductInfo() throws Exception {
		// ****this TC will failed till this issue get fix --
		// https://www.pivotaltracker.com/n/projects/1268844 --***********
		Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
	}

	@Test(priority = 180, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_Cancelled() throws Exception {
		Account.cancel_Order(orderID);
		Navigation.navigate_Logout();
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Account();
		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_Cancelled(pricingDetails, salesTaxCost);
	}

	@Test(priority = 181, groups = { "regression_Tests", "all_tests", "not_citi" })
	public void PT3235_2ndRun_Validate_Points_Reimbursed() throws Exception {
		Navigation.verify_pointsMatchExpected(points_Before_Checkout);
	}

	@Test(priority = 182, groups = { "regression_Tests", "all_tests" })
	public void PT3235_2ndRun_Validate_AccountHistory_OrderCancelled() throws Exception {
		Navigation.navigate_Account();
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase_Cancelled(pricingDetails.points);
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			// Account.cancel_Order(orderID);
		} finally {
			Browser.closeBrowser();
		}
	}

}// End of Class
