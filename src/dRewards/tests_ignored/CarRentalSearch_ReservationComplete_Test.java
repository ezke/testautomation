package dRewards.tests_ignored;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.CarRentalSearch_Checkout;
import dRewards.pages.CarRentalSearch_ConfirmationPage;
import dRewards.pages.CarRental_ModifySearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class CarRentalSearch_ReservationComplete_Test extends BaseTest {

	private CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();
	private TableData carRentalData;
	private String cityNameText;
	private String airportLocation;
	private String avgBaseRate;
	private String points_OrSmartDollars;
	private String youPay;
	private String totalWithTax;

	// Driver info
	private String driverFirstName = "QA";
	private String driverLastName = "Automation";
	private String driverEmail = "qa.automation@qa.com";
	private String confirmationText = "A confirmation of your reservation has just been sent to the e-mail address provided for the reservation. Please print this page, save a copy for your records and present it at the pickup location.";

	// Airline Info
	private String airlineName = "AMERICAN AIRLINES";
	private String airlineNum = "A1270";
	// Map info
	String airport_PickUpAddressDisplayedMap1 = "600 Terminal Dr, Fort Lauderdale, FL 33315";
	@BeforeClass
	public void setup_method() throws Exception {

		searchCriteria.airportCode_CityName = "FLL";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(30);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(32);
		searchCriteria.carClassOptionDisplayed = "Full Size";
		searchCriteria.pickUpTimeDisplayed = "11:00 AM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";

		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_CarRental_Page();
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
		// carRentalData = CarRental_ModifySearchResults.get_Table_Data_Avis();

		cityNameText = BaseUI
				.get_Attribute_FromField(Locator.lookupElement("carRental_ModifySearchResults_Location"), "innerText")
				.replaceAll("Location:", "").trim();
		BaseUI.scroll_to_element(Locator.lookupElement("carRentalSearchResult_MapButton"));
		CarRental_ModifySearchResults.click_List_Btn();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("carRentalSelect_GoogleMaps_MapTab"));

		airportLocation = BaseUI
				.getTextFromField(Locator.lookupElement("carRentalSearchResult_ListViewResult_Avis_AirportAddress"))
				.trim();
		CarRental_ModifySearchResults.select_CarRentalCompany_Details();

	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step29_CarRentalSearchResult_CheckoutPage_EnterDriverDetails() throws Exception {

		CarRentalSearch_Checkout.EnterDriverDetails(driverFirstName, driverLastName, driverEmail);

	}

	@Test(priority = 15, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step29_CarRentalSearchResult_CheckoutPage_EnterAirlineDetails() throws Exception {

		CarRentalSearch_Checkout.EnterAirlineDetails(airlineName, airlineNum);
	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step29_CarRentalSearchResult_CheckoutPage_ClickReserveCarButton() throws Exception {

		CarRentalSearch_Checkout.clickCarRentalSearchResult_CheckoutPage_ReserveCarBtn();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmMsg"));
	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage()
			throws Exception {

		// BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmMsg"));
		String confirmationMsgText = BaseUI
				.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_ConfirmMsg"));
		BaseUI.verify_true_AndLog(confirmationMsgText.equals("Your reservation has been completed."),
				"Confirmation message " + confirmationMsgText + " is displayed ",
				"Confirmation message " + confirmationMsgText + " is not displayed ");
	}

	@Test(priority = 35, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationNumber()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmationNumText"));
		String confirmationNumText = BaseUI
				.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_ConfirmationNumText"));
		// String confirmationNum =
		// BaseUI.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_ConfirmationNum"));

		BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_ConfirmationNumText",
				confirmationNumText);
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarRentalTitleHeader()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_HeaderTitle"));
		String headerTitle = BaseUI.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_HeaderTitle"));

		BaseUI.verify_true_AndLog(
				headerTitle.equals("Your Avis Rental Car") || headerTitle.equals("Your Budget Rental Car"),
				"Header title " + headerTitle + " is Displayed", "Header title " + headerTitle + " is not Displayed");
	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarClass() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_CarClass"));
		String carClassDisplayed_ReserveConfirmationPage = BaseUI
				.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_CarClass"))
				.replaceAll("Car Class: ", "").trim();
		BaseUI.verify_true_AndLog(
				carClassDisplayed_ReserveConfirmationPage.contains(searchCriteria.carClassOptionDisplayed),
				"Car Class displayed on checkout page matches " + searchCriteria.carClassOptionDisplayed,
				"Car Class displayed on checkout page does not match " + searchCriteria.carClassOptionDisplayed);
	}

	@Test(priority = 52, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarName() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarNameDetail"));
		String carNameDetail = BaseUI
				.getTextFromField(Locator.lookupElement("carRentalSearchResult_CheckoutPage_CarNameDetail"));

		BaseUI.verify_true_AndLog(
				(Locator.lookupElement("carRentalSearchResult_CheckoutPage_CarNameDetail").isDisplayed()),
				"Car Details " + carNameDetail + "are displayed", "Car Details " + carNameDetail + "are not displayed");
	}

	@Test(priority = 53, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarDetails() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_CarModalDetail"));
		String carSpecificationDetails = BaseUI
				.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_CarModalDetail"));

		BaseUI.verify_true_AndLog((Locator.lookupElement("carRental_ReserveConfirmPage_CarModalDetail").isDisplayed()),
				"Car Details " + carSpecificationDetails + " are displayed",
				"Car Details " + carSpecificationDetails + " are not displayed");
	}

	@Test(priority = 54, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarImageDisplayed() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarImage"));
	}

	@Test(priority = 55, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarRentalCompanyLogo()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarRentalCompanyLogo"));
	}

	@Test(priority = 60, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyPickUpDateAndTime() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PickUpDateTime"));
		String pickUpDate_Time = BaseUI
				.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_PickUpDateTime"));
		String pickUpDate = BaseUI.return_Date_AsDifferentFormat(searchCriteria.pickUpDateDisplayed, "MM/dd/yyyy",
				"MMMM dd, yyyy");

		BaseUI.verify_true_AndLog(pickUpDate_Time.contains(pickUpDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page match "
						+ (pickUpDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page does not match "
						+ (pickUpDate + "-" + searchCriteria.pickUpTimeDisplayed));
	}

	@Test(priority = 70, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyReturnDateAndTime() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ReturnDateTime"));
		String returnDate_Time = BaseUI
				.getTextFromField(Locator.lookupElement("carRental_ReserveConfirmPage_ReturnDateTime"));
		String returnDate = BaseUI.return_Date_AsDifferentFormat(searchCriteria.returnDateDisplayed, "MM/dd/yyyy",
				"MMMM dd, yyyy");

		BaseUI.verify_true_AndLog(returnDate_Time.contains(returnDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page match "
						+ (returnDate + "-" + searchCriteria.pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page does not match "
						+ (returnDate + "-" + searchCriteria.pickUpTimeDisplayed));
	}

	@Test(priority = 75, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyAirplaneIcon() throws Exception {

		BaseUI.verifyElementAppears(
				Locator.lookupElement("carRentalSearchResult_CheckoutPage_AirplaneIcon_PickUpLocation"));
		BaseUI.verifyElementAppears(
				Locator.lookupElement("carRentalSearchResult_CheckoutPage_AirplaneIcon_ReturnLocation"));
	}

	@Test(priority = 80, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyAirport_PickUpAddress()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PickUpLocation"));
		BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_PickUpLocation", airportLocation);
	}

	@Test(priority = 85, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyAirport_ReturnAddress()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ReturnLocation"));
		BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_ReturnLocation", airportLocation);
	}

	@Test(priority = 90, groups = { "regression_Tests", "all_tests" })
	public void TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyPrintButton() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PrintYourReceipt_Btn"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("carRental_ReserveConfirmPage_PrintYourReceipt_Btn"));
	}
	
	@Test(priority = 95, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_ReserveConfirmationPage_VerifyPoliciesLnk() throws Exception {

		String currentWindow = Browser.driver.getWindowHandle();
		CarRentalSearch_ConfirmationPage.verifyCarRentalSearchResult_ConfirmationPage_VerifyPoliciesLnk();
		BaseUI.close_ExtraWindows(currentWindow);

	}

	@Test(priority = 100, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_ReserveConfirmationPage_VerifyFaqsLnk() throws Exception {

		String currentWindow = Browser.driver.getWindowHandle();
		CarRentalSearch_ConfirmationPage.verifyCarRentalSearchResult_ConfirmationPage_VerifyFaqsLnk();
		BaseUI.close_ExtraWindows(currentWindow);

	}
	
	@Test(priority = 105, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_ReserveConfirmationPage_VerifyMapFrom() throws Exception {

	
		String currentWindow = Browser.driver.getWindowHandle();
		CarRentalSearch_ConfirmationPage.verifyCarRentalSearchResult_ConfirmationPage_VerifyMapFrom();		
		String airport_PickUpAddressDisplayedMap = BaseUI
				.getTextFromInputBox(Locator.lookupElement("carRental_ReserveConfirmPage_SearchBoxFrom"));
		BaseUI.baseStringCompare("mapAddress", airport_PickUpAddressDisplayedMap1, airport_PickUpAddressDisplayedMap);
		BaseUI.close_ExtraWindows(currentWindow);

		
	

	}
	@Test(priority = 105, groups = { "regression_Tests", "all_tests" })
	public void TC3134_StepN_A_CarRentalSearchResult_ReserveConfirmationPage_VerifyMapTo() throws Exception {

	
		String currentWindow = Browser.driver.getWindowHandle();
		CarRentalSearch_ConfirmationPage.verifyCarRentalSearchResult_ConfirmationPage_VerifyMapTo();		
		String airport_PickUpAddressDisplayedMap = BaseUI
				.getTextFromInputBox(Locator.lookupElement("carRental_ReserveConfirmPage_SearchBoxTo"));
		BaseUI.baseStringCompare("mapAddress", airport_PickUpAddressDisplayedMap1, airport_PickUpAddressDisplayedMap);
		BaseUI.close_ExtraWindows(currentWindow);

		
	

	}

	// @Test(priority = 91, groups = { "regression_Tests", "all_tests" })
	// public void
	// TC3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_ClickPrintButton()
	// throws Exception {
	//
	// BaseUI.click(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PrintYourReceipt_Btn"));
	// BaseUI.switchToAlert_GetText_AndDismiss();
	// Robot r = new Robot();
	// r.keyPress(KeyEvent.VK_CANCEL);
	// r.keyRelease(KeyEvent.VK_CANCEL);
	// }

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			Account.verifyCarRentalDetail_CancelReservation();
		} finally {
			Browser.closeBrowser();
		}
	}
}
