package dRewards.tests_ignored;

import java.util.concurrent.ThreadLocalRandom;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.ShippingAddress;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class MerchCheckout_VerifyAddressInformation_ShippingAndBilling_toOrderConfirmation extends BaseTest {

	private String firstName = "Dominic";

	private ShippingAddress shippingAddress_Checkout_ShippingPage = new ShippingAddress();
	private ShippingAddress shippingAddress_OrderConfirmation = new ShippingAddress();

	private String cardType;
	private String cardNumber;
	private String cardHolder;
	private String expiresOn;

	private String orderID;

	@BeforeClass
	public void setup_method() throws Exception {
		// Open Browser
		Browser.openBrowser(ClientDataRetrieval.url);

		// to get user ansd password infor from config file
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Home_viaLogo();
		Navigation.navigate_Merchandise_Tech();

		// add 1 product to cart.
		Integer randomItem = ThreadLocalRandom.current().nextInt(3, 48);
		Merchandise.add_To_Cart_ByNumber(randomItem.toString());

		ShoppingCart.click_ProceedToCheckout();
		if(BaseUI.pageSourceContainsString(firstName))
		{
			CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio_ByFirstName(firstName);
			shippingAddress_Checkout_ShippingPage = CheckoutPage_ShippingInfo
					.retrieve_ExistingShippingInformation(firstName);
		}
		else
		{
			CheckoutPage_ShippingInfo.continue_To_NextPage();
			String lastName = "Giallombardo";
			String address1 = "1224 broken sound";
			String city = "boca raton";
			String state = "FL";
			String zip = "33481";
			String phone = "7155555555";
			CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, 
					state, zip, phone);
			shippingAddress_Checkout_ShippingPage = new ShippingAddress(firstName, lastName, address1, "", city, state, zip, "United States", phone);
			
		}
	
		// CheckoutPage_ShippingInfo.navigate_ToPaymentInfo();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		if ((BaseUI.elementExists("checkout_ExistingPaymentRows", null, null))) {
			cardType = Locator.lookupElement("checkout_Existing_CreditCard_Type").getText();
			cardNumber = Locator.lookupElement("checkout_Existing_CreditCard_Number").getText();
			cardHolder = Locator.lookupElement("checkout_Existing_CreditCard_Holder").getText();
			expiresOn = Locator.lookupElement("checkout_Existing_CreditCard_ExpirationDate").getText();
			CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		} else {
			CheckoutPage_PaymentInfo.add_newPaymentOption_UsingDefaults();
			cardType = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardType"));
			cardNumber = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardNumber"));
			cardHolder = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_newPayment_CardholderName"));
			expiresOn = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth")) + "/"
					+ BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear"));
			CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
		}
		// CheckoutPage_PaymentInfo.navigate_toReviewPayment();

		CheckoutPage_ReviewOrder.submit_Order();
		shippingAddress_OrderConfirmation = OrderConfirmationPage.retrieve_ShippingAddress();

		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_FirstName_Matches() throws Exception {
		BaseUI.baseStringCompare("First Name", shippingAddress_Checkout_ShippingPage.firstName,
				shippingAddress_OrderConfirmation.firstName);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_LastName_Matches() throws Exception {
		BaseUI.baseStringCompare("Last Name", shippingAddress_Checkout_ShippingPage.lastName,
				shippingAddress_OrderConfirmation.lastName);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_Address_Matches() throws Exception {
		BaseUI.baseStringCompare("Address 1", shippingAddress_Checkout_ShippingPage.address1,
				shippingAddress_OrderConfirmation.address1);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_City_Matches() throws Exception {
		BaseUI.baseStringCompare("City", shippingAddress_Checkout_ShippingPage.city,
				shippingAddress_OrderConfirmation.city);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_State_Matches() throws Exception {
		BaseUI.baseStringCompare("State/Province", shippingAddress_Checkout_ShippingPage.state,
				shippingAddress_OrderConfirmation.state);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_Zip_Matches() throws Exception {
		BaseUI.baseStringCompare("Zip/Postal Code", shippingAddress_Checkout_ShippingPage.zip,
				shippingAddress_OrderConfirmation.zip);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_Country_Matches() throws Exception {
		BaseUI.baseStringCompare("Country", shippingAddress_Checkout_ShippingPage.country,
				shippingAddress_OrderConfirmation.country);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_ShippingInfo_PhoneNumber_Matches() throws Exception {
		BaseUI.baseStringCompare("Phone Number", shippingAddress_Checkout_ShippingPage.phoneNumber,
				shippingAddress_OrderConfirmation.phoneNumber);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_PaymentInfo_Cardholder_Matches() throws Exception {
		String fieldName = "Cardholder Name";
		String cardholder_OrderConfirm = OrderConfirmationPage.get_PaymentInfo_ByText(fieldName);
		BaseUI.baseStringCompare(fieldName, cardHolder, cardholder_OrderConfirm);

	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_PaymentInfo_CardType_Matches() throws Exception {
		String fieldName = "Card Type";
		String cardholder_OrderConfirm = OrderConfirmationPage.get_PaymentInfo_ByText(fieldName);
		BaseUI.baseStringCompare(fieldName, cardType, cardholder_OrderConfirm);

	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_PaymentInfo_CardNumber_Matches() throws Exception {
		String fieldName = "Card Number";
		String cardholder_OrderConfirm = OrderConfirmationPage.get_PaymentInfo_ByText(fieldName);
		BaseUI.baseStringCompare(fieldName, cardNumber, cardholder_OrderConfirm);

	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC2756_Step23_PaymentInfo_CardExpirationDate_Matches() throws Exception {
		String fieldName = "Expiration Date";
		String cardholder_OrderConfirm = OrderConfirmationPage.get_PaymentInfo_ByText(fieldName);
		BaseUI.baseStringCompare(fieldName, expiresOn, cardholder_OrderConfirm);

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			// cancel order
			if (orderID != null) {
				Account.cancel_Order(orderID);
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
