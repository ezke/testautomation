package dRewards.tests_ignored;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

//Tests Step 4-7 of the Test Case PDF Eber sent me.
public class MerchCheckout_AddItemToCart extends BaseTest {
	//
	@BeforeClass
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);		
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Home_viaLogo();

		// Can either navigate to Merchandise page and from there navigate to
		// Tech page.
		// Navigation.navigate_Merchandise();
		// Merchandise.navigate_Category_ByLinkText("Tech");
		// Or we can just hover over Merchandise and select Tech.
		Navigation.navigate_Merchandise_Tech();
		// Merchandise.navigate_ToPage_ByPageNumber("2");

	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step4_Verify_User_CanAdd_ItemToShoppingCart() throws Exception {
		Merchandise.navigate_ToPage_ByPageNumber("4");
		Thread.sleep(2000);
		String productNumber = BaseUI.random_NumberAsString(1, 11);
		// Stored the product text to verify on the Checkout page that element
		// exists.
		
		//String productText = Merchandise.get_ProductText_ByIndexAsString(productNumber);
		//BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_ProductDescriptionLink_ByText", productText, null));
		String productText = Merchandise.add_To_Cart_ByNumber(productNumber);
		ShoppingCart.verify_product_OnShoppingCartPage(productText);
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step5_Verify_User_CanAdd_Item_AndClick_ContinueShoppingButton() throws Exception {
		Merchandise.navigate_ToPage_ByPageNumber("3");
		Thread.sleep(2000);
		String productNumber = BaseUI.random_NumberAsString(1, 11);
		// Stored the product text to verify on the Checkout page that element
		// exists.
		String productText = Merchandise.add_To_Cart_ByNumber(productNumber);

		ShoppingCart.verify_product_OnShoppingCartPage(productText);
		//BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_ProductDescriptionLink_ByText", productText, null));
		ShoppingCart.click_ContinueShopping();
		Merchandise.verify_subCategoryActive("", "Tech");

	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step6_Verify_User_CanAdd_2ndItemToCart() throws Exception {
		String productNumber = BaseUI.random_NumberAsString(1, 11);
		String productNumber2 = BaseUI.random_NumberAsString(1, 11);
		Merchandise.navigate_ToPage_ByPageNumber("4");
		// Stored the product text to verify on the Checkout page that element
		// exists.

		String productText1 = Merchandise.add_To_Cart_ByNumber(productNumber);
		//String productText1 = Merchandise.add_To_Cart_ByNumber("4");
		Navigation.navigate_Merchandise_Tech();
		Merchandise.navigate_ToPage_ByPageNumber("5");
		String productText2 = Merchandise.add_To_Cart_ByNumber(productNumber2);
		//String productText2 = Merchandise.add_To_Cart_ByNumber("26");
		//BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_ProductDescriptionLink_ByText", productText1, null));
		//BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_ProductDescriptionLink_ByText", productText2, null));
		
		ShoppingCart.verify_product_OnShoppingCartPage(productText1);
		ShoppingCart.verify_product_OnShoppingCartPage(productText2);
		
	}

	@Test (groups = { "regression_Tests", "all_tests" })
	public void TC3235_Step7_Verify_User_Can_ProceedToCheckout() throws Exception {
		Merchandise.navigate_ToPage_ByPageNumber("3");
		Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 11));
		Navigation.navigate_Merchandise_Tech();
		Merchandise.navigate_ToPage_ByPageNumber("2");
		Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 11));
		ShoppingCart.click_ProceedToCheckout();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkout_ShippingPage_Title"));
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		Navigation.navigate_Merchandise_Tech();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}
}
