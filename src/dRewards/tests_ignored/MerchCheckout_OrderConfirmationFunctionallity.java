package dRewards.tests_ignored;

import java.util.concurrent.ThreadLocalRandom;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class MerchCheckout_OrderConfirmationFunctionallity extends BaseTest {

	private String mainWindowHandle = "";
	private String orderID;
	
	@BeforeClass
	public void setup_method() throws Exception {
		//Working on getting other browsers to work.
		//Browser.defaultBrowser = "internetexplorer";
		
		Browser.openBrowser(ClientDataRetrieval.url); 
		//Browser.openBrowser(GlobalVariables.url_cityEasyDeals_QA);

		mainWindowHandle = Browser.driver.getWindowHandle();

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		if (BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"))) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.remove_AllItems();
		}
		//Navigation.navigate_Home_viaLogo();
		Navigation.navigate_Home_viaHomeLink();
		Navigation.navigate_Merchandise_Tech();

		// add 1 product to cart.
		Merchandise.navigate_ToPage_ByPageNumber("2");
		Integer randomItem = ThreadLocalRandom.current().nextInt(3, 49);
		Merchandise.add_To_Cart_ByNumber(randomItem.toString());

		ShoppingCart.click_ProceedToCheckout();
		CheckoutPage_ShippingInfo.navigate_To_NextPage();
		CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		CheckoutPage_ReviewOrder.submit_Order();
		BaseUI.verifyElementAppearsByString("oederConfNumber");
		
	    orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
	    
	}

	@Test (groups = { "regression_Tests", "all_tests" }, priority=2)
	public void TC2756_Step28_ContinueShopping(String text1) throws Exception {
		OrderConfirmationPage.click_ContinueShopping();
		///BaseUI.verifyElementDoesNotAppear(Locator.lookupRequiredElement("orderConf_Title"));
		Navigation.navigate_Account();
		Thread.sleep(50);
		//String text1 = "oederConfNumber";
		String text2 = "orderIdNum";
		BaseUI.baseStringCompare("orderIdNum", text1, text2);
		
		System.out.println(text1 + text2);
	}

	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		BaseUI.closeAlertIfPresent();
		
		//Checks for extra windows and closes any that are not the main window.
		BaseUI.close_ExtraWindows(mainWindowHandle);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			// cancel order
			if (orderID != null) {
				Account.cancel_Order(orderID);
				Thread.sleep(3000);
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
