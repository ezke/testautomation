package dRewards.tests_ignored;

import dRewards.data.BaseTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.Browser;


public class HotelSearch extends BaseTest {

	@BeforeMethod(alwaysRun = true)
	public void setup_method()throws Exception{
		ClientDataRetrieval.set_TestData_XML();
		Browser.openBrowser(ClientDataRetrieval.url);
		//LaunchPage.click_sign_On();
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		Thread.sleep(500);
	}
	
	@Test (groups = { "regression_Tests", "all_tests" })	
	public void Navigate_hotel_page() throws Exception {
		
		//Travel_landingPage.navigate_Travel_Landingpage();
		
		
		
		Navigation.navigate_Hotel_Landingpage();
		HotelsAndCondos.hotelSearchInput_withCity("Orlando");
		
		
		Thread.sleep(1000);
		//Travel_landingPage.Left_nav_menu();
		
		
		
		//Navigation.navigate_Hotel_Landingpage();
		//Thread.sleep(500);
		//BaseUI.elementExists("travel_Header", null, null);
		
		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}
	
}
