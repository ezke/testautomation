package dRewards.tests_smoke;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account_History;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.HotelsAndCondos_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HomePageTests extends BaseTest {

	String subCategory_Name = "Handbags";
	String subCategory1_Name = "Jewelry";
	int startPage = 1;
	int endPage = 2;
	String quantity = "2";

	@BeforeClass (alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = { "smoke_tests", "all_tests" })
	public void PT3214_HomePage_Step1_VerifyLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
	}

	@Test(groups = { "smoke_tests", "all_tests" })
	public void PT3214_HomePage_Step2_VerifyAccountLink() throws Exception {
		Account_History.verify_ClickingAccountLink_NavigatesTo_AccountHistoryPage();
	}

	@Test(groups = { "not_tvc", "not_citi", "smoke_tests", "all_tests", "not_redesign", "not_aarpredesign" })
	public void PT3214_HomePage_Step4_Verify_Message_Link() throws Exception {
		Navigation.verify_MessageLink();
	}

	@Test(groups = { "not_citi", "smoke_tests", "all_tests" })
	public void PT3214_HomePage_Step5_Verify_PointsAndCashRewards_Appear() throws Exception {
		Navigation.verify_PointsAndCashRewards_Appear();
	}

	@Test(groups = { "smoke_tests", "all_tests" })
	public void PT3243_Merchandise_LandingPage_Step7_VerifyItemsIn_ShoppingCartIncrease() throws Exception {

		ShoppingCart.verifyShoppingCartDisplayed_OnHomePage(subCategory1_Name, startPage, endPage);
	}

	@Test(groups = { "smoke_tests", "all_tests" })
	public void PT3243_Merchandise_Verify_Merchandise_LandingPage() throws Exception {

		Navigation.navigate_Menu_Submenu("Merchandise", "Merchandise");
		String[] breadcrumbs = new String[] { "Home", "Merchandise" };
		Navigation.verify_BreadcrumbList(breadcrumbs);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
			|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			Merchandise.verify_subCategoryActive("Merchandise", "Merchandise");
		} else {
			Navigation.verify_IntroBanner_Appears(Navigation.getIntroBanner("Merchandise"));
		}

	}
	
	@Test(groups = { "smoke_tests", "all_tests" ,"not_redesign" })
	public void PT3135_Step7_Navigate_Travel_HomePage_SearchForHotel() throws Exception {
		String location = "Savannah, GA";
		String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
		String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
		String numberOfAdults = "2";
		String numberOfChildren = "2";
		String ageOfChildren = "7";

		Navigation.navigate_Hotel_Landingpage();
		HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
				ageOfChildren);
		HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
				numberOfChildren, ageOfChildren);
		//HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(location);
		String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", location};
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearchResult_SubHeader"));
			BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
		}
		Navigation.verify_BreadcrumbList(breadcrumbArray);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
