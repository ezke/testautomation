package dRewards.pages;

import java.util.HashMap;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Auctions_Checkout_OrderReview {

	public static void verify_OrderReview_Info(HashMap<String, String> addressInfo, String productText,
			String productRetailPrice, String expectedEndTime, String expectedAuctionBid) throws Exception {
		BaseUI.verifyElementHasExpectedText("auctOrderRev_AuctionDescriptionName", productText);

		String currency = "Points";
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			currency = "Smart Dollars";
		}
		if (!ClientDataRetrieval.isRedesignClient()) {

			expectedAuctionBid = expectedAuctionBid.split("\\.")[1].length() == 1 ? expectedAuctionBid + "0" : expectedAuctionBid;
		}
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.isRedesignClient()) {
			expectedAuctionBid = expectedAuctionBid.split("\\.")[0];
		}

		BaseUI.verifyElementHasExpectedText("auctOrderRev_WinningBid", expectedAuctionBid + " " + currency);
		if (ClientDataRetrieval.isRedesignClient()) {
			String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
			String endTime = auctInfo.split("\\n")[5];
			expectedEndTime = expectedEndTime.replace((GlobalVariables.auctionTimeZone), "");
			endTime = endTime.replace("at", "");
			String endTimeFormated = BaseUI.return_Date_AsDifferentFormat(endTime,
					"MMMM dd, yyyy hh:mm a", "MMM dd, yyyy h:mm a ");
			BaseUI.baseStringCompare("Auction End Time", expectedEndTime, endTimeFormated);

		} else {
			String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
			String endTime = auctInfo.split("\\n")[2];
			expectedEndTime = "Auction End Date: " + expectedEndTime;
			endTime.replace("0", "");
			BaseUI.baseStringCompare("Auction End Time", expectedEndTime, endTime);
		}


		verify_Address(addressInfo.get("clientName"), addressInfo.get("address"), addressInfo.get("city"),
				addressInfo.get("state"), addressInfo.get("zipCode"), addressInfo.get("country"), addressInfo.get("phone"));

		BaseUI.verifyElementHasExpectedText("auctOrderRev_AuctionDescriptionName", productText);
		BaseUI.verifyElementHasExpectedText("auctOrderRev_RetailPrice", productRetailPrice);

		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupRequiredElement("auctOrderRev_PointsSpent"), expectedAuctionBid+" Points");
		} else {
			BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupRequiredElement("auctOrderRev_PointsSpent"), expectedAuctionBid);
			BaseUI.verifyElementHasExpectedText("auctOrderRev_OrderTotal", "$0.00");
		}
	}
	public static void verify_Auctions_OrderReviewInfo_DualCurrency(HashMap<String, String> addressInfo, String auctionName, String auctionBid, String expectedEndTime,
			Double usdAmount, String productRetailPrice, String tax_Cost) throws Exception {

		auctionBid = auctionBid.split("\\.")[1].length() == 1 ? auctionBid + "0" : auctionBid;

		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARP)
				||ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			auctionBid = auctionBid.split("\\.")[0];
		}
		BaseUI.verifyElementHasExpectedText("auctShip_AuctionDescriptionName", auctionName);
		BaseUI.verifyElementHasExpectedText("auctShip_winningBid",
				auctionBid + " " + ClientDataRetrieval.client_Currency);
		BaseUI.verifyElementHasExpectedText("auctOrderRev_AuctDesc_PaymentDue",
				"$" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount)+ " USD");
		String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
		String endTime = auctInfo.split("\\n")[3];
		String claimDate = auctInfo.split("\\n")[4];
		expectedEndTime = "Auction End Date: " + expectedEndTime;
		BaseUI.baseStringCompare("Auction End Time", expectedEndTime, endTime);
		verify_ClaimByDate(endTime, claimDate);
		
		verify_Address(addressInfo.get("clientName"), addressInfo.get("address"), addressInfo.get("city"),
				addressInfo.get("state"), addressInfo.get("zipCode"), addressInfo.get("country"), addressInfo.get("phone"));
		
		tax_Cost = tax_Cost.replace("$", "").replace(",", "");
		Double taxCost_Double = Double.parseDouble(tax_Cost);
		
		Double newTotal = usdAmount + taxCost_Double;
		String order_Total = "$" + BaseUI.convertDouble_ToString_ForCurrency(newTotal);
		
		BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupRequiredElement("auctOrderRev_PointsSpent"), auctionBid);
		BaseUI.verifyElementHasExpectedText("auctOrderRev_AuctionDescriptionName", auctionName);
		BaseUI.verifyElementHasExpectedText("auctOrderRev_RetailPrice", productRetailPrice);
		BaseUI.verifyElementHasExpectedText("auctOrderRev_PaymentDue",
				"$" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount));
		BaseUI.verifyElementHasExpectedText("auctOrderRev_OrderTotal",  order_Total);
		
	}
	
	public static void verify_ClaimByDate(String auctionEndDateString, String actualClaimDate) throws Exception {
		auctionEndDateString = auctionEndDateString.replace("Auction End Date: ", "");
		String claimDate = Auctions.return_expectedClaimDate(auctionEndDateString);

		BaseUI.baseStringCompare("Claim Date", actualClaimDate, "Claim By: " + claimDate);
	}

	public static void verify_Address(String recipientName, String address, String city, String state, String zip,
			String country, String phone) {

			BaseUI.verifyElementHasExpectedPartialText("auctOrderRev_RecipientName", recipientName);
			BaseUI.verifyElementHasExpectedText("auctOrderRev_City", city);
			BaseUI.verifyElementHasExpectedText("auctOrderRev_Country", country);

		if(!ClientDataRetrieval.isRedesignClient()){
			BaseUI.verifyElementHasExpectedText("auctOrderRev_Address", address);
			BaseUI.verifyElementHasExpectedText("auctOrderRev_State", state);
			BaseUI.verifyElementHasExpectedText("auctOrderRev_Zip", zip);
			BaseUI.verifyElementHasExpectedText("auctOrderRev_Phone", phone);
		}

	}
	
	public static String return_SalesTaxAsString() {
		String salesTaxOnPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctOrderRev_SalesTax"));

		return salesTaxOnPage;
	}
	
	public static String return_Order_Total() {
		String oderTotalOnPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctOrderRev_OrderTotal"));

		return oderTotalOnPage;
	}
	
	public static void verify_AllStateCash(String allstate_Cash){
		if(ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)){
			BaseUI.verifyElementHasExpectedText("auctOrderRev_CashRewards", allstate_Cash);
		}
	}

	public static void click_AcceptConditions_AndCompleteOrder() throws Exception {
		BaseUI.checkCheckbox("auctOrderRev_AgreeToTerms_Checkbox");
		BaseUI.click(Locator.lookupRequiredElement("auctOrderRev_CompleteYourOrder_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("auctOrderRev_CompleteYourOrder_Button",null,null,10);
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
	}

}// End of Class
