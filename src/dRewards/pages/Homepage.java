package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.DatePicker;
import dRewards.pageControls.HomePage_LPGWidget;
import dRewards.pages_Administration.*;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Homepage {
    static String firstItemText;
    static String firstItemSavingText;
    static String savingTextLandingPage;

    // visible on SmartRewards
    public static void verify_TravelSearch_ElementsPresent() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("home_travel_HotelsTab"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("home_travel_CruisesTab"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("home_travel_CarRentalsTab"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("home_travel_TopDealsHeader"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_hotelSearchInput_Textbox"));

        String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
        String tomorrowsDate = BaseUI.getDateAsString_InRelationToTodaysDate(1);

        ArrayList<String> cruiseList = returnCruiseList();

        BaseUI.verify_true_AndLog(cruiseList.size() > 0, "Found cruises on Homepage.",
                "Could not find cruises on Homepage.");
        BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("homePage_HotelCheckIn_DatePicker"), "value",
                todaysDate);
        BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("homePage_HotelCheckout_DatePicker"),
                "value", tomorrowsDate);
        BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("homepage_hotelAdultsDropdown"), "2");
        BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("homepage_hotelChildrenDropdown"), "0");
    }

    // visible on AARP
    public static void verify_HotelSearch_ElementsPresent() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_hotelSearchInput_Textbox"));

        String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
        String tomorrowsDate = BaseUI.getDateAsString_InRelationToTodaysDate(1);
        BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("homePage_HotelCheckIn_DatePicker"), "value",
                todaysDate);
        BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("homePage_HotelCheckout_DatePicker"),
                "value", tomorrowsDate);
        BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("homepage_hotelAdultsDropdown"), "2");
        BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("homepage_hotelChildrenDropdown"), "0");
        if(! ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)){
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("home_travel_CarRentalsLink"));
        }
    }

    public static void verify_DestinationRequired_Error() throws Exception {
        click_hotelSearchSubmit();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DestinationRequiredError"));
    }

    // visible on SmartRewards
    public static ArrayList<String> returnCruiseList() {
        ArrayList<WebElement> cruiseList = Locator.lookup_multipleElements("home_travel_ListOfCruiseLinks", null, null);
        ArrayList<String> cruiseTextList = new ArrayList<String>();

        for (WebElement cruise : cruiseList) {
            cruiseTextList.add(BaseUI.getTextFromField(cruise));
        }

        return cruiseTextList;
    }

    // AARP only, click on the Travel/Hotel Image
    public static void click_Travel_ImageLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_TravelHotel_Link"));
        BaseUI.waitForElementToBeDisplayed("hotelSearchInput", null, null);
        Thread.sleep(1000);
    }

    // AARP only, click on the Local Offers Image
    public static void click_LocalOffer_ImageLink() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_LocalOffer_ImageLink"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_LocalOffer_ImageLink"));
        Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
    }

    // visible on SmartRewards and TVC
    public static void verify_click_CruiseTab_NavigatesTo_LandingPage() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("home_travel_CruisesTab"));
        Thread.sleep(1000);
        BaseUI.switch_ToIframe(0);
        try {
            String cruiseHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("cruise_Header"));
            String expectedTitle = "Cruise Search";
            BaseUI.baseStringCompare("Cruise", expectedTitle, cruiseHeader);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("cruise_Header"));
        } finally {
            BaseUI.switch_ToDefaultContent();
        }
    }

    public static void verify_click_CarRentals_NavigatesTo_LandingPage() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            Navigation.navigate_CarRental_Page();
            String carRentalsHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_HeaderText"));
            String expectedTitle = "Find Car Rentals. Reserve Now, Pay Later!";
            BaseUI.baseStringCompare("CarRentals", expectedTitle, carRentalsHeader);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_HeaderText"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
            BaseUI.click(Locator.lookupRequiredElement("home_travel_CarRentalsTab"));
            Thread.sleep(1000);
            String carRentalsHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_HeaderText"));
            String expectedTitle = "Find Car Rentals. Reserve Now, Pay Later!";
            BaseUI.baseStringCompare("CarRentals", expectedTitle, carRentalsHeader);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_HeaderText"));
        } else {
            BaseUI.click(Locator.lookupRequiredElement("home_travel_CarRentalsLink"));
            Thread.sleep(1000);
            String carRentalsHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_HeaderText"));
            String expectedTitle = "Find Car Rentals. Reserve Now, Pay Later!";
            BaseUI.baseStringCompare("CarRentals", expectedTitle, carRentalsHeader);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_HeaderText"));
        }
    }

    public static void hotel_Search_WithCriteria(String address, String fromDate, String toDate, String adultCount,
                                                 String childCount, String ageOfChildren) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_hotelSearchInput_Textbox"), address);
        BaseUI.waitForElementToBeDisplayed("homepage_hotelInput_ListThatAppears_FirstEntry",null,null);
        Thread.sleep(200);
        BaseUI.click(Locator.lookupRequiredElement("homepage_hotelInput_ListThatAppears_FirstEntry"));
        Thread.sleep(200);

        setCheckin_AndCheckoutDates(fromDate, toDate);
        select_Adults_AndChildren(adultCount, childCount, ageOfChildren);

        click_hotelSearchSubmit();
    }

    public static void setCheckin_AndCheckoutDates(String checkin, String checkout) throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("homePage_HotelCheckIn_DatePicker"));
        Thread.sleep(200);
        DatePicker datePicker = ClientDataRetrieval.getDatePicker();
        datePicker.selectTheDate(checkin);

        BaseUI.click(Locator.lookupRequiredElement("homePage_HotelCheckout_DatePicker"));
        Thread.sleep(200);
        datePicker.selectTheDate(checkout);

        Thread.sleep(200);
    }

    public static void select_Adults_AndChildren(String number_adults, String number_children, String childrenAge)
            throws Exception {
        Select adult_dropdown = new Select(Locator.lookupRequiredElement("homepage_hotelAdultsDropdown"));
        adult_dropdown.selectByVisibleText(number_adults);
        Thread.sleep(50);

        Select children_dropdown = new Select(Locator.lookupRequiredElement("homepage_hotelChildrenDropdown"));
        children_dropdown.selectByVisibleText(number_adults);
        Thread.sleep(50);

        Integer numberOfChildren = Integer.parseInt(number_children);
        for (Integer i = 1; i <= numberOfChildren; i++) {
            Select childAge_dropdown = new Select(
                    Locator.lookupRequiredElement("homepage_hotelChildAge_Dropdown_ByChildNumber", i.toString(), null));
            childAge_dropdown.selectByVisibleText(childrenAge);
            Thread.sleep(50);
        }
    }

    public static void click_hotelSearchSubmit() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_hotelSearchHotel_Button"));
        BaseUI.waitForElementToBeDisplayed("hotelsAndCondos_ElementToWaitFor", null, null, 50);
        Thread.sleep(1000);

    }

    // Citi deals Top Travel Deals controls.
    public static void verify_TopTravelDeals_Carousel() {
        ArrayList<WebElement> travelCarousel = Locator.lookup_multipleElements("ttd_EntryList", null, null);
        BaseUI.verify_true_AndLog(travelCarousel.size() > 2, "Found Carousel results.",
                "Could NOT find Carousel results.");
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("ttd_MoreTravelDeals"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("ttd_TravelCarousel"));
    }

    // Citi deals Top Travel Deals controls.
    public static void navigate_TTD_right_Carousel() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("ttd_NextDeal_ArrowButton"));
        Thread.sleep(1500);
    }

    // Citi deals Top Travel Deals controls.
    public static void navigate_TTD_left_Carousel() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("ttd_PreviousDeal_ArrowButton"));
        Thread.sleep(1500);
    }

    public static void verify_LocalOffer_Appears() {
        String localOfferTitle = null;
        String expectedTitle = null;
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"));
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            localOfferTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"))
                    .split("\\n")[0];
            expectedTitle = "Search Citi Easy Deals to find local offers in your area*";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_LocalOffer_SeeLocalOffer_Button"));
            localOfferTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"));
            expectedTitle = "Search for Deals Near You";
        } else {
            localOfferTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"));
            expectedTitle = "Local Offers";
        }
        BaseUI.baseStringCompare("Local Offers", expectedTitle, localOfferTitle);
    }

    // Need to enter the Zipcode first to verify fields blank
    public static void verify_LocalOffer_AddressFields_Blank(String zipCode) throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi) && Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"));
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"), zipCode);
            String addressText = BaseUI
                    .getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"));
            String cityText = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
            String stateDropdown = BaseUI.getSelectedOptionFromDropdown("homepage_LocalOffer_State_Dropdown");
            BaseUI.verify_true_AndLog(BaseUI.stringEmpty(addressText), "Address Field was empty",
                    "Address Field was Not empty");
            BaseUI.verify_true_AndLog(BaseUI.stringEmpty(cityText), "City Field was empty", "City Field was Not empty");
            BaseUI.verify_true_AndLog(BaseUI.stringEmpty(stateDropdown), "State Dropdown was empty",
                    "State Dropdown was Not empty");
        } else {
            // Was unable to enter Zipcode with the methods we have in BaseUI
            // class so had to use Actions to enter Zipcode
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"));
            Actions actions = new Actions(Browser.driver);
            actions.moveToElement(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"));
            actions.click();
            actions.sendKeys(zipCode);
            actions.build().perform();
            String addressText = BaseUI
                    .getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"));
            String cityText = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
            String stateDropdown = BaseUI.getSelectedOptionFromDropdown("homepage_LocalOffer_State_Dropdown");
            BaseUI.verify_true_AndLog(BaseUI.stringEmpty(addressText), "Address Field was empty",
                    "Address Field was Not empty");
            BaseUI.verify_true_AndLog(BaseUI.stringEmpty(cityText), "City Field was empty", "City Field was Not empty");
            BaseUI.verify_true_AndLog(BaseUI.stringEmpty(stateDropdown), "State Dropdown was empty",
                    "State Dropdown was Not empty");
        }
    }

    public static void verify_LocalOffer_Error() throws Exception {
        WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
        addressTextBox.clear();
        BaseUI.tabThroughField(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"));
        WebElement cityTextField = Browser.driver.switchTo().activeElement();
        BaseUI.enterText_IntoInputBox(cityTextField, "");
        BaseUI.tabThroughField(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
        Navigation.click_SeeLocalOffer_Button();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_LocalOffer_error"));
    }

    public static void localOffer_Search_WithCriteria(String address, String city, String state) {
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Textbox"), (city + ", " + state));
        } else {
            WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
            addressTextBox.clear();
            WebElement cityTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox");
            cityTextBox.clear();
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"), address);
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"), city);
            BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("homepage_LocalOffer_State_Dropdown"), state);
        }
    }

    public static void localOffer_Search_WithZipcode(String zipCode) {
        WebElement cityTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox");
        cityTextBox.clear();
        WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
        addressTextBox.clear();
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"), zipCode);
        } else {
            Actions actions = new Actions(Browser.driver);
            actions.moveToElement(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"));
            actions.click();
            actions.sendKeys(zipCode);
            actions.build().perform();
        }
    }

    public static void verify_SearchResultPageLocation_Matches_HomePage(String address, String city, String state)
            throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Textbox"), (city + ", " + state));
        } else {
            localOffer_Search_WithCriteria(address, city, state);
        }
        Navigation.click_SeeLocalOffer_Button();
        String currentLocation;

        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            String currentLocation_ReplaceValue = ClientDataRetrieval.environment.equals("RC") ? "Fort" : "Fort ";
            currentLocation = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_CurrentLocation"))
                    .replaceAll("Ft.", currentLocation_ReplaceValue);
            BaseUI.baseStringPartialCompare("Address Match", (city + ", " + state), currentLocation);
            Navigation.navigate_Home_viaHomeLink();
        } else {
            currentLocation = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_CurrentLocation"))
                    .replaceAll("Ft.", "Fort");
            Navigation.navigate_Home_viaLogo();
        }
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_LocalOffer_HeaderTitle"));

        if (ClientDataRetrieval.isLegacyClient()) {
            String addressText = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"));
            String cityName = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
            String stateText = BaseUI.getSelectedOptionFromDropdown("homepage_LocalOffer_State_Dropdown");

            BaseUI.baseStringCompare("Address", address, addressText);
            BaseUI.baseStringCompare("City", currentLocation.split(",")[0], cityName);
            BaseUI.baseStringCompare("State", currentLocation.split(",")[1].trim(), stateText);
        }
    }

    public static void verify_Auction_Widget_Section() throws Exception {
        // BaseUI.verifyElementAppears(Locator.lookupRequiredElement("Auction_Section"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("Auction_Header"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("Auction_Img"));
    }

    public static void verify_Auction_Widget_EndIn() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("Auction_EndIn"));
    }

    public static void verify_Auction_Widget_ClickSeeDetails() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            Navigation.navigate_Home_viaHomeLink();
        } else {
            Navigation.navigate_Home_viaLogo();
        }
        if (ClientDataRetrieval.isRedesignClient()) {
            String auctionName = BaseUI.getTextFromField(Locator.lookupRequiredElement("Auction_See_Details_btn"));
            BaseUI.click(Locator.lookupRequiredElement("Auction_See_Details_btn"));
            BaseUI.waitForElementToBeDisplayed("AuctionLandingPage_Header_DetailPage", null, null, 10);
            String auctionPageName = BaseUI.getTextFromField(Locator.lookupRequiredElement("AuctionLandingPage_Header_DetailPage"));
            BaseUI.baseStringCompare("AuctionPageTitle", auctionName, auctionPageName);
        } else {
            BaseUI.click(Locator.lookupRequiredElement("Auction_See_Details_btn"));
            BaseUI.waitForElementToBeDisplayed("AuctionLandingPage_Header", null, null, 10);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("AuctionLandingPage_Header"));
        }
    }

    public static void verify_Auction_Widget_ClickSeeDetailsLink() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            Navigation.navigate_Home_viaHomeLink();
        } else {
            Navigation.navigate_Home_viaLogo();
        }
        BaseUI.click(Locator.lookupRequiredElement("Auction_SeeAll_ST"));
        BaseUI.waitForElementToBeDisplayed("AuctionLandingPage_Header", null, null, 20);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("AuctionLandingPage_Header"));
        Thread.sleep(1500);
    }

    public static ArrayList<String> return_LeftPanel_CategoryList(String category) {
        ArrayList<String> categoryList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator
                .lookup_multipleElements("homepage_LeftPanel_CategoryLinks_List_ByText", category, null);

        for (WebElement categoryElement : categoryElements) {
            String categoryText = categoryElement.getAttribute("innerText");
            categoryText = categoryText.trim();
            categoryList.add(categoryText);
        }

        return categoryList;
    }

    public static void minimize_Category(String category) throws Exception {
        WebElement linkToCheck = Locator.lookupRequiredElement("homepage_Header_ActiveElement", category, null);
        if (BaseUI.elementHasExpectedAttribute(linkToCheck, "class", "activemenu")) {
            BaseUI.click(linkToCheck);
            BaseUI.waitForElementToNOTBeDisplayed("homepage_LeftPanel_WaitElement", null, null, 20);
            Thread.sleep(500);
        }
    }

    public static void maximize_Category(String category) throws Exception {
        WebElement linkToCheck = Locator.lookupRequiredElement("homepage_Header_ActiveElement", category, null);
        if (!BaseUI.elementHasExpectedAttribute(linkToCheck, "class", "activemenu")) {
            BaseUI.click(linkToCheck);
            BaseUI.waitForElementToBeDisplayed("homepage_LeftPanel_WaitElement", null, null, 20);
            Thread.sleep(500);
        }
    }

    public static void verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(String category,
                                                                                     String expectedCategoryHeader) throws Exception {
        ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
        BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

        ArrayList<String> topCategoryList = Navigation.return_List_OfLinks_ByCategoryText(category);
        if (expectedCategoryHeader != null) {
            topCategoryList.set(topCategoryList.indexOf(category), expectedCategoryHeader);
        } else {
            topCategoryList.remove(category);
        }
        if (category.equals("Featured Brands")) {
            topCategoryList.set(topCategoryList.indexOf("Brands A-Z"), "View Brands A-Z");
        } else if (category.equals("Sweepstakes")) {
            categoryList.set(categoryList.indexOf("Upcoming  Sweepstakes"), "Upcoming Sweepstakes");
        }

        BaseUI.verify_true_AndLog(topCategoryList.size() == categoryList.size(), "Lists match in size.",
                "Lists did NOT match in size.");

        for (String link : topCategoryList) {
            BaseUI.verify_true_AndLog(categoryList.contains(link),
                    "Link " + link + " matched in both Top and Left Panel.",
                    "Could not find " + link + " in left panel.");
        }
    }

    public static void CreateNewAuction_HomePage_Widget() throws Exception {
        String auctionName = "AutomationTest";
        Double startingBid = 500.00;
        Double usdAmount = 3.99;
        // Extend Time is in seconds.
        Integer extendTime = 20;

        // String productSku = "auc46samsungsmarthdtvDEMO";
        String productSku = "aucchevycameroblkDEMO";

        Boolean isDemo = true;
        Integer auctionTimeToLast = 5;
        new TableData();
        if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
            auctionTimeToLast += 60;
        }

        String randomNumber = BaseUI.random_NumberAsString(0, 70000);
        auctionName += randomNumber;

        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        LoginPage_Admin.login_Admin();
        Navigation_Admin.navigate_ClickNavLink("Create Auction");
        AddNewAuction_Admin.Add_New_Auction_DualCurrency(auctionName, RedemptionTypes.DualCurrency, startingBid,
                usdAmount, extendTime, productSku, AuctionTypes.Standard, isDemo);

        // BaseUI.click(Locator.lookupRequiredElement("auction_Add_subauction_Button"));
        // Thread.sleep(1000);

        AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
        AddNewAuction_Admin.initialize_Auction();
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
        Browser.closeBrowser();

        // Browser.navigateTo(ClientDataRetrieval.url);
        // LoginPage.login(ClientDataRetrieval.userName,
        // ClientDataRetrieval.password);
    }

    // click on search button on Home Page
    public static void click_MerchandiseSearchSubmit() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
                || ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
            BaseUI.click_js(Locator.lookupRequiredElement("homepage_SearchButton"));
        } else {
            BaseUI.click_js(Locator.lookupRequiredElement("homepage_SearchButton"));
        }
        Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
    }

    // click the 1st option in merchandise autosearch result
    public static void click_FirstOptionDisplayed_MerchandiseSearch(String searchOptionText) throws Exception {

        BaseUI.click_js(Locator.lookupRequiredElement("homepage_SelectFirstOption_SearchResult", searchOptionText, null));
        Thread.sleep(1000);
    }

    // enter the valid or invalid text or brand name in merchandise search text
    // box
    public static void enter_MerchandiseBrandName_SearchBox(String brandName) throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            BaseUI.click(Locator.lookupRequiredElement("homepage_SearchIcon"));
            BaseUI.waitForElementToBeDisplayed("homepage_SearchBox", null, null, 20);
        }
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_SearchBox"), brandName);

        if (ClientDataRetrieval.isLegacyClient()) {
            BaseUI.waitForElementToBeDisplayed("homepage_SearchResult_Displays_BrandWord_Title", null, null, 20);
        }
        Thread.sleep(500);
    }

    // Verify Merchandise Search text entered is valid
    public static void verify_MerchandiseSearchText_Is_Valid(String brandName) throws Exception {

        String displayed_ValidBrandNameString;

        enter_MerchandiseBrandName_SearchBox(brandName);

        //For AARP redesign client when we select the 1st search option, it does not display
        //search button to click and navigate to search result page
        //That is why removing this step.
        if (!(ClientDataRetrieval.isRedesignClient())) {
            click_FirstOptionDisplayed_MerchandiseSearch(brandName);
        }

        Homepage.click_MerchandiseSearchSubmit();

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_verify_ValidBrandString_Displays"));
            displayed_ValidBrandNameString = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("homepage_verify_ValidBrandString_Displays"))
                    .replace("\u203A", "").replace("[^\\p{ASCII}]", "").replace(" [X]", "")
                    .replace('"', ' ').trim();
            // brandName);
            BaseUI.baseStringCompare("homepage_verify_ValidBrandString_Displays", brandName,
                    displayed_ValidBrandNameString);
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_ValidBrand_ResultPage_Message"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_ValidBrandText_ResultPage"));

            if (ClientDataRetrieval.isLegacyClient()) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_verify_ValidBrandString_Displays"));
                displayed_ValidBrandNameString = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("homepage_verify_ValidBrandString_Displays"))
                        .replace("[^\\p{ASCII}]", "").replace(" [X]", "").replace('"', ' ')
                        .replace("\u203A", "").trim();
                // brandName);
                BaseUI.baseStringCompare("homepage_verify_ValidBrandString_Displays", brandName,
                        displayed_ValidBrandNameString);

                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_verify_ValidBrand_CategoryList_ResultPage"));
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_ValidBrand_SeeDetailsButton_Or_Description_ResultPage"));
        ArrayList<WebElement> searchElementTitleList = Locator.lookup_multipleElements("homepage_SearchItem_Results",
                null, null);
        BaseUI.verify_true_AndLog(searchElementTitleList.size() > 0, "Found Results Title Lists.",
                "Did NOT Results Title Lists.");
        // }
    }

    // Verify merchandise seach text entered is invalid
    public static void verify_MerchandiseSearchText_Is_Invalid() throws Exception {

        String brandName = "shy";
        String oopsMessage = "Oops!";
        String message_OnInvalidText = ("We have no matches for") + " " + '"' + brandName + '"' + ".";

        enter_MerchandiseBrandName_SearchBox(brandName);

        //if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_SearchResult_Displays_BrandWord"))) {
        Homepage.click_MerchandiseSearchSubmit();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_InvalidBrand_ResultPage_OopsMessage"));
        BaseUI.verifyElementHasExpectedPartialText("homepage_InvalidBrand_ResultPage_OopsMessage", oopsMessage);
        if (ClientDataRetrieval.isLegacyClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_verify_InvalidBrand_ResultPage_NoMatchText"));
            BaseUI.verifyElementHasExpectedText("homepage_verify_InvalidBrand_ResultPage_NoMatchText",
                    message_OnInvalidText);

            BaseUI.verifyElementHasExpectedText("homepage_InvalidBrand_ResultPage_HelpYouMessage",
                    "Still can't find what you are looking for, or can't decide? Let us help you...");
        }
        //}
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_SearchBox_Displayed"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_GoButton_Displayed"));
    }

    public static void navigate_Travel_LPGTravelpage() throws Exception {
        if (ClientDataRetrieval.isLegacyClient()) {
            if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
                BaseUI.log_AndFail("No Travel Link available for Allstate");
            }else{
                BaseUI.click(Locator.lookupRequiredElement("homepage_lpgTravel_Tab"));
                Thread.sleep(500);
                BaseUI.waitForElementToBeDisplayed("homepage_Lpg_Terms_TravelLink", null, null, 20);
                BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
            }

        } else {
            BaseUI.waitForElementToBeDisplayed("homepage_Lpg_Terms_TravelLink", null, null, 20);
            BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
        }

        if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            BaseUI.waitForElementToBeDisplayed("lpgPage_BreadCrumb", null, null);
        } else {
            BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
        }
        Thread.sleep(500);
    }

    public static void navigate_LPGModal_Merchandise_coreClient() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("homepage_ClickForDetailsWidget"));
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("homepage_lpgMerchandise_Tab", null, null, 20);
        BaseUI.click(Locator.lookupRequiredElement("homepage_lpgMerchandise_Tab"));
        Thread.sleep(500);
    }

    public static void verify_Merchandise_LPG_homepage() throws Exception {

        String msgExpected;
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            msgExpected = "Every item we offer is reviewed each day to guarantee that Your Price is as low as any you will find at online retail including:";
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            msgExpected = "BUT if you find a lower price online within 7 days of purchase,"
                    + "\n" +"we'll give you back your points and refund the difference.";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            msgExpected = "Once we find the lowest price, we apply your points to make your price even better.";
        } else {
            msgExpected = "Every item we offer is reviewed each day to guarantee that our Retail Price (that's the price before your "
                    + ClientDataRetrieval.client_Currency
                    + " is applied) is the lowest you will find at online retailers including:";
        }

        LPG_Modal.navigate_Merchandise_LPGhomepage();

        if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){
            WebElement merchActiveElement = Locator.lookupRequiredElement("homepage_lpgWidget_Merch_ActiveContent");
            BaseUI.verifyElementAppears(merchActiveElement);
            String expectedURL = "/img/allstate/outline/merch-tab-contents.png";
            String actualURL = BaseUI.get_CSSAttribute_FromField(merchActiveElement, "background-image");
            BaseUI.baseStringPartialCompare("Merchandise Tab Active Image URL", expectedURL, actualURL);
        }else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_Modal_lpgMerchandise_ActiveContent"));
            String merchandiseMSGAARP = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("lpg_Modal_lpgMerchandise_ActiveContent"));
            BaseUI.baseStringCompare("lpg_Modal_lpgMerchandise_ActiveContent", msgExpected, merchandiseMSGAARP);
        }


    }

    public static void verify_HomePage_SiteMedia_Zone1() throws Exception {
        //Adding this if condition to check if element exists, because sometimes site media attribute
        //and sometimes it is not set up and test is failing.
        if (BaseUI.elementExists("homepage_SitemediaZone1", null, null)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_SitemediaZone1"));
        }
    }

    public static void verify_HomePage_SiteMedia_Zone2() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaIMG_LO"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            //Adding this if condition to check if element exists, because sometimes site media attribute
            //and sometimes it is not set up and test is failing.
            if (BaseUI.elementExists("homePage_SiteMediaIMG_Travel", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaIMG_Travel"));
            }
        } else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
            if (BaseUI.elementExists("homePage_SiteMediaIMG_Travel", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaIMG_Travel"));
            }
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            if (BaseUI.elementExists("homepage_SitemediaZone2", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_SitemediaZone2"));
            }
        } else {
            if (BaseUI.elementExists("homePage_SiteMediaIMG_LO", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaIMG_LO"));
            } else if (BaseUI.elementExists("homePage_SiteMediaIMG_Travel", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaIMG_Travel"));
            }
        }
    }

    public static void verify_HomePage_SiteMedia_Zone3() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            //Adding this if condition to check if element exists, because sometimes site media attribute
            //and sometimes it is not set up and test is failing.
            if (BaseUI.elementExists("homePage_SiteMedia_MagazinesIMG", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMedia_MagazinesIMG"));
            } else if (BaseUI.elementExists("homePage_SiteMedia_HowItWork", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMedia_HowItWork"));
            }
        } else if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            if (BaseUI.elementExists("homePage_SiteMediaSideBanner", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaSideBanner"));
            }
        } else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
            //SiteMedia is not longer available in the site - no validation require at this time
            //BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMediaSideBanner"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            if (BaseUI.elementExists("homePage_SiteMedia_Zone3", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMedia_Zone3"));
            }
        } else {
            if (BaseUI.elementExists("homePage_siteMedia_MixedAdZone", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_siteMedia_MixedAdZone"));
            } else if (BaseUI.elementExists("homePage_SiteMedia_Mid_Ads", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMedia_Mid_Ads"));
            } else if (BaseUI.elementExists("homePage_SiteMedia_FooterAD", null, null)) {
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_SiteMedia_FooterAD"));
            }
        }
    }

    public static void Navigate_Travel_LPGHomePage() throws Exception {
        Navigate_Travel_LPGMerchandise_Modal();
        HomePage_LPGWidget.click_LearnMore_ExpandWidget();
        BaseUI.click(Locator.lookupRequiredElement("homepage_lpgTravel_Tab"));
        Thread.sleep(500);
    }

    public static void Navigate_Travel_LPGMerchandise_Modal() throws Exception {

        BaseUI.click_js(Locator.lookupRequiredElement("homepage_lpgTravel_Tab"));
        Thread.sleep(500);
    }

    public static void Navigate_Travel_LPGHomePageCoreClient() throws Exception {
        HomePage_LPGWidget.click_LearnMore_ExpandWidget();
        Navigate_Travel_LPGMerchandise_Modal();
    }

    public static void verify_Travel_LPG_homepage() throws Exception {

        String msgExpected;
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            msgExpected = "Travel is reviewed each month to guarantee Your Price is as low as any you will find at online sites including:";
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            msgExpected = "BUT if you find a lower price online within 7 days of purchase,"
                    + "\n" +"we'll give you back your points and refund the difference.";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            msgExpected = "Once we find the lowest price, we apply your points to make your price even better.";
        } else {
            msgExpected = "Travel is reviewed each month to guarantee that our Retail Price (that's the price before your "
                    + ClientDataRetrieval.client_Currency
                    + " is applied) is the lowest you will find at online sites including:";
        }

        Navigate_Travel_LPGHomePage();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_lpgTravel_TabActive"));
        String merchandiseMSGAARP = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_lpgTravel_TabActive"));
        BaseUI.baseStringCompare("homepage_lpgTravel_TabActive", msgExpected, merchandiseMSGAARP);
    }

    public static void verify_Travel_LPG_Modal() throws Exception {

        String msgExpectedAARP = "Travel is reviewed each month to guarantee that our Retail Price (that's the price before your "
                + ClientDataRetrieval.client_Currency
                + " is applied) is the lowest you will find at online sites including:";
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            msgExpectedAARP = "Travel is reviewed each month to guarantee Your Price is as low as any you will find at online sites including:";
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate)) {
            msgExpectedAARP = "Travel is reviewed each month to guarantee that our Retail Price (that's the price before your Points are applied) is the lowest you will find at online sites including:";
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            msgExpectedAARP = "Travel is reviewed each month to guarantee that our Retail Price (that's the price before your Points are applied) is the lowest you will find at online sites including:";
        }
        Navigate_Travel_LPGMerchandise_Modal();
        String travelMSGAARP = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_lpgTravel_TabActive"));

        BaseUI.baseStringCompare("homepage_lpgTravel_TabActive", travelMSGAARP, msgExpectedAARP);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_lpgTravel_TabActive"));
    }

//	public static void verifyTravelLPG_TandC_landingpage() throws Exception {
//
//		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
//			Navigate_Travel_LPGHomePage();
//			BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
//			String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
//			String titleExpected = "Low Price Guarantee";
//			BaseUI.baseStringCompare("lpgPage_Title", lpgTitle, titleExpected);
//			Thread.sleep(500);
//			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
//		} else {
//			Navigate_Travel_LPGHomePage();
//			BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
//			String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
//			String titleExpected = "Lowest Price Guarantee";
//			BaseUI.baseStringCompare("lpgPage_Title", lpgTitle, titleExpected);
//			Thread.sleep(500);
//			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
//		}
//	}

    public static void verifyTravelLPG_TandC_landingpage() throws Exception {
        //clickLPG_LearnMoreLink();
        if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){
            LPG_Modal.navigate_Merchandise_LPGhomepage();
        }else {
            navigate_Travel_LPGTravelpage();
        }

        LPG_Page.verifyLPG_HeaderTitle();
    }

//	public static void verifyMerchandiseLPG_TandC_landingpage() throws Exception {
//		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
//			navigate_Merchandise_LPGhomepage();
//			BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
//			String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
//			String titleExpected = "Low Price Guarantee";
//			BaseUI.baseStringCompare("lpgPage_Title", lpgTitle, titleExpected);
//			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
//		} else {
//			navigate_Merchandise_LPGhomepage();
//			BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
//			String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
//			String titleExpected = "Lowest Price Guarantee";
//			BaseUI.baseStringCompare("lpgPage_Title", lpgTitle, titleExpected);
//			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
//		}
//	}


    public static void verifyMerchandiseLPG_TandC_LandingPageCoreClient() throws Exception {
        navigate_LPGModal_Merchandise_coreClient();
        BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null, 20);
        String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
        String titleExpected = "Lowest Price Guarantee";
        BaseUI.baseStringCompare("lpgPage_Title", lpgTitle, titleExpected);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
    }

    public static void verifyTravelLPG_TandC_LandingPageCoreClient() throws Exception {
        Navigate_Travel_LPGHomePageCoreClient();
        BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
        BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null, 20);
        Thread.sleep(500);
        String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
        String titleExpected = "Lowest Price Guarantee";
        BaseUI.baseStringCompare("lpgPage_Title", lpgTitle, titleExpected);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
    }

    // click Gift Cards Carousel right navigation arrow
    // citi only
    public static void click_GiftCardsCarousel_RightNavigationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_RightNavigation"));
        Thread.sleep(500);
    }

    // click Gift Cards Carousel left navigation arrow
    // citi only
    public static void click_GiftCardsCarousel_LeftNavigationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_LeftNavigation"));
        Thread.sleep(500);
    }

    public static void verify_GiftCardsCarousel_Header_Appears() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_Header"));
        BaseUI.verifyElementHasExpectedPartialText("homepage_GiftCardsCarousel_Header", "Gift Cards");
    }

    // citi only
    public static void verify_GiftCardsCarousel_SeeMoreLink_Appears() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_SeeMore_Link"));
        BaseUI.verifyElementHasExpectedPartialText("homepage_GiftCardsCarousel_SeeMore_Link", "See More");
    }

    // citi only
    public static void click_GiftCardsCarousel_SeeMoreLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_SeeMore_Link"));
        BaseUI.waitForElementToBeDisplayed("giftCards_Page_header", null, null, 20);
        Thread.sleep(500);
    }

    // click the first image on Gift Cards Carousel
    // citi only
    public static void click_FirstItemImage_GiftCardsCarousel() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_FirstItem_Displayed"));
        Thread.sleep(500);
    }

    // citi only
    public static void verify_GiftCardsCarousel_LeftNavigationArrow_Appears() throws Exception {
        click_GiftCardsCarousel_RightNavigationArrow();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_LeftNavigation"));
    }

    // click all right arrow of Gift Cards Carousel
    // only for city
    public static void click_NavigateGiftCardsCarousel_ToFarRight() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_GiftCardsCarousel_LastItem_Displayed"))) {
            click_GiftCardsCarousel_RightNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_LastItem_Displayed"));
    }

    // click all left arrow of Daily Category Special
    // only for citi
    public static void click_NavigateGiftCardsCarousel_ToFarLeft() throws Exception {

        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_GiftCardsCarousel_FirstItem_Displayed"))) {
            click_GiftCardsCarousel_LeftNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_GiftCardsCarousel_FirstItem_Displayed"));
    }

    // click See more link to navigate to Gift Card Landing Page
    // only for citi
    public static void verify_click_SeeMoreLink_NavigatesTo_GiftCardsCarousel_LandingPage() throws Exception {
        click_GiftCardsCarousel_SeeMoreLink();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_Page_header"));
        BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupRequiredElement("giftCards_Page_header"),
                "Gift Cards");
        ArrayList<String> navMenuList = GiftCards_LandingPage.return_GiftCards_NavMenuList();
        BaseUI.verify_true_AndLog(navMenuList.size() > 0, "Found Gift Cards Nav Menu list on GiftCards Landing Page.",
                "Could not find Gift Cards Nav Menu list on GiftCards Landing Page.");
    }

    // click first item of Gift Carousel to navigate to Gift Card Landing page
    // only for citi
    public static void verify_GiftCardsCarousel_ClickingFirstItem_NavigatesTo_LandingPage() throws Exception {
        WebElement element = Locator.lookupRequiredElement("homepage_GiftCardsCarousel_FirstItem_Displayed");
        String firstItemText = element.getAttribute("alt");

        click_FirstItemImage_GiftCardsCarousel();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_Selected_Item_HeaderTitle"));

        String firstItemDisplayedText = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("giftCards_Selected_Item_NameField"))
                .replace("[X]", "");
        String firstItemDisplayedTextStr = firstItemDisplayedText.trim();
        BaseUI.baseStringCompare("ItemName", firstItemText, firstItemDisplayedTextStr);
    }

    // click GiftCards Carousel Gift Card Image
    // only for citi
    public static void click_GiftCardsCarousel_Image_ByGiftCardName(String giftCardName) throws Exception {

        BaseUI.click_js(Locator.lookupRequiredElement(giftCardName));
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("giftCard_LandingPage_WaitElement", null, null, 20);
        Thread.sleep(1000);
    }

    // only for citi
    public static void clickAndVerify_GiftCardsCarousel_Displayed_NavigatesTo_LandingPage(String giftCardName)
            throws Exception {

        click_GiftCardsCarousel_Image_ByGiftCardName(giftCardName);
        Navigation.verify_No_ApplicationErrorMessage();
        Navigation.verify_OopsPage_DoesNOT_Appear();
        BaseUI.verify_true_AndLog((Locator.lookupRequiredElement("giftCards_Selected_Item_HeaderTitle").isDisplayed()),
                "Gift Cards header is displayed", "Gift Cards Header does Not display");

        String firstItemDisplayedText = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("giftCards_Selected_Item_NameField"));
        BaseUI.verifyElementHasExpectedText("giftCards_Selected_Item_NameField", firstItemDisplayedText);
    }

    // click Daily Category right navigation arrow
    public static void click_DailyCategoryCarousel_RightNavigationArrow() throws Exception {

        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_RightNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_DailyCatCarousel_RightNavigation"));
        Thread.sleep(500);
    }

    // click Daily Category left navigation arrow
    public static void click_DailyCategoryCarousel_LeftNavigationArrow() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LeftNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LeftNavigation"));
        Thread.sleep(500);
    }

    public static void verify_DailyCarousel_Header_Appears() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_Header"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_Header"));
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementHasExpectedPartialText("homepage_DailyCatCarousel_Header", "Daily Deals");
        } else if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedPartialText("homepage_DailyCatCarousel_Header", "Check out our new arrivals.");
        } else {
            BaseUI.verifyElementHasExpectedPartialText("homepage_DailyCatCarousel_Header", "Daily Category Specials");
        }
    }

    public static void verify_DailyCarousel_SeeMoreLink_Appears() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_Header"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_SeeMore_Link"));
        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            BaseUI.verifyElementHasExpectedPartialText("homepage_DailyCatCarousel_SeeMore_Link", "See All Merchandise");
        } else {
            BaseUI.verifyElementHasExpectedPartialText("homepage_DailyCatCarousel_SeeMore_Link", "See More");
        }
    }

    public static void verify_DailyCarousel_RightNavigationArrow_Appears() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_Header"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_RightNavigation"));
    }

    public static void verify_DailyCarousel_LeftNavigationArrow_Appears() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_Header"));
        Homepage.click_DailyCategoryCarousel_RightNavigationArrow();
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LeftNavigation"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LeftNavigation"));
    }

    // click See more link in Daily Category Special Carousel
    // not for core clients
    public static void click_SeeMoreLink() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LeftNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_DailyCatCarousel_SeeMore_Link"));
        Thread.sleep(500);
    }

    // click the first image on Daily Category Carousel
    public static void click_FirstItemImage_DailyCategoryCarousel() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyCatCarousel_FirstItem_Displayed"))) {
            click_DailyCategoryCarousel_LeftNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.click(Locator.lookupRequiredElement("homepage_DailyCatCarousel_FirstItem_Displayed"));
        Thread.sleep(500);
    }

    // clicks first item in Daily Category Carousel
    // gets text and saving percent from firstItem from carousel and compare
    // with landing page
    public static void verify_DailyCategoryCarousel_ClickingFirstItem_NavigatesTo_LandingPage() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_Header"));
        if (ClientDataRetrieval.isRedesignClient()) {
            firstItemText = BaseUI.get_Attribute_FromField(
                    Locator.lookupRequiredElement("homepage_DailyCatCarousel_FirstItemProductName"), "alt");
            firstItemSavingText = BaseUI
                    .get_Attribute_FromField(Locator.lookupRequiredElement("homepage_DailyCatCarousel_FirstItem_SavingPercentage"),
                            "innerText").substring(0, 3);
        } else {
            firstItemText = BaseUI.get_Attribute_FromField(
                    Locator.lookupRequiredElement("homepage_DailyCatCarousel_FirstItem_Displayed"), "alt");
            firstItemSavingText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("homepage_DailyCatCarousel_FirstItem_Saving_Text"))
                    .substring(0, 3);
        }

        click_FirstItemImage_DailyCategoryCarousel();

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementHasExpectedPartialText("homepage_DailyCatCarousel_LandingPage_Header", "Daily Deals");
        } else if (!ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LandingPage_Header"));
            BaseUI.verifyElementHasExpectedText("homepage_DailyCatCarousel_LandingPage_Header",
                    "Daily Category Special");
        }
        String itemTextLandingPage = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LandingPage_ItemText"));
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
                || ClientDataRetrieval.isRedesignClient()) {
            savingTextLandingPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LandingPage_SavingText"));
        } else {
            savingTextLandingPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LandingPage_SavingText"))
                    .substring(1, 4);
        }

        if (ClientDataRetrieval.isRedesignClient()) {
            savingTextLandingPage = savingTextLandingPage.split("\\s")[0].trim();
            savingTextLandingPage = savingTextLandingPage.replace("(", "");
            BaseUI.baseStringPartialCompare("ItemText", firstItemText, itemTextLandingPage);
        } else {
            BaseUI.baseStringCompare("ItemText", firstItemText, itemTextLandingPage);
        }
        BaseUI.baseStringCompare("SavingText", firstItemSavingText, savingTextLandingPage);
    }

    // click all right arrow of Daily Category Special
    // only for custom clients
    public static void click_NavigateAllDailyCategoryCarousel_ToFarRight() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyCatCarousel_LastItem_Displayed"))) {
            click_DailyCategoryCarousel_RightNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_LastItem_Displayed"));
    }

    // click all left arrow of Daily Category Special
    // only for custom clients
    public static void click_NavigateAllDailyCategoryCarousel_ToFarLeft() throws Exception {

        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyCatCarousel_FirstItem_Displayed"))) {
            click_DailyCategoryCarousel_LeftNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyCatCarousel_FirstItem_Displayed"));
    }

    public static void verify_DailyCategoryCarousel_Clicks_EntireRightNavigation_Arrow() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_RightNavigation"));
        click_NavigateAllDailyCategoryCarousel_ToFarRight();
    }

    public static void verify_DailyCategoryCarousel_Clicks_EntireLeftNavigation_Arrow() throws Exception {

        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_DailyCatCarousel_RightNavigation"));
        click_NavigateAllDailyCategoryCarousel_ToFarRight();
        click_NavigateAllDailyCategoryCarousel_ToFarLeft();
    }

    // return all the items displayed in Daily Category Carousel
    public static ArrayList<String> return_DailyCategorySpecials_ItemDisplayed() {

        ArrayList<String> itemList = new ArrayList<String>();
        ArrayList<WebElement> itemElements = Locator
                .lookup_multipleElements("homepage_DailyCatCarousel_ListOfItem_Displayed", null, null);

        for (WebElement itemElement : itemElements) {
            String itemNameText = itemElement.getAttribute("alt");
            itemNameText = itemNameText.trim();
            itemList.add(itemNameText);
        }

        return itemList;
    }

    // return all the Category displayed in Daily Category Special
    // not for Citi
    public static ArrayList<String> return_DailyCategorySpecials_CategoryDisplayed() {

        ArrayList<String> categoryList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator
                .lookup_multipleElements("homepage_DailyCatCarousel_ListOfCategoryTitle", null, null);

        for (WebElement categoryElement : categoryElements) {
            String categoryNameText = categoryElement.getAttribute("text");
            // categoryNameText = categoryNameText.trim();
            categoryList.add(categoryNameText);
        }

        return categoryList;
    }

    // return all the Category list in See More Landing page
    public static ArrayList<String> return_SeeMoreCategory_CategoryDisplayed() {

        ArrayList<String> categoryList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator
                .lookup_multipleElements("homepage_DailyCatCarousel_SeeMore_CategoryMenu", null, null);

        for (WebElement categoryElement : categoryElements) {
            String categoryNameText = categoryElement.getAttribute("text");
            // categoryNameText = categoryNameText.trim();
            categoryList.add(categoryNameText);
        }

        return categoryList;
    }

    // Category displayed in Daily Category Special Carousel displays in See
    // More Page not for Citi
    public static void verify_CategoryDisplayedInDailyCategoryCarousel_Exists_In_SeeMorePage() throws Exception {
        ArrayList<String> categoryListFromCarousel = new ArrayList<String>();
        ArrayList<String> categoryListFromLandingPage = new ArrayList<String>();

        categoryListFromCarousel = return_DailyCategorySpecials_CategoryDisplayed();
        BaseUI.verify_true_AndLog(categoryListFromCarousel.size() > 0, "Found Daily Category List on HomePage.",
                "Did not find Daily Category List on Home Page.");

        click_SeeMoreLink();

        categoryListFromLandingPage = return_SeeMoreCategory_CategoryDisplayed();
        BaseUI.verify_true_AndLog(categoryListFromLandingPage.size() > 0,
                "Found Daily Category List on Merchandise Page.",
                "Did not find Daily Category List on Merchandise Page.");

        for (String dailyCategoryCarousel : categoryListFromCarousel) {
            BaseUI.verify_true_AndLog(categoryListFromLandingPage.contains(dailyCategoryCarousel),
                    "Found " + dailyCategoryCarousel + " on Merchandise page.",
                    "Did NOT find " + dailyCategoryCarousel + " on Merchandise page.");
        }
    }

    public static void click_FeaturedCategories_Carousel_RightNavigation_Arrow() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_RightNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_RightNavigation"));
        Thread.sleep(500);
    }

    public static void click_FeaturedCategories_Carousel_LeftNavigation_Arrow() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_LeftNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_LeftNavigation"));
        Thread.sleep(500);
    }

    // click all right arrow of Featured Category Special
    // only for core clients
    public static void click_NavigateFeaturedCategoryCarouselArrow_ToFarRight() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_FeaturedCatCarousel_LastItemDisplayed"))) {
            click_FeaturedCategories_Carousel_RightNavigation_Arrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_LastItemDisplayed"));
    }

    // click all left arrow of Featured Category Special
    // only for core clients
    public static void click_NavigateFeaturedCategoryCarouselArrow_ToFarLeft() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_FeaturedCatCarousel_FirstItemDisplayed"))) {
            click_FeaturedCategories_Carousel_LeftNavigation_Arrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_FirstItemDisplayed"));
    }

    public static void click_FeaturedCategoryCarousel_FirstItem_Displayed() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_FeaturedCatCarousel_FirstItemDisplayed"));
        Thread.sleep(500);
    }

    public static void verify_Clicking_FeaturedCatCarousel_FirstItem_NavigatesTo_LandingPage() throws Exception {
        click_FeaturedCategoryCarousel_FirstItem_Displayed();
        ArrayList<WebElement> availableItems = Locator.lookup_multipleElements("merch_SearchResults_ProductTitle", null,
                null);
        BaseUI.verify_true_AndLog(availableItems.size() > 0, "Found Merchandise Products.",
                "Could NOT find Merchandise Products.");
        for (WebElement items : availableItems) {
            BaseUI.verifyElementHasExpectedPartialAttributeValue(items, "href", "/gateway?t=viewproductdetail");
        }
    }

    // click the Shop by brand right navigation arrow
    public static void click_BrandCarousel_RightNavigationArrow() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
        Thread.sleep(500);
    }

    // click the Shop by brand right navigation arrow
    public static void click_BrandCarousel_LeftNavigationArrow() throws Exception {

        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_LeftNavigation"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_BrandCarousel_LeftNavigation"));
        Thread.sleep(500);
    }

    // click the Shop by brand right navigation arrow
    public static void click_BrandCarousel_SeeAllBrandsLink() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
        BaseUI.click(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifySeeAllBrands_Link"));
        if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.waitForElementToBeDisplayed("homepage_BrandCarousel_SeeAllBrands_LandingPage_VerifyAllBrands_Header", null, null, 30);
        } else {
            BaseUI.waitForElementToBeDisplayed("homepage_LandingPage_BrandNameHeader_Displayed", null, null, 30);
        }
        Thread.sleep(2000);
    }

    // click the brand image from Shop by brand carousel
    public static void click_BrandImage_ShopByBrand_Carousel(String brandNameElement, String brandName) throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement(brandNameElement))) {

                clickBrandImage(brandNameElement, brandName);
            } else {
                Homepage.click_BrandCarousel_RightNavigationArrow();
                clickBrandImage(brandNameElement, brandName);
            }
        } else {
            clickBrandImage(brandNameElement, brandName);
        }
    }

    public static void clickBrandImage(String brandNameElement, String brandName) throws Exception {

        BaseUI.click_js(Locator.lookupRequiredElement(brandNameElement, brandName, null));
        BaseUI.wait_forPageToFinishLoading();
        Thread.sleep(2500);
    }

    // click the right arrow from Shop by brand carousel
    public static void click_ShopByBrandCarousel_RightArrow(String choosebrandNameElement, String choosebrandName) throws Exception {

        List<WebElement> allBrandsDisplayed = Locator
                .lookup_multipleElements("homepage_BrandCarousel_NumberOfBrandsDisplayed", null, null);
        BaseUI.verify_true_AndLog(allBrandsDisplayed.size() > 0, "Brand Carousel displays brand images",
                "Brand Carousel DOES NOT display brand images");
        int numberOfBrands = allBrandsDisplayed.size();
        String brandNameToBeSelected = BaseUI.random_NumberAsString(1, numberOfBrands);

        click_BrandImage_ShopByBrand_Carousel(choosebrandNameElement, brandNameToBeSelected);
        Navigation.navigate_Home_viaHomeLink();
        // BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));

        for (int i = 0; i < numberOfBrands; i++) {
            Homepage.click_BrandCarousel_RightNavigationArrow();
            numberOfBrands = numberOfBrands - 1;
            Thread.sleep(500);
        }

        click_BrandImage_ShopByBrand_Carousel(choosebrandNameElement, brandNameToBeSelected);
        Navigation.navigate_Home_viaHomeLink();
    }

    // click the left arrow from Shop by brand carousel
    public static void click_ShopByBrandCarousel_LeftArrow(String choosebrandNameElement, String choosebrandName) throws Exception {

        List<WebElement> allBrandsDisplayed = Locator
                .lookup_multipleElements("homepage_BrandCarousel_NumberOfBrandsDisplayed", null, null);
        BaseUI.verify_true_AndLog(allBrandsDisplayed.size() > 0, "Brand Carousel displays brand images",
                "Brand Carousel DOES NOT display brand images");
        int numberOfBrands = allBrandsDisplayed.size();
        String brandNameToBeSelected = BaseUI.random_NumberAsString(1, numberOfBrands);

        click_BrandImage_ShopByBrand_Carousel(choosebrandNameElement, brandNameToBeSelected);
        Navigation.navigate_Home_viaHomeLink();
        // BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));

        for (int i = 0; i < numberOfBrands; i++) {
            Homepage.click_BrandCarousel_LeftNavigationArrow();
            numberOfBrands = numberOfBrands - 1;
            Thread.sleep(500);
        }
    }

    // click the left arrow from Shop by brand carousel
    public static void click_SeeAllBrands_BrandLeftNavigationOptions(String chooseBrandName) throws Exception {

        List<WebElement> allBrandsDisplayed = Locator.lookup_multipleElements("merch_SeeAllBrands_BrandNameMenu",
                chooseBrandName, null);
        int numberOfBrands = allBrandsDisplayed.size();
        System.out.println(numberOfBrands);

        for (WebElement brandTabs : allBrandsDisplayed) {
            System.out.println(brandTabs.getText());
            if (brandTabs.getText().equals(chooseBrandName)) {
                BaseUI.click_js(brandTabs);
                Thread.sleep(2000);
                break;
            }
        }
    }

    // Return all the brands displayed in Shop by Brand carousel
    public static ArrayList<String> return_ShopByBrand_BrandsDisplayed() {
        ArrayList<String> brandsList = new ArrayList<String>();
        ArrayList<WebElement> brandElements = Locator
                .lookup_multipleElements("homepage_BrandCarousel_NumberOfBrandsDisplayed", null, null);

        for (WebElement brandElement : brandElements) {
            String brandNameText = brandElement.getAttribute("alt");
            //brandNameText.equals("North face".);
            brandNameText = brandNameText.trim().replace("North", "The North");
            brandsList.add(brandNameText);
        }

        return brandsList;
    }

    // Brands displayed in Shop By Brands and on See All Brands page
    // match/exists
    public static void verify_BrandsDisplayedOnCarousel_Exists_SeeAllBrandsPage() throws Exception {
        ArrayList<String> listFromCarousel = new ArrayList<String>();
        ArrayList<String> listFromMerchandisePage = new ArrayList<String>();

        listFromCarousel = Homepage.return_ShopByBrand_BrandsDisplayed();
        System.out.println(listFromCarousel);
        BaseUI.verify_true_AndLog(listFromCarousel.size() > 0, "Found brands on HomePage.",
                "Did not find brands on Home Page.");

        Homepage.click_BrandCarousel_SeeAllBrandsLink();
        listFromMerchandisePage = Merchandise.return_SeeAllBrands_BrandsDisplayed();
        System.out.println(listFromMerchandisePage);
        BaseUI.verify_true_AndLog(listFromMerchandisePage.size() > 0, "Found brands on Merchandise Page.",
                "Did not find brands on Merchandise Page.");


        SoftAssert softAssert = new SoftAssert();

        for (String carouselBrand : listFromCarousel) {
            softAssert.assertTrue(containsIgnoreCase(listFromMerchandisePage, carouselBrand.toLowerCase()),
                    "Did NOT find " + carouselBrand + " on Merchandise page.");
        }

        softAssert.assertAll();
    }

    private static boolean containsIgnoreCase(ArrayList<String> list, String soughtFor) {
        for (String current : list) {
            if (current.equalsIgnoreCase(soughtFor)) {
                return true;
            }
        }
        return false;
    }


    // Verify shop by brand left navigation
    public static void verify_ShopByBrand_EntireLeftNavigation(String brandName) throws Exception {

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));

        if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            Homepage.click_ShopByBrandCarousel_LeftArrow("homepage_BrandCarousel_BrandImage", brandName);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Homepage.click_ShopByBrandCarousel_RightArrow("homepage_BrandCarousel_BrandImage", brandName);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
            Homepage.click_BrandCarousel_RightNavigationArrow();
            Homepage.click_ShopByBrandCarousel_LeftArrow("homepage_BrandCarousel_BrandImage", brandName);
        } else {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
            Homepage.click_ShopByBrandCarousel_LeftArrow("homepage_BrandCarousel_BrandImage", brandName);
        }
    }

    // Verify shop by brand entire right navigation
    public static void verify_ShopByBrand_EntireRightNavigation(String brandName) throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));

        if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            Homepage.click_ShopByBrandCarousel_RightArrow("homepage_BrandCarousel_BrandImage", brandName);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Homepage.click_ShopByBrandCarousel_RightArrow("homepage_BrandCarousel_BrandImage", brandName);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
            Homepage.click_BrandCarousel_RightNavigationArrow();
            Homepage.click_ShopByBrandCarousel_RightArrow("homepage_BrandCarousel_BrandImage", brandName);
        } else {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
            Homepage.click_ShopByBrandCarousel_RightArrow("homepage_BrandCarousel_BrandImage", brandName);
        }
    }

    // Verify when any brand clicked it is navigated to right landing page
    public static void clickAndVerify_BrandsDisplayed_ShopByBrandCarousel_LandingPage(String brandName) throws Exception {

        List<WebElement> allBrandsDisplayed = Locator
                .lookup_multipleElements("homepage_BrandCarousel_NumberOfBrandsDisplayed", null, null);
        int numberOfBrands = allBrandsDisplayed.size();
        String brandNameToBeSelected = BaseUI.random_NumberAsString(1, numberOfBrands);
        String brandNameSelected;
        if (ClientDataRetrieval.isRedesignClient()) {
            brandNameSelected = BaseUI
                    .get_Attribute_FromField(Locator.lookupRequiredElement("homepage_BrandCarousel_BrandImage", brandNameToBeSelected, null), "alt");
            Homepage.click_BrandImage_ShopByBrand_Carousel("homepage_BrandCarousel_BrandImage", brandNameToBeSelected);
            Navigation.verify_OopsPage_DoesNOT_Appear();
            BaseUI.verify_true_AndLog(
                    (Locator.lookupRequiredElement("homepage_LandingPage_BrandNameHeader_Displayed").isDisplayed()),
                    "Brand Name is displayed", "Header does not display Brand name");
            String brandNameHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_LandingPage_BrandNameHeader_Displayed"));
            BaseUI.baseStringCompare("Header Brand name on landing page match selected Brand name",
                    brandNameSelected.toLowerCase().replace("'", ""), brandNameHeader.toLowerCase().replace("'", ""));
        } else {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_RightNavigation"));
            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                brandNameSelected = BaseUI
                        .get_Attribute_FromField(Locator.lookupRequiredElement("homepage_BrandCarousel_BrandImage", brandNameToBeSelected, null), "alt");
                Homepage.click_BrandImage_ShopByBrand_Carousel("homepage_BrandCarousel_BrandImage", brandNameToBeSelected);
                Navigation.verify_OopsPage_DoesNOT_Appear();
                String[] breadcrumbs = {"Home", "All Brands", brandNameSelected};
                Navigation.verify_BreadcrumbList(breadcrumbs);
            } else {
                String actualBrandNameHeader;
                brandNameSelected = BaseUI
                        .get_Attribute_FromField(Locator.lookupRequiredElement("homepage_BrandCarousel_BrandImage", brandNameToBeSelected, null), "alt");
                Homepage.click_BrandImage_ShopByBrand_Carousel("homepage_BrandCarousel_BrandImage", brandNameToBeSelected);
                Navigation.verify_OopsPage_DoesNOT_Appear();
                if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
                        || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                        || ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                        || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
                    verifyBrandNameHeaderDisplayed(brandNameSelected);
                } else if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
                    if(!(BaseUI.elementExists("homepage_BrandCarousel_LandingPage_CategoryText", null, null))) {
                        BaseUI.verify_true_AndLog(
                                (Locator.lookupRequiredElement("homepage_Verify_LandingPage_BrandImageDisplayed").isDisplayed()),
                                "Brand Image is displayed", "Brand Image is not displayed");
                        verifyBrandNameHeaderDisplayed(brandNameSelected);
                    } else {
                        BaseUI.verifyElementHasExpectedText("homepage_BrandCarousel_LandingPage_CategoryText", "Category");
                    }
                } else {
                    BaseUI.verify_true_AndLog(
                            (Locator.lookupRequiredElement("homepage_Verify_LandingPage_BrandImageDisplayed").isDisplayed()),
                            "Brand Image is displayed", "Brand Image is not displayed");
                    verifyBrandNameHeaderDisplayed(brandNameSelected);
                }
            }
        }
    }

    public static void verify_ShopByBrandCarousel_HeaderTitle() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
                || ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
            String headerTitle = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
            BaseUI.verify_true_AndLog(
                    headerTitle.contains("Shop by brand") || headerTitle.equals("Shop Our Most Popular Brands")
                            || headerTitle.equals("Shop our most popular brands"),
                    "Header title " + headerTitle + " is Displayed",
                    "Header title " + headerTitle + " is not Displayed");
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
            BaseUI.verifyElementHasExpectedPartialText("homepage_BrandCarousel_VerifyShopByBrand_Header",
                    "Shop By Brand");
        }
    }

    public static void verify_SeeAllBrandsLink() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.AARP) ||
                ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifySeeAllBrands_Link"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifySeeAllBrands_Link"));
            BaseUI.verifyElementHasExpectedPartialText("homepage_BrandCarousel_VerifySeeAllBrands_Link", "See All Brands");
        } else if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifySeeAllBrands_Link"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifySeeAllBrands_Link"));
            BaseUI.verifyElementHasExpectedPartialText("homepage_BrandCarousel_VerifySeeAllBrands_Link", "View All Brands");
        } else {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifyShopByBrand_Header"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_VerifySeeAllBrands_Link"));
            BaseUI.verifyElementHasExpectedPartialText("homepage_BrandCarousel_VerifySeeAllBrands_Link", "See More");
            BaseUI.verifyElementHasExpectedText("homepage_BrandCarousel_VerifySubHeader_Message", "Check out what's available by your favorite brand.");
        }
    }

    public static HashMap<String, String> return_Homepage_DailyDealInfo() {
        HashMap<String, String> dailyDealInfo = new HashMap<String, String>();
        if (ClientDataRetrieval.isRedesignClient()) {
            dailyDealInfo.put("dailyDealName",
                    BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_ProductTitle")).replace("...", ""));
            dailyDealInfo.put("dailyDeal_EndTime", BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_EndTime"))
                    .substring(0, 3));
        } else {
            dailyDealInfo.put("dailyDealName",
                    BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_ProductTitle")).replace("...", ""));
            dailyDealInfo.put("retailPrice",
                    BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_RetailPrice")));

            dailyDealInfo.put("points",
                    BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_Points")).replace("\u002D", "").replace("\u2013", "")
                            .replace(",", ""));
            dailyDealInfo.put("youPay", BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_YouPay"))
                    .replace("$", "").replace(",", ""));
            dailyDealInfo.put("dailyDeal_EndTime", BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_EndTime"))
                    .substring(0, 3).replace(":", ""));
        }
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)
                || !ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
            String savings = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_Savings"));
            savings = savings.split("\n")[0].replace("%", "").replace("\u0028", "")
                    .replace("\u0029", "").replaceAll("Savings", "").replace("\u0025", "").trim();
            dailyDealInfo.put("savings", savings.trim());
        } else {
            dailyDealInfo.put("savings", BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_Savings"))
                    .replace("% Savings", "").replace("(", "").replace(")", "")
                    .replace("\u0028", "").replace("\u0029", "").replaceAll("Savings", "")
                    .replace("\u0025", "").trim());
        }

        return dailyDealInfo;
    }

    public static TableData return_HomePage_DailyDeal_TableData() {
        HashMap<String, String> tableMappings = new HashMap<String, String>();
        tableMappings.put("dailyDealName", "//div[@class='dd-productname']");
        tableMappings.put("retailPrice", "//dd[@class='retail']|//td[@class='retail']");
        tableMappings.put("points", "//dd[@class='incentives']|//span[@class='incentives']");
        tableMappings.put("youPay", "//dd[@class='price']|//td[@class='price']");
        tableMappings.put("savings", "//ul[@id='dailyDeals']//div[@class='savings']".split("\\%")[0]);

        TableData dailyDealTable = new TableData();
        dailyDealTable.data = BaseUI.tableExtractor("homepage_DailyDeals_Row", tableMappings);
        dailyDealTable.remove_Character("$", "youPay");
        dailyDealTable.remove_Character(",", "youPay");
        dailyDealTable.remove_Character("\u002D", "points");
        dailyDealTable.remove_Character("\u2013", "points");
        dailyDealTable.remove_Character(",", "points");
        dailyDealTable.remove_Character("%", "savings");
        dailyDealTable.remove_Character("Savings", "savings");
        dailyDealTable.remove_Character("(", "savings");
        dailyDealTable.remove_Character(")", "savings");
        dailyDealTable.remove_Character("$", "retailPrice");
        dailyDealTable.remove_Character(",", "retailPrice");

        return dailyDealTable;
    }

    public static void click_DailyDeal_SeeMoreDeals() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("homepage_DailyDeals_DailyDealTitle"));
        } else {
            BaseUI.click(Locator.lookupRequiredElement("homepage_DailyDeals_SeeMoreDeals_Button"));
        }
        BaseUI.waitForElementToBeDisplayed("dailyDeals_ProductTitle", null, null, 30);
        Thread.sleep(500);
    }

    public static void verify_DailyDeal_ImageSoldout_Appears() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyDeals_Image_SoldOut"));
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementHasExpectedText("homepage_DailyDeals_Image_SoldOut", "SOLD OUT");
        }
    }

    public static String getShopCartItemsCount() {
        String shopCartItemsCount = null;
        if (ClientDataRetrieval.isRedesignClient()) {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("Shp_ItemsInCart"))) {
                shopCartItemsCount = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"))
                        .replace("(", "").replace(")", "").replaceAll("Cart", "").trim();
            }
        } else {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink"))) {
                shopCartItemsCount = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_ShoppingCartLink"));
            } else {
                shopCartItemsCount = "0";
            }
        }
        return shopCartItemsCount;
    }

    public static void verifyItemsInCartDecreased() throws Exception {
        String itemsInCart;
        String itemsInCart_Increased, finalPriceAfterAddingItemToCart;
        String itemsInCart_Decreased, finalPriceAfterRemovingItemFromCart;
        String shopCart_ItemCount = getShopCartItemsCount()
                .replace("(", "").replace(")", "").replaceAll("Cart", "").trim();

        if (!(shopCart_ItemCount.equals("0")) &&
                BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink"))) {
            BaseUI.log_Status("Shopping Cart is displayed");
            itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            System.out.println("Number of items in cart : " + itemsInCart);

            Navigation.navigate_ShoppingCart();
            finalPriceAfterAddingItemToCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));
            ShoppingCart.remove_AllItems();
            finalPriceAfterRemovingItemFromCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice")).trim();
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
                    finalPriceAfterAddingItemToCart, finalPriceAfterRemovingItemFromCart);
            //BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("nav_ShoppingCartLink"));
            BaseUI.verify_true_AndLog(BaseUI.elementAppears(Locator.lookupOptionalElement("Shp_ItemsInCart")) ||
                            !BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink")), "Shopping cart IS EMPTY",
                    "Shopping Cart IS NOT EMPTY");
        } else {
            BaseUI.log_Status("Shopping Cart is not displayed");
            // adding 1st item to cart
            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Navigation.navigate_Merchandise_AutoAccessories();
                Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));
            } else {
                Navigation.navigate_Merchandise_Jewelry();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }

            // adding 2nd item to cart
            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Navigation.navigate_Merchandise_AutoAccessories();
                Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));
            } else {
                Navigation.navigate_Merchandise_Jewelry();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }

            itemsInCart_Increased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            String finalPriceAfterAddingSecondItemToCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));

            // deleting 1st item from cart
            ShoppingCart.removeFirstItemFromShoppingCart();
            itemsInCart_Decreased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            finalPriceAfterRemovingItemFromCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart_Increased,
                    itemsInCart_Decreased);
            BaseUI.log_Status("Items in shopping cart decreased :" + itemsInCart_Decreased);
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
                    finalPriceAfterAddingSecondItemToCart, finalPriceAfterRemovingItemFromCart);
        }
    }

    public static void verifyItemsInCartIncreased() throws Exception {
        String itemsInCart;
        String itemsInCart_Increased, finalPriceBeforeAddingItemToCart, finalPriceAfterAddingItemToCart;

        String shopCart_ItemCount = getShopCartItemsCount()
                .replace("(", "").replace(")", "").replaceAll("Cart", "").trim();

        if (!(shopCart_ItemCount.equals("0")) &&
                BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink"))) {
            BaseUI.log_Status("Shopping Cart is displayed");
            itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            Navigation.navigate_ShoppingCart();
            finalPriceBeforeAddingItemToCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));

            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Navigation.navigate_Merchandise_Magazines();
                Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 10));
            } else {
                Navigation.navigate_Merchandise_Sunglasses();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }

            itemsInCart_Increased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            finalPriceAfterAddingItemToCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
                    finalPriceBeforeAddingItemToCart, finalPriceAfterAddingItemToCart);
        } else {
            BaseUI.log_Status("Shopping Cart is not displayed");
            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Navigation.navigate_Merchandise_Magazines();
                Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 24));
            } else {
                Navigation.navigate_Merchandise_Sunglasses();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }

            itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            finalPriceBeforeAddingItemToCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));

            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Navigation.navigate_Merchandise_Magazines();
                Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 10));
            } else {
                Navigation.navigate_Merchandise_Sunglasses();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }

            itemsInCart_Increased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            finalPriceAfterAddingItemToCart = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            System.out.println("Items in shopping cart increased :" + itemsInCart_Increased);
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
                    finalPriceBeforeAddingItemToCart, finalPriceAfterAddingItemToCart);
        }
    }

    public static void verifyShoppingCartDisplayed() {
        String shopCart_ItemCount = Homepage.getShopCartItemsCount()
                .replace("(", "").replace(")", "").replaceAll("Cart", "").trim();
        if (!(shopCart_ItemCount.equals("0")) &&
                BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink"))) {
            BaseUI.log_Status("Shopping Cart icon is displayed and has items: " + shopCart_ItemCount);
            BaseUI.verifyElementExists("Shp_ItemsInCart", null, null);
        } else {
            if (ClientDataRetrieval.isRedesignClient()) {
                BaseUI.verify_true_AndLog(shopCart_ItemCount.contains("0"), "Shopping cart icon is DISPLAYED and IS EMPTY",
                        "Shopping cart icon IS DISPLAYED and NOT EMPTY");
            } else {
                BaseUI.log_Status("Shopping Cart icon is not displayed and does not have items");
                BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("Shp_ItemsInCart"));
            }
        }
    }

    public static HashMap<String, String> clickDailyDeal() throws Exception {
        HashMap<String, String> homepagedailyDealData = null;
        Navigation.navigate_Home_viaHomeLink();
        if (ClientDataRetrieval.isRedesignClient()) {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_DailyDealTitle"))
                    && BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_Images"))) {
                // Add logic here to pull daily Deal info from Home page
                homepagedailyDealData = Homepage.return_Homepage_DailyDealInfo();
                Homepage.click_DailyDeal_SeeMoreDeals();
            } else {
                BaseUI.log_AndFail("Daily Deal is not setup in Admin Site");
            }

            if (BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_SoldOut_Status"))) {
                BaseUI.log_AndFail("Daily Deal is Sold Out. Need to update in Admin Site");
            }
        } else {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_Modal"))
                    && BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_SeeMoreDeals_Button"))) {
                // Add logic here to pull daily Deal info from Home page
                homepagedailyDealData = Homepage.return_Homepage_DailyDealInfo();
                Homepage.click_DailyDeal_SeeMoreDeals();
            } else {
                BaseUI.log_AndFail("Daily Deal is not setup in Admin Site");
            }

            if (BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_SoldOut_Button"))) {
                BaseUI.log_AndFail("Daily Deal is Sold Out. Need to update in Admin Site");
            }
        }

        return homepagedailyDealData;
    }

    public static void deleteItemsFromShoppingCart() throws Exception {
        String shopCart_ItemCount = Homepage.getShopCartItemsCount()
                .replace("(", "").replace(")", "").replaceAll("Cart", "").trim();
        if (!(shopCart_ItemCount.equals("0")) &&
                BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink"))) {
            Navigation.navigate_ShoppingCart();
            ShoppingCart.remove_AllItems();
        }
    }

    public static void clickLPG_LearnMoreLink() throws Exception {
        WebElement learnMoreLink = Locator.lookupRequiredElement("homepage_LearnMoreLnk");
        BaseUI.scroll_to_element(learnMoreLink);
        BaseUI.click(learnMoreLink);
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("homepage_lpgWaitElement", null, null, 20);
    }

    //Core clients only
    public static void clickLPG_LpgWidget() throws Exception {
        WebElement lpgWidget = Locator.lookupRequiredElement("homepage_ClickForDetailsWidget");
        BaseUI.scroll_to_element(lpgWidget);
        BaseUI.click(lpgWidget);
        BaseUI.waitForElementToBeDisplayed("homepage_lpgWaitElement", null, null, 20);
        Thread.sleep(500);
    }

    public static void verifyAllBrandItemsOnBrandCarousel() throws Exception {
        click_BrandCarousel_SeeAllBrandsLink();
        if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_SeeAllBrands_LandingPage_VerifyAllBrands_Header"));
        } else if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_SeeAllBrands_LandingPage_VerifyAllBrands_Header"));
            BaseUI.verifyElementHasExpectedPartialText("homepage_BrandCarousel_SeeAllBrands_LandingPage_VerifyAllBrands_Header", "Brands");
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BrandCarousel_SeeAllBrands_LandingPage_VerifyAllBrands_Header"));
            BaseUI.verifyElementHasExpectedPartialText("homepage_BrandCarousel_SeeAllBrands_LandingPage_VerifyAllBrands_Header", "All Brands");
        }
    }

    public static void verifyBrandNameHeaderDisplayed(String brandNameSelected) {
        String actualBrandNameHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_LandingPage_BrandNameHeader_Displayed"));
        BaseUI.verify_true_AndLog(
                (Locator.lookupRequiredElement("homepage_LandingPage_BrandNameHeader_Displayed").isDisplayed()),
                "Brand Name is displayed", "Header does not display Brand name");
        BaseUI.baseStringCompare("homepage_LandingPage_BrandNameHeader_Displayed",
                brandNameSelected.toLowerCase().replace("'", "").replace("\u0027", ""),
                actualBrandNameHeader.toLowerCase().replace("'", "").replace("\u0027", ""));
    }

    public static HashMap<String, String> return_Homepage_SneakPreview_DailyDealInfo() {
        HashMap<String, String> dailyDealInfo = new HashMap<String, String>();

        dailyDealInfo.put("dailyDealName",
                BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DailyDealProductName")).replace("...", ""));
        dailyDealInfo.put("dailyDeal_EndTime", BaseUI.get_Attribute_FromField(Locator
                .lookupRequiredElement("homepage_DailyDeals_SneakPreview_DealStartTime"), "innerText").substring(0,3));
        String savings = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DailyDeal_Savings"));
        savings = savings.split("\n")[0].replace("%", "").replace("\u0028", "")
                .replace("\u0029", "").replaceAll("Savings", "").replace("\u0025", "").trim();
        dailyDealInfo.put("savings", savings.trim());

        return dailyDealInfo;
    }

    //AARP Redesign User dropdown list validation
    public static void verify_AarpUser_Dropdown() throws Exception {

        String[] userDropDownOptions = { "Join", "My Account", "Donate", "Volunteer", "Community", "Newsletters", "Logout" };

        List<WebElement> allUserDropdown_Options = Locator
                .lookup_multipleElements("homepage_UserDropDownItems", null, null);
        List<String> user_DropdownList = new ArrayList<String>();

        for (WebElement dropdownOption : allUserDropdown_Options) {
            user_DropdownList.add(BaseUI.get_NonVisible_TextFromField(dropdownOption).trim());
        }

        BaseUI.verify_true_AndLog(user_DropdownList.size() == userDropDownOptions.length,
                "Dropdown matched expected values in size.", "Dropdown did not match expected values in size.");

        for (String dropdownOption : userDropDownOptions) {
            BaseUI.verify_true_AndLog(user_DropdownList.contains(dropdownOption),
                    "User dropdown option " + dropdownOption + " found in list.",
                    "User dropdown option " + dropdownOption + " NOT found in list.");
        }
    }

    public static ArrayList<String> return_BestSellersCarousel_ImageList() {

        ArrayList<String> bestSellerCarouselImageList = new ArrayList<>();
        ArrayList<WebElement> carouselImageElements = Locator
                .lookup_multipleElements("homepage_BestSellerCarousel_ImagesList", null, null);

        for (WebElement itemElement : carouselImageElements) {
            String categoryHeaderText = BaseUI.getTextFromField(itemElement);
            bestSellerCarouselImageList.add(categoryHeaderText);
        }

        return bestSellerCarouselImageList;
    }

    // Click Best Sellers Selected Carousel Right navigation arrow
    public static void click_BestSellerCarousel_RightNavigationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_BestSellerCarousel_RightNavigationArrow"));
        Thread.sleep(500);
    }

    // Click Best Sellers Selected Carousel Left navigation arrow
    public static void click_BestSellersCarousel_LeftNavigationArrow() throws Exception {
        BaseUI.click_js(Locator.lookupRequiredElement("homepage_BestSellerCarousel_LeftNavigationArrow"));
        Thread.sleep(500);
    }

    private static ArrayList<String> return_VisibleBestSellersCarouselItemsAltValues(){
        ArrayList<WebElement> listOfElements = Locator.lookup_multipleElements("homepage_BestSellerCarousel_ImagesList",
                null, null);
        ArrayList<String> originalElementAltValues = new ArrayList<String>();
        for (WebElement bestSellersCarouselElement : listOfElements) {
            if (bestSellersCarouselElement.isDisplayed()) {
                String alt = bestSellersCarouselElement.getAttribute("alt");
                BaseUI.verify_String_Not_EmptyOrNull(alt);
                originalElementAltValues.add(alt);
            }
        }

        return originalElementAltValues;
    }

    //Redesign Client Best Sellers Carousel Right Navigation Carousel
    public static void verify_ClickRightNavigationArrow_List_Of_VisibleBestSellersCarousel_ItemsChanged() throws Exception {

        ArrayList<String> originalElementAltValues = return_VisibleBestSellersCarouselItemsAltValues();

        click_BestSellerCarousel_RightNavigationArrow();

        ArrayList<String> newElementAltValues = return_VisibleBestSellersCarouselItemsAltValues();

        BaseUI.verify_true_AndLog(originalElementAltValues.size() > 2, "Found Elements before click.", "Could not find carousel elements");
        BaseUI.verify_true_AndLog(newElementAltValues.size() > 2, "Found Elements after click.", "Could not find carousel elements");

        BaseUI.verify_true_AndLog(!originalElementAltValues.equals(newElementAltValues), "Lists of visible Alt values changed.",
                "Lists of visible Alt values did NOT change.");
    }

    //Redesign Client Best Sellers Carousel Left Navigation validation
    public static void verify_ClickLeftNavigationArrow_List_Of_VisibleBestSellersCarouselItemsChanged() throws Exception {
        ArrayList<String> originalElementAltValues = return_VisibleBestSellersCarouselItemsAltValues();

        click_BestSellersCarousel_LeftNavigationArrow();

        ArrayList<String> newElementAltValues = return_VisibleBestSellersCarouselItemsAltValues();

        BaseUI.verify_true_AndLog(originalElementAltValues.size() > 2, "Found Elements before click.", "Could not find carousel elements");
        BaseUI.verify_true_AndLog(newElementAltValues.size() > 2, "Found Elements after click.", "Could not find carousel elements");

        BaseUI.verify_true_AndLog(!originalElementAltValues.equals(newElementAltValues), "Lists of visible Alt values changed.",
                "Lists of visible Alt values did NOT change.");
    }

    // Verify when any best sellers item clicked it is navigated to right landing page
    public static void clickAndVerify_BestSellerItem_BestSellersSelectedCarousel_LandingPageDetails() throws Exception {

        List<WebElement> allBestSellersItems = Locator
                .lookup_multipleElements("homepage_BestSellerCarousel_ImagesList", null, null);
        int numberOfBestSellersItems = allBestSellersItems.size();
        String bestSellersItemToBeSelected = BaseUI.random_NumberAsString(1, numberOfBestSellersItems);
        String bestSellersItemSelected_ProductName, bestSellersItemSelected_SavingPercentage;

        bestSellersItemSelected_ProductName = BaseUI
                .get_Attribute_FromField(Locator.lookupRequiredElement("homepage_BestSellerCarousel_ItemSelected",
                        bestSellersItemToBeSelected, null), "alt");
        bestSellersItemSelected_SavingPercentage = BaseUI
                .get_Attribute_FromField(Locator.lookupRequiredElement("homepage_BestSellerCarousel_ItemSelected_SavingsPercentage",
                        bestSellersItemToBeSelected, null), "innerText").replaceAll("savings", "Savings");
        String[] breadcrumbList = {"Home", "Merchandise", bestSellersItemSelected_ProductName};

        Homepage.click_BrandImage_ShopByBrand_Carousel("homepage_BestSellerCarousel_ItemSelected", bestSellersItemToBeSelected);
        Navigation.verify_OopsPage_DoesNOT_Appear();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchProduct_Header"));
        Navigation.verify_BreadcrumbList(breadcrumbList);
        BaseUI.verifyElementHasExpectedText("merchProduct_Header", bestSellersItemSelected_ProductName);
        BaseUI.verifyElementHasExpectedText("merchProduct_SavingsPercentage", bestSellersItemSelected_SavingPercentage);
    }

    public static void verifyPromoCode_OopsMessageOnPopUpModal(String promoCode) throws Exception {
        try {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Error_Title"));

            String expectedErrorMessage = MessageFormat.format(
                    "Oops. The Promo Code ({0}) that you have entered is either not valid or has expired.", promoCode);
            String promoErrorText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

            BaseUI.baseStringCompare("Promo Message", expectedErrorMessage, promoErrorText);
            Navigation.click_Closed_PromoCodeCloseErrorModal();
        } finally {
            BaseUI.log_Warning("Encountered error while closing the Promo Code modal. Refreshing browser");
            Navigation.refreshBrowser();
        }
    }

    //Redesign Core client
    public static void clickPromoCodeLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCodeLink"));
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("nav_PromoCode_SubmitButton", null, null, 20);
    }

    //Redesign Core client - Promo code modal
    public static void clickPromoCodeModal_WhereIFindTheseLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("promoCodeModal_WhereDoIFindTheseLink"));
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("promoCodeModal_EmailIcon", null, null, 20);
    }

    //Redesign Core client - Promo code modal
    public static void clickPromoCodeModal_WhereIFindThese_EmailPreferencesPageLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("promoCodeModal_WhereDoIFindThese_EmailPreferencesLink"));
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("emailPreferencesPage_Header", null, null, 30);
        BaseUI.waitForElementToBeDisplayed("emailPreferencesPage_SaveChangesBtn", null, null, 30);
    }

    //Redesign Core Promo Code modal Email Preferences Page Left Navigation link list validation
    public static void verify_PromoCodeModal_EmailPreferencesLandingPage_LeftNavigationList() throws Exception {

        String[] leftNavOptions = { "Profile", "Contact Info", "Address Book", "Email Preferences", "Change Password" };

        List<WebElement> allLeftNavigation_Options = Locator
                .lookup_multipleElements("emailPreferencesPage_LeftNavLinks", null, null);
        List<String> leftNavigationList = new ArrayList<String>();

        for (WebElement dropdownOption : allLeftNavigation_Options) {
            leftNavigationList.add(BaseUI.get_NonVisible_TextFromField(dropdownOption).trim());
        }

        BaseUI.verify_true_AndLog(leftNavigationList.size() == leftNavOptions.length,
                "Left Navigation List matched expected values in size.",
                "Left Navigation List did not match expected values in size.");

        for (String leftNavigationOption : leftNavOptions) {
            BaseUI.verify_true_AndLog(leftNavigationList.contains(leftNavigationOption),
                    "Left Navigation List option " + leftNavigationOption + " found in list.",
                    "Left Navigation List option " + leftNavigationOption + " NOT found in list.");
        }
    }

    public static void clickSelectedFeaturedDealWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails(String index) throws Exception {
        String selectedFeaturedDeal_ProductName = BaseUI.getTextFromField(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_ProductName", index, null));
        String selectedFeaturedDeal_SavingPercentage = BaseUI.getTextFromField(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_SavingsPercentage", index, null));
        BaseUI.click(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_Image", index, null));
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("merch_ProductName", null, null, 30);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_ProductName"));
        BaseUI.verifyElementHasExpectedText("merch_ProductName", selectedFeaturedDeal_ProductName);
        BaseUI.verifyElementHasExpectedText("merchProduct_SavingsPercentage",
                selectedFeaturedDeal_SavingPercentage + " Savings");
    }


    public static void clickFeaturedSweepstakesWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails() throws Exception {
        String featuresSweepstakes_ProductName = BaseUI.getTextFromField(Locator.lookupRequiredElement("homePage_FeaturedSweepstakesWidget_ProductName"));
        String featuredSweepstakes_RetailValue = BaseUI.getTextFromField(Locator.lookupRequiredElement("homePage_FeaturedSweepstakesWidget_RetailValue"));
        BaseUI.click(Locator.lookupRequiredElement("homePage_FeaturedSweepstakesWidget_Image"));
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("sweepstakes_LandingPage_ProductName", null, null, 30);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("sweepstakes_LandingPage_ProductName"));
        BaseUI.verifyElementHasExpectedText("sweepstakes_LandingPage_ProductName", featuresSweepstakes_ProductName);
        BaseUI.verifyElementHasExpectedText("sweepstakes_LandingPage_RetailValue",
                featuredSweepstakes_RetailValue);
    }

    public static void clickFeaturedAuctionsWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails() throws Exception {
        String featuresAuctions_ProductName = BaseUI.getTextFromField(Locator.lookupRequiredElement("homePage_FeaturedAuctionsWidget_ProductName"));
        String featuredAuctions_RetailValue = BaseUI.getTextFromField(Locator.lookupRequiredElement("homePage_FeaturedAuctionsWidget_RetailValue"));
        BaseUI.click(Locator.lookupRequiredElement("homePage_FeaturedAuctionsWidget_Image"));
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("auctions_LandingPage_ProductName", null, null, 30);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctions_LandingPage_ProductName"));
        BaseUI.verifyElementHasExpectedText("auctions_LandingPage_ProductName",  featuresAuctions_ProductName);
        BaseUI.verifyElementHasExpectedText("auctions_LandingPage_RetailValue",
                featuredAuctions_RetailValue +  ".00");
    }
}
