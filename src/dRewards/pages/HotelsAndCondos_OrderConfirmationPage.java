package dRewards.pages;

import java.util.HashMap;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class HotelsAndCondos_OrderConfirmationPage {

	public static void validate_SearchResults(HashMap<String, String> hotelData, String fromDate, String toDate,
			String adultCount, String childCount, String childAge) throws Exception {

		verify_HotelAddress_Info(hotelData);

		// Validate Adult and Child Info

		verify_Checkin_And_Checkout_Dates(fromDate, toDate);

		verify_GuestInfo(adultCount, childCount, childAge);

		verify_Hotel_Costs(hotelData);
		
		// return taxesAndFees;
	}

	public static void verify_ApplicationError_NOT_Present(){

		BaseUI.verify_true_AndLog(!BaseUI.pageSourceContainsString("Could not reserve the room due to a hotel system error."),
				"Did Not find Application Error.", "Application Error present.");

	}

	public static void verify_Hotel_Costs(HashMap<String, String> hotelData) {
		// Validate ACTUAL totals
		//
		String taxesAndFees = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_TaxesAndFees"))
				.replace("$", "").replace(",", "");

		String totalCost_AfterFees = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_TotalCost"))
				.replace("$", "").replace(",", "");
		Double expectedAmountDouble = Double.parseDouble(taxesAndFees)
				+ Double.parseDouble(hotelData.get("hotelRate").replace("$", "").replace(",", ""));
		expectedAmountDouble = BaseUI.round_Double_ToPassedInDecimalPlaces(expectedAmountDouble, 2);

		String expectedAmount = expectedAmountDouble.toString();
		expectedAmount = expectedAmount.split("\\.")[1].length() == 1 ? expectedAmount + "0" : expectedAmount;
		BaseUI.baseStringCompare("ACTUAL Total Cost", expectedAmount, totalCost_AfterFees);
		
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String actual_points = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_Points"))
					.replace(",", "");
			BaseUI.baseStringCompare("Actual Points", hotelData.get("points"), actual_points);
		}
		Double expected_finalACTUALTotal = Double.parseDouble(hotelData.get("youPay"))
				+ Double.parseDouble(taxesAndFees);
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			String cashRewardsDeduction = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_CashRewards")).replace("$", "")
					.replace(",", "");
			expected_finalACTUALTotal = expected_finalACTUALTotal - Double.parseDouble(cashRewardsDeduction);

		}
		expected_finalACTUALTotal = BaseUI.round_Double_ToPassedInDecimalPlaces(expected_finalACTUALTotal, 2);
		String expected_finalACTUALTotalStr = expected_finalACTUALTotal.toString();
		expected_finalACTUALTotalStr = expected_finalACTUALTotalStr.split("\\.")[1].length() == 1
				? expected_finalACTUALTotalStr + "0" : expected_finalACTUALTotalStr;

		String finalACTUALTOTAL = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_ACTUALTotalCost"))
				.replace("$", "").replace(",", "");

		finalACTUALTOTAL = finalACTUALTOTAL.split("\\.")[1].length() == 1 ? finalACTUALTOTAL + "0" : finalACTUALTOTAL;

		BaseUI.baseStringCompare("ACTUAL Total Price after points", expected_finalACTUALTotalStr, finalACTUALTOTAL);
		BaseUI.verifyElementHasExpectedText("hotelConfirmation_RetailRate", hotelData.get("hotelRate"));
	}

	public static void verify_GuestInfo(String adultCount, String childCount, String childAge) {
		String guestInfoOnPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_GuestInfo"));
		String childAges = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_ChildAges"));

		Integer childCountInt = Integer.parseInt(childCount);
		if (childCountInt > 0) {
			String expectedAdultChildLine = "";
			String expectedChildrenAgesLine = "Ages of Children:";
//			if (ClientDataRetrieval.clientName.equals("Citi-9485")) {
//				expectedChildrenAgesLine += "";
//			}

			// If we have 1 Child
			if (childCountInt == 1) {
				expectedAdultChildLine = adultCount + " Adults   1 Child";
				expectedChildrenAgesLine += childAge;
				// If we have more than 1 child.
			} else {
				expectedAdultChildLine = adultCount + " Adults   " + childCount + " Children";
				expectedChildrenAgesLine += childAge;
				for (int i = 1; i < childCountInt; i++) {
//					if (ClientDataRetrieval.clientName.equals("Citi-9485")) {
//						expectedChildrenAgesLine += "," + childAge;
//					} else {
						expectedChildrenAgesLine += "," + childAge;
//					}
				}
			}
			BaseUI.baseStringCompare("Children Ages", expectedChildrenAgesLine, childAges);
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("hotelConfirmation_GuestInfo"),
					expectedAdultChildLine);
			// if we have no children.
		} else {
			BaseUI.baseStringCompare("Number of Adults", "Number of Adults: " + adultCount, guestInfoOnPage);
		}

	}

	public static void verify_Checkin_And_Checkout_Dates(String fromDate, String toDate) {
		BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("hotelConfirmation_CheckinDate"), fromDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("hotelConfirmation_CheckoutDate"), toDate);
	}

	public static void verify_HotelAddress_Info(HashMap<String, String> hotelData) {
		BaseUI.verifyElementHasExpectedText("hotelConfirmation_HotelName", hotelData.get("hotelName"));

		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			String address = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_HotelAddress"))
					.replace("    ", " ").replace(",  ", ",");

			address = address.substring(0, address.lastIndexOf("\n"));
			String adressSTR = address.replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ");
			BaseUI.baseStringCompare("Address", hotelData.get("address").replace(", ", ", ").replaceAll("[\r\n]+", " "), adressSTR.replaceAll("[\r\n]+", " "));
		} else {
			String address = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_HotelAddress"))
					.replace("    ", " ").replace(",  ", ",");

			address = address.substring(0, address.lastIndexOf("\n"));
			String adressSTR = address.replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ");
			BaseUI.baseStringCompare("Address", hotelData.get("address").replaceAll("[\r\n]+", " "),
					adressSTR.replaceAll("[\r\n]+", " "));
		}

	}

	public static String return_ReservationNumber() {
		String reservationNumber = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_ReservationNumber"));
		return reservationNumber;
	}

	public static void verify_HotelLogo_Accurate(String expectedLogoURL) {
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("hotelConfirmation_HotelLogo"), "src",
				expectedLogoURL);

	}

	public static void verify_addressInfo_Accurate(String expectedAddress) {
		String adressInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_GuestAddressInfo"));

		BaseUI.baseStringCompare("Address Info", expectedAddress, adressInfo);
	}
}// end of class
