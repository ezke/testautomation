package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Locator;

public class Auctions_OpenAuctions {

	//Assumes first auction is the Auction we are claiming
	public static void verify_Description(String auctionDescription) {
		String description = BaseUI.getTextFromField(Locator.lookupRequiredElement("openAuct_Auctions_FirstAuction"));
		BaseUI.baseStringCompare("Description", auctionDescription, description);
	}
	
	public static String return_EndTime(){
		String expectedEndTime = BaseUI.getTextFromField(Locator.lookupRequiredElement("openAuct_Auctions_First_EndTime"));
		expectedEndTime = expectedEndTime.split("\\:")[1].replace(" -", "");
		
		return expectedEndTime;
	}
	
	//Assumes first auction is the Auction we are claiming
	public static void expand_AuctionName() throws Exception{
		BaseUI.click(Locator.lookupRequiredElement("openAuct_Auctions_FirstAuction"));
		Thread.sleep(1000);
	}
	
	public static void click_ViewAllBids() throws Exception{
		BaseUI.click(Locator.lookupRequiredElement("openAuct_ViewAllBids_FirstLink"));
		Thread.sleep(500);
	}
	
	public static void expandAuctions_And_ClickViewAllBids_Link() throws Exception{
		expand_AuctionName();

		 if (! ClientDataRetrieval.isRedesignClient()) {
			 click_ViewAllBids();
			 
		}	
	}
	
	
}// End of Class