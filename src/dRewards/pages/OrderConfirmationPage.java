package dRewards.pages;

import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import dRewards.ClassObjects.PricingDetails;
import dRewards.ClassObjects.Product;
import dRewards.ClassObjects.ShippingAddress;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class OrderConfirmationPage {

	public static ShippingAddress retrieve_ShippingAddress() {
		ShippingAddress shippingAddress = new ShippingAddress();
		
		if (ClientDataRetrieval.isRedesignClient()) {
			shippingAddress.firstName = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_RecipientName"));
			// split by space character and take first half of split.
			shippingAddress.firstName = shippingAddress.firstName.split("\\s+")[0];
			// can combine the above 2 steps.
			shippingAddress.lastName = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_RecipientName"))
					.split("\\s+")[1];
			shippingAddress.address1 = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_Address"));
			shippingAddress.city = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_CityStateZip"));
			shippingAddress.country = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_Country"));
		} else {
			shippingAddress.firstName = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "Recipient Name:", null));
			// split by space character and take first half of split.
			shippingAddress.firstName = shippingAddress.firstName.split("\\s+")[0];
			// can combine the above 2 steps.
			shippingAddress.lastName = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "Recipient Name:", null))
					.split("\\s+")[1];
			
			shippingAddress.address1 = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "Address:", null));
			shippingAddress.city = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "City:", null));
			shippingAddress.state = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "State/Province:", null));
			shippingAddress.zip = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "Zip/Postal Code:", null));
			shippingAddress.country = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "Country:", null));
			shippingAddress.phoneNumber = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("orderConf_ShippinInfo_ByText", "Phone Number:", null));
		}

		return shippingAddress;
	}

	public static void verify_ShippingInformation(String recipientName, String address, String city, String state,
			String zip, String country, String phoneNumber) {
		ShippingAddress shipAddress = retrieve_ShippingAddress();

		BaseUI.baseStringCompare("Recipient Name", recipientName, shipAddress.firstName + " " + shipAddress.lastName);
		BaseUI.baseStringCompare("Address", address, shipAddress.address1);

		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("Address", address, shipAddress.address1);
			BaseUI.baseStringCompare("City", (city+", "+state+" "+"*****"), shipAddress.city);
			//BaseUI.baseStringCompare("Country", country, shipAddress.country);
		} else {
			BaseUI.baseStringCompare("Address", address, shipAddress.address1);
			BaseUI.baseStringCompare("City", city, shipAddress.city);
			BaseUI.baseStringCompare("State", state, shipAddress.state);
			BaseUI.baseStringCompare("Zip Code", zip, shipAddress.zip);
			
			String formattedPhoneNumber = "******" + phoneNumber.substring(6, phoneNumber.length());
			BaseUI.baseStringCompare("Phone Number", formattedPhoneNumber, shipAddress.phoneNumber);
		}
		String formattedCountry = country.equals("United States") ? "US" : country;
		BaseUI.baseStringCompare("Country", formattedCountry, shipAddress.country);
	}

	public static void verify_BillingInformation(String cardHolder, String address, String city, String state,
			String cardType, String cardNumber, String zip) {

		String cardholder_OrderConfirm, paymentAddress, paymentCity, paymentState, paymentCountry;
		String paymentZip, paymentCardType, expected_creditCardFormatted, paymentCardNumber;
		
		if (ClientDataRetrieval.isRedesignClient()) {
			ShippingAddress billAddress = retrieve_BillingAddress();
			cardholder_OrderConfirm =  billAddress.firstName + " " + billAddress.lastName;
			paymentAddress = billAddress.address1;
			paymentCity = billAddress.city;
			paymentCountry = billAddress.country;
			expected_creditCardFormatted = "ending in "
					+ cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
			paymentCardNumber = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConf_BillingInfo_Cardnumber"));
			
			BaseUI.baseStringCompare("City", (city+", "+state+" "+"*****"), paymentCity);
			
			BaseUI.baseStringCompare("Country", "US", paymentCountry);
			
			BaseUI.baseStringCompare("Card Number", "MASTERCARD "+expected_creditCardFormatted, paymentCardNumber);
		} else {
			cardholder_OrderConfirm = OrderConfirmationPage.get_PaymentInfo_ByText("Cardholder Name");
			paymentAddress = OrderConfirmationPage.get_PaymentInfo_ByText("Address");
			paymentCity = OrderConfirmationPage.get_PaymentInfo_ByText("City");
			paymentState = OrderConfirmationPage.get_PaymentInfo_ByText("State/Province");
			paymentZip = OrderConfirmationPage.get_PaymentInfo_ByText("Zip/Postal Code");
			paymentCardType = OrderConfirmationPage.get_PaymentInfo_ByText("Card Type");
			expected_creditCardFormatted = "XXXX-XXXX-XXXX-"
					+ cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
			paymentCardNumber = OrderConfirmationPage.get_PaymentInfo_ByText("Card Number");

			String paymentExpirationDate = OrderConfirmationPage.get_PaymentInfo_ByText("Expiration Date");
			
			BaseUI.baseStringCompare("State/Province", state, paymentState);
			
			BaseUI.baseStringCompare("Zip/Postal Code", "*****", paymentZip);
			
			BaseUI.baseStringCompare("Expiration Date", "** ****", paymentExpirationDate);
			
			BaseUI.baseStringCompare("Card Type", cardType, paymentCardType);
			
			BaseUI.baseStringCompare("Card Number", expected_creditCardFormatted, paymentCardNumber);
		}
		
		
        BaseUI.baseStringCompare("Cardholder Name", cardHolder, cardholder_OrderConfirm);
		
		BaseUI.baseStringCompare("Address", address, paymentAddress);
	}
	
	
	public static ShippingAddress retrieve_BillingAddress() {
		ShippingAddress shippingAddress = new ShippingAddress();
		
		shippingAddress.firstName = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("orderConf_BillingInfo_RecipientName"));
		// split by space character and take first half of split.
		shippingAddress.firstName = shippingAddress.firstName.split("\\s+")[0];
		// can combine the above 2 steps.
		shippingAddress.lastName = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("orderConf_BillingInfo_RecipientName"))
				.split("\\s+")[1];
		shippingAddress.address1 = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("orderConf_BillingInfo_Address"));
		shippingAddress.city = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("orderConf_BillingInfo_CityStateZip"));
		shippingAddress.country = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("orderConf_BillingInfo_Country"));

		return shippingAddress;	
	}

	public static void verify_ProductInfo(TableData originalData) throws Exception {

		TableData reviewOrder_ProductData = get_Table_Data();
	
		reviewOrder_ProductData.replace_Characters_FromBeginning("Gift Card - ", "", "Product Name");

		BaseUI.verify_TableColumns_Match("Product Name", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("quantity", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("pricing_Retail", originalData.data, reviewOrder_ProductData.data);

		if(!ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			originalData.remove_ExtraSpaces("pricing_Points");
			reviewOrder_ProductData.remove_ExtraSpaces("pricing_Points");
		}
		
		BaseUI.verify_TableColumns_Match("pricing_Points", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("pricing_YouPay", originalData.data, reviewOrder_ProductData.data);

		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.verify_TableColumns_Match("Percent Savings", originalData.data, reviewOrder_ProductData.data);
		}		
	}

	public static void verify_PricingDetails(PricingDetails expectedDetails) throws Exception {
		PricingDetails pricingDetails;
		if (ClientDataRetrieval.isRedesignClient()) {
			pricingDetails = retrieve_OrderConfirmationPage_PricingDetails();
			
			BaseUI.baseStringCompare("Points", expectedDetails.points, "- "+pricingDetails.points);
		} else {
			pricingDetails = retrieve_PricingDetails();
			if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				BaseUI.baseStringCompare("Points", expectedDetails.points, pricingDetails.points);
			}
			BaseUI.baseStringCompare("Tax Cost", expectedDetails.tax_Cost, pricingDetails.tax_Cost);
		}

		//BaseUI.baseStringCompare("Order Total", expectedDetails.order_Total, pricingDetails.order_Total);
		BaseUI.baseStringCompare("You Pay", expectedDetails.priceAfterPoints, pricingDetails.priceAfterPoints);
		BaseUI.baseStringCompare("Retail Price", expectedDetails.retailPrice, pricingDetails.retailPrice);
		BaseUI.baseStringCompare("Percent Savings", expectedDetails.savings_Percent, pricingDetails.savings_Percent);
		BaseUI.baseStringCompare("Shipping Cost", expectedDetails.shipping_Cost, pricingDetails.shipping_Cost);
		
		
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_PriceDetails_Savings"))){
			BaseUI.baseStringCompare("Savings", expectedDetails.savings, pricingDetails.savings);
		}

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			String allstateCash = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConf_AllstateCashAmount"))
					.replace("-", "\u2013").trim();
			String expected_AllstateCash = expectedDetails.allstate_Cash;
			if (!expected_AllstateCash.equals("$0.00")) {
				expected_AllstateCash = "\u2013 " +expected_AllstateCash.trim();
			}
			if(expected_AllstateCash.equals("$0.00")) {
				BaseUI.baseStringCompare("Allstate Cash", expected_AllstateCash, allstateCash);
			} else {
				BaseUI.baseStringCompare("Allstate Cash", expected_AllstateCash.trim(), allstateCash.trim());
			}
		}
	}

	// Pass in the payment information you want and it will return the element's
	// text.
	// Example: Pass in "Cardholder Name" and you'll get the Cardholder.
	public static String get_PaymentInfo_ByText(String fieldText) {
		WebElement elementToGet = Locator.lookupRequiredElement("orderConf_BillingInfo_ByText", fieldText, null);

		return BaseUI.getTextFromField(elementToGet);
	}

	public static void click_ContinueShopping() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("orderConf_ContinueShoppingBttn_Top"));
		Thread.sleep(500);
	}

	// Return a List of our Product Classes that contains all of the product
	// data.
	public static ArrayList<Product> retrieve_AllProductData() {
		ArrayList<Product> products = new ArrayList<Product>();
		ArrayList<WebElement> productRows = new ArrayList<WebElement>();
		productRows.addAll(Locator.lookup_multipleElements("checkout_ProductRows", null, null));

		for (WebElement productRow : productRows) {
			Product product = new Product();
			product.productName = productRow.findElement(By.xpath(".//div[@class='infoWrap']/a")).getText();
			product.quantity = productRow.findElement(By.xpath(".//li[@class='qty_cart_item']")).getText();
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				product.pricing_Retail = productRow.findElement(By.xpath(".//td[@class='retail']")).getText();
				product.pricing_Points = productRow.findElement(By.xpath(".//span[@class='incentives']")).getText();
				product.pricing_YouPay = productRow.findElement(By.xpath(".//td[@class='price']")).getText();	
			}else{
				product.pricing_Retail = productRow.findElement(By.xpath(".//dd[@class='retail']")).getText();
				product.pricing_Points = productRow.findElement(By.xpath(".//dd[@class='incentives']")).getText();
				product.pricing_YouPay = productRow.findElement(By.xpath(".//dd[@class='price']")).getText();
			}
			// product.pricing_PercentSavings =
			// productRow.findElement(By.xpath(".//div[@class='price-matrix']/span"))
			// .getText();

			products.add(product);
		}

		return products;
	}

	// gets the table data utilizing our generic Base Class for retrieving table
	// data.
	public static TableData get_Table_Data() throws Exception {
		HashMap<String, String> fields_toGet = new HashMap<String, String>();
		// fields_toGet.put("Product Name", ".//div[@class='infoWrap']/a");
		// fields_toGet.put("quantity", ".//input[contains(@class,'qty')]");
		if (ClientDataRetrieval.isRedesignClient()) {
			if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
				||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))) {
				fields_toGet.put("Product Name", ".//h3");
			} else {
				fields_toGet.put("Product Name", ".//h4");
			}
			fields_toGet.put("quantity", ".//ul[@class='order-details']/li[contains(text(),'Quantity')]/strong");
			fields_toGet.put("pricing_Retail", ".//div[@class='price-matrix']/div[@class='retail-price']");
			fields_toGet.put("pricing_Points", ".//div[@class='price-matrix']/div[@class='points-required']");
			fields_toGet.put("pricing_YouPay", ".//div[@class='price-matrix']/div[@class='rewards-price']");
			//fields_toGet.put("Percent Savings", ".//div[@class='price-matrix']/div[@class='savings']");
		} else {
			fields_toGet.put("quantity", ".//li[@class='qty_cart_item']");
			
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				fields_toGet.put("Product Name", ".//ul/li[@class='productname firstchild']");
				fields_toGet.put("pricing_Retail", ".//td[@class='retail']");
				fields_toGet.put("pricing_Points", ".//span[@class='incentives']");
				fields_toGet.put("pricing_YouPay", ".//td[@class='price']");	
			}else{
				fields_toGet.put("Product Name", "./ul/li/a");
				fields_toGet.put("pricing_Retail", ".//dd[@class='retail']");
				fields_toGet.put("pricing_Points", ".//dd[@class='incentives']");
				fields_toGet.put("pricing_YouPay", ".//dd[@class='price']");
			}
			
			fields_toGet.put("Percent Savings", ".//div[@class='price-matrix']/span");
		}
		
		TableData table_Data = new TableData();
		table_Data.data = BaseUI.tableExtractor("orderReviewPage_ProductRows", fields_toGet);
		
		table_Data.split_column("Product Name");
		table_Data.trim_column("Product Name");

		if (ClientDataRetrieval.isRedesignClient()) {
			table_Data.remove_Character(" ", "pricing_Retail");
			table_Data.replace_Characters_FromBeginning("Retail:", "", "pricing_Retail");
			table_Data.remove_Character("points", "pricing_Points");
			table_Data.trim_column("pricing_Points");
			table_Data.replace_Characters_FromBeginning("after", "", "pricing_Points");
		}
			
		return table_Data;

	}

	public static PricingDetails retrieve_PricingDetails() {
		PricingDetails pricingDetails = new PricingDetails();
		pricingDetails.retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_RetailPrice"));
		pricingDetails.points = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_points"));

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			pricingDetails.points = pricingDetails.points.split("\\s")[1];
		}
		pricingDetails.priceAfterPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_priceAfterPoints"));
		pricingDetails.savings_Percent = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_savingsPercent"));
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_PriceDetails_Savings"))){
			pricingDetails.savings = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_Savings"));
			pricingDetails.savings= pricingDetails.savings.split("\\s")[1];
		}
		pricingDetails.shipping_Cost = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_shippingCost"));
		pricingDetails.order_Total = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConf_OrderTotal"));
		pricingDetails.order_Total = pricingDetails.order_Total.split("\\n")[0];
		pricingDetails.tax_Cost = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConf_TaxCost"));
		
		return pricingDetails;
	}
	
	
	public static PricingDetails retrieve_OrderConfirmationPage_PricingDetails() {
		PricingDetails pricingDetails = new PricingDetails();
		pricingDetails.retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_RetailPrice"));
		pricingDetails.points = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_PointsDeduction"));

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			pricingDetails.points = pricingDetails.points.split("\\s")[1];
		}
		pricingDetails.priceAfterPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_YouPay"));
		pricingDetails.savings_Percent = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_PercentSavings"));
//		if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_PriceDetails_Savings"))){
//			pricingDetails.savings = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_Savings"));
//			pricingDetails.savings= pricingDetails.savings.split("\\s")[1];
//		}
		pricingDetails.shipping_Cost = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_ShippingCost"));
		pricingDetails.order_Total = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_OrderTotal"));
		pricingDetails.order_Total = pricingDetails.order_Total.split("\\n")[0];
		pricingDetails.tax_Cost = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfirmPage_PricingDetails_EstimateSalesTax"));
		
		return pricingDetails;
	}

	public static String VerifyOrderID() throws Exception {

		WebElement ordersplit = Locator.lookupRequiredElement("orderConfNumber", null, null);
		String OrderNumber = BaseUI.getTextFromField(ordersplit);
		OrderNumber = OrderNumber.split("-")[1].trim();

		OrderConfirmationPage.click_ContinueShopping();
		Thread.sleep(2000);
		Navigation.navigate_Account();
		Thread.sleep(2000);

		WebElement ordersplitAccount = Locator.lookupRequiredElement("orderIdNum", null, null);
		String OrderNumberAccount = BaseUI.getTextFromField(ordersplitAccount);
		OrderNumberAccount = OrderNumberAccount.split("-")[1].trim();

		BaseUI.baseStringCompare("orderIdNum", OrderNumber, OrderNumberAccount);

		return OrderNumber;
	}
	
	public static String getOrderID() throws Exception {
		
		String orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		
		return orderID;
	}
}








