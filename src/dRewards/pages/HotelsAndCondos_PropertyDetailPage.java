package dRewards.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class HotelsAndCondos_PropertyDetailPage {

    public static void validate_SearchResults(HashMap<String, String> hotelData, String fromDate, String toDate,
                                              String adultCount, String childCount, String childAge) throws Exception {
        verify_checkin_And_checkout_Dates(fromDate, toDate);
        verify_Adults_AndChildren_Dropdowns(adultCount, childCount, childAge);

        verify_HotelPropertyDetails(hotelData);

        // Booking Details information.
        verify_BookingDetails_CheckinAndCheckout_Dates(fromDate, toDate);
        verify_BookingDetails_AdultsAndChildren(adultCount, childCount, childAge);
    }

    public static void verify_HotelPropertyDetails(HashMap<String, String> hotelData) {

        BaseUI.verifyElementHasExpectedPartialText("hotelPropertyDetails_HotelName", hotelData.get("hotelName"));

        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
            String address = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_Address"))
                    .replace(" US ", " ").replaceAll("[\r\n]+", " ").replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();
            String expectedAddress = hotelData.get("address").replace(",  ", ", ").replaceAll("[\r\n]+", " ");
            BaseUI.baseStringCompare("Address", expectedAddress, address);
        } else {
            String address = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_Address"))
                    .replaceAll("[\r\n]+", " ").replace(" US ", " ").replace(", ", ",  ").replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();

            BaseUI.baseStringCompare("Address", hotelData.get("address").replaceAll("[\r\n]+", " "), BaseUI.remove_ExtraSpaces(address));
        }
        BaseUI.verifyElementHasExpectedText("hotelPropertyDetails_TotalAmount_NotReduced", hotelData.get("hotelRate"));
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            String pointsDeduction = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_PointsDeduction")).replace("-", "")
                    .replace(",", "");
            BaseUI.baseStringCompare("Points Reduction", hotelData.get("points"), pointsDeduction);
        }
        // BaseUI.baseStringCompare("hotelPropertyDetails_PointsDeduction",
        // hotelData.get("points"), pointsDeduction);

        String totalAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_FinalTotal"))
                .replace("$", "").replace(",", "");
        String expectedYouPay = hotelData.get("youPay");
        BaseUI.baseStringCompare("Total Amount", expectedYouPay, totalAmount);
    }

    public static void verify_BookingDetails_AdultsAndChildren(String adultCount, String childCount, String childAge) {
        String bookingDetails = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_BookingDetails"));
        String numberAdults = bookingDetails.split("\\n")[2];

        BaseUI.baseStringCompare("Number of Adults", "Number of Adults: " + adultCount, numberAdults);
        Integer childCountInt = Integer.parseInt(childCount);
        if (childCountInt > 0) {
            String numberChildren = bookingDetails.split("\\n")[3];
            String childrenAges = bookingDetails.split("\\n")[4];
            // If there was one child.
            if (childCountInt == 1) {
                BaseUI.baseStringCompare("Ages of Children", "Ages of Children: " + childAge, childrenAges);
            }
            // More than 1 child.
            else {
                String childAgesStringFormatted = "Ages of Children: " + childAge;
                for (int i = 1; i < childCountInt; i++) {
                    childAgesStringFormatted += ", " + childAge;
                }
                BaseUI.baseStringCompare("Ages of Children", childAgesStringFormatted, childrenAges);
            }

            BaseUI.baseStringCompare("Number Of Children", "Number of Children: " + childCount, numberChildren);
        }
    }

    public static void verify_BookingDetails_CheckinAndCheckout_Dates(String fromDate, String toDate) {
        String bookingDetails = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_BookingDetails"));
        String checkinDate = bookingDetails.split("\\n")[0];
        String checkoutDate = bookingDetails.split("\\n")[1];

        BaseUI.baseStringCompare("Check In Date", "Check in " + fromDate, checkinDate);
        BaseUI.baseStringCompare("Check Out Date", "Check out " + toDate, checkoutDate);
    }

    public static void verify_HotelLogo_Accurate(String expectedLogoURL) {
        BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("hotelPropertyDetails_HotelLogo"),
                "src", expectedLogoURL);
    }

    public static void verify_Top_BookItNow_ButtonAppears() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_BookItNowTOP_Button"));
        BaseUI.verifyElementEnabled(Locator.lookupRequiredElement("hotelPropertyDetails_BookItNowTOP_Button"));
    }

    public static void click_TOP_BookItNow_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("hotelPropertyDetails_BookItNowTOP_Button"));
        Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void verify_checkin_And_checkout_Dates(String checkinDate, String checkoutDate) {
        String checkinDate_OnPage = BaseUI
                .getTextFromInputBox(Locator.lookupRequiredElement("hotelPropertyDetails_checkinDate"));
        String checkoutDate_OnPage = BaseUI
                .getTextFromInputBox(Locator.lookupRequiredElement("hotelPropertyDetails_checkoutDate"));

        BaseUI.baseStringCompare("Check In Date", checkinDate, checkinDate_OnPage);
        BaseUI.baseStringCompare("Check In Date", checkoutDate, checkoutDate_OnPage);
    }

    // This method assumes all children are of the same age.
    public static void verify_Adults_AndChildren_Dropdowns(String numberAdults, String numberChildren,
                                                           String childrenAge) throws Exception {

        BaseUI.verify_Value_Selected_InDropdown(Locator.lookupRequiredElement("hotelPropertyDetails_numberOfAdultsDropdown"),
                numberAdults);
        BaseUI.verify_Value_Selected_InDropdown(Locator.lookupRequiredElement("hotelPropertyDetails_numberOfChildrenDropdown"),
                numberChildren);

        Integer numberOfChildren = Integer.parseInt(numberChildren);
        for (Integer i = 1; i <= numberOfChildren; i++) {
            BaseUI.verify_Value_Selected_InDropdown(
                    Locator.lookupRequiredElement("hotelPropertyDetails_ChildAge_Dropdown_ByChildNumber", i.toString(), null),
                    childrenAge);
        }
    }

    public static void click_PropertyFeatures_Tab() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("hotelPropertyDetails_Tabs_ByName", "Property Features", null));
        Thread.sleep(300);
    }

    public static void verify_PropertyFeatures_Tab() throws Exception {
        click_PropertyFeatures_Tab();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_PropertyFeatures_TopTitle"));
        BaseUI.verifyElementHasExpectedText("hotelPropertyDetails_PropertyFeatures_TopTitle", "Property Rating:");
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_PropertyFeatures_CheckinAndOut_Text"));
        BaseUI.verifyElementHasExpectedText("hotelPropertyDetails_PropertyFeatures_CheckinAndOut_Text",
                "Check-in and Check-out times:");
    }

    public static void click_Photos_Tab() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("hotelPropertyDetails_Tabs_ByName", "Photos", null));
        BaseUI.waitForElementToBeDisplayed("hotelPropertyDetails_PhotoTab_ElementToWaitFor", null, null, 40);
        Thread.sleep(500);
    }

    public static void verify_Photos_Tab() throws Exception {
        click_Photos_Tab();
        if(BaseUI.elementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_NoPhotosMsg"))) {
            BaseUI.verifyElementHasExpectedText("hotelPropertyDetails_NoPhotosMsg",
                    "We are sorry, there are no photos available for this property.");
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_Photos_Image"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_Photos_RightArrow"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_Photos_Container"));
        }
    }

    public static void click_MapDirections_Tab() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("hotelPropertyDetails_Tabs_ByName", "Maps & Directions", null));
        //Thread.sleep(3000);
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("hotelPropertyDetails_MapDirections_EndAddress_TextBox", null, null, 50);
        Thread.sleep(3000);

        try {
            ArrayList<WebElement> availableIframes = Locator.lookup_multipleElements("nav_ListIframes", null, null);

            for (int i = 0; i < availableIframes.size(); i++) {
                BaseUI.switch_ToIframe(i);
                BaseUI.wait_forPageToFinishLoading();
            }
        } catch (Exception e) {
            BaseUI.log_Warning("Encountered error while attempting to wait for all iframes to load " + e.getMessage());
        }
        BaseUI.switch_ToDefaultContent();
    }

    public static void verify_MapDirections_Tab(HashMap<String, String> hotelData) throws Exception {
        click_MapDirections_Tab();

        BaseUI.switch_ToIframe(Locator.lookupRequiredElement("hotelPropertyDetails_GoogleMaps_Iframe"));
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("hotelPropertyDetails_MapDirections_GoogleMaps_MapTab", null, null, 50);

        try {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("hotelPropertyDetails_MapDirections_GoogleMaps_MapTab"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelPropertyDetails_MapDirections_GoogleMaps_MapTab"));
            BaseUI.verifyElementHasExpectedText("hotelPropertyDetails_MapDirections_GoogleMaps_MapTab", "Map");
            BaseUI.verifyElementAppears(
                    Locator.lookupRequiredElement("hotelPropertyDetails_MapDirections_GoogleMaps_Satellite_Tab"));
            BaseUI.verifyElementHasExpectedText("hotelPropertyDetails_MapDirections_GoogleMaps_Satellite_Tab",
                    "Satellite");
        } finally {
            BaseUI.switch_ToDefaultContent();
        }

        WebElement addressText = Locator.lookupRequiredElement("hotelPropertyDetails_MapDirections_EndAddress_TextBox");
        String addressTextSTR = addressText.getAttribute("value");
        if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            BaseUI.baseStringCompare("Address",
                    hotelData.get("address").replaceAll("\\r\\n|\\r|\\n", " "),
                    addressTextSTR.replace("US ", "").replace(",", ", ").replace(",  ", ", "));
        } else {
            BaseUI.baseStringCompare("Address", hotelData.get("address").replaceAll("\\r\\n|\\r|\\n", " "),
                    addressTextSTR.replace("US ", ""));
        }
    }

    public static HashMap<String, String> return_HotelInfo_ByHotelNameSelected() {
        HashMap<String, String> hotelInfo = new HashMap<String, String>();
        hotelInfo.put("hotelName", BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_HotelName")));
        hotelInfo.put("address", BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_Address"))
                .replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").replace("US ", "").trim());
        hotelInfo.put("youPay", BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_FinalTotal"))
                .replace("$", "").replace(",", ""));
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            hotelInfo.put("points", BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_PointsDeduction"))
                    .replace("-", "").replace(",", ""));
        }
        hotelInfo.put("hotelRate",
                BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelPropertyDetails_TotalAmount_NotReduced")));

        return hotelInfo;
    }

    public static String return_ImageURL() {

        String url = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("hotelPropertyDetails_HotelLogo"), "src");

        return url;
    }
}// end of class
