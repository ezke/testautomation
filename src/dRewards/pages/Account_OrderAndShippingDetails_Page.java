package dRewards.pages;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebElement;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.Parameter;
import utils.TableData;

public class Account_OrderAndShippingDetails_Page {

    // orderDate must be in format MM-dd-yyyy
    public static void verify_PaymentInformation(String orderNumber, String orderDate, Double orderSubtotal,
                                                 Double salesTax, Double shippingCost, Double orderTotal, String winningBid) {
        if (ClientDataRetrieval.isRedesignClient()) {


            String auctualorderNumber = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAuct_OrderAndShip_OrderNumber"));
            auctualorderNumber = orderNumber.split("\\-")[0];
            BaseUI.baseStringCompare("Order NUmber", auctualorderNumber, orderNumber);

            String auctualorderDate = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAuct_OrderAndShip_OrderDate"));
            auctualorderDate = auctualorderDate.split("\\:")[1];
            BaseUI.baseStringCompare("Order Date", auctualorderDate, orderDate);
            // BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderDate", orderDate);

        } else {
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderNumber", orderNumber);
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderDate", orderDate);
        }
        String expectedSubTotal;
        if (orderSubtotal.equals(0.0)) {
            expectedSubTotal = "$ 0";
            if (ClientDataRetrieval.isRedesignClient()) {
                expectedSubTotal = "$0.00";
            }
        } else {
            expectedSubTotal = "$ " + BaseUI.convertDouble_ToString_ForCurrency(orderSubtotal);//String.valueOf(Math.round(Float.parseFloat(
        }
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderSubtotal", expectedSubTotal);
        if (ClientDataRetrieval.clientName.startsWith(""))
            if (!ClientDataRetrieval.isRedesignClient()) {
                BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_SalesTax", format_Double_ForPage(salesTax));
                BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShippingCost", format_Double_ForPage(shippingCost));
                BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderTotal", format_Double_ForPage(orderTotal));

                if (winningBid != null) {
                    BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_WinningBid", "- " + winningBid);
                }
            }
    }
    public static void verify_Cancelled(PricingDetails pricingDetails, String salesTaxCost) {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("OrderAndShip_CancelledTitle"));

        String orderDate = getFormatted_OrderDate();
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedText("OrderAndShip_CancelDate", orderDate);
            BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalPointsRefunded",
                    "- " + pricingDetails.points);
        } else {
            BaseUI.verifyElementHasExpectedText("OrderAndShip_CancelDate",
                    orderDate);
            if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalPointsRefunded",
                        pricingDetails.points);
            }
        }

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            if (pricingDetails.allstate_Cash != null && !pricingDetails.allstate_Cash.equals("$0.00")) {
                Double orderTotalDoub = Double
                        .parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""));
                Double cashRewardsDoub = Double
                        .parseDouble(pricingDetails.allstate_Cash.replace("$", "").replace(",", ""));

                // Double creditCardAmount = orderTotalDoub - cashRewardsDoub;

                Double completeOrderTotal = orderTotalDoub + cashRewardsDoub;

                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded",
                        "$" + BaseUI.convertDouble_ToString_ForCurrency(completeOrderTotal));
                if (pricingDetails.allstate_Cash != null && pricingDetails.order_Total.equals("$0.00")) {
                    BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCreditCardRefunded", "$.00");
                } else {
                    BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCreditCardRefunded", pricingDetails.order_Total);
                }
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCashRewardsRefunded",
                        pricingDetails.allstate_Cash);
            } else {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", pricingDetails.order_Total);
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCreditCardRefunded", pricingDetails.order_Total);
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCashRewardsRefunded", "$.00");
            }
        } else {
            if (/*ClientDataRetrieval.client_Matches(client_Designation.AARP) || */
                    ClientDataRetrieval.client_Matches(client_Designation.Allstate) && pricingDetails.order_Total.equals("$0.00")) {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", "$.00");
            } else {
                if (pricingDetails.tax_Cost.equals("$0.00")) {
                    DecimalFormat df = new DecimalFormat("###.00");
                    String orderTotal = String.valueOf(df.format(Double.parseDouble(pricingDetails.order_Total.replace("$", ""))
                            + Double.parseDouble(salesTaxCost.replace("$", ""))));
                    BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", "$" + orderTotal);
                } else {
                    BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", pricingDetails.order_Total);
                }
            }
        }
    }

    public static void verify_PaymentInformation(@NotNull PricingDetails pricingDetails, @NotNull String orderNumber, @NotNull String orderDate,
                                                 @NotNull String creditCardNumber) {
        Parameter.named("pricingDetails").withValue(pricingDetails).mustBeNonNull();
        Parameter.named("orderNumber").withValue(orderNumber).mustBeNonNull();
        Parameter.named("orderDate").withValue(orderDate).mustBeNonNull();
        Parameter.named("creditCardNumber").withValue(creditCardNumber).mustBeNonNull();

        Double orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));

        Double salesTax = Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));

        Double shippingCost = 0.0;
        if (ClientDataRetrieval.isRedesignClient()) {
            if (!(pricingDetails.shipping_Cost.contains("FREE"))) {
                shippingCost = Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
            }
        } else {
            shippingCost = Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
        }

        Double orderTotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""));

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash) && pricingDetails.allstate_Cash != null
                && !pricingDetails.allstate_Cash.equals("$0.00")) {
            orderSubtotal += Double.parseDouble(
                    pricingDetails.allstate_Cash.replace("$", "").replace(",", "").replace("-", "").replace(" ", ""));

            String allstateCashString = pricingDetails.allstate_Cash;
            allstateCashString = allstateCashString.replace("$", "").replace(",", "").replace("-", "").replace(" ", "");
            allstateCashString = "- $" + allstateCashString;
            BaseUI.verifyElementHasExpectedText("OrderAndShip_AllstateCash", allstateCashString);
        }

        verify_PaymentInformation(pricingDetails, orderNumber, orderDate, orderSubtotal, salesTax, String.valueOf(shippingCost), orderTotal,
                pricingDetails.points, creditCardNumber);
    }

    public static void verify_PaymentInformation(@NotNull PricingDetails pricingDetails, @NotNull String orderNumber, @NotNull String orderDate, @NotNull Double orderSubtotal,
                                                 @NotNull Double salesTax, @NotNull String shippingCost, @NotNull Double orderTotal, String points, @NotNull String creditCardNumber) {
        // Parameter validation
        Parameter.named("pricingDetails").withValue(pricingDetails).mustBeNonNull();
        Parameter.named("orderNumber").withValue(orderNumber).mustBeNonNull();
        Parameter.named("orderDate").withValue(orderDate).mustBeNonNull();
        Parameter.named("orderSubtotal").withValue(orderSubtotal).mustBeNonNull();
        Parameter.named("salesTax").withValue(salesTax).mustBeNonNull();
        Parameter.named("shippingCost").withValue(shippingCost).mustBeNonNull();
        Parameter.named("orderTotal").withValue(orderTotal).mustBeNonNull();
        Parameter.named("creditCardNumber").withValue(creditCardNumber).mustBeNonNull();
        // Don't validate the "points" parameter; it usually comes from a field that isn't initialized in its
        // class's constructor, so it could well be null - and our code won't blow up on a null, anyway.

        String formattedCreditCardNumber;
        Double shipping_Cost = 0.0;
        if (ClientDataRetrieval.isRedesignClient()) {
            orderNumber = orderNumber.replaceAll("OrderId : ", "").split("\\-")[1];
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderNumber", "DRWS-" + orderNumber);

            if (!(pricingDetails.shipping_Cost.contains("FREE"))) {
                shipping_Cost = Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
            }
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderDate", "Order Date: " + orderDate);
        } else {
            shipping_Cost = Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
            orderNumber = orderNumber.split("\\-")[1];
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderNumber", orderNumber);
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderDate", orderDate);
        }

        String expectedSubTotal;
        if (orderSubtotal.equals(0.0)) {
            expectedSubTotal = "$.00";
            if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
                expectedSubTotal = "$ 0.00";
            }
            else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
                expectedSubTotal = "$0.00";
            }
            else if (ClientDataRetrieval.isRedesignClient()) {
                expectedSubTotal = "$0.00";
            }
        } else {
            expectedSubTotal = "$" + BaseUI.convertDouble_ToString_ForCurrency(orderSubtotal);
        }

        formattedCreditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4,
                creditCardNumber.length());
        if (ClientDataRetrieval.isRedesignClient()) {
            formattedCreditCardNumber = "MASTERCARD ending in " + formattedCreditCardNumber;

            //Commenting this validation because the order subtotal is different when we place order.
            //Since estimated tax is not same as final tax.
            //BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderSubtotal", expectedSubTotal);

            if (pricingDetails.shipping_Cost.contains("FREE")) {
                BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShippingCost", "FREE");
            } else {
                BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShippingCost", String.valueOf(shippingCost));
            }
        } else {
            formattedCreditCardNumber = "************" + formattedCreditCardNumber;
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderSubtotal", expectedSubTotal.replace("$", "$ "));

            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShippingCost", format_Double_ForPage(shipping_Cost));
        }

        String currency = "Points:";
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
            currency = "-(Member Savings)";
        }else{
            currency = ClientDataRetrieval.client_Currency + ":";
        }

        BaseUI.verifyElementHasExpectedText("OrderAndShip_PointsName", currency);

        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementHasExpectedText("OrderAndShip_Points", "- " + points);
        }
        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash) && pricingDetails.allstate_Cash != null
                && pricingDetails.order_Total.equals("$0.00")) {

        } else if (pricingDetails.order_Total.equals("$0.00")) {
        } else {
            BaseUI.verifyElementHasExpectedText("OrderAndShip_CreditCardNumber", formattedCreditCardNumber);
        }

        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderTotal", format_Double_ForPage(orderTotal));
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_SalesTax", format_Double_ForPage(salesTax));
    }

    // Formats to $.00 when Double is 0.0 or $12.34.
    private static String format_Double_ForPage(@NotNull Double doubleToFormat) {
        Parameter.named("doubleToFormat").withValue(doubleToFormat).mustBeNonNull();

        String formattedValue;
        if (doubleToFormat.equals(0.0)) {
            formattedValue = "$.00";
            if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                    || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                    || ClientDataRetrieval.isRedesignClient()
                    || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
                formattedValue = "$0.00";
            }
            //Formats to $.20 when double is less than 1 and 0 is absent
        } else if (doubleToFormat < 1 && !doubleToFormat.equals(0.0)) {
            if (ClientDataRetrieval.isRedesignClient()
                    || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)
                    || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
                formattedValue = "$" + doubleToFormat;
            } else {
                formattedValue = "$" + BaseUI.convertDouble_ToString_ForLessThanOne(doubleToFormat);
            }
        } else {
            formattedValue = "$" + BaseUI.convertDouble_ToString_ForCurrency(doubleToFormat);
        }

        return formattedValue;
    }

    public static void verify_Auction_FullRedemption_PaymentInformation(String orderNumber, String orderDate,
                                                                        String winningBid) {
        // if (!ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
        winningBid = winningBid.split("\\.")[0];
        //  }
        if (ClientDataRetrieval.isRedesignClient()) {
            verify_PaymentInformation(orderNumber, orderDate, 0.0, null, null, null, winningBid);
        } else {
            verify_PaymentInformation(orderNumber, orderDate, 0.0, 0.0, 0.0, 0.0, winningBid);
        }
    }
    public static void verify_Auction_PaymentInfo_DualCurrency(String orderNumber, String orderDate,
                                                               Double orderSubtotal, Double salesTax, Double orderTotal, String winningBid) {
        if (!ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
            winningBid = winningBid.split("\\.")[0];
        }

        verify_PaymentInformation(orderNumber, orderDate, orderSubtotal, salesTax, 0.0, orderTotal, winningBid);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_CreditCardUsed", "************5100");
    }

    public static void verify_ShippingAddress(String fullName, String address1, String city, String state,
                                              String country, String zip) {

        BaseUI.verifyElementHasExpectedPartialText("myAuct_OrderAndShip_ShipToName", fullName);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_Address1", address1);

        if (ClientDataRetrieval.isRedesignClient()) {
            String cityAuc = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAuct_OrderAndShip_ShipCityStateZip"));
            //cityAuc.replace("null *****","");
            String expectedCityDetails = city + ", "+ state + " *****";

            BaseUI.baseStringCompare("myAuct_OrderAndShip_ShipCityStateZip",
                    expectedCityDetails.replace(", null *****","") ,cityAuc);

    } else {
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShipCity", city);
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShipState", ", " + state);
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShipZipCode", "*****");
        }
        String countryFormatted = country.equals("United States") ? "US" : country;
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_ShipCountry", countryFormatted);
    }

    // public static void
    // verify_orderDetails_FullRedemption_NotShippedYet(String itemDescription,
    // Integer quantity,
    // Double unitCost) {
    // verify_ProductInfo(itemDescription, quantity, unitCost);
    // verify_OrderDetails_ShippingInfo_NotShipped();
    // }

    // This overload is for full redemption, where You Pay will = 0.
    public static void verify_ProductInfo(String itemDescription, Integer quantity, Double unitCost) {
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_productDescription", itemDescription);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_productQuantity", quantity.toString());

        String unitCostStr = "$" + BaseUI.convertDouble_ToString_ForCurrency(unitCost);
        String actualUnitCost = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAuct_OrderAndShip_unitCost"));
        actualUnitCost = actualUnitCost.replace(",", "").replace("Retail:","").replace(" ","");
        BaseUI.baseStringCompare("UnitCost", unitCostStr, actualUnitCost);
//		BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_unitCost", unitCostStr);
        String expectedYouPay = "$ 0";
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
        ||(ClientDataRetrieval.isRedesignClient())){
            expectedYouPay = "$0.00";
        }
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_YouPay", expectedYouPay);
    }

    public static void verify_ProductInfo_DualCurrency(String itemDescription, Integer quantity, Double unitCost,
                                                       String expectedYouPay) {

        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_productDescription", itemDescription);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_productQuantity", quantity.toString());

        String unitCostStr = "$" + BaseUI.convertDouble_ToString_ForCurrency(unitCost);
        String actualUnitCost = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAuct_OrderAndShip_unitCost"));
        actualUnitCost = actualUnitCost.replace(",", "");
        BaseUI.baseStringCompare("UnitCost", unitCostStr, actualUnitCost);

        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_YouPay",
                "$ " + expectedYouPay); //String.valueOf(Math.round(Float.parseFloat(expectedYouPay))))
    }

    public static void verify_ProductInfo_MultipleItems(TableData expectedTable) {
        TableData orderAndShippingData = return_ProductInfo();

        expectedTable.sort_ByColumn_Descending("pricing_YouPay");

        BaseUI.verify_TableColumns_Match("Product Name", expectedTable.data, orderAndShippingData.data);
        BaseUI.verify_TableColumns_Match("quantity", expectedTable.data, orderAndShippingData.data);
        if (orderAndShippingData.data.get(0).get("pricing_YouPay").equals("$.00")) {

        } else {
            BaseUI.verify_TableColumns_Match("pricing_YouPay", expectedTable.data,
                    orderAndShippingData.data);//, "pricing_Retail"
            BaseUI.verify_TableColumns_Match("pricing_YouPay", expectedTable.data, orderAndShippingData.data);
        }
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verify_TableColumn_AllColumnValues_MatchString("status", "Not Available",
                    orderAndShippingData.data);
        } else {
            BaseUI.verify_TableColumn_AllColumnValues_MatchString("status", "Pending", orderAndShippingData.data);
            BaseUI.verify_TableColumn_AllColumnValues_MatchString("shippingDate", "Not Available",
                    orderAndShippingData.data);
            BaseUI.verify_TableColumn_AllColumnValues_MatchString("tracking", "Tracking not available.",
                    orderAndShippingData.data);
        }

        BaseUI.verify_TableColumn_AllColumnValues_MatchString("shippingMethod", "Not Available",
                orderAndShippingData.data);
    }

    public static TableData return_ProductInfo() {
        TableData productInfo = new TableData();

        HashMap<String, String> dataMapping = new HashMap<String, String>();

        if (ClientDataRetrieval.isRedesignClient()) {
            dataMapping.put("Product Name", ".//section[@class='product-details']/h3");
            dataMapping.put("quantity", ".//section[@class='product-details']/ul[@class='order-details']/li[contains(text(),'Quantity')]/strong");
            dataMapping.put("pricing_Retail", ".//section[@class='product-details']/div[@class='price-matrix']/div[@class='retail-price']");
            dataMapping.put("pricing_YouPay", ".//section[@class='product-details']/div[@class='price-matrix']/div[@class='rewards-price']");
            dataMapping.put("pricing_Points", ".//section[@class='product-details']/div[@class='price-matrix']/div[@class='points-required']");
            //dataMapping.put("pricing_Savings", ".//section[@class='product-details']/div[@class='price-matrix']/div[@class='savings']");
            dataMapping.put("status", ".//div/section[@class='shipping-status']//h4[@class='shipped']");
            dataMapping.put("shippingMethod", ".//div/section[@class='shipping-method']//h4[@class='mobile-shipping-method']");
        } else {
            dataMapping.put("Product Name", "./td[@class='item']");
            dataMapping.put("quantity", "./td[@class='qty']");
            dataMapping.put("pricing_Retail", "./td[@class='unitCost']");
            dataMapping.put("pricing_YouPay", "./td[@class='youPay']");
            dataMapping.put("status", "./td[@class='status']");
            dataMapping.put("shippingDate", ".//td[@class='status']/following-sibling::td[1]");
            dataMapping.put("shippingMethod", ".//td[@class='status']/following-sibling::td[2]");
            dataMapping.put("tracking", "./td[@class='tracking']");
        }

        // /td[@class='youPay']
        // /td[@class='status']
        // ShippingDate//td[@class='status']/following-sibling::td[1]
        // shippinMethod//td[@class='status']/following-sibling::td[2]
        // /td[@class='tracking']

        // dataMapping.put("pricing_Retail", "./td[@class='unitCost']");
        // dataMapping.put("pricing_Points", ".//dd[@class='incentives']");
        // dataMapping.put("pricing_YouPay", ".//dd[@class='price']");
        // dataMapping.put("Percent Savings",
        // ".//div[@class='price-matrix']/span");

        productInfo.data = BaseUI.tableExtractor("OrderAndShip_OrderDetails_Rows", dataMapping);
        productInfo.remove_Character(" ", "pricing_YouPay");
        productInfo.sort_ByColumn_Descending("pricing_YouPay");
        productInfo.remove_Character(" ", "pricing_Retail");
        productInfo.replace_Characters_FromBeginning("Retail:", "", "pricing_Retail");
        productInfo.remove_Character(" ", "pricing_Points");
        productInfo.replace_Characters_FromBeginning("after", "", "pricing_Points");
        productInfo.replace_Character("Points", "", "pricing_Points");

        return productInfo;
    }

    public static void cancel_OrderLink() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            WebElement cancelOrderBtnElement = Locator.lookupRequiredElement("accnt_OrderDetails_CancelOrderButton");
            BaseUI.scroll_to_element(cancelOrderBtnElement);
            BaseUI.click(cancelOrderBtnElement);
            BaseUI.waitForElementToBeDisplayed("accnt_OrderDetails_CancelModal_YesCancelOrderButton", null, null);
        } else {
            BaseUI.click(Locator.lookupRequiredElement("accnt_OrderDetails_CancelOrderButton"));
        }

        Thread.sleep(1000);
    }

    public static void cancel_Order() throws Exception {
        cancel_OrderLink();
        BaseUI.click(Locator.lookupRequiredElement("accnt_OrderDetails_CancelModal_NoCancelOrderButton"));
        Thread.sleep(2000);
        cancel_OrderLink();
        BaseUI.click(Locator.lookupRequiredElement("accnt_OrderDetails_CancelModal_YesCancelOrderButton"));
        Thread.sleep(3000);
    }

    public static void verify_OrderDetails_ShippingInfo_NotShipped() {
        if (ClientDataRetrieval.isRedesignClient()){

            verify_OrderDetails_ShippingInfoRedesign("Not Available", "Not Available");
        }else{
        verify_OrderDetails_ShippingInfo("Pending", "Not Available", "Not Available", "Tracking not available.");
    }
    }
        public static void verify_OrderDetails_ShippingInfoRedesign(
                String shippingMethod, String trackingInfo) {
            //  BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_shippingStatus", shippingStatus);
            // BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_shippingDate", shippingDate);
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_shippingMethod", shippingMethod);
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_TrackingInfo", trackingInfo);
        }
    public static void verify_OrderDetails_ShippingInfo(String shippingStatus, String shippingDate,
                                                        String shippingMethod, String trackingInfo) {
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_shippingStatus", shippingStatus);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_shippingDate", shippingDate);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_shippingMethod", shippingMethod);
        BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_TrackingInfo", trackingInfo);
    }

    public static void verify_Cancelled_FreeProducts(PricingDetails pricingDetails) {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("OrderAndShip_CancelledTitle"));

        BaseUI.verifyElementHasExpectedText("OrderAndShip_CancelDate",
                getFormatted_OrderDate());

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalPointsRefunded", "- " + pricingDetails.points);
        } else {
            if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalPointsRefunded", pricingDetails.points);
            }
        }

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            if (pricingDetails.allstate_Cash != null && !pricingDetails.allstate_Cash.equals("$0.00")) {
                Double orderTotalDoub = Double
                        .parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""));
                Double cashRewardsDoub = Double
                        .parseDouble(pricingDetails.allstate_Cash.replace("$", "").replace(",", ""));

                // Double creditCardAmount = orderTotalDoub - cashRewardsDoub;

                Double completeOrderTotal = orderTotalDoub + cashRewardsDoub;

                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded",
                        "$" + BaseUI.convertDouble_ToString_ForCurrency(completeOrderTotal));
                if (pricingDetails.allstate_Cash != null && pricingDetails.order_Total.equals("$0.00")) {
                    BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCreditCardRefunded", "$.00");
                } else {
                    BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCreditCardRefunded", pricingDetails.order_Total);
                }
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCashRewardsRefunded",
                        pricingDetails.allstate_Cash);
            } else {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", "$.00");
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCreditCardRefunded", "$.00");
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalCashRewardsRefunded", "$.00");
            }
        } else {
            if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
                    && pricingDetails.order_Total.equals("$0.00")
                    || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                    && pricingDetails.order_Total.equals("$0.00")) {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", "$0.00");
            } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                    || pricingDetails.order_Total.equals("$0.00")
                    || ClientDataRetrieval.client_Matches(client_Designation.Allstate)
                    && pricingDetails.order_Total.equals("$0.00")) {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", "$.00");
            } else {
                BaseUI.verifyElementHasExpectedText("OrderAndShip_TotalAmountRefunded", pricingDetails.order_Total);
            }
        }
    }

    public static String getFormatted_OrderDate() {
        String orderDate;
        if (ClientDataRetrieval.isRedesignClient()) {
            String todayDate = BaseUI.getTodaysDate_AsString();
            if (todayDate.substring(3).startsWith("0")) {
                orderDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM d, yyyy");
            } else {
                orderDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM dd, yyyy");
            }
        } else {
            orderDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM-dd-yyyy");
        }

        return orderDate;
    }

    public static void verifyPaymentInfo_OnOrderAndShippingPage(PricingDetails pricingDetails, TableData shoppingCartData,
                                                                String orderID, String ccNumber) throws Exception {
        String orderDate = Account_OrderAndShippingDetails_Page.getFormatted_OrderDate();
        Double orderSubtotal;
        TableData reviewOrder_ProductData;
        Boolean giftCardAddedToCart;
        if (ClientDataRetrieval.isRedesignClient()) {
            reviewOrder_ProductData = CheckoutPage_ReviewOrder.get_Table_Data();
            giftCardAddedToCart = reviewOrder_ProductData.data.get(0).get("Product Name").contains("Gift Card");
        } else {
            giftCardAddedToCart = shoppingCartData.data.get(0).get("Product Name").contains("Gift Card");
        }

        if (giftCardAddedToCart) {
            if (ClientDataRetrieval.isRedesignClient()) {
                if (pricingDetails.shipping_Cost.equals("FREE")) {
                    orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                            - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));
                } else {
                    orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                            - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
                            - Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
                }
            } else if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
                orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                        - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
                        - Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""))
                        + Double.parseDouble(pricingDetails.allstate_Cash.replace("$", ""));
            } else {
                orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                        - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
                        - Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
            }
        } else {
            if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
                orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                        - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
                        + Double.parseDouble(pricingDetails.allstate_Cash.replace("$", ""));
            } else {
                orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                        - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));
            }
        }
        Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID, orderDate,
                orderSubtotal, Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", "")),
                pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", "")),
                pricingDetails.points, ccNumber);
    }
}// end of class