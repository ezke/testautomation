package dRewards.pages;

import utils.Locator;
import org.openqa.selenium.WebElement;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.TableData;

import java.time.Duration;

public class CheckoutPage_PaymentInfo {

	public static void apply_GiftCard_NameExpectedFormatting(TableData expectedCartData){
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards) && ClientDataRetrieval.environment.equals("XUAT")){
			expectedCartData.forEach_Row(a->{
				String productName = a.get("Product Name");
				if(productName.startsWith("Gift Card")){
					a.put("Product Name", productName.substring(9, productName.length()).trim());
				}
			});
		}

	}


	public static void verify_FieldErrorDisplayed(String fieldText, String expectedErrorText) throws Exception {
		WebElement errorElement = Locator.lookupRequiredElement("checkout_ErrorMessage_ByFieldText", fieldText, null);
		BaseUI.scroll_to_element(errorElement);
		BaseUI.verifyElementAppears(errorElement);
		String errorText = errorElement.getText();
		BaseUI.baseStringCompare("Error Text", expectedErrorText, errorText);
	}

	public static void verify_FieldError_forDropdown_Displayed(String fieldText, String expectedErrorText) {
		WebElement errorElement = Locator.lookupRequiredElement("checkout_ErrorMessage_ForDropdown_ByText", fieldText, null);
		BaseUI.verifyElementAppears(errorElement);
		String errorText = errorElement.getText();
		BaseUI.baseStringCompare("Error Text", expectedErrorText, errorText);
	}

	public static void verify_FieldError_ExistingPayment_CVVFieldError() {
		WebElement errorElement = Locator.lookupRequiredElement("checkout_ErrorMessage_ExistingCard_CVV");
		BaseUI.verifyElementAppears(errorElement);
		String errorText = errorElement.getText();
		BaseUI.baseStringCompare("Error Text", "Please enter your credit card security number", errorText);
	}

	public static void click_AddNewPayment_Radio() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			if (BaseUI.elementExists("checkout_BillingAddress_ChangeBtn", null, null)) {
				click_BillingAddress_ChangeBtn();

				//Clear all the existing details to check the error text for each field
				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardNumber"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_CVV"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_CardholderName"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_Address1"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_City"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_newPayment_Zip"));
			}
		} else {
			if (BaseUI.elementExists("checkout_AddNewPayment_Radio", null, null)) {
				BaseUI.click(Locator.lookupRequiredElement("checkout_AddNewPayment_Radio"));
				Thread.sleep(300);
			}
		}
	}

	public static void click_ExistingPayment_Radio() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_ExistingPayment_Radio"));
		Thread.sleep(300);
	}

	public static void enter_CVV_Code(String CVVCode) {
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_Existing_CVV_TextBox"), CVVCode);
	}

	public static void click_ReviewPayment_Button() throws Exception {
		WebElement checkout_ReviewOrder_button = Locator.lookupRequiredElement("checkout_ReviewOrder_button");
		BaseUI.scroll_to_element(checkout_ReviewOrder_button);
		BaseUI.click(checkout_ReviewOrder_button);
		if(ClientDataRetrieval.isRedesignClient()){
			BaseUI.wait_ForCondition_ToBeMet(()->{
				if(BaseUI.element_isEnabled(Locator.lookupRequiredElement("checkout_PlaceYourOrder_Button"))){
					return true;
				} else if(BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_ReviewOrderButton_ElementToWaitFor"))) {
					return true;
				}
				return false;

			}, Duration.ofSeconds(10),Duration.ofSeconds(1));
		} else {
			BaseUI.waitForElementToBeDisplayed("checkout_ReviewOrderButton_ElementToWaitFor", null, null);
		}
		Thread.sleep(2000);
	}

	public static void check_SaveBillingAddressForLater() throws Exception {
		BaseUI.checkCheckbox("checkout_newPayment_SaveForLater_Checkbox");
		Thread.sleep(200);
	}

	// selects existing payment option and enters 1234 as the CVV and then
	// navigates to Review Payment page.
	public static void navigate_toReviewPayment() throws Exception {
		// Check to see if there is an existing payment method
		if (BaseUI.elementExists("checkout_ExistingPaymentRows", null, null)) {
			// If there's an existing payment method and an Existing Payment
			// radio,
			// we click the radio button and enter a CVV code.
			if (ClientDataRetrieval.isRedesignClient()) {
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_Existing_CVV_TextBox"), "211");
				BaseUI.verifyElementEnabled(Locator.lookupRequiredElement("checkout_PlaceYourOrder_Button"));
			} else {
				if (BaseUI.elementExists("checkout_ExistingPayment_Radio", null, null)) {
					click_ExistingPayment_Radio();
					click_ReviewPayment_Button();
				}
			}
		} else {
			// If there isn't an existing payment method we'll add a new one.
			add_newPaymentOption_UsingDefaults();
			click_ReviewPayment_Button();
		}
	}

	public static void add_newPaymentOption_UsingDefaults() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			add_NewPaymentOption("Mastercard", "5105105105105100", "12", "22", "211", "qa automation", "1225 broken sound",
					"United States", "Boca Raton", "Florida", "33481");
			clickUseMyShippingAddressCheckbox();
			clickSavePaymentMethod_FutureUseCheckbox();
		} else {
			add_NewPaymentOption("MasterCard", "5105105105105100", "12", "22", "211", "qa automation", "1225 broken sound",
					"United States", "Boca Raton", "FL", "33481");
		}
	}

	public static void add_NewPaymentOption_Redesign(String creditCardType, String creditCardNumber, String expirationMonth,
			String expirationYear, String cvvCode, String cardholderName) throws Exception {

//		if (BaseUI.elementExists("checkout_AddNewPayment_Radio", null, null)) {
//			click_AddNewPayment_Radio();
//		}
		enter_requiredNewPaymentFields_Redesign(creditCardType, creditCardNumber, expirationMonth, expirationYear, cvvCode,
				cardholderName);
	}


	public static void enter_requiredNewPaymentFields_Redesign(String creditCardType, String creditCardNumber,
			String expirationMonth, String expirationYear, String cvvCode, String cardholderName) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_CreditCardType"), creditCardType);
		Thread.sleep(200);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardNumber"), creditCardNumber);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth"), expirationMonth);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear"), expirationYear);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CVV"), cvvCode);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CardholderName"), cardholderName);
	}

	public static void add_NewPaymentOption(String creditCardType, String creditCardNumber, String expirationMonth,
			String expirationYear, String cvvCode, String cardholderName, String address1, String country, String city,
			String state, String zip) throws Exception {

		if (ClientDataRetrieval.isLegacyClient()) {
			if (BaseUI.elementExists("checkout_AddNewPayment_Radio", null, null)) {
				click_AddNewPayment_Radio();
			}
		}
		enter_requiredNewPaymentFields(creditCardType, creditCardNumber, expirationMonth, expirationYear, cvvCode,
				cardholderName, address1, country, city, state, zip);
	}


	//When shopping cart has Gift cards. Address fields are non-editable.
	//So created separate method for giftcards.
	public static void add_NewPaymentOption_ForGiftCards(String creditCardType, String creditCardNumber, String expirationMonth,
			String expirationYear, String cvvCode, String cardholderName, String address1, String country, String city,
			String State, String zip) throws Exception {

		if (ClientDataRetrieval.isLegacyClient()) {
			if (BaseUI.elementExists("checkout_AddNewPayment_Radio", null, null)) {
				click_AddNewPayment_Radio();
			}
		}
		enter_requiredNewPaymentFields_ForGiftCards(creditCardType, creditCardNumber, expirationMonth, expirationYear, cvvCode,
				cardholderName, address1, country, city, State, zip);
	}


	//When shopping cart has Gift cards. Address fields are non-editable.
	//So created separate method for giftcards.
	public static void enter_requiredNewPaymentFields_ForGiftCards(String creditCardType, String creditCardNumber,
			String expirationMonth, String expirationYear, String cvvCode, String cardholderName, String address1,
			String country, String city, String State, String zip) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_CreditCardType"), creditCardType);
		Thread.sleep(200);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardNumber"), creditCardNumber);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth"), expirationMonth);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear"), expirationYear);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CVV"), cvvCode);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CardholderName"), cardholderName);

		if (! ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_Address1"), address1);
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_Country"), country);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_City"), city);
			// make sure to pass in 2 letter state code (example: WI)
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_State"), State);
			Thread.sleep(100);
			if (ClientDataRetrieval.isLegacyClient()) {
				BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_Zip_label"));
				Thread.sleep(100);
			}
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_Zip"), zip);
		}
	}

	public static void verify_AllstateCash(String allstate_Cash, String newTotal) {
		BaseUI.verifyElementHasExpectedText("checkout_AllstateCashRedeemed", allstate_Cash + " Cash Rewards");
		BaseUI.verifyElementHasExpectedText("checkout_NewTotal", newTotal);
	}


	//When shopping cart has merchandise product other than Gift cards. Address fields are editable.
	//So created separate method for other merchandise product.
	public static void enter_requiredNewPaymentFields(String creditCardType, String creditCardNumber,
			String expirationMonth, String expirationYear, String cvvCode, String cardholderName, String address1,
			String country, String city, String State, String zip) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_CreditCardType"), creditCardType);
		Thread.sleep(200);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardNumber"), creditCardNumber);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth"), expirationMonth);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear"), expirationYear);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CVV"), cvvCode);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CardholderName"), cardholderName);

	    BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_Address1"), address1);
	    BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_Country"), country);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_City"), city);
		// make sure to pass in 2 letter state code (example: WI)
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_State"), State);
		Thread.sleep(100);

		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_Zip_label"));
			Thread.sleep(100);
		}
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_Zip"), zip);
	}


	public static void clickUseMyShippingAddressCheckbox() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_UseMyShippingAdd_Checkbox"));
		Thread.sleep(200);
	}


    public static void clickSavePaymentMethod_FutureUseCheckbox() throws Exception {
    	BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_SaveForLater_Checkbox"));
		Thread.sleep(200);
	}

    public static void click_BillingAddress_ChangeBtn() throws Exception{
    	BaseUI.click(Locator.lookupRequiredElement("checkout_BillingAddress_ChangeBtn"));
    	BaseUI.wait_forPageToFinishLoading();
    	Thread.sleep(300);
    }

    public static void enterCvvCode_IfCvvTextboxIsBlank(String cvvCode) {
    	if(BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_Existing_CVV_TextBox"))){
			CheckoutPage_PaymentInfo.enter_CVV_Code(cvvCode );
		}
    }
}
