package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.LoginPage_Admin;
import org.openqa.selenium.WebElement;
import utils.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

public class Sweepstakes {

    static String beginDate_CurrentSweepstakesPage;
    static String endDate_CurrentSweepstakesPage;
    static String drawingDate_CurrentSweepstakesPage;
    static String beginDate_SweepstakesDetailsPage, endDate_SweepstakesDetailsPage, drawingDate_SweepstakesDetailsPage;
    static String sweepstakesNameOnHomePage;
    static String sweepstakesRetailValueOnHomePage;
    static String sweepstakesName_SweepstakesPage;
    static String sweepstakesRetailValue_SweepstakesPage;
    public static String mySweepstakesName;

    static String[] sweepstakesType_Dropdown = {"Current  Sweepstakes", "Upcoming  Sweepstakes", "All Sweepstakes"};

    public static void clickUpcomingSweepstakesHomePage() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("upcomingSweepstakes_Link"));
        Sweepstakes_Upcoming.waitForPageToLoad();
    }

    public static void clickSweepstakesSeeDetailsButtonOnHomePage() throws Exception {

        if (ClientDataRetrieval.isLegacyClient()) {
            BaseUI.click(Locator.lookupRequiredElement("sweepstakes_seeDetails_button"));
            BaseUI.waitForElementToNOTBeDisplayed("sweepstakes_seeDetails_button", null, null, 10);
        } else {
            BaseUI.click(Locator.lookupRequiredElement("homePage_Sweepstakes_ImageLink"));
            BaseUI.waitForElementToNOTBeDisplayed("homePage_Sweepstakes_ImageLink", null, null, 10);
        }

        BaseUI.wait_forPageToFinishLoading();
    }

    public static void clickSweepstakesSeeDetailsButton_CurrentSweepstakesPage() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_seeDetails_button"));
        BaseUI.waitForElementToNOTBeDisplayed("currentSweepstakes_seeDetails_button", null, null, 10);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void verifySweepstakes_BeginEnd_DrawingDates() throws Exception {
        if (ClientDataRetrieval.isLegacyClient()) {
            beginDate_CurrentSweepstakesPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_BeginDate_Value_CurrentSweepstakesPage"));
            drawingDate_CurrentSweepstakesPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_DrawingDate_Value_CurrentSweepstakesPage"));
            endDate_CurrentSweepstakesPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_EndDate_Value_CurrentSweepstakesPage"));
        } else {
            endDate_CurrentSweepstakesPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("homePage_Sweepstakes_EndsOnDate_Text"));
        }

        clickSweepstakesSeeDetailsButton_CurrentSweepstakesPage();
        if (ClientDataRetrieval.isLegacyClient()) {
            beginDate_SweepstakesDetailsPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("BeginDate_TextValue"));
        }
        endDate_SweepstakesDetailsPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("EndDate_TextValue"));
        drawingDate_SweepstakesDetailsPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("DrawingDate_TextValue"));
        if (ClientDataRetrieval.isLegacyClient()) {
            BaseUI.baseStringCompare("BeginDate_TextValue", beginDate_CurrentSweepstakesPage,
                    beginDate_SweepstakesDetailsPage);
            BaseUI.baseStringCompare("DrawingDate_TextValue", drawingDate_CurrentSweepstakesPage,
                    drawingDate_SweepstakesDetailsPage);
        }
        BaseUI.baseStringCompare("EndDate_TextValue", endDate_CurrentSweepstakesPage, endDate_SweepstakesDetailsPage);
    }

    public static void verifySweepstakesDetails() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            Navigation.navigate_SweepstakesLink_currentSweepstakes();
            sweepstakesNameOnHomePage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesNameOnCurrentPage"));
            clickSweepstakesSeeDetailsButton_CurrentSweepstakesPage();

            sweepstakesName_SweepstakesPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesName"))
                    .replaceAll(" Sweepstakes", "");
            verifySweepstakes_NameAndPageTitle();
        } else {
            sweepstakesNameOnHomePage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesNameDisplayed_HomePage"));
            sweepstakesRetailValueOnHomePage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_Sweepstakes_ValuedAt_Value"));

            clickSweepstakesSeeDetailsButtonOnHomePage();

            sweepstakesName_SweepstakesPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesName"))
                    .replaceAll(" Sweepstakes", "");
            if (ClientDataRetrieval.isRedesignClient()) {
                sweepstakesRetailValue_SweepstakesPage = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_RetailValue"));
            } else {
                sweepstakesRetailValue_SweepstakesPage = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesValue")).replaceAll("Retail Price:", "");
            }
            verifySweepstakes_NameAndPageTitle();
        }
    }

    public static TableData return_automation_SweepstakesTable() {
        TableData sweepstakesTable;

        String[] headers = {"Quantity", "Earn Date", "Earn Method"};
        if (Browser.currentBrowser.equals("internetexplorer")) {
            sweepstakesTable = BaseUI.tableExtractor_ByCell("mySweepstakes_CurrentSweepstakesSub_Tab_TableCells", headers);
        } else {
            sweepstakesTable = BaseUI.tableExtractorV2("mySweepstakes_CurrentSweepstakesSub_Tab_TableBody", headers);
        }

        return sweepstakesTable;
    }

    public static TableData return_automation_SweepstakesTable2nd() {
        TableData sweepstakesTable = new TableData();

        HashMap<String, String> tableRows = new HashMap<String, String>();
        tableRows.put("Quantity", "./td[1]");
        tableRows.put("Earn Date", "./td[2]");
        tableRows.put("Earn Method", "./td[3]");

        sweepstakesTable.data = BaseUI.tableExtractor("mySweepstakes_CurrentSweepstakesSub_Tab_Table2ndRow", tableRows);

        return sweepstakesTable;
    }

    public static void verifyMysweepstakes_Tickets() throws Exception {

        expand_autoTestRow();
        String todayDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
        TableData automationSweeps = Sweepstakes.return_automation_SweepstakesTable();
        HashMap<String, String> expectedRow = new HashMap<>();
        expectedRow.put("Quantity", "5");
        expectedRow.put("Earn Date", todayDate);

        try {
            automationSweeps.verify_MatchingRow_Found(expectedRow);
        } catch (AssertionError e){
            automationSweeps.logTable();
            BaseUI.log_AndFail("Did Not find row " + expectedRow.toString());
        }
    }

    public static void expand_autoTestRow() throws Exception {

        Boolean containerOpen = BaseUI.elementHasExpectedAttribute(
                Locator.lookupRequiredElement("sweepstakes_Autotest_Container"), "class", "contest openContainer");

        if (!containerOpen) {
            BaseUI.click(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_Name"));
            BaseUI.waitForElementToBeDisplayed("mySweepstakes_CurrentSweepstakesSub_Tab_Table", null, null);
        }
        Thread.sleep(500);
    }

    public static ArrayList<String> return_MySweepstakes_NameList() throws Exception {

        ArrayList<WebElement> mySweepstakesList = Locator.lookup_multipleElements("mySweepstakes_Name_List",
                null, null);

        ArrayList<String> titleList = new ArrayList<String>();

        for (WebElement mySweepstakes : mySweepstakesList) {
            String mySweepstakesStr = BaseUI.get_Attribute_FromField(mySweepstakes, "innerText").split("\\(")[0].trim();
            titleList.add(mySweepstakesStr);
        }

        return titleList;
    }

    public static void expand_MySweepstakes_Row_ByName(String sweepstakes) throws Exception {

        ArrayList<String> mySweepstakesList = return_MySweepstakes_NameList();

        BaseUI.verify_true_AndLog(mySweepstakesList.size() > 0, "My Sweepstakes results Found",
                "My Sweepstakes results Not Found");
        for (Integer i = 0; i < mySweepstakesList.size(); i++) {
            if (sweepstakes.equals(mySweepstakesList.get(i))) {
                BaseUI.click(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ByName", mySweepstakesList.get(i), null));
                BaseUI.waitForElementToBeDisplayed("mySweepstakes_CurrentSweepstakesSub_TabTable_ByName", mySweepstakesList.get(i), null);
                Thread.sleep(500);
                break;
            }
        }
    }

    public static void verifyMySweepstakes_Tickets_ByName(String sweepstakes, String tickets, String beginDate) throws Exception {
        if(ClientDataRetrieval.isRedesignClient()){
            Navigation.navigate_Logout();
            Browser.closeBrowser();
            Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        } else {
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
        }
        LoginPage_Admin.login_Admin();
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Sweepstakes_MySweepstakes();

        Sweepstakes.verifyMySweepstakes_Header();
        Sweepstakes.verify_mySweepstakes_mySweepstakes_CurrentSweepstakesTab();
        expand_MySweepstakes_Row_ByName(sweepstakes);

        String todayDate;
        if (ClientDataRetrieval.isRedesignClient()) {
            todayDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM d, yyyy").toUpperCase();
            todayDate.toUpperCase();
        } else {
            todayDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
        }

        TableData mySweeps = return_automation_SweepstakesTable();

        mySweeps.verify_Value_InColumn("Quantity", tickets);
        mySweeps.verify_Value_InColumn("Earn Date", todayDate);
        mySweeps.verify_Value_InColumn("Earn Method", "Ticket Purchase");

        mySweepstakesName = BaseUI.getTextFromField(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ByName", sweepstakes, null)).replaceAll("[\r\n]+", " ");

        Sweepstakes.click_MySweeptakes_ViewSweeptakesButton_ByName(sweepstakes);

        if (ClientDataRetrieval.isLegacyClient()) {
            String beginDate_SweepstakesDetailsPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("BeginDate_TextValue"));
            BaseUI.baseStringCompare("Begin Date", beginDate, beginDate_SweepstakesDetailsPage);
        }

        sweepstakesName_SweepstakesPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesName"))
                .replaceAll(" Sweepstakes", "");
        BaseUI.baseStringCompare("Sweepstake Name", sweepstakes, sweepstakesName_SweepstakesPage);
    }

    public static void verifyMySweepstakes_Tickets_ByName2nd(String sweepstakes, String tickets) throws Exception {

        Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
        LoginPage_Admin.login_Admin();
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

        if(ClientDataRetrieval.isRedesignClient()) {
            Browser.closeBrowser();
            Browser.openBrowser(ClientDataRetrieval.url);
        } else {
            Browser.navigateTo(ClientDataRetrieval.url);
        }

        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Sweepstakes_MySweepstakes();

        Sweepstakes.verifyMySweepstakes_Header();
        Sweepstakes.verify_mySweepstakes_mySweepstakes_CurrentSweepstakesTab();
        expand_MySweepstakes_Row_ByName(sweepstakes);

        String todayDate;

        todayDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM d, yyyy").toUpperCase();
        todayDate.toUpperCase();

        TableData mySweeps = return_automation_SweepstakesTable2nd();

        mySweeps.verify_Value_InColumn("Quantity", tickets);
        mySweeps.verify_Value_InColumn("Earn Date", todayDate);
        mySweeps.verify_Value_InColumn("Earn Method", "Ticket Purchase");

        mySweepstakesName = BaseUI.getTextFromField(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ByName", sweepstakes, null)).replaceAll("[\r\n]+", " ");

        Sweepstakes.click_MySweeptakes_ViewSweeptakesButton_ByName(sweepstakes);

        BaseUI.waitForElementToBeDisplayed("verify_SweepstakesName", null, null, 20);
        sweepstakesName_SweepstakesPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesName"))
                .replaceAll(" Sweepstakes", "");
        BaseUI.baseStringCompare("SweepstakeName", sweepstakes, sweepstakesName_SweepstakesPage);
    }

    public static void click_MySweeptakes_ViewSweeptakesButton_ByName(String sweepstakes) throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton_ByName", sweepstakes, null));
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.waitForElementToBeDisplayed("currentSweepstakes_WaysToEnterLabel", null, null);
        } else {
            BaseUI.waitForElementToBeDisplayed("currentSweepstakes_Product_Title", null, null);
        }

        Thread.sleep(500);
    }

    public static void verifyMySweepstakes_Header() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("mySweepstakes_Header"));
        } else {

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_Header"));
        }
    }

    public static void verify_mySweepstakes_mySweepstakes_CurrentSweepstakesTab() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakesTab"));
            BaseUI.verifyElementHasExpectedText("mySweepstakes_CurrentSweepstakesTab", "Live Sweepstakes");
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakesTab"));
            BaseUI.verifyElementHasExpectedText("mySweepstakes_CurrentSweepstakesTab", "Current Sweepstakes");
        }
    }

//	public static void verifyMySweepstakes_PastSweepstakes_Tickets_ByName(String sweepstakes, String tickets,
//			String todayDate_ForColumn) throws Exception {
//
//		//Need to Navigate to admin site first to do memcashe flush
//		Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
//		LoginPage_Admin.login_Admin();
//		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
//
//		//Navigate to application and verify ticket redeemed appears
//		Browser.navigateTo(ClientDataRetrieval.url);
//		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
//		Navigation.navigate_Sweepstakes_MySweepstakes();
//
//		Sweepstakes.verifyMySweepstakes_Header();
//		Sweepstakes.verify_mySweepstakes_mySweepstakes_CurrentSweepstakesTab();
//		expand_MySweepstakes_Row_ByName(sweepstakes);
//		String todayDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
//		TableData mySweeps = return_automation_SweepstakesTable();
//
//		mySweeps.verify_Value_InColumn("Quantity", tickets);
//		mySweeps.verify_Value_InColumn("Earn Date", todayDate_ForColumn);
//		mySweeps.verify_Value_InColumn("Earn Method", "Ticket Purchase");
//
//		mySweepstakesName = BaseUI.getTextFromField(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ByName", sweepstakes, null))
//				.replaceAll("[\r\n]+", " ");
//
//		Sweepstakes.click_MySweeptakes_ViewSweeptakesButton_ByName(sweepstakes);
//
//		String endedDate = BaseUI.getTextFromField(Locator.lookupRequiredElement("pastSweepstakes_EndDate_Text"))
//				.replaceAll("[\r\n]+", " ").replaceAll("Ended: ", "");
//
//		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("pastSweepstakes_SweepstakesInfo_Text"));
//		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("pastSweepstakes_Winners_Text"));
//		BaseUI.verifyElementHasExpectedText("pastSweepstakes_SweepstakesInfo_Text", "Sweepstakes Info");
//		BaseUI.verifyElementHasExpectedText("pastSweepstakes_Winners_Text", "Winners");
//		BaseUI.baseStringCompare("Ended Date", todayDate, endedDate);
//	}

    public static void verifySweepstakes_NameAndPageTitle() throws Exception {
        if (ClientDataRetrieval.isLegacyClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("verify_SweepstakesPageTitle"));
            BaseUI.verifyElementHasExpectedText("verify_SweepstakesPageTitle", "Sweepstakes");
            BaseUI.verifyElementHasExpectedText("verify_SweepstakesInfo_Header", "Sweepstakes Info");
        }

        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            BaseUI.baseStringCompare("verify_SweepstakesName", sweepstakesNameOnHomePage,
                    sweepstakesName_SweepstakesPage);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {

            BaseUI.baseStringCompare("verify_SweepstakesValue", sweepstakesRetailValueOnHomePage,
                    sweepstakesRetailValue_SweepstakesPage);
        } else {
            BaseUI.baseStringCompare("verify_SweepstakesName", sweepstakesNameOnHomePage,
                    sweepstakesName_SweepstakesPage);
            BaseUI.baseStringCompare("verify_SweepstakesValue", sweepstakesRetailValueOnHomePage,
                    sweepstakesRetailValue_SweepstakesPage);
        }
    }

    public static void verifySweepstakesSku_RetailsPriceOver600() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            Navigation.navigate_SweepstakesLink_currentSweepstakes();
            clickSweepstakesSeeDetailsButton_CurrentSweepstakesPage();

            sweepstakesRetailValue_SweepstakesPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesValue")).replaceAll("Retail Price:", "")
                    .replace("$", "").replaceAll("Valued At: ", "");
            verifySkuOrRetailsPrice(sweepstakesRetailValue_SweepstakesPage);
        } else {

            sweepstakesRetailValueOnHomePage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_Sweepstakes_ValuedAt_Value"))
                    .replaceAll("Retail Price:", "").replace("$", "").replaceAll("Valued At: ", "");
            verifySkuOrRetailsPrice(sweepstakesRetailValueOnHomePage);
        }
    }

    public static void verifySkuOrRetailsPrice(String retailPrice) {
        // String sweepstakesRetailValue_SweepstakesPage1 =
        // BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesValue")).replaceAll("Retail
        // Price:", "").replace("$", "");
        try {
            // Integer result = Integer.valueOf(retailPrice);
            Double result = Double.parseDouble(retailPrice);
            System.out.println(result);
            BaseUI.verify_true_AndLog(result >= 600, "Retails Value or SKU is greater than 600" + result,
                    "Retails Value or SKU IS NOT greater than 600" + result);
            // if (result >= 600) {
            // System.out.println("Retails Value or SKU is greater than 600");
            // } else {
            // System.out.println("Retails Value or SKU is less than 600");
            // }
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }

    public static void click_ListView_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_ListView"));
        Thread.sleep(300);
    }

    public static TableData return_UpcomingSweepstakesTable() {
        TableData sweepstakesTable = new TableData();

        HashMap<String, String> tableRows = new HashMap<String, String>();
        tableRows.put("Sweepstakes Name", ".//div[@class='description']/h1/a");
        tableRows.put("Begin Date", ".//dd[@class='begdate']");
        tableRows.put("End Date", ".//dd[@class='enddate']");
        tableRows.put("Drawing Date", ".//dd[@class='drawdate']");

        sweepstakesTable.data = BaseUI.tableExtractor("currentSweepstakes_RowList", tableRows);

        return sweepstakesTable;
    }

    public static void click_UpcomingSweepstakes_SeeDetails_Button_ByName(String sweepstakesName) throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("upcomingSweepstakes_SeeDetails_Button_BySweepstakesName",
                sweepstakesName, null));

        BaseUI.waitForElementToBeDisplayed("verify_SweepstakesName", null, null, 10);
    }

    public static void verify_UpcomingSweepstakes_ByName(String sweepstakesName) throws Exception {

        TableData sweepstakesTable = return_UpcomingSweepstakesTable();

        BaseUI.verify_true_AndLog(sweepstakesTable.data.size() > 0, "Search Results found.",
                "Search Results not found.");

        for (Integer i = 0; i < sweepstakesTable.data.size(); i++) {
            if (sweepstakesName.equals(sweepstakesTable.data.get(i).get("Sweepstakes Name"))) {
                click_UpcomingSweepstakes_SeeDetails_Button_ByName(sweepstakesTable.data.get(i).get("Sweepstakes Name"));

                sweepstakesName_SweepstakesPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesName"))
                        .replaceAll(" Sweepstakes", "");
                beginDate_SweepstakesDetailsPage = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("BeginDate_TextValue"));
                drawingDate_SweepstakesDetailsPage = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("DrawingDate_TextValue"));

                BaseUI.baseStringCompare("SweepstakeName", sweepstakesName, sweepstakesName_SweepstakesPage);
                BaseUI.baseStringCompare("BeginDate_TextValue", sweepstakesTable.data.get(i).get("Begin Date"),
                        beginDate_SweepstakesDetailsPage);
                BaseUI.baseStringCompare("DrawingDate_TextValue", sweepstakesTable.data.get(i).get("Drawing Date"),
                        drawingDate_SweepstakesDetailsPage);
                break;
            }
        }
    }

    public static TableData return_PastSweepstakesTable() throws Exception {

        TableData sweepstakesTable = new TableData();

        HashMap<String, String> tableRows = new HashMap<String, String>();
        tableRows.put("Sweepstakes Name", ".//div[@class='description']/h1/a");
        tableRows.put("End Date", ".//*[@class='description']/h4");
        tableRows.put("Winner", ".//*[@class='pricing']//span");

        sweepstakesTable.data = BaseUI.tableExtractor("currentSweepstakes_RowList", tableRows);

        return sweepstakesTable;
    }

    public static void click_PastSweepstakes_ListView_Button() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("pastSweepstakes_SearchResult_ListView_Button"));
        Thread.sleep(300);
    }

    public static void click_PastSweepstakes_SeeDetails_Button_ByName(String sweepstakesName) throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("upcomingSweepstakes_SeeDetails_Button_BySweepstakesName",
                sweepstakesName, null));
        BaseUI.waitForElementToBeDisplayed("pastSweepstakes_EndDate_Text", null, null, 10);
    }

    //Need to navigate to Passt Sweepstakes page
    public static void verify_PastSweepstakes_ByName(String sweepstakesName) throws Exception {

        TableData sweepstakesTable = return_PastSweepstakesTable();

        BaseUI.verify_true_AndLog(sweepstakesTable.data.size() > 0, "Search Results found.",
                "Search Results not found.");

        for (Integer i = 0; i < sweepstakesTable.data.size(); i++) {
            if (sweepstakesName.equals(sweepstakesTable.data.get(i).get("Sweepstakes Name"))) {
                click_PastSweepstakes_SeeDetails_Button_ByName(sweepstakesTable.data.get(i).get("Sweepstakes Name"));

                String endedDate = BaseUI.getTextFromField(Locator.lookupRequiredElement("pastSweepstakes_EndDate_Text")).replaceAll("[\r\n]+", " ");

                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("pastSweepstakes_SweepstakesInfo_Text"));
                BaseUI.verifyElementAppears(Locator.lookupRequiredElement("pastSweepstakes_Winners_Text"));
                BaseUI.verifyElementHasExpectedText("pastSweepstakes_SweepstakesInfo_Text", "Sweepstakes Info");
                BaseUI.verifyElementHasExpectedText("pastSweepstakes_Winners_Text", "Winners");
                BaseUI.baseStringCompare("Ended Date", sweepstakesTable.data.get(i).get("End Date"),
                        endedDate);
                break;
            }
        }
    }

    public static TableData return_CurrentSweepstakesTable() {
        TableData sweepstakesTable = new TableData();
        HashMap<String, String> tableRows = new HashMap<String, String>();

        if (ClientDataRetrieval.isRedesignClient()) {
            tableRows.put("Sweepstakes Name", ".//div[@class='sweepsname-n-desc']/h2");
            //tableRows.put("Begin Date", ".//dd[@class='begdate']");
            tableRows.put("End Date", ".//div//span[@class='countdown']");
            //  tableRows.put("Drawing Date", ".//div//span[@class='countdown']");
        } else {
            tableRows.put("Sweepstakes Name", ".//div[@class='description']/h1/a");
            tableRows.put("Begin Date", ".//dd[@class='begdate']");
            tableRows.put("End Date", ".//dd[@class='enddate']");
            tableRows.put("Drawing Date", ".//dd[@class='drawdate']");
        }
        sweepstakesTable.data = BaseUI.tableExtractor("currentSweepstakes_RowList", tableRows);

        return sweepstakesTable;
    }

    public static void click_SeeDetails_Button_ByName(String sweepstakesName) throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click_js(Locator.lookupRequiredElement("currentSweepstakes_SeeDetails_Button_BySweepstakesName",
                    sweepstakesName, null));
            BaseUI.waitForElementToBeDisplayed("currentSweepstakes_WaysToEnterLabel", null, null);
            Thread.sleep(1500);
        } else {
            BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_SeeDetails_Button_BySweepstakesName",
                    sweepstakesName, null));
            BaseUI.waitForElementToBeDisplayed("currentSweepstakes_WaysToEnter_Quantity_TextBox", null, null);
            Thread.sleep(1000);
        }
    }

    public static void verify_CurrentSweepstakes_ByName(String sweepstakesName, String drawingDate) throws Exception {

        TableData sweepstakesTable = return_CurrentSweepstakesTable();

        BaseUI.verify_true_AndLog(sweepstakesTable.data.size() > 0, "Search Results found.",
                "Search Results not found.");

        int indexWeWant = 0;

        for (Integer i = 0; i < sweepstakesTable.data.size(); i++) {
            if (sweepstakesName.equals(sweepstakesTable.data.get(i).get("Sweepstakes Name"))) {
                indexWeWant = i;
                break;
            }
        }
        click_SeeDetails_Button_ByName(sweepstakesTable.data.get(indexWeWant).get("Sweepstakes Name"));

        if (ClientDataRetrieval.isRedesignClient()) {

            endDate_SweepstakesDetailsPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("EndDate_TextValue"));
            drawingDate_SweepstakesDetailsPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("DrawingDate_TextValue"));
            BaseUI.baseStringCompare("End Date", sweepstakesTable.data.get(indexWeWant).get("End Date"),
                    endDate_SweepstakesDetailsPage);
            BaseUI.baseStringCompare("Drawing Date", drawingDate,
                    drawingDate_SweepstakesDetailsPage);
        } else {

            beginDate_SweepstakesDetailsPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("BeginDate_TextValue"));
            endDate_SweepstakesDetailsPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("EndDate_TextValue"));
            drawingDate_SweepstakesDetailsPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("DrawingDate_TextValue"));

            BaseUI.baseStringCompare("Begin Date", sweepstakesTable.data.get(indexWeWant).get("Begin Date"),
                    beginDate_SweepstakesDetailsPage);
            BaseUI.baseStringCompare("End Date", sweepstakesTable.data.get(indexWeWant).get("End Date"),
                    endDate_SweepstakesDetailsPage);
            BaseUI.baseStringCompare("Drawing Date", sweepstakesTable.data.get(indexWeWant).get("Drawing Date"),
                    drawingDate_SweepstakesDetailsPage);
            BaseUI.baseStringCompare("Drawing Date from Sweepstakes landing page", sweepstakesTable.data.get(indexWeWant).get("Drawing Date"),
                    drawingDate);
        }
    }

    public static void verify_FeaturedSweepstakes_ByName(String sweepstakesName) throws Exception {
        Navigation.navigate_SweepstakesLink_currentSweepstakes();

        if (BaseUI.elementAppears(Locator.lookupOptionalElement("featuredSweepstakes_Right_NavigationArrow"))) {
            ArrayList<String> featuredSweepstakesList = return_FeaturedSweepstakes_NameList();
            BaseUI.verify_true_AndLog(featuredSweepstakesList.size() > 0, "Featured Sweepstakes results Found",
                    "Featured Sweepstakes results Not Found");

            for (Integer i = 0; i < featuredSweepstakesList.size(); i++) {
                if (sweepstakesName.equals(featuredSweepstakesList.get(i))) {
                    click_FeaturedSweepstakes_SeeDetails_ButtonByName(featuredSweepstakesList.get(i));
                    break;
                }
            }
        } else {
            click_FeaturedSweepstakes_SeeDetails_ButtonByName(sweepstakesName);
        }
        sweepstakesName_SweepstakesPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesName"))
                .replaceAll(" Sweepstakes", "");
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("verify_SweepstakesPageTitle"));
        BaseUI.verifyElementHasExpectedText("verify_SweepstakesPageTitle", "Sweepstakes");
        BaseUI.verifyElementHasExpectedText("verify_SweepstakesInfo_Header", "Sweepstakes Info");

        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
            BaseUI.baseStringCompare("verify_SweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName,
                    sweepstakesName_SweepstakesPage);
        } else {
            BaseUI.baseStringCompare("verify_SweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName,
                    sweepstakesName_SweepstakesPage);
            sweepstakesRetailValue_SweepstakesPage = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("verify_SweepstakesValue")).replaceAll("Retail Price:", "")
                    .replace("$", "").replaceAll("Valued At: ", "");
            verifySkuOrRetailsPrice(sweepstakesRetailValue_SweepstakesPage);
        }
    }

    public static void click_FeaturedSweepstakes_SeeDetails_ButtonByName(String sweepstakesName) throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("featuredSweepstakes_SeeDetails_Button_BySweepstakesName",
                sweepstakesName, null))) {
            click_FeaturedSweepstakes_RightNavigation_Arrow();
            count++;
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("featuredSweepstakes_SeeDetails_Button_BySweepstakesName",
                    sweepstakesName, null))) {
                break;
            }
            if (count > 5) {
                break;
            }
        }
        BaseUI.click_js(Locator.lookupRequiredElement("featuredSweepstakes_SeeDetails_Button_BySweepstakesName",
                sweepstakesName, null));
        Thread.sleep(300);
    }

    public static void click_FeaturedSweepstakes_RightNavigation_Arrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("featuredSweepstakes_Right_NavigationArrow"));
        Thread.sleep(300);
    }

    public static void click_SeeOfficialRules_Link_ByName(String sweepstakesName) throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {

            BaseUI.click_js(Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Link_BySweepstakesName", sweepstakesName, null));
        } else {
            BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Link_BySweepstakesName",
                    sweepstakesName, null));
        }
        Thread.sleep(300);
    }

    public static void click_SeeOfficialRules_Link() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Link"));
        Thread.sleep(300);
    }

    public static void click_SeeOfficialRules_Popup_CloseButton() throws Exception {
        BaseUI.click(
                Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Popup_Below_SeeDetailsButton_CloseButton"));
        BaseUI.wait_forPageToFinishLoading();
    }

    public static ArrayList<String> return_FeaturedSweepstakes_NameList() throws Exception {

        ArrayList<WebElement> featuredSweepstakesList = Locator.lookup_multipleElements("featuredSweepstakes_Name_List",
                null, null);

        ArrayList<String> titleList = new ArrayList<String>();

        for (WebElement featuredSweepstakes : featuredSweepstakesList) {
            String featuredSweepstakesStr = BaseUI.get_Attribute_FromField(featuredSweepstakes, "innerText");
            titleList.add(featuredSweepstakesStr);
        }

        return titleList;
    }

    public static void click_Description_MoreButton() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_Description_More_Button"));
        Thread.sleep(300);
    }

    public static void click_Description_LessButton() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_Description_Less_Button"));
        Thread.sleep(300);
    }

    public static void verify_SweeptakesType_DropdownList() {

        ArrayList<WebElement> sweeptakesType_DropdownList = Locator
                .lookup_multipleElements("currentSweepstakes_SweeptakeType_DropDownList", null, null);

        ArrayList<String> sweeptakesTypeList = new ArrayList<String>();

        for (WebElement sweepstakesDropdown_list : sweeptakesType_DropdownList) {
            sweeptakesTypeList.add(BaseUI.get_NonVisible_TextFromField(sweepstakesDropdown_list).trim());
        }

        BaseUI.verify_true_AndLog(sweeptakesTypeList.size() == sweepstakesType_Dropdown.length,
                "Dropdown matched expected values in size.", "Dropdown did not match expected values in size.");

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_SweeptakeType_DropDownList"));

        for (String sweepstakesOption : sweepstakesType_Dropdown) {
            if (Browser.currentBrowser.equals("internetexplorer")) {
                String sweepstakesOptionStr = sweepstakesOption.replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();
                BaseUI.verify_true_AndLog(sweeptakesTypeList.contains(sweepstakesOptionStr),
                        "Sweepstakes Type " + sweepstakesOption + " found in list.",
                        "Sweepstakes Type " + sweepstakesOption + " NOT found in list.");
            } else {
                BaseUI.verify_true_AndLog(sweeptakesTypeList.contains(sweepstakesOption),
                        "Sweepstakes Type " + sweepstakesOption + " found in list.",
                        "Sweepstakes Type " + sweepstakesOption + " NOT found in list.");
            }
        }
    }

    public static void verify_SweeptakesType_Dropdown(String dropdown, String[] breadcrumb) throws Exception {

        Sweepstakes.select_Type_Dropdown(dropdown);
        BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("currentSweepstakes_SweeptakeType_DropDown"),
                dropdown);
        TableData allsweepsTable = Sweepstakes.return_CurrentSweepstakesTable();
        BaseUI.verify_true_AndLog(allsweepsTable.data.size() > 0, "Sweepstakes was found in the results",
                "Sweepstakes was NOT found in the results");
        Navigation.verify_BreadcrumbList(breadcrumb);
    }

    public static void select_Type_Dropdown(String typeOption) throws Exception {
        BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("currentSweepstakes_SweeptakeType_DropDown"), typeOption);
        Thread.sleep(1000);
    }

    public static void select_ResultPerPage_Dropdown(String resultPerPage) throws Exception {
        String currentValue = BaseUI.getSelectedOptionFromDropdown("currentSweepstakes_SweeptakeResultPerPage_DropDown");

        if (!currentValue.equals(resultPerPage)) {
            String valueToInjectIntoPage = "Selecting " + resultPerPage + " from dropdown.";
            BaseUI.inject_TextNode_IntoElement(Locator.lookupRequiredElement("giftCards_SearchResult_TopResult_Count"), valueToInjectIntoPage);

            BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("currentSweepstakes_SweeptakeResultPerPage_DropDown"), resultPerPage);
            BaseUI.waitForElementToNOTContain_PartialAttributeMatch("giftCards_SearchResult_TopResult_Count", null, null, "innerText", valueToInjectIntoPage, 20);
            BaseUI.wait_forPageToFinishLoading();
        }
    }

    public static void click_CurrentSweepstakes_RedeemNow_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Redeem_Button"));
        if (ClientDataRetrieval.isLegacyClient()) {
            BaseUI.waitForElementToBeDisplayed("currentSweepstakes_WaysToEnter_ClaimNow_ElemenToWaitFor", null, null);
        }
        Thread.sleep(500);
    }

    //Specific to redesign, attempting to clear the textbox just results in 1.
    public static void verifyQuantity_CantBeEmpty() throws Exception {
        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);

        WebElement quantityTextBox = Locator.lookupRequiredElement("currentSweepstakes_QuantityTextBox");
        BaseUI.enterText_IntoInputBox_DontUseClear(quantityTextBox, "");
        BaseUI.verifyElementHasExpectedAttributeValue(quantityTextBox, "value", "1");
    }

    // Need to enter quantity more than Sweeptakes available to get the error
    public static void verify_CurrentSweeptakes_Quatity_Error(String quantity, String expectedText) throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            String cumulativeCap = "100";
            expectedText = "You may only redeem up to " + cumulativeCap + " entries for this Sweepstakes";
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                    quantity);
            //Checkbox is not checked with BaseUI method so using js to click
            WebElement agreementCheckBox = Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox");
            BaseUI.click_js(agreementCheckBox);
            Thread.sleep(200);

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            String errorText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            BaseUI.baseStringCompare("Quatity Error", expectedText, errorText);
        } else {
            String ticketError = "You may enter up to 0 tickets.";

            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                    quantity);
            //Checkbox is not checked with BaseUI method so using js to click
            WebElement agreementCheckBox = Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox");
            BaseUI.click_js(agreementCheckBox);
            Thread.sleep(200);

            click_CurrentSweepstakes_RedeemNow_Button();
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            String errorText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            BaseUI.baseStringCompare("Quatity Error", expectedText, errorText);
        }
    }

    public static void verify_CurrentSweeptakes_PleaseEnterQuantity_Error() throws Exception {

        String expectedText = "Please enter a quantity.";

        //Redesign shouldn't have the ability to not enter a quantity
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.log_AndFail("Cannot run this validation for Redesign, please refactor test.");
        } else {

            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                    "");
            //Checkbox is not checked with BaseUI method so using js to click
            WebElement agreementCheckBox = Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox");
            BaseUI.click_js(agreementCheckBox);
            Thread.sleep(200);

            click_CurrentSweepstakes_RedeemNow_Button();
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            String errorText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            BaseUI.baseStringCompare("Quantity Error", expectedText, errorText);
        }
    }

    // Need to enter quantity more than Sweeptakes available to get the error
    public static void verify_CurrentSweeptakes_Quantity_Error(int quantity, int maxAmount) throws Exception {

        String expectedText = "You may only redeem up to " + String.valueOf(maxAmount) + " entries for this Sweepstakes";

        if (ClientDataRetrieval.isRedesignClient()) {
//            String cumulativeCap = "100";
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                    String.valueOf(quantity));
            //Checkbox is not checked with BaseUI method so using js to click
            WebElement agreementCheckBox = Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox");
            BaseUI.click_js(agreementCheckBox);
            Thread.sleep(200);
            click_CurrentSweepstakes_RedeemNow_Button();

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            String errorText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            BaseUI.baseStringCompare("Quantity Error", expectedText, errorText);
        } else {
            expectedText = "You may enter up to " + String.valueOf(maxAmount) + " tickets.";
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                    String.valueOf(quantity));
            //Checkbox is not checked with BaseUI method so using js to click
            WebElement agreementCheckBox = Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox");
            BaseUI.click_js(agreementCheckBox);
            Thread.sleep(200);

            click_CurrentSweepstakes_RedeemNow_Button();
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            String errorText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_Error"));
            BaseUI.baseStringCompare("Quantity Error", expectedText, errorText);
        }
    }

    public static void verify_CurrentSweeptakes_Agreement_Error(String quantity, String expectedText) throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Redeem_ButtonDsbl"));
        } else {
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                    quantity);
            click_CurrentSweepstakes_RedeemNow_Button();
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_Error"));
            String errorText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_Error"));
            BaseUI.baseStringCompare("Agreement Error", expectedText, errorText);
        }
    }

    public static void click_CurrentSweepstakes_Redeem_Popup_Ok_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Popup_Ok_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("currentSweepstakes_WaysToEnter_Popup_Ok_Button", null, null);
        Thread.sleep(500);
    }

    public static void verify_CurrentSweepstakes_Redeemed(String quantity, String expectedText) throws Exception {

        String beforePoints = Navigation.returnPoints();
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"),
                quantity);
        //Checkbox is not checked with BaseUI method so using js to click
        WebElement agreementCheckBox = Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox");
        BaseUI.click_js(agreementCheckBox);

        click_CurrentSweepstakes_RedeemNow_Button();

        if (ClientDataRetrieval.isRedesignClient()) {

            String afterPoints = Navigation.returnPoints();

            BaseUI.verify_true_AndLog(beforePoints != afterPoints,
                    "Points " + beforePoints + " was deducted to " + afterPoints, "Points were Not deducted");
        } else {

            String popupText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Popup_Box_Text"));
            String goodLuckText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Popup_GoodLuck_Text"));

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Popup_Box_Text"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Popup_GoodLuck_Text"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Popup_Ok_Button"));
            BaseUI.baseStringCompare("Popup Text", expectedText, popupText);
            BaseUI.baseStringCompare("Popup Text", "Good Luck!", goodLuckText);

            click_CurrentSweepstakes_Redeem_Popup_Ok_Button();

            String afterPoints = Navigation.returnPoints();

            BaseUI.verify_true_AndLog(beforePoints != afterPoints,
                    "Points " + beforePoints + " was deducted to " + afterPoints, "Points were Not deducted");
        }
    }

    public static ArrayList<String> return_Sweepstakes_NavMenuList() throws Exception {

        ArrayList<WebElement> sweepstakesNavMenuList = Locator.lookup_multipleElements("sweepstakes_Left_NavMenu_List",
                null, null);

        ArrayList<String> menuList = new ArrayList<String>();

        for (WebElement menu : sweepstakesNavMenuList) {
            String menuStr = BaseUI.getTextFromField(menu);
            menuList.add(menuStr);
        }

        return menuList;
    }

    public static void navigate_SweepstakesLeftNav_ByText(String linkText) throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("sweepstakes_LeftNavigation_ByText", linkText, null));
        Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static String returnSweepstakesQts(String expectedText, String ticketCost, String ticketQty) throws Exception {

        Integer ticketQtyInt = Integer.parseInt(ticketQty);

        Double ticketTotal = Double.parseDouble(ticketCost);
        Integer ticketTotalInt = ticketTotal.intValue();
        int totalRewards = (int) (ticketTotalInt * ticketQtyInt);
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
            double totalRewardsD = totalRewards;
            String totalRewardsStr = BaseUI.convertDouble_ToString_ForCurrency(totalRewardsD);
            expectedText = "You have successfully redeemed " + totalRewardsStr + " "
                    + ClientDataRetrieval.client_Currency + " for " + ticketQty + " Sweepstakes Tickets.";
        } else {
            expectedText = "You have successfully redeemed " + totalRewards + " " + ClientDataRetrieval.client_Currency
                    + " for " + ticketQty + " Sweepstakes Tickets.";
        }

        return expectedText;
    }

    public static void clickCurrentSweepstakes_Redeemed() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox"));
        Thread.sleep(1000);
    }

    public static void clickClosedSweepstakes_Tab() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("closedSweepstakes_Tab"));
        BaseUI.waitForElementToContain_PartialAttributeMatch("closedSweepstakes_Tab", null, null, "class", "active", 20);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void clickLiveSweepstakes_Tab() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("liveSweepstakes_Tab"));
        BaseUI.waitForElementToContain_PartialAttributeMatch("liveSweepstakes_Tab", null, null, "class", "active", 20);
        BaseUI.wait_forPageToFinishLoading();
    }


    public static void verifyupcomingSweepstakes_PageTitle_BySweepstakesName_Redeemed() throws Exception {
        Navigation.nav_Sweepstakes_LandingPage();
        Sweepstakes.clickUpcomingSweepstakesHomePage();
        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("upcomingSweepstakes_PageTitle_BySweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName, null));
    }

    public static void clickMySweepstakes_tileName(String nameToExpand) throws Exception {
        if (ClientDataRetrieval.isRedesignClient() && !BaseUI.elementAppears(Locator.lookupOptionalElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton_ByName", nameToExpand, null))) {
            BaseUI.click(Locator.lookupRequiredElement("mySweepstakes_Name_ContainingText", nameToExpand, null));
            BaseUI.waitForElementToBeDisplayed("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton_ByName", nameToExpand, null);
            Thread.sleep(500);
        }
    }

    public static void clickMySweepstakes_tileName() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("mySweepstakes_Name_List"));
            BaseUI.waitForElementToBeDisplayed("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton", null, null);
            Thread.sleep(500);
        }
    }

    public static void click_ClosedSweepStakes_ViewSweepstake_Button() throws Exception {
        BaseUI.click_js(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton"));
        BaseUI.waitForElementToBeDisplayed("pastSweepstakes_Winners_Text", null, null);
        Thread.sleep(1000);
    }
}
