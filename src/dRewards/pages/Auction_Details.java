package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

import java.util.ArrayList;
import java.util.HashMap;

public class Auction_Details {

	public static void click_place_Bid() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctionBid_PlaceBid_Button"));
		BaseUI.waitForElementToBeDisplayed("auctionBid_ConfirmBtn_WaitElement", null, null, 30);
		Thread.sleep(1000);
	}
	
	public static void click_AddToMyAuctions() throws Exception{
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(Locator.lookupRequiredElement("auctionBid_AddOrRemoveAuctionStar"));
			BaseUI.waitForElementToBeDisplayed("auctionBid_RemoveAuctionStar",null,null,10);
			Thread.sleep(1000);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("auctionBid_AddToMyAuction_Link"));
			BaseUI.waitForElementToBeDisplayed("auctionBid_AddToMyAuction_FollowingLink",null,null,10);
			Thread.sleep(1000);
		}	
	}
	
	public static void verify_AddToMyAuctions_LinkAppears()throws Exception{
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_AddToMyAuction_Link"));
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("auctionBid_AddToMyAuction_Link"), "Add to Watch List");
		} else {
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("auctionBid_AddToMyAuction_Link"), "Add to My Auctions");
		}	
	}
	
	public static void click_YouAreFollowingThisAuction_Link() throws Exception{
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(Locator.lookupRequiredElement("auctionBid_AddOrRemoveAuctionStar"));
			BaseUI.waitForElementToNOTBeDisplayed("auctionBid_RemoveAuctionStar",null,null,10);
			Thread.sleep(1000);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("auctionBid_YouAreFollowing_Link"));
			BaseUI.waitForElementToNOTBeDisplayed("auctionBid_AddToMyAuction_FollowingLink",null,null,10);
			Thread.sleep(1000);
		}	
	}
	
	public static void verify_YourAreFollowingThisAuction_LinkAppears()throws Exception{
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Menu("Auctions");
			WebElement auction_DropdownElement = Locator.lookupRequiredElement("auction_Dropdowns");
			BaseUI.selectValueFromDropDown(auction_DropdownElement, "Watch List");
			BaseUI.verifyElementAppears(auction_DropdownElement);
			BaseUI.verify_Value_Selected_InDropdown(auction_DropdownElement,
					"Watch List");
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_YouAreFollowing_Link"));
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("auctionBid_YouAreFollowing_Link"),
					"You are following this auction");
		}	
	}
	
	public static void verify_StopFollowingAuction_Text() throws Exception{
		if (ClientDataRetrieval.isRedesignClient()) {
			WebElement removeFromWatchListElement = Locator.lookupRequiredElement("auctionBid_RemoveFromWatchList_Link");
			BaseUI.verifyElementAppears(removeFromWatchListElement);
			BaseUI.verifyElementHasExpectedText(removeFromWatchListElement,
					"Remove from Watch List");
		} else {
			WebElement element = Locator.lookupRequiredElement("auctionBid_YouAreFollowing_Link");
			BaseUI.elementHover(element);
			String hoverText = BaseUI.getTextFromField(element);
			BaseUI.baseStringCompare("Stop Following Text", "Stop following this auction", hoverText);
		}		
	}

	public static void click_Confirm_Bid() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctionBid_ConfirmBidPopup_ConfirmBidButton"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("nav_AmountOfPoints", null, null, 30);
		Thread.sleep(2000);
	}

	public static void enter_MaxBid(String maxBidToEnter) throws Exception{
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			String maxBid = maxBidToEnter.split("\\.")[0];
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("auctionBid_MaxBidAmount_TextBox"), maxBid);
		} else {
			BaseUI.clearText_Textbox(Locator.lookupRequiredElement("auctionBid_MaxBidAmount_TextBox"));
			Thread.sleep(1000);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("auctionBid_MaxBidAmount_TextBox"), maxBidToEnter);
		}
	}

	public static void enter_And_Confirm_MaxBid(String maxBidToEnter) throws Exception {
		enter_MaxBid(maxBidToEnter);
		click_place_Bid();
		click_Confirm_Bid();
	}

	// Verify Bid successful for Points based bidding.
	public static void verify_AuctionDetails_DualCurrency(
			Double originalBid, /* Double amountOfPoints_IncreaseExpected, */
			String expectedPointsOrRewards, Double usd_Amount) throws Exception {

		verify_AuctionDetails_SingleCurrency(originalBid, expectedPointsOrRewards);
		verify_USD_Payment_AmountListed(usd_Amount);		
	}

	public static void verify_AuctionDetails_SingleCurrency(Double originalBid,
			/* Double amountOfPoints_IncreaseExpected */ String expectedPointsOrRewards) {
		verify_CurrentBid(originalBid);

		verify_PointsOrRewards_ToBid(expectedPointsOrRewards);
		verify_BidButtonDisabled();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_MaxBidAmount_TextBox"));
		
		if (ClientDataRetrieval.isLegacyClient()) {
			verify_ViewLargerImage_Link();
		}		
		verify_Auctions_Image();
	}
	
	public static void verify_ConfirmBid_ModalAppears(){
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_ConfirmBidPopup"));
	}
	
	public static void verify_ConfirmBidModal_Info(String expectedPointsOrRewards) {
		verify_ConfirmBid_ModalAppears();
		if (!ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			expectedPointsOrRewards = expectedPointsOrRewards.split("\\.")[0];
		}
		BaseUI.verifyElementHasExpectedPartialText("auctionBid_ConfirmBidPopup_PointsOrRewards",
				expectedPointsOrRewards + " " + ClientDataRetrieval.client_Currency);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_ConfirmBidPopup_ConfirmBidButton"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_ConfirmBidPopup_CancelButton"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_ConfirmBidPopup_Disclaimer_Text"));
	}

	public static void verify_USD_Payment_AmountListed(Double usdAmount) {
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("auctionBid_AmountDueOnWin_Title", "Additional payment due upon winning");
		} else {
			BaseUI.verifyElementHasExpectedText("auctionBid_AmountDueOnWin_Title", "Plus payment due upon winning:");
		}
		
//		BaseUI.verifyElementHasExpectedText("auctionBid_AmountDue_Currency", "US Dollars");
		BaseUI.verifyElementHasExpectedText("auctionBid_DollarAmountDue_OnWin",
				"$" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount));
	}

	public static void click_IAgree_DualCurrencyModal() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctionBid_AmountDuePopup_AgreeButton"));
		BaseUI.waitForElementToNOTBeDisplayed("auctionBid_AmountDuePopup_AgreeButton",null,null, 10);
		Thread.sleep(1000);
	}

	public static void verify_DualCurrency_AcceptModal_NotVisible() throws Exception {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("auctionBid_AmountDuePopup_AgreeButton"));
	}

	public static void verify_DualCurrency_AcceptModal(Double usdAmount) {

		String currency = "Points";
		String expectedMessage;

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			currency = "Smart Dollars";
		}

		expectedMessage = "I agree to pay the amount of my winning bid of " + currency
				+ ", plus the fixed amount of $" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount)
				+ " US Dollars, upon checkout if I am the winning bidder of this auction. If I win the bid and do not checkout within 30 days, I will still forfeit my "
				+ currency + " bid.";

		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.verifyElementHasExpectedText("auctionBid_AmountDuePopup_Text", expectedMessage);
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_AmountDuePopup_AgreeButton"));
		} 
	}

	public static void verify_PointsOrRewards_ToBid(String expectedPointsOrRewards) {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_MaxBidAmount_Text"));
		BaseUI.verifyElementHasExpectedPartialText("auctionBid_MaxBidAmount_Text", expectedPointsOrRewards);
		// String actualPointsOrRewards =
		// BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_MaxBidAmount_Text"));
		// BaseUI.baseStringCompare("PointsToBid", expectedPointsOrRewards,
		// actualPointsOrRewards);
	}

	public static void verify_ViewLargerImage_Link() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_ViewLargerImage_Link"));
		BaseUI.verifyElementHasExpectedText("auctionBid_ViewLargerImage_Link", "View Larger Image");
	}

	public static void verify_Auctions_Image() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_Image"));
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("auctionBid_Image"), "src", ".jpg");
	}

	public static void verify_CurrentBid(Double amountExpected) {
		String originalBidStr = "";

		Integer originalBidAmount = amountExpected.intValue();
		originalBidStr = originalBidAmount.toString().replaceAll(" Points", "");
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("auctionBid_CurrentBidAmount", originalBidStr.trim()+" Points");
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
				originalBidStr = BaseUI.convertDouble_ToString_ForCurrency(amountExpected);
			}
			BaseUI.verifyElementHasExpectedText("auctionBid_CurrentBidAmount", originalBidStr);
		}
	
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"));
		// Smart rewards Current Bid has issue so verifying with pagesource
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			BaseUI.verify_false_AndLog(BaseUI.pageSourceContainsString("Current Bid"),
					"Current Bid text found in Page Source.", "Current Bid text was Not found in Page Source.");
		}

	}

	// Verify that you WERE able to bid
	public static void verify_BidSuccessful(String expectedNewAmount) {
		// BaseUI.wait_ForElement_ToContainText("auctionBid_CurrentBidAmount",
		// 5, expectedNewAmount);
        String actualPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"));
		
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("Bid Amount", (expectedNewAmount+" Points"), actualPoints);
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"), expectedNewAmount+" Points");
		} else {
			BaseUI.baseStringCompare("Bid Amount", expectedNewAmount, actualPoints);
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"), expectedNewAmount);
		}
		

		verify_BidButtonDisabled();
	}

	public static void verify_LeadingBidder(String expectedNewAmount){
		String actualPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"));		
		String maxBidAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_MaxBid_Amount"));
		String leadingBidderText;
		if (ClientDataRetrieval.isRedesignClient()) {
			 leadingBidderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_LeadingBidder_Text"))
					 .replaceAll("52 Points", "").replace("\n", "");
		BaseUI.baseStringCompare("Bid Amount", expectedNewAmount + " Points", actualPoints);
		}else{
			 leadingBidderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_LeadingBidder_Text"));
		BaseUI.baseStringCompare("Bid Amount", expectedNewAmount, actualPoints);
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_LeadingBidder_Text"));
		BaseUI.baseStringCompare("LeadinggBidderText", "You're the leading bidder", leadingBidderText);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_MaxBid_Text"));
		if(ClientDataRetrieval.isRedesignClient()) {
	
			BaseUI.baseStringCompare("MaxBidAmount","Your max bid is "+ expectedNewAmount+" Points.", maxBidAmount);
		} else {
			BaseUI.baseStringCompare("MaxBidAmount", expectedNewAmount, maxBidAmount);
		}
		BaseUI.verifyElementHasExpectedPartialText("auctionBid_RaiseMaxBid_Text", "Raise your max bid");
	}

	public static void verify_BidButtonDisabled() {
		// String expectedClassForDisabledBidButton = "btn place-bid single
		// disabled";
		// String actualClassText = BaseUI
		// .get_Attribute_FromField(Locator.lookupRequiredElement("auctionBid_PlaceBid_Button"),
		// "class").replace(" ", " ");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_PlaceBid_Button"));
		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("auctionBid_PlaceBid_Button"),
					"class", "disabled");
		} 	
		// BaseUI.baseStringCompare("Place Bid Class",
		// expectedClassForDisabledBidButton, actualClassText);
	}

	// public static void verify_NextBid(Integer newAmount, Double
	// newAmountDouble, String expectedNewAmount,
	// Double amountOfPoints_IncreaseExpected) {
	//
	// }

	// Validation for this is extremely weak. There is no text in the banner
	// element. So we have to just assert that the text for the other statuses
	// doesn't appear in the class.
	public static void verify_StartBidding() {
		WebElement bidElement = Locator.lookupRequiredElement("auctionBid_BidStatusElement");
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(bidElement, "class", "winning");
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(bidElement, "class", "outbid");
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(bidElement, "class", "won");
		BaseUI.verifyElementDoesNotHavePartialAttributeValue(bidElement, "class", "expired");
	}

	public static void verify_Bid_ErrorMessages_SingleCurrency(String maxBidToEnter, String expectedErrorMessage)
			throws Exception {
		enter_MaxBid(maxBidToEnter);
		click_place_Bid();
		if (BaseUI.pageSourceContainsString("You are about to submit a bid ")) {
			click_Confirm_Bid();
		}
		String actualErrorMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_ErrorMessage"));
		BaseUI.baseStringCompare("InvalidBid Error Message", expectedErrorMessage, actualErrorMessage);
	}
	
	public static void verify_WinLimit_Message(String expectedMessage){
		String  actualMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_WinLimit_Message"));
		BaseUI.baseStringCompare("Win Limit Message", expectedMessage, actualMessage);
		
	}

	public static void verify_WinningAuction() {
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("auctionBid_BidStatusElement"),
				"class", "winning");
	}

	public static void verify_Outbid(String initialPoints) throws Exception {
		
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("auctionBid_BidStatusElement"), 
					"innerText", "outbid");
		} else {
			BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("auctionBid_BidStatusElement"), 
					"class", "outbid");
		}
		Navigation.verify_pointsMatchExpected(initialPoints);
	}

	public static void verify_AuctionWon_Banner_Appears() throws InterruptedException {

		if (ClientDataRetrieval.isRedesignClient()) {
			Thread.sleep(1000);
			BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("auctionBid_BidStatusElement"), 
					"innerText", "Winner");
		} else {
			BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("auctionBid_BidStatusElement"),
					"class", "won");
		}		
	}

	public static void verify_TimeLeft_ContainsCorrectFormat() {
				
		if (ClientDataRetrieval.isRedesignClient()) {
			
		     String timeText = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_TimeLeft"));
			///String timeText = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_TimeLeft_DateTime"));
			
			
			BaseUI.verify_true_AndLog(timeText.matches("^(\\d{2}m\\s\\d{2}s)$"), "Matched pattern MM/dd/yy at h:mm a",
					"Text did not look right, seeing value " + timeText);			
		} else {
			String timeText = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_TimeLeft"));
			BaseUI.verify_true_AndLog(timeText.matches("^(\\d{2}h\\s\\d{2}m\\s\\d{2}s)$"), "Matched pattern 00h 00m 00s",
					"Text did not look right, seeing value " + timeText);		
	    }
	}
	public static String return_RetailPrice() {
		String retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_RetailPrice"));
		retailPrice = retailPrice.substring(retailPrice.indexOf("$"), retailPrice.length());

		return retailPrice;
	}
	
	public static String return_UserName(){
		String expectedUserName = BaseUI.getTextFromField(Locator.lookupRequiredElement("homepage_UserName"));
		if(ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				||ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)){
			expectedUserName = expectedUserName.replaceAll("[\r\n]+", " ").split("\\|")[0].split("\\,")[1].trim();
		}else{
			expectedUserName = expectedUserName.split("\\,")[1].trim();
		}
		expectedUserName = expectedUserName.toLowerCase();
		
		return expectedUserName;
	}
	
	
	public static void verify_User_DidNot_Win_Auction(String expectedNewAmount){
		
		 if (ClientDataRetrieval.isRedesignClient()) {
			
				String actualPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount_ClosedAuction"));
				
			actualPoints = actualPoints.split("\\n")[0];
				
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_TimeLeft"));
				String Ended = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_TimeLeft"));
				BaseUI.verifyElementHasExpectedText("auctionBid_TimeLeft", Ended);
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount_ClosedAuction"));
				BaseUI.baseStringCompare("Bid Amount", expectedNewAmount+" Points", actualPoints);
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_Winner_Text"));
				BaseUI.verifyElementHasExpectedText("auctionBid_Winner_Text", "Winner");
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_Winner_Name")); 
		 }else{
		String actualPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_ClosedAuction_Text"));
		BaseUI.verifyElementHasExpectedText("auctionBid_ClosedAuction_Text", "Closed");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_CurrentBidAmount"));
		BaseUI.baseStringCompare("Bid Amount", expectedNewAmount, actualPoints);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_Winner_Text"));
		BaseUI.verifyElementHasExpectedText("auctionBid_Winner_Text", "Winner:");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_Winner_Name"));
		 }
	}
	public static void wait_For_End_OfAuction(Integer timeToWaitInSeconds) throws Exception {
//		if (ClientDataRetrieval.isRedesignClient()) {
//			String auctionEndingTime = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_TimeLeft_DateTime"));
//			auctionEndingTime = auctionEndingTime
//					.substring(auctionEndingTime.lastIndexOf("at"), auctionEndingTime.length()).trim();
//			if(timeToWaitInSeconds.equals(00)) {
//				BaseUI.verify_true_AndLog((BaseUI.get_SystemTimezone()).equals(auctionEndingTime), 
//						"Auction Ending time displayed MATCH the system time", 
//						"Auction Ending time displayed DOES NOT MATCH the system time");
//			}
//			
//			Browser.driver.navigate().refresh();			
//			BaseUI.waitForElementToBeDisplayed("auctionBid_TimeLeft", null, null, 30);			
//			BaseUI.verifyElementHasExpectedText("auctionBid_TimeLeft", "Ended ");	
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.wait_ForElement_ToContainText("auctionBid_TimeLeft", timeToWaitInSeconds, "Ended");
		} else {	
			BaseUI.wait_ForElement_ToContainText("auctionBid_TimeLeft", timeToWaitInSeconds, "00:00:00");		
	    }
    }
	
	public static void wait_For_ClaimPopup_ToAppear() throws Exception {
		BaseUI.wait_for_PageSource_ToContainText("You are the winning bidder!", 20);
	}

	public static void verify_AuctionWon_PopupAppears() {
		
		if(!ClientDataRetrieval.isRedesignClient());
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionPop_WinningBidderPopup"));
		BaseUI.verifyElementHasExpectedText("auctionPop_WinMessage", "You are the winning bidder!");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionPop_ClaimNow_Button"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionPop_RemindMeLater_Button"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionPop_DontShowMeAgain_Button"));
	}

	public static void verify_AuctionWon_NotVisible() {
		BaseUI.verifyElementDoesNotAppearByString("auctionPop_WinningBidderPopup");
	}

	// returns hashmap with myBid, bidTime in format of MMM. d, yyyy h:mm:ss a
	// EST and bidAmount.
	public static HashMap<String, String> return_bidHistoryInfo_AsRow(Boolean myBid, String bidAmount)
			throws Exception {
		HashMap<String, String> newBid = new HashMap<String, String>();
		newBid.put("myBid", myBid.toString());
		Integer timeOffset = 0;
		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			timeOffset += 60;
		}
		// String currentTime =
		// BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(timeOffset,
		// "MMM. d, yyyy h:mm:ss a") + " EST";
		// newBid.put("bidTime", currentTime);
		String currentTime;
		if (ClientDataRetrieval.isRedesignClient()) {
			currentTime = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(timeOffset,
					"MMM d, yyyy h:mm:ss a");

			newBid.put("bidTime_Month",
					BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM d, yyyy h:mm:ss a", "MMM"));
			newBid.put("bidTime_Day", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM d, yyyy h:mm:ss a", "d"));
			newBid.put("bidTime_Year", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM d, yyyy h:mm:ss a", "yyyy"));
			newBid.put("bidTime_Hours", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM d, yyyy h:mm:ss a", "h"));
		
			newBid.put("bidTime_AM/PM", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM d, yyyy h:mm:ss a", "a"));
			newBid.put("bidTime_TimeZone",(GlobalVariables.auctionTimeZone.replace("," , " ")));
			newBid.remove(" ");
		} else {
			currentTime = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(timeOffset,
					"MMM. d, yyyy h:mm:ss a");

			newBid.put("bidTime_Month",
					BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM. d, yyyy h:mm:ss a", "MMM."));
			newBid.put("bidTime_Day", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM. d, yyyy h:mm:ss a", "d"));
			newBid.put("bidTime_Year", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM. d, yyyy h:mm:ss a", "yyyy"));
			newBid.put("bidTime_Hours", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM. d, yyyy h:mm:ss a", "h"));
			// Time showing on screen by vary by a few seconds, so cannot compare
			// Minutes or Seconds.
			// newBid.put("bidTime_Minutes",
			// BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM. d, yyyy
			// h:mm:ss a", "mm"));
			newBid.put("bidTime_AM/PM", BaseUI.return_Date_AsDifferentFormat(currentTime, "MMM. d, yyyy h:mm:ss a", "a"));
			newBid.put("bidTime_TimeZone",(GlobalVariables.auctionTimeZone.replace("," , " ")));
			newBid.remove(" ");
		}
				

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			Double bidDouble = Double.parseDouble(bidAmount);
			bidAmount = BaseUI.convertDouble_ToString_ForCurrency(bidDouble);
		} else {
			bidAmount = bidAmount.split("\\.")[0];
		}
		newBid.put("bidAmount", bidAmount);

		return newBid;
	}

	public static void click_ClaimNow_InWinningPopup() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctionPop_ClaimNow_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("auctionPop_ClaimNow_Button",null,null,30);
		Thread.sleep(2000);
	}

	public static void click_RemindMeLater_InWinningPopup() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctionPop_RemindMeLater_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("auctionPop_RemindMeLater_Button",null,null,30);
		Thread.sleep(2000);
	}

	public static void click_View_BidHistory() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctionBid_bidHistoryLink"));
		BaseUI.waitForElementToBeDisplayed("bidHistory_TotalBidders", null, null, 10);
		Thread.sleep(2000);
	}

	public static void click_Close_BidHistoryWindow() throws Exception {
		
	
		BaseUI.click(Locator.lookupRequiredElement("bidHistory_Close_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("bidHistory_Close_Button", null,null, 20);
	}

	public static void verify_BidBox_Links_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_HowBidWorks_Link"));
		BaseUI.verifyElementHasExpectedPartialText("auctionBid_HowBidWorks_Link", "How does bidding work?");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctionBid_bidHistoryLink"));	
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedPartialText("auctionBid_bidHistoryLink", "View Total Bids (");
		} else {
			BaseUI.verifyElementHasExpectedPartialText("auctionBid_bidHistoryLink", "Total Bids");
			}
	}
	
	
	public static void verify_BidHistory(TableData expectedTableData, Integer bidderCount, Integer totalBids)
			throws Exception {
		expectedTableData.sort_ByColumn_numeric_Descending("bidAmount");
		TableData actualTableData = return_BidHistoryData();
		BaseUI.verify_true_AndLog(expectedTableData.data.size() == actualTableData.data.size(),
				"Bid History count was correct.", "Bid History count was NOT correct.");
		BaseUI.verify_true_AndLog(actualTableData.data.size() > 0, "Found Bid History.", "Did NOT find bid history.");

		for (Integer i = 0; i < actualTableData.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), expectedTableData.data.get(i), actualTableData.data.get(i));
		}
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("bidHistory_TotalBidders", bidderCount.toString());
			BaseUI.verifyElementHasExpectedText("bidHistory_TotalBids", totalBids.toString());

		} else {
			BaseUI.verifyElementHasExpectedText("bidHistory_TotalBidders" ,"Bidders: " + bidderCount.toString());
			BaseUI.verifyElementHasExpectedText("bidHistory_TotalBids","Total Bids: " + totalBids.toString());
		}
			BaseUI.verifyElementHasExpectedText("bidHistory_BiddingHistoryHeader", "Bidding History");
	}
	public static TableData return_BidHistoryData() throws Exception {
		TableData bidHistory = new TableData();
		ArrayList<WebElement> rowList = Locator.lookup_multipleElements("bidHistory_BidHistory_Rows", "", "");
		


		for (WebElement row : rowList) {
			HashMap<String, String> rowData = new HashMap<String, String>();
			String myBid = BaseUI.get_Attribute_FromField(row, "class");
		
			String bidTime = "";
			String timeZone = "";
			String bidAmount;
			if (ClientDataRetrieval.isRedesignClient()) {
				rowData.put("myBid", myBid.contains("white-bg") ? Boolean.toString(true) : Boolean.toString(false));
				bidTime = BaseUI.getTextFromField(row.findElement(By.xpath("./td[3]")));
				timeZone = bidTime.substring(bidTime.lastIndexOf(" "), bidTime.length()).trim();
				bidTime = bidTime.replaceAll("at", "");
			//	bidTime = bidTime.replaceAll(",", "");
				rowData.put("bidTime_Month",
						BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM d, yyyy h:mm:ss a", "MMM"));
				
				rowData.put("bidTime_Day", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM d, yyyy h:mm:ss a", "d"));
				rowData.put("bidTime_Year",
						BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM d, yyyy h:mm:ss a", "yyyy"));
				rowData.put("bidTime_Hours", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM d, yyyy h:mm:ss a", "h"));
				rowData.put("bidTime_AM/PM", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM d, yyyy h:mm:ss a", "a"));
				rowData.put("bidTime_TimeZone", timeZone);
				
				rowData.replace(".", "");
				bidAmount = BaseUI.getTextFromField(row
						.findElement(By.xpath("./td[2]")));
			} else{
				rowData.put("myBid", myBid.contains("currentBidder") ? Boolean.toString(true) : Boolean.toString(false));
				bidTime = BaseUI.getTextFromField(row.findElement(By.xpath("./td[@class='last']")));
				timeZone = 	bidTime.substring(bidTime.lastIndexOf(" "), bidTime.length()).trim();
				bidTime = bidTime.substring(0, bidTime.lastIndexOf(" ")).trim();
				rowData.put("bidTime_Month",
						BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "MMM."));
				rowData.put("bidTime_Day", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "d"));
				rowData.put("bidTime_Year",
						BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "yyyy"));
				rowData.put("bidTime_Hours", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "h"));
				// rowData.put("bidTime_Minutes",
				// BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy
				// h:mm:ss a", "mm"));
				rowData.put("bidTime_AM/PM", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "a"));
				rowData.put("bidTime_TimeZone", timeZone);
				
				rowData.replace(",", ".");
				bidAmount = BaseUI.getTextFromField(row.findElement(By.xpath("./td[@class='bidAmount']")));
			}
		
			// rowData.put("bidTime", bidTime);
			rowData.put("bidAmount", bidAmount);

			bidHistory.data.add(rowData);
		}
	
		return bidHistory;

	}
	
    public static TableData return_BidHistoryDataMyAccountRedesign() throws Exception {
        TableData bidHistory = new TableData();
        ArrayList<WebElement> rowList = Locator.lookup_multipleElements("bidHistory_BidHistory_Rows", "", "");

        for (WebElement row : rowList) {
            HashMap<String, String> rowData = new HashMap<String, String>();
            String myBid = BaseUI.get_Attribute_FromField(row, "class");
            rowData.put("myBid", myBid.contains("currentBidder") ? Boolean.toString(true) : Boolean.toString(false));
            String bidTime = "";
            String timeZone = "";


                bidTime = BaseUI.getTextFromField(row.findElement(By.xpath("//td[@class='collapsable']//tbody/tr/td/div[@class='text-label nowrap small-text']")));
                timeZone = bidTime.substring(bidTime.lastIndexOf(" "), bidTime.length()).trim();
                bidTime = bidTime.substring(0, bidTime.lastIndexOf("at")).trim();
                rowData.put("bidTime_Month",
                        BaseUI.return_Date_AsDifferentFormat(bidTime, "MMMM d, yyyy h:mm:ss a", "MMMM"));

        
            // rowData.put("bidTime", bidTime);

            rowData.put("bidTime_Day", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "d"));
            rowData.put("bidTime_Year",
                    BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "yyyy"));
            rowData.put("bidTime_Hours", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "h"));
            // rowData.put("bidTime_Minutes",
            // BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy
            // h:mm:ss a", "mm"));
            rowData.put("bidTime_AM/PM", BaseUI.return_Date_AsDifferentFormat(bidTime, "MMM. d, yyyy h:mm:ss a", "a"));
            rowData.put("bidTime_TimeZone", timeZone);
            
            rowData.replace(",", ".");
            String bidAmount = BaseUI.getTextFromField(row.findElement(By.xpath("./td[@class='bidAmount']")));
            rowData.put("bidAmount", bidAmount);

            bidHistory.data.add(rowData);
        }
    
        return bidHistory;
    }
    
}


