package dRewards.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class Navigation {

    public static void navigate_ShoppingCart() throws Exception {
        BaseUI.click_js(Locator.lookupRequiredElement("nav_ShoppingCartLink"));
        BaseUI.waitForElementToBeDisplayed("shpCart_Page_Header", null, null, 30);
        Thread.sleep(500);
    }

    public static void navigate_Home_viaLogo() throws Exception {

        // AARP logo currently taking you to live AARP site.
        if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("nav_HomeLink"));
            navigate_Home_viaHomeLink();
        } else {
            BaseUI.click(Locator.lookupRequiredElement("nav_HomeLogo"));
            BaseUI.waitForElementToBeDisplayed("homePage_ElementToWaitFor", null, null, 30);
            Thread.sleep(2000);
        }
    }

    public static void navigate_Home_viaHomeLink() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {

//            String textToInject = "Navigating to Home page via Automation";
//            BaseUI.inject_TextNode_IntoElement(Locator.lookupRequiredElement("nav_HomeLogo"), textToInject);

            if (BaseUI.elementExists("nav_HomeLink", null, null)) {
                BaseUI.click_js(Locator.lookupRequiredElement("nav_HomeLink"));
            } else {
                BaseUI.click(Locator.lookupRequiredElement("nav_HomeLogo"));
            }

           // BaseUI.waitForElementToNOTContain_PartialAttributeMatch("nav_HomeLogo", null, null, "innerText", textToInject, 40);
        }else{
            if (BaseUI.elementExists("nav_HomeLink", null, null)) {
                BaseUI.click_js(Locator.lookupRequiredElement("nav_HomeLink"));
            } else {
                BaseUI.click(Locator.lookupRequiredElement("nav_HomeLogo"));
            }
        }

        BaseUI.waitForElementToBeDisplayed("homePage_ElementToWaitFor", null, null, 30);
        Thread.sleep(1500);
    }

    public static void navigate_Logout() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            BaseUI.click(Locator.lookupRequiredElement("homepage_ClickUsername"));
            BaseUI.waitForElementToBeDisplayed("nav_Logout_Link", null, null, 30);
            click_Logout();
        } else {
            click_Logout();
        }
    }

    public static void click_Logout() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_Logout_Link"));
        if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            BaseUI.waitForAlertToAppear(10);
            BaseUI.switchToAlert_Accept();
        }
        BaseUI.wait_forPageToFinishLoading();
        Thread.sleep(1000);
    }

    public static void navigate_Menu_ByText(String menuText) throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_MenuItem", menuText, null));
        Thread.sleep(500);
    }

    public static void navigate_Merchandise() throws Exception {
        if (ClientDataRetrieval.clientName.equals("Verizon Wireless-9338")) {
            navigate_Menu_Submenu("Merchandise", "Merchandise");
            Thread.sleep(2000);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
            navigate_Menu_Submenu("Merchandise", "Merchandise");
        } else if (ClientDataRetrieval.isRedesignClient()) {
            navigate_Menu_ByText("Merchandise");
            BaseUI.waitForElementToBeDisplayed("merchandisePage_Header", null, null, 30);
            Thread.sleep(2000);
        } else {
            navigate_Menu_ByText("Merchandise");
            BaseUI.waitForElementToBeDisplayed("merchandisePage_Header", null, null, 30);
            Thread.sleep(1000);
        }
    }

    // Click Merchandise Menu Tab or Merchandise Search Button to navigate to
    // Merchandise Landing Page
    public static void navigate_Click_MerchandiseMenuTab_MerchandisePage() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
                || (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both))
                || ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("nav_Merchandise_MenuTab"));
            BaseUI.wait_forPageToFinishLoading();
            BaseUI.waitForElementToBeDisplayed("merch_FeaturedBrand_Header", null, null, 50);
            Thread.sleep(2000);
        } else {
            Homepage.click_MerchandiseSearchSubmit();
        }
    }

    public static ArrayList<String> return_ListOfMerchandise_TopLinks() {
        ArrayList<String> textList = new ArrayList<String>();
        ArrayList<WebElement> merchandiseOptions = Locator.lookup_multipleElements("nav_Merchandise_ListOfLinks", null,
                null);
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            for (WebElement link : merchandiseOptions) {
                textList.add(BaseUI.get_Attribute_FromField(link, "innerHTML").trim().replace("&amp;", "&"));
            }
        } else {
            for (WebElement link : merchandiseOptions) {
                textList.add(BaseUI.get_NonVisible_TextFromField(link).trim());
            }
        }

        return textList;
    }

    public static ArrayList<String> return_List_OfLinks_ByCategoryText(String category) throws Exception {
        ArrayList<String> textList = new ArrayList<String>();
        ArrayList<WebElement> merchandiseOptions = new ArrayList<WebElement>();

        if (ClientDataRetrieval.isRedesignClient()) {
            if (category.equals("Travel")) {
                clickMenuItemForEachCategory_VerifyOops_Or404ErrorDisplays(category);
                merchandiseOptions = Locator.lookup_multipleElements("nav_Travel_ListOfLinks", category, null);
            } else if (category.equals("Local Deals")) {
                clickMenuItemForEachCategory_VerifyOops_Or404ErrorDisplays(category);
                merchandiseOptions = Locator.lookup_multipleElements("nav_LocalOffers_ListOfLinks", category, null);
            } else if (category.equals("Featured Brands")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_FeaturedBrands_ListOfLinks", category, null);
            } else if (category.equals("Auctions")) {
                clickMenuItemForEachCategory_VerifyOops_Or404ErrorDisplays(category);
                merchandiseOptions = Locator.lookup_multipleElements("nav_Auctions_ListOfLinks", category, null);
            } else if (category.equals("Sweepstakes")) {
                clickMenuItemForEachCategory_VerifyOops_Or404ErrorDisplays(category);
                merchandiseOptions = Locator.lookup_multipleElements("nav_Sweepstakes_ListOfLinks", category, null);
            } else {
                merchandiseOptions = Locator.lookup_multipleElements("nav_ListOfLinks_BySectionHeader", category, null);
            }
        } else if (!ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
                && !ClientDataRetrieval.client_Matches(client_Designation.Citi)
                && !ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            if (category.equals("Travel")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_Travel_ListOfLinks", category, null);
            } else if (category.equals("Local Offers")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_LocalOffers_ListOfLinks", category, null);
            } else if (category.equals("Featured Brands")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_FeaturedBrands_ListOfLinks", category, null);
            } else if (category.equals("Auctions")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_Auctions_ListOfLinks", category, null);
            } else if (category.equals("Sweepstakes")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_Sweepstakes_ListOfLinks", category, null);
            } else {
                merchandiseOptions = Locator.lookup_multipleElements("nav_ListOfLinks_BySectionHeader", category, null);
            }
        } else {
            if (category.equals("Merchandise")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_Merchandise_ListOfLinks", category, null);
            } else if (category.equals("Featured Brands")) {
                merchandiseOptions = Locator.lookup_multipleElements("nav_FeaturedBrands_ListOfLinks", category, null);
            } else {
                merchandiseOptions = Locator.lookup_multipleElements("nav_ListOfLinks_BySectionHeader", category, null);
            }
        }

        for (WebElement link : merchandiseOptions) {
            String linkText = BaseUI.get_Attribute_FromField(link, "innerHTML").replace("&amp;", "&").trim();// getTextFromField(link).trim();

            textList.add(linkText);
        }

        return textList;
    }

    public static void clickMenuItemForEachCategory_VerifyOops_Or404ErrorDisplays(String category) throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_MenuItem", category, null));
        BaseUI.wait_forPageToFinishLoading();
        verify_OopsPage_DoesNOT_Appear();
        verifyPageDoesNotDisplay_404Error();
    }

    public static void verify_IntroBanner_Appears(introBanner pageValue) {
        WebElement introBannerElement = Locator.lookupRequiredElement("nav_IntroBanner");

        BaseUI.verifyElementAppears(introBannerElement);
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementHasExpectedAttributeValue(introBannerElement, "alt", pageValue.getValue());
        }
    }

    public enum introBanner {
        Merchandise("marketplace"), GiftCards("giftcardsLanding"), Travel("travel_landing"), CarRentals(
                "aviscarsearch"), HotelsAndCondos("hotel_search"), TravelGiftCards(
                "travel_gift_cards.jpg "), TopTravelDeals("top_travel_deals"), TravelEssentials(
                "non_0213_travel-essential-subcat_b1291032_v4_banneronly.jpg ");

        private String value;

        private introBanner(final String val) {
            value = val;
        }

        public String getValue() {
            return value;
        }
    }

    public static introBanner getIntroBanner(String code) {

        code = code.replace(" ", "");

        switch (code) {
            case "Merchandise":
                return introBanner.Merchandise;
            case "GiftCards":
                return introBanner.GiftCards;
            case "Travel":
                return introBanner.Travel;
            case "CarRentals":
                return introBanner.CarRentals;
            case "CarRental":
                return introBanner.CarRentals;
            case "HotelsandCondos":
                return introBanner.HotelsAndCondos;
            case "TravelGiftCards":
                return introBanner.TravelGiftCards;
            case "TopTravelDeals":
                return introBanner.TopTravelDeals;
            case "TravelDeals":
                return introBanner.TopTravelDeals;
            case "TravelEssentials":
                return introBanner.TravelEssentials;
            default:
                return null;
        }
    }

    public static void verify_No_ApplicationErrorMessage() {
        BaseUI.verify_false_AndLog(BaseUI.pageSourceContainsString("Application Error"),
                "No Application Error text found in Page Source.", "Found Application Error.");
    }

    public static void verify_OopsPage_DoesNOT_Appear() {
        if (BaseUI.pageSourceContainsString("Oops!")) {
            BaseUI.log_AndFail("Oops! Text appears in the page");
        }
    }

    public static void verifyPageDoesNotDisplay_404Error() {
        if (BaseUI.pageSourceContainsString("404 Not Found")) {
            BaseUI.log_AndFail("404 Not Found! Text appears on the page");
        }
    }

    public static ArrayList<String> return_List_OfLinks_ByCategoryText_AndKeepFormatting(String category) {
        ArrayList<String> textList = new ArrayList<String>();
        ArrayList<WebElement> merchandiseOptions = new ArrayList<WebElement>();
        if (category.equals("Travel")) {
            merchandiseOptions = Locator.lookup_multipleElements("nav_Travel_ListOfLinks", category, null);
        } else if (category.equals("Local Offers")) {
            merchandiseOptions = Locator.lookup_multipleElements("nav_LocalOffers_ListOfLinks", category, null);
        } else if (category.equals("Featured Brands")) {
            merchandiseOptions = Locator.lookup_multipleElements("nav_FeaturedBrands_ListOfLinks", category, null);
        } else if (category.equals("Auctions")) {
            merchandiseOptions = Locator.lookup_multipleElements("nav_Auctions_ListOfLinks", category, null);
        } else if (category.equals("Sweepstakes")) {
            merchandiseOptions = Locator.lookup_multipleElements("nav_Sweepstakes_ListOfLinks", category, null);
        } else {
            merchandiseOptions = Locator.lookup_multipleElements("nav_ListOfLinks_BySectionHeader", category, null);
        }

        for (WebElement link : merchandiseOptions) {
            textList.add(BaseUI.get_NonVisible_TextFromField(link));
        }

        return textList;
    }

    public static void verify_BreadcrumbList(String[] breadcrumbArray) {
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            ArrayList<WebElement> breadCrumbElements = Locator.lookup_multipleElements("nav_Breadcrumb_ListOfLinks",
                    null, null);
            ArrayList<String> breadCrumbList = new ArrayList<String>();
            for (WebElement breadcrumbElement : breadCrumbElements) {
                String breadCrumbText;
                if (ClientDataRetrieval.isRedesignClient()) {
                    breadCrumbText = BaseUI.get_Attribute_FromField(breadcrumbElement, "innerText");
                } else {
                    breadCrumbText = BaseUI.getTextFromField(breadcrumbElement);
                }

                if (!BaseUI.stringEmpty(breadCrumbText)) {
                    breadCrumbList.add(BaseUI.getTextFromField(breadcrumbElement).replace("\"", ""));
                }
            }

            BaseUI.verify_true_AndLog(breadCrumbList.size() > 0, "Found breadcrumbs.", "Did NOT find breadcrumbs.");

            if (ClientDataRetrieval.isRedesignClient()) {
                BaseUI.verify_true_AndLog(breadCrumbList.size() == breadcrumbArray.length || breadCrumbList.size() >= breadcrumbArray.length,
                        "Breadcrumbs matched in length.",
                        "Breadcrumbs did NOT match in length.");
            } else {
                BaseUI.verify_true_AndLog(breadCrumbList.size() == breadcrumbArray.length,
                        "Breadcrumbs matched in length.",
                        "Breadcrumbs did NOT match in length.");
            }

            // for (int i = 0; i < breadcrumbArray.length; i++) {
            // BaseUI.baseStringCompare("BreadCrumb", breadcrumbArray[i],
            // breadCrumbList.get(i));
            // }
            for (int i = 0; i < breadcrumbArray.length; i++) {

                if (ClientDataRetrieval.isRedesignClient()) {
                    if (breadcrumbArray[i].contains("eGift Cards") || breadcrumbArray[i].contains("e-Gift Cards")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("-", ""),
                                breadCrumbList.get(i).replace("-", ""));
                    } else if (breadcrumbArray[i].contains("View All")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("View ", ""),
                                breadCrumbList.get(i));
                    } else if (breadcrumbArray[i].startsWith("Under $")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("Under $", "Price: Up to "),
                                breadCrumbList.get(i));
                    } else if (breadcrumbArray[i].startsWith("Under")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("Under", "Points: Up to"),
                                breadCrumbList.get(i));
                    } else if (!(breadcrumbArray[i].startsWith("$")) && (breadcrumbArray[i].endsWith("Above"))) {
//                        String getIntegerValue = breadcrumbArray[i].split("\\&")[0].replace(",", "");
//                        String breadCrumbPart1 = String.valueOf((Double.parseDouble(getIntegerValue) / 100)).replace(".0", ".00");
//                        String breadCrumbPart2 = breadcrumbArray[i].split(" ")[1].replaceAll("&", "and");
//                        BaseUI.baseStringPartialCompare("BreadCrumb", "Points: " + breadCrumbPart1 + " " + breadCrumbPart2,
//                                breadCrumbList.get(i));
                        BaseUI.baseStringPartialCompare("BreadCrumb", "Points: " + breadcrumbArray[i].replaceAll("&", "and"),
                                breadCrumbList.get(i));
                    } else if ((breadcrumbArray[i].contains(" to "))) {
//                        String getIntegerValue1 = breadcrumbArray[i].split(" ")[0].replace(",", "");
//                        String breadCrumbPart1 = String.valueOf((Double.parseDouble(getIntegerValue1) / 100)).replace(".0", ".00");
//                        String getIntegerValue2 = breadcrumbArray[i].split("to ")[1].replace(",", "");
//                        String breadCrumbPart2 = String.valueOf((Double.parseDouble(getIntegerValue2) / 100)).replace(".0", ".00");
//                        BaseUI.baseStringPartialCompare("BreadCrumb", "Points: " + breadCrumbPart1 + " to " + breadCrumbPart2,
//                                breadCrumbList.get(i));
                        BaseUI.baseStringPartialCompare("BreadCrumb", "Points: " + breadcrumbArray[i],
                                breadCrumbList.get(i));
                    } else if ((breadcrumbArray[i].startsWith("$")) && !(breadcrumbArray[i].endsWith("Above"))) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("$", "Value: $"),
                                breadCrumbList.get(i));
                    } else if (breadcrumbArray[i].startsWith("$") && breadcrumbArray[i].endsWith("& Above")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("& Above", "and Above").replace("$", "Price: "),
                                breadCrumbList.get(i));
                    } else if (breadcrumbArray[i].contains("Sports & Outdoors")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replaceAll("&", "and"),
                                breadCrumbList.get(i));
                    } else {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i],
                                breadCrumbList.get(i));
                    }
                } else {
                    if (breadcrumbArray[i].endsWith("...")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replace("...", ""),
                                breadCrumbList.get(i));
                    } else if (breadcrumbArray[i].contains("Sports & Outdoors")) {
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replaceAll("&", "and"),
                                breadCrumbList.get(i));
                    } else {
                        // BaseUI.baseStringCompare("BreadCrumb",breadcrumbArray[i],
                        // breadCrumbList.get(i));
                        BaseUI.baseStringPartialCompare("BreadCrumb", breadcrumbArray[i].replaceAll("\\-", " ").replaceAll("'", "").replaceAll("\\<.*?>", ""),
                                breadCrumbList.get(i).replaceAll("'", "").replaceAll("\\-", " "));
                    }
                }
            }
        }
    }

    public static void navigate_Menu_Submenu(String menu, String subMenu) throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            BaseUI.elementHover(Locator.lookupRequiredElement("nav_SeeAllDepartments_Menu"));

            if (menu == null || menu.equals(subMenu)) {
                BaseUI.waitForElementToBeDisplayed("nav_SeeAll_MenuItem", subMenu, null);
                Thread.sleep(200);
                if (Browser.currentBrowser.equals("internetexplorer")) {
                    BaseUI.click_js(Locator.lookupRequiredElement("nav_SeeAll_MenuItem", subMenu, null));
                } else {
                    BaseUI.click(Locator.lookupRequiredElement("nav_SeeAll_MenuItem", subMenu, null));
                }
            } else {
                BaseUI.waitForElementToBeDisplayed("nav_SubMenuItem", menu, subMenu);
                Thread.sleep(200);
                if (Browser.currentBrowser.equals("internetexplorer")) {
                    BaseUI.click_js(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
                } else {
                    BaseUI.click(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
                }
            }
            Thread.sleep(500);
        } else if (ClientDataRetrieval.clientName.equals("Verizon Wireless-9338")) {

            BaseUI.elementHover(Locator.lookupRequiredElement("nav_MenuItem", menu, null));
            Thread.sleep(200);
            BaseUI.click_js(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
            Thread.sleep(500);
            BaseUI.elementHover(Locator.lookupRequiredElement("nav_HomeLink"));
            Thread.sleep(500);
        } else if (ClientDataRetrieval.isRedesignClient()) {
            if (menu.equals("Sweepstakes")) {
                BaseUI.click(Locator.lookupRequiredElement("nav_Sweepstakes_SweepstakesLinks", subMenu, null));
                Thread.sleep(300);
            } else if (menu.equals("Auctions")) {
                Navigation.navigate_Menu("Auctions");
                BaseUI.click_js(Locator.lookupRequiredElement("nav_Auctions_AuctionsLinks", subMenu, null));
                Thread.sleep(300);
            } else if (menu.equals("Travel")) {
                BaseUI.click(Locator.lookupRequiredElement("nav_Travel_TravelLinks", subMenu, null));
                Thread.sleep(300);
            } else if (menu.equals("Local Deals")) {
                BaseUI.click(Locator.lookupRequiredElement("nav_LocalOffers_LocalOfferLinks", subMenu, null));
                BaseUI.waitForElementToBeDisplayed("localOffer_WaitElement", null, null, 30);
                Thread.sleep(500);
            } else {
                BaseUI.elementHover(Locator.lookupRequiredElement("nav_MenuItem", menu, null));
                BaseUI.waitForElementToBeDisplayed("nav_SubMenuItem", menu, subMenu);
                Thread.sleep(200);
                BaseUI.click(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
                BaseUI.wait_forPageToFinishLoading();
                Thread.sleep(2000);
                if (BaseUI.elementAppears(Locator.lookupOptionalElement("merch_Page_ViewAllOption"))) {
                    String merchCategoryHeaderTitle = BaseUI.getTextFromField(
                            Locator.lookupRequiredElement("merch_merchandise_Category_LandingPage_Header"));
                    if (!(merchCategoryHeaderTitle.contains("Entertainment") || merchCategoryHeaderTitle.contains("Dining")
                            || merchCategoryHeaderTitle.contains("Shopping") || merchCategoryHeaderTitle.contains("Travel")
                            || merchCategoryHeaderTitle.contains("e-Gift Cards") || merchCategoryHeaderTitle.contains("Gift Cards"))) {
                        BaseUI.click(Locator.lookupRequiredElement("merch_Page_ViewAllOption"));
                        Thread.sleep(2000);
                    }
                }
            }
        } else {
            BaseUI.elementHover(Locator.lookupRequiredElement("nav_MenuItem", menu, null));
            Thread.sleep(200);
            BaseUI.click_js(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
            BaseUI.wait_forPageToFinishLoading();
            Thread.sleep(750);
        }
    }

    public static void click_Menu_SubMenuItem(String menu, String subMenu) throws Exception {
        if (!Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.click(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
        } else {
            BaseUI.click_js(Locator.lookupRequiredElement("nav_SubMenuItem", menu, subMenu));
        }
        Thread.sleep(750);
    }

    // This method will click on the menu option instead of hovering and
    // selecting submenu option.
    public static void navigate_Menu(String menuToClickOn) throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            BaseUI.elementHover(Locator.lookupRequiredElement("nav_SeeAllDepartments_Menu"));
           // BaseUI.waitForElementToBeDisplayed("nav_SeeAllDepartments_Menu",null,null,10);
            Thread.sleep(200);
            BaseUI.click_js(Locator.lookupRequiredElement("nav_SeeAll_MenuItem", menuToClickOn, null));
            BaseUI.waitForElementToNOTBeDisplayed("nav_SeeAll_MenuItem",null,null,10);
            Thread.sleep(500);
        } else if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("nav_MenuItem", menuToClickOn, null));
            BaseUI.wait_forPageToFinishLoading();
            BaseUI.waitForElementToBeDisplayed("auctionRows", null, null, 30);
            Thread.sleep(2000);
        } else {
            BaseUI.click(Locator.lookupRequiredElement("nav_MenuItem", menuToClickOn, null));
            BaseUI.wait_forPageToFinishLoading();
            Thread.sleep(2000);
        }
    }

    public static void navigate_Auctions_SmartDollars_OR_Points_USD_And_ExtendedPlay() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
            BaseUI.elementHover(Locator.lookupRequiredElement("nav_SeeAllDepartments_Menu"));
            Thread.sleep(200);
        } else {
            BaseUI.elementHover(Locator.lookupRequiredElement("nav_MenuItem", "Auctions", null));
            Thread.sleep(200);
        }
        BaseUI.click_js(Locator.lookupRequiredElement("nav_Points_USD_ExtendedPlay"));
        Thread.sleep(500);
    }

    public static void navigate_Auctions_Points_Only_And_ExtendedPlay() throws Exception {
        BaseUI.elementHover(Locator.lookupRequiredElement("nav_MenuItem", "Auctions", null));
        Thread.sleep(200);

        BaseUI.click_js(Locator.lookupRequiredElement("nav_PointsOnly_ExtendedPlay"));
        Thread.sleep(500);
    }

    public static void navigate_Auctions_SmartDollars_Only_And_ExtendedPlay() throws Exception {
        BaseUI.elementHover(Locator.lookupRequiredElement("nav_SeeAllDepartments_Menu"));
        Thread.sleep(200);
        BaseUI.click(Locator.lookupRequiredElement("nav_SmartDollars_Only_And_ExtendedPlay"));
        Thread.sleep(500);
    }

    public static void close_AuctionBidPopup_IfAppears() throws Exception {
        if (BaseUI.pageSourceContainsString("Check out what's new in Auctions")) {
            //if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Auctions_BidPopup"))) {
            BaseUI.click(Locator.lookupRequiredElement("nav_Auctions_BidPopup_Close_Button"));
            BaseUI.waitForElementToNOTBeDisplayed("nav_Auctions_BidPopup_Close_Button",null,null,5);
            Thread.sleep(200);
        }
    }

    public static void navigate_Merchandise_DailyDeals() throws Exception {
        navigate_Menu_Submenu("Merchandise", "Daily Deals");
        if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
            BaseUI.click(Locator.lookupRequiredElement("dailyDeals_HeaderTitle"));
            Thread.sleep(200);
        }
    }

    public static void navigate_Merchandise_Tech() throws Exception {
        navigate_Menu_Submenu("Merchandise", "Tech");
        wait_ForMerchandisePage("Tech");
    }

    public static void navigate_Merchandise_MensShop() throws Exception {
        navigate_Menu_Submenu("Merchandise", "Men's Shop");
    }

    public static void navigate_Merchandise_WomensShop() throws Exception {
        navigate_Menu_Submenu("Merchandise", "Women's Shop");
    }

    public static void navigate_Merchandise_HomeAndKitchen() throws Exception {
        String subMenu = "Home & Kitchen";
        navigate_Menu_Submenu("Merchandise", subMenu);
        wait_ForMerchandisePage(subMenu);
    }

    public static void navigate_Merchandise_Jewelry() throws Exception {

        navigate_Menu_Submenu("Merchandise", "Jewelry");
        wait_ForMerchandisePage("Jewelry");
    }

    public static void navigate_Merchandise_Beauty() throws Exception {
        String subMenu = "Beauty";
        navigate_Menu_Submenu("Merchandise", subMenu);
        wait_ForMerchandisePage(subMenu);
    }

    private static void wait_ForMerchandisePage(String merchandiseCategoryText) throws Exception {
        BaseUI.waitForElementToContain_AttributeMatch("merch_merchandise_Category_LandingPage_Header", null, null,
                "innerText", merchandiseCategoryText, 20);
        Thread.sleep(1000);
        if (ClientDataRetrieval.isRedesignClient()) {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("merch_ViewAll_Link"))) {
                BaseUI.click(Locator.lookupRequiredElement("merch_ViewAll_Link"));
                BaseUI.waitForElementToBeDisplayed("merch_resultList_Images", null, null);
                BaseUI.waitForElementToBeDisplayed("merch_ResultsCount_Footer", null, null);
                Thread.sleep(1000);
            }
        }
    }

    public static void navigate_Merchandise_Sunglasses() throws Exception {

        navigate_Menu_Submenu("Merchandise", "Sunglasses");
        wait_ForMerchandisePage("Sunglasses");
    }

    public static void navigate_Merchandise_Magazines() throws Exception {

        navigate_Menu_Submenu("Merchandise", "Magazines");
    }

    public static void navigate_Merchandise_AutoAccessories() throws Exception {

        navigate_Menu_Submenu("Merchandise", "Auto Accessories");
    }

    public static void navigate_GiftCard_Page() throws Exception {
        navigate_Menu_Submenu("Gift Cards", "Gift Cards");
    }

    public static void navigate_GiftCard_All() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards"));
                BaseUI.waitForElementToBeDisplayed("giftCards_Page_header", null, null, 30);
            } else {
                navigate_GiftCards();
                BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards_AllGiftCards_Link"));
                BaseUI.waitForElementToBeDisplayed("giftCards_SearchResult_TopResult_Count", null, null, 30);
            }
        } catch (Exception e) {
            BaseUI.log_Warning("Encountered error clicking All Gift Cards Link.  Attempting to re-click with Javascript.");
            BaseUI.click_js(Locator.lookupRequiredElement("nav_GiftCards_AllGiftCards_Link"));
            BaseUI.waitForElementToBeDisplayed("giftCards_SearchResult_TopResult_Count", null, null, 20);
        }
        Thread.sleep(1000);
    }

    public static void navigate_GiftCard_Dining() throws Exception {
        String categoryToNavigateTo = "Dining";
        navigate_GiftCards();
        BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards_Dining_Link"));
        wait_ForGiftCards_CategoryPage(categoryToNavigateTo);
    }

    public static void navigate_GiftCard_Shopping() throws Exception {
        navigate_GiftCards();
        String categoryToWaitFor = "Shopping";
        BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards_Shopping_Link"));
        wait_ForGiftCards_CategoryPage(categoryToWaitFor);
    }

    public static void navigate_GiftCard_Travel() throws Exception {
        navigate_GiftCards();
        BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards_Travel_Link"));
        BaseUI.waitForElementToBeDisplayed("giftCards_CategoryLandingPage_WaitElement", null, null, 20);
        Thread.sleep(500);
    }

    public static void navigate_GiftCard_Entertaiment() throws Exception {
        navigate_GiftCards();

        String categoryToWaitFor = "Movie Tickets";
        BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards_Entertainment_Link"));
        wait_ForGiftCards_CategoryPage(categoryToWaitFor);
    }

    public static void wait_ForGiftCards_CategoryPage(String categoryToWaitFor) throws Exception {
        BaseUI.waitForElementToBeDisplayed("giftCards_CategoryLandingPage_WaitElement_WithText", categoryToWaitFor, null, 20);
        BaseUI.wait_forPageToFinishLoading();
    }


    public static void navigate_GiftCards() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {

            // BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards"));
            // Thread.sleep(200);
            //
            // BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards_ViewAllGiftCards"));
            // Thread.sleep(2000);
            navigate_Menu("Gift Cards");
        } else {
            BaseUI.click(Locator.lookupRequiredElement("nav_GiftCards"));
            Thread.sleep(2000);
        }
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void navigate_Account() throws Exception {
        if (ClientDataRetrieval.isRedesignClient() && BaseUI.elementAppears(Locator.lookupOptionalElement("nav_AccountDropdown"))) {
            BaseUI.click(Locator.lookupRequiredElement("nav_AccountDropdown"));
            BaseUI.waitForElementToBeDisplayed("nav_AccountDropdown_MyAccountLink", null, null);
            Thread.sleep(300);
            BaseUI.click(Locator.lookupRequiredElement("nav_AccountDropdown_MyAccountLink"));
        } else {
            if (Browser.currentBrowser.equals("internetexplorer")) {
                BaseUI.click_js(Locator.lookupRequiredElement("nav_AccountLink"));
            } else {
                BaseUI.click(Locator.lookupRequiredElement("nav_AccountLink"));
            }
        }
        BaseUI.waitForElementToBeDisplayed("account_HeaderText", null, null, 30);
        BaseUI.waitForElementToBeDisplayed_ButDontFail("accnt_UpdateAddressBookLink", null, null, 30);
        Thread.sleep(500);
    }

    public static void navigate_Account_History() throws Exception {
        if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            navigateToMyRewards();
        }
          else if(ClientDataRetrieval.client_Matches(client_Designation.McAfee)
                ||(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore))) {
            navigate_Account();
        } else {
            navigate_Account();
            if (!BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_activeNavLink")).equals("History")) {
                BaseUI.click(Locator.lookupRequiredElement("accnt_NavLinkByText", "History", null));
                Thread.sleep(2000);
            }
        }
    }

    public static void navigate_Account_UpdateAddressBook() throws Exception {
        if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            navigateToMyRewards();
        } else {
            navigate_Account();
        }
        BaseUI.click(Locator.lookupRequiredElement("accnt_UpdateAddressBookLink"));
        BaseUI.waitForElementToBeDisplayed("accnt_addr_ElementsToWaitFor", null, null);
        Thread.sleep(2500);
    }

    public static void navigate_Account_PaymentOptions() throws Exception {
        if(ClientDataRetrieval.isRedesignClient()) {
            navigateToMyRewards();
        } else {
            navigate_Account();
        }
        BaseUI.click(Locator.lookupRequiredElement("accnt_UpdatePaymentOptions_Link"));
        Thread.sleep(1000);
    }

    public static void navigate_Hotel_Landingpage() throws Exception {
        if (ClientDataRetrieval.isRedesignClient() && BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Travel_ItemListElement"))) {
            navigate_Menu_Submenu("Travel", "Hotels and Condos");
        } else if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("nav_Travel_Link"));
        } else {
            navigate_Menu_Submenu("Travel", "Hotels and Condos");
        }
        BaseUI.waitForElementToBeDisplayed("hotelSearchInput", null, null);
        Thread.sleep(500);
    }

    public static void navigate_Auctions_AllAuctions() throws Exception {
        Navigation.navigate_Menu("Auctions");
        close_AuctionBidPopup_IfAppears();
    }

    public static void navigate_Auctions_CloseAuctionsRedesign() throws Exception {
        Navigation.navigate_Menu("Auctions");
        close_AuctionBidPopup_IfAppears();
        get_RID_OF_STUPID_POOPUP();
        BaseUI.click(Locator.lookupRequiredElement("auctionBid_ClosedAuction_Text"));
        BaseUI.waitForElementToNOTBeDisplayed("auctionBid_ClosedAuction_Text", null, null, 20);
    }

    public static void navigate_MyAccount_CloseAuctionsRedesign() throws Exception {
        if(ClientDataRetrieval.isRedesignClient()) {
            navigateToMyRewards();
        } else {
            navigate_Account();
        }
        BaseUI.click(Locator.lookupRequiredElement("accnt_ClosedAuctions_Link"));
        BaseUI.waitForElementToNOTBeDisplayed("accnt_ClosedAuctions_Link",null,null,30);
        Thread.sleep(1000);
    }

    public static void clickMyAccount_CloseAuctionsRedesign() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("accnt_ClosedAuctions_ButtonSubmit"));
        Thread.sleep(1000);
    }

    public static void navigate_Auctions_MyAuctions() throws Exception {
        String myAuctions = "My Auctions";
//		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
//			myAuctions = "MyAuctions";
//		}
        navigate_Menu_Submenu("Auctions", myAuctions);
    }

    public static void navigate_Account_OpenAuctions() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            navigateToMyRewards();
        } else {
            navigate_Account();
        }
        BaseUI.click(Locator.lookupRequiredElement("accnt_OpenAuctions_Link"));
        BaseUI.waitForElementToBeDisplayed("accnt_OpenAuctions_HeaderText", null, null, 30);
        Thread.sleep(1000);
    }

    public static void navigate_Account_ClosedAuctions() throws Exception {
        if(ClientDataRetrieval.isRedesignClient()) {
            navigateToMyRewards();
        } else {
            navigate_Account();
        }
        BaseUI.click(Locator.lookupRequiredElement("accnt_ClosedAuctions_Link"));
        BaseUI.waitForElementToBeDisplayed("accnt_ClosedAuctions_HeaderText", null, null, 30);
        Thread.sleep(1000);
    }

    public static void navigate_Sweepstakes_MySweepstakes() throws Exception {
        String mySweepstakes = "My Sweepstakes";
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Account_History();
            BaseUI.click(Locator.lookupRequiredElement("mySweepstakes_AccountLink"));
            BaseUI.waitForElementToBeDisplayed("mySweepstakes_Header", null, null);
            Thread.sleep(500);
        } else {
            navigate_Menu_Submenu("Sweepstakes", mySweepstakes);
            BaseUI.waitForElementToBeDisplayed("mySweepstakes_CurrentSweepstakesTab", null, null);
            Thread.sleep(500);
        }
    }

    public static void navigate_CarRental_Page() throws Exception {

        if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            Navigation.navigate_Home_viaHomeLink();
            if(BaseUI.elementAppears(Locator.lookupRequiredElement("Shp_ItemsInCart"))) {
                BaseUI.click(Locator.lookupRequiredElement("nav_Travel_Link"));
                BaseUI.waitForElementToBeDisplayed("travelLandingPageHeader", null, null, 20);
                Thread.sleep(300);
                BaseUI.click(Locator.lookupRequiredElement("carRental_MenuTab_Text"));
                Thread.sleep(300);
            } else {
                BaseUI.click(Locator.lookupRequiredElement("carRental_LeftNavigationLink"));
                Thread.sleep(300);
            }
            BaseUI.waitForElementToBeDisplayed("carRental_Header", null, null, 20);
        } else {
            navigate_Menu_Submenu("Travel", "Car Rental");
            BaseUI.waitForElementToBeDisplayed("carRental_HeaderText", null, null, 20);
            Thread.sleep(1000);
        }
    }

    public static void navigate_LocalOffers_SearchPage() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            navigate_Menu_Submenu("Local Deals", "Local Deals");
        } else {
            navigate_Menu_Submenu("Local Offers", "Local Offers");
        }

        if (BaseUI.elementExists("localOffer_change_locationBtn", null, null)) {
            BaseUI.click(Locator.lookupRequiredElement("localOffer_change_locationBtn"));
            BaseUI.waitForElementToBeDisplayed("localOffer_ChangeLocation_EnterNewLocTitle", null, null, 20);
            Thread.sleep(1000);
        }
    }

    public static void enter_PromoCode(String codeToEnter) throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
            ||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))) {
            BaseUI.click(Locator.lookupRequiredElement("nav_PromoCodeLink"));
            BaseUI.wait_forPageToFinishLoading();
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("enterPromoCode_Text"), codeToEnter);
            Thread.sleep(200);
        } else {
            BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("nav_PromoCode_TextBox"), codeToEnter);
            Thread.sleep(200);
        }
        click_PromoCodeSubmitButton();
    }

    // Click Enter Promo Code Submit button on Home Page
    public static void click_PromoCodeSubmitButton() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_SubmitButton"));
        BaseUI.waitForElementToBeDisplayed("nav_PromoCode_Message", null, null);
        Thread.sleep(500);
    }

    public static void click_Closed_PromoCodeCloseErrorModal() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_Error_CloseModal"));
        BaseUI.waitForElementToNOTBeDisplayed("nav_PromoCode_Error_CloseModal", null, null);
        Thread.sleep(500);
    }

    // Use this method to validate promotional codes that don't exist in promo
    // system.
    public static void validate_PromoCode_NotACode(String invalidPromoCode) throws Exception {

        enter_PromoCode(invalidPromoCode); //try running again

        if (ClientDataRetrieval.isRedesignClient()) {

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Error_Title"));

            String expectedErrorMessage = MessageFormat.format(
                    "Oops. The Promo Code ({0}) that you have entered is either not valid or has expired.", invalidPromoCode);
            String promoErrorText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

            BaseUI.baseStringCompare("Promo Message", expectedErrorMessage, promoErrorText);
            click_Closed_PromoCodeCloseErrorModal();
        } else {

            enter_PromoCode(invalidPromoCode);

            String promoHeaderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Error_Title"));
            BaseUI.baseStringCompare("Promo Message Header", "Oops!", promoHeaderText);

            String expectedErrorMessage = MessageFormat.format(
                    "The Promo Code ({0}) that you have entered is either not valid or has expired.", invalidPromoCode);
            String promoErrorText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

            BaseUI.baseStringCompare("Promo Message", expectedErrorMessage, promoErrorText);
        }
    }

    // Use this method to validate promotional codes that don't exist in promo
    // system.
    public static void validate_PromoCode_CannotBeUsedTwice(String invalidPromoCode) throws Exception {
        enter_PromoCode(invalidPromoCode);

        if (ClientDataRetrieval.isRedesignClient()) {

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Error_Title"));

            String expectedErrorMessage = MessageFormat.format(
                    "Oops. The Promo Code ({0}) that you have entered is either not valid or has expired.", invalidPromoCode);
            String promoErrorText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

            BaseUI.baseStringCompare("Promo Message", expectedErrorMessage, promoErrorText);
            click_Closed_PromoCodeCloseErrorModal();
        } else {

            String promoHeaderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Error_Title"));
            BaseUI.baseStringCompare("Promo Message Header", "Oops!", promoHeaderText);

            String expectedErrorMessage = MessageFormat.format("This Promo Code ({0}) has already been used.",
                    invalidPromoCode);
            String promoErrorText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

            BaseUI.baseStringCompare("Promo Message", expectedErrorMessage, promoErrorText);
        }
    }

    public static void get_RID_OF_STUPID_POOPUP() throws Exception {
        // if (!Browser.currentBrowser.equals("internetexplorer")) {
//		if (ClientDataRetrieval.client_Matches(client_Designation.AARP) && Browser.currentBrowser.equals("chrome")) {
//			String id = "";
//			final List<WebElement> iframes = Browser.driver.findElements(By.tagName("iframe"));
//
//			for (Integer i = 0; i < iframes.size(); i++) {
//
//				try {
//					Browser.driver.switchTo().frame(i);
//					if (BaseUI.pageSourceContainsString("dc_btnClose")) {
//						BaseUI.click_js(Locator.lookupRequiredElement("loginPage_popupXButton"));
//						Thread.sleep(1000);
//						break;
//					}
//				} catch (Exception e) {
//					Reporter.log("Exception occurred while trying to deal with advertisement.");
//					System.out.println("Exception occurred while trying to deal with advertisement.");
//					break;
//				}
//
//			}
//			Browser.driver.switchTo().defaultContent();
//			Thread.sleep(200);
//		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
//			Thread.sleep(12000);
//		}
    }

    public static void validate_PromoWindow_Closes(String promoCode) throws Exception {
        enter_PromoCode(promoCode);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_CloseButton"));
        close_PromoWindow();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("nav_PromoCode_CloseButton"));
    }

    public static void validate_PromoCode_Valid(String promocode, String rewardAmount, String promodescription)
            throws Exception {

        // Before we enter promocode we have to create logic to get the current
        // points.
        // Then afterwards we'll verify that the new rewards match.
        String amountOfPoints_Before = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        amountOfPoints_Before = amountOfPoints_Before.replace(",", "");
        amountOfPoints_Before = amountOfPoints_Before.replace("pts", "");

        enter_PromoCode(promocode);

        String promoHeaderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_PromoBox_Title"));
        BaseUI.baseStringCompare("Promo Message Header", "Congratulations!", promoHeaderText);

        String expectedMessage = "";
        String promoText = "";
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
            Double amountAsDouble = Double.parseDouble(rewardAmount);
            rewardAmount = amountAsDouble.toString();
            if (rewardAmount.substring(rewardAmount.indexOf("."), rewardAmount.length()).length() == 2) {
                rewardAmount += "0";
            }

            expectedMessage = MessageFormat.format("{0} Smart Dollars has been added to your account", rewardAmount);
        } else {

            expectedMessage = MessageFormat.format("{0} Points has been added to your account", rewardAmount);
        }
        if (ClientDataRetrieval.isRedesignClient()) {

            expectedMessage = MessageFormat.format("{0} Points have been added to your account.", rewardAmount);
            promoText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_MessageText"));
            BaseUI.baseStringCompare("Promo Message", expectedMessage, promoText);
            BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_UseMyPoint"));
            BaseUI.waitForElementToNOTBeDisplayed("nav_PromoCode_UseMyPoint", null, null);
            Thread.sleep(500);
        } else {
            promoText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

            BaseUI.baseStringCompare("Promo Message", expectedMessage, promoText);
        }

        //if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {

        //	BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_UseMyPoint"));
        //}
        Navigation.navigate_Account_History();

        String amountOfPoints_After = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        amountOfPoints_After = amountOfPoints_After.replace(",", "");
        amountOfPoints_After = amountOfPoints_After.replace("pts", "");

        // Compare the before points and make sure the after points reflect the
        // change in the rewardAmount
        Double pointsBefore = Double.parseDouble(amountOfPoints_Before);
        pointsBefore = pointsBefore + Double.parseDouble(rewardAmount);

        BaseUI.verify_true_AndLog(pointsBefore == Double.parseDouble(amountOfPoints_After),
                "Points were accurately updated.", "Points were NOT accurately updated.");
        Account_History.Verify_PromoCode(promocode, rewardAmount, promodescription);
    }

    public static void validate_PromoCode_Valid_SweepsTakes(String promocode, String rewardAmount,
                                                            String promodescription) throws Exception {

        enter_PromoCode(promocode);
        String promoHeaderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_PromoBox_Title"));
        BaseUI.baseStringCompare("Promo Message Header", "Congratulations!", promoHeaderText);

        String promoText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));
        BaseUI.baseStringCompare("Promo sweepstakes Msg", "You have received 5 ticket(s) in the autotestDAF.",
                promoText);

        //
        // Navigation.navigate_Account_History();
        //
        // String amountOfPoints_After =
        // BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        // amountOfPoints_After = amountOfPoints_After.replace(",", "");

        // Compare the before points and make sure the after points reflect the
        // change in the rewardAmount
        // Double pointsBefore = Double.parseDouble(amountOfPoints_Before);
        // pointsBefore = pointsBefore + Double.parseDouble(rewardAmount);

        // BaseUI.verify_true_AndLog(pointsBefore ==
        // Double.parseDouble(amountOfPoints_After),
        // "Points were accurately updated.", "Points were NOT accurately
        // updated.");
        // Account_History.Verify_PromoCode(promocode, rewardAmount,
        // promodescription);

    }

    // Applies only if multiple offers
    public static void click_PromoCode_RightNavigationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_RightNavigationArrow"));
        Thread.sleep(300);
    }

    // Applies only if multiple offers
    public static void click_PromoCode_LeftNavigationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_LeftNavigationArrow"));
        Thread.sleep(300);
    }

    // Applies only if multiple offers
    public static ArrayList<String> return_PromoCode_OfferList() {
        ArrayList<WebElement> promoCodeOfferList = Locator.lookup_multipleElements("nav_PromoCode_OfferList", null,
                null);

        ArrayList<String> offerList = new ArrayList<String>();

        for (WebElement offer : promoCodeOfferList) {
            String offerStr = BaseUI.getTextFromField(offer);
            offerList.add(offerStr);
        }

        return offerList;
    }

    public static void click_PromoCode_SelectButton() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_Select_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("nav_PromoCode_Select_Button", null, null);
        Thread.sleep(1000);
    }

    public static void clickAndVerify_NavigatePromoCode_OfferCarousel_ToFarRight() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("nav_PromoCode_LastItemDisplayed"))) {
            click_PromoCode_RightNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_LastItemDisplayed"));
    }

    public static void clickAndVerify_NavigatePromoCode_OfferCarousel_ToFarLeft() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("nav_PromoCode_FirstItemDisplayed"))) {
            click_PromoCode_LeftNavigationArrow();
            count++;
            if (count > 10) {
                break;
            }
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_FirstItemDisplayed"));
    }

    public static void click_PromoCode_ViewDetails_FirstItem() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_ViewDetails_FirstItem"));
        Thread.sleep(1500);
    }

    public static String get_PromoCodeOfferText_ByIndexAsString(String offerNumber) {
        WebElement offerName = Locator.lookupRequiredElement("nav_PromoCode_OfferList_ByNumber", offerNumber, null);
        String offerText = BaseUI.getTextFromField(offerName);
        offerText = offerText.replaceAll("\\s+ ", " ");
        return offerText;
    }

    public static void click_PromoCode_ViewDetails_ByOfferName(String offerName) throws Exception {
        ArrayList<String> offerList = return_PromoCode_OfferList();

        for (int i = 0; i <= offerList.size() - 1; i++) {
            if (offerList.get(i).contains(offerName)) {
                BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_ViewDetails_ByOfferText", offerName, null));
                Thread.sleep(1000);
            }
        }
    }

    public static void verify_PromoCode_ForRewardsOrPoints(String offername, String promocode, String rewardAmount,
                                                           String promodescription) throws Exception {
        String amountOfPoints_Before = returnPoints();

        click_PromoCode_ViewDetails_ByOfferName(ClientDataRetrieval.client_Currency);
        click_PromoCode_SelectButton();

        String promoHeaderText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_OfferSuccessful_Text"));
        BaseUI.baseStringCompare("Promo Message Header", "Congratulations!", promoHeaderText);

        String expectedMessage = "";
        String promoText = "";
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
            Double amountAsDouble = Double.parseDouble(rewardAmount);
            rewardAmount = amountAsDouble.toString();
            if (rewardAmount.substring(rewardAmount.indexOf("."), rewardAmount.length()).length() == 2) {
                rewardAmount += "0";
            }

            expectedMessage = MessageFormat.format(
                    "{0} " + ClientDataRetrieval.client_Currency + " have been applied to your account.", rewardAmount);
        } else {

            expectedMessage = MessageFormat.format(
                    "{0} " + ClientDataRetrieval.client_Currency + " have been applied to your account.", rewardAmount);
        }

        promoText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

        BaseUI.baseStringCompare("Promo Message", expectedMessage, promoText);

        Navigation.navigate_Account_History();

        String amountOfPoints_After = returnPoints();

        Double pointsBefore = Double.parseDouble(amountOfPoints_Before);
        pointsBefore = pointsBefore + Double.parseDouble(rewardAmount);

        BaseUI.verify_true_AndLog(pointsBefore == Double.parseDouble(amountOfPoints_After),
                "Points were accurately updated.", "Points were NOT accurately updated.");
        Account_History.Verify_PromoCode(promocode, rewardAmount, promodescription);
    }

    public static TableData return_PromoCode_FreeProducts_TableData() {
        HashMap<String, String> tableMappings = new HashMap<String, String>();
        tableMappings.put("retailPrice", ".//dd[@class='retail']");
        tableMappings.put("youPay", ".//dd[@class='price']");
        tableMappings.put("points", ".//dd[@class='incentives']");
        // tableMappings.put("savings", ".//dd[@class='savings']");

        TableData promoCodeTable = new TableData();
        promoCodeTable.data = BaseUI.tableExtractor("nav_PromoCode_RowList", tableMappings);
        promoCodeTable.remove_Character("$", "youPay");
        promoCodeTable.remove_Character(",", "youPay");
        promoCodeTable.remove_Character(",", "points");
        promoCodeTable.remove_Character("\u2013 ", "points");
        promoCodeTable.remove_Character("$", "retailPrice");
        promoCodeTable.remove_Character(",", "retailPrice");

        return promoCodeTable;
    }

    public static void verify_PromoCode_FreeProducts() throws Exception {


        click_PromoCode_ViewDetails_FirstItem();

        TableData promoCodeTable = return_PromoCode_FreeProducts_TableData();
        BaseUI.verify_true_AndLog(
                BaseUI.string_isNumeric(promoCodeTable.data.get(0).get("retailPrice"))
                        && BaseUI.string_IsDouble(promoCodeTable.data.get(0).get("retailPrice"))
                        && Double.parseDouble(promoCodeTable.data.get(0).get("retailPrice")) >= 0.0,
                "RetailPrice is Numeric, double and greater than 0.00.",
                "RetailPrice is NOT Numeric, double and greater than 0.00.");
        BaseUI.verify_true_AndLog(
                BaseUI.string_isNumeric(promoCodeTable.data.get(0).get("points"))
                        && Double.parseDouble(promoCodeTable.data.get(0).get("points")) >= 0.0,
                ClientDataRetrieval.client_Currency + " is Numeric and greater than 0.00.",
                ClientDataRetrieval.client_Currency + " is NOT Numeric and greater than 0.00.");
        // BaseUI.verify_true_AndLog(
        // BaseUI.string_isNumeric(promoCodeTable.data.get(0).get("savings"))
        // && Double.parseDouble(promoCodeTable.data.get(0).get("savings")) >=
        // 0.0,
        // "Savings is Numeric and greater than 0.00.",
        // "Savings is NOT Numeric and greater than 0.00.");
        BaseUI.verify_true_AndLog(
                BaseUI.string_isNumeric(promoCodeTable.data.get(0).get("youPay"))
                        && BaseUI.string_IsDouble(promoCodeTable.data.get(0).get("youPay"))
                        && Double.parseDouble(promoCodeTable.data.get(0).get("youPay")) == 0.0,
                "You Pay is Numeric, double and equal to 0.00.", "You Pay is NOT Numeric, double and equal to 0.00.");

    }

    public static void verify_PromoCode_FreeProduct_ClickSelect_MessageIsCorrect(String firstSelectedItemText) throws Exception {
        String amountOfPoints_Before = returnPoints();

        click_PromoCode_SelectButton();

        // Skipping this validation for both All State and AARP as text does not
        // appear when running the script
        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {

        } else {
            String promoHeaderText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_OfferSuccessful_Text"));
            BaseUI.baseStringCompare("Promo Message Header", "Congratulations!", promoHeaderText);
        }

        String expectedMessage = MessageFormat
                .format("You have received an item (" + "{0}" + ") in your shopping cart.", firstSelectedItemText);

        String promoText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

        BaseUI.baseStringCompare("Promo Message", expectedMessage, promoText);

        String amountOfPoints_After = returnPoints();

        Double pointsBefore = Double.parseDouble(amountOfPoints_Before);
        BaseUI.verify_true_AndLog(pointsBefore == Double.parseDouble(amountOfPoints_After), "Points were Not updated.",
                "Points were updated.");
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_ContinueShopping_Button"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Checkout_Button"));
    }

    public static TableData return_PromoCode_DiscountProducts_TableData() {
        HashMap<String, String> tableMappings = new HashMap<String, String>();
        tableMappings.put("retailPrice", ".//dd[@class='retail']");
        tableMappings.put("youPay", ".//dd[@class='price']");
        tableMappings.put("points", ".//dd[@class='incentives']");
        tableMappings.put("savings", ".//dd[@class='savings']");

        TableData promoCodeTable = new TableData();
        promoCodeTable.data = BaseUI.tableExtractor("nav_PromoCode_RowList", tableMappings);
        promoCodeTable.remove_Character("$", "youPay");
        promoCodeTable.remove_Character(",", "youPay");
        promoCodeTable.remove_Character(",", "points");
        promoCodeTable.remove_Character("\u2013", "points");
        promoCodeTable.trim_column("points");
        promoCodeTable.remove_Character("$", "retailPrice");
        promoCodeTable.remove_Character(",", "retailPrice");
        promoCodeTable.remove_Character(",", "savings");

        return promoCodeTable;
    }

    public static void verify_PromoCode_DiscountProducts_ViewDetails(HashMap<String, String> promoCodeTable)
            throws Exception {

        // String amountOfPoints_Before = returnPoints();

        BaseUI.verify_true_AndLog(
                BaseUI.string_isNumeric(promoCodeTable.get("retailPrice"))
                        && BaseUI.string_IsDouble(promoCodeTable.get("retailPrice"))
                        && Double.parseDouble(promoCodeTable.get("retailPrice")) >= 0.0,
                "RetailPrice is Numeric, double and greater than 0.00.",
                "RetailPrice is NOT Numeric, double and greater than 0.00.");

        boolean validCurrency_Points = false;
        if (ClientDataRetrieval.client_Currency.equals("Points")) {
            validCurrency_Points = BaseUI.string_isNumeric(promoCodeTable.get("points")) && (Integer.parseInt(promoCodeTable.get("points")) >= 0);

        } else {
            validCurrency_Points = BaseUI.string_isNumeric(promoCodeTable.get("points"))
                    && Double.parseDouble(promoCodeTable.get("points")) >= 0.0;
        }


        BaseUI.verify_true_AndLog(validCurrency_Points,
                ClientDataRetrieval.client_Currency + " is Numeric and greater than 0.00.",
                ClientDataRetrieval.client_Currency + " is NOT Numeric and greater than 0.00.  Seeing " + promoCodeTable.get("points"));
        BaseUI.verify_true_AndLog(
                BaseUI.string_isNumeric(promoCodeTable.get("savings"))
                        && Double.parseDouble(promoCodeTable.get("savings")) >= 0.0,
                "Savings is Double and greater than 0.00.", "Savings is NOT Double and greater than 0.00.  Seeing " + promoCodeTable.get("savings"));
        BaseUI.verify_true_AndLog(
                BaseUI.string_isNumeric(promoCodeTable.get("youPay"))
                        && BaseUI.string_IsDouble(promoCodeTable.get("youPay"))
                        && Double.parseDouble(promoCodeTable.get("youPay")) > 0.0,
                "You Pay is Numeric, double and greater than 0.00.",
                "You Pay is NOT Numeric, double and greater than 0.00.");
    }

    public static void click_SelectAndVerify_PromoCode_DiscountProduct(String firstSelectedText) throws Exception {

        String amountOfPoints_Before = returnPoints();
        click_PromoCode_SelectButton();

        // Skipping this validation for both All State and AARP as text does not
        // appear when running the script
        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {

        } else {
            String promoHeaderText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_OfferSuccessful_Text"));
            BaseUI.baseStringCompare("Promo Message Header", "Congratulations!", promoHeaderText);
        }

        String expectedMessage = MessageFormat
                .format("You have received an item (" + "{0}" + ") in your shopping cart.", firstSelectedText);

        String promoText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_Message"));

        BaseUI.baseStringCompare("Promo Message", expectedMessage, promoText);

        String amountOfPoints_After = returnPoints();

        Double pointsBefore = Double.parseDouble(amountOfPoints_Before);

        BaseUI.verify_true_AndLog(pointsBefore == Double.parseDouble(amountOfPoints_After), "Points were Not updated.",
                "Points were updated.");
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_ContinueShopping_Button"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Checkout_Button"));
    }

    public static void click_Promocode_Checkout_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_PromoCode_Checkout_Button"));
        BaseUI.waitForElementToBeDisplayed("shpCart_Page_Header", null, null, 30);
        Thread.sleep(1500);
    }

    // Citi doesn't have points.
    public static String returnPoints() {
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            String points = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_AmountOfPoints"));
            points = points.replace(",", "");
            BaseUI.verify_false_AndLog(BaseUI.stringEmpty(points), MessageFormat.format("Found {0} Points.", points),
                    "Did NOT find points.");
            return points;
        }
        return "";
    }

    public static String returnAllState_Cash() {
        String cash = "";
        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            cash = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_AmountOfCash"));
            cash = cash.replace(",", "").replace("$", "");
            BaseUI.verify_false_AndLog(BaseUI.stringEmpty(cash),
                    MessageFormat.format("Found {0} AllState Rewards Cash.", cash),
                    "Did NOT find AllState Rewards Cash.");
        }

        return cash;
    }

    // Returns the points we should be seeing after a points change.
    public static String return_Expected_AllStateCash_AsString(String cashBefore, String cashChange) {
        String pointsAfter = "";

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            Double expectedPoints = Double.parseDouble(cashBefore) - Double.parseDouble(cashChange);
            String expectedPointsStr = expectedPoints.toString();
            // Fix for formatting issue after converting Double to String to
            // convert .0 to .00
            expectedPointsStr = expectedPointsStr.split("\\.")[1].length() == 1 ? expectedPointsStr + "0"
                    : expectedPointsStr;
            pointsAfter = expectedPointsStr;
        }
        return pointsAfter;
    }

    public static void verify_pointsMatchExpected(String expectedPoints) throws Exception {
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            String textToInject = "Automation - Refreshing Page";
            BaseUI.inject_TextNode_IntoElement(Locator.lookupRequiredElement("nav_AmountOfPoints"), textToInject);
            Browser.driver.navigate().refresh();
            Thread.sleep(1000);
            BaseUI.waitForElementToNOTContain_PartialAttributeMatch("nav_AmountOfPoints", null, null, "innerText", textToInject, 30);
            BaseUI.wait_forPageToFinishLoading();
            String visiblePoints = returnPoints();
            // visiblePoints = visiblePoints.replace(",", "");
            BaseUI.baseStringCompare("Points", expectedPoints, visiblePoints);
        }
    }

    public static void verify_AllStateCash_MatchesExpected(String expectedPoints) {
        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            String visiblePoints = returnAllState_Cash();
            visiblePoints = visiblePoints.replace(",", "").replace("$", "");
            BaseUI.baseStringCompare("AllState Cash", expectedPoints, visiblePoints);
        }
    }

    // Returns the points we should be seeing after a points change.
    public static String return_ExpectedPoints_AsString(String pointsBefore, String pointsChange) {
        String pointsAfter = "";

        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            Double expectedPoints = Double.parseDouble(pointsBefore) - Double.parseDouble(pointsChange);
            String expectedPointsStr = expectedPoints.toString();
            // Fix for formatting issue after converting Double to String to
            // convert .0 to .00
            expectedPointsStr = expectedPointsStr.split("\\.")[1].length() == 1 ? expectedPointsStr + "0"
                    : expectedPointsStr;
            pointsAfter = expectedPointsStr;
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
        } else {
            pointsBefore = pointsBefore.split("\\.")[0];
            pointsBefore = pointsBefore.replace(",", "");
            pointsChange = pointsChange.split("\\.")[0];
            pointsChange = pointsChange.replace(",", "");
            Integer expectedPoints = Integer.parseInt(pointsBefore) - Integer.parseInt(pointsChange);
            pointsAfter = expectedPoints.toString();
        }

        return pointsAfter;
    }

    public static void close_PromoWindow() throws Exception {
        WebElement promoCloseButton = Locator.lookupRequiredElement("nav_PromoCode_CloseButton");
        if (BaseUI.elementAppears(promoCloseButton)) {
            BaseUI.click(promoCloseButton);
            Thread.sleep(500);
        }
    }

    public static void verify_PointsAndCashRewards_Appear() {
        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfCash"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        }
    }

    public static void verify_PointsAndCashRewards_Text_Appears() throws InterruptedException {
        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = pointsText.getText().split(":")[0];
            String expectedPointText = "Smart Dollars to Spend";
            BaseUI.baseStringCompare("Points", expectedPointText, actualPointText);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = pointsText.getText().split(":")[0];
            String expectedPointText = "Points to Spend";
            BaseUI.baseStringCompare("Points", expectedPointText, actualPointText);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = pointsText.getText().split(":")[0];
            String expectedPointText = "Rewards Dollars to Spend";
            BaseUI.baseStringCompare("Points", expectedPointText, actualPointText);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = pointsText.getText().split(":")[0];
            String expectedPointText = "Savings Dollars to Spend";
            BaseUI.baseStringCompare("Points", expectedPointText, actualPointText);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = BaseUI.getTextFromField(pointsText);
            String expectedPointText = "Points";
            BaseUI.baseStringCompare("Points", expectedPointText, actualPointText);

            WebElement cashText = Locator.lookupRequiredElement("nav_AmountOfCash_Text");
            String actualCashText = BaseUI.getTextFromField(cashText);
            String expectedCashText = "Cash Rewards";
            BaseUI.baseStringCompare("Points", expectedCashText, actualCashText);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = BaseUI.getTextFromField(pointsText);
            String expectedPointText = "pts";
            BaseUI.baseStringPartialCompare("Points", expectedPointText, actualPointText);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        } else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
            ||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))) {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = BaseUI.getTextFromField(pointsText);
            String expectedPointText = "Points";
            BaseUI.baseStringPartialCompare("Points", expectedPointText, actualPointText);
        } else {
            WebElement pointsText = Locator.lookupRequiredElement("nav_AmountOfPoints_Text");
            String actualPointText = BaseUI.getTextFromField(pointsText);
            String expectedPointText = "Points";
            BaseUI.baseStringCompare("Points", expectedPointText, actualPointText);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AmountOfPoints"));
        }
    }

    public static void clickMessageLink() throws Exception {
        WebElement messageLink = Locator.lookupRequiredElement("nav_Message_Link");
        if (BaseUI.elementAppears(messageLink)) {
            BaseUI.click(messageLink);
            Thread.sleep(500);
            BaseUI.waitForElementToBeDisplayed("nav_Message_RemindMeLater_Link", null, null, 10);
        } else {
            BaseUI.log_AndFail("Message link does not appear on Home page!");
        }
    }

    public static void verify_MessageLink() throws Exception {
        clickMessageLink();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("messages_DontShowAgainLink"));
    }

    public static void clickRemindMeLaterLinkOnMessageDialog() throws Exception {
        clickMessageLink();
        BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
        Thread.sleep(1000);

        BaseUI.waitForElementToNOTBeDisplayed("nav_Message_RemindMeLater_Link", null, null);
    }

    public static void click_SeeLocalOffer_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_LocalOffer_SeeLocalOffer_Button"));
        Thread.sleep(2000);
    }

    public static void click_SeeLocalOffer_SearchPageButton() throws Exception {

//		if (!ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
//				||(!ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)));
//			//BaseUI.elementExists("localOffer_SearchOffersBtn", null, null);	
//	    else
        if (BaseUI.elementExists("localOffer_SearchOffersBtn", null, null)) {
            BaseUI.click_js(Locator.lookupRequiredElement("localOffer_SearchOffersBtn"));
            Thread.sleep(1000);
            BaseUI.waitForElementToBeDisplayed("localOffer_SearchLocalOffer_WaitElement", null, null, 20);
        }
    }

    public static void nav_Sweepstakes_LandingPage() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_Sweepstakes_LandingPage"));
        BaseUI.waitForElementToBeDisplayed_ButDontFail("sweepstakes_FirstEntryCount", null, null, 30);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void navigate_Sweepstakes_UpcomingSweepstakes() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            nav_Sweepstakes_LandingPage();
            Sweepstakes.clickUpcomingSweepstakesHomePage();
        } else {
            navigate_Menu_Submenu("Sweepstakes", "Upcoming Sweepstakes");
            Sweepstakes_Upcoming.waitForPageToLoad();
        }
    }


    public static void navigate_SweepstakesLink_currentSweepstakes() throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
                || (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
            BaseUI.click(Locator.lookupRequiredElement("nav_SweepstakesLink"));
            Thread.sleep(500);
            BaseUI.click(Locator.lookupRequiredElement("nav_CurrentSweepstakesLink"));
            Thread.sleep(500);
            ;
        } else if (ClientDataRetrieval.isRedesignClient()) {
            nav_Sweepstakes_LandingPage();
        } else {
            navigate_Sweepstakes_CurrentSweepstakesLink();
        }
    }

    public static void navigate_MerchandisePage_AndOpen_LPG_Modal() throws Exception {
        navigate_Merchandise();
        open_LPG_Modal();
    }

    public static void open_LPG_Modal() throws Exception {
        if(ClientDataRetrieval.isLegacyClient()) {
            BaseUI.scroll_to_element(Locator.lookupRequiredElement("merch_LPG_SiteMedia"));
            BaseUI.click(Locator.lookupRequiredElement("merch_LPG_SiteMedia"));
            BaseUI.waitForElementToBeDisplayed("nav_LPG_ElementToWaitFor", null, null);
            Thread.sleep(500);
        }
    }


    public static void Navigate_LPG_LandingPage() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("merch_LPG_SiteMedia"));
        BaseUI.waitForElementToBeDisplayed("nav_LPG_ElementToWaitFor", null, null);
        Thread.sleep(500);
        BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
        BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
        Thread.sleep(500);
    }


    public static void Navigate_Merchandise_TravelLPG() throws Exception {
        Navigation.navigate_Merchandise();
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("merch_LPG_SiteMedia"));
        BaseUI.click(Locator.lookupRequiredElement("merch_LPG_SiteMedia"));
        BaseUI.waitForElementToBeDisplayed("nav_LPG_ElementToWaitFor", null, null);
        Thread.sleep(500);
        if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){
            BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
            BaseUI.waitForElementToBeDisplayed("lpgPage_LowPriceGuaranteeBanner", null, null);
        }else{
            BaseUI.click(Locator.lookupRequiredElement("lpgModal_TravelTab"));
            Thread.sleep(500);
            BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
            BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
        }
        Thread.sleep(500);
    }

    public static void navigate_Sweepstakes_CurrentSweepstakesLink() throws Exception {
        navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
        Thread.sleep(1000);
    }

    public static Object[][] returnLinkTextAs_ObjectArray(ArrayList<String> dataSet) {
        BaseUI.verify_true_AndLog(dataSet.size() > 0, "Found links", "Did NOT find links.");

        Object[][] linksToNavigate = new Object[dataSet.size()][1];
        int objectIndex = 0;
        for (String linkItem : dataSet) {
            linksToNavigate[objectIndex][0] = linkItem;
            objectIndex++;
        }

        return linksToNavigate;
    }

    public static void navigateToMyRewards() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("nav_MyRewards"));
        BaseUI.waitForElementToBeDisplayed("acct_SubHeader_Text", null, null, 30);
        Thread.sleep(1000);
    }

    public static void refreshBrowser() throws Exception {
        Browser.driver.navigate().refresh();
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("nav_ShoppingCartLink", null, null, 30);
    }

    public static void navigate_MyAccount_ClosedSweepstakes() throws Exception {
        navigate_Account();
        BaseUI.click(Locator.lookupRequiredElement("accnt_ClosedSweepstakes_Link"));
        BaseUI.waitForElementToNOTBeDisplayed("accnt_ClosedSweepstakes_Link", null, null, 30);
        Thread.sleep(1000);
    }

    //This method is only for AARPRedesign
    public static void verifyAd_AtTopOfPage() throws Exception {
        BaseUI.switch_ToIframe(0);
        Thread.sleep(2000);
        if(!BaseUI.elementAppears(Locator.lookupOptionalElement("eachMenuTab_LandingPage_TopAd"))
                && !BaseUI.elementAppears(Locator.lookupOptionalElement("eachMenuTab_LandingPage_TopAd_InsideFrame1"))) {
            if(BaseUI.elementAppears(Locator.lookupOptionalElement("merchLandingPage_Ads_Iframe"))) {
                BaseUI.switch_ToIframe(Locator.lookupRequiredElement("merchLandingPage_Ads_Iframe"));
            } else {
                BaseUI.switch_ToIframe(1);
            }
            Thread.sleep(2000);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_TopFooterAd_InsideSecondIframe"));
        } else if (!BaseUI.elementAppears(Locator.lookupOptionalElement("eachMenuTab_LandingPage_TopAd"))) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("eachMenuTab_LandingPage_TopAd_InsideFrame1"));
        } else {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("eachMenuTab_LandingPage_TopAd"));
        }
    }

    public static void validate_PromoWindow_Closes_WhenBrowserRefresh(String promoCode) throws Exception {
        enter_PromoCode(promoCode);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_CloseButton"));
        Navigation.refreshBrowser();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("nav_PromoCode_CloseButton"));
    }
}