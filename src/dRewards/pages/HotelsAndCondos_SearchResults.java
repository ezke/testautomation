package dRewards.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.DatePicker;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class HotelsAndCondos_SearchResults {

	public static void verify_ErrorNotPresent(){
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("hotelSearchResult_ErrorMessage"));
	}


	public static void validate_SearchResults(String address, String fromDate, String toDate, String adultCount,
			String childCount, String childAge) throws Exception {
		verify_checkin_And_checkout_Dates(fromDate, toDate);
		verify_Adults_AndChildren_Dropdowns(adultCount, childCount, childAge);
		BaseUI.verifyElementHasExpectedText("hotelSearchResult_AddressTitle", address);
		// verifies the google map is present.
		//Turned off for XUAT environment per conversation with Jon.
		if(!ClientDataRetrieval.environment.equals("XUAT")) {
			verifyMap_And_ResultList_Appear();
		}

		ArrayList<String> mapLinks = return_MapLinks();
		ArrayList<String> hotelList = return_HotelLinksList();

		// if (ClientDataRetrieval.clientName.startsWith("TVC Marketing")) {
		// for (String mapLink : mapLinks) {
		// String newMapLink = mapLink.replace(" - ", " ");
		// mapLinks.set(mapLinks.indexOf(mapLink), newMapLink);
		// }
		// for (String hotel : hotelList) {
		// String newHotelText = hotel.replace(" - ", " ");
		// hotelList.set(hotelList.indexOf(hotel), newHotelText);
		// }
		// }
		if(!ClientDataRetrieval.environment.equals("XUAT")) {
			BaseUI.verify_true_AndLog(mapLinks.size() > 0, "Results found for Hotel Search.",
					"Results NOT found for Hotel Search.");

			BaseUI.verify_true_AndLog(mapLinks.size() == hotelList.size(), "Links list matched hotel list in size.",
					"Links list did NOT match hotel list in size.");
		}else{
			BaseUI.verify_true_AndLog(hotelList.size() > 0, "Found Hotels/Condos on page.",
					"Did NOT find Hotels/Condos on page.");
		}

		if (!ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) && !ClientDataRetrieval.environment.equals("XUAT")) {
			String output = "";
			for (int i = 0; i < mapLinks.size(); i++) {
				if (mapLinks.get(i).equals(hotelList.get(i))) {
				} else {
					output += MessageFormat.format("Expected value {0}, but saw value {1}", mapLinks.get(i),
							hotelList.get(i)) + "\n";
				}
			}
			BaseUI.verify_true_AndLog(output.equals(""), "Map list matched hotel List.", output);
		} else {
			String logMessage = "Unable to run map comparison to results list due to TVC names not matching up.  Cannot run Map tests for XUAT due to ongoing issues";
			System.out.println(logMessage);
			Reporter.log(logMessage);
		}

	}

	public static void waitForPageToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("hotelSearchResult_TopResultCount", null, null, 40);
		Thread.sleep(1500);
	}


	public static String return_ImageURL() {

		String url = Locator.lookupRequiredElement("hotelSearchResult_HotelLogo_FirstLogo").getAttribute("src");

		return url;
	}

	public static void click_First_BookItNow() throws Exception {
		
//		if(BaseUI.elementAppears(Locator.lookupOptionalElement("hotelSearchResult_SpecialPricingTextTab"))) {
//			List<WebElement> allSpecialOffers = Locator.lookup_multipleElements("hotelSearchResult_SpecialPricingTextTab", null, null);		
//			
//			for (WebElement allSpecialOffersText : allSpecialOffers ) {
//				if(BaseUI.getTextFromField(allSpecialOffersText).trim().contains("SPECIAL PRICING") ||
//						! BaseUI.getTextFromField(allSpecialOffersText).trim().contains("NON-REFUNDABLE SALE")) { 
//					BaseUI.log_Status("This Hotel has SPECIAL PRICING and is not on NON-REFUNDABLE SALE");
//					BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_OfferHotel_BookItNow_FirstLink"));
//					Thread.sleep(1000);
//					break;
//				}
//			}
//		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_NoOfferHotel_BookItNow_FirstLink"));
			BaseUI.wait_forPageToFinishLoading();
			BaseUI.waitForElementToBeDisplayed("hotelPropertyDetails_BookItNowTOP_Button", null, null, 40);
			BaseUI.waitForElementToBeDisplayed("hotelPropertyDetails_BookingDetails_BackToResultsTextLink", null, null, 30);
		    Thread.sleep(1000);
		  //  }
	}
	
	public static void click_HotelName_ByHotelProviders(String providerName) throws Exception {

		boolean providerFound = false;
		while (!providerFound) {
			ArrayList<WebElement> hotelList = Locator.lookup_multipleElements("hotelSearchResult_ListOf_HotelTitles",
					null, null);

			for (int i = 0; i < hotelList.size(); i++) {
				if (BaseUI.get_Attribute_FromField(hotelList.get(i), "href").contains(providerName)) {
					BaseUI.click(hotelList.get(i));
					Thread.sleep(1000);
					providerFound = true;
					break;
				}
			}
			if (!providerFound) {
				if (!BaseUI.elementAppears(Locator.lookupOptionalElement("hotelSearchResult_NextPage_Top_Active_Button"))) {
					BaseUI.log_AndFail("Could Not find the provider.");
				}else{
					navigate_NextPage_ByTOP_Link();
				}
			}
		}
	}
	
	public static void click_HotelName_ByDHISCO_Providers() throws Exception {

		boolean providerFound = false;
		while (!providerFound) {
			ArrayList<WebElement> hotelList = Locator.lookup_multipleElements("hotelSearchResult_ListOf_HotelTitles",
					null, null);

			for (int i = 0; i < hotelList.size(); i++) {
				if (!BaseUI.get_Attribute_FromField(hotelList.get(i), "href").contains("propertyid=TOU")
						&& !BaseUI.get_Attribute_FromField(hotelList.get(i), "href").contains("propertyid=EX")
						&& !BaseUI.get_Attribute_FromField(hotelList.get(i), "href").contains("propertyid=HB")) {
					BaseUI.click(hotelList.get(i));
					Thread.sleep(1000);
					providerFound = true;
					break;
				}
			}
			if (!providerFound) {
				if (!BaseUI.elementAppears(Locator.lookupOptionalElement("hotelSearchResult_NextPage_Top_Active_Button"))) {
					BaseUI.log_AndFail("Could Not find the provider.");
				} else {
					navigate_NextPage_ByTOP_Link();
				}
			}
		}
	}

	public static void navigate_ToPage_ByTOP_PageNumber(Integer pageNumber) throws Exception {
		BaseUI.click(
				Locator.lookupRequiredElement("hotelSearchResult_PageNumber_Top_Link_ByText", pageNumber.toString(), null));
		Thread.sleep(1500);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.get_RID_OF_STUPID_POOPUP();
		}
	}

	public static void navigate_ToPage_ByBOTTOM_PageNumber(Integer pageNumber) throws Exception {
		BaseUI.click(
				Locator.lookupRequiredElement("hotelSearchResult_PageNumber_BOTTOM_Link_ByText", pageNumber.toString(), null));
		Thread.sleep(1500);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.get_RID_OF_STUPID_POOPUP();
		}
	}

	public static void navigate_NextPage_ByTOP_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_NextPage_Top_Button"));
		Thread.sleep(1500);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.get_RID_OF_STUPID_POOPUP();
		}
	}

	public static void navigate_PreviousPage_ByTOP_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_PreviousPage_Top_Button"));
		Thread.sleep(1500);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.get_RID_OF_STUPID_POOPUP();
		}
	}

	public static void navigate_NextPage_ByBOTTOM_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_NextPage_BOTTOM_Button"));
		Thread.sleep(1500);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.get_RID_OF_STUPID_POOPUP();
		}
	}

	public static void navigate_PreviousPage_ByBOTTOM_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_PreviousPage_BOTTOM_Button"));
		Thread.sleep(1500);
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.get_RID_OF_STUPID_POOPUP();
		}
	}

	public static void click_BackToTop_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_BackToTop_Link"));
		Thread.sleep(200);
	}

	public static void verify_BackToTopButton() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("hotelSearchResult_BackToTop_Link"));
		click_BackToTop_Link();
		BaseUI.verify_WindowPosition_Is_AtTop();
	}

	// Assumes that there were more than 40 results.
	public static void verify_Page_Active(Integer pageNumber) {
		WebElement pageToVerify = Locator.lookupRequiredElement("hotelSearchResult_PageNumber_Top_Link_ByText",
				pageNumber.toString(), null);
		BaseUI.verifyElementHasExpectedAttributeValue(pageToVerify, "class", "goToPage");
		Integer entryCount = return_ResultsCount();

		Integer startingNumber = 0;
		Integer endNumber = 0;
		String expectedResult = "";
		if (entryCount >= pageNumber * 20) {
			startingNumber = pageNumber * 20 - 19;
			endNumber = pageNumber * 20;

		} else {
			startingNumber = pageNumber * 20 - 19;
			endNumber = entryCount;
		}

		expectedResult = MessageFormat.format("Showing {0}-{1} of {2}", startingNumber, endNumber, entryCount);

		String resultToReturn = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelSearchResult_TopResultCount"));

		BaseUI.baseStringCompare("Entries visible.", expectedResult, resultToReturn);

	}

	public static void verify_ListOfHotels_AllWithinDistance() throws Exception {

		ArrayList<String> distanceList = return_ListOfDistances();

		for (String distance : distanceList) {
			Double distanceDouble = Double.parseDouble(distance);

			BaseUI.verify_true_AndLog(distanceDouble < 30.00, "Distance was reasonable.",
					"Hotel was too far away from address entered.");
		}
	}

	public static void verify_Distance_FromHotels_Ascending() throws Exception {
		ArrayList<String> distanceList = return_ListOfDistances();

		for (int i = 1; i < distanceList.size(); i++) {
			Double currentDistance = Double.parseDouble(distanceList.get(i));
			Double previousDistance = Double.parseDouble(distanceList.get(i - 1));

			BaseUI.verify_true_AndLog(currentDistance >= previousDistance, "Distance was expected.",
					"Distance was NOT as expected.");
		}

	}

	public static ArrayList<String> return_ListOfDistances() {

		ArrayList<WebElement> addressElementList = Locator
				.lookup_multipleElements("hotelSearchResult_ListOf_HotelAddresses", null, null);
		ArrayList<String> distanceList = new ArrayList<String>();

		BaseUI.verify_true_AndLog(addressElementList.size() > 0, "Found addresses.", "Did NOT find addresses.");

		for (WebElement address : addressElementList) {
			String addressStr = BaseUI.getTextFromField(address);
			String distance = returnDistance_FromAddress(addressStr);
			distanceList.add(distance);
		}

		return distanceList;

	}

	public static String returnDistance_FromAddress(String address) {
		String distance = address.split("\n")[2];
		if (!distance.contains("Distance")) {
			distance = address.split("\n")[3];
		}

		distance = distance.replace("Distance ", "").replace("mile", "").trim();

		return distance;
	}

	// preserved older table extraction solution to avoid problems.
	public static TableData return_hotelList_TableData_old() {
		HashMap<String, String> tableMappings = new HashMap<String, String>();
		tableMappings.put("hotelName", ".//p[@class='title']");
		tableMappings.put("address", ".//p[@class='address']");
		tableMappings.put("youPay", ".//dd[@class='price']");
		tableMappings.put("points", ".//dd[@class='cash']|.//dd[@class='cash']/b");
		tableMappings.put("hotelRate", ".//dd[@class='reg']");

		TableData hotelTable = new TableData();
		hotelTable.data = BaseUI.tableExtractor("hotelSearchResult_RowList", tableMappings);
		hotelTable.removeIntChars_FromBeginningOfAll_Values_InColumn("hotelName", 3);
		hotelTable.remove_Character("$", "youPay");
		hotelTable.remove_Character(",", "youPay");
		hotelTable.remove_Character("-", "points");
		hotelTable.remove_Character(",", "points");

		hotelTable.replace_Character(" , ", ",", "address");
		hotelTable.replace_Character("    ", " ", "address");
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			hotelTable.replace_Character("  ", " ", "address");
		}

		return hotelTable;
	}

	public static Integer indexOf_PartialMatch(String[] arrayToSearch, String valueToFind) {
		for (int i = 0; i < arrayToSearch.length; i++) {
			if (arrayToSearch[i].contains(valueToFind)) {
				return i;
			}

		}
		return null;

	}

	public static TableData return_hotelList_TableData() throws Exception {

		TableData hotelTable = new TableData();

//		//if (Browser.currentBrowser.equals("chromee")) {
//			ArrayList<WebElement> hotelResultList = Locator.lookup_multipleElements("hotelSearchResult_RowList", null,
//					null);
//
//			for (WebElement hotelResult : hotelResultList) {
//				HashMap<String, String> tableRow = new HashMap<String, String>();
//
//				String rowExtract = BaseUI.get_Attribute_FromField(hotelResult, "innerText");
//				String[] extractArray = rowExtract.split("\n");
//
//				String address = extractArray[2] + "\n" + extractArray[3];
//				// + "\n" + extractArray[4];
//				address = address.replace(",", ", ");
//				address = address.replace(",  ", ", ");
//				tableRow.put("address", address);
//				
//				if(rowExtract.contains("NON-REFUNDABLE SALE") || rowExtract.contains("SPECIAL PRICING") ||
//						rowExtract.contains("AMENITY OFFERS")) {
//					BaseUI.log_Status("Hotel is on NON-REFUNDABLE SALE" + tableRow.get("hotelName"));
//				} else {
//					tableRow.put("hotelName", extractArray[1]);
//					BaseUI.log_Status("Adding Hotel info for " + tableRow.get("hotelName"));
//					String rowWithPayInfo = "";
//					String rowWithYourPrice = "";
//					if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
//						rowWithPayInfo = extractArray[indexOf_PartialMatch(extractArray, "Retail Rate")];
//						rowWithYourPrice = extractArray[11];
//					} else {
//						rowWithPayInfo = extractArray[indexOf_PartialMatch(extractArray, "Hotel Rate")];
//					}
//
//					String currency = ClientDataRetrieval.client_Currency;
//					String points = "";
//					if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
//
//					} else {
//						points = rowWithPayInfo.substring(rowWithPayInfo.indexOf(currency));
//						points = points.split("\\:")[1];
//						points = points.substring(0, points.indexOf("Y"));
//					}
//
//					String hotelRate = rowWithPayInfo.split("\\:")[1].substring(0,
//							rowWithPayInfo.split("\\:")[1].indexOf(".") + 3);
//
//					String youPay = "";
//					if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
//						youPay = rowWithYourPrice.substring(rowWithYourPrice.lastIndexOf("$"), rowWithYourPrice.length());
//					}else{
//						youPay = rowWithPayInfo.substring(rowWithPayInfo.lastIndexOf("$"), rowWithPayInfo.length());
//					}
//
//					tableRow.put("hotelRate", hotelRate);
//					tableRow.put("youPay", youPay);
//					tableRow.put("points", points);
//
//					hotelTable.data.add(tableRow);
//				}
//				
//				
//			}
//
//			hotelTable.removeIntChars_FromBeginningOfAll_Values_InColumn("hotelName", 3);
//			hotelTable.remove_Character("$", "youPay");
//			hotelTable.remove_Character(",", "youPay");
//			hotelTable.remove_Character("-", "points");
//			hotelTable.remove_Character(",", "points");
//			// hotelTable.remove_Character("$", "hotelRate");
//			hotelTable.remove_Character(",", "hotelRate");
//			hotelTable.trim_column("hotelRate");
//			hotelTable.trim_column("address");
//			hotelTable.trim_column("points");
//
//			hotelTable.replace_Character(" , ", ", ", "address");
//			hotelTable.replace_Character("    ", " ", "address");
//
//			for (HashMap<String, String> row : hotelTable.data) {
//				String[] addressLines = row.get("address").split("\n");
//				String newAddress = addressLines[0].replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim() + "\n"
//						+ addressLines[1].replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();
//				newAddress = BaseUI.remove_ExtraSpaces(newAddress);
//				row.put("address", newAddress);
//			}
//
//			if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
//					|| ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
//				hotelTable.replace_Character("  ", " ", "address");
//			}
//			// Use this for IE
//		} else {
			// hotelTable = return_hotelList_TableData_old();

			String[] headers = { "hotelName", "address", "hotelRate", "points", "youPay" };
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				headers = new String[] { "hotelName", "address", "hotelRate", "youPay" };
			}
			// else if
			// (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash))
			// {
			// headers = new String[] { "hotelName", "points", "hotelRate",
			// "points", "address" };
			// }

			TableData experimental = BaseUI.tableExtractor_ByCell("hotelSearchResult_RowCells", headers);
			// Citi has no points, nothing to pull, so I added them manually so
			// later tests can validate
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				for (HashMap<String, String> row : experimental.data) {
					row.put("points", "");
				}
			}
			experimental.remove_Character("$", "youPay");
			experimental.remove_Character(",", "youPay");
			experimental.remove_Character("-", "points");
			experimental.remove_Character(",", "points");
			// experimental.remove_Character("$", "hotelRate");
			experimental.remove_Character(",", "hotelRate");
			experimental.removeIntChars_FromBeginningOfAll_Values_InColumn("hotelName", 3);
			experimental.trim_column("hotelName");
			experimental.trim_column("hotelRate");
			experimental.trim_column("points");
			experimental.trim_column("youPay");
			for (HashMap<String, String> row : experimental.data) {
				String[] addressLines = row.get("address").split("\n");
				String newAddress = addressLines[0].replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim() + "\n"
						+ addressLines[1].replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();
				newAddress = BaseUI.remove_ExtraSpaces(newAddress);
				row.put("address", newAddress);
			}
			// experimental.replace_Character("([^a-zA-Z0-9,^\\n]|\\s)+", " ",
			// "address");
			experimental.replace_Character("    ", " ", "address");
			experimental.replace_Character("  ", " ", "address");
			experimental.replace_Character(" ", " ", "address");
			experimental.replace_Character("  ", " ", "address");
			experimental.replace_Character(" ,", ",", "address");
			experimental.replace_Character(",", ", ", "address");
			experimental.replace_Character(",  ", ", ", "address");
			experimental.remove_ExtraSpaces("address");
			experimental.trim_column("address");

			// for (HashMap<String, String> row : experimental.data) {
			// String[] addressLines = row.get("address").split("\n");
			// String newAddress = addressLines[0].trim() + "\n" +
			// addressLines[1].trim();
			// row.put("address", newAddress);
			// }

			hotelTable.data = experimental.data;
//		}
		return hotelTable;
}

	public static void verify_NonAdvertised_Entries_ContainCondoImage() {
		ArrayList<WebElement> rowList = new ArrayList<WebElement>();
		rowList.addAll(Locator.lookup_multipleElements("hotelSearchResult_RowList_NonAdvertised", null, null));

		BaseUI.verify_true_AndLog(rowList.size() > 0, "Entries found.", "Entries NOT found.");

		
		for (WebElement row : rowList) {
			BaseUI.verifyElementAppears(row.findElement(By.xpath(".//img[@src='img/banners/condo.jpg']")));

		}
	}

	public static void verify_Count_OfEntries() throws Exception {
		TableData resultsList = return_hotelList_TableData();
		Integer numberEntries = return_ResultsCount();

		BaseUI.verify_true_AndLog(resultsList.data.size() > 0, "Found entries.", "Did NOT find entries.");

		if (numberEntries >= 20) {
			BaseUI.verify_true_AndLog(resultsList.data.size() == 20, "Entries counts matched.",
					"Entries counts did NOT match.");
		} else {
			BaseUI.verify_true_AndLog(resultsList.data.size() == numberEntries, "Entries counts matched.",
					"Entries counts did NOT match.");
		}
	}

	public static void revise_CheckinDate(String newCheckin) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_checkinDate"));
		Thread.sleep(200);
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(newCheckin);

		click_ReviseSearchLink();

	}

	public static void revise_CheckoutDate(String newCheckin) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_checkoutDate"));
		Thread.sleep(200);
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(newCheckin);

		click_ReviseSearchLink();

	}

	public static void revise_adultCount(String newCount) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("hotelSearchResult_numberOfAdultsDropdown"), newCount);
		Thread.sleep(100);
		click_ReviseSearchLink();
	}

	public static void revise_childCount(String numberChildren, String childrenAge) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("hotelSearchResult_numberOfChildrenDropdown"),
				numberChildren);
		Thread.sleep(400);

		for (Integer i = 1; i <= Integer.parseInt(numberChildren); i++) {
			BaseUI.selectValueFromDropDown(
					Locator.lookupRequiredElement("hotelSearchResult_ChildAge_Dropdown_ByChildNumber", i.toString(), null),
					childrenAge);
			Thread.sleep(50);

		}
	}

	public static void setCheckin_AndCheckoutDates(String checkin, String checkout) throws Exception {

		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("hotelSearch_checkinDate"));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelSearch_checkinDate"));
		}
		Thread.sleep(200);
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(checkin);

		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("hotelSearch_checkoutDate"));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelSearch_checkoutDate"));
		}
		Thread.sleep(200);
		datePicker.selectTheDate(checkout);

		Thread.sleep(200);
	}

	public static void sortBy(String sortOption) throws Exception {
		WebElement dropdown = Locator.lookupRequiredElement("hotelSearchResult_SortBy_Dropdown");
		String currentDropdownValue = BaseUI.getSelectedOptionFromDropdown(dropdown);
		currentDropdownValue = currentDropdownValue.replaceAll("\\s+|\\n", " ").trim();
		if(!sortOption.equals(currentDropdownValue)) {

			String valueToInjectIntoPage = "Switching to dropdown value " + sortOption;
			BaseUI.inject_TextNode_IntoElement(Locator.lookupRequiredElement("hotelSearchResult_TopResultCount"), valueToInjectIntoPage);

			BaseUI.selectValueFromDropDown(dropdown, sortOption);
			BaseUI.waitForElementToNOTContain_PartialAttributeMatch("hotelSearchResult_TopResultCount", null, null,
					"innerText", valueToInjectIntoPage, 30);
			waitForPageToLoad();
		}
	}

	public static void selectTypeOfResult(String typeOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("hotelSearchResult_TypeOfHotelCondo_Dropdown"),
				typeOption);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}

	public static void verify_Breadcrumb_ContainsText(String textToContain) {
		BaseUI.verifyElementHasExpectedPartialTextByElement(
				Locator.lookupRequiredElement("hotelSearchResult_BreadcrumbLink_ContainingText", textToContain, null),
				textToContain);
	}

	public static void click_NewSearchLink() throws Exception {
		BaseUI.waitForElementToBeDisplayed("hotelSearchResult_NewSearchButton", null, null, 30);
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_NewSearchButton"));
		Thread.sleep(1300);

	}

	public static void click_ReviseSearchLink() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelSearchResult_ReviseSearch_Button"));
		Thread.sleep(1000);

	}

	public static Integer return_ResultsCount() {
		String resultToReturn = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelSearchResult_TopResultCount"));
		resultToReturn = resultToReturn.substring(resultToReturn.indexOf("of ") + 3, resultToReturn.length());

		return Integer.parseInt(resultToReturn);
	}

	public static void verify_ResultsListsMatch(TableData expectedResults, TableData actualResults) {
		BaseUI.verify_true_AndLog(expectedResults.data.size() > 0, "Found Hotels.", "Did NOT find Hotels.");
		BaseUI.verify_true_AndLog(expectedResults.data.size() == actualResults.data.size(),
				"Tables were thes same size.", "Tables were NOT the same size.");
		for (Integer i = 0; i < expectedResults.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), expectedResults.data.get(i), actualResults.data.get(i));
		}

	}

	public static void verifyMap_And_ResultList_Appear() throws Exception {
		try {
			switch_ToMapFrame();
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearchResult_hotelContainer"));
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearchResult_Map"));
		} finally {
			Browser.driver.switchTo().defaultContent();
			Thread.sleep(150);
		}

	}

	public static void switch_ToMapFrame() throws Exception {
		Browser.driver.switchTo().frame("hotelmapiframe");
		Thread.sleep(150);
	}

	public static ArrayList<String> return_HotelLinksList() throws Exception {

		ArrayList<WebElement> hotelTitleList = Locator
				.lookup_multipleElements("hotelSearchResult_ListOf_HotelTitles", null, null);

		ArrayList<String> titleList = new ArrayList<String>();

		for (WebElement hotel : hotelTitleList) {
			String hotelStr = hotel.getAttribute("innerText");
			hotelStr = hotelStr.trim();
			titleList.add(hotelStr);
		}

		return titleList;
	}

	// List of link text that exists by the Google Map.
	public static ArrayList<String> return_MapLinks() throws Exception {
		ArrayList<String> hotelList = new ArrayList<String>();

		try {
			switch_ToMapFrame();
			ArrayList<WebElement> googleMapsLinkList = Locator.lookup_multipleElements("hotelSearchResult_LetterList",
					null, null);

			for (WebElement hotelLetter : googleMapsLinkList) {
				String hotel = hotelLetter.getAttribute("innerText");

				WebElement hotelName = hotelLetter.findElement(By.xpath("./following-sibling::span[1]/a"));
				hotel += hotelName.getAttribute("innerText");
				hotelList.add(hotel);
			}
		} finally {
			Browser.driver.switchTo().defaultContent();
			Thread.sleep(150);
		}

		return hotelList;
	}

	public static void verify_checkin_And_checkout_Dates(String checkinDate, String checkoutDate) {
		String checkinDate_OnPage = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("hotelSearchResult_checkinDate"));
		String checkoutDate_OnPage = BaseUI
				.getTextFromInputBox(Locator.lookupRequiredElement("hotelSearchResult_checkoutDate"));

		BaseUI.baseStringCompare("Check In Date", checkinDate, checkinDate_OnPage);
		BaseUI.baseStringCompare("Check In Date", checkoutDate, checkoutDate_OnPage);

	}

	// This method assumes all children are of the same age.
	public static void verify_Adults_AndChildren_Dropdowns(String numberAdults, String numberChildren,
			String childrenAge) throws Exception {

		BaseUI.verify_Value_Selected_InDropdown(Locator.lookupRequiredElement("hotelSearchResult_numberOfAdultsDropdown"),
				numberAdults);
		BaseUI.verify_Value_Selected_InDropdown(Locator.lookupRequiredElement("hotelSearchResult_numberOfChildrenDropdown"),
				numberChildren);

		Integer numberOfChildren = Integer.parseInt(numberChildren);
		for (Integer i = 1; i <= numberOfChildren; i++) {
			BaseUI.verify_Value_Selected_InDropdown(
					Locator.lookupRequiredElement("hotelSearchResult_ChildAge_Dropdown_ByChildNumber", i.toString(), null),
					childrenAge);
		}
	}

}
