package dRewards.pages;

import API.REST;
import API.REST.REST_Call_Type;
import dRewards.data.ClientDataRetrieval;
import utils.DatabaseConnection;

public class DRewards_Utilities {
	
	static String authenticationToken = "B7pb3waA6E95vxOTplU3uaxa5ZCe2wjv";
	static String sqlStatement = "exec catalogupload";

	public static String flush_Memcache() throws Exception {

		String url = "https://rundeck.dlx/api/14/job/024fe515-1831-4aa7-9ee3-b0a1482d1963/run";
		//String url = "https://172.22.120.137/api/2/job/024fe515-1831-4aa7-9ee3-b0a1482d1963/run";
		
		String body = "{\"argString\":" +"\"-Environment " + ClientDataRetrieval.environment + "\"" + "}";

		String output = REST.http_RESTCall(url, body, REST_Call_Type.Post, authenticationToken, "X-Rundeck-Auth-Token");
				
		//REST.httpPost(url, body, authenticationToken);

		return output;
	}
	
	public static void catalog_upload() throws Exception {
		DatabaseConnection.dbUrl = GlobalVariables.connectionString();

		DatabaseConnection.runSQLServerCommand(sqlStatement);
	}
	
	public static void catalog_upload_AndWait() throws Exception {
		DatabaseConnection.dbUrl = GlobalVariables.connectionString();

		DatabaseConnection.runSQLServerUpdate(sqlStatement);
	}
	
	
	
	
}//End of Class
