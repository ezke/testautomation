package dRewards.pages;

import java.util.HashMap;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.ClassObjects.CarRentalSearch_ListViewResults;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.DatePicker;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class CarRental_ModifySearchResults {

	CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();
	static CarRentalSearch_ListViewResults ListViewResultsDetails = new CarRentalSearch_ListViewResults();

	public static String airportLocation;

	// Click New Search hyperlink
	public static void click_NewSearch_Link() throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_ModifySearchResults_NewSearchLink"));
		BaseUI.waitForElementToBeDisplayed("carRental_HeaderText", null, null, 20);
		Thread.sleep(1000);
	}

	// Details from Car Rental Search resulting page
	public static void verifyCarRentalSearchResults_MatchSearchCriteria(String cityNameDisplayed,
			String carClassDisplayed, String pickUpDateDisplayed, String returnDateDisplayed,
			String pickUpTimeDisplayed, String returnTimeDisplayed) {

		String cityNameText = BaseUI
				.get_Attribute_FromField(Locator.lookupRequiredElement("carRental_ModifySearchResults_Location"), "innerText")
				.replaceAll("Location:", "").trim();
		BaseUI.baseStringCompare("carRental_ModifySearchResults_Location", cityNameDisplayed, cityNameText);

		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("carRental_ModifySearchResults_CarClassOption"),
				carClassDisplayed);

		String pickupDateTextBox = BaseUI
				.getTextFromInputBox(Locator.lookupRequiredElement("carRental_ModifySearchResults_PickUpDate"));
		BaseUI.baseStringCompare("Pickup Date TextBox", pickUpDateDisplayed, pickupDateTextBox);

		String returnDateTextBox = BaseUI
				.getTextFromInputBox(Locator.lookupRequiredElement("carRental_ModifySearchResults_ReturnDate"));
		BaseUI.baseStringCompare("Return Date TextBox", returnDateDisplayed, returnDateTextBox);

		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("carRental_ModifySearchResults_PickUpTime"),
				pickUpTimeDisplayed);
		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("carRental_ModifySearchResults_ReturnTime"),
				returnTimeDisplayed);
	}

	// Click continue button
	public static void click_Continue_Btn() throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_SearchResults_WithDifferentLocation_ContinueBtn"));
		BaseUI.waitForElementToBeDisplayed("carRental_SearchResults_WithDifferentLocation_ContinueBtn_WaitElement", null, null, 30);
		Thread.sleep(1200);
	}

	// CLick First option from the search result option
	public static void click_FirstOption_SearchResult_RadioBtn(String option) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement(option));
		Thread.sleep(200);
	}

	// Click Map button
	public static void click_Map_Btn() throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("carRental_SearchResults_SearchResultHeader", null, null, 30);
		Thread.sleep(500);
	}

	// Click List button
	public static void click_List_Btn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("carRentalSearchResult_ListButton"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToNOTBeDisplayed("carRentalSelect_GoogleMaps_MapTab", null, null, 30);
		Thread.sleep(500);
	}

	// Return a List of our CarRentalSearch_ListViewResults Classes that
	// contains all of the CarDetails for Avis company
	// public static ArrayList<CarRentalSearch_ListViewResults>
	// retrieve_AllAvisCarDetails() {
	// ArrayList<CarRentalSearch_ListViewResults> avisCarRentalDetails = new
	// ArrayList<CarRentalSearch_ListViewResults>();
	// ArrayList<WebElement> avisCarDetailsRows = new ArrayList<WebElement>();
	// avisCarDetailsRows.addAll(Locator.lookup_multipleElements("carRentalSearchResult_ListViewResult_AvisCarDetails_Rows",
	// null, null));
	// for (WebElement carDetailsRow : avisCarDetailsRows) {
	// CarRentalSearch_ListViewResults carDetails = new
	// CarRentalSearch_ListViewResults();
	// carDetails.carClass =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[@class='view_car_details']/a")).getText();
	// carDetails.avgBaseRate =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[2]")).getText();
	// carDetails.points_Or_SmartDollars =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[@class='currency']")).getText();
	// //carDetails.youPay =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[4]")).getText();
	// carDetails.totalWithTax =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[5]")).getText();
	//
	// if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
	//
	// carDetails.youPay =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[3]")).getText();
	// }
	// else
	// {
	// carDetails.youPay =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_avis']//td[4]")).getText();
	// }
	// avisCarRentalDetails.add(carDetails);
	// }
	//
	// return avisCarRentalDetails;
	// }

	// Had to include Reserve as it was taking up a table cell.
	public static String[] return_SearchResultHeaders() {
		String[] headers;

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			headers = new String[] { "carClass", "avgBaseRate", "youPay", "points_Or_SmartDollars", "totalWithTax",
					"Reserve Cell" };
		} else {
			headers = new String[] { "carClass", "avgBaseRate", "points_Or_SmartDollars", "youPay", "totalWithTax",
					"Reserve Cell" };
		}

		return headers;
	}

	// gets the table data utilizing our generic Base Class for retrieving table
	// data.
	public static TableData get_Table_Data_Avis() throws Exception {
		TableData table_Data = new TableData();
		if (Browser.currentBrowser.equals("internetexplorer")) {
			HashMap<String, String> fields_toGet = return_CarRental_CellMappings();

			table_Data.data = BaseUI.tableExtractor("carRentalSearchResult_ListViewResult_AvisCarDetails_Rows",
					fields_toGet);
		} else {
			String[] headers = return_SearchResultHeaders();

			table_Data = BaseUI.tableExtractorV2("carRentalSearchResult_AvisTable_Body", headers);
			table_Data.trim_column("carClass");
		}

		return table_Data;
	}

	// gets the table data utilizing our generic Base Class for retrieving table
	// data.
	public static TableData get_Table_Data_Budget() throws Exception {
		TableData table_Data = new TableData();

		if (Browser.currentBrowser.equals("internetexplorer")) {
			HashMap<String, String> fields_toGet = return_CarRental_CellMappings();

			table_Data.data = BaseUI.tableExtractor("carRentalSearchResult_ListViewResult_BudgetCarDetails_Rows",
					fields_toGet);
		} else {
			String[] headers = return_SearchResultHeaders();

			table_Data = BaseUI.tableExtractorV2("carRentalSearchResult_BudgetTable_Body", headers);
			table_Data.trim_column("carClass");
		}

		return table_Data;
	}

	public static HashMap<String, String> return_CarRental_CellMappings() {
		HashMap<String, String> fields_toGet = new HashMap<String, String>();
		fields_toGet.put("carClass", ".//td[@class='view_car_details']/a");
		fields_toGet.put("avgBaseRate", ".//td[2]");
		fields_toGet.put("points_Or_SmartDollars", ".//td[@class='currency']");
		// fields_toGet.put("youPay", "//table[@id='results_avis']//td[4]");
		fields_toGet.put("totalWithTax", ".//td[5]");

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {

			fields_toGet.put("youPay", ".//td[3]");
		} else {
			fields_toGet.put("youPay", ".//td[4]");
		}

		return fields_toGet;
	}

	// Return a List of our CarRentalSearch_ListViewResults Classes that
	// contains all of the CarDeatils for Budget company
	// data.
	// public static ArrayList<CarRentalSearch_ListViewResults>
	// retrieve_AllBudgetCarDetails() {
	// ArrayList<CarRentalSearch_ListViewResults> avisCarRentalDetails = new
	// ArrayList<CarRentalSearch_ListViewResults>();
	// ArrayList<WebElement> avisCarDetailsRows = new ArrayList<WebElement>();
	// avisCarDetailsRows.addAll(Locator.lookup_multipleElements("carRentalSearchResult_ListViewResult_AvisCarDetails_Rows",
	// null, null));
	// for (WebElement carDetailsRow : avisCarDetailsRows) {
	// CarRentalSearch_ListViewResults carDetails = new
	// CarRentalSearch_ListViewResults();
	// carDetails.carClass =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[@class='view_car_details']/a")).getText();
	// carDetails.avgBaseRate =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[2]")).getText();
	// carDetails.points_Or_SmartDollars =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[@class='currency']")).getText();
	// //carDetails.youPay =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[4]")).getText();
	// carDetails.totalWithTax =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[5]")).getText();
	//
	// if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
	//
	// carDetails.youPay =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[3]")).getText();
	// }
	// else
	// {
	// carDetails.youPay =
	// carDetailsRow.findElement(By.xpath("//table[@id='results_budget']//td[4]")).getText();
	// }
	//
	// avisCarRentalDetails.add(carDetails);
	// }
	//
	// return avisCarRentalDetails;
	// }

	public static void verify_AvisCarRental_PriceDetails(Integer rowValue, String carClassOption,
			TableData carRentalData) {

		HashMap<String, String> carRentalRow = carRentalData.data.get(rowValue);

		BaseUI.baseStringCompare("Car Class for Avis match selected", carClassOption, carRentalRow.get("carClass"));

		String avgBaseRate = carRentalRow.get("avgBaseRate").replace("$", "");
		Double avgBaseRate_Double = Double.parseDouble(avgBaseRate);
		BaseUI.verify_true_AndLog(avgBaseRate_Double > 0.0,
				"Avg Base Rate " + avgBaseRate + "  is greater than 0 and numeric value",
				"Avg Base Rate " + avgBaseRate + " is not greater than 0");

		String points_OrSmartDollars = carRentalRow.get("points_Or_SmartDollars").replace("%", "");
		BaseUI.verify_true_AndLog((Double.parseDouble(points_OrSmartDollars) > 0),
				"Points or Smart Dollars " + points_OrSmartDollars + "  is greater than 0 and numeric value",
				"Points or Smart Dollars " + points_OrSmartDollars + " is not greater than 0");

		String youPay = carRentalRow.get("youPay").replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(youPay) > 0,
				"You Pay " + youPay + "  is greater than 0 and numeric value",
				"You Pay " + youPay + " is not greater than 0");

		String totalWithTax = carRentalRow.get("totalWithTax").replace("$", "");
		BaseUI.verify_true_AndLog((Double.parseDouble(totalWithTax) > 0),
				"Total with Tax " + totalWithTax + "  is greater than 0 and numeric value",
				"Total with Tax " + totalWithTax + " is not greater than 0");
	}

	// Verify Budget List view results car rental price details
	public static void verify_BudgetCarRental_PriceDetails(int rowValue, String carClassOption,
			TableData carRentalData) {

		HashMap<String, String> carRentalRows = carRentalData.data.get(rowValue);

		BaseUI.baseStringCompare("Car Class for Budget match selected", carClassOption, carRentalRows.get("carClass"));

		String avgBaseRate = carRentalRows.get("avgBaseRate").replace("$", "");
		BaseUI.verify_true_AndLog((Double.parseDouble(avgBaseRate) > 0),
				"Avg Base Rate " + avgBaseRate + "  is greater than 0 and numeric value",
				"Avg Base Rate " + avgBaseRate + " is not greater than 0");

		String points_Or_SmartDollars = carRentalRows.get("points_Or_SmartDollars").replace("%", "");
		BaseUI.verify_true_AndLog((Double.parseDouble(points_Or_SmartDollars) > 0),
				"Points or Smart Dollars " + points_Or_SmartDollars + "  is greater than 0 and numeric value",
				"Points or Smart Dollars " + points_Or_SmartDollars + " is not greater than 0");

		String youPay = carRentalRows.get("youPay").replace("$", "");
		BaseUI.verify_true_AndLog((Double.parseDouble(youPay) > 0),
				"You Pay " + youPay + "  is greater than 0 and numeric value",
				"You Pay " + youPay + " is not greater than 0");

		String totalWithTax = carRentalRows.get("totalWithTax").replace("$", "");
		BaseUI.verify_true_AndLog((Double.parseDouble(totalWithTax) > 0),
				"Total with Tax " + totalWithTax + "  is greater than 0 and numeric value",
				"Total with Tax " + totalWithTax + " is not greater than 0");
	}

	// Select pick up date
	public static void select_PickUpDate(String pickUpDate) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_ModifySearchResults_PickUpDate"));
		Thread.sleep(200);
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(pickUpDate);
	}

	// Select return date
	public static void select_ReturnDate(String returnDate) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_ModifySearchResults_ReturnDate"));
		Thread.sleep(200);
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(returnDate);
	}

	// Select car class from dropdown option
	public static void choose_CarClassOption(String carClass) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_ModifySearchResults_CarClassOption"), carClass);
		Thread.sleep(200);
	}

	// Select pick up time
	public static void select_PickUpTime(String pickUpTime) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_ModifySearchResults_PickUpTime"), pickUpTime);
		Thread.sleep(200);
	}

	// Select return time
	public static void select_ReturnTime(String returnTime) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_ModifySearchResults_ReturnTime"), returnTime);
		Thread.sleep(200);
	}

	// Click Revise button
	public static void click_Revise_Button() throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_ModifySearchResults_ReviseSearchBtn"));
		BaseUI.waitForElementToBeDisplayed("carRental_ModifySearchResults_Header", null, null, 20);
		BaseUI.waitForElementToBeDisplayed("carRental_ModifySearchResults_NewSearchLink", null, null, 20);
		Thread.sleep(2000);
	}

	public static void carRentalSearch_LandingPage_ModifySearchResults_NewOrReviseSearch(String pickUpDate,
			String returnDate, String carClassOption) throws Exception {

		CarRental_ModifySearchResults.select_PickUpDate(pickUpDate);
		CarRental_ModifySearchResults.select_ReturnDate(returnDate);
		CarRental_ModifySearchResults.choose_CarClassOption(carClassOption);

		CarRental_ModifySearchResults.click_Revise_Button();
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("carRental_ModifySearchResults_NoSearchResultMessage"))) {
			BaseUI.log_AndFail("Sorry, there is no available vehicle at this time, please try again later.");
		}
	}

	// Click Reserve link for 1st row to reserve car rental
	public static void click_ReserveLink(String reserveLink) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement(reserveLink));
		BaseUI.waitForElementToBeDisplayed("carRentalSearchResult_CheckoutPage_HeaderTitle", null, null, 20);
		Thread.sleep(1000);
	}

	// Check Result page and choose 1st available car rental company
	public static void select_CarRentalCompany_Details() throws Exception {

		TableData carRentalData;

		String avgBaseRate;
		String points_OrSmartDollars;
		String youPay;
		String totalWithTax;

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("carRentalSearchResult_ListViewResult_AvisLogo"))) {

			carRentalData = get_Table_Data_Avis();
		} else {

			carRentalData = get_Table_Data_Budget();
		}
		// airportLocation =
		// BaseUI.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_AirportAddress")).trim();

		HashMap<String, String> carRentalRow = carRentalData.data.get(0);

		avgBaseRate = carRentalRow.get("avgBaseRate").replace("$", "");
		points_OrSmartDollars = carRentalRow.get("points_Or_SmartDollars").replace("%", "");
		youPay = carRentalRow.get("youPay").replace("$", "");
		totalWithTax = carRentalRow.get("totalWithTax").replace("$", "");

		reserveCar_From_AvisOrBudget();

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_HeaderTitle"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_HeaderTitle",
				"Car Rentals | Additional Information");
		String headerTitle = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_YourCarRentalTitle"));
		BaseUI.verify_true_AndLog(
				headerTitle.equals("Your Avis Rental Car") || headerTitle.equals("Your Budget Rental Car"),
				"Header title " + headerTitle + " is Displayed", "Header title " + headerTitle + " is not Displayed");

		CarRentalSearch_Checkout.verify_AvisCarRentalCheckout_PriceDetails(avgBaseRate, points_OrSmartDollars, youPay,
				totalWithTax);
	}

	public static void reserveCar_From_AvisOrBudget() throws Exception {

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("carRentalSearchResult_ListViewResult_AvisLogo"))) {

			click_ReserveLink("carRentalSearchResult_ListViewResult_Avis_ReserveBtn");
		} else {

			click_ReserveLink("carRentalSearchResult_ListViewResult_Budget_ReserveBtn");
		}
	}

	public static String returnAvis_Or_Budget_AirportLocation() {
		if (BaseUI.elementAppears(Locator.lookupOptionalElement("carRentalSearchResult_ListViewResult_AvisLogo"))) {
			airportLocation = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Avis_AirportAddress"))
					.trim();
		} else {
			airportLocation = BaseUI.getTextFromField(
					Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_AirportAddress")).trim();
		}
		return airportLocation;
	}
}
