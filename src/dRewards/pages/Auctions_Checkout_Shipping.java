package dRewards.pages;

import java.util.HashMap;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Auctions_Checkout_Shipping extends CheckoutPage_ShippingInfo {

	public static void click_Use_FirstShippingAddress() throws Exception {
		BaseUI.checkCheckbox("auctShip_FirstAddress_RadioButton");
		Thread.sleep(100);
	}

	public static void verify_AuctionInfo(String auctionName, String auctionBid, String expectedEndTime)
			throws Exception {
		BaseUI.verifyElementHasExpectedText("auctShip_AuctionDescriptionName", auctionName);

		String currency = "Points";
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			currency = "Smart Dollars";
		}

		auctionBid = auctionBid.split("\\.")[1].length() == 1 ? auctionBid + "0" : auctionBid;

		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARP)
				||ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
			||ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
			||ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			
			auctionBid = auctionBid.split("\\.")[0];
		}

		BaseUI.verifyElementHasExpectedText("auctShip_winningBid", auctionBid + " " + currency);
		if (ClientDataRetrieval.isRedesignClient()) {
			String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
			String endTime = auctInfo.split("\\n")[5];
			String claimDate = auctInfo.split("\\n")[8];
			//String expectedEndTimeRedesign	 =  expectedEndTime;
			//BaseUI.baseStringCompare("Auction End Time", expectedEndTimeRedesign, endTime);
			verify_ClaimByDate(endTime, claimDate);
		} else {
			String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
			String endTime = auctInfo.split("\\n")[2];
			String claimDate = auctInfo.split("\\n")[3];
			expectedEndTime = "Auction End Date: " + expectedEndTime;
			BaseUI.baseStringCompare("Auction End Time", expectedEndTime, endTime);
			verify_ClaimByDate(endTime, claimDate);

		}
	}
	public static void verify_AuctionsInfo_DualCurrency(String auctionName, String auctionBid, String expectedEndTime,
			Double usdAmount) throws Exception {

		auctionBid = auctionBid.split("\\.")[1].length() == 1 ? auctionBid + "0" : auctionBid;

		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			auctionBid = auctionBid.split("\\.")[0];
		}
		BaseUI.verifyElementHasExpectedText("auctShip_AuctionDescriptionName", auctionName);
		BaseUI.verifyElementHasExpectedText("auctShip_winningBid",
				auctionBid + " " + ClientDataRetrieval.client_Currency);
		BaseUI.verifyElementHasExpectedText("auctShip_PaymentDue",
				"$" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount) + " USD");
		String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
		String endTime = auctInfo.split("\\n")[3];
		String claimDate = auctInfo.split("\\n")[4];
		expectedEndTime = "Auction End Date: " + expectedEndTime;
		BaseUI.baseStringCompare("Auction End Time", expectedEndTime, endTime);
		if (!ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			verify_ClaimByDate(endTime, claimDate);
		}
	}

	public static void verify_ClaimByDate(String auctionEndDateString, String actualClaimDate) throws Exception {
		auctionEndDateString = auctionEndDateString.replace("Auction End Date: ", "");
		String claimDate = Auctions.return_expectedClaimDate(auctionEndDateString);

		if (ClientDataRetrieval.isRedesignClient()) {

			claimDate = claimDate.replace("at", "");
			actualClaimDate = actualClaimDate.replace("at", "");
			claimDate = BaseUI.return_Date_AsDifferentFormat(claimDate,
					"MMMM dd, yy hh:mm a", "MMMM dd, yyyy  hh:mm a");
			BaseUI.baseStringCompare("Claim Date", actualClaimDate, claimDate);

		} else {

			BaseUI.baseStringCompare("Claim Date", actualClaimDate, "Claim By: " + claimDate);
		}
	}
	public static HashMap<String, String> return_ShippingInfo() {
		HashMap<String, String> shippingInfo = new HashMap<String, String>();
		if (ClientDataRetrieval.isRedesignClient()) {

			shippingInfo.put("clientName", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_FirstName")));
			shippingInfo.put("address", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_Address")));
			shippingInfo.put("city", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_City")));
			shippingInfo.put("country", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_Country")));

		} else {
			
		shippingInfo.put("clientName", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_FirstName")));
		//+ " " + BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_LastName")));
		shippingInfo.put("address", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_Address")));
		shippingInfo.put("city", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_City")));
		shippingInfo.put("state", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_State")));
		shippingInfo.put("zipCode",
				BaseUI.get_NonVisible_TextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_ZipCode")));
		shippingInfo.put("zipCode-masked",
				BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_ZipCode_masked")));
		shippingInfo.put("country", BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_Country")));
		shippingInfo.put("phone",
				BaseUI.get_NonVisible_TextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_Phone")));
		shippingInfo.put("phone-masked",
				BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_1stAddress_Phone_masked")));
	}
		return shippingInfo;
	}

	// just uses default Shipping Address and navigates to next Checkout page.
	// public static void navigate_ToOrderReview() throws Exception {
	// HashMap<String, String> defaultShippingInfo =
	// return_DefaultShippingInfo();
	//
	// if (BaseUI.elementExists("checkout_ShipToExistingAddress_Radio", null,
	// null)) {
	// click_AddExistingAddress_Radio();
	// click_ReviewYourOrder_Button();
	//
	// }
	//
	// else {
	// String firstName =
	// defaultShippingInfo.get("clientName").split("\\s+")[0];
	// //
	// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameLabel"));
	// BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"),
	// firstName);
	// String lastName = defaultShippingInfo.get("clientName").split("\\s+")[1];
	// //
	// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_LastNameLabel"));
	// BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"),
	// lastName);
	// String address1 = defaultShippingInfo.get("address");
	// //
	// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_Address1Label"));
	// BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"),
	// address1);
	// String city = defaultShippingInfo.get("city");
	// // BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_City1Label"));
	// BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"),
	// city);
	// //
	// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown"));
	// Thread.sleep(200);
	// String zip = defaultShippingInfo.get("zipCode");
	// Object state = defaultShippingInfo.get("state");
	// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown")
	// .findElement(By.xpath(MessageFormat.format("./option[@value=''{0}'']",
	// state))));
	// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeLabel"));
	// BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"),
	// zip);
	// String phone = defaultShippingInfo.get("phone");
	// BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_phoneMaskedTxtBx"),
	// phone);
	// BaseUI.checkCheckbox(Locator.lookupRequiredElement("checkout_NewAddress_SavedShipping_Address"));
	//
	// click_ReviewYourOrder_Button();
	// }
	//
	// }
	public static HashMap<String, String> return_AuctionEndInfoRedesign() {
		HashMap<String, String> auctionEndInfo = new HashMap<String, String>();
		String auctionInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
		String endTime = auctionInfo.split("\\n")[5];
		endTime = endTime.replace("Auction End Date: ", "");
		String claimBy = auctionInfo.split("\\n")[8];
		claimBy = claimBy.replace("Claim By: ", "");
		auctionEndInfo.put("auctionEnded", endTime);
		auctionEndInfo.put("claimBy", claimBy);

		return auctionEndInfo;
	}
	public static HashMap<String, String> return_AuctionEndInfo() {
		HashMap<String, String> auctionEndInfo = new HashMap<String, String>();
		String auctionInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
		String endTime = auctionInfo.split("\\n")[2];
		endTime = endTime.replace("Auction End Date: ", "");
		String claimBy = auctionInfo.split("\\n")[3];
		claimBy = claimBy.replace("Claim By: ", "");
		auctionEndInfo.put("auctionEnded", endTime);
		auctionEndInfo.put("claimBy", claimBy);

		return auctionEndInfo;
	}

	public static HashMap<String, String> return_AuctionEndInfo_DualCurrency() {
		HashMap<String, String> auctionEndInfo = new HashMap<String, String>();
		String auctionInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
		String endTime = auctionInfo.split("\\n")[3];
		endTime = endTime.replace("Auction End Date: ", "");
		String claimBy = auctionInfo.split("\\n")[4];
		claimBy = claimBy.replace("Claim By: ", "");
		auctionEndInfo.put("auctionEnded", endTime);
		auctionEndInfo.put("claimBy", claimBy);

		return auctionEndInfo;
	}

	// Takes me to next page, in this case it's Review Order page.
	// @Override
	// public static void click_ContinueTo_NextPage() throws Exception {
	// BaseUI.click(Locator.lookupRequiredElement("auctShip_ReviewYourOrder_Button"));
	// Thread.sleep(1000);
	// }

}// end of class
