package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.AuctionTile;
import dRewards.pageControls.WebAuctionTile;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Auctions {

	private static TableData return_VisibleAuctions(String rowElementLocator) {
		TableData auctions = new TableData();
		HashMap<String, String> auctionMappings = new HashMap<>();
		
		if (ClientDataRetrieval.isRedesignClient()) {
			auctionMappings.put("auctionTitle", ".//div[@class='sweepsname-n-desc']/h2");
			auctionMappings.put("currentBid", ".//div[@class='enddate-n-entries']//span[@class='points']");
			auctionMappings.put("auctionTimeLeft", ".//div[@class='enddate-n-entries']/div[@class='time-container']//span[@class='countdown']");
		} else {
			auctionMappings.put("auctionTitle", ".//div[@class='description']//a");
			auctionMappings.put("currentBid", ".//div[@class='bidAmt']");
			auctionMappings.put("currencyType", ".//span[@class='currencyName']");
			auctionMappings.put("auctionTimeLeft", ".//div[@class='timeLeft']/div");
		}
		
		auctions.data = BaseUI.tableExtractor(rowElementLocator, auctionMappings);

		return auctions;
	}

	public static AuctionTile return_AuctionTile_ByAuctionNameAndCurrentBid(String auctionName, Double currentBid, String rowElementLocator) {
		TableData auctions = return_VisibleAuctions(rowElementLocator);
		String currentBidAsString;

		currentBidAsString = String.valueOf(currentBid);//currentBid.toString();
		if (ClientDataRetrieval.isRedesignClient()) {
			currentBidAsString = (currentBidAsString.substring(0, currentBidAsString.indexOf("."))) + " Points";
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)){
				if (currentBidAsString.split("\\.")[1].length() == 1) {
					currentBidAsString += "0";
				}
			} else {
				currentBidAsString = currentBidAsString.substring(0, currentBidAsString.indexOf("."));
			}
		}
		
		Integer auctionIndex = auctions.return_RowIndex_BasedOn_2MatchingFields("auctionTitle", auctionName,
				"currentBid", currentBidAsString);
		if (auctionIndex == null) {
			BaseUI.log_AndFail("Unable to find auction with name '" + auctionName + "' and current bid '" + currentBid + "'");
		}

		int oneBasedAuctionIndexForXPath = auctionIndex + 1;
		return new WebAuctionTile(oneBasedAuctionIndexForXPath);
	}

	public static AuctionTile return_AuctionTile_ByAuctionNameAndCurrentBid(String auctionName, Double currentBid) {
		return return_AuctionTile_ByAuctionNameAndCurrentBid(auctionName, currentBid, "auctionRows");
	}

	public static String return_Auction_TimeLeftFormat_forCheckout(Integer minutesFromNow_ThatItShouldEnd) {

		// if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
		// minutesFromNow_ThatItShouldEnd += 60;
		// }
		String time;
		if (ClientDataRetrieval.isRedesignClient()) {
			time = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesFromNow_ThatItShouldEnd,
					"MMM d, yyyy h:mm a");
		} else {
			time = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(minutesFromNow_ThatItShouldEnd,
					"MMM. d, yyyy h:mm a");
		}
		
		time = time + " "+(GlobalVariables.auctionTimeZone);
		//time += " EDT";
		return time;
	}

	//Apr. 14, 2017 9:40 AM EDT, or format "MMM. d, yyyy h:mm a" + " " + EDT
	public static String return_expectedClaimDate(String auctionEndDate) throws Exception
	{
		String endDate = auctionEndDate.substring(0, auctionEndDate.lastIndexOf(" ")).trim();
		String endDate_TimeZone = auctionEndDate.substring(auctionEndDate.lastIndexOf(" "), auctionEndDate.length()).trim();

		String claimDate;
		if (ClientDataRetrieval.isRedesignClient()) {
			endDate = endDate.replace("at","");

			claimDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(endDate, "MMMM dd, yyyy hh:mm",
					"MMMM dd, yy hh:mm", 30);
		} else {
			claimDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(endDate, "MMM. d, yyyy h:mm a", 
					"MMM. d, yyyy h:mm a", 30);
		}
		claimDate += " " + endDate_TimeZone;

		return claimDate;
	}

	public static void verify_AuctionDetails_ByUserPoints(String expectedPoints, String productDescription,
			Double currentBid, Boolean isDualCurrency) {
		String actualPoints = Navigation.returnPoints();
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)){
			//||(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))) {
			actualPoints = actualPoints.split("\\.")[0];
		}
		AuctionTile auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(productDescription, currentBid);
		auctionTile.verify_AuctionDetails_Info(productDescription, currentBid, isDualCurrency);
		BaseUI.baseStringCompare("Points", expectedPoints, actualPoints);
	}

	//For redesign clients
	public static void loginBackAndClickMyAccount_CloseAuctions() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
			Browser.closeBrowser();
			Browser.openBrowser(ClientDataRetrieval.url);
			LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
			Navigation.navigate_MyAccount_CloseAuctionsRedesign();
			Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();
			Navigation.clickMyAccount_CloseAuctionsRedesign();
		}
	}

	//Redesign Core Promo Code modal Email Preferences Page Left Navigation link list validation
	public static void verify_AuctionsThreeTierNavigationMenu() throws Exception {

		String[] auctions3TierNavOptions = { "Live Auctions", "Upcoming Auctions", "Closed Auctions" };

		List<WebElement> allLNavigationMenu_Options = Locator
				.lookup_multipleElements("auctions_NavigationMenuTabs", null, null);
		List<String> navigationMenuList = new ArrayList<String>();

		for (WebElement navMenuOption : allLNavigationMenu_Options) {
			navigationMenuList.add(BaseUI.getTextFromField(navMenuOption).trim());
		}

		BaseUI.verify_true_AndLog(navigationMenuList.size() == auctions3TierNavOptions.length,
				"Navigation Menu List matched expected values in size.",
				"Navigation Menu List did not match expected values in size.");

		for (String navigationOption : auctions3TierNavOptions) {
			BaseUI.verify_true_AndLog(navigationMenuList.contains(navigationOption),
					"Navigation Menu List option " + navigationOption + " found in list.",
					"Navigation Menu List option " + navigationOption + " NOT found in list.");
		}
	}

	public static void clickAuctionsNavigationMenuTab(String menuTabOption) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("nav_Auctions_AuctionsLinks", menuTabOption, null));
		BaseUI.waitForElementToBeDisplayed("auctionsHeaderTitle", menuTabOption, null, 30);
		Thread.sleep(1000);
	}

	public static void verifyEachClosedAuctionsHasEndedHeader() {
		List<WebElement> allLeftNavigation_Options = Locator
				.lookup_multipleElements("auctions_ClosedAuctionsList", null, null);
		List<String> leftNavigationList = new ArrayList<String>();

		for (WebElement dropdownOption : allLeftNavigation_Options) {
			leftNavigationList.add(BaseUI.get_Attribute_FromField(dropdownOption, "innerText").trim());
		}

		BaseUI.verify_true_AndLog(leftNavigationList.size() > 0,
				"Closed Auctions page displays ALL the closed auctions",
				"Closed Auctions page DOES NOT display ALL the closed auctions");
	}

}// End of Class