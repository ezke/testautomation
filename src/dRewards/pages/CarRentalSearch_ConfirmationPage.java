package dRewards.pages;

import java.util.HashMap;
import dRewards.ClassObjects.CarRentalSearch_CheckoutDetails;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class CarRentalSearch_ConfirmationPage {

	static CarRentalSearch_CheckoutDetails confirmation_PriceDetails = new CarRentalSearch_CheckoutDetails();
	
	public static void verifyCarRentalSearchResult_ConfirmationPage_VerifyPoliciesLnk() throws Exception {

		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PoliciesLnk"), true,
				1000);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PrivacyPoliciesHeader"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PrivacyPoliciesLinks"));

	}

	public static void verifyCarRentalSearchResult_ConfirmationPage_VerifyFaqsLnk() throws Exception {

		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FaqsLnk"), true, 1000);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FaqsPageHeader"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FaqsPageLeftNavLinks"));
	}

	public static void verifyCarRentalSearchResult_ConfirmationPage_VerifyMapFrom() throws Exception {

		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_pickupDirections_Btn"), true,
				1000);
		BaseUI.waitForElementToBeDisplayed("carRental_ReserveConfirmPage_SignIn_Btns", null, null, 50);
		verifyCarRentalSearchResult_ConfirmationPage_MapPopup_signaAndApp_Btns();		

	}

	public static void verifyCarRentalSearchResult_ConfirmationPage_VerifyMapTo() throws Exception {

		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_returnDirections_Btn"), true,
				1000);
		BaseUI.waitForElementToBeDisplayed("carRental_ReserveConfirmPage_SignIn_Btns", null, null, 50);
		verifyCarRentalSearchResult_ConfirmationPage_MapPopup_signaAndApp_Btns();
		
	}

	public static void verifyCarRentalSearchResult_ConfirmationPage_MapPopup_signaAndApp_Btns() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_SignIn_Btns"));	
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_GoogleApps_Btn"));
	}
	
	
	public static CarRentalSearch_CheckoutDetails retrieve_CarRental_ConfirmationPage_PriceDetails() {
		CarRentalSearch_CheckoutDetails carRentalPriceDetails = new CarRentalSearch_CheckoutDetails();

		carRentalPriceDetails.avgBaseRate = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_RentalChargePerDay"));
		carRentalPriceDetails.points_OrSmartDollars = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PointsOrSmartDollars"));
		carRentalPriceDetails.rentalCharge = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_RentalCharge"));
		carRentalPriceDetails.taxes_OrFees = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_TaxesOrFees"));
		carRentalPriceDetails.totalWithTax = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_TotalPriceWithTax"));

		return carRentalPriceDetails;
	}
	
	
	public static void verify_CarRentalConfirmationPage_PriceDetails(String avgBaseRate, String points_OrSmartDollars,
			String youPay, String totalWithTax) throws Exception {

		CarRentalSearch_CheckoutDetails confirmation_PriceDetails = new CarRentalSearch_CheckoutDetails();
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_RentalChargePerDay"));
		confirmation_PriceDetails = CarRentalSearch_ConfirmationPage.retrieve_CarRental_ConfirmationPage_PriceDetails();

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {

			BaseUI.verifyElementAppears(
					Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PointsOrSmartDollars"));
		} else {
			String points_smartDollars = confirmation_PriceDetails.points_OrSmartDollars
					.replace(",", "").replace("\u2013", "-").replace("- ", "");
			//long points = Math.round(Double.parseDouble(points_smartDollars));
			int points_converted = (int) Math.round(Double.parseDouble(points_smartDollars));
			BaseUI.verify_true_AndLog(Double.parseDouble(points_smartDollars) > 0.0,
					"Points or Smart Dollars " + points_smartDollars + "  is greater than 0 and numeric value",
					"Points or Smart Dollars " + points_smartDollars + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRental_ReserveConfirmPage_PointsOrSmartDollars", points_OrSmartDollars,
					String.valueOf(points_converted));
		}

		String avgBaseRate_PerDay = confirmation_PriceDetails.avgBaseRate.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(avgBaseRate_PerDay) > 0.0,
				"Avg Base Rate " + avgBaseRate_PerDay + "  is greater than 0 and numeric value",
				"Avg Base Rate " + avgBaseRate_PerDay + " is not greater than 0");
		BaseUI.baseStringCompare("carRental_ReserveConfirmPage_RentalChargePerDay", avgBaseRate,
				avgBaseRate_PerDay);

		String youPayPrice = confirmation_PriceDetails.rentalCharge.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(youPayPrice) > 0,
				"Rental Charge " + youPayPrice + "  is greater than 0 and numeric value",
				"Rental Charge " + youPayPrice + "  is not greater than 0 and numeric value");
		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_RentalCharge", youPay, youPayPrice);

		String finalPrice = confirmation_PriceDetails.totalWithTax.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(finalPrice) > 0,
				"Total with tax " + finalPrice + "  is greater than 0 and numeric value",
				"Total with tax " + finalPrice + "  is not greater than 0 and numeric value");
		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", totalWithTax, finalPrice);

		String taxesOrFees = confirmation_PriceDetails.taxes_OrFees.replace("+ ", "").replace("$", "");
		String FinalPriceAfterTaxes = String.valueOf(BaseUI
				.convertDouble_ToString_ForCurrency(Double.parseDouble(youPayPrice) + Double.parseDouble(taxesOrFees)));
		BaseUI.baseStringCompare("carRental_ReserveConfirmPage_TotalPriceWithTax", finalPrice,
				FinalPriceAfterTaxes);
		BaseUI.verify_true_AndLog((Double.parseDouble(FinalPriceAfterTaxes) == Double.parseDouble(finalPrice)),
				"Total with tax " + FinalPriceAfterTaxes + " match " + finalPrice,
				"Total with tax " + FinalPriceAfterTaxes + " does not match " + finalPrice);

	}
	
	
	//Check Result page and choose 1st available car rental company
    public static void verify_CarRentalCompany_PriceDetails_ConfirmationPage(String driverFirstName, String driverLastName,
    		String driverEmail, String airlineName, String airlineNum) throws Exception {
    	
    	TableData carRentalData;

    	String avgBaseRate;
    	String points_OrSmartDollars;
    	String youPay;
    	String totalWithTax;
    	
        if(BaseUI.elementAppears(Locator.lookupOptionalElement("carRentalSearchResult_ListViewResult_AvisLogo"))) {
			
        	carRentalData = CarRental_ModifySearchResults.get_Table_Data_Avis();
		}
		else {
			
			carRentalData = CarRental_ModifySearchResults.get_Table_Data_Budget();
		}
    		
    	HashMap<String, String> carRentalRow = carRentalData.data.get(0);
       
        avgBaseRate = carRentalRow.get("avgBaseRate").replace("$", "");
        points_OrSmartDollars = carRentalRow.get("points_Or_SmartDollars").replace("%", "")
				.replace("\u2013", "-")
				.replace("– ", "");
        youPay = carRentalRow.get("youPay").replace("$", "");
        totalWithTax = carRentalRow.get("totalWithTax").replace("$", "");
        
        CarRental_ModifySearchResults.reserveCar_From_AvisOrBudget();
        
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_HeaderTitle"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_CheckoutPage_HeaderTitle", "Car Rentals | Additional Information");
		
		CarRentalSearch_Checkout.verify_AvisCarRentalCheckout_PriceDetails(avgBaseRate, points_OrSmartDollars, 
				youPay, totalWithTax);	
				
		//Enter Driver details and Airline details
	    CarRentalSearch_Checkout.EnterDriverDetails(driverFirstName, driverLastName, driverEmail);
		CarRentalSearch_Checkout.EnterAirlineDetails(airlineName, airlineNum);
		String driverEmailAddress = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_Email"));
		CarRentalSearch_Checkout.clickCarRentalSearchResult_CheckoutPage_ReserveCarBtn();
								
		//Validate Confirmation Message after completing the reservation
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmMsg"));
		
		//Validating driver's email address on confirmation page
		String driver_Email = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_DriverEmail"));
		BaseUI.verify_true_AndLog(driver_Email.contains(driverEmailAddress), 
				 " Driver email on reservation page MATCH Driver email entered " + driverEmailAddress, 
				 " Driver email On reservation page DOES NOT MATCH Driver email entered " + driverEmailAddress);
        
		//Verify price details on confirmation page match with checkout page.
		verify_CarRentalConfirmationPage_PriceDetails(avgBaseRate, points_OrSmartDollars, youPay, totalWithTax);
    }
    
    
    public static void verifyDriversName_ReserveConfirmationPage(String driverFirstName,
    		String driverLastName) throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_YourReservationInfo_Header"));
		
		String driverName = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_DriverName"));
		BaseUI.verify_true_AndLog(driverName.contains(driverFirstName + " " + driverLastName),
				driverName + " Driver name on reservation page MATCH Driver name entered " + driverFirstName + " " + driverLastName,
				driverName + " Driver name On reservation page DOES NOT MATCH Driver name entered " + driverFirstName + " " + driverLastName);
	}
    
    
    //Validate Confirmation message on Resevation Confirm page
    public static void verifyConfirmationMessage_ReserveConfirmationPage()
			throws Exception {

		String confirmationMsgText = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmMsg"));
		BaseUI.verify_true_AndLog(confirmationMsgText.equals("Your reservation has been completed."),
				"Confirmation message " + confirmationMsgText + " is displayed ",
				"Confirmation message " + confirmationMsgText + " is not displayed ");
	}
    
    
    public static void verifyCarRentalTitleHeader_ReserveConfirmationPage()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_HeaderTitle"));
		String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_HeaderTitle"));

		BaseUI.verify_true_AndLog(
				headerTitle.equals("Your Avis Rental Car") || headerTitle.equals("Your Budget Rental Car"),
				"Header title " + headerTitle + " is Displayed", "Header title " + headerTitle + " is not Displayed");
	}
    
    //Validate car class match selected in car rental search
    public static void verifyCarClass_ReserveConfirmationPage(String carClassOptionDisplayed) throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_CarClass"));
		String carClassDisplayed_ReserveConfirmationPage = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_CarClass"))
				.replaceAll("Car Class: ", "").trim();
		BaseUI.verify_true_AndLog(
				carClassDisplayed_ReserveConfirmationPage.contains(carClassOptionDisplayed),
				"Car Class displayed on checkout page matches " + carClassOptionDisplayed,
				"Car Class displayed on checkout page does not match " + carClassOptionDisplayed);
	}
    
    
    //Validate pickup date match search criteria
    public static void verifyPickUpDateAndTime_CarRentalSearchResult_ReserveConfirmationPage(String pickUpDateDisplayed, 
    		String pickUpTimeDisplayed) throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PickUpDateTime"));
		String pickUpDate_Time = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PickUpDateTime"));
		String pickUpDate = BaseUI.return_Date_AsDifferentFormat(pickUpDateDisplayed, "MM/dd/yyyy",
				"MMMM dd, yyyy");

		BaseUI.verify_true_AndLog(pickUpDate_Time.contains(pickUpDate + "-" + pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page match "
						+ (pickUpDate + "-" + pickUpTimeDisplayed),
				"Pick Up date and Time displayed on checkout page does not match "
						+ (pickUpDate + "-" + pickUpTimeDisplayed));
	}
    
    
    //Validate return date and time match search criteria
    public static void verifyReturnDateAndTime_CarRentalSearchResult_ReserveConfirmationPage(String returnDateDisplayed, 
    		String returnTimeDisplayed) throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ReturnDateTime"));
		String returnDate_Time = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ReturnDateTime"));
		String returnDate = BaseUI.return_Date_AsDifferentFormat(returnDateDisplayed, "MM/dd/yyyy",
				"MMMM dd, yyyy");

		BaseUI.verify_true_AndLog(returnDate_Time.contains(returnDate + "-" + returnTimeDisplayed),
				"Return date and Time displayed on checkout page match "
						+ (returnDate + "-" + returnTimeDisplayed),
				"Return date and Time displayed on checkout page does not match "
						+ (returnDate + "-" + returnTimeDisplayed));
	}
    
    
    //Validate pickup airport location
    public static void verifyAirport_PickUpAddress_CarRentalSearchResult_ReserveConfirmationPage(String airportLocation)
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PickUpLocation"));
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_PickUpLocation", 
					airportLocation.replaceAll("Avis", "").replaceFirst("\n", "").replaceAll("Budget", "").trim());
		} else {
			BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_PickUpLocation", airportLocation);
		}
	}
    
    
    //Validate return airport location
    public static void verifyAirport_ReturnAddress_CarRentalSearchResult_ReserveConfirmationPage(String airportLocation)
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ReturnLocation"));
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_ReturnLocation", 
					airportLocation.replaceAll("Avis", "").replaceFirst("\n", "").replaceAll("Budget", "").trim());
		} else {
			BaseUI.verifyElementHasExpectedPartialText("carRental_ReserveConfirmPage_PickUpLocation", airportLocation);
		}
	}
}
