package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class LPG_Page {

    public static void verifyLPG_HeaderTitle() {
        String titleExpected = "Lowest Price Guarantee";
        String subTitleExpected = "";
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            titleExpected = "Low Price Guarantee";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            titleExpected = "Best Price, Period.";
            subTitleExpected = "You won't find a better price online. We guarantee it.";
        }

        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)) {
            WebElement guaranteeElement = Locator.lookupRequiredElement("lpgPage_Allstate_GuaranteeElement");
            BaseUI.verifyElementAppears(guaranteeElement);
            String expectedImageURLMatch = "/img/allstate/lpg/landing-lpg-guarantee.png";
            String actualURL = BaseUI.get_CSSAttribute_FromField(guaranteeElement, "background-image");
            BaseUI.baseStringPartialCompare("Guarantee Image", expectedImageURLMatch, actualURL);
        } else if (ClientDataRetrieval.isRedesignClient()) {
            String expectedImageLogo = "img/icons/lpg-icon.png";
            WebElement aarpRedLogoElement = Locator.lookupRequiredElement("lpg_Logo");
            BaseUI.verifyElementAppears(aarpRedLogoElement);
            String actualImageLogo = BaseUI.get_Attribute_FromField(aarpRedLogoElement, "src");
            BaseUI.baseStringPartialCompare("LPG Logo", expectedImageLogo, actualImageLogo);

            String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));
            BaseUI.baseStringPartialCompare("lpgPage_Title", titleExpected, lpgTitle);

            String lpgSubTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpg_LogoHeaderDescription"));
            BaseUI.baseStringPartialCompare("lpg_LogoHeaderDescription", subTitleExpected, lpgSubTitle);
        } else {
            String lpgTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpgPage_Title"));

            BaseUI.baseStringPartialCompare("lpgPage_Title", titleExpected, lpgTitle);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpgPage_Content"));
        }
    }

    public static void click_TravelTab() throws Exception {
        if(!ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            BaseUI.click(Locator.lookupRequiredElement("lpgPage_TravelTabLink"));
            BaseUI.waitForElementToContain_PartialAttributeMatch("lpgPage_TravelTab_Content", null, null, "class", "active", 10);
            Thread.sleep(500);
        }
    }

    public static void validate_Travel_Active() {
        //Citi doesn't have separate tabs, validation skipped
        if(!ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            WebElement travelContentElement = Locator.lookupRequiredElement("lpgPage_TravelTab_Content");

            BaseUI.verifyElementAppears(travelContentElement);
            BaseUI.verifyElementHasExpectedPartialAttributeValue(travelContentElement, "class", "active");
            String backgroundImageExpectedURLMatch = "/img/allstate/lpg/landing-lpg-travel-tab-content.png";
            String actualBackgrounImageURL = BaseUI.get_CSSAttribute_FromField(travelContentElement, "background-image");
            BaseUI.baseStringPartialCompare("Travel Background Content Image", backgroundImageExpectedURLMatch, actualBackgrounImageURL);
        }
    }

    public static void validate_Merchandise_Active() {
        if (ClientDataRetrieval.isLegacyClient() && !ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            WebElement merchandiseContentElement = Locator.lookupRequiredElement("lpgPage_MerchandiseTab_Content");
            BaseUI.verifyElementAppears(merchandiseContentElement);
            BaseUI.verifyElementHasExpectedPartialAttributeValue(merchandiseContentElement, "class", "active");
            String backgroundImageExpectedURLMatch = "/img/allstate/lpg/landing-lpg-merch-tab-content.png";
            String actualBackgroundImageURL = BaseUI.get_CSSAttribute_FromField(merchandiseContentElement, "background-image");
            BaseUI.baseStringPartialCompare("Merchandise Background Content Image", backgroundImageExpectedURLMatch, actualBackgroundImageURL);
        } else if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            BaseUI.log_Status("Citi client doesn't have separate tabs for Merchandise and Travel, validation skipped.");
        } else {
            WebElement topHeaderElement = Locator.lookupRequiredElement("lpg_TopSection_Title");
            BaseUI.verifyElementAppears(topHeaderElement);
            BaseUI.verifyElementHasExpectedText(topHeaderElement,"We do the work so you don't have to...");
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_TopSection_Logo"));

            WebElement midHeaderElement = Locator.lookupRequiredElement("lpg_MidSection_Title");
            BaseUI.verifyElementAppears(midHeaderElement);
            BaseUI.verifyElementHasExpectedText(midHeaderElement,"Once we find the lowest price, we apply your points to make your price even better.");
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_MidSection_Logo"));

            WebElement bottomHeaderElement = Locator.lookupRequiredElement("lpg_BottomSection_Title");
            BaseUI.verifyElementAppears(bottomHeaderElement);
            BaseUI.verifyElementHasExpectedText(bottomHeaderElement,"Found a better price? We'll make it right!");
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_BottomSection_Logo"));

            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_SubmitAClaim_Button"));
            BaseUI.verifyElementHasExpectedText("lpg_SubmitAClaim_Button","Submit a Claim");
        }
    }
}//End of Class
