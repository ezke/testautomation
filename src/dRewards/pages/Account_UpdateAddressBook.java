package dRewards.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class Account_UpdateAddressBook {

	// Pass in the name for the address and it will delete the address.
	public static void delete_Address_ByPassedInName(String userName) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("accnt_addr_DeleteButton_ByPassedInText", userName, null));
		Thread.sleep(500);
		BaseUI.click(Locator.lookupRequiredElement("accnt_addr_DeleteYes_ConfirmButton"));
		Thread.sleep(1000);
		if (ClientDataRetrieval.isLegacyClient()){
			BaseUI.click(Locator.lookupRequiredElement("accnt_addr_CloseDeletePopup"));
			Thread.sleep(500);
		}
	}

	// will delete all address listed in UpdateAddressBook page
	public static void delete_Address() throws Exception {

		ArrayList<WebElement> remove_Link_List = new ArrayList<WebElement>();
		remove_Link_List.addAll(Locator.lookup_multipleElements("accnt_addr_DeleteButton", null, null));

		for (int i = 0; i < remove_Link_List.size(); i++) {
			BaseUI.click(Locator.lookupRequiredElement("accnt_addr_DeleteButton"));
			BaseUI.waitForElementToBeDisplayed("accnt_addr_DeleteYes_ConfirmButton", null, null);
			Thread.sleep(500);
			BaseUI.click(Locator.lookupRequiredElement("accnt_addr_DeleteYes_ConfirmButton"));
			BaseUI.waitForElementToNOTBeDisplayed("accnt_addr_DeleteYes_ConfirmButton", null, null);
			Thread.sleep(500);
			if (ClientDataRetrieval.isLegacyClient()) {
				BaseUI.click(Locator.lookupRequiredElement("accnt_addr_CloseDeletePopup"));
				BaseUI.waitForElementToNOTBeDisplayed("accnt_addr_CloseDeletePopup", null, null);
				Thread.sleep(500);
			}
		}

	}

	public static void click_AddNewAddress() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("accnt_AddAddress_Button"));
		Thread.sleep(1000);
	}

	public static void add_Address_AndSetToDefault(String firstName, String lastName, String address1, String country,
			String city, String state, String zip, String phone) throws Exception {
		click_AddNewAddress();
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_firstName"), firstName);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_lastName"), lastName);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_address1"), address1);
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("accnt_newAddr_country_Dropdown"), country);
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("accnt_newAddr_state_Dropdown"), state);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_city"), city);
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_zip"), zip);
			BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_phone"), phone);
		} else {
			BaseUI.tabThroughField("accnt_newAddr_city");
			WebElement stateDropdownActive = Browser.driver.switchTo().activeElement();
			BaseUI.tabThroughField(stateDropdownActive);
			WebElement zipCode = Browser.driver.switchTo().activeElement();
			BaseUI.enterText_IntoInputBox(zipCode, zip);
			BaseUI.tabThroughField(zipCode);
			WebElement phoneNumber = Browser.driver.switchTo().activeElement();
			BaseUI.enterText_IntoInputBox(phoneNumber, phone);
		}
 		

		click_Submit_NewAddress();

		set_Address_ToDefault_BasedOnNameText(firstName);
	}

	public static void click_Submit_NewAddress() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("accnt_newAddr_Confirm_Button"));
		Thread.sleep(1000);
	}

	public static void set_Address_ToDefault_BasedOnNameText(String firstName) throws Exception {
		BaseUI.checkCheckbox(Locator.lookupRequiredElement("accnt_MakeDefaultAddress_Radio_ByNameText", firstName, null));
		Thread.sleep(1000);
	}


	//For AARP Redesign only
	public static void add_Address_AndSetToDefault(String firstName, String lastName, String address1,
												   String city, String zip, String phone) throws Exception {
		click_AddNewAddress();
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_firstName"), firstName);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_lastName"), lastName);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_address1"), address1);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_city"), city);

		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_zip"), zip);
		BaseUI.enterText(Locator.lookupRequiredElement("accnt_newAddr_phone"), phone);

		click_Submit_NewAddress();
	}

	public static void clickEditButton() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("accnt_addr_EditButton"));
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("accnt_newAddr_firstName", null, null, 30);
	}

}// End of Class
