package dRewards.pages;

import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages_Administration.CancelRefundReOrder_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Shopping_SearchOrders_Admin;
import dRewards.pages_Administration.ViewOrder_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.Parameter;

public class Account {

	public static Boolean does_UpdatePaymentOptionsExist() {
		Boolean paymentOptionsExist = null;

		if (BaseUI.pageSourceContainsString("Update Payment Options")) {
			paymentOptionsExist = true;
		} else {
			paymentOptionsExist = false;
		}

		return paymentOptionsExist;
	}

	public static void clear_PaymentHistory() throws InterruptedException {
		if (does_UpdatePaymentOptionsExist()) {
			navigate_toUpdatePaymentOptions();
			Account_UpdatePaymentOptions.delete_all_PaymentOptions();
		}
	}

	public static void navigate_toUpdatePaymentOptions() throws InterruptedException {
		BaseUI.click(Locator.lookupRequiredElement("accnt_UpdatePaymentOptions_Link"));
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
	}

	// Cancel Hotel Reservations.
	public static void click_CancelReservation() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("accnt_Row1_TransactionCancelLink"));
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void cancel_Order(@NotNull String orderNumber) throws Exception {
		Parameter.named("orderNumber").withValue(orderNumber).mustBeNonNull();

		String shortenedOrderNumber = orderNumber.substring(orderNumber.indexOf("-") + 1, orderNumber.length());

		Navigation.navigate_Account();
		if (BaseUI.elementExists("accnt_CancelOrder_ByOrderID", shortenedOrderNumber, null)) {
			BaseUI.click(Locator.lookupRequiredElement("accnt_CancelOrder_ByOrderID", shortenedOrderNumber, null));
			Thread.sleep(1000);

			if (ClientDataRetrieval.isRedesignClient()) {
				BaseUI.click(Locator.lookupRequiredElement("accnt_Row1_TransactionDetailsLink"));
				BaseUI.waitForElementToBeDisplayed("accnt_OrderDetails_CancelOrderButton", null, null, 20);
			}
			Account_OrderAndShippingDetails_Page.cancel_Order();

			// BaseUI.click(Locator.lookupRequiredElement("accunt_HistoryLink"));
			// Thread.sleep(1000);
		} else {
			String mainHandle = Browser.driver.getWindowHandle();
			WebDriver originalDriver = Browser.driver;
			try {

				Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
				LoginPage_Admin.login_Admin();
				Navigation_Admin.navigate_Shopping_SearchOrders();
				Shopping_SearchOrders_Admin.search_Order_ByOrderID(orderNumber);
				Shopping_SearchOrders_Admin.clickOrderLink_ByOrderID(orderNumber);
				ViewOrder_Admin.cancelOrder();
				CancelRefundReOrder_Admin.cancel_Order();

			} finally {
				// Browser.close_CurrentBrowserWindow();
				Browser.closeBrowser();
				Browser.changeDriver(originalDriver, Browser.currentBrowser);
				BaseUI.switch_ToWindow(mainHandle);
			}
		}
	}

	public static void clickCancel_Order_NoLink() throws InterruptedException {

		BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CancelReservation_NoLink"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}

	public static void click_Cancel_ByOrderNumber(String orderNumber) throws Exception {
		orderNumber = return_Shortened_OrderNumber(orderNumber);

		BaseUI.click(Locator.lookupRequiredElement("accnt_CancelOrder_ByOrderID", orderNumber, null));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}

	public static void click_ViewDetails_ByOrderNumber(@NotNull String orderNumber) throws Exception {
		Parameter.named("orderNumber").withValue(orderNumber).mustBeNonNull();

		orderNumber = return_Shortened_OrderNumber(orderNumber);

		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("accnt_ViewDetails_ByOrderID", orderNumber, null));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("accnt_ViewDetails_ByOrderID", orderNumber, null));
		}
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}

	public static String return_Shortened_OrderNumber(@NotNull String orderNumber) {
		Parameter.named("orderNumber").withValue(orderNumber).mustBeNonNull();

		if (orderNumber.contains("-")) {
			orderNumber = orderNumber.substring(orderNumber.indexOf("-") + 1, orderNumber.length());	
		}

		return orderNumber;
	}
	
	//Click Cancel Reservation for car rental and click Yes option
	public static void click_CancelReservation_CarRental_YesOption() throws Exception {
		if(ClientDataRetrieval.isRedesignClient()) {
			clickDetailsText();
			BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"));
			BaseUI.waitForElementToBeDisplayed("myAccount_carRental_CancelReservation_YesLink", null, null, 20);
		} else {
			BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"), true, 1500);
		}
		clickCancelReservation_CarRental_YesLink();
	}
	
	//Click Cancel Reservation for car rental
	public static void click_CancelReservation_CarRental() throws Exception {
		if(ClientDataRetrieval.isRedesignClient()) {
//			clickDetailsText();
			BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"));
			BaseUI.waitForElementToBeDisplayed("myAccount_carRental_CancelReservation_YesLink", null, null, 20);
		} else {
			BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"), true, 500);
		}
	}
	
	
	public static void clickCancelReservation_CarRental_YesLink() throws InterruptedException {
		BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CancelReservation_YesLink"));
		Thread.sleep(2000);
	}
	
	
    public static void clickCancelReservation_CarRental_NoLink() throws InterruptedException {
		
		BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CancelReservation_NoLink"));
		Thread.sleep(2000);
	}
    
    
   //Click Cancel Reservation for car rental and click No in pop-up
  	public static void click_CancelReservation_CarRental_NoOption() throws Exception {
        if(ClientDataRetrieval.isRedesignClient()) {
            //clickDetailsText();
            BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"));
            BaseUI.waitForElementToBeDisplayed("myAccount_carRental_CancelReservation_NoLink", null, null, 20);
        } else {
            BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"), true, 500);
        }
  		clickCancelReservation_CarRental_NoLink();
  	}
	
	
	public static void verifyCarRentalDetail_CancelReservation() throws Exception {
		if(ClientDataRetrieval.isRedesignClient()) {
			navigate_MyAccountPage_ForAARPRedesign();
		} else {
			Navigation.navigate_Account();
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
		
		//Click cancel reservation link, switch to pop up window
		//and click Yes option

		String currentWindow = Browser.driver.getWindowHandle();
		Account.click_CancelReservation_CarRental_YesOption();
		BaseUI.switch_ToWindow(currentWindow);

		BaseUI.waitForElementToBeDisplayed("myAccount_carRental_ReservationCancelled_Text", null, null);
		
		//Validate Car rental booking/reservation is cancelled. 
		verifyCarRental_ReservationCancelled_Text();
	}
	
	
	public static void verifyCarRental_ReservationDatesMatch(String pickupDate, String returnDate, String airportAddress) throws Exception {
		
		//String datesDisplayed = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"), "innerText");//.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"));
		String pickup_Date = BaseUI.return_Date_AsDifferentFormat(pickupDate, "MM/dd/yyyy", "yyyy-MM-dd");
		String return_Date = BaseUI.return_Date_AsDifferentFormat(returnDate, "MM/dd/yyyy", "yyyy-MM-dd");
        String dates_AirportDetails_Displayed;

		if(ClientDataRetrieval.isRedesignClient()) {
            dates_AirportDetails_Displayed =  BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_Description"));
            String airportLocation = airportAddress.split(",")[0];
            String airportCode = airportAddress.split(",")[1];
            if(dates_AirportDetails_Displayed.contains("Avis")){
                BaseUI.baseStringPartialCompare("myAccount_carRental_CarDetails_Description",
                        (pickup_Date + " thru " +  return_Date + " Avis - " + airportLocation + "," + airportCode), dates_AirportDetails_Displayed);
            } else {
                BaseUI.baseStringPartialCompare("myAccount_carRental_CarDetails_Description",
                        (pickup_Date + " thru " +  return_Date + " Budget - "+ airportLocation + "," + airportCode), dates_AirportDetails_Displayed);
            }
        } else {
            dates_AirportDetails_Displayed =  BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"));
            if(dates_AirportDetails_Displayed.contains("Avis")){
                BaseUI.baseStringPartialCompare("myAccount_carRental_CarDetails_DatesAndName",
                        (pickup_Date + " thru " +  return_Date + "\n" + "Avis - "+ airportAddress), dates_AirportDetails_Displayed);
            } else {
                BaseUI.baseStringPartialCompare("myAccount_carRental_CarDetails_DatesAndName",
                        (pickup_Date + " thru " +  return_Date + "\n" + "Budget - "+ airportAddress), dates_AirportDetails_Displayed);
            }
        }
	}
	
	
	public static void verifyCarRental_ReservationCancelled_Text() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ReservationCancelled_Text"));
		String cancelReservation = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ReservationCancelled_Text")).trim();
		if(ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("Reservation Cancelled", "Reservation Cancelled", cancelReservation);
		} else {
			BaseUI.baseStringCompare("Reservation Cancelled", "Reservation canceled", cancelReservation);
		}
	}
	
	
	//Click Cancel Reservation for car rental
	public static void click_CarRental_ViewConfirmationLink() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            clickDetailsText();
            BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_ViewConfirmLink"));
            Thread.sleep(200);
            BaseUI.switch_ToIframe(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_Iframe"));
            Thread.sleep(300);
        } else {
            BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_ViewConfirmLink"),
                    true, 500);
        }
		Browser.driver.manage().window().maximize();
	}
	
	
	public static void navigateAccountPage_ClickViewConfirm_CarRental() throws Exception {
		//Navigation.navigate_Account();
		Account.click_CarRental_ViewConfirmationLink();

		//Added these lines to check if View confirm page 
		//does contain throw Application error. If yes log status and fails the test.
        if (BaseUI.pageSourceContainsString("There has been an error in the application. Please call customer support for assistance.")) {
			BaseUI.log_AndFail("View Confirmation Validation Fail - There has been an error in the application. Please call customer support for assistance.");		
		}
	}
	
	//Validate points for Car rental reservation
    public static void verifyPointsDisplayed_Match_ReservationPagePoints(String points_OrSmartDollars) throws Exception {
    	
    	String points;
		Navigation.navigate_Account();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"));
		if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			points = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"))
					.replace("- ", "").replace(",", "").replace("$", "").trim();
		} else {

			points = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"))
					.replace("- ", "").replace(",", "").replace("$", "").replace(".00", "").trim();
		}
	}
    
    
    //Validate View Confirm Link for car rental reservation
    public static void verifyViewConfirmLink_CarRentalReservation() throws Exception {
        String expectedViewConfirmText;
		if(ClientDataRetrieval.isRedesignClient()) {
			Account.navigate_MyAccountPage_ForAARPRedesign();
			Account.clickDetailsText();
            expectedViewConfirmText = "VIEW RECEIPT";
		} else {
			Navigation.navigate_Account();
            expectedViewConfirmText = "View Confirm";
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_ViewConfirmLink"));
		String viewConfirm_Text = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_ViewConfirmLink"));
		BaseUI.baseStringCompare("myAccount_carRental_CarDetails_ViewConfirmLink", expectedViewConfirmText, viewConfirm_Text);
	}
    
    
    public static void verifyCarRental_ReservationCancelled_Date() throws Exception {
		String todayDate;

		if(ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"));
			String carRentalReservationCancellationDate = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"));
			todayDate = BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/dd/yyyy", "MMM"+"\n"+"dd");
			BaseUI.baseStringCompare("Car Rental reservation cancelled Details", todayDate.toUpperCase(), carRentalReservationCancellationDate);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
			String carRentalDetails = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
			todayDate = BaseUI.return_Date_AsDifferentFormat(BaseUI.getTodaysDate_AsString(), "MM/dd/yyyy", "yyyy-MM-dd");
			BaseUI.baseStringCompare("Car Rental reservation cancelled Details", (todayDate + "\n" + "AVISBUDGET CANCEL"), carRentalDetails);
		}
	}
    
    
    public static void verifyCarRental_ReservationCancelled_Points(String pointsBeforeCancellation) throws Exception {
    	if(! ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
    		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"));
    		String pointsOrSmartDollars = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"));
    		
    		BaseUI.baseStringCompare("Points after Car Rental reservation cancelled", pointsBeforeCancellation, pointsOrSmartDollars);
    	}		
	}
    
    
    public static void verifyCarRental_ReservationNotCancelled_CarBookingDetails(String todayDate) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
		String carBookingDetails_AfterReservationNotCancelled;

		if(ClientDataRetrieval.isRedesignClient()) {
			carBookingDetails_AfterReservationNotCancelled = BaseUI.getTextFromField(
					Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"));
			BaseUI.baseStringCompare("myAccount_carRental_CarDetails_DatesAndName", todayDate.toUpperCase(),
					carBookingDetails_AfterReservationNotCancelled);
		} else {
			carBookingDetails_AfterReservationNotCancelled = BaseUI.getTextFromField(
					Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
			BaseUI.baseStringCompare("myAccount_carRental_CarDetails", (todayDate + "\n" + "AVISBUDGET BOOKING"),
					carBookingDetails_AfterReservationNotCancelled);
		}
	}
    
    
    public static void verifyCarRental_ReservationNotCancelled_Points(String points) throws Exception {
    	if(! ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
    		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"));
            String points_AfterReservationNotCancelled = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"));
    				
    		BaseUI.baseStringCompare("Points after reservation not cancelled", points, points_AfterReservationNotCancelled);
    	}		
	}
    
    
    public static void verify_TotalPointsOrSmartDollars_ToSpend(String totalPointsOrSmartRewardsToSpend) throws Exception {
    	if(! ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
    		String pointsBeforeCancellation;
    		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) || 
    				ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
    				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
    			pointsBeforeCancellation = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"))
    					.replace("- ", "").replace(",", "").trim();
    			//pointsBeforeCancellation = String.valueOf(Double.parseDouble(points_RewardPoints)/100);
    			String expectedTotal_PointsAfterCancellation = String.valueOf(
        				Integer.parseInt(totalPointsOrSmartRewardsToSpend) + Integer.parseInt(pointsBeforeCancellation))
        				.replace(",", "").replace("$", "");
        		Navigation.verify_pointsMatchExpected(expectedTotal_PointsAfterCancellation);
    		} else {
    			pointsBeforeCancellation = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"))
    					.replace("- ", "").trim();
    			String expectedTotal_PointsAfterCancellation = BaseUI.convertDouble_ToString_ForCurrency(
        				Double.parseDouble(totalPointsOrSmartRewardsToSpend) + Double.parseDouble(pointsBeforeCancellation))
        				.replace(",", "").replace("$", "");
        		Navigation.verify_pointsMatchExpected(expectedTotal_PointsAfterCancellation);
    		}
    	}
    }
    
    
    public static void verify_MyAccount_CancelReservation_YesAndNoLink() throws Exception {
    	//Navigation.navigate_Account();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
		
		click_CancelReservation_CarRental();
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CancelReservation_NoLink"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CancelReservation_YesLink"));
		//BaseUI.switch_ToWindow(primaryWindow);
    }
    
    
    public static void verify_MyAccount_CarBookingNotCancelled_Date() throws Exception {
    	Navigation.navigate_Account();
    	BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));

        String carDetails, todayDate;
        if (ClientDataRetrieval.isRedesignClient()) {
            carDetails = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_DatesAndName"));
            todayDate = BaseUI.return_Date_AsDifferentFormat(
                    BaseUI.getTodaysDate_AsString(), "MM/dd/yyyy", "MMM"+"\n"+"dd");
            BaseUI.baseStringCompare("myAccount_carRental_CarDetails",
                    todayDate.toUpperCase(), carDetails);
        } else {
            carDetails = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
            todayDate = BaseUI.return_Date_AsDifferentFormat(
                    BaseUI.getTodaysDate_AsString(), "MM/dd/yyyy", "yyyy-MM-dd");
            BaseUI.baseStringCompare("myAccount_carRental_CarDetails",
                    (todayDate + "\n" + "AVISBUDGET BOOKING"), carDetails);
        }

		//Click Cancel Reservation, switch to popup window
		//and click No option
		String primaryWindow = Browser.driver.getWindowHandle();
		click_CancelReservation_CarRental_NoOption();
		BaseUI.switch_ToWindow(primaryWindow);
		
		//Validate Car Rental booking is not cancelled
		verifyCarRental_ReservationNotCancelled_CarBookingDetails(todayDate);
    }
    
    
    public static void cancelOrderAndValidateCancelledOrder(String orderID, PricingDetails pricingDetails, 
    		String salesTaxCost) throws Exception {
    	Account.cancel_Order(orderID);
		Navigation.navigate_Logout();
		//For AARPRedesign when logged out the test is failing due to connection lost error,
		//So killing browser session and opening new browser session
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			Browser.closeBrowser();
			Browser.openBrowser(ClientDataRetrieval.url);
			BaseUI.close_ExtraWindows(Browser.driver.getWindowHandle());
		} else {
			Browser.navigateTo(ClientDataRetrieval.url);
		}
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Account();
		Account.click_ViewDetails_ByOrderNumber(orderID);
		BaseUI.waitForElementToBeDisplayed("OrderAndShip_CancelledTitle", null, null);
		Thread.sleep(500);
		if (pricingDetails.order_Total.equals("$0.00")) {
			Account_OrderAndShippingDetails_Page.verify_Cancelled_FreeProducts(pricingDetails);
		} else {
			Account_OrderAndShippingDetails_Page.verify_Cancelled(pricingDetails, salesTaxCost);
		}
    }


    public static void navigate_MyAccountPage_ForAARPRedesign() throws Exception {
		Navigation.navigate_Home_viaHomeLink();
		Navigation.navigateToMyRewards();
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAccount_RecentTransactionsHeader"));
		Thread.sleep(300);
	}

	public static void clickDetailsText() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("myAccount_carRental_CarDetails"));
		BaseUI.waitForElementToBeDisplayed("myAccount_carRental_CancelReservationLink", null, null, 30);
		Thread.sleep(300);
	}

	public static void clickCloseIconToClose_ViewConfirm_Or_CancelBookingModal() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_CloseIcon"));
		Thread.sleep(500);
	}
}// End of class
