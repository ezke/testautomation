package dRewards.pages;

import utils.Locator;
import utils.TableData;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import dRewards.ClassObjects.PricingDetails;
import dRewards.ClassObjects.Product;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;

public class CheckoutPage_ReviewOrder {

	// Return a List of our Product Classes that contains all of the product
	// data.
	public static ArrayList<Product> retrieve_AllProductData() {
		ArrayList<Product> products = new ArrayList<Product>();
		ArrayList<WebElement> productRows = new ArrayList<WebElement>();
		productRows.addAll(Locator.lookup_multipleElements("checkout_ProductRows", null, null));

		for (WebElement productRow : productRows) {
			Product product = new Product();
			product.productName = productRow.findElement(By.xpath(".//div[@class='infoWrap']/a")).getText();
			product.quantity = productRow.findElement(By.xpath(".//li[@class='qty_cart_item']")).getText();
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				product.pricing_Retail = productRow.findElement(By.xpath(".//td[@class='retail']")).getText();
				product.pricing_Points = productRow.findElement(By.xpath(".//span[@class='incentives']")).getText();
				product.pricing_YouPay = productRow.findElement(By.xpath(".//td[@class='price']")).getText();	
			}else{
				product.pricing_Retail = productRow.findElement(By.xpath(".//dd[@class='retail']")).getText();
				product.pricing_Points = productRow.findElement(By.xpath(".//dd[@class='incentives']")).getText();
				product.pricing_YouPay = productRow.findElement(By.xpath(".//dd[@class='price']")).getText();
			}
			
			// product.pricing_PercentSavings =
			// productRow.findElement(By.xpath(".//div[@class='price-matrix']/span"))
			// .getText();

			products.add(product);
		}

		return products;
	}

	// gets the table data utilizing our generic Base Class for retrieving table
	// data.
	public static TableData get_Table_Data() throws Exception {
		HashMap<String, String> fields_toGet = new HashMap<String, String>();
				
	   if (ClientDataRetrieval.isRedesignClient()) {
		   if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
			   ||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))) {
			   fields_toGet.put("Product Name", ".//h3");
		   } else {
			   fields_toGet.put("Product Name", ".//h4");
		   }
		   fields_toGet.put("quantity", ".//ul[@class='order-details']/li[contains(text(),'Quantity')]/strong");
		   fields_toGet.put("pricing_Retail", ".//div[@class='price-matrix']/div[@class='retail-price']");
		   fields_toGet.put("pricing_Points", ".//div[@class='price-matrix']/div[@class='points-required']");
		   fields_toGet.put("pricing_YouPay", ".//div[@class='price-matrix']/div[@class='rewards-price']");
		   //fields_toGet.put("pricing_Savings", ".//div[@class='price-matrix']/div[@class='savings']");
		} else {
			fields_toGet.put("Product Name", ".//div[@class='infoWrap']/a");
			fields_toGet.put("quantity", ".//li[@class='qty_cart_item']");
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				fields_toGet.put("pricing_Retail", ".//td[@class='retail']");
				fields_toGet.put("pricing_Points", ".//span[@class='incentives']");
				fields_toGet.put("pricing_YouPay", ".//td[@class='price']");
			} else {
				fields_toGet.put("pricing_Retail", ".//dd[@class='retail']");
				fields_toGet.put("pricing_Points", ".//dd[@class='incentives']");
				fields_toGet.put("pricing_YouPay", ".//dd[@class='price']");
			}
		}
		
		// ArrayList<HashMap<String, String>> table_Data =
		// BaseUI.tableExtractor("checkout_ProductRows", fields_toGet);
		TableData table_Data = new TableData(BaseUI.tableExtractor("checkout_ProductRows", fields_toGet));

		if (ClientDataRetrieval.isRedesignClient()) {
			table_Data.remove_Character(" ", "pricing_Retail");
			table_Data.replace_Characters_FromBeginning("Retail:", "", "pricing_Retail");
			table_Data.remove_Character("points", "pricing_Points");
			table_Data.trim_column("pricing_Points");
			table_Data.replace_Characters_FromBeginning("after", "", "pricing_Points");
		}

		table_Data.replace_Characters_FromBeginning("Gift Card", "", "Product Name");
	
		return table_Data;
	}

	public static void check_IAgree_Checkbox() throws Exception{
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("checkout_IAgree_Message"));
		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.checkCheckbox("checkout_AgreeToTerms_Checkbox");
		}		
	}

	//If we don't have Allstate Cash just pass in null or $0.00.
	public static void verify_IAgreeMessage(String amount, String allstateCash) throws Exception {

		String copyrightSymbol = "\u00AE";
		String expectedMessage;

		String clientSpecificString = "Smart Rewards";
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			clientSpecificString = "Allstate Rewards" + copyrightSymbol;
		} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			clientSpecificString = "Pro Driver Rewards";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			clientSpecificString = "Citi Easy Deals";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARP) 
				|| ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			clientSpecificString = "Rewards for Good";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			clientSpecificString = "Streetwise Rewards";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			clientSpecificString = "Jeep Wave Savings Network";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			clientSpecificString = "Deluxe Rewards";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.McAfee)) {
			clientSpecificString = "McAfee Loyalty Rewards";
		}
		if (ClientDataRetrieval.isRedesignClient()) {
			expectedMessage = "By placing your order, you agree to the "+ clientSpecificString
					+ " Terms and Conditions including return policies, and acknowledge your credit card will be charged "
					+ amount;
		} else {
			expectedMessage = "I agree to the " + clientSpecificString
					+ " Terms and Conditions including return policies, and acknowledge my credit card will be charged "
					+ amount + ".";

			
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash) && allstateCash != null && !allstateCash.equals("$0.00")) {
				expectedMessage = "I agree to the " + clientSpecificString
						+ " Terms and Conditions including return policies, and acknowledge my credit card will be charged "
						+ amount + ", and I am applying " + allstateCash + " of my Cash Rewards balance to this order.";
			}
		}
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("checkout_IAgree_Message"));
		String actualMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_IAgree_Message"));

		BaseUI.baseStringCompare("I Agree Message", expectedMessage, actualMessage);

		// if (ClientDataRetrieval.clientName.startsWith("Smart rewards")) {
		// BaseUI.verifyElementHasExpectedText("checkout_IAree_Message",
		// "I agree to the Smart Rewards Terms and Conditions including return
		// policies, and acknowledge my credit card will be charged "
		// + amount + ".");
		//
		// }
		//
		// else if (ClientDataRetrieval.clientName.startsWith("Citi")) {
		// BaseUI.verifyElementHasExpectedText("checkout_IAree_Message",
		// "I agree to the Citi Easy Deals Terms and Conditions including return
		// policies, and acknowledge my credit card will be charged "
		// + amount + ".");
		// }
	}
	
	public static void verify_PromoCode_IAgreeMessage(){
		String clientSpecificString = "Smart Rewards";
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			clientSpecificString = "Allstate Rewards"+"\u00AE";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			clientSpecificString = "Rewards for Good";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			clientSpecificString = "Streetwise Rewards";
		}
		
		String expectedMessage = "I agree to the " + clientSpecificString
				+ " Terms and Conditions including return policies.";
		
		String actualMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_IAgree_Message"));

		BaseUI.baseStringCompare("I Agree Message", expectedMessage, actualMessage);
	}
	
	//This message appears only if Order total is 0.00
	public static void verify_DailyDeals_IAgreeMessages(){
		String clientSpecificString;
		String expectedMessage;
		if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			clientSpecificString = "Deluxe Rewards";
			expectedMessage = "By placing your order, you agree to the "+ clientSpecificString +
					" Terms and Conditions including return policies, and acknowledge your credit card will be charged $0.00";
		} else {
			clientSpecificString = "Allstate Rewards"+"\u00AE";
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				clientSpecificString = "Citi Easy Deals";
			} else if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				clientSpecificString = "Streetwise Rewards";
			} else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
				clientSpecificString = "Jeep Wave Savings Network";
			} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
				clientSpecificString = "Pro Driver Rewards";
			}
			
			expectedMessage = "I agree to the " + clientSpecificString
					+ " Terms and Conditions including return policies.";
		}		
		String actualMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_IAgree_Message"));

		BaseUI.baseStringCompare("I Agree Message", expectedMessage, actualMessage);
	}

	// retrieve Pricing Details for the Pricing totals.
	public static PricingDetails retrieve_PricingDetails() {
		PricingDetails pricingDetails = new PricingDetails();
		pricingDetails.retailPrice = Locator.lookupRequiredElement("shpCart_PriceDetails_RetailPrice").getText();
		pricingDetails.points = Locator.lookupRequiredElement("shpCart_PriceDetails_points").getText();
		pricingDetails.priceAfterPoints = Locator.lookupRequiredElement("shpCart_PriceDetails_priceAfterPoints").getText();
		pricingDetails.savings_Percent = Locator.lookupRequiredElement("shpCart_PriceDetails_savingsPercent").getText();
		pricingDetails.shipping_Cost = Locator.lookupRequiredElement("shpCart_PriceDetails_shippingCost").getText();
		pricingDetails.order_Total = Locator.lookupRequiredElement("checkout_FinalPrice").getAttribute("innerHTML");
		// Normal getText didn't work. Had to get the Inner HTML and then take
		// everything before the "<".
		pricingDetails.order_Total = pricingDetails.order_Total.split("\\<")[0];
		pricingDetails.tax_Cost = Locator.lookupRequiredElement("checkout_SalesTax").getText();

		return pricingDetails;
	}
	
	// retrieve Pricing Details for the Checkout Review order page - Pricing totals.
	// For Redesign clients Pricing Details element path vary from page to page
	// So added this method to fetch all the pricing details from checkout page
	public static PricingDetails retrieve_CheckoutReviewOrder_PricingDetails() {
		PricingDetails pricingDetails = new PricingDetails();
		pricingDetails.retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_RetailPrice"));
		pricingDetails.points = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_PointsDeduction"));
		pricingDetails.priceAfterPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_YouPay"));
		pricingDetails.savings_Percent = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_PercentSavings"));
		pricingDetails.shipping_Cost = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_ShippingCost"));
		pricingDetails.order_Total = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_OrderTotal"));//.getAttribute("innerHTML");
		pricingDetails.order_Total = pricingDetails.order_Total.split("\\<")[0];
		pricingDetails.tax_Cost = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_PricingDetails_EstimateSalesTax"));

		return pricingDetails;
	}


	public static void verify_CreditCard_NotDeclined()
	{
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("checkout_CreditCardDeclinedError"));
	}

	public static void verify_PaymentInfo(String cardHolder, String address, String city, String state, String zip,
			String cardType, String cardNumber, String expirationDate) {

		String creditCardFormatted;
		BaseUI.verifyElementHasExpectedText("checkout_PayInfo_CardHolder", cardHolder);
		BaseUI.verifyElementHasExpectedPartialText("checkout_PayInfo_address", address);
		if (ClientDataRetrieval.isRedesignClient()) {
			String expectedCityStateZip = city+", "+ state +" "+ "*****";
			BaseUI.verifyElementHasExpectedText("checkout_PayInfo_City", expectedCityStateZip);
			creditCardFormatted = cardType.toUpperCase() + " ending in "
					+ cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
			cardType = cardType.toUpperCase();
		} else {
			BaseUI.verifyElementHasExpectedText("checkout_PayInfo_City", city);
			BaseUI.verifyElementHasExpectedText("checkout_PayInfo_State", state);
			BaseUI.verifyElementHasExpectedText("checkout_PayInfo_Zip", zip);
			creditCardFormatted = "XXXX-XXXX-XXXX-"
					+ cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
			BaseUI.verifyElementHasExpectedText("checkout_PayInfo_ExpirationDate", expirationDate);
		}
		BaseUI.verifyElementHasExpectedText("checkout_PayInfo_CardNumber", creditCardFormatted);
		BaseUI.verifyElementHasExpectedText("checkout_PayInfo_CardType", cardType);

	}

	public static void verify_ShippingInfo(String recipientName, String address, String city, String state, String zip,
			String country, String phoneNumber) {

		BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_RecipientName", recipientName);
		if (ClientDataRetrieval.isRedesignClient()) {
			String expectedCityStateZip = city+", "+ state+" "+ "*****";
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_Address", address);
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_City", expectedCityStateZip);
		} else {
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_Address", address);
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_City", city);
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_State", state);
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_Zip", zip);
			BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_PhoneNumber", phoneNumber);
		}
		String country_Formatted = country.equals("United States") ? "US" : country;
		BaseUI.verifyElementHasExpectedText("checkout_ShippingInfo_Country", country_Formatted);
	}

	public static void verify_ProductInfo(TableData originalData) throws Exception {

		TableData reviewOrder_ProductData = get_Table_Data();
		
		BaseUI.verify_TableColumns_Match("Product Name", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("quantity", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("pricing_Retail", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("pricing_Points", originalData.data, reviewOrder_ProductData.data);

		BaseUI.verify_TableColumns_Match("pricing_YouPay", originalData.data, reviewOrder_ProductData.data);

		//BaseUI.verify_TableColumns_Match("pricing_YouPay", originalData.data, reviewOrder_ProductData.data);

	}

	public static void verify_AllstateCash(String allstate_Cash) {
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			if (!allstate_Cash.equals("$0.00")) {
				allstate_Cash = "- " + allstate_Cash;
			}
			BaseUI.verifyElementHasExpectedText("checkout_PayInfo_AllstateCash", allstate_Cash);
		}
	}

	public static void verify_PricingDetails(PricingDetails pricingDetails) {
		verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
				pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
				pricingDetails.order_Total, pricingDetails.tax_Cost);
	}

	public static void verify_PricingDetails(String pointsAmount, String retailPrice, String youPay,
			String percentSavings, String shippingCost, String orderTotal, String salesTax) {

		BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_RetailPrice", retailPrice);

		String currency = "Points";
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			currency = "Smart Dollars";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			currency = "Rewards Dollars";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			currency = "-(Member Savings)";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			currency = "Savings Dollars";
		}
		
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_PointsName", currency);
		} else {
			BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_PointsName", currency + ":");
		}
		// BaseUI.verifyElementHasExpectedPartialText("checkout_PricingDetails_PointsDeduction",
		// pointsAmount);

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			// BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_PointsName",
			// currency + ":");
			BaseUI.verifyElementHasExpectedPartialText("checkout_PricingDetails_PointsDeduction", pointsAmount);
		}

		BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_YouPay", youPay);
		BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_PercentSavings", percentSavings);
		BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_ShippingCost", shippingCost);
		BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_EstimateSalesTax", salesTax);

		BaseUI.verifyElementHasExpectedPartialText("checkout_PricingDetails_OrderTotal", orderTotal.replace(",", ""));
		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_OrderTotalSavings", percentSavings);
		}
	}

	public static void click_EditShipping() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(Locator.lookupRequiredElement("checkout_ShipAddress_ChangeBtn"));
			Thread.sleep(500);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("checkout_EditShippingButton"));
			BaseUI.waitForElementToBeDisplayed("checkout_Step1_ClickContinue_ElementToWaitFor", null, null);
			Thread.sleep(500);
		}
	}

	public static void click_EditBilling() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_ShoppingCart();
			ShoppingCart.click_ProceedToCheckout();
			if(BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_BillingAddress_ChangeBtn"))) {
				WebElement billingChangeBtnElement = Locator.lookupRequiredElement("checkout_BillingAddress_ChangeBtn");
				BaseUI.scroll_to_element(billingChangeBtnElement);
				BaseUI.click(billingChangeBtnElement);
				Thread.sleep(500);
			}		
		} else {
			BaseUI.click(Locator.lookupRequiredElement("checkout_EditPaymentButton"));
			if(ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)){
				BaseUI.waitForElementToBeDisplayed("checkout_PaymentInfo_ElementToWaitForPageToLoad", null, null);
			}else {
				BaseUI.waitForElementToBeDisplayed("checkout_ReviewOrder_button", null, null);
			}
			Thread.sleep(500);
		}
	}

	public static void click_PlaceYourOrder() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_PlaceYourOrder_Button"));
		try {
			BaseUI.waitForElementToBeDisplayed("orderConfNumber", null, null, 20);
			Thread.sleep(500);
		}catch(AssertionError e){
			if(BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_CreditCardDeclinedError"))){
				BaseUI.log_AndFail("Credit Card wad declined, unable to continue.");
			}else{
				BaseUI.log_AndFail("Encountered waiting for page to load after clicking Place Your Order Button.\n" + e.getMessage());
			}
		}
	}

	public static void submit_Order() throws Exception {
		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.checkCheckbox("checkout_AgreeToTerms_Checkbox");
			Thread.sleep(100);
		}
		click_PlaceYourOrder();
	}
	
	
	public static Boolean returnGiftCardProductNameAddedToCart(TableData shoppingCartData) throws Exception {
		TableData reviewOrder_ProductData;
		Boolean giftCardProductName;
		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			reviewOrder_ProductData = get_Table_Data();
			giftCardProductName = reviewOrder_ProductData.data.get(0).get("Product Name").contains("Gift Card");
		} else {
			giftCardProductName = shoppingCartData.data.get(0).get("Product Name").contains("Gift Card");
		}	
		
		return giftCardProductName;		
	}
}
