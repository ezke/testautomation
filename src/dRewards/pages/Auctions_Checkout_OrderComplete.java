package dRewards.pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Auctions_Checkout_OrderComplete {

	public static void verify_Order_Info(HashMap<String, String> addressInfo, String productText,
										 String productRetailPrice, String expectedEndTime, String expectedAuctionBid) {
		if (ClientDataRetrieval.isRedesignClient()) {
			verify_AddressRedesign(addressInfo.get("clientName"), addressInfo.get("address"), addressInfo.get("city"),
					addressInfo.get("country"),null,null,null);
		} else {
			verify_Address(addressInfo.get("clientName"), addressInfo.get("address"), addressInfo.get("city"),
					addressInfo.get("state"), addressInfo.get("zipCode-masked"), addressInfo.get("country"), addressInfo.get("phone-masked"));
		}

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			expectedAuctionBid = expectedAuctionBid.split("\\.")[1].length() == 1 ? expectedAuctionBid + "0"
					: expectedAuctionBid;
		}

		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			expectedAuctionBid = expectedAuctionBid.split("\\.")[0];
		}

		BaseUI.verifyElementHasExpectedPartialText("auctOrderComplete_pointsSpent", expectedAuctionBid);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_productName", productText);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_retailPrice", productRetailPrice);
		if (!ClientDataRetrieval.isRedesignClient())
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_finalPrice", "$0.00");
	}

	public static void verify_Order_Info_DualCurrency(HashMap<String, String> addressInfo, String productText,
													  String productRetailPrice, String expectedEndTime, String expectedAuctionBid, Double usdAmount, String tax_Cost) {

		verify_Address(addressInfo.get("clientName"), addressInfo.get("address"), addressInfo.get("city"),
				addressInfo.get("state"), addressInfo.get("zipCode-masked"), addressInfo.get("country"),
				addressInfo.get("phone-masked"));

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			expectedAuctionBid = expectedAuctionBid.split("\\.")[1].length() == 1 ? expectedAuctionBid + "0"
					: expectedAuctionBid;
		}

		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			expectedAuctionBid = expectedAuctionBid.split("\\.")[0];
		}

		tax_Cost = tax_Cost.replace("$", "").replace(",", "");
		Double taxCost_Double = Double.parseDouble(tax_Cost);

		Double newTotal = usdAmount + taxCost_Double;
		String order_Total = "$" + BaseUI.convertDouble_ToString_ForCurrency(newTotal);

		BaseUI.verifyElementHasExpectedPartialText("auctOrderComplete_pointsSpent", expectedAuctionBid);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_PaymentDue",
				"$" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount));
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_productName", productText);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_retailPrice", productRetailPrice);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_finalPrice", order_Total);
	}

	public static void verify_PrintYourReceiptButton_VisibleAndEnabled() {
		WebElement printYourReceipt_Button = Locator.lookupRequiredElement("auctOrderComplete_PrintYourReceiptButton");
		BaseUI.verifyElementEnabled(printYourReceipt_Button);
		BaseUI.verifyElementAppears(printYourReceipt_Button);
	}

	public static String return_OrderNumber() {
		String orderNumber = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctOrderComplete_OrderNumber"));
		orderNumber = orderNumber.split("\\-")[1];

		return orderNumber;
	}

	public static void verify_AddressRedesign(String recipientName, String address, String city,
											  String country, String state,String zip, String phone) {
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_RecipientName", recipientName);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_Address", address);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_City", city);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_Country", country);
		} else {
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_RecipientName", recipientName);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_Address", address);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_City", city);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_State", state);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_Zip", zip);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_Country", country);
			BaseUI.verifyElementHasExpectedText("auctOrderComplete_Phone",
					phone);
		}

}
	public static void verify_Address(String recipientName, String address, String city, String state, String zip,
			String country, String phone) {
		BaseUI.verifyElementHasExpectedPartialText("auctOrderComplete_RecipientName", recipientName);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_Address", address);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_City", city);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_State", state);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_Zip", zip);
		BaseUI.verifyElementHasExpectedText("auctOrderComplete_Country", country);
		// BaseUI.verifyElementHasExpectedText("auctOrderComplete_Phone",
		// phone);
	}

}// End of Class
