package dRewards.pages;

import com.google.common.util.concurrent.AtomicDouble;
import dRewards.ClassObjects.PricingDetails;
import utils.Locator;

import java.text.MessageFormat;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import dRewards.ClassObjects.ShippingAddress;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Browser;
import utils.TableData;

public class CheckoutPage_ShippingInfo {

	public static HashMap<String, String> return_DefaultShippingInfo() {
		HashMap<String, String> defaultShippingInfo = new HashMap<String, String>();
		defaultShippingInfo.put("clientName", "QAauto Auto");
		defaultShippingInfo.put("address", "1225 broken sound");
		defaultShippingInfo.put("city", "Boca Raton");
		defaultShippingInfo.put("zipCode", "33487");
		defaultShippingInfo.put("zipCode-masked", "*****");
		defaultShippingInfo.put("country", "US");
		defaultShippingInfo.put("state", "FL");
		defaultShippingInfo.put("phone", "9544561212");
		defaultShippingInfo.put("phone-masked", "******1212");

		return defaultShippingInfo;
	}

	public static String recalculateOrderTotal(TableData shoppingCartData){
			AtomicDouble productSubTotals = new AtomicDouble(0.0);
			shoppingCartData.forEach_Row(a -> {
				productSubTotals.getAndAdd(Double.parseDouble(a.get("pricing_YouPay").replace("$", "").replace(",", "").trim()));
			});
			return productSubTotals.toString();

	}

	// just uses default Shipping Address and navigates to next Checkout page.
	public static void navigate_To_NextPage() throws Exception {
		HashMap<String, String> defaultShippingInfo = return_DefaultShippingInfo();

		if (ClientDataRetrieval.isRedesignClient()) {
			if(BaseUI.elementExists("checkout_ShipAddress_ChangeBtn", null, null)) {
				click_ContinueTo_NextPage();
			}			
		} else {
			if (BaseUI.elementExists("checkout_ShipToExistingAddress_Radio", null, null)) {
				click_AddExistingAddress_Radio();
				if(ClientDataRetrieval.client_Matches(client_Designation.AllstateCash) && BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_ContinueToCashRewards"))) {
					BaseUI.click(Locator.lookupRequiredElement("checkout_ContinueToCashRewards"));
					BaseUI.waitForElementToBeDisplayed("checkout_ChooseCashRewardsOptionHeaderTitle", null, null, 60);
				} else {
					click_ContinueTo_NextPage();
				}			
			} else {
				String firstName = defaultShippingInfo.get("clientName").split("\\s+")[0];
				// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameLabel"));
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"), firstName);
				String lastName = defaultShippingInfo.get("clientName").split("\\s+")[1];
				// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_LastNameLabel"));
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"), lastName);
				String address1 = defaultShippingInfo.get("address");
				// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_Address1Label"));
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"), address1);
				String city = defaultShippingInfo.get("city");
				// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_City1Label"));
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"), city);
				// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown"));
				Thread.sleep(200);
				String zip = defaultShippingInfo.get("zipCode");
				Object state = defaultShippingInfo.get("state");
				BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown")
						.findElement(By.xpath(MessageFormat.format("./option[@value=''{0}'']", state))));
				BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeLabel"));
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"), zip);
				String phone = defaultShippingInfo.get("phone");
				BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_phoneMaskedTxtBx"), phone);
				BaseUI.checkCheckbox(Locator.lookupRequiredElement("checkout_NewAddress_SavedShipping_Address"));
				click_ContinueTo_NextPage();
			}
		}
	}

	// Will continue to whichever page comes after Shipping Info.
	public static void click_ContinueTo_NextPage() throws Exception {
		if(Browser.currentBrowser.equals("internetexplorer") || ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			BaseUI.click_js(Locator.lookupRequiredElement("checkout_ContinueToPaymentInfo"));
		}else{
			BaseUI.click(Locator.lookupRequiredElement("checkout_ContinueToPaymentInfo"));
		}

		BaseUI.waitForElementToBeDisplayed("checkout_PaymentInfo_ElementToWaitForPageToLoad", null, null, 30);
		Thread.sleep(1000);
		// was updated by adding if and else statment check tomorrow 11117
		// EG..added method updated as 012617
	}

	public static void click_AddExistingAddress_Radio() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_ShipToExistingAddress_Radio"));
		Thread.sleep(3000);
	}

	public static void click_AddExistingAddress_Radio_ByFirstName(String firstName) throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_ShipToExistingAddress_Radio_ByFirstName", firstName, null));
		Thread.sleep(500);
	}

	public static void click_AddNewShippingAddress_Radio() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("checkout_AddNewShippingAddress"));
		BaseUI.waitForElementToBeDisplayed("checkout_NewAddress_FirstNameTxtBx", null, null);
		Thread.sleep(500);
	}

	public static Boolean newShippingAddress_Radio_Exists() {
		Boolean newShipping_radio_Exists = BaseUI.elementExists("checkout_AddNewShippingAddress", null, null);
		return newShipping_radio_Exists;
	}

	public static void continue_To_NextPage() throws Exception {

		Boolean newShipping_radio_Exists = BaseUI.elementExists("checkout_AddNewShippingAddress", null, null);
		if (newShipping_radio_Exists) {
			if (ClientDataRetrieval.isRedesignClient()) {
				click_Checkout_ShippingAddress_ChangeBtn();
	
				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"));
				
				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"));
				
				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"));

				BaseUI.clearText_Textbox(Locator.lookupRequiredElement("checkout_NewAddress_phoneTxtBx"));
			} else {
				click_AddNewShippingAddress_Radio();
			}
		} else {
			String firstName = "QAauto";
			// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameLabel"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"), firstName);
			String lastName = "Auto";
			// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_LastNameLabel"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"), lastName);
			String address1 = "1225 broken sound";
			// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_Address1Label"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"), address1);
			String city = "Boca Raton";
			// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_City1Label"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"), city);
			// BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown"));
			Thread.sleep(200);
			String zip = "33487";
			Object state = "FL";
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown")
					.findElement(By.xpath(MessageFormat.format("./option[@value=''{0}'']", state))));
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeLabel"));
			BaseUI.enterText(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"), zip);
			String phone = "9544561212";
			BaseUI.enterText(Locator.lookupRequiredElement("checkout_NewAddress_phoneMaskedTxtBx"), phone);
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_SavedShipping_Address"));
			BaseUI.click(Locator.lookupRequiredElement("checkout_ContinueToPaymentInfo"));
			Thread.sleep(1000);

		}

	}

	public static void verify_FieldErrorDisplayed(String fieldText, String expectedErrorText) {
		WebElement errorElement = Locator.lookupRequiredElement("checkout_ErrorMessage_ByFieldText", fieldText, null);
		BaseUI.verifyElementAppears(errorElement);
		String errorText = errorElement.getText();
		BaseUI.baseStringCompare("Error Text", expectedErrorText, errorText);
	}

	public static void verify_TopErrorDisplayed(String expectedText) {
		WebElement errorElement = Locator.lookupRequiredElement("checkout_Errors");
		BaseUI.verifyElementAppears(errorElement);
		String errorText = errorElement.getText();
		BaseUI.baseStringCompare("Error Text", expectedText, errorText);
	}

	// Pass in true or false for elementAppears to test whether or not element
	// appears. Passing in true means it should appear.
	// Passing false means it shouldn't.
	// Also checks to see if the text is correct.
	public static void verify_Element_AppearsOrDoesnt_AndText_Correct(String elementLocator, String expectedText,
			Boolean elementAppears) {
		WebElement elementToVerify = Locator.lookupRequiredElement(elementLocator, "QaAuto", null);
		String elementText = elementToVerify.getAttribute("innerHTML");
		BaseUI.baseStringCompare("Address 1", expectedText, elementText);

		if (elementAppears) {
			BaseUI.verifyElementAppears(elementToVerify);
		} else {
			BaseUI.verifyElementDoesNotAppear(elementToVerify);
		}
	}

	public static ShippingAddress retrieve_ExistingShippingInformation(String firstName) {
		ShippingAddress shippingAddress = new ShippingAddress();
		shippingAddress.firstName = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_firstName", firstName, null));
		shippingAddress.lastName = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_lastName", firstName, null));
		shippingAddress.address1 = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress1", firstName, null));
		shippingAddress.address2 = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress2", firstName, null));
		shippingAddress.city = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_City", firstName, null));
		shippingAddress.state = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_State", firstName, null));
		shippingAddress.zip = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ZipCodeMasked", firstName, null));
		shippingAddress.country = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_Country", firstName, null));
		shippingAddress.phoneNumber = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_PhoneMasked", firstName, null));

		return shippingAddress;

	}

	// public static ShippingAddress retrieve_NewShippingInformation(String
	// firstName)
	// {
	// ShippingAddress shippingAddress = new ShippingAddress();
	// shippingAddress.firstName =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx",
	// firstName, null));
	// shippingAddress.lastName =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx",
	// firstName, null));
	// shippingAddress.address1 =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx",
	// firstName, null));
	// shippingAddress.address2 =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress2",
	// firstName, null));
	// shippingAddress.city =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx",
	// firstName, null));
	// shippingAddress.state =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_ExistingAddress_State",
	// firstName, null));
	// shippingAddress.zip =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx",
	// firstName, null));
	// shippingAddress.country =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_ExistingAddress_Country",
	// firstName, null));
	// shippingAddress.phoneNumber =
	// BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("checkout_ExistingAddress_PhoneMasked",
	// firstName, null));
	//
	// return shippingAddress;
	//
	// }

	// Fills in New Address information. Did not add an Address2.
	public static void add_newDeliveryAddress(String firstName, String lastName, String address1, String city,
			String state, String zip, String phone) throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"), firstName);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"), lastName);
			Thread.sleep(200);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"), address1);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"), city);
			Thread.sleep(200);
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown"), state);
			Thread.sleep(200);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"), zip);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_phoneTxtBx"), phone);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameLabel"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"), firstName);
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_LastNameLabel"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"), lastName);
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_Address1Label"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"), address1);
			BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_City1Label"));
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"), city);
			Thread.sleep(200);
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_NewAddress_StateDropdown"), state);
			
			if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
				BaseUI.click_js(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeLabel"));
				Thread.sleep(2000);
			} else {
				BaseUI.click(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeLabel"));
				Thread.sleep(2000);
			}		
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"), zip);

			// ran into an issue where I for some reason couldn't target the correct
			// element to enter the phone number.
			// work around was to tab into the phone field and then ask the driver
			// to return the actively focused element.
			// I then took that element and entered the text that I wanted.
			BaseUI.tabThroughField(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"));
			WebElement phoneElementToEnter = Browser.driver.switchTo().activeElement();
			BaseUI.enterText_IntoInputBox(phoneElementToEnter, phone);
		}		
	}

	public static void check_SaveForLater_Checkbox() throws Exception {
		BaseUI.checkCheckbox("checkout_NewAddress_SavedShipping_Address");
		Thread.sleep(300);
	}
	
	//Redesign clients Change button on checkout page for updating 
	//Shipping address with different address
	public static void click_Checkout_ShippingAddress_ChangeBtn() throws Exception{
		BaseUI.click(Locator.lookupRequiredElement("checkout_ShipAddress_ChangeBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}
	
	
	//Redesign clients Use existing shipping address for billing too
	//checkbox
	public static void click_Checkout_ExistingShippingAddress_Checkbox() throws Exception{
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_UseMyShippingAdd_Checkbox"));
		Thread.sleep(200);
	}
	
	
	//This button is added for Redesign client checkout page, when item added to cart
	// is free product and order total is $0.00. Redesign client do not have Billing 
	//address info, click this button and Place an order button enables to add order
	public static void click_ContinueAdditionalInfoBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_ContinueToAdditionalInfoBtn"));
		Thread.sleep(3000);
	}

	//For AARP Redesign only
	public static void add_CanadaAddress(String firstName, String lastName, String address1, String city,
											  String zip, String phone) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_FirstNameTxtBx"), firstName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_LastNameTxtBx"), lastName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_Address1TxtBx"), address1);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_CityTxtBx"), city);
		Thread.sleep(200);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_zipCodeTxtBx"), zip);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_NewAddress_phoneTxtBx"), phone);
	}

	public static void verifyElementHasExpectedTextChange() {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkout_AddNewShippingAddress"));
		if (ClientDataRetrieval.client_Matches(client_Designation.McAfee)){
			BaseUI.verifyElementHasExpectedText("checkout_AddNewShippingAddress", "Change");
		} else {
			BaseUI.verifyElementHasExpectedText("checkout_AddNewShippingAddress", "CHANGE");
		}
	}
}
