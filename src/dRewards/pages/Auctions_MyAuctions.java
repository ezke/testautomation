package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.AuctionTile;
import utils.BaseUI;
import utils.Locator;

public class Auctions_MyAuctions {

	// Assumes the auction we just claimed appears last in the carousel.
	public static void verify_AuctionWon_AndAlreadyClaimed(String auctionDescription, Double expectedPoints,
			String auctionEnded) throws Exception {

		verify_Description(auctionDescription);

		verify_WinningBid(expectedPoints);

		verify_endDate(auctionEnded);

		verify_Currency();

		verify_Auction_AlreadyClaimed();
	}

	private static void verify_Currency() {
		String currency = "Points";
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			currency = "Smart Dollars";
		}

		String actualCurrency = BaseUI
				.get_NonVisible_TextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_Currency"));
		BaseUI.baseStringCompare("Currency", currency, actualCurrency);
	}

	private static void verify_WinningBid(Double expectedPoints) {
		String expectedPointsStr = expectedPoints.toString();
		if (!ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			expectedPointsStr = expectedPointsStr.split("\\.")[0];
		} else {
			expectedPointsStr = expectedPointsStr.split("\\.")[1].length() == 1 ? expectedPointsStr + "0"
					: expectedPointsStr;
		}

		String actualPoints = BaseUI
				.get_NonVisible_TextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_BidAmount"));
		//actualPoints.replace("Points", "");
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("Winning Bid", expectedPointsStr + " Points", actualPoints);
		}else{
			BaseUI.baseStringCompare("Winning Bid", expectedPointsStr, actualPoints);
		}
	}

	private static void verify_Description(String auctionDescription) {
		String description;
		if (ClientDataRetrieval.isRedesignClient()){
			 description = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_Description"));
		}else {
			 description = BaseUI
					.get_NonVisible_TextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_Description"));
		}
		BaseUI.baseStringCompare("Description", auctionDescription, description);
	}

	// Takes date with format MMM. d, yyyy h:mm a. Will then convert it to
	// format MM/dd/yyyy hh:mm a before comparison
	private static void verify_endDate(String auctionEnded) throws Exception {
//		String actualAuctionEnded = BaseUI
//				.get_NonVisible_TextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_EndTime"));
//
//		// need to format our Auction Ended time.
//	//	auctionEnded = auctionEnded.replace(" EDT", "");
//		auctionEnded = auctionEnded.replace((GlobalVariables.auctionTimeZone), "");
//		String expectedAuctionEnded_Formatted = BaseUI.return_Date_AsDifferentFormat(auctionEnded,
//				"MMM. d, yyyy h:mm a", "MM/dd/yyyy hh:mm a");
//		expectedAuctionEnded_Formatted += " ET";
//		BaseUI.baseStringCompare("Auction Ended", expectedAuctionEnded_Formatted, actualAuctionEnded);

		String actualAuctionEnded = BaseUI
				.get_NonVisible_TextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_EndTime")).replaceAll("at","");
		actualAuctionEnded = actualAuctionEnded.replace("at", "");
		// need to format our Auction Ended time.
		//	auctionEnded = auctionEnded.replace(" EDT", "");
		if (ClientDataRetrieval.isRedesignClient()) {
			
			auctionEnded = auctionEnded.replace("at", "");
			String expectedAuctionEnded_Formatted = BaseUI.return_Date_AsDifferentFormat(auctionEnded,
					"MMMM dd, yyyy hh:mm a", "MM/dd/yy hh:mm a");
			BaseUI.baseStringCompare("Auction Ended", expectedAuctionEnded_Formatted, actualAuctionEnded);
		} else {

			auctionEnded = auctionEnded.replace((GlobalVariables.auctionTimeZone), "");
			String expectedAuctionEnded_Formatted = BaseUI.return_Date_AsDifferentFormat(auctionEnded,
					"MMM. d, yyyy h:mm a", "MM/dd/yyyy hh:mm a");
			expectedAuctionEnded_Formatted += " ET";
			BaseUI.baseStringCompare("Auction Ended", expectedAuctionEnded_Formatted, actualAuctionEnded);
		}
	}

	private static void verify_Auction_AlreadyClaimed() {
		BaseUI.verifyElementExists("myAuct_AuctsWon_LastItem_ViewOrderButton", null, null);
	}

	public static void click_LastAuction_ViewDetails() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_ViewOrderButton"));
		BaseUI.waitForElementToNOTBeDisplayed("myAuct_AuctsWon_LastItem_ViewOrderButton",null,null,10);
		Thread.sleep(1000);
	}

	public static void verify_CurrentAuctionFollowing_Appears(){
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAuct_Current_AuctionsFollowing"));
		BaseUI.verifyElementHasExpectedText("myAuct_Current_AuctionsFollowing", "Current auctions I am following");
	}

	public static AuctionTile return_AuctionTile_ByAuctionNameAndCurrentBid(String auctionName, Double currentBid) {
		return Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(auctionName, currentBid, "myAuct_Current_AuctionRows");
	}

}// End of Class
