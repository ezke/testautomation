package dRewards.pages;

import java.text.MessageFormat;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class HotelsAndCondos_CheckoutPage {

	public static void validate_SearchResults(HashMap<String, String> hotelData, String fromDate, String toDate,
			String adultCount, String childCount, String childAge) throws Exception {

		verify_HotelAddress_Info(hotelData);
		verify_HotelTotals(hotelData);

		// Validate Adult and Child Info
		verify_Checkin_And_Checkout_Dates(fromDate, toDate);
		verify_GuestInfo_AdultsAndChildren(adultCount, childCount, childAge);

	}
	
	
	public static void verify_HotelTotals(HashMap<String, String> hotelData)
	{
		// Validate Top totals. (Not the Actual total, the made up total they
		// lure them in with).
		BaseUI.verifyElementHasExpectedText("hotelCheckout_TotalBeforeReduction", hotelData.get("hotelRate"));
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String pointsDeduction = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_PointsReduction"))
					.replace("-", "").replace(",", "");
			BaseUI.baseStringCompare("Points Reduction", hotelData.get("points"), pointsDeduction);
		}
		String totalAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_CostAfterPoints"))
				.replace("$", "").replace(",", "");
		BaseUI.baseStringCompare("Total Amount", hotelData.get("youPay"), totalAmount);

		// Validate ACTUAL totals
		BaseUI.verifyElementHasExpectedText("hotelCheckout_Actual_TotalBeforeReduction", hotelData.get("hotelRate"));
		String taxesAndFees = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_Actual_TaxesAndFees"))
				.replace("$", "").replace(",", "");

		String totalCost_AfterFees = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_Actual_TotalAfter_TaxesAndFees"))
				.replace("$", "").replace(",", "");
		Double expectedAmountDouble = Double.parseDouble(taxesAndFees)
				+ Double.parseDouble(hotelData.get("hotelRate").replace("$", "").replace(",", ""));
		expectedAmountDouble = BaseUI.round_Double_ToPassedInDecimalPlaces(expectedAmountDouble, 2);
		String expectedAmount = expectedAmountDouble.toString();
		expectedAmount = expectedAmount.split("\\.")[1].length() == 1 ? expectedAmount + "0" : expectedAmount;
		BaseUI.baseStringCompare("ACTUAL Total Cost", expectedAmount, totalCost_AfterFees);

		Double expected_finalACTUALTotal = Double.parseDouble(hotelData.get("youPay"))
				+ Double.parseDouble(taxesAndFees);
		expected_finalACTUALTotal = BaseUI.round_Double_ToPassedInDecimalPlaces(expected_finalACTUALTotal, 2);

		String finalACTUALTOTAL = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_Actual_ActualTotalCost"))
				.replace("$", "").replace(",", "");
		String expected_finalACTUALTotalStr = expected_finalACTUALTotal.toString();
		expected_finalACTUALTotalStr = expected_finalACTUALTotalStr.split("\\.")[1].length() == 1
				? expected_finalACTUALTotalStr + "0" : expected_finalACTUALTotalStr;
		BaseUI.baseStringCompare("ACTUAL Total Price after points", expected_finalACTUALTotalStr, finalACTUALTOTAL);

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String actual_points = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_Actual_Points"))
					.replace(",", "");
			BaseUI.baseStringCompare("Actual Points", hotelData.get("points"), actual_points);
		}
		
	}
	
	public static void verify_HotelAddress_Info(HashMap<String, String> hotelData)
	{
		BaseUI.verifyElementHasExpectedText("hotelCheckout_HotelName", hotelData.get("hotelName"));

		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			String address = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_Address"))
					.replace("    ", " ").replace(",  ", ", ").replaceAll("[\r\n]+", " ").replace(".", "");
			BaseUI.baseStringCompare("Address",
					hotelData.get("address").replaceAll("[\r\n]+", " ").replace(",  ", ", "), address);

		} else {
			String address = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_Address"))
					.replace("    ", " ").replace(",  ", ", ").replaceAll("[\r\n]+", " ").replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();

			BaseUI.baseStringCompare("Address", hotelData.get("address").replaceAll("[\r\n]+", " "), address);
		}
	}

	public static void verify_GuestInfo_AdultsAndChildren(String adultCount, String childCount, String childAge)
	{
		String guestInfoOnPage = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_GuestInfo"));
		String childAges = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_AgesOfChildren"));

		Integer childCountInt = Integer.parseInt(childCount);
		if (childCountInt > 0) {
			String expectedAdultChildLine = "";
			String expectedChildrenAgesLine = "Ages of Children: ";

			// If we have 1 Child
			if (childCountInt == 1) {
				expectedAdultChildLine = adultCount + " Adults 1 Child";
				expectedChildrenAgesLine += childAge;
				// If we have more than 1 child.
			} else {
				expectedAdultChildLine = adultCount + " Adults " + childCount + " Children";
				expectedChildrenAgesLine += childAge;
				for (int i = 1; i < childCountInt; i++) {
					expectedChildrenAgesLine += ", " + childAge;
				}
			}
			BaseUI.baseStringCompare("Children Ages", expectedChildrenAgesLine, childAges);
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("hotelCheckout_GuestInfo"),
					expectedAdultChildLine);
			// if we have no children.
		} else {
			BaseUI.baseStringCompare("Number of Adults", "Number of Adults: " + adultCount, guestInfoOnPage);
		}
	}
	
	public static void verify_Checkin_And_Checkout_Dates(String fromDate, String toDate) {
		BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("hotelCheckout_CheckInDate"), fromDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("hotelCheckout_CheckOutDate"), toDate);
	}

	public static void verify_HotelLogo_Accurate(String expectedLogoURL) {
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("hotelCheckout_HotelLogo"), "src",
				expectedLogoURL);
	}

	public static String return_AddressInfo_String() {
		String firstName = "";
		String lastName = "";
		String address1 = "";
		String city = "";
		String state = "";
		String zipCode = "";
		// String phoneNumber = "1234567890";

		// Smart Rewards doesn't have a default, so I get the first address
		// entry and click the radio button for it.
//		if (ClientDataRetrieval.clientName.startsWith("Smart rewards")
//				|| ClientDataRetrieval.clientName.startsWith("Citi")
//				|| ClientDataRetrieval.clientName.startsWith("AARP")
//				|| ClientDataRetrieval.clientName.startsWith("TVC Marketing")
//				|| ClientDataRetrieval.clientName.startsWith("Allstate Rewards")) {
			firstName = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_1stAddress_FirstName"));
			lastName = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_1stAddress_LastName"));
			address1 = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_1stAddress_Address1"));
			city = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_1stAddress_City"));
			state = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_1stAddress_State"));
			zipCode = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_1stAddress_ZipCode"));

			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_1stAddress_RadioButton"));
//		} else {
//			firstName = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_defaultAddress_FirstName"));
//			lastName = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_defaultAddress_LastName"));
//			address1 = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_defaultAddress_Address1"));
//			city = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_defaultAddress_City"));
//			state = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_defaultAddress_State"));
//			zipCode = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelCheckout_defaultAddress_ZipCode"));
//		}

		String addressCompiled = firstName + " " + lastName + "\n" + address1 + "\n" + city.toUpperCase() + ", " + state
				+ " " + zipCode;

		return addressCompiled;
	}

	public static void select_DefaultAddress() throws Exception {
		BaseUI.checkCheckbox("hotelCheckoutAddress_DefaultAddress_RadioButton");
		Thread.sleep(50);
	}

	public static void click_ContinueToPaymentInfo() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_ContinueToPayInfo_Button"));
		BaseUI.waitForElementToBeDisplayed("hotelCheckoutPay_CompleteYourOrder",null,null);
		Thread.sleep(500);
	}
	

	public static void click_AddNewPayment_Radio() throws Exception {
		BaseUI.checkCheckbox("hotelCheckoutPay_AddNewPayment_RadioButton");
		Thread.sleep(100);

	}
	
	public static void clear_Default_GuestInfo() throws Exception {
		WebElement firstNametextField = Locator.lookupRequiredElement("hotelCheckout_NewAddress_FirstName_TextBox");
		firstNametextField.sendKeys(Keys.CONTROL + "a");
		firstNametextField.sendKeys(Keys.DELETE);
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_FirstName_TextBox"));
		WebElement lastNameTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(lastNameTextField, "");
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_LastName_TextBox"));
		WebElement address1TextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(address1TextField, "");
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_Address1_TextBox"));
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_Address2_TextBox"));
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
				|| ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				||ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				||ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_Country_Dropdown"));
		}
		WebElement cityTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(cityTextField, "");
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_City_TextBox"));
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_StateDropdown"));
		WebElement zipcodeTextField = Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCode_TextBox");
		zipcodeTextField.sendKeys(Keys.CONTROL + "a");
		zipcodeTextField.sendKeys(Keys.DELETE);
		BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCode_TextBox"));
		WebElement phoneTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(phoneTextField, "");
		
	}
	
	public static void add_NewGuestContact_Information(String firstName, String lastName, String address1, String city,
			String state, String zip, String phone) throws Exception {
		clear_Default_GuestInfo();

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_FirstName_TextBox"),
					firstName);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_LastName_TextBox"), lastName);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_Address1_TextBox"), address1);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_City_TextBox"), city);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_StateDropdown")
					.findElement(By.xpath(MessageFormat.format("./option[@value=''{0}'']", state))));
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCodeLabel"));
			Thread.sleep(100);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCode_TextBox"), zip);
			BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCode_TextBox"));
			WebElement phoneElementToEnter = Browser.driver.switchTo().activeElement();
			BaseUI.enterText_IntoInputBox(phoneElementToEnter, phone);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_FirstNameLabel"));
			Thread.sleep(100);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_FirstName_TextBox"),
					firstName);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_LastNameLabel"));
			Thread.sleep(100);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_LastName_TextBox"), lastName);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_Address1Label"));
			Thread.sleep(100);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_Address1_TextBox"), address1);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_CityLabel"));
			Thread.sleep(100);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_City_TextBox"), city);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_StateDropdown"));
			Thread.sleep(200);
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_StateDropdown")
					.findElement(By.xpath(MessageFormat.format("./option[@value=''{0}'']", state))));
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCodeLabel"));
			Thread.sleep(100);
			BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCode_TextBox"), zip);
			BaseUI.tabThroughField(Locator.lookupRequiredElement("hotelCheckout_NewAddress_ZipCode_TextBox"));
			WebElement phoneElementToEnter = Browser.driver.switchTo().activeElement();
			BaseUI.enterText_IntoInputBox(phoneElementToEnter, phone);
		}
	}

	public static void add_newPaymentOption_UsingDefaults() throws Exception {
		add_NewPaymentOption("MasterCard", "5105105105105100", "12", "22", "211", "qa", "automation",
				"1225 broken sound", "United States", "Boca Raton", "FL", "33481", "7151234555");
	}

	public static void add_RewardsCash(Double amountToAdd) throws Exception {
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			click_ContinueToCashRewards();
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_ApplyOtherAmount_Radio"));
			BaseUI.enterText(Locator.lookupRequiredElement("hotelCheckout_ApplyOtherAmount_TextBox"), amountToAdd.toString());
		}
	}
	
	public static void add_Default_CVVCode(String securityCode){
	//	BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_Default_PaymentInfo_CVV_Label"));
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckout_Default_PaymentInfo_CVV_TxtBox"), securityCode);
	}

	public static void click_ContinueToCashRewards() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelCheckout_ContinueToCashRewards_Button"));
		BaseUI.waitForElementToBeDisplayed("hotelCheckout_ApplyOtherAmount_Radio",null,null);
		Thread.sleep(500);
	}
	

	public static void add_NewPaymentOption(String creditCardType, String creditCardNumber, String expirationMonth,
			String expirationYear, String cvvCode, String firstName, String lastName, String address1, String country,
			String city, String State, String zip, String phone) throws Exception {

		if (BaseUI.elementExists("hotelCheckoutPay_AddNewPayment_RadioButton", null, null)) {
			click_AddNewPayment_Radio();
		}
		enter_requiredNewPaymentFields(creditCardType, creditCardNumber, expirationMonth, expirationYear, cvvCode,
				firstName, lastName, address1, country, city, State, zip, phone);
		
		BaseUI.checkCheckbox("hotelCheckoutPay_Save_DefaultBilling_Checkbox");
		Thread.sleep(50);

		check_CheckoutPay_Acknowledgement_Chckbox();

		click_CompleteYourOrder();
	}
	
	public static void check_CheckoutPay_Acknowledgement_Chckbox() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_Acknowledgement_Checkbox"));
		Thread.sleep(1000);
	}

	public static void click_CompleteYourOrder() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_CompleteYourOrder"));
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void enter_requiredNewPaymentFields(String creditCardType, String creditCardNumber,
			String expirationMonth, String expirationYear, String cvvCode, String firstName, String lastName,
			String address1, String country, String city, String State, String zip, String phone) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("hotelCheckoutPay_CreditCardType_Dropdown"),
				creditCardType);
		BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_CreditCardNumber_MaskElement"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_CreditCardNumber_Textbox"),
				creditCardNumber);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_CardHolderName"),
				firstName + " " + lastName);

		BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_CCExpireMonth_MaskElement"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_CCExpireMonth_Textbox"), expirationMonth);
		BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_CCExpireYear_MaskElement"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_CCExpireYear_Textbox"), expirationYear);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_CVV_Code_Textbox"), cvvCode);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_FirstName"), firstName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_LastName"), lastName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_Address"), address1);
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("hotelCheckoutPay_Country_Dropdown"), country);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_City"), city);
		// make sure to pass in 2 letter state code (example: WI)
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("hotelCheckoutPay_State_Dropdown"), State);
		
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_ZipCode"));
			Thread.sleep(100);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelCheckoutPay_ZipCode_Mask"));
			Thread.sleep(100);
		}
		
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_ZipCode"), zip);
//      BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelCheckoutPay_PhoneNumber"), phone);
	}

}// end of class
