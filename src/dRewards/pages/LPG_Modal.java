package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class LPG_Modal {

    public static void navigate_Merchandise_LPGhomepage() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("homepage_LearnMoreLnk"));
        BaseUI.waitForElementToBeDisplayed("homepage_lpgMerchandise_Tab", null, null);
        Thread.sleep(500);

        BaseUI.click(Locator.lookupRequiredElement("homepage_lpgMerchandise_Tab"));
        Thread.sleep(500);
    }

    public static void click_TravelTab() throws Exception {
        if(!ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)) {
            BaseUI.click(Locator.lookupRequiredElement("lpgModal_TravelTab"));
            Thread.sleep(500);
        }
    }

    public static void click_TravelLink() throws Exception {
        //Allstate doesn't have a Travel Link, will click Merchandise link instead.
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)) {
            BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
            BaseUI.waitForElementToBeDisplayed("lpgPage_LowPriceGuaranteeBanner", null, null);
        } else {
            click_TravelTab();
            BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_TravelLink"));
            BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
        }
        Thread.sleep(500);
    }

    public static void verify_Merchandise_LPG_Modal_Displayed() throws Exception {

        String msgExpected;
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            msgExpected = "Every item we offer is reviewed each day to guarantee that Your Price is as low as any you will find at online retail including:";
        } else if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)) {
            msgExpected = "BUT if you find a lower price online within 7 days of purchase,"
                    + "\n" +"we'll give you back your points and refund the difference.";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            msgExpected = "Once we find the lowest price, we apply your points to make your price even better.";
        } else {
            msgExpected = "Every item we offer is reviewed each day to guarantee that our Retail Price (that's the price before your "
                    + ClientDataRetrieval.client_Currency
                    + " is applied) is the lowest you will find at online retailers including:";
        }

        WebElement lpgMerchImage = Locator.lookupRequiredElement("lpg_Modal_lpgMerchandise_ActiveContent");
        BaseUI.verifyElementAppears(lpgMerchImage);
        if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)){
            String imageURL = BaseUI.get_CSSAttribute_FromField(lpgMerchImage, "background-image");
            BaseUI.baseStringPartialCompare("LPG Modal Image", "img/allstate/allstate-lpg-flyout.png", imageURL);
        }else{
            String merchandiseMSG = BaseUI.getTextFromField(Locator.lookupRequiredElement("lpg_Modal_lpgMerchandise_ActiveContent"));
            BaseUI.baseStringCompare("lpg_Modal_lpgMerchandise_ActiveContent", msgExpected, merchandiseMSG);
        }
    }

    public static void navigate_Merchandise_LPGMerchandisePage() throws Exception {
        if (ClientDataRetrieval.isLegacyClient()) {
            if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Allstate_both)) {
                BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
                BaseUI.waitForElementToBeDisplayed("lpgPage_LowPriceGuaranteeBanner", null, null);
            }else{
                BaseUI.click(Locator.lookupRequiredElement("homepage_lpgMerchandise_Tab"));
                BaseUI.waitForElementToBeDisplayed("homepage_Lpg_Terms_MerchandiseLink", null, null, 20);
                Thread.sleep(500);
                BaseUI.click(Locator.lookupRequiredElement("homepage_Lpg_Terms_MerchandiseLink"));
                BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
            }

        } else {
            BaseUI.click(Locator.lookupRequiredElement("homepage_LearnMoreLnk"));
            BaseUI.waitForElementToBeDisplayed("lpgPage_Title", null, null);
        }

        Thread.sleep(500);
    }
}
