package dRewards.pages;

import java.text.MessageFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import dRewards.ClassObjects.PricingDetails;
import dRewards.ClassObjects.Product;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class ShoppingCart {

    static String itemsInCart;
    static String itemsInCart_Increased, finalPriceBeforeAddingItemToCart, finalPriceAfterAdding_OneItemToCart,
            finalPriceAfterUpdatingItemToCart;
    static String itemsInCart_Decreased, finalPriceAfterRemovingItemFromCart, finalPriceAfterAdding_SecondItemToCart;
    static String itemsInCart_Increased_AfterQuantityUpdate, finalPointsAfterRemovingItemFromCart;

    static PricingDetails pricingDetails;
    static String retailPrice_FirstItem;
    static String points_FirstItem;
    static String finalAmount_YouPay;
    static String finalPrice_YouPay;
    static int finalRetailPrice_YouPay;
    static String finalPointsAfterAdding_OneItemToCart;
    static String finalPointsAfterAdding_SecondItemToCart;
    static String points_SecondItem;
    static String points_AllItems;
    static String retailPrice_SecondItem;
    static String retailPrice_AllItems;

    public static void remove_AllItems() throws Exception {
        // Creates a list of all of the Remove links.
        // We will use this list to identify how many Remove links are on the
        // page.
        ArrayList<WebElement> remove_Link_List = new ArrayList<WebElement>();
        remove_Link_List.addAll(Locator.lookup_multipleElements("shpCart_RemoveButton", null, null));
        // Loop through and remove all of the items in the shopping cart.
        // This for loop will go through each of the remove links and click
        // Remove.
        for (int i = 1; i <= remove_Link_List.size(); i++) {
            final int counter = i;
            BaseUI.click(Locator.lookupRequiredElement("shpCart_RemoveButton"));
            Thread.sleep(2000);
            BaseUI.wait_forPageToFinishLoading();
            Thread.sleep(500);
        }
    }

    public static void wait_ForPage_ToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("shpCart_Page_Header", null, null, 30);
        BaseUI.waitForElementToBeDisplayed("shpCart_RemoveButton", null, null, 10);
        Thread.sleep(500);
    }

    public static void verify_product_OnShoppingCartPage(String productText) {
        ArrayList<WebElement> listOfCheckout_Items_Elements = Locator
                .lookup_multipleElements("shpCart_ProductList_TextLink", null, null);
        ArrayList<String> listOfCheckout_Items = new ArrayList<String>();
        for (WebElement item : listOfCheckout_Items_Elements) {
            listOfCheckout_Items.add(item.getText());
        }

        boolean testPass = false;
        for (String product : listOfCheckout_Items) {
            if (product.contains(productText)) {
                testPass = true;
            }
        }

        BaseUI.verify_true_AndLog(testPass,
                MessageFormat.format("Found product {0} on Shopping Cart page.", productText),
                MessageFormat.format("Did NOT find product {0} on Shopping Cart page.", productText));
    }

    public static void click_ContinueShopping() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_ContinueShoppingButton"));
        Thread.sleep(500);
    }

    public static void click_ProceedToCheckout() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_ProceedToCheckoutButton"));
        BaseUI.waitForElementToBeDisplayed("checkout_Step1_ClickContinue_ElementToWaitFor", null, null, 30);
        Thread.sleep(500);
    }

    // Return a List of our Product Classes that contains all of the product
    // data.
    public static ArrayList<Product> retrieve_AllProductData() {
        ArrayList<Product> products = new ArrayList<Product>();
        ArrayList<WebElement> productRows = new ArrayList<WebElement>();
        productRows.addAll(Locator.lookup_multipleElements("shpCart_ProductRows", null, null));
        for (WebElement productRow : productRows) {
            Product product = new Product();
            if (ClientDataRetrieval.isRedesignClient()) {
                product.productName = BaseUI
                        .getTextFromField(productRow.findElement(By.xpath(".//td[@class='product-name']/a")));
                product.quantity = BaseUI.get_Attribute_FromField(
                        productRow.findElement(By.xpath("./following-sibling::tr[@class='item-controls'][1]//input[@type='tel']")), "value");
                product.pricing_Retail = BaseUI
                        .getTextFromField(productRow.findElement(By.xpath(".//td[@class='retail-price']")));
                product.pricing_Points = BaseUI
                        .getTextFromField(productRow.findElement(By.xpath(".//td[@class='points']")));
                product.pricing_YouPay = BaseUI
                        .getTextFromField(productRow.findElement(By.xpath(".//div[@class='price']")));
            } else {
                product.productName = BaseUI
                        .getTextFromField(productRow.findElement(By.xpath(".//div[@class='infoWrap']/a")));
                product.quantity = BaseUI.get_Attribute_FromField(
                        productRow.findElement(By.xpath(".//input[contains(@class,'qty')]")), "value");
                if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
                    product.pricing_Retail = BaseUI.getTextFromField(productRow
                            .findElement(By.xpath(".//td[@class='retail'][not(contains(@class,'retail label'))]")));
                    product.pricing_Points = BaseUI
                            .getTextFromField(productRow.findElement(By.xpath(".//span[@class='incentives']")));
                    product.pricing_YouPay = BaseUI
                            .getTextFromField(productRow.findElement(By.xpath(".//td[@class='price']")));
                    // product.pricing_PercentSavings =
                    // BaseUI.getTextFromField(productRow.findElement(By.xpath(".//div[@class='price-matrix']/span")));
                } else {
                    product.pricing_Retail = BaseUI
                            .getTextFromField(productRow.findElement(By.xpath(".//dd[@class='retail']")));
                    product.pricing_Points = BaseUI
                            .getTextFromField(productRow.findElement(By.xpath(".//dd[@class='incentives']")));
                    product.pricing_YouPay = BaseUI
                            .getTextFromField(productRow.findElement(By.xpath(".//dd[@class='price']")));
                    // productRow.findElement(By.xpath(".//dd[@class='price']")).getText();
                }
                product.pricing_PercentSavings = BaseUI
                        .getTextFromField(productRow.findElement(By.xpath(".//div[@class='price-matrix']/span")));
            }
            products.add(product);
        }

        return products;
    }

    // gets the table data utilizing our generic Base Class for retrieving table
    // data.
    public static TableData get_Table_Data_Old() throws Exception {
        HashMap<String, String> fields_toGet = new HashMap<String, String>();

        if (ClientDataRetrieval.isRedesignClient()) {
            fields_toGet.put("Product Name", ".//a/span[@class='description']");
            fields_toGet.put("quantity", ".//div[@class='operations']//div[contains(@class,'count-control medium')]/input[@class='inputtext-thin']");
            fields_toGet.put("pricing_Retail", ".//span[@class='retail']");
            fields_toGet.put("pricing_Points", ".//span[@class='points']");
            fields_toGet.put("pricing_YouPay", ".//span[@class='price']");
        } else {
            fields_toGet.put("Percent Savings", ".//div[@class='price-matrix']/span");

            fields_toGet.put("quantity", ".//input[contains(@class,'qty')]");
            fields_toGet.put("Product Name", ".//div[@class='infoWrap']/a|//div[@class='infoWrap']");

            if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
                fields_toGet.put("pricing_Retail", ".//td[@class='retail']");
                fields_toGet.put("pricing_Points", ".//span[@class='incentives']");
                fields_toGet.put("pricing_YouPay", ".//td[@class='price']");
            } else {
                fields_toGet.put("pricing_Retail", ".//dd[@class='retail']");
                fields_toGet.put("pricing_Points", ".//dd[@class='incentives']");
                fields_toGet.put("pricing_YouPay", ".//dd[@class='price']");
            }
        }

        TableData table_Data = new TableData();
        table_Data.data = BaseUI.tableExtractor("shpCart_ProductRows", fields_toGet);

        table_Data.split_column("Product Name");
        table_Data.trim_column("Product Name");
        table_Data.replace_Characters_FromBeginning("Gift Card", "", "Product Name");

        // String[] headers = {"Product Name", "quantity", "pricing_Retail",
        // "pricing_Points", "pricing_YouPay", "Percent Savings"};
        // table_Data = BaseUI.tableExtractor_ByCell("shpCart_RowCells",
        // headers);

        return table_Data;
    }

    // gets the table data utilizing our generic Base Class for retrieving table
    // data.
    public static TableData get_Table_Data() throws Exception {
        HashMap<String, String> fields_toGet = new HashMap<String, String>();
        if (ClientDataRetrieval.isRedesignClient()) {
            fields_toGet.put("Product Name", ".//td[@class='product-name']/a");
            fields_toGet.put("quantity", "./following-sibling::tr[@class='item-controls'][1]//input[@type='tel']");
            fields_toGet.put("pricing_Retail", ".//td[@class='retail-price']");
            fields_toGet.put("pricing_Points", ".//td[@class='points']");
            fields_toGet.put("pricing_YouPay", ".//div[@class='price']");
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {

            fields_toGet.put("Product Name", ".//div[@class='infoWrap']/a");
            fields_toGet.put("quantity", ".//input[contains(@class,'qty')]");
            fields_toGet.put("pricing_Retail", ".//td[@class='retail']");
            fields_toGet.put("pricing_Points", ".//span[@class='incentives']");
            fields_toGet.put("pricing_YouPay", ".//td[@class='price']");
            fields_toGet.put("Percent Savings", ".//div[@class='price-matrix']/span");
        } else {
            fields_toGet.put("Product Name", ".//div[@class='infoWrap']/a");
            fields_toGet.put("quantity", ".//input[contains(@class,'qty')]");
            fields_toGet.put("pricing_Retail", ".//dd[@class='retail']");
            fields_toGet.put("pricing_Points", ".//dd[@class='incentives']");
            fields_toGet.put("pricing_YouPay", ".//dd[@class='price']");
            fields_toGet.put("Percent Savings", ".//div[@class='price-matrix']/span");
        }

        TableData table_Data = new TableData();
        table_Data.data = BaseUI.tableExtractor("shpCart_ProductRows", fields_toGet);

//		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
//				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
//			String[] headers = { "Product Name", "quantity", "pricing_Retail", "pricing_Points", "pricing_YouPay" }; 
//			table_Data = BaseUI.tableExtractor_ByCell("shpCart_RowCells", headers);
//		} else {
//			String[] headers = { "Product Name", "quantity", "pricing_Retail", "pricing_Points", "pricing_YouPay",
//			"Percent Savings" };
//			table_Data = BaseUI.tableExtractor_ByCell("shpCart_RowCells", headers);
//		}
//
//		BaseUI.log_Status("Table extracted had entry count of: " + String.valueOf(table_Data.data.size()));
//		for (HashMap<String, String> row : table_Data.data) {
//			BaseUI.log_Status("Table Row: " + row.toString());
//	
        return table_Data;
    }

    public static PricingDetails retrieve_PricingDetails() {
        PricingDetails pricingDetails = new PricingDetails();
        pricingDetails.retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_RetailPrice"));
        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            pricingDetails.points = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_points"));
            // Get rid of dash.
            pricingDetails.points = pricingDetails.points.split("\\s")[1];
        }
        pricingDetails.priceAfterPoints = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_priceAfterPoints"));
        pricingDetails.savings_Percent = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_savingsPercent"));
//		if (BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_PriceDetails_Savings"))) {
//			pricingDetails.savings = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_Savings"));
//			pricingDetails.savings = pricingDetails.savings.split("\\s")[1];
//		}
        pricingDetails.shipping_Cost = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_shippingCost"));
        pricingDetails.order_Total = BaseUI
                .get_NonVisible_TextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_finalPrice")).split("\\n")[0];
        pricingDetails.order_Total = pricingDetails.order_Total.trim();
        pricingDetails.finalPoints_AfterAddingItem = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_points"));
        pricingDetails.finalRetailPrice_AfterAddingItem = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints"));

        if (ClientDataRetrieval.isRedesignClient()) {
            pricingDetails.tax_Cost = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_EstimatedSalesTax"));
        }

        return pricingDetails;
    }

    //
    public static void addItemsToCart_VerifyItemsPointsAndRetailPrice_MatchFinalValues(String subCategory_Name,
                                                                                       String subCategory1_Name, int startPage, int endPage) throws Exception {

        // Add 1st item to cart
        if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Navigation.navigate_Merchandise_AutoAccessories();
            Merchandise.add_To_Cart_ByNumber(BaseUI
                    .random_NumberAsString(1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())));
        } else {
            Merchandise.addItems_To_ShoppingCart(subCategory1_Name, startPage, endPage);
        }

        pricingDetails = ShoppingCart.retrieve_PricingDetails();
        itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
        retailPrice_FirstItem = ShoppingCart.retrieve_AllProductData().get(0).pricing_Retail;
        points_FirstItem = ShoppingCart.retrieve_AllProductData().get(0).pricing_Points;
        finalPointsAfterAdding_OneItemToCart = ShoppingCart.retrieve_PricingDetails().finalPoints_AfterAddingItem.replace("- ", "");
        finalPriceAfterAdding_OneItemToCart = ShoppingCart.retrieve_PricingDetails().finalRetailPrice_AfterAddingItem;
        finalAmount_YouPay = (ShoppingCart.retrieve_AllProductData().get(0).pricing_YouPay
                .replace("$", "").trim());
        finalPrice_YouPay = String.valueOf(finalAmount_YouPay);

        BaseUI.baseStringCompare("shpCart_PriceDetails_points", points_FirstItem, finalPointsAfterAdding_OneItemToCart);
        BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_FirstItem,
                finalPriceAfterAdding_OneItemToCart);

        // Add 2nd item to cart
        if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Navigation.navigate_Merchandise_AutoAccessories();
            Merchandise.add_To_Cart_ByNumber(BaseUI
                    .random_NumberAsString(1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())));
        } else {
            Merchandise.addItems_To_ShoppingCart(subCategory_Name, startPage, endPage);
        }

        itemsInCart_Increased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
        points_SecondItem = ShoppingCart.retrieve_AllProductData().get(1).pricing_Points;
        retailPrice_SecondItem = ShoppingCart.retrieve_AllProductData().get(1).pricing_Retail;
        finalPointsAfterAdding_SecondItemToCart = (ShoppingCart.retrieve_PricingDetails().finalPoints_AfterAddingItem)
                .replace(",", "").replace("$", "").replaceAll("\\.00", "").replace("- ", "").replace("– ","-").trim();
        finalPointsAfterAdding_SecondItemToCart = GlobalVariables.replace_NonStandardMinus_WithStandardMinus(finalPointsAfterAdding_SecondItemToCart).replace("- ","-");
        finalPriceAfterAdding_SecondItemToCart = (ShoppingCart
                .retrieve_PricingDetails().finalRetailPrice_AfterAddingItem).replace(",", "").replace("$", "")
                .replace("– ","-").trim();



        finalPrice_YouPay = (ShoppingCart.retrieve_PricingDetails().priceAfterPoints.replace("$", "").trim());
        retailPrice_AllItems = PricingDetails.validate_ItemsPoints_Added_ToFinalPoints(retailPrice_FirstItem,
                retailPrice_SecondItem);

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
                || ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            // |ClientDataRetrieval.clientName.startsWith("Allstate")){
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_AllItems,
                    finalPriceAfterAdding_SecondItemToCart);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            points_AllItems = PricingDetails.validate_ItemsPoints_Added_ToFinalPoints(points_FirstItem,
                    points_SecondItem);
            BaseUI.baseStringCompare("shpCart_PriceDetails_points", points_AllItems.replaceAll("\\.00", ""),
                    finalPointsAfterAdding_SecondItemToCart);
            BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_AllItems,
                    finalPriceAfterAdding_SecondItemToCart);
        } else {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            points_AllItems = PricingDetails.validate_ItemsPoints_Added_ToFinalPoints(points_FirstItem,
                    points_SecondItem);
            BaseUI.baseStringCompare("shpCart_PriceDetails_points", points_AllItems.replaceAll("\\.00", ""),
                    finalPointsAfterAdding_SecondItemToCart);
            BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_AllItems,
                    finalPriceAfterAdding_SecondItemToCart);
            BaseUI.baseStringCompare("shpCart_PriceDetails_priceAfterPoints", finalPrice_YouPay, (PricingDetails
                    .validate_ItemsPoints_Subtracted_ToFinalPrice(points_AllItems, retailPrice_AllItems)));
        }
    }

    // Remove or delete first item from shopping cart
    public static void removeFirstItemFromShoppingCart() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_RemoveFirstItem"));
        Thread.sleep(1000);
    }

    // Remove or delete first item from shopping cart
    public static void removeItemFromShoppingCart() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_RemoveItem"));
        Thread.sleep(1000);
    }

    // Remove or delete last item from shopping cart
    public static void removeLastItemFromShoppingCart() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_RemoveLastItem"));
        Thread.sleep(1000);
    }

    // Update first item from shopping cart
    public static void updateFirstItemFromShoppingCart(String newQuantity) throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_UpdateFirstItem"));
        BaseUI.waitForElementToContain_AttributeMatch("shpCart_EnterQuantity_FirstItem", null, null,
                "value", newQuantity, 40);
        Thread.sleep(1000);
    }

    // Update last item from shopping cart shpCart_UpdateOnlyItem
    public static void updateLastItemFromShoppingCart(String newQuantity) throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_UpdateLastItem"));
        Thread.sleep(1000);
//        BaseUI.waitForElementToContain_AttributeMatch("shpCart_EnterQuantity_LastItem", null, null, "value",
//                newQuantity, 40);
        BaseUI.waitForElementToBeDisplayed("shpCart_EnterQuantity_LastItem", newQuantity, null, 30);
    }

    // Update last item from shopping cart
    public static void updateOnlyItemInShoppingCart() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("shpCart_UpdateOnlyItem"));
        Thread.sleep(1000);
    }

    // Enter quantity for first item from shopping cart
    public static void enterQuantity_FirstItemInShoppingCart(String quantity) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("shpCart_EnterQuantity_FirstItem"), quantity);
        Thread.sleep(1000);
    }

    // Enter quantity for last item from shopping cart
    public static void enterQuantity_LastItemInShoppingCart(String quantity) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("shpCart_EnterQuantity_LastItem"), quantity);
        Thread.sleep(1000);
    }

    // Enter quantity for last item from shopping cart
    public static void enterQuantity_OnlyItemInShoppingCart(String quantity) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("shpCart_OnlyItemInCart_Quantity"), quantity);
        Thread.sleep(1000);
    }

    // Increase or decrease quantity for 1st item and verify the items in cart and
    // price increase or decrease as well accordingly
    public static void enterQuantity_AndVerify_ItemsCart_Price_Change(String quantity) throws Exception {

        // Increase the quantity for first item and click Update button
        itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
        ShoppingCart.enterQuantity_LastItemInShoppingCart(quantity);
        ShoppingCart.updateLastItemFromShoppingCart(quantity);

        // Now Verify if the price increased with increase in quantity
        itemsInCart_Increased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
        ArrayList<Product> cartData = ShoppingCart.retrieve_AllProductData();

        retailPrice_FirstItem = cartData.get(0).pricing_Retail;
        points_FirstItem = cartData.get(0).pricing_Points;
        /*
         * finalPriceAfterIncreasingQuantity_FirstItemInCart = BaseUI
         * .getTextFromField(Locator.lookupRequiredElement(
         * "shpCart_PriceDetails_OrderFinalPrice"));
         * finalPointsAfterIncreasingQuantity_FirstItemInCart = BaseUI
         * .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_points"));
         */

        points_SecondItem = cartData.get(1).pricing_Points;
        retailPrice_SecondItem = cartData.get(1).pricing_Retail;
        finalPointsAfterAdding_SecondItemToCart = (ShoppingCart.retrieve_PricingDetails().finalPoints_AfterAddingItem)
                .replace(",", "").replace("$", "").replace(",", "")
                .replace("\u002D", "").replace("\u2013", "").replace("– ","").trim();
        finalPriceAfterAdding_SecondItemToCart = (ShoppingCart
                .retrieve_PricingDetails().finalRetailPrice_AfterAddingItem).replace(",", "").replace("$", "")
                .replace(",", "").trim();
        finalAmount_YouPay = /* Double.parseDouble */(ShoppingCart.retrieve_PricingDetails().priceAfterPoints
                .replace("$", "").replace(",", "").trim());
        // finalRetailPrice_YouPay = finalAmount_YouPay.intValue();
        finalPrice_YouPay = String.valueOf(finalAmount_YouPay);
        retailPrice_AllItems = PricingDetails.validate_ItemsPoints_Added_ToFinalPoints(retailPrice_FirstItem,
                retailPrice_SecondItem);

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
                || ClientDataRetrieval.client_Matches(client_Designation.Allstate_both))
        // | ClientDataRetrieval.clientName.startsWith("Allstate"))
        {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_AllItems,
                    finalPriceAfterAdding_SecondItemToCart);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
                || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
                || ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            points_AllItems = PricingDetails
                    .validate_ItemsPoints_Added_ToFinalPoints(points_FirstItem, points_SecondItem)
                    .replaceAll("\\.00", "");
            BaseUI.baseStringCompare("shpCart_PriceDetails_points", points_AllItems.replace("- ", "").replace("\u002D", ""),
                    finalPointsAfterAdding_SecondItemToCart.replace("\u002D", ""));
            BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_AllItems,
                    finalPriceAfterAdding_SecondItemToCart);
        } else {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCart, itemsInCart_Increased);
            points_AllItems = PricingDetails.validate_ItemsPoints_Added_ToFinalPoints(points_FirstItem,
                    points_SecondItem);
            BaseUI.baseStringCompare("shpCart_PriceDetails_points", points_AllItems.replace("-","")
                            .replace("\u002D", "").replace("\u2013", ""), finalPointsAfterAdding_SecondItemToCart);
            BaseUI.baseStringCompare("shpCart_ProductDetails_RetailPrice_BeforeTakingPoints", retailPrice_AllItems,
                    finalPriceAfterAdding_SecondItemToCart);
            BaseUI.baseStringCompare("shpCart_PriceDetails_priceAfterPoints", finalPrice_YouPay, (PricingDetails
                    .validate_ItemsPoints_Subtracted_ToFinalPrice(points_AllItems, retailPrice_AllItems)));
        }
    }

    // Increase or decrease quantity for 1st item and verify the items in cart and
    // price increase or decrease as well accordingly
    public static void enterQuantityForOnlyItem_AndVerify_ItemsInCart_Price_Update(String quantity,
                                                                                   String itemsInCartBefore_QuantityUpdate, String finalPriceBefore_QuantityUpdate,
                                                                                   String finalPointsAfterAddingItemToCart) throws Exception {

        // Increase the quantity for first item and click Update button
        ShoppingCart.enterQuantity_OnlyItemInShoppingCart(quantity);
        ShoppingCart.updateOnlyItemInShoppingCart();

        // Now Verify if the price increased with increase in quantity
        itemsInCart_Increased_AfterQuantityUpdate = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
        finalPriceAfterUpdatingItemToCart = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));
        finalPointsAfterRemovingItemFromCart = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_points"));
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCartBefore_QuantityUpdate,
                    itemsInCart_Increased_AfterQuantityUpdate);
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
                    finalPriceBefore_QuantityUpdate, finalPriceAfterUpdatingItemToCart);
        } else {
            BaseUI.baseStringCompareStringsAreDifferent("Shp_ItemsInCart", itemsInCartBefore_QuantityUpdate,
                    itemsInCart_Increased_AfterQuantityUpdate);
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
                    finalPriceBefore_QuantityUpdate, finalPriceAfterUpdatingItemToCart);
            BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_points", finalPointsAfterAddingItemToCart,
                    finalPointsAfterRemovingItemFromCart);
        }
    }

    //
    public static void updateQuantity_FirstItemInCart(String itemsInCart, String quantity,
                                                      String itemsInCartBefore_QuantityUpdate, String finalPriceBefore_QuantityUpdate,
                                                      String finalPointsAfterAddingItemToCart) throws Exception {

        if (Integer.valueOf(itemsInCart) != 1) {
            ShoppingCart.enterQuantity_AndVerify_ItemsCart_Price_Change(quantity);
        } else {
            ShoppingCart.enterQuantityForOnlyItem_AndVerify_ItemsInCart_Price_Update(quantity,
                    itemsInCartBefore_QuantityUpdate, finalPriceBefore_QuantityUpdate,
                    finalPointsAfterAddingItemToCart);
        }
    }

    // Verify shopping cart is displayed and if true delete all the items from cart
    public static void verifyShoppingCartDisplayed_AndDelete_ItemsInCart() throws Exception {
        Homepage.deleteItemsFromShoppingCart();

        if (ClientDataRetrieval.isRedesignClient()) {
            String itemsInShoppingCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
            BaseUI.verify_true_AndLog(itemsInShoppingCart.contains("0"), "Shopping Cart is Empty",
                    "Shopping Cart is not Empty");
        } else {
            BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("Shp_ItemsInCart"));
        }
    }

    public static void verifyShoppingCartDisplayed_OnHomePage(String subCategory1_Name, int startPage, int endPage)
            throws Exception {

        if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_ShoppingCartLink"))) {
            BaseUI.log_Status("Shopping Cart icon is displayed on Home Page");
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("Shp_ItemsInCart"));
        } else {
            Merchandise.addItems_To_ShoppingCart(subCategory1_Name, startPage, endPage);
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink"));
        }
    }

    public static void verify_DailyDeals_AddedToCart_Popup() throws Exception {

        String expected_DailyDeal_PopupDescription;
        String expected_DailyDeal_PopupTitle = "The Daily Deal has been added to your cart";
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            expected_DailyDeal_PopupDescription = "We will reserve this Daily Deal for you for 20 Minute(s) or until the next Daily Deal is posted (at 1PM ET daily), whichever comes first. That means you can take advantage of other great deals on our website without the need to check out immediately. Happy shopping!";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            expected_DailyDeal_PopupDescription = "We will reserve this Daily Deal for you for 20 minutes or until the next Daily Deal is posted (at 3PM ET daily), whichever comes first. That means you can take advantage of other great deals on our website without the need to checkout immediately. Happy shopping!";
        } else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            expected_DailyDeal_PopupDescription = "We will reserve this Daily Deal for you for 1 Minute(s) or until the next Daily Deal is posted (at 3PM ET daily), whichever comes first. That means you can take advantage of other great deals on our website without the need to check out immediately. Happy shopping!";
        } else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
                || ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            expected_DailyDeal_PopupDescription = "We will reserve this Daily Deal for you for 20 Minute(s) or until the next Daily Deal is posted (at 3PM ET daily), whichever comes first. That means you can take advantage of other great deals on our website without the need to check out immediately. Happy shopping!";
        } else {
            expected_DailyDeal_PopupDescription = "We will reserve this Daily Deal for you for 20 Minute(s) or until the next Daily Deal is posted (at 3 PM ET. daily), whichever comes first. That means you can take advantage of other great deals on our website without the need to check out immediately. Happy shopping!";
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_Modal"));
        String dailyDeal_PopupTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_Title"));
        String dailyDeal_PopupDescription = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_Description"));
        BaseUI.baseStringCompare("DailyDeal Popup Title", expected_DailyDeal_PopupTitle, dailyDeal_PopupTitle);
        BaseUI.baseStringCompare("DailyDeal Popup Description", expected_DailyDeal_PopupDescription,
                dailyDeal_PopupDescription);
    }

    public static void close_DailyDeal_AddedToCart_Popup() throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
            BaseUI.click_js(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_CloseButton"));
        } else {
            BaseUI.click(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_CloseButton"));
        }
        BaseUI.waitForElementToNOTBeDisplayed("shpCart_DailyDeal_Popup_CloseButton", null, null, 10);
        Thread.sleep(500);
    }

    public static void verify_CartEmptyText_Appears() {
        WebElement emptyCartElement = Locator.lookupRequiredElement("shpCart_CartEmpty_Text");
        BaseUI.verifyElementAppears(emptyCartElement);
        String emptyCartText = BaseUI.getTextFromField(emptyCartElement);
        BaseUI.verify_true_AndLog(emptyCartText.contains("Your shopping cart is currently empty") || emptyCartText.contains("0 items in your shopping cart")
                        || emptyCartText.contains("Your shopping cart is empty"),
                "Shopping Cart is EMPTY", "Shopping Cart is NOT EMPTY");
    }

    public static void verify_DailyDeal_Limit_One_PerCustomer_Message(HashMap<String, String> dailyDealData) {
        String errorMessage;
        if (dailyDealData.get("dailyDealName").contains("Magazine")) {
            errorMessage = "This Magazine Is Already In Your Shopping Cart. The Quantity Of Magazine Is Limited To One";
        } else {
            if (ClientDataRetrieval.isRedesignClient()) {
                errorMessage = "You already have today's Daily Deal in your cart. You can purchase only 1 item per offer per customer.";
            } else {
                errorMessage = "You Already Have Today's Daily Deal In Your Cart. You Can Purchase Only 1 Item Per Offer Per Customer.";
            }
        }

        String actualErrorMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Error_Message"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_DailyDeal_Error_Message"));
        BaseUI.baseStringCompare("Daily Deal Limit Message", errorMessage, actualErrorMessage);
    }

    public static void verify_DailyDeal_Reached_Max_Limit_Message() {
        String errorMessage;
        if (ClientDataRetrieval.isRedesignClient()) {
            errorMessage = "You already reached the maximum purchase limit for this item.You can purchase only 1 item per offer per customer.";
        } else {
            errorMessage = "You Already Reached The Maximum Purchase Limit For This Item.You Can Purchase Only 1 Item Per Offer Per Customer.";
        }
        String actualErrorMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Error_Message"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_DailyDeal_Error_Message"));
        BaseUI.baseStringCompare("Daily Deal Limit Message", errorMessage, actualErrorMessage);
    }
}
