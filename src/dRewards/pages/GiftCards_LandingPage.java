package dRewards.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.WebElement;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class GiftCards_LandingPage {

	public static ArrayList<String> return_GiftCards_NavMenuList() throws Exception {

		ArrayList<WebElement> giftCardsNavMenuList = Locator.lookup_multipleElements("giftCards_Left_NavMenu_List",
				null, null);

		ArrayList<String> menuList = new ArrayList<String>();

		for (WebElement menu : giftCardsNavMenuList) {
			String menuStr = menu.getText();
			menuList.add(menuStr);
		}

		return menuList;
	}

	public static void navigate_LeftPanel_ByText(String linkText) throws Exception {
		if(! BaseUI.elementExists("giftCards_LeftPanelNavigation_ByText", linkText, null)) {
			BaseUI.log_AndFail(linkText + " : category does not exists");
		} else {
			BaseUI.click(Locator.lookupRequiredElement("giftCards_LeftPanelNavigation_ByText", linkText, null));
			BaseUI.waitForElementToBeDisplayed("giftCards_CategoryLandingPage_WaitElement", null, null, 40);
			BaseUI.wait_forPageToFinishLoading();
			Thread.sleep(1000);
			checkIfLandingPage_OopsMessageDisplayedOrNot(linkText);
		}
	}

	public static void sortBy(String sortOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("giftCards_SortBy_Dropdown"), sortOption);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}

	public static TableData return_GiftCards_TableData() {
		HashMap<String, String> tableMappings = new HashMap<String, String>();
		
		if(ClientDataRetrieval.isRedesignClient()) {
			tableMappings.put("giftcards", ".//div[@class='left']/p");
			tableMappings.put("youPay", ".//div[@class='price-matrix']/div[@class='rewards-price']");
			tableMappings.put("points", ".//div[@class='price-matrix']/div[@class='points-required']");
			tableMappings.put("retailRate", ".//div[@class='price-matrix']/div[@class='retail-price']");
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				tableMappings.put("giftcards", ".//div[@class='description']/h4/a");
				tableMappings.put("youPay", ".//td[@class='price']");
				tableMappings.put("points", ".//span[@class='incentives']");
				tableMappings.put("retailRate", ".//td[@class='retail']");
			} else {
				tableMappings.put("giftcards", ".//div[@class='description']/h1/a");
				tableMappings.put("youPay", ".//dd[@class='price']");
				tableMappings.put("points", ".//dd[@class='incentives']");
				tableMappings.put("retailRate", ".//dd[@class='retail']");
			}
		}	

		TableData giftCardsTable = new TableData();
		giftCardsTable.data = BaseUI.tableExtractor("giftCards_SearchResult_RowList", tableMappings);
		giftCardsTable.remove_Character("$", "youPay");
		giftCardsTable.remove_Character(",", "youPay");
		giftCardsTable.replace_Character("\u2013", "", "points");
		giftCardsTable.remove_Character("after ", "points");
		giftCardsTable.remove_Character("Points", "points");
		giftCardsTable.remove_Character(",", "points");
		giftCardsTable.remove_Character("$", "retailRate");
		giftCardsTable.remove_Character(",", "retailRate");
		giftCardsTable.remove_Character(" Retail: ", "retailRate");
		giftCardsTable.remove_Character(" points", "points");

		return giftCardsTable;
	}

	public static void resultPerPage(String resultPerPageOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("giftCards_SearchResult_ResultPerPage_Dropdown"),
				resultPerPageOption);
		BaseUI.wait_forPageToFinishLoading();
	}

	public static Integer return_ResultsCount() {
		String resultToReturn = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_TopResult_Count"))
				.replaceAll("Deals.", "").trim();
		resultToReturn = resultToReturn.substring(resultToReturn.indexOf("of ") + 3, resultToReturn.length());

		return Integer.parseInt(resultToReturn);
	}

	public static Integer return_ShowingItemsCount() {
		String showingItemsToReturn = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_TopResult_Count"));
		showingItemsToReturn = showingItemsToReturn
				.substring(showingItemsToReturn.indexOf("-") + 1, showingItemsToReturn.lastIndexOf("of")).trim();

		return Integer.parseInt(showingItemsToReturn);
	}

	public static void navigate_ToPage_ByTOP_PageNumber(Integer pageNumber) throws Exception {
		ArrayList<String> pageCount = retrieve_TotalNumberOfPages();
		if(pageCount.size() > 1) {
			BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_PageNumber_Top_Link_ByText", pageNumber.toString(),
					null));
			Thread.sleep(1500);
		} else {
			BaseUI.log_AndFail("Total number pages on Gift Card category page is not greater than 1");
		}
	}

	public static void navigate_NextPage_ByTOP_Link() throws Exception {
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			BaseUI.click_js(Locator.lookupRequiredElement("giftCards_SearchResult_NextPage_Top_Button"));
			Thread.sleep(1500);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_NextPage_Top_Button"));
			Thread.sleep(1500);
		}
	}

	public static void navigate_PreviousPage_ByTOP_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_PreviousPage_Top_Button"));
		Thread.sleep(1500);	
	}

	public static void navigate_ToPage_ByBOTTOM_PageNumber(Integer pageNumber) throws Exception {
		ArrayList<String> pageCount = retrieve_TotalNumberOfPages();
		if(pageCount.size() > 1) {
			BaseUI.click_js(Locator.lookupRequiredElement("giftCards_SearchResult_PageNumber_BOTTOM_Link_ByText",
					pageNumber.toString(), null));
			Thread.sleep(1500);
		} else {
			BaseUI.log_AndFail("Total number pages on Gift Card category page is not greater than 1");
		}
	}

	public static void navigate_NextPage_ByBOTTOM_Link() throws Exception {
		if(BaseUI.element_isEnabled(Locator.lookupRequiredElement("giftCards_SearchResult_NextPage_BOTTOM_Button"))){
			BaseUI.click_js(Locator.lookupRequiredElement("giftCards_SearchResult_NextPage_BOTTOM_Button"));
			Thread.sleep(1500);
		} else {
			BaseUI.log_AndFail("Bottom Next Page link is not active or enabled to click");
		}
	}

	public static void navigate_PreviousPage_ByBOTTOM_Link() throws Exception {
		if(BaseUI.element_isEnabled(Locator.lookupRequiredElement("giftCards_SearchResult_PreviousPage_BOTTOM_Button"))){
			BaseUI.click_js(Locator.lookupRequiredElement("giftCards_SearchResult_PreviousPage_BOTTOM_Button"));
			Thread.sleep(1500);
		} else {
			BaseUI.log_AndFail("Bottom Previous Page link is not active or enabled to click");
		}
	}

	public static void click_BackToTop_Link() throws Exception {
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			BaseUI.click_js(Locator.lookupRequiredElement("giftCards_SearchResult_BackToTop_Link"));
			Thread.sleep(200);
		} else {
			BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_BackToTop_Link"));
			Thread.sleep(200);
		}
	}

	public static void verify_BackToTopButton() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("giftCards_SearchResult_BackToTop_Link"));
		click_BackToTop_Link();
		BaseUI.verify_WindowPosition_Is_AtTop();
	}

	// Assumes default results is 48 for all clients except AARP
	// Assumes default results is 12 for AARP
	public static void verify_Page_Active(Integer pageNumber) throws Exception {
		ArrayList<String> pageCount = retrieve_TotalNumberOfPages();
		if(pageCount.size() > 1) {
			if(BaseUI.element_isEnabled(Locator.lookupRequiredElement("giftCards_SearchResult_PageNumber_Top_Link_ByText", pageNumber.toString(), null))) {
				WebElement pageToVerify = Locator.lookupRequiredElement("giftCards_SearchResult_PageNumber_Top_Link_ByText",
						pageNumber.toString(), null);
				if(BaseUI.elementAppears(pageToVerify)) {
					BaseUI.verifyElementHasExpectedAttributeValue(pageToVerify, "class", "goToPage");
					Integer entryCount = return_ResultsCount();

					Integer startingNumber = 0;
					Integer endNumber = 0;
					String expectedResult = "";
					if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
						if (entryCount >= pageNumber * 12) {
							startingNumber = pageNumber * 12 - 11;
							endNumber = pageNumber * 12;

						} else {
							startingNumber = pageNumber * 12 - 11;
							endNumber = entryCount;
						}
					} else {
						if (entryCount >= pageNumber * 48) {
							startingNumber = pageNumber * 48 - 47;
							endNumber = pageNumber * 48;

						} else {
							startingNumber = pageNumber * 48 - 47;
							endNumber = entryCount;
						}
					}

					expectedResult = MessageFormat.format("Showing {0}-{1} of {2}", startingNumber, endNumber, entryCount);

					String resultToReturn = BaseUI
							.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_TopResult_Count"));

					BaseUI.baseStringCompare("Entries visible.", expectedResult, resultToReturn);
				}
			} else {
				BaseUI.log_AndFail(pageNumber.toString() + " Page number is not active or page number does not exists");
			}
		} else {
			BaseUI.log_AndFail("Total number pages on Gift Card category page is not greater than 1");
		}

	}

	public static void verify_resultPerPage(String resultPerPage) throws Exception {
		TableData previousResultList = return_GiftCards_TableData();
		Integer previousShowingItemsCount = return_ShowingItemsCount();

		resultPerPage(resultPerPage);

		TableData currentResultList = return_GiftCards_TableData();
		Integer currentShowingItemsCount = return_ShowingItemsCount();

		BaseUI.verify_true_AndLog(previousResultList.data.size() != currentResultList.data.size(),
				MessageFormat.format("Previous Result List {0}does NOT match Current Result List{1}.",
						previousResultList, currentResultList),
				MessageFormat.format("Previous Result List {0} does match Current Result List {1}.", previousResultList,
						currentResultList));
		BaseUI.verify_true_AndLog(previousShowingItemsCount != currentShowingItemsCount,
				"Showing Items counts did NOT match.", "Showing Items counts matched.");
		Integer resultPerPageInt = Integer.parseInt(resultPerPage);
		BaseUI.verify_true_AndLog(currentShowingItemsCount.equals(resultPerPageInt),
				MessageFormat.format("Showing Items {0} matches item per page {1}.", currentShowingItemsCount,
						resultPerPageInt),
				MessageFormat.format("Showing Items {0} does Not matches item per page{1}.", currentShowingItemsCount,
						resultPerPageInt));				
	}

	public static void click_ListView_Button() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_ListView_Button"));
		Thread.sleep(500);
	}

	public static ArrayList<String> retrieve_FeaturedCategories_SubcategoryHeader() {

		ArrayList<WebElement> giftCardsFeaturedCategories = Locator
				.lookup_multipleElements("giftCards_FeaturedCategories_Subcategory", null, null);

		ArrayList<String> subcategoryList = new ArrayList<String>();

		for (WebElement Option : giftCardsFeaturedCategories) {
			subcategoryList.add(BaseUI.getTextFromField(Option));
		}
		return subcategoryList;
	}

	// Validate Featured Categories Header title.
	public static void verify_FeaturedCategories_Subcategory() {

		ArrayList<String> subcategoryHeaderList = retrieve_FeaturedCategories_SubcategoryHeader();
		
		BaseUI.verify_true_AndLog(subcategoryHeaderList.size() == 3, "Found sub categories",
				"Did NOT find sub categories.");

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_FeaturedCategories_Subcategory"));

		for (String subcategoryOption : subcategoryHeaderList) {
			BaseUI.verify_true_AndLog(
					subcategoryOption.contains("Dining") | subcategoryOption.contains("Shopping")
							| subcategoryOption.contains("Travel") | subcategoryOption.contains("Entertainment"),
					subcategoryOption + " Subcategory displayed under Gift card Featured Categories Option.",
					subcategoryOption + " Subcategory is not displayed under Gift card Featured Categories Option.");
		}
	}

	public static void Click_FeaturedCategories_SubcategoryHeader_LinkText(String subcategorylinkText)
			throws Exception {
		BaseUI.click(
				Locator.lookupRequiredElement("giftCards_FeaturedCategories_Subcategory_LinkText", subcategorylinkText, null));
		BaseUI.wait_forPageToFinishLoading();
		checkIfLandingPage_OopsMessageDisplayedOrNot(subcategorylinkText);
		Thread.sleep(1000);
	}

	// Validating all the links displayed under each featured categories
	public static ArrayList<String> retrieve_Links_BelowSubcategoryImage(String category) throws Exception {

		ArrayList<WebElement> category_SubCategoryElementList = Locator.lookup_multipleElements(
				"giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName", category, null);

		ArrayList<String> category_SubCategoryTextList = new ArrayList<String>();

		for (WebElement Option : category_SubCategoryElementList) {
			String linkText = BaseUI.getTextFromField(Option);
			category_SubCategoryTextList.add(linkText);
		}

		return category_SubCategoryTextList;
	}

	// Validating Image is displayed for each one of Featured categories
	public static void verify_GiftCardFeaturedCategories_EachSubcategoryImage() {

		ArrayList<String> subcategoryList = retrieve_FeaturedCategories_SubcategoryHeader();

		BaseUI.verify_true_AndLog(subcategoryList.size() == 3, "Found sub category headers",
				"Did NOT find sub category headers.");

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubcategoryImage"));

		for (String subcategoryHeader : subcategoryList) {

			String firstLink = BaseUI.getTextFromField(Locator.lookupRequiredElement(
					"giftCards_FeaturedCategories_SubCategory_FirstLink_ByCategoryName", subcategoryHeader, null));
			firstLink = firstLink.replace("'", "").replace(" ", "").replace("_", "");
			firstLink = firstLink.toLowerCase();

			WebElement header_Image = Locator.lookupRequiredElement(
					"giftCards_FeaturedCategories_SubcategoryImage_ByCategoryName", subcategoryHeader, null);
			String imgSrc = header_Image.getAttribute("src");
			// String imgSrc = subcategoryImage.get(i).getAttribute("src");
			BaseUI.verify_true_AndLog(imgSrc.endsWith(".jpg"),
					"Image is displayed for " + subcategoryHeader + " category : " + imgSrc,
					"Image is displayed for " + subcategoryHeader + " category.");

			// commenting out this validation because it is giving out error for
			// someclients.
			// BaseUI.verify_true_AndLog(imgSrc.toLowerCase().contains(firstLink),
			// "Image source contained " + firstLink,
			// "Image source did NOT contain " + firstLink);
		}
	}

	// Validating all the links displayed under each featured categories
	public static void verify_Links_BelowSubcategoryImage(String category) throws Exception {

		ArrayList<WebElement> category_SubCategoryElementList = Locator.lookup_multipleElements(
				"giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName", category, null);

		ArrayList<String> category_SubCategoryTextList = new ArrayList<String>();

		for (WebElement Option : category_SubCategoryElementList) {
			String linkText = BaseUI.getTextFromField(Option);
			category_SubCategoryTextList.add(linkText);
		}

		BaseUI.verify_true_AndLog(category_SubCategoryTextList.size() <= 5, "Found sub categories" + category_SubCategoryTextList.size(),
				"Did NOT find sub categories." + category_SubCategoryTextList.size());

		BaseUI.verify_true_AndLog(category_SubCategoryTextList.contains("View All..."), "Found View All.. link",
				"Did NOT find View All... link.");

	}

	public static void click_FeaturedCategories_Subcategory_LinkText(String subcategorylinkText, 
			String subcategoryHeader) throws Exception {
		if(!(subcategorylinkText.contains("View All..."))) {
			BaseUI.click(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText",
					subcategorylinkText, null));
			BaseUI.wait_forPageToFinishLoading();
			checkIfLandingPage_OopsMessageDisplayedOrNot(subcategorylinkText);
			Thread.sleep(1000);						
		} else  { 
			if (subcategoryHeader.contains("Travel")) {
				BaseUI.click_js(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_Travel_ViewAll"));
			    Thread.sleep(1000);
			    BaseUI.wait_forPageToFinishLoading();
			    checkIfLandingPage_OopsMessageDisplayedOrNot(subcategorylinkText);
			} else if (subcategoryHeader.contains("Dining")) { 
				BaseUI.click_js(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_Dining_ViewAll"));
			    Thread.sleep(1000);
			    BaseUI.wait_forPageToFinishLoading();
			    checkIfLandingPage_OopsMessageDisplayedOrNot(subcategorylinkText);
			} else if(subcategoryHeader.contains("Shopping")){ 
				BaseUI.click_js(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_Shopping_ViewAll"));
			    Thread.sleep(1000);
			    BaseUI.wait_forPageToFinishLoading();
			    checkIfLandingPage_OopsMessageDisplayedOrNot(subcategorylinkText);
			}
		}	
    }	

	
	public static ArrayList<String> return_GiftCards_FeaturedGiftCards() throws Exception {

		ArrayList<WebElement> featuredGiftCardsList = Locator.lookup_multipleElements("giftCards_FeaturedGiftCard_List",
				null, null);

		ArrayList<String> giftCardList = new ArrayList<String>();

		for (WebElement menu : featuredGiftCardsList) {
			String menuStr = menu.getText();
			giftCardList.add(menuStr);
		}

		return giftCardList;
	}
		
	//This method is added to test Redesign clients Featured Gift Card details	
	public static TableData return_FeaturedGiftCard_TableData() {
		HashMap<String, String> tableMappings = new HashMap<String, String>();
		
		tableMappings.put("featured_GiftCardTitle", ".//div[@class='gift-card-description-container']//h4");
		tableMappings.put("featured_GiftCardName", ".//div[@class='gift-card-description-container']//p");
		tableMappings.put("featured_GiftCardImage", ".//div[@class='image-box']//img");
		tableMappings.put("giftCard_RetailPrice", ".//div[@class='gift-card-description-container']//div[@class='retail-price']");
		tableMappings.put("giftCard_Points", ".//div[@class='gift-card-description-container']//div[@class='points-required']");
		tableMappings.put("giftCard_YouPay", ".//div[@class='gift-card-description-container']//div[@class='rewards-price']");
		tableMappings.put("giftCard_PercentSavings", ".//div[@class='gift-card-description-container']//div[@class='savings']");

		TableData featuredGiftCardTable = new TableData();
		featuredGiftCardTable.data = BaseUI.tableExtractor("giftCards_FeaturedGiftCardRows", tableMappings);

		return featuredGiftCardTable;
	}
	
	public static void clickViewAllOption() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("giftCard_ViewAllBtn"));
		BaseUI.waitForElementToBeDisplayed("giftCard_ViewAllLandingPage_HeaderTitle", null, null, 30);
		Thread.sleep(1000);
	}
	
	public static void checkIfLandingPage_OopsMessageDisplayedOrNot(String subcategoryLinkText) {
        if(BaseUI.elementAppears(Locator.lookupOptionalElement("categoryLandingPage_OopsMessage"))) {
			BaseUI.log_AndFail(subcategoryLinkText + " landing page displays: Oops! We have no matches for your entry");
		} else {
			BaseUI.waitForElementToBeDisplayed("giftCards_CategoryLandingPage_WaitElement", null, null, 30);
		}
	}

	public static void verifyLinksDisplayedBelowEachGiftCardSubcategory(String  subcategoryHeader, String link_subcategoryHeader) throws Exception {
		String[] breadcrumbs = { "Home", "Gift Cards", subcategoryHeader, link_subcategoryHeader.replaceAll("\'", "") };

		GiftCards_LandingPage.click_FeaturedCategories_Subcategory_LinkText(link_subcategoryHeader, subcategoryHeader);

		if(link_subcategoryHeader.contains("View All...")){
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_ViewAllResultsPage"));
			BaseUI.verifyElementHasExpectedText("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_ViewAllResultsPage",
					"Narrow Your Results By:");
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				//Validation for Citi
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle"));
				String headerTitle1 = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle"));
				BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", subcategoryHeader, headerTitle1);
			} else {
				Navigation.verify_BreadcrumbList(breadcrumbs);
			}

			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_LinkTextHeader"));
			String linkHeaderTitle = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("giftCards_FeaturedCategories_SubCategory_LinksList_ByCategoryName_LinkText_LinkTextHeader"))
					.replace(" \u203A ", "").replace("\u203A", "").replace(" [X]", "").replaceAll("\'", "")
					.trim();

			if(linkHeaderTitle.endsWith("...")){
				BaseUI.baseStringPartialCompare("giftCards_SearchResult_HeaderTitle", link_subcategoryHeader.replace("\'", ""), linkHeaderTitle);
			} else {
				BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle",
						link_subcategoryHeader.replace("\'", "" ).replace("\u0027", "").replaceAll("CVS", "CVS/pharmacy"),
						linkHeaderTitle.replace("\u0027", ""));
			}
		}
	}

	public static ArrayList<String> retrieve_TotalNumberOfPages() {

		ArrayList<WebElement> giftCards_TotalNumbeOfPages = Locator
				.lookup_multipleElements("giftCards_SearchResult_PageNumber_Top_Link_List", null, null);

		ArrayList<String> pageCountList = new ArrayList<String>();

		for (WebElement Option : giftCards_TotalNumbeOfPages) {
			pageCountList.add(BaseUI.getTextFromField(Option));
		}
		return pageCountList;
	}
}
