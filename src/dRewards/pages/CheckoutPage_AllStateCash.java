package dRewards.pages;

import utils.BaseUI;
import utils.Locator;

public class CheckoutPage_AllStateCash {

	public static void select_ApplyMaximum() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_AllCash_ApplyAllCash_Radio"));
		Thread.sleep(300);
	}

	public static void select_DoNotApplyAllstateCash() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_AllCash_DoNotApplyAllCash_Radio"));
		Thread.sleep(300);
	}

	public static void apply_Other_Amount(Double allstateCash_Amount) throws Exception {
		select_otherAmount();
		BaseUI.enterText(Locator.lookupRequiredElement("checkout_AllCash_OtherAmount_Textbox"), allstateCash_Amount.toString());
	}

	public static void select_otherAmount() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_AllCash_ApplyAllCash_OtherAmount_Radio"));
		Thread.sleep(300);
	}

	public static String return_ApplyMaximumAmount() throws Exception {
		String applyMaxAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_AllCash_ApplyAll_AmountText"));

		applyMaxAmount = applyMaxAmount.substring(0, applyMaxAmount.indexOf(" ")).trim();

		return applyMaxAmount;
	}

	public static void click_Continue_ToNextPage() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("checkout_AllCash_ContinueToPayment_Button"));
		BaseUI.waitForElementToBeDisplayed("checkout_ReviewOrder_button",null,null);
		Thread.sleep(1000);
	}

	public static void verify_OnCashRewards_Page() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkout_AllstateCash_Header"));
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("checkout_AllstateCash_Header"),
				"class", "active");
	}

}// End of Class
