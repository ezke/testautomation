package dRewards.pages;

import java.util.ArrayList;

import dRewards.pageControls.DatePicker_Citi;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.DatePicker;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class HotelsAndCondos {

	public static void verify_HotelSearch_ElementsPresent() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearchInput"));

		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		String tomorrowsDate = BaseUI.getDateAsString_InRelationToTodaysDate(1);
		BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("hotelSearch_checkinDate"), "value",
				todaysDate);
		BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupRequiredElement("hotelSearch_checkoutDate"), "value",
				tomorrowsDate);
		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("hotelSearch_NumberOfAdults"), "2");
		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("hotelSearch_NumberOfChildren"), "0");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearchSubmit"));

		// Top Travel Deals elements present
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearch_TTD_Header"));
		ArrayList<WebElement> listOfTopDeals = Locator
				.lookup_multipleElements("hotelSearch_TTD_ListOfTravelDestinationLinks", null, null);
		BaseUI.verify_true_AndLog(listOfTopDeals.size() > 0, "Found Top Travel Deals.",
				"Did NOT find Top Travel Deals.");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("hotelSearch_TTD_SeeAllDealsButton"));

	}



	public static void setCheckin_AndCheckoutDates(String checkin, String checkout) throws Exception {

		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("hotelSearch_checkinDate"));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelSearch_checkinDate"));
		}
		Thread.sleep(200);

		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(checkin);

		if (Browser.currentBrowser.equals("internetexplorer") && ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			BaseUI.click_js(Locator.lookupRequiredElement("hotelSearch_checkoutDate"));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("hotelSearch_checkoutDate"));
		}
		Thread.sleep(200);
		datePicker.selectTheDate(checkout);

		Thread.sleep(200);
	}

	public static void select_Adults_AndChildren(String number_adults, String number_children, String childrenAge)
			throws Exception {
		Select adult_dropdown = new Select(Locator.lookupRequiredElement("hotelSearch_NumberOfAdults"));
		adult_dropdown.selectByVisibleText(number_adults);
		Thread.sleep(50);

		Select children_dropdown = new Select(Locator.lookupRequiredElement("hotelSearch_NumberOfChildren"));
		children_dropdown.selectByVisibleText(number_children);
		Thread.sleep(50);

		Integer numberOfChildren = Integer.parseInt(number_children);
		for (Integer i = 1; i <= numberOfChildren; i++) {
			Select childAge_dropdown = new Select(
					Locator.lookupRequiredElement("hotelSearch_ChildAge_Dropdown_ByChildNumber", i.toString(), null));
			childAge_dropdown.selectByVisibleText(childrenAge);
			Thread.sleep(50);

		}
	}


	
	public static ArrayList<String> getPageCountForHotelSearch() {
		ArrayList<String> pageCountList = new ArrayList<String>();
		ArrayList<WebElement> pageCounts = Locator.lookup_multipleElements("hotelSearchResult_PageNumber_Top_LinkList", null, null);

		for (WebElement pageCount : pageCounts) {
			pageCountList.add(pageCount.getAttribute("innerText"));
		}
				
		return pageCountList;
	}


    // This method assumes that the predictive search appears and then it clicks
    // the first entry in that list.
    public static String hotel_Search_WithCriteria(String address, String fromDate, String toDate, String adultCount,
                                                   String childCount, String ageOfChildren) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelSearchInput"), address);
        BaseUI.waitForElementToBeDisplayed("hotelSearch_Input_ListThatAppears_FirstEntry", null, null);
        Thread.sleep(200);
        BaseUI.click(Locator.lookupRequiredElement("hotelSearch_Input_ListThatAppears_FirstEntry"));
        BaseUI.waitForElementToNOTBeDisplayed("hotelSearch_Input_ListThatAppears_FirstEntry", null, null);
        Thread.sleep(200);

        String entrySelected = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("hotelSearchInput"));

        setCheckin_AndCheckoutDates(fromDate, toDate);
        select_Adults_AndChildren(adultCount, childCount, ageOfChildren);

        click_hotelSearchSubmit();

        return entrySelected;
    }

    public static String hotel_Search_WithCriteria_SelectPredictiveListItem(String address, String listItem,
                                                                            String fromDate, String toDate, String adultCount, String childCount, String ageOfChildren)
            throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelSearchInput"), address);
        BaseUI.waitForElementToBeDisplayed("hotelSearch_Input_ListThatAppears_FirstEntry", null, null);
        Thread.sleep(200);
        String listItemFormatted_ForList = listItem.substring(address.length(), listItem.length());
        BaseUI.click(Locator.lookupRequiredElement("hotelSearch_Input_ListThatAppears_byStrongText_AndText", address,
                listItemFormatted_ForList));
        Thread.sleep(200);

        String entrySelected = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("hotelSearchInput"));

        setCheckin_AndCheckoutDates(fromDate, toDate);
        select_Adults_AndChildren(adultCount, childCount, ageOfChildren);

        click_hotelSearchSubmit();

        return entrySelected;
    }

    // This method assumes that the predictive search appears and then it clicks
    // the first entry in that list.
    public static void hotel_Search_WithCriteria_NoPredictiveResult(String address, String fromDate, String toDate,
                                                                    String adultCount, String childCount, String ageOfChildren) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelSearchInput"), address);
        Thread.sleep(200);

        setCheckin_AndCheckoutDates(fromDate, toDate);
        select_Adults_AndChildren(adultCount, childCount, ageOfChildren);

        click_hotelSearchSubmit();
    }



    public static void click_hotelSearchSubmit() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("hotelSearchSubmit"));
        HotelsAndCondos_SearchResults.waitForPageToLoad();
    }


    public static void hotelSearchInput_withCity(String city1) throws Exception {

        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("hotelSearchInput"), city1);
        Thread.sleep(1000);
        BaseUI.click(Locator.lookupRequiredElement("cityName"));
        BaseUI.click(Locator.lookupRequiredElement("hotelSearchSubmit"));
        Thread.sleep(1000);
    }

    public static void check_ForErrorPage_AndFailI() throws Exception {

        if (BaseUI.pageSourceContainsString("Could not reserve the room due to a hotel system error. ")) {

            BaseUI.log_AndFail("Booking Fail - Could not reserve the room due to a hotel system error.");
        }
        if(BaseUI.elementAppears(Locator.lookupOptionalElement("hotelPropertyDetails_ErrorMessage"))){
        	BaseUI.log_AndFail("Seeing error message: " + BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("hotelPropertyDetails_ErrorMessage"),  "innerText"));
		}

    }


}
