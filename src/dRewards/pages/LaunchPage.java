package dRewards.pages;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class LaunchPage {

	public static void click_sign_On() throws InterruptedException {
		// if (BaseUI.elementExists("lnchPge_SignOnButton", null, null)) {
		if (Browser.driver.getPageSource().contains("SSO-button")) {
			BaseUI.click(Locator.lookupRequiredElement("lnchPge_SignOnButton"));
			BaseUI.waitForElementToNOTBeDisplayed("lnchPge_SignOnButton",null,null,10);
			Thread.sleep(500);
		}
	}

}
