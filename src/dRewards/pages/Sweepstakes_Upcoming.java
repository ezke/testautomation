package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class Sweepstakes_Upcoming {


    public static void waitForPageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("upcomingSweepstakes_TabActive", null, null);
        if(ClientDataRetrieval.isRedesignClient()) {
            BaseUI.waitForElementToBeDisplayed("upcomingSweepstakes_ElementToWaitFor", null, null, 40);
        }
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void verify_UpcomingSweepstakes_NoneAvailable() {
        if (ClientDataRetrieval.isRedesignClient()) {
            String expectedText = "Sorry, there are no Upcoming Sweepstakes at this time. Check back soon.";
            WebElement noSweepstakesTextElement = Locator.lookupRequiredElement("upcomingSweepstakes_NoSweepstakes_Text");

            BaseUI.verifyElementAppears(noSweepstakesTextElement);
            BaseUI.verifyElementHasExpectedText(noSweepstakesTextElement, expectedText);
        } else {
            String expectedText = "We're Sorry\n" +
                    "There are no Sweepstakes available right now. Remember to check back frequently and look for email updates for more information!\n" +
                    "Since you're already here, why don't you stay awhile? Take advantage of tons of deals and use your points towards daily deals, brand-name merchandise, gift cards, travel, local offers, and much more!\n" +
                    "\n" +
                    "See Deals";
            WebElement noSweepstakesTextElement = null;
            try {
                noSweepstakesTextElement = Locator.lookupRequiredElement("upcomingSweepstakes_NoSweepstakes_Text");
            }catch(org.openqa.selenium.NoSuchElementException e){
                BaseUI.log_AndFail("Unable to find No Sweepstakes Text Element.");
            }

            String innerTextFromNoSweeps = BaseUI.getTextFromField(noSweepstakesTextElement).trim();
            BaseUI.verifyElementAppears(noSweepstakesTextElement);
            BaseUI.baseStringCompare("No Sweepstakes Message", expectedText, innerTextFromNoSweeps);

            WebElement seeDealsButton = Locator.lookupRequiredElement("upcomingSweepstakes_NoSweepstakes_Banner_SeeDeals_Button");
            BaseUI.verifyElementAppears(seeDealsButton);
            BaseUI.verifyElementEnabled(seeDealsButton);
        }
    }
}//End of Class
