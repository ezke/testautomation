package dRewards.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.Locator;

public class Merchandise {

    public static void navigate_Category_ByLinkText(String linkText) throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("merch_ShopByCategoryLink_ByText", linkText, null));
            BaseUI.waitForElementToContain_AttributeMatch("merch_merchandise_Category_LandingPage_Header", null, null,
                    "innerText", linkText, 40);
            Thread.sleep(2000);
        } else {
            BaseUI.click(Locator.lookupRequiredElement("merch_CategoryLink_ByText", linkText, null));
            Thread.sleep(1000);
            BaseUI.wait_forPageToFinishLoading();
        }
    }

    // Pass in the page number as a String and it will click on it.
    // Will need a different method if page number is not visible on page.
    public static void navigate_ToPage_ByPageNumber(String pageNumber) throws Exception {
        if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            BaseUI.click_js(Locator.lookupRequiredElement("merch_Page_ByText_TopLink", pageNumber, null));
            Thread.sleep(2000);
        } else {
            if (ClientDataRetrieval.isRedesignClient()) {
                if (!(BaseUI.elementAppears(Locator.lookupOptionalElement("merch_Page_ByText_TopLink", pageNumber, null)))) {
                    BaseUI.click(Locator.lookupRequiredElement("merch_Page_ByText_TopLink_RightPagination"));
                    Thread.sleep(2000);
                }
            }
            BaseUI.click(Locator.lookupRequiredElement("merch_Page_ByText_TopLink", pageNumber, null));
            String classToMatch = "goToPage";
            if (ClientDataRetrieval.isRedesignClient()) {
                classToMatch = "active";
            }

            BaseUI.waitForElementToContain_AttributeMatch("merch_Page_ByText_TopLink_CheckIfActive", pageNumber, null, "class", classToMatch, 20);
            BaseUI.waitForElementToBeDisplayed("merch_resultList_Images", null, null, 10);
            Thread.sleep(1500);
        }
    }

    public static void navigate_ToNextPage() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("merch_Page_NextButton_TopLink"));
        Thread.sleep(500);
    }

    public static void verify_ClickRight_List_Of_VisibleCarouselItemsChanged() throws Exception {

        ArrayList<String> originalElementAltValues = return_VisibleCarouselItemsAltValues();

        click_Merchandise_BrandCarousel_RightNavigationArrow();

        ArrayList<String> newElementAltValues = return_VisibleCarouselItemsAltValues();

        BaseUI.verify_true_AndLog(originalElementAltValues.size() > 2, "Found Elements before click.", "Could not find carousel elements");
        BaseUI.verify_true_AndLog(newElementAltValues.size() > 2, "Found Elements after click.", "Could not find carousel elements");

        BaseUI.verify_true_AndLog(!originalElementAltValues.equals(newElementAltValues), "Lists of visible Alt values changed.",
                "Lists of visible Alt values did NOT change.");

    }

    public static void verify_CarouselImagesFound_AndSomeVisible(){
        ArrayList<String> originalElementAltValues = return_VisibleCarouselItemsAltValues();

        BaseUI.verify_true_AndLog(originalElementAltValues.size() > 2, "Found Displayed Carousel Items.", "Did NOT find Displayed Carousel Items.");
    }

    private static ArrayList<String> return_VisibleCarouselItemsAltValues(){
        ArrayList<WebElement> listOfElements = Locator.lookup_multipleElements("merch_BrandCarousel_ListOfImages", null, null);
        ArrayList<String> originalElementAltValues = new ArrayList<String>();
        for (WebElement carouselElement : listOfElements) {
            if (carouselElement.isDisplayed()) {
                String alt = carouselElement.getAttribute("alt");
                BaseUI.verify_String_Not_EmptyOrNull(alt);
                originalElementAltValues.add(alt);
            }
        }

        return originalElementAltValues;
    }


    public static void verify_ClickLeft_List_Of_VisibleCarouselItemsChanged() throws Exception {
        ArrayList<String> originalElementAltValues = return_VisibleCarouselItemsAltValues();

        click_Merchandise_BrandCarousel_LeftNavigationArrow();


        ArrayList<String> newElementAltValues = return_VisibleCarouselItemsAltValues();

        BaseUI.verify_true_AndLog(originalElementAltValues.size() > 2, "Found Elements before click.", "Could not find carousel elements");
        BaseUI.verify_true_AndLog(newElementAltValues.size() > 2, "Found Elements after click.", "Could not find carousel elements");

        BaseUI.verify_true_AndLog(!originalElementAltValues.equals(newElementAltValues), "Lists of visible Alt values changed.",
                "Lists of visible Alt values did NOT change.");

    }

    public static Integer get_ItemCount_OnPage() {
        String resultCountFooter = BaseUI.getTextFromField(Locator.lookupRequiredElement("merch_ResultsCount_Footer"));
        if (ClientDataRetrieval.isRedesignClient()) {
            resultCountFooter = resultCountFooter.substring(0, resultCountFooter.indexOf(" ")).trim();
        } else {
            resultCountFooter = resultCountFooter.substring(resultCountFooter.indexOf(" "),
                    resultCountFooter.indexOf(" ", resultCountFooter.indexOf(" ") + 1)).trim();
        }
        Integer starting_number = Integer.parseInt(resultCountFooter.split("-")[0]);
        Integer ending_number = Integer.parseInt(resultCountFooter.split("-")[1]);

        Integer count = ending_number - starting_number;
        return count;
    }

    // If there is not main category, just send "" for category. Category is
    // useful for specific cases where some categories have different header
    // design.
    public static void verify_subCategoryActive(String category, String subCategoryName) {

       Navigation.verify_OopsPage_DoesNOT_Appear();

        if (category.equals("Local Offers")) {
            WebElement activeCategory = Locator.lookupRequiredElement("merch_category_Active_ByText", subCategoryName, null);
            BaseUI.verifyElementAppears(activeCategory);
            BaseUI.verifyElementHasExpectedPartialAttributeValue(activeCategory, "class", "activemenu");
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
                && subCategoryName.equals("Daily Deals")) {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            "Daily Deal", null));

            BaseUI.baseStringCompare("Category Header", "Daily Deal", headerText);
        } else if (ClientDataRetrieval.isRedesignClient() && subCategoryName.contains("Daily Deals")) {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            "Daily Deals", null));

            BaseUI.baseStringPartialCompare("Category Header", "Daily Deal",
                    headerText.replaceAll("New item daily at 3PM ET", ""));
        } else if (category.equals("Magazines")) {
            subCategoryName = subCategoryName.replace("&", "and");
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", subCategoryName, null));

            BaseUI.baseStringCompare("Category Header", subCategoryName, headerText);
        } else if (subCategoryName.toLowerCase().equals("loccitane") || subCategoryName.toUpperCase().equals("LOCCITANE")
                || subCategoryName.contains("L'Occitane")) {

            if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
                    || ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
                    || ClientDataRetrieval.isRedesignClient()) {

                String headerText = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", "L'OCCITANE", null));
                BaseUI.baseStringCompare("Category Header", "L'OCCITANE", headerText.toUpperCase());
            } else {
                String headerText = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", "LOccitane", null));
                BaseUI.baseStringCompare("Category Header", subCategoryName.toLowerCase(), headerText.toLowerCase());
            }
        } else if (subCategoryName.equals("Sports & Outdoors") || subCategoryName.equals("Sports and Outdoors")) {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            subCategoryName.replaceAll("&", "and"), null));

            BaseUI.baseStringCompare("Category Header", "Sports and Outdoors", headerText);
        } else if (subCategoryName.equals("Entertainment")) {
            String headerText;
            if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore) &&
                    ClientDataRetrieval.environment.equals("QA") || ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) &&
                    ClientDataRetrieval.environment.equals("QA")){
                headerText = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", "Entertainment", null));
                BaseUI.baseStringCompare("Category Header", "Entertainment", headerText);
            } else {
                headerText = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", "Movie Tickets", null));
                BaseUI.baseStringCompare("Category Header", "Movie Tickets", headerText);
            }
        } else if (subCategoryName.contains("\'") && !subCategoryName.contains("L'Occitane") && !subCategoryName.contains("Men's Shop")
                && !subCategoryName.contains("Women's Shop") && !subCategoryName.contains("What's Trending") &&
                !subCategoryName.contains("Valentine's Day")) {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            subCategoryName.replace("'", ""), null)).replace("'", "");//

            BaseUI.baseStringCompare("Category Header", subCategoryName.replace("'", ""), headerText);
        } else if (subCategoryName.contains("Mothers Day")) {
            //Adding this because DR Admin site does not allow to add apostrophe in certain places
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            "Mother's Day", null)).replace("'", "");

            BaseUI.baseStringCompare("Category Header", subCategoryName, headerText);
        } else if (subCategoryName.contains("Fathers Day")) {
            //Adding this because DR Admin site does not allow to add apostrophe in certain places
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            "Father's Day", null)).replace("'", "");

            BaseUI.baseStringCompare("Category Header", subCategoryName, headerText);
        } else if (subCategoryName.contains("Back-to-School")) {
            String backToSchool = "Back to School";
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory",
                            backToSchool, null)).replace("'", "");

            BaseUI.baseStringCompare("Category Header", backToSchool, headerText);
        } else if (subCategoryName.equals("eGift Cards") &&
                ClientDataRetrieval.client_Matches(client_Designation.RedesignCore) &&
                ClientDataRetrieval.environment.equals("QA")) {
            String headerText;
            headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", "e-Gift Cards", null));
            BaseUI.baseStringCompare("Category Header", "e-Gift Cards", headerText);
        } else if (subCategoryName.equals("Holiday Travel")) {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", subCategoryName, null))
                    .replaceAll("Essentials", "");

            BaseUI.baseStringPartialCompare("Category Header", subCategoryName, headerText);
        } else if (subCategoryName.equals("Auto Accessories")) {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", subCategoryName, null));

            BaseUI.baseStringPartialCompare("Category Header", "Tech", headerText);
        } else {
            String headerText = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("merch_merchandiseActiveCategory", subCategoryName, null))
                    .replaceAll("new item daily at 3pm et", "");

            BaseUI.baseStringCompare("Category Header", subCategoryName.toLowerCase(),
                    headerText.toLowerCase());
        }
    }

    public static void verify_NoSubCategories_Active() {
        ArrayList<WebElement> availableCategories = Locator.lookup_multipleElements("merch_Categories_Available", null,
                null);

        BaseUI.verify_true_AndLog(availableCategories.size() > 0, "Found Available Sub Categories.",
                "Could NOT find available sub categories.");

        for (WebElement category : availableCategories) {
            BaseUI.verifyElementDoesNotHavePartialAttributeValue(category, "class", "activemenu");
        }
    }

    // Category should be Category or Featured Brands
    public static void verify_Merchandise_Category_Link(String category, String linkText, String expectedHeaderText)
            throws Exception {
        WebElement linkElement;
        WebElement active_categoryElement = Locator.lookupRequiredElement("merch_ActiveCategoryHeader");

        // If our link is under Category we want Category to be expanded.
        if (ClientDataRetrieval.isRedesignClient()) {
            if (category.equals("Shop By Category")) {
                linkElement = Locator.lookupRequiredElement("merch_ShopByCategoryLink_ByText", linkText, null);
            } else if (category.equals("Featured Shops")) {
                linkElement = Locator.lookupRequiredElement("merch_FeaturedShops_ByText", linkText, null);
            } else {
                linkElement = Locator.lookupRequiredElement("merch_FeaturedBrands_ByText", linkText, null);
            }
        } else {
            if (category.equals("Category")) {
                // This a little bit of reflection to determine which grouping is
                // expanded. If Featured Brands is expanded,
                // Category will then be extended.
                if (BaseUI.getTextFromField(active_categoryElement).equals("Featured Brands")) {//active_categoryElement.findElement(By.xpath("./a")).getText()
                    Merchandise.Click_Category_Or_FeaturedBrands_Tab("Category");
                }
                linkElement = Locator.lookupRequiredElement("merch_CategoryLink_ByText", linkText, null);
            }
            // If our link is NOT under Category we want Featured Brands to be
            // expanded.
            else {
                if (BaseUI.getTextFromField(active_categoryElement).equals("Category")) {
                    Merchandise.Click_Category_Or_FeaturedBrands_Tab("Featured Brands");
                }
                linkElement = Locator.lookupRequiredElement("merch_FeaturedBrands_ByText", linkText, null);
            }
        }

        BaseUI.scroll_to_element(linkElement);
        BaseUI.click_js(linkElement);
        BaseUI.wait_forPageToFinishLoading();
        Thread.sleep(1500);
        Merchandise.verify_subCategoryActive(category, expectedHeaderText);
    }

    public static void Click_Category_Or_FeaturedBrands_Tab(String menuTab) throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("merch_categoryHeader_ByText", menuTab, null));
        Thread.sleep(750);
    }

    public static void minimize_Category() throws Exception {
        WebElement linkToCheck = Locator.lookupRequiredElement("merch_category_Active_ByText", "Category", null);
        if (BaseUI.elementHasExpectedAttribute(linkToCheck, "class", "active activemenu")) {
            BaseUI.click(linkToCheck);
            Thread.sleep(500);
        }
    }

    public static void maximize_Category() throws Exception {
        WebElement linkToCheck = Locator.lookupRequiredElement("merch_category_Active_ByText", "Category", null);
        if (!BaseUI.elementHasExpectedAttribute(linkToCheck, "class", "active activemenu")) {
            BaseUI.click(linkToCheck);
            Thread.sleep(500);
        }
    }

    public static void minimize_FeaturedBrands() throws Exception {
        WebElement linkToCheck = Locator.lookupRequiredElement("merch_category_Active_ByText", "Featured Brands", null);
        if (BaseUI.elementHasExpectedAttribute(linkToCheck, "class", "active activemenu")) {
            BaseUI.click(linkToCheck);
            Thread.sleep(500);
        }
    }

    public static void maximize_FeaturedBrands() throws Exception {
        WebElement linkToCheck = Locator.lookupRequiredElement("merch_category_Active_ByText", "Featured Brands", null);
        if (!BaseUI.elementHasExpectedAttribute(linkToCheck, "class", "active activemenu")) {
            BaseUI.click(linkToCheck);
            Thread.sleep(500);
        }
    }

    // gets product text and strips out the (More... ending.
    public static String get_ProductText_ByIndexAsString(String productNumber) {
        WebElement product = Locator.lookupRequiredElement("merch_Text_ByNumber", productNumber, null);
        String productText = BaseUI.getTextFromField(product);
        productText = productText.split("\\(")[0].trim();

        return productText;
    }

    // Pass in a number as String and that will be used to add the item to the
    // cart.
    // Returns a String containing the name of the product added.
    public static String add_To_Cart_ByNumber(String itemNumber) throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("merch_ResultPerPage_DropDown"), "48");
            Thread.sleep(2000);
        }

        WebElement addToCart_OR_seeDetails = Locator.lookupRequiredElement("merch_AddToCart_OR_SeeDetails_ByNumber", itemNumber,
                null);

        String productName = "";

        if (ClientDataRetrieval.isRedesignClient()) {
            //Click Random Product Image from resulting list
            BaseUI.click(Locator.lookupRequiredElement("merch_AddToCart_OR_SeeDetails_ByNumber", itemNumber, null));
            BaseUI.waitForElementToBeDisplayed("merchProduct_Header", null, null, 40);
            Thread.sleep(500);

            productName = BaseUI.getTextFromField(Locator.lookupRequiredElement("merch_ProductName"));
            // get a list of all of the dropdowns
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("merchProduct_AllDropdowns"))) {
                ArrayList<WebElement> dropdown_List = new ArrayList<WebElement>();
                dropdown_List.addAll(Locator.lookup_multipleElements("merchProduct_AllDropdowns", null, null));
                for (Integer i = 1; i <= dropdown_List.size(); i++) {
                    // For each dropdown that was found we'll select the first
                    // option in that dropdown.
                    Merchandise_ProductPage.select_firstOption(
                            Locator.lookupRequiredElement("merchProduct_Dropdown_SelectByIndex", i.toString(), null));
                }
            }
            //BaseUI.waitForElementToNOTBeDisplayed("merch_ProdModal_ProceedToCheckoutBtn", null, "60");
            //Click Add to Shopping Cart Button
            BaseUI.click(Locator.lookupRequiredElement("merch_AddToCart_ByNumberGc", null, null));
            Thread.sleep(2000);
            BaseUI.click(Locator.lookupRequiredElement("merchProduct_ViewCartBtn"));
            //Thread.sleep(2000);
            ShoppingCart.wait_ForPage_ToLoad();
        }
        // if the button is "Add to Cart" all that we need to do is click it.
        else if (addToCart_OR_seeDetails.getAttribute("class").equals("btn add-to-cart")) {
            productName = get_ProductText_ByIndexAsString(itemNumber);
            if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
                BaseUI.click_js(Locator.lookupRequiredElement("merch_AddToCart_OR_SeeDetails_ByNumber", itemNumber, null));
                Thread.sleep(2000);
            } else {
                BaseUI.click(Locator.lookupRequiredElement("merch_AddToCart_OR_SeeDetails_ByNumber", itemNumber, null));
                Thread.sleep(2000);
            }
        }
        // if the button is the See Details button, we'll need to go to the
        // Merchandise Product page and select values for any dropdowns.
        // And finally click Add to Shopping Cart.
        else {
            BaseUI.click(Locator.lookupRequiredElement("merch_AddToCart_OR_SeeDetails_ByNumber", itemNumber, null));
            Thread.sleep(1500);
            // get a list of all of the dropdowns
            ArrayList<WebElement> dropdown_List = new ArrayList<WebElement>();
            dropdown_List.addAll(Locator.lookup_multipleElements("merchProduct_AllDropdowns", null, null));
            for (Integer i = 1; i <= dropdown_List.size(); i++) {
                // For each dropdown that was found we'll select the first
                // option in that dropdown.
                Merchandise_ProductPage.select_firstOption(
                        Locator.lookupRequiredElement("merchProduct_Dropdown_SelectByIndex", i.toString(), null));
                Thread.sleep(1000);
            }

            productName = Locator.lookupRequiredElement("merchProduct_ProductTitle").getText();
            if (productName.contains("(")) {
                productName = productName.substring(0, productName.indexOf("(")).trim();
            }

            if(!BaseUI.elementAppears(Locator.lookupOptionalElement("merchProduct_SoldOutButton"))) {
                Merchandise_ProductPage.add_toShoppingCart();
            } else {
                Browser.driver.navigate().back();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }
        }
        return productName;
    }

    public static void add_To_Cart_GiftCard(String itemNumber) throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.click(Locator.lookupRequiredElement("merch_resultList_Images", itemNumber, null));
            Thread.sleep(2000);
        } else {
            if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
                BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("merch_ResultPerPage_DropDown"), "48");
                Thread.sleep(2000);
            }
            WebElement giftCard_addToCart = Locator.lookupRequiredElement("merch_AddToCart_ByNumberGc", null,
                    null);
            giftCard_addToCart.getAttribute("class").equals("btn add-to-cart");
        }
        BaseUI.wait_forPageToFinishLoading();
        //productName = get_ProductText_ByIndexAsString(itemNumber);
        BaseUI.click(Locator.lookupRequiredElement("merch_AddToCart_ByNumberGc", null, null));
        Thread.sleep(2000);

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.waitForElementToNOTBeDisplayed("merch_ProdModal_ProceedToCheckoutBtn", null, null);
            BaseUI.waitForElementToBeDisplayed_ButDontFail("merchProduct_ViewCartBtn",null,null, 5);
            if(BaseUI.elementAppears(Locator.lookupOptionalElement("merchProduct_ViewCartBtn"))){
                BaseUI.click(Locator.lookupRequiredElement("merchProduct_ViewCartBtn"));
            }else {
                BaseUI.click(Locator.lookupRequiredElement("nav_ShoppingCartLink"));
            }
            Thread.sleep(1000);
            BaseUI.wait_forPageToFinishLoading();
        }
    }

    // return a list of the links that were saved in the speified txt file.
    public static HashMap<String, String> return_LinkList() throws Exception {
        String listPath = "\\src\\dRewards\\data\\NavLinks\\"
                + ClientDataRetrieval.clientName.substring(0, ClientDataRetrieval.clientName.indexOf("-"))
                + "_CategoriesAndFavorites.txt";

        HashMap<String, String> linkList = DataBuilder.GetConfig_DataFromFile(listPath);

        return linkList;
    }

    public static ArrayList<String> return_CategoryList() {
        ArrayList<String> categoryList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator.lookup_multipleElements("merch_CategoryList", null, null);

        for (WebElement categoryElement : categoryElements) {
            categoryList.add(categoryElement.getAttribute("innerText"));
        }

        return categoryList;
    }

    public static ArrayList<String> return_FeaturedBrandList() {
        ArrayList<String> categoryList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator.lookup_multipleElements("merch_FeaturedBrandList", null, null);

        for (WebElement categoryElement : categoryElements) {

            categoryList.add(categoryElement.getAttribute("innerText"));
        }

        return categoryList;
    }

    public static ArrayList<String> return_FeaturedShopsList() {
        ArrayList<String> categoryList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator.lookup_multipleElements("merch_FeaturedShopsList", null, null);

        for (WebElement categoryElement : categoryElements) {

            categoryList.add(categoryElement.getAttribute("innerText"));
        }

        return categoryList;
    }

    // click on any subcategory under category menu
    public static void Click_Category_Subcategory_Link(String subCategory) throws Exception {

        List<WebElement> allCategoryMenuTabsDisplayed = Locator.lookup_multipleElements("merch_CategoryList",
                subCategory, null);
        int numberOfsubCategories = allCategoryMenuTabsDisplayed.size();
        System.out.println(numberOfsubCategories);

        for (WebElement category : allCategoryMenuTabsDisplayed) {
            System.out.println(category.getText());
            if (category.getText().equals(subCategory)) {
                BaseUI.click(category);
                Thread.sleep(2000);
                break;
            }
        }
    }

    // Click any subcategory and verify subcategory header on subcategory
    // landing page
    public static void clickAndVerify_Category_SubcategoryLink(String subCategory) throws Exception {

        Navigation.navigate_Click_MerchandiseMenuTab_MerchandisePage();
        Merchandise.navigate_Category_ByLinkText(subCategory);

        if (ClientDataRetrieval.isRedesignClient()) {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("merch_Page_ViewAllOption"))) {
                String merchCategoryHeaderTitle = BaseUI.getTextFromField(
                        Locator.lookupRequiredElement("merch_merchandise_Category_LandingPage_Header"));
                if (!(merchCategoryHeaderTitle.contains("Entertainment") || merchCategoryHeaderTitle.contains("Dining")
                        || merchCategoryHeaderTitle.contains("Shopping") || merchCategoryHeaderTitle.contains("Travel")
                        || merchCategoryHeaderTitle.contains("e-Gift Cards") || merchCategoryHeaderTitle.contains("Gift Cards"))) {
                    BaseUI.click(Locator.lookupRequiredElement("merch_Page_ViewAllOption"));
                    BaseUI.waitForElementToBeDisplayed("merch_ResultPerPage_DropDown", null, null);
                    Thread.sleep(2000);
                }
            }
        }
        //commenting this below lines of validation for Citi
        //because for Citi Merchandise every subcategory landing page does not have
        //subcategory image banner
//		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
//			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchandisePage_CategoryImg"));
//		} else {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_merchandise_Category_LandingPage_Header"));
        BaseUI.verifyElementHasExpectedText("merch_merchandise_Category_LandingPage_Header", subCategory);
        //}
    }

    // Add items to shopping cart from any category under Merchandise section
    public static void addItems_To_ShoppingCart(String category, int startPage, int endPage) throws Exception {

        // adding item to cart
        Merchandise.clickAndVerify_Category_SubcategoryLink(category);
        Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        //Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(startPage, endPage));
        //Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));
    }

    // click Merchandise Brand Carousel Right navigation arrow
    public static void click_Merchandise_BrandCarousel_RightNavigationArrow() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("merch_BrandCarousel_RightNavigation_Arrow"));
        Thread.sleep(500);
    }

    // click Merchandise Brand Carousel Left navigation arrow
    public static void click_Merchandise_BrandCarousel_LeftNavigationArrow() throws Exception {
        BaseUI.click_js(Locator.lookupRequiredElement("merch_BrandCarousel_LeftNavigation_Arrow"));
        Thread.sleep(500);
    }

    // click Merchandise Brand Carousel Brand Image
    public static void click_Merchandise_BrandCarousel_Image_ByBrandName(String brandNameElement,
                                                                         String brandName) throws Exception {
        BaseUI.click_js(Locator.lookupRequiredElement(brandNameElement, brandName, null));
        Thread.sleep(2500);
        BaseUI.wait_forPageToFinishLoading();
    }

    // click Merchandise Brand Carousel right navigation arrow all the way to
    // right
    public static void click_Merchandise_BrandCarousel_RightArrow_Navigates_ToFarRight() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("merch_BrandCarousel_LastItem_Displayed"))) {
            click_Merchandise_BrandCarousel_RightNavigationArrow();
            count++;
            if (count > 25) {
                break;
            }
        }
    }

    // click Merchandise Brand Carousel left navigation arrow all the way to
    // left
    public static void click_Merchandise_BrandCarousel_LeftArrow_Navigates_ToFarLeft() throws Exception {
        int count = 0;
        while (!BaseUI.elementAppears(Locator.lookupOptionalElement("merch_BrandCarousel_FirstItem_Displayed"))) {
            click_Merchandise_BrandCarousel_LeftNavigationArrow();
            count++;
            if (count > 25) {
                break;
            }
        }
    }

    // click the first image on Brand Carousel
    public static void click_FirstItemImage_BrandCarousel() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("merch_BrandCarousel_FirstItem_Displayed"));
        Thread.sleep(500);
    }

    public static void clickAndVerify_Merchandise_BrandCarousel_Displayed_NavigatesTo_LandingPage(String brandNameElement, String brandName)
            throws Exception {

        click_Merchandise_BrandCarousel_Image_ByBrandName(brandNameElement, brandName);
        BaseUI.verify_true_AndLog(
                (Locator.lookupRequiredElement("merch_BrandCarousel_Brand_LandingPage_Header_Title").isDisplayed()),
                "Brand Name is displayed", "Header does not display Brand name");

        String brandHeaderLandingPage = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("merch_BrandCarousel_Brand_LandingPage_Header_Title"));
        BaseUI.verifyElementHasExpectedPartialText("merch_BrandCarousel_Brand_LandingPage_Header_Title",
                brandHeaderLandingPage);
    }

    // Return all the brands displayed on See All Brands landing page.
    public static ArrayList<String> return_SeeAllBrands_BrandsDisplayed() {
        ArrayList<String> brandsList = new ArrayList<String>();
        // Take out brandName variable
        ArrayList<WebElement> brandElements;
        brandElements = Locator.lookup_multipleElements("merch_SeeAllBrands_BrandsList_Links",
                    null, null);

        for (WebElement brandElement : brandElements) {
            String brandNameText = brandElement.getAttribute("text");
            brandNameText = brandNameText.trim();
            brandsList.add(brandNameText);
        }

        return brandsList;
    }

    // click the any available menu tab on See All Brands page
    public static void click_SeeAllBrands_MenuTab(String chooseMenuTab) throws Exception {

        List<WebElement> allMenuTabDisplayed = Locator.lookup_multipleElements("merch_SeeAllBrands_MenuTab",
                chooseMenuTab, null);
        int numberOfBrands = allMenuTabDisplayed.size();
        System.out.println(numberOfBrands);

        for (WebElement brandTab : allMenuTabDisplayed) {
            System.out.println(brandTab.getText());
            if (brandTab.getText().equals(chooseMenuTab)) {
                BaseUI.click_js(brandTab);
                Thread.sleep(2000);
                break;
            }
        }
    }

    //CHoose a random product from a page
    //Using this method since many category for Redesign clients
    //do not have multiple pages of product list. It is just 1 or 2 pages
    public static void selectRandom_MerchandiseCategoryProduct(int startPage, int endPage) throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, 24));
        } else {
            Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(startPage, endPage));
            Merchandise.add_To_Cart_ByNumber(BaseUI.random_NumberAsString(1, Merchandise.get_ItemCount_OnPage()));
        }
    }

    //if the Merchandise > Subcategory landing page does not have multiple page with products
    //And it has just few items/products on only 1 page.
    //We can use this method to get item count on that page.
    public static String getItemCount_MerchandiseSubcategoryLandingPage() {
        String pageItemCount = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("merch_merchandise_Category_LandingPage_PageItemCount"));
        pageItemCount = pageItemCount.substring(pageItemCount.indexOf("of ") + 3, pageItemCount.length());

        return pageItemCount;
    }

    //This method is used to add items to StreetWise client, as it had just 3 category and
    //we are using Auto accessories for all Merchandise checkout flow.
    public static void addItemsToShoppingCart_Streetwise() throws Exception {
        Navigation.navigate_Merchandise_AutoAccessories();
        Merchandise.add_To_Cart_ByNumber(BaseUI
                .random_NumberAsString(1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())/2));

        Navigation.navigate_Merchandise_AutoAccessories();
        Merchandise.add_To_Cart_ByNumber(BaseUI
                .random_NumberAsString(Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())/2 + 1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())));
    }

    //This method finds the number of pages displayed for each merchandise category
    public static ArrayList<String> getPagesCount_MerchandiseSubcategoryLandingPage() {
        ArrayList<String> pageCountList = new ArrayList<String>();
        ArrayList<WebElement> pageCounts = Locator.lookup_multipleElements("merchProduct_NumberOfPagesList", null, null);

        for (WebElement pageCount : pageCounts) {
            pageCountList.add(pageCount.getAttribute("innerText"));
        }

        return pageCountList;
    }

    //This method will check if Mechandise > Category landing page has just 1 or more than 1
    // pages displayed. And accordingly will select the page number. This will save just time from
    // changing page number everytime, since some clients have 1 page and rest have more than 1 for same subcategory.
    public static void selectPageToNavigate_AndChooseRandomProduct() throws Exception {
        ArrayList<String> pageCount = getPagesCount_MerchandiseSubcategoryLandingPage();
        int productCount = get_ItemCount_OnPage();
        if (pageCount.size() == 2 && productCount >= 30) {
            Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(1, 2));
        } else if (pageCount.size() >= 3) {
            Merchandise.navigate_ToPage_ByPageNumber(BaseUI.random_NumberAsString(2, 3));
        } else {
            Merchandise.navigate_ToPage_ByPageNumber("1");
        }

        String productName = BaseUI.getTextFromField(Locator.lookupRequiredElement("merch_ProductName_LandingPage"));

        int countOnPage =  Merchandise.get_ItemCount_OnPage();
        String numberToPick = "1";
        if(countOnPage != 1){
            numberToPick = BaseUI.random_NumberAsString(1, countOnPage);
        }

//        if (productName.contains("Gift Card")) {
//            Merchandise.add_To_Cart_GiftCard(numberToPick);
//        } else {
            Merchandise.add_To_Cart_ByNumber(numberToPick);
       // }
    }

    public static void clickViewAllBrandsLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("merchLandingPage_ViewAllBrandsHeader"));
        BaseUI.waitForElementToBeDisplayed("merch_BrandCarousel_Brand_LandingPage_Header_Title", null, null, 30);
    }

    //Redesign clients only
    //Merchandise landing page - Get displayed category header list
    public static ArrayList<String> return_CategoryImages_Displayed() {

        ArrayList<String> categoryImagesList = new ArrayList<String>();
        ArrayList<WebElement> categoryElements = Locator
                .lookup_multipleElements("merchandisePage_CategoryImages", null, null);

        for (WebElement itemElement : categoryElements) {
            String categoryImagesText = BaseUI.get_Attribute_FromField(itemElement, "alt");
            categoryImagesList.add(categoryImagesText);
        }

        return categoryImagesList;
    }

    //Redesign clients only
    //Merchandise landing page - Get displayed category header list
    public static ArrayList<String> return_CategoryHeaders_Displayed() {

        ArrayList<String> categoryHeaderList = new ArrayList<>();
        ArrayList<WebElement> categoryElements = Locator
                .lookup_multipleElements("merchandisePage_CategoryHeaders", null, null);

        for (WebElement itemElement : categoryElements) {
            String categoryHeaderText = BaseUI.getTextFromField(itemElement);
            categoryHeaderList.add(categoryHeaderText);
        }

        return categoryHeaderList;
    }

    //Redesign clients only
    //Click on each category image on merchandise landing page and validate breadcrumb and category header
    public static void clickOnCategoryImage_ValidateBreadcrumbAndHeader(String categoryName) throws Exception {
        String[] breadcrumb = {"Home", "Merchandise", categoryName};
        BaseUI.click(Locator.lookupRequiredElement("merchandisePage_EachCategoryImage", categoryName, null));
        BaseUI.waitForElementToBeDisplayed("merchandise_CategoryPage_Header",
                null, null, 40);
        Thread.sleep(2000);
        Navigation.verify_BreadcrumbList(breadcrumb);

        BaseUI.verifyElementHasExpectedText("merchandise_CategoryPage_Header",
                categoryName.replaceAll("Sports & Outdoors", "Sports and Outdoors"));
    }

    public static void verifyCategoryImagesDisplayed() {
        ArrayList<String> allCategoryImagesList = return_CategoryImages_Displayed();
        String[] categoryHeaderOptions = { "Category Name - Handbags", "Category Name - Sunglasses", "Category Name - Home & Kitchen", "Category Name - Sports & Outdoors", "Category Name - Jewelry", "Category Name - Beauty & Fragrance" };

        BaseUI.verify_true_AndLog(allCategoryImagesList.size() > 0, "Category images list IS NOT EMPTY",
                "Category images list IS EMPTY");

        BaseUI.verify_true_AndLog(allCategoryImagesList.size() == categoryHeaderOptions.length,
                "Category image matched expected values in size.", "Category image did not match expected values in size.");

        for (String imageOption : categoryHeaderOptions) {
            BaseUI.verify_true_AndLog(allCategoryImagesList.contains("Category Name - "+imageOption),
                    "Category image alt has " + imageOption + " found in list.",
                    "Category image alt " + imageOption + " NOT found in list.");
        }
    }

    public static void verifyCategoryHeadersDisplayed() {
        ArrayList<String> allCategoryHeadersList = return_CategoryHeaders_Displayed();
        String[] categoryHeaderOptions = { "Sunglasses", "Handbags", "Home & Kitchen", "Sports & Outdoors", "Jewelry", "Beauty & Fragrance" };

        BaseUI.verify_true_AndLog(allCategoryHeadersList.size() > 0, "Category headers list IS NOT EMPTY",
                "Category headers list IS EMPTY");

        BaseUI.verify_true_AndLog(allCategoryHeadersList.size() == categoryHeaderOptions.length,
                "Category Header matched expected values in size.", "Category Header did not match expected values in size.");

        for (String headerOption : categoryHeaderOptions) {
            BaseUI.verify_true_AndLog(allCategoryHeadersList.contains(headerOption),
                    "Category Header option " + headerOption + " found in list.",
                    "Category Header option " + headerOption + " NOT found in list.");
        }
    }
}
