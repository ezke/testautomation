package dRewards.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import utils.BaseUI;
import utils.Locator;

public class Merchandise_ProductPage {

	public static void select_firstOption(WebElement dropdown) throws Exception {
		Select dropdownSelect = new Select(dropdown);
		if (dropdownSelect.getFirstSelectedOption().getText().equals("merchProduct_AllDropdowns")){
			dropdownSelect.selectByIndex(2);	
		} else {
			try {
				dropdownSelect.selectByIndex(1);
			}catch(NoSuchElementException e){
				BaseUI.log_Warning("Unable to find option at index 1.");
			}
		}
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
//		WebElement firstOption = dropdown.findElement(By.xpath(".//option[2]"));
//		BaseUI.click(firstOption);
//		Thread.sleep(1000);
	}
	
	public static void add_toShoppingCart() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("merchProduct_AddToShoppingCartButton"));
		Thread.sleep(1500);		
	}

}
