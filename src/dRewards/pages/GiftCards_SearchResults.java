package dRewards.pages;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class GiftCards_SearchResults {

	public static void click_LeftPanelLink_ByText(String linkText) throws Exception {
		if(ClientDataRetrieval.isLegacyClient() 
				&& BaseUI.elementAppears(Locator.lookupOptionalElement("giftCards_SearchResult_linkBoldText_ByText", linkText, null))) {
			BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_linkBoldText_ByText", linkText, null));
			BaseUI.waitForElementToBeDisplayed("giftCards_LeftPanelSelectedItem_HeaderText", null, null, 30);
		} else {
			BaseUI.waitForElementToBeDisplayed("giftCards_SearchResult_link_ByText", linkText, null, 30);
			BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_link_ByText", linkText, null));
			BaseUI.wait_forPageToFinishLoading();
			Thread.sleep(1000);
		}		
	}
	
	public static ArrayList<String> return_List_OfLinks_ByCategoryText(String category) throws Exception {
		GiftCards_LandingPage.navigate_LeftPanel_ByText(category);

		if(ClientDataRetrieval.isRedesignClient()) {
			if(BaseUI.elementAppears(Locator.lookupOptionalElement("giftCards_CategoryLandingPage_NoDealFoundMessage"))) {			
				BaseUI.log_AndFail(category +": No Deals Found is displayed on category landing page.");
				BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("giftCards_SearchResult_ListOfLinks"));
			} else {
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_ListOfLinks"));
			}
		}
		ArrayList<WebElement> linkList = Locator.lookup_multipleElements("giftCards_SearchResult_ListOfLinks", null,
				null);
		ArrayList<String> linkList_Text = new ArrayList<String>();
		for (WebElement link : linkList) {
			String linkText = BaseUI.getTextFromField(link);
			linkText = linkText
					.replace("\u203A", "").trim();

			linkList_Text.add(linkText);
		}
		return linkList_Text;
	}
	
	public static void verifySubcategoryTitle_LeftPanel(String categoryName) throws Exception {
		//Validate the Entertainment title in left panel
		String itemSelectedText = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("giftCards_SearchResult_SubcategoryHeaderTitle"), "innerText");
		BaseUI.baseStringCompare("giftCards_SearchResult_SubcategoryHeaderTitle", categoryName, itemSelectedText);
	}
	
	
	public static void verifySubcategory_FeaturedProductHeaderTitle(int index) throws Exception {
		//Validate the Featured Product header title
		String featuredProductText = null;
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			featuredProductText = featuredGiftCardList.data.get(index).get("featured_GiftCardTitle");
			BaseUI.verify_true_AndLog(featuredProductText.contains("Featured Dining") || featuredProductText.contains("Featured Shopping") 
					|| featuredProductText.contains("Featured Entertainment") || featuredProductText.contains("Featured Travel"), 
					"Featured Category Gift Card Title is Displayed: " + featuredProductText, 
					"Featured Category Gift Card Title is NOT Displayed: " + featuredProductText);
		} else {
			featuredProductText = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductHeaderTitle"));
			BaseUI.baseStringCompare("giftCards_SearchResult_FeaturedProductHeaderTitle", "Featured Products", featuredProductText);
		}		
	}
		
	public static void verifySubcategory_FeaturedProductTitle(int index) throws Exception {
		//Validate the Feature Product displayed has title
		String featuredProductTitle = null;
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			featuredProductTitle = featuredGiftCardList.data.get(index).get("featured_GiftCardName");
		} else {
			featuredProductTitle = BaseUI
					.get_Attribute_FromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Title"), "innerText");
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Title"));
		BaseUI.verify_true_AndLog(BaseUI.elementAppears(Locator.lookupOptionalElement("giftCards_SearchResult_FeaturedProductDetail_Title")),
				"Feature Product title is displayed " + featuredProductTitle  , "Feature Product title is not displayed " + featuredProductTitle);
	}
	
	
	public static void verifySubcategory_FeaturedProductImage() throws Exception {
		//Validate the logo/Image is displayed
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductImage"));
		if (ClientDataRetrieval.isLegacyClient()) {
			String featuredProductTitle = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Title"), "innerText");
			String imgSrc = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductImage"), "alt");
			BaseUI.verify_true_AndLog(imgSrc.contains(featuredProductTitle),
					"Image source contained " + featuredProductTitle,
					"Image source did NOT contain " + featuredProductTitle);
		}
	}
	
	
	public static void close_FeaturedProduct_TooltipDisplayed() throws Exception {
		
		BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_HoverOver_Tooltip"));
		Thread.sleep(1000);	
	}
	
	
	public static void verifySubcategory_FeaturedProduct_TooltipDisplayed() throws Exception {
		//Validate the tooltip is displayed		
		BaseUI.element_ClickAndHold(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Tooltip"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("giftCards_SearchResult_FeaturedProductDetail_HoverOver_Tooltip", null, null);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_HoverOver_Tooltip"));
//		String toolTip_Text = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Tooltip"), "original-title");
//		BaseUI.verify_true_AndLog(BaseUI.elementAppears(Locator.lookupOptionalElement("giftCards_SearchResult_FeaturedProductDetail_Tooltip")),
//				"Tooltip is displaying text "+ toolTip_Text, "Tooltip IS NOT displaying text "+ toolTip_Text);
//		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Tooltip"));
	}
	
	
	
	public static void verifySubcategory_FeaturedProduct_RetailPrice(int index) throws Exception {
		//Validate Retail Price, Points and You pay are numeric and greater than zero
		String retailPrice = null;
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			retailPrice = featuredGiftCardList.data.get(index).get("giftCard_RetailPrice")
					.replace("$", "").replace(",", "").replaceAll("Retail: ", "");
		} else {
			retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_RetailPrice"))
		    		.replace("$", "").replace(",", "").replaceAll("Retail: ", "");
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_RetailPrice"));
		BaseUI.verify_true_AndLog((BaseUI.string_isNumeric(retailPrice) && BaseUI.string_IsDouble(retailPrice) && Double.parseDouble(retailPrice) > 0.0), 
				"Retail Price is numeric, double and greater than 0.0: " + retailPrice, "Retail Price IS NOT numeric, double and greater than 0.0: " + retailPrice);
	}
	
	
	public static void verifySubcategory_FeaturedProduct_PointsOrSmartDollars(int index) throws Exception {
		String pointsOrSmartDollars = null;
		//Validate points or smart dollars for citi and other clients.
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars"));
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			
			pointsOrSmartDollars = featuredGiftCardList.data.get(index).get("giftCard_Points")
					.replace("\u2013", "-").replace("-", "").replace("(", "").replace(")", "").replace(":", "").replace(",", "")
					.replaceAll("after ", "").replaceAll(" Points", "")
					.replace("[^\\p{ASCII}]", "").replace("– ", "").replaceAll(" points", "").trim();
		} else {
			pointsOrSmartDollars = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars"))
					.replace("\u2013", "-").replace("-", "").replace("(", "").replace(")", "").replace(":", "").replace(",", "")
					.replaceAll("after ", "").replaceAll(" Points", "").replaceAll("\n", "")
					.replace("[^\\p{ASCII}]", "").replace("– ", "").trim();
		}
		
		
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			BaseUI.baseStringCompare("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars", "Member Savings", pointsOrSmartDollars);
		} else {
			BaseUI.verify_true_AndLog((BaseUI.string_isNumeric(pointsOrSmartDollars) || BaseUI.string_IsDouble(pointsOrSmartDollars) && Double.parseDouble(pointsOrSmartDollars) > 0.0), 
					"pointsOrSmartDollars is numeric and double: " + pointsOrSmartDollars, "pointsOrSmartDollars IS NOT numeric and double: " + pointsOrSmartDollars);
		}
	}
	
	
	public static void verifySubcategory_FeaturedProduct_YouPay(int index) throws Exception {
		String youPay = null;
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			
			youPay = featuredGiftCardList.data.get(index).get("giftCard_YouPay").replace("$", "").replace(",", "");
		} else {
			youPay = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_YouPay"))
					.replace("$", "").replace(",", "");
		}		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_YouPay"));
		BaseUI.verify_true_AndLog((BaseUI.string_isNumeric(youPay) && BaseUI.string_IsDouble(youPay) && Double.parseDouble(youPay) > 0.0), 
				"You Pay is numeric and double: " + youPay, "You Pay IS NOT numeric and double: " + youPay);
	}
	
	
	public static void verifySubcategory_FeaturedProduct_Savings(int index) throws Exception {
		String savingsDisplayed = null;
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			
			savingsDisplayed = featuredGiftCardList.data.get(index).get("giftCard_PercentSavings");
		} else {
			savingsDisplayed = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Savings"));
		}		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Savings"));
		BaseUI.verify_true_AndLog(BaseUI.elementAppears(Locator.lookupOptionalElement("giftCards_SearchResult_FeaturedProductDetail_Savings")),
				"Savings is displayed " + savingsDisplayed, "Savings IS NOT displayed " + savingsDisplayed);
	}
	
	
	
	public static void verifySubcategory_FeaturedProduct_ValidateYouPay(int index) throws Exception {
		//Validating You Pay value is correct
		String retailPrice = null, pointsOrSmartDollars = null, calculated_YouPay = null;
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			
			retailPrice = featuredGiftCardList.data.get(index).get("giftCard_RetailPrice")
					.replace("$", "").replace(",", "").replaceAll("Retail: ", "");
			pointsOrSmartDollars = featuredGiftCardList.data.get(index).get("giftCard_Points")
					.replace("\u2013", "").replace(",", "")
					.replaceAll("after ", "").replaceAll(" Points", "").replaceAll(" points", "").trim();
			
			calculated_YouPay = calculated_YouPay(pointsOrSmartDollars, retailPrice);
			
			BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_YouPay_ByText", calculated_YouPay, null));
			BaseUI.waitForElementToBeDisplayed("giftCards_SearchResult_FeaturedProductDetailPage_ProductTitle", null, null, 20);
			BaseUI.verifyElementHasExpectedText("giftCards_SearchResult_FeatutedProductDetailPage_YouPay", "$"+calculated_YouPay.trim());
		} else {
			retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_RetailPrice"))
		    		.replace("$", "").replace(",", "");
			
			pointsOrSmartDollars = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars"))
					.replace("\u2013", "").replace(",", "").trim();
			
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				String pointsOrSmartDollars1 = BaseUI.get_NonVisible_TextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars_PointsText"))
						.replace("\u2013", "").replace("(", "").replace(")", "").trim();
				calculated_YouPay = calculated_YouPay(pointsOrSmartDollars1, retailPrice);			
			} else {
				calculated_YouPay = calculated_YouPay(pointsOrSmartDollars, retailPrice);				
			}
			
			BaseUI.verifyElementHasExpectedPartialText("giftCards_SearchResult_FeaturedProductDetail_YouPay", calculated_YouPay);
		}
	}
	
	
	//Validating Savings percentage value is correct
	public static void verifySubcategory_FeaturedProduct_ValidateSavingsPercentage(int index) throws Exception {
		String retailPrice = null, pointsOrSmartDollars = null, savingsDisplayed = null, savingsPercentage = null;
		
		if (ClientDataRetrieval.isRedesignClient()) {
			TableData featuredGiftCardList = GiftCards_LandingPage.return_FeaturedGiftCard_TableData();

			BaseUI.verify_true_AndLog(featuredGiftCardList.data.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.data.size(), 
					"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.data.size());
			
			retailPrice = featuredGiftCardList.data.get(index).get("giftCard_RetailPrice")
					.replace("$", "").replace(",", "").replaceAll("Retail: ", "");
			pointsOrSmartDollars = featuredGiftCardList.data.get(index).get("giftCard_Points")
					.replace("\u2013", "").replace(",", "").replaceAll("after ", "")
					.replaceAll(" Points", "").replace(" points", "").trim();
			savingsDisplayed = featuredGiftCardList.data.get(index).get("giftCard_PercentSavings")
					.replace("%", "").replaceAll("Savings", "").replace("(", "").replace(")", "").trim();
			
			savingsPercentage = calculated_SavingsPercentage(pointsOrSmartDollars, retailPrice);
		} else {
			retailPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_RetailPrice"))
		    		.replace("$", "").replace(",", "").replaceAll("Retail: ", "");
			pointsOrSmartDollars = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars"))
					.replace("\u2013", "").replace(",", "").replaceAll("after ", "")
					.replaceAll(" Points", "").replace("– ", "").trim();
			savingsDisplayed = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_Savings"))
					.replace("%", "").replaceAll("Savings", "").replace("(", "").replace(")", "").trim();
			
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				String pointsOrSmartDollars1 = BaseUI.get_NonVisible_TextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductDetail_PointsOrSmartDollars_PointsText"))
						.replace("\u2013", "").replace("(", "").replace(")", "").trim();
				
				savingsPercentage = calculated_SavingsPercentage(pointsOrSmartDollars1, retailPrice);  
			} else {
				savingsPercentage = calculated_SavingsPercentage(pointsOrSmartDollars, retailPrice);
			}
		}
		
		BaseUI.verify_true_AndLog(savingsDisplayed.equals(savingsPercentage), 
				"Savings calculated "+ savingsDisplayed + " MATCH displayed " + savingsPercentage, 
				"Savings calculated "+ savingsDisplayed + " DOES NOT MATCH displayed " + savingsPercentage);
	}

	
	public static String calculated_SavingsPercentage(String pointsOrSmartDollars, String retailPrice) {
		
		String savingsPercentage;
		
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.isRedesignClient()) {
				
			int points = (Integer.parseInt(pointsOrSmartDollars)/100);
			double points_convertedToDouble = points;
			String points_SmartDollars = String.valueOf(points_convertedToDouble);
			
			Double savings_Calculated = ((Double.parseDouble(points_SmartDollars)/Double.parseDouble(retailPrice))*100);
			int convert_savings_Calculated = savings_Calculated.intValue();
			
			savingsPercentage = String.valueOf(convert_savings_Calculated);
		} else {
			Double savings_Calculated = ((Double.parseDouble(pointsOrSmartDollars)/Double.parseDouble(retailPrice))*100);
			int convert_savings_Calculated = savings_Calculated.intValue();
			
			savingsPercentage = String.valueOf(convert_savings_Calculated);
		}

		return savingsPercentage;
	}
	
	
	public static String calculated_YouPay (String pointsOrSmartDollars, String retailPrice) {
		
		String calculated_YouPay;
		
		if (ClientDataRetrieval.isRedesignClient()
				|| ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)  
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards) 
				|| ClientDataRetrieval.isRedesignClient()) {
			
			int points = (Integer.parseInt(pointsOrSmartDollars)/100);
			double points_convertedToDouble = points;
			DecimalFormat df = new DecimalFormat("###.00");
			String points_SmartDollars = String.valueOf(points_convertedToDouble);
			calculated_YouPay = String.valueOf(df.format(Double.parseDouble(retailPrice) - Double.parseDouble(points_SmartDollars)));
		}
		else {
			calculated_YouPay = String.valueOf(Double.parseDouble(retailPrice) - Double.parseDouble(pointsOrSmartDollars));
		}
		
		return calculated_YouPay;	
	}
	
	
	public static void click_ViewMore_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_LeftPanel_ViewMore_Text"));
		Thread.sleep(200);
	}
	
	
    public static void click_ViewLess_Link() throws Exception {
    	BaseUI.click(Locator.lookupRequiredElement("giftCards_SearchResult_LeftPanel_ViewLess_Text"));
		Thread.sleep(200);
	}


	public static void verify_ViewMore_ListOfLinks_InEach_GiftCardsCategory(ArrayList<String> leftnavigationLinks) throws Exception {
		ArrayList<WebElement> linkList = Locator.lookup_multipleElements("giftCards_SearchResult_LeftPanel_LinkList", null,
				null);
		ArrayList<String> viewMoreLists = new ArrayList<String>();
		for (WebElement link : linkList) {
			String linkText = BaseUI.getTextFromField(link);
			viewMoreLists.add(linkText);
		}
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verify_true_AndLog(viewMoreLists.size() > 0 , "Found expanded list: " + viewMoreLists.size(),
					"Did NOT find expanded list: " + viewMoreLists.size());
			BaseUI.verify_true_AndLog(viewMoreLists.size() > leftnavigationLinks.size() ,
					"Expanded list increased the left navigation panel options: " + viewMoreLists.size(),
					"Expanded list Did NOT increase the left navigation panel options: " + viewMoreLists.size());
		} else {
			BaseUI.verify_true_AndLog(viewMoreLists.size() > 0, "Found expanded list: " + viewMoreLists.size(),
					"Did NOT find expanded list: " + viewMoreLists.size());
		}
			
		BaseUI.verify_true_AndLog(viewMoreLists.size() > 0, "Found expanded list: " + viewMoreLists.size(),
				"Did NOT find expanded list: " + viewMoreLists.size());
			
	    for (int i=0; i<viewMoreLists.size(); i++) {
	    	BaseUI.scroll_to_element(linkList.get(i));
	    	//BaseUI.verifyElementAppears(linkList.get(i));
	    	if(!(viewMoreLists.get(i).equals("-  View less"))) {
	    		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
					BaseUI.verify_true_AndLog(!(viewMoreLists.get(i).equals("View More") && 
							!BaseUI.stringEmpty(viewMoreLists.get(i).trim())), 
							 "Item displayed when View More link is clicked : "+ viewMoreLists.get(i), 
							 "Item NOT displayed when View More link is clicked : "+ viewMoreLists.get(i));
					//break;
				} else {
					BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(viewMoreLists.get(i).trim()), /*(viewMoreLists.get(i).isEmpty()*/
							 "Item displayed when View More link is clicked : "+ viewMoreLists.get(i), 
							 "Item NOT displayed when View More link is clicked : "+ viewMoreLists.get(i));
				}
	    	} 
		}
		
	}
	
	
	public static void verify_ViewLess_ListOfLinks_ByEach_GiftcardCategory() throws Exception {
		//GiftCards_LandingPage.navigate_LeftPanel_ByText(category);

		ArrayList<WebElement> linkList = Locator.lookup_multipleElements("giftCards_SearchResult_LeftPanel_LinkList_Less", null,
				null);
		ArrayList<String> viewLessLists = new ArrayList<String>();

		for (WebElement link : linkList) {
			String linkText = BaseUI.getTextFromField(link);
			viewLessLists.add(linkText);
		}
			
		for (int i=0; i<viewLessLists.size(); i++) {
		    BaseUI.verifyElementAppears(linkList.get(i));
		    BaseUI.verify_true_AndLog(viewLessLists.get(i).isEmpty(), 
					"Items are hidden when View Less is clicked", "Items still visible when View Less is clicked");
		}
	}
	
	
	public static void verifyLeftPanel_ViewMore_Text() throws Exception {
		//Validate the View More text in left panel
		String viewMoreText = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_LeftPanel_ViewMore_Text"));
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("giftCards_SearchResult_LeftPanel_ViewMore_Text", "+ View more", viewMoreText);
		} else {
			BaseUI.baseStringCompare("giftCards_SearchResult_LeftPanel_ViewMore_Text", "+  View More", viewMoreText);
		}		
	}
	
	
	public static void clickAndVerifyLeftPanel_ViewMoreLink() throws Exception {
		//Click View More text in left panel and validate links displayed
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("giftCards_SearchResult_LeftPanel_ViewMore_Text"));
		ArrayList<String> leftnavigationLinks = return_List_OfLinks_ByCategoryText();
		ArrayList<WebElement> viewMoreLinks = Locator.lookup_multipleElements("giftCards_SearchResult_LeftPanel_ViewMore_Text", null, null);
		BaseUI.verify_true_AndLog(viewMoreLinks.size() > 0, "Found View More links.", "Did NOT find View More links.");
		
		for(WebElement viewMoreLink : viewMoreLinks) {
			BaseUI.click(viewMoreLink);
			BaseUI.waitForElementToBeDisplayed("giftCards_SearchResult_LeftPanel_ViewLess_Text", null, null);
			Thread.sleep(2000);
		}
		
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("giftCards_SearchResult_LeftPanel_ViewMore_Text"));		
		verify_ViewMore_ListOfLinks_InEach_GiftCardsCategory(leftnavigationLinks);
	}
	
	
	public static void verifyLeftPanel_ViewLess_Text() throws Exception {
		//Validate the View Less text in left panel
		String viewLessText = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_LeftPanel_ViewLess_Text"));
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("giftCards_SearchResult_LeftPanel_ViewLess_Text", "- View less", viewLessText);
		} else {
			BaseUI.baseStringCompare("giftCards_SearchResult_LeftPanel_ViewLess_Text", "-  View less", viewLessText);
		}
	}
	
		
	public static void clickAndVerifyLeftPanel_ViewLessLink() throws Exception {
		//Click View More text in left panel and validate links displayed
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("giftCards_SearchResult_LeftPanel_ViewLess_Text"));
		
		GiftCards_SearchResults.click_ViewLess_Link();
		
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("giftCards_SearchResult_LeftPanel_ViewLess_Text"));
       // ArrayList<String> viewLessLists = new ArrayList<String>(GiftCards_SearchResults.return_ViewLess_ListOfLinks_ByCategoryText());
        
		verify_ViewLess_ListOfLinks_ByEach_GiftcardCategory();
	}
	
	
	public static ArrayList<String> return_List_OfLinks_ByCategoryText() throws Exception {
		ArrayList<WebElement> linkList = Locator.lookup_multipleElements("giftCards_SearchResult_ListOfLinks", null,
				null);
		ArrayList<String> linkList_Text = new ArrayList<String>();
		for (WebElement link : linkList) {
			String linkText = BaseUI.getTextFromField(link);

			linkList_Text.add(linkText);
		}
		return linkList_Text;
	}

	public static void clickAndVerifyEachLink_ForEachGiftCardSubcategory(String categoryName, String link, int numberOfResults) throws Exception {
		GiftCards_LandingPage.navigate_LeftPanel_ByText(categoryName);

		GiftCards_SearchResults.click_LeftPanelLink_ByText(link);

		if(categoryName.equals("Entertainment")) {
			categoryName = "Movie Tickets";
		}

		String[] breadcrumbs = { "Home", "Gift Cards", categoryName, link};

		Navigation.verify_BreadcrumbList(breadcrumbs);
		Integer results = GiftCards_LandingPage.return_ResultsCount();

		BaseUI.verify_true_AndLog(results == numberOfResults,
				MessageFormat.format("Result list {0} match the Showing Item of results {1}", results, numberOfResults),
				MessageFormat.format("Result list {0} did NOT match the Showing Item of results {1}", results,
						numberOfResults));
		if (ClientDataRetrieval.isLegacyClient()) {
			String itemSelectedText;
			if(link.startsWith("New Arrivals")) {
				itemSelectedText = BaseUI
						.getTextFromField(Locator.lookupRequiredElement("giftCards_Selected_Item_NewArrival_NameField"));
			} else {
				itemSelectedText = BaseUI
						.getTextFromField(Locator.lookupRequiredElement("giftCards_Selected_Item_NameField"));
			}
			itemSelectedText = itemSelectedText.replace("\u203A", "").replace(" [X]", "").replace(" (X)", "")
					.replace(" (X) ", "").trim();
			BaseUI.baseStringCompare("Item Selected Text", link, itemSelectedText);
		}
	}
}
