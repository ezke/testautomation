package dRewards.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Account_History {

	// Verify for first row.
	public static void Verify_PromoCode(String promocode, String rewardAmount, String promodescription) throws Exception {
		String promoCodeNameAndDate = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
		String promoCodeDate = promoCodeNameAndDate.split("\\n")[0];
		String promoCodeName = promoCodeNameAndDate.split("\\n")[1];
		String promoCodeDescription = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionDescription"));
		String promoCodeAmount = "";
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			promoCodeAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_PointsUsed"));
		} else {
			promoCodeAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionAmount"));
		}

		// Comparison to verify the Account History fields are accurate.
		if (ClientDataRetrieval.isRedesignClient()) {
			String promoCodeNameAndDateAARP = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
			String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM\ndd").toUpperCase();

			BaseUI.verify_true_AndLog(promoCodeNameAndDateAARP.equals(todaysDate),
					"Promocode date matched today's date.", "Promocode date did not match today's date.");
			BaseUI.verify_true_AndLog(promoCodeDescription.equals(promocode), "Promo Description matched.",
					"Promo Description did NOT match.");
			BaseUI.verify_true_AndLog(promoCodeAmount.equals(rewardAmount), "Promo amount matched.",
					"Promo amount did NOT match.");
		} else {

			BaseUI.verify_true_AndLog(promoCodeDate.equals(getTodaysDate_AsString()),
					"Promocode date matched today's date.", "Promocode date did not match today's date.");
			BaseUI.verify_true_AndLog(promoCodeName.equals("Promo Code: " + promocode), "Promocode name matched.",
					"Promocode name did NOT match.");
			BaseUI.verify_true_AndLog(promoCodeDescription.equals(promodescription), "Promo Description matched.",
					"Promo Description did NOT match.");
			BaseUI.verify_true_AndLog(promoCodeAmount.equals(rewardAmount), "Promo amount matched.",
					"Promo amount did NOT match.");
		}
	}
	// Verify for first row.
	// From and To Dates must be in format "yyyy-MM-dd"
	public static void Verify_HotelBooking(String fromDate, String toDate, String rewardAmount, String hotelName,
			String allstateCash) {
		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyy-MM-dd");
		String dateBooked = null;
		String bookingText = null;
		String transactionName;

		if (! ClientDataRetrieval.isRedesignClient()) {
			transactionName = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
			String[] transactionParts = transactionName.split("\\n");
			if (transactionParts.length < 2) {
				BaseUI.log_AndFail("Transaction name '" + transactionName + "': Expected two lines of text, but found only one");
			}
			dateBooked = transactionParts[0];
			bookingText = transactionParts[1];
		}

		String bookingDescription = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionDescription"));
		String bookingPointsDeductedAmount = "";
		String allState_RewardsCash = "";
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			bookingPointsDeductedAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_PointsUsed"))
					.replace("-", "").replace(",", "").trim();
			allState_RewardsCash = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_CashRewardsUsed"))
					.replace("-", "").replace(",", "").replace("$", "").trim();
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
		} else {
			bookingPointsDeductedAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionAmount"))
					.replace("-", "").replace(",", "").trim();
		}
		String expected_Description;

		// Comparison to verify the Account History fields are accurate.
		if (ClientDataRetrieval.isRedesignClient()) {
			todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM" + "\n" + "dd")
					.toUpperCase();
			dateBooked = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
			bookingText = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionType"));
			expected_Description = fromDate + " thru " + toDate + " " + hotelName;
			BaseUI.baseStringCompare("Date Booked", todaysDate, dateBooked);
		} else {
			expected_Description = fromDate + " thru " + toDate + "\n" + "Cancel Reservation" + "\n" + hotelName;
			BaseUI.baseStringCompare("Date Booked", todaysDate, dateBooked);
		}
		BaseUI.baseStringCompare("Hotel Booking text", "Hotel Booking", bookingText);
		BaseUI.baseStringCompare("Description", expected_Description, bookingDescription);

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			BaseUI.baseStringCompare("Points", rewardAmount, bookingPointsDeductedAmount);
			BaseUI.baseStringCompare("Cash Rewards", allstateCash, allState_RewardsCash);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String transactionAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionAmount"));
			transactionAmount = transactionAmount.replace("$", "").trim();
			Integer transactionAmountInt = Integer.parseInt(transactionAmount);
			BaseUI.verify_true_AndLog(transactionAmountInt > 0, "Saving Amount is greater than 0",
					"Saving Amount is Not greater than 0");

		} else {
			BaseUI.baseStringCompare("Amount", rewardAmount, bookingPointsDeductedAmount);
		}
	}

	public static void verify_Purchase(String orderID, String points) throws Exception {
		String date = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
		String pointsAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionAmount"));
		String transactionDescription = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionDescription"));

		String todaysDate;
		String actualPoints;
		if (ClientDataRetrieval.isRedesignClient()) {
			todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM" + "\n" + "dd")
					.toUpperCase();
			actualPoints = pointsAmount.replace("- ", "");
		} else {
			todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyy-MM-dd");
			String[] pointsParts = pointsAmount.split("\\s");
			if (pointsParts.length < 2) {
				BaseUI.log_AndFail("Points amount '" + pointsAmount + "': expected at least two words, but found only one");
			}
			actualPoints = pointsParts[1];
		}
		
		if (date.contains("PROMO")) {
			BaseUI.baseStringCompare("Today's Date", todaysDate + "\n" + "Shopping - PROMO", date);
		} else if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.baseStringCompare("Today's Date", todaysDate, date);
		} else {
			BaseUI.baseStringCompare("Today's Date", todaysDate + "\n" + "Shopping", date);
		}
		orderID = orderID.split("\\-")[1];
		String actualOrderID = transactionDescription.split("\\n")[0];
		actualOrderID = actualOrderID.substring(actualOrderID.indexOf("-") + 1, actualOrderID.length());
		BaseUI.baseStringCompare("Order ID", orderID.trim(), actualOrderID);

		BaseUI.baseStringCompare("Points", points, actualPoints);

	}

	public static void verify_Purchase_AllstateCash(String orderID, String points, String allstate_cash)
			throws Exception {

		String date = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
		String pointsAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_PointsUsed"));
		String cashAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_CashRewardsUsed"));
		String transactionDescription = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionDescription"));

		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyy-MM-dd");
		if (date.contains("PROMO")) {
			BaseUI.baseStringCompare("Today's Date", todaysDate + "\n" + "Shopping - PROMO", date);
		} else {
			BaseUI.baseStringCompare("Today's Date", todaysDate + "\n" + "Shopping", date);
		}

		orderID = orderID.split("\\-")[1];
		String actualOrderID = transactionDescription.split("\\n")[0];
		actualOrderID = actualOrderID.substring(actualOrderID.indexOf("-") + 1, actualOrderID.length());
		BaseUI.baseStringCompare("Order ID", orderID, actualOrderID);

		String actualPoints = pointsAmount.split("\\s")[1];
		BaseUI.baseStringCompare("Points", points, actualPoints);

		String actualCashAmount = cashAmount;
		if (allstate_cash != null && !allstate_cash.equals("$0.00")) {
			allstate_cash = allstate_cash.replace("- ", "");
			BaseUI.baseStringCompare("Allstate Cash", allstate_cash, actualCashAmount);
		} else {
			BaseUI.baseStringCompare("Allstate Cash", "--", actualCashAmount);
		}

	}

	public static void verify_LocalOffer_PointsRedeemed(String points) throws Exception {
		String date = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
		String pointsAmount = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionAmount"));
		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyy-MM-dd");

		BaseUI.baseStringCompare("Today's Date", todaysDate + "\n" + "Offer", date);
		String actualPoints = pointsAmount.split("\\s")[1].replace(",", "");
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.baseStringCompare("Points", points, actualPoints);
		}
	}

	public static void verify_LocalOffer_ViewDetailLink_ByOfferName(String localOfferName) {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("accnt_ViewDetails_ByOrderID", localOfferName, null));
		BaseUI.verifyElementHasExpectedPartialTextByElement(
				Locator.lookupRequiredElement("accnt_ViewDetails_ByOrderID", localOfferName, null), "View Detail");
	}

	public static void click_LocalOffer_ViewDetail_ByOfferName(String localOfferName) throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("accnt_ViewDetails_ByOrderID", localOfferName, null), true,
				500);
	}

	public static void verify_LocalOffer_ViewDetail_HeaderTitle() {
		String expectedHeader = "";
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			expectedHeader = "Thank you for shopping at Destination Rewards.";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			expectedHeader = "Thank you for shopping at TVC Holding Inc.";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			expectedHeader = "Thank you for shopping at Rewards for Good.";
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			expectedHeader = "Thank you for shopping at Allstate Rewards"+"\u00AE"+".";
		}else if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			expectedHeader = "Thank you for shopping at Citi Easy Deals.";
		}if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			expectedHeader = "Thank you for shopping at Answer Financial.";
		}else if(ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)){
			expectedHeader = "Thank you for shopping at Jeep Wave.";
		}	
		String actualHeader = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_HeaderTitle"));
		BaseUI.baseStringCompare("HeaderTitle", expectedHeader, actualHeader);
	}

	public static void verify_LocalOffer_ViewDetail_HeaderMessage(String points) {
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String headerMessage = "You used " + points + " " + ClientDataRetrieval.client_Currency
					+ " for access to the coupon offer below.";

			String actualHeaderMessage = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_HeaderMessage"));
			BaseUI.baseStringCompare("HeaderMessage", headerMessage, actualHeaderMessage);
		}
	}

	public static void verify_LocalOffer_ViewDetail_OfferRedeemedDate() {
		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyy-MM-dd");
		String actualDate = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_OrderDate"));
		BaseUI.baseStringCompare("Order Date", todaysDate, actualDate);
	}

	public static void verify_LocalOffer_ViewDetail_CouponOffer_Message() {
		String expectedCouponOfferMsg = LocalOffers_LandingPage.localOfferDealText.replace("an ongoing ", "");
		String actualCouponOfferMsg = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_CouponOffer"))
				.replace("an ongoing ", "");
		BaseUI.baseStringCompare("Coupon Offer Description", expectedCouponOfferMsg, actualCouponOfferMsg);
	}

	public static void verify_LocalOffer_ViewDetail_Points(String points) {
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String actualPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_PointsUsed"));
			BaseUI.baseStringCompare("Coupon Offer Description", points, actualPoints);
		}
	}

	public static void verify_LocalOffer_ViewDetail_MerchantName(String merchantName) {
		String actualMerchantName = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_MerchantName"));
		BaseUI.baseStringCompare("Merchant Name", merchantName, actualMerchantName);
	}

	public static void verify_LocalOffer_ViewDetail_MerchantAddress() {
		String location = ClientDataRetrieval.environment
				.equals("RC") ? "Fort" : "Ft.";
		String expectedAddress = LocalOffers_LandingPage.localOfferAddress;
		expectedAddress = expectedAddress.split("\\(")[0].trim();
		String actualAddress = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_LocalOffer_MerchantAddress"));
		BaseUI.baseStringCompare("Merchant Address", expectedAddress.replace("Fort", location), actualAddress);
	}

	public static void verify_Purchase_Cancelled(String points) throws Exception {
		String date = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionName"));
		// String pointsAmount =
		// BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionAmount"));
		String transactionDescription;
		String todaysDate;
		if (ClientDataRetrieval.isRedesignClient()) {
			todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM" + "\n" + "dd")
					.toUpperCase();
			transactionDescription = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionType"));
			
		} else {
			todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyy-MM-dd");
			transactionDescription = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("accnt_Row1_TransactionDescription"));
		}
		
		BaseUI.baseStringCompare("Today's Date", todaysDate.trim(), date.trim());
		BaseUI.baseStringCompare("Order Status", "Shopping order cancellation", transactionDescription);
		BaseUI.verifyElementHasExpectedPartialText("accnt_Row1_TransactionAmount", points);
		// BaseUI.baseStringCompare("Points", points, pointsAmount);

	}

	public static String getTodaysDate_AsString() {
		String format = "yyyy-MM-dd";
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String todaysDate = dateFormat.format(date);

		return todaysDate;
	}

	public static String getTodaysDate_AsStringAARP() {
		String format = "dd";
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String todaysDate = dateFormat.format(date);

		return todaysDate;
	}
	public static void verify_ClickingAccountLink_NavigatesTo_AccountHistoryPage() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_AccountLink"));
		Navigation.navigate_Account();
		if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			String[] breadcrumbList = {"Home", "My Rewards"};
			WebElement subHeaderText = Locator.lookupRequiredElement("acct_SubHeader_Text");
			BaseUI.verifyElementAppears(subHeaderText);
			Navigation.verify_BreadcrumbList(breadcrumbList);
			String accountPage_Header = BaseUI.getTextFromField(subHeaderText);
			BaseUI.baseStringCompare("acct_SubHeader_Text", "My Rewards", accountPage_Header);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
			||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))) {
			WebElement headerText = Locator.lookupRequiredElement("account_HeaderText");
			BaseUI.verifyElementAppears(headerText);
			String accountPage_Header = BaseUI.getTextFromField(headerText);
			BaseUI.baseStringCompare("account_HeaderText", "Account Overview", accountPage_Header);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("acct_SubHeader_Text"));
		}
		// }else
		// if(ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)){
		// BaseUI.verifyElementAppears(Locator.lookupRequiredElement("acct_SubHeader_Text"));
		//
		// }else
		// if(ClientDataRetrieval.client_Matches(client_Designation.Allstate)){
		// BaseUI.verifyElementAppears(Locator.lookupRequiredElement("acct_SubHeader_Text"));
		//
		// }else {
		// BaseUI.verifyElementAppears(Locator.lookupRequiredElement("acct_SubHeader_Text"));
		// }
	}

	public static void verify_ClosedAuctionLandingPage_HeaderAndBreadcrumb() throws Exception {
		Navigation.navigate_MyAccount_CloseAuctionsRedesign();
		String[] breadcrumbList = {"Home", "My Rewards", "My Auctions"};
		WebElement closedAuctionsHeader = Locator.lookupRequiredElement("auctionClosed_LandingPage_HeaderText");
		BaseUI.verifyElementAppears(closedAuctionsHeader);
		Navigation.verify_BreadcrumbList(breadcrumbList);
		String closedAuctionsPageHeader = BaseUI.getTextFromField(closedAuctionsHeader);
		BaseUI.baseStringCompare("auctionClosed_LandingPage_HeaderText", "My Auctions", closedAuctionsPageHeader);
	}

	public static void verify_ClosedSweepstakesLandingPage_HeaderAndBreadcrumb() throws Exception {
		Navigation.navigate_MyAccount_ClosedSweepstakes();
		String[] breadcrumbList = {"Home", "My Rewards", "My Sweepstakes"};
		WebElement closedSweepstakesHeader = Locator.lookupRequiredElement("sweepstakesClosed_LandingPage_HeaderText");
		BaseUI.verifyElementAppears(closedSweepstakesHeader);
		Navigation.verify_BreadcrumbList(breadcrumbList);
		String closedSweepstakesPageHeader = BaseUI.getTextFromField(closedSweepstakesHeader);
		BaseUI.baseStringCompare("sweepstakesClosed_LandingPage_HeaderText", "My Sweepstakes", closedSweepstakesPageHeader);
	}

	public static void clickViewTransactionHistoryBtn() throws Exception {
		Navigation.navigate_Account();
		BaseUI.click(Locator.lookupRequiredElement("accnt_ViewTransactionHistoryBtn"));
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("transactionHistory_LandingPage_HeaderText", null, null, 30);
	}

	public static void verify_TransactionHistoryLandingPage_HeaderAndBreadcrumb() throws Exception {
		clickViewTransactionHistoryBtn();
		String[] breadcrumbList = {"Home", "My Rewards", "Transaction History"};
		WebElement transactionHistoryHeader = Locator.lookupRequiredElement("transactionHistory_LandingPage_HeaderText");
		BaseUI.verifyElementAppears(transactionHistoryHeader);
		Navigation.verify_BreadcrumbList(breadcrumbList);
		String transactionHistoryPageHeader = BaseUI.getTextFromField(transactionHistoryHeader);
		BaseUI.baseStringCompare("transactionHistory_LandingPage_HeaderText", "Transaction History", transactionHistoryPageHeader);
	}
}
