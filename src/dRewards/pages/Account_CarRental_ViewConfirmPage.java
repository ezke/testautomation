package dRewards.pages;

import dRewards.ClassObjects.CarRentalSearch_CheckoutDetails;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Account_CarRental_ViewConfirmPage {

	
	public static CarRentalSearch_CheckoutDetails retrieve_CarRental_ViewConfirmPage_PriceDetails() {
		CarRentalSearch_CheckoutDetails carRentalPriceDetails = new CarRentalSearch_CheckoutDetails();
		
		if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) ||
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			carRentalPriceDetails.avgBaseRate = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_AvgBaseRate"));
			carRentalPriceDetails.points_OrSmartDollars = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars"))
					.replace("\u2013", "");
			carRentalPriceDetails.taxes_OrFees = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TaxesOrFees"));
			carRentalPriceDetails.totalWithTax = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TotalWithTax"));
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			carRentalPriceDetails.avgBaseRate = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_AvgBaseRate"));
			carRentalPriceDetails.rentalCharge = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_RentalCharge"));
			carRentalPriceDetails.taxes_OrFees = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TaxesOrFees"));
			carRentalPriceDetails.totalWithTax = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TotalWithTax"));
		} else if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)){
					//||(ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
			carRentalPriceDetails.avgBaseRate = BaseUI
					.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_AvgBaseRate"), "innerText");
			carRentalPriceDetails.taxes_OrFees = BaseUI
					.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TaxesOrFees"), "innerText");
			carRentalPriceDetails.totalWithTax = BaseUI
					.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TotalWithTax"), "innerText");
			carRentalPriceDetails.points_OrSmartDollars = BaseUI
					.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars"), "innerText");
		} else {
			carRentalPriceDetails.avgBaseRate = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_AvgBaseRate"));
			carRentalPriceDetails.points_OrSmartDollars = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars"));
			carRentalPriceDetails.rentalCharge = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_RentalCharge"));
			carRentalPriceDetails.taxes_OrFees = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TaxesOrFees"));
			carRentalPriceDetails.totalWithTax = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_TotalWithTax"));
		}
	
		return carRentalPriceDetails;
	}
	
	
	public static void verify_CarRental_MyAccount_ViewConfirmPage_PriceDetails(String avgBaseRate, String points_OrSmartDollars,
			String youPay, String taxesOrFees, String totalWithTax) throws Exception {

		//String currentWindow = Browser.driver.getWindowHandle();
        //Account.navigateAccountPage_ClickViewConfirm_CarRental();
        
		CarRentalSearch_CheckoutDetails viewConfirmPage_PriceDetails = new CarRentalSearch_CheckoutDetails();
		viewConfirmPage_PriceDetails = retrieve_CarRental_ViewConfirmPage_PriceDetails();

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {

			BaseUI.verifyElementAppears(
					Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars"));
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			String points_smartDollars = viewConfirmPage_PriceDetails.points_OrSmartDollars
					.replace(",", "").replace("\u2013", "").replace("-", "").trim();
			int points_converted = (int) Math.round(Double.parseDouble(points_smartDollars));
			BaseUI.verify_true_AndLog(Double.parseDouble(points_smartDollars) > 0.0,
					"Points or Smart Dollars " + points_smartDollars + "  is greater than 0 and numeric value",
					"Points or Smart Dollars " + points_smartDollars + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars", points_OrSmartDollars,
					String.valueOf(points_converted));
		}else {
			String points_smartDollars = viewConfirmPage_PriceDetails.points_OrSmartDollars
					.replace("\u2013", "").replace(",", "").replace("- ", "")
                    .replace("-", "").replace("– ", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(points_smartDollars) > 0.0,
					"Points or Smart Dollars " + points_smartDollars + "  is greater than 0 and numeric value",
					"Points or Smart Dollars " + points_smartDollars + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("myAccount_carRental_ViewConfirmationPage_PointsOrSmartDollars", points_OrSmartDollars,
					points_smartDollars);//String.valueOf(points_converted)
		}
		
	
		if(ClientDataRetrieval.client_Matches(client_Designation.AARP) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) || 
				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			String avgBaseRate_PerDay = viewConfirmPage_PriceDetails.avgBaseRate.replace("$", "")
					.replace("\n", "").replace(".00", "").replace("-", "").trim();
			String exp_avgBaseRate = String.valueOf(Math.round(Double.parseDouble(avgBaseRate)));
			
			BaseUI.verify_true_AndLog(Double.parseDouble(avgBaseRate_PerDay) > 0.0,
					"Avg Base Rate " + avgBaseRate_PerDay + "  is greater than 0 and numeric value",
					"Avg Base Rate " + avgBaseRate_PerDay + " is not greater than 0");
			BaseUI.baseStringCompare("myAccount_carRental_ViewConfirmationPage_AvgBaseRate", exp_avgBaseRate,
					avgBaseRate_PerDay);
			
			String taxes_Fees = viewConfirmPage_PriceDetails.taxes_OrFees.replace("+ ", "")
					.replace("$", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(taxes_Fees) > 0,
					"Taxes Or Fees " + taxes_Fees + "  is greater than 0 and numeric value",
					"Taxes Or Fees " + taxes_Fees + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_RentalCharge", taxesOrFees, taxes_Fees);

			String finalPrice = viewConfirmPage_PriceDetails.totalWithTax.replace("$", "")
					.replaceAll("Your Estimated Total: ", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(finalPrice) > 0,
					"Total with tax " + finalPrice + "  is greater than 0 and numeric value",
					"Total with tax " + finalPrice + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", totalWithTax, finalPrice);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)){
		    String avgBaseRate_PerDay = viewConfirmPage_PriceDetails.avgBaseRate.replace("$", "")
		    		.replace("\n", "").replace("-", "").trim();

			BaseUI.verify_true_AndLog(Double.parseDouble(avgBaseRate_PerDay) > 0.0,
					"Avg Base Rate " + avgBaseRate_PerDay + "  is greater than 0 and numeric value",
					"Avg Base Rate " + avgBaseRate_PerDay + " is not greater than 0");
			BaseUI.baseStringCompare("myAccount_carRental_ViewConfirmationPage_AvgBaseRate", avgBaseRate ,
					avgBaseRate_PerDay);
			
			String taxes_Fees = viewConfirmPage_PriceDetails.taxes_OrFees.replace("+ ", "")
					.replace("$", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(taxes_Fees) > 0,
					"Taxes Or Fees " + taxes_Fees + "  is greater than 0 and numeric value",
					"Taxes Or Fees " + taxes_Fees + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_RentalCharge", taxesOrFees, taxes_Fees);
			
			String finalPrice = viewConfirmPage_PriceDetails.totalWithTax.replace("$", "")
					.replaceAll("Your Estimated Total: ", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(finalPrice) > 0,
					"Total with tax " + finalPrice + "  is greater than 0 and numeric value",
					"Total with tax " + finalPrice + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", totalWithTax, finalPrice);
		} else {
			BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_RentalCharge"));
			String avgBaseRate_PerDay = viewConfirmPage_PriceDetails.avgBaseRate.replace("$", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(avgBaseRate_PerDay) > 0.0,
					"Avg Base Rate " + avgBaseRate_PerDay + "  is greater than 0 and numeric value",
					"Avg Base Rate " + avgBaseRate_PerDay + " is not greater than 0");
			BaseUI.baseStringCompare("myAccount_carRental_ViewConfirmationPage_AvgBaseRate", avgBaseRate,
					avgBaseRate_PerDay);

			String youPayPrice = viewConfirmPage_PriceDetails.rentalCharge.replace("$", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(youPayPrice) > 0,
					"Rental Charge " + youPayPrice + "  is greater than 0 and numeric value",
					"Rental Charge " + youPayPrice + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_RentalCharge", youPay, youPayPrice);
			
			String taxes_Fees = viewConfirmPage_PriceDetails.taxes_OrFees.replace("+ ", "")
					.replace("$", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(taxes_Fees) > 0,
					"Taxes Or Fees " + taxes_Fees + "  is greater than 0 and numeric value",
					"Taxes Or Fees " + taxes_Fees + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_RentalCharge", taxesOrFees, taxes_Fees);

			String finalPrice = viewConfirmPage_PriceDetails.totalWithTax.replace("$", "").trim();
			BaseUI.verify_true_AndLog(Double.parseDouble(finalPrice) > 0,
					"Total with tax " + finalPrice + "  is greater than 0 and numeric value",
					"Total with tax " + finalPrice + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", totalWithTax, finalPrice);

			String FinalPriceAfterTaxes = String.valueOf(BaseUI
					.convertDouble_ToString_ForCurrency(Double.parseDouble(youPayPrice) + Double.parseDouble(taxes_Fees)));
			BaseUI.baseStringCompare("carRental_ReserveConfirmPage_TotalPriceWithTax", finalPrice,
					FinalPriceAfterTaxes);
			BaseUI.verify_true_AndLog((Double.parseDouble(FinalPriceAfterTaxes) == Double.parseDouble(finalPrice)),
					"Total with tax " + FinalPriceAfterTaxes + " match " + finalPrice,
					"Total with tax " + FinalPriceAfterTaxes + " does not match " + finalPrice);
		}
	}
	
	//Validate Header title on View COnfirm page
    public static void ClickAndVerify_ViewConfirmPage_HeaderTitle() throws Exception {
		
		//String currentWindow = Browser.driver.getWindowHandle();
		Account.navigateAccountPage_ClickViewConfirm_CarRental();

        //Validate View Conformation page title and Client logo
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_HeaderTitle"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_HeaderTitle"));
		}
	}
    
    
    //Validate Client logo on View confirm page
    public static void verify_ViewConfirmPage_ClientLogo() throws Exception {

        //Validate View Conformation page title and Client logo
		if(ClientDataRetrieval.client_Matches(client_Designation.AARP) || 
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ClientLogo"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ClientLogo"));
		}
	} 
	
	//Validate Header Message on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_HeaderMessage() throws Exception {
		String headerMessage;
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_HeaderMessage"));

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			headerMessage = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_HeaderMessage"))
					.trim();
			BaseUI.baseStringCompare("Header Message under Vehicle header Title",
					"This letter is your confirmation and must be presented at check-in.", headerMessage);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			headerMessage = BaseUI.get_Attribute_FromField(
					Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_HeaderMessage"), "innerText")
					.replace("QA,", "").replace("\n", "").trim();
			BaseUI.baseStringPartialCompare("Header Message under Vehicle header Title", "This receipt is intended for your records; please review to confirm all pricing.", headerMessage);
		} else if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards) 
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			String clientName, custServiceNumber;
			if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				clientName = "Streetwise Rewards";
				custServiceNumber = "1-866-257-3927";
			} else {
				clientName = "Pro Driver Rewards";
				custServiceNumber = "1-866-25REWARDS";
			}
			headerMessage = BaseUI
					.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_HeaderMessage"))
					.trim();
			BaseUI.baseStringCompare("Header Message under Vehicle header Title",
					"This letter is your confirmation and must be presented at the pick-up location. Please print a copy to be presented at the car rental counter."
					+ " If you would like a duplicate, you can visit the "+'"'+"My Account"+'"'+" section of the " + clientName + " website at any time. We value you as a customer "
							+ "and if we can assist you in any way, please contact Customer Service at "+ custServiceNumber +" with any questions.", headerMessage);
		}
	}
	
	
	//Validate Confirmation Number on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_ConfirmationNumber(String confirmationNumText) 
			throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) || 
				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"));
			
			String confirmationNum = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"), 
					"innerText");
			System.out.println("bookingDetail: "+ confirmationNum);
			
			BaseUI.baseStringPartialCompare("Confirmation Number", confirmationNumText, confirmationNum);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReservationNum"));
			
			String confirmationNum = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReservationNum")).trim();
			BaseUI.baseStringCompare("Confirmation Number", confirmationNumText, confirmationNum);
		}	
	}
	
	
	//Validate Car Class on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_CarClass(String expected_CarClass) throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) ||
				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"));
			
			String carClass = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"), 
					"innerText");		
			BaseUI.baseStringPartialCompare("Car Class", expected_CarClass, carClass);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_CarClass"));
			
			String carClass = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_CarClass")).trim();
			BaseUI.baseStringCompare("Car Class", expected_CarClass, carClass);
		}	
	}
	
	
	//Validate Car Modal Name on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_CarModal(String carSpecificationDetails) 
			throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"));
			
			String carModalName = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"), 
					"innerText");		
			BaseUI.baseStringPartialCompare("Car Modal Name", carSpecificationDetails, carModalName);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_CarModal"));
			
			String carModalName = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_CarModal")).trim();
			BaseUI.baseStringCompare("Car Modal Name", carSpecificationDetails, carModalName);
		}
	}
	
	
	//Validate pickup date on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_PickUpDate(String expected_PickupDate, String airportDetail) throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpDate"));
			
			String exp_pickupDate = expected_PickupDate.replaceAll("Pickup:", "").trim();
			String pickUpDate = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpDate"))
					.trim(); 
			BaseUI.baseStringPartialCompare("Pickup Date", exp_pickupDate, pickUpDate);
		} else if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
			||(ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpDate"));
			
			String pickUpDate = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpDate")).trim();
			BaseUI.baseStringPartialCompare("Pickup Date", expected_PickupDate.replaceAll("Pickup:", "")
					.replaceAll("\n", ""), pickUpDate);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpDate"));
			
			String pickUpDate = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpDate")).trim();
			BaseUI.baseStringCompare("Pickup Date", expected_PickupDate, pickUpDate);
			
//			String airportDetail1 = (BaseUI.get_Attribute_FromField
//					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpAirportDetails"), "innerText"))
//					.replaceAll(expected_PickupDate, "").trim();
//			System.out.println("airportDetail : " + airportDetail1);
		}
	}
	
	
	//Validate return date on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_ReturnDate (String expected_ReturnDate, String airportDetail) 
			throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReturnDate"));
			
			String exp_returnDate = expected_ReturnDate.replaceAll("Return:", "").trim();
			String returnDate = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReturnDate"))
					.trim();
			BaseUI.baseStringPartialCompare("Return Date", exp_returnDate, returnDate);
		}  else if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReturnDate"));
			
			String returnDate = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReturnDate"))
					.replaceAll("Return:", "").trim();
			BaseUI.baseStringPartialCompare("Return Date", expected_ReturnDate.replaceAll("Return:", "")
					.replaceAll("\n", ""), returnDate);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReturnDate"));
			
			String returnDate = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_ReturnDate")).trim();
			BaseUI.baseStringCompare("Return Date", expected_ReturnDate, returnDate);
		}
	}
	
	
	//Validate Driver name on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_DriverName(String expected_driverName) throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) ||
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) ||
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) ||
				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"));
			
			String expDriverName = expected_driverName.replace("   ", " ");
			String driverName = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_BookingDetail"), 
					"innerText");		
			BaseUI.baseStringPartialCompare("Driver Name", expDriverName, driverName);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverName"));
			BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverName"));
			
			String driverName = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverName")).trim();
			
			BaseUI.baseStringCompare("Driver Name", expected_driverName, driverName);
		}	
	}
	
	
	//Validate Driver Email on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_DriverEmail(String driverEmail) throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverEmail"));
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverEmail"));
		
		String driver_Email = BaseUI.getTextFromField
				(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverEmail")).trim();
		BaseUI.baseStringCompare("Driver Email", driverEmail, driver_Email);
	}
	
	
	//Validate Airline number on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_AirlineNumber(String airlineNum) throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverAirlineNum"));
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverAirlineNum"));
		
		String airlineNumber = BaseUI.getTextFromField
				(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_DriverAirlineNum")).trim();
		BaseUI.baseStringCompare("Airline number", airlineNum, airlineNumber);
	}
	
	
	//Validate Car Rental Price details on View Confirm page
	public static void verifyCarRental_ViewConfirmPage_PriceDetails(String avgBaseRate,
			String points_OrSmartDollars, String youPay, String taxesOrFees, String totalWithTax, String currentWindow) throws Exception {
		
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_RentalCharge"));
		Account_CarRental_ViewConfirmPage.verify_CarRental_MyAccount_ViewConfirmPage_PriceDetails(avgBaseRate, 
				points_OrSmartDollars, youPay, taxesOrFees, totalWithTax);
	}
	
	//Validate Car Rental company Logo
    public static void verifyCarRental_ViewConfirmPage_CarCompanyLogo() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_CarRentalCompany_Logo"));
	}
    
    
    //Validate Savings message dispalyed at the bottom of View Confirm page
    //For AARP, Allstate, AllstateCash and TVC_Marketing
    public static void verifyCarRental_ViewConfirmPage_SavingsMessage() throws Exception {
		
		CarRentalSearch_CheckoutDetails viewconfirmPage_PriceDetails = new CarRentalSearch_CheckoutDetails();
		viewconfirmPage_PriceDetails = retrieve_CarRental_ViewConfirmPage_PriceDetails();
		
		String avgBaseRate = viewconfirmPage_PriceDetails.avgBaseRate.replace("$", "").trim();
		String pointsOrSmartDollars;
		if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) || 
				ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			String points = viewconfirmPage_PriceDetails.points_OrSmartDollars
					.replace("\u2013", "").replace(",", "").replace("-", "")
					.replace(".", "").trim();
			pointsOrSmartDollars = String.valueOf(Double.parseDouble(points)/100);
		} else {
			pointsOrSmartDollars = viewconfirmPage_PriceDetails.points_OrSmartDollars
					.replace("\u2013", "").replace(",", "").replace("-", "").trim();
		}
		

		//Calculate Savings percentage
		String	savingsPercentage = String.valueOf(/*Math.round*/(int)(
				(Double.parseDouble(pointsOrSmartDollars)/Double.parseDouble(avgBaseRate)) * 100));
		BaseUI.log_Status("savingsPercentage: " + savingsPercentage);
				
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_SavingsMessage"));
		String savingsMessage = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_SavingsMessage")).trim();
		
		BaseUI.verify_true_AndLog(savingsMessage.contains("You saved " + savingsPercentage +"% using your Pro Driver Rewards Rewards Dollars.") || 
				savingsMessage.contains("You saved " + savingsPercentage + "% using your Rewards for Good Points.") || 
				savingsMessage.contains("You've saved " + savingsPercentage + "% using your Allstate Rewards"+"\u00AE"+" Points.") ||
				savingsMessage.contains("You saved " + savingsPercentage + "% using your Streetwise Rewards Points.") ||
				savingsMessage.contains("You saved " + savingsPercentage + "% using your Jeep Wave Savings Network Savings Dollars."),
				"Savings message is displaying right savings percentage " + savingsPercentage +"% " + savingsMessage, 
				"Savings message DOES NOT display right savings percentage " + savingsPercentage + "% " + savingsMessage); 
    }
    
    
    public static void verifyCarRental_ViewConfirmPage_AirportDetails(String airportDetail) throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) ||
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) || 
				ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpAirportDetails"));
			
			String exp_airportDetail = airportDetail.replaceAll("Return:", "").trim();
			String airportName = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpAirportDetails"))
					.replace(" (", "US (").trim();
			BaseUI.baseStringPartialCompare("Airport Name and address", exp_airportDetail, airportName);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpAirportDetails"));
			
			String airportName = BaseUI.getTextFromField
					(Locator.lookupRequiredElement("myAccount_carRental_ViewConfirmationPage_PickUpAirportDetails")).trim();
			BaseUI.baseStringPartialCompare("Airport Name and address", airportDetail, airportName);
		}
    }
}
