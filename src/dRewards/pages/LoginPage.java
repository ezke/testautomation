package dRewards.pages;

import java.util.ArrayList;

import dRewards.pages_Administration.ConsumerService;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

import static dRewards.data.ClientDataRetrieval.*;

public class LoginPage {

	public static void login(String userName, String password) throws Exception {

		//Allstate  and AllState cash for XUAT env we can only login through Admin,
		//due to SSO login issue.
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			if (ClientDataRetrieval.environment.equals("XUAT")) {
				Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
				LoginPage_Admin.login_Admin();
				Navigation_Admin.navigate_ExpandCategory("Consumer Service");
				Navigation_Admin.navigate_ClickNavLink("Customers");
				ConsumerService.clickUserID_NavigateTo_EditConsumerPage(ClientDataRetrieval.userName);
				ConsumerService.clickLoginAsThisUser_OnEditConsumerPage();
			} else {
				BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
				BaseUI.click(Locator.lookupRequiredElement("loginPage_HostTextBox"));
				Thread.sleep(300);
				BaseUI.enterText(Locator.lookupRequiredElement("loginPage_HostTextBox"),
						ClientDataRetrieval.url.substring(0, ClientDataRetrieval.url.lastIndexOf("com") + 3));
				BaseUI.enterText(Locator.lookupRequiredElement("loginPage_BuyerIdTextBox"), ClientDataRetrieval.buyerID);
				BaseUI.click(Locator.lookupRequiredElement("loginPage_GETEncryptedRewardId"));
				Thread.sleep(100);
				BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
				BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButton", null, null, 10);
				Thread.sleep(1000);
			}

			if (BaseUI.pageSourceContainsString("Don't")
					&& Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link").isDisplayed()) {
				BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
				BaseUI.waitForElementToNOTBeDisplayed("nav_Message_RemindMeLater_Link", null, null, 10);
				Thread.sleep(1000);
			}
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			LaunchPage.click_sign_On();

			BaseUI.enterText(Locator.lookupRequiredElement("citiDomain_Textbox"), ClientDataRetrieval.citiDomain);
			Thread.sleep(100);
			BaseUI.enterText(Locator.lookupRequiredElement("citiEasyDeals_UserName"), userName);
			Thread.sleep(100);
			BaseUI.enterText(Locator.lookupRequiredElement("citiEasyDeals_Password"), password);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("citiEasyDeals_SignOnButton"));
			BaseUI.waitForElementToNOTBeDisplayed("citiEasyDeals_SignOnButton", null, null, 10);
			Thread.sleep(500);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButton", null, null, 10);
			BaseUI.wait_forPageToFinishLoading();
			Thread.sleep(2000);
			clickRemindMeLaterLink_RedesignHomePage();
		} else if (ClientDataRetrieval.client_Matches(client_Designation.McAfee)) {
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButton", null, null, 10);
			BaseUI.wait_forPageToFinishLoading();
			clickRemindMeLaterLink_RedesignHomePage();
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
			BaseUI.wait_forPageToFinishLoading();
			Thread.sleep(2000);

			if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {

				BaseUI.log_Status("Login with AARPRedesign AdminSite URL");
				Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
				LoginPage_Admin.login_Admin();
				Navigation_Admin.navigate_ExpandCategory("Consumer Service");
				Navigation_Admin.navigate_ClickNavLink("Customers");
				ConsumerService.clickUserID_NavigateTo_EditConsumerPage(ClientDataRetrieval.userName);
				ConsumerService.clickLoginAsThisUser_OnEditConsumerPage();

//				BaseUI.log_Status("Login with AARPRedesign Bypass URL");
//				ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
//				Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);
//				ClientDataRetrieval.set_secondaryUser_Info_AARP_Bpass("chrome");
//				BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
//				BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), secondaryUser_AARP_Bpass);
//				BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), secondaryPassword_AARP_Bpass);
//				Thread.sleep(100);
//				BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
//				BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
//				Thread.sleep(2000);
			}
			clickRemindMeLaterLink_RedesignHomePage();
		} else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("loginPage_SSOLevel_Dropdown"), "Level 2");
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_GETEncryptedRewardId"));
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButton", null, null, 10);
			Thread.sleep(1000);
			BaseUI.wait_forPageToFinishLoading();

			if (BaseUI.pageSourceContainsString("Don't")
					&& Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link").isDisplayed()) {
				BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
				Thread.sleep(1000);
			}
		} else {
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordMasked"));
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButton", null, null, 10);
			// BaseUI.activatePopupWindow();
			Thread.sleep(5000);
			// BaseUI.click(Locator.lookupRequiredElement("LoginPage_popup"));

			if (BaseUI.pageSourceContainsString("Don't")
					&& Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link").isDisplayed()) {
				try {
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
				} catch (Exception e) {
					Thread.sleep(5000);
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
				}
				Thread.sleep(1000);

				if (BaseUI.pageSourceContainsString("Don't")
						&& Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link").isDisplayed()) {
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
					Thread.sleep(1000);
				}

				BaseUI.wait_forPageToFinishLoading();
				// try {
				// BaseUI.click(Locator.lookupRequiredElement("messages_DontShowAgainLink"));
				// } catch (Exception e) {
				// Thread.sleep(5000);
				// BaseUI.click(Locator.lookupRequiredElement("messages_DontShowAgainLink"));
				// }
				// Thread.sleep(1000);
				//
				// if (BaseUI.pageSourceContainsString("Don't")
				// &&
				// Locator.lookupRequiredElement("messages_DontShowAgainLink").isDisplayed())
				// {
				// BaseUI.click(Locator.lookupRequiredElement("messages_DontShowAgainLink"));
				// Thread.sleep(1000);
				//
				// }

			}
		}
	}

	public static void loginBypassAARPRedesign(String userName, String password) throws Exception {
		BaseUI.log_Status("Login with AARPRedesign Bypass URL");
		ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
		Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);
		ClientDataRetrieval.set_secondaryUser_Info_AARP_BpassAuction("chrome");
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_BpassAuction);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_BpassAuction);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
		BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
		Thread.sleep(2000);
		//BaseUI.waitForElementToBeDisplayed_ButDontFail("nav_Message_RemindMeLater_Link", null, null, 5);
		if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_RemindMeLater_Link"))) {
			ArrayList<WebElement> remindMeLaterLinkList = Locator.lookup_multipleElements("nav_Message_RemindMeLater_Link",
					null, null);
			int i = 0;
			//remindMeLaterLinkList is pulling only 2 or 3 elements at a time,
			//even when we have 8-10 remind me later popups.
			while (true) {
				if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_RemindMeLater_Link"))) {
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
					Thread.sleep(1000);
					i++;
				} else {
					break;
				}
			}
		}
		if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_CloseBtn"))) {
			ArrayList<WebElement> winnerModalCloseBtnList = Locator.lookup_multipleElements("nav_Message_CloseBtn",
					null, null);
			for (int i = 0; i < winnerModalCloseBtnList.size(); i++) {
				if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_CloseBtn"))) {
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_CloseBtn"));
					Thread.sleep(1000);
				} else {
					break;
				}
			}
		}
	}

	//For AARPRedesign use this login for 2nd bidder for Auction test cases only
	public static void loginForSecondBidder_ForAuctions(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);
			ClientDataRetrieval.set_secondaryUser_Info_AARP_BpassAuction("chrome");
			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_BpassAuction);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_BpassAuction);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}

	//For AARPRedesign use this login for Micro Auctions No Points test cases only
	public static void loginForMicroAuctions_NoPoints(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);

			ClientDataRetrieval.set_SecondaryUser_AARP_ByPass_NoPoints("chrome");

			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_Bpass_NoPoints);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_Bpass_NoPoints);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}

	//For AARPRedesign use this login for Micro Auctions One Points.
	public static void loginForMicroAuctions_OnePoints(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);

			ClientDataRetrieval.set_SecondaryUser_AARP_ByPass_OnePoints("chrome");

			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_Bpass_OnePoints);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_Bpass_OnePoints);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}

	//For AARPRedesign use this login for Micro Auctions 1499 Points test cases only
	public static void loginForMicroAuctions_1499Points(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);

			ClientDataRetrieval.set_SecondaryUser_AARP_ByPass_1499Points("chrome");

			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_Bpass_1499Points);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_Bpass_1499Points);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}

	//For AARPRedesign use this login for Micro Auctions 1500 Points test cases only
	public static void loginForMicroAuctions_1500Points(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);

			ClientDataRetrieval.set_SecondaryUser_AARP_ByPass_1500Points("chrome");

			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_Bpass_1500Points);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_Bpass_1500Points);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}


	//For AARPRedesign use this login for Micro Auctions 30001 Points test cases only
	public static void loginForMicroAuctions_30001Points(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);

			ClientDataRetrieval.set_SecondaryUser_AARP_ByPass_30001Points("chrome");

			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_Bpass_30001Points);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_Bpass_30001Points);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}

	//For AARPRedesign use this login for Micro Auctions 31000 Points test cases only
	public static void loginForMicroAuctions_31000Points(String userName, String password) throws Exception {
		BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserName", null, null, 50);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserName"), userName);
		BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_Password"), password);
		Thread.sleep(100);
		BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButton"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);

		if (BaseUI.elementAppears(Locator.lookupOptionalElement("loginPage_NucaptchaIsPresent"))) {
			BaseUI.log_Status("Login with AARPRedesign Bypass URL");
			ClientDataRetrieval.set_secondary_AARP_Bpass_URL();
			Browser.navigateTo(ClientDataRetrieval.secondary_AARP_Bpass_URL);

			ClientDataRetrieval.set_SecondaryUser_AARP_ByPass_31000Points("chrome");

			BaseUI.waitForElementToBeDisplayed("loginPage_easyDeals_UserNameBpass", null, null, 30);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_UserNameBpass"), ClientDataRetrieval.secondaryUser_AARP_Bpass_31000Points);
			BaseUI.enterText(Locator.lookupRequiredElement("loginPage_easyDeals_PasswordBpass"), ClientDataRetrieval.secondaryPassword_AARP_Bpass_31000Points);
			Thread.sleep(100);
			BaseUI.click(Locator.lookupRequiredElement("loginPage_easyDeals_SignOnButtonBpass"));
			BaseUI.waitForElementToNOTBeDisplayed("loginPage_easyDeals_SignOnButtonBpass", null, null, 10);
			Thread.sleep(2000);
		}
		clickRemindMeLaterLink_RedesignHomePage();
	}

	public static void clickRemindMeLaterLink_RedesignHomePage() throws Exception {
		BaseUI.waitForElementToBeDisplayed_ButDontFail("nav_Message_RemindMeLater_Link", null, null, 5);
		if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_RemindMeLater_Link"))) {
			ArrayList<WebElement> remindMeLaterLinkList = Locator.lookup_multipleElements("nav_Message_RemindMeLater_Link",
					null, null);
			int i = 0;
			//remindMeLaterLinkList is pulling only 2 or 3 elements at a time,
			//even when we have 8-10 remind me later popups.
			while (true) {
				if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_RemindMeLater_Link"))) {
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_RemindMeLater_Link"));
					Thread.sleep(1000);
					i++;
				} else {
					break;
				}
			}
		}
		if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_CloseBtn"))) {
			ArrayList<WebElement> winnerModalCloseBtnList = Locator.lookup_multipleElements("nav_Message_CloseBtn",
					null, null);
			for (int i = 0; i < winnerModalCloseBtnList.size(); i++) {
				if (BaseUI.elementAppears(Locator.lookupOptionalElement("nav_Message_CloseBtn"))) {
					BaseUI.click(Locator.lookupRequiredElement("nav_Message_CloseBtn"));
					Thread.sleep(1000);
				} else {
					break;
				}
			}
		}
	}
}// End of Class