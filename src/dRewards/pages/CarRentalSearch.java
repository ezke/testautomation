package dRewards.pages;

import java.util.ArrayList;
import java.util.List;

import dRewards.data.ClientDataRetrieval;
import org.openqa.selenium.WebElement;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.pageControls.DatePicker;
import utils.BaseUI;
import utils.Locator;

public class CarRentalSearch {
	
	static String[] carClassOptions = { "No preference", "Compact", "Economy", "Intermediate", "Standard", "Full Size",
			"Luxury", "Premium", "Convertible", "Intermediate SUV", "Standard SUV", "Full Size SUV", "Premium SUV",
			"Crossover", "Mini Van", "Passenger Van", "Full Size Pick Up" };

	static String defaultPickUpLocation;


	// Verify or validate car rental subheader
	public static void verify_CarRentalPage_Subheader() throws Exception {

		// Navigation.navigate_CarRental_Page();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_MenuTab_Text"));
		BaseUI.verifyElementHasExpectedText("carRental_HeaderText", "Find Car Rentals. Reserve Now, Pay Later!");
		BaseUI.verify_true_AndLog((Locator.lookupRequiredElement("carRental_HeaderText").isDisplayed()),
				"Find Car Rentals. Reserve Now, Pay Later! text is displayed",
				"Find Car Rentals. Reserve Now, Pay Later! is not displayed");
	}

	// Verify or validate default pick up option is selected
	public static void verify_DefaultLocation_SelectedFor_PickUp() throws Exception {

		// Navigation.navigate_CarRental_Page();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_MenuTab_Text"));
		defaultPickUpLocation = Locator.lookupRequiredElement("carRental_PickUp_Default").getText();

		BaseUI.baseStringCompare("carRental_PickUp_Default", "At an airport", defaultPickUpLocation);
	}

	// Verify or validate default return option is selected
	public static void verify_Default_ReturnCarLocation_Selected() throws Exception {

		// Navigation.navigate_CarRental_Page();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_MenuTab_Text"));
		BaseUI.verify_true_AndLog((Locator.lookupRequiredElement("carRental_SamePickUpLocation_RadioBtn").isSelected()),
				"Same Pick-up Location is selected by default", "Same Pick-up Location is selected by default");
	}

	// Enter cityname or code when airport location is selected for pick up
	public static void enter_CityName_AirportCode(String airportCode) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_Enter_PickUpCity_OrAirportCode"), airportCode);
		Thread.sleep(200);
        BaseUI.waitForElementToBeDisplayed("carRental_SelectFirstOption_SearchResult", airportCode, null, 30);
		click_FirstOption_SearchResult(airportCode);
	}

	// Select the 1st option in the resulting search
	public static void click_FirstOption_SearchResult(String airportCode) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_SelectFirstOption_SearchResult", airportCode, null));
		Thread.sleep(500);
	}

	// Click Return location
	public static void click_CarRental_ReturnLocation(String ReturnLocation) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement(ReturnLocation));
		Thread.sleep(200);

	}

	// Select pick up location option
	public static void select_PickUpLocationOption(String returnLocationOption) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_PickUp_Dropdown"), returnLocationOption);
		Thread.sleep(200);
	}

	// Select pick up date
	public static void select_PickUpDate(String pickUpDate) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_PickUpDate"));
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(pickUpDate);
	}

	// Select return date
	public static void select_ReturnDate(String returnDate) throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_ReturnDate"));
		DatePicker datePicker = ClientDataRetrieval.getDatePicker();
		datePicker.selectTheDate(returnDate);
	}

	// Select car class from dropdown option
	public static void choose_CarClassOption(String carClass) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_CarClass"), carClass);
		Thread.sleep(200);
	}

	// Select pick up time
	public static void select_PickUpTime(String pickUpTime) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_PickUpTime_Dropdown"), pickUpTime);
		Thread.sleep(200);
	}

	// Select return time
	public static void select_ReturnTime(String returnTime) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_ReturnTime_Dropdown"), returnTime);
		Thread.sleep(200);
	}

	//Car class dropdown list validation
	public static void verify_CarClass_Dropdown() throws Exception {

		List<WebElement> allCarClassDropdown_Options = Locator
				.lookup_multipleElements("carRental_CarClass_DropdownList", null, null);
		List<String> carClass_DropdownList = new ArrayList<String>();

		for (WebElement dropdownOption : allCarClassDropdown_Options) {
			carClass_DropdownList.add(BaseUI.get_NonVisible_TextFromField(dropdownOption).trim());
		}

		BaseUI.verify_true_AndLog(carClass_DropdownList.size() == carClassOptions.length,
				"Dropdown matched expected values in size.", "Dropdown did not match expected values in size.");

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_CarClass_DropdownList"));

		for (String carOption : carClassOptions) {
			BaseUI.verify_true_AndLog(carClass_DropdownList.contains(carOption),
					"Car Option " + carOption + " found in list.", "Car Option " + carOption + " NOT found in list.");
		}


	}

	public static void select_PickUpLocation(String pickUpOption) throws Exception {

		List<WebElement> allPickUpLocation_Displayed = Locator.lookup_multipleElements("carRental_PickUp_Dropdown",
				pickUpOption, null);
		int numberOfCarClass = allPickUpLocation_Displayed.size();
		System.out.println(numberOfCarClass);

		for (WebElement carPickUpLocation : allPickUpLocation_Displayed) {
			System.out.println(carPickUpLocation.getText());
			if (carPickUpLocation.getText().equals(pickUpOption)) {
				BaseUI.click(carPickUpLocation);
				Thread.sleep(2000);
				break;
			}
		}
	}

	// Click Search Car Rental button
	public static void click_SearchCarRental_Button() throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_SearchCarRental_Btn"));
		BaseUI.waitForElementToBeDisplayed("carRental_ModifySearchResults_HeaderMessage", null, null, 50);
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(2000);
	}

	// enter pickUp street address when Near an address option is selected for
	// PickUp
	public static void enterPickUpAddress(String valueToEnter) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_PickUpAddress"), valueToEnter);
		Thread.sleep(500);
	}

	// enter pickUp city when Near an address option is selected for PickUp
	public static void enterPickUpCity(String valueToEnter) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_PickUpCity"), valueToEnter);
		Thread.sleep(500);
	}

	// enter pickUp zipcode when Near an address option is selected for PickUp
	public static void enterPickUpZip(String valueToEnter) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_PickUpZipcode"), valueToEnter);
		Thread.sleep(500);
	}

	// enter return city when Near an address option is selected for return
	// location
	public static void enterReturnCity(String valueToEnter) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_ReturnCity"), valueToEnter);
		Thread.sleep(500);
	}

	// enter return zipcode when Near an address option is selected for return
	// location
	public static void enterReturnZip(String valueToEnter) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_ReturnZipcode"), valueToEnter);
		Thread.sleep(500);
	}

	// enter return street address when Near an address option is selected for
	// return location
	public static void enterReturnAddress(String valueToEnter) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRental_ReturnAddress"), valueToEnter);
		Thread.sleep(500);
	}

	// select pickup state when Near an address option is selected for pickup
	// location
	public static void select_PickUpState(String pickUpState) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_PickUpState"), pickUpState);
		Thread.sleep(200);
	}

	// enter return state when Near an address option is selected for return
	// location
	public static void select_ReturnState(String returnState) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_ReturnState"), returnState);
		Thread.sleep(200);
	}

	// Only airport locations checkbox
	public static void click_OnlyAirportocations_PickUp() throws Exception {

		BaseUI.click(Locator.lookupRequiredElement("carRental_PickUpAddress_OnlyAirportLocation_Checkbox"));
		Thread.sleep(500);
	}

	public static void select_ReturnLocationOption(String returnLocationOption) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRental_ReturnLocation"), returnLocationOption);
		Thread.sleep(200);
	}

	// Enter pick up address when Near an address option selected
	public static void enter_PickUpLocationAddress(String pickUpAddress, String pickUpCity, String pickUpZip,
			String pickUpState) throws Exception {

		CarRentalSearch.enterPickUpAddress(pickUpAddress);
		CarRentalSearch.enterPickUpCity(pickUpCity);
		CarRentalSearch.enterPickUpZip(pickUpZip);
		CarRentalSearch.select_PickUpState(pickUpState);
		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("carRental_PickUpCountry"), "United States");

	}

	// Enter return address when Near an address option selected
	public static void enter_returnLocationAddress(String returnAddress, String returnCity, String returnZip,
			String returnState) throws Exception {

		CarRentalSearch.enterReturnAddress(returnAddress);
		CarRentalSearch.enterReturnCity(returnCity);
		CarRentalSearch.enterReturnZip(returnZip);
		CarRentalSearch.select_ReturnState(returnState);
		BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("carRental_ReturnCountry"), "United States");

	}
	
	
	public static void carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(CarRentalSearchCriteria searchInfo)
			throws Exception {
		carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchInfo.airportCode_CityName,
				searchInfo.pickUpDateDisplayed, searchInfo.returnDateDisplayed, searchInfo.carClassOptionDisplayed,
				searchInfo.pickUpTimeDisplayed, searchInfo.returnTimeDisplayed);

	}

	// Navigate to Car Rental page, make car search with
	// airport pickup and cityName/airport code search criteria and
	// Verify the selected details on search results page
	public static void carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(String airportORCity_Code,
			String pickUpDate, String returnDate, String carClassOption, String pickUpTime, String returnTime)
			throws Exception {

		Navigation.navigate_CarRental_Page();
		CarRentalSearch.enter_CityName_AirportCode(airportORCity_Code);
		enter_SearchCriteria (pickUpDate, returnDate, carClassOption, pickUpTime, returnTime);
		String cityName_AirportCode = CarRentalSearchCriteria.storeCarRentalSearchCriteria().cityNameSelected;  
		
		CarRentalSearch.click_SearchCarRental_Button();

		if(BaseUI.elementAppears(Locator.lookupOptionalElement("carRental_ModifySearchResults_NoSearchResultMessage"))) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ModifySearchResults_NoSearchResultMessage"));
			BaseUI.verifyElementHasExpectedText("carRental_ModifySearchResults_NoSearchResultMessage", 
					"Sorry, there is no available vehicle at this time, please try again later.");
			
			BaseUI.log_AndFail("Sorry, there is no available vehicle at this time, please try again later.");
		} else {
			CarRental_ModifySearchResults.verifyCarRentalSearchResults_MatchSearchCriteria(cityName_AirportCode,
					carClassOption, pickUpDate, returnDate, pickUpTime, returnTime);
		}
	}

	public static void enter_SearchCriteria (String pickUpDate, String returnDate,
			String carClassOption, String pickUpTime, String returnTime) throws Exception {
	
		CarRentalSearch.select_PickUpDate(pickUpDate);
		CarRentalSearch.select_ReturnDate(returnDate);
		CarRentalSearch.select_PickUpTime(pickUpTime);
		CarRentalSearch.select_ReturnTime(returnTime);
		CarRentalSearch.choose_CarClassOption(carClassOption);
	}
	
	
	public static void carRentalSearchCriteria_NearAddress_Return_SamePickUpLocation(CarRentalSearchCriteria searchDetails) throws Exception {
		
		carRentalSearchCriteria_NearAddress_Return_SamePickUpLocation(searchDetails.pickUpOption,
				searchDetails.pickUpAddressOption, searchDetails.pickUpCityOption, searchDetails.pickUpZipOption, 
				searchDetails.pickUpStateOption, searchDetails.pickUpDateDisplayed,
				searchDetails.pickUpTimeDisplayed, searchDetails.returnDateDisplayed, searchDetails.returnTimeDisplayed, 
				searchDetails.carClassOptionDisplayed);
		
	}

	/*
	 * Choose Near an address option for both pickup and same pick up address
	 * option for return location. And verify the search criteria on search
	 * result page
	 * 
	 */
	public static void carRentalSearchCriteria_NearAddress_Return_SamePickUpLocation(String pickUpOption,
			String pickUpAddress, String pickUpCity, String pickUpZip, String pickUpState, String pickUpDate,
			String pickUpTime, String returnDate, String returnTime, String carClassOption) throws Exception {

		Navigation.navigate_CarRental_Page();
		
		CarRentalSearch.select_PickUpLocationOption(pickUpOption);
		CarRentalSearch.enter_PickUpLocationAddress(pickUpAddress, pickUpCity, pickUpZip, pickUpState);
		enter_SearchCriteria(pickUpDate, returnDate, carClassOption, pickUpTime, returnTime);
		
		String pickUpLocationAddress = pickUpAddress + ", " + pickUpCity + ", "  + "FL US" + " " + pickUpZip;

		CarRentalSearch.click_SearchCarRental_Button();
		BaseUI.verifyElementAppears(
				Locator.lookupRequiredElement("carRental_SearchResults_WithDifferentLocation_VerifyHeaderText"));
		BaseUI.verifyElementHasExpectedText("carRental_SearchResults_WithDifferentLocation_VerifyHeaderText",
				("These are the closest locations to " + pickUpAddress + ", " + pickUpCity + ", " + "FL US" + " "
						+ pickUpZip));

		BaseUI.click(Locator.lookupRequiredElement("carRental_SearchResults_WithDifferentLocation_FirstOption_RadioBtn"));
		Thread.sleep(200);

		CarRental_ModifySearchResults.click_Continue_Btn();

		CarRental_ModifySearchResults.verifyCarRentalSearchResults_MatchSearchCriteria(pickUpLocationAddress, carClassOption, 
				 pickUpDate, returnDate, pickUpTime, returnTime);

	}
	
	
	//
	public static void carRentalSearchCriteria_PickUp_NearAddress_Return_DifferentPickUpLocation_NearAddress(CarRentalSearchCriteria searchDetails) throws Exception {
		
		carRentalSearchCriteria_PickUp_NearAddress_Return_DifferentPickUpLocation_NearAddress(searchDetails.pickUpOption,
				searchDetails.pickUpAddressOption, searchDetails.pickUpCityOption, searchDetails.pickUpZipOption, searchDetails.pickUpStateOption, 
				searchDetails.pickUpDateDisplayed, searchDetails.pickUpTimeDisplayed, searchDetails.returnDateDisplayed, searchDetails.returnTimeDisplayed, 
				searchDetails.carClassOptionDisplayed, searchDetails.returnOption, searchDetails.returnAddressOption,
				searchDetails.returnCityOption, searchDetails.returnZipOption, searchDetails.returnStateOption);
	}

	/*
	 * Choose Near an address option for both pickup and return location. And
	 * verify the search criteria on search result page
	 * 
	 */

	public static void carRentalSearchCriteria_PickUp_NearAddress_Return_DifferentPickUpLocation_NearAddress(String pickUpOption,
			String pickUpAddress, String pickUpCity, String pickUpZip, String pickUpState, String pickUpDate,
			String pickUpTime, String returnDate, String returnTime, String carClassOption, String returnOption,
			String returnAddress, String returnCity, String returnZip, String returnState) throws Exception {

		Navigation.navigate_CarRental_Page();
		CarRentalSearch.select_PickUpLocationOption(pickUpOption);
		CarRentalSearch.enter_PickUpLocationAddress(pickUpAddress, pickUpCity, pickUpZip, pickUpState);
		CarRentalSearch.click_CarRental_ReturnLocation("carRental_DifferentPickUpLocation_RadioBtn");
		CarRentalSearch.select_ReturnLocationOption(returnOption);
		CarRentalSearch.enter_returnLocationAddress(returnAddress, returnCity, returnZip, returnState);
		enter_SearchCriteria(pickUpDate, returnDate, carClassOption, pickUpTime, returnTime);
		String pickUpLocationAddress = pickUpAddress + ", " + pickUpCity + ", "  + "FL US" + " " + pickUpZip;

		CarRentalSearch.click_SearchCarRental_Button();
		BaseUI.verifyElementAppears(
				Locator.lookupRequiredElement("carRental_SearchResults_WithDifferentLocation_VerifyHeaderText"));
		BaseUI.verifyElementHasExpectedText("carRental_SearchResults_WithDifferentLocation_VerifyHeaderText",
				("These are the closest locations to " + pickUpAddress + ", " + pickUpCity + ", " + "FL US" + " "
						+ pickUpZip));

		CarRental_ModifySearchResults.click_FirstOption_SearchResult_RadioBtn(
				"carRental_SearchResults_WithDifferentLocation_FirstOption_RadioBtn");

		CarRental_ModifySearchResults.click_Continue_Btn();

		CarRental_ModifySearchResults.click_FirstOption_SearchResult_RadioBtn(
				"carRental_SearchResults_WithDifferentLocation_FirstOption_RadioBtn1");

		CarRental_ModifySearchResults.click_Continue_Btn();

		CarRental_ModifySearchResults.verifyCarRentalSearchResults_MatchSearchCriteria(pickUpLocationAddress,
				carClassOption, pickUpDate, returnDate, pickUpTime, returnTime);
	}
}
