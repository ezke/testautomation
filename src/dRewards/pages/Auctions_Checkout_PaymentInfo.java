package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Auctions_Checkout_PaymentInfo {

	public static void verify_AuctionsInfo_DualCurrency(String auctionName, String auctionBid, String expectedEndTime,
			Double usdAmount) throws Exception {

		auctionBid = auctionBid.split("\\.")[1].length() == 1 ? auctionBid + "0" : auctionBid;

		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARP)
				||ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			auctionBid = auctionBid.split("\\.")[0];
		}
		BaseUI.verifyElementHasExpectedText("auctShip_AuctionDescriptionName", auctionName);
		BaseUI.verifyElementHasExpectedText("auctShip_winningBid",
				auctionBid + " " + ClientDataRetrieval.client_Currency);
		BaseUI.verifyElementHasExpectedText("auctPymtInfo_PaymentDue",
				"Payment Due: $" + BaseUI.convertDouble_ToString_ForCurrency(usdAmount)+ " USD");
		String auctInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctShip_auctionInfo"));
		String endTime = auctInfo.split("\\n")[3];
		String claimDate = auctInfo.split("\\n")[4];
		expectedEndTime = "Auction End Date: " + expectedEndTime;
		BaseUI.baseStringCompare("Auction End Time", expectedEndTime, endTime);
		verify_ClaimByDate(endTime, claimDate);
	}
	
	public static void verify_ClaimByDate(String auctionEndDateString, String actualClaimDate) throws Exception {
		auctionEndDateString = auctionEndDateString.replace("Auction End Date: ", "");
		String claimDate = Auctions.return_expectedClaimDate(auctionEndDateString);

		BaseUI.baseStringCompare("Claim Date", actualClaimDate, "Claim By: " + claimDate);
	}
	public static void click_AddNewPayment_Radio() throws Exception {
		if (BaseUI.elementExists("checkout_AddNewPayment_Radio", null, null)) {
			BaseUI.click(Locator.lookupRequiredElement("checkout_AddNewPayment_Radio"));
			Thread.sleep(300);
		}
	}
	
	public static void add_NewPaymentOption(String creditCardType, String creditCardNumber, String expirationMonth,
			String expirationYear, String cvvCode, String cardholderName, String address1, String country, String city,
			String State, String zip) throws Exception {

		if (BaseUI.elementExists("checkout_AddNewPayment_Radio", null, null)) {
			click_AddNewPayment_Radio();
		}
		enter_requiredNewPaymentFields(creditCardType, creditCardNumber, expirationMonth, expirationYear, cvvCode,
				cardholderName, address1, country, city, State, zip);
	}
	
	public static void enter_requiredNewPaymentFields(String creditCardType, String creditCardNumber,
			String expirationMonth, String expirationYear, String cvvCode, String cardholderName, String address1,
			String country, String city, String State, String zip) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_CreditCardType"), creditCardType);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CreditCardNumber"), creditCardNumber);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationMonth"), expirationMonth);
		BaseUI.click(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_ExpirationYear"), expirationYear);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CVV"), cvvCode);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_CardholderName"), cardholderName);
		
	    BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_Address1"), address1);
	    BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_Country"), country);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_City"), city);
		// make sure to pass in 2 letter state code (example: WI)
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_State"), State);	
		BaseUI.click_js(Locator.lookupRequiredElement("checkout_newPayment_Zip_label"));
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("checkout_newPayment_Zip"), zip);		
	}
	
	public static void check_SaveBillingAddressForLater() throws Exception {
		BaseUI.checkCheckbox("checkout_newPayment_SaveForLater_Checkbox");
		Thread.sleep(200);
	}
	
	public static void click_ReviewPayment_Button() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("auctOrderRev_ReviewYourOrder_Button"));
		//no able to add wait logic as been call  for different method 
		Thread.sleep(2000);
	}
	
	public static void verify_ReviewPayment_Button_Appears(){
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("auctOrderRev_ReviewYourOrder_Button"));
		BaseUI.verifyElementEnabled(Locator.lookupRequiredElement("auctOrderRev_ReviewYourOrder_Button"));
	}
}
