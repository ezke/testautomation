package dRewards.pages;

import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class DailyDeals {

	public static HashMap<String, String> return_DailyDealInfo() {
		HashMap<String, String> dailyDealInfo = new HashMap<String, String>();
		dailyDealInfo.put("dailyDealName", BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_ProductTitle")));
		dailyDealInfo.put("retailPrice", BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_RetailPrice")));
		dailyDealInfo.put("youPay",
				BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_YouPay")).replace("$", "")
						.replace(",", "").replace("(", "").replace(")", ""));

		dailyDealInfo.put("points",
				BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_Points")).replace("-", "").replace(",", ""));

		dailyDealInfo.put("savings", BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_Savings"))
				.replace("% Savings", "").replace("(", "").replace(")", "")
				.replace("\u0028 ", "").replace(" \u0029", "").replaceAll("Savings", "").trim());
		dailyDealInfo.put("dailyDeal_EndTime", BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("dailyDeals_EndTime"), "innerText")
				.substring(0, 3).replace(":", ""));

		return dailyDealInfo;
	}

	public static TableData return_DailyDeal_TableData() {
		HashMap<String, String> tableMappings = new HashMap<String, String>();
		tableMappings.put("dailyDealName", "//p[@class='mainProdTitle']|//h2[@class='mainProdTitle']");
		tableMappings.put("retailPrice", "//dd[@class='retail']|//td[@class='retail']");
		tableMappings.put("points", "//dt[@class='incentives']|//span[@class='incentives']");
		tableMappings.put("youPay", "//dd[@class='price']|//td[@class='price']");
		tableMappings.put("savings",
				"//div[@id='dailydeal']//div[@class='price-matrix']//span[not(@class='incentives')]");

		TableData dailyDealTable = new TableData();

		dailyDealTable.data = BaseUI.tableExtractor("dailyDeals_Row", tableMappings);
		dailyDealTable.remove_Character("$", "youPay");
		dailyDealTable.remove_Character(",", "youPay");
		dailyDealTable.remove_Character("-", "points");
		dailyDealTable.remove_Character(",", "points");
		dailyDealTable.remove_Character("% Savings", "savings");
		dailyDealTable.remove_Character("(", "savings");
		dailyDealTable.remove_Character(")", "savings");
		dailyDealTable.remove_Character("$", "retailPrice");
		dailyDealTable.remove_Character(",", "retailPrice");

		return dailyDealTable;
	}

	public static void verify_DailyDealInfo(HashMap<String, String> originalData, HashMap<String, String> dailyDealData)
			throws Exception {

		 BaseUI.baseStringPartialCompare("dailyDealName", originalData.get("dailyDealName"), dailyDealData.get("dailyDealName"));

		if (ClientDataRetrieval.isLegacyClient()) {
			BaseUI.baseStringCompare("retailPrice", originalData.get("retailPrice"), dailyDealData.get("retailPrice"));

			BaseUI.baseStringCompare("youPay", originalData.get("youPay"), dailyDealData.get("youPay"));

			BaseUI.baseStringCompare("points", originalData.get("points"), dailyDealData.get("points"));
		} 
		BaseUI.baseStringCompare("savings", originalData.get("savings"), dailyDealData.get("savings"));
		
		BaseUI.baseStringPartialCompare("dailyDeal_EndTime", originalData.get("dailyDeal_EndTime"), dailyDealData.get("dailyDeal_EndTime"));

	}

	public static void verify_DailyDealInfo(TableData originalData) {

		TableData dailyDeal_Data = return_DailyDeal_TableData();

		BaseUI.verify_TableColumns_Match("dailyDealName", originalData.data, dailyDeal_Data.data);

		BaseUI.verify_TableColumns_Match("retailPrice", originalData.data, dailyDeal_Data.data);

		BaseUI.verify_TableColumns_Match("points", originalData.data, dailyDeal_Data.data);

		BaseUI.verify_TableColumns_Match("youPay", originalData.data, dailyDeal_Data.data);

		BaseUI.verify_TableColumns_Match("savings", originalData.data, dailyDeal_Data.data);

	}

	public static void verify_Image_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_Image"));
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("dailyDeals_Image"), "src", ".jpg");
	}

	public static void verify_ViewLargerImage_Link_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Link"));
		BaseUI.verifyElementHasExpectedPartialText("dailyDeals_ViewLargerImage_Link", "View Larger Image");
	}

	public static void click_ViewLargerImage_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Link"));
		Thread.sleep(1000);
	}

	public static void close_ViewLargerImage_Popup() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Popup_Close"));
		Thread.sleep(500);
	}

	public static void verify_ViewLargerImage_Popup(String dailDealName) throws Exception {
		click_ViewLargerImage_Link();
		String dailyDealTitle = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Popup_Title"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Popup"));
		BaseUI.baseStringCompare("Daily Deal Name", dailDealName, dailyDealTitle);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Popup_Image"));
		BaseUI.verifyElementHasExpectedPartialAttributeValue(
				Locator.lookupRequiredElement("dailyDeals_ViewLargerImage_Popup_Image"), "src", ".jpg");

	}

	public static void click_AddToShoppingCart() throws Exception {
		if (BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_AllDropdowns"))) {
			ArrayList<WebElement> dropdown_List = new ArrayList<WebElement>();
			dropdown_List.addAll(Locator.lookup_multipleElements("dailyDeals_AllDropdowns", null, null));
			for (Integer i = 1; i <= dropdown_List.size(); i++) {
				// For each dropdown that was found we'll select the first
				// option in that dropdown.
				select_firstDropdownOption(
						Locator.lookupRequiredElement("dailyDeals_Dropdown_SelectByIndex", i.toString(), null));
			}
		}

		WebElement addShoppingCartBtn = Locator.lookupRequiredElement("dailyDeals_AddToShoppingCart_Button");
		BaseUI.scroll_to_element(addShoppingCartBtn);
		BaseUI.click(addShoppingCartBtn);
		BaseUI.waitForElementToBeDisplayed("dailyDealAddToCart_WaitElement", null, null, 20);
		Thread.sleep(500);

//		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
//			if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeal_reCaptchaVerificationModal_Text"))){
//				BaseUI.click(Locator.lookupRequiredElement("dailyDeal_reCaptchaVerificationModal_RecaptchaTxtbox"));
//				Thread.sleep(200);
//				BaseUI.click(Locator.lookupRequiredElement("dailyDeal_reCaptchaVerificationModal_RecaptchaContinueBtn"));
//				Thread.sleep(500);
//			}
//		}
	}

	public static void click_More_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("dailyDeals_Description_More_Link"));
		Thread.sleep(500);
	}

	public static void verify_LessLink_Appears() throws Exception {
		click_More_Link();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_Description_Less_Link"));
	}

	public static void select_firstDropdownOption(WebElement dropdown) throws Exception {

		Select dropdownSelect = new Select(dropdown);
		if (dropdownSelect.getFirstSelectedOption().getText().equals("dailyDeals_AllDropdowns")) {
			dropdownSelect.selectByIndex(2);
		} else {
			dropdownSelect.selectByIndex(1);
		}
		Thread.sleep(1000);
		// BaseUI.selectValueFromDropDownByIndex(Locator.lookupRequiredElement("dailyDeals_Dropdown_SelectByIndex",
		// firstIndex, null), Integer.valueOf(firstIndex));
		// Thread.sleep(200);
	}

	public static void verify_Available_ActiveStatus() {
		String dealTimeLeft = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_AvailableTab_DealTimeLeft")).trim();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_AvailableTab_Active"));
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("dailyDeals_AvailableTab_Text",
					"This item is still AVAILABLE."+"\n"+"Hurry, quantities are limited!");
			BaseUI.verifyElementHasExpectedPartialText("dailyDeals_AvailableTab_DealCounter_Text", 
					"This deal expires in:"+"\n"+BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_EndTime")));
		} else if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			BaseUI.verifyElementHasExpectedText("dailyDeals_AvailableTab_Text",
					"This item is still AVAILABLE. Hurry, quantities are limited!");
			BaseUI.verify_true_AndLog(dealTimeLeft.matches("^(\\d{2}:\\d{2}:\\d{2})$"), "Matched pattern 00:00:00",
					"Text did not look right, seeing value " + dealTimeLeft);
		} else {
			BaseUI.verifyElementHasExpectedText("dailyDeals_AvailableTab_Text",
					"This item is still AVAILABLE. Hurry, quantities are limited!");
			BaseUI.verifyElementHasExpectedText("dailyDeals_AvailableTab_DealCounter_Text", "This Deal Expires In:");
			BaseUI.verify_true_AndLog(dealTimeLeft.matches("^(\\d{2}:\\d{2}:\\d{2})$"), "Matched pattern 00:00:00",
					"Text did not look right, seeing value " + dealTimeLeft);
		}
	}

	public static void verify_InCarts_ActiveStatus() {
		String inCartExpectedText = "All items are IN CARTS. One might become available soon, so stick around!";
		String dealTimeLeft = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_InCartTab_DealTimeLeft")).trim();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_InCartTab_Active"));
		if (ClientDataRetrieval.isRedesignClient()) {
			String incartText = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_InCartTab_Text"));
			BaseUI.baseStringCompare("DailyDeals_InCart Text", 
					"All items are currently IN CARTS."+"\n"+"One might become available soon, so stick around!",
					incartText);
			BaseUI.verifyElementHasExpectedText("dailyDeals_InCartTab_DealCounter_Text", 
					"This deal expires in:"+"\n"+BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_InCartTab_DealTimeLeft")));
		} else {
			if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){ 
				String incartText = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_InCartTab_Text"));
			    incartText = incartText.replaceAll("[\r\n]+", " ");
			    BaseUI.baseStringCompare("DailyDeals_InCart Text", inCartExpectedText, incartText);
			    } else {
			    	BaseUI.verifyElementHasExpectedText("dailyDeals_InCartTab_Text",
					"All items are IN CARTS. One might become available soon, so stick around!");
			    }
			BaseUI.verifyElementHasExpectedText("dailyDeals_InCartTab_DealCounter_Text", "This Deal Expires In:");
			BaseUI.verify_true_AndLog(dealTimeLeft.matches("^(\\d{2}:\\d{2}:\\d{2})$"),
					"Matched pattern 00:00:00", "Text did not look right, seeing value " + dealTimeLeft);
		}
	}
	
	public static void verify_SoldOut_ActiveStatus() {

		String soldOutExpectedText = "This item is SOLD OUT. Check back at 3 PM ET. for the next Daily Deal.";
		String dealTimeLeft = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_SoldOutTab_DealTimeLeft"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_SoldOutTab_Active"));
		if (ClientDataRetrieval.isRedesignClient()) {
			String soldOutText = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_SoldOutTab_Text"))+" "
					+BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_SoldOutTab_Text2"));		
			BaseUI.baseStringCompare("DailyDeals_Soldout Text", "The previous Daily Deal has SOLD OUT", soldOutText);
			BaseUI.verifyElementHasExpectedText("dailyDeals_SoldOutTab_DealCounter_Text", "The next Daily Deal starts In:");
		} else {
			if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)){
				String soldOutText = BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_SoldOutTab_Text"));
				soldOutText = soldOutText.replaceAll("[\r\n]+", " ");
				BaseUI.baseStringCompare("DailyDeals_Soldout Text", soldOutExpectedText, soldOutText);
			}else if(ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				BaseUI.verifyElementHasExpectedText("dailyDeals_SoldOutTab_Text",
						"This item is SOLD OUT. Check back at 1PM ET for the next Daily Deal.");
			}
			BaseUI.verifyElementHasExpectedText("dailyDeals_SoldOutTab_DealCounter_Text", "Next Deal Starts In:");
			BaseUI.verify_true_AndLog(dealTimeLeft.matches("^(\\d{2}:\\d{2}:\\d{2})$"),
					"Matched pattern 00:00:00", "Text did not look right, seeing value " + dealTimeLeft);
		}	
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_SoldOutTab_Image_SoldOut"));
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedText("dailyDeals_SoldOutTab_Image_SoldOut", "SOLD OUT");
		}
		
	}
	
	public static void verify_ShoppingCart_Expired_Message(){
		String expectedMessage = "We apologize; this item/price is no longer available. Please remove it to continue the checkout process.";
		//We apologize; this item/price is no longer available.  Please remove it to continue the checkout process.
		String actualMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Expire_ErrorMessage"));
		BaseUI.baseStringCompare("Shopping Cart Expire Message", expectedMessage, actualMessage);
	}
	
	
	public static ArrayList<String> return_DailyDeals_AllImagesDisplayed_OnSlides() throws Exception {

		ArrayList<WebElement> imagesOnSlidesList = Locator.lookup_multipleElements("dailyDeals_AllImages_OnSlides",
				null, null);
		ArrayList<String> slideImagesList = new ArrayList<String>();

		for (WebElement images : imagesOnSlidesList) {
			String menuStr = images.getText();
			slideImagesList.add(menuStr);
		}

		return slideImagesList;
	}
	
	// click right arrow of Daily Deals Images slide
	// For Redesign Clients
	public static void click_DailyDealsImagesSlides_ToFarRight() throws Exception {
		int count = 0;
		while (!BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_Images_RightNavigationArrow_Disabled"))) {
			click_DailyDealsImagesSlide_RightNavigationArrow();
			count++;
			if (count > 3) {
				break;
			}
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_AllImages_OnSlides_LastItem_Displayed"));
	}

	// click left arrow of Daily Deals Images slide
	// For Redesign Clients
	public static void click_DailyDealsImagesSlides_ToFarLeft() throws Exception {
		int count = 0;
		while (!BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_Images_RightNavigationArrow_Disabled"))) {
			click_DailyDealsImagesSlide_LeftNavigationArrow();
			count++;
			if (count > 3) {
				break;
			}
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_AllImages_OnSlides_FirstItem_Displayed"));
	}
	
	
	// click Daily Deals slide Images right navigation arrow
	// For Redesign Clients
	public static void click_DailyDealsImagesSlide_RightNavigationArrow() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("dailyDeals_Images_RightNavigationArrow"));
		Thread.sleep(500);
	}

	// click Daily Deals slide Images left navigation arrow
	// For Redesign Clients
	public static void click_DailyDealsImagesSlide_LeftNavigationArrow() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("dailyDeals_Images_LeftNavigationArrow"));
		Thread.sleep(500);
	}

	// click No Thanks link on Daily Deal's Re-captcha modal
	// For Redesign Clients
	public static void click_DailyDealsRecaptchModal_NoThanksLink() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("dailyDeals_AddToCart_PopUpModal_NoThanksHyperLink"));
		Thread.sleep(1000);
	}
}
