package dRewards.pages;

import dRewards.ClassObjects.CarRentalSearch_CheckoutDetails;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class CarRentalSearch_Checkout {

	static CarRentalSearch_CheckoutDetails checkoutDetails = new CarRentalSearch_CheckoutDetails();

	public static CarRentalSearch_CheckoutDetails retrieve_CarRentalPriceDetails() {
		CarRentalSearch_CheckoutDetails carRentalPriceDetails = new CarRentalSearch_CheckoutDetails();

		carRentalPriceDetails.avgBaseRate = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TotalPricePerDay"));
		carRentalPriceDetails.points_OrSmartDollars = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_Points_SmartDollars"))
				.replace("\u2013", "-").replace("- ", "");
		carRentalPriceDetails.rentalCharge = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_RentalCharge"));
		carRentalPriceDetails.taxes_OrFees = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees"));
		carRentalPriceDetails.totalWithTax = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TotalPriceWithTax"));

		return carRentalPriceDetails;
	}

	public static CarRentalSearch_CheckoutDetails retrieve_TaxesOrFeesDetails() {
		CarRentalSearch_CheckoutDetails taxesOrFeesDetails = new CarRentalSearch_CheckoutDetails();

		taxesOrFeesDetails.airportConcessionFee = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_AirportConcessionFee"));
		taxesOrFeesDetails.localTax = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_LocalTax"));
		taxesOrFeesDetails.energy_RecoveryFee = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_EnergyRecovFee"));
		taxesOrFeesDetails.stateSurcharge = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_StateSurcharge"));
		taxesOrFeesDetails.tire_BatteryFee = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_TireBatteryFee"));
		taxesOrFeesDetails.vehLicenseFee = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_VehicleLicenseFee"));
		taxesOrFeesDetails.taxes_OrFees = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees"));
		taxesOrFeesDetails.airportCityOtherSurcharge = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees_AirportCityOtherSurcharge"));

		return taxesOrFeesDetails;
	}

	public static void verify_AvisCarRentalCheckout_PriceDetails(String avgBaseRate, String points_OrSmartDollars,
			String youPay, String totalWithTax) throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TotalPricePerDay"));
		checkoutDetails = CarRentalSearch_Checkout.retrieve_CarRentalPriceDetails();

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementAppears(
					Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_Points_SmartDollars"));
		} else {
			String points_smartDollars = checkoutDetails.points_OrSmartDollars.replace("\u2013", "-")
					.replace("– ", "");
			BaseUI.verify_true_AndLog(Integer.parseInt(points_smartDollars) > 0.0,
					"Points or Smart Dollars " + points_smartDollars + "  is greater than 0 and numeric value",
					"Points or Smart Dollars " + points_smartDollars + "  is not greater than 0 and numeric value");
			BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_Points_SmartDollars", points_OrSmartDollars,
					points_smartDollars);
		}

		String avgBaseRate_PerDay = checkoutDetails.avgBaseRate.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(avgBaseRate_PerDay) > 0.0,
				"Avg Base Rate " + avgBaseRate_PerDay + "  is greater than 0 and numeric value",
				"Avg Base Rate " + avgBaseRate_PerDay + " is not greater than 0");
		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPricePerDay", avgBaseRate,
				avgBaseRate_PerDay);

		String youPayPrice = checkoutDetails.rentalCharge.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(youPayPrice) > 0,
				"Rental Charge " + youPayPrice + "  is greater than 0 and numeric value",
				"Rental Charge " + youPayPrice + "  is not greater than 0 and numeric value");
		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_RentalCharge", youPay, youPayPrice);

		String finalPrice = checkoutDetails.totalWithTax.replace("$", "").replaceAll("Approx USD ", "")
				.replace("(", "").replace(")", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(finalPrice) > 0,
				"Total with tax " + finalPrice + "  is greater than 0 and numeric value",
				"Total with tax " + finalPrice + "  is not greater than 0 and numeric value");
		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", totalWithTax, finalPrice);

		String taxesOrFees = checkoutDetails.taxes_OrFees.replace("+ ", "").replace("$", "");
		String FinalPriceAfterTaxes = String.valueOf(BaseUI
				.convertDouble_ToString_ForCurrency(Double.parseDouble(youPayPrice) + Double.parseDouble(taxesOrFees)));
		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", finalPrice,
				FinalPriceAfterTaxes);
		BaseUI.verify_true_AndLog((Double.parseDouble(FinalPriceAfterTaxes) == Double.parseDouble(finalPrice)),
				"Total with tax " + FinalPriceAfterTaxes + " match " + finalPrice,
				"Total with tax " + FinalPriceAfterTaxes + " does not match " + finalPrice);
	}

	public static void verify_CarRental_TaxesOrFees_Subcategory() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxesOrFees"));
		checkoutDetails = CarRentalSearch_Checkout.retrieve_TaxesOrFeesDetails();
		//  need to add airport other charge and change and total should be .10 and for airport consiensofee should be 12.71 and so on
		String airportCityOtherSurcharge = checkoutDetails.airportCityOtherSurcharge.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(airportCityOtherSurcharge) > 0.0,
				"Avg Base Rate " + airportCityOtherSurcharge + "  is greater than 0 and numeric value",
				"Avg Base Rate " + airportCityOtherSurcharge + " is not greater than 0");
		
		String airportConcessionFee = checkoutDetails.airportConcessionFee.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(airportConcessionFee) > 0.0,
				"Avg Base Rate " + airportConcessionFee + "  is greater than 0 and numeric value",
				"Avg Base Rate " + airportConcessionFee + " is not greater than 0");

		String localTax = checkoutDetails.localTax.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(localTax) > 0.0,
				"Points or Smart Dollars " + localTax + "  is greater than 0 and numeric value",
				"Points or Smart Dollars " + localTax + "  is not greater than 0 and numeric value");

		String energyRecoveryFee = checkoutDetails.energy_RecoveryFee.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(energyRecoveryFee) > 0.0,
				"Rental Charge " + energyRecoveryFee + "  is greater than 0 and numeric value",
				"Rental Charge " + energyRecoveryFee + "  is not greater than 0 and numeric value");

		String stateSurcharge = checkoutDetails.stateSurcharge.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(stateSurcharge) > 0.0,
				"State Surcharge " + stateSurcharge + "  is greater than 0 and numeric value",
				"State Surcharge " + stateSurcharge + "  is not greater than 0 and numeric value");

		String tireBatteryFee = checkoutDetails.tire_BatteryFee.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(tireBatteryFee) > 0.0,
				"Tire Battery Fee " + tireBatteryFee + "  is greater than 0 and numeric value",
				"Tire Battery Fee " + tireBatteryFee + "  is not greater than 0 and numeric value");

		String vehLicenseFee = checkoutDetails.vehLicenseFee.replace("$", "");
		BaseUI.verify_true_AndLog(Double.parseDouble(vehLicenseFee) >= 0.0,
				"VehLicense Fee " + vehLicenseFee + "  is greater than 0 and numeric value",
				"VehLicense Fee " + vehLicenseFee + "  is not greater than 0 and numeric value");

		String totalTaxes = checkoutDetails.taxes_OrFees.replace("$", "").replace("+ ", "");
		System.out.println("totalTaxes " + totalTaxes);

		Double taxes_FeesAdded = (Double.parseDouble(airportConcessionFee) + Double.parseDouble(localTax)
				+ Double.parseDouble(energyRecoveryFee) + Double.parseDouble(stateSurcharge)
				+ Double.parseDouble(tireBatteryFee) + Double.parseDouble(vehLicenseFee)+ Double.parseDouble(airportCityOtherSurcharge));

		// DecimalFormat f = new DecimalFormat("##.00");
		// String taxesOrFeesAdded = String.valueOf(f.format(taxes_FeesAdded));

		String taxesOrFeesAdded = String.valueOf(BaseUI.convertDouble_ToString_ForCurrency(taxes_FeesAdded));

		BaseUI.baseStringCompare("carRentalSearchResult_CheckoutPage_TotalPriceWithTax", totalTaxes, taxesOrFeesAdded);
		BaseUI.verify_true_AndLog((Double.parseDouble(taxesOrFeesAdded) == Double.parseDouble(totalTaxes)),
				"Total with tax " + taxesOrFeesAdded + " match " + totalTaxes,
				"Total with tax " + taxesOrFeesAdded + " does not match " + totalTaxes);
	}

	public static void clickTaxes_FeesLink() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_TaxAndFees_Link"));
		BaseUI.waitForElementToBeDisplayed("carRentalSearchResult_CheckoutPage_TaxAndFees_AirportConcFee", null, null);
		Thread.sleep(200);
	}

	public static void clickAdditionalOptionsPlus_ToExpand() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AdditionalOptions_Header"));
		BaseUI.waitForElementToBeDisplayed("carRentalSearchResult_CheckoutPage_AdditionalOptions_Note", null, null);
		Thread.sleep(150);
	}

	public static void clickCarRentalSearchResult_CheckoutPage_ReserveCarBtn() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_ReserveCarBtn"));
		BaseUI.click(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_ReserveCarBtn"));
		BaseUI.waitForElementToBeDisplayed("carRentalSearchResult_CheckoutPage_WaitForElementAfterReserveCar",
				null, null, 30);
		Thread.sleep(1500);
	}

	public static void verifyCarRentalSearchResult_CheckoutPage_ErrorMessaging() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FnameErrorMsg"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_LnameErrorMsg"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_EmailErrorMsg"));
	}

	public static void verifyCarRentalSearchResult_CheckoutPage_VerifyPoliciesLnk() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PoliciesLnk"), true,
				1000);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PrivacyPoliciesHeader"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_PrivacyPoliciesLinks"));
	}

	public static void verifyCarRentalSearchResult_CheckoutPage_VerifyFaqsLnk() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FaqsLnk"), true, 1000);
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FaqsPageHeader"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_FaqsPageLeftNavLinks"));
	}

	
	public static void EnterDriverDetails(String driverFirstName, String driverLastName, String driverEmail)
			throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_FirstName"),
				driverFirstName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_LastName"),
				driverLastName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_DriverInfo_Email"),
				driverEmail);
	}

	public static void EnterAirlineDetails(String airlineName, String airlineNum) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AirlineInfo_Name"),
				airlineName);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AirlineInfo_Number"),
				airlineNum);
	}
	
	public static void verifyCarRentalSearchResult_CheckoutPage_AirportPickLocation(String airportLocation) throws Exception {
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			airportLocation	= airportLocation.replaceAll("Avis", "").replaceAll("Budget", "").replaceFirst("\n ", "").trim();
			BaseUI.verifyElementHasExpectedPartialText("carRentalSearchResult_CheckoutPage_PickUpLocation", airportLocation);
		} else {
			BaseUI.verifyElementHasExpectedPartialText("carRentalSearchResult_CheckoutPage_PickUpLocation", airportLocation);
		}
	}
	
	public static void verifyCarRentalSearchResult_CheckoutPage_AirportReturnLocation(String airportLocation) throws Exception {
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			BaseUI.verifyElementHasExpectedPartialText("carRentalSearchResult_CheckoutPage_ReturnLocation", airportLocation
					.replaceAll("Avis", "").replaceAll("Budget", "").replaceFirst("\n ", "").trim());
		} else {
		    BaseUI.verifyElementHasExpectedPartialText("carRentalSearchResult_CheckoutPage_ReturnLocation", airportLocation);
		}
	}
}
