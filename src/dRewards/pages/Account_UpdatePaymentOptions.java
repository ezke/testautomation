package dRewards.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class Account_UpdatePaymentOptions {

	public static void delete_all_PaymentOptions() throws InterruptedException {

		if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
			||(ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))
			||(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))
			||(ClientDataRetrieval.client_Matches(client_Designation.AARP))) {

			if (!BaseUI.elementAppears(Locator.lookupOptionalElement("accnt_UpdatePay_DeleteButton"))) {

				 BaseUI.elementAppears(Locator.lookupOptionalElement("accnt_UpdatePaymentOptions_Msg"));

			} else {
				ArrayList<WebElement> delete_Buttons = Locator.lookup_multipleElements("accnt_UpdatePay_DeleteButton",
						null, null);

				for (int i = 0; i < delete_Buttons.size(); i++) {
					delete_PaymentOption();
				}
			}

		} else {

			ArrayList<WebElement> delete_Buttons = Locator.lookup_multipleElements("accnt_UpdatePay_DeleteButton", null,
					null);

			for (int i = 0; i < delete_Buttons.size(); i++) {
				delete_PaymentOption();
			}

		}

	}

	// This will just delete the first Payment Option it sees.
	public static void delete_PaymentOption() throws InterruptedException {

		BaseUI.click(Locator.lookupRequiredElement("accnt_UpdatePay_DeleteButton"));
		Thread.sleep(400);
		confirmDelete();

	}

	// Pass in the cardholder and it will target the delete button for that
	// cardholder and delete the payment option.
	public static void delete_PaymentOption_ByCardholder(String cardholder) throws InterruptedException {
		BaseUI.click(Locator.lookupRequiredElement("accnt_DeleteButton_ByPassedInCardholderName", cardholder, null));
		Thread.sleep(400);
		confirmDelete();
	}

	public static void confirmDelete() throws InterruptedException {
		BaseUI.click(Locator.lookupRequiredElement("accnt_UpdatePay_Delete_ConfirmYesButton"));
		Thread.sleep(500);
		BaseUI.click(Locator.lookupRequiredElement("accnt_UpdatePay_Delete_ConfirmPopup_XButton"));
		BaseUI.waitForElementToNOTBeDisplayed("accnt_UpdatePay_Delete_OverlayElement", null, null);
		Thread.sleep(1000);
	}

}
