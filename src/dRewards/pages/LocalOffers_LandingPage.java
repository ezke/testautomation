package dRewards.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class LocalOffers_LandingPage {
	
	public static String localOfferDealText;
	public static String localOfferAddress;
	public static String localOfferPoints;
	public static String pointsRedeemed;

	public static void verify_ClickingSeeLocalOfferButton_NavigatesTo_LocalOffersPage() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_LocalOffer_SeeLocalOffer_Button"));
		Navigation.click_SeeLocalOffer_Button();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("acct_SubHeader_Text"));
	}

	public static void verify_LocalOffer_SearchPageError() throws Exception {
		WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
		if(ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(addressTextBox);
		}
		addressTextBox.clear();
		BaseUI.tabThroughField(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"));
		WebElement cityTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(cityTextField, "");
		BaseUI.tabThroughField(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
		Navigation.click_SeeLocalOffer_SearchPageButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_SearchPage_CityTxtboxError"));
			BaseUI.verifyElementHasExpectedText("localOffer_SearchPage_CityTxtboxError", 
					"Your City is required.");
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_SearchPage_StateDropdownError"));
			BaseUI.verifyElementHasExpectedText("localOffer_SearchPage_StateDropdownError", 
					"Your State/Province is required.");
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_SearchPageError"));
			if(ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) 
					|| ClientDataRetrieval.client_Matches(client_Designation.Citi)
					|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)
					|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
					|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
				BaseUI.verifyElementHasExpectedText("localOffer_SearchPageError", 
						"Please Continue To Enter A City Or U.S. Zip Code/Canada Postal Code.");
			} else {
				BaseUI.verifyElementHasExpectedText("localOffer_SearchPageError", 
						"Please continue to enter a City or U.S. Zip Code/Canada Postal Code.");
			}
		}
	}

	public static void localOffer_SearchPage_WithCriteria(String state, String city, String category) throws Exception {
		WebElement zipTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox");
		if(ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(zipTextBox);
		}
		zipTextBox.clear();
		WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
		addressTextBox.clear();
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"), city);
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("homepage_LocalOffer_State_Dropdown"), state);
		if (ClientDataRetrieval.isRedesignClient()) {
			clickCategoryBtn(category);
		} else {
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("localOffer_SearchPage_OfferDropdownList"), category);
		}
	}

	public static void localOffer_SearchDiningPage_WithCriteria(String state, String city, String category,
			String address) throws Exception {
		WebElement zipTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox");
		if(ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(zipTextBox);
		}
		zipTextBox.clear();
		WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
		addressTextBox.clear();
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox"), address);
		Thread.sleep(100);
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"), city);
		Thread.sleep(100);
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("homepage_LocalOffer_State_Dropdown"), state);
		Thread.sleep(300);
		if (ClientDataRetrieval.isRedesignClient()) {
			clickCategoryBtn(category);
		} else {
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("localOffer_SearchPage_OfferDropdownList"), category);
		}
	}

	public static void localOffer_Search_WithZipcode(String zipCode, String category) throws Exception {
		WebElement zipTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox");
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.enterText_IntoInputBox(zipTextBox, zipCode);
			clickCategoryBtn(category);
		} else {
			zipTextBox.clear();
			WebElement addressTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_Address_Textbox");
			addressTextBox.clear();
		
			Actions actions = new Actions(Browser.driver);
			actions.moveToElement(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"));
			actions.click();
			actions.sendKeys(zipCode);
			actions.build().perform();
			
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("localOffer_SearchPage_OfferDropdownList"), category);
		}
	}

	public static void click_Canada_RadioButton() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchPage_Canada_RadioButton"));
		Thread.sleep(200);
	}

	public static void verify_availableOffer_TextLable() {
		String availableOffer = "Available Offer";
		String availableOffers = "Available Offers";
		String currentAvailableOffer;

		if (ClientDataRetrieval.isRedesignClient()) {
			currentAvailableOffer = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_AvailableOffer"))
					.split(" ")[1];
			BaseUI.baseStringCompare("localOffer_AvailableOffer", "Deals", currentAvailableOffer);
		} else {
			currentAvailableOffer = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_AvailableOffer"))
					.split("\\n")[0];
			if (BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_AvailableOffer")).equals(availableOffer)) {
				BaseUI.baseStringCompare("localOffer_AvailableOffer", availableOffer, currentAvailableOffer);
			} else {
				BaseUI.baseStringCompare("localOffer_AvailableOffer", availableOffers, currentAvailableOffer);
		    }
		}
	}

	public static void verify_AvailableOffer_TextAndNumber(HashMap<String, String> localOfferData) {
		BaseUI.verifyElementHasExpectedText("localOffer_ResultTitle_List", localOfferData.get("localOfferName"));
		String currentAvailableOffer = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_AvailableOffer"))
				.split("\\n")[0];
		String availableOffer = "Available Offer";
		String availableOffers = "Available Offers";
		String availableOfferNumber = null;

		if (BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_AvailableOffer")).equals(availableOffer)) {
			BaseUI.baseStringCompare("localOffer_AvailableOffer", currentAvailableOffer, availableOffer);
		} else {
			if (ClientDataRetrieval.isRedesignClient()) {
				BaseUI.baseStringPartialCompare("localOffer_AvailableOffer", currentAvailableOffer.substring(2), "Deals");
				availableOfferNumber = localOfferData.get("availableOffers").replaceAll(" Deals", "");
			} else {
				BaseUI.baseStringCompare("localOffer_AvailableOffer", currentAvailableOffer, availableOffers);
				availableOfferNumber = localOfferData.get("availableOffers");
			} 
			
		    Integer availableOfferInt = Integer.parseInt(availableOfferNumber);
		    BaseUI.verify_true_AndLog(availableOfferInt > 0, "Available offer is greater than 0: "+ availableOfferInt,
				"Available offer is NOT greater than 0: "+ availableOfferInt);
		}
	}

	public static void verify_LocalOffer_LogoIMG() throws Exception {
		String logoImg = "";
		if (ClientDataRetrieval.isRedesignClient()) {
			clickFirstDeal();
		} 
		String currentLogo = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_LogoIMG"));
		BaseUI.baseStringCompare("localOffer_LogoIMG", currentLogo, logoImg);

	}

	public static void verify_LocalOffer_SearchResultPage_DetailsCityName(String expectedCityDetails) throws Exception {
		String currentCityDetails;
		if (ClientDataRetrieval.isRedesignClient()) {
			clickFirstDeal();
			currentCityDetails = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("localOffer_DetailsCity"), "innerText");
		} else {
			currentCityDetails = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_DetailsCity"))
					.split("\\n")[1];
		}
		String location = ClientDataRetrieval.environment.equals("RC") ? "Ft." : "Ft";
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi) &&
				ClientDataRetrieval.environment.equals("XUAT")){
			BaseUI.baseStringCompare("localOffer_DetailsCity", expectedCityDetails,
					currentCityDetails.trim());
		} else {
			BaseUI.baseStringCompare("localOffer_DetailsCity", expectedCityDetails.replaceAll("Fort", location),
					currentCityDetails.trim());
		}
	}

	public static void verify_Breadcrumb_ContainsText(String textToContain) {
		BaseUI.verifyElementHasExpectedPartialTextByElement(
				Locator.lookupRequiredElement("LocalOffer_SearchResult_BreadcrumbLink_ContainingText", textToContain, null),
				textToContain);

	}

	public static void verify_LocalOffer_Title_Appears() {

		String localOffersTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_TitleText"))
				.split("\\n")[0];
		String expectedText = null;
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_TitleText"));
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
				|| ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			expectedText = "Shop Local. Save Local.";
		} else if (ClientDataRetrieval.isRedesignClient()) {
			expectedText = "Deals Near";
			BaseUI.verify_true_AndLog(localOffersTitle.contains("Deals Near") || localOffersTitle.contains("Deals near"), 
					"Expected Local Deals Title MATCH Actual: "+ localOffersTitle, 
					"Expected Local Deals Title DOES NOT MATCH Actual: "+ localOffersTitle);
		} else {
			expectedText = "Local Offers";
			BaseUI.baseStringPartialCompare("Local Offers Title", expectedText, localOffersTitle);
		}		
	}

	public static TableData return_LocalOffer_TableData() {
		HashMap<String, String> tableMappings = new HashMap<String, String>();
		if (ClientDataRetrieval.isRedesignClient()) {
			tableMappings.put("localOfferName", ".//div[@class='merchant-name']");
			tableMappings.put("availableOffers", ".//span[@class='count']");
		} else {
			tableMappings.put("localOfferName", ".//h4 | .//h2");
			tableMappings.put("address", ".//*[@class='locationAddress']/span");
			tableMappings.put("availableOffers", ".//*[@class='offerCount']/span[1]");
		}

		TableData localOfferTable = new TableData();
		localOfferTable.data = BaseUI.tableExtractor("localOffer_SearchResult_RowList", tableMappings);

		return localOfferTable;
	}

	// only applies to US zipcode
	public static void verify_LocalOffer_Zipcode(HashMap<String, String> localOfferData, int zipcode) {
		BaseUI.verifyElementHasExpectedText("localOffer_ResultTitle_List", localOfferData.get("localOfferName"));

		String address = localOfferData.get("address").split("\n")[1];
		address = address.replaceAll("[^0-9]", "");

		Integer addressInt = Integer.parseInt(address);
		BaseUI.verify_true_AndLog(addressInt >= zipcode,
				MessageFormat.format("Local Offer Result page Zipcode match {0} or is greater than {0}", zipcode),
				MessageFormat.format("Local Offer Result page Zipcode did NOT match {0} or is NOT greater than {0}",
						zipcode));

	}

	public static String return_ImageURL() throws Exception {

		String url;
		if (ClientDataRetrieval.isRedesignClient()) {
			clickFirstDeal();
		}
		
		url = Locator.lookupRequiredElement("localOffer_Logo_FirstLogo").getAttribute("src");

		return url;
	}

	public static void verify_LocalOfferLogo_Accurate(String expectedLogoURL) {
		
		BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("localOffer_LogoIMG"), "src",
				expectedLogoURL);	
	}

	public static ArrayList<String> return_LocalOfferTitleList() throws Exception {

		ArrayList<WebElement> localOfferTitleList = Locator.lookup_multipleElements("localOffer_ResultTitle_List", null,
				null);

		ArrayList<String> titleList = new ArrayList<String>();

		for (WebElement localOffer : localOfferTitleList) {
			String localOfferStr = localOffer.getText();
			titleList.add(localOfferStr);
		}

		return titleList;
	}

	public static void click_ChangeLocation_Link() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_change_locationBtn"));
		Thread.sleep(500);
	}

	public static void verify_SearchResultPageLocation_Matches_SearchPage(String state, String city, String category, String state_Abbreviation)
			throws Exception {
		localOffer_SearchPage_WithCriteria(state, city, category);

		if (ClientDataRetrieval.isRedesignClient()) {
			String cityName = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
			Navigation.click_SeeLocalOffer_SearchPageButton();
			String currentLocation = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_CurrentLocation"));

			BaseUI.baseStringCompare("City", currentLocation.split(",")[0].replaceAll("Deals near ", ""),
					cityName);
			BaseUI.baseStringCompare("State", currentLocation.split(",")[1].replace(" ", ""), state_Abbreviation);
			verifySelectedCategoryButtonEnabled(category);
		} else {
			Navigation.click_SeeLocalOffer_SearchPageButton();
			String currentLocation = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_CurrentLocation"));
			click_ChangeLocation_Link();
			String cityName = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_City_Textbox"));
			String stateText = BaseUI.getSelectedOptionFromDropdown("homepage_LocalOffer_State_Dropdown");
			String categoryText = BaseUI.getSelectedOptionFromDropdown("localOffer_SearchPage_OfferDropdownList");

			BaseUI.baseStringCompare("City", currentLocation.split(",")[0], cityName);
			BaseUI.baseStringCompare("State", currentLocation.split(",")[1].replace(" ", ""), stateText);
			BaseUI.baseStringCompare("Category", category, categoryText);
		
			BaseUI.verifyCheckboxStatus(Locator.lookupRequiredElement("localOffer_StoreInfo_Checkbox"), true);
		}	
	}

	public static void verify_SearchResultZipcode_Matches_SearchPage(String zipCode, String category) throws Exception {
		localOffer_Search_WithZipcode(zipCode, category);

		if (ClientDataRetrieval.isRedesignClient()) {
			String zipcodeText = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"));
			Navigation.click_SeeLocalOffer_SearchPageButton();
			BaseUI.baseStringCompare("Zipcode", zipCode, zipcodeText);
			
			verifySelectedCategoryButtonEnabled(category);
		} else {
			Navigation.click_SeeLocalOffer_SearchPageButton();
			click_ChangeLocation_Link();
			String zipcodeText = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox"));
			String categoryText = BaseUI.getSelectedOptionFromDropdown("localOffer_SearchPage_OfferDropdownList");

			BaseUI.baseStringCompare("Zipcode", zipCode, zipcodeText);
			BaseUI.baseStringCompare("Category", category, categoryText);

			BaseUI.verifyCheckboxStatus(Locator.lookupRequiredElement("localOffer_StoreInfo_Checkbox"), true);
		}
	}

	public static void verify_Distance_FromLocalOffer_Ascending() throws Exception {
		ArrayList<String> distanceList = return_ListOfDistances();

		for (int i = 1; i < distanceList.size(); i++) {
			Double currentDistance = Double.parseDouble(distanceList.get(i).replace(" mi", ""));
			Double previousDistance = Double.parseDouble(distanceList.get(i - 1).replace(" mi", ""));

			BaseUI.verify_true_AndLog(currentDistance >= previousDistance, "Distance was expected.",
					"Distance was NOT as expected.");
		}

	}

	public static ArrayList<String> return_ListOfDistances() {

		ArrayList<WebElement> addressElementList = Locator
				.lookup_multipleElements("localOffer_SearchResult_ListOf_OfferAddresses", null, null);
		ArrayList<String> distanceList = new ArrayList<String>();

		BaseUI.verify_true_AndLog(addressElementList.size() > 0, "Found addresses.", "Did NOT find addresses.");

		String distance;
		for (WebElement address : addressElementList) {
			if (ClientDataRetrieval.isRedesignClient()) {
				distance = BaseUI.getTextFromField(address);
			} else {
				String addressStr = BaseUI.getTextFromField(address);
				distance = returnDistance_FromAddress(addressStr);
			}	
			distanceList.add(distance);
		}
	
		return distanceList;
	}

	public static String returnDistance_FromAddress(String address) {
		String distance = null;
		if (ClientDataRetrieval.isLegacyClient()) {
			distance = address.split("\n")[3];
		}		
		distance = distance.replace("mi", "");

		return distance;
	}

	public static void sort(String sortOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("localOffer_SearchResult_Sort_Dropdown"), sortOption);
		Thread.sleep(500);
	}

	public static void range(String rangeOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("localOffer_SearchResult_Range_Dropdown"), rangeOption);
		Thread.sleep(500);
	}

	public static void itemPerPage(String itemPerPageOption) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("localOffer_SearchResult_ItemPerPage_Dropdown"),
				itemPerPageOption);
		Thread.sleep(500);
	}

	public static Integer return_ResultsCount() {
		String resultToReturn = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("localOffer_SearchResult_TopResult_Count"));
		resultToReturn = resultToReturn.substring(resultToReturn.indexOf("of ") + 3, resultToReturn.length())
				.replace("near", "").trim();

		return Integer.parseInt(resultToReturn);
	}

	public static Integer return_ShowingItemsCount() {
		String showingItemsToReturn = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("localOffer_SearchResult_TopResult_Count"));
		showingItemsToReturn = showingItemsToReturn
				.substring(showingItemsToReturn.indexOf("-") + 1, showingItemsToReturn.lastIndexOf("of")).trim();

		return Integer.parseInt(showingItemsToReturn);
	}

	public static void click_SearchWithinResult_GoButton() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchResult_SearchWithin_Go_Button"));
		Thread.sleep(1500);
	}

	public static void launch_SearchWithinResult(String valueToSet, String category) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("localOffer_SearchResult_SearchWithin_TextBox"),
				valueToSet);
		click_SearchWithinResult_GoButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			clickCategoryBtn(category);
		}
	}

	public static void verify_SearchWithinResults_Matches_GivenOfferName(String expectedValue) throws Exception {
		ArrayList<String> localOfferTitleList = return_LocalOfferTitleList();

		for (Integer i = 0; i < localOfferTitleList.size(); i++) {
			BaseUI.verify_true_AndLog(localOfferTitleList.get(i).equals(expectedValue),
					MessageFormat.format("Value: {0} matched Value: {1}", expectedValue, localOfferTitleList.get(i)),
					MessageFormat.format("Expected Value: {0}, Current Value: {1}", expectedValue,
							localOfferTitleList.get(i)));
		}
	}

	public static void verify_SearchWithinResults_Error(String valueToSet, String expectedtText) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("localOffer_SearchResult_SearchWithin_TextBox"),
				valueToSet);
		click_SearchWithinResult_GoButton();
		WebElement element = Locator.lookupRequiredElement("localOffer_SearchResult_SearchWithin_Error");
		BaseUI.verifyElementAppears(element);
		String elementActualText = element.getText();
		BaseUI.baseStringCompare("SearWithin Error", expectedtText, elementActualText);

	}

	public static String return_randomZipCode() throws Exception {
		String[] zipcodes = { "75001", " 88901", "60007", "33130", " 94016", "20001" };

		String indexToUse = BaseUI.random_NumberAsString(0, zipcodes.length - 1);

		return zipcodes[Integer.parseInt(indexToUse)];
	}

	public static String return_randomDropdown() {
		ArrayList<WebElement> offerDropdownList = Locator
				.lookup_multipleElements("localOffer_SearchPage_OfferDropdownLists", null, null);

		ArrayList<String> dropdownList = new ArrayList<String>();
		for (int i = 0; i < offerDropdownList.size(); i++) {
			String dropdownValue = offerDropdownList.get(i).getText();
			dropdownList.add(dropdownValue);
		}

		Integer indexOfDropdown = dropdownList.size() - 1;
		String indexToUse = BaseUI.random_NumberAsString(0, indexOfDropdown);
		Integer indexToUseInt = Integer.parseInt(indexToUse);

		return dropdownList.get(indexToUseInt);
	}

	public static void redeem_PremiumDiscount_Offer(String availableOffer) throws Exception {
		TableData localOffer = return_LocalOffer_TableData();

		BaseUI.verify_true_AndLog(localOffer.data.size() > 0, "Search Results found.", "Search Results not found.");

		for (Integer i = 0; i < localOffer.data.size(); i++) {
			if (availableOffer.equals(localOffer.data.get(i).get("availableOffers"))) {
				click_ViewButton_ByTitleAndAvailableOffer(localOffer.data.get(i).get("localOfferName"),
						localOffer.data.get(i).get("availableOffers"));
				if (BaseUI.elementAppears(Locator.lookupOptionalElement(
						"localOffer_SearchResult_PremiumDiscount_ByTitleAndAvailableOffer",
						localOffer.data.get(i).get("localOfferName"), localOffer.data.get(i).get("availableOffers")))
						&& BaseUI.elementAppears(
								Locator.lookupRequiredElement("localOffer_SearchResult_RedeemButton_ByTitleAndAvailableOffer",
										localOffer.data.get(i).get("localOfferName"),
										localOffer.data.get(i).get("availableOffers")))) {
					click_RedeemButton_ByTitleAndAvailableOffer(localOffer.data.get(i).get("localOfferName"),
							localOffer.data.get(i).get("availableOffers"));

					BaseUI.verifyElementHasExpectedText("localOffer_SearchResult_Redeem_DialogBox_Text",
							"Ready to Print?");
					click_Redeem_ConfirmButton();

					if (BaseUI.pageSourceContainsString(
							"Server connection timed out please try again or contact customer service.")) {
						close_ServerConnectionError();
						continue;
					}
					break;
				}

			}
		}
	}

	public static void redeem_RepeatSavings_Offer() throws Exception {
		TableData localOffer = return_LocalOffer_TableData();

		BaseUI.verify_true_AndLog(localOffer.data.size() > 0, "Search Results found.", "Search Results not found.");

		for (Integer i = 0; i < localOffer.data.size(); i++) {
			click_ViewButton_ByTitleAndAvailableOffer(localOffer.data.get(i).get("localOfferName"),
					localOffer.data.get(i).get("availableOffers"));

			if (BaseUI.elementAppears(Locator.lookupOptionalElement(
					"localOffer_SearchResult_RepeatSavings_ByTitleAndAvailableOffer",
					localOffer.data.get(i).get("localOfferName"), localOffer.data.get(i).get("availableOffers")))
					&& BaseUI.elementAppears(
							Locator.lookupRequiredElement("localOffer_SearchResult_RedeemButton_ByTitleAndAvailableOffer",
									localOffer.data.get(i).get("localOfferName"),
									localOffer.data.get(i).get("availableOffers")))) {

				click_RedeemButton_ByTitleAndAvailableOffer(localOffer.data.get(i).get("localOfferName"),
						localOffer.data.get(i).get("availableOffers"));

				if (BaseUI.pageSourceContainsString(
						"Server connection timed out please try again or contact customer service.")) {
					close_ServerConnectionError();
					continue;
				}

				BaseUI.verifyElementHasExpectedText("localOffer_SearchResult_Redeem_DialogBox_Text", "Ready to Print?");
				click_Redeem_ConfirmButton();

//				BaseUI.verifyElementAppears(
//						Locator.lookupRequiredElement("localOffer_SearchResult_Redeem_PrintCouponDialogBox"));

				close_ServerConnectionError_IfAppears();
				break;
			}
		}

	}

	public static void redeem_RepeatSaving_OfferAgain(String localOfferName, String category) throws Exception {
		launch_SearchWithinResult(localOfferName, category);

		TableData localOffer = return_LocalOffer_TableData();

		BaseUI.verify_true_AndLog(localOffer.data.size() > 0, "Search Results found.", "Search Results not found.");

		for (Integer i = 0; i < localOffer.data.size(); i++) {
			click_ViewButton_ByTitleAndAvailableOffer(localOfferName, localOffer.data.get(i).get("availableOffers"));

			if (BaseUI.elementAppears(
					Locator.lookupRequiredElement("localOffer_SearchResult_RepeatSavings_ByTitleAndAvailableOffer",
							localOfferName, localOffer.data.get(i).get("availableOffers")))
					&& BaseUI.elementAppears(
							Locator.lookupRequiredElement("localOffer_SearchResult_RedeemButton_ByTitleAndAvailableOffer",
									localOfferName, localOffer.data.get(i).get("availableOffers")))) {

				click_RedeemButton_ByTitleAndAvailableOffer(localOfferName,
						localOffer.data.get(i).get("availableOffers"));

				if (BaseUI.pageSourceContainsString(
						"Server connection timed out please try again or contact customer service.")) {
					close_ServerConnectionError();
					continue;
				}

				BaseUI.verifyElementHasExpectedText("localOffer_SearchResult_Redeem_DialogBox_Text", "Ready to Print?");
				click_Redeem_ConfirmButton();

//				BaseUI.verifyElementAppears(
//						Locator.lookupRequiredElement("localOffer_SearchResult_Redeem_PrintCouponDialogBox"));
				close_ServerConnectionError_IfAppears();
				break;
			}
		}
	}
	
	public static void redeem_RepeatSaving_Offer_ByOfferName(String localOfferName, String category) throws Exception {
		launch_SearchWithinResult(localOfferName, category);

		TableData localOffer = return_LocalOffer_TableData();

		BaseUI.verify_true_AndLog(localOffer.data.size() > 0, "Search Results found.", "Search Results not found.");

		for (Integer i = 0; i < localOffer.data.size(); i++) {
			click_ViewButton_ByTitleAndAvailableOffer(localOfferName, localOffer.data.get(i).get("availableOffers"));

			if (BaseUI.elementAppears(
					Locator.lookupRequiredElement("localOffer_SearchResult_RepeatSavings_ByTitleAndAvailableOffer",
							localOfferName, localOffer.data.get(i).get("availableOffers")))
					&& BaseUI.elementAppears(
							Locator.lookupRequiredElement("localOffer_SearchResult_RepeatSaving_RedeemButton_ByTitleAndAvailableOffer",
									localOfferName, localOffer.data.get(i).get("availableOffers")))) {

				localOfferDealText = BaseUI.getTextFromField(
						Locator.lookupRequiredElement("localOffer_SearchResult_OfferDealText_ByTitleAndAvailableOffer",
								localOfferName, localOffer.data.get(i).get("availableOffers")));
				if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
					localOfferPoints = BaseUI
							.getTextFromField(Locator.lookupRequiredElement(
									"localOffer_SearchResult_OfferPoints_ByTitleAndAvailableOffer", localOfferName,
									localOffer.data.get(i).get("availableOffers")))
							.replace("Points", "").replace(",", "").trim();
					if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
							|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
							|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
							|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {

						localOfferPoints = localOfferPoints.trim();
					}
				}
				localOfferAddress = localOffer.data.get(i).get("address");
				click_RepeatSaving_RedeemButton_ByTitleAndAvailableOffer(localOfferName,
						localOffer.data.get(i).get("availableOffers"));
				break;
			}
		}
	}
	
	public static void verify_RepeatSaving_Redeemed(String pointsBefore) throws Exception{
		LocalOffers_LandingPage.click_Redeem_ConfirmButton();
		BaseUI.wait_for_PageSource_ToContainText("Your Offer will start printing shortly.", 10);
		
		if (BaseUI.pageSourceContainsString(
				"Server connection timed out please try again or contact customer service.")) {
			BaseUI.log_AndFail("Server Connecton Error Exist so can NOt Redeem the offer");
		}

		// Closing Browser to escape print dialog box
		Browser.closeBrowser();
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			String pointsAfter = Navigation.returnPoints();
			pointsRedeemed = Navigation.return_ExpectedPoints_AsString(pointsBefore, pointsAfter);
			pointsRedeemed = pointsRedeemed.replace(",", "");
			BaseUI.baseStringCompare("Points", localOfferPoints.replace("\u2013", "").trim(),
					pointsRedeemed.trim());

			BaseUI.verify_true_AndLog(!pointsBefore.equals(pointsAfter),
					MessageFormat.format("Before points {0} were deducted to {1} after offer redeemed.", pointsBefore,
							pointsAfter),
					MessageFormat.format("Before points {0} were Not deducted to {1} after offer redeemed.",
							pointsBefore, pointsAfter));
		}
	}


	public static void click_ViewButton_ByTitleAndAvailableOffer(String localOfferName, String availableOffers)
			throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("localOffer_SearchResult_ViewButton_ByTitleAndAvailableOffer",
					localOfferName, availableOffers));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchResult_ViewButton_ByTitleAndAvailableOffer",
					localOfferName, availableOffers));
		}
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("localOffer_SearchResult_RedeemNowBtn", null, null, 20);
	}

	public static void click_RedeemButton_ByTitleAndAvailableOffer(String localOfferName, String availableOffers)
			throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("localOffer_SearchResult_RedeemButton_ByTitleAndAvailableOffer",
					localOfferName, availableOffers));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchResult_RedeemButton_ByTitleAndAvailableOffer",
					localOfferName, availableOffers));
		}
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("localOffer_SearchResult_RedeemNowBtn", null, null, 20);
	}
	
	public static void click_RepeatSaving_RedeemButton_ByTitleAndAvailableOffer(String localOfferName, String availableOffers)
			throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("localOffer_SearchResult_RepeatSaving_RedeemButton_ByTitleAndAvailableOffer",
					localOfferName, availableOffers));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchResult_RepeatSaving_RedeemButton_ByTitleAndAvailableOffer",
					localOfferName, availableOffers));
		}
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("localOffer_SearchResult_Redeem_DialogBox_ConfirmButton", null, null, 20);
	}

	public static void click_Redeem_ConfirmButton() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("localOffer_SearchResult_Redeem_DialogBox_ConfirmButton"));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchResult_Redeem_DialogBox_ConfirmButton"));
		}
		Thread.sleep(1000);
		BaseUI.waitForElementToBeDisplayed("localOffer_SearchResult_Redeem_PrintCouponDialogBox", null, null, 20);
	}
	
	public static void close_Redeem_ReadyToPrintPopupBox() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupRequiredElement("localOffer_SearchResult_Redeem_DialogBox_Close"));
		} else {
			BaseUI.click(Locator.lookupRequiredElement("localOffer_SearchResult_Redeem_DialogBox_Close"));
		}
		Thread.sleep(1000);
		BaseUI.waitForElementToNOTBeDisplayed("localOffer_SearchResult_Redeem_DialogBox_Close", null, null);
	}
	

	public static void close_ServerConnectionError_IfAppears() throws Exception {
		if (BaseUI.pageSourceContainsString(
				"Server connection timed out please try again or contact customer service.")) {
			close_ServerConnectionError();
		}
	}

	public static void close_ServerConnectionError() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("error_CloseButton"));
		Thread.sleep(500);
	}
	
	
	public static void clickCategoryBtn(String category) throws Exception {
		if(category.equals("Dining")) {
			clickEntertainmentBtn();
			clickShoppingBtn();
		} else if(category.equals("Entertainment")) {
			clickShoppingBtn();
			clickDiningBtn();
		} else {
			clickDiningBtn();
			clickEntertainmentBtn();
		}
	}
	
	
	public static void verifySelectedCategoryIconDisplayed(String category){
		if(category.equals("Dining")) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_DiningIcon"));
		} else if(category.equals("Entertainment")) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_EntertainmentIcon"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ShoppingIcon"));
		}
	}
	
	
	public static void verifySelectedCategoryButtonEnabled(String category){
		if(category.equals("Dining")) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_DiningBtnSelected"));
		} else if(category.equals("Entertainment")) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_EntertainmentBtnSelected"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ShoppingBtnSelected"));
		}
	}
	
	
	public static void clickZipcode_GetDealsBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_Zip_GetDealsBtn"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("localOffer_SearchLocalOffer_WaitElement", null, null, 20);
	}
	
	
	public static void clickFirstDeal() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_FirstDeals"));
		Thread.sleep(500);
	}
	
	//This method is used for Redesign clients
	public static void verifyZipcodeRequiredError() throws Exception {
		WebElement zipTextBox = Locator.lookupRequiredElement("homepage_LocalOffer_ZipCode_TextBox");
		zipTextBox.clear();
		clickZipcode_GetDealsBtn();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_SearchPage_ZipTxtboxError"));
		BaseUI.verifyElementHasExpectedText("localOffer_SearchPage_ZipTxtboxError", "Your Zip Code is required.");
	}
	
	public static void verifySearchListIsValidSearchResult(String searchWithResults_ValidSearch, 
			String category, String searchWithResults_ExpectedValue) throws Exception {
		TableData previousResultList = return_LocalOffer_TableData();

		launch_SearchWithinResult(searchWithResults_ValidSearch, category);

		TableData currentResultList = return_LocalOffer_TableData();

		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			if(BaseUI.elementAppears(Locator.lookupOptionalElement("localOffer_HeaderErrorText"))) {
				String headerMsg = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_HeaderErrorText"));
				BaseUI.verify_true_AndLog(!(headerMsg.contains("No results found. Try searching a specific food, activity, or establishment.")),
						"Search result displays value", "Search result DOES NOT display value");
			} else {
				BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("localOffer_HeaderErrorText"));
			}
		} else {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("localOffer_HeaderErrorText"));
			String headerMsg = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_TitleText"));
			BaseUI.verify_true_AndLog(!(headerMsg.contains("No results found. Try searching a specific food, activity, or establishment.")),
					"Search result displays value", "Search result DOES NOT display value");
		}
		
		BaseUI.verify_true_AndLog(previousResultList.data.size() != currentResultList.data.size(),
				MessageFormat.format("Previous Result List {0} does NOT match Current Result List {1}.", previousResultList.data, currentResultList.data),
				MessageFormat.format("Previous Result List {0} does match Current Result List {1}.", previousResultList.data, currentResultList.data));
		verify_SearchWithinResults_Matches_GivenOfferName(searchWithResults_ExpectedValue);
	}

	//Redesign clients only
	public static void clickDiningBtn() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("localOffer_DiningBtn"));
		BaseUI.waitForElementToBeDisplayed("localOffer_LandingPage_WaitElement", null, null, 30);
		Thread.sleep(2000);
	}

	public static void clickEntertainmentBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_EntertainmentBtn"));
		BaseUI.waitForElementToBeDisplayed("localOffer_LandingPage_WaitElement", null, null, 30);
		Thread.sleep(1000);
	}

	public static void clickShoppingBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_ShoppingBtn"));
		BaseUI.waitForElementToBeDisplayed("localOffer_LandingPage_WaitElement", null, null, 30);
		Thread.sleep(1000);
	}


	//Redesign client only
	public static void verifyLocalOfferSearchResultList(String pageNumberList) {
		String pageCountListUpdate = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_PageNumberList_LastPageCount"));
		BaseUI.verify_true_AndLog(Integer.parseInt(pageCountListUpdate) > 0,
				"Local Offer search result list is NOT EMPTY",
				"Local offer search results list is EMPTY");
		BaseUI.verify_true_AndLog(!(pageCountListUpdate.contains(pageNumberList)),
				pageCountListUpdate+" Local Deals when expanded DOES NOT MATCH the Deal count displayed on search results page "+ pageNumberList,
				pageCountListUpdate+" Local Deals when expanded DOES MATCH the Deal count displayed on search results page "+ pageNumberList);
	}

	//Redesign clients only
	public static void verifySearchResultDealCountDiffer_WhenOneCategoryIsDisabled(int numberOfResults) {
		String actualShowingLocationResultValue = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_ShowingLocation_ResultCount"));
		Integer numberOfResultsUpdated = Integer.parseInt(actualShowingLocationResultValue.split("of")[1].trim());

		BaseUI.verify_true_AndLog(numberOfResultsUpdated > 0, "Local Offer search result IS NOT EMPTY",
				"Local Offer search result IS EMPTY");

		BaseUI.verify_true_AndLog(numberOfResultsUpdated != numberOfResults, "Local offer search result count differ after disabling one of the category",
				"Local offer search result count DOES NOT differ after disabling one of the category");
	}

    //For Redesign client only
	public static void clickOnLocalDealsPinOntheMap() throws Exception {
		BaseUI.click_js(Locator.lookupRequiredElement("localOffer_FirstDealOnMap"));
		BaseUI.wait_forPageToFinishLoading();
		Thread.sleep(1000);
	}

    public static void clickRightPaginationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_RightPaginationArrow"));
        BaseUI.waitForElementToBeDisplayed("localOffer_ResultList_LeftPaginationArrow", null, null, 10);
        Thread.sleep(1000);
    }

    public static void clickLeftPaginationArrow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_LeftPaginationArrow"));
        BaseUI.waitForElementToBeDisplayed("localOffer_ResultList_PageClicked_WaitElement", null, null, 10);
        Thread.sleep(1000);
    }

    public static void clickRandomPageNummberFromList() throws Exception {
        ArrayList<WebElement> pageCountList = Locator
                .lookup_multipleElements("localOffer_PageNumberList", null, null);
        BaseUI.verify_true_AndLog(pageCountList.size() > 0,
                "Local Offer search result list is NOT EMPTY",
                "Local offer search results list is EMPTY");

        ArrayList<String> pageCount = new ArrayList<String>();
        for (int i = 0; i < pageCountList.size(); i++) {
            String pageCountValue = pageCountList.get(i).getText();
            pageCount.add(pageCountValue);
        }

        Integer indexOfPageCount = pageCount.size() - 1;
        String pageNumberToClick = BaseUI.random_NumberAsString(2, indexOfPageCount);

        BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_AnyPageNumber", pageNumberToClick, null));
        BaseUI.waitForElementToBeDisplayed("localOffer_ResultList_PageClickedActive", pageNumberToClick, null, 10);
        Thread.sleep(1000);
    }

    public static ArrayList<String> returnLocalDealList() {
		ArrayList<WebElement> availableDealList = Locator
				.lookup_multipleElements("localOffer_ResultTitle_List", null, null);
		BaseUI.verify_true_AndLog(availableDealList.size() > 0,
				"Local Offer search result list is NOT EMPTY",
				"Local offer search results list is EMPTY");

		ArrayList<String> dealName = new ArrayList<String>();
		for (int i = 0; i < availableDealList.size(); i++) {
			String pageCountValue = availableDealList.get(i).getText();
			dealName.add(pageCountValue);
		}

		return dealName;
	}

    public static void clickEachAvailableDeal_VerifyDealLogoAndAddressDisplayed(String city) throws Exception {

		ArrayList<String> dealName = returnLocalDealList();
		for(int i = 0; i < dealName.size() - 1; i++) {
			BaseUI.scroll_to_element(Locator.lookupRequiredElement("localOffer_ResultTitle_ListItem", dealName.get(i), null));
			BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultTitle_ListItem", dealName.get(i), null));
			BaseUI.waitForElementToBeDisplayed("localOffer_ResultList_DealImgLogo", null, null, 20);
			Thread.sleep(1000);

			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_DealImgLogo"));

			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_DealAddressDetail"));
			BaseUI.verifyElementHasExpectedPartialText("localOffer_ResultList_DealAddressDetail", city);
		}
	}

	//This method for Redesign clients only
	public static void clickFirstDeal_RedeemNowBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemNowBtn"));
		BaseUI.waitForElementToBeDisplayed("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_CancelBtn", null, null, 30);
		Thread.sleep(2000);
	}

	//This method for Redesign clients only
	public static void clickFirstDeal_RedeemNow_PopupModal_CancelBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_CancelBtn"));
		BaseUI.waitForElementToNOTBeDisplayed("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_CancelBtn", null, null, 30);
		Thread.sleep(2000);
	}

	//This method for Redesign clients only
	public static void clickFirstDeal_RedeemNow_PopupModal_PrintNowBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_PrintNowBtn"));
		Thread.sleep(2000);
	}

	//This method for Redesign clients only
	public static void clickPopupModal_CloseIcon() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("localOffer_ResultList_PrintOffer_PopupModal_CloseIcon"));
		//BaseUI.waitForElementToNOTBeDisplayed("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_CancelBtn", null, null, 30);
		Thread.sleep(2000);
	}

	public static void verifyLocalDealSearchResults_DiningBtnDisplayed() {
		String expectedValue;
		if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AARPRedesign)){
			expectedValue = "DINING";
		} else {
			expectedValue = "Dining";
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_DiningBtnSelected"));
		BaseUI.verifyElementHasExpectedPartialText("localOffer_DiningBtnSelected", expectedValue);
	}

	public static void verifyLocalDealSearchResults_ShoppingBtnDisplayed() {
		String expectedValue;
		if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AARPRedesign)){
			expectedValue = "SHOPPING";
		} else {
			expectedValue = "Shopping";
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ShoppingBtnSelected"));
		BaseUI.verifyElementHasExpectedPartialText("localOffer_ShoppingBtnSelected", expectedValue);
	}

	public static void verifyLocalDealSearchResults_EntertainmentBtnDisplayed() {
		String expectedValue;
		if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AARPRedesign)){
			expectedValue = "ENTERTAINMENT";
		} else {
			expectedValue = "Entertainment";
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_EntertainmentBtnSelected"));
		BaseUI.verifyElementHasExpectedPartialText("localOffer_EntertainmentBtnSelected", expectedValue);
	}
}
