package dRewards.pages;

import utils.BaseUI;
import utils.Locator;

public class TravelCarousel_ResultPage {

	public static void validate_TravelCarousel_ResultPage(String location) throws Exception
	{
		String checkinDate = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("ttdResult_CheckIn_DatePicker"));
		String checkoutDate = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("ttdResult_CheckOut_DatePicker"));
		String numberOfAdults = BaseUI.getSelectedOptionFromDropdown("ttdResult_NumberOfAdults_Dropdown");
		String numberOfChildren = BaseUI.getSelectedOptionFromDropdown("ttdResult_NumberOfChildren_Dropdown");
		
		String hotelName = BaseUI.getTextFromField(Locator.lookupRequiredElement("ttdResult_LocationName"));
		String hotelAddress = BaseUI.getTextFromField(Locator.lookupRequiredElement("ttdResult_LocationAddress"));
		String reservationInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("ttdResult_LocationReservationInfo"));
		
		String hotelRate = BaseUI.getTextFromField(Locator.lookupRequiredElement("ttdResult_TotalRetailRate"));
		String yourPrice = BaseUI.getTextFromField(Locator.lookupRequiredElement("ttdResult_TotalYourPrice"));
		
		
		//Verify checkin date is after today's date.
		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		BaseUI.verify_FirstDate_IsBefore_SecondDate(todaysDate, checkinDate, "MM/dd/yyyy");
		
		//Verify checkout date is after checkin date.
		BaseUI.verify_FirstDate_IsBefore_SecondDate(checkinDate, checkoutDate, "MM/dd/yyyy");
		
		BaseUI.baseStringCompare("Default Number of Adults", "2", numberOfAdults);
		BaseUI.baseStringCompare("Default Number of Children", "0", numberOfChildren);
		
		BaseUI.verify_String_Not_EmptyOrNull(hotelName);
		BaseUI.verify_true_AndLog(hotelAddress.contains(location), "Location " + location + " found in Hotel Address.", 
				"Did NOT find location " + location + " in Hotel Address of " + hotelAddress);
		
		//Verify Reservation Info
		BaseUI.baseStringCompare("Checkin Date", "Check in " + checkinDate, reservationInfo.split("\\n")[0]);
		BaseUI.baseStringCompare("Checkout Date", "Check out " + checkoutDate, reservationInfo.split("\\n")[1]);
		BaseUI.baseStringCompare("Number of Adults", "Number of Adults: " + numberOfAdults, reservationInfo.split("\\n")[2]);
		
		BaseUI.verify_String_Not_EmptyOrNull(hotelRate);
		BaseUI.verify_true_AndLog(hotelRate.contains("$") && hotelRate.contains("."), "Hotel Rate was found.", "Hotel Rate was not found.");
		BaseUI.verify_String_Not_EmptyOrNull(yourPrice);
		BaseUI.verify_true_AndLog(yourPrice.contains("$") && yourPrice.contains("."), "Your Price was found.", "Your Price was not found.");
	}
	
}//End of Class
