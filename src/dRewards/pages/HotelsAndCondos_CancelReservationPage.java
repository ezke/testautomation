package dRewards.pages;

import java.util.HashMap;

import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import utils.BaseUI;
import utils.Locator;

public class HotelsAndCondos_CancelReservationPage {

    public static void click_SubmitCancellation() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("cancelRes_Summary_SubmitButton"));
        BaseUI.waitForElementToBeDisplayed("cancelRes_CancellationSuccessfulHeader", null, null, 50);
        Thread.sleep(1000);
    }

    public static void verify_Cancellation_Successful() throws Exception {

        String cancellation_message = "Your reservation has been successfully cancelled.";
        if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            cancellation_message = "Your booking has been cancelled.";
        } else if (ClientDataRetrieval.isRedesignClient()) {
            cancellation_message = "Hotel Booking cancellation";
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("cancelRes_CancellationSuccessfulHeader"));
        BaseUI.verifyElementHasExpectedText("cancelRes_CancellationSuccessfulHeader", cancellation_message);
    }

    public static void validate_cancellation_Information(HashMap<String, String> hotelData, String fromDate,
                                                         String toDate, String numAdults, String numChildren, String guestContactInfo, String taxesAndFees,
                                                         String finalCost) {

        verify_Hotel_Information(hotelData, fromDate, toDate);

        verify_GuestInfo(numAdults, numChildren, fromDate, toDate, guestContactInfo);

        verify_Cancellation_PaymentInfo(hotelData, taxesAndFees, finalCost);

        verify_SubmitButton_Exists();
    }

    public static void verify_SubmitButton_Exists() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("cancelRes_Summary_SubmitButton"));
    }

    public static void verify_Cancellation_PaymentInfo(HashMap<String, String> hotelData, String taxesAndFees,
                                                       String finalCost) {
        String cancelRes_RetailRate = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_RetailRate"));
        BaseUI.baseStringCompare("Retail Rate", hotelData.get("hotelRate"), cancelRes_RetailRate);

        String cancelRes_TaxAndFees = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_TaxAndFees"));
        BaseUI.baseStringCompare("Taxes and Fees", taxesAndFees, cancelRes_TaxAndFees);

        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            String cancelRes_Points = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_Points")).replace(",",
                    "");
            BaseUI.baseStringCompare("Points", hotelData.get("points"), cancelRes_Points);
        }

        String cancelRes_ActualTotal = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_ActualTotal"));
        BaseUI.baseStringCompare("Final Cost", finalCost, cancelRes_ActualTotal);

        String cancelRes_Summary_OriginalCharge = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_OriginalCharge"));
        BaseUI.baseStringCompare("Original Charge", finalCost, cancelRes_Summary_OriginalCharge);

        String cancelRes_Summary_CancellationPenalty = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_CancellationPenalty"));
        if (!ClientDataRetrieval.client_Matches(client_Designation.Allstate)) {
            String originalChargeValueOnScreen = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_OriginalCharge"));

            BaseUI.baseStringCompare("Original Charge", finalCost, originalChargeValueOnScreen);
        }

        if (!ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            String cancelRes_Summary_AmountRefunded = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_AmountRefunded"));

            Double originalCost = Double.parseDouble(finalCost.replace("$", "").replace(",", ""));
            Double cancellationPenalty = Double.parseDouble(cancelRes_Summary_CancellationPenalty.replace("$", "").replace(",", ""));

            Double expected_AmountRefunded = originalCost - cancellationPenalty;

            Double actualValue = Double.parseDouble(cancelRes_Summary_AmountRefunded.replace("$", "").replace(",", ""));
            BaseUI.verify_true_AndLog(actualValue.equals(expected_AmountRefunded),
                    "Refund Amount matched " + cancelRes_Summary_AmountRefunded,
                    "Refund Amount did not match " + String.valueOf(expected_AmountRefunded) + " seeing " + cancelRes_Summary_AmountRefunded);
            //BaseUI.baseStringCompare("Amount Refunded", finalCost, cancelRes_Summary_AmountRefunded);
        }

        if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            String cancelRes_Summary_AmountPointsRedeemed = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_AmountPointsRedeemed")).replace(",", "");
            BaseUI.baseStringCompare("Amount Points Redeemed", hotelData.get("points"),
                    cancelRes_Summary_AmountPointsRedeemed);

            String cancelRes_Summary_AmountPointsRefunded = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_AmountPointsRefunded")).replace(",", "");
            BaseUI.baseStringCompare("Amount Points Refunded", hotelData.get("points"),
                    cancelRes_Summary_AmountPointsRefunded);
        }
    }

    public static void verify_GuestInfo(String numAdults, String numChildren, String fromDate, String toDate,
                                        String guestContactInfo) {
        String cancelRes_GuestInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_GuestInfo"));
        Integer numberChildrenInt = Integer.parseInt(numChildren);
        String expected_guestInfoLine = "Guests: " + numAdults + " Adults";
        if (numberChildrenInt == 1) {
            expected_guestInfoLine += " " + numChildren + " Child";
        } else if (numberChildrenInt > 1) {
            expected_guestInfoLine += " " + numChildren + " Children";
        }
        expected_guestInfoLine += "\n" + "Checking In on: " + fromDate + "\n" + "Checking Out on: " + toDate;
        BaseUI.baseStringCompare("Hotel Guest Info", expected_guestInfoLine, cancelRes_GuestInfo);

        String cancelRes_GuestContactInfo = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("cancelRes_GuestContactInfo"));
        BaseUI.baseStringCompare("Guest Contact Info", guestContactInfo, cancelRes_GuestContactInfo);
    }

    public static void verify_Hotel_Information(HashMap<String, String> hotelData, String fromDate, String toDate) {
        String topPropertyName = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_TopPropertyName"));
        BaseUI.baseStringCompare("Top Hotel Name", hotelData.get("hotelName"), topPropertyName);

        String datesAndCancelInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_DatesAndCancelInfo"));
        BaseUI.baseStringCompare("Hotel Name", hotelData.get("hotelName"), datesAndCancelInfo.split("\\n")[1]);
        BaseUI.baseStringCompare("Date Range", fromDate + " - " + toDate, datesAndCancelInfo.split("\\n")[2]);

        String cancelRes_HotelAddress = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_HotelAddress"));
        BaseUI.baseStringCompare("Hotel Name for Address", hotelData.get("hotelName"),
                cancelRes_HotelAddress.split("\\n")[0]);
//		BaseUI.baseStringCompare("Hotel Address",
//				hotelData.get("address").split("\\n")[0].trim() + "\n" + fromDate + " - " + toDate,
//				cancelRes_HotelAddress.split("\\n")[1] + "\n" + datesAndCancelInfo.split("\\n")[2]);
    }

    public static void verify_AllStateCash_Information(String cashRewards, String totalAmountSpent,
                                                       String pointsSpent) {

        String cancellationPenalty = "";
        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {

            cancellationPenalty = return_CancellationPenalty();

            String cashRewardsSpent = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelRes_CashRewardsSpent"));
            cashRewardsSpent = cashRewardsSpent.replace("$", "").replace(",", "");
            BaseUI.baseStringCompare("Cash Rewards Spent", cashRewards, cashRewardsSpent);

            String cashRewardsRedeemed = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelRes_CashRewardsRedeemed"));
            cashRewardsRedeemed = cashRewardsRedeemed.replace("$", "").replace(",", "");
            BaseUI.baseStringCompare("Cash Rewards Redeemeed", cashRewards, cashRewardsRedeemed);

            if (cancellationPenalty.equals("$0.00")) {
                String cashRewardsRefunded = BaseUI
                        .getTextFromField(Locator.lookupRequiredElement("cancelRes_CashRewardsRefunded"));
                cashRewardsRefunded = cashRewardsRefunded.replace("$", "").replace(",", "");
                BaseUI.baseStringCompare("Cash Rewards Refunded", cashRewards, cashRewardsRefunded);
            } else {
                BaseUI.verifyElementHasExpectedText("cancelRes_CashRewardsRefunded", "$0.00");
            }
        }
    }

    public static String return_CancellationPenalty() {
        String cancellationPenalty = null;

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            cancellationPenalty = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelRes_Summary_CancellationPenalty"));
        }

        return cancellationPenalty;
    }

    public static void verify_AllStateCash_Cancellation_Successful(HashMap<String, String> hotelData, String fromDate,
                                                                   String toDate, String numberOfAdults, String numberOfChildren, String checkoutAddress,
                                                                   String expected_cancellationPenalty, String cashReward, String finalCost, String hotelImgURL)
            throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            String totalPaid = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalPaid"));
            totalPaid = totalPaid.replace("$", "").replace(",", "");
            finalCost = finalCost.replace("$", "");
            Double finalCostAsDouble = Double.parseDouble(finalCost);
            Double cashRewardAsDouble = Double.parseDouble(cashReward);
            Double expectedValueAsDouble = finalCostAsDouble + cashRewardAsDouble;
            expectedValueAsDouble = BaseUI.round_Double_ToPassedInDecimalPlaces(expectedValueAsDouble, 2);
            String expectedValueAsString = expectedValueAsDouble.toString();
            expectedValueAsString = expectedValueAsString.split("\\.")[1].length() == 1 ? expectedValueAsString + "0"
                    : expectedValueAsString;
            BaseUI.baseStringCompare("Total Paid", expectedValueAsString, totalPaid);

            String cancellationPenalty = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_CancellationPenalty"));
            cancellationPenalty = cancellationPenalty.replace("-", "").replace(",", "");
            BaseUI.baseStringCompare("Cancellation Penalty", expected_cancellationPenalty, cancellationPenalty);

            String refundedPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalPoints"));
            refundedPoints = refundedPoints.replace(",", "");
            BaseUI.baseStringCompare("Refunded Points", hotelData.get("points"), refundedPoints);

            String cashRewardsRefunded = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalCash"));
            cashRewardsRefunded = cashRewardsRefunded.replace("$", "").replace(",", "");
            if (expected_cancellationPenalty.equals("$0.00")) {
                BaseUI.baseStringCompare("Refunded cash rewards.", cashReward, cashRewardsRefunded);
            } else {
                BaseUI.baseStringCompare("Refunded cash rewards.", "0.00", cashRewardsRefunded);
            }

            String totalAmountRefunded = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalRefunded"));
            totalAmountRefunded = totalAmountRefunded.replace("$", "").replace(",", "");
            if (expected_cancellationPenalty.equals("$0.00")) {

                BaseUI.baseStringCompare("Total Amount Refunded", finalCost, totalAmountRefunded);
            } else {
                BaseUI.baseStringCompare("Total Amount Refunded", ".00", totalAmountRefunded);
            }

            // Validate Guest Info
            String guestInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_GuestInfo"));
            BaseUI.baseStringCompare("Contact Name", "Guest Name: " + checkoutAddress.split("\\n")[0],
                    guestInfo.split("\\n")[1]);
            BaseUI.baseStringCompare("Cancellation Date",
                    "Cancellation Date: " + BaseUI.getDateAsString_InRelationToTodaysDate(0),
                    guestInfo.split("\\n")[3]);
            BaseUI.baseStringCompare("Check-In Date", "Check-In: " + fromDate, guestInfo.split("\\n")[4]);
            BaseUI.baseStringCompare("Check-Out Date", "Check-Out: " + toDate, guestInfo.split("\\n")[5]);
            if (numberOfChildren.equals("0")) {
                BaseUI.baseStringCompare("Adult Count", "Adults: " + numberOfAdults, guestInfo.split("\\n")[7]);
            } else if (numberOfChildren.equals("1")) {
                BaseUI.baseStringCompare("Adult and Child Count",
                        "Adults: " + numberOfAdults + "     Child: " + numberOfChildren, guestInfo.split("\\n")[7]);
            } else {
                BaseUI.baseStringCompare("Adult and Child Count",
                        "Adults: " + numberOfAdults + "     Children: " + numberOfChildren, guestInfo.split("\\n")[7]);
            }

            hotelImgURL = hotelImgURL.replace("https", "http");
            String logoURL = Locator.lookupRequiredElement("cancelResSuccess_HotelLogo").getAttribute("src");
            // BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupRequiredElement("cancelResSuccess_HotelLogo"),
            // "src", hotelImgURL);
            BaseUI.baseStringCompare("Hotel Image.", hotelImgURL, logoURL);

            // Validate Hotel Info
            String hotelName = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_HotelName"));
            BaseUI.baseStringCompare("Hotel Name", hotelData.get("hotelName"), hotelName);

            String hotelAddress = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_HotelAddress"))
                    .replaceAll("[\r\n]+", " ").replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();
            BaseUI.baseStringCompare("Hotel Address", hotelData.get("address").replaceAll("[\r\n]+", " "), hotelAddress);
        }
    }

    public static void verify_AllState_Cancellation_Successful(HashMap<String, String> hotelData, String fromDate,
                                                               String toDate, String numberOfAdults, String numberOfChildren, String checkoutAddress, String finalCost,
                                                               String hotelImgURL) throws Exception {

        if (ClientDataRetrieval.client_Matches(client_Designation.Allstate)) {
            String totalPaid = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalPaid"));
            totalPaid = totalPaid.replace("$", "").replace(",", "");
            finalCost = finalCost.replace("$", "");

            BaseUI.baseStringCompare("Total Paid", finalCost, totalPaid);

            String cancellationPenalty = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_CancellationPenalty"));
            cancellationPenalty = cancellationPenalty.replace("-", "").replace(",", "");
         //   BaseUI.baseStringCompare("Cancellation Penalty", "$0.00", cancellationPenalty);

            String refundedPoints = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalPoints"));
            refundedPoints = refundedPoints.replace(",", "");
            BaseUI.baseStringCompare("Refunded Points", hotelData.get("points"), refundedPoints);

            String totalAmountRefunded = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_TotalRefunded"));
            totalAmountRefunded = totalAmountRefunded.replace("$", "").replace(",", "");
            if (cancellationPenalty.equals("$0.00")) {
                BaseUI.baseStringCompare("Total Amount Refunded", finalCost, totalAmountRefunded);
            } else {
                String expectedValue = ".00";

                BaseUI.baseStringCompare("Total Amount Refunded", expectedValue, totalAmountRefunded);
            }

            // Validate Guest Info
            String guestInfo = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_GuestInfo"));
            BaseUI.baseStringCompare("Contact Name", "Guest Name: " + checkoutAddress.split("\\n")[0],
                    guestInfo.split("\\n")[1]);
            BaseUI.baseStringCompare("Cancellation Date",
                    "Cancellation Date: " + BaseUI.getDateAsString_InRelationToTodaysDate(0),
                    guestInfo.split("\\n")[3]);
            BaseUI.baseStringCompare("Check-In Date", "Check-In: " + fromDate, guestInfo.split("\\n")[4]);
            BaseUI.baseStringCompare("Check-Out Date", "Check-Out: " + toDate, guestInfo.split("\\n")[5]);
            if (numberOfChildren.equals("0")) {
                BaseUI.baseStringCompare("Adult Count", "Adults: " + numberOfAdults, guestInfo.split("\\n")[7]);
            } else if (numberOfChildren.equals("1")) {
                BaseUI.baseStringCompare("Adult and Child Count",
                        "Adults: " + numberOfAdults + "     Child: " + numberOfChildren, guestInfo.split("\\n")[7]);
            } else {
                BaseUI.baseStringCompare("Adult and Child Count",
                        "Adults: " + numberOfAdults + "     Children: " + numberOfChildren, guestInfo.split("\\n")[7]);
            }

            hotelImgURL = hotelImgURL.replace("https", "http");
            String logoURL = Locator.lookupRequiredElement("cancelResSuccess_HotelLogo").getAttribute("src");
            BaseUI.baseStringCompare("Hotel Image.", hotelImgURL, logoURL);

            // Validate Hotel Info
            String hotelName = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_HotelName"));
            BaseUI.baseStringCompare("Hotel Name", hotelData.get("hotelName"), hotelName);

            String hotelAddress = BaseUI.getTextFromField(Locator.lookupRequiredElement("cancelResSuccess_HotelAddress"))
                    .replaceAll("[\r\n]+", " ").replaceAll("([^a-zA-Z0-9,-]|\\s)+", " ").trim();
            BaseUI.baseStringCompare("Hotel Address", hotelData.get("address").replaceAll("[\r\n]+", " "),
                    hotelAddress);
        }
    }

    public static void verify_CancellationNumber(String expectedNumber) {
        BaseUI.verifyElementHasExpectedText("cancelRes_ReservationNumber", expectedNumber);
    }

    public static void clickDetailsLink() throws Exception {
        BaseUI.click_js(Locator.lookupRequiredElement("accnt_Row1_TransactionDetailsLink"));
        BaseUI.waitForElementToBeDisplayed("accnt_Row1_TransactionDetails_WaitElement", null, null, 30);
        Thread.sleep(300);
    }

    public static void clickCancelBookingBtn() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("accnt_Row1_TransactionCancelLink"));
        BaseUI.waitForElementToBeDisplayed("accnt_Row1_TransactionCancelBooking_ModalMsg", null, null, 30);
        Thread.sleep(300);
    }

    public static void clickCancelBookingYesOption() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("accnt_Row1_TransactionCancelBooking_YesOption"));
        BaseUI.waitForElementToBeDisplayed("cancelRes_CancellationSuccessfulHeader", null, null, 30);
        Thread.sleep(300);
    }

    public static void clickCancelBookingNoOption() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("accnt_Row1_TransactionCancelBooking_NoOption"));
        BaseUI.waitForElementToBeDisplayed("accnt_Row1_TransactionType", null, null, 30);
        Thread.sleep(300);
    }

    public static void cancelHotelBooking() throws Exception {
        clickDetailsLink();
        clickCancelBookingBtn();
        clickCancelBookingYesOption();
    }
}
