package dRewards.pages;

import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Locator;

public class Auctions_ClosedAuctions {

	// Assumes first auction is the Auction we are claiming
	public static void verify_Description(String auctionDescription) {

		if (ClientDataRetrieval.isRedesignClient()) {
			String description = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAuct_AuctsWon_LastItem_Description"));
			BaseUI.baseStringCompare("Description", auctionDescription, description);
		} else {
			String description = BaseUI.getTextFromField(Locator.lookupRequiredElement("closed_Auctions_FirstAuction"));
			BaseUI.baseStringCompare("Description", auctionDescription, description);
		}
	}

	public static String return_EndTime() {
		String expectedEndTime = BaseUI.getTextFromField(Locator.lookupRequiredElement("closed_Auctions_First_EndTime"));
		expectedEndTime = expectedEndTime.split("\\:")[1].replace(" -", "");

		return expectedEndTime;
	}

	// Assumes first auction is the Auction we are claiming
	public static void expand_AuctionName() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("openAuct_Auctions_FirstAuction"));
		Thread.sleep(500);
	}

	public static void click_ViewAllBids() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("openAuct_ViewAllBids_FirstLink"));
		Thread.sleep(500);
	}

	public static void expandAuctions_And_ClickViewAllBids_Link() throws Exception {
		expand_AuctionName();
		click_ViewAllBids();

	}

	// Takes date with format MMM. d, yyyy h:mm a. Will then convert it to
	// format MMMM. d, yyyy h:mm a before comparison
	public static void verify_EndDate(String auctionEnded) throws Exception {
		String actualAuctionEnded = BaseUI.getTextFromField(Locator.lookupRequiredElement("closed_Auctions_First_EndTime"));
		actualAuctionEnded = actualAuctionEnded.replace("Auction End Date: ", "").replace(" -", "");
		// need to format our Auction Ended time.
		//auctionEnded = auctionEnded.replace(" EDT", "");
		if (ClientDataRetrieval.isRedesignClient()) {
			auctionEnded = auctionEnded.replace((GlobalVariables.auctionTimeZone), " ");
			auctionEnded = auctionEnded.replace("at", " ");
			String expectedAuctionEnded_Formatted = BaseUI.return_Date_AsDifferentFormat(auctionEnded,
					"MMMM dd, yyyy h:mm a", "MMMM dd, yyyy");
			BaseUI.baseStringCompare("Auction Ended", "Claimed on " + expectedAuctionEnded_Formatted, actualAuctionEnded);
		} else {
			auctionEnded = auctionEnded.replace((GlobalVariables.auctionTimeZone), "").replace("at", "");
			String expectedAuctionEnded_Formatted = BaseUI.return_Date_AsDifferentFormat(auctionEnded,
					"MMM. d, yyyy h:mm a", "MMMM d, yyyy h:mm a");
			expectedAuctionEnded_Formatted += " ET";
			BaseUI.baseStringCompare("Auction Ended", expectedAuctionEnded_Formatted, actualAuctionEnded);
		}
	}

	public static void expand_ClosedAuction() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("openAuct_Auctions_FirstAuction"));
		BaseUI.waitForElementToBeDisplayed("closedAuctions_ExpandView_PointsBid", null, null, 30);
		Thread.sleep(500);
	}

	public static void click_ViewAuctionsBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("closedAuctions_ViewAuctionsBtn"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_Header", null, null, 30);
		Thread.sleep(1000);
	}

	public static void click_ViewDetailsBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_ViewDetailsBtn"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("myAuct_OrderAndShip_OrderDetailsHeader", null, null, 30);
		Thread.sleep(1000);
	}

	public static void click_ClaimMyAuctionsBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("closedAuctions_ClaimNowLandingPage_ClaimMyAuctions"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("closedAuctions_ClaimedConfirmationPage_Header", null, null, 30);
		Thread.sleep(1000);
	}

	public static void click_ClaimBtn() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_ClaimBtn"));
		BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("closedAuctions_ClaimNowLandingPage_CongratulationHeaderMsg", null, null, 30);
		Thread.sleep(1000);
	}

	public static void clickIAgreeCheckbox() throws Exception {
		BaseUI.click(Locator.lookupRequiredElement("closedAuctions_ClaimNowLandingPage_IAgreeCheckbox"));
		Thread.sleep(300);
	}

	public static void claimAuctions_VerifyOrderDetails(String closedAuctionsTitle) throws Exception {
		Auctions_ClosedAuctions.click_ClaimBtn();
		clickIAgreeCheckbox();
		click_ClaimMyAuctionsBtn();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("closedAuctions_ClaimedConfirmationPage_Header"));
		BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_Header", "You've claimed your auction!");
	}

	public static void verifyClosedAuctions_ConfirmationPage_ShippingDetails(String username, String address1, String cityStateZipCode, String country) {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("closedAuctions_ClaimedConfirmationPage_OrderConfirmation_ShippingAddressHeader"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("closedAuctions_ClaimNowLandingPage_BuyerName"));
		BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimNowLandingPage_ShippingAddress1", address1);
		BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimNowLandingPage_ShippingCityState_Zip",
				cityStateZipCode.replace("****", "*****"));
		BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimNowLandingPage_ShippingCountry", country);
	}

	public static void verifyClosedAuctions_OrderDetails_AuctionsDetails(String closedAuction_ProductName, String pointsBid, String retailPrice,
																		 String quantity) {
		if(BaseUI.elementExists("closedAuctions_ClaimedConfirmationPage_Header", null, null)) {
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_OrderConfirmation_ProductTitle", closedAuction_ProductName);
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_OrderConfirmation_PointsBid", pointsBid);
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_OrderConfirmation_RetailsPrice", retailPrice);
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_OrderConfirmation_Quantity", quantity);
		} else {
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedDetailsPage_OrderDetails_ProductTitle", closedAuction_ProductName);
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedDetailsPage_OrderDetails_PointsBid",
					"after "+pointsBid+" points");
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedDetailsPage_OrderDetails_RetailsPrice",
					"Retail: "+ retailPrice.replace(" Retail Value", "") +".00");
			BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedDetailsPage_OrderDetails_Quantity", quantity);
		}
	}
}