package dRewards.ClassObjects;

public class CarRentalSearch_CheckoutDetails {
	
	
	public String airportConcessionFee;
    public String localTax;
    public String energy_RecoveryFee;
    public String stateSurcharge;
    public String tire_BatteryFee;
    public String vehLicenseFee;
    public String airportCityOtherSurcharge;
    
    public String avgBaseRate;
    public String points_OrSmartDollars;
    public String rentalCharge;
    public String totalWithTax;
    public String taxes_OrFees;

}
