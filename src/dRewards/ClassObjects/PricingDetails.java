package dRewards.ClassObjects;

import java.text.DecimalFormat;

import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;

public class PricingDetails {
	public String retailPrice;
	public String points;
	public String priceAfterPoints;
	public String savings_Percent;
	public String finalPoints_AfterAddingItem;
	public String finalRetailPrice_AfterAddingItem;
	public String savings;

	// Savings amount is Citi only.
	public String savingsAmount;
	public String shipping_Cost;
	public String order_Total;
	public String tax_Cost;
	public String allstate_Cash;

	public void add_Taxes_To_OrderTotal() {
		String orderTotal = order_Total.replace("$", "").replace(",", "");
		String taxCost = tax_Cost.replace("$", "").replace(",", "");

		Double orderTotal_Double = Double.parseDouble(orderTotal);
		Double taxCost_Double = Double.parseDouble(taxCost);

		Double newTotal = orderTotal_Double + taxCost_Double;

		order_Total = "$" + BaseUI.convertDouble_ToString_ForCurrency(newTotal);
	}

	public void subtract_AllstateCash_From_OrderTotal() {
		if (ClientDataRetrieval.clientName.startsWith("Allstate Cash")) {
			String orderTotal = order_Total.replace("$", "").replace(",", "");
			String allstateCash = allstate_Cash.replace("$", "").replace(",", "");

			Double orderTotal_Double = Double.parseDouble(orderTotal);
			Double allstateCash_Double = Double.parseDouble(allstateCash);

			Double newTotal = orderTotal_Double - allstateCash_Double;

			order_Total = "$" + BaseUI.convertDouble_ToString_ForCurrency(newTotal);
		}
	}

	// Figure this stuff out before we get to Order Total increasing due to
	// taxes/shipping
	public void set_SavingsAmount_Citi() {
		if (ClientDataRetrieval.clientName.startsWith("Citi")) {

			Double retail_price = Double.parseDouble(retailPrice.replace("$", "").replace(",", ""));

			Double orderTotal = Double.parseDouble(order_Total.replace("$", "")
					.replace(",", "").replace("(", "").replace(")", ""));

			Double savingsDouble = retail_price - orderTotal;

			savingsAmount = BaseUI.convertDouble_ToString_ForCurrency(savingsDouble);
			savingsAmount = savingsAmount.split("\\.")[0];
		}
	}
	
	
	//Calculate the right points from item in cart added to final price/final points
	public static String validate_ItemsPoints_Subtracted_ToFinalPrice(String itemPoints, String finalPoints) {
			
		String validated_FinalPrice;
		Double evaluatedFinalPrice;
		Double itemPts = Double.parseDouble(itemPoints.replace(",", "")
				.replace("$", "").trim());//valueOf(itemPoints);
		Double finalPrice = Double.parseDouble(finalPoints.replace(",", "")
				.replace("$", "").trim());//valueOf(finalPoints);
			
		evaluatedFinalPrice =(finalPrice - Math.abs(itemPts));
		DecimalFormat format = new DecimalFormat("0.00");
        String evaluatedFinalPrice_formatted = format.format(evaluatedFinalPrice);
			
		//int evaluated_FinalPts = evaluatedFinalPoints.intValue();
		validated_FinalPrice = String.valueOf(evaluatedFinalPrice_formatted);
		return validated_FinalPrice;
	}
		
		
	//Calculate the right points from item in cart added to final price/final points
	public static String validate_ItemsPoints_Added_ToFinalPoints(String firstitemPoints, String seconditemPoints) {

		String nonStandard_Minus = "\u2013";
		String standard_Minus = "\u002D";

		firstitemPoints = firstitemPoints.replace(" ", "").replace("–","-");
		seconditemPoints = seconditemPoints.replace(" ", "").replace("–","-");

		String validated_FinalPoints;
		Double evaluatedFinalPoints;
		Double firstItemPts = Double.parseDouble(firstitemPoints.replace(",", "")
				.replace("$", "").replace(nonStandard_Minus,standard_Minus).trim());
		Double secondItemPts = Double.parseDouble(seconditemPoints.replace(",", "")
				.replace("$", "").replace(nonStandard_Minus,standard_Minus).trim());
					
		evaluatedFinalPoints =(firstItemPts + secondItemPts);
		DecimalFormat format = new DecimalFormat("0.00");
        String evaluatedFinalPoints_formatted = format.format(evaluatedFinalPoints);
		
		//int evaluated_FinalPts = evaluatedFinalPoints.intValue();
		validated_FinalPoints = String.valueOf(evaluatedFinalPoints_formatted);
		return validated_FinalPoints;
	}

}
