package dRewards.ClassObjects;

import utils.BaseUI;
import utils.Locator;

public class CarRentalSearchCriteria {

	public String airportCode_CityName;	
	public String cityNameDisplayed;
	public String carClassOptionDisplayed;
	public String pickUpDateDisplayed;
	public String returnDateDisplayed;
	public String pickUpTimeDisplayed;

	public String carRental_ReturnLocation;
	public String returnTimeDisplayed;
	
	public String pickUpOption = "Near an address";
	public String pickUpAddressOption;
	public String pickUpCityOption;
	public String pickUpZipOption;
	public String pickUpStateOption;
	
	public String returnOption;
	public String returnAddressOption;
	public String returnCityOption;
	public String returnZipOption;
	public String returnStateOption;
	public String cityNameSelected;
	public String carClassSelected;
	public String pickUpDateSelected;
	public String returnDateSelected;
	public String pickUpTimeSelected;
	public String returnTimeSelected; 
	
	
	public CarRentalSearchCriteria () {
		
	}
	
	
	public static CarRentalSearchCriteria storeCarRentalSearchCriteria() {
		
		CarRentalSearchCriteria carRentalSearch = new CarRentalSearchCriteria();
		
		carRentalSearch.cityNameSelected = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_Enter_PickUpCity_OrAirportCode"));
		carRentalSearch.carClassSelected = BaseUI.getSelectedOptionFromDropdown("carRental_CarClass");
		carRentalSearch.pickUpDateSelected = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_PickUpDate"));
		carRentalSearch.returnDateSelected = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_ReturnDate"));
		carRentalSearch.pickUpTimeSelected = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_PickUpTime_Dropdown"));
		carRentalSearch.returnTimeSelected = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_ReturnTime_Dropdown"));	
   	    
		return carRentalSearch;
				
	}
	
	
    public static CarRentalSearchCriteria storeCarRentalSearchResults() {
		
		CarRentalSearchCriteria carRentalSearchResults = new CarRentalSearchCriteria();
		
		carRentalSearchResults.cityNameDisplayed = BaseUI.getTextFromField(Locator.lookupElement("carRental_ModifySearchResults_Location"));
  		System.out.println(carRentalSearchResults.cityNameDisplayed);
  		carRentalSearchResults.carClassOptionDisplayed = BaseUI.getSelectedOptionFromDropdown("carRental_ModifySearchResults_CarClassOption");
  		carRentalSearchResults.pickUpDateDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_ModifySearchResults_PickUpDate"));
  		carRentalSearchResults.returnDateDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_ModifySearchResults_ReturnDate"));
  		carRentalSearchResults.pickUpTimeDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_ModifySearchResults_PickUpTime"));
  		carRentalSearchResults.returnTimeDisplayed = BaseUI.getTextFromInputBox(Locator.lookupElement("carRental_ModifySearchResults_ReturnTime"));
   	    
		return carRentalSearchResults;
				
	}	
 	
}
