 package dRewards.ClassObjects;

public class ShippingAddress {

	public String firstName;
	public String lastName;
	public String address1;
	public String address2;
	public String city;
	public String city1;
	public String state;
	public String zip;
	public String country;
	public String phoneNumber;

	public ShippingAddress() {
	}

	public ShippingAddress(String FirstName, String LastName, String Address1, String Address2, String City,
			String State, String Zip, String Country, String PhoneNumber) {
		firstName = FirstName;
		lastName = LastName;
		address1 = Address1;
		address2 = Address2;
		city = City;
		state = State;
		zip = Zip;
		country = Country;
		phoneNumber = PhoneNumber;
	}

}
