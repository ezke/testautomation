package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Auctions;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;
import java.util.List;

public class AuctionLandingPage_Validation extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Menu("Auctions");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step2_VerifyAuctions_NavigationMenu() throws Exception {
        Auctions.verify_AuctionsThreeTierNavigationMenu();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step2_VerifyAuctions_NavigationMenu_LiveAuctionsActiveTab() {
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_ActiveNavigationMenuTab"));
        BaseUI.verifyElementHasExpectedText("auctions_ActiveNavigationMenuTab", "Live Auctions");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step3_VerifyAuctions_TopOfPage_LeftNavigationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_TopOfPage_LeftNavigationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step3_VerifyAuctions_TopOfPage_RightNavigationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_TopOfPage_RightNavigationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step3_VerifyAuctions_BottomOfPage_LeftNavigationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_BottomOfPage_LeftNavigationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step3_VerifyAuctions_BottomOfPage_RightNavigationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_BottomOfPage_RightNavigationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step4_VerifyAuctions_LiveAuctionsPage_Dropdown() {
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_LiveAuctions_DropDown"));
        BaseUI.verifySelectedItemInDropdown(Locator.lookupElement("auctions_LiveAuctions_DropDown"), "All Auctions");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step5_VerifyUpcomingAuctionsPage() throws Exception {
        Auctions.clickAuctionsNavigationMenuTab("Upcoming Auctions");
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_UpcomingAuction_NotAvailableMsg"));
        BaseUI.verifyElementHasExpectedText("auctions_UpcomingAuction_NotAvailableMsg",
                "Sorry, there are no Upcoming Auctions at this time. Check back soon.");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4026_Step7_VerifyClosedAuctionsPage() throws Exception {
        Auctions.clickAuctionsNavigationMenuTab("Closed Auctions");
        BaseUI.verifyElementAppears(Locator.lookupElement("auctions_ClosedAuctionsList"));
        Auctions.verifyEachClosedAuctionsHasEndedHeader();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.navigate_Menu("Auctions");
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
