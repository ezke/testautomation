package dRewards.tests_regression;

import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Sweepstakes_UpcomingSweepstakes_Tests extends BaseTest {

    String randomNum = BaseUI.random_NumberAsString(40001, 50000);
    String sweepstakesName = "autDAF" + randomNum;
    String sweepstakesDescription = sweepstakesName;
    Boolean isFeaturedSweepstakes = false;
    String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
    String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    Integer hourToStart = 120;
    Integer minuteToStart = 45;
    Integer hourToLast = 180;
    Integer minutesToLast = 45;
    String ticketCost = "2.0";
    String cumulativeCap = "10";
    String docID = "385";
    String productSku = "ssdemochevycamerored";
    String position = "1";
    String dCash = "0";
    String quantity = "0";
    String numDays_Claim = "2";
    String ticketQty = "1";
    String ticketLimit = "100";
    String option = "Ticket Purchase";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        Navigation.navigate_Sweepstakes_UpcomingSweepstakes();
    }

    @Test(priority = 10, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3999_Step_UpcomingSweepstakes_Verify_Breadcrumb() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            String[] breadcrumb = {"Home", "Sweepstakes", "Upcoming Sweepstakes"};
            Navigation.verify_BreadcrumbList(breadcrumb);
        } else {
            String[] breadcrumb = {"Home", "Upcoming Sweepstakes"};
            Navigation.verify_BreadcrumbList(breadcrumb);
        }
    }

    // will fail if  there is a upcoming sweepstakes setup/active
    @Test(priority = 20, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3999_Step_UpcomingSweepstakes_Verify_No_Sweepstakes() throws Exception {
        Sweepstakes_Upcoming.verify_UpcomingSweepstakes_NoneAvailable();
    }

    // This a setup to Navigate to Admin site to create upcoming sweepstakes, creates upcoming sweepstakes
    @Test(priority = 30, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3999_Step_UpcomingSweepstakes_SetUp_UpcomingSweepstakes_AdminSite() throws Exception {

        Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
        LoginPage_Admin.login_Admin();

        Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
        Navigation_Admin.navigate_Sweepstakes_CreateProgram();

        AddNewSweepstakes_Admin.add_UpcomingSweepstakes(hourToStart, minuteToStart, hourToLast, minutesToLast,
                sweepstakesName, sweepstakesDescription, Client_Info_Admin.return_ProgramIDs(), isFeaturedSweepstakes,
                startDate, endDate, drawingDate, displayEndDate, ticketCost, cumulativeCap, docID, productSku, position,
                dCash, quantity, numDays_Claim, ticketQty, ticketLimit, option);

        // validate created sweepstakes displays in admin site
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();

        // validate created sweepstakes Pending Start
        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .click_ActivateButton_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_UpcomingSweepstakes_IsPendingStart();

        // launch memcache flush to update sweepstakes
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
    }

    @Test(priority = 40, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT30_PT3999_Step_UpcomingSweepstakes_Verify_UpcomingSweepstakes_DropdownList() throws Exception {

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Sweepstakes_UpcomingSweepstakes();
        Sweepstakes.verify_SweeptakesType_DropdownList();
    }

    @Test(priority = 50, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT30_PT3999_Step_Verify_Upcoming_Sweepstakes_Type_DefaultDropdown() throws Exception {

        if (Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.verifySelectedItemInDropdown(Locator.lookupElement("currentSweepstakes_SweeptakeType_DropDown"),
                    "Upcoming Sweepstakes");
        } else {
            BaseUI.verifySelectedItemInDropdown(Locator.lookupRequiredElement("currentSweepstakes_SweeptakeType_DropDown"),
                    "Upcoming  Sweepstakes");
        }
    }

    @Test(priority = 60, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3999_Step_Verify_Upcoming_Sweepstakes_SeeDetails() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            Browser.closeBrowser();
            Browser.openBrowser(ClientDataRetrieval.url);
            LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
            Sweepstakes.verifyupcomingSweepstakes_PageTitle_BySweepstakesName_Redeemed();
        } else {
            BaseUI.verifyElementAppears(Locator.lookupElement("upcomingSweepstakes_SeeDetails_Button_BySweepstakesName",
                    AddNewSweepstakes_Admin.entered_sweepstakesName, null));
        }
    }

    @Test(priority = 70, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT30_PT3999_Step_Verify_Upcoming_Sweepstakes_Verify_UpcomingSweepstakesPresent() throws Exception {

        Sweepstakes.verify_UpcomingSweepstakes_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {

        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
                Browser.closeBrowser();
                Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
            } else {
                Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            }
            LoginPage_Admin.login_Admin();
            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            AddNewSweepstakes_Admin.edit_Sweepstakes_ExpireTheSweepstake(sweepstakesName);
        } finally {
            Browser.closeBrowser();
        }
    }
}
