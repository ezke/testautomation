package dRewards.tests_regression;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account;
import dRewards.pages.Account_History;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.HotelsAndCondos_CancelReservationPage;
import dRewards.pages.HotelsAndCondos_CheckoutPage;
import dRewards.pages.HotelsAndCondos_OrderConfirmationPage;
import dRewards.pages.HotelsAndCondos_PropertyDetailPage;
import dRewards.pages.HotelsAndCondos_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


//@Test(groups = { "not_QA" })
public class HotelSearch_BookAHotel_DHISCO_Providers extends BaseTest {

	//Options for location to choose: Orlando, Las Vegas, Atlanta and San Antonio
	String location = "Miami, FL";
	String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(90);
	String fromDate_AccountHistoryFormat = BaseUI.getDateAsString_InRelationToTodaysDate(90, "yyyy-MM-dd");
	String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(92);
	String toDate_AccountHistoryFormat = BaseUI.getDateAsString_InRelationToTodaysDate(92, "yyyy-MM-dd");
	String numberOfAdults = "2";
	String numberOfChildren = "2";
	String ageOfChildren = "3";
	String hotelLogoURL = "";
	String checkoutAddress = "";
	HashMap<String, String> hotelList;
	String taxesAndFees = "";
	String finalCost = "";
	String pointsBefore = "";
	String pointsAfter = "";
	String allStateCashBefore = "";
	String allStateCashAfter = "";

	String reservationNumber = "";
	Double rewardsCashToDeduct = 100.00;
	String rewardsCashAsString = rewardsCashToDeduct.toString().split("\\.")[1].length() == 1
			? rewardsCashToDeduct.toString() + "0" : rewardsCashToDeduct.toString();
	String cancellationPenalty = "";
	String securityCode = "211";

	// Variables for New Guest Info
	String firstName = "Automation";
	String lastName = "Guest";
	String address1 = "1225 Hillsbery Ground";
	String city = "Wausau";
	String state = "WI";
	String zip = "53001";
	String phone = "7152456542";

	String addressCompiled = firstName + " " + lastName + "\n" + address1 + "\n" + city.toUpperCase() + ", " + state
			+ " " + "*****";
	

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all Shipping Info
		Navigation.navigate_Account_UpdateAddressBook();
		Account_UpdateAddressBook.delete_Address();

		// Logic to remove all Billing Info.
		Navigation.navigate_Account();
		Account.clear_PaymentHistory();
		Navigation.navigate_Home_viaLogo();

		Navigation.navigate_Hotel_Landingpage();

		HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
				ageOfChildren);

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			HotelsAndCondos_SearchResults.sortBy("Your Price - Low to High");
		} else {
			HotelsAndCondos_SearchResults.sortBy("You Pay - Low to High");
		}

		HotelsAndCondos_SearchResults.click_HotelName_ByDHISCO_Providers();
		pointsBefore = Navigation.returnPoints();
		allStateCashBefore = Navigation.returnAllState_Cash();
	}
	
	//add before method  if we land ao the error page 
//	@BeforeMethod(alwaysRun = true)
//	public void ShowStoper_KillTest(ITestResult result) throws Exception {
//
//		HotelsAndCondos.check_ForErrorPage_AndFailI();
//	}
	
	@Test(priority = 11, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step37_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_Dates() throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_checkin_And_checkout_Dates(fromDate, toDate);
	}

	@Test(priority = 12, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA"})
	public void PT3135_Step37_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_AdultsAndChildren_Dropdowns()
			throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_Adults_AndChildren_Dropdowns(numberOfAdults, numberOfChildren,
				ageOfChildren);
	}

	@Test(priority = 13, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step37_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_BookingDetails_FromAndToDates()
			throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_BookingDetails_CheckinAndCheckout_Dates(fromDate, toDate);
	}

	@Test(priority = 14, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step37_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_BookingDetails_AdultAndChildCounts()
			throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_BookingDetails_AdultsAndChildren(numberOfAdults, numberOfChildren,
				ageOfChildren);
	}

	@Test(priority = 15, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step37_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_BookItNowAppears()
			throws Exception {

		hotelList = HotelsAndCondos_PropertyDetailPage.return_HotelInfo_ByHotelNameSelected();
		hotelLogoURL = HotelsAndCondos_PropertyDetailPage.return_ImageURL();
		HotelsAndCondos_PropertyDetailPage.verify_Top_BookItNow_ButtonAppears();
	}

	@Test(priority = 16, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step37_HotelsAndCondos_PropertyDetailsPage_BookItNow_And_HotelLogo() throws Exception {
		hotelLogoURL = hotelLogoURL.replace("http:", "");
		HotelsAndCondos_PropertyDetailPage.click_TOP_BookItNow_Button();
		HotelsAndCondos_CheckoutPage.verify_HotelLogo_Accurate(hotelLogoURL);
	}

	@Test(priority = 21, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step38_HotelsAndCondos_CheckoutPageValidation_CheckinAndCheckout_Dates() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_Checkin_And_Checkout_Dates(fromDate, toDate);
	}

	@Test(priority = 22, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step38_HotelsAndCondos_CheckoutPageValidation_AdultAndChildCounts() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_GuestInfo_AdultsAndChildren(numberOfAdults, numberOfChildren,
				ageOfChildren);
	}

	@Test(priority = 23, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step38_HotelsAndCondos_CheckoutPageValidation_HotelAddress() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_HotelAddress_Info(hotelList);
	}

	@Test(priority = 24, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step38_HotelsAndCondos_CheckoutPageValidation_Hotel_Points_And_Totals() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_HotelTotals(hotelList);
	}

	@Test(priority = 25, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" })
	public void PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo() throws Exception {
		// Adding new Guest Contact information
		HotelsAndCondos_CheckoutPage.add_NewGuestContact_Information(firstName, lastName, address1, city, state, zip,
				phone);
		HotelsAndCondos_CheckoutPage.add_RewardsCash(rewardsCashToDeduct);
		HotelsAndCondos_CheckoutPage.click_ContinueToPaymentInfo();

		// Adding new payment option using Dafault Payment info
		HotelsAndCondos_CheckoutPage.add_newPaymentOption_UsingDefaults();

		HotelsAndCondos_OrderConfirmationPage.verify_ApplicationError_NOT_Present();
	}

	@Test(priority = 30, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_HotelsAndCondos_NewPaymentOption_AndConfirmation_hotelLogo() throws Exception {
		pointsAfter = Navigation.return_ExpectedPoints_AsString(pointsBefore, hotelList.get("points"));
		allStateCashAfter = Navigation.return_Expected_AllStateCash_AsString(allStateCashBefore,
				rewardsCashToDeduct.toString());


		finalCost = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_ACTUALTotalCost"));
		reservationNumber = HotelsAndCondos_OrderConfirmationPage.return_ReservationNumber();
		taxesAndFees = BaseUI.getTextFromField(Locator.lookupRequiredElement("hotelConfirmation_TaxesAndFees"));

		HotelsAndCondos_OrderConfirmationPage.verify_HotelLogo_Accurate(hotelLogoURL);
	}

	@Test(priority = 31, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_HotelsAndCondos_NewPaymentOption_AndConfirmation_PointsMatched() throws Exception {
		Navigation.verify_pointsMatchExpected(pointsAfter);

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Navigation.verify_AllStateCash_MatchesExpected(allStateCashAfter);
		}
	}

	@Test(priority = 32, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_HotelsAndCondos_NewPaymentOption_AndConfirmation_HotelAddress() throws Exception {

		HotelsAndCondos_OrderConfirmationPage.verify_HotelAddress_Info(hotelList);
	}

	@Test(priority = 33, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_HotelsAndCondos_NewPaymentOption_AndConfirmation_GuestInfo() throws Exception {

		HotelsAndCondos_OrderConfirmationPage.verify_GuestInfo(numberOfAdults, numberOfChildren, ageOfChildren);
	}

	@Test(priority = 34, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_HotelsAndCondos_NewPaymentOption_AndConfirmation_HotelCosts() throws Exception {

		HotelsAndCondos_OrderConfirmationPage.verify_Hotel_Costs(hotelList);
	}

	@Test(priority = 35, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_AccountHistory_CancelBooking() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			Navigation.navigate_Home_viaHomeLink();
			Navigation.navigateToMyRewards();
		} else {
			Navigation.navigate_Account();
		}

		Account_History.Verify_HotelBooking(fromDate_AccountHistoryFormat, toDate_AccountHistoryFormat,
				hotelList.get("points"), hotelList.get("hotelName"), rewardsCashAsString);
		Navigation.verify_pointsMatchExpected(pointsAfter);
	}

	@Test(priority = 40, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_CancelBooking_CancellationNumber() throws Exception {
		Account.click_CancelReservation();

		HotelsAndCondos_CancelReservationPage.validate_cancellation_Information(hotelList, fromDate, toDate,
				numberOfAdults, numberOfChildren, addressCompiled, taxesAndFees, finalCost);

		HotelsAndCondos_CancelReservationPage.verify_CancellationNumber(reservationNumber);
	}

	@Test(priority = 41, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_CancelBooking_HotelInfo() throws Exception {
		HotelsAndCondos_CancelReservationPage.verify_Hotel_Information(hotelList, fromDate, toDate);
	}

	@Test(priority = 42, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_CancelBooking_GuestInfo() throws Exception {

		HotelsAndCondos_CancelReservationPage.verify_GuestInfo(numberOfAdults, numberOfChildren, fromDate, toDate,
				addressCompiled);
	}

	@Test(priority = 43, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_CancelBooking_PayInfo() throws Exception {
		cancellationPenalty = HotelsAndCondos_CancelReservationPage.return_CancellationPenalty();

		HotelsAndCondos_CancelReservationPage.verify_AllStateCash_Information(rewardsCashAsString,
				hotelList.get("hotelRate"), hotelList.get("points"));

		HotelsAndCondos_CancelReservationPage.verify_Cancellation_PaymentInfo(hotelList, taxesAndFees, finalCost);
	}

	@Test(priority = 44, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_CancelBooking_SubmitButton_Exists() throws Exception {

		HotelsAndCondos_CancelReservationPage.verify_SubmitButton_Exists();
	}

	@Test(priority = 50, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_SubmitCancellation() throws Exception {

		String guestNameCompiled = firstName + " " + lastName;
		if(ClientDataRetrieval.isRedesignClient()) {
			HotelsAndCondos_CancelReservationPage.cancelHotelBooking();
		} else {
			HotelsAndCondos_CancelReservationPage.click_SubmitCancellation();
		}
		HotelsAndCondos_CancelReservationPage.verify_Cancellation_Successful();

		// Validation for AllStateCash
		HotelsAndCondos_CancelReservationPage.verify_AllStateCash_Cancellation_Successful(hotelList, fromDate, toDate,
				numberOfAdults, numberOfChildren, guestNameCompiled, cancellationPenalty, rewardsCashAsString,
				finalCost, hotelLogoURL);
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			if (cancellationPenalty.equals("$0.00")) {
				Navigation.verify_AllStateCash_MatchesExpected(allStateCashBefore);
			}
		}

		// Validation to run if we're checking AllState
		HotelsAndCondos_CancelReservationPage.verify_AllState_Cancellation_Successful(hotelList, fromDate, toDate,
				numberOfAdults, numberOfChildren, guestNameCompiled, finalCost, hotelLogoURL);

		Navigation.verify_pointsMatchExpected(pointsBefore);
	}

	// These tests are for our second run through for the Hotel and Condos
	// workflow.
	// This run will be using the Guest and Payment information that we used
	// for the first run through.

	@Test(priority = 60, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_HotelsAndCondos_Setup_SecondRun_Preexisting_GuestAndBilling_Info() throws Exception {
		Navigation.navigate_Hotel_Landingpage();

		HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
				ageOfChildren);

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			HotelsAndCondos_SearchResults.sortBy("Your Price - Low to High");
		} else {
			HotelsAndCondos_SearchResults.sortBy("You Pay - Low to High");
		}

		HotelsAndCondos_SearchResults.click_HotelName_ByDHISCO_Providers();

		pointsBefore = Navigation.returnPoints();
		allStateCashBefore = Navigation.returnAllState_Cash();
	}

	@Test(priority = 70, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step37_2ndRun_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_Dates() throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_checkin_And_checkout_Dates(fromDate, toDate);
	}

	@Test(priority = 72, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step37_2ndRun_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_AdultsAndChildren_Dropdowns()
			throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_Adults_AndChildren_Dropdowns(numberOfAdults, numberOfChildren,
				ageOfChildren);
	}

	@Test(priority = 73, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step37_2ndRun_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_BookingDetails_FromAndToDates()
			throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_BookingDetails_CheckinAndCheckout_Dates(fromDate, toDate);
	}

	@Test(priority = 74, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step37_2ndRun_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_BookingDetails_AdultAndChildCounts()
			throws Exception {
		HotelsAndCondos_PropertyDetailPage.verify_BookingDetails_AdultsAndChildren(numberOfAdults, numberOfChildren,
				ageOfChildren);
	}

	@Test(priority = 75, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step37_2ndRun_HotelsAndCondos_PropertyDetailsPage_CheckinAndCheckout_BookingDetails()
			throws Exception {
		
		hotelList = HotelsAndCondos_PropertyDetailPage.return_HotelInfo_ByHotelNameSelected();
		hotelLogoURL = HotelsAndCondos_PropertyDetailPage.return_ImageURL();
		HotelsAndCondos_PropertyDetailPage.verify_Top_BookItNow_ButtonAppears();
	}


	@Test(priority = 80, groups = { "regression_Tests", "critical_Tests", "all_tests" , "not_QA"},
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_2ndRun_HotelsAndCondos_CheckoutPageValidation_HotelLogo() throws Exception {
		hotelLogoURL = hotelLogoURL.replace("http:", "");
		HotelsAndCondos_PropertyDetailPage.click_TOP_BookItNow_Button();
		HotelsAndCondos_CheckoutPage.verify_HotelLogo_Accurate(hotelLogoURL);
	}

	@Test(priority = 81, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step38_2ndRun_HotelsAndCondos_CheckoutPageValidation_CheckinAndCheckout_Dates()
			throws Exception {
		HotelsAndCondos_CheckoutPage.verify_Checkin_And_Checkout_Dates(fromDate, toDate);
	}

	@Test(priority = 82, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step38_2ndRun_HotelsAndCondos_CheckoutPageValidation_AdultAndChildCounts() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_GuestInfo_AdultsAndChildren(numberOfAdults, numberOfChildren,
				ageOfChildren);
	}

	@Test(priority = 83, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step38_2ndRun_HotelsAndCondos_CheckoutPageValidation_HotelAddress() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_HotelAddress_Info(hotelList);
	}

	@Test(priority = 84, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step38_2ndRun_HotelsAndCondos_CheckoutPageValidation_Hotel_Points_And_Totals() throws Exception {
		HotelsAndCondos_CheckoutPage.verify_HotelTotals(hotelList);
	}

	@Test(priority = 85, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step38_2ndRun_HotelsAndCondos_CheckoutPage_UseExisting_GuestInfoAndPaymentInfo()
			throws Exception {

		checkoutAddress = HotelsAndCondos_CheckoutPage.return_AddressInfo_String();
		HotelsAndCondos_CheckoutPage.add_RewardsCash(rewardsCashToDeduct);
		HotelsAndCondos_CheckoutPage.click_ContinueToPaymentInfo();
		HotelsAndCondos_CheckoutPage.add_Default_CVVCode(securityCode);
		HotelsAndCondos_CheckoutPage.check_CheckoutPay_Acknowledgement_Chckbox();
		HotelsAndCondos_CheckoutPage.click_CompleteYourOrder();

		pointsAfter = Navigation.return_ExpectedPoints_AsString(pointsBefore, hotelList.get("points"));
		allStateCashAfter = Navigation.return_Expected_AllStateCash_AsString(allStateCashBefore,
				rewardsCashToDeduct.toString());

		finalCost = BaseUI.getTextFromField(Locator.lookupElement("hotelConfirmation_ACTUALTotalCost"));
		reservationNumber = HotelsAndCondos_OrderConfirmationPage.return_ReservationNumber();
		taxesAndFees = BaseUI.getTextFromField(Locator.lookupElement("hotelConfirmation_TaxesAndFees"));
	}

	@Test(priority = 90, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_2ndRun_HotelsAndCondos_ExistingPaymentOption_AndConfirmation_hotelLogo()
			throws Exception {
		HotelsAndCondos_OrderConfirmationPage.verify_HotelLogo_Accurate(hotelLogoURL);
	}

	@Test(priority = 91, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_2ndRun_HotelsAndCondos_ExistingPaymentOption_AndConfirmation_PointsMatched()
			throws Exception {
		Navigation.verify_pointsMatchExpected(pointsAfter);

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Navigation.verify_AllStateCash_MatchesExpected(allStateCashAfter);
		}
	}

	@Test(priority = 92, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_2ndRun_HotelsAndCondos_ExistingPaymentOption_AndConfirmation_HotelAddress()
			throws Exception {
		HotelsAndCondos_OrderConfirmationPage.verify_HotelAddress_Info(hotelList);
	}

	@Test(priority = 93, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_2ndRun_HotelsAndCondos_ExistingPaymentOption_AndConfirmation_GuestInfo()
			throws Exception {
		HotelsAndCondos_OrderConfirmationPage.verify_GuestInfo(numberOfAdults, numberOfChildren, ageOfChildren);
	}

	@Test(priority = 94, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step39_2ndRun_HotelsAndCondos_ExistingPaymentOption_AndConfirmation_HotelCosts()
			throws Exception {
		HotelsAndCondos_OrderConfirmationPage.verify_Hotel_Costs(hotelList);
	}

	@Test(priority = 95, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_AccountHistory_CancelBooking() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			Navigation.navigate_Home_viaHomeLink();
			Navigation.navigateToMyRewards();
		} else {
			Navigation.navigate_Account();
		}

		Account_History.Verify_HotelBooking(fromDate_AccountHistoryFormat, toDate_AccountHistoryFormat,
				hotelList.get("points"), hotelList.get("hotelName"), rewardsCashAsString);
		Navigation.verify_pointsMatchExpected(pointsAfter);
	}

	@Test(priority = 100, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_CancelBooking_CancellationNumber() throws Exception {

		Account.click_CancelReservation();

		HotelsAndCondos_CancelReservationPage.validate_cancellation_Information(hotelList, fromDate, toDate,
				numberOfAdults, numberOfChildren, checkoutAddress, taxesAndFees, finalCost);

		HotelsAndCondos_CancelReservationPage.verify_CancellationNumber(reservationNumber);
	}

	@Test(priority = 101, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_CancelBooking_HotelInfo() throws Exception {
		HotelsAndCondos_CancelReservationPage.verify_Hotel_Information(hotelList, fromDate, toDate);
	}

	@Test(priority = 102, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_CancelBooking_GuestInfo() throws Exception {

		HotelsAndCondos_CancelReservationPage.verify_GuestInfo(numberOfAdults, numberOfChildren, fromDate, toDate,
				checkoutAddress);
	}

	@Test(priority = 103, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_CancelBooking_PayInfo() throws Exception {
		cancellationPenalty = HotelsAndCondos_CancelReservationPage.return_CancellationPenalty();

		HotelsAndCondos_CancelReservationPage.verify_AllStateCash_Information(rewardsCashAsString,
				hotelList.get("hotelRate"), hotelList.get("points"));

		HotelsAndCondos_CancelReservationPage.verify_Cancellation_PaymentInfo(hotelList, taxesAndFees, finalCost);
	}

	@Test(priority = 104, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA", "not_aarpredesign" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_CancelBooking_SubmitButton_Exists() throws Exception {
		HotelsAndCondos_CancelReservationPage.verify_SubmitButton_Exists();
	}

	@Test(priority = 105, groups = { "regression_Tests", "critical_Tests", "all_tests", "not_QA" },
			dependsOnMethods = {"PT3135_Step38_HotelsAndCondos_CheckoutPage_Add_NewGuestInfoAndNewPaymentInfo"})
	public void PT3135_Step47_2ndRun_HotelsAndCondos_SubmitCancellation() throws Exception {

		if(ClientDataRetrieval.isRedesignClient()) {
			HotelsAndCondos_CancelReservationPage.cancelHotelBooking();
		} else {
			HotelsAndCondos_CancelReservationPage.click_SubmitCancellation();
		}
		HotelsAndCondos_CancelReservationPage.verify_Cancellation_Successful();

		// Validation for AllStateCash
		HotelsAndCondos_CancelReservationPage.verify_AllStateCash_Cancellation_Successful(hotelList, fromDate, toDate,
				numberOfAdults, numberOfChildren, checkoutAddress, cancellationPenalty, rewardsCashAsString, finalCost,
				hotelLogoURL);
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			if (cancellationPenalty.equals("$0.00")) {
				Navigation.verify_AllStateCash_MatchesExpected(allStateCashBefore);
			}
		}

		// Validation to run if we're checking AllState
		HotelsAndCondos_CancelReservationPage.verify_AllState_Cancellation_Successful(hotelList, fromDate, toDate,
				numberOfAdults, numberOfChildren, checkoutAddress, finalCost, hotelLogoURL);

		Navigation.verify_pointsMatchExpected(pointsBefore);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}

}
