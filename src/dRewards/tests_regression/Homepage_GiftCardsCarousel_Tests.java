package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Homepage_GiftCardsCarousel_Tests extends BaseTest {
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}
	
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_Verify_GiftCardsCarousel_Header_Appears() throws Exception {
		Homepage.verify_GiftCardsCarousel_Header_Appears();

	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_Verify_GiftCardsCarousel_SeeMoreLink_Appears() throws Exception {
		Homepage.verify_GiftCardsCarousel_SeeMoreLink_Appears();

	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step19_Verify_GiftCardsCarousel_RightNavigationArrow_Appears() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_GiftCardsCarousel_RightNavigation"));
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_Verify_GiftCardsCarousel_LeftNavigationArrow_Appears() throws Exception {
		Homepage.verify_GiftCardsCarousel_LeftNavigationArrow_Appears();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_GiftCardsCarousel_Carousel_Click_Entire_RightNavigationArrow()
			throws Exception {
		Homepage.click_NavigateGiftCardsCarousel_ToFarRight();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_GiftCardsCarousel_Carousel_Click_Entire_LeftNavigationArrow()
			throws Exception {
		Homepage.click_NavigateGiftCardsCarousel_ToFarLeft();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_Verify_GiftCardsCarousel_Verify_Clicking_GiftCardImage_Navigates_ToLandingPage()
			throws Exception {
		Homepage.clickAndVerify_GiftCardsCarousel_Displayed_NavigatesTo_LandingPage("homepage_GiftCardsCarousel_Macys_GiftCardImage");
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise","not_mcafee"})
	public void PT3214_HomePage_Step22_Verify_GiftCardsCarousel_SeeMoreLink_NavigatesTo_LandingPage() throws Exception {
		Homepage.verify_click_SeeMoreLink_NavigatesTo_GiftCardsCarousel_LandingPage();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
}
