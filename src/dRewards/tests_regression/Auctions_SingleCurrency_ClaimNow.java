package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.AuctionTile;
import dRewards.pages.*;
import dRewards.pages_Administration.*;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

//@Test(groups = { "not_IE" })
public class Auctions_SingleCurrency_ClaimNow extends BaseTest {

    private WebDriver originalDriver;
    private WebDriver secondBidderDriver;
    private String original_Browser;

    private String auctionName = "AutAuc" + BaseUI.random_NumberAsString(1000, 9999);
    private Double startingBid = 50.00;
    private Double currentBid = 50.00;
    // Extend Time is in seconds.
    private Integer extendTime = 20;
    // String productSku = "aucdysonDC44demo";
    private String productSku = "auc46samsungsmarthdtvDEMO";
    // String expectedProductName = "DC44 Animal Digital Slim Cordless Vacuum
    // with Handheld Tool Kit - DEMO ONLY";
    private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
    private Boolean isDemo = true;

    private AuctionTile auctionTile = AuctionTile.empty();
    private String expectedEndTime = "";
    // Auction time in Minutes
    private Integer auctionTimeToLast = 4;
    private HashMap<String, String> shippingInfo;
    private HashMap<String, String> auctionEndInfo;
    private String retailPrice;

    private String user1_previousPoints;
    private String user2_previousPoints;
    private String orderNumber;
    private TableData bidHistory;
    private Integer numberOfBidders = 2;
    private Integer numberOfBids = 3;
    private String quantity = "1000";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        bidHistory = new TableData();
        if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
            auctionTimeToLast += 60;
        }

        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        original_Browser = Browser.currentBrowser;
        originalDriver = Browser.driver;
        // Adding additional time for when tests run in Internet Explorer.
        if (Browser.currentBrowser.equals("internetexplorer")) {
            auctionTimeToLast += 1;
        }
        // Adding additional time for when tests run in AARP
        if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
            auctionTimeToLast += 1;
        }

        LoginPage_Admin.login_Admin();
        EditBuyerProgram_AdminSite.UpdateWinLimit_Attribute_13011();

        Browser.driver = originalDriver;
        Navigation_Admin.navigate_ProductCatalog_Catalog();
        ProductCatalog_CatalogPage.search_Product_BySku(productSku);
        ProductCatalog_CatalogPage.update_Product_Inventory(quantity);

        Navigation_Admin.navigate_ClickNavLink("Create Auction");
        AddNewAuction_Admin.Add_New_Auction(auctionName, RedemptionTypes.FullRedemption, startingBid, extendTime,
                productSku, AuctionTypes.Standard, isDemo);
        expectedEndTime = Auctions.return_Auction_TimeLeftFormat_forCheckout(auctionTimeToLast);

        AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
        AddNewAuction_Admin.initialize_Auction();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Auctions_AllAuctions();
        user1_previousPoints = Navigation.returnPoints();
    }

    @Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests",
            "all_tests", "not_IE"})
    public void PT3664_PT4743_Step3_Verify_Auctions_DetailsPage() throws Exception {

        String expectedPointsOrRewards = "Enter an amount that is higher than the current bid"; //+ ClientDataRetrieval.client_Currency + "";

        if (ClientDataRetrieval.isRedesignClient()) {
            expectedPointsOrRewards = "Enter a Points amount that is at least 1 higher than the current bid.";
        }
        auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
        auctionTile.click_DetailsButton();
        Auction_Details.verify_AuctionDetails_SingleCurrency(currentBid, expectedPointsOrRewards);
        Auction_Details.verify_TimeLeft_ContainsCorrectFormat();
    }

    @Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests",
            "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_1stUserBid_Successful() throws Exception {

        currentBid += 1;
        retailPrice = Auction_Details.return_RetailPrice();
        bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
        Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(0).get("bidAmount"));
        Auction_Details.verify_BidSuccessful(bidHistory.data.get(0).get("bidAmount"));
        Navigation.verify_pointsMatchExpected(
                Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
        Auction_Details.verify_BidBox_Links_Appears();
    }

    @Test(priority = 30, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests",
            "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_2ndUserBid_Successful() throws Exception {

        // Set up Second Bidder
        originalDriver = Browser.driver;
        ClientDataRetrieval.set_secondaryUser_Info(original_Browser);
        Browser.openBrowser(ClientDataRetrieval.url, "chrome");
        secondBidderDriver = Browser.driver;
        
        if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            LoginPage.loginBypassAARPRedesign(ClientDataRetrieval.secondaryUser_AARP_BpassAuction, ClientDataRetrieval.secondaryPassword_AARP_BpassAuction);
        }else {
            LoginPage.login(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
        }
        Navigation.navigate_Auctions_AllAuctions();
        auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, currentBid);
        currentBid += 1;
        user2_previousPoints = Navigation.returnPoints();

        auctionTile.click_DetailsButton();
        bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(false, String.valueOf(currentBid)));
        Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(1).get("bidAmount"));
        Auction_Details.verify_LeadingBidder(bidHistory.data.get(1).get("bidAmount"));
        Navigation.verify_pointsMatchExpected(
                Navigation.return_ExpectedPoints_AsString(user2_previousPoints, currentBid.toString()));
        Auction_Details.verify_BidBox_Links_Appears();
    }

    @Test(priority = 40, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_1stBidder_WasOutbid() throws Exception {

        Browser.driver = originalDriver;
        Auction_Details.verify_Outbid(user1_previousPoints);
        BaseUI.baseStringCompare("Points match previous count.", user1_previousPoints, Navigation.returnPoints());
    }

    @Test(priority = 50, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_1stBidder_CounterBidSuccessful() throws Exception {

        currentBid += 1;
        Browser.driver = originalDriver;
        bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
        Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(2).get("bidAmount"));
        Auction_Details.verify_BidSuccessful(bidHistory.data.get(2).get("bidAmount"));
        Navigation.verify_pointsMatchExpected(
                Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
        Auction_Details.verify_BidBox_Links_Appears();

        if (ClientDataRetrieval.isRedesignClient()) {
            String redesignText = "View Total Bids (3)";
            BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", redesignText);
        } else {
            BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", numberOfBids.toString());
        }
        //	BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", numberOfBids.toString());
    }

    @Test(priority = 60, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_2ndBidder_WasOutbid() throws Exception {

        Browser.driver = secondBidderDriver;
        Auction_Details.verify_Outbid(user2_previousPoints);
    }

    @Test(priority = 70, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_ReviewBidHistory() throws Exception {

        Browser.driver = originalDriver;
        Auction_Details.click_View_BidHistory();
        Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
    }

    @Test(priority = 80, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Close_BidHistory_Window() throws Exception {

        Auction_Details.click_Close_BidHistoryWindow();
        BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
    }

    //Step 12
    @Test(priority = 90, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_MyAccount_OpenAuctions_BidHistory_And_Description() throws Exception {

        Navigation.navigate_Account_OpenAuctions();
        Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();

        if (ClientDataRetrieval.isRedesignClient()) {
            Auction_Details.return_BidHistoryDataMyAccountRedesign();
        } else {
            Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
        }

        //Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
        //Auctions_OpenAuctions.verify_Description(expectedProductName);

    }

    @Test(priority = 100, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE", "not_redesign", "not_aarpredesign"})
    public void PT3664_PT4743_Step6To12_Auction_OpenAuctions_Close_BidHistory_Window() throws Exception {

        Auction_Details.click_Close_BidHistoryWindow();
        BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
    }

    @Test(priority = 110, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_PopupAppears() throws Exception {

        Browser.driver = originalDriver;
        Navigation.navigate_Auctions_AllAuctions();
        auctionTile.click_DetailsButton();
            Auction_Details.wait_For_End_OfAuction(auctionTimeToLast * 60);
        if (!ClientDataRetrieval.isRedesignClient()) {
            Auction_Details.wait_For_ClaimPopup_ToAppear();
            Auction_Details.verify_AuctionWon_PopupAppears();
        }
    }

    @Test(priority = 120, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_WonBannerAppears() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Logout();
            Browser.navigateTo(ClientDataRetrieval.url);
            LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
            Navigation.navigate_MyAccount_CloseAuctionsRedesign();

            Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();
            Navigation.clickMyAccount_CloseAuctionsRedesign();
        }

        Auction_Details.verify_AuctionWon_Banner_Appears();
    }

    
    @Test(priority = 130, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_ClaimAuction_ShippingPage() throws Exception {

        Auction_Details.click_ClaimNow_InWinningPopup();
        if (BaseUI.elementExists("auctShip_1stAddress_FirstName", null, null)) {
            shippingInfo = Auctions_Checkout_Shipping.return_ShippingInfo();
        } else {
            shippingInfo = CheckoutPage_ShippingInfo.return_DefaultShippingInfo();
        }
         if  (ClientDataRetrieval.isRedesignClient()){
            auctionEndInfo = Auctions_Checkout_Shipping.return_AuctionEndInfoRedesign();
        }else {
            auctionEndInfo = Auctions_Checkout_Shipping.return_AuctionEndInfo();
        }
        Auctions_Checkout_Shipping.verify_AuctionInfo(expectedProductName, currentBid.toString(), expectedEndTime);
    }

    //Step 11
    @Test(priority = 140, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_2ndUser_DidNotWin() throws Exception {
        String expectedUserName = "";

        if (ClientDataRetrieval.isRedesignClient()) {

            Browser.driver = secondBidderDriver;
        } else {
            Browser.driver = originalDriver;
            expectedUserName = Auction_Details.return_UserName();

            Browser.driver = secondBidderDriver;
            String winnerName = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_Winner_Name")).toLowerCase();
            BaseUI.baseStringPartialCompare("WinnerName", expectedUserName, winnerName);
        }
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_MyAccount_CloseAuctionsRedesign();
            Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();
            Navigation.clickMyAccount_CloseAuctionsRedesign();
        }

        Auction_Details.verify_User_DidNot_Win_Auction(bidHistory.data.get(0).get("bidAmount"));
    }

    @Test(priority = 150, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_OrderReview() throws Exception {

        Browser.driver = originalDriver;

        if (!ClientDataRetrieval.isRedesignClient()) {
            Auctions_Checkout_Shipping.navigate_To_NextPage();
        }
        Auctions_Checkout_OrderReview.verify_OrderReview_Info(shippingInfo, expectedProductName, retailPrice,
                expectedEndTime, currentBid.toString());
    }

    @Test(priority = 160, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_OrderComplete() throws Exception {

        Auctions_Checkout_OrderReview.click_AcceptConditions_AndCompleteOrder();
        orderNumber = Auctions_Checkout_OrderComplete.return_OrderNumber();

        Auctions_Checkout_OrderComplete.verify_Order_Info(shippingInfo, expectedProductName, retailPrice,
                expectedEndTime, currentBid.toString());
    }

    @Test(priority = 170, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE", "not_redesign", "not_aarpredesign"})
    public void PT3664_PT4743_Step6To12_Verify_OrderComplete_PrintReceiptVisible() throws Exception {

        Auctions_Checkout_OrderComplete.verify_PrintYourReceiptButton_VisibleAndEnabled();
    }

    // Test for non-allstateCash
    @Test(priority = 180, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_OrderComplete_PointsCorrect()
            throws Exception {

        Navigation.verify_pointsMatchExpected(
                Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
    }

    @Test(priority = 190, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_AuctionEnds_1stUserWins_Validate_MyAuction() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_MyAccount_CloseAuctionsRedesign();
            Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();
            Navigation.clickMyAccount_CloseAuctionsRedesign();
        }else {
            Navigation.navigate_Auctions_MyAuctions();
        }
        Auctions_MyAuctions.verify_AuctionWon_AndAlreadyClaimed(expectedProductName, currentBid,
                auctionEndInfo.get("auctionEnded"));
    }

    @Test(priority = 200, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Navigate_AuctionEnds_1stUserWins_Validate_MyAuction_ViewDetails() throws Exception {
        Auctions_MyAuctions.click_LastAuction_ViewDetails();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAuct_OrderAndShip_Header"));
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedPartialText("myAuct_OrderAndShip_Header", "Order Details");
        } else {
            BaseUI.verifyElementHasExpectedPartialText("myAuct_OrderAndShip_Header", "Order & Shipping Details");
        }
    }
    @Test(priority = 210, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_History_OrderAndShippingDetails_PaymentInfo() throws Exception {
//        Account_OrderAndShippingDetails_Page.verify_Auction_FullRedemption_PaymentInformation(orderNumber,
//                BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM-dd-yyyy"),
//                BaseUI.convertDouble_ToString_ForCurrency(currentBid));


        if (ClientDataRetrieval.isRedesignClient()) {

            Account_OrderAndShippingDetails_Page.verify_Auction_FullRedemption_PaymentInformation(orderNumber,
                    BaseUI.getDateAsString_InRelationToTodaysDate(0, " MMM d, yyyy"), BaseUI.convertDouble_ToString_ForCurrency(currentBid));

        } else {
            Account_OrderAndShippingDetails_Page.verify_Auction_FullRedemption_PaymentInformation(orderNumber,
                    BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM-dd-yyyy"), BaseUI.convertDouble_ToString_ForCurrency(currentBid));
        }
    }

    @Test(priority = 220, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step6To12_Verify_History_OrderAndShippingDetails_ShippingInfo() throws Exception {
        Account_OrderAndShippingDetails_Page.verify_OrderDetails_ShippingInfo_NotShipped();
    }

    @Test(priority = 220, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step13_Verify_History_OrderAndShippingDetails_ProductInfo() throws Exception {
        Account_OrderAndShippingDetails_Page.verify_ProductInfo(expectedProductName, 1,
                Double.parseDouble(retailPrice.replace("$", "").replace(",", "").replace("Retail:","")));
    }

    @Test(priority = 220, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step13_Verify_History_OrderAndShippingDetails_CustomerAddressInfo() throws Exception {
        Account_OrderAndShippingDetails_Page.verify_ShippingAddress(shippingInfo.get("clientName"),
                shippingInfo.get("address"), shippingInfo.get("city"), shippingInfo.get("state"),
                shippingInfo.get("country"), shippingInfo.get("zip"));
    }

    @Test(priority = 230, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step13_AuctionEnds_MyAccount_ClosedAuctions_ReviewBidHistory() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_MyAccount_CloseAuctionsRedesign();
            Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();
            Navigation.clickMyAccount_CloseAuctionsRedesign();
            Auctions_ClosedAuctions.click_ViewAllBids();
        }else {
            Navigation.navigate_Account_ClosedAuctions();
            Auctions_ClosedAuctions.expandAuctions_And_ClickViewAllBids_Link();
        }
        Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
    }

    @Test(priority = 240, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE", "not_redesign", "not_aarpredesign"})
    public void PT3664_PT4743_Step13_AuctionEnds_MyAccount_ClosedAuctions_CloseBidHistory_Window() throws Exception {

        Auction_Details.click_Close_BidHistoryWindow();
        BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
    }

    @Test(priority = 250, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
            "critical_Tests", "all_tests", "not_IE"})
    public void PT3664_PT4743_Step13_Verify_AuctionEnds_MyAccount_ClosedAuctions_EndTime_And_Description() throws Exception {

        Auctions_ClosedAuctions.verify_Description(expectedProductName);
        Auctions_ClosedAuctions.verify_EndDate(auctionEndInfo.get("auctionEnded"));
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        if (originalDriver != null) {
            Browser.driver = originalDriver;
            Browser.closeBrowser();
        } else {
            Browser.closeBrowser();
        }
        if (secondBidderDriver != null) {
            Browser.driver = secondBidderDriver;
            Browser.closeBrowser();
        }
    }
}
