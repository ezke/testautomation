package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;

public class LocalOffers_MapValidation extends BaseTest {

    private String city = "Boca Raton";
    private String category = "Dining";
    private String address = "861 NW 51st ST";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_LocalOffers_SearchPage();
        LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria("Florida", city, category, address);
        Navigation.click_SeeLocalOffer_SearchPageButton();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step6_VerifyLocalDealSearchResults_TextDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ShowingLocation_ResultCount", "Showing Locations");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step6_VerifyLocalDealSearchResults_MapDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SearchResult_Map"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifyFullScreenIcon_DisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_FullscreenIcon"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifyMapIcon_DisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_MapBtn"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifySatelliteIcon_DisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SatelliteBtn"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifyPegmanIcon_DisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_PegmanIcon"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifyZoomInIcon_DisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ZoomInIcon"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifyZoomOutIcon_DisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ZoomOutIcon"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step6_VerifyFirstLocalDeal_PinDisplayedOnMap() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_FirstDealOnMap"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_ImgLogoDisplayed() throws Exception {
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealImgLogo"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_DealNameMatches() throws Exception {
        String dealName = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal"));
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Title"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ResultList_FirstDealExpanded_Title", dealName);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_DealCountMatches() throws Exception {
        String dealCount = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal_Count"))
                .replace(" Deals", "");
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_DealCount"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ResultList_FirstDealExpanded_DealCount", dealCount);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_DealDistanceMatches() throws Exception {
        String dealDistance = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal_Distance"));
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Distance"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_Distance", dealDistance);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_DealCountAndDealCountListMatches() throws Exception {
        String dealCount = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal_Count"))
                .replace(" Deals", "");
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();

        ArrayList<WebElement> localDealList = Locator
                .lookup_multipleElements("localOffer_ResultList_FirstDealExpanded_DealCountList", null, null);
        BaseUI.verify_true_AndLog(localDealList.size() == Integer.parseInt(dealCount),
                "Local Deals when expanded MATCH the Deal count displayed on search results page",
                "Local Deals when expanded DOES NOT MATCH the Deal count displayed on search results page");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_DealPointsDisplayedWhenExpanded() throws Exception {
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_DealPoints"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_PrinterIconDisplayed() throws Exception {
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrinterIcon"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_MobileIconDisplayed() throws Exception {
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_MobileIcon"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_RedeemBtnDisplayedForPrinterIconDeal() throws Exception {
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemNowBtn"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" }, enabled = false)
    public void PT4538_Step7_VerifySelectedLocalDeal_DescriptionDisplayedForMobileIconDeal() throws Exception {
        String dealDescriptionForMobileDeal = "This deal can only be redeemed on a mobile device.";
        LocalOffers_LandingPage.clickOnLocalDealsPinOntheMap();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Mobile_Description"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_Mobile_Description", dealDescriptionForMobileDeal);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Logout();
        }

        Browser.closeBrowser();
    }
}
