package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.CarRental_ModifySearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CarRentalSearch_ValidateMapViewResults extends BaseTest {
	
	CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		searchCriteria.airportCode_CityName = "JAX";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(30);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(32);
		searchCriteria.carClassOptionDisplayed = "Economy";
		searchCriteria.pickUpTimeDisplayed = "2:00 PM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_CarRental_Page();
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
	}
	
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyModifySearchResults_Header() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ModifySearchResults_Header"));
		BaseUI.verifyElementHasExpectedPartialText("carRental_ModifySearchResults_Header", "Modify Search Results");	
	}
	
	
	@Test(priority = 20, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyMapButtonExists() throws Exception {
		
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		} else {	
		   BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		   BaseUI.verifyElementHasExpectedText("carRentalSearchResult_MapButton_MapText", "Map");
		}		
	}
	
	
	@Test(priority = 40, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyMapTab() throws Exception {
		
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		CarRental_ModifySearchResults.click_Map_Btn();
		
		if(ClientDataRetrieval.client_Name.equals(client_Designation.AARPRedesign)) {
			BaseUI.switch_ToIframe(Locator.lookupRequiredElement("carRental_GoogleMaps_Iframe"));
		} else {		
			BaseUI.switch_ToIframe(0);
			Thread.sleep(500);
		}
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_MapTab"));
		BaseUI.verifyElementHasExpectedText("carRentalSelect_GoogleMaps_MapTab", "Map");				
	}
	
	
	@Test(priority = 50, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifySatelliteTab() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_SatelliteTab"));
		BaseUI.verifyElementHasExpectedText("carRentalSelect_GoogleMaps_SatelliteTab", "Satellite");		
	}
	
	
	@Test(priority = 60, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyStreetViewButton() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_StreetViewButton"));		
	}
	
	
	@Test(priority = 70, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyZoomInButton() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_ZoomInButton"));		
	}
	
	
	@Test(priority = 80, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyZoomOutButton() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_ZoomOutButton"));		
	}
	
	
	@Test(priority = 90, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step24_CarRentalSearchResult_LandingPage_VerifyMappingElementExists() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSelect_GoogleMaps_mappingElement"));		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				BaseUI.switch_ToDefaultContent();
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}//End of class
