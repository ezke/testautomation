package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.AuctionTile;
import dRewards.pages.*;
import dRewards.pages_Administration.*;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

//@Test(groups = { "not_IE" })
public class Auctions_DualCurrency_ClaimNow extends BaseTest {
	private WebDriver originalDriver;
	private WebDriver secondBidderDriver;
	private String original_Browser;

	private String auctionName = "AutomationTest";
	private Double startingBid = 50.00;
	private Double usdAmount = 3.99;
	private Double currentBid = 50.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;

	String productSku = "auc46samsungsmarthdtvDEMO";
	private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
	private Boolean isDemo = true;
	private Integer auctionTimeToLast = 4;
	private AuctionTile auctionTile = AuctionTile.empty();

	private String expectedEndTime = "";

	private HashMap<String, String> shippingInfo;
	private HashMap<String, String> auctionEndInfo;
	String retailPrice;

	private String user1_previousPoints;
	private String user2_previousPoints;
	private String orderNumber;
	private TableData bidHistory;
	private Integer numberOfBidders = 2;
	private Integer numberOfBids = 3;

	String firstName = "QA";
	String lastName = "Auto";
	String address1 = "1225 Broken Sound";
	String city = "Boca Raton";
	String state = "FL";
	String zip = "33487";

	private String ccType = "MasterCard";
	private String ccNumber = "5105105105105100";
	private String cvvCode = "211";
	private String expire_month = "12";
	private String expire_year = "22";
	String country = "United States";
	String tax_Cost;
	String allstate_Cash;
	String order_Total;
	private Double orderSubtotal;
	private String quantity = "1000";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		bidHistory = new TableData();
		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);

		auctionName += randomNumber;
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		original_Browser = Browser.currentBrowser;

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			auctionTimeToLast += 1;
		}

		LoginPage_Admin.login_Admin();

		EditBuyerProgram_AdminSite.UpdateWinLimit_Attribute_13011();


		Navigation_Admin.navigate_ProductCatalog_Catalog();
		ProductCatalog_CatalogPage.search_Product_BySku(productSku);
		ProductCatalog_CatalogPage.update_Product_Inventory(quantity);

		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction_DualCurrency(auctionName, RedemptionTypes.DualCurrency, startingBid,
				usdAmount, extendTime, productSku, AuctionTypes.Standard, isDemo);
		expectedEndTime = Auctions.return_Auction_TimeLeftFormat_forCheckout(auctionTimeToLast);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all Billing Info.
		Navigation.navigate_Account();
		Account.clear_PaymentHistory();

		Navigation.navigate_Auctions_AllAuctions();
		user1_previousPoints = Navigation.returnPoints();

	}

	@Test(priority = 10, groups = { "not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionFoundInList_AndAccurate() throws Exception {
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.verify_AuctionDetails_Info(expectedProductName, currentBid, true);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" })
	public void PT3664_Step5_Verify_Auctions_DetailsPage() throws Exception {

		String expectedPointsOrRewards;
		if (ClientDataRetrieval.isRedesignClient()) {
			expectedPointsOrRewards = "Enter a Points amount that is at least 1 higher than the current bid.";
			auctionTile.click_AuctionsImage();
		} else {
			expectedPointsOrRewards = "Enter an amount that is higher than the current bid";// + ClientDataRetrieval.client_Currency + "";
			auctionTile.click_DetailsButton();
		}
		Auction_Details.verify_AuctionDetails_DualCurrency(currentBid, expectedPointsOrRewards, usdAmount);
	}

	@Test(priority = 30, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_PlaceBid_Launches_DualCurrency_Modal() throws Exception {
		currentBid += 1;
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.enter_MaxBid(bidHistory.data.get(0).get("bidAmount"));
		Auction_Details.click_place_Bid();
		Auction_Details.verify_DualCurrency_AcceptModal(usdAmount);
	}

	@Test(priority = 40, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_DualCurrency_AcceptModal_ClickIAgree_And_ConfirmBidModal_Info() throws Exception {
		if (ClientDataRetrieval.isLegacyClient()) {
			Auction_Details.click_IAgree_DualCurrencyModal();
		}
		Auction_Details.verify_ConfirmBidModal_Info(BaseUI.convertDouble_ToString_ForCurrency(currentBid));
	}

	@Test(priority = 50, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_1stUser_BidSuccessful() throws Exception {

		if (ClientDataRetrieval.isLegacyClient()) {
			retailPrice = Auction_Details.return_RetailPrice();
		}

		Auction_Details.click_Confirm_Bid();
		Auction_Details.verify_BidSuccessful(bidHistory.data.get(0).get("bidAmount"));
		Auction_Details.verify_BidButtonDisabled();
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));

	}

	@Test(priority = 60, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_2ndUserBid_FoundAndAccurate() throws Exception {

		// Set up Second Bidder
		originalDriver = Browser.driver;
		ClientDataRetrieval.set_secondaryUser_Info(original_Browser);
		Browser.openBrowser(ClientDataRetrieval.url, "chrome");
		secondBidderDriver = Browser.driver;
		LoginPage.login(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);

		Navigation.navigate_Auctions_AllAuctions();
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, currentBid);
		user2_previousPoints = Navigation.returnPoints();
		auctionTile.verify_AuctionDetails_Info(expectedProductName, currentBid, true);
	}

	@Test(priority = 70, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_2ndUser_BidSuccessful() throws Exception {
		currentBid += 1;
		Browser.driver = secondBidderDriver;
		auctionTile.click_DetailsButton();
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(false, String.valueOf(currentBid)));
		Auction_Details.enter_MaxBid(bidHistory.data.get(1).get("bidAmount"));
		Auction_Details.click_place_Bid();

		if (ClientDataRetrieval.isLegacyClient()) {
			Auction_Details.click_IAgree_DualCurrencyModal();
		}

		Auction_Details.click_Confirm_Bid();
		Auction_Details.verify_LeadingBidder(bidHistory.data.get(1).get("bidAmount"));
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user2_previousPoints, currentBid.toString()));
	}

	@Test(priority = 80, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_1stBidder_WasOutbid() throws Exception {

		Browser.driver = originalDriver;
		Auction_Details.verify_Outbid(user1_previousPoints);
		BaseUI.baseStringCompare("Points match previous count.", user1_previousPoints, Navigation.returnPoints());
	}

	@Test(priority = 90, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_1stBidder_CounterBidSuccessful() throws Exception {

		currentBid += 1;
		Browser.driver = originalDriver;
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.enter_MaxBid(bidHistory.data.get(2).get("bidAmount"));
		Auction_Details.click_place_Bid();
		Auction_Details.click_Confirm_Bid();
		Auction_Details.verify_BidSuccessful(bidHistory.data.get(2).get("bidAmount"));
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));

	}

	@Test(priority = 100, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_2ndBidder_WasOutbid() throws Exception {

		Browser.driver = secondBidderDriver;
		Auction_Details.verify_Outbid(user2_previousPoints);
	}

	@Test(priority = 110, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_ReviewBidHistory() throws Exception {

		Browser.driver = originalDriver;
		Auction_Details.click_View_BidHistory();
		Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
	}

	@Test(priority = 120, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Close_BidHistory_Window() throws Exception {

		Auction_Details.click_Close_BidHistoryWindow();
		BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
	}

	// Step 12
	@Test(priority = 130, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_MyAccount_OpenAuctions_BidHistory_And_Descripton() throws Exception {

		Navigation.navigate_Account_OpenAuctions();
		Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();
		Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
		Auctions_OpenAuctions.verify_Description(expectedProductName);

	}

	@Test(priority = 140, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Auction_OpenAuctions_Close_BidHistory_Window() throws Exception {

		Auction_Details.click_Close_BidHistoryWindow();
		BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
	}

	@Test(priority = 150, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_PopupAppears() throws Exception {

		Browser.driver = originalDriver;
		Navigation.navigate_Auctions_AllAuctions();
		auctionTile.click_DetailsButton();

		Auction_Details.wait_For_End_OfAuction(auctionTimeToLast * 60);

		Auction_Details.wait_For_ClaimPopup_ToAppear();
		Auction_Details.verify_AuctionWon_PopupAppears();
	}

	@Test(priority = 160, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_WonBannerAppears() throws Exception {

		Auction_Details.verify_AuctionWon_Banner_Appears();
	}

	@Test(priority = 170, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_ClaimAuction_ShippingPage() throws Exception {

		Auction_Details.click_ClaimNow_InWinningPopup();
		if (BaseUI.elementExists("auctShip_1stAddress_FirstName", null, null)) {
			shippingInfo = Auctions_Checkout_Shipping.return_ShippingInfo();
		} else {
			shippingInfo = CheckoutPage_ShippingInfo.return_DefaultShippingInfo();
		}
		auctionEndInfo = Auctions_Checkout_Shipping.return_AuctionEndInfo_DualCurrency();
		Auctions_Checkout_Shipping.verify_AuctionsInfo_DualCurrency(expectedProductName, currentBid.toString(),
				expectedEndTime, usdAmount);
	}

	@Test(priority = 180, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_2ndUser_DidNotWin() throws Exception {

		Browser.driver = originalDriver;
		String expectedUserName = Auction_Details.return_UserName();

		Browser.driver = secondBidderDriver;
		String winnerName = BaseUI.getTextFromField(Locator.lookupRequiredElement("auctionBid_Winner_Name")).toLowerCase();
		BaseUI.baseStringPartialCompare("WinnerName", expectedUserName, winnerName);

		Auction_Details.verify_User_DidNot_Win_Auction(bidHistory.data.get(0).get("bidAmount"));
	}

	@Test(priority = 190, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_PaymentInfo_AuctionInfo()
			throws Exception {

		Browser.driver = originalDriver;
		Auctions_Checkout_Shipping.navigate_To_NextPage();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			// Allstate Cash was not used for this run through.
			CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
			CheckoutPage_AllStateCash.click_Continue_ToNextPage();
			Auctions_Checkout_PaymentInfo.verify_AuctionsInfo_DualCurrency(expectedProductName, currentBid.toString(),
					expectedEndTime, usdAmount);
		} else {
			Auctions_Checkout_PaymentInfo.verify_AuctionsInfo_DualCurrency(expectedProductName, currentBid.toString(),
					expectedEndTime, usdAmount);
		}
	}

	@Test(priority = 200, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_AuctionEnds_1stUserWins_ClaimAuction_Checkout_AddPaymentInfo_() throws Exception {
		Browser.driver = originalDriver;
		Auctions_Checkout_PaymentInfo.add_NewPaymentOption(ccType, ccNumber, expire_month, expire_year, cvvCode,
				firstName + " " + lastName, address1, country, city, state, zip);
		Auctions_Checkout_PaymentInfo.verify_ReviewPayment_Button_Appears();
	}

	@Test(priority = 210, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_OrderReview() throws Exception {

		Browser.driver = originalDriver;
		allstate_Cash = "$0.00";

		Auctions_Checkout_PaymentInfo.click_ReviewPayment_Button();
		tax_Cost = Auctions_Checkout_OrderReview.return_SalesTaxAsString();
		order_Total = Auctions_Checkout_OrderReview.return_Order_Total();
		Auctions_Checkout_OrderReview.verify_Auctions_OrderReviewInfo_DualCurrency(shippingInfo, expectedProductName,
				currentBid.toString(), expectedEndTime, usdAmount, retailPrice, tax_Cost);
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Auctions_Checkout_OrderReview.verify_AllStateCash(allstate_Cash);
		}
	}

	@Test(priority = 220, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_OrderComplete() throws Exception {

		Auctions_Checkout_OrderReview.click_AcceptConditions_AndCompleteOrder();
		orderNumber = Auctions_Checkout_OrderComplete.return_OrderNumber();
		Auctions_Checkout_OrderComplete.verify_Order_Info_DualCurrency(shippingInfo, expectedProductName, retailPrice,
				expectedEndTime, currentBid.toString(), usdAmount, tax_Cost);
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Auctions_Checkout_OrderReview.verify_AllStateCash(allstate_Cash);
		}
	}

	@Test(priority = 230, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_OrderComplete_PrintReceiptVisible() throws Exception {

		Auctions_Checkout_OrderComplete.verify_PrintYourReceiptButton_VisibleAndEnabled();
	}

	// Test for non-allstateCash
	@Test(priority = 240, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_ClaimAuction_Checkout_OrderComplete_PointsCorrect()
			throws Exception {

		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
	}

	@Test(priority = 250, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_AuctionEnds_1stUserWins_Validate_MyAuction() throws Exception {

		Navigation.navigate_Auctions_MyAuctions();
		Auctions_MyAuctions.verify_AuctionWon_AndAlreadyClaimed(expectedProductName, currentBid,
				auctionEndInfo.get("auctionEnded"));
	}

	@Test(priority = 260, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Navigate_AuctionEnds_1stUserWins_Validate_MyAuction_ViewDetails() throws Exception {
		Auctions_MyAuctions.click_LastAuction_ViewDetails();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAuct_OrderAndShip_Header"));
		BaseUI.verifyElementHasExpectedPartialText("myAuct_OrderAndShip_Header", "Order & Shipping Details");
	}

	@Test(priority = 270, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_History_OrderAndShippingDetails_PaymentInfo() throws Exception {

		orderSubtotal = Double.parseDouble(order_Total.replace("$", "").replace(",", ""))
				- Double.parseDouble(tax_Cost.replace("$", "").replace(",", ""));
		Account_OrderAndShippingDetails_Page.verify_Auction_PaymentInfo_DualCurrency(orderNumber,
				BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM-dd-yyyy"), orderSubtotal,
				Double.parseDouble(tax_Cost.replace("$", "").replace(",", "")),
				Double.parseDouble(order_Total.replace("$", "").replace(",", "")),
				BaseUI.convertDouble_ToString_ForCurrency(currentBid));
	}

	@Test(priority = 270, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_History_OrderAndShippingDetails_ShippingInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_OrderDetails_ShippingInfo_NotShipped();
	}

	@Test(priority = 270, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign",
			"critical_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step5_Verify_History_OrderAndShippingDetails_ProductInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_ProductInfo_DualCurrency(expectedProductName, 1,
				Double.parseDouble(retailPrice.replace("$", "").replace(",", "")),
				BaseUI.convertDouble_ToString_ForCurrency(orderSubtotal)); //String.valueOf(Math.round(Float.parseFloat(
	}

	@Test(priority = 270, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests",
			"all_tests" ,"not_IE"})
	public void PT3249_Verify_History_OrderAndShippingDetails_CustomerAddressInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(shippingInfo.get("clientName"),
				shippingInfo.get("address"), shippingInfo.get("city"), shippingInfo.get("state"),
				shippingInfo.get("country"), shippingInfo.get("zip"));
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondBidderDriver != null) {
			Browser.driver = secondBidderDriver;
			Browser.closeBrowser();
		}
	}
}