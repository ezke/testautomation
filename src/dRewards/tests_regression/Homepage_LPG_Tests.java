package dRewards.tests_regression;

import dRewards.pageControls.HomePage_LPGWidget;
import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Homepage_LPG_Tests extends BaseTest {


	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests" ,"not_streetwise"})
	public void PT3214_Step24_PT4787_Step1_VerifyLpg_LearnMoreLink() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LearnMoreLnk"));
	}
	
	
	@Test(groups = { "not_smart", "regression_Tests", "all_tests" })
	public void PT3214_Step24_PT4787_Step1_VerifyLpg_LpgLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgLogo"));
	}
	
	
	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests" ,"not_streetwise"})
	public void PT3214_Step24_PT4787_Step1_VerifyLpg_LpgBottomFooter() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgBFooter"));
	}

	@Test(groups = {"not_jeepwave", "not_allstate", "not_allstateCash", "not_tvc", "not_smart", "regression_Tests", "all_tests" ,"not_streetwise"})
	public void PT3214_Step24_PT4787_Step1_ClickLearnMore_VerifyLPGHeader() throws Exception {
		Homepage.clickLPG_LearnMoreLink();
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgHeader"));
	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests", "not_streetwise",
			"not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step24_MerchandiseTab() throws Exception {
		Homepage.verify_Merchandise_LPG_homepage();
	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests", "not_streetwise",
			"not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step24_TravelTab() throws Exception {
		Homepage.verify_Travel_LPG_homepage();
	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step24_VerifyTravelLPG_TandC_LandingPage() throws Exception {
		HomePage_LPGWidget.click_LearnMore_ExpandWidget();
		HomePage_LPGWidget.click_Travel_Tab();
		HomePage_LPGWidget.click_Current_SeeTermsAndConditionsLink();
		LPG_Page.verifyLPG_HeaderTitle();
	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step24_VerifyMerchandiseLPG_TandC_LandingPage() throws Exception {
		HomePage_LPGWidget.click_LearnMore_ExpandWidget();
		HomePage_LPGWidget.click_Current_SeeTermsAndConditionsLink();
		LPG_Page.verifyLPG_HeaderTitle();
	}

	@Test(groups = { "not_citi", "not_aarp", "regression_Tests", "all_tests", "not_streetwise" })
	public void PT3214_Step24_PT4787_Step1_LPG_WidgetPresent() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_ClickForDetailsWidget"));
	}

	@Test(groups = { "not_citi", "not_allstate", "not_allstateCash", "not_aarp", "regression_Tests",
			"all_tests", "not_streetwise", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step24_LPG_WidgetPresentWindowCoreClient() throws Exception {
		Homepage.clickLPG_LpgWidget();
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_ClickForDetailsWidget"));

	}

	@Test(groups = { "not_citi", "not_allstate", "not_allstateCash", "not_aarp", "regression_Tests",
			"all_tests", "not_streetwise", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step24_MerchandiseTabCoreClient() throws Exception {
		Homepage.navigate_LPGModal_Merchandise_coreClient();
		LPG_Modal.verify_Merchandise_LPG_Modal_Displayed();
	}

	@Test(groups = { "not_citi", "not_allstate", "not_allstateCash", "not_aarp", "regression_Tests",
			"all_tests", "not_streetwise", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step24_TravelTabCoreClient() throws Exception {

		Homepage.verify_Travel_LPG_homepage();
	}

	@Test(groups = { "not_citi", "not_smart", "not_allstate", "not_allstateCash", "not_aarp",
			"regression_Tests", "all_tests", "not_streetwise", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step24_VerifyTravelLPG_TandC_LandingPageCore() throws Exception {

		Homepage.verifyMerchandiseLPG_TandC_LandingPageCoreClient();
	}

	@Test(groups = { "not_citi", "not_smart", "not_allstate", "not_allstateCash", "not_aarp",
			"regression_Tests", "all_tests", "not_streetwise", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step24_VerifyMerchandiseLPG_TandC_LandingPageCore() throws Exception {

		Homepage.verifyTravelLPG_TandC_LandingPageCoreClient();
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
		} finally {
			Browser.closeBrowser();
		}
	}
}