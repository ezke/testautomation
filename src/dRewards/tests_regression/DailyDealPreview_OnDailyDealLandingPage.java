package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.*;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.ProductCatalog_CatalogPage;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;
import java.util.HashMap;

public class DailyDealPreview_OnDailyDealLandingPage extends BaseTest {

    private HashMap<String, String> homePageDailyDealData;
    private HashMap<String, String> dailyDeal_Data;
    private String inventoryCount;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_SneakPreviewWidget"))) {
            BaseUI.log_Status("Daily Deal is Sold Out. Need to update in Admin Site");
            BaseUI.verifyElementHasExpectedText("homepage_DailyDeals_SneakPreviewText", "Sneak Preview");
        } else if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_DailyDealTitle"))
                && BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_Images"))) {
            BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("homepage_DailyDeals_SneakPreviewWidget"));
            BaseUI.log_Status("Daily Deal is Live cannot validate the sneak preview");

            homePageDailyDealData = Homepage.clickDailyDeal();
            dailyDeal_Data = DailyDeals.return_DailyDealInfo();

            //If Daily Deal is live, navigate to admin site and set the daily deal,
            //inventory value to 0. So we can test the daily deal in preview state.
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();

            //Save the Daily Deal ProductSku and Inventory value, so it is be reset back to original after validation.
            String productSku = ProductCatalog_CatalogPage.returnProductSku_ForSelectedDailyDeal(dailyDeal_Data.get("dailyDealName"));
            inventoryCount = ProductCatalog_CatalogPage.returnInventoryCountValue_ForSelectedProductSku(productSku);
            ProductCatalog_CatalogPage
                    .update_DailyDeal_ProductSku_Quantity(dailyDeal_Data.get("dailyDealName"), "0");

            Browser.navigateTo(ClientDataRetrieval.url);
            LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
            Navigation.navigate_Home_viaHomeLink();
        } else {
            BaseUI.log_AndFail("Daily Deal is not setup in Admin Site");
        }

        homePageDailyDealData = Homepage.return_Homepage_SneakPreview_DailyDealInfo();
        Homepage.click_DailyDeal_SeeMoreDeals();
        dailyDeal_Data = DailyDeals.return_DailyDealInfo();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_Info_On_DailyDealPreviewPage() throws Exception {

        DailyDeals.verify_DailyDealInfo(homePageDailyDealData, dailyDeal_Data);
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDealsHeaderTitle_On_DailyDealPreviewPage() throws Exception {
        WebElement dailyDealsHeaderTitleElement = Locator.lookupRequiredElement("dailyDeals_HeaderTitle");
        String actualDailyDealsHeaderTitle = BaseUI.getTextFromField(dailyDealsHeaderTitleElement);
        BaseUI.verifyElementAppears(dailyDealsHeaderTitleElement);
        BaseUI.baseStringPartialCompare("dailyDeals_HeaderTitle", "Daily Deal", actualDailyDealsHeaderTitle);
    }


    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT3751_Step2_Validate_DailyDealsStartTime_On_DailyDealPreviewPage() throws Exception {
        WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("dailyDeals_HeaderTitle_StartTime");
        String actualDailyDealsHeaderTitle_StartTime = BaseUI.getTextFromField(dailyDealsStartTimeElement);
        BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
        BaseUI.baseStringCompare("dailyDeals_HeaderTitle_StartTime", "New item daily at 3PM ET", actualDailyDealsHeaderTitle_StartTime);
    }


    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT3751_Step2_Validate_NextDailyDealStartStatus_On_DailyDealPreviewPage() throws Exception {
        WebElement availableStatusElement = Locator.lookupRequiredElement("dailyDeals_NextDealStart_Status");
        String actualAvailableStatusText = BaseUI.getTextFromField(availableStatusElement);
        BaseUI.verifyElementAppears(availableStatusElement);
        BaseUI.baseStringCompare("dailyDeals_NextDealStart_Status",
                "The next Daily Deal starts in:"+"\n"
                        +BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("dailyDeals_EndTime"), "innerText"),
                actualAvailableStatusText);
    }


    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_PreviousDealSoldOutText_On_DailyDealPreviewPage() throws Exception {
        WebElement dealExpiresTextElement = Locator.lookupRequiredElement("dailyDeals_PreviousDealSoldOut_Status");
        String actualDealExpiresText = BaseUI.getTextFromField(dealExpiresTextElement);
        BaseUI.verifyElementAppears(dealExpiresTextElement);
        BaseUI.baseStringPartialCompare("dealExpiresText",
                "The previous Daily Deal has "
                        +BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_SoldOut_Status")), actualDealExpiresText);
    }


    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_Image_Appears_OnPreviewPage() {
        DailyDeals.verify_Image_Appears();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_Description_Text_Appears_OnPreviewPage() {
        BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("dailyDeals_Description"), "Description");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDealsPreviewPage_AllImage_OnSlide_Appears() throws Exception {
        ArrayList<String> imagesOnSlidesList = DailyDeals.return_DailyDeals_AllImagesDisplayed_OnSlides();
        BaseUI.verify_true_AndLog(imagesOnSlidesList.size() > 0, "Images are DISPLAYED ON SLIDE: "+ imagesOnSlidesList.size(),
                "Images are NOT DISPLAYED ON SLIDE: "+ imagesOnSlidesList.size());
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_ShippingType_On_DailyDealPreviewPage() {
        WebElement dealShippingTypeElement = Locator.lookupRequiredElement("dailyDeals_ShippingType");
        String actualDailyDeals_ShippingTypesText = BaseUI.getTextFromField(dealShippingTypeElement);
        BaseUI.verifyElementAppears(dealShippingTypeElement);
        BaseUI.baseStringCompare("Free Shipping Text", "Free Shipping", actualDailyDeals_ShippingTypesText);
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_RetailValueAppears_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_RetailPrice"));
        BaseUI.verifyElementHasExpectedPartialText("dailyDeals_RetailPrice", "Retail: ");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_PointsValueAppears_OnPreviewPage() {
        WebElement pointsElement = Locator.lookupRequiredElement("dailyDeals_Points");
        String actualDailyDeals_PointsText = BaseUI.getTextFromField(pointsElement);
        BaseUI.verifyElementAppears(pointsElement);

        BaseUI.verify_true_AndLog(actualDailyDeals_PointsText.startsWith("after") && actualDailyDeals_PointsText.endsWith("points"),
                "Daily Deal points Starts with AFTER and ends with POINTS", "Daily Deal points field DOES NOT Starts with AFTER and ends with POINTS");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_YouPayValueAppears_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_YouPay"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_DailyDeals_AddToShoppingCartBtn_Disabled_OnPreviewPage() {
        BaseUI.verifyElementDisabled(Locator.lookupRequiredElement("dailyDeals_AddToShoppingCart_Button"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_LPGLogoAppears_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_lpgLogo"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_LPGLearnMoreLinkAppears_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_LearnMoreLnk"));
        BaseUI.verifyElementHasExpectedText("homepage_LearnMoreLnk", "Learn More");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_LPGTitleTextAppears_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_lpgHeader"));
        BaseUI.verifyElementHasExpectedPartialText("homepage_lpgHeader",
                "Our Retail Price is the lowest found in the online marketplace.");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_RecaptchaLogo_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_RecaptchaLogo"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step2_Validate_RecaptchaLabel_OnPreviewPage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_RecaptchaLabel"));
        BaseUI.verifyElementHasExpectedPartialText("dailyDeals_RecaptchaLabel",
                "Protected by reCAPTCHA Security");
    }


    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step3_Validate_LPGPage_HeaderTitle_Appears() throws Exception {
        Homepage.clickLPG_LearnMoreLink();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_LogoHeaderTitle"));
        BaseUI.verifyElementHasExpectedPartialText("lpg_LogoHeaderTitle",
                "Best Price, Period.");
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step3_Validate_LPGPage_HeaderLogo_Appears() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_LogoHeaderImage"));
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4498_Step3_Validate_LPGPage_HeaderTitleDescription_Appears() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("lpg_LogoHeaderDescription"));
        BaseUI.verifyElementHasExpectedPartialText("lpg_LogoHeaderDescription",
                "You won't find a better price online. We guarantee it.");
    }


    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();
            ProductCatalog_CatalogPage.
                    update_DailyDeal_ProductSku_Quantity(dailyDeal_Data.get("dailyDealName"), inventoryCount);
        } finally {
            Browser.closeBrowser();
        }
    }
}
