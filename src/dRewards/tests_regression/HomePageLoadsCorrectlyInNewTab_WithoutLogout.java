package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.time.Duration;
import java.util.ArrayList;

public class HomePageLoadsCorrectlyInNewTab_WithoutLogout extends BaseTest{

    ArrayList<String> tabs;
    private String newTab;
    private String currentWindow;
    private WebDriver originalDriver;

    @BeforeClass (alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        originalDriver = Browser.driver;
        LoginPage_Admin.login_Admin();
        Navigation_Admin.navigate_ClickNavLink("Clients");
        Clients_SearchClients.search_ClientID();
        Clients_SearchClients.click_ClientNumber_FromResult_Option();
        Clients_SearchClients.click_ProgramName_ByProgramID("9363");

        Clients_SearchClients.update_Attribute("12221", "90.00000");
        Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
        Browser.driver = originalDriver;

        Navigation_Admin.navigate_ClickNavLink("Memcache Flush");
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee"})
    public void PT4704_Step2_HomePage_VerifyLogo() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_HomeLogo"));
    }

    //Commenting this test validation, because this widget is present only if we have Daily deal,
    //Auctions or sweepstakes present.
//    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
//            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" })
//    public void PT4704_Step2_HomePage_VerifyDailyDeal_Sweepstakes_AuctionsWidgetSection_HeaderTitle() throws Exception {
//        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_DailyDeal_Sweepstakes_AuctionsWidgetSection_HeaderTitle"));
//        BaseUI.verifyElementHasExpectedPartialText("homepage_DailyDeal_Sweepstakes_AuctionsWidgetSection_HeaderTitle",
//                "take advantage of these offers ending soon.");
//    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee"})
    public void PT4704_Step2_HomePage_VerifyAccountLink() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_AccountLink"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_HomePage_Verify_PointsAndCashRewards_Appear() throws Exception {
        Navigation.verify_PointsAndCashRewards_Appear();
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_Verify_ShopByBrandCarousel_Header() throws Exception {
        Homepage.verify_ShopByBrandCarousel_HeaderTitle();
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" })
    public void PT4704_Step2_Verify_SeeAllBrands_Link() throws Exception {
        Homepage.verify_SeeAllBrandsLink();
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_VerifyLpg_LearnMoreLink() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LearnMoreLnk"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_VerifyLpg_LpgLogo() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgLogo"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_VerifyLpg_LpgBottomFooter() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgBFooter"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_HomePage_Verify_LocalOffer_Appears() throws Exception {
        Homepage.verify_LocalOffer_Appears();
    }

    @Test(priority = 5, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step2_HomePage_Verify_LocalOffer_BannerImgAppears() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_BannerImg"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step3_OpenANewTabInSameBrowser_VerifyNewTabIsOpened() throws Exception {
        String url = Browser.driver.getCurrentUrl();

        //Get current tab window handle
        tabs = new ArrayList<String> (Browser.driver.getWindowHandles());
        currentWindow = Browser.driver.getWindowHandle();
        String currentTab_Title = Browser.driver.getTitle();

        //Open new tab in the same browser session
        ((JavascriptExecutor)Browser.driver).executeScript("window.open()");
        Thread.sleep(2000);
        BaseUI.wait_forPageToFinishLoading();

        //Switch to the new window
        BaseUI.verify_true_AndLog(tabs.size() > 0, "New Tab Opened",
                "New tab DID NOT open");
        newTab = BaseUI.wait_ForNewWindow_ToExist_AndSwitchToIt(tabs, Duration.ofSeconds(30));
        String newTab_Title = Browser.driver.getTitle();

        BaseUI.verify_true_AndLog(!(newTab_Title.equals(currentTab_Title)), "New tab window title DOES NOT MATCH",
                "New tab window title MATCH");

        Browser.driver.switchTo().window(currentWindow);
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_HomeLogo"));
    }

    @Test(priority = 15, groups = { "smoke_tests", "all_tests" })
    public void PT4704_Step4To5_CloseCurrentWindowTabAndSwitchToNewTab_NavigateToUrl_VerifyLoginPageUsername_DoesNotDisplay() throws Exception {
        ((JavascriptExecutor)Browser.driver.switchTo().window(currentWindow)).executeScript("window.close();");
        Thread.sleep(2000);

        Browser.driver.switchTo().window(newTab);
        Browser.navigateTo(ClientDataRetrieval.url);
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("loginPage_easyDeals_UserName"));
    }

    @Test(priority = 15, groups = { "smoke_tests", "all_tests" })
    public void PT4704_Step6To7_VerifyHomePageLoadsCorrectly() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_HomeLogo"));
        Navigation.verify_PointsAndCashRewards_Appear();
    }

    @Test(priority = 20, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee"})
    public void PT4704_Step6To7_Verify_SeeAllBrands_Link() throws Exception {
        Homepage.verify_SeeAllBrandsLink();
    }

    @Test(priority = 2, groups = { "regression_Tests", "not_redesign", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4704_Step6To7_VerifyLpg_LearnMoreLink() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LearnMoreLnk"));
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            Navigation.navigate_Logout();
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            originalDriver = Browser.driver;
            LoginPage_Admin.login_Admin();
            Navigation_Admin.navigate_ClickNavLink("Clients");
            Clients_SearchClients.search_ClientID();
            Clients_SearchClients.click_ClientNumber_FromResult_Option();
            Clients_SearchClients.click_ProgramName_ByProgramID("9363");
            Clients_SearchClients.update_Attribute("12221", "1800.00000");
            Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
        } finally {
            Browser.closeBrowser();
        }
    }
}
