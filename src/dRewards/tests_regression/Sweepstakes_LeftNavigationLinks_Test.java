package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Sweepstakes_LeftNavigationLinks_Test extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_Menu_Submenu("Sweepstakes", "Sweepstakes");		
	}

	@DataProvider(name = "Sweepstakes")
	public Object[][] createData_Sweepstakes() throws Exception {

		ArrayList<String> sweepstakes_LeftNavList = new ArrayList<String>(Sweepstakes.return_Sweepstakes_NavMenuList());

		return return_linkTextAs_ObjectArray(sweepstakes_LeftNavList);
	}

	public Object[][] return_linkTextAs_ObjectArray(ArrayList<String> dataSet) throws Exception {
		BaseUI.verify_true_AndLog(dataSet.size() > 0, "Found links", "Did NOT find links.");

		Object[][] linksToNavigate = new Object[dataSet.size()][1];
		int objectIndex = 0;
		for (String linkItem : dataSet) {
			linksToNavigate[objectIndex][0] = linkItem;
			objectIndex++;
		}

		return linksToNavigate;
	}

	@Test(dataProvider = "Sweepstakes", groups = { "all_tests", "regression_Tests", "not_jeepwave", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
	public void PT30_Step_LeftNavigation_Sweepstakes(String linkText) throws Exception {
		String[] breadcrumbs;

		Sweepstakes.navigate_SweepstakesLeftNav_ByText(linkText);

		if (linkText.equals("My Sweepstakes")) {
			breadcrumbs = new String[] { "Home", "Sweepstakes | History" };
		} else if (linkText.equals("Help")) {
			breadcrumbs = new String[] { "Home", "Sweepstakes/Help" };
		} else {
			breadcrumbs = new String[] { "Home", linkText };
		}

		Navigation.verify_BreadcrumbList(breadcrumbs);

		if (!(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
			BaseUI.verifyElementAppears(Locator.lookupElement("sweepstakes_HeaderTitle"));
			if (linkText.equals("Current Sweepstakes")) {
				BaseUI.verifyElementHasExpectedText("sweepstakes_HeaderTitle", "Sweepstakes");
			} else if (linkText.equals("Help")) {
				BaseUI.verifyElementHasExpectedText("sweepstakes_HeaderTitle", "Sweepstakes Help");
			} else {
				BaseUI.verifyElementHasExpectedText("sweepstakes_HeaderTitle", linkText);
			}
		}

		Navigation.verify_No_ApplicationErrorMessage();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}

}
