package dRewards.tests_regression;

import dRewards.pages.*;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.ProductCatalog_CatalogPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class MerchCheckout_ItemSoldOut_Tests extends BaseTest {

	WebDriver originalDriver;
	private String originalDriver_Browser;

	WebDriver adminDriver;
	private String adminDriver_Browser = "chrome";
	//String productName = "L'OCCITANE Shea Butter Foot Cream 5.2 fl. oz.";//updated sku
	String productName = "L'OCCITANE Immortelle Intense Foaming Cleanser - 5 fl. oz.";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		originalDriver = Browser.driver;
		originalDriver_Browser = Browser.currentBrowser;

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Homepage.deleteItemsFromShoppingCart();
		Navigation.navigate_Home_viaLogo();
		Homepage.enter_MerchandiseBrandName_SearchBox(productName);	
		Homepage.click_MerchandiseSearchSubmit();
		// String productName = BaseUI.getTextFromField(Locator.lookupRequiredElement("merch_SearchResults_ProductTitle"));
	//	BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_SearchResults_ProductTitle"));
	//	BaseUI.verifyElementHasExpectedText("merch_SearchResults_ProductTitle", productName);
	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests","not_citi" ,"not_streetwise"})
	public void PT3235_First_Update_Admin_ProductCatalog_Inventory() throws Exception {
		// Set admin driver to update inventory
		// Inventory set to 15 to add item in cart
		String quantity = "15";
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		adminDriver = Browser.driver;
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ProductCatalog_Catalog();
		ProductCatalog_CatalogPage.search_Product_ByName(productName);
		ProductCatalog_CatalogPage.update_Product_Inventory(quantity);
		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
	}

	@Test(priority = 20, groups = { "regression_Tests", "all_tests" ,"not_citi","not_streetwise"})
	public void PT3235_AddItemInCart() throws Exception {
		Browser.changeDriver(originalDriver, originalDriver_Browser);
		Navigation.navigate_Home_viaLogo();
		Homepage.enter_MerchandiseBrandName_SearchBox(productName);
		Homepage.click_MerchandiseSearchSubmit();
		Merchandise.add_To_Cart_ByNumber("1");
	}

	@Test(priority = 30, groups = { "regression_Tests", "all_tests" ,"not_citi","not_streetwise"})
	public void PT3235_Second_Update_Admin_ProductCatalog_Inventory() throws Exception {
		// Set admin driver to update inventory
		// Inventory set to 0 to verify sold out message in shopping cart
		String quantity = "0";
		Browser.changeDriver(adminDriver, adminDriver_Browser);
		Navigation_Admin.navigate_ProductCatalog_Catalog();
		ProductCatalog_CatalogPage.search_Product_ByName(productName);	
		ProductCatalog_CatalogPage.update_Product_Inventory(quantity);
		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
	}

	@Test(priority = 40, groups = { "regression_Tests", "all_tests" ,"not_citi","not_streetwise"})
	public void PT3235_Verify_SoldOut_Message_AppearsIn_ShoppingCart() throws Exception {
		String expectedError = "Product has been sold out. Please remove it in order to continue check out";
		Browser.changeDriver(originalDriver, originalDriver_Browser);
		Navigation.navigate_Home_viaLogo();
		Navigation.navigate_ShoppingCart();
		if(ClientDataRetrieval.isRedesignClient()){
			ShoppingCart.click_ProceedToCheckout();
		}

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_ProductSoldOut_ErrorText"));
		BaseUI.verifyElementHasExpectedText("shpCart_ProductSoldOut_ErrorText", expectedError);
	}

	@Test(priority = 50, groups = { "regression_Tests", "all_tests" ,"not_citi","not_streetwise"})
	public void PT3235_Verify_SoldOut_AppearsIn_SearchPage() throws Exception {
		Navigation.navigate_Home_viaLogo();
		Homepage.enter_MerchandiseBrandName_SearchBox(productName);
		Homepage.click_MerchandiseSearchSubmit();
		if(ClientDataRetrieval.isRedesignClient()) {
			BaseUI.click(Locator.lookupRequiredElement("merch_AddToCart_ByNumber", "1", null));
			BaseUI.wait_forPageToFinishLoading();
			Thread.sleep(2000);
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("merchProduct_AddToShoppingCartButton"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_SoldOut_Button_ByNumber", "1", null));
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("merch_SoldOut_Button_ByNumber", "1", null),
					"Sold Out");
		}
	}

	@Test(priority = 60, groups = { "regression_Tests", "all_tests" ,"not_citi", "not_streetwise"})
	public void PT3235_Verify_AddToCart_DoesNot_AppearIn_SearchPage() throws Exception {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("merch_AddToCart_ByNumber", "1", null));
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
			String quantity = "15";
			Browser.changeDriver(adminDriver, adminDriver_Browser);
			Navigation_Admin.navigate_ProductCatalog_Catalog();
			ProductCatalog_CatalogPage.search_Product_ByName(productName);
			ProductCatalog_CatalogPage.update_Product_Inventory(quantity);
			CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();
		} finally {
			if (originalDriver != null) {
				Browser.changeDriver(originalDriver,originalDriver_Browser);
				Browser.closeBrowser();
			} else {
				Browser.closeBrowser();
			}
			if (adminDriver != null) {
				Browser.changeDriver(adminDriver, adminDriver_Browser);
				Browser.closeBrowser();
			}
		}
	}
}