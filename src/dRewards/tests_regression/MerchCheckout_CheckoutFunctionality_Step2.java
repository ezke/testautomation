package dRewards.tests_regression;

import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

//Tests Step 7 of the Test Case PDF Eber sent me.
public class MerchCheckout_CheckoutFunctionality_Step2 extends BaseTest {

	// Variables for Shipping Info and same values used for Billing.
	private static final String firstName = "QA";
	private static final String lastName = "Auto";
	private static final String address1 = "1225 broken sound";
	private static final String city = "Boca Raton";
	private static String state_Abbreviated = "FL";
	private static String state_FullName = "Florida";
	private static String zip = "33487";
	private static final String phone = "9544151992";
	private static final String country = "United States";

	private String expCreditCard_ErrorText;
	private String expExpireMonth;
	private String expExpireYear;
	private String expCVVCode;
	private String expCardHolderName;
	private String expAddress1;
	private String expCityName;
	private String expStateName;
	private String expZipCode;
	private String cvvCode = "211";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		//Browser.openBrowser(ClientDataRetrieval.url);
		//LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		//Browser.set_ImplicitWait_AndSaveValue(0);

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		Homepage.deleteItemsFromShoppingCart();

		// Logic to remove all Shipping Info
		Navigation.navigate_Account_UpdateAddressBook();
		Account_UpdateAddressBook.delete_Address();

		//Add new address
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, address1,
					country, city, state_Abbreviated, zip, phone);
		} else {
			Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, address1,
					country, city, state_FullName, zip, phone);
		}

		// Logic to remove all Billing Info.
		Navigation.navigate_Account();
		Account.clear_PaymentHistory();
		
		Navigation.navigate_Home_viaLogo();

		//Add 2 merchandise items to cart
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Merchandise.addItemsToShoppingCart_Streetwise();		
		} else {
			Navigation.navigate_Merchandise_Jewelry();
			Merchandise.selectPageToNavigate_AndChooseRandomProduct();
			
			Navigation.navigate_Merchandise_Tech();		
			Merchandise.selectPageToNavigate_AndChooseRandomProduct();
		}
		
		
		//Adding this condition to check if item added to shopping cart is
		//not sold out. If yes we remove all items from cart and add new product
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
			ShoppingCart.remove_AllItems();
			
			if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				Merchandise.addItemsToShoppingCart_Streetwise();		
			} else {
				Navigation.navigate_Merchandise_Jewelry();
				Merchandise.selectPageToNavigate_AndChooseRandomProduct();
				
				Navigation.navigate_Merchandise_Tech();		
				Merchandise.selectPageToNavigate_AndChooseRandomProduct();
			}
		}
		
		ShoppingCart.click_ProceedToCheckout();
			
		if (ClientDataRetrieval.isRedesignClient()) {
			//If Billing address and payment change button is not displayed
			//(this means billing details were not saved for future purchases 
			// and if we click Review Order button, we see below error text
			if(!(BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_BillingAddress_ChangeBtn")))){
				expCreditCard_ErrorText = "Credit Card Number is required";
				expExpireMonth = "Expiration Month is required";
				expExpireYear = "Expiration Year is required";
				expCVVCode = "Security Code (CVV) is required";
				expCardHolderName = "Name on Card is required";
				expAddress1= "Address Line 1 is required";
				expCityName = "City is required";
				expStateName = "State is required";
				expZipCode = "Zip / Postal Code is required";
			} else {
				//If for instant we have saved the payment details for future purchases,
				//then we are clearing the text from all billing textboxes. Now when we click
				// Review order button we see below error text.
				CheckoutPage_PaymentInfo.click_AddNewPayment_Radio();
				expCreditCard_ErrorText = "Your Credit Card Number is required.";
				expExpireMonth = "Your Expiration Month is required.";
				expExpireYear = "Your Expiration Year is required.";
				expCVVCode = "Security Code should be 3 or 4 numeric value.";
				expCardHolderName = "Your Name on Card is required.";
				expAddress1= "Your Address Line 1 is required.";
				expCityName = "Your City is required.";
				expStateName = "State is required";
				expZipCode = "Your Zip Code is required.";
			}
		} else {
			//These values to validate legacy clients error text.
			expCreditCard_ErrorText = "Please enter your credit card number";
			expCVVCode = "Please enter your credit card security number";
			expCardHolderName = "Your Name on Card is required.";
			expAddress1= "Billing address is required";
			expCityName = "Billing city is required";
			expZipCode = "Please enter your zip code";
			
			CheckoutPage_ShippingInfo.navigate_To_NextPage();
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
				CheckoutPage_AllStateCash.click_Continue_ToNextPage();
			}
			CheckoutPage_PaymentInfo.click_AddNewPayment_Radio();
		}
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("checkout_newPayment_State"), "Select one...");
		}

		CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_CreditCardNumber() throws Exception {
		CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Credit Card Number", 
				expCreditCard_ErrorText);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_ExpirationMonth() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Expiration Date", 
					expExpireMonth);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Month", "Please enter expiration month");
		} else {
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("MM", "Please enter expiration month");
		}		
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_ExpirationYear() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			String actualExpdate = BaseUI.getTextFromField(Locator.lookupRequiredElement("checkout_ErrorMessage_ExpYear"));
			BaseUI.baseStringCompare("Expiration Year error text", expExpireYear, actualExpdate);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Year", "Please enter the expiration year");		
		} else {
		    CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("YY", "Please enter the expiration year");	
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_CVV_Code() throws Exception {
		CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Security Code (CVV)", 
				expCVVCode);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_CardholderName() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Name on Card",
					expCardHolderName);
		} else {
			if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				BaseUI.verifyElementHasExpectedText("checkout_CardHolderName_ErrorMessage", 
						"Please enter your name as appear on your credit card");
			} else {
				CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Cardholder Name",
						"Please enter your name as appear on your credit card");
			}
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_Address1() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedText("checkout_Address1_ErrorMessage", expAddress1);
		} else {
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Address Line 1", expAddress1);
		}		
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_Country() throws Exception {
		CheckoutPage_PaymentInfo.verify_FieldError_forDropdown_Displayed("Country", "Billing country is required");
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_City() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedText("checkout_City_ErrorMessage", expCityName);
		} else {
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("City", expCityName);
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests" })
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_State() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_PaymentInfo.verify_FieldError_forDropdown_Displayed("State", expStateName);
		} else {
			CheckoutPage_PaymentInfo.verify_FieldError_forDropdown_Displayed("State/Province", 
					"Billing state is required");
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step12_PT4790_Step19_NegativeTest_AddNewPayment_ErrorMessage_Zip() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			BaseUI.verifyElementHasExpectedText("checkout_Zipcode_ErrorMessage", expZipCode);
		} else {
			CheckoutPage_PaymentInfo.verify_FieldErrorDisplayed("Zip/Postal Code", expZipCode);
		}		
	}

	@Test(priority = 2, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3235_Step12_PT4790_Step19_NegativeTest_ExistingPayment_ErrorMessage_CVVField() throws Exception {
		Navigation.navigate_ShoppingCart();
		ShoppingCart.click_ProceedToCheckout();
		CheckoutPage_ShippingInfo.navigate_To_NextPage();
		if (BaseUI.elementExists("checkout_ExistingPayment_Radio", null, null)) {
			CheckoutPage_PaymentInfo.click_ExistingPayment_Radio();
			CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
				CheckoutPage_AllStateCash.click_Continue_ToNextPage();
			}
			CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
		}
		CheckoutPage_PaymentInfo.verify_FieldError_ExistingPayment_CVVFieldError();
	}

	@Test(priority = 3, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step15_PT4790_Step20_To_21_ExistingPayment_AbleTo_GoToReviewOrder() throws Exception {
		Navigation.navigate_ShoppingCart();
		ShoppingCart.click_ProceedToCheckout();
		if (ClientDataRetrieval.isLegacyClient()) {
			CheckoutPage_ShippingInfo.navigate_To_NextPage();
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
				CheckoutPage_AllStateCash.click_Continue_ToNextPage();
			}
			CheckoutPage_PaymentInfo.enterCvvCode_IfCvvTextboxIsBlank(cvvCode);
			CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		} else{
			CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		}
		BaseUI.verifyElementAppearsByString("checkout_ReviewOrder_Title");
	}

	@Test(priority = 4, groups = { "regression_Tests", "all_tests"})
	public void PT2756_Step17_PT4790_Step22_Click_EditPaymentInformation_TakesUser_toPaymentInfoPage() throws Exception {
		CheckoutPage_ReviewOrder.click_EditBilling();
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_AllStateCash.verify_OnCashRewards_Page();
		} else {
			BaseUI.verifyElementExists("checkoutPay_PaymentInfoTitle", null, null);
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		// Navigation.navigate_ShoppingCart();
		// ShoppingCart.click_ProceedToCheckout();
		// CheckoutPage_ShippingInfo.navigate_ToPaymentInfo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
