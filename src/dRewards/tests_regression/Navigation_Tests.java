package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.data.BaseTest;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.Navigation.introBanner;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Navigation_Tests extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {	
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	// Each set of data will generate a new test.
	@DataProvider(name = "Merchandise")
	public Object[][] createData_Merchandise() throws Exception {
		ArrayList<String> merchandiseList = new ArrayList<String>(Navigation.return_ListOfMerchandise_TopLinks());

		return return_linkTextAs_ObjectArray(merchandiseList);
	}

	@DataProvider(name = "Gift Cards")
	public Object[][] createData_GiftCards() throws Exception {
		ArrayList<String> giftCardList = Navigation.return_List_OfLinks_ByCategoryText("Gift Cards");

		return return_linkTextAs_ObjectArray(giftCardList);
	}

	@DataProvider(name = "LocalOffers")
	public Object[][] createData_LocalOffers() throws Exception {
		ArrayList<String> localOffersList;
		if (ClientDataRetrieval.isRedesignClient()) {
			localOffersList = Navigation.return_List_OfLinks_ByCategoryText("Local Deals");
		} else {
			localOffersList = Navigation.return_List_OfLinks_ByCategoryText("Local Offers");
		}
		return return_linkTextAs_ObjectArray(localOffersList);
	}

	@DataProvider(name = "Travel")
	public Object[][] createData_Travel() throws Exception {
		ArrayList<String> travelList = Navigation.return_List_OfLinks_ByCategoryText("Travel");

		return return_linkTextAs_ObjectArray(travelList);
	}

	@DataProvider(name = "Auctions")
	public Object[][] createData_Auctions() throws Exception {
		ArrayList<String> auctionsList = Navigation.return_List_OfLinks_ByCategoryText("Auctions");

		return return_linkTextAs_ObjectArray(auctionsList);
	}

	@DataProvider(name = "Sweepstakes")
	public Object[][] createData_Sweepstakes() throws Exception {
		ArrayList<String> sweepstakesList = Navigation.return_List_OfLinks_ByCategoryText("Sweepstakes");

		return return_linkTextAs_ObjectArray(sweepstakesList);
	}
	
	@DataProvider(name = "Magazines")
	public Object[][] createData_Magazines() throws Exception {
		ArrayList<String> magazinesList = Navigation.return_List_OfLinks_ByCategoryText("Magazines");

		return return_linkTextAs_ObjectArray(magazinesList);
	}

	public Object[][] return_linkTextAs_ObjectArray(ArrayList<String> dataSet) {
		Object[][] linksToNavigate = Navigation.returnLinkTextAs_ObjectArray(dataSet);

		return linksToNavigate;
	}
	
	
	@Test(dataProvider = "Magazines", groups = { "all_tests", "regression_Tests", "not_tvc", "not_smart", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_streetwise", "not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat", "not_jeepwave","not_mcafee" })
	public void PT3243_Step1_PT4777_Step1_Navigation_Magazines(String linkText) throws Exception {
		String category = "Magazines";

		Navigation.navigate_Menu_Submenu(category, linkText);
		
		Merchandise.verify_subCategoryActive(category, linkText);
		Navigation.verify_No_ApplicationErrorMessage();
	}

	
	@Test(dataProvider = "Sweepstakes", groups = {  "all_tests", "regression_Tests", "not_tvc", "not_citi" , "smoke_tests_rc_xuat"})
	public void PT3243_Step1_PT4777_Step1_Navigation_Sweepstakes(String linkText) throws Exception {
		String category = "Sweepstakes";
		String[] breadcrumbs;

		Navigation.navigate_Menu_Submenu(category, linkText);
		if (ClientDataRetrieval.isRedesignClient()) {
			breadcrumbs = new String[] { "Home", category };
			BaseUI.verifyElementHasExpectedText("verify_SweepstakesName", linkText);
		} else {
			if (linkText.equals(category)) {
				breadcrumbs = new String[] { "Home", "Current Sweepstakes" };
			} else if (linkText.equals("My Sweepstakes")) {
				breadcrumbs = new String[] { "Home", "Sweepstakes | History" };
			} else {
				breadcrumbs = new String[] { "Home", linkText };
			}
		}
		Navigation.verify_BreadcrumbList(breadcrumbs);
		Navigation.verify_No_ApplicationErrorMessage();
	}

	@Test(dataProvider = "LocalOffers",  groups = {  "all_tests", "regression_Tests" , "smoke_tests_rc_xuat"})
	public void PT3243_Step1_PT4777_Step1_Navigation_LocalOffers(String linkText) throws Exception {
		String category;
		String[] breadcrumbs;
		
		if (ClientDataRetrieval.isRedesignClient()) {
			category = "Local Deals";
			breadcrumbs = new String[] { "Home", category };
			Navigation.navigate_Menu_Submenu(category, linkText);
		} else {
			category = "Local Offers";
			Navigation.navigate_Menu_Submenu(category, linkText);
			if (!linkText.equals(category)) {
				breadcrumbs = new String[] { "Home", category, linkText };
				Merchandise.verify_subCategoryActive(category, linkText);
			} else {
				breadcrumbs = new String[] { "Home", category };
				Merchandise.verify_NoSubCategories_Active();
			}
		}	

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Navigation.verify_BreadcrumbList(breadcrumbs);
		}

		Navigation.verify_No_ApplicationErrorMessage();
	}

	@Test(dataProvider = "Auctions", groups = {  "all_tests", "regression_Tests", "not_tvc", "not_citi", "smoke_tests_rc_xuat", "not_jeepwave" })
	public void PT3243_Step1_PT4777_Step1_Navigation_Auctions(String linkText) throws Exception {
		String category = "Auctions";
		String[] breadcrumbs;
		
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Menu_Submenu(category, linkText);
			BaseUI.verifyElementHasExpectedText("AuctionLandingPage_Header_DetailPage", linkText);
		} else {
			String formatted_LinkText = BaseUI.remove_ExtraSpaces(linkText);
			formatted_LinkText = formatted_LinkText.replaceAll("\\<.*?>","");
			if (formatted_LinkText.equals("Smart Dollars + USD - Extended Play")) {
				Navigation.navigate_Auctions_SmartDollars_OR_Points_USD_And_ExtendedPlay();
			} else if (formatted_LinkText.equals("Points + USD - Extended Play")) {
				Navigation.navigate_Auctions_SmartDollars_OR_Points_USD_And_ExtendedPlay();
			} else if (formatted_LinkText.equals("Points Only - Extended Play")) {
				Navigation.navigate_Auctions_Points_Only_And_ExtendedPlay();
			} else {
				Navigation.close_AuctionBidPopup_IfAppears();
				Navigation.navigate_Menu_Submenu(category, linkText);
			}

			if (!linkText.equals(category)) {
				if (!ClientDataRetrieval.isRedesignClient()) {
					linkText = BaseUI.remove_ExtraSpaces(linkText);
				}
				breadcrumbs = new String[] { "Home", category, linkText };
			} else {
				breadcrumbs = new String[] { "Home", category, "All Auctions" };
			}

			Navigation.verify_BreadcrumbList(breadcrumbs);
		}
		Navigation.verify_No_ApplicationErrorMessage();
	}

	@Test(dataProvider = "Gift Cards",  groups = {  "all_tests", "regression_Tests" , "smoke_tests_rc_xuat"})
	public void PT3243_Step1_PT4777_Step1_Navigation_GiftCards(String linkText) throws Exception {
		String category = "Gift Cards";
		String[] breadcrumbs = null;

		Navigation.navigate_Menu_Submenu(category, linkText);
		
//		linkText = linkText.equals("e-Gift Cards") ? "eGift Cards" : linkText;
		
		if (!linkText.equals(category)) {
			if (ClientDataRetrieval.isRedesignClient()) {
				if(!linkText.equals("Entertainment")) {
					breadcrumbs = new String[] { "Home", "Gift Cards", linkText, "All" };
				} else {
					breadcrumbs = new String[] { "Home", "Gift Cards", "Movie Tickets", "All" };
				}
			} else {
				if(!linkText.equals("Entertainment")) {
					breadcrumbs = new String[] { "Home", "Gift Cards", linkText };
				} else {
					breadcrumbs = new String[] { "Home", "Gift Cards", "Movie Tickets" };
				}
			}
			Merchandise.verify_subCategoryActive(category, linkText);
		} else {
			breadcrumbs = new String[] { "Home", category };
			if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
					|| ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) 
					|| ClientDataRetrieval.isRedesignClient()) {
				Merchandise.verify_subCategoryActive(category, linkText);
			} else {
				Navigation.verify_IntroBanner_Appears(introBanner.GiftCards);
			}
		}
		
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Navigation.verify_BreadcrumbList(breadcrumbs);
		}

		Navigation.verify_No_ApplicationErrorMessage();	
	}
	
	@Test(dataProvider = "Merchandise",  groups = {  "all_tests", "regression_Tests", "smoke_tests_rc_xuat" })
	public void PT3243_Step1_PT4777_Step1_Navigation_Merchandise(String linkText) throws Exception {
		String category = "Merchandise";
		String[] breadcrumbs;

		Navigation.navigate_Menu_Submenu(category, linkText);
		if (!linkText.equals(category) && !linkText.equals("All Merchandise")) {
			breadcrumbs = new String[] { "Home", category, linkText };
			Merchandise.verify_subCategoryActive(category, linkText);
		} else {
			breadcrumbs = new String[] { "Home", category };
			if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
					|| ClientDataRetrieval.client_Matches(client_Designation.Allstate_both) 
					|| ClientDataRetrieval.isRedesignClient()) {
				Merchandise.verify_subCategoryActive(category, category);
			} else {
				Navigation.verify_IntroBanner_Appears(Navigation.getIntroBanner(linkText));
			}
		}

		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) && linkText.equals("Daily Deals")) {
				} else if (ClientDataRetrieval.client_Matches(client_Designation.RedesignCore) && linkText.equals("Daily Deals")) {
				} else {
					Navigation.verify_BreadcrumbList(breadcrumbs);	
				}			
		}
		Navigation.verify_No_ApplicationErrorMessage();

	}

	@Test(dataProvider = "Travel",  groups = {  "all_tests", "regression_Tests", "smoke_tests_rc_xuat"})
	public void PT3243_Step1_PT4777_Step1_Navigation_Travel(String linkText) throws Exception {
		String category = "Travel";
		String[] breadcrumbs;

		Navigation.navigate_Menu_Submenu(category, linkText);
		
		if (ClientDataRetrieval.isRedesignClient()) {
			breadcrumbs = new String[] { "Home", category };
		} else {
			if (linkText.equals("Travel Gift Cards")) {
				breadcrumbs = new String[] { "Home", "Gift Cards", "Travel" };
				Merchandise.verify_subCategoryActive(category, "Travel");
			} else if (linkText.equals("Travel Essentials")) {
				breadcrumbs = new String[] { "Home", "Merchandise", linkText };
				Merchandise.verify_subCategoryActive(category, linkText);
			} else if (!linkText.equals(category)) {
				if (linkText.equals("Cruises")) {
					linkText = "Cruise";
				}

				breadcrumbs = new String[] { "Home", category, linkText };
			} else {
				breadcrumbs = new String[] { "Home", category };
			}

			if (!ClientDataRetrieval.client_Matches(client_Designation.AARP)
					&& !ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
				if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards) && !linkText.equals("Cruises")) {
				} else if (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing) && !linkText.equals("Cruises")) {
				} else if ((ClientDataRetrieval.client_Matches(client_Designation.Citi) && linkText.equals("Travel Essentials"))
						 || (ClientDataRetrieval.client_Matches(client_Designation.Citi) && linkText.equals("Travel Gift Cards"))) {
					BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("nav_IntroBanner"));
				} else if ((ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)||
						ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) && (linkText.equals("Travel Gift Cards")
						||linkText.equals("Travel Essentials"))) {
					BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("nav_IntroBanner"));
				} else {
					Navigation.verify_IntroBanner_Appears(Navigation.getIntroBanner(linkText));
				}
			}
		}
		
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Navigation.verify_BreadcrumbList(breadcrumbs);
		}
		Navigation.verify_No_ApplicationErrorMessage();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
	
}// End of Class
