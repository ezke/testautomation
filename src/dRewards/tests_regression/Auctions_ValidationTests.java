package dRewards.tests_regression;

import dRewards.pageControls.AuctionTile;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Auction_Details;
import dRewards.pages.Auctions;
import dRewards.pages.Auctions_OpenAuctions;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewAuction_Admin;
import dRewards.pages_Administration.AddNewAuction_SubAuctionModal_Admin;
import dRewards.pages_Administration.EditBuyerProgram_AdminSite;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

//@Test(groups = { "not_IE" })
public class Auctions_ValidationTests extends BaseTest {

	private WebDriver originalDriver;
	private WebDriver secondBidderDriver;
	private String original_Browser;

	private String auctionName = "AutomationTest";
	private Double startingBid = 500.00;
	private Double currentBid = 500.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;
	// String productSku = "aucdysonDC44demo";
	private String productSku = "auc46samsungsmarthdtvDEMO";
	// String expectedProductName = "DC44 Animal Digital Slim Cordless Vacuum
	// with Handheld Tool Kit - DEMO ONLY";
	private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
	private Boolean isDemo = true;

	// Auction time in Minutes
	private Integer auctionTimeToLast = 5;

	private String user1_previousPoints;
	private TableData bidHistory;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		bidHistory = new TableData();
		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		auctionName += randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		original_Browser = Browser.currentBrowser;

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			auctionTimeToLast += 4;
		}

		LoginPage_Admin.login_Admin();
		EditBuyerProgram_AdminSite.UpdateWinLimit_Attribute_13011();

		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction(auctionName, RedemptionTypes.FullRedemption, startingBid, extendTime,
				productSku, AuctionTypes.Standard, isDemo);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Auctions_AllAuctions();
		user1_previousPoints = Navigation.returnPoints();
	}

	@Test(priority = 9, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step3_PT4754_Step2_Verify_Auctions_DetailsPage() throws Exception {

		String expectedPointsOrRewards = "Enter an amount that is higher than the current bid";// + ClientDataRetrieval.client_Currency + "";

		if (ClientDataRetrieval.isRedesignClient()) {
			expectedPointsOrRewards = "Enter a Points amount that is at least 1 higher than the current bid.";
		}
		AuctionTile auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.click_DetailsButton();
		Auction_Details.verify_AuctionDetails_SingleCurrency(currentBid, expectedPointsOrRewards);
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step4_PT4754_Step3_Verify_Invalid_Error_Message() throws Exception {

		String invalidBid = "abc";
		String expectedErrorMessage = "Please enter a valid max bid amount.";

		if (ClientDataRetrieval.isRedesignClient()) {
			expectedErrorMessage = "Your bid is required.";
		}
		Auction_Details.verify_Bid_ErrorMessages_SingleCurrency(invalidBid, expectedErrorMessage);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step4_PT4754_Step3_Verify_BidLow_Error_Message() throws Exception {

		currentBid += 1;
		String expectedErrorMessage = "";
		String lowBid = "450";
		String currentBidStr = BaseUI.convertDouble_ToString_ForCurrency(currentBid);
		if (ClientDataRetrieval.isRedesignClient()) {
			currentBidStr = currentBidStr.split("\\.")[0];
			expectedErrorMessage = "Bid Rejected. Max bid is too low, Enter " + currentBidStr + " or more.";
		} else {
			currentBidStr = currentBidStr.split("\\.")[0];
			expectedErrorMessage = "Your bid is too low. Enter " + currentBidStr + " or more.";
		}
		Auction_Details.verify_Bid_ErrorMessages_SingleCurrency(lowBid, expectedErrorMessage);
	}

	@Test(priority = 30, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step4_PT4754_Step3_Verify_FirstUserBid_Successful() throws Exception {

		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(0).get("bidAmount"));
		Auction_Details.verify_BidSuccessful(bidHistory.data.get(0).get("bidAmount"));

	}

	@Test(priority = 40, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step4_PT4754_Step3_Verify_SecondUserBid_Successful() throws Exception {

		// Set up Second Bidder
		originalDriver = Browser.driver;
		ClientDataRetrieval.set_secondaryUser_Info(original_Browser);
		Browser.openBrowser(ClientDataRetrieval.url, "chrome");
		secondBidderDriver = Browser.driver;

		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			LoginPage.loginForSecondBidder_ForAuctions(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
		} else {
			LoginPage.login(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
		}

		Navigation.navigate_Auctions_AllAuctions();
		AuctionTile auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, currentBid);
		currentBid += 9;

		auctionTile.click_DetailsButton();
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(1).get("bidAmount"));

		Integer currentBidInt = (int) (currentBid - 8);
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)) {
			String currentBidSTR = BaseUI.convertDouble_ToString_ForCurrency(Double.valueOf(currentBidInt)).trim();
			Auction_Details.verify_BidSuccessful(String.valueOf(currentBidSTR));
		} else {
			Auction_Details.verify_BidSuccessful(String.valueOf(currentBidInt));
		}
	}

	@Test(priority = 50, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", 
			"all_tests", "not_IE"})
	public void PT3664_Step4_PT4754_Step3_Verify_FirstUser_Bidder_WasOutbid() throws Exception {
		Browser.driver = originalDriver;
		Auction_Details.verify_Outbid(user1_previousPoints);
		BaseUI.baseStringCompare("Points match previous count.", user1_previousPoints, Navigation.returnPoints());
	}

	@Test(priority = 60, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests",
			"all_tests", "not_IE"})
	public void PT3664_Step4_PT4754_Step3_Verify_User_Bid_WasQuicker() throws Exception {
		String expectedMessage;
		Browser.driver = originalDriver;
		if (ClientDataRetrieval.isRedesignClient()) {
			expectedMessage = "Bid Rejected. Someone else got that bid amount in quicker. Don't give up.";
		} else {
			expectedMessage = "Someone else got that bid amount in quicker. Don't give up.";
		}
		Auction_Details.verify_Bid_ErrorMessages_SingleCurrency(bidHistory.data.get(1).get("bidAmount"),
				expectedMessage);
	}
	
	@Test(priority = 70, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", 
			"all_tests", "not_IE"})
	public void PT3664_Step16_Verify_User_DoesNot_HaveEnoughPointsOrRewards() throws Exception {

		String expectedMessage = "";
		String pointsOrRewards = Navigation.returnPoints();
		if (ClientDataRetrieval.isRedesignClient()) {
			expectedMessage = "The amount you entered exceeds your Points balance";
		} else {
			expectedMessage = "Your max bid can not be more than your total points balance.";
		}
		Integer pointsOrRewardsINT = Integer.parseInt(pointsOrRewards);
		pointsOrRewardsINT+=100;
		Auction_Details.verify_Bid_ErrorMessages_SingleCurrency(String.valueOf(pointsOrRewardsINT), expectedMessage);
	}
	
	@Test(priority = 80, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", 
			"all_tests", "not_IE"})
	public void PT3664_Step16_Verify_AuctionEnds_SecondUserWins_PopupAppears() throws Exception {

		Browser.driver = secondBidderDriver;
		Auctions.loginBackAndClickMyAccount_CloseAuctions();
		Auction_Details.wait_For_End_OfAuction(auctionTimeToLast * 60);
		Auction_Details.wait_For_ClaimPopup_ToAppear();

		if (!ClientDataRetrieval.isRedesignClient()) {
			Auction_Details.verify_AuctionWon_PopupAppears();
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondBidderDriver != null) {
			Browser.driver = secondBidderDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
	}

}
