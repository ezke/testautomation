package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;

public class LocalDeals_RedeemFunctionality extends BaseTest {

    private String city = "Boca Raton";
    private String category = "Dining";
    private String address = "861 NW 51st ST";
    private String state = "FL";
    private String pointBeforeRedeemingOffer, pointsAfterCancelBtnClicked;
    private String dealsPoints;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_LocalOffers_SearchPage();
        LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria("Florida", city, category, address);
        Navigation.click_SeeLocalOffer_SearchPageButton();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4538_Step1_VerifyLocalDealSearchResults_HeaderTitle() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_TitleText"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_TitleText",
                "Deals near "+ city + ", "+ state);
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifyLocalDealSearchResults_TextDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ShowingLocation_ResultCount", "Showing Locations");
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_ClickFirstDeal_VerifyRedeemButtonDisplayed() throws Exception {
        LocalOffers_LandingPage.clickFirstDeal();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemNowBtn"));
        BaseUI.pageSourceContainsString( "Redeem Now");
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_ImgLogoDisplayed() {
        
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealImgLogo"));
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_DealNameMatches() {
        String dealName = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Title"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ResultList_FirstDealExpanded_Title", dealName);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_DealCountMatches() {
        String dealCount = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal_Count"))
                .replace(" Deals", "");
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_DealCount"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ResultList_FirstDealExpanded_DealCount", dealCount);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_DealDistanceMatches() {
        String dealDistance = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal_Distance"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Distance"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_Distance", dealDistance);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_DealCountAndDealCountListMatches() {
        String dealCount = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDeal_Count"))
                .replace(" Deals", "");

        ArrayList<WebElement> localDealList = Locator
                .lookup_multipleElements("localOffer_ResultList_FirstDealExpanded_DealCountList", null, null);
        BaseUI.verify_true_AndLog(localDealList.size() > 0, "Resulting Local Deals list is NOT EMPTY",
                "Resulting Local Deals list is EMPTY");
        BaseUI.verify_true_AndLog(localDealList.size() == Integer.parseInt(dealCount),
                "Local Deals when expanded MATCH the Deal count displayed on search results page",
                "Local Deals when expanded DOES NOT MATCH the Deal count displayed on search results page");
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_DealPointsDisplayedWhenExpanded() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_DealPoints"));
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_PrinterIconDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrinterIcon"));
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_MobileIconDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_MobileIcon"));
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_RedeemBtnDisplayedForPrinterIconDeal() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemNowBtn"));
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_DescriptionDisplayedForMobileIconDeal() {
        String dealDescriptionForMobileDeal = "This deal can only be redeemed on a mobile device.";

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_Mobile_Description"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_Mobile_Description", dealDescriptionForMobileDeal);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_RedeemThiDealText_ForPrinterIconDeal() {
        String dealDescriptionAboveRedeemNowBtn = "Redeem this deal for";

        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemThisDealText"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ResultList_FirstDealExpanded_Printer_RedeemThisDealText", dealDescriptionAboveRedeemNowBtn);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_VerifySelectedLocalDeal_Description_OnceRedeemedThisDeal_AvailableFor14Days_ForPrinterIconDeal() {
        String dealDescriptionBelowRedeemNowBtn = "Once you redeem this deal, it will be available for 14 days.";

        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_Printer_OnceRedeemThisDeal_AvailableFor14DaysText"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_Printer_OnceRedeemThisDeal_AvailableFor14DaysText", dealDescriptionBelowRedeemNowBtn);
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step1_ClickRedeemNowBtn_VerifyPopupModalAppears() throws Exception {
        pointBeforeRedeemingOffer = BaseUI.getTextFromField(Locator.lookupElement("nav_AmountOfPoints"));
        LocalOffers_LandingPage.clickFirstDeal_RedeemNowBtn();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_PrintOffer_PopupModal_CloseIcon"));
    }

    @Test(priority = 35, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4732_Step1_ClickRedeemNowBtn_VerifyPopupModalAppears" })
    public void PT4732_Step2_ClickCancelBtn_VerifyPopupModalCloses() throws Exception {
        LocalOffers_LandingPage.clickFirstDeal_RedeemNow_PopupModal_CancelBtn();
        BaseUI.verifyElementDoesNOTExist("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_MakeSurePrinterIsOperationalText", null, null);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step2_VerifyLocalDealHeaderAppears_AfterRedeemNowModalClosed() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_CurrentLocation"));
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step2_VerifyRedeemNowButtonIsNotDisabled() throws Exception {
        BaseUI.verifyElementEnabled(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemNowBtn"));
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step2_VerifyPointsNotDeducted_WhenLocalDealNotRedeemed() throws Exception {
        pointsAfterCancelBtnClicked = BaseUI.getTextFromField(Locator.lookupElement("nav_AmountOfPoints"));
        BaseUI.verify_true_AndLog(pointsAfterCancelBtnClicked.equals(pointBeforeRedeemingOffer), "Points are NOT DEDUCTED",
                "Points are DEDUCTED");
    }

    @Test(priority = 45, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4732_Step3_ClickRedeemNowBtn_VerifyPopUpModal_PrintIconIsDisplayed() throws Exception {
        dealsPoints = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_DealPoints"));
        LocalOffers_LandingPage.clickFirstDeal_RedeemNowBtn();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_PrinterIcon"));
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4732_Step3_ClickRedeemNowBtn_VerifyPopUpModal_PrintIconIsDisplayed" })
    public void PT4732_Step3_VerifyPopUpModal_PrinterIsAvailableText_IsDisplayed() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_MakeSurePrinterIsOperationalText"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_MakeSurePrinterIsOperationalText",
                "Make sure your printer is operational.");
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4732_Step3_ClickRedeemNowBtn_VerifyPopUpModal_PrintIconIsDisplayed" })
    public void PT4732_Step3_VerifyPopUpModal_VerbiageTextAbovePrintNowBtn() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_TextDescription_AbovePrintNowBtn"));
        BaseUI.verifyElementHasExpectedText("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_TextDescription_AbovePrintNowBtn",
                "When you click "+'"'+"Print Now,"+'"'+ "\n"+ dealsPoints +" will be deducted from your account and" + "\n"+
                        "your deal will automatically be sent to your printer." + "\n"+ "Your deal must be used within 14 days and may only be used once.");
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4732_Step3_ClickRedeemNowBtn_VerifyPopUpModal_PrintIconIsDisplayed" })
    public void PT4732_Step3_VerifyPrintNowButtonIsDisplayedAndEnabled() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_PrintNowBtn"));
        BaseUI.verifyElementEnabled(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_PrintNowBtn"));
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4732_Step3_ClickRedeemNowBtn_VerifyPopUpModal_PrintIconIsDisplayed" })
    public void PT4732_Step3_VerifyCancelButtonIsDisplayedAndEnabled() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_CancelBtn"));
        BaseUI.verifyElementEnabled(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_PrintOffer_PopupModal_CancelBtn"));
    }

    @Test(priority = 55, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4732_Step3_ClickRedeemNowBtn_VerifyPopUpModal_PrintIconIsDisplayed" })
    public void PT4732_Step4_ClickCancelBtn_VerifyPopupModalClosed() throws Exception {
        //LocalOffers_LandingPage.clickFirstDeal_RedeemNow_PopupModal_PrintNowBtn();
        LocalOffers_LandingPage.clickPopupModal_CloseIcon();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("localOffer_ResultList_PrintOffer_PopupModal_CloseIcon"));
        //BaseUI.verifyElementDisabled(Locator.lookupElement("localOffer_ResultList_FirstDealExpanded_Printer_RedeemNowBtn"));
    }

    //Removing this validation because when Print Now button is clicked,
    //it opens a print modal and we are not interact with windows page.
//    @Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
//            "not_allstateCash", "not_streetwise", "not_citi" })
//    public void PT4732_Step4_ClickRedeemNowBtn_VerifyDealRedeemed_AndPointsDeducted() throws Exception {
//        pointsAfterCancelBtnClicked = BaseUI.getTextFromField(Locator.lookupElement("nav_AmountOfPoints"));
//        BaseUI.verify_true_AndLog(!(pointsAfterCancelBtnClicked.equals(pointBeforeRedeemingOffer)), "Points are DEDUCTED",
//                "Points are NOT DEDUCTED");
//    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Logout();
        }
        Browser.closeBrowser();
    }
}
