package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;

public class MerchandiseLandingPage_CategoryImageValidation extends BaseTest {

    private ArrayList<String> allCategoryHeaders;
    private ArrayList<String> allCategoryImages;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Merchandise();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4794_Step1_VerifyMerchandiseHeader() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchandisePage_Header"));
        BaseUI.verifyElementHasExpectedText("merchandisePage_Header", "Merchandise");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4794_Step2_VerifyMerchandise_AllCategoryImages() {
        Merchandise.verifyCategoryImagesDisplayed();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4794_Step2_VerifyMerchandise_AllCategoryHeaders() {
        Merchandise.verifyCategoryHeadersDisplayed();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4794_Step2_VerifyMerchandise_CategoryHeaders_And_CategoryImages_ListMatch() {
        allCategoryHeaders = Merchandise.return_CategoryHeaders_Displayed();
        allCategoryImages = Merchandise.return_CategoryImages_Displayed();

        BaseUI.verify_true_AndLog(allCategoryHeaders.size() == allCategoryImages.size(), "Category Headers displayed MATCH Category images list",
                "Category Headers displayed DOES NOT MATCH Category images list");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4794_Step2_VerifyMerchandise_AllCategoryImages"})
    public void PT4794_Step3_Merchandise_ClickSunglassesCategoryImage_VerifyCategoryLandingPage_Header() throws Exception {
        Merchandise.clickOnCategoryImage_ValidateBreadcrumbAndHeader("Sunglasses");
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4794_Step2_VerifyMerchandise_AllCategoryImages"})
    public void PT4794_Step3_Merchandise_ClickHandbagsCategoryImage_VerifyCategoryLandingPage_HeaderAndBreadcrumb() throws Exception {
        Merchandise.clickOnCategoryImage_ValidateBreadcrumbAndHeader("Handbags");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4794_Step2_VerifyMerchandise_AllCategoryImages"})
    public void PT4794_Step3_Merchandise_ClickJewelryCategoryImage_VerifyCategoryLandingPage_HeaderAndBreadcrumb() throws Exception {
        Merchandise.clickOnCategoryImage_ValidateBreadcrumbAndHeader("Jewelry");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4794_Step2_VerifyMerchandise_AllCategoryImages"})
    public void PT4794_Step3_Merchandise_ClickHomeAndKitchenCategoryImage_VerifyCategoryLandingPage_HeaderAndBreadcrumb() throws Exception {
        Merchandise.clickOnCategoryImage_ValidateBreadcrumbAndHeader( "Home & Kitchen");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4794_Step2_VerifyMerchandise_AllCategoryImages"})
    public void PT4794_Step3_Merchandise_ClickSportsAndOutdoorsCategoryImage_VerifyCategoryLandingPage_HeaderAndBreadcrumb() throws Exception {
        Merchandise.clickOnCategoryImage_ValidateBreadcrumbAndHeader( "Sports & Outdoors");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4794_Step2_VerifyMerchandise_AllCategoryImages"})
    public void PT4794_Step3_Merchandise_ClickBeautyCategoryImage_VerifyCategoryLandingPage_HeaderAndBreadcrumb() throws Exception {
        Merchandise.clickOnCategoryImage_ValidateBreadcrumbAndHeader( "Beauty");
    }


    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {

        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.navigate_Merchandise();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }

}
