package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class LocalOffers_SearchPageResult_and_ErrorValidation extends BaseTest {

	String state_Abbreviation = "FL";
	String state_FullName = "Florida";
	String city = "Fort Lauderdale";
	String cityDiff = "Boca Raton";
	String category = "All Offers";
	String categoryDiff = "Dining";
	String location = "Fort Lauderdale, FL";
	String locationDiff = "Boca Raton, FL";
	String availableOffer = "Available Offer";
	String availableOffers = "Available Offers";
	String address = "861 Nw 51st ST";
	String cityDetailsDiff = "Boca Raton, FL 33431";
	String cityDetails = "Fort Lauderdale, FL 33301";
	String cityDetailsDiff_WithNoZip = "Boca Raton, FL";
	String cityDetails_WithNoZip = "Fort Lauderdale, FL";

	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_LocalOffers_SearchPage();
	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step21_Verify_LocalOffer_SearchPageTitle() throws Exception {

		LocalOffers_LandingPage.verify_LocalOffer_Title_Appears();
	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step21_Verify_LocalOffer_SearchPageCityState_ErrorValidation() throws Exception {

		LocalOffers_LandingPage.verify_LocalOffer_SearchPageError();
	}
	
	
	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests", "not_jeepwave", "not_smart", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_tvc", "not_citi" })
	public void PT3247_LocalOffer_Step21_Verify_LocalOffer_SearchPageZip_ErrorValidation() throws Exception {

		LocalOffers_LandingPage.verifyZipcodeRequiredError();
	}
	

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step5_Verify_SearchResultPage_BreadcrumbList() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_FullName, city, category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			String[] breadcrumbArray = { "Home", "Local Deals" };
			Navigation.verify_BreadcrumbList(breadcrumbArray);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Abbreviation, city, category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				String[] breadcrumbArray = { "Home", "Local Offers" };
				Navigation.verify_BreadcrumbList(breadcrumbArray);
			}
		}
		
		String currentLocation = BaseUI.getTextFromField(Locator.lookupElement("localOffer_CurrentLocation"));
		String location_abbreviated =  ClientDataRetrieval.environment.equals("RC") ? "Ft." : "Ft";
		BaseUI.baseStringCompare("localOffer_CurrentLocation", location, 
				currentLocation.replaceAll("Deals near ", "").replaceAll(location_abbreviated, "Fort"));

	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step5_Verify_LocalOffer_SearchResultPage_LogoIMG() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_FullName, city, category);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Abbreviation, city, category);
		}
		Navigation.click_SeeLocalOffer_SearchPageButton();
		LocalOffers_LandingPage.verify_LocalOffer_LogoIMG();

	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step5_Verify_LocalOffer_SearchResultPage_DetailsCity() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_FullName, city, category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			LocalOffers_LandingPage.verify_LocalOffer_SearchResultPage_DetailsCityName(cityDetails_WithNoZip);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Abbreviation, city, category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			LocalOffers_LandingPage.verify_LocalOffer_SearchResultPage_DetailsCityName(cityDetails);
		}
	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step5_Verify_LocalOffer_SearchResultPage_AvailableOffer() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_FullName, city, category);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Abbreviation, city, category);
		}
		Navigation.click_SeeLocalOffer_SearchPageButton();

		LocalOffers_LandingPage.verify_availableOffer_TextLable();

	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step7_8_Verify_SearchDining_ResultPage_BreadcrumbList() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_FullName, cityDiff, categoryDiff, address);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			String[] breadcrumbArray = { "Home", "Local Deals" };
			Navigation.verify_BreadcrumbList(breadcrumbArray);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_Abbreviation, cityDiff, categoryDiff, address);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				String[] breadcrumbArray = { "Home", "Local Offers", "Dining" };
				Navigation.verify_BreadcrumbList(breadcrumbArray);
			}
		}
		
		String currentLocation = BaseUI.getTextFromField(Locator.lookupElement("localOffer_CurrentLocation"))
				.replaceAll("Deals near ", "");
		BaseUI.baseStringCompare("localOffer_CurrentLocation", currentLocation, locationDiff);

	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step7_8_Verify_LocalOffer_SearchDining_ResultPage_LogoIMG() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_FullName, cityDiff, categoryDiff, address);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_Abbreviation, cityDiff, categoryDiff, address);
		}
		Navigation.click_SeeLocalOffer_SearchPageButton();
		LocalOffers_LandingPage.verify_LocalOffer_LogoIMG();

	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step7_8_Verify_LocalOffer_SearchDining_ResultPage_DetailsCity() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_FullName, cityDiff, categoryDiff, address);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			LocalOffers_LandingPage.verify_LocalOffer_SearchResultPage_DetailsCityName(cityDetailsDiff_WithNoZip);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_Abbreviation, cityDiff, categoryDiff, address);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			LocalOffers_LandingPage.verify_LocalOffer_SearchResultPage_DetailsCityName(cityDetailsDiff);
		}
	}

	@Test(groups = { "regression_Tests", "critical_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step7_8_Verify_LocalOffer_SearchDining_ResultPage_AvailableOffer() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_FullName, cityDiff, categoryDiff, address);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state_Abbreviation, cityDiff, categoryDiff, address);
		}
		Navigation.click_SeeLocalOffer_SearchPageButton();
		LocalOffers_LandingPage.verify_availableOffer_TextLable();

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Home_viaLogo();
		}
		Navigation.navigate_LocalOffers_SearchPage();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}