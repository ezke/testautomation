package dRewards.tests_regression;

import dRewards.pageControls.AuctionTile;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Auction_Details;
import dRewards.pages.Auctions;
import dRewards.pages.Auctions_MyAuctions;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewAuction_Admin;
import dRewards.pages_Administration.AddNewAuction_SubAuctionModal_Admin;
import dRewards.pages_Administration.EditBuyerProgram_AdminSite;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

//@Test(groups = { "not_IE" })
public class Auctions_FollowAuctions_Tests extends BaseTest {

	private String auctionName = "AutomationTest";
	private Double startingBid = 50.00;
	private Double currentBid = 50.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;
	// String productSku = "aucdysonDC44demo";
	private String productSku = "auc46samsungsmarthdtvDEMO";
	private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
	private Boolean isDemo = true;

	private AuctionTile auctionTile = AuctionTile.empty();
	// Auction time in Minutes
	private Integer auctionTimeToLast = 4;
	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		auctionName += randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.isRedesignClient()) {
			auctionTimeToLast += 1;
		}

		LoginPage_Admin.login_Admin();
		EditBuyerProgram_AdminSite.UpdateWinLimit_Attribute_13011();
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction(auctionName, RedemptionTypes.FullRedemption, startingBid, extendTime,
				productSku, AuctionTypes.Standard, isDemo);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Auctions_AllAuctions();
	}

	@Test(priority = 10, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step17_PT4788_Step1_Verify_Auctions_DetailsPage() throws Exception {

		String expectedPointsOrRewards; 
		if (ClientDataRetrieval.isRedesignClient()) {
			expectedPointsOrRewards = "Enter a Points amount that is at least 1 higher than the current bid.";
			auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
			auctionTile.click_AuctionsImage();
		} else {
			expectedPointsOrRewards = "Enter an amount that is higher than the current bid";// + ClientDataRetrieval.client_Currency + "";
			auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
			auctionTile.click_DetailsButton();
		}		
		Auction_Details.verify_AuctionDetails_SingleCurrency(currentBid, expectedPointsOrRewards);
		Auction_Details.verify_AddToMyAuctions_LinkAppears();

	}

	@Test(priority = 20, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step17_PT4788_Step1_Auctions_DetailsPage_Add_Auction_ToMyAuctions() throws Exception {

		Auction_Details.click_AddToMyAuctions();
		Auction_Details.verify_YourAreFollowingThisAuction_LinkAppears();
	}

	@Test(priority = 30, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step17_PT4788_Step1_Verify_MyAuctions_CurrentAuction_Following() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			Thread.sleep(1000);
			String actualAuctionTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("auction_Title"));
			BaseUI.baseStringCompare("Validate Auction Added to Watch List is displayed", 
					expectedProductName, actualAuctionTitle);
		} else {
			Navigation.navigate_Auctions_MyAuctions();
			Auctions_MyAuctions.verify_CurrentAuctionFollowing_Appears();
		}		
	}

	@Test(priority = 40, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step17_PT4788_Step1_Verify_Auctions_DetailsPage_StopFollowingLink() throws Exception {

		Navigation.navigate_Auctions_AllAuctions();
		if (ClientDataRetrieval.isRedesignClient()) {
			auctionTile.click_AuctionsImage();
		} else {
			auctionTile.click_DetailsButton();
		}
		
		Auction_Details.verify_StopFollowingAuction_Text();
	}

	@Test(priority = 50, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step17_PT4788_Step1_Verify_Auctions_DetailsPage_YouAreFollowingAuction_Removed() throws Exception {

		Auction_Details.click_YouAreFollowingThisAuction_Link();
		Auction_Details.verify_AddToMyAuctions_LinkAppears();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {

		} finally {
			Browser.closeBrowser();
		}
	}

}
