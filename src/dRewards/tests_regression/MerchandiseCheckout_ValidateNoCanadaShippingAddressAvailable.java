package dRewards.tests_regression;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class MerchandiseCheckout_ValidateNoCanadaShippingAddressAvailable extends BaseTest {

    // Variables for Shipping Info and same values used for Billing.
    private static final String firstName = "QA";
    private static final String lastName = "Auto";
    private static final String address1 = "1225 Broken Sound";
    private static final String city = "Boca Raton";
    private static String state_Abbreviated = "FL";
    private static String state_FullName = "Florida";
    private static String zip = "33487";
    private static final String phone = "9544151992";

    private static String ccType = "MasterCard";
    private static final String ccNumber = "5105105105105100";
    private static final String cvvCode = "211";
    private static final String expire_month = "12";
    private static final String expire_year = "22";
    private static final String country = "United States";

    //Canada mailing address
    private static final String canada_StreetAddress1 = "1010 Easy St";
    private static final String canada_CityName = "Ottawa";
    private static String canada_State_FullName = "Ontario";
    private static String canada_Zip = "K1A0B1";
    private static String canada_PhoneNum = "416-555-0134";


    // Variables for product info/pay info
    private TableData shoppingCartData;
    private PricingDetails pricingDetails;
    private String points_Before_Checkout;


    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        // Logic to remove all items from Shopping Cart.
        Homepage.deleteItemsFromShoppingCart();

        // Logic to remove all Shipping Info
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.delete_Address();

        // Logic to remove all Billing Info.
        Navigation.navigate_Account();
        Account.clear_PaymentHistory();
        Navigation.navigate_Home_viaLogo();

        // Add a products to cart.
        //Replace Men's shop to Jewelry because this category is been removed for all clients.

        Navigation.navigate_Merchandise_Beauty();
        Merchandise.selectPageToNavigate_AndChooseRandomProduct();


        //Adding this condition to check if item added to shopping cart is
        //not sold out. If yes we remove all items from cart and add new product
        if (BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
            ShoppingCart.remove_AllItems();

            Navigation.navigate_Merchandise_Beauty();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        }

        // Add logic here to pull product info from Shopping Cart page
        pricingDetails = ShoppingCart.retrieve_PricingDetails();
        pricingDetails.set_SavingsAmount_Citi();
        shoppingCartData = ShoppingCart.get_Table_Data();
        points_Before_Checkout = Navigation.returnPoints();

        ShoppingCart.click_ProceedToCheckout();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step1_CheckoutPage_ValidateStateDropdown_DoesNotHaveCanadaState() throws Exception {
        if (CheckoutPage_ShippingInfo.newShippingAddress_Radio_Exists()) {
            CheckoutPage_ShippingInfo.click_AddNewShippingAddress_Radio();
        }

        CheckoutPage_ShippingInfo.add_CanadaAddress(firstName, lastName, canada_StreetAddress1,
                canada_CityName, canada_Zip, phone);
        BaseUI.verifyElementDoesNotHaveExpectedText("checkout_NewAddress_StateDropdown", canada_State_FullName);
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step1_CheckoutPage_ShippingInfo_ValidateCountryDropdown_DoesNotHaveCanada() {
        BaseUI.verifyElementDoesNotHaveExpectedText("checkout_NewAddress_CountryDropdown", "Canada");
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step1_CheckoutPage_ShippingInfo_ValidateCountryDropdown_HasUnitedStatesOnly() {
        BaseUI.verifyElementHasExpectedText("checkout_NewAddress_CountryDropdown", "United States");
    }


    @Test(priority = 21, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step2_NavigateToMyRewardsAddressPage_UpdateAddress_ValidateCheckoutPage_ShippingInfoDisplayed() throws Exception {
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, address1,
                country, city, state_FullName, zip, phone);
        Navigation.navigate_ShoppingCart();
        ShoppingCart.click_ProceedToCheckout();
        String actualAddress = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress1",
                        firstName, null));
        BaseUI.baseStringCompare("checkout_ExistingAddress_ShippingAddress1", address1, actualAddress);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step2_CheckoutPage_SavedShippingInfo_ChangeButtonDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkout_AddNewShippingAddress"));
        BaseUI.verifyElementHasExpectedText("checkout_AddNewShippingAddress", "CHANGE");
    }


    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step2_CheckoutPage_SavedShippingInfo_ClickChangeButton_ValidateStateDropdown_DoesNotDisplayCanadianState() throws Exception {
        CheckoutPage_ShippingInfo.click_Checkout_ShippingAddress_ChangeBtn();
        CheckoutPage_ShippingInfo.add_CanadaAddress(firstName, lastName, canada_StreetAddress1,
                canada_CityName, canada_Zip, phone);
        BaseUI.verifyElementDoesNotHaveExpectedText("checkout_NewAddress_StateDropdown", canada_State_FullName);
    }

    @Test(priority = 35, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step2_CheckoutPage_SavedShippingInfo_ValidateCountryDropdown_HasUnitedStatesOnly() {
        BaseUI.verifyElementHasExpectedText("checkout_NewAddress_CountryDropdown", "United States");
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step4_NavigateToMyRewardsAddressPage_ClickEdit_ValidateStateDropdown_DoesNotHaveCanadianStateOption() throws Exception {
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.clickEditButton();
        BaseUI.verifyElementDoesNotHaveExpectedText("accnt_newAddr_state_Dropdown", canada_State_FullName);
    }

    @Test(priority = 45, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Ste4_MyRewards_EditAddressPage_ValidateCountryDropdown_HasUnitedStatesOnly() {
        BaseUI.verifyElementHasExpectedText("accnt_newAddr_country_Dropdown", "United States");
    }


    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step5_NavigateToMyRewardsAddressPage_ValidateStateDropdown_DoesNotHaveCanadianStateOption() throws Exception {
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.delete_Address();
        Account_UpdateAddressBook.click_AddNewAddress();
        BaseUI.verifyElementDoesNotHaveExpectedText("accnt_newAddr_state_Dropdown", canada_State_FullName);
    }

    @Test(priority = 55, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step5_MyRewards_NewAddressPage_ValidateCountryDropdown_HasUnitedStatesOnly() {
        BaseUI.verifyElementHasExpectedText("accnt_newAddr_country_Dropdown", "United States");
    }

    @Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public static void PT4639_Step5_MyRewards_EnterCanadianAddress_ValidateZipCode_ErrorMessage() throws Exception {
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, canada_StreetAddress1, canada_CityName,
                canada_Zip, canada_PhoneNum);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("accnt_AddAddress_ZipCodeErrorMsg"));
        BaseUI.verifyElementHasExpectedText("accnt_AddAddress_ZipCodeErrorMsg", "Zip Code is not a valid US zip");
    }

    @Test(priority = 65, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4639_Step5_MyRewards_NewAddressPage_ValidateStateErrorMessage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("accnt_AddAddress_StateErrorMsg"));
        BaseUI.verifyElementHasExpectedText("accnt_AddAddress_StateErrorMsg", "Your State/Province is required.");
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
