package dRewards.tests_regression;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.Account;
import dRewards.pages.Account_History;
import dRewards.pages.Account_OrderAndShippingDetails_Page;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import dRewards.pages_Administration.AddNewPromoCampaign_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PromoCode_DiscountProducts extends BaseTest {
	String campaignName = "Automation_Test";
	String transactionDescription = "Automation DiscountPromo";
	String promocode = "Automation";
	String[] discountPrice = { "2.35", "1" };
	String[] discount_SkuValue = { "pcdemofandango", "pcdemoringtones" };
	TableData promocodeTable;

	TableData shoppingCartData;
	PricingDetails pricingDetails;
	String points_Before_Checkout;
	String points_After_Checkout;
	String orderID;

	String ccType = "MasterCard";
	String ccNumber = "5105105105105100";
	String cvvCode = "211";
	String expire_month = "12";
	String expire_year = "22";
	String country = "United States";

	String firstName = "QA";
	String lastName = "Auto";
	String address1 = "1225 Broken Sound";
	String city = "Boca Raton";
	String state = "FL";
	String zip = "33487";
	String phone = "9544151992";

	String firstSelectedItemText;
	private String salesTaxCost;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		campaignName += "_" + randomNumber;
		promocode += "_" + randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();

		Navigation_Admin.navigate_CreatePromoCodes();
		AddNewPromoCampaign_Admin.add_Promo_DiscountProducts(campaignName, discount_SkuValue, discountPrice,
				transactionDescription, promocode);

		// Launch Catalog Upload and memcache flush as promocode setup doesnot
		// get updated in QA without the catalog upload and flush
		CatalogUpload_And_MemcacheFlush.launch_CatalogUpload();
		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all items from Shopping Cart.
		Homepage.deleteItemsFromShoppingCart();

		// Logic to remove all Shipping Info
		Navigation.navigate_Account_UpdateAddressBook();
		Account_UpdateAddressBook.delete_Address();

		// Logic to remove all Billing Info.
		Navigation.navigate_Account();
		Account.clear_PaymentHistory();
		Navigation.navigate_Home_viaLogo();

		Navigation.enter_PromoCode(promocode);
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" , 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_Verify_PromoCode_Displays() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Message"));
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_Verify_PromoCode_OfferList_Displays() throws Exception {

		ArrayList<String> offerList = Navigation.return_PromoCode_OfferList();
		BaseUI.verify_true_AndLog(offerList.size() > 0, "Found Promo Code Offer list.",
				"Could not find Promo Code Offer list.");
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" , 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_Verify_PromoCode_DiscountProducts_ViewDetails() throws Exception {
		firstSelectedItemText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_FirstItemDisplayed"));
		String offerName = "Two Fandango Tickets";
		Navigation.click_PromoCode_ViewDetails_ByOfferName(offerName);
		promocodeTable = Navigation.return_PromoCode_DiscountProducts_TableData();

		Navigation.verify_PromoCode_DiscountProducts_ViewDetails(promocodeTable.data.get(0));
	}

	@Test(priority = 21, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" , 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_Click_SelectAndVerify_PromoCode_DiscountProduct() throws Exception {

		Navigation.click_SelectAndVerify_PromoCode_DiscountProduct(firstSelectedItemText);
	}

	@Test(priority = 30, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" , 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_ShoppingCart_ClickProceed_Verify_ShippingPage_PriceInfo()
			throws Exception {

		Navigation.click_Promocode_Checkout_Button();
		pricingDetails = ShoppingCart.retrieve_PricingDetails();
		shoppingCartData = ShoppingCart.get_Table_Data_Old();
		points_Before_Checkout = Navigation.returnPoints();

		ShoppingCart.click_ProceedToCheckout();
		PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
		BaseUI.verifyElementHasExpectedText("priceSum_Savings", pricingDetails.savings);
	}

	@Test(priority = 40, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" , 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_ContinueToPage_AddAddress() throws Exception {
		if (CheckoutPage_ShippingInfo.newShippingAddress_Radio_Exists()) {
			CheckoutPage_ShippingInfo.click_AddNewShippingAddress_Radio();
		}
		CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, state, zip, phone);
		CheckoutPage_ShippingInfo.check_SaveForLater_Checkbox();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
		pricingDetails.add_Taxes_To_OrderTotal();
	}

	@Test(priority = 50, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
			"not_aarp","not_streetwise", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_AllStateCash_Continue_ToNextPage() throws Exception {
		// Allstate Cash was not used for this run through.
		pricingDetails.allstate_Cash = "$0.00";
		CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
		CheckoutPage_AllStateCash.click_Continue_ToNextPage();


		//Missing Validations
	}

	@Test(priority = 60, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_BillingPage_PricingInfo() throws Exception {

		PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
		BaseUI.verifyElementHasExpectedText("priceSum_Savings", pricingDetails.savings);

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
		}
	}

	@Test(priority = 70, groups = {"not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests" , 
			"not_redesign", "not_aarpredesign","not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OrderReviewPage_ProductDetails() throws Exception {

		CheckoutPage_PaymentInfo.add_NewPaymentOption(ccType, ccNumber, expire_month, expire_year, cvvCode,
				firstName + " " + lastName, address1, country, city, state, zip);
		CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
		CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

		CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 71, groups = {"not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT3235_Validate_OrderReviewPage_PaymentInformation() throws Exception {

		CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, address1, city, state, zip, ccType,
				ccNumber, expire_month + "/" + expire_year);

		// Verify Allstate Cash
		CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
	}

	@Test(priority = 72, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OrderReviewPage_PriceSummary() throws Exception {

		PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
		BaseUI.verifyElementHasExpectedText("priceSum_Savings", pricingDetails.savings);

	}

	@Test(priority = 73, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OrderReviewPage_ShippingInformation() throws Exception {

		CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, address1, city, state, zip, country,
				phone);

	}

	@Test(priority = 74, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OrderReviewPage_PricingDetails() throws Exception {

		CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
				pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
				pricingDetails.order_Total, pricingDetails.tax_Cost);
		BaseUI.verifyElementHasExpectedText("checkout_PricingDetails_Savings", pricingDetails.savings);
	}

	@Test(priority = 75, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OrderReviewPage_IAgreeInfo() throws Exception {

		CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
	}

	@Test(priority = 80, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {

		CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
		CheckoutPage_ReviewOrder.click_PlaceYourOrder();
		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, address1, city, state, "*****",
				country, phone);
	}

	@Test(priority = 81, groups = { "not_jeepwave", "not_tvc", "regression_Tests", "all_tests","not_citi", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OnOrderConfirmationPage_PointsDeducted()
			throws Exception {

		points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
				pricingDetails.points);
		Navigation.verify_pointsMatchExpected(points_After_Checkout);
	}

	@Test(priority = 82, groups = {"not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests", "not_allstateCash", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OrderConfirmationPage_BillingInformation()
			throws Exception {
		OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, address1, city, state, ccType,
				ccNumber, zip);
	}

	@Test(priority = 83, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OnOrderConfirmationPage_ProductInformation()
			throws Exception {

		OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 84, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_OnOrderConfirmationPage_PricingDetails()
			throws Exception {

		PricingDetails pricingDetails_OrderConfirmPage;
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		} else {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		}
		OrderConfirmationPage.verify_PricingDetails(pricingDetails);
	}

	@Test(priority = 90, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_AccountHistory_Page() throws Exception {

		Navigation.navigate_Account();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
		} else {
			Account_History.verify_Purchase(orderID, pricingDetails.points);
		}
	}

	@Test(priority = 100, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_CancelOrder_Page_ShippingInfo() throws Exception {

		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, address1, city, state,
				country, zip);
	}

	@Test(priority = 101, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_CancelOrder_Page_ProductInfo() throws Exception {

		Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
	}

	@Test(priority = 110, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_Cancelled() throws Exception {

		Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
	}

	@Test(priority = 120, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_Points_Reimbursed() throws Exception {

		Navigation.verify_pointsMatchExpected(points_Before_Checkout);
	}

	@Test(priority = 130, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" }, enabled=false)
	public void PT44_Step5_PromoCode_DiscountProducts_Validate_AccountHistory_OrderCancelled() throws Exception {

		Navigation.navigate_Account();
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase_Cancelled(pricingDetails.points);
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();

	}
}
