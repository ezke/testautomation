package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.ConsumerService;
import dRewards.pages_Administration.ConsumerService_EditConsumerPage;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class LocalDeals_LandingPageDisplays_UserHasInvalidProfileAddress extends BaseTest {

    private String city = "Ninilchik";
    private String country = "United States";
    private String address = "861 NW 51st ST";
    private String state = "AK";
    private String zipcode = "99639";
    private String username = "9362-qaauto22";
    private String city_ResetValue = "Boca Raton";
    private String state_ResetValue = "FL";
    private String zipcode_ResetValue = "33487";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        //Navigate to client url and check if re-captcha is displayed on
        //login page
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        ConsumerService_EditConsumerPage.updateUserAddress_ForAARPRedesign(username, address, city,
                state, zipcode, country);

        //Navigate to client url and login with valid credentials
        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_LocalOffers_SearchPage();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResults_Breadcrumb() {
        String [] breadcrumb = {"Home", "Local Deals"};
        Navigation.verify_BreadcrumbList(breadcrumb);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_Title() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ChangeLocation_EnterNewLocTitle"));
        BaseUI.verifyElementHasExpectedText("localOffer_ChangeLocation_EnterNewLocTitle",
                "Enter a new location.");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_SearchByStreetAddressText() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ChangeLocation_EnterNewLoc_SearchByAddressText"));
        BaseUI.verifyElementHasExpectedText("localOffer_ChangeLocation_EnterNewLoc_SearchByAddressText",
                "For the most relevant results, enter a street address.");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_SearchByZipCodeText() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ChangeLocation_EnterNewLoc_SearchByZipCodeText"));
        BaseUI.verifyElementHasExpectedText("localOffer_ChangeLocation_EnterNewLoc_SearchByZipCodeText",
                "In a hurry? Enter just a zip/postal code for quick results.");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_UsaRadioBtn() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_SearchPage_USA_RadioButtonLabel"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_CanadaRadioBtn() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("localOffer_SearchPage_Canada_RadioButtonLabel"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_Address_ZipCodeSection_Separated() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SearchPage_MiddleDivider"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_AddressSection_AddressTextbox() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_Address_Textbox"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_AddressSection_CityTextbox() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_City_Textbox"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_AddressSection_StateTextbox() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_State_Dropdown"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4535_Step1_VerifyLocalDealSearchResultsForm_EnterNewLocation_ZipCodeSection_LabelAndAddress() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_ZipCode_TextBox"));
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
            Browser.navigateTo(ClientDataRetrieval.url);
            ConsumerService_EditConsumerPage.updateUserAddress_ForAARPRedesign(username, address, city_ResetValue,
                    state_ResetValue, zipcode_ResetValue, country);
        } finally {
            Browser.closeBrowser();
        }
    }
}
