package dRewards.tests_regression;

import dRewards.pageControls.AuctionTile;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Auction_Details;
import dRewards.pages.Auctions;
import dRewards.pages.Auctions_MyAuctions;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewAuction_Admin;
import dRewards.pages_Administration.AddNewAuction_SubAuctionModal_Admin;
import dRewards.pages_Administration.ConsumerService;
import dRewards.pages_Administration.EditBuyerProgram_AdminSite;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

//@Test(groups = { "not_IE" })
public class Auctions_MicroAuctions_DualCurrency extends BaseTest {

	private String auction_0Points = "AutoTest";
	private String auction_1499Points = "Auto1499";
	private String auction_30000Points = "Auto30000";
	private String auction_31000Points = "Auto31000";
	private Double startingBid = 50.00;
	private Double usdAmount = 3.99;
	private Double currentBid = 50.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;

	private String productSku_For0Points = "auczune64blkDEMO";
	private String productSku_For1499Points = "aucxboxkinectDEMO";
	private String productSku_For30000Points = "auccanonsx30isDEMO";
	private String productSku_For31000Points = "aucGcvs25demo";

	private String productName_0Points = "Microsoft Zune HD 64 GB Digital Player/Radio - Black";
	private String productName_1499Points = "Microsoft Xbox 360 4GB Console with Kinect";
	private String productName_30000Points = "Canon Powershot SX30IS 14.1MP Digital Camera 35x Zoom";
	private String productName_31000Points = "CVS Gift Card $25 - DEMO ONLY";

	private Boolean isDemo = true;
	private Integer auctionTimeToLast = 120;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		auction_0Points += "_" + randomNumber;
		auction_1499Points += "_" + randomNumber;
		auction_30000Points += "_" + randomNumber;
		auction_31000Points += "_" + randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)
				 || ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
				 || ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			auctionTimeToLast += 1;
		}

		LoginPage_Admin.login_Admin();
		EditBuyerProgram_AdminSite.UpdateWinLimit_Attribute_13011();
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		// Auction with no min/max values
		AddNewAuction_Admin.Add_New_Auction_DualCurrency(auction_0Points, RedemptionTypes.DualCurrency, startingBid,
				usdAmount, extendTime, productSku_For0Points, AuctionTypes.Standard, isDemo);
		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		// Auction with min=1 and max=1499
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction_DualCurrency(auction_1499Points, RedemptionTypes.DualCurrency, startingBid,
				usdAmount, extendTime, productSku_For1499Points, AuctionTypes.Standard, isDemo);
		AddNewAuction_SubAuctionModal_Admin.add_Subauction_WithMinAndMax_Points(auctionTimeToLast, "1", "1499");
		AddNewAuction_Admin.initialize_Auction();

		// Auction with min=1500 and max=30000
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction_DualCurrency(auction_30000Points, RedemptionTypes.DualCurrency, startingBid,
				usdAmount, extendTime, productSku_For30000Points, AuctionTypes.Standard, isDemo);
		AddNewAuction_SubAuctionModal_Admin.add_Subauction_WithMinAndMax_Points(auctionTimeToLast, "1500", "30000");
		AddNewAuction_Admin.initialize_Auction();

		// Auction with min 31000 and max=100000
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction_DualCurrency(auction_31000Points, RedemptionTypes.DualCurrency, startingBid,
				usdAmount, extendTime, productSku_For31000Points, AuctionTypes.Standard, isDemo);
		AddNewAuction_SubAuctionModal_Admin.add_Subauction_WithMinAndMax_Points(auctionTimeToLast, "31000", "100000");
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
	}

	@Test(priority = 10, groups = {"not_jeepwave","not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE" })
	public void PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_NoPoints() throws Exception {

		LoginPage.login(ClientDataRetrieval.secondaryUser_NoPoints, ClientDataRetrieval.secondaryPassword_NoPoints);
		Navigation.navigate_Auctions_AllAuctions();
		Auctions.verify_AuctionDetails_ByUserPoints("0", productName_0Points, currentBid, true);
	}

	@Test(priority = 20, groups = {"not_jeepwave","not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_1Points() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)
				|| (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))) {
			Navigation.click_Logout();
		}
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.secondaryUser_OnePoints, ClientDataRetrieval.secondaryPassword_OnePoints);
		Navigation.navigate_Auctions_AllAuctions();
		Auctions.verify_AuctionDetails_ByUserPoints("1", productName_0Points, currentBid, true);
		Auctions.verify_AuctionDetails_ByUserPoints("1", productName_1499Points, currentBid, true);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_1499Points() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)
				|| (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))) {
			Navigation.click_Logout();
		}
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.secondaryUser_1499Points, ClientDataRetrieval.secondaryPassword_1499Points);
		Navigation.navigate_Auctions_AllAuctions();
		Auctions.verify_AuctionDetails_ByUserPoints("1499", productName_0Points, currentBid, true);
		Auctions.verify_AuctionDetails_ByUserPoints("1499", productName_1499Points, currentBid, true);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_1500Points() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)
				|| (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))) {
			Navigation.click_Logout();
		}
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.secondaryUser_1500Points, ClientDataRetrieval.secondaryPassword_1500Points);
		Navigation.navigate_Auctions_AllAuctions();
		Auctions.verify_AuctionDetails_ByUserPoints("1500", productName_0Points, currentBid, true);
		Auctions.verify_AuctionDetails_ByUserPoints("1500", productName_30000Points, currentBid, true);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_30001Points() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)
				|| (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))) {
			Navigation.click_Logout();
		}
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.secondaryUser_30001Points,
				ClientDataRetrieval.secondaryPassword_30001Points);
		Navigation.navigate_Auctions_AllAuctions();
		Auctions.verify_AuctionDetails_ByUserPoints("30001", productName_0Points, currentBid, true);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_31000Points() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)
				|| (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards))) {
			Navigation.click_Logout();
		}
		Browser.navigateTo(ClientDataRetrieval.url);

		LoginPage.login(ClientDataRetrieval.secondaryUser_31000Points,
				ClientDataRetrieval.secondaryPassword_31000Points);
		Navigation.navigate_Auctions_AllAuctions();
		Auctions.verify_AuctionDetails_ByUserPoints("31000", productName_0Points, currentBid, true);
		Auctions.verify_AuctionDetails_ByUserPoints("31000", productName_31000Points, currentBid, true);
	}

	@Test(priority = 30, groups = {"not_jeepwave", "not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step9_Verify_Auctions_MicroAuctions_Users_31000Points_Click_FollowAuctions() throws Exception {

		AuctionTile auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(productName_31000Points, startingBid);
		auctionTile.click_DetailsButton();
		Auction_Details.click_AddToMyAuctions();
		Auction_Details.verify_YourAreFollowingThisAuction_LinkAppears();
	}

	// Navigate to admin site and update points to verify user able to follow
	// out of range auction
	@Test(priority = 40, groups = {"not_jeepwave", "not_smart","not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step9_Verify_Auctions_MicroAuctions_UpdatePoints_UserWith_31000Points_AdminSite()
			throws Exception {

		Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ExpandCategory("Consumer Service");

		ConsumerService.clickUserID_NavigateTo_EditConsumerPage(ClientDataRetrieval.secondaryUser_31000Points);
		ConsumerService.updatePointsOrRewards("1");
		ConsumerService.verify_PointsOrRewards_Updated("1");
	}

	@Test(priority = 50, groups = {"not_jeepwave","not_smart", "not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step9_Verify_Auctions_MicroAuctions_User_With_AbleTo_FollowOutOfRange_Auction()
			throws Exception {

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.secondaryUser_31000Points,
				ClientDataRetrieval.secondaryPassword_31000Points);

		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Home_viaHomeLink();
			Navigation.navigate_Menu("Auctions");
			String actualAuctionTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("auction_Title"));
			BaseUI.baseStringCompare("Validate Auction Added to Watch List is displayed", 
					productName_31000Points, actualAuctionTitle);
		} else {
			Navigation.navigate_Auctions_MyAuctions();
			Auctions_MyAuctions.verify_CurrentAuctionFollowing_Appears();
		}	
		AuctionTile auctionTile = Auctions_MyAuctions.return_AuctionTile_ByAuctionNameAndCurrentBid(productName_31000Points,
				startingBid);
		auctionTile.verify_MyAuctions_CurrentAuctionDetails_Info("1", productName_31000Points, currentBid);

	}

	@Test(priority = 60, groups = { "not_jeepwave","not_smart","not_citi", "not_tvc", "critical_Tests", "regression_Tests", "not_aarpredesign", "all_tests", 
			"not_allstate" ,"not_IE"})
	public void PT4052_Step9_Verify_Auctions_MicroAuctions_UpdatePoints_OfUserWith_31000Points_ToOriginalPoints_AdminSite()
			throws Exception {

		Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ExpandCategory("Consumer Service");
		ConsumerService.clickUserID_NavigateTo_EditConsumerPage(ClientDataRetrieval.secondaryUser_31000Points);
		ConsumerService.updatePointsOrRewards("31000");
		ConsumerService.verify_PointsOrRewards_Updated("31000");
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {

		} finally {

			Browser.closeBrowser();
		}
	}
}
