package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.*;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.ProductCatalog_CatalogPage;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.HashMap;

public class DailyDealPreview_HomePage extends BaseTest {

    private HashMap<String, String> homePageDailyDealData, dailyDeal_Data;
    private String inventoryCount;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        // Logic to remove all items from Shopping Cart.
        Homepage.deleteItemsFromShoppingCart();

        if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_SneakPreviewWidget"))) {
            BaseUI.log_Status("Daily Deal is Sold Out. Need to update in Admin Site");
            BaseUI.verifyElementHasExpectedText("homepage_DailyDeals_SneakPreviewText", "Sneak Preview");
        } else if (BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_DailyDealTitle"))
                && BaseUI.elementAppears(Locator.lookupOptionalElement("homepage_DailyDeals_Images"))) {
            BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("homepage_DailyDeals_SneakPreviewWidget"));
            BaseUI.log_Status("Daily Deal is Live cannot validate the sneak preview");

            homePageDailyDealData = Homepage.clickDailyDeal();
            dailyDeal_Data = DailyDeals.return_DailyDealInfo();

            //If Daily Deal is live, navigate to admin site and set the daily deal,
            //inventory value to 0. So we can test the daily deal in preview state.
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();

            //Save the Daily Deal ProductSku and Inventory value, so it is be reset back to original after validation.
            String productSku = ProductCatalog_CatalogPage.returnProductSku_ForSelectedDailyDeal(dailyDeal_Data.get("dailyDealName"));
            inventoryCount = ProductCatalog_CatalogPage.returnInventoryCountValue_ForSelectedProductSku(productSku);
            ProductCatalog_CatalogPage
                    .update_DailyDeal_ProductSku_Quantity(dailyDeal_Data.get("dailyDealName"), "0");

            Browser.navigateTo(ClientDataRetrieval.url);
            LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
            Navigation.navigate_Home_viaHomeLink();
        } else {
            BaseUI.log_AndFail("Daily Deal is not setup in Admin Site");
        }
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsHeaderTitle_On_DailyDealWidget() throws Exception {
        WebElement dailyDealsHeaderTitleElement = Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreviewText");
        String actualDailyDealsHeaderTitle = BaseUI.getTextFromField(dailyDealsHeaderTitleElement);
        BaseUI.verifyElementAppears(dailyDealsHeaderTitleElement);
        BaseUI.baseStringPartialCompare("homepage_DailyDeals_SneakPreviewText", "Sneak Preview", actualDailyDealsHeaderTitle);
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsThisDealStartsText_On_DailyDealWidget() throws Exception {
        WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_ThisDealStartText");
        String actualDailyDealsHeaderTitle_StartTime = BaseUI.getTextFromField(dailyDealsStartTimeElement);
        BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
        BaseUI.baseStringCompare("homepage_DailyDeals_SneakPreview_ThisDealStartText", "This deal starts in", actualDailyDealsHeaderTitle_StartTime);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsStartTime_On_DailyDealWidget() throws Exception {
        WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DealStartTime");
        BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsText_On_DailyDealWidget() throws Exception {
        WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DailyDealText");
        String actualDailyDealsHeaderTitle_StartTime = BaseUI.getTextFromField(dailyDealsStartTimeElement);
        BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
        BaseUI.baseStringCompare("homepage_DailyDeals_SneakPreview_DailyDealText", "Daily Deal", actualDailyDealsHeaderTitle_StartTime);
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsSavings_On_DailyDealWidget() throws Exception {
        WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DailyDeal_Savings");
        String actualDailyDealsHeaderTitle_StartTime = BaseUI.getTextFromField(dailyDealsStartTimeElement);
        BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
        BaseUI.baseStringPartialCompare("homepage_DailyDeals_SneakPreview_DailyDeal_Savings", "savings", actualDailyDealsHeaderTitle_StartTime);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsProductName_On_DailyDealWidget() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DailyDealProductName"));
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4500_Step1_Validate_DailyDealsImage_On_DailyDealWidget() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_DailyDeals_SneakPreview_DailyDeal_Img"));
    }


    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();
            ProductCatalog_CatalogPage.
                    update_DailyDeal_ProductSku_Quantity(dailyDeal_Data.get("dailyDealName"), inventoryCount);
        } finally {
            Browser.closeBrowser();
        }
    }

}
