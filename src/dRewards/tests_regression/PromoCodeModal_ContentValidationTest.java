package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PromoCodeModal_ContentValidationTest extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step2_HomePage_VerifyPromoCodeLinkDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCodeLink"));
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step2_HomePage_ClickPromoCodeLink_VerifyPromoCodeModalDisplayed() throws Exception {
        Homepage.clickPromoCodeLink();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("promoCodeModal_Title"));
        BaseUI.verifyElementHasExpectedText("promoCodeModal_Title", "Have a Promo Code?");
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step2_HomePage_VerifyPromoCodeModal_WhereDoIFindTheseLink() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("promoCodeModal_WhereDoIFindTheseLink"));
        BaseUI.verifyElementHasExpectedText("promoCodeModal_WhereDoIFindTheseLink", "Where do I find these?");
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step3_HomePage_ClickPromoCodeModal_WhereDoIFindTheseLink_VerifyDescription() throws Exception {
        Homepage.clickPromoCodeModal_WhereIFindTheseLink();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("promoCodeModal_WhereDoIFindThese_Info1"));
        BaseUI.verifyElementHasExpectedText("promoCodeModal_WhereDoIFindThese_Info1",
                "Special Offer Codes can be found in your monthly estatements and promotional offers, so make sure you check your email!");
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step3_HomePage_VerifyPromoCodeModal_WhereDoIFindTheseLink_EmailPreferencesDescription() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("promoCodeModal_WhereDoIFindThese_Info2"));
        BaseUI.verifyElementHasExpectedText("promoCodeModal_WhereDoIFindThese_Info2",
                "To see if you’re signed up to receive emails, go to your email preferences page ►");
    }

    @Test(priority = 35, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step4_HomePage_ClickPromoCodeModal_WhereDoIFindThese_EmailPreferencesLink_VerifyNavigatesLandingPage() throws Exception {
        Homepage.clickPromoCodeModal_WhereIFindThese_EmailPreferencesPageLink();
        BaseUI.verifyElementHasExpectedText("emailPreferencesPage_Header", "Email Preferences");
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step4_HomePage_VerifyPromoCodeModal_WhereDoIFindTheseLink_EmailPreferencesPage_SaveChangesBtn() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("emailPreferencesPage_SaveChangesBtn"));
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" } )
    public void PT4804_Step4_HomePage_VerifyPromoCodeModal_WhereDoIFindTheseLink_EmailPreferences_EmailAddressTextBox() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("emailPreferencesPage_EmailAddressTextBox"));
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4804_Step4_HomePage_VerifyPromoCodeModal_WhereDoIFindTheseLink_EmailPreferences_ConfirmEmailAddressTextBox() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("emailPreferencesPage_ConfirmEmailAddressTextBox"));
    }

    @Test(priority = 45, groups = { "regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4804_Step4_HomePage_VerifyPromoCodeModal_WhereDoIFindTheseLink_EmailPreferences_LeftNavigationList() throws Exception {
        Homepage.verify_PromoCodeModal_EmailPreferencesLandingPage_LeftNavigationList();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            Navigation.navigate_Home_viaLogo();
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
