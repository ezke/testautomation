package dRewards.tests_regression;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.HotelsAndCondos_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class HotelSearch_SearchResults extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        // LaunchPage.click_sign_On();
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Hotel_Landingpage();
    }

    @Test(groups = {"regression_Tests", "all_tests"})
    public void PT3135_Step7_Navigate_Travel_HomePage_SearchForHotel() throws Exception {
        String location = "Savannah, GA";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(120);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(125);
        String numberOfAdults = "2";
        String numberOfChildren = "2";
        String ageOfChildren = "7";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        //HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(location);
        String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", location};
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("hotelSearchResult_SubHeader"));
            BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
        }
        Navigation.verify_BreadcrumbList(breadcrumbArray);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step8_SearchForHotel_NewSearch_SearchForSecondHotel() throws Exception {
        // First Search variables
        String location = "Savannah, GA";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(50);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(55);
        String numberOfAdults = "2";
        String numberOfChildren = "2";
        String ageOfChildren = "7";

        // Second Search variables
        String location2 = "Orlando, FL";
        String fromDate2 = BaseUI.getDateAsString_InRelationToTodaysDate(10);
        String toDate2 = BaseUI.getDateAsString_InRelationToTodaysDate(12);
        String numberOfAdults2 = "3";
        String numberOfChildren2 = "1";
        String ageOfChildren2 = "8";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);
        HotelsAndCondos_SearchResults.click_NewSearchLink();

        HotelsAndCondos.hotel_Search_WithCriteria(location2, fromDate2, toDate2, numberOfAdults2, numberOfChildren2,
                ageOfChildren2);
        HotelsAndCondos_SearchResults.validate_SearchResults(location2, fromDate2, toDate2, numberOfAdults2,
                numberOfChildren2, ageOfChildren2);
        //HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(location2);
        String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", location2};
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("hotelSearchResult_SubHeader"));
            BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
        }
        Navigation.verify_BreadcrumbList(breadcrumbArray);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step9_SearchForHotel_UseHotelName() throws Exception {
        String location = "Palmer House";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(30);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(35);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        String hotelSelected = HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        HotelsAndCondos_SearchResults.validate_SearchResults(hotelSelected, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        //HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(hotelSelected);
        String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", hotelSelected};
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("hotelSearchResult_SubHeader"));
            BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
        }
        Navigation.verify_BreadcrumbList(breadcrumbArray);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step10_SearchForHotel_UseAddress() throws Exception {
        String location = "1225 Broken sound pkway, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(50);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(55);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        HotelsAndCondos_SearchResults.verify_ListOfHotels_AllWithinDistance();
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        //HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(location);
        String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", location};
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("hotelSearchResult_SubHeader"));
            BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
        }
        Navigation.verify_BreadcrumbList(breadcrumbArray);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step11_SearchForHotel_Airport_AddressList_Sorted_Ascending_ByDistance() throws Exception {
        String location = "MCO";

        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        HotelsAndCondos_SearchResults.verify_Distance_FromHotels_Ascending();
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step12_SearchForHotel_Landmark() throws Exception {
        String location = "Golden Gate";
        String selectLocation = "Golden Gate National Recreation Area";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_SelectPredictiveListItem(location, selectLocation, fromDate, toDate,
                numberOfAdults, numberOfChildren, ageOfChildren);
        //HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(selectLocation);
        String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", selectLocation};
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("hotelSearchResult_SubHeader"));
            BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
        }
        Navigation.verify_BreadcrumbList(breadcrumbArray);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step13_SearchForHotel_ZipCode() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
        //HotelsAndCondos_SearchResults.verify_Breadcrumb_ContainsText(location);
        String[] breadcrumbArray = {"Home", "Travel", "Hotels and Condos", location};
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("hotelSearchResult_SubHeader"));
            BaseUI.verifyElementHasExpectedText("hotelSearchResult_SubHeader", "Hotels and Condos");
        }
        Navigation.verify_BreadcrumbList(breadcrumbArray);
        HotelsAndCondos_SearchResults.verify_Distance_FromHotels_Ascending();
    }

    // have nothing to compare Top Picks to. Can only check that there are
    // results.
    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step14_SearchForHotel_SortBy_TopPicks() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        HotelsAndCondos_SearchResults.sortBy("Top Picks");
        TableData newList = HotelsAndCondos_SearchResults.return_hotelList_TableData();

        BaseUI.verify_true_AndLog(newList.data.size() > 0, "Found Hotels.", "Did NOT find Hotels.");
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step15_SearchForHotel_SortBy_NameAtoZ() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        HotelsAndCondos_SearchResults.sortBy("Name A to Z");

        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        sortList.verify_Column_Sorted_Ascending("hotelName");
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step16_SearchForHotel_SortBy_NameZtoA() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        HotelsAndCondos_SearchResults.sortBy("Name Z to A");
        //TableData previousList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        //sortList.sort_ByColumn_Descending("hotelName");
        sortList.verify_Column_Sorted_Descending("hotelName");
        //HotelsAndCondos_SearchResults.verify_ResultsListsMatch(sortList, previousList);

    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step17_SearchForHotel_SortBy_YouPay_LowToHigh() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            HotelsAndCondos_SearchResults.sortBy("Your Price - Low to High");
        } else {
            HotelsAndCondos_SearchResults.sortBy("You Pay - Low to High");
        }
        //TableData previousList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        //sortList.sort_ByColumn_numeric_Ascending("youPay");
        sortList.verify_Column_Sorted_Numeric_Ascending("youPay");
        //HotelsAndCondos_SearchResults.verify_ResultsListsMatch(sortList, previousList);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step18_SearchForHotel_SortBy_YouPay_HighToLow() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            HotelsAndCondos_SearchResults.sortBy("Your Price - High to Low");
        } else {
            HotelsAndCondos_SearchResults.sortBy("You Pay - High to Low");
        }

        //TableData previousList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        //sortList.sort_ByColumn_numeric_Descending("youPay");
        sortList.verify_Column_Sorted_Numeric_Descending("youPay");
        //HotelsAndCondos_SearchResults.verify_ResultsListsMatch(sortList, previousList);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests", "not_citi"})
    public void PT3135_Step19_SearchForHotel_SortBy_Points_LowToHigh() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        HotelsAndCondos_SearchResults.sortBy(ClientDataRetrieval.client_Currency + " - Low to High");

        //TableData previousList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        //sortList.sort_ByColumn_numeric_Ascending("points");
        sortList.verify_Column_Sorted_Numeric_Ascending("points");
        //HotelsAndCondos_SearchResults.verify_ResultsListsMatch(sortList, previousList);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests", "not_citi"})
    public void PT3135_Step20_SearchForHotel_SortBy_Points_HighToLow() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        HotelsAndCondos_SearchResults.sortBy(ClientDataRetrieval.client_Currency + " - High to Low");

        //TableData previousList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        //sortList.sort_ByColumn_numeric_Descending("points");
        sortList.verify_Column_Sorted_Numeric_Descending("points");
        //HotelsAndCondos_SearchResults.verify_ResultsListsMatch(sortList, previousList);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step21_SearchForHotel_SortBy_HotelsAndCondos() throws Exception {
        String location = "33301";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria_NoPredictiveResult(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);

        Integer numberOfResults = HotelsAndCondos_SearchResults.return_ResultsCount();

        HotelsAndCondos_SearchResults.selectTypeOfResult("All Inclusive");
        HotelsAndCondos_SearchResults.selectTypeOfResult("Hotels and Condos");
        Integer new_numberOfResults = HotelsAndCondos_SearchResults.return_ResultsCount();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();

        // HotelsAndCondos_SearchResults.verify_ResultsListsMatch(previousList,
        // sortList);
        BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Hotels and Condos.",
                "Did NOT find Hotels and Condos.");
        BaseUI.verify_true_AndLog(new_numberOfResults.equals(numberOfResults), "Number of Results matches.",
                "New Number of Results " + new_numberOfResults.toString() + " does not match old number of results "
                        + numberOfResults.toString() + ".");
    }

    // Must have Condos returned.
    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step21_SearchForHotel_SortType_Condos_ThenHotelsCondos() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        Integer numberOfResults = HotelsAndCondos_SearchResults.return_ResultsCount();

        HotelsAndCondos_SearchResults.selectTypeOfResult("Condos Only");
        TableData condosList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        BaseUI.verify_true_AndLog(condosList.data.size() > 0, "Found Condos.", "Did NOT find Condos.");
        HotelsAndCondos_SearchResults.selectTypeOfResult("Hotels and Condos");
        Integer new_numberOfResults = HotelsAndCondos_SearchResults.return_ResultsCount();
        TableData sortList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Hotels and Condos.",
                "Did NOT find Hotels and Condos.");
        // HotelsAndCondos_SearchResults.verify_ResultsListsMatch(previousList,
        // sortList);

        BaseUI.verify_true_AndLog(new_numberOfResults.equals(numberOfResults), "Number of Results matches.",
                "New Number of Results " + new_numberOfResults.toString() + " does not match old number of results "
                        + numberOfResults.toString() + ".");
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step23_SearchForHotel_SortType_Condos() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.selectTypeOfResult("Condos Only");
        HotelsAndCondos_SearchResults.verify_NonAdvertised_Entries_ContainCondoImage();
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step24_SearchForHotel_SortType_Condos_VerifyCount() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.selectTypeOfResult("Condos Only");
        HotelsAndCondos_SearchResults.verify_Count_OfEntries();
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step25_SearchForHotel_Revise_CheckinDate() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String fromDate_revised = BaseUI.getDateAsString_InRelationToTodaysDate(147);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.revise_CheckinDate(fromDate_revised);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate_revised, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step26_SearchForHotel_Revise_CheckoutDate() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String toDate_revised = BaseUI.getDateAsString_InRelationToTodaysDate(160);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.revise_CheckoutDate(toDate_revised);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate_revised, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step27_SearchForHotel_Revise_AdultsCount() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfAdults_revised = "3";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.revise_adultCount(numberOfAdults_revised);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults_revised,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step28_SearchForHotel_Revise_ChildCount() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        String numberOfChildren_revised = "3";
        String ageOfChildren_revised = "5";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.revise_childCount(numberOfChildren_revised, ageOfChildren_revised);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren_revised, ageOfChildren_revised);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step29_SearchForHotel_Navigate_To_Page2() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(100);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(105);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 2;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.navigate_ToPage_ByTOP_PageNumber(page);
        HotelsAndCondos_SearchResults.verify_Page_Active(page);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step30_SearchForHotel_Navigate_To_Page2_UsingNextTOPButton() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(100);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(105);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 2;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);
        TableData page1Results = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        BaseUI.verify_true_AndLog(page1Results.data.size() > 0, "Found results on page 1.",
                "Found NO results on page 1.");

        HotelsAndCondos_SearchResults.navigate_NextPage_ByTOP_Link();
        HotelsAndCondos_SearchResults.verify_Page_Active(page);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step30_SearchForHotel_Navigate_To_Page1_UsingPreviousTOPButton() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 1;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.navigate_NextPage_ByTOP_Link();
        HotelsAndCondos_SearchResults.navigate_PreviousPage_ByTOP_Link();
        HotelsAndCondos_SearchResults.verify_Page_Active(page);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step31_SearchForHotel_Navigate_To_Page2_ByBottomPageLink() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(100);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(105);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 2;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.navigate_ToPage_ByBOTTOM_PageNumber(page);
        HotelsAndCondos_SearchResults.verify_Page_Active(page);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step32_SearchForHotel_Navigate_To_Page2_UsingNextBOTTOMButton() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(100);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(105);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 2;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        TableData page1Results = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        BaseUI.verify_true_AndLog(page1Results.data.size() > 0, "Found results on page 1.",
                "Found NO results on page 1.");

        ArrayList<String> pageCount = HotelsAndCondos.getPageCountForHotelSearch();
        if (pageCount.size() > 1) {
            HotelsAndCondos_SearchResults.navigate_NextPage_ByBOTTOM_Link();
            HotelsAndCondos_SearchResults.verify_Page_Active(page);
            HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                    numberOfChildren, ageOfChildren);
        } else {
            BaseUI.log_Status("This search result does not have 3 pages");
            BaseUI.verify_true_AndLog(pageCount.size() < 2, "Search Result does not have 2 pages",
                    "Search result have 2 pages");
        }
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step32_SearchForHotel_Navigate_To_Page1_UsingPreviousBOTTOMButton() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 1;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.navigate_NextPage_ByBOTTOM_Link();
        HotelsAndCondos_SearchResults.navigate_PreviousPage_ByBOTTOM_Link();
        HotelsAndCondos_SearchResults.verify_Page_Active(page);
        HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
                numberOfChildren, ageOfChildren);
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step33_SearchForHotel_Navigate_To_Page3_ByTopLink_NoDuplicateEntries() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(50);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(55);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        Integer page = 3;

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);
        TableData page1Results = HotelsAndCondos_SearchResults.return_hotelList_TableData();
        BaseUI.verify_true_AndLog(page1Results.data.size() > 0, "Found results on page 1.",
                "Found NO results on page 1.");

        ArrayList<String> pageCount = HotelsAndCondos.getPageCountForHotelSearch();
        if (pageCount.size() > 2) {
            HotelsAndCondos_SearchResults.navigate_ToPage_ByTOP_PageNumber(page);
            TableData page3Results = HotelsAndCondos_SearchResults.return_hotelList_TableData();
            BaseUI.verify_true_AndLog(page1Results.data.size() > 0, "Found results on page 3.",
                    "Found NO results on page 3.");

            for (HashMap<String, String> page1row : page1Results.data) {
                for (HashMap<String, String> page3Row : page3Results.data) {
                    BaseUI.verify_false_AndLog(page1row.equals(page3Row), "Rows did NOT match.",
                            "Found duplicate entry from page 1.");
                }
            }
        } else {
            BaseUI.log_Status("This search result does not have 3 pages");
            BaseUI.verify_true_AndLog(pageCount.size() < 3, "Search Result does not have 3 pages",
                    "Search result have 3 pages");
        }
    }

    @Test(groups = {"regression_Tests", "critical_Tests", "all_tests"})
    public void PT3135_Step34_SearchForHotel_Click_BackToTopButton() throws Exception {
        String location = "Orlando, FL";
        String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
        String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
        String numberOfAdults = "2";
        String numberOfChildren = "0";
        String ageOfChildren = "0";

        HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
                ageOfChildren);

        HotelsAndCondos_SearchResults.verify_BackToTopButton();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
            Navigation.navigate_Home_viaHomeLink();
        }
        Navigation.navigate_Hotel_Landingpage();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Home_viaHomeLink();
            Navigation.navigate_Logout();
        }
        Browser.closeBrowser();
    }
}
