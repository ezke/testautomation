package dRewards.tests_regression;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.*;
import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class MerchandiseCheckout_SavedShippingAddress_NewPayment extends BaseTest {

    // Variables for Shipping Info and same values used for Billing.
    private static final String firstName = "QA";
    private static final String lastName = "Auto";
    private static final String address1 = "1225 broken sound";
    private static final String city = "Boca Raton";
    private static String state_Abbreviated = "FL";
    private static String state_FullName = "Florida";
    private static String zip = "33487";
    private static final String phone = "9544151992";

    private static String ccType = "MasterCard";
    private static final String ccNumber = "5105105105105100";
    private static final String cvvCode = "211";
    private static final String expire_month = "12";
    private static final String expire_year = "22";
    private static final String country = "United States";


    // Variables for product info/pay info
    private TableData shoppingCartData;
    private PricingDetails pricingDetails;
    private String points_Before_Checkout;
    private PricingDetails pricingDetails_CheckoutPage, pricingDetails_OrderConfirmPage;
    private ValueSetByOneTestAndUsedByOthers<String> orderIDValue = new ValueSetByOneTestAndUsedByOthers<>("order ID");
    private String salesTaxCost;

    @NotNull
    private String getOrderID() {
        return orderIDValue.get();
    }

    private void setOrderID(@NotNull String value) {
        orderIDValue.set(value);
    }


    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        // Logic to remove all items from Shopping Cart.
        Homepage.deleteItemsFromShoppingCart();

        // Logic to remove all Shipping Info
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.delete_Address();

        //Add new address
        Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, address1,
                country, city, state_FullName, zip, phone);

        // Logic to remove all Billing Info.
        Navigation.navigate_Account();
        Account.clear_PaymentHistory();
        Navigation.navigate_Home_viaLogo();

        // Add a products to cart.
        Navigation.navigate_Merchandise_Beauty();
        Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        Navigation.navigate_Merchandise_HomeAndKitchen();
        Merchandise.selectPageToNavigate_AndChooseRandomProduct();


        //Adding this condition to check if item added to shopping cart is
        //not sold out. If yes we remove all items from cart and add new product
        if (BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
            ShoppingCart.remove_AllItems();

            Navigation.navigate_Merchandise_Beauty();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            Navigation.navigate_Merchandise_HomeAndKitchen();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        }

        // Add logic here to pull product info from Shopping Cart page
        pricingDetails = ShoppingCart.retrieve_PricingDetails();
        pricingDetails.set_SavingsAmount_Citi();
        shoppingCartData = ShoppingCart.get_Table_Data();
        points_Before_Checkout = Navigation.returnPoints();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed() throws Exception {
        ShoppingCart.click_ProceedToCheckout();
        CheckoutPage_ShippingInfo.verifyElementHasExpectedTextChange();
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4505_Step5_CheckoutPage_ValidateShippingAddressDisplayed() throws Exception {
        String actualAddress = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress1",
                        firstName, null));
        BaseUI.baseStringCompare("checkout_ExistingAddress_ShippingAddress1", address1, actualAddress);
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step7_Validate_PaymentPage_ClickChange_BillingAddress() throws Exception {
        // Update Payment Info. Save for later use.
        CheckoutPage_ShippingInfo.click_Checkout_ExistingShippingAddress_Checkbox();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkout_newPayment_CreditCardType"));
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step9_To_10_Validate_OrderReviewPage_ProductDetails() throws Exception {
        CheckoutPage_PaymentInfo.add_NewPaymentOption("Mastercard", ccNumber, expire_month, expire_year, cvvCode,
                firstName + " " + lastName, address1, country, city, state_FullName, zip);

        CheckoutPage_PaymentInfo.apply_GiftCard_NameExpectedFormatting(shoppingCartData);

        CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
        CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

        CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step11_Validate_OrderReviewPage_PriceSummary() throws Exception {
        PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step11_Validate_OrderReviewPage_PaymentInformation() throws Exception {

        CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, address1, city, state_Abbreviated, zip, ccType,
                ccNumber, expire_month + "/" + expire_year);

        // Add Logic here for Allstate Cash
        CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step11_Validate_OrderReviewPage_ShippingInformation() throws Exception {
        CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();

        CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, address1, city, state_Abbreviated, zip, country,
                phone);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step11_Validate_OrderReviewPage_PricingDetails() throws Exception {

        CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step11_Validate_OrderReviewPage_IAgreeInfo() throws Exception {
        pricingDetails_CheckoutPage = CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();
        salesTaxCost = pricingDetails_CheckoutPage.tax_Cost;

        CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
    }

    @Test(priority = 45, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step12_Validate_AbleToGetTo_OnOrderConfirmationPage() throws Exception {
        CheckoutPage_ReviewOrder.click_PlaceYourOrder();
        CheckoutPage_ReviewOrder.verify_CreditCard_NotDeclined();

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("orderConfNumber"));
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step13_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {

        setOrderID(BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber")));

        OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, address1, city, state_Abbreviated, "*****",
                country, phone);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step13_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
        String points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
                pricingDetails.points);
        Navigation.verify_pointsMatchExpected(points_After_Checkout);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step13_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
        OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, address1, city, state_Abbreviated, ccType,
                ccNumber, zip);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step13_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
        OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step13_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
            OrderConfirmationPage.verify_PricingDetails(pricingDetails_CheckoutPage);
        } else {
            pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
            OrderConfirmationPage.verify_PricingDetails(pricingDetails);
        }
    }

    @Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step17_To_18_Validate_AccountHistory_Page() throws Exception {
        Navigation.navigate_Account();

        String orderID = getOrderID();
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AllstateCash)) {
            Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
        } else if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            Account_History.verify_Purchase(orderID, pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase(orderID, pricingDetails.points);
        }
    }

    @Test(priority = 70, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step19_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
        String orderId = getOrderID();
        Account.click_ViewDetails_ByOrderNumber(orderId);
        Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, address1, city, state_Abbreviated,
                country, zip);
    }

    @Test(priority = 71, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step19_Validate_CancelOrder_Page_ProductInfo() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAuct_OrderAndShip_YouPay"));
        Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
    }

    @Test(priority = 72, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step19_Validate_CancelOrder_Page_PaymentInfo() throws Exception {

        String orderDate = Account_OrderAndShippingDetails_Page.getFormatted_OrderDate();
        Double orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));

        Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, getOrderID(), orderDate,
                orderSubtotal, Double.parseDouble(pricingDetails_OrderConfirmPage.tax_Cost.replace("$", "").replace(",", "")),
                pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails_OrderConfirmPage.order_Total.replace("$", "").replace(",", "")),
                pricingDetails.points, ccNumber);
    }

    @Test(priority = 80, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step19_Validate_Cancelled() throws Exception {
        String orderID = getOrderID();
        if (ClientDataRetrieval.isRedesignClient()) {
            Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails_OrderConfirmPage, salesTaxCost);
        } else {
            Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
        }
    }

    @Test(priority = 81, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step19_Validate_Points_Reimbursed() throws Exception {
        Navigation.verify_pointsMatchExpected(points_Before_Checkout);
    }

    @Test(priority = 82, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4505_Step4_CheckoutPage_ValidateStateDropdown_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4505_Step19_Validate_AccountHistory_OrderCancelled() throws Exception {
        Navigation.navigate_Account();
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase_Cancelled(pricingDetails.points);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
