package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CarRentalSearch_LandingPage_NearByAddress_NoPreferenceCarClass extends BaseTest {
	
	CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		
		searchCriteria.airportCode_CityName = "JAX";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(60);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(62);
		searchCriteria.carClassOptionDisplayed = "No preference";
		searchCriteria.pickUpTimeDisplayed = "2:00 PM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";
		searchCriteria.pickUpOption = "Near an address";
		searchCriteria.pickUpAddressOption = "1225 Broken Sound Pkwy";
		searchCriteria.pickUpCityOption = "Boca Raton";
		searchCriteria.pickUpZipOption = "33487";
		searchCriteria.pickUpStateOption = "Florida";
		
		searchCriteria.returnOption = "Near an address";
		searchCriteria.returnAddressOption = "13400 W Sunrise Blvd";
		searchCriteria.returnCityOption = "Sunrise";
		searchCriteria.returnZipOption = "33323";
		searchCriteria.returnStateOption = "Florida";

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_CarRental_Page();
	}
	
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_citi", "not_smart", "not_tvc", 
			"not_streetwise", "not_jeepwave" })
	public void PT3134_Step1_CarRentalSearch_LandingPage_VerifyCarRentalHeader() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_BreadcrumHeader"));
        BaseUI.verifyElementHasExpectedText("carRental_Header", "Car Rentals");	      
	}
	
	
	@Test(priority = 15, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_aarpredesign", "not_allstate", "not_allstateCash" })
	public void PT3134_Step1_CarRentalSearch_LandingPage_VerifyCarRentalImage() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_IntroBanner"));		
	}
	
	
	@Test(priority = 20, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step2_CarRentalSearch_LandingPage_VerifyCarRental_SubHeader() throws Exception {
		
		CarRentalSearch.verify_CarRentalPage_Subheader();
	}
	
	
	@Test(priority = 30, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step3_CarRentalSearch_LandingPage_VerifyPickUpLocation_Default() throws Exception {
		
		CarRentalSearch.verify_DefaultLocation_SelectedFor_PickUp();		
	}
	
	
	@Test(priority = 40, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step4_CarRentalSearch_LandingPage_VerifyPickUpReturnLocation_Default() throws Exception {
		
		CarRentalSearch.verify_Default_ReturnCarLocation_Selected();
	}
	
	
	/*
	 * Choose Near an address option for both pickup and same pick up address option 
	 * for return location. 
	 * And verify the search criteria on search result page
	 * 
	 */
	@Test(priority = 50, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step11To23_CarRentalSearch_LandingPage_Enter_NearAddress_SameReturnLocation() throws Exception {
		
		CarRentalSearch.carRentalSearchCriteria_NearAddress_Return_SamePickUpLocation(searchCriteria);
	}
		
	
	/*
	 * Choose Near an address option for both pickup and return location. 
	 * And verify the search criteria on search result page
	 * 
	 */
	@Test(priority = 50, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step11To23_CarRentalSearch_LandingPage_Enter_NearAddress_DifferentReturnLocation() throws Exception {
		
		CarRentalSearch.carRentalSearchCriteria_PickUp_NearAddress_Return_DifferentPickUpLocation_NearAddress(searchCriteria);	
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}//End of class
