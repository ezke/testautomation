package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import dRewards.pages_Administration.AddNewPromoCampaign_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class Promotion_Tests_Rewards_SweepsTakes extends BaseTest {

	String mainWindowHandle = "";
	String adminWindowHandle = "";

	String campaignName = "Automation_Test_";
	String transactionDescription = "Testing Automation Promo";
	String promocode = "Automation_";
	String sweepstakesOption = "autotestDAF";
	String sweepstakesTicketsQuantity = "5";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		String randomNumber = BaseUI.random_NumberAsString(70000, 1000000);
		campaignName += randomNumber;
		promocode += randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		
		Navigation_Admin.navigate_ClickNavLink("Create Promo Codes");
		// AddNewPromoCampaign_Admin.add_Promo(campaignName, rewardsCashAmount,
		// transactionDescription, promocode);
		AddNewPromoCampaign_Admin.add_Promo_SweepstakesTickets(campaignName, sweepstakesTicketsQuantity,
				transactionDescription, promocode, sweepstakesOption);
		AddNewPromoCampaign_Admin.verify_AddPromo_Successful();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT44_InvalidPromoCode_DoesntExist() throws Exception {
		String invalidCode = "acd2202";
		Navigation.validate_PromoCode_NotACode(invalidCode);
	}

	@Test(groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_InvalidPromoCode_WindowCloses_AfterClickingClose() throws Exception {
		String invalidCode = "abc1101";
		Navigation.validate_PromoWindow_Closes(invalidCode);
	}

	@Test(priority = 1, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT44_Step4_Sweepstakes_ValidPromoCode() throws Exception {
		Navigation.validate_PromoCode_Valid_SweepsTakes(promocode, sweepstakesTicketsQuantity, transactionDescription);
		// Sweepstakes.verifySweepstakesDetails();
	}

	@Test(priority = 2, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step4__Sweepstakes_ValidPromoCode_EnteredSecondTime_DoesntWork() throws Exception {
		Navigation.validate_PromoCode_CannotBeUsedTwice(promocode);

	}

	@Test(priority = 3, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_ValidPromoCode_MySweepstakes_Page() throws Exception {
		Navigation.navigate_Sweepstakes_MySweepstakes();
		Sweepstakes.verifyMySweepstakes_Header();
		Sweepstakes.verify_mySweepstakes_mySweepstakes_CurrentSweepstakesTab();
		Sweepstakes.verifyMysweepstakes_Tickets();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.navigate_Home_viaHomeLink();
		} else {
			Navigation.navigate_Home_viaLogo();
		}
		// Navigation.close_PromoWindow();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {

		} finally {
			Browser.closeBrowser();
		}
	}

}
