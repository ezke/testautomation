package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Sweepstakes_CurrentSweepstakes_Redeem extends BaseTest {

    String randomNum = BaseUI.random_NumberAsString(10000, 20000);
    String sweepstakesName = "autDAF" + randomNum;
    String sweepstakesDescription = sweepstakesName;
    Boolean isFeaturedSweepstakes = false;
    String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(-2);
    private String beginDateForDR = BaseUI.getDateAsString_InRelationToTodaysDate(-2, "MM/dd/yy");
    String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String ticketCost = "2.0";
    String cumulativeCap = "100";
    String docID = "385";
    String productSku = "ssdemochevycamerored";
    String position = "1";
    String dCash = "0";
    String quantity = "0";
    String numDays_Claim = "2";
    String ticketQty = "1";
    String ticketLimit = "1000";
    String option = "Ticket Purchase";
    String expectedText;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();

        Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
        Navigation_Admin.navigate_Sweepstakes_CreateProgram();

        AddNewSweepstakes_Admin.add_CurrentSweepstakes(sweepstakesName, sweepstakesDescription,
                Client_Info_Admin.return_ProgramIDs(), isFeaturedSweepstakes, startDate, endDate, drawingDate,
                displayEndDate, ticketCost, cumulativeCap, docID, productSku, position, dCash, quantity, numDays_Claim,
                ticketQty, ticketLimit, option);

        // validate created sweepstakes displays in admin site
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();

        // validate created sweepstakes active
        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .click_ActivateButton_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_Sweepstakes_IsActivated();

        // launch memcache flush to update sweepstakes
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
        } else {

            Navigation.navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("48");
        }
    }

    @Test(priority = 10, groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step23To28_VerifyCurrent_Sweepstakes_CanBe_Redeemed() throws Exception {

        String expectedText = "";
        ticketQty = "1";
        Integer ticketQtyInt = Integer.parseInt(ticketQty);

        Double ticketTotal = Double.parseDouble(ticketCost);
        Integer ticketTotalInt = ticketTotal.intValue();
        int totalRewards = (int) (ticketTotalInt * ticketQtyInt);
        if (ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
            double totalRewardsD = totalRewards;
            String totalRewardsStr = BaseUI.convertDouble_ToString_ForCurrency(totalRewardsD);
            expectedText = "You have successfully redeemed " + totalRewardsStr + " "
                    + ClientDataRetrieval.client_Currency + " for " + ticketQty + " Sweepstakes Ticket.";
        } else {
            expectedText = "You have successfully redeemed " + totalRewards + " " + ClientDataRetrieval.client_Currency
                    + " for " + ticketQty + " Sweepstakes Ticket.";
        }
        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes.verify_CurrentSweepstakes_Redeemed(ticketQty, expectedText);

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    ticketQty);
        } else {
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    ticketQty + " Ticket");
        }
    }

    @Test(priority = 20, groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step23To28_VerifyMySweepstakes_NumberOfTickets_Redeemed() throws Exception {

        String tickets = "1";
        Sweepstakes.verifyMySweepstakes_Tickets_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName, tickets,
                beginDateForDR);
        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    ticketQty);
            Navigation.navigate_Sweepstakes_MySweepstakes();
            BaseUI.baseStringCompare("MysweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName, Sweepstakes.mySweepstakesName);
        } else {
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    ticketQty + " Ticket");
            Navigation.navigate_Sweepstakes_MySweepstakes();
            BaseUI.baseStringCompare("MysweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName + " (" + tickets + " Ticket )", Sweepstakes.mySweepstakesName);
        }
    }

    @Test(priority = 30, groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step23To28_VerifyCurrent_Sweepstakes_AllSweeps_Redeemed() throws Exception {

        ticketQty = "99";
        expectedText = Sweepstakes.returnSweepstakesQts(expectedText, ticketCost, ticketQty);

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes.verify_CurrentSweepstakes_Redeemed(ticketQty, expectedText);

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
            Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    "100");
        } else {

            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    "100 Tickets");
        }
    }

    @Test(priority = 40, groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step23To28_VerifyMySweepstakes_AllTickets_Redeemed() throws Exception {

        String tickets = "99";
        if (ClientDataRetrieval.isRedesignClient()) {
            Sweepstakes.verifyMySweepstakes_Tickets_ByName2nd(AddNewSweepstakes_Admin.entered_sweepstakesName, tickets);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    "100");
            Navigation.navigate_Sweepstakes_MySweepstakes();
            BaseUI.baseStringCompare("MysweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName, Sweepstakes.mySweepstakesName);
        } else {
            Sweepstakes.verifyMySweepstakes_Tickets_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName, tickets, beginDateForDR);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    "100 Tickets");
            Navigation.navigate_Sweepstakes_MySweepstakes();
            BaseUI.baseStringCompare("MysweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName + " (100 Tickets)", Sweepstakes.mySweepstakesName);
        }
    }

    @Test(priority = 50, groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_PT4000_Step23To28_Verify_All_Current_Sweepstakes_Has_QuantityError() throws Exception {
        int quantity = 1;
        int maxQuantity = 0;
        if (ClientDataRetrieval.isRedesignClient()) {
            maxQuantity = 100;
        }

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes.verify_CurrentSweeptakes_Quantity_Error(quantity, maxQuantity);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
        } else {

            Navigation.navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("48");
        }
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
                Browser.closeBrowser();
                Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
            } else {
                Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            }
            LoginPage_Admin.login_Admin();
            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            AddNewSweepstakes_Admin.edit_Sweepstakes_ExpireTheSweepstake(sweepstakesName);
        } finally {
            Browser.closeBrowser();
        }
    }
}
