package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class Sweepstakes_PastSweepstakes_Tests extends BaseTest {

    String randomNum = BaseUI.random_NumberAsString(999, 9999);
    String sweepstakesName = "autotestDAF" + randomNum;
    String sweepstakesDescription = sweepstakesName;
    Boolean isFeaturedSweepstakes = false;
    String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(-2);
    private String beginDateForDR = BaseUI.getDateAsString_InRelationToTodaysDate(-2, "MM/dd/yy");
    String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(3);
    private String endDate_ForWebSite = BaseUI.getDateAsString_InRelationToTodaysDate(3, "MM/dd/yy");
    String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(3);
    private String drawingDate_ForWebsite = BaseUI.getDateAsString_InRelationToTodaysDate(3, "MM/dd/yy");
    String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(3);
    Integer hourToLast = -60;
    Integer minutesToLast = -15;
    String ticketCost = "2.0";
    String cumulativeCap = "100";
    String docID = "385";
    String productSku = "ssdemochevycamerored";
    String position = "1";
    String dCash = "0";
    String quantity = "0";
    String numDays_Claim = "2";
    String ticketQty = "1";
    String ticketLimit = "1000";
    String option = "Ticket Purchase";
    String officialRules_TopHeaderTitle;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();

        Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
        Navigation_Admin.navigate_Sweepstakes_CreateProgram();

        AddNewSweepstakes_Admin.add_CurrentSweepstakes(sweepstakesName, sweepstakesDescription,
                Client_Info_Admin.return_ProgramIDs(), isFeaturedSweepstakes, startDate, endDate, drawingDate,
                displayEndDate, ticketCost, cumulativeCap, docID, productSku, position, dCash, quantity, numDays_Claim,
                ticketQty, ticketLimit, option);

        // validate created sweepstakes displays in admin site
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();

        // validate created sweepstakes active
        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .click_ActivateButton_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_Sweepstakes_IsActivated();

        // launch memcache flush to update sweepstakes
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
        } else {
            Navigation.navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("48");
        }
    }

    @Test(priority = 10, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_Verify_CurrentSweepstakes() throws Exception {

        Sweepstakes.verify_CurrentSweepstakes_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName, drawingDate_ForWebsite);
    }

    @Test(priority = 20, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_VerifyCurrent_Sweepstakes_CanBe_Redeemed_ForPastSweepstakes() throws Exception {

        String expectedText = "";
        ticketQty = "1";
        Integer ticketQtyInt = Integer.parseInt(ticketQty);

        Double ticketTotal = Double.parseDouble(ticketCost);
        Integer ticketTotalInt = ticketTotal.intValue();
        int totalRewards = (int) (ticketTotalInt * ticketQtyInt);
        if (ClientDataRetrieval.isRedesignClient()) {
            String totalRewardsStr = BaseUI.convertInteger_ToString_ForCurrency(totalRewards);
            expectedText = "You have successfully redeemed " + totalRewardsStr + " "
                    + ClientDataRetrieval.client_Currency + " for " + ticketQty + " Sweepstakes Ticket.";
            Sweepstakes.verify_CurrentSweepstakes_Redeemed(ticketQty, expectedText);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    ticketQty);
        } else {
            if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.JeepwaveRewards)){
                expectedText = "You have successfully redeemed " + totalRewards + ".00 " + ClientDataRetrieval.client_Currency
                        + " for " + ticketQty + " Sweepstakes Ticket.";
            }else {
                expectedText = "You have successfully redeemed " + totalRewards + " " + ClientDataRetrieval.client_Currency
                        + " for " + ticketQty + " Sweepstakes Ticket.";
            }
            Sweepstakes.verify_CurrentSweepstakes_Redeemed(ticketQty, expectedText);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    ticketQty + " Ticket");
        }
    }

    @Test(priority = 30, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_VerifyMySweepstakes_NumberOfTickets_Redeemed() throws Exception {

        String tickets = "1";
        if (ClientDataRetrieval.isRedesignClient()) {

            Sweepstakes.verifyMySweepstakes_Tickets_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName, tickets, beginDateForDR);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    tickets);
            Navigation.navigate_Sweepstakes_MySweepstakes();
            BaseUI.baseStringCompare("MysweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName,
                    Sweepstakes.mySweepstakesName);
        } else {
            Sweepstakes.verifyMySweepstakes_Tickets_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName, tickets, beginDateForDR);
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_YourEntries_Value"),
                    tickets + " Ticket");
            Navigation.navigate_Sweepstakes_MySweepstakes();
            BaseUI.baseStringCompare("MysweepstakesName", AddNewSweepstakes_Admin.entered_sweepstakesName + " (" + tickets + " Ticket )",
                    Sweepstakes.mySweepstakesName);
        }
    }

    // Navigate to Admin site to setup past sweeptakes
    @Test(priority = 40, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_Admin_Setup_PastSweepstakes() throws Exception {

        Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
        LoginPage_Admin.login_Admin();

        Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
        Navigation_Admin.navigate_Sweepstakes_SearchProgram();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .verify_Sweepstakes_ResultDisplays_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);

        AddNewSweepstakes_Admin.edit_Sweepstakes_ForPastSweepstakes(hourToLast, minutesToLast,
                AddNewSweepstakes_Admin.entered_sweepstakesName, startDate);
    }

    @Test(priority = 50, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_PastSweepstakes_VerifySweepstakes_In_MySweepstakesPage() throws Exception {
        // Navigate to application and verify Mysweepstakes Page
        Browser.closeBrowser();
        Browser.openBrowser(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Sweepstakes_MySweepstakes();

        if (ClientDataRetrieval.isRedesignClient()) {
            Sweepstakes.clickLiveSweepstakes_Tab();
            String[] breadcrumb = {"Home", "My Rewards"};
            Navigation.verify_BreadcrumbList(breadcrumb);
        } else {

            String[] breadcrumb = {"Home", "Sweepstakes | History"};
            Navigation.verify_BreadcrumbList(breadcrumb);
        }

        Sweepstakes.verifyMySweepstakes_Header();
        Sweepstakes.verify_mySweepstakes_mySweepstakes_CurrentSweepstakesTab();
    }

    @Test(priority = 60, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_PastSweepstakes_Verify_Sweepstakes_Tickets_Column() throws Exception {
        TableData mySweeps;
        String tickets = "1";

        if(ClientDataRetrieval.isRedesignClient()){
            Sweepstakes.clickClosedSweepstakes_Tab();
        }

        Sweepstakes.expand_MySweepstakes_Row_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);

        Sweepstakes.clickMySweepstakes_tileName(AddNewSweepstakes_Admin.entered_sweepstakesName);

        mySweeps = Sweepstakes.return_automation_SweepstakesTable();

        if (ClientDataRetrieval.isRedesignClient()) {
            startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM d, yyyy").toUpperCase();
        } else {
            startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
        }

        mySweeps.verify_Value_InColumn("Quantity", tickets);
        mySweeps.verify_Value_InColumn("Earn Date", startDate);
        mySweeps.verify_Value_InColumn("Earn Method", "Ticket Purchase");
    }

    @Test(priority = 70, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_ClickAndVerify_PastSweepstakes_ViewSweepstakes_Button_SweepstakesInfo()
            throws Exception {

        String endedDate = "";

        if (ClientDataRetrieval.isRedesignClient()) {

            Sweepstakes.clickClosedSweepstakes_Tab();
            Sweepstakes.clickMySweepstakes_tileName(AddNewSweepstakes_Admin.entered_sweepstakesName);
            Sweepstakes.click_ClosedSweepStakes_ViewSweepstake_Button();

            endedDate = BaseUI.getTextFromField(Locator.lookupElement("pastSweepstakes_EndDate_Text"))
                    .replaceAll("[\r\n]+", " ").replaceAll("Ended: ", "");
            BaseUI.verifyElementAppears(Locator.lookupElement("pastSweepstakes_SweepstakesInfo_Text"));
            BaseUI.verifyElementAppears(Locator.lookupElement("pastSweepstakes_Winners_Text"));
            BaseUI.verifyElementHasExpectedText("pastSweepstakes_Winners_Text", "Winner Pending");
            BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endedDate,"MM/dd/yy"),"End Date matched format MM/dd/yy",
                    "End Date did NOT match format MM/dd/yy, seeing " + endedDate);
         //   BaseUI.baseStringCompare("Ended Date", endDate_ForWebSite, endedDate);
        } else {

            Sweepstakes.mySweepstakesName = BaseUI.getTextFromField(Locator.lookupElement("mySweepstakes_CurrentSweepstakes_ByName",
                    AddNewSweepstakes_Admin.entered_sweepstakesName, null)).replaceAll("[\r\n]+", " ");
            Sweepstakes.click_MySweeptakes_ViewSweeptakesButton_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);

            endedDate = BaseUI.getTextFromField(Locator.lookupElement("pastSweepstakes_EndDate_Text"))
                    .replaceAll("[\r\n]+", " ").replaceAll("Ended: ", "");
            BaseUI.verifyElementAppears(Locator.lookupElement("pastSweepstakes_SweepstakesInfo_Text"));
            BaseUI.verifyElementAppears(Locator.lookupElement("pastSweepstakes_Winners_Text"));
            BaseUI.verifyElementHasExpectedText("pastSweepstakes_SweepstakesInfo_Text", "Sweepstakes Info");
            BaseUI.verifyElementHasExpectedText("pastSweepstakes_Winners_Text", "Winners");
            BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endedDate,"MM/dd/yy"),"End Date matched format MM/dd/yy",
                    "End Date did NOT match format MM/dd/yy, seeing " + endedDate);
          //  BaseUI.baseStringCompare("Ended Date", endDate_ForWebSite, endedDate);
        }
    }

    @Test(priority = 80, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT30_PT3998_Step7To11_PastSweepstakes_Verify_PastSweepstakes_BreadCrumb() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            //	Sweepstakes.clickUpcomingSweepstakesHomePage();
            String[] breadcrumb = {"Home", "Closed Sweepstakes"};
            Navigation.verify_BreadcrumbList(breadcrumb);
        } else {

            String[] breadcrumb = {"Home", "Past Sweepstakes"};

            Navigation.navigate_Menu_Submenu("Sweepstakes", "Past Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("24");
            Navigation.verify_BreadcrumbList(breadcrumb);
        }
    }

    @Test(priority = 90, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT30_Step7To11_Verify_PastSweepstakes_SeeDetails_Button_Present() throws Exception {

        BaseUI.verifyElementAppears(Locator.lookupElement("pastSweepstakes_SeeDetails_Button_BySweepsatekesName",
                AddNewSweepstakes_Admin.entered_sweepstakesName, null));
    }

    @Test(priority = 100, groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT30_Step7To11_ClickAndVerify_PastSweepstakes_SeeDetails_SweepstakesInfo() throws Exception {

        Sweepstakes.click_PastSweepstakes_ListView_Button();
        Sweepstakes.verify_PastSweepstakes_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {

        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();
            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            Sweepstakes_SearchSweepstakesProgramSearch_Admin.search_For_AndThen_UpdateEndTime(sweepstakesName, BaseUI.getDateAsString_InRelationToTodaysDate(-1));
        } finally {
            Browser.closeBrowser();
        }
    }
}
