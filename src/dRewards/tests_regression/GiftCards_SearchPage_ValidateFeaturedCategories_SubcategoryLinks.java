package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GiftCards_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class GiftCards_SearchPage_ValidateFeaturedCategories_SubcategoryLinks extends BaseTest {
	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
			
		Navigation.navigate_GiftCards();	
	}
	
	
	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
		
	@DataProvider(name = "SubcategoryHeaders")
	public Object[][] createData_SubcategoryHeader() throws Exception {
		
		ArrayList<String> subcategoryHeaders = new ArrayList<String>(GiftCards_LandingPage.retrieve_FeaturedCategories_SubcategoryHeader());

		return return_linkTextAs_ObjectArray(subcategoryHeaders);
	}
	
	
	@DataProvider(name = "LinksBelow_DiningSubcategoryHeaders")
	public Object[][] createData_LinksBelow_DiningSubcategoryHeaders() throws Exception {
		
		String categoryName = "Dining";
		ArrayList<String> subcategoryHeaders_links = new ArrayList<String>(GiftCards_LandingPage.retrieve_Links_BelowSubcategoryImage(categoryName));

		return return_linkTextAs_ObjectArray(subcategoryHeaders_links);
	}
	
	
	@DataProvider(name = "LinksBelow_ShoppingSubcategoryHeaders")
	public Object[][] createData_LinksBelow_ShoppingSubcategoryHeaders() throws Exception {
		
		String categoryName = "Shopping";
		ArrayList<String> subcategoryHeaders_links = new ArrayList<String>(GiftCards_LandingPage.retrieve_Links_BelowSubcategoryImage(categoryName));

		return return_linkTextAs_ObjectArray(subcategoryHeaders_links);
	}
	
	
	@DataProvider(name = "LinksBelow_TravelSubcategoryHeaders")
	public Object[][] createData_LinksBelow_TravelSubcategoryHeaders() throws Exception {
		
		String categoryName = "Travel";
		ArrayList<String> subcategoryHeaders_links = new ArrayList<String>(GiftCards_LandingPage.retrieve_Links_BelowSubcategoryImage(categoryName));

		return return_linkTextAs_ObjectArray(subcategoryHeaders_links);
	}
		
		
	public Object[][] return_linkTextAs_ObjectArray(ArrayList<String> dataSet) throws Exception {
		Object[][] linksToNavigate = Navigation.returnLinkTextAs_ObjectArray(dataSet);

		return linksToNavigate;
	}
	
		
	@Test(dataProvider = "SubcategoryHeaders", alwaysRun = true, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3242_GiftCardSearchPage_ClickAndVerify_FeaturedCategories_EachSubcategoryHeaderTitle(String SubcategoryHeader) throws Exception {

		String[] breadcrumbs = { "Home", "Gift Cards", SubcategoryHeader };
		String[] breadcrumbs_Entertainment = { "Home", "Gift Cards", "Movie Tickets" };
		GiftCards_LandingPage.Click_FeaturedCategories_SubcategoryHeader_LinkText(SubcategoryHeader);
		
		if(SubcategoryHeader.equals("Entertainment")) {
			Navigation.verify_BreadcrumbList(breadcrumbs_Entertainment);
		} else {
			Navigation.verify_BreadcrumbList(breadcrumbs);
		}
		//Validation for Citi
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle")); 
		String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle"));
		BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", SubcategoryHeader, headerTitle);
	}
	
	
	@Test(dataProvider = "SubcategoryHeaders", alwaysRun = true, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardSearchPage_Verify_EachCategory_HasLinks(String category) throws Exception {	
		GiftCards_LandingPage.verify_Links_BelowSubcategoryImage(category);	
	}
		
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardSearchPage_VerifyFeaturedCategories_HeaderTitle() throws Exception {

		BaseUI.scroll_to_element(Locator.lookupRequiredElement("giftCards_FeaturedCategories_header"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_FeaturedCategories_header"));	
		BaseUI.verifyElementHasExpectedPartialText("giftCards_FeaturedCategories_header", "Featured Categories");
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3242_GiftCardSearchPage_VerifyFeaturedCategories_EachSubcategoryHeaderTitle() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_FeaturedCategories_Subcategory"));
		GiftCards_LandingPage.verify_FeaturedCategories_Subcategory();
	}
	
	
	@Test(dataProvider = "LinksBelow_DiningSubcategoryHeaders", groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardSearchPage_verify_LinksBelow_DiningSubcategoryImage(String link_subcategoryHeader) throws Exception {

		String subcategoryHeader = "Dining";
		GiftCards_LandingPage.verifyLinksDisplayedBelowEachGiftCardSubcategory(subcategoryHeader, link_subcategoryHeader.trim());
	}
	
	
	@Test(dataProvider = "LinksBelow_ShoppingSubcategoryHeaders", groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardSearchPage_verify_LinksBelow_ShoppingSubcategoryImage(String link_subcategoryHeader) throws Exception {

		String subcategoryHeader = "Shopping";
		GiftCards_LandingPage.verifyLinksDisplayedBelowEachGiftCardSubcategory(subcategoryHeader, link_subcategoryHeader);
	}
	
	
	@Test(dataProvider = "LinksBelow_TravelSubcategoryHeaders", groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3242_GiftCardSearchPage_verify_LinksBelow_TravelSubcategoryImage(String link_subcategoryHeader) throws Exception {

		String subcategoryHeader = "Travel";
		GiftCards_LandingPage.verifyLinksDisplayedBelowEachGiftCardSubcategory(subcategoryHeader, link_subcategoryHeader);
	}
		
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardSearchPage_VerifyEachSubcategoryImage() throws Exception {
		GiftCards_LandingPage.verify_GiftCardFeaturedCategories_EachSubcategoryImage();
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_GiftCards();	
	}
	
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}//End of class
