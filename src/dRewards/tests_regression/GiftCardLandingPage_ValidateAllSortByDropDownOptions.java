package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GiftCards_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class GiftCardLandingPage_ValidateAllSortByDropDownOptions extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_GiftCard_All();
		GiftCards_LandingPage.clickViewAllOption();
	}
	
	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_NewArrivals() throws Exception {

		GiftCards_LandingPage.sortBy("New Arrivals");
		TableData newList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(newList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_TopPicks() throws Exception {

		GiftCards_LandingPage.sortBy("Name A-Z");
		GiftCards_LandingPage.sortBy("Top Picks");
		TableData newList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(newList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_NameAToZ() throws Exception {

		GiftCards_LandingPage.sortBy("Name A-Z");
		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Ascending("giftcards");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_NameZToA() throws Exception {

		GiftCards_LandingPage.sortBy("Name Z-A");
		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Descending("giftcards");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_YouPay_LowToHigh() throws Exception {

		GiftCards_LandingPage.sortBy("You Pay (low to high)");

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Ascending("youPay");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_YouPay_HighToLow() throws Exception {

		GiftCards_LandingPage.sortBy("You Pay (high to low)");

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Descending("youPay");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_Points_LowToHigh() throws Exception {

		String sortBy = "Points (low to high)";
		GiftCards_LandingPage.sortBy(sortBy);

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Ascending("points");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi", "not_citi" })
	public void PT3666_GiftCardsPage_Step4_SortBy_Points_HighToLow() throws Exception {

		String sortBy = "Points (high to low)"; 
		GiftCards_LandingPage.sortBy(sortBy);

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Descending("points");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step5_Verify_ResultPerPage_24PerPage() throws Exception {

		String resultPerPage1 = "48";
		GiftCards_LandingPage.verify_resultPerPage(resultPerPage1);
		
		String resultPerPage = "24";
		GiftCards_LandingPage.verify_resultPerPage(resultPerPage);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step5_Verify_ResultPerPage_12PerPage() throws Exception {

		String resultPerPage1 = "24";
		GiftCards_LandingPage.resultPerPage(resultPerPage1);

		String resultPerPage = "12";
		GiftCards_LandingPage.verify_resultPerPage(resultPerPage);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_GiftCardsPage_Step5_Verify_ResultPerPage_48PerPage() throws Exception {

		String resultPerPage1 = "24";
		GiftCards_LandingPage.resultPerPage(resultPerPage1);

		String resultPerPage = "48";
		GiftCards_LandingPage.verify_resultPerPage(resultPerPage);
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaHomeLink();
		Navigation.navigate_GiftCard_All();
		GiftCards_LandingPage.clickViewAllOption();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		try {

		} finally {
			Browser.closeBrowser();
		}
	}
} //End of class
