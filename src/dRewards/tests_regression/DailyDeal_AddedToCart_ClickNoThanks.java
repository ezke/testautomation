package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.*;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;
import java.util.HashMap;

public class DailyDeal_AddedToCart_ClickNoThanks extends BaseTest {

    HashMap<String, String> homepageDailyDealData;
    HashMap<String, String> dailyDeal_Data;
    private ArrayList<String> imagesOnSlidesList;
    String expected_DailyDeal_PopupDescription = "This item will be reserved in your cart for 20 minutes.Please complete the following verification to proceed. This step reduces fraudulent activity so we can ensure Rewards for Good users have the best experience.";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        // Logic to remove all items from Shopping Cart.
        Homepage.deleteItemsFromShoppingCart();

        //Store Daily Deal details on home page and on Daily deal landing page
        homepageDailyDealData = Homepage.clickDailyDeal();
        dailyDeal_Data = DailyDeals.return_DailyDealInfo();
    }

    @Test(priority = 10, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4315_Step1_Validate_DailyDeals_Info_On_DailyDealPage() throws Exception {

        DailyDeals.verify_DailyDealInfo(homepageDailyDealData, dailyDeal_Data);
    }


    @Test(priority = 15, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDealsHeaderTitle_On_DailyDealPage() throws Exception {
        WebElement dailyDealsHeaderTitleElement = Locator.lookupRequiredElement("dailyDeals_HeaderTitle");
        String actualDailyDealsHeaderTitle = BaseUI.getTextFromField(dailyDealsHeaderTitleElement);
        BaseUI.verifyElementAppears(dailyDealsHeaderTitleElement);
        BaseUI.baseStringPartialCompare("dailyDeals_HeaderTitle", "Daily Deal", actualDailyDealsHeaderTitle);
    }


    @Test(priority = 15, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDealsStartTime_On_DailyDealPage() throws Exception {
        WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("dailyDeals_HeaderTitle_StartTime");
        String actualDailyDealsHeaderTitle_StartTime = BaseUI.getTextFromField(dailyDealsStartTimeElement);
        BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
        BaseUI.baseStringCompare("dailyDeals_HeaderTitle_StartTime", "New item daily at 3PM ET", actualDailyDealsHeaderTitle_StartTime);
    }


    @Test(priority = 15, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_AvailableStatus_On_DailyDealPage() throws Exception {
        WebElement availableStatusElement = Locator.lookupRequiredElement("dailyDeals_Available_Status");
        String actualAvailableStatusText = BaseUI.getTextFromField(availableStatusElement);
        BaseUI.verifyElementAppears(availableStatusElement);
        BaseUI.baseStringCompare("dailyDeals_Available_Status",
                "This item is still AVAILABLE."+"\n"+"Hurry, quantities are limited!", actualAvailableStatusText);
    }


    @Test(priority = 15, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_DealExpiresText_On_DailyDealPage() throws Exception {
        WebElement dealExpiresTextElement = Locator.lookupRequiredElement("dailyDeals_DealExpireText");
        String actualDealExpiresText = BaseUI.getTextFromField(dealExpiresTextElement);
        BaseUI.verifyElementAppears(dealExpiresTextElement);
        BaseUI.baseStringPartialCompare("dealExpiresText",
                "This deal expires in:"+"\n"+BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_EndTime")), actualDealExpiresText);
    }


    @Test(priority = 15, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
			"not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_Image_Appears() throws Exception {
        DailyDeals.verify_Image_Appears();
    }


    @Test(priority = 20, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_ShippingType_On_DailyDealPage() throws Exception {
        if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_ShippingType"))) {
            WebElement dealShippingTypeElement = Locator.lookupRequiredElement("dailyDeals_ShippingType");
            String actualDailyDeals_ShippingTypesText = BaseUI.getTextFromField(dealShippingTypeElement);
            BaseUI.verifyElementAppears(dealShippingTypeElement);
            BaseUI.baseStringCompare("dealExpiresText", "Free Shipping", actualDailyDeals_ShippingTypesText);
        }
    }


    @Test(priority = 21, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4315_Step1_Validate_DailyDeals_Image_LeftNavigationArrow_Appears() throws Exception {
        WebElement leftNavigationArrow = Locator.lookupRequiredElement("dailyDeals_Images_LeftNavigationArrow");

        //Left Navigation arrow is not always present for every daily deal,
        //so checking before validation
        if(BaseUI.elementAppears(leftNavigationArrow)){
            BaseUI.scroll_to_element(leftNavigationArrow);
            BaseUI.verifyElementAppears(leftNavigationArrow);
        }
    }

    @Test(priority = 21, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_Image_RightNavigationArrow_Appears() throws Exception {
        WebElement rightNavigationArrow = Locator.lookupRequiredElement("dailyDeals_Images_RightNavigationArrow");

        //Right Navigation arrow is not always present for every daily deal,
        //so checking before validation
        if(BaseUI.elementAppears(rightNavigationArrow)){
            BaseUI.scroll_to_element(rightNavigationArrow);
            BaseUI.verifyElementAppears(rightNavigationArrow);
        }
    }


    @Test(priority = 22, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_AllImage_OnSlide_Appears() throws Exception {
        imagesOnSlidesList = DailyDeals.return_DailyDeals_AllImagesDisplayed_OnSlides();
        BaseUI.verify_true_AndLog(imagesOnSlidesList.size() > 0, "Images are DISPLAYED ON SLIDE: "+ imagesOnSlidesList.size(),
                "Images are NOT DISPLAYED ON SLIDE: "+ imagesOnSlidesList.size());
    }


    @Test(priority = 26, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step1_Validate_DailyDeals_Description_Text_Appears() throws Exception {
        BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("dailyDeals_Description"), "Description");
    }

    @Test(priority = 31, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4315_Step2_Validate_DailyDeals_AddToCart_RecaptchaPopupModal_Description() throws Exception {
        DailyDeals.click_AddToShoppingCart();
        String dailyDeal_PopupDescription = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_Description"));
        BaseUI.baseStringCompare("DailyDeal Popup Description", expected_DailyDeal_PopupDescription,
                dailyDeal_PopupDescription);
    }

    @Test(priority = 32, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step2_Validate_DailyDeals_AddToCart__RecaptchaPopupModal_ContinueBtn() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeal_reCaptchaVerificationModal_RecaptchaContinueBtn"));
    }


    @Test(priority = 33, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step2_Validate_DailyDeals_AddToCart__RecaptchaPopupModal_IAmNotRobotText() throws Exception {
        BaseUI.switch_ToIframe(Locator.lookupRequiredElement("dailyDeal_reCaptchaVerificationModal_Recaptcha_Iframe"));
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeal_reCaptchaVerificationModal_Recaptcha_ImNotARobotText"));
        //Through automation script it does not show "I'm not a robot" text. So commenting this line.
        //BaseUI.verifyElementHasExpectedText("dailyDeal_reCaptchaVerificationModal_Recaptcha_ImNotARobotText", "I'm not a robot");
    }

    @Test(priority = 35, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step3_Validate_DailyDeals_Item_InShoppingCart_PopUpWithMessageAppears() throws Exception {
        BaseUI.switch_ToDefaultContent();
        DailyDeals.click_DailyDealsRecaptchModal_NoThanksLink();
        Navigation.navigate_ShoppingCart();
        ShoppingCart.verify_DailyDeals_AddedToCart_Popup();
    }

    @Test(priority = 40, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step3_DailyDeals_ShoppingCart_DailyDealPopup_Closed_ValidateProductDescription() throws Exception {

        ShoppingCart.close_DailyDeal_AddedToCart_Popup();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("shpCart_DailyDeal_Popup_Modal"));
        String dailyDealName = dailyDeal_Data.get("dailyDealName");
        ShoppingCart.verify_product_OnShoppingCartPage(dailyDealName);
    }

    @Test(priority = 45, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
			"not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4315_Step4_Validate_DailyDeals_RecaptchaPopUpAppears_WithDescription_AfterProceedToCheckoutClicked() throws Exception {
        ShoppingCart.click_ProceedToCheckout();
        String dailyDeal_PopupDescription = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("shpCart_DailyDeal_Popup_Description"));
        BaseUI.baseStringCompare("DailyDeal Popup Description", expected_DailyDeal_PopupDescription,
                dailyDeal_PopupDescription);
    }

    @Test(priority = 50, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4315_Step4_DailyDeals_ShoppingCart_RefreshBrowser_DailyDealRecaptchaModalClosed() throws Exception {
        Navigation.refreshBrowser();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_ProductList_TextLink"));
    }


    @Test(priority = 55, groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
			"not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4315_Step4_Validate_DailyDeals_Item_Removed_FromShoppingCart() throws Exception {
        ShoppingCart.removeItemFromShoppingCart();
        ShoppingCart.verify_CartEmptyText_Appears();
    }


    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
