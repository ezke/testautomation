package dRewards.tests_regression;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Navigation;
import dRewards.pages.LoginPage;
import dRewards.pages_Administration.AddNewPromoCampaign_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class PromoCode_MultipleOffers extends BaseTest {

	String campaignName = "Automation_Test";
	String rewardsCashAmount = "500";
	String transactionDescription = "Testing Automation Promo";
	String promocode = "Automation";
	String[] discountPrice = { "2", "3", "5", "4" };
	String[] discount_SkuValue = { "pcdemofandango", "pcdemoringtones", "pcdemofandango" };
	String todayDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
	String ticketQuantity = "10";
	String sweepstakesOption = "autotestDAF";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		campaignName += "_" + randomNumber;
		promocode += "_" + randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		
		Navigation_Admin.navigate_ClickNavLink("Create Promo Codes");
		AddNewPromoCampaign_Admin.add_Promocode_With_MultiOffers(campaignName, rewardsCashAmount,
				transactionDescription, promocode, ticketQuantity, sweepstakesOption, discount_SkuValue, discountPrice);

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.enter_PromoCode(promocode);

	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests",
			"all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_Verify_PromoCode_Displays() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Message"));
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_Verify_PromoCode_RightNavigationArrow_Displays() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_RightNavigationArrow"));
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT44_Step2_Verify_PromoCode_LeftNavigationArrow_Displays() throws Exception {
		
		Navigation.click_PromoCode_RightNavigationArrow();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_LeftNavigationArrow"));
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_Verify_PromoCode_OfferList_Displays() throws Exception {
		
		ArrayList<String> offerList = Navigation.return_PromoCode_OfferList();
		BaseUI.verify_true_AndLog(offerList.size() > 0, "Found Promo Code Offer list.",
				"Could not find Promo Code Offer list.");
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_PromoCode_OfferList_Carousel_ClickAndVerify_RightArrow_NavigatedTo_FarRight() throws Exception {
		
		Navigation.clickAndVerify_NavigatePromoCode_OfferCarousel_ToFarRight();
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_PromoCode_OfferList_Carousel_ClickAndVerify_LeftArrow_NavigatedTo_FarLeft() throws Exception {
		
		Navigation.clickAndVerify_NavigatePromoCode_OfferCarousel_ToFarRight();
		Navigation.clickAndVerify_NavigatePromoCode_OfferCarousel_ToFarLeft();
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_PromoCode_OfferList_Carousel_ClickItem_AndVerify() throws Exception {
		
		String offerName = Navigation.get_PromoCodeOfferText_ByIndexAsString("1");
		Navigation.click_PromoCode_ViewDetails_FirstItem();
		String offerSelectedText = BaseUI.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_SelectedItem_Text"));
		BaseUI.baseStringCompare("PromoCode", offerName, offerSelectedText);
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_Verify_PromoCode_OfferList_Click_SweepstakesTickets() throws Exception {
		
		String offerName = "Sweepstakes Tickets";
		Navigation.click_PromoCode_ViewDetails_ByOfferName(offerName);
		BaseUI.verifyElementHasExpectedPartialText("nav_PromoCode_SelectedItem_Text", offerName);
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "critical_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT44_Step2_Verify_PromoCode_OfferList_Rewards() throws Exception {

		Navigation.verify_PromoCode_ForRewardsOrPoints(ClientDataRetrieval.client_Currency, promocode, rewardsCashAmount, transactionDescription);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
		Navigation.enter_PromoCode(promocode);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();

	}

}
