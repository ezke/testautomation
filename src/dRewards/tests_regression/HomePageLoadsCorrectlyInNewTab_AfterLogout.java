package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.time.Duration;
import java.util.ArrayList;

public class HomePageLoadsCorrectlyInNewTab_AfterLogout extends BaseTest {

    ArrayList<String> tabs;
    private String newTab;
    private String currentWindow;

    @BeforeClass (alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_HomePage_VerifyLogo() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_HomeLogo"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_HomePage_VerifyDailyDeal_Sweepstakes_AuctionsWidgetSection_HeaderTitle() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_DailyDeal_Sweepstakes_AuctionsWidgetSection_HeaderTitle"));
        BaseUI.verifyElementHasExpectedPartialText("homepage_DailyDeal_Sweepstakes_AuctionsWidgetSection_HeaderTitle",
                "take advantage of these offers ending soon.");
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" ,"not_mcafee"})
    public void PT4705_Step2_HomePage_VerifyAccountLink() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_AccountLink"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_HomePage_Verify_PointsAndCashRewards_Appear() throws Exception {
        Navigation.verify_PointsAndCashRewards_Appear();
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" ,"not_mcafee"})
    public void PT4705_Step2_Verify_ShopByBrandCarousel_Header() throws Exception {
        Homepage.verify_ShopByBrandCarousel_HeaderTitle();
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_Verify_SeeAllBrands_Link() throws Exception {
        Homepage.verify_SeeAllBrandsLink();
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_VerifyLpg_LearnMoreLink() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LearnMoreLnk"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_VerifyLpg_LpgLogo() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgLogo"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_VerifyLpg_LpgBottomFooter() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_lpgBFooter"));
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_HomePage_Verify_LocalOffer_Appears() throws Exception {
        Homepage.verify_LocalOffer_Appears();
    }

    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step2_HomePage_Verify_LocalOffer_BannerImgAppears() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_BannerImg"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4705_Step3_OpenANewTabInSameBrowser_VerifyNewTabIsOpened() throws Exception {
        //Get current tab window handle
        tabs = new ArrayList<String> (Browser.driver.getWindowHandles());
        currentWindow = Browser.driver.getWindowHandle();
        String currentTab_Title = Browser.driver.getTitle();
        //Open new tab in the same browser session
        ((JavascriptExecutor)Browser.driver).executeScript("window.open()");
        Thread.sleep(2000);
        BaseUI.wait_forPageToFinishLoading();

        //Switch to the new window
        BaseUI.verify_true_AndLog(tabs.size() > 0, "New Tab Opened",
                "New tab DID NOT open");
        newTab = BaseUI.wait_ForNewWindow_ToExist_AndSwitchToIt(tabs, Duration.ofSeconds(30));
        String newTab_Title = Browser.driver.getTitle();

        BaseUI.verify_true_AndLog(!(newTab_Title.equals(currentTab_Title)), "New tab window title DOES NOT MATCH",
                "New tab window title MATCH");

        Browser.driver.switchTo().window(currentWindow);
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_HomeLogo"));
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" ,"not_mcafee"})
    public void PT4705_Step4To5_LogoutFromCurrentSessionAndSwitchToNewTab_NavigateToUrl_VerifyLoginPageUsernameDisplayed() throws Exception {
        Navigation.navigate_Logout();

        Browser.driver.switchTo().window(newTab);
        Browser.navigateTo(ClientDataRetrieval.url);
        BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_easyDeals_UserName"));
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" ,"not_mcafee"})
    public void PT4705_Step6To7_LoginWithValidCredentials_VerifyHomePageLoadsCorrectly() throws Exception {
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_HomeLogo"));
        Navigation.verify_PointsAndCashRewards_Appear();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
