package dRewards.tests_regression;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GiftCards_LandingPage;
import dRewards.pages.GiftCards_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class GiftCards_ResultsPage_ValidateAllGiftCardType_FeaturedProducts extends BaseTest {
	
	private int index;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_GiftCard_All();		
	}
	
	
	@DataProvider(name = "LeftMenu_GiftCard_Subcategories")
	public Object[][] createData_SubcategoryHeader() throws Exception {
		
		ArrayList<String> subcategoryHeaders = new ArrayList<String>(GiftCards_LandingPage.return_GiftCards_NavMenuList());
		return return_linkTextAs_ObjectArray(subcategoryHeaders);
	}
	
	
	public Object[][] return_linkTextAs_ObjectArray(ArrayList<String> dataSet) throws Exception {
		BaseUI.verify_true_AndLog(dataSet.size() > 0, "Found links", "Did NOT find links.");

		Object[][] linksToNavigate = new Object[dataSet.size()][1];
		int objectIndex = 0;
		for (String linkItem : dataSet) {
			linksToNavigate[objectIndex][0] = linkItem;
			objectIndex++;
		}
	
		return linksToNavigate;		
	}
	
	
	@Test(dataProvider = "LeftMenu_GiftCard_Subcategories", alwaysRun = true, groups = { "regression_Tests", "all_tests", 
			"not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_Step14_GiftCardSearchPage_ClickAndVerify_FeaturedCategoriesDetails_ForEachSubcategory(String SubcategoryHeader) throws Exception {
		String[] breadcrumbs = { "Home", "Gift Cards", SubcategoryHeader };
		String[] breadcrumbs_Entertainment = { "Home", "Gift Cards", "Movie Tickets" };
		GiftCards_LandingPage.navigate_LeftPanel_ByText(SubcategoryHeader);
			
		if(SubcategoryHeader.contains("All Gift Card")) {
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_AllGiftcard_LandingPage_Title")); 
				String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_AllGiftcard_LandingPage_Title"));
				BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", "Gift Cards", headerTitle);
			} else {
				Navigation.verify_BreadcrumbList(breadcrumbs);
			}
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				//Validation for Citi
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle")); 
				String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle"));
				if(SubcategoryHeader.contains("Entertainment")) {
					BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", "Movie Tickets", headerTitle);
				} else {
					BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", SubcategoryHeader, headerTitle);
				}
			} else {
				if(SubcategoryHeader.equals("Entertainment")) {
					Navigation.verify_BreadcrumbList(breadcrumbs_Entertainment);
				} else {
					Navigation.verify_BreadcrumbList(breadcrumbs);
				}
			}			
				
			//Validation for Citi
//			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle")); 
//			String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle"));
//			BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", SubcategoryHeader, headerTitle);
			if(SubcategoryHeader.equals("Entertainment")) {
				GiftCards_SearchResults.verifySubcategoryTitle_LeftPanel("Movie Tickets");
			} else {
				GiftCards_SearchResults.verifySubcategoryTitle_LeftPanel(SubcategoryHeader);
			}
			GiftCards_SearchResults.verifySubcategory_FeaturedProductHeaderTitle(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProductTitle(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProductImage();
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_RetailPrice(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_YouPay(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_PointsOrSmartDollars(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_Savings(index);
				
			if(! ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				GiftCards_SearchResults.verifySubcategory_FeaturedProduct_ValidateSavingsPercentage(index);
				GiftCards_SearchResults.verifySubcategory_FeaturedProduct_ValidateYouPay(index);
			}					
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_TooltipDisplayed();
			GiftCards_SearchResults.close_FeaturedProduct_TooltipDisplayed();
		}				
	}
	
	@Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
	public void PT3666_Step6_GiftCardSearchPage_ClickAndVerify_FeaturedCategoriesGiftCards() throws Exception {
		
		ArrayList<String> featuredGiftCardList = GiftCards_LandingPage.return_GiftCards_FeaturedGiftCards();
		BaseUI.verify_true_AndLog(featuredGiftCardList.size() > 0, "Featured Gift Cards are displayed: "+ featuredGiftCardList.size(), 
				"Featured Gift Cards are NOT displayed: "+ featuredGiftCardList.size());
		
		for (index=0; index<featuredGiftCardList.size(); index++) {
			Navigation.navigate_GiftCard_All();
		    BaseUI.scroll_to_element(Locator.lookupRequiredElement("giftCards_SearchResult_FeaturedProductHeaderTitle"));
			GiftCards_SearchResults.verifySubcategory_FeaturedProductHeaderTitle(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProductTitle(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProductImage();
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_RetailPrice(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_PointsOrSmartDollars(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_YouPay(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_Savings(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_ValidateSavingsPercentage(index);
			GiftCards_SearchResults.verifySubcategory_FeaturedProduct_ValidateYouPay(index);
		}
	} 
			
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {	
		ResultWriter.checkForFailureAndScreenshot(result);
		Navigation.navigate_GiftCard_All();		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
