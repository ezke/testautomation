package dRewards.tests_regression;

import dRewards.pageControls.AuctionTile;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Auction_Details;
import dRewards.pages.Auctions;
import dRewards.pages.Auctions_Checkout_Shipping;
import dRewards.pages.Auctions_OpenAuctions;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewAuction_Admin;
import dRewards.pages_Administration.AddNewAuction_SubAuctionModal_Admin;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.AddNewAuction_Admin.AuctionTypes;
import dRewards.pages_Administration.AddNewAuction_Admin.RedemptionTypes;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

//@Test(groups = { "not_IE" })
public class Auctions_WinLimit_Tests extends BaseTest {
	private WebDriver originalDriver;
	private WebDriver secondBidderDriver;
	private String original_Browser;

	private String auctionName = "AutomationTest";
	private String auction2ndName = "AutoTest";
	private Double startingBid = 50.00;
	private Double currentBid = 50.00;
	// Extend Time is in seconds.
	private Integer extendTime = 20;
	// String productSku = "aucdysonDC44demo";
	private String productSku = "auc46samsungsmarthdtvDEMO";
	// String expectedProductName = "DC44 Animal Digital Slim Cordless Vacuum
	// with Handheld Tool Kit - DEMO ONLY";
	private String expectedProductName = "Samsung 46-inch LED 8000 Series Smart TV";
	private Boolean isDemo = true;

	private AuctionTile auctionTile = AuctionTile.empty();
	private String expectedEndTime = "";
	// Auction time in Minutes
	private Integer auctionTimeToLast = 6;

	private String user1_previousPoints;
	private String user2_previousPoints;
	private TableData bidHistory;
	private Integer numberOfBidders = 2;
	private Integer numberOfBids = 3;
	private String attribute = "13011";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		bidHistory = new TableData();
		if (BaseUI.get_SystemTimezone().equals("Central Standard Time")) {
			auctionTimeToLast += 60;
		}

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		auctionName += randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
		original_Browser = Browser.currentBrowser;

		originalDriver = Browser.driver;

		// Adding additional time for when tests run in Internet Explorer.
		if (Browser.currentBrowser.equals("internetexplorer")) {
			auctionTimeToLast += 1;
		}
		// Adding additional time for when tests run in AARP
		if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			auctionTimeToLast += 4;
		}

		LoginPage_Admin.login_Admin();

		// Check Auction Win Limit attribute value is greater than 1, if not set greater than 1
		Navigation_Admin.navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		String attributeValue = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("clients_ProgramAttributes_Value_ByAttributeID", attribute, null));

		if (!attributeValue.equals("1.00000")) {

		} else {
			Navigation_Admin.navigate_ExpandCategory("Clients");
			Clients_SearchClients.search_ClientID();
			Clients_SearchClients.click_ClientNumber_FromResult_Option();
			Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
			Clients_SearchClients.update_Attribute(attribute, "1000");

			Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
			Browser.driver = originalDriver;
		}
		Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction(auctionName, RedemptionTypes.FullRedemption, startingBid, extendTime,
				productSku, AuctionTypes.Standard, isDemo);
		expectedEndTime = Auctions.return_Auction_TimeLeftFormat_forCheckout(auctionTimeToLast);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Auctions_AllAuctions();
		user1_previousPoints = Navigation.returnPoints();
	}

	@Test(priority = 9, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_Auctions_DetailsPage() throws Exception {

		String expectedPointsOrRewards = "Enter an amount that is higher than the current bid"; //+ ClientDataRetrieval.client_Currency+ "";
		if (ClientDataRetrieval.isRedesignClient()) {
			expectedPointsOrRewards = "Enter a Points amount that is at least 1 higher than the current bid.";
		}
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.click_DetailsButton();
		Auction_Details.verify_AuctionDetails_SingleCurrency(currentBid, expectedPointsOrRewards);
		if (!ClientDataRetrieval.isRedesignClient()) {
			Auction_Details.verify_TimeLeft_ContainsCorrectFormat();
		}
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_1stUserBid_Successful() throws Exception {

		currentBid += 1;
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(0).get("bidAmount"));
		
		Auction_Details.verify_BidSuccessful(bidHistory.data.get(0).get("bidAmount"));
		
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
		Auction_Details.verify_BidBox_Links_Appears();
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_2ndUserBid_Successful() throws Exception {

		// Set up Second Bidder
		originalDriver = Browser.driver;
		ClientDataRetrieval.set_secondaryUser_Info(original_Browser);
		Browser.openBrowser(ClientDataRetrieval.url, "chrome");
		secondBidderDriver = Browser.driver;
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			LoginPage.loginForSecondBidder_ForAuctions(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
		} else {
			LoginPage.login(ClientDataRetrieval.secondaryUser, ClientDataRetrieval.secondaryPassword);
		}

		Navigation.navigate_Auctions_AllAuctions();
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, currentBid);
		currentBid += 1;
		user2_previousPoints = Navigation.returnPoints();

		auctionTile.click_DetailsButton();
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(false, String.valueOf(currentBid)));
		Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(1).get("bidAmount"));
		Auction_Details.verify_LeadingBidder(bidHistory.data.get(1).get("bidAmount"));
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user2_previousPoints, currentBid.toString()));
		Auction_Details.verify_BidBox_Links_Appears();
	}

	@Test(priority = 30, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_1stBidder_WasOutbid() throws Exception {

		Browser.driver = originalDriver;
		Auction_Details.verify_Outbid(user1_previousPoints);
		BaseUI.baseStringCompare("Points match previous count.", user1_previousPoints, Navigation.returnPoints());
	}

	@Test(priority = 40, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_1stBidder_CounterBidSuccessful() throws Exception {

		currentBid += 1;
		Browser.driver = originalDriver;
		bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
		Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(2).get("bidAmount"));
		Auction_Details.verify_BidSuccessful(bidHistory.data.get(2).get("bidAmount"));
		Navigation.verify_pointsMatchExpected(
				Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
		Auction_Details.verify_BidBox_Links_Appears();

		if (ClientDataRetrieval.isRedesignClient()) {
			String redesignText = "View Total Bids (3)";
			BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", redesignText);
		} else {
			BaseUI.verifyElementHasExpectedText("auctionBid_bidHistoryBids", numberOfBids.toString());
		}
	}

	@Test(priority = 50, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_2ndBidder_WasOutbid() throws Exception {

		Browser.driver = secondBidderDriver;
		Auction_Details.verify_Outbid(user2_previousPoints);
	}

	@Test(priority = 60, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_ReviewBidHistory() throws Exception {

		Browser.driver = originalDriver;
		Auction_Details.click_View_BidHistory();
		Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
	}

	@Test(priority = 70, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Close_BidHistory_Window() throws Exception {

		Auction_Details.click_Close_BidHistoryWindow();
		BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
	}

	// Step 12
	@Test(priority = 80, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_MyAccount_OpenAuctions_BidHistory_And_Description() throws Exception {
        //for redesign should verify when open carrot
		Navigation.navigate_Account_OpenAuctions();
		Auctions_OpenAuctions.expandAuctions_And_ClickViewAllBids_Link();

		if (ClientDataRetrieval.isRedesignClient()) {
			Auction_Details.return_BidHistoryDataMyAccountRedesign();
		} else {
			Auction_Details.verify_BidHistory(bidHistory, numberOfBidders, numberOfBids);
		}
		Auctions_OpenAuctions.verify_Description(expectedProductName);
		
	}
	@Test(priority = 90, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests"
			,"not_IE", "not_redesign", "not_aarpredesign"})
	public void PT3664_Step15_Auction_OpenAuctions_Close_BidHistory_Window() throws Exception {

		Auction_Details.click_Close_BidHistoryWindow();
		BaseUI.verifyElementDoesNotAppearByString("bidHistory_Close_Button");
	}

	@Test(priority = 100, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_AuctionEnds_1stUserWins_PopupAppears() throws Exception {

		Browser.driver = originalDriver;
		Navigation.navigate_Auctions_AllAuctions();
		auctionTile.click_DetailsButton();

		if (!ClientDataRetrieval.isRedesignClient()) {
			Auction_Details.wait_For_End_OfAuction(auctionTimeToLast * 60);

			Auction_Details.wait_For_ClaimPopup_ToAppear();
			Auction_Details.verify_AuctionWon_PopupAppears();
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("auctionBid_Winner_Text"));
		}
	}

	@Test(priority = 110, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_AuctionEnds_1stUserWins_WonBannerAppears() throws Exception {

		Auctions.loginBackAndClickMyAccount_CloseAuctions();
		Auction_Details.verify_AuctionWon_Banner_Appears();
	}

	@Test(priority = 120, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Verify_AuctionEnds_1stUserWins_ClaimAuction_ShippingPage() throws Exception {

		Auction_Details.click_ClaimNow_InWinningPopup();
		Auctions_Checkout_Shipping.verify_AuctionInfo(expectedProductName, currentBid.toString(), expectedEndTime);
	}

	
	// Create Another Auctions to set win limit attribute value to one
	@Test(priority = 130, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Create_SecondAuctions_ForWinLimit() throws Exception {

		Browser.driver = originalDriver;
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ClickNavLink("Create Auction");
		AddNewAuction_Admin.Add_New_Auction(auction2ndName, RedemptionTypes.FullRedemption, startingBid, extendTime,
				productSku, AuctionTypes.Standard, isDemo);
		expectedEndTime = Auctions.return_Auction_TimeLeftFormat_forCheckout(auctionTimeToLast);

		AddNewAuction_SubAuctionModal_Admin.add_subauction(auctionTimeToLast);
		AddNewAuction_Admin.initialize_Auction();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_Auctions_AllAuctions();

		String expectedPointsOrRewards = "Enter an amount that is higher than the current bid"; //+ ClientDataRetrieval.client_Currency + "";
		
		 if (ClientDataRetrieval.isRedesignClient()) {
			expectedPointsOrRewards = "Enter a Points amount that is at least 1 higher than the current bid.";
			}
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.click_DetailsButton();
		Auction_Details.verify_AuctionDetails_SingleCurrency(startingBid, expectedPointsOrRewards);
	}

	// Set Win Limit Attribute value to one in Admin site
	@Test(priority = 140, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Set_WinLimit_AttributeValue_ToOne() throws Exception {

		Browser.driver = secondBidderDriver;
		Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		Clients_SearchClients.update_Attribute(attribute, "1");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("clients_EditAllProgram_UpdateSuccessful_Text"));
	}

	@Test(priority = 150, alwaysRun = true, groups = { "not_jeepwave","not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Reload_AttributeValue() throws Exception {
		Browser.driver = secondBidderDriver;
		if (ClientDataRetrieval.isRedesignClient()) {
			Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
		} else {
			ClientDataRetrieval.set_AttributesReload_URL();
			Browser.navigateTo(ClientDataRetrieval.attributesReload_URL);
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("clients_Attributes_Reloaded_Text"));
		}
	}
	@Test(priority = 160, alwaysRun = true, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests" ,"not_IE"})
	public void PT3664_Step15_PT4755_Step2_Auctions_Verify_WinLimit_Message() throws Exception {

		Browser.driver = originalDriver;
		Navigation.navigate_Auctions_AllAuctions();
		auctionTile = Auctions.return_AuctionTile_ByAuctionNameAndCurrentBid(expectedProductName, startingBid);
		auctionTile.click_DetailsButton();
		String expectedMessage = "You've reached your monthly win limit!";
		if (ClientDataRetrieval.isRedesignClient()) {
			currentBid += 1;
			bidHistory.data.add(Auction_Details.return_bidHistoryInfo_AsRow(true, String.valueOf(currentBid)));
			Auction_Details.enter_And_Confirm_MaxBid(bidHistory.data.get(0).get("bidAmount"));
			//Auction_Details.verify_BidSuccessful(bidHistory.data.get(0).get("bidAmount"));
			//Navigation.verify_pointsMatchExpected(
			//Navigation.return_ExpectedPoints_AsString(user1_previousPoints, currentBid.toString()));
			//Auction_Details.verify_BidBox_Links_Appears();
			String expectedMessageRedesign = "Bid Rejected. Reached max win limit.";
			Auction_Details.verify_WinLimit_Message(expectedMessageRedesign);
		} else {
			Auction_Details.verify_WinLimit_Message(expectedMessage);
		}
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			Browser.driver = originalDriver;
			//Set Auction Win limit Attribute Value greater than 1
			Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
			LoginPage_Admin.login_Admin();
			Navigation_Admin.navigate_ExpandCategory("Clients");
			Clients_SearchClients.search_ClientID();
			Clients_SearchClients.click_ClientNumber_FromResult_Option();
			Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
			Clients_SearchClients.update_Attribute(attribute, "1000");
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("clients_EditAllProgram_UpdateSuccessful_Text"));
			if (ClientDataRetrieval.isRedesignClient()) {
				Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
			} else {

				ClientDataRetrieval.set_AttributesReload_URL();
				Browser.navigateTo(ClientDataRetrieval.attributesReload_URL);
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("clients_Attributes_Reloaded_Text"));
			}
		} finally {
			if (originalDriver != null) {
				Browser.driver = originalDriver;
				Browser.closeBrowser();
			} else {
				Browser.closeBrowser();
			}
			if (secondBidderDriver != null) {
				Browser.driver = secondBidderDriver;
				Browser.closeBrowser();
			}
		}
	}
}
