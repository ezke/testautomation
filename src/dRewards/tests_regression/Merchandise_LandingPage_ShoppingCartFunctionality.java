package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Merchandise_LandingPage_ShoppingCartFunctionality extends BaseTest {

	private String itemsInCart, itemsInCart1;
	private String subCategory_Name = "Sunglasses";//"Handbags";
	private String subCategory1_Name = "Beauty";//"Jewelry";
	private String increaseQuantity_Item = "2";
	private String decreaseQuantity_Item = "1";
	private int startPage = 1;
	private int endPage = 2;
	private int startPage1 = 3;
	private int endPage1 = 4;
	private String ItemName_First;

	private PricingDetails pricingDetails;
	private String finalPrice_ItemToCart;
	String finalPriceAfterAdding_OneItemToCart, itemsInCart_Increased;
	private String finalPriceAfterRemovingItemFromCart;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}
	
	
	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step1_Merchandise_LandingPage_VerifyMerchandiseTab() throws Exception {
	
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
				|| (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both))
				|| ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_Merchandise_MenuTab"));
			BaseUI.verifyElementHasExpectedText("nav_Merchandise_MenuTab", "Merchandise");
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_Merchandise_DropDown"));
			BaseUI.verifyElementHasExpectedText("nav_Merchandise_DropDown", "All Merchandise");
		}
	}
	
	
	@Test(priority = 20, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step2_Merchandise_LandingPage_ClickMerchandiseTab_AndVerifyHeader() throws Exception {
	
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)
				|| ClientDataRetrieval.isRedesignClient()){
			
			Navigation.navigate_Click_MerchandiseMenuTab_MerchandisePage();
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchandisePage_Header"));
			BaseUI.verifyElementHasExpectedText("merchandisePage_Header", "Merchandise");
		}
		else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Navigation.navigate_Click_MerchandiseMenuTab_MerchandisePage();
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_IntroBanner"));
		}
		else {
			Navigation.navigate_Click_MerchandiseMenuTab_MerchandisePage();
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_IntroBanner"));
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchandisePage_Header"));
			BaseUI.verifyElementHasExpectedText("merchandisePage_Header", "Merchandise");
		}		
	}
	
	
	@Test(priority = 30, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step3_Merchandise_LandingPage_VerifyCategory_AndFeaturedBrand_MenuHeader() throws Exception {
		
		Navigation.navigate_Click_MerchandiseMenuTab_MerchandisePage();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_ActiveCategoryHeader"));
		
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementHasExpectedText("merch_ActiveCategoryHeader", "Shop By Category");
		} else {
			BaseUI.verifyElementHasExpectedText("merch_ActiveCategoryHeader", "Category");
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_FeaturedBrand_Header"));
		BaseUI.verifyElementHasExpectedText("merch_FeaturedBrand_Header", "Featured Brands");
	}
	
	
	@Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_aarp", "not_allstate", "not_allstateCash", "not_citi", 
			"not_redesign", "not_aarpredesign","not_mcafee"})
	public void PT3243_Step4_Merchandise_LandingPage_VerifyFeaturedCategories_Header() throws Exception {
		
		Navigation.navigate_Click_MerchandiseMenuTab_MerchandisePage();
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_FeaturedCategories_Header"));
		BaseUI.verifyElementHasExpectedText("merch_FeaturedCategories_Header", "Featured Categories");
	}
	
	
	@Test(priority = 50, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step5_Merchandise_LandingPage_ClickAndVerify_Category_SubcategoryLink() throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Merchandise.clickAndVerify_Category_SubcategoryLink("Magazines");
		} else {
			Merchandise.clickAndVerify_Category_SubcategoryLink(subCategory_Name);
		}					
	}
	
	
	@Test(priority = 60, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step6_Merchandise_LandingPage_VerifyShoppingCart_IsDisplayed() throws Exception {
		
		Homepage.verifyShoppingCartDisplayed();
	}
	

	@Test(priority = 70, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step7_Merchandise_LandingPage_VerifyItemsIn_ShoppingDeleted() throws Exception {

		ShoppingCart.verifyShoppingCartDisplayed_AndDelete_ItemsInCart();	
	}
	
	
	@Test(priority = 75, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step7_Merchandise_LandingPage_VerifyItemsIn_ShoppingCartIncrease() throws Exception {
		ShoppingCart.addItemsToCart_VerifyItemsPointsAndRetailPrice_MatchFinalValues(subCategory_Name, 
				subCategory1_Name, startPage, endPage);	
	}
	

	@Test(priority = 80, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step8_Merchandise_LandingPage_VerifyItemsIn_ShoppingCartDecrease() throws Exception {
		String shopCart_ItemCount = Homepage.getShopCartItemsCount();
		if (shopCart_ItemCount.equals("0")  &&
				!(BaseUI.elementAppears(Locator.lookupRequiredElement("nav_ShoppingCartLink")))) {
			//Add 1st item to cart
			if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				Navigation.navigate_Merchandise_Magazines();
				Merchandise.add_To_Cart_ByNumber(BaseUI
						.random_NumberAsString(1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())));
			} else { 
				Merchandise.addItems_To_ShoppingCart(subCategory1_Name, startPage, endPage);
			}
			
			pricingDetails = ShoppingCart.retrieve_PricingDetails();
			itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
			finalPriceAfterAdding_OneItemToCart = ShoppingCart.retrieve_PricingDetails().finalRetailPrice_AfterAddingItem;
			
			// adding 2nd item to cart
			if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				Navigation.navigate_Merchandise_AutoAccessories();
				Merchandise.add_To_Cart_ByNumber(BaseUI
						.random_NumberAsString(1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())));
			} else {
				Merchandise.addItems_To_ShoppingCart(subCategory_Name, startPage1, endPage1);
			}
			
			itemsInCart_Increased = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
			finalPrice_ItemToCart = ShoppingCart.retrieve_PricingDetails().finalRetailPrice_AfterAddingItem;
		}
		
		Navigation.navigate_ShoppingCart();
		pricingDetails = ShoppingCart.retrieve_PricingDetails();
		finalPrice_ItemToCart = ShoppingCart.retrieve_PricingDetails().finalRetailPrice_AfterAddingItem;
		ShoppingCart.remove_AllItems();

		if (ClientDataRetrieval.isRedesignClient()) {
			//For AARPRedesign and Redesign Core when we remove all items from cart,
			//We see text "Your shopping cart is empty" and not pricing details section.
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("shpCart_EmptyMessage"));
			BaseUI.verifyElementHasExpectedText("shpCart_EmptyMessage", "Your shopping cart is empty");
			shopCart_ItemCount = Homepage.getShopCartItemsCount();
			BaseUI.verify_true_AndLog(shopCart_ItemCount.contains("0"), "Shopping cart icon is DISPLAYED and IS EMPTY", 
					"Shopping cart icon IS DISPLAYED and NOT EMPTY");
		} else {
			finalPriceAfterRemovingItemFromCart = ShoppingCart.retrieve_PricingDetails().finalRetailPrice_AfterAddingItem;//BaseUI
			//.getTextFromField(Locator.lookupRequiredElement("shpCart_PriceDetails_OrderFinalPrice"));

			BaseUI.baseStringCompareStringsAreDifferent("shpCart_PriceDetails_OrderFinalPrice",
					finalPrice_ItemToCart, finalPriceAfterRemovingItemFromCart);
			shopCart_ItemCount = Homepage.getShopCartItemsCount();

			BaseUI.log_Status("Shopping Cart icon is not displayed and does not have items");
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("Shp_ItemsInCart"));
		}		
	}
	
	
	@Test(priority = 90, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step9_Merchandise_LandingPage_EnterQuantity_ForItems_ShoppingCartIncrease() throws Exception {

		ShoppingCart.verifyShoppingCartDisplayed_AndDelete_ItemsInCart();
		ShoppingCart.addItemsToCart_VerifyItemsPointsAndRetailPrice_MatchFinalValues(subCategory_Name, subCategory1_Name, startPage, endPage);
		ShoppingCart.enterQuantity_AndVerify_ItemsCart_Price_Change(increaseQuantity_Item);					
	}		
		
	
	@Test(priority = 100, groups = { "regression_Tests", "all_tests", "not_citi" })
	public void PT3243_Step10_Merchandise_LandingPage_EnterQuantity_ForItems_ShoppingCartDecrease() throws Exception {
		
		ShoppingCart.verifyShoppingCartDisplayed_AndDelete_ItemsInCart();
		ShoppingCart.addItemsToCart_VerifyItemsPointsAndRetailPrice_MatchFinalValues(subCategory_Name, subCategory1_Name, startPage, endPage);
		ShoppingCart.enterQuantity_AndVerify_ItemsCart_Price_Change(increaseQuantity_Item);
		ShoppingCart.enterQuantity_AndVerify_ItemsCart_Price_Change(decreaseQuantity_Item);		
	}
		
	
	@Test(priority = 110, groups = { "regression_Tests", "all_tests" })
	public void PT3243_Step11_Merchandise_LandingPage_ShoppingCart_ClickProceedToCheckbox_VerifyNoShoppingCart() throws Exception {

		ShoppingCart.verifyShoppingCartDisplayed_AndDelete_ItemsInCart();
			
		//Add item to cart 
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Navigation.navigate_Merchandise_Magazines();
			Merchandise.add_To_Cart_ByNumber(BaseUI
					.random_NumberAsString(1, Integer.parseInt(Merchandise.getItemCount_MerchandiseSubcategoryLandingPage())));
		} else {
			Merchandise.addItems_To_ShoppingCart(subCategory_Name, startPage, endPage);
		}
		
		itemsInCart = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
		String firstItemName_InCart = ShoppingCart.retrieve_AllProductData().get(0).productName;
		ShoppingCart.click_ProceedToCheckout();
		BaseUI.verifyElementAppearsByString("checkout_Page_Header");
		BaseUI.verifyElementHasExpectedText("checkout_Page_Header", "Checkout");
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("shpCart_Page_Header"));
			
		Navigation.navigate_ShoppingCart();
		itemsInCart1 = BaseUI.getTextFromField(Locator.lookupRequiredElement("Shp_ItemsInCart"));
		ItemName_First = ShoppingCart.retrieve_AllProductData().get(0).productName;
		BaseUI.baseStringCompare("Shp_ItemsInCart", itemsInCart, itemsInCart1);
		BaseUI.baseStringCompare("shpCart_ProductDetails_ProductName", firstItemName_InCart, ItemName_First);	
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}
		
			Browser.closeBrowser();
	}

}
