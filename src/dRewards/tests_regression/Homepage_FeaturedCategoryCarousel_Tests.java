package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Homepage_FeaturedCategoryCarousel_Tests extends BaseTest {
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigateExpandCategory_SelectClientProgramId();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp" ,"not_mcafee"})
	public void PT3214_HomePage_Step19_Verify_FeaturedCategoriesCarousel_Header_Appears() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("homepage_FeaturedCatCarousel_Header"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_FeaturedCatCarousel_Header"));
		BaseUI.verifyElementHasExpectedPartialText("homepage_FeaturedCatCarousel_Header", "Featured Categories");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp","not_mcafee" })
	public void PT3214_HomePage_Step19_Verify_FeaturedCategories_RightNavigationArrow_Appears() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("homepage_FeaturedCatCarousel_Header"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_FeaturedCatCarousel_RightNavigation"));
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp","not_mcafee" })
	public void PT3214_HomePage_Step19_Verify_FeaturedCategories_LeftNavigationArrow_Appears() throws Exception {

		Homepage.click_FeaturedCategories_Carousel_RightNavigation_Arrow();
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_FeaturedCatCarousel_LeftNavigation"));
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp","not_mcafee" })
	public void PT3214_HomePage_Step19_Verify_FeaturedCategories_Clicks_Entire_LeftNavigationArrow() throws Exception {

		Homepage.click_FeaturedCategories_Carousel_RightNavigation_Arrow();
		Homepage.click_NavigateFeaturedCategoryCarouselArrow_ToFarLeft();
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp","not_mcafee" })
	public void PT3214_HomePage_Step19_Verify_FeaturedCategories_Clicks_Entire_RightNavigationArrow() throws Exception {

		Homepage.click_NavigateFeaturedCategoryCarouselArrow_ToFarRight();
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp","not_mcafee" })
	public void PT3214_HomePage_Step19_Verify_ClickingFirstItem_NavigatesTo_LandingPage() throws Exception {

		Homepage.verify_Clicking_FeaturedCatCarousel_FirstItem_NavigatesTo_LandingPage();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}
		Browser.closeBrowser();
	}

}
