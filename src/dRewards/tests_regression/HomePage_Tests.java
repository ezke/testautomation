package dRewards.tests_regression;


import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account_History;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HomePage_Tests extends BaseTest {

	String searchText = "shoes";
	private String brandName_ClientSpecific = "Wagan";
	private String brandName_AllClient = "Swarovski";
	private String brandName_ClientSpecificDiff = "Naztech";
	private String brandName_AllClientDiff = "Oakley";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = { "regression_Tests", "all_tests" , "smoke_tests_rc_xuat" })
	public void PT3214_PT4764_Step1_HomePage_VerifyLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
	}

	@Test(groups = { "regression_Tests", "all_tests", "smoke_tests_rc_xuat" })
	public void PT3214_PT4764_Step2_HomePage_VerifyAccountLink() throws Exception {
		Account_History.verify_ClickingAccountLink_NavigatesTo_AccountHistoryPage();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_tvc", "not_citi" , "smoke_tests_rc_xuat"})
	public void PT3214_PT4764_Step3_HomePage_VerifyEnterPromoCodeTextDisplayedOnTextBox() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
			||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))){
			BaseUI.verifyElementAppears(Locator.lookupElement("nav_PromoCodeLink"));
		} else if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			BaseUI.verifyElementAppears(Locator.lookupElement("enterPromoCode_HelpText"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("enterPromoCode_label"));
			BaseUI.verifyElementHasExpectedText("enterPromoCode_label", "Enter Promo Code");
		}		
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_tvc", "not_citi", "smoke_tests_rc_xuat" })
	public void PT3214_PT4764_Step3_HomePage_VerifyEnterPromoCodeSubmitButton() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
			||(ClientDataRetrieval.client_Matches(client_Designation.McAfee))){
			BaseUI.click(Locator.lookupElement("nav_PromoCodeLink"));
			Thread.sleep(500);
		    BaseUI.waitForElementToBeDisplayed("nav_PromoCode_SubmitButton", null, null, 10);
		    BaseUI.click(Locator.lookupElement("promoCodeModal_CloseBtn"));
			Thread.sleep(500);
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("nav_PromoCode_SubmitButton"));
		}
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_tvc", "not_citi", "not_aarp",
			"not_mcafee","not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat"})
	public void PT3214_Step3_HomePage_VerifyEnterPromoCodeHelpIconOrToolTipIcon() throws Exception {
		WebElement promoCodeHelpText = Locator.lookupElement("enterPromoCode_HelpText");
		BaseUI.verifyElementAppears(promoCodeHelpText);
		BaseUI.elementHover(promoCodeHelpText);
	}

	@Test(groups = { "not_citi", "regression_Tests", "all_tests", "not_tvc",
			"not_mcafee","not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat"})
	public void PT3214_Step4_HomePage_Verify_Message_Link() throws Exception {
		Navigation.verify_MessageLink();
	}

	@Test(groups = { "not_citi", "regression_Tests", "all_tests", "not_tvc",
			"not_mcafee","not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat"})
	public void PT3214_Step4_HomePage_clickRemindMeLaterLinkOnMessageDialog() throws Exception {
		Navigation.clickRemindMeLaterLinkOnMessageDialog();
	}

	@Test(groups = {"not_jeepwave", "not_smart", "not_allstate", "not_allstateCash", "not_aarp", "regression_Tests",
			"all_tests", "not_streetwise","not_mcafee", "not_aarpredesign", "not_redesign" , "smoke_tests_rc_xuat"})
	public void PT3214_Step4_HomePage_Verify_Message_Link_DoesNot_Appear() throws Exception {
		// Clients-Citi and TVC does not have Auction,no message center
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("nav_Message_Link"));
	}


	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "not_allstate", "not_allstateCash", "not_aarp",
			"regression_Tests", "all_tests", "not_streetwise", "not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat","not_mcafee"})
	public void PT3214_Step5_HomePage_Verify_Citi_DoesNotHave_Points() throws Exception {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("nav_AmountOfPoints"));
	}

	@Test(groups = { "not_citi", "regression_Tests", "all_tests" , "smoke_tests_rc_xuat"})
	public void PT3214_Step5_PT4764_Step4_HomePage_Verify_PointsAndCashRewards_Appear() throws Exception {
		Navigation.verify_PointsAndCashRewards_Appear(); 

	}

	@Test(groups = { "not_citi", "regression_Tests", "all_tests", "smoke_tests_rc_xuat" })
	public void PT3214_Step5_PT4764_Step4_HomePage_Verify_PointsAndCashRewards_Text_Appears() throws Exception {
		Navigation.verify_PointsAndCashRewards_Text_Appears();

	}

	@Test(groups = { "regression_Tests", "all_tests" , "smoke_tests_rc_xuat"})
	public void PT3214_Step6_PT4764_Step5_HomePage_VerifyCartLinkIsDisplayed() throws Exception {
		Homepage.verifyShoppingCartDisplayed();
	}

	@Test(groups = { "regression_Tests", "all_tests" , "smoke_tests_rc_xuat"})
	public void PT3214_Step6_PT4764_Step5_HomePage_VerifyItemsInCartIncrease() throws Exception {
		Homepage.verifyItemsInCartIncreased();
	}

	@Test(groups = { "regression_Tests", "all_tests" , "smoke_tests_rc_xuat"})
	public void PT3214_Step6_PT4764_Step5_HomePage_VerifyItemsInCartDecrease() throws Exception {
		Homepage.verifyItemsInCartDecreased();		
	}

	@Test(groups = { "not_citi", "not_allstate", "not_allstateCash", "not_aarp", "regression_Tests",
			"all_tests" ,"not_streetwise", "not_redesign", "not_aarpredesign", "smoke_tests_rc_xuat","not_mcafee"}, enabled=false)
	public void PT3214_Step13_HomePage_VerifyCruiseTab() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("home_travel_CruisesTab"));
	}

	//DR team has informed me that this functionallity has been turned off and will be returning at some point in a different form.
	@Test(groups = { "not_citi", "not_allstate", "not_allstateCash", "not_aarp", "regression_Tests",
			"all_tests","not_streetwise", "not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat","not_mcafee"}, enabled=false)
	public void PT3214_Step13_HomePage_ClickCruiseTab_NavigatesTo_LandingPage() throws Exception {
		Homepage.verify_click_CruiseTab_NavigatesTo_LandingPage();
	}

	@Test(groups = { "not_citi", "regression_Tests", "all_tests", "not_aarp" , "not_redesign", "not_aarpredesign",
			"smoke_tests_rc_xuat", "not_jeepwave","not_mcafee"})
	public void PT3214_Step14_HomePage_VerifyCarRentalsTabs() throws Exception {
		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| (ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards))) {
			BaseUI.verifyElementAppears(Locator.lookupElement("home_travel_CarRentalsTab"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("home_travel_CarRentalsLink"));
		}
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_aarp", "not_redesign", "not_aarpredesign" ,
			"smoke_tests_rc_xuat", "not_jeepwave","not_mcafee"})
	public void PT3214_Step14_HomePage_clickCarRentals_NavigatesTo_LandingPage() throws Exception {
		Homepage.verify_click_CarRentals_NavigatesTo_LandingPage();
	}

	
	//Step 20-21 Shop by brand carousel tests
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise", "smoke_tests_rc_xuat" })
	public void PT3214_Step20_PT4764_Step19_Verify_ShopByBrandCarousel_Header() throws Exception {
		Homepage.verify_ShopByBrandCarousel_HeaderTitle();		
	}
	
	
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise", "smoke_tests_rc_xuat" })
	public void PT3214_Step20_PT4764_Step19_Verify_SeeAllBrands_Link() throws Exception {
		Homepage.verify_SeeAllBrandsLink();
	}

	@Test(groups = { "regression_Tests", "all_tests","not_mcafee", "not_redesign", "not_aarpredesign", "smoke_tests_rc_xuat" })
	public void PT3214_Step20_PT4764_Step19_Verify_BrandCarousel_RightNavigationArrow() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_BrandCarousel_RightNavigation"));
	}

	@Test(groups = { "regression_Tests", "all_tests","not_mcafee", "not_redesign", "not_aarpredesign" , "smoke_tests_rc_xuat"})
	public void PT3214_Step20_PT4764_Step19_Verify_BrandCarousel_LeftNavigationArrow() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
		   Homepage.click_BrandCarousel_RightNavigationArrow();
		   BaseUI.verifyElementAppears(Locator.lookupElement("homepage_BrandCarousel_LeftNavigation"));
		} else {
		   BaseUI.verifyElementAppears(Locator.lookupElement("homepage_BrandCarousel_LeftNavigation"));
		}
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc" ,"not_streetwise", "smoke_tests_rc_xuat"})
	public void PT3214_Step20_PT4764_Step19_Click_BrandCarousel_SeeAllBrands_Link() throws Exception {
		Homepage.verifyAllBrandItemsOnBrandCarousel();
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee", "smoke_tests_rc_xuat"})
	public void PT3214_Step20_PT4764_Step19_Click_BrandCarousel_EntireRightNavigation() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Homepage.verify_ShopByBrand_EntireRightNavigation(brandName_ClientSpecificDiff);
		} else {
			Homepage.verify_ShopByBrand_EntireRightNavigation(brandName_AllClientDiff);
		}
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "smoke_tests_rc_xuat","not_mcafee"})
	public void PT3214_Step20_PT4764_Step19_Click_BrandCarousel_EntireLeftNavigation() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Homepage.verify_ShopByBrand_EntireLeftNavigation(brandName_ClientSpecific);
		} else {
			Homepage.verify_ShopByBrand_EntireLeftNavigation(brandName_AllClient);
		}
	}

	
	@Test(groups = { "regression_Tests", "all_tests" , "smoke_tests_rc_xuat"})
	public void PT3214_Step21_PT4764_Step20_ClickAndVerify_BrandCarousel_BrandImages_Displayed() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Homepage.clickAndVerify_BrandsDisplayed_ShopByBrandCarousel_LandingPage(brandName_ClientSpecific);
		} else {
			Homepage.clickAndVerify_BrandsDisplayed_ShopByBrandCarousel_LandingPage(brandName_AllClient);
		}			
	}
	
	
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise", "smoke_tests_rc_xuat" })
	public void PT3214_Step21_PT4764_Step20_HomePage_Verify_ShopByBrands_SeeAllBrands_BrandList() throws Exception {
		Homepage.verify_BrandsDisplayedOnCarousel_Exists_SeeAllBrandsPage();		
	}
	

	//Validate Search textbox is displayed
	@Test(groups = { "regression_Tests", "all_tests", "smoke_tests_rc_xuat" })
	public void PT3214_Step25_PT4764_Step22_HomePage_VerifySearchBox_IsDisplayed() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			BaseUI.click(Locator.lookupElement("homepage_SearchIcon"));
			Thread.sleep(200);
			BaseUI.verifyElementAppears(Locator.lookupElement("homepage_SearchBox"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("homepage_SearchBox"));
		}		
	}
		

	//Validate Search button is displayed
	@Test(groups = { "regression_Tests", "all_tests", "smoke_tests_rc_xuat"})
	public void PT3214_Step25_PT4764_Step22_HomePage_VerifySearchButton_IsDisplayed() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			BaseUI.click(Locator.lookupElement("homepage_SearchIcon"));
			Thread.sleep(200);
			BaseUI.verifyElementAppears(Locator.lookupElement("homepage_SearchButton"));
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("homepage_SearchButton"));
		}
	}
		
	//Validate the Merchandise search string is valid
	@Test(groups = { "regression_Tests", "all_tests", "smoke_tests_rc_xuat"})
	public void PT3214_Step25_PT4764_Step22_HomePage_VerifySearchText_IsValid() throws Exception {
		Homepage.verify_MerchandiseSearchText_Is_Valid(searchText);
	}

	//Validate the Merchandise search string is invalid
	@Test(groups = { "regression_Tests", "all_tests", "smoke_tests_rc_xuat"})
	public void PT3214_Step26_PT4764_Step23_HomePage_VerifySearchText_Is_InValid() throws Exception {
		Homepage.verify_MerchandiseSearchText_Is_Invalid();
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "smoke_tests_rc_xuat"})
	public void PT3243_Step4_PT4764_Step8_verify_HomePage_SiteMedia_Zone1() throws Exception {
		Homepage.verify_HomePage_SiteMedia_Zone1();
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign" , "smoke_tests_rc_xuat"})
	public void PT3243_Step4_PT4764_Step8_verify_HomePage_SiteMedia_Zone2() throws Exception {
		Homepage.verify_HomePage_SiteMedia_Zone2();
	}
	
	
	@Test(groups = {"not_jeepwave", "not_allstate", "regression_Tests", "all_tests", "not_redesign",
            "smoke_tests_rc_xuat", "not_allstateCash" })
	public void PT3243_Step4_PT4764_Step8_verify_HomePage_SiteMedia_Zone3() throws Exception {
		Homepage.verify_HomePage_SiteMedia_Zone3();
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
