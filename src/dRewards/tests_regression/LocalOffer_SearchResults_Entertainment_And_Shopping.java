package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class LocalOffer_SearchResults_Entertainment_And_Shopping extends BaseTest {

	TableData localOfferList = new TableData();
	int incrementLocalOfferNumberToRedeem = 0;
	
	//variable for Entertainment Search and Canada Search
	String city = "Ottawa";
	String state_abbreviation = "ON";
	String first_Category= "Entertainment";
	String location = "Ottawa, ON";
	String state_Fullname = "Ontario";
	
	//variable for Shopping Search
	String zipcode= "33130";
	String second_Category= "Shopping";
	
	String localOfferLogoURL="";
	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_LocalOffers_SearchPage();
	}
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3247_LocalOffer_Step20_Verify_LocalOffer_StoreInfo_Checkbox() throws Exception {

		BaseUI.verifyCheckboxStatus(Locator.lookupElement("localOffer_StoreInfo_Checkbox"), true);
	}
	
	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step9_Verify_LocalOffer_SearchPagerTitle() throws Exception {

		LocalOffers_LandingPage.verify_LocalOffer_Title_Appears();
	}
	
	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step9_Verify_LocalOffer_BreadCrumb_AndLocation_ForEntertainment_Category() throws Exception {

		LocalOffers_LandingPage.click_Canada_RadioButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Fullname, city, first_Category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			String[] breadcrumbArray = { "Home", "Local Deals" };
			Navigation.verify_BreadcrumbList(breadcrumbArray);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_abbreviation, city, first_Category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				String[] breadcrumbArray = { "Home", "Local Offers", first_Category };
				Navigation.verify_BreadcrumbList(breadcrumbArray);
			}
		}
		
		String currentLocation = BaseUI.getTextFromField(Locator.lookupElement("localOffer_CurrentLocation"))
				.replaceAll("Deals near ", "");
		BaseUI.baseStringCompare("localOffer_CurrentLocation", currentLocation, location);
	}
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3247_LocalOffer_Step9_Verify_LocalOffer_SearchResultPage_Zipcode() throws Exception {

		LocalOffers_LandingPage.localOffer_Search_WithZipcode(zipcode, second_Category);
		Navigation.click_SeeLocalOffer_SearchPageButton();
		
		localOfferList= LocalOffers_LandingPage.return_LocalOffer_TableData();
		Integer zipcodeInt = Integer.parseInt(zipcode);
		
		LocalOffers_LandingPage.verify_LocalOffer_Zipcode(localOfferList.data.get(incrementLocalOfferNumberToRedeem), zipcodeInt);
		
	}
	
	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step9_Verify_LocalOffer_SearchResultPage_Logo() throws Exception {

		LocalOffers_LandingPage.click_Canada_RadioButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Fullname, city, first_Category);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_abbreviation, city, first_Category);
		}
		Navigation.click_SeeLocalOffer_SearchPageButton();
		localOfferLogoURL = LocalOffers_LandingPage.return_ImageURL();
		
		LocalOffers_LandingPage.verify_LocalOfferLogo_Accurate(localOfferLogoURL);

	}
	
	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step9_Verify_LocalOffer_BreadCrumb_AndLocation_ForShopping_Category() throws Exception {

		LocalOffers_LandingPage.click_Canada_RadioButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Fullname, city, second_Category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			String[] breadcrumbArray = { "Home", "Local Deals" };
			Navigation.verify_BreadcrumbList(breadcrumbArray);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_abbreviation, city, second_Category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
				String[] breadcrumbArray = { "Home", "Local Offers", second_Category };
				Navigation.verify_BreadcrumbList(breadcrumbArray);
			}
		}
		
		String currentLocation = BaseUI.getTextFromField(Locator.lookupElement("localOffer_CurrentLocation"))
				.replaceAll("Deals near ", "");;
		BaseUI.baseStringCompare("localOffer_CurrentLocation", currentLocation, location);
	}
	
	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step9_Verify_LocalOffer_SearchResultPage_AvailableOffer() throws Exception {

		LocalOffers_LandingPage.click_Canada_RadioButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_Fullname, city, second_Category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			
			localOfferList= LocalOffers_LandingPage.return_LocalOffer_TableData();
			LocalOffers_LandingPage.verifySelectedCategoryIconDisplayed(second_Category);
		} else {
			LocalOffers_LandingPage.localOffer_SearchPage_WithCriteria(state_abbreviation, city, first_Category);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			
			localOfferList= LocalOffers_LandingPage.return_LocalOffer_TableData();
		}

		LocalOffers_LandingPage.verify_AvailableOffer_TextAndNumber(localOfferList.data.get(incrementLocalOfferNumberToRedeem));
	}
	
	@Test( groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step20_Verify_LocalOffer_SearchLocation_Matches_SearchPage() throws Exception {
		LocalOffers_LandingPage.click_Canada_RadioButton();
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.verify_SearchResultPageLocation_Matches_SearchPage(state_Fullname, city, second_Category, state_abbreviation);
		} else {
			LocalOffers_LandingPage.verify_SearchResultPageLocation_Matches_SearchPage(state_abbreviation, city, first_Category, state_abbreviation);
		}
	}
	
	@Test( groups = { "regression_Tests", "all_tests" })
	public void PT3247_LocalOffer_Step20_Verify_LocalOffer_SearchZipcode_Matches_SearchPage() throws Exception {

		LocalOffers_LandingPage.verify_SearchResultZipcode_Matches_SearchPage(zipcode, second_Category);
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Home_viaLogo();
		}
		Navigation.navigate_LocalOffers_SearchPage();
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
