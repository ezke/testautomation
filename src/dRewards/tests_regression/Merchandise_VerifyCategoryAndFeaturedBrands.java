package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Merchandise_VerifyCategoryAndFeaturedBrands extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Can either navigate to Merchandise page and from there navigate to
		// Tech page.
		Navigation.navigate_Merchandise();
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "Links")
	public Object[][] createData() throws Exception {

		ArrayList<String> categoryList = Merchandise.return_CategoryList();
		ArrayList<String> featuredBrandsList = Merchandise.return_FeaturedBrandList();

		Integer listCount;
		Object[][] linksToNavigate;
		
		if(ClientDataRetrieval.isRedesignClient()) {
			ArrayList<String> featuredShopsList = Merchandise.return_FeaturedShopsList();
			listCount = categoryList.size() + featuredBrandsList.size() + featuredShopsList.size();
			linksToNavigate = new Object[listCount][2];

			BaseUI.verify_true_AndLog(listCount > 0, "Found Links", "Did not find links");

			for (int i = 0; i < listCount;) {
				for (String category : categoryList) {
					linksToNavigate[i][0] = "Shop By Category";
					linksToNavigate[i][1] = category;
					i++;
				}
				for (String brand : featuredBrandsList) {
					linksToNavigate[i][0] = "Featured Brands";
					linksToNavigate[i][1] = brand;
					i++;
				}
				for (String category : featuredShopsList) {
					linksToNavigate[i][0] = "Featured Shops";
					linksToNavigate[i][1] = category;
					i++;
				}
			}
		} else {
			listCount = categoryList.size() + featuredBrandsList.size();
			linksToNavigate = new Object[listCount][2];

			BaseUI.verify_true_AndLog(listCount > 0, "Found Links", "Did not find links");

			for (int i = 0; i < listCount;) {
				for (String category : categoryList) {
					linksToNavigate[i][0] = "Category";
					linksToNavigate[i][1] = category;
					i++;
				}
				for (String brand : featuredBrandsList) {
					linksToNavigate[i][0] = "Featured Brands";
					linksToNavigate[i][1] = brand;
					i++;
				}
			}
		}
		return linksToNavigate;
	}

	@Test(priority = 5, dataProvider = "Links")
	public void PT2756_Step3_PT4793_Step2_ValidateLeftNavigationPanelLinks(String category, String linkText) throws Exception {
		Navigation.navigate_Merchandise();
		
		if (!ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				&& !ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				&& !ClientDataRetrieval.client_Matches(client_Designation.Citi) && linkText.equals("Daily Deals")
				&& !ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Merchandise.verify_Merchandise_Category_Link(category, linkText, "Daily Deal");
		} else if (linkText.equals("Brands A-Z")) {
			if (ClientDataRetrieval.isRedesignClient()) {
				Merchandise.verify_Merchandise_Category_Link(category, linkText, "Brands");
			} else {
				Merchandise.verify_Merchandise_Category_Link(category, linkText, "All Brands");
			}
		} else if (!ClientDataRetrieval.client_Matches(client_Designation.SmartRewards) 
				&& ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards) 
				&& linkText.equals("Featured Brands")) {
			Merchandise.verify_Merchandise_Category_Link(category, linkText, "All Brands");
		} else {
			Merchandise.verify_Merchandise_Category_Link(category, linkText, linkText);
		}

	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step8_Category_MinusLink() throws Exception {
		ArrayList<String> categoryList = Merchandise.return_CategoryList();

		Merchandise.minimize_Category();
		for (String category : categoryList) {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("merch_CategoryLink_ByText", category, null));
		}

	}
	
	@Test(priority = 2, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step8_Category_MaximizeLink() throws Exception {
		ArrayList<String> categoryList = Merchandise.return_CategoryList();
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");
		
		Merchandise.maximize_Category();
		for (String category : categoryList) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_CategoryLink_ByText", category, null));
		}
	}
	
	@Test(priority = 3, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_streetwise" ,"not_mcafee"})
	public void PT3243_Step8_FeaturedCategories_MaximizeLink() throws Exception {
		ArrayList<String> featuredBrandsList = Merchandise.return_FeaturedBrandList();
		BaseUI.verify_true_AndLog(featuredBrandsList.size() > 0, "Found Links", "Did not find links");
		
		Merchandise.maximize_FeaturedBrands();
		for (String brand : featuredBrandsList) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_FeaturedBrands_ByText", brand, null));
		}
	}

	@Test(priority = 4, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_streetwise" ,"not_mcafee"})
	public void PT3243_Step8_FeaturedCategories_MinimizeLink() throws Exception {
		ArrayList<String> featuredBrandsList = Merchandise.return_FeaturedBrandList();

		Merchandise.minimize_FeaturedBrands();
		for (String brand : featuredBrandsList) {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("merch_FeaturedBrands_ByText", brand, null));
		}
	}


	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
