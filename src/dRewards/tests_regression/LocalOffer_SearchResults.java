package dRewards.tests_regression;

import java.text.MessageFormat;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class LocalOffer_SearchResults extends BaseTest {

	// variable for Entertainment Search and Canada Search
	String address = "861 NW 51st ST";
	String city = "Boca Raton";
	String state = "FL";
	String category = "Dining";

	String sort_TitleAtoZ = "Title A to Z";
	String sort_TitleZtoA = "Title Z to A";
	String range = "5 Miles";
	String itemPerPage = "15";

	//Sal's option does not exist for all legacy clients,
	//So updated to Dean option after taking to DR team.
	String searchWithResults_ValidSearch_Legacy = "Dean";
	String searchWithResults_ExpectedValue_Legacy = "Dean Anthony's";
	String searchWithResults_ValidSearch_Redesign = "Big Joes";
	String searchWithResults_ExpectedValue_Redesign = "Big Joes Burgers and Wings";
	String searchWithResults_InvalidSearch = "akjh@#$123";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_LocalOffers_SearchPage();
	}

	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_Step21_SearchLocalOffer_Dining_AddressList_Sorted_Ascending_ByDistance() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria("Florida", city, category, address);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
		}
		Navigation.click_SeeLocalOffer_SearchPageButton();

		LocalOffers_LandingPage.verify_Distance_FromLocalOffer_Ascending();
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3247_Step21_SearchLocalOffer_Sort_TitleAtoZ() throws Exception {
			
		LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
		Navigation.click_SeeLocalOffer_SearchPageButton();

		LocalOffers_LandingPage.sort(sort_TitleAtoZ);

		TableData sortList = LocalOffers_LandingPage.return_LocalOffer_TableData();
		sortList.verify_Column_Sorted_Ascending("localOfferName");		
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3247_Step21_SearchLocalOffer_Sort_TitleZtoA() throws Exception {

		LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
		Navigation.click_SeeLocalOffer_SearchPageButton();

		LocalOffers_LandingPage.sort(sort_TitleZtoA);

		TableData sortList = LocalOffers_LandingPage.return_LocalOffer_TableData();
		sortList.verify_Column_Sorted_Descending("localOfferName");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3247_Step21_SearchLocalOffer_Verify_Range_5Miles() throws Exception {

		LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
		Navigation.click_SeeLocalOffer_SearchPageButton();
		TableData previousResultList = LocalOffers_LandingPage.return_LocalOffer_TableData();
		Integer previousNumberEntries = LocalOffers_LandingPage.return_ResultsCount();

		LocalOffers_LandingPage.range(range);

		TableData currentResultList = LocalOffers_LandingPage.return_LocalOffer_TableData();
		Integer currentNumberEntries = LocalOffers_LandingPage.return_ResultsCount();

		BaseUI.verify_true_AndLog(previousResultList.data.size() != currentResultList.data.size(),
				MessageFormat.format("Previous Result List {0}does NOT match Current Result List{1}.", previousResultList.data, currentResultList.data),
				MessageFormat.format("Previous Result List {0} does match Current Result List {1}.",previousResultList.data, currentResultList.data));
		BaseUI.verify_true_AndLog(previousNumberEntries != currentNumberEntries, "Entries counts did NOT match.",
				"Entries counts matched.");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3247_Step21_SearchLocalOffer_Verify_ItemPerPage_15PerPage() throws Exception {

		LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
		Navigation.click_SeeLocalOffer_SearchPageButton();
		TableData previousResultList = LocalOffers_LandingPage.return_LocalOffer_TableData();
		Integer previousShowingItemsCount = LocalOffers_LandingPage.return_ShowingItemsCount();

		LocalOffers_LandingPage.itemPerPage(itemPerPage);

		TableData currentResultList = LocalOffers_LandingPage.return_LocalOffer_TableData();
		Integer currentShowingItemsCount = LocalOffers_LandingPage.return_ShowingItemsCount();

		BaseUI.verify_true_AndLog(previousResultList.data.size() != currentResultList.data.size(),
				MessageFormat.format("Previous Result List {0}does NOT match Current Result List{1}.", previousResultList.data, currentResultList.data),
				MessageFormat.format("Previous Result List {0} does match Current Result List {1}.", previousResultList.data, currentResultList.data));
		BaseUI.verify_true_AndLog(previousShowingItemsCount != currentShowingItemsCount,
				"Showing Items counts did NOT match.", "Showing Items counts matched.");
		Integer itemPerPageInt = Integer.parseInt(itemPerPage);
		BaseUI.verify_true_AndLog(currentShowingItemsCount.equals(itemPerPageInt),
				MessageFormat.format("Showing Items {0} matches item per page {1}.", currentShowingItemsCount,
						itemPerPageInt),
				MessageFormat.format("Showing Items {0} does Not matches item per page{1}.", currentShowingItemsCount,
						itemPerPageInt));
	}
	

	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_Step21_PT4538_Step5_LocalOfferResult_Verify_SearchWithInResults_ValidSearch() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria("Florida", city, category, address);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			
			//The search value used for RC environment is not available in XUAT environment.
			//So going with below solution to validate this test successfully in both RC and XUAT
			String searchOption = ClientDataRetrieval.environment
					.equals("RC") ? searchWithResults_ValidSearch_Redesign : searchWithResults_ValidSearch_Legacy;
			String expectedResultValue = searchOption
					.equals(searchWithResults_ValidSearch_Redesign) ? searchWithResults_ExpectedValue_Redesign : searchWithResults_ExpectedValue_Legacy;
			LocalOffers_LandingPage.verifySearchListIsValidSearchResult(searchOption, 
					category, expectedResultValue);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
			Navigation.click_SeeLocalOffer_SearchPageButton();
			LocalOffers_LandingPage.verifySearchListIsValidSearchResult(searchWithResults_ValidSearch_Legacy, 
					category, searchWithResults_ExpectedValue_Legacy);
		}		
	}
	

	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3247_Step21_PT4538_Step5_LocalOfferResult_Verify_SearchWithInResults_InvalidSearch() throws Exception {

		String expectedErrorText = "No results found. Try searching a specific food, activity, or establishment.";
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria("Florida", city, category, address);
		} else {
			LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria(state, city, category, address);
		}
		
		Navigation.click_SeeLocalOffer_SearchPageButton();

		LocalOffers_LandingPage.launch_SearchWithinResult(searchWithResults_InvalidSearch, category);
		LocalOffers_LandingPage.verify_SearchWithinResults_Error(searchWithResults_InvalidSearch, expectedErrorText);
	}
	

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Home_viaLogo();
		}
		Navigation.navigate_LocalOffers_SearchPage();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}

}
