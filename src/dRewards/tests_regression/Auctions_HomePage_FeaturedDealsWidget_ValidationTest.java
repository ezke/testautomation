package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Auction_SearchSubAuction_Admin;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Auctions_HomePage_FeaturedDealsWidget_ValidationTest extends BaseTest {

    private String status = "Active";
    private String firstWidget = "1";
    private String secondWidget = "2";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.searchActiveSweepstakesForGivenProgramID(status,
                Client_Info_Admin.return_ClientId(), Client_Info_Admin.return_ProgramIDs());
        String sweepstakes_SearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("searchSweepstakes_SearchResult_NoRecordsMsg"));

        Auction_SearchSubAuction_Admin.searchActiveAuctionsForGivenProgramID(status, Client_Info_Admin.return_ClientId());
        String auctions_SearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("searchSubAuction_SearchResult_NoRecordsMsg"));

        if(!(sweepstakes_SearchResult.equals("No results found for search criteria you specified"))
                && !(auctions_SearchResult.equals("No results found for search criteria you specified"))) {
            BaseUI.log_Status("HomePage will Not display Sweepstakes and Auctions widget in 2nd and 3rd section. But we will see Featured Deal Widget");
        } else {
            BaseUI.log_AndFail("HomePage will display Sweepstakes and Auctions widget in 2nd and 3rd Section");
        }

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifyFirst_FeaturedDealWidget_Image() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_Image", firstWidget, null));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifyFirst_FeaturedDealWidget_ProductName() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_ProductName", firstWidget, null));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifyFirst_FeaturedDealWidget_SavingPercentage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_SavingsPercentage", firstWidget, null));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifyFirst_FeaturedDealWidget_FeaturedDealHeader() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homePage_FeaturedDealWidget_Title", firstWidget, null));
        BaseUI.verifyElementHasExpectedText("homePage_FeaturedDealWidget_Title", "Featured Deal");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifySecond_FeaturedDealWidget_Image() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_Image", secondWidget, null));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifySecond_FeaturedDealWidget_ProductName() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_ProductName", secondWidget, null));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifySecond_FeaturedDealWidget_SavingPercentage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedDealWidget_SavingsPercentage", secondWidget, null));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_VerifySecond_FeaturedDealWidget_FeaturedDealHeader() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homePage_FeaturedDealWidget_Title", secondWidget, null));
        BaseUI.verifyElementHasExpectedText("homePage_FeaturedDealWidget_Title", "Featured Deal");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_ClickFirstFeaturedDealWidget_VerifyLandingPage_Details() throws Exception {
        Homepage.clickSelectedFeaturedDealWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails(firstWidget);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step4_HomePage_ClickSecondFeaturedDealWidget_VerifyLandingPage_Details() throws Exception {
        Homepage.clickSelectedFeaturedDealWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails(secondWidget);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.navigate_Home_viaHomeLink();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
