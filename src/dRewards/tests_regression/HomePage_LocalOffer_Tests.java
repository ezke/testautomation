package dRewards.tests_regression;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.ConsumerService;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class HomePage_LocalOffer_Tests extends BaseTest {

	TableData localOfferList = new TableData();
	int incrementLocalOfferNumberToRedeem = 0;
	
	String adminUserAddressText;
	String adminUserCityText;
	String adminUserState;
	
	String address = "401 E Las Olas Blvd.";
	String city = "Fort Lauderdale";
	String state = "FL";
	String zipCode ="33130";
	
	String location = "Fort Lauderdale, FL";
	String localOfferLogoURL="";
	

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		Navigation_Admin.navigate_ExpandCategory("Consumer Service");
		ConsumerService.clickUserID_NavigateTo_EditConsumerPage();
		adminUserAddressText = BaseUI.getTextFromInputBox(Locator.lookupElement("editConsumer_Address_TextBox"));
		adminUserCityText = BaseUI.getTextFromInputBox(Locator.lookupElement("editConsumer_City_TextBox"));
		adminUserState = BaseUI.getSelectedOptionFromDropdown("editConsumer_State_Dropdown");
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise" })
	public void PT3214_Step10_PT4780_Step1_HomePage_Verify_LocalOffer_Appears() {
		Homepage.verify_LocalOffer_Appears();
	}
		
	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise" })
	public void PT3214_Step10_PT4780_Step1_HomePage_Verify_LocalOffer_BannerImgAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_LocalOffer_BannerImg"));
	}

	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step10_HomePage_Verify_LocalOffer_Default_Address() {
		String userAddressText = BaseUI
				.getTextFromInputBox(Locator.lookupElement("homepage_LocalOffer_Address_Textbox"));
		String userCityText = BaseUI.getTextFromInputBox(Locator.lookupElement("homepage_LocalOffer_City_Textbox"));
		String userState = BaseUI.getSelectedOptionFromDropdown("homepage_LocalOffer_State_Dropdown");

		BaseUI.baseStringCompare("Address", adminUserAddressText, userAddressText);
		BaseUI.baseStringCompare("City", adminUserCityText, userCityText);
		BaseUI.baseStringCompare("State", adminUserState, userState);
	}

	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_aarp", "not_streetwise", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step10_HomePage_Enter_ZipCode_Verify_AddressFields_Blank() throws Exception {
		String zipCode = "68116";
		Homepage.verify_LocalOffer_AddressFields_Blank(zipCode);
	}

	@Test(groups = { "not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise"})
	public void PT3214_Step10_PT4780_Step2_HomePage_Enter_ZipCode_And_ClickSeeLocalOffers_Verify_NavigatesTo_NextPage() throws Exception {
		String zipCode = "68116";
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Homepage.verify_LocalOffer_AddressFields_Blank(zipCode);
			Navigation.click_SeeLocalOffer_Button();
			ArrayList<String> localOfferList = LocalOffers_LandingPage.return_LocalOfferTitleList();
			BaseUI.verify_true_AndLog(localOfferList.size() > 0, "Found LocalOffers list on LocalOffers Landinpage.",
					"Could not find LocalOffers list on LocalOffers Landinpage.");
		} else {
			if (ClientDataRetrieval.isRedesignClient()) {
				BaseUI.enterText_IntoInputBox(Locator.lookupElement("homepage_LocalOffer_Textbox"), zipCode);
			} else {
				Homepage.verify_LocalOffer_AddressFields_Blank(zipCode);
			}
			
			Navigation.click_SeeLocalOffer_Button();
			LocalOffers_LandingPage.verify_LocalOffer_Title_Appears();
		}
	}
	

	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise", 
			"not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step10_HomePage_NoAddress_Verify_Error() throws Exception {
		Homepage.verify_LocalOffer_Error();
	}
	
	
	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise"})
	public void PT3247_Step19_PT4780_Step2_HomePage_LocalOffer_Verify_SearchResultPageLocation_Matches_Homepage() throws Exception {
		Homepage.verify_SearchResultPageLocation_Matches_HomePage(address, city, state);
	}
	
	
	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise" })
	public void PT3247_Step19_PT4780_Step2_HomePage_LocalOffer_Verify_BreadCrumb_AndLocation() throws Exception {
		String actualLocation = location;
		Homepage.localOffer_Search_WithCriteria(address, city, state);
		Navigation.click_SeeLocalOffer_Button();
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			if (ClientDataRetrieval.isRedesignClient()) {
				String[] breadcrumbArray = { "Home", "Local Deals"};
				Navigation.verify_BreadcrumbList(breadcrumbArray);
				String location_Abbreviation = ClientDataRetrieval.environment.equals("RC") ? "Ft." : "Ft";
				actualLocation = "Deals near "+ location.replaceAll("Fort", location_Abbreviation);
			} else {
				String[] breadcrumbArray = { "Home", "Local Offers"};
				Navigation.verify_BreadcrumbList(breadcrumbArray);
			}
		} 	
		String currentLocation = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_CurrentLocation"));
		BaseUI.baseStringCompare("localOffer_CurrentLocation", currentLocation, actualLocation);
	}
	
	
	@Test(groups = { "not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3247_Step19_HomePage_LocalOffer_Verify_SearchResultPage_Logo() throws Exception {
		Homepage.localOffer_Search_WithCriteria(address, city, state);
		Navigation.click_SeeLocalOffer_Button();
		
		localOfferLogoURL = LocalOffers_LandingPage.return_ImageURL();
		
		LocalOffers_LandingPage.verify_LocalOfferLogo_Accurate(localOfferLogoURL);
	}
	
	
	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise" })
	public void PT3247_Step19_PT4780_Step2_HomePage_LocalOffer_Verify_SearchResultPage_AvailableOffer() throws Exception {
		Homepage.localOffer_Search_WithCriteria(address, city, state);
		Navigation.click_SeeLocalOffer_Button();
		
		localOfferList= LocalOffers_LandingPage.return_LocalOffer_TableData();

		LocalOffers_LandingPage.verify_AvailableOffer_TextAndNumber(localOfferList.data.get(incrementLocalOfferNumberToRedeem));
	}
	
	
	@Test(groups = {"not_jeepwave", "not_smart", "not_tvc", "regression_Tests", "all_tests", "not_streetwise", 
			"not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3247_Step19_HomePage_LocalOffer_Verify_SearchResultPage_ZipCode() throws Exception {
		Homepage.localOffer_Search_WithZipcode(zipCode);
		Navigation.click_SeeLocalOffer_Button();
		
		localOfferList= LocalOffers_LandingPage.return_LocalOffer_TableData();
		
		Integer zipcodeInt = Integer.parseInt(zipCode);

		LocalOffers_LandingPage.verify_LocalOffer_Zipcode(localOfferList.data.get(incrementLocalOfferNumberToRedeem), zipcodeInt);
	}
	
	
	@Test(groups = {"not_jeepwave", "not_citi", "not_allstate", "not_allstateCash", "not_tvc", "not_smart", "regression_Tests",
			"all_tests", "not_streetwise", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3247_Step19_HomePage_LocalOfferImage_NavigatesTo_LocalOfferPage() throws Exception {
		
		Homepage.click_LocalOffer_ImageLink();
		LocalOffers_LandingPage.verify_LocalOffer_Title_Appears();
	}


	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();		
	}
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
