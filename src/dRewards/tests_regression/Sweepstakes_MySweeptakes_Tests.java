package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Sweepstakes_MySweeptakes_Tests extends BaseTest {

	String mySweepstakesOnAccountPage;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Account_History();
			mySweepstakesOnAccountPage = BaseUI.getTextFromField(Locator.lookupElement("mySweepstakes_AccountLink"));
		}
		Navigation.navigate_Sweepstakes_MySweepstakes();
	}
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi" })
	public void PT30_PT3998_Step3_VerifyMySweepstakes_Breadcrumb() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			String[] breadcrumb = {"Home", "My Rewards", "My Sweepstakes"};
			Navigation.verify_BreadcrumbList(breadcrumb);
		} else {
			String[] breadcrumb = {"Home", "Sweepstakes | History"};
			Navigation.verify_BreadcrumbList(breadcrumb);
		}
	}

	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi" })
	public void PT30_PT3998_Step3_VerifyMySweepstakes_Header() throws Exception {

		Sweepstakes.verifyMySweepstakes_Header();
	}
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi" })
	public void PT30_PT3998_Step3_VerifyMySweepstakes_CurrentSweepstakes_Header() throws Exception {

		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakesTab"));
			if(mySweepstakesOnAccountPage.equals("Closed Sweepstakes")) {
				BaseUI.verifyElementHasExpectedText("mySweepstakes_CurrentSweepstakesTab", "Closed Sweepstakes");
			} else {
				BaseUI.verifyElementHasExpectedText("mySweepstakes_CurrentSweepstakesTab", "Live Sweepstakes");
			}
		} else {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakesTab"));
			BaseUI.verifyElementHasExpectedText("mySweepstakes_CurrentSweepstakesTab", "Current Sweepstakes");
		}
	}
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi" })
	public void PT30_PT3998_Step3_VerifyMySweepstakes_Sweepstakes_TableRow() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Sweepstakes.clickMySweepstakes_tileName();
			//will veryfy submit button
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton"));
			BaseUI.verifyElementEnabled(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton"));
		}

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakesSub_Tab_Table"));
	}	

	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi" , "not_redesign", "not_aarpredesign","not_mcafee"})
	public void PT30_Step3_VerifyMySweepstakes_Sweepstakes_ViewSweeptakes_Button() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton"));
		BaseUI.verifyElementEnabled(Locator.lookupRequiredElement("mySweepstakes_CurrentSweepstakes_ViewSweepstakesButton"));
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
