package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;

//@Test(groups = { "not_IE" })
public class HomePage_AuctionWidget_Test extends BaseTest {

	WebDriver originalDriver;
	WebDriver secondBidderDriver;
	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		originalDriver = Browser.driver;
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		if (!BaseUI.elementExists("Auction_Header", null, null)) {
			Homepage.CreateNewAuction_HomePage_Widget();
		}
	}
	
	@Test(priority = 10, groups = { "not_jeepwave", "not_tvc", "not_citi", "not_smart", "regression_Tests", 
			"all_tests" , "not_IE", "not_streetwise","not_mcafee"})
	public void PT3214_Step9_PT4779_Step1_HomePage_VerifyAuctionWidget() throws Exception {
		
		Browser.driver = originalDriver;
		if (ClientDataRetrieval.isRedesignClient()) {
			Browser.driver.navigate().refresh();
			Thread.sleep(2000);
			BaseUI.waitForElementToBeDisplayed("nav_HomeLink", null, null, 50);
		}
		else {
			Navigation.navigate_Home_viaLogo();
		}
		Homepage.verify_Auction_Widget_Section();
		Homepage.verify_Auction_Widget_EndIn();
		Homepage.verify_Auction_Widget_ClickSeeDetailsLink();
		Navigation.close_AuctionBidPopup_IfAppears();
		
		if (!ClientDataRetrieval.isRedesignClient()){
			Homepage.verify_Auction_Widget_ClickSeeDetails();
		}		
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondBidderDriver != null) {
			Browser.driver = secondBidderDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
	}
}
