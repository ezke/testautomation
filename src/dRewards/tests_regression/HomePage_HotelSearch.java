package dRewards.tests_regression;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Homepage;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.HotelsAndCondos_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class HomePage_HotelSearch extends BaseTest {

	// testchange
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);

		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

	}

	// AARP, AllState, AllState Cash
	// Not Smart Rewards, Citi, TVC
	// Steps: 1, 2
	@Test(groups = {"not_citi", "not_tvc", "not_smart", "regression_Tests", "all_tests", "not_aarpredesign","not_mcafee" })
	public void PT3135_Step1_Navigate_Homepage_SearchForHotel() throws Exception {
		String location = "Orlando, FL";
		String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
		String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
		String numberOfAdults = "2";
		String numberOfChildren = "2";
		String ageOfChildren = "5";

		Homepage.verify_HotelSearch_ElementsPresent();
		Homepage.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren, ageOfChildren);
		HotelsAndCondos_SearchResults.validate_SearchResults(location, fromDate, toDate, numberOfAdults,
				numberOfChildren, ageOfChildren);
	}

	@Test(groups = { "not_citi", "regression_Tests", "all_tests" })
	public void PT3135_Step6_PT4785_Step1_Navigate_Travel_HomePage_SearchForHotel() throws Exception {
		Navigation.navigate_Hotel_Landingpage();
		HotelsAndCondos.verify_HotelSearch_ElementsPresent();
	}

	// Not citi deals
	@Test(groups = { "not_citi", "regression_Tests", "all_tests", "not_aarpredesign","not_mcafee" })
	public void PT3135_Step4_Homepage_Search_WithoutSelecting_Criteria() throws Exception {
		Homepage.verify_DestinationRequired_Error();
	}

	// AARP only.  AARP redesign just has a Travel Image that you click, it takes you to Hotel Search page.
	@Test(groups={"not_jeepwave", "not_citi", "not_allstate", "not_allstateCash", "not_tvc", 
			"not_smart", "regression_Tests", "all_tests", "not_streetwise"})
	public void PT3135_PT4785_Step1_Navigate_Homepage_TravelImage_NavigatesToHotelSearchPage() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("homepage_TravelHotel_Link"));
		Homepage.click_Travel_ImageLink();
		HotelsAndCondos.verify_HotelSearch_ElementsPresent();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
		if (!ClientDataRetrieval.client_Matches(client_Designation.Citi) 
				&& !ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			BaseUI.enterText(Locator.lookupElement("homepage_hotelSearchInput_Textbox"), "");
		}
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
