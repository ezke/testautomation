package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.List;

public class MerchandiseLandingPage_TopMediaCenter extends BaseTest {

    List<WebElement> allFeaturedBrand;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Merchandise();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4795_Step1_VerifyMerchandiseHeader() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchandisePage_Header"));
        BaseUI.verifyElementHasExpectedText("merchandisePage_Header", "Merchandise");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4795_Step2_VerifyMerchandise_Featured_Shop_And_CategoryCarousel() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4795_Step2_VerifyMerchandise_Featured_Shop_And_CategoryCarousel_LeftNavArrow() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel_LeftNavArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4795_Step2_VerifyMerchandise_Featured_Shop_And_CategoryCarousel_RightNavArrow() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel_RightNavArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step2_VerifyMerchandise_ShopMostPopularCarousel_MultipleSlide() {
        allFeaturedBrand = Locator
                .lookup_multipleElements("merchLandingPage_Featured_Shop_CategoryCarousel_Count", null, null);
        BaseUI.verify_true_AndLog(allFeaturedBrand.size() > 0, "Featured Brand Carousel displays Featured brand images",
                "Featured Brand Carousel DOES NOT display featured brand images");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4795_Step2_VerifyMerchandise_Featured_Shop_And_CategoryCarousel_EntireRightNavigation() {
        for (int i=0; i< allFeaturedBrand.size(); i++) {
            BaseUI.click(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel_RightNavArrow"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel_Header"));
        }
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4795_Step2_VerifyMerchandise_Featured_Shop_And_CategoryCarousel_EntireLeftNavigation() {
        for (int i=0; i< allFeaturedBrand.size(); i++) {
            BaseUI.click(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel_LeftNavArrow"));
            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_Featured_Shop_CategoryCarousel_SubHeader"));
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {

        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
