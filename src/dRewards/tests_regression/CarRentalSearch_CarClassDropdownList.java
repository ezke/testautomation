package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.Browser;
import utils.ResultWriter;

public class CarRentalSearch_CarClassDropdownList extends BaseTest {


	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_CarRental_Page();
	}
	
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step7_CarRentalSearch_LandingPage_VerifyCarClass_Dropdown() throws Exception {
				
		CarRentalSearch.verify_CarClass_Dropdown();		
    }
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}//End of class
