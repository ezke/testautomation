package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GiftCards_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class GiftCard_ValidateLinks extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_GiftCard_All();
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "Entertainment")
	public Object[][] createData_Entertainment() throws Exception {
		String categoryName;
		//if(ClientDataRetrieval.isLegacyClient() || ClientDataRetrieval.isRedesignClient()) {
			categoryName = "Movie Tickets";
//		} else {
//			categoryName = "Entertainment";
//		}
		ArrayList<String> entertainmentLinks = new ArrayList<String>(GiftCards_SearchResults.return_List_OfLinks_ByCategoryText(categoryName));

		return return_linkTextAs_ObjectArray(entertainmentLinks);
	}
	
	@DataProvider(name = "Dining")
	public Object[][] createData_Dining() throws Exception {
		String categoryName ="Dining";		
		ArrayList<String> diningLinks = new ArrayList<String>(GiftCards_SearchResults.return_List_OfLinks_ByCategoryText(categoryName));

		return return_linkTextAs_ObjectArray(diningLinks);
	}
	
	@DataProvider(name = "Shopping")
	public Object[][] createData_Shopping() throws Exception {
		String categoryName ="Shopping";		
		ArrayList<String> shoppingLinks = new ArrayList<String>(GiftCards_SearchResults.return_List_OfLinks_ByCategoryText(categoryName));

		return return_linkTextAs_ObjectArray(shoppingLinks);
	}
	
	@DataProvider(name = "Travel")
	public Object[][] createData_Travel() throws Exception {
		String categoryName ="Travel";		
		ArrayList<String> travelLinks = new ArrayList<String>(GiftCards_SearchResults.return_List_OfLinks_ByCategoryText(categoryName));

		return return_linkTextAs_ObjectArray(travelLinks);
	}
	
	@DataProvider(name = "e-Gift Cards")
	public Object[][] createData_eGiftCards() throws Exception {
		String categoryName ="e-Gift Cards";
		ArrayList<String> eGiftCardsLinks = new ArrayList<String>(GiftCards_SearchResults.return_List_OfLinks_ByCategoryText(categoryName));

		return return_linkTextAs_ObjectArray(eGiftCardsLinks);
	}

	@DataProvider(name = "Special Offers")
	public Object[][] createData_SpecialOffers() throws Exception {
		String categoryName ="Special Offers";
		ArrayList<String> specialOffersLinks = new ArrayList<String>(GiftCards_SearchResults.return_List_OfLinks_ByCategoryText(categoryName));

		return return_linkTextAs_ObjectArray(specialOffersLinks);
	}
	
	public Object[][] return_linkTextAs_ObjectArray(ArrayList<String> dataSet) throws Exception {
		BaseUI.verify_true_AndLog(dataSet.size() > 0, "Found links", "Did NOT find links.");

		Object[][] linksToNavigate = new Object[dataSet.size()][1];
		int objectIndex = 0;
		for (String linkItem : dataSet) {
			linksToNavigate[objectIndex][0] = linkItem;
			objectIndex++;
		}
		
		Navigation.navigate_GiftCard_All();
		return linksToNavigate;
	}
	
	@Test(dataProvider = "Entertainment", alwaysRun = true, groups = { "regression_Tests", "all_tests" }, priority = 55)
	public void PT3242_Step4_PT3666_Step8_Entertainment(String linkText) throws Exception {
		String categoryName;
		if(ClientDataRetrieval.isLegacyClient() || ClientDataRetrieval.isRedesignClient()) {
			categoryName = "Movie Tickets";
		} else {
			categoryName = "Entertainment";
		}
		String link = linkText.substring(0, linkText.indexOf("("))
				.replace("\u203A", "").trim();
		Integer numberOfResults = Integer.parseInt(linkText.split("\\(")[1].replace(")", "").trim());

		GiftCards_SearchResults.clickAndVerifyEachLink_ForEachGiftCardSubcategory(categoryName, link, numberOfResults);
	}

	@Test(dataProvider = "Dining", alwaysRun = true, groups = { "regression_Tests", "all_tests" }, priority = 20)
	public void PT3242_Step4_PT3666_Step7_Dining(String linkText) throws Exception {
		String categoryName = "Dining";

		String link = linkText.substring(0, linkText.indexOf("("))
				.trim().replace("\u203A", "");
		Integer numberOfResults = Integer.parseInt(linkText.split("\\(")[1].replace(")", "").trim());

		GiftCards_SearchResults.clickAndVerifyEachLink_ForEachGiftCardSubcategory(categoryName, link, numberOfResults);
	}
	
	@Test(dataProvider = "Shopping", alwaysRun = true, groups = { "regression_Tests", "all_tests" }, priority = 10)
	public void PT3242_Step4_PT3666_Step9_Shopping(String linkText) throws Exception {
		String categoryName = "Shopping";

		String link = linkText.substring(0, linkText.indexOf("("))
				.trim().replace("\u203A", "").replace("› ", "");
		Integer numberOfResults = Integer.parseInt(linkText.split("\\(")[1].replace(")", "").trim());

		GiftCards_SearchResults.clickAndVerifyEachLink_ForEachGiftCardSubcategory(categoryName, link, numberOfResults);
	}
	
	@Test(dataProvider = "Travel", alwaysRun = true, groups = { "regression_Tests", "all_tests" }, priority = 60)
	public void PT3242_Step4_PT3666_Step10_Travel(String linkText) throws Exception {
		String categoryName = "Travel";

		String link = linkText.substring(0, linkText.indexOf("("))
				.trim().replace("\u203A", "");
		Integer numberOfResults = Integer.parseInt(linkText.split("\\(")[1].replace(")", "").trim());

		GiftCards_SearchResults.clickAndVerifyEachLink_ForEachGiftCardSubcategory(categoryName, link, numberOfResults);
	}
	
	@Test(dataProvider = "e-Gift Cards", alwaysRun = true, groups = {"not_jeepwave", "regression_Tests", "all_tests",
			"not_smart", "not_tvc", "not_aarp", "not_streetwise", "not_redesign", "not_aarpredesign","not_mcafee" }, priority = 40)
	public void PT3242_Step4_eGiftCards(String linkText) throws Exception {
		String categoryName = "e-Gift Cards";

		String link = linkText.substring(0, linkText.indexOf("("))
				.trim().replace("\u203A", "");
		Integer numberOfResults = Integer.parseInt(linkText.split("\\(")[1].replace(")", "").trim());

		GiftCards_SearchResults.clickAndVerifyEachLink_ForEachGiftCardSubcategory(categoryName, link, numberOfResults);
	}

	@Test(dataProvider = "Special Offers", alwaysRun = true, groups = {"not_jeepwave", "regression_Tests", "all_tests",
			"not_smart", "not_tvc", "not_aarp", "not_streetwise", "not_redesign", "not_aarpredesign", "not_allstate", "not_allstateCash" ,"not_mcafee"}, priority = 50)
	public void PT3242_Step4_specialOffers(String linkText) throws Exception {
		String categoryName = "Special Offers";

		String link = linkText.substring(0, linkText.indexOf("("))
				.trim().replace("\u203A", "");
		Integer numberOfResults = Integer.parseInt(linkText.split("\\(")[1].replace(")", "").trim());

		GiftCards_SearchResults.clickAndVerifyEachLink_ForEachGiftCardSubcategory(categoryName, link, numberOfResults);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_GiftCard_All();
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
