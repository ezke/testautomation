package dRewards.tests_regression;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

//Tests Step 7 of the Test Case PDF Eber sent me.
public class MerchCheckout_CheckoutFunctionality_Step1 extends BaseTest {

	String firstName = "Checkout1Name";
	String lastName = "Auto";
	String address1 = "1225 broken sound";
	String address2 = "";
	String city = "Boca Raton";
	String state = "FL";
	String country = "United States";
	String zip = "33487";
	String phone = "9544561212";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		
		Navigation.navigate_Account_UpdateAddressBook();
		if (ClientDataRetrieval.isRedesignClient()) {
			state = "Florida";
		}
		Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, address1, country, city, state, zip,
				phone);

		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		Homepage.deleteItemsFromShoppingCart();

		// Navigation.navigate_Account_UpdateAddressBook();
		// Account_UpdateAddressBook.delete_Address();

		Navigation.navigate_Home_viaLogo();
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Merchandise.addItemsToShoppingCart_Streetwise();			
		} else {
			// add 2 products to cart.
			Navigation.navigate_Merchandise_Beauty();
			Merchandise.selectPageToNavigate_AndChooseRandomProduct();
			
			Navigation.navigate_Merchandise_Tech();
			Merchandise.selectPageToNavigate_AndChooseRandomProduct();
		}
		
		
		//Adding this condition to check if item added to shopping cart is
		//not sold out. If yes we remove all items from cart and add new product
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
			ShoppingCart.remove_AllItems();
			
			if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				Merchandise.addItemsToShoppingCart_Streetwise();			
			} else {
				// add 2 products to cart.
				Navigation.navigate_Merchandise_Beauty();
				Merchandise.selectPageToNavigate_AndChooseRandomProduct();
				
				Navigation.navigate_Merchandise_Tech();
				Merchandise.selectPageToNavigate_AndChooseRandomProduct();
			}
		}
		
		ShoppingCart.click_ProceedToCheckout();

		if (ClientDataRetrieval.isRedesignClient()) {
			if (BaseUI.elementExists("checkout_ShipAddress_ChangeBtn", null, null)) {
				CheckoutPage_ShippingInfo.continue_To_NextPage();
			}
		} else {
			if (BaseUI.elementExists("checkout_AddNewShippingAddress", null, null)) {
				CheckoutPage_ShippingInfo.continue_To_NextPage();
			}
		}	
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();

	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step8_PT4789_Step18_NegativeTest_AddNewDeliveryAddress_ErrorMessage_FirstName() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("First Name", "Your First Name is required.");
		} else {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Recipient's First Name", "Please enter your first name");
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step8_PT4789_Step18_NegativeTest_AddNewDeliveryAddress_ErrorMessage_LastName() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Last Name", "Your Last Name is required.");
		} else {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Recipient's Last Name", "Please enter your last name");
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step8_PT4789_Step18_NegativeTest_AddNewDeliveryAddress_ErrorMessage_AddressLine1() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Address Line 1", "Your Address Line 1 is required.");
		} else {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Address Line 1", "Shipping address is required");
		}	
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step8_PT4789_Step18_NegativeTest_AddNewDeliveryAddress_ErrorMessage_City() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("City", "Your City is required.");
		} else {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("City", "Shipping city is required");
		}
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step8_PT4789_Step18_NegativeTest_AddNewDeliveryAddress_ErrorMessage_ZipCode() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Zip/Postal Code", "Your Zip Code is required.");
		} else {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Zip/Postal Code", "Please enter your zip code");
		}	
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step8_PT4789_Step18_NegativeTest_AddNewDeliveryAddress_ErrorMessage_Phone() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Phone", "Your Phone Number is required.");
		} else {
			CheckoutPage_ShippingInfo.verify_FieldErrorDisplayed("Phone", "Please enter a valid phone number");
		}	
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_shippingAddress1() throws Exception {
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct(
				"checkout_1stExistingAddr_ShippingAddress1", address1, true);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_shippingAddress2() throws Exception {

		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct(
				"checkout_1stExistingAddr_ShippingAddress2", address2, false);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_city() throws Exception {
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_1stExistingAddr_City", city,
				true);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_state() throws Exception {
		String state = "FL";
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_1stExistingAddr_State",
				state, true);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_zipCode() throws Exception {
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_1stExistingAddr_ZipCode",
				zip, false);
		WebElement zipCodeMasked = Locator.lookupRequiredElement("checkout_1stExistingAddr_ZipCodeMasked", "QaAuto", null);
		BaseUI.verifyElementAppears(zipCodeMasked);
		BaseUI.verifyElementHasExpectedText(zipCodeMasked, "*****");

	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_country() throws Exception {
		String abbreviatedCountry = country.equals("United States") ? "US" : country;

		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_1stExistingAddr_Country",
				abbreviatedCountry, true);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3235_Step9_PT4789_Step19_Validate_ExistingAddress_phoneNumber() throws Exception {
		CheckoutPage_ShippingInfo.verify_Element_AppearsOrDoesnt_AndText_Correct("checkout_1stExistingAddr_Phone",
				phone, false);
		WebElement phoneNumberMasked = Locator.lookupRequiredElement("checkout_1stExistingAddr_PhoneMasked", "QAauto", null);
		BaseUI.verifyElementAppears(phoneNumberMasked);
		BaseUI.verifyElementHasExpectedText(phoneNumberMasked,
				"******" + phone.substring(phone.length() - 4, phone.length()));
	}

	/*
	 * @Test //(priority = 2) public void
	 * PT3235_Step10_PT4789_Step20_User_AbleToSelect_ExistingAddress_andContinueToPaymentInfo(
	 * ) throws Exception { if
	 * (BaseUI.elementExists("checkout_ShipToExistingAddress_Radio", null,
	 * null)){ CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio();
	 * CheckoutPage_ShippingInfo.click_ContinueToPaymentInfo();
	 * BaseUI.verifyElementAppears(Locator.lookupRequiredElement(
	 * "checkoutPay_PaymentInfoTitle"));
	 * 
	 * } CheckoutPage_ShippingInfo.click_ContinueToPaymentInfo();
	 * BaseUI.verifyElementAppears(Locator.lookupRequiredElement(
	 * "checkoutPay_PaymentInfoTitle"));
	 * 
	 * }
	 */

	@Test(priority = 2, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step10_PT4789_Step20_User_AbleToSelect_NewShippingAddress_andContinueToPaymentInfo() throws Exception {

		// CheckoutPage_ShippingInfo.continue_To_NextPage();
		CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, state, zip, phone);
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
			CheckoutPage_AllStateCash.click_Continue_ToNextPage();
		}

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("checkoutPay_PaymentInfoTitle"));

		// Navigation.navigate_Account_UpdateAddressBook();
		// Account_UpdateAddressBook.delete_Address_ByPassedInName("QAauto");
	}

	@Test(priority = 3, groups = { "regression_Tests", "all_tests"})
	public void PT2756_Step17_PT4789_Step20_Click_EditShippingInformation_TakesUser_toShippingInfoPage() throws Exception {
		CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		CheckoutPage_ReviewOrder.click_EditShipping();
		BaseUI.verifyElementExists("checkout_ShippingPage_Title", null, null);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			// Make sure to delete the address I added for this test if it still
			// exists.
			Navigation.navigate_Account_UpdateAddressBook();
			if (BaseUI.elementExists("accnt_addr_DeleteButton_ByPassedInText", firstName, null)) {
				Account_UpdateAddressBook.delete_Address_ByPassedInName(firstName);
			}
		} finally {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
			Browser.closeBrowser();
		}
	}
}
