package dRewards.tests_regression;

import java.util.ArrayList;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GiftCards_LandingPage;
import dRewards.pages.GiftCards_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class GiftCards_ValidateLeftMenuNavigation_ViewMoreAndViewLess extends BaseTest {

	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_GiftCard_All();
	}
	
	
	@DataProvider(name = "LeftMenu_GiftCard_Subcategories")
	public Object[][] createData_SubcategoryHeader() throws Exception {
		
		ArrayList<String> subcategoryHeaders = new ArrayList<String>(GiftCards_LandingPage.return_GiftCards_NavMenuList());

		return return_linkTextAs_ObjectArray(subcategoryHeaders);
	}
	
	
	public Object[][] return_linkTextAs_ObjectArray(ArrayList<String> dataSet) throws Exception {
		BaseUI.verify_true_AndLog(dataSet.size() > 0, "Found links", "Did NOT find links.");

		Object[][] linksToNavigate = new Object[dataSet.size()][1];
		int objectIndex = 0;
		for (String linkItem : dataSet) {
			linksToNavigate[objectIndex][0] = linkItem;
			objectIndex++;
		}
	
		return linksToNavigate;		
	}
	
	
	@Test(dataProvider = "LeftMenu_GiftCard_Subcategories", alwaysRun = true, groups = { "regression_Tests", "all_tests" })
	public void PT3242_Step16_PT3666_Step7To10_GiftCardSearchPage_ClickAndVerify_FeaturedCategories_EachSubcategory_ViewMore_ViewLessLink(String SubcategoryHeader) throws Exception {

		String[] breadcrumbs = { "Home", "Gift Cards", SubcategoryHeader };
		String[] breadcrumbs_Entertainment = { "Home", "Gift Cards", "Movie Tickets" };
		String[] breadcrumbs_DepartmentStores = { "Home", "Gift Cards", "Shopping", "Type: Department Stores" };
		GiftCards_LandingPage.navigate_LeftPanel_ByText(SubcategoryHeader);
		
		if(SubcategoryHeader.contains("All Gift Card")) {
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_AllGiftcard_LandingPage_Title")); 
				String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_AllGiftcard_LandingPage_Title"));
				BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", "Gift Cards", headerTitle);
				BaseUI.verifyElementHasExpectedText("giftCards_SearchResult_AllGiftcard_LandingPage_SubTitle", "All Gift Cards");
			} else {
				Navigation.verify_BreadcrumbList(breadcrumbs);
			}			
		}
		else {
			if (ClientDataRetrieval.client_Matches(client_Designation.Citi)){
				//Validation for Citi
				BaseUI.verifyElementAppears(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle")); 
				String headerTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("giftCards_SearchResult_HeaderTitle"));
				if(SubcategoryHeader.contains("Entertainment")) {
					BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", "Movie Tickets", headerTitle);
				} else {
					BaseUI.baseStringCompare("giftCards_SearchResult_HeaderTitle", SubcategoryHeader, headerTitle);
				}				
			} else {
				if(SubcategoryHeader.contains("Entertainment")) {
					Navigation.verify_BreadcrumbList(breadcrumbs_Entertainment);
				} else if(SubcategoryHeader.contains("Department Stores")) {
					Navigation.verify_BreadcrumbList(breadcrumbs_DepartmentStores);
				} else {
					Navigation.verify_BreadcrumbList(breadcrumbs);
				}
			}

			if(BaseUI.elementAppears(Locator.lookupOptionalElement("giftCards_SearchResult_LeftPanel_ViewMore_Text"))) {
				GiftCards_SearchResults.verifyLeftPanel_ViewMore_Text();
				GiftCards_SearchResults.clickAndVerifyLeftPanel_ViewMoreLink();
				GiftCards_SearchResults.verifyLeftPanel_ViewLess_Text();
				GiftCards_SearchResults.click_ViewLess_Link();
			} else {
				BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("giftCards_SearchResult_LeftPanel_ViewMore_Text"));
			}
		}
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_GiftCard_All();
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
