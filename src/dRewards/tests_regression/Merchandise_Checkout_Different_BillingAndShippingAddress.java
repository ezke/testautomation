package dRewards.tests_regression;

import com.google.common.util.concurrent.AtomicDouble;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.Account;
import dRewards.pages.Account_History;
import dRewards.pages.Account_OrderAndShippingDetails_Page;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class Merchandise_Checkout_Different_BillingAndShippingAddress extends BaseTest {

    // Variables for Billing Info
    String firstName = "QA";
    String lastName = "Auto";
    String billingAddress1 = "1225 Broken Sound";
    String billingCity = "Boca Raton";
    String billingState_Abbreviated = "FL";
    String billingState_FullName = "Florida";
    String billingZip = "33487";
    String billingPhone = "9544151992";

    // Variables for Shipping Info
    String shippingAddress1 = "1225 Hillsbery Ground";
    String shippingCity = "Wausau";
    String shippingState_Abbreviated = "WI";
    String shippingState_FullName = "Wisconsin";
    String shippingZip = "53001";
    String shippingPhone = "7152456542";

    String ccType = "MasterCard";
    String ccNumber = "5105105105105100";
    String cvvCode = "211";
    String expire_month = "12";
    String expire_year = "22";
    String country = "United States";

    // Variables for product info/pay info
    TableData shoppingCartData;
    PricingDetails pricingDetails;
    PricingDetails pricingDetails_CheckoutPage;
    PricingDetails pricingDetails_OrderConfirmPage;
    String points_Before_Checkout;
    String points_After_Checkout;
    String orderID;

    String secondRun_allstate_Cash = "$5.50";
    private String salesTaxCost;

    // Variable for Points available and Allstate Cash?

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        // Logic to remove all items from Shopping Cart.
        Homepage.deleteItemsFromShoppingCart();

        // Logic to remove all Shipping Info
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.delete_Address();

        // Logic to remove all Billing Info.
        Navigation.navigate_Account();
        Account.clear_PaymentHistory();
        Navigation.navigate_Home_viaLogo();

        // Add 2 products to cart.
        //Replace Men's shop to Jewelry because this category is been removed for all clients.
        //For Streetwise client we have just 3 subcategories available under Merchandise section
        if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Merchandise.addItemsToShoppingCart_Streetwise();
        } else {
            Navigation.navigate_Merchandise_Beauty();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            Navigation.navigate_Merchandise_Sunglasses();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        }

        //Adding this condition to check if item added to shopping cart is
        //not sold out. If yes we remove all items from cart and add new product
        if (BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
            ShoppingCart.remove_AllItems();

            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Merchandise.addItemsToShoppingCart_Streetwise();
            } else {
                Navigation.navigate_Merchandise_Beauty();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
                Navigation.navigate_Merchandise_Sunglasses();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }
        }

        // Add logic here to pull product info from Shopping Cart page
        pricingDetails = ShoppingCart.retrieve_PricingDetails();
        pricingDetails.set_SavingsAmount_Citi();
        shoppingCartData = ShoppingCart.get_Table_Data();
        points_Before_Checkout = Navigation.returnPoints();

        ShoppingCart.click_ProceedToCheckout();
    }

    @Test(priority = 10, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT3235_Validate_ShippingPage_PriceInfo() throws Exception {
        PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
    }

    @Test(priority = 20, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Continue_ToPage_AfterShippingInfo() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            if (BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_ShipAddress_ChangeBtn"))) {
                CheckoutPage_ShippingInfo.click_Checkout_ShippingAddress_ChangeBtn();
            }
            CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, shippingAddress1, shippingCity,
                    shippingState_FullName, shippingZip, shippingPhone);
        } else {
            if (CheckoutPage_ShippingInfo.newShippingAddress_Radio_Exists()) {
                CheckoutPage_ShippingInfo.click_AddNewShippingAddress_Radio();
            }
            CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, shippingAddress1, shippingCity,
                    shippingState_Abbreviated, shippingZip, shippingPhone);
        }

        CheckoutPage_ShippingInfo.check_SaveForLater_Checkbox();
        CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();

        pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
        if(ClientDataRetrieval.isRedesignClient()){
            pricingDetails.order_Total = CheckoutPage_ShippingInfo.recalculateOrderTotal(shoppingCartData);
        }
        pricingDetails.add_Taxes_To_OrderTotal();

    }

    @Test(priority = 25, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
           "not_mcafee", "not_aarp", "not_aarpredesign", "not_streetwise", "criticalTests_AllStateCash"})
    public void PT3235_Validate_AllStateCash_PriceInfo() throws Exception {
        PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
    }

    @Test(priority = 26, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
            "not_aarp", "not_streetwise", "criticalTests_AllStateCash", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT3235_Validate_AllStateCash_Continue_ToBillingPage() throws Exception {
        // Allstate Cash was not used for this run through.
        pricingDetails.allstate_Cash = "$0.00";
        CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
        CheckoutPage_AllStateCash.click_Continue_ToNextPage();
    }

    @Test(priority = 27, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_BillingPage_PricingInfo() throws Exception {
//        if(ClientDataRetrieval.isRedesignClient()){
//            pricingDetails.
//        }

        // Verify some stuff
        PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
        }
    }

    @Test(priority = 30, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderReviewPage_ProductDetails() throws Exception {
        // Update Payment Info. Save for later use.
        if (ClientDataRetrieval.isRedesignClient()) {
            CheckoutPage_ShippingInfo.click_Checkout_ExistingShippingAddress_Checkbox();
            CheckoutPage_PaymentInfo.add_NewPaymentOption("Mastercard", ccNumber, expire_month, expire_year, cvvCode,
                    firstName + " " + lastName, billingAddress1, country, billingCity, billingState_FullName, billingZip);
        } else {
            CheckoutPage_PaymentInfo.add_NewPaymentOption(ccType, ccNumber, expire_month, expire_year, cvvCode,
                    firstName + " " + lastName, billingAddress1, country, billingCity, billingState_Abbreviated, billingZip);
        }
        CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
        CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

        CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 40, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderReviewPage_PriceSummary() throws Exception {
        PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
    }

    @Test(priority = 40, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderReviewPage_PaymentInformation() throws Exception {
        CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, billingAddress1, billingCity,
                billingState_Abbreviated, billingZip, ccType, ccNumber, expire_month + "/" + expire_year);

        // Add Logic here for Allstate Cash
        CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
    }

    @Test(priority = 40, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderReviewPage_ShippingInformation() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();
        } else {
            CheckoutPage_ReviewOrder.retrieve_PricingDetails();
        }
        CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, shippingAddress1, shippingCity,
                shippingState_Abbreviated, shippingZip, country, shippingPhone);
    }

    @Test(priority = 40, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderReviewPage_PricingDetails() throws Exception {

        CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
                pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
                pricingDetails.order_Total, pricingDetails.tax_Cost);
    }

    @Test(priority = 40, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderReviewPage_IAgreeInfo() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            pricingDetails_CheckoutPage = CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();
            salesTaxCost = pricingDetails_CheckoutPage.tax_Cost;
        }
        CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
    }

    @Test(priority = 50, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {
        if (!(ClientDataRetrieval.isRedesignClient())) {
            CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
        }
        CheckoutPage_ReviewOrder.click_PlaceYourOrder();
        orderID = OrderConfirmationPage.getOrderID();

        OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, shippingAddress1, shippingCity,
                shippingState_Abbreviated, "*****", country, shippingPhone);
    }

    @Test(priority = 51, groups = {"regression_Tests", "all_tests", "not_citi", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
        points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
                pricingDetails.points);
        Navigation.verify_pointsMatchExpected(points_After_Checkout);
    }

    @Test(priority = 51, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
        billingState_Abbreviated = "FL";
        OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, billingAddress1, billingCity,
                billingState_Abbreviated, ccType, ccNumber, billingZip);
    }

    @Test(priority = 51, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
        OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 51, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
            OrderConfirmationPage.verify_PricingDetails(pricingDetails_CheckoutPage);
        } else {
            pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
            OrderConfirmationPage.verify_PricingDetails(pricingDetails);
        }
    }

    @Test(priority = 60, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_AccountHistory_Page() throws Exception {
        Navigation.navigate_Account();

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            Account_History.verify_Purchase(orderID, pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase(orderID, pricingDetails.points);
        }
    }

    @Test(priority = 70, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
        Account.click_ViewDetails_ByOrderNumber(orderID);
        Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, shippingAddress1,
                shippingCity, shippingState_Abbreviated, country, shippingZip);
    }

    @Test(priority = 71, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_CancelOrder_Page_ProductInfo() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAuct_OrderAndShip_YouPay"));
        Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
    }

    @Test(priority = 72, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_CancelOrder_Page_PaymentInfo() throws Exception {

        String orderDate = Account_OrderAndShippingDetails_Page.getFormatted_OrderDate();

        Double orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));

        if (ClientDataRetrieval.isRedesignClient()) {
            Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID, orderDate,
                    orderSubtotal, Double.parseDouble(pricingDetails_OrderConfirmPage.tax_Cost.replace("$", "").replace(",", "")),
                    pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails_OrderConfirmPage.order_Total.replace("$", "").replace(",", "")),
                    pricingDetails.points, ccNumber);
        } else {
            Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID, orderDate,
                    orderSubtotal, Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", "")),
                    pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", "")),
                    pricingDetails.points, ccNumber);
        }
    }

    @Test(priority = 80, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_Cancelled() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails_OrderConfirmPage, salesTaxCost);
        } else {
            Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
        }
    }

    @Test(priority = 81, groups = {"regression_Tests", "all_tests", "not_citi", "criticalTests_AllStateCash"})
    public void PT3235_Validate_Points_Reimbursed() throws Exception {
        Navigation.verify_pointsMatchExpected(points_Before_Checkout);
    }

    @Test(priority = 82, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Validate_AccountHistory_OrderCancelled() throws Exception {
        Navigation.navigate_Account();
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase_Cancelled(pricingDetails.points);
        }
    }

    // These tests are for our second run through the Merchandise workflow.
    // This run will be using the shipping and billing information that we used
    // for the first run through.

    @Test(priority = 100, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"})
    public void PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info() throws Exception {
        Navigation.navigate_Home_viaLogo();

        //For Streetwise client we have just 3 subcategories available under Merchandise section
        if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Merchandise.addItemsToShoppingCart_Streetwise();
        } else {
            Navigation.navigate_Merchandise_Beauty();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            Navigation.navigate_Merchandise_Sunglasses();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        }

        //Adding this condition to check if item added to shopping cart is
        //not sold out. If yes we remove all items from cart and add new product
        if (BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
            ShoppingCart.remove_AllItems();

            if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
                Merchandise.addItemsToShoppingCart_Streetwise();
            } else {
                Navigation.navigate_Merchandise_Beauty();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
                Navigation.navigate_Merchandise_Sunglasses();
                Merchandise.selectPageToNavigate_AndChooseRandomProduct();
            }
        }

        // Add logic here to pull product info from Shopping Cart page
        pricingDetails = ShoppingCart.retrieve_PricingDetails();
        pricingDetails.set_SavingsAmount_Citi();
        shoppingCartData = ShoppingCart.get_Table_Data();
        points_Before_Checkout = Navigation.returnPoints();

        ShoppingCart.click_ProceedToCheckout();
    }

    @Test(priority = 110, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash", "not_redesign", "not_aarpredesign","not_mcafee"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_ShippingPage_PriceInfo() throws Exception {
        PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
    }

    @Test(priority = 120, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_ExistingAddress() throws Exception {

        if (!(ClientDataRetrieval.isRedesignClient())) {
            CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio();
        }
        String actualAddress = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress1",
                        firstName, null));
        BaseUI.baseStringCompare("checkout_ExistingAddress_ShippingAddress1", shippingAddress1, actualAddress);
    }

    @Test(priority = 121, groups = {"regression_Tests", "critical_Tests", "all_tests"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Continue_ExistingPayment() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
            pricingDetails.order_Total = CheckoutPage_ShippingInfo.recalculateOrderTotal(shoppingCartData);
            pricingDetails.add_Taxes_To_OrderTotal();
        } else {
            CheckoutPage_ShippingInfo.click_AddExistingAddress_Radio();
            CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
            pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
            pricingDetails.add_Taxes_To_OrderTotal();
        }
    }

    @Test(priority = 125, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
            "not_aarp", "not_aarpredesign", "not_streetwise", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_AllStateCash_PriceInfo() throws Exception {
        PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
    }

    @Test(priority = 126, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
            "not_aarp", "not_streetwise", "criticalTests_AllStateCash", "not_redesign", "not_aarpredesign","not_mcafee"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_AllStateCash_Continue_ToBillingPage() throws Exception {
        // Allstate Cash was not used for this run through.
        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            pricingDetails.allstate_Cash = secondRun_allstate_Cash;
        }
        pricingDetails.subtract_AllstateCash_From_OrderTotal();

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            CheckoutPage_AllStateCash
                    .apply_Other_Amount(Double.parseDouble(secondRun_allstate_Cash.replace("$", "").replace(",", "")));
            CheckoutPage_AllStateCash.click_Continue_ToNextPage();
        }
    }

    @Test(priority = 127, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_BillingPage_PricingInfo() throws Exception {
        PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
        }
    }

    @Test(priority = 130, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderReviewPage_ProductDetails() throws Exception {
        // Update Payment Info. Save for later use.
        //
        // CheckoutPage_PaymentInfo.add_NewPaymentOption(ccType, ccNumber,
        // expire_month, expire_year, cvvCode,
        // firstName + " " + lastName, address1, country, city, state, zip);
        // CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();

        if (ClientDataRetrieval.isRedesignClient()) {
            CheckoutPage_PaymentInfo.enter_CVV_Code(cvvCode);
        } else {
            CheckoutPage_PaymentInfo.click_ExistingPayment_Radio();
            CheckoutPage_PaymentInfo.enter_CVV_Code(cvvCode);
            // Add logic to verify Existing Payment Radio.
            CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
        }
        CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 140, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderReviewPage_PriceSummary() throws Exception {
        PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
    }

    @Test(priority = 140, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderReviewPage_PaymentInformation() throws Exception {
        CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, billingAddress1, billingCity,
                billingState_Abbreviated, billingZip, ccType, ccNumber, expire_month + "/" + expire_year);

        // Add Logic here for Allstate Cash
        CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
    }

    @Test(priority = 140, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderReviewPage_ShippingInformation() throws Exception {
        CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, shippingAddress1, shippingCity,
                shippingState_Abbreviated, shippingZip, country, shippingPhone);
    }

    @Test(priority = 140, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderReviewPage_PricingDetails() throws Exception {
        CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
                pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
                pricingDetails.order_Total, pricingDetails.tax_Cost);
    }

    @Test(priority = 140, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderReviewPage_IAgreeInfo() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            pricingDetails_CheckoutPage = CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();
            salesTaxCost = pricingDetails_CheckoutPage.tax_Cost;
        }
        CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
    }

    @Test(priority = 150, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {
        if (!(ClientDataRetrieval.isRedesignClient())) {
            CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
        }
        CheckoutPage_ReviewOrder.click_PlaceYourOrder();

        orderID = OrderConfirmationPage.getOrderID();

        OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, shippingAddress1, shippingCity,
                shippingState_Abbreviated, "*****", country, shippingPhone);
    }

    @Test(priority = 151, groups = {"regression_Tests", "all_tests", "not_citi", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
        points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
                pricingDetails.points);
        Navigation.verify_pointsMatchExpected(points_After_Checkout);
    }

    @Test(priority = 151, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
        OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, billingAddress1, billingCity,
                billingState_Abbreviated, ccType, ccNumber, billingZip);
    }

    @Test(priority = 151, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
        OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 151, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            OrderConfirmationPage.verify_PricingDetails(pricingDetails_CheckoutPage);
        } else {
            OrderConfirmationPage.verify_PricingDetails(pricingDetails);
        }
    }

    @Test(priority = 160, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_AccountHistory_Page() throws Exception {
        Navigation.navigate_Account();

        if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
            Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            Account_History.verify_Purchase(orderID, pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase(orderID, pricingDetails.points);
        }
    }

    @Test(priority = 170, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
        Account.click_ViewDetails_ByOrderNumber(orderID);
        Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, shippingAddress1,
                shippingCity, shippingState_Abbreviated, country, shippingZip);
    }

    @Test(priority = 171, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_CancelOrder_Page_ProductInfo() throws Exception {
        Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
    }

    @Test(priority = 172, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_CancelOrder_Page_PaymentInfo() throws Exception {

        String orderDate = Account_OrderAndShippingDetails_Page.getFormatted_OrderDate();

        Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID,
                orderDate, ccNumber);
    }

    @Test(priority = 180, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_Cancelled() throws Exception {

        Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
    }

    @Test(priority = 181, groups = {"regression_Tests", "all_tests", "not_citi", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_Points_Reimbursed() throws Exception {
        Navigation.verify_pointsMatchExpected(points_Before_Checkout);
    }

    @Test(priority = 182, groups = {"regression_Tests", "all_tests", "criticalTests_AllStateCash"},
            dependsOnMethods = {"PT3235_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"})
    public void PT3235_2ndRun_Validate_AccountHistory_OrderCancelled() throws Exception {
        Navigation.navigate_Account();
        if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase_Cancelled(pricingDetails.points);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            // Account.cancel_Order(orderID);
        } finally {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
            Browser.closeBrowser();
        }
    }
}// End of Class
