package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class MyAuction_ValidationTest extends BaseTest {

    private String closedAuctionDate, closedAuctions_EndedOnDate;
    private String closedAuctions_RetailValue;
    private String closedAuctions_PointsBid;
    private String closedAuctionsTitle;
    private String quantity = "1";
    private String userFirstName;
    private String address1, cityStateZipCode, country;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_Account_UpdateAddressBook();
        userFirstName = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_AddressBook_Name"));
        address1 = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_AddressBook_Address1"));
        cityStateZipCode = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_AddressBook_CityStateZipCode"));
        country = BaseUI.getTextFromField(Locator.lookupRequiredElement("accnt_AddressBook_Country"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee"} )
    public void PT4027_Step1_HomePage_VerifyLogo() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" , "not_mcafee"} )
    public void PT4027_Step2_MyRewards_Verify_LiveOrClosedAuctionsLink() throws Exception {
        Navigation.navigate_Account();
        if(!BaseUI.elementExists("accnt_OpenAuctions_Link", null, null)) {
            BaseUI.verifyElementAppears(Locator.lookupElement("accnt_ClosedAuctions_Link"));
        } else {
            BaseUI.verifyElementAppears(Locator.lookupElement("accnt_OpenAuctions_Link"));
        }
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_MyRewards_ClickClosedAuctionsLink_Verify_NavigatesTo_ClosedAuctionsPage() throws Exception {
        Account_History.verify_ClosedAuctionLandingPage_HeaderAndBreadcrumb();
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_MyAuctions_VerifyClosedAuctionsTitle() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("closedAuctions_Title"));
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_MyAuctions_VerifyClosedAuctionsEndedOnDate() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("closedAuctions_EndedOnDate"));
    }

    @Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_MyAuctions_ClickClosedAuctionsExpands_VerifyPointsBidDisplayed() throws Exception {
        Auctions_ClosedAuctions.expand_ClosedAuction();
        BaseUI.verifyElementAppears(Locator.lookupElement("closedAuctions_ExpandView_PointsBid"));
    }

    @Test(priority = 70, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_MyAuctions_VerifyClosedAuctionsExpands_DateMatch() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("closedAuctions_ExpandView_BidDateAndTime"));
        String dateClosed = BaseUI.getTextFromField(Locator.lookupRequiredElement("closedAuctions_ExpandView_BidDateAndTime"));
        closedAuctionDate = dateClosed.split(" at")[0];
        BaseUI.verifyElementHasExpectedText("closedAuctions_EndedOnDate", closedAuctionDate);
    }


    @Test(priority = 80, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_MyRewards_ClickClosedAuctions_ViewAuctionsBtn_VerifyLandingPageHeader() throws Exception {
        closedAuctionsTitle = BaseUI.getTextFromField(Locator.lookupRequiredElement("closedAuctions_Title"));
        closedAuctions_RetailValue = BaseUI.getTextFromField(Locator.lookupRequiredElement("closedAuctions_RetailValue"));
        closedAuctions_PointsBid = BaseUI.getTextFromField(Locator.lookupRequiredElement("closedAuctions_ExpandView_PointsBid"));
        Auctions_ClosedAuctions.click_ViewAuctionsBtn();
        BaseUI.verifyElementHasExpectedText("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_Header", closedAuctionsTitle);
    }

    @Test(priority = 90, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" , "not_mcafee"} )
    public void PT4027_Step5_MyRewards_ClickClosedAuctions_VerifyClosedAuctionsExpands() throws Exception {
        closedAuctions_EndedOnDate = BaseUI.return_Date_AsDifferentFormat(closedAuctionDate, "MMM d, yyyy", "MM/dd/yy");
        String endedOnDate = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_EndedDate"));
        String auctionEndedOnDate = endedOnDate.split(" at")[0];
        BaseUI.baseStringCompare("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_EndedDate",
                closedAuctions_EndedOnDate, auctionEndedOnDate);
    }

    @Test(priority = 90, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_ClosedAuctionsDetailsPage_VerifyRetailValue()  {
        BaseUI.verifyElementHasExpectedText("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_RetailValue",
                closedAuctions_RetailValue.replaceAll(" Retail Value", "")+".00");
    }

    @Test(priority = 90, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" , "not_mcafee"} )
    public void PT4027_Step5_ClosedAuctionsDetailsPage_VerifyBidPoints()  {
        BaseUI.verifyElementHasExpectedText("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_PointsBid",
                closedAuctions_PointsBid+" Points");
    }

    @Test(priority = 95, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_ClosedAuctionsDetailsPage_VerifyClaimedDateDisplayed() throws Exception {
        if(! BaseUI.elementAppears(Locator.lookupElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_DateClaimed"))) {
            Auctions_ClosedAuctions.claimAuctions_VerifyOrderDetails(closedAuctionsTitle);
        } else {
            BaseUI.verifyElementAppears(Locator.lookupElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_DateClaimed"));
            BaseUI.verifyElementHasExpectedText("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_DateClaimed",
                    "Claimed on "+BaseUI.return_Date_AsDifferentFormat(closedAuctionDate, "MMM dd, yyyy", "MMMM dd, yyyy"));
        }
    }

    @Test(priority = 96, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" } )
    public void PT4027_Step5_ClosedAuctionsDetailsPage_VerifyClosedAuctions_WinnerStatus_Or_AuctionClaimedSuccessfully() throws Exception {
        if(! BaseUI.elementAppears(Locator.lookupElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_DateClaimed"))) {
            BaseUI.verifyElementAppears(Locator.lookupElement("closedAuctions_ClaimedConfirmationPage_OrderConfirmationMessage"));
            BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_OrderConfirmationMessage",
                    "Your confirmation number is: ");
        } else {
            BaseUI.verifyElementAppears(Locator.lookupElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_AuctionWinStatus"));
            BaseUI.verifyElementHasExpectedText("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_AuctionWinStatus",
                    "Winner");
        }
    }

    @Test(priority = 100, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" }, dependsOnMethods = {"PT4027_Step5_ClosedAuctionsDetailsPage_VerifyClaimedDateDisplayed"})
    public void PT4027_Step5_ClosedAuctionsDetailsPage_ClickViewDetailsBtn_VerifyNavigatesToOrderDetailsPage_Or_AuctionsClaimedEmailSentMessage() throws Exception {
        if(! BaseUI.elementAppears(Locator.lookupElement("closedAuction_ViewAuctionDetails_ClosedAuctLandingPage_DateClaimed"))) {
            BaseUI.verifyElementHasExpectedText("closedAuctions_ClaimedConfirmationPage_OrderConfirmation_EmailSentMsg",
                    "You will receive an email confirmation shortly. For your records you may also print your receipt.");
        } else {
            Auctions_ClosedAuctions.click_ViewDetailsBtn();
            BaseUI.verifyElementAppears(Locator.lookupElement("myAuct_OrderAndShip_OrderDetails_ConfirmationMsgText"));
            BaseUI.verifyElementHasExpectedText("myAuct_OrderAndShip_OrderDetails_OrderDate",
                    "Order Date: "+BaseUI.return_Date_AsDifferentFormat(closedAuctionDate, "MMM dd, yyyy", "MMM dd, yyyy"));
        }
    }

    @Test(priority = 105, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" }, dependsOnMethods = {"PT4027_Step5_ClosedAuctionsDetailsPage_VerifyClaimedDateDisplayed"})
    public void PT4027_Step5_AuctionsOrderConfirmationPage_ShippingDetails() {
        Auctions_ClosedAuctions.verifyClosedAuctions_ConfirmationPage_ShippingDetails(userFirstName, address1,
                cityStateZipCode, country);
    }

    @Test(priority = 110, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_mcafee" }, dependsOnMethods = {"PT4027_Step5_ClosedAuctionsDetailsPage_VerifyClaimedDateDisplayed"})
    public void PT4027_Step5_AuctionsOrderConfirmationPage_ClosedAuctionsDetails() {
        Auctions_ClosedAuctions.verifyClosedAuctions_OrderDetails_AuctionsDetails(closedAuctionsTitle, closedAuctions_PointsBid,
                closedAuctions_RetailValue, quantity);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
