package dRewards.tests_regression;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

public class GiftCardCheckout_SavedShippingAddress_NewPayment extends BaseTest {

    // Variables for Billing Info
    String firstName = "Autoqa";
    String lastName = "DRQA";
    String billingAddress1 = "1225 broken sound";
    String billingCity = "Boca Raton";
    String billingState_Abbreviated = "FL";
    String billingState_FullName = "Florida";
    String billingZip = "33487";
    String billingPhone = "9544151992";

    // Variables for Shipping Info
    String shippingAddress1 = "1225 broken sound";
    String shippingCity = "Boca Raton";
    String shippingState_Abbreviated = "FL";
    String shippingState_FullName = "Florida";
    String shippingZip = "33487";
    String shippingPhone = "7152456542";

    private static String ccType = "MasterCard";
    private static final String ccNumber = "5105105105105100";
    private static final String cvvCode = "211";
    private static final String expire_month = "12";
    private static final String expire_year = "22";
    private static final String country = "United States";


    // Variables for product info/pay info
    private TableData shoppingCartData;
    private PricingDetails pricingDetails;
    private String points_Before_Checkout;
    private PricingDetails pricingDetails_CheckoutPage;
    private String salesTaxCost;
    private String points_After_Checkout;

    String orderID;


    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        // Logic to remove all items from Shopping Cart.
        Homepage.deleteItemsFromShoppingCart();

        // Logic to remove all Shipping Info
        Navigation.navigate_Account_UpdateAddressBook();
        Account_UpdateAddressBook.delete_Address();

        //Add new address
        Account_UpdateAddressBook.add_Address_AndSetToDefault(firstName, lastName, shippingAddress1,
                country, shippingCity, shippingState_FullName, shippingZip, shippingPhone);

        // Logic to remove all Billing Info.
        Navigation.navigate_Account();
        Account.clear_PaymentHistory();
        Navigation.navigate_Home_viaLogo();

        // Add 2 products to cart.
        Navigation.navigate_GiftCard_Dining();
        Merchandise.selectPageToNavigate_AndChooseRandomProduct();

        Navigation.navigate_GiftCard_Shopping();
        Merchandise.selectPageToNavigate_AndChooseRandomProduct();


        //Adding this condition to check if item added to shopping cart is
        //not sold out. If yes we remove all items from cart and add new product
        if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
            ShoppingCart.remove_AllItems();

            Navigation.navigate_GiftCard_Dining();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();

            Navigation.navigate_GiftCard_Shopping();
            Merchandise.selectPageToNavigate_AndChooseRandomProduct();
        }

        // Add logic here to pull product info from Shopping Cart page
        pricingDetails = ShoppingCart.retrieve_PricingDetails();
        shoppingCartData = ShoppingCart.get_Table_Data();

        points_Before_Checkout = Navigation.returnPoints();
    }

    //Test for defect 7268
    @Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4752_Validate_ShoppingCart_GiftCards_Name_DoesNOT_StartWith_GiftCards() throws Exception {
        shoppingCartData.replace_Characters_FromBeginning("Gift Card ", "", "Product Name");
        shoppingCartData.replace_Characters_FromBeginning("Gift Card  ", "", "Product Name");
        shoppingCartData.trim_column("Product Name");

        BaseUI.verify_true_AndLog(shoppingCartData.data.size() > 0, "Shopping Cart has Items", "Shopping Cart does not have items");
        for (HashMap<String, String> giftCardRow : shoppingCartData.data) {
            BaseUI.verify_false_AndLog(giftCardRow.get("Product Name").startsWith("Gift Card"),
                    "Product Name should not have started with Gift Card.", "Product Name did start with Gift Card");
        }
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed() throws Exception {
        ShoppingCart.click_ProceedToCheckout();
        CheckoutPage_ShippingInfo.verifyElementHasExpectedTextChange();

    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step5_CheckoutPage_ValidateShippingAddressDisplayed() {
        String actualAddress = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("checkout_ExistingAddress_ShippingAddress1",
                        firstName, null));
        BaseUI.baseStringCompare("checkout_ExistingAddress_ShippingAddress1", shippingAddress1, actualAddress);
    }


    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step5_Validate_BillingPage_PricingInfo() {
        PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step9_To_10_Validate_OrderReviewPage_ProductDetails() throws Exception {
        if(BaseUI.elementExists("checkout_newPayment_CreditCardNumber", null, null)
                && BaseUI.element_isEnabled(Locator.lookupRequiredElement("checkout_newPayment_Address1"))) {
            CheckoutPage_PaymentInfo.add_newPaymentOption_UsingDefaults();
        } else {
            CheckoutPage_PaymentInfo.add_NewPaymentOption_Redesign("Mastercard", ccNumber, expire_month, expire_year,
                    cvvCode, firstName + " " + lastName);
            CheckoutPage_PaymentInfo.clickUseMyShippingAddressCheckbox();
        }
        CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
        CheckoutPage_PaymentInfo.click_ReviewPayment_Button();

        CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step11_Validate_OrderReviewPage_PriceSummary() throws Exception {
        PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step11_Validate_OrderReviewPage_PaymentInformation() {

        CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, billingAddress1, billingCity,
                billingState_Abbreviated, billingZip, ccType, ccNumber, expire_month + "/" + expire_year);

        // Add Logic here for Allstate Cash
        CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step11_Validate_OrderReviewPage_ShippingInformation() {
        CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();
        CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, shippingAddress1, shippingCity,
                shippingState_Abbreviated, shippingZip, country, shippingPhone);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step11_Validate_OrderReviewPage_PricingDetails() throws Exception {

        CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step11_Validate_OrderReviewPage_IAgreeInfo() throws Exception {
        pricingDetails_CheckoutPage = CheckoutPage_ReviewOrder.retrieve_CheckoutReviewOrder_PricingDetails();
        salesTaxCost = pricingDetails_CheckoutPage.tax_Cost;

        CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step12_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {
        CheckoutPage_ReviewOrder.click_PlaceYourOrder();
        orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));

        OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, shippingAddress1, shippingCity,
                shippingState_Abbreviated, "*****", country, shippingPhone);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" },
            dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed" })
    public void PT4752_Step13_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
        points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
                pricingDetails.points);
        Navigation.verify_pointsMatchExpected(points_After_Checkout);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step13_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
        OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, billingAddress1, billingCity,
                billingState_Abbreviated, ccType, ccNumber, billingZip);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step13_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
        OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
    }

    @Test(priority = 51, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step13_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
        OrderConfirmationPage.verify_PricingDetails(pricingDetails_CheckoutPage);
    }

    @Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step17_To_18_Validate_AccountHistory_Page() throws Exception {
        Navigation.navigate_Account();

        Account_History.verify_Purchase(orderID, pricingDetails.points);
    }

    @Test(priority = 70, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step19_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
        Account.click_ViewDetails_ByOrderNumber(orderID);
        Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, shippingAddress1,
                shippingCity, shippingState_Abbreviated, country, shippingZip);
    }

    @Test(priority = 71, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step19_Validate_CancelOrder_Page_ProductInfo() throws Exception {
        BaseUI.scroll_to_element(Locator.lookupRequiredElement("myAuct_OrderAndShip_YouPay"));
        Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
    }

    @Test(priority = 72, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step19_Validate_CancelOrder_Page_PaymentInfo() throws Exception {

        String orderDate = Account_OrderAndShippingDetails_Page.getFormatted_OrderDate();

        Double orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
                - Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
                - Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));

        Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID, orderDate,
                orderSubtotal, Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", "")),
                pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", "")),
                pricingDetails.points, ccNumber);
    }

    @Test(priority = 80, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step19_Validate_Cancelled() throws Exception {
        Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
    }

    @Test(priority = 81, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step19_Validate_Points_Reimbursed() throws Exception {
        Navigation.verify_pointsMatchExpected(points_Before_Checkout);
    }

    @Test(priority = 82, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" }, dependsOnMethods = {"PT4752_Step4_Validate_CheckoutPage_ShippingAddress_ChangeBtnDisplayed"})
    public void PT4752_Step19_Validate_AccountHistory_OrderCancelled() throws Exception {
        Navigation.navigate_Account();
        if (ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.Citi)) {
            Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
        } else {
            Account_History.verify_Purchase_Cancelled(pricingDetails.points);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
