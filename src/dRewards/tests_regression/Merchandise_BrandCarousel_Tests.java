package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class Merchandise_BrandCarousel_Tests extends BaseTest {

    public static String brandName_Redesign = "Callaway";
    public static String brandName_Legacy = "Swarovski";
    public static String brandName_StreetwiseClient = "Wagan";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        Navigation.navigate_Merchandise();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step1_VerifyMerchandiseHeader() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchandisePage_Header"));
        BaseUI.verifyElementHasExpectedText("merchandisePage_Header", "Merchandise");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step2_VerifyMerchandise_ShopMostPopularHeader() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_ShopMostPopularBrandHeader"));
        BaseUI.verifyElementHasExpectedText("merchLandingPage_ShopMostPopularBrandHeader",
                "Shop our most popular brands");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step2_VerifyMerchandise_ViewAllBrandsHeader() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_ViewAllBrandsHeader"));
        BaseUI.verifyElementHasExpectedText("merchLandingPage_ViewAllBrandsHeader", "View All Brands");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step2_VerifyMerchandise_ShopMostPopularCarousel_LeftNavArrow() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_ShopMostPopularBrand_LeftNavArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step2_VerifyMerchandise_ShopMostPopularCarousel_RightNavArrow() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_ShopMostPopularBrand_RightNavArrow"));
    }


    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4792_Step2_Merchandise_BrandCarousel_ClickViewAllBrands_VerifyLandingPageHeader() throws Exception {
        Merchandise.clickViewAllBrandsLink();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_BrandCarousel_Brand_LandingPage_Header_Title"));
        BaseUI.verifyElementHasExpectedText("merch_BrandCarousel_Brand_LandingPage_Header_Title", "Brands");
    }

    @Test(groups = {"regression_Tests", "all_tests",})
    public void PT3243_PT4792_Step2_Merchandise_BrandCarousel_Verify_LeftNavigation_Arrow_Appears() throws Exception {
        if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
            Merchandise.click_Merchandise_BrandCarousel_RightNavigationArrow();
        }
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_BrandCarousel_LeftNavigation_Arrow"));
    }

    @Test(groups = {"regression_Tests", "all_tests",})
    public void PT3243_PT4792_Step2_Merchandise_BrandCarousel_Verify_CarouselImagesFound() throws Exception {
        Merchandise.verify_CarouselImagesFound_AndSomeVisible();
    }

    @Test(groups = {"regression_Tests", "all_tests",})
    public void PT3243_PT4792_Step2_Merchandise_BrandCarousel_Verify_RightNavigation_Arrow_Appears() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merch_BrandCarousel_RightNavigation_Arrow"));
    }

    //Cannot validate these for some clients since carousel is in motion, would cause false positive.
    @Test(groups = {"regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_allstate", "not_tvc", "not_streetwise"})
    public void PT3243_PT4792_Step2_Merchandise_BrandCarousel_Verify_Clicking_RightArrow_Navigates_ToRight() throws Exception {
        Merchandise.verify_ClickRight_List_Of_VisibleCarouselItemsChanged();
    }

    //Cannot validate these for some clients since carousel is in motion, would cause false positive.
    @Test(groups = {"regression_Tests", "all_tests", "not_aarpredesign", "not_jeepwave", "not_allstate", "not_tvc", "not_streetwise"})
    public void PT3243_PT4792_Step2_Merchandise_BrandCarousel_Verify_Clicking_LeftArrow_Navigates_ToLeft() throws Exception {
        Merchandise.click_Merchandise_BrandCarousel_RightNavigationArrow();
        Merchandise.verify_ClickLeft_List_Of_VisibleCarouselItemsChanged();
    }

    @Test(groups = {"regression_Tests", "all_tests",})
    public void PT3243_PT4792_Step2_Merchandise_BrandCarousel_Verify_Clicking_Brand_Navigates_ToLandingPage() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Merchandise.clickAndVerify_Merchandise_BrandCarousel_Displayed_NavigatesTo_LandingPage("merch_BrandCarousel_BrandImage",
                    brandName_Redesign);
        } else if (ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
            Merchandise.clickAndVerify_Merchandise_BrandCarousel_Displayed_NavigatesTo_LandingPage("merch_BrandCarousel_BrandImage",
                    brandName_StreetwiseClient);
        } else {
            Merchandise.clickAndVerify_Merchandise_BrandCarousel_Displayed_NavigatesTo_LandingPage("merch_BrandCarousel_BrandImage",
                    brandName_Legacy);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        Navigation.navigate_Home_viaLogo();
        Navigation.navigate_Merchandise();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Logout();
        }
        Browser.closeBrowser();
    }
}