package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.Account;
import dRewards.pages.Account_History;
import dRewards.pages.Account_OrderAndShippingDetails_Page;
import dRewards.pages.Account_UpdateAddressBook;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import dRewards.pages_Administration.AddNewPromoCampaign_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class PromoCode_FreeProducts extends BaseTest {

	String campaignName = "Automation_Test";
	String transactionDescription = "Automation Promo";
	String promoCode = "Automation";
	String[] discount_SkuValue = { "pcdemoipad2", "pcdemoipad2" };
	
	TableData shoppingCartData;
	PricingDetails pricingDetails;
	String points_Before_Checkout;
	String points_After_Checkout;
	String orderID;

	String firstName = "QA";
	String lastName = "Auto";
	String address1 = "1225 Broken Sound";
	String city = "Boca Raton";
	String state = "FL";
	String zip = "33487";
	String phone = "9544151992";
	String country = "United States";
	private String salesTaxCost;
	private String firstSelectedItemText;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		String randomNumber = BaseUI.random_NumberAsString(0, 70000);
		campaignName += "_" + randomNumber;
		promoCode += "_"+ randomNumber;

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		
		Navigation_Admin.navigate_ClickNavLink("Create Promo Codes");
		AddNewPromoCampaign_Admin.add_Promo_FreeProducts(campaignName, discount_SkuValue, transactionDescription,promoCode);
		
		//Not doing catalog upload as sku provided doesnot need it as of now but might need in future.
//		CatalogUpload_And_MemcacheFlush.launch_CatalogUpload();
		//Need to do the memcache flush
		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all items from Shopping Cart.
		Homepage.deleteItemsFromShoppingCart();

		// Logic to remove all Shipping Info
		Navigation.navigate_Account_UpdateAddressBook();
		Account_UpdateAddressBook.delete_Address();

		Navigation.enter_PromoCode(promoCode);
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_Verify_PromoCode_Displays() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_PromoCode_Message"));
	}

	@Test(priority = 10, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=true)
	public void PT43_Step1To3_Verify_PromoCode_OfferList_Displays() throws Exception {

		ArrayList<String> offerList = Navigation.return_PromoCode_OfferList();
		BaseUI.verify_true_AndLog(offerList.size() > 0, "Found Promo Code Offer list.",
				"Could not find Promo Code Offer list.");
	}

	@Test(priority = 20, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_Verify_PromoCode_HasFreeProduct() throws Exception {
		firstSelectedItemText = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("nav_PromoCode_FirstItemDisplayed"));
		Navigation.verify_PromoCode_FreeProducts();
	}

	@Test(priority = 25, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_Verify_PromoCode_OfferList_FreeProduct_SelectMessage() throws Exception {

		Navigation.verify_PromoCode_FreeProduct_ClickSelect_MessageIsCorrect(firstSelectedItemText);
	}



	@Test(priority = 30, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_FreeProducts_ShoppingCart_ClickProceed_Verify_ShippingPage_PriceInfo()
			throws Exception {

		Navigation.click_Promocode_Checkout_Button();
		pricingDetails = ShoppingCart.retrieve_PricingDetails();
		shoppingCartData = ShoppingCart.get_Table_Data();
		points_Before_Checkout = Navigation.returnPoints();

		ShoppingCart.click_ProceedToCheckout();
		PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
	}

	@Test(priority = 40, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_ContinueToPage_AddAddress() throws Exception {
		if (CheckoutPage_ShippingInfo.newShippingAddress_Radio_Exists()) {
			CheckoutPage_ShippingInfo.click_AddNewShippingAddress_Radio();
		}
		CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, state, zip, phone);
		CheckoutPage_ShippingInfo.check_SaveForLater_Checkbox();
		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
	}
	//These tests are commented out because the page does not exists anymore in the application (it was a bug and got fixed)
//	@Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_citi", "not_tvc", "not_smart", "not_allstate",
//			"not_aarp" })
//	public void PT43_Step1To3_PromoCode_OfferList_FreeProducts_Validate_AllStateCash_Continue_ToNextPage()
//			throws Exception {
//		// Allstate Cash was not used for this run through.
//		pricingDetails.allstate_Cash = "$0.00";
//		CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
//		CheckoutPage_AllStateCash.click_Continue_ToNextPage();
//	}

//	@Test(priority = 60, groups = { "not_citi", "not_tvc", "regression_Tests", "all_tests" })
//	public void PT43_Step1To3_PromoCode_OfferList_Validate_OrderReviewPage_PriceSummary() throws Exception {
//
//		pricingDetails.tax_Cost = "$0.00";
//		pricingDetails.add_Taxes_To_OrderTotal();
//
//		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
//			CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
//		}
//		CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();
//		PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
//
//	}

	@Test(priority = 61, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OrderReviewPage_ShippingInformation() throws Exception {

		pricingDetails.allstate_Cash = "$0.00";
		pricingDetails.tax_Cost = "$0.00";
		pricingDetails.add_Taxes_To_OrderTotal();
		PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
		CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, address1, city, state, zip, country,
				phone);

	}

	@Test(priority = 62, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OrderReviewPage_ProductDetails() throws Exception {

		CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 63, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OrderReviewPage_PricingDetails() throws Exception {

		CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
				pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
				pricingDetails.order_Total, pricingDetails.tax_Cost);
	}

	@Test(priority = 64, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OrderReviewPage_IAgreeInfo() throws Exception {

		CheckoutPage_ReviewOrder.verify_PromoCode_IAgreeMessage();
	}

	@Test(priority = 70, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {

		CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
		CheckoutPage_ReviewOrder.click_PlaceYourOrder();
		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, address1, city, state, "*****",
				country, phone);
	}

	@Test(priority = 71, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign" ,"not_mcafee"}, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OnOrderConfirmationPage_PointsDeducted() throws Exception {

		points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
				pricingDetails.points);
		Navigation.verify_pointsMatchExpected(points_After_Checkout);
	}

	@Test(priority = 72, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OnOrderConfirmationPage_ProductInformation()
			throws Exception {

		OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 73, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_OnOrderConfirmationPage_PricingDetails() throws Exception {

		PricingDetails pricingDetails_OrderConfirmPage;
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		} else {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		}
		OrderConfirmationPage.verify_PricingDetails(pricingDetails);
	}

	@Test(priority = 80, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_AccountHistory_Page() throws Exception {

		Navigation.navigate_Account();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
		} else {
			Account_History.verify_Purchase(orderID, pricingDetails.points);
		}
	}

	@Test(priority = 90, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_CancelOrder_Page_ShippingInfo() throws Exception {

		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, address1, city, state,
				country, zip);
	}

	@Test(priority = 91, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_CancelOrder_Page_ProductInfo() throws Exception {

		Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
	}

	@Test(priority = 100, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_Cancelled() throws Exception {

		Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
	}

	@Test(priority = 110, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_Points_Reimbursed() throws Exception {

		Navigation.verify_pointsMatchExpected(points_Before_Checkout);
	}

	@Test(priority = 120, groups = {"not_jeepwave", "not_citi", "not_tvc", "regression_Tests", "all_tests","not_redesign", "not_aarpredesign","not_mcafee" }, enabled=true)
	public void PT43_Step1To3_PromoCode_OfferList_Validate_AccountHistory_OrderCancelled() throws Exception {

		Navigation.navigate_Account();
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase_Cancelled(pricingDetails.points);
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();

	}
}
