package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HomePage_Sweepstakes_Tests extends BaseTest {

	String randomNum = BaseUI.random_NumberAsString(50001, 60000);
	String sweepstakesName = "autDAF" + randomNum;
	String sweepstakesDescription = sweepstakesName;
	Boolean isFeaturedSweepstakes = true;
	String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
	String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
	String endDate_FormattedHomePage = BaseUI.getDateAsString_InRelationToTodaysDate(2,"MM/dd/yy");
	String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
	String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
	String ticketCost = "2.0";
	String cumulativeCap = "100";
	String docID = "385";
	String productSku = "ssdemochevycamerored";
	String position = "1";
	String dCash = "0";
	String quantity = "0";
	String numDays_Claim = "2";
	String ticketQty = "1";
	String ticketLimit = "1000";
	String option = "Ticket Purchase";
	private String retailValue = "$24,005";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();

		Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
		Navigation_Admin.navigate_Sweepstakes_CreateProgram();

		AddNewSweepstakes_Admin.Add_NewSweepstakes(sweepstakesName, sweepstakesDescription,
				Client_Info_Admin.return_ProgramIDs(), isFeaturedSweepstakes, startDate, endDate, drawingDate,
				displayEndDate, ticketCost, cumulativeCap, docID, productSku, position, dCash, quantity, numDays_Claim,
				ticketQty, ticketLimit, option);

		// validate created sweepstakes displays in admin site
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();

		// validate created sweepstakes active
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.click_ActivateButton();
		Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_Sweepstakes_IsActivated();

		// launch memcache flush to update sweepstakes
		CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_redesign", "not_tvc", "not_citi" })
	public void PT3214_Step8_PT4778_Step1_HomePage_VerifySweepstakesTitleOnHomePage() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			Navigation.navigate_SweepstakesLink_currentSweepstakes();
			BaseUI.verifyElementAppears(Locator.lookupElement("verify_CurrentSweepstakesPageTitle"));
			BaseUI.verifyElementHasExpectedText("verify_CurrentSweepstakesPageTitle", "Current Sweepstakes");
		} else if (ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)) {
			BaseUI.verifyElementAppears(Locator.lookupElement("verify_Sweepstakes_headerOnHomePage"));
			BaseUI.verifyElementHasExpectedText("verify_Sweepstakes_headerOnHomePage", "Sweepstakes");
		} else {
			BaseUI.verifyElementAppears(Locator.lookupElement("verify_Sweepstakes_headerOnHomePage"));
			BaseUI.verifyElementHasExpectedText("verify_Sweepstakes_headerOnHomePage", "Sweepstakes");
		}
	}

	@Test(groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_redesign", "not_tvc", "not_citi" })
	public void PT3214_Step8_PT4778_Step1_HomePage_verifySweepstakesDetailsOnHomePage() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.SmartRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)) {
			Navigation.navigate_SweepstakesLink_currentSweepstakes();
			BaseUI.verifyElementAppears(Locator.lookupElement("sweepstakes_Header"));
			BaseUI.verifyElementHasExpectedText("sweepstakes_Header", "Featured Sweepstakes");
		} else if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)){
			BaseUI.verifyElementAppears(Locator.lookupElement("homePage_Sweepstakes_EndsOnDate_Text"));
			BaseUI.verifyElementHasExpectedPartialText("homePage_Sweepstakes_EndsOnText", "Ends on");
			BaseUI.verifyElementHasExpectedPartialText("verify_Sweepstakes_ValuedAt_Value", retailValue);
		} else {
			BaseUI.verifyElementHasExpectedText("verify_Sweepstakes_EndsIn_Text", "Ends In:");
			BaseUI.verifyElementHasExpectedText("verify_Sweepstakes_ValuedAt_Text", "Valued At:");
		}
	}

	@Test(groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_redesign", "not_tvc", "not_citi" })
	public void PT3214_Step8_PT4778_Step1_HomePage_clickSweepstakesSeeDetailsButton_AndVerifySweepstakesDetails_SweepstakesPage()
			throws Exception {
		Sweepstakes.verifySweepstakesDetails();
	}

	@Test(groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_redesign", "not_tvc", "not_citi", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step8_PT4778_Step1_HomePage_VerifySweepstakesDates_SweepstakesPage() throws Exception {
		Navigation.navigate_SweepstakesLink_currentSweepstakes();
		Sweepstakes.verifySweepstakes_BeginEnd_DrawingDates();
	}

	@Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_redesign", "not_tvc", "not_citi" })
	public void PT3214_Step8_PT4778_Step1_HomePage_VerifySweepstakesSKU_RetailPriceOver600() throws Exception {
		Sweepstakes.verifySweepstakesSku_RetailsPriceOver600();
	}
	
	@Test(groups = { "regression_Tests", "not_jeepwave", "all_tests", "not_redesign", "not_aarpredesign", "not_tvc", "not_citi","not_mcafee" })
	public void PT26_HomePage_Step2_VerifyFeaturedSweepstakes() throws Exception {
		Sweepstakes.verify_FeaturedSweepstakes_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
				Browser.closeBrowser();
				Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
			} else {
				Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
			}
			LoginPage_Admin.login_Admin();
			Navigation_Admin.navigate_Sweepstakes_SearchProgram();
			Sweepstakes_SearchSweepstakesProgramSearch_Admin.search_For_AndThen_UpdateEndTime(sweepstakesName, BaseUI.getDateAsString_InRelationToTodaysDate(-1));
		} finally {
			Browser.closeBrowser();
		}
	}
}
