package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Homepage_DailyCategory_CarouselTests extends BaseTest {

	private String attribute = "13108";
	private String attributeValueSet = "False";

	@BeforeClass(alwaysRun = true)
	public void setup_Method() throws Exception {
		
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage_Admin.login_Admin();
		if(ClientDataRetrieval.isLegacyClient()) {
			Navigation_Admin.navigateExpandCategory_SelectClientProgramId();
		} else {
			Navigation_Admin.navigateExpandCategory_SelectClientProgramId_CheckAttributeSetToTrue(attribute, attributeValueSet);
		}
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart",
			"not_tvc", "not_streetwise" })
	public void PT3214_Step19_PT4783_Step1_HomePage_Verify_DailyCategorySpecial_Header_Appears() throws Exception {
		Homepage.verify_DailyCarousel_Header_Appears();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", 
			"not_tvc", "not_streetwise" ,"not_mcafee"})
	public void PT3214_Step19_HomePage_Verify_DailyCategorySpecial_SeeMoreLink_Appears() throws Exception {
		Homepage.verify_DailyCarousel_SeeMoreLink_Appears();

	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step19_PT4783_Step1_HomePage_Verify_DailyCategorySpecial_RightNavigationArrow_Appears() throws Exception {
		Homepage.verify_DailyCarousel_RightNavigationArrow_Appears();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step19_PT4783_Step1_HomePage_Verify_DailyCategorySpecial_LeftNavigationArrow_Appears() throws Exception {
		Homepage.verify_DailyCarousel_LeftNavigationArrow_Appears();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step19_PT4783_Step1_HomePage_DailyCategorySpecial_Carousel_Click_Entire_RightNavigationArrow()
			throws Exception {
		Homepage.verify_DailyCategoryCarousel_Clicks_EntireRightNavigation_Arrow();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step19_PT4783_Step1_HomePage_DailyCategorySpecial_Carousel_Click_Entire_LeftNavigationArrow()
			throws Exception {
		Homepage.verify_DailyCategoryCarousel_Clicks_EntireLeftNavigation_Arrow();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step19_PT4783_Step1_HomePage_Verify_DailyCategorySpecial_ClickFirstItem_NavigatesTo_LandingPage()
			throws Exception {
		Homepage.verify_DailyCategoryCarousel_ClickingFirstItem_NavigatesTo_LandingPage();
	}

	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_smart", 
			"not_tvc", "not_citi", "not_aarp", "not_streetwise","not_mcafee" })
	public void PT3214_Step19_HomePage_Verify_DailyCategorySpecial_CategoryList_Exists_LandingPage() throws Exception {
		if (BaseUI.elementExists("homepage_DailyCatCarousel_Modal", null, null)) {
			Homepage.verify_CategoryDisplayedInDailyCategoryCarousel_Exists_In_SeeMorePage();
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
