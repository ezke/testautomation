package dRewards.tests_regression;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account_History;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class LocalOffer_RepeatSavings_RedeemOffer_ByOfferName extends BaseTest {

	// This test is restructured test for LocalOffer_RepeatSavings_RedeemOffer
	// because of Server connection error we were getting

	String pointsBefore;
	String pointsAfter;
	String pointsRedeemed;
	String transactionDescription;
	String localOfferName = "Smoothie King";
	String localOfferPoints;
	String category = "Dining";
	String zipcode = "33304";
	TableData localOfferData;
	String currentWindow;
	String expectedHeader = "";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		Navigation.navigate_LocalOffers_SearchPage();
		LocalOffers_LandingPage.localOffer_Search_WithZipcode(zipcode, category);
		if (ClientDataRetrieval.isRedesignClient()) {
			LocalOffers_LandingPage.clickZipcode_GetDealsBtn();
		} else {
			Navigation.click_SeeLocalOffer_SearchPageButton();
		}		
	}

	@Test(priority = 10, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_Offer() throws Exception {

		pointsBefore = Navigation.returnPoints();

		LocalOffers_LandingPage.redeem_RepeatSaving_Offer_ByOfferName(localOfferName, category);
		BaseUI.verifyElementHasExpectedText("localOffer_SearchResult_Redeem_DialogBox_Text", "Ready to Print?");
	}

	@Test(priority = 20, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests" ,"not_mcafee"})
	public void PT3247_LocalOffer_Redeem_RepeatSavings_Verify_Confirm_Redeemed() throws Exception {
		LocalOffers_LandingPage.verify_RepeatSaving_Redeemed(pointsBefore);
	}

	@Test(priority = 30, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyPoints() throws Exception {
		Navigation.navigate_Account();
		transactionDescription = BaseUI.getTextFromField(Locator.lookupElement("accnt_Row1_TransactionDescription"));
		Account_History.verify_LocalOffer_PointsRedeemed(LocalOffers_LandingPage.pointsRedeemed);
	}

	@Test(priority = 40, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetailsLink() throws Exception {
		Account_History.verify_LocalOffer_ViewDetailLink_ByOfferName(localOfferName);
	}

	@Test(priority = 50, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_HeaderTitle() throws Exception {

		currentWindow = Browser.driver.getWindowHandle();
		Account_History.click_LocalOffer_ViewDetail_ByOfferName(localOfferName);
		Account_History.verify_LocalOffer_ViewDetail_HeaderTitle();
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_ClientLogo() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("accnt_LocalOffer_Logo"));
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_HeaderMessage() throws Exception {
		Account_History.verify_LocalOffer_ViewDetail_HeaderMessage(LocalOffers_LandingPage.pointsRedeemed);
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests" ,"not_mcafee"})
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_OrderDate() throws Exception {
		Account_History.verify_LocalOffer_ViewDetail_OfferRedeemedDate();
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_CouponOffer() throws Exception {
		Account_History.verify_LocalOffer_ViewDetail_CouponOffer_Message();
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_Points() throws Exception {
		Account_History.verify_LocalOffer_ViewDetail_Points(LocalOffers_LandingPage.pointsRedeemed);
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_MerchantName() throws Exception {
		Account_History.verify_LocalOffer_ViewDetail_MerchantName(transactionDescription);
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_AccountPage_VerifyViewDetails_MerchantAddress()
			throws Exception {
		Account_History.verify_LocalOffer_ViewDetail_MerchantAddress();
	}

	@Test(priority = 70, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_RedeemSameOffer_SetToLocalOfferPage_VerifyBreadcrumb() throws Exception {
		BaseUI.close_ExtraWindows(currentWindow);
		// setting up to redeem same offer again
		String[] breadcrumbs = { "Home", "Local Offers", category };
		Navigation.navigate_LocalOffers_SearchPage();
		LocalOffers_LandingPage.localOffer_Search_WithZipcode(zipcode, category);
		Navigation.click_SeeLocalOffer_SearchPageButton();
		Navigation.verify_BreadcrumbList(breadcrumbs);
	}

	@Test(priority = 80, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_RedeemSameOffer_Again_VerifyRedeemDialogBoxText() throws Exception {
		pointsBefore = Navigation.returnPoints();

		LocalOffers_LandingPage.redeem_RepeatSaving_Offer_ByOfferName(localOfferName, category);
		BaseUI.verifyElementHasExpectedText("localOffer_SearchResult_Redeem_DialogBox_Text", "Ready to Print?");
	}

	@Test(priority = 90, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests","not_mcafee" })
	public void PT3247_LocalOffer_Redeem_RepeatSavings_Verify_Confirm_Redeemed_Again() throws Exception {
		LocalOffers_LandingPage.verify_RepeatSaving_Redeemed(pointsBefore);
	}

	@Test(priority = 100, groups = { "regression_Tests", "not_redesign", "not_aarpredesign", "critical_Tests", "all_tests" ,"not_mcafee"})
	public void PT3247_LocalOffer_Redeem_RepeatSavings_RedeemSameOffer_Again_AccountPage_VerifyPoints()
			throws Exception {
		Navigation.navigate_Account();
		Account_History.verify_LocalOffer_PointsRedeemed(LocalOffers_LandingPage.pointsRedeemed);
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}
		Browser.closeBrowser();
	}
}
