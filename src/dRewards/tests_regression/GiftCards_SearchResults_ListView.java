package dRewards.tests_regression;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GiftCards_LandingPage;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class GiftCards_SearchResults_ListView extends BaseTest {
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_GiftCard_All();
		GiftCards_LandingPage.click_ListView_Button();
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step5_Verify_Dining_SearchText_IsValid() throws Exception {

		String dinningSearch = "applebee";
		Homepage.verify_MerchandiseSearchText_Is_Valid(dinningSearch);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step5_Verify_Entertainment_SearchText_IsValid() throws Exception {

		String entertainmentSearch = "regal";
		Homepage.verify_MerchandiseSearchText_Is_Valid(entertainmentSearch);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step5_Verify_Shopping_SearchText_IsValid() throws Exception {

		String entertainmentSearch = "macy";
		Homepage.verify_MerchandiseSearchText_Is_Valid(entertainmentSearch);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee"})
	public void PT3242_GiftCardsPage_Step5_Verify_Travel_SearchText_IsValid() throws Exception {

		String travelSearch = "royal";
		Homepage.verify_MerchandiseSearchText_Is_Valid(travelSearch);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_SortBy_NewArrivals() throws Exception {

		GiftCards_LandingPage.sortBy("New Arrivals");
		TableData newList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(newList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3242_GiftCardsPage_Step3_SortBy_TopPicks() throws Exception {

		GiftCards_LandingPage.sortBy("New Arrivals");
		GiftCards_LandingPage.sortBy("Top Picks");
		TableData newList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(newList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_SortBy_NameAToZ() throws Exception {

		GiftCards_LandingPage.sortBy("Name A to Z");
		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Ascending("giftcards");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_SortBy_NameZToA() throws Exception {

		GiftCards_LandingPage.sortBy("Name Z to A");
		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Descending("giftcards");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3242_GiftCardsPage_Step3_SortBy_YouPay_LowToHigh() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			GiftCards_LandingPage.sortBy("Your Price - Low to High");
		} else {
			GiftCards_LandingPage.sortBy("You Pay - Low to High");
		}

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Ascending("youPay");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_SortBy_YouPay_HighToLow() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			GiftCards_LandingPage.sortBy("Your Price - High to Low");
		} else {
			GiftCards_LandingPage.sortBy("You Pay - High to Low");
		}

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Descending("youPay");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign", "not_citi" ,"not_mcafee"})
	public void PT3242_GiftCardsPage_Step3_SortBy_Points_LowToHigh() throws Exception {

		String sortBy = ClientDataRetrieval.client_Currency + " - Low to High";
		GiftCards_LandingPage.sortBy(sortBy);

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Ascending("points");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" , "not_citi","not_mcafee"})
	public void PT3242_GiftCardsPage_Step3_SortBy_Points_HighToLow() throws Exception {

		String sortBy = ClientDataRetrieval.client_Currency + " - High to Low";
		GiftCards_LandingPage.sortBy(sortBy);

		TableData sortList = GiftCards_LandingPage.return_GiftCards_TableData();

		BaseUI.verify_true_AndLog(sortList.data.size() > 0, "Found Gift Cards.", "Did NOT Gift Cards.");
		sortList.verify_Column_Sorted_Numeric_Descending("points");
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Verify_ResultPerPage_24PerPage() throws Exception {

		String resultPerPage = "24";

		GiftCards_LandingPage.verify_resultPerPage(resultPerPage);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Verify_ResultPerPage_12PerPage() throws Exception {

		String resultPerPage1 = "24";
		GiftCards_LandingPage.resultPerPage(resultPerPage1);

		String resultPerPage = "12";
		GiftCards_LandingPage.verify_resultPerPage(resultPerPage);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Verify_ResultPerPage_48PerPage() throws Exception {

		String resultPerPage1 = "24";
		GiftCards_LandingPage.resultPerPage(resultPerPage1);

		String resultPerPage = "48";
		GiftCards_LandingPage.verify_resultPerPage(resultPerPage);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page2() throws Exception {

		Integer page = 2;
		GiftCards_LandingPage.navigate_ToPage_ByTOP_PageNumber(page);
		GiftCards_LandingPage.verify_Page_Active(page);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page2_UsingNextTOPButton() throws Exception {

		Integer page = 2;
		GiftCards_LandingPage.navigate_NextPage_ByTOP_Link();
		;
		GiftCards_LandingPage.verify_Page_Active(page);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page1_UsingPreviousTOPButton() throws Exception {

		Integer page = 1;
		GiftCards_LandingPage.navigate_NextPage_ByTOP_Link();
		GiftCards_LandingPage.navigate_PreviousPage_ByTOP_Link();
		GiftCards_LandingPage.verify_Page_Active(page);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page2_ByBottomPageLink() throws Exception {

		Integer page = 2;
		GiftCards_LandingPage.navigate_ToPage_ByBOTTOM_PageNumber(page);
		GiftCards_LandingPage.verify_Page_Active(page);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page2_UsingNext_BottomButton() throws Exception {

		Integer page = 2;
		GiftCards_LandingPage.navigate_NextPage_ByBOTTOM_Link();
		GiftCards_LandingPage.verify_Page_Active(page);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page1_UsingPrevious_BottomButton() throws Exception {

		Integer page = 1;
		GiftCards_LandingPage.navigate_NextPage_ByBOTTOM_Link();
		GiftCards_LandingPage.navigate_PreviousPage_ByBOTTOM_Link();
		GiftCards_LandingPage.verify_Page_Active(page);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Navigate_To_Page3_ByTopLink_NoDuplicateEntries() throws Exception {

		Integer page = 3;

		TableData page1Results = GiftCards_LandingPage.return_GiftCards_TableData();
		BaseUI.verify_true_AndLog(page1Results.data.size() > 0, "Found results on page 1.",
				"Found NO results on page 1.");
		GiftCards_LandingPage.navigate_ToPage_ByTOP_PageNumber(page);

		TableData page3Results = GiftCards_LandingPage.return_GiftCards_TableData();
		BaseUI.verify_true_AndLog(page1Results.data.size() > 0, "Found results on page 3.",
				"Found NO results on page 3.");
		for (HashMap<String, String> page1row : page1Results.data) {
			for (HashMap<String, String> page3Row : page3Results.data) {
                BaseUI.verify_false_AndLog(page1row.equals(page3Row), page1row + " Rows did NOT match. "+ page3Row,
                        page1row+" Found duplicate entry from page 1. "+ page3Row);
			}
		}
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3242_GiftCardsPage_Step3_Click_BackToTopButton() throws Exception {

		GiftCards_LandingPage.verify_BackToTopButton();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (ClientDataRetrieval.client_Matches(client_Designation.AARP)) {
			Navigation.navigate_Home_viaHomeLink();
			Navigation.navigate_GiftCard_All();
		} else {
			Navigation.navigate_Home_viaLogo();
			Navigation.navigate_GiftCard_All();
		}
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		try {

		} finally {
			Browser.closeBrowser();
		}
	}

}
