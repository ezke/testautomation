package dRewards.tests_regression;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.DailyDeals;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DailyDeals_ImageAndDescription_Validations extends BaseTest {

	HashMap<String, String> homepagedailyDealData;
	HashMap<String, String> dailyDeal_Data;
	ArrayList<String> imagesOnSlidesList;
	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all items from Shopping Cart.
		Homepage.deleteItemsFromShoppingCart();

		homepagedailyDealData = Homepage.clickDailyDeal();
		dailyDeal_Data = DailyDeals.return_DailyDealInfo();
	}
	
	@Test(priority = 10, groups = { "critical_Tests", "regression_Tests", "all_tests" })
	public void PT3956_Validate_DailyDeals_Info_On_DailyDealPage() throws Exception {
		
		DailyDeals.verify_DailyDealInfo(homepagedailyDealData, dailyDeal_Data);
	}
	
	
	@Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Step2_Validate_DailyDealsHeaderTitle_On_DailyDealPage() {
		WebElement dailyDealsHeaderTitleElement = Locator.lookupRequiredElement("dailyDeals_HeaderTitle");
		String actualDailyDealsHeaderTitle = BaseUI.getTextFromField(dailyDealsHeaderTitleElement);
		BaseUI.verifyElementAppears(dailyDealsHeaderTitleElement);
		BaseUI.baseStringPartialCompare("dailyDeals_HeaderTitle", "Daily Deal", actualDailyDealsHeaderTitle);
	}
	
	
	@Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Step2_Validate_DailyDealsStartTime_On_DailyDealPage() {
		WebElement dailyDealsStartTimeElement = Locator.lookupRequiredElement("dailyDeals_HeaderTitle_StartTime");
		String actualDailyDealsHeaderTitle_StartTime = BaseUI.getTextFromField(dailyDealsStartTimeElement);
		BaseUI.verifyElementAppears(dailyDealsStartTimeElement);
		BaseUI.baseStringCompare("dailyDeals_HeaderTitle_StartTime", "New item daily at 3PM ET", actualDailyDealsHeaderTitle_StartTime);
	}
	
	
	@Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Step2_Validate_DailyDeals_AvailableStatus_On_DailyDealPage() {
		WebElement availableStatusElement = Locator.lookupRequiredElement("dailyDeals_Available_Status");
		String actualAvailableStatusText = BaseUI.getTextFromField(availableStatusElement);
		BaseUI.verifyElementAppears(availableStatusElement);
		BaseUI.baseStringCompare("dailyDeals_Available_Status", 
				"This item is still AVAILABLE."+"\n"+"Hurry, quantities are limited!", actualAvailableStatusText);
	}
	
	
	@Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Step2_Validate_DailyDeals_DealExpiresText_On_DailyDealPage() {
		WebElement dealExpiresTextElement = Locator.lookupRequiredElement("dailyDeals_DealExpireText");
		String actualDealExpiresText = BaseUI.getTextFromField(dealExpiresTextElement);
		BaseUI.verifyElementAppears(dealExpiresTextElement);
		BaseUI.baseStringPartialCompare("dealExpiresText", 
				"This deal expires in:"+"\n"+BaseUI.getTextFromField(Locator.lookupRequiredElement("dailyDeals_EndTime")), actualDealExpiresText);
	}
	
	
	@Test(priority = 16, groups = { "critical_Tests", "regression_Tests", "all_tests" })
	public void PT3956_Validate_DailyDeals_Image_Appears() {
		
		DailyDeals.verify_Image_Appears();
	}
	
	
	@Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Validate_DailyDeals_ShippingType_On_DailyDealPage() {
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_ShippingType"))) {
			WebElement dealShippingTypeElement = Locator.lookupRequiredElement("dailyDeals_ShippingType");
			String actualDailyDeals_ShippingTypesText = BaseUI.getTextFromField(dealShippingTypeElement);
			BaseUI.verifyElementAppears(dealShippingTypeElement);
			BaseUI.baseStringCompare("Free Shipping Text", "Free Shipping", actualDailyDeals_ShippingTypesText);
		}
	}
	
	
	@Test(priority = 21, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Validate_DailyDeals_Image_LeftNavigationArrow_Appears() throws Exception {
		WebElement leftNavigationArrow = Locator.lookupRequiredElement("dailyDeals_Images_LeftNavigationArrow");
		
		//Left Navigation arrow is not always present for every daily deal, 
		//so checking before validation
		if(BaseUI.elementAppears(leftNavigationArrow)){		
			BaseUI.scroll_to_element(leftNavigationArrow);
			BaseUI.verifyElementAppears(leftNavigationArrow);
		}	
	}
	
	@Test(priority = 21, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Validate_DailyDeals_Image_RightNavigationArrow_Appears() throws Exception {
		WebElement rightNavigationArrow = Locator.lookupRequiredElement("dailyDeals_Images_RightNavigationArrow");
		
		//Right Navigation arrow is not always present for every daily deal, 
		//so checking before validation
		if(BaseUI.elementAppears(rightNavigationArrow)){	
			BaseUI.scroll_to_element(rightNavigationArrow);
			BaseUI.verifyElementAppears(rightNavigationArrow);
		}	
	}
	
	
	@Test(priority = 22, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Validate_DailyDeals_AllImage_OnSlide_Appears() throws Exception {
		imagesOnSlidesList = DailyDeals.return_DailyDeals_AllImagesDisplayed_OnSlides();
		BaseUI.verify_true_AndLog(imagesOnSlidesList.size() > 0, "Images are DISPLAYED ON SLIDE: "+ imagesOnSlidesList.size(), 
				"Images are NOT DISPLAYED ON SLIDE: "+ imagesOnSlidesList.size());
	}
	
	@Test(priority = 23, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Validate_DailyDeals_AllImage_OnSlide_ClickEntire_RightNavigationArrow() throws Exception {
		//getting this image array list at class level because not all Daily deals have images
		//And if they have images it may or may not have image carousel.
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_AllImages_OnSlides"))) {
			imagesOnSlidesList = DailyDeals.return_DailyDeals_AllImagesDisplayed_OnSlides();
			
			if(imagesOnSlidesList.size() > 4) {
				if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_Images_RightNavigationArrow"))) {
					BaseUI.scroll_to_element(Locator.lookupRequiredElement("dailyDeals_AllImages_OnSlides_FirstItem_Displayed"));
					DailyDeals.click_DailyDealsImagesSlides_ToFarRight();
				}
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("dailyDeals_AllImages_OnSlides_FirstItem_Displayed"));
			}
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_AllImages_OnSlides_LastItem_Displayed"));
		} else {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("dailyDeals_AllImages_OnSlides"));
		}
	}

	@Test(priority = 24, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
	public void PT3751_Validate_DailyDeals_AllImage_OnSlide_ClickEntire_LeftNavigationArrow() throws Exception {
		//getting this image array list at class level because not all Daily deals have images
				//And if they have images it may or may not have image carousel.
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_AllImages_OnSlides"))) {
			imagesOnSlidesList = DailyDeals.return_DailyDeals_AllImagesDisplayed_OnSlides();

			if(imagesOnSlidesList.size() > 4) {
				if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_Images_RightNavigationArrow"))) {
					BaseUI.scroll_to_element(Locator.lookupRequiredElement("dailyDeals_AllImages_OnSlides_FirstItem_Displayed"));
					DailyDeals.click_DailyDealsImagesSlides_ToFarLeft();
				}
				BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("dailyDeals_AllImages_OnSlides_LastItem_Displayed"));	
			}
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_AllImages_OnSlides_FirstItem_Displayed"));
		} else {
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("dailyDeals_AllImages_OnSlides"));
		}	
	}
	
	@Test(priority = 25, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee"})
	public void PT3956_Validate_DailyDeals_ViewLargerImage_LinkAppears() {
		
		DailyDeals.verify_ViewLargerImage_Link_Appears();
	}

	
	@Test(priority = 26, groups = { "critical_Tests", "regression_Tests", "all_tests" })
	public void PT3956_Validate_DailyDeals_Description_Text_Appears() {
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)
				|| ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign)
				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)
				|| ClientDataRetrieval.client_Matches(client_Designation.McAfee)
				|| ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)
				|| ClientDataRetrieval.client_Matches(client_Designation.JeepwaveRewards)
				|| ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("dailyDeals_Description"), "Description");
		} else {
			BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("dailyDeals_Description"), "DESCRIPTION");
		}
	}

	@Test(priority = 27, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3956_Validate_DailyDeals_MoreLink_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("dailyDeals_Description_More_Link"));
	}

	@Test(priority = 28, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3956_Validate_DailyDeals_LessLink_Appears() throws Exception {
		DailyDeals.verify_LessLink_Appears();
	}

	@Test(priority = 29, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3956_Validate_DailyDeals_ViewLargerImage_PopupAppears() throws Exception {

		DailyDeals.verify_ViewLargerImage_Popup(dailyDeal_Data.get("dailyDealName"));
	}

	@Test(priority = 30, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" },
			dependsOnMethods = {"PT3956_Validate_DailyDeals_ViewLargerImage_PopupAppears"})
	public void PT3956_Validate_DailyDeals_ViewLargerImage_Popup_DoesNot_Appear() throws Exception {
		DailyDeals.close_ViewLargerImage_Popup();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("dailyDeals_ViewLargerImage_Popup"));
	}

	@Test(priority = 31, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarpredesign", "not_redesign","not_mcafee"},
			dependsOnMethods = {"PT3956_Validate_DailyDeals_ViewLargerImage_PopupAppears"})
	public void PT3935_Validate_DailyDeals_AddToCart_Popup() throws Exception {
		DailyDeals.click_AddToShoppingCart();
		ShoppingCart.verify_DailyDeals_AddedToCart_Popup();
	}

	@Test(priority = 35, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarpredesign", "not_redesign","not_mcafee"},
			dependsOnMethods = {"PT3956_Validate_DailyDeals_ViewLargerImage_PopupAppears"})
	public void PT3935_Validate_DailyDeals_Item_InShoppingCart() throws Exception {
		ShoppingCart.close_DailyDeal_AddedToCart_Popup();
		String dailyDealName = dailyDeal_Data.get("dailyDealName").split("\\n")[0];
		ShoppingCart.verify_product_OnShoppingCartPage(dailyDealName);
	}

	@Test(priority = 36, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarpredesign", "not_redesign","not_mcafee"},
			dependsOnMethods = {"PT3956_Validate_DailyDeals_ViewLargerImage_PopupAppears"})
	public void PT3935_Validate_DailyDeals_Limit_One_PerCustomer_Message() throws Exception {
		Navigation.navigate_Merchandise_DailyDeals();
		DailyDeals.click_AddToShoppingCart();
		ShoppingCart.verify_DailyDeal_Limit_One_PerCustomer_Message(dailyDeal_Data);
	}

	@Test(priority = 40, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarpredesign", "not_redesign","not_mcafee"},
			dependsOnMethods = {"PT3956_Validate_DailyDeals_ViewLargerImage_PopupAppears"})
	public void PT3935_Validate_DailyDeals_Item_Removed_FromShoppingCart() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_ShoppingCart();
		}
		ShoppingCart.removeFirstItemFromShoppingCart();
		ShoppingCart.verify_CartEmptyText_Appears();
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		try {

		} finally {
			Browser.closeBrowser();
		}
	}
	
	
}
