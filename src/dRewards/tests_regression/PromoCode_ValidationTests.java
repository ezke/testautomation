package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.AddNewPromoCampaign_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class PromoCode_ValidationTests extends BaseTest {

    String firstCampaignName = "Automation_Test1_";
    String secCampaignName = "Automation_Test2_";
    String diffCampaignName = "Automation_Test3_";
    String rewardsCashAmount = "500";
    String transactionDescription = "Testing Automation Promo";
    String firstPromoCode = "Automation_";
    String secondPromoCode = "Automation1_";
    String diffClientPromoCode = "AutomationDiff_";
    String clientID = "611";
    private String programID = "9363";
    String diffClient_ClientID = "635";
    private String diffClient_ProgramID = "9474";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        String randomNumber = BaseUI.random_NumberAsString(0, 70000);
        firstCampaignName += randomNumber;
        firstPromoCode += randomNumber;

        secCampaignName += randomNumber;
        secondPromoCode += randomNumber;

        diffCampaignName += randomNumber;
        diffClientPromoCode += randomNumber;

        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();

        //Create 1st promo code
        Navigation_Admin.navigate_ClickNavLink("Create Promo Codes");
        AddNewPromoCampaign_Admin.add_Promo(firstCampaignName, rewardsCashAmount, transactionDescription, firstPromoCode);
        AddNewPromoCampaign_Admin.verify_AddPromo_Successful();

        //Create second promo code
        Navigation_Admin.navigate_ClickNavLink("Create Promo Codes");
        AddNewPromoCampaign_Admin.add_Promo(secCampaignName, rewardsCashAmount, transactionDescription, secondPromoCode);
        AddNewPromoCampaign_Admin.verify_AddPromo_Successful();

        //Create PromoCode for different client
        //Create second promo code
        Navigation_Admin.navigate_ClickNavLink("Create Promo Codes");
        if(ClientDataRetrieval.client_Matches(ClientDataRetrieval.client_Designation.AARPRedesign)) {
            AddNewPromoCampaign_Admin.add_Promo(diffCampaignName, rewardsCashAmount, transactionDescription, diffClientPromoCode,
                    diffClient_ClientID, diffClient_ProgramID);
        } else {
            AddNewPromoCampaign_Admin.add_Promo(diffCampaignName, rewardsCashAmount, transactionDescription, diffClientPromoCode,
                    clientID, programID);
        }
        AddNewPromoCampaign_Admin.verify_AddPromo_Successful();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }
    

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step2_ValidPromoCode() throws Exception {
        Navigation.validate_PromoCode_Valid(firstPromoCode, rewardsCashAmount, transactionDescription);
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step3_ValidPromoCode_NotCaseSensitive() throws Exception {
        Navigation.validate_PromoCode_Valid(secondPromoCode.toUpperCase(), rewardsCashAmount, transactionDescription);
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step4_ValidExpiredPromoCode() throws Exception {
        Navigation.enter_PromoCode("Automation_");
        Homepage.verifyPromoCode_OopsMessageOnPopUpModal("Automation_");
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step5_ValidPromoCode_EnteredSecondTime_DoesNotWork() throws Exception {
        Navigation.validate_PromoCode_CannotBeUsedTwice(firstPromoCode);
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step6_VerifyUserNotAbleToRedeem_DifferentPID_PromoCode() throws Exception {
        Navigation.enter_PromoCode(diffClientPromoCode);
        Homepage.verifyPromoCode_OopsMessageOnPopUpModal(diffClientPromoCode);
    }

    @Test(priority = 35, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step7_InvalidPromoCode_DoesNotExist() throws Exception {
        String invalidCode = "afsdfewrewrwerg";
        Navigation.validate_PromoCode_NotACode(invalidCode);
    }

    @Test(priority = 40,groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step7_InvalidPromoCode_WindowCloses_AfterClickingClose() throws Exception {
        String invalidCode = "afsdfewrewrwerg";
        Navigation.validate_PromoWindow_Closes(invalidCode);
    }

    @Test(priority = 45,groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4469_PT4627_Step8_InvalidPromoCode_WindowCloses_AfterClickingOutside_PromoCodeModal() throws Exception {
        String invalidCode = "afsdfewrewrwerg";
        Navigation.validate_PromoWindow_Closes_WhenBrowserRefresh(invalidCode);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        Navigation.navigate_Home_viaLogo();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
