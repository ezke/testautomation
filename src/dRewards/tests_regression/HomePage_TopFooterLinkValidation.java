package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HomePage_TopFooterLinkValidation extends BaseTest {

    String currentWindowHandle;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }


    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4776_Step1_HomePage_VerifyLogo() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" ,"not_mcafee"})
    public void PT4776_Step1_HomePage_VerifyEarnMoreLink() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_EarnMoreLink"));
        BaseUI.verifyElementHasExpectedText("homepage_EarnMoreLink", "Earn More");
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign" ,"not_mcafee"})
    public void PT4776_Step1_HomePage_VerifyRewardsForGoodLink() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_RewardsForGoodLink"));
        BaseUI.verifyElementHasExpectedText("homepage_RewardsForGoodLink", "Rewards for Good");
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4776_Step1_HomePage_VerifyUserDropDownList() throws Exception {
        Homepage.verify_AarpUser_Dropdown();
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4776_Step1_HomePage_VerifyEarnMore_LandingPage_HeaderAndUrl() throws Exception {
        currentWindowHandle = Browser.driver.getWindowHandle();
        BaseUI.ClickAndSwitchWindow(Locator.lookupElement("homepage_EarnMoreLink"), true, 1000);
        
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_EarnMoreLink_LandingPage_Header"));
        BaseUI.verifyElementHasExpectedText("homepage_EarnMoreLink_LandingPage_Header", "Earn Points");

        String earnPointsLandingPage_Url = Browser.driver.getCurrentUrl();
        String expectedUrl = "https://www.aarp.org/rewards-for-good/earn-points/";
        BaseUI.verify_true_AndLog(earnPointsLandingPage_Url.equals(expectedUrl), "Earn Points landing page URL MATCH",
                "Earn Points landing page URL DOES NOT MATCH");
    }

    @Test(priority = 60, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4776_Step1_HomePage_VerifyRewardsForGood_LandingPage_HeaderAndUrl() throws Exception {
        BaseUI.switch_ToDefaultContent();
        BaseUI.close_ExtraWindows(currentWindowHandle);
        BaseUI.ClickAndSwitchWindow(Locator.lookupElement("homepage_RewardsForGoodLink"), true, 1000);

        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_RewardsForGoodLink_LandingPage_Header"));
        BaseUI.verifyElementHasExpectedText("homepage_RewardsForGoodLink_LandingPage_Header", "Rewards for Good");

        String rewardsForGoodLandingPage_Url = Browser.driver.getCurrentUrl();
        String expectedUrl = "https://www.aarp.org/rewards-for-good/";
        BaseUI.verify_true_AndLog(rewardsForGoodLandingPage_Url.equals(expectedUrl), "Rewards for Good landing page URL MATCH",
                "Rewards for Good landing page URL DOES NOT MATCH");
    }

    @Test(priority = 70, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi", "not_redesign","not_mcafee" })
    public void PT4776_Step1_HomePage_VerifyHomePage_SearchIcon() throws Exception {
        BaseUI.switch_ToDefaultContent();
        BaseUI.close_ExtraWindows(currentWindowHandle);
        BaseUI.verifyElementAppears(Locator.lookupElement("homepage_SearchIcon"));
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {

        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
