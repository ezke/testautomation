package dRewards.tests_regression;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.ClassObjects.PricingDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pageControls.PriceSummary;
import dRewards.pages.Account;
import dRewards.pages.Account_History;
import dRewards.pages.Account_OrderAndShippingDetails_Page;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.DailyDeals;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.OrderConfirmationPage;
import dRewards.pages.ShoppingCart;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.ProductCatalog_CatalogPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class DailyDeals_Validations_StatusUpdate extends BaseTest {

	WebDriver originalDriver;
	WebDriver secondBidderDriver;
	String original_Browser;
	// Variables for Shipping Info and same values used for Billing.
	String firstName = "QA";
	String lastName = "Auto";
	String address1 = "1225 Broken Sound";
	String city = "Boca Raton";
	String state = "FL";
	String zip = "33487";
	String phone = "9544151992";

	String ccType = "MasterCard";
	String ccNumber = "5105105105105100";
	String cvvCode = "211";
	String expire_month = "12";
	String expire_year = "22";
	String country = "United States";
	String inventoryCount = null;

	// Variables for product info/pay info
	TableData shoppingCartData;
	HashMap<String, String> homepagedailyDealData;
	HashMap<String, String> dailyDeal_Data;
	PricingDetails pricingDetails;
	String points_Before_Checkout;
	String points_After_Checkout;
	String orderID;
	private String salesTaxCost;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		original_Browser = Browser.currentBrowser;
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		// Logic to remove all items from Shopping Cart.
		Homepage.deleteItemsFromShoppingCart();

		// Logic to remove all Shipping Info
		//Navigation.navigate_Account_UpdateAddressBook();
		//Account_UpdateAddressBook.delete_Address();

		// Logic to remove all Billing Info.
		//Navigation.navigate_Account();
		//Account.clear_PaymentHistory();

		homepagedailyDealData = Homepage.clickDailyDeal();
		dailyDeal_Data = DailyDeals.return_DailyDealInfo();
	
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_AllDropdowns"))){
			throw new SkipException(
					"Skipping all the tests as Daily Deal is Multi Sku and can't verify SoldOut Scenario.");
		}
	}

	@Test(priority = 10, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart" })
	public void PT3935_Validate_DailyDeals_Info_On_DailyDealPage() throws Exception {

		DailyDeals.verify_DailyDealInfo(homepagedailyDealData, dailyDeal_Data);
	}

	@Test(priority = 20, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart" })
	public void PT3935_Validate_DailyDeals_Available_Active_Status() throws Exception {

		DailyDeals.verify_Available_ActiveStatus();
	}

	@Test(priority = 30, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Setup_DailyDeals_AdminSite_Special_Quantity_ToOne() throws Exception {

		originalDriver = Browser.driver;
		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		secondBidderDriver = Browser.driver;
		LoginPage_Admin.login_Admin();

		String productSku = ProductCatalog_CatalogPage.returnProductSku_ForSelectedDailyDeal(dailyDeal_Data.get("dailyDealName"));
		inventoryCount = ProductCatalog_CatalogPage.returnInventoryCountValue_ForSelectedProductSku(productSku);
		ProductCatalog_CatalogPage
				.update_DailyDeal_ProductSku_Quantity(dailyDeal_Data.get("dailyDealName"), "1");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("memcacheFlush_Successful"));

	}

	@Test(priority = 40, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_DailyDeals_InCarts_Active_Status() throws Exception {
		
		Browser.driver = originalDriver;
		
		//For Redesign client they have added SSO authentication, so if after logging in
		//no activity on site, it logs the user out and User login page is displayed.
	    //So if catalog upload and memcash flush takes more then given time. After returning to client url
		//validating Shopping cart button is visible, if not login with user credentials to continue.
		if(!BaseUI.elementAppears(Locator.lookupOptionalElement("dailyDeals_AddToShoppingCart_Button"))) {
			LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		}
		DailyDeals.click_AddToShoppingCart();
		ShoppingCart.close_DailyDeal_AddedToCart_Popup();
		Navigation.navigate_Merchandise_DailyDeals();
		DailyDeals.verify_InCarts_ActiveStatus();
	}

	@Test(priority = 50, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_ShoppingCart_DailyDeal_RetrieveInfo_AndClick_ProceedToCheckout() throws Exception {

		Navigation.navigate_ShoppingCart();
		pricingDetails = ShoppingCart.retrieve_PricingDetails();
		pricingDetails.set_SavingsAmount_Citi();
		shoppingCartData = ShoppingCart.get_Table_Data_Old();
		points_Before_Checkout = Navigation.returnPoints();
		ShoppingCart.click_ProceedToCheckout();
		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			if(!(pricingDetails.order_Total.equals("$0.00"))) {
				PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
			} else {
				BaseUI.log_Status("Daily Deal added is free product and Order Summary price details are not displayed");
				BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("priceSum_priceSummaryTitle"));
			}		
		} else {
			PriceSummary.verify_PriceSummary_Info_SalesTaxNOTPresent(pricingDetails);
		}	
	}

	@Test(priority = 60, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_DailyDeals_Continue_ToPage_AfterShippingInfo() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore) && 
				pricingDetails.order_Total.equals("$0.00")) {
			CheckoutPage_ShippingInfo.click_Checkout_ShippingAddress_ChangeBtn();
			CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, state, zip, phone);
			CheckoutPage_ShippingInfo.click_ContinueAdditionalInfoBtn();
		} else {
			if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
				if (BaseUI.elementAppears(Locator.lookupOptionalElement("checkout_ShipAddress_ChangeBtn"))) {
					CheckoutPage_ShippingInfo.click_Checkout_ShippingAddress_ChangeBtn();
				}
			} else {
				if (CheckoutPage_ShippingInfo.newShippingAddress_Radio_Exists()) {
					CheckoutPage_ShippingInfo.click_AddNewShippingAddress_Radio();
				}
			}

			CheckoutPage_ShippingInfo.add_newDeliveryAddress(firstName, lastName, address1, city, state, zip, phone);
			CheckoutPage_ShippingInfo.check_SaveForLater_Checkbox();
			CheckoutPage_ShippingInfo.click_ContinueTo_NextPage();

			pricingDetails.tax_Cost = PriceSummary.return_SalesTaxAsString();
			if(!(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore))){
				pricingDetails.add_Taxes_To_OrderTotal();
			}
		}	
	}

	@Test(priority = 70, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_citi", 
			"not_smart", "not_allstate", "not_aarp", "not_aarpredesign", "not_redesign", "not_tvc", "not_jeepwave", "not_streetwise","not_mcafee" })
	public void PT3935_Validate_AllStateCash_PriceInfo() throws Exception {
		if (pricingDetails.order_Total.equals("$0.00")) {
			BaseUI.log_Status("Daily Deal added to cart is free product");
			String expOrderTotal = BaseUI.getTextFromField(Locator.lookupRequiredElement("priceSum_orderTotal"));
			BaseUI.verify_true_AndLog(pricingDetails.tax_Cost.equals(expOrderTotal), 
					"Sales tax is $0.00", "Sales tax is not $0.00");
		} else if (shoppingCartData.data.get(0).get("Product Name").contains("Gift Card")) {
			PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);
		} else {
			PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
		}
	}

	@Test(priority = 80, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_citi", 
			"not_smart", "not_allstate", "not_aarp", "not_aarpredesign", "not_redesign", "not_tvc", "not_jeepwave", "not_streetwise","not_mcafee" })
	public void PT3935_Validate_AllStateCash_Continue_ToBillingPage() throws Exception {
		// Allstate Cash was used for this run through.
		if (pricingDetails.order_Total.equals("$0.00")) {
			pricingDetails.allstate_Cash = "$0.00";
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				pricingDetails.allstate_Cash = BaseUI
						.getTextFromField(Locator.lookupRequiredElement("checkout_AllStateCash_RadioButton_Text"));
				pricingDetails.allstate_Cash = pricingDetails.allstate_Cash.replace("in Cash Rewards", "").trim();
			}
			pricingDetails.subtract_AllstateCash_From_OrderTotal();

			CheckoutPage_AllStateCash.click_Continue_ToNextPage();
		}

	}

	@Test(priority = 85, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_BillingPage_PricingInfo() throws Exception {

		Boolean dailyDeal_AddedIsGiftCard = CheckoutPage_ReviewOrder.returnGiftCardProductNameAddedToCart(shoppingCartData);
		if (pricingDetails.order_Total.equals("$0.00") && 
				ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			BaseUI.log_Status("Daily Deal added to cart is free product");
			BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("checkout_PricingDetails_RetailPrice"));
		} else if (dailyDeal_AddedIsGiftCard) {
			PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
			}
		} else {
			if(pricingDetails.order_Total.equals("$0.00")) {
				BaseUI.verify_true_AndLog(pricingDetails.tax_Cost.equals("$0.00"), "Sales Tax was found and is equal to 0.",
						"Seeing " + pricingDetails.tax_Cost + " on page.");
			} else {
				PriceSummary.verify_PriceSummary_Info_SalesTaxPresent(pricingDetails);
			}
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				CheckoutPage_PaymentInfo.verify_AllstateCash(pricingDetails.allstate_Cash, pricingDetails.order_Total);
			}
		}
	}

	@Test(priority = 90, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderReviewPage_ProductDetails() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash) && pricingDetails.allstate_Cash != null
				&& !pricingDetails.allstate_Cash.equals("$0.00")) {
			CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
		} else if (!pricingDetails.order_Total.equals("$0.00")) {
			if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
				CheckoutPage_PaymentInfo.add_NewPaymentOption("Mastercard", ccNumber, expire_month, expire_year, cvvCode,
						firstName + " " + lastName, address1, country, city, "Florida", zip);
				CheckoutPage_ShippingInfo.click_Checkout_ExistingShippingAddress_Checkbox();
			} else {
				CheckoutPage_PaymentInfo.add_NewPaymentOption(ccType, ccNumber, expire_month, expire_year, cvvCode,
						firstName + " " + lastName, address1, country, city, state, zip);
			}			
			CheckoutPage_PaymentInfo.check_SaveBillingAddressForLater();
			CheckoutPage_PaymentInfo.click_ReviewPayment_Button();
		} else if (pricingDetails.order_Total.equals("$0.00")) {
			BaseUI.log_Status("Daily Deal added to cart is a free product");
		}

		CheckoutPage_ReviewOrder.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 100, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderReviewPage_PriceSummary() throws Exception {

		Boolean dailyDeal_AddedIsGiftCard = CheckoutPage_ReviewOrder.returnGiftCardProductNameAddedToCart(shoppingCartData);
		
		if (pricingDetails.order_Total.equals("$0.00")
				&& dailyDeal_AddedIsGiftCard) {
			if(!ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)){
				PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);
			}			
		} else if (!pricingDetails.order_Total.equals("$0.00")
				&& dailyDeal_AddedIsGiftCard) {
			PriceSummary.verify_PriceSummary_Info_SalesTaxPresent_GiftCard(pricingDetails);
		} else {
			PriceSummary.verify_PriceSummary_Info_TaxAmountKnown(pricingDetails);
		}
	}

	@Test(priority = 110, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderReviewPage_PaymentInformation() throws Exception {

		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore) 
				&& !(pricingDetails.order_Total.equals("$0.00"))) {
			CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, address1, city, state, zip,
					ccType, ccNumber, expire_month + "/" + expire_year);
		} else {
			if (pricingDetails.order_Total.equals("$0.00")) {
				CheckoutPage_ReviewOrder.verify_AllstateCash(pricingDetails.allstate_Cash);
			} else {
				if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
					BaseUI.verifyElementHasExpectedText("orderConf_AllstateCashAmount",
							"- " + pricingDetails.allstate_Cash);
				} else {
					CheckoutPage_ReviewOrder.verify_PaymentInfo(firstName + " " + lastName, address1, city, state, zip,
							ccType, ccNumber, expire_month + "/" + expire_year);
				}
			}
		}
	}

	@Test(priority = 110, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderReviewPage_ShippingInformation() throws Exception {

		CheckoutPage_ReviewOrder.verify_ShippingInfo(firstName + " " + lastName, address1, city, state, zip, country,
				phone);
	}

	@Test(priority = 110, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderReviewPage_PricingDetails() throws Exception {

		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			if(!(pricingDetails.order_Total.equals("$0.00"))) {
				CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
						pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
						pricingDetails.order_Total, pricingDetails.tax_Cost);
			}		
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				pricingDetails.allstate_Cash = pricingDetails.allstate_Cash.replace("- ", "");
			}
			CheckoutPage_ReviewOrder.verify_PricingDetails(pricingDetails.points, pricingDetails.retailPrice,
					pricingDetails.priceAfterPoints, pricingDetails.savings_Percent, pricingDetails.shipping_Cost,
					pricingDetails.order_Total, pricingDetails.tax_Cost);
		}	
	}

	@Test(priority = 110, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderReviewPage_IAgreeInfo() throws Exception {

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)
				&& !pricingDetails.allstate_Cash.equals("$0.00")) {
			CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
		} else if (pricingDetails.order_Total.equals("$0.00")) {
			CheckoutPage_ReviewOrder.verify_DailyDeals_IAgreeMessages();
		} else {
			if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
				CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, "0.00");
			} else {
				CheckoutPage_ReviewOrder.verify_IAgreeMessage(pricingDetails.order_Total, pricingDetails.allstate_Cash);
			}
		}
	}

	@Test(priority = 120, groups = {  "critical_Tests", "regression_Tests", "all_tests", "not_aarp", "not_smart",
			"not_streetwise", "not_aarpredesign" })
	public void PT3935_Validate_OnOrderConfirmationPage_ShippingInfo() throws Exception {

		if(!ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			CheckoutPage_ReviewOrder.check_IAgree_Checkbox();
		}		
		CheckoutPage_ReviewOrder.click_PlaceYourOrder();
		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		OrderConfirmationPage.verify_ShippingInformation(firstName + " " + lastName, address1, city, state, "*****",
				country, phone);
	}

	@Test(priority = 125, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_citi", "not_aarpredesign" })
	public void PT3935_Validate_OrderConfirmationPage_PointsDeducted() throws Exception {
		points_After_Checkout = Navigation.return_ExpectedPoints_AsString(points_Before_Checkout,
				pricingDetails.points);
		Navigation.verify_pointsMatchExpected(points_After_Checkout);
	}

	@Test(priority = 130, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_allstateCash", "not_aarpredesign" })
	public void PT3935_Validate_OrderConfirmationPage_BillingInformation() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			if (pricingDetails.order_Total.equals("$0.00")) {
				BaseUI.log_Status("Daily Deal added to cart is a free product");
				BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("orderConf_BillingInfo_RecipientName"));
			} else {
				OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, address1, city, state, ccType,
						ccNumber, zip);
			}
		} else {
			if(pricingDetails.order_Total.equals("$0.00")) {
				BaseUI.log_Status("Daily Deal added to cart is a free product");
				BaseUI.verifyElementDoesNOTExist("checkoutOrderReviewPage_CardHolderName", null, null);
			} else {
				OrderConfirmationPage.verify_BillingInformation(firstName + " " + lastName, address1, city, state, ccType,
						ccNumber, zip);
			}
		}				
	}

	@Test(priority = 130, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderConfirmationPage_ProductInformation() throws Exception {
		OrderConfirmationPage.verify_ProductInfo(shoppingCartData);
	}

	@Test(priority = 130, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_OrderConfirmationPage_PricingDetails() throws Exception {
		PricingDetails pricingDetails_OrderConfirmPage;
		if(ClientDataRetrieval.client_Matches(client_Designation.AARPRedesign) 
				|| ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_OrderConfirmationPage_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		} else {
			pricingDetails_OrderConfirmPage = OrderConfirmationPage.retrieve_PricingDetails();
			salesTaxCost = pricingDetails_OrderConfirmPage.tax_Cost;
		}
		OrderConfirmationPage.verify_PricingDetails(pricingDetails);
	}

	@Test(priority = 140, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_AccountHistory_Page() throws Exception {
		Navigation.navigate_Account();

		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			Account_History.verify_Purchase_AllstateCash(orderID, pricingDetails.points, pricingDetails.allstate_Cash);
		} else if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase(orderID, pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase(orderID, pricingDetails.points);
		}
	}

	@Test(priority = 140, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_CancelOrder_Page_ShippingInfo() throws Exception {
		Account.click_ViewDetails_ByOrderNumber(orderID);
		Account_OrderAndShippingDetails_Page.verify_ShippingAddress(firstName + " " + lastName, address1, city, state,
				country, zip);
	}

	@Test(priority = 145, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_CancelOrder_Page_ProductInfo() throws Exception {
		Account_OrderAndShippingDetails_Page.verify_ProductInfo_MultipleItems(shoppingCartData);
	}

	@Test(priority = 150, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_CancelOrder_Page_PaymentInfo() throws Exception {

		String orderDate = Account_OrderAndShippingDetails_Page.getFormatted_OrderDate();
		Double orderSubtotal;
		Boolean dailyDeal_AddedIsGiftCard = CheckoutPage_ReviewOrder.returnGiftCardProductNameAddedToCart(shoppingCartData);
		
		if (dailyDeal_AddedIsGiftCard) {
			if(ClientDataRetrieval.client_Matches(client_Designation.RedesignCore)) {
				if(pricingDetails.shipping_Cost.equals("FREE")){
					orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
							- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));
				} else {
					orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
							- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
							- Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
				}
			} else if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
						- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
						- Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""))
						+ Double.parseDouble(pricingDetails.allstate_Cash.replace("$", ""));
			} else {
				orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
						- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
						- Double.parseDouble(pricingDetails.shipping_Cost.replace("$", "").replace(",", ""));
			}
		} else {
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
						- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""))
						+ Double.parseDouble(pricingDetails.allstate_Cash.replace("$", ""));
			} else {
				orderSubtotal = Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", ""))
						- Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", ""));
			}
		}

		Account_OrderAndShippingDetails_Page.verify_PaymentInformation(pricingDetails, orderID, orderDate,
				orderSubtotal, Double.parseDouble(pricingDetails.tax_Cost.replace("$", "").replace(",", "")),
				pricingDetails.shipping_Cost, Double.parseDouble(pricingDetails.order_Total.replace("$", "").replace(",", "")), 
				pricingDetails.points, ccNumber);
	}

	@Test(priority = 160, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_DailyDeals_Soldout_Active_Status() throws Exception {

		Navigation.navigate_Merchandise_DailyDeals();
		DailyDeals.verify_SoldOut_ActiveStatus();
	}

	@Test(priority = 170, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp",
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_DailyDeals_Homapeage_Soldout_ImageText_Appears() throws Exception {

		Navigation.navigate_Home_viaHomeLink();
		Homepage.verify_DailyDeal_ImageSoldout_Appears();
	}

	@Test(priority = 180, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp",
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_DailyDeals_Validate_Cancelled() throws Exception {

		Account.cancelOrderAndValidateCancelledOrder(orderID, pricingDetails, salesTaxCost);
	}

	@Test(priority = 190, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_citi", "not_aarpredesign" })
	public void PT3935_Validate_Points_Reimbursed() throws Exception {
		Navigation.verify_pointsMatchExpected(points_Before_Checkout);
	}

	@Test(priority = 195, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_Validate_AccountHistory_OrderCancelled() throws Exception {
		Navigation.navigate_Account();
		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			Account_History.verify_Purchase_Cancelled(pricingDetails.savingsAmount);
		} else {
			Account_History.verify_Purchase_Cancelled(pricingDetails.points);
		}
	}
	
	@Test(priority = 200, groups = { "critical_Tests", "regression_Tests", "all_tests", "not_aarp", 
			"not_smart", "not_aarpredesign" })
	public void PT3935_DailyDeals_AdminSite_Update_DailyDeal_Quantity() throws Exception {
		
		Browser.driver = secondBidderDriver;
		ProductCatalog_CatalogPage.
				update_DailyDeal_ProductSku_Quantity(dailyDeal_Data.get("dailyDealName"), inventoryCount);// "20",
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("memcacheFlush_Successful"));
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondBidderDriver != null) {
			Browser.driver = secondBidderDriver;
			Browser.closeBrowser();
		}
	}
}
