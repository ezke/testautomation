package dRewards.tests_regression;


import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.DailyDeals;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DailyDeals_ShoppingCart_Expires extends BaseTest {

	String attribute = "899";
	int minutes = 1*1000*60;
	private WebDriver originalDriver;
	private WebDriver secondBidderDriver;
	private String original_Browser;



	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
		Browser.set_ImplicitWait_AndSaveValue(0);

		original_Browser = Browser.currentBrowser;
		originalDriver = Browser.driver;
		LoginPage_Admin.login_Admin();
		
		//Set Daily Deals ShoppingCart Expires Attribute value to 1 min
		Navigation_Admin.navigate_ExpandCategory("Clients");
		Clients_SearchClients.search_ClientID();
		Clients_SearchClients.click_ClientNumber_FromResult_Option();
		Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
		String attributeValue = BaseUI.getTextFromField(
				Locator.lookupRequiredElement("clients_ProgramAttributes_Value_ByAttributeID", attribute, null));
		
		if (attributeValue.equals("1.00000")) {

		} else {
			Clients_SearchClients.update_Attribute(attribute, "1");
			Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
			Browser.driver = originalDriver;

		}
		
		Browser.navigateTo(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		
		// Logic to remove all items from Shopping Cart.
		Homepage.deleteItemsFromShoppingCart();
		
		Homepage.clickDailyDeal();		
	}
	
	@Test(priority = 10, groups = {"not_jeepwave", "critical_Tests", "regression_Tests", "all_tests", "not_aarpredesign"})
	public void PT3935_Validate_DailyDeals_ShoppingCart_Expired_Message() throws Exception {
		
		DailyDeals.click_AddToShoppingCart();
		ShoppingCart.close_DailyDeal_AddedToCart_Popup();
		Thread.sleep(minutes);
		Navigation.navigate_ShoppingCart();
		DailyDeals.verify_ShoppingCart_Expired_Message();
	}
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			//Admin Site update Daily Deals Shopping Cart Expires Attribute Value to 20min
			Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
			LoginPage_Admin.login_Admin();
			Navigation_Admin.navigate_ExpandCategory("Clients");
			Clients_SearchClients.search_ClientID();
			Clients_SearchClients.click_ClientNumber_FromResult_Option();
			Clients_SearchClients.click_ProgramID(Client_Info_Admin.return_ProgramIDs());
			Clients_SearchClients.update_Attribute(attribute, "20");
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("clients_EditAllProgram_UpdateSuccessful_Text"));

			Clients_SearchClients.search_ClientID_ReloadProgramAttributes();
			Browser.driver = originalDriver;

		} finally {
			Browser.closeBrowser();
		}
	}
}
