package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.ClassObjects.CarRentalSearch_CheckoutDetails;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account;
import dRewards.pages.Account_CarRental_ViewConfirmPage;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.CarRentalSearch_ConfirmationPage;
import dRewards.pages.CarRental_ModifySearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class CarRental_MyAccount_ValidateMyAccountAndViewConfirmPage extends BaseTest {

	CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();
	CarRentalSearch_CheckoutDetails confirmation_PriceDetails = new CarRentalSearch_CheckoutDetails();
	
	String cityNameText;
	String airportLocation;
	
	//Car Rental price details
	String avgBaseRate;
	String points_OrSmartDollars, points;
	String youPay;
	String taxesOrFees;
	String totalWithTax;
	String airportName, airportLocation_ModifyPage;

	// Driver info
	String driverFirstName = "QA";
	String driverLastName = "Automation";
	String driverEmail = "qa.automation@qa.com";
	String confirmationText = "A confirmation of your reservation has just been sent to the e-mail address provided for the reservation. Please print this page, save a copy for your records and present it at the pickup location.";
	String expected_driverName = (driverFirstName + "   " + driverLastName).trim();
	
	//Date formatted
	String expected_ReturnDate;
	String expected_PickupDate;
	
	// Airline Info
	String airlineName = "AMERICAN AIRLINES";
	String airlineNum = "A1270";
	String airportAddress;
		
	//Confirmation Page Info
	String confirmationNumText;
	String carSpecificationDetails;
	String airportDetail;
	
	String currentWindow;
	String pointsBeforeCancellation;
	String totalPointsOrSmartRewardsToSpend;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		searchCriteria.airportCode_CityName = "PBI";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(60);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(62);
		searchCriteria.carClassOptionDisplayed = "Economy";
		searchCriteria.pickUpTimeDisplayed = "11:00 AM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_CarRental_Page();
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
		// carRentalData = CarRental_ModifySearchResults.get_Table_Data_Avis();

		cityNameText = BaseUI
				.get_Attribute_FromField(Locator.lookupRequiredElement("carRental_ModifySearchResults_Location"), "innerText")
				.replaceAll("Location:", "").trim();
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		CarRental_ModifySearchResults.click_List_Btn();

		airportLocation = CarRental_ModifySearchResults.returnAvis_Or_Budget_AirportLocation();
	
		airportLocation_ModifyPage = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("carRental_ModifySearchResults_Location"), "innerText")
				.replaceAll("Location:", "").replaceAll(searchCriteria.airportCode_CityName, "").replace(" (", "").replace(")", "").trim();
		
		//Enter Driver details and Airline details and Click Reserve Car button
		//Verify Price details on Confirmation page match checkout page
		CarRentalSearch_ConfirmationPage.verify_CarRentalCompany_PriceDetails_ConfirmationPage(driverFirstName,
				driverLastName, driverEmail, airlineName, airlineNum);
				
		airportName = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AirplaneName_PickUpLocation"));
		
		airportAddress = airportLocation_ModifyPage + "("+ airportName + ")";
		
		expected_PickupDate = "Pickup:" +"\n" + BaseUI.return_Date_AsDifferentFormat(searchCriteria.pickUpDateDisplayed, 
				"MM/dd/yyyy", "yyyy-MM-dd");
		
		expected_ReturnDate = "Return:" +"\n"+ BaseUI.return_Date_AsDifferentFormat(searchCriteria.returnDateDisplayed, 
				"MM/dd/yyyy", "yyyy-MM-dd");
		
		//Airport Name and address
		airportDetail = BaseUI.get_Attribute_FromField
				(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PickUpLocation"), "innerText")
				.replace("US", "").trim();
		System.out.println("airportDetail : " + airportDetail);
		
		if(ClientDataRetrieval.client_Matches(client_Designation.SmartRewards) || 
				ClientDataRetrieval.client_Matches(client_Designation.TVC_Marketing)) {
			points_OrSmartDollars = BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PointsOrSmartDollars"))
					.replace("\u2013", "").replace("\u002D", "").replace(",", "").trim();
		} else if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			points_OrSmartDollars = BaseUI.get_NonVisible_TextFromField
					(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PointsOrSmartDollars_Text"))
					.replace(",", "").trim().replace("\u2013", "").replace("\u002D", "");
		}else {
			points_OrSmartDollars = String.valueOf((int) Math.round(Double.parseDouble
					(BaseUI.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PointsOrSmartDollars"))
					.replace(",", "").replace("\u2013", "").replace("\u002D", "").trim())));
		}
		
		//Details from Reservation Confirm/Complete page
		confirmationNumText = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmationNumText"))
				.replaceAll("Confirmation Number: ", "").trim();
		
		carSpecificationDetails = BaseUI
				.getTextFromField(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_CarModalName"))
				.replaceAll(" or similar", "").trim();	
		
		//Get Car Rental price details from Reservation confirmation Page
		CarRentalSearch_CheckoutDetails confirmation_PriceDetails = new CarRentalSearch_CheckoutDetails();
		confirmation_PriceDetails = CarRentalSearch_ConfirmationPage.retrieve_CarRental_ConfirmationPage_PriceDetails();
		
		avgBaseRate = confirmation_PriceDetails.avgBaseRate.replace("$", "");
		points = confirmation_PriceDetails.points_OrSmartDollars
                .replace("\u2013", "").replace("\u002D", "").replace(",", "").trim();
		youPay = confirmation_PriceDetails.rentalCharge.replace("$", "");
		taxesOrFees = confirmation_PriceDetails.taxes_OrFees
				.replace("+ ", "").replace("$", "");
		totalWithTax = confirmation_PriceDetails.totalWithTax.replace("$", "");
		
	}

	@Test(priority = 30, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage()
			throws Exception {

		CarRentalSearch_ConfirmationPage.verifyConfirmationMessage_ReserveConfirmationPage();
	}

	@Test(priority = 35, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationNumber()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_ConfirmationNumText"));
	}

	@Test(priority = 40, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarRentalTitleHeader()
			throws Exception {

		CarRentalSearch_ConfirmationPage.verifyCarRentalTitleHeader_ReserveConfirmationPage();
	}

	@Test(priority = 50, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarClass() throws Exception {

		CarRentalSearch_ConfirmationPage.verifyCarClass_ReserveConfirmationPage(searchCriteria.carClassOptionDisplayed);
	}

	@Test(priority = 52, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarName() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarNameDetail"));
	}

	@Test(priority = 53, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarDetails() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_CarModalDetail"));
	}

	@Test(priority = 54, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarImageDisplayed() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarImage"));
	}

	@Test(priority = 55, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyCarRentalCompanyLogo()
			throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_CarRentalCompanyLogo"));
	}

	@Test(priority = 60, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyPickUpDateAndTime() throws Exception {

		CarRentalSearch_ConfirmationPage.verifyPickUpDateAndTime_CarRentalSearchResult_ReserveConfirmationPage(
				searchCriteria.pickUpDateDisplayed, searchCriteria.pickUpTimeDisplayed);
	}

	@Test(priority = 70, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyReturnDateAndTime() throws Exception {

		CarRentalSearch_ConfirmationPage.verifyReturnDateAndTime_CarRentalSearchResult_ReserveConfirmationPage(
				searchCriteria.returnDateDisplayed, searchCriteria.returnTimeDisplayed);
	}

	@Test(priority = 75, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyAirplaneIcon() throws Exception {

		BaseUI.verifyElementAppears(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AirplaneIcon_PickUpLocation"));
		BaseUI.verifyElementAppears(
				Locator.lookupRequiredElement("carRentalSearchResult_CheckoutPage_AirplaneIcon_ReturnLocation"));
	}

	@Test(priority = 80, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyAirport_PickUpAddress()
			throws Exception {

		CarRentalSearch_ConfirmationPage.verifyAirport_PickUpAddress_CarRentalSearchResult_ReserveConfirmationPage(
				airportLocation);
	}

	@Test(priority = 85, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyAirport_ReturnAddress()
			throws Exception {

		CarRentalSearch_ConfirmationPage.verifyAirport_ReturnAddress_CarRentalSearchResult_ReserveConfirmationPage(
				airportLocation);
	}

	@Test(priority = 90, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyPrintButton() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PrintYourReceipt_Btn"));
		BaseUI.verifyElementEnabled(Locator.lookupRequiredElement("carRental_ReserveConfirmPage_PrintYourReceipt_Btn"));
	}
	
	@Test(priority = 101, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyDriversName() throws Exception {
		
		CarRentalSearch_ConfirmationPage.verifyDriversName_ReserveConfirmationPage(driverFirstName, driverLastName);
	}

	@Test(priority = 102, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_VerifyViewConfirmLink() throws Exception {
		
		Account.verifyViewConfirmLink_CarRentalReservation();
	}
	
	@Test(priority = 102, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_VerifyCancelReservationLink() throws Exception {

		if(ClientDataRetrieval.isRedesignClient()) {
			Account.navigate_MyAccountPage_ForAARPRedesign();
			Account.clickDetailsText();
		} else {
			Navigation.navigate_Account();
		}
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"));
	}
	
	
	@Test(priority = 102, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_VerifyPointsDisplayed_Match() throws Exception {
		
		Account.verifyPointsDisplayed_Match_ReservationPagePoints(points_OrSmartDollars);
	}
	
	@Test(priority = 102, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_VerifyPickup_ReturnDateAndAirportName_Address() throws Exception {

		if(ClientDataRetrieval.isRedesignClient()) {
			Account.navigate_MyAccountPage_ForAARPRedesign();
		} else {
			Navigation.navigate_Account();
		}
		Account.verifyCarRental_ReservationDatesMatch(searchCriteria.pickUpDateDisplayed, 
				searchCriteria.returnDateDisplayed, airportAddress);
	}
	
	
	@Test(priority = 105, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle() throws Exception {

		if(ClientDataRetrieval.isRedesignClient()) {
			Account.navigate_MyAccountPage_ForAARPRedesign();
		} else {
			Navigation.navigate_Account();
		}
		currentWindow = Browser.driver.getWindowHandle();
		Account_CarRental_ViewConfirmPage.ClickAndVerify_ViewConfirmPage_HeaderTitle();
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_ClientLogo() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verify_ViewConfirmPage_ClientLogo();
	}
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_HeaderMessage() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_HeaderMessage();
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_CarCompanyLogo() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_CarCompanyLogo();
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_ConfirmationNumber() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_ConfirmationNumber(confirmationNumText);
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_CarClass() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_CarClass(searchCriteria.carClassOptionDisplayed);
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_CarModal() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_CarModal(carSpecificationDetails);
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_PickUpDate() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_PickUpDate(expected_PickupDate, airportDetail);
	}
	
	
	@Test(priority = 106, groups = {"not_jeepwave", "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_ReturnDate() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_ReturnDate(expected_ReturnDate, airportDetail);
	}
	
	
	@Test(priority = 106, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_DriverName() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_DriverName(expected_driverName);
	}
	
	
	@Test(priority = 106, groups = {"not_jeepwave", "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_aarpredesign", "not_tvc",
			"not_allstate", "not_allstateCash", "not_streetwise" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_DriverEmail() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_DriverEmail(driverEmail);
	}
	
	
	@Test(priority = 106, groups = { "not_jeepwave", "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_aarpredesign", "not_tvc",
			"not_allstate", "not_allstateCash", "not_streetwise" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_AirlineNumber() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_AirlineNumber(airlineNum);
	}
	
	
	@Test(priority = 110, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_PriceDetails() throws Exception {

		Account_CarRental_ViewConfirmPage.verify_CarRental_MyAccount_ViewConfirmPage_PriceDetails(avgBaseRate, 
				points, youPay, taxesOrFees, totalWithTax);
	}
	
	
	@Test(priority = 107, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_smart", "not_citi" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_SavingsMessage() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_SavingsMessage();
	}
	
	
	@Test(priority = 108, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step32_CarRentalSearchResult_MyAccount_ClickAndVerify_ViewConfirmPage_HeaderTitle" })
	public void PT3134_Step32_CarRentalSearchResult_MyAccount_verifyCarRental_ViewConfirmPage_AirportName_Address() throws Exception {
		
		Account_CarRental_ViewConfirmPage.verifyCarRental_ViewConfirmPage_AirportDetails(airportName);
	}
	
	
	@Test(priority = 115, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_VerifyYesAndNoLink() throws Exception {
	    if(ClientDataRetrieval.isRedesignClient()){
            BaseUI.switch_ToDefaultContent();
            Account.clickCloseIconToClose_ViewConfirm_Or_CancelBookingModal();
        } else {
            BaseUI.close_ExtraWindows(currentWindow);
        }
		Account.verify_MyAccount_CancelReservation_YesAndNoLink();
	}
	
	
	@Test(priority = 116, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickNoLink_ValidateReservationNotCancelled_Date() throws Exception {
		if(ClientDataRetrieval.isRedesignClient()){
			Account.clickCloseIconToClose_ViewConfirm_Or_CancelBookingModal();
		} else {
			BaseUI.switch_ToWindow(currentWindow);
		}
		pointsBeforeCancellation = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"))
				.replace("- ", "").trim();
		totalPointsOrSmartRewardsToSpend = Navigation.returnPoints()
				.replace(",", "").trim();
		Account.verify_MyAccount_CarBookingNotCancelled_Date();
	}
	
	@Test(priority = 116, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickNoLink_ValidateReservationNotCancelled_Points() throws Exception {
		String points = BaseUI.getTextFromField(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_points"));
		Account.verifyCarRental_ReservationNotCancelled_Points(points);
	}
	
	
	@Test(priority = 116, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickNoLink_ValidateReservationNotCancelled_ViewLink() throws Exception {
		//Validate View confirm links are visible
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CarDetails_ViewConfirmLink"));
	}
	
	
	@Test(priority = 117, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickNoLink_ValidateReservationNotCancelled_CancelLink() throws Exception {
		//Validate Cancel Reservation links are visible
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("myAccount_carRental_CancelReservationLink"));
	}
	
	
	@Test(priority = 118, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickYesLink_ValidateReservationCancelled_CancelText() throws Exception {
		Account.verifyCarRentalDetail_CancelReservation();
	}
	
	
	@Test(priority = 119, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickYesLink_ValidateReservationCancelled_CancelDate() throws Exception {
		Account.verifyCarRental_ReservationCancelled_Date();
	}
	
	
	@Test(priority = 119, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickYesLink_ValidateReservationCancelled_CancelPoints() throws Exception {
		Account.verifyCarRental_ReservationCancelled_Points(pointsBeforeCancellation);
	}
	
	
	@Test(priority = 118, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickYesLink_ValidateReservationCancelled_NoViewLink() throws Exception {
		//Validate View confirm links are not visible
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("myAccount_carRental_CarDetails_ViewConfirmLink"));			
	}
	
	
	@Test(priority = 118, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickYesLink_ValidateReservationCancelled_NoCancelLink() throws Exception {
		//Validate Cancel Reservation links are not visible
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("myAccount_carRental_CancelReservationLink"));
	}
	
	
	@Test(priority = 118, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" }, 
			dependsOnMethods = {"PT3134_Step30_CarRentalSearchResult_ReserveConfirmationPage_VerifyConfirmationMessage" })
	public void PT3134_Step35_CarRentalSearchResult_MyAccount_ClickYesLink_ValidateReservationCancelled_TotalPoints() throws Exception {
		//Validate Total reward points or smart dollars to spend 
		//Points are added back to Total points or smart dollars
		Account.verify_TotalPointsOrSmartDollars_ToSpend(totalPointsOrSmartRewardsToSpend);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}
