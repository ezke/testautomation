package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Sweepstakes_CurrentSweepstakes_WaysToEnter_Tests extends BaseTest {

    String randomNum = BaseUI.random_NumberAsString(30001, 40000);
    String sweepstakesName = "autDAF" + randomNum;
    String sweepstakesDescription = sweepstakesName;
    Boolean isFeaturedSweepstakes = false;
    String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(-2);
    String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String ticketCost = "2.0";
    String cumulativeCap = "100";
    String docID = "385";
    String productSku = "ssdemochevycamerored";
    String position = "1";
    String dCash = "0";
    String quantity = "0";
    String numDays_Claim = "2";
    String ticketQty = "1";
    String ticketLimit = "1000";
    String ticketOption = "Ticket Purchase";
    String loginOption = "Login";
    String officialRules_TopHeaderTitle;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();

        Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
        Navigation_Admin.navigate_Sweepstakes_CreateProgram();

        AddNewSweepstakes_Admin.add_CurrentSweepstakes_WithLogin_And_Ticket_Option(sweepstakesName, sweepstakesDescription,
                Client_Info_Admin.return_ProgramIDs(), isFeaturedSweepstakes, startDate, endDate, drawingDate,
                displayEndDate, ticketCost, cumulativeCap, docID, productSku, position, dCash, quantity, numDays_Claim,
                ticketQty, ticketLimit, ticketOption, loginOption);

        // validate created sweepstakes displays in admin site
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();

        // validate created sweepstakes active
        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .click_ActivateButton_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_Sweepstakes_IsActivated();

        // launch memcache flush to update sweepstakes
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
        } else {

            Navigation.navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("48");
        }
        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_LogIn_Displays() throws Exception {

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Login"));

        if (ClientDataRetrieval.isRedesignClient()) {

            BaseUI.verifyElementHasExpectedText("currentSweepstakes_WaysToEnter_Login", "Log in Daily");
        } else {

            BaseUI.verifyElementHasExpectedText("currentSweepstakes_WaysToEnter_Login", "Log In");
        }
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_LogIn_Text() throws Exception {
        String expectedtLoginText = "";
        if (ClientDataRetrieval.isRedesignClient()) {

            expectedtLoginText = "Automatically receive " + ticketQty + " entry(ies) just by logging in. Limit " + ticketLimit + " today.";
        } else {
            expectedtLoginText = "You will automatically receive " + ticketQty + " ticket! Limit " + ticketLimit + " per day";
        }

        String loginText = BaseUI.getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Login_Text"));
        String loginTextStr = BaseUI.remove_ExtraSpaces(loginText);
        BaseUI.baseStringCompare("Login Text", expectedtLoginText, loginTextStr);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Redeem_Displays() throws Exception {

        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Redeem"));

        if (ClientDataRetrieval.isRedesignClient()) {
            BaseUI.verifyElementHasExpectedText("currentSweepstakes_WaysToEnter_Redeem", "Promotional Code");
        } else {
            BaseUI.verifyElementHasExpectedText("currentSweepstakes_WaysToEnter_Redeem", "Redeem");
        }
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Redeem_Text() throws Exception {

        String expectedLoginText = "";
        if (ClientDataRetrieval.isRedesignClient()) {
            expectedLoginText = "Use a promotional code.";
        } else {
            expectedLoginText = "Redeem " + ClientDataRetrieval.client_Currency + " to get Sweepstakes tickets.";
        }

        String loginText = BaseUI.getTextFromField(Locator.lookupElement("currentSweepstakes_WaysToEnter_Redeem_Text"));
        BaseUI.baseStringCompare("Redeem Text", expectedLoginText, loginText);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_PointOrRewards() throws Exception {

        String pointsOrRewards = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_PointsOrRewards"));
        BaseUI.verify_true_AndLog(Double.parseDouble(pointsOrRewards) >= 0.00 && BaseUI.string_isNumeric(pointsOrRewards),
                ClientDataRetrieval.client_Currency + " is Double Numeric and greater than 0.00.",
                ClientDataRetrieval.client_Currency + " is NOT Double Numeric and greater than 0.00.");
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Quantity() throws Exception {

        String quantity = BaseUI.getTextFromInputBox(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Quantity_TextBox"));
        BaseUI.baseStringCompare("Quantity", ticketQty, quantity);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Total() throws Exception {

        String pointsOrRewards = BaseUI.getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_PointsOrRewards"));
        String total = BaseUI.getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Total"));
        BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(total) && Double.parseDouble(total) >= 0.00,
                "Total " + total + "is Double Numeric and greater than 0.00.",
                "Total " + total + " is NOT Double Numeric and greater than 0.00.");
        BaseUI.baseStringCompare("Quantity", pointsOrRewards, total);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Agreement_CheckBox() throws Exception {

        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox"));
        BaseUI.verifyCheckboxStatus(Locator.lookupElement("currentSweepstakes_WaysToEnter_Agreement_CheckBox"), false);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Agreement_Text() throws Exception {

        String total = BaseUI.getTextFromField(Locator.lookupElement("currentSweepstakes_WaysToEnter_Total"));
        String agreementText = "I understand that " + total + " " + ClientDataRetrieval.client_Currency + " will be deducted from my account and will not be returned at the end of the Sweepstakes.";
        String agreementActualText = BaseUI.getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_WaysToEnter_Agreement_Text"));
        BaseUI.baseStringCompare("Agreement Text", agreementText, agreementActualText);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step20_Current_Sweepstakes_WaysToEnter_Verify_Redeeem_Button() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {

            BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_WaysToEnter_Redeem_ButtonDsbl"));
            Sweepstakes.clickCurrentSweepstakes_Redeemed();
            BaseUI.verifyElementEnabled(Locator.lookupElement("currentSweepstakes_WaysToEnter_Redeem_Button"));
        } else {

            BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_WaysToEnter_Redeem_Button"));
            BaseUI.verifyElementEnabled(Locator.lookupElement("currentSweepstakes_WaysToEnter_Redeem_Button"));
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }

            Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            LoginPage_Admin.login_Admin();
            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            Sweepstakes_SearchSweepstakesProgramSearch_Admin.search_For_AndThen_UpdateEndTime(sweepstakesName, BaseUI.getDateAsString_InRelationToTodaysDate(-1));
        } finally {
            Browser.closeBrowser();
        }
    }
}
