package dRewards.tests_regression;

import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HomePage_Tests_ShopByBrandCarousel extends BaseTest {

	private String brandName_ClientSpecific = "Wagan";
	private String brandName_AllClient = "Swarovski";
	private String brandName_ClientSpecificDiff = "Naztech";
	private String brandName_AllClientDiff = "Oakley";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}
	
	
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step20_PT4786_Step1_Verify_ShopByBrandCarousel_Header() throws Exception {
		Homepage.verify_ShopByBrandCarousel_HeaderTitle();		
	}
	
	
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise"})
	public void PT3214_Step20_PT4786_Step1_Verify_SeeAllBrands_Link() throws Exception {
		Homepage.verify_SeeAllBrandsLink();		
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3214_Step20_Verify_BrandCarousel_RightNavigationArrow() throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("homepage_BrandCarousel_RightNavigation"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homepage_BrandCarousel_RightNavigation"));
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step20_Verify_BrandCarousel_LeftNavigationArrow() throws Exception {
		
		if(ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
		   Homepage.click_BrandCarousel_RightNavigationArrow();
		   WebElement leftNavigationElement = Locator.lookupElement("homepage_BrandCarousel_LeftNavigation");
		   BaseUI.scroll_to_element(leftNavigationElement);
		   BaseUI.verifyElementAppears(leftNavigationElement);
		} else {
			WebElement leftNavigationElement = Locator.lookupElement("homepage_BrandCarousel_LeftNavigation");
			BaseUI.scroll_to_element(leftNavigationElement);
			BaseUI.verifyElementAppears(leftNavigationElement);
		}
	}
	
	
	@Test(groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise" })
	public void PT3214_Step20_PT4786_Step2_Click_BrandCarousel_SeeAllBrands_Link() throws Exception {
		
		Homepage.verifyAllBrandItemsOnBrandCarousel();
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step20_Click_BrandCarousel_EntireRightNavigation() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Homepage.verify_ShopByBrand_EntireRightNavigation(brandName_ClientSpecificDiff);
		} else {
			Homepage.verify_ShopByBrand_EntireRightNavigation(brandName_AllClientDiff);
		}		
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3214_Step20_Click_BrandCarousel_EntireLeftNavigation() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Homepage.verify_ShopByBrand_EntireLeftNavigation(brandName_ClientSpecific);
		} else {
			Homepage.verify_ShopByBrand_EntireLeftNavigation(brandName_AllClient);
		}		
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests" })
	public void PT3214_Step21_PT4786_Step3_ClickAndVerify_BrandCarousel_BrandImages_Displayed() throws Exception {
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Homepage.clickAndVerify_BrandsDisplayed_ShopByBrandCarousel_LandingPage(brandName_ClientSpecific);
		} else {
			Homepage.clickAndVerify_BrandsDisplayed_ShopByBrandCarousel_LandingPage(brandName_AllClient);
		}				
	}
	
	
	@Test(groups = { "regression_Tests", "all_tests", "not_smart", "not_tvc", "not_streetwise", "not_jeepwave"})
	public void PT3214_Step21_PT4786_Step3_Verify_ShopByBrands_SeeAllBrands_BrandList() throws Exception {
		
		Homepage.verify_BrandsDisplayedOnCarousel_Exists_SeeAllBrandsPage();		
	}


	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_Home_viaHomeLink();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}
		Browser.closeBrowser();
	}
}
