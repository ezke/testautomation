package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Account_History;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class ValidateAuctionSweepstakesTransactionHistoryPageBreadcrumb_FromMyRewards extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4489_Step2_HomePage_VerifyLogo() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate", 
			"not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4489_Step2_HomePage_ValidateMyRewardsLink() {
        BaseUI.verifyElementAppears(Locator.lookupElement("nav_AccountLink"));
        BaseUI.verifyElementHasExpectedText("nav_AccountLink", "My Rewards");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4489_Step2_HomePage_ClickMyRewards_ValidateBreadcrumb() throws Exception {
        Account_History.verify_ClickingAccountLink_NavigatesTo_AccountHistoryPage();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4489_Step2_HomePage_ClickClosedAuctions_ValidateLandingPage_Breadcrumb() throws Exception {
        Account_History.verify_ClosedAuctionLandingPage_HeaderAndBreadcrumb();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4489_Step2_HomePage_ClickClosedSweepstakes_ValidateLandingPage_Breadcrumb() throws Exception {
        Account_History.verify_ClosedSweepstakesLandingPage_HeaderAndBreadcrumb();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4489_Step2_HomePage_ClickViewTransactionHistory_ValidateLandingPage_Breadcrumb() throws Exception {
        Account_History.verify_TransactionHistoryLandingPage_HeaderAndBreadcrumb();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.navigate_Home_viaLogo();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
