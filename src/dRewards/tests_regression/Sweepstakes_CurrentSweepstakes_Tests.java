package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages.Sweepstakes;
import dRewards.pages_Administration.AddNewSweepstakes_Admin;
import dRewards.pages_Administration.CatalogUpload_And_MemcacheFlush;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Sweepstakes_CurrentSweepstakes_Tests extends BaseTest {

    String randomNum = BaseUI.random_NumberAsString(20001, 30000);
    String sweepstakesName = "autDAF" + randomNum;
    String sweepstakesDescription = sweepstakesName;
    Boolean isFeaturedSweepstakes = false;
    String startDate = BaseUI.getDateAsString_InRelationToTodaysDate(-2);
    String endDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String drawingDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String displayEndDate = BaseUI.getDateAsString_InRelationToTodaysDate(2);
    String ticketCost = "2.0";
    String cumulativeCap = "100";
    String docID = "385";
    String productSku = "ssdemochevycamerored";
    String position = "1";
    String dCash = "0";
    String quantity = "0";
    String numDays_Claim = "2";
    String ticketQty = "1";
    String ticketLimit = "1000";
    String option = "Ticket Purchase";
    String officialRules_TopHeaderTitle;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();

        Navigation_Admin.navigate_ExpandCategory("Sweepstakes");
        Navigation_Admin.navigate_Sweepstakes_CreateProgram();

        AddNewSweepstakes_Admin.add_CurrentSweepstakes(sweepstakesName, sweepstakesDescription,
                Client_Info_Admin.return_ProgramIDs(), isFeaturedSweepstakes, startDate, endDate, drawingDate,
                displayEndDate, ticketCost, cumulativeCap, docID, productSku, position, dCash, quantity, numDays_Claim,
                ticketQty, ticketLimit, option);

        // validate created sweepstakes displays in admin site
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.verify_Navigated_ToSearchSweepstakes_Page();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_NewSweepstakes_IsDisplayed_Result();

        // validate created sweepstakes active
        Sweepstakes_SearchSweepstakesProgramSearch_Admin
                .click_ActivateButton_BySweepstakesName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.validate_Sweepstakes_IsActivated();

        // launch memcache flush to update sweepstakes
        CatalogUpload_And_MemcacheFlush.launch_MemcacheFlush();

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
        } else {

            Navigation.navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("48");
        }
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step1_VerifyCurrent_Or_Active_Sweepstakes() throws Exception {

        Sweepstakes.verify_CurrentSweepstakes_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName, BaseUI.return_Date_AsDifferentFormat(drawingDate,
                "MM/dd/yyyy", "MM/dd/yy"));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step1_Verify_Current_Sweepstakes_SeeOfficialRules_Link_BelowSeeDetailsButton() throws Exception {

        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_SeeOfficialRules_Link_BySweepstakesName",
                AddNewSweepstakes_Admin.entered_sweepstakesName, null));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step1_Current_Sweepstakes_ClickAndVerify_SeeOfficialRulesLink_BelowSeeDetailButton()
            throws Exception {

        Sweepstakes.click_SeeOfficialRules_Link_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        String officialRulesHeaderTitle = BaseUI.getTextFromField(
                Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Popup_Below_SeeDetailsButton_Header"));
        String officialRulesTitle = officialRulesHeaderTitle.replace(" Sweepstakes", "");
        BaseUI.baseStringCompare("officialRulesHeaderTitle", AddNewSweepstakes_Admin.entered_sweepstakesName,
                officialRulesTitle);
        Sweepstakes.click_SeeOfficialRules_Popup_CloseButton();
        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_SeeOfficialRules_Link_BySweepstakesName",
                AddNewSweepstakes_Admin.entered_sweepstakesName, null));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step1_Verify_Current_Sweepstakes_SeeofficialRules_Link_AboveWaysToEnter() throws Exception {

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Link"));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step1_Current_Sweepstakes_ClickAndVerify_SeeOfficialRulesLink_AboveWaysToEnter() throws Exception {

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes.click_SeeOfficialRules_Link();
        String officialRulesHeaderTitle = BaseUI.getTextFromField(
                Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Popup_Below_SeeDetailsButton_Header"));
        String officialRulesTitle = officialRulesHeaderTitle.replace(" Sweepstakes", "");
        BaseUI.baseStringCompare("officialRulesHeaderTitle", AddNewSweepstakes_Admin.entered_sweepstakesName,
                officialRulesTitle);
        Sweepstakes.click_SeeOfficialRules_Popup_CloseButton();
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Link"));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step12To15_Current_Sweepstakes_Verify_CurrentSweepstakes_ProductTitle_And_RetailPrice()
            throws Exception {

        Sweepstakes.click_SeeOfficialRules_Link_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        BaseUI.scroll_to_element(Locator.lookupElement("currentSweepstakes_SeeOfficialRules_Popup_DescriptionOfPrize"));
        String sweepstakesName = BaseUI.getTextFromField(

                Locator.lookupElement("currentSweepstakes_SeeOfficialRules_Popup_DescriptionOfPrize"));
        String sweepstakesValue = BaseUI.getTextFromField(
                Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Popup_CurrentRetailValue"));
        Sweepstakes.click_SeeOfficialRules_Popup_CloseButton();

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);

        String sweepstakesProductTitle = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_Product_Title"));
        String sweepstakesRetailPriceValue = BaseUI
                .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_RetailPrice_Value"))
                .replace("Retail Price:", "");

        BaseUI.baseStringCompare("Sweepstakes Name", sweepstakesName, sweepstakesProductTitle);
        BaseUI.baseStringCompare("Sweepstakes Value", sweepstakesValue, sweepstakesRetailPriceValue);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step12To15_Current_Sweepstakes_Verify_CurrentSweepstakes_Image_And_Description() throws Exception {

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_Description"));
        if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_Description"), "DESCRIPTION");
        } else {
            BaseUI.verifyElementHasExpectedText(Locator.lookupElement("currentSweepstakes_Description"), "Description");
        }
        BaseUI.verifyElementHasExpectedPartialAttributeValue(Locator.lookupElement("currentSweepstakes_Product_Image"),
                "src", ".jpg");
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step12To15_Current_Sweepstakes_Verify_CurrentSweepstakes_More_And_Less_Button() throws Exception {

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);

        if (BaseUI.elementAppears(Locator.lookupElement("currentSweepstakes_Description_More_Button"))) {
            Sweepstakes.click_Description_MoreButton();
        }
        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_Description_Feature_Text"));
        BaseUI.verifyElementHasExpectedText("currentSweepstakes_Description_Feature_Text", "Features:");
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("currentSweepstakes_Description_More_Button"));
        Sweepstakes.click_Description_LessButton();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("currentSweepstakes_Description_Less_Button"));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step1_Verify_Current_Sweepstakes_SeeOfficialRules_Info_AboveWaysToEnter() throws Exception {

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_SeeOfficialRules_Link"));

        Sweepstakes.click_SeeOfficialRules_Link();

        String officialRulesHeaderTitle = BaseUI
                .getTextFromField(Locator
                        .lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Popup_Below_SeeDetailsButton_Header"))
                .replace(" Sweepstakes", "");
        BaseUI.baseStringCompare("officialRulesHeaderTitle", AddNewSweepstakes_Admin.entered_sweepstakesName,
                officialRulesHeaderTitle);
        if (!ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
            officialRules_TopHeaderTitle = BaseUI
                    .getTextFromField(Locator.lookupRequiredElement("currentSweepstakes_SeeOfficialRules_Popup_TopHeader"))
                    .replace("Official Rules-", "");
            BaseUI.baseStringCompare("officialRulesTopHeader", AddNewSweepstakes_Admin.entered_sweepstakesName,
                    officialRules_TopHeaderTitle);
        }
        BaseUI.baseStringCompare("officialRulesHeaderTitle", AddNewSweepstakes_Admin.entered_sweepstakesName,
                officialRulesHeaderTitle);
        BaseUI.verifyElementHasExpectedText("currentSweepstakes_SeeOfficialRules_Popup_OfficialRules",
                "Official Rules");
        Sweepstakes.click_SeeOfficialRules_Popup_CloseButton();
        BaseUI.verifyElementAppears(Locator.lookupElement("currentSweepstakes_SeeOfficialRules_Link"));
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step4_Verify_Current_Sweepstakes_Type_DropdownList() throws Exception {

        Sweepstakes.verify_SweeptakesType_DropdownList();
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step4_Verify_Current_Sweepstakes_Type_DefaultDropdown() throws Exception {

        BaseUI.verifySelectedItemInDropdown(Locator.lookupElement("currentSweepstakes_SweeptakeType_DropDown"),
                "Current  Sweepstakes");
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi", "not_redesign", "not_aarpredesign","not_mcafee"})
    public void PT26_Step4_Verify_Current_Sweepstakes_Type_CurrentSweepstakes_And_AllSweepstakes() throws Exception {

        String[] allSweepsBreadcrumb = {"Home", "Sweepstakes"};
        String[] currentSweepsBreadcrumb = {"Home", "Current Sweepstakes"};

        Sweepstakes.verify_SweeptakesType_Dropdown("All Sweepstakes", allSweepsBreadcrumb);
        Sweepstakes.verify_SweeptakesType_Dropdown("Current  Sweepstakes", currentSweepsBreadcrumb);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step26_Verify_Current_Sweepstakes_Quantity_Error() throws Exception {

        String expectedText = "You may enter up to " + cumulativeCap + " tickets.";
        //String expectedTextRedesign = "You may only redeem up to " + cumulativeCap + " entries for this Sweepstakes";
        int quantity = 200;
        int maxQuantity = 100;

        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes.verify_CurrentSweeptakes_Quantity_Error(quantity, maxQuantity);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step22_Verify_Current_Sweepstakes_Agreement_Error() throws Exception {

        String expectedText = "You must check " + ClientDataRetrieval.client_Currency + " Agreement below.";
        String quantity = "10";
        Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
        Sweepstakes.verify_CurrentSweeptakes_Agreement_Error(quantity, expectedText);
    }

    @Test(groups = {"regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_tvc", "not_citi"})
    public void PT26_PT4000_Step26_Verify_Current_Sweepstakes_Quantity_Null_Error() throws Exception {

        if (ClientDataRetrieval.isRedesignClient()) {
            Sweepstakes.verifyQuantity_CantBeEmpty();
        } else {
            Sweepstakes.click_SeeDetails_Button_ByName(AddNewSweepstakes_Admin.entered_sweepstakesName);
            Sweepstakes.verify_CurrentSweeptakes_PleaseEnterQuantity_Error();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.nav_Sweepstakes_LandingPage();
        } else {
            Navigation.navigate_Home_viaLogo();
            Navigation.navigate_Menu_Submenu("Sweepstakes", "Current Sweepstakes");
            Sweepstakes.select_ResultPerPage_Dropdown("48");
        }
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
                Browser.closeBrowser();
                Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
            } else {
                Browser.navigateTo(GlobalVariables.dRewards_Admin_URL);
            }
            LoginPage_Admin.login_Admin();
            Navigation_Admin.navigate_Sweepstakes_SearchProgram();
            Sweepstakes_SearchSweepstakesProgramSearch_Admin.search_For_AndThen_UpdateEndTime(sweepstakesName, BaseUI.getDateAsString_InRelationToTodaysDate(-1));
        } finally {
            Browser.closeBrowser();
        }
    }
}
