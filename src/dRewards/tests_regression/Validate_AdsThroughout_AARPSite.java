package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Validate_AdsThroughout_AARPSite extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4479_Step2_HomePage_VerifyLogo() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4479_Step2_VerifyHomePage_TopAd() throws Exception {
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" ,"not_mcafee"})
    public void PT4479_Step2_VerifyMerchandise_LandingPage_TopFooterAd() throws Exception {
        Navigation.navigate_Merchandise();
        Navigation.verifyAd_AtTopOfPage();
    }


    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyLocalDeals_LandingPage_TopAd() throws Exception {
        Navigation.navigate_LocalOffers_SearchPage();
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyGiftCards_LandingPage_TopAd() throws Exception {
        Navigation.navigate_GiftCard_Page();
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyAuctions_LandingPage_TopAd() throws Exception {
        Navigation.navigate_Menu("Auctions");
//        BaseUI.switch_ToIframe(0);
//        Thread.sleep(500);
//        if (BaseUI.elementAppears(Locator.lookupElement("auctionsLandingPage_TopFooterAd"))) {
//            BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupElement("auctionsLandingPage_TopFooterAd"),
//                    "src", "https://");
//        } else {
//            BaseUI.switch_ToIframe(Locator.lookupElement("merchLandingPage_Ads_Iframe"));
//            Thread.sleep(500);
//            BaseUI.verifyElementAppears(Locator.lookupRequiredElement("merchLandingPage_TopFooterAd"));
//        }
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifySweepstakes_LandingPage_TopAd() throws Exception {
        Navigation.navigate_Menu("Sweepstakes");
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyMerchandise_Category_LandingPage_TopFooterAd() throws Exception {
        Navigation.navigate_Merchandise_Beauty();
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyMerchandise_CategoryProductDetails_LandingPage_TopFooterAd() throws Exception {
        Navigation.navigate_Merchandise_Beauty();
        Merchandise.selectRandom_MerchandiseCategoryProduct(1, 2);
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyMyRewards_LandingPage_TopFooterAd() throws Exception {
        Navigation.navigate_Account();
        Navigation.verifyAd_AtTopOfPage();
    }

    @Test(groups = { "regression_Tests", "not_redesign",  "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4479_Step2_VerifyShoppingCart_LandingPage_TopFooterAd() throws Exception {
        Navigation.navigate_ShoppingCart();
        Navigation.verifyAd_AtTopOfPage();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        BaseUI.switch_ToDefaultContent();
        Navigation.navigate_Home_viaHomeLink();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
