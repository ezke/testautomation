package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.CarRental_ModifySearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class CarRentalSearch_ValidateBudget_ListViewResults extends BaseTest {
	
	CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();
	TableData carRentalData;
	String pickUpDateDisplayed, returnDateDisplayed;

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		searchCriteria.airportCode_CityName = "PBI";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(60);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(62);
		searchCriteria.carClassOptionDisplayed = "Economy";
		searchCriteria.pickUpTimeDisplayed = "2:00 PM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_CarRental_Page();
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
		carRentalData = CarRental_ModifySearchResults.get_Table_Data_Budget();
	}
	
	
	@Test(priority = 10, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step26_CarRentalSearchResult_LandingPage_VerifyModifySearchResults_Header() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_ModifySearchResults_Header"));
		BaseUI.verifyElementHasExpectedPartialText("carRental_ModifySearchResults_Header", "Modify Search Results");	
	}
	
	
	@Test(priority = 20, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step26_CarRentalSearchResult_LandingPage_VerifyListButtonExists() throws Exception {
		
		if (ClientDataRetrieval.client_Matches(client_Designation.Allstate_both)) {
			BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListButton"));
		} else {	
		   BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListButton"));
		   BaseUI.verifyElementHasExpectedText("carRentalSearchResult_ListButton_ListText", "List");
		}		
	}
	
	
	@Test(priority = 30, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step26_CarRentalSearchResult_LandingPage_ClickAndVerifyListButton() throws Exception {
		
		BaseUI.scroll_to_element(Locator.lookupRequiredElement("carRentalSearchResult_MapButton"));
		CarRental_ModifySearchResults.click_List_Btn();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupOptionalElement("carRentalSelect_GoogleMaps_MapTab"));		
		
	}
	
	
	@Test(priority = 40, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step26_CarRentalSearchResult_LandingPage_VerifyBudgetLogoExists() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_BudgetLogo"));
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_MapIndicator"));
		BaseUI.verifyElementHasExpectedText("carRentalSearchResult_ListViewResult_Budget_MapIndicator", "B");
	}
	
	
	@Test(priority = 50, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step26_CarRentalSearchResult_LandingPage_VerifyAirplaneImageExists_LeftBudgetLogo() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_AirplaneImage"));
	}
	
	
	@Test(priority = 55, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step25_CarRentalSearchResult_LandingPage_VerifyAirportAddressExists_RightBudgetLogo() throws Exception {
		
		BaseUI.verify_true_AndLog(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_AirportAddress").isDisplayed(), 
				"Airport address displayed is " + Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_AirportAddress").getText(), 
				"Airport address " + Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_AirportAddress").getText() + " is not displayed");
	}
	
	
	@Test(priority = 56, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step25_CarRentalSearchResult_LandingPage_VerifyPricePerDayExists_RightBudgetLogo() throws Exception {
		
		BaseUI.verify_true_AndLog(Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_PricedPerDay").isDisplayed(), 
				"Price per day displayed is " + Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_PricedPerDay").getText(), 
				"Price per day " + Locator.lookupRequiredElement("carRentalSearchResult_ListViewResult_Budget_PricedPerDay").getText() + " is not displayed");
	}
	
	
	@Test(priority = 60, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step26_CarRentalSearchResult_LandingPage_VerifyCarClassDisplayed_ForAvis_MatchSearchCriteria() throws Exception {
		
		String pickUpDate = BaseUI.return_Date_AsDifferentFormat(searchCriteria.pickUpDateDisplayed, "MM/dd/yyyy", "MMM dd, yyyy");
		String returnDate = BaseUI.return_Date_AsDifferentFormat(searchCriteria.returnDateDisplayed, "MM/dd/yyyy", "MMM dd, yyyy");
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_SearchResults_SearchResultHeader"));
		BaseUI.verifyElementHasExpectedText("carRental_SearchResults_SearchResultHeader", "Your Search Results | " + pickUpDate + " - " + returnDate);	
	}
	
	
	@Test(priority = 70, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step25_CarRentalSearchResult_LandingPage_VerifyCarPriceDetails() throws Exception {
		
		CarRental_ModifySearchResults.verify_BudgetCarRental_PriceDetails(0, searchCriteria.carClassOptionDisplayed, carRentalData);
	}
	
	
	@Test(priority = 80, groups = { "regression_Tests", "not_jeepwave", "all_tests" })
	public void PT3134_Step25_CarRentalSearchResult_LandingPage_ReviseSearch_VerifyPickUp_ReturnDate() throws Exception {
		
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(65);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(67);
		searchCriteria.carClassOptionDisplayed = "Full Size";
		CarRental_ModifySearchResults.carRentalSearch_LandingPage_ModifySearchResults_NewOrReviseSearch
		(searchCriteria.pickUpDateDisplayed, searchCriteria.returnDateDisplayed, 
				searchCriteria.carClassOptionDisplayed);
		
		pickUpDateDisplayed = BaseUI.return_Date_AsDifferentFormat(searchCriteria.pickUpDateDisplayed, "MM/dd/yyyy", "MMM dd, yyyy");
		returnDateDisplayed = BaseUI.return_Date_AsDifferentFormat(searchCriteria.returnDateDisplayed, "MM/dd/yyyy", "MMM dd, yyyy");
		
		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_SearchResults_SearchResultHeader"));
		BaseUI.verifyElementHasExpectedText("carRental_SearchResults_SearchResultHeader",
				"Your Search Results | " + pickUpDateDisplayed + " - " + returnDateDisplayed);
	}
	
	
	@Test(priority = 85, groups = { "regression_Tests", "not_jeepwave", "all_tests" }, dependsOnMethods = {
	"PT3134_Step25_CarRentalSearchResult_LandingPage_ReviseSearch_VerifyPickUp_ReturnDate" })
	public void PT3134_Step25_CarRentalSearchResult_LandingPage_ReviseSearch_VerifyCarCompanyLogo() throws Exception {
		CarRental_ModifySearchResults.verify_BudgetCarRental_PriceDetails(0, searchCriteria.carClassOptionDisplayed,
				CarRental_ModifySearchResults.get_Table_Data_Budget());			
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}//End of class
