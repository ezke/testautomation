package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.Account;
import dRewards.pages.CheckoutPage_AllStateCash;
import dRewards.pages.CheckoutPage_PaymentInfo;
import dRewards.pages.CheckoutPage_ReviewOrder;
import dRewards.pages.CheckoutPage_ShippingInfo;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Merchandise;
import dRewards.pages.Navigation;
import dRewards.pages.ShoppingCart;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class MerchCheckout_CheckoutFunctionality_Step3 extends BaseTest {

	String orderID;
	String cvvCode = "211";

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Browser.set_ImplicitWait_AndSaveValue(0);
		// check to see if the shopping cart link is displayed.
		// If it is we navigate to Shopping Cart page and remove all of the
		// items.
		Homepage.deleteItemsFromShoppingCart();
		
		Navigation.navigate_Home_viaLogo();
		

		// add 2 products to cart.
		if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
			Merchandise.addItemsToShoppingCart_Streetwise();		
		} else {
			Navigation.navigate_Merchandise_Jewelry();
			Merchandise.selectPageToNavigate_AndChooseRandomProduct();
			Navigation.navigate_Merchandise_Tech();
			Merchandise.selectPageToNavigate_AndChooseRandomProduct();
		}
		
		//Adding this condition to check if item added to shopping cart is
		//not sold out. If yes we remove all items from cart and add new product
		if(BaseUI.elementAppears(Locator.lookupOptionalElement("shpCart_ProductAdded_SoldOutAlertMessage"))) {
			ShoppingCart.remove_AllItems();
			
			// add 2 products to cart.
			if(ClientDataRetrieval.client_Matches(client_Designation.StreetwiseRewards)) {
				Merchandise.addItemsToShoppingCart_Streetwise();		
			} else {
				Navigation.navigate_Merchandise_Jewelry();
				Merchandise.selectPageToNavigate_AndChooseRandomProduct();
				Navigation.navigate_Merchandise_Tech();
				Merchandise.selectPageToNavigate_AndChooseRandomProduct();
			}
		}

		ShoppingCart.click_ProceedToCheckout();
		if (ClientDataRetrieval.isRedesignClient()) {
			BaseUI.elementExists("checkout_ShipAddress_ChangeBtn", null, null);
		} else {
			CheckoutPage_ShippingInfo.navigate_To_NextPage();
			if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
				CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
				CheckoutPage_AllStateCash.click_Continue_ToNextPage();
			}
			//If you are using the existing payment details, need 
			//to enter CVV code to proceed with payment.
			if(!ClientDataRetrieval.isRedesignClient()){
				CheckoutPage_PaymentInfo.enterCvvCode_IfCvvTextboxIsBlank(cvvCode);
			}		
			CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		}	
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step17_PT4762_Step19_To_23_Click_EditShippingInformation_TakesUser_toShippingInfoPage() throws Exception {
		CheckoutPage_ReviewOrder.click_EditShipping();
		BaseUI.verifyElementExists("checkout_ShippingPage_Title", null, null);
		CheckoutPage_ShippingInfo.navigate_To_NextPage();
		if (ClientDataRetrieval.client_Matches(client_Designation.AllstateCash)) {
			CheckoutPage_AllStateCash.select_DoNotApplyAllstateCash();
			CheckoutPage_AllStateCash.click_Continue_ToNextPage();
		}
		
		//If you are using the existing payment details, need 
		//to enter CVV code to proceed with payment.
		if(!ClientDataRetrieval.isRedesignClient()){
			CheckoutPage_PaymentInfo.enterCvvCode_IfCvvTextboxIsBlank(cvvCode);
		}		
		CheckoutPage_PaymentInfo.navigate_toReviewPayment();
		CheckoutPage_ReviewOrder.submit_Order();
		BaseUI.verifyElementAppearsByString("orderConfNumber");
	}

	@Test(priority = 2, groups = { "regression_Tests", "all_tests"})
	public void PT3235_Step17_PT4762_Step24_To_27_Click_EditPaymentInformation_TakesUser_toPaymentInfoPage() throws Exception {

		orderID = BaseUI.getTextFromField(Locator.lookupRequiredElement("orderConfNumber"));
		Account.cancel_Order(orderID);
		Thread.sleep(3000);

	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		if (ClientDataRetrieval.isRedesignClient()) {
			Navigation.navigate_Logout();
		}

		Browser.closeBrowser();
	}
}
