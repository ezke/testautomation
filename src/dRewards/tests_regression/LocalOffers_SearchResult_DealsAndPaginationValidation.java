package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import org.jsoup.Connection;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.ArrayList;

import static utils.BaseUI.verifyElementDisabled;

public class LocalOffers_SearchResult_DealsAndPaginationValidation extends BaseTest {

    private String city = "Boca Raton";
    private String category = "Dining";
    private String address = "861 NW 51st ST";
    private String state = "FL";
    private String pageNumberList;
    String firstDealLetterBeforeClick, showLocationSearchResult;
    String expectedShowingLocationResultValue, actualShowingLocationResultValue;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_LocalOffers_SearchPage();
        LocalOffers_LandingPage.localOffer_SearchDiningPage_WithCriteria("Florida", city, category, address);
        Navigation.click_SeeLocalOffer_SearchPageButton();

        pageNumberList = BaseUI.getTextFromField(Locator.lookupElement("localOffer_PageNumberList_LastPageCount"));
        firstDealLetterBeforeClick = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealLetter"));

        showLocationSearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_ShowingLocation_ResultCount"));
        showLocationSearchResult = showLocationSearchResult.split("of ")[0];
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step1_VerifyLocalDealSearchResults_HeaderTitle() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_TitleText"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_TitleText", 
                "Deals near "+ city + ", "+ state);
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step1_VerifyLocalDealSearchResults_TextDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ShowingLocation_ResultCount", "Showing Locations 1-8");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step1_VerifyLocalDealSearchResults_DiningBtnDisplayed() {
        LocalOffers_LandingPage.verifyLocalDealSearchResults_DiningBtnDisplayed();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step1_VerifyLocalDealSearchResults_EntertainmentBtnDisplayed() {
        LocalOffers_LandingPage.verifyLocalDealSearchResults_EntertainmentBtnDisplayed();
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step1_VerifyLocalDealSearchResults_ShoppingBtnDisplayed() {
        LocalOffers_LandingPage.verifyLocalDealSearchResults_ShoppingBtnDisplayed();
    }

    @Test(priority = 11, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step3_ClickRightPaginationArrow_VerifySearchResultsUpdated() throws Exception {
        LocalOffers_LandingPage.clickRightPaginationArrow();
        String firstDealLetterAfterRightPaginationArrowClick = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealLetter"));
        BaseUI.baseStringCompare("First Letter after right pagination arrow is same",
                firstDealLetterBeforeClick, firstDealLetterAfterRightPaginationArrowClick);
        String actualShowLocationSearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_ShowingLocation_ResultCount"));
        actualShowLocationSearchResult = actualShowLocationSearchResult.split("of ")[0];
        BaseUI.baseStringCompareStringsAreDifferent("Showing Locations search result total count differs",
                showLocationSearchResult, actualShowLocationSearchResult);
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ShowingLocation_ResultCount", "Showing Locations 9-16");
    }

    @Test(priority = 12, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step3_ClickLeftPaginationArrow_VerifySearchResultsUpdated() throws Exception {
        firstDealLetterBeforeClick = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealLetter"));

        LocalOffers_LandingPage.clickLeftPaginationArrow();
        String firstDealLetterAfterRightPaginationArrowClick = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealLetter"));
        BaseUI.baseStringCompare("First Letter after clicking left pagination is same",
                firstDealLetterBeforeClick, firstDealLetterAfterRightPaginationArrowClick);
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ShowingLocation_ResultCount", "Showing Locations 1-8");
    }

    @Test(priority = 13, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step3_ClickAnyRandomPage_VerifySearchResultsUpdated() throws Exception {
        showLocationSearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_ShowingLocation_ResultCount"));
        showLocationSearchResult = showLocationSearchResult.split("of ")[0];
        firstDealLetterBeforeClick = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealLetter"));
        LocalOffers_LandingPage.clickRandomPageNummberFromList();

        String actualShowLocationSearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("localOffer_ShowingLocation_ResultCount"));
        actualShowLocationSearchResult = actualShowLocationSearchResult.split("of ")[0];
        BaseUI.baseStringCompareStringsAreDifferent("Showing Locations search result total count differs",
                showLocationSearchResult, actualShowLocationSearchResult);

        String firstDealLetterAfterRightPaginationArrowClick = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ResultList_FirstDealLetter"));
        BaseUI.baseStringCompare("First Letter after clicking left pagination arrow is same",
                firstDealLetterBeforeClick, firstDealLetterAfterRightPaginationArrowClick);
    }

    @Test(priority = 15, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step2_VerifyLocalDealSearchResults_DiningBtnDisabledWhenClicked() throws Exception {
        LocalOffers_LandingPage.clickDiningBtn();
        BaseUI.pageSourceNotContainsString("dining activated");
        //verifyElementDisabled(Locator.lookupRequiredElement("localOffer_DiningBtn"));
        BaseUI.verifyElementDoesNOTExist("localOffer_DiningIcon", null, null);
    }

    @Test(priority = 20, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step2_VerifyLocalDealSearchResults_EntertainmentBtnDisabledWhenClicked() throws Exception {
        LocalOffers_LandingPage.clickDiningBtn();
        LocalOffers_LandingPage.clickEntertainmentBtn();
        //verifyElementDisabled(Locator.lookupElement("localOffer_EntertainmentBtn"));
        BaseUI.verifyElementDoesNOTExist("localOffer_EntertainmentIcon", null, null);
    }

    @Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step2_VerifyLocalDealSearchResults_ShoppingBtnDisabledWhenClicked() throws Exception {
        LocalOffers_LandingPage.clickEntertainmentBtn();
        LocalOffers_LandingPage.clickShoppingBtn();
        //verifyElementDisabled(Locator.lookupRequiredElement("localOffer_ShoppingBtn"));
        BaseUI.verifyElementDoesNOTExist("localOffer_ShoppingIcon", null, null);
    }

    @Test(priority = 30, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step2_ClickDiningBtn_VerifySearchResultsUpdated() throws Exception {
        LocalOffers_LandingPage.clickEntertainmentBtn();
        LocalOffers_LandingPage.clickShoppingBtn();

        expectedShowingLocationResultValue = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        Integer numberOfResults = Integer.parseInt(expectedShowingLocationResultValue.split("of")[1].trim());
        pageNumberList = BaseUI.getTextFromField(Locator.lookupElement("localOffer_PageNumberList_LastPageCount"));

        LocalOffers_LandingPage.clickDiningBtn();

        actualShowingLocationResultValue = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));

        BaseUI.baseStringCompareStringsAreDifferent("Showing Location search result differs", expectedShowingLocationResultValue, actualShowingLocationResultValue);
        LocalOffers_LandingPage.verifySearchResultDealCountDiffer_WhenOneCategoryIsDisabled(numberOfResults);
    }

    @Test(priority = 35, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step2_VerifyPageCountListUpdated() throws Exception {
        LocalOffers_LandingPage.verifyLocalOfferSearchResultList(pageNumberList);
    }

    @Test(priority = 40, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step2_ClickEntertainmentAndShoppingBtn_VerifySearchResultsUpdated() throws Exception {
        expectedShowingLocationResultValue = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        Integer numberOfResults = Integer.parseInt(expectedShowingLocationResultValue.split("of")[1].trim());
        LocalOffers_LandingPage.clickEntertainmentBtn();
        LocalOffers_LandingPage.clickShoppingBtn();
        BaseUI.verifyElementDoesNotHaveExpectedText("localOffer_ShowingLocation_ResultCount", expectedShowingLocationResultValue);

        actualShowingLocationResultValue = BaseUI.getTextFromField(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        BaseUI.baseStringCompareStringsAreDifferent("Showing Location search result differs", expectedShowingLocationResultValue, actualShowingLocationResultValue);

        LocalOffers_LandingPage.verifySearchResultDealCountDiffer_WhenOneCategoryIsDisabled(numberOfResults);
    }

    @Test(priority = 50, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4538_Step4_ClickEachDealDisplayed_VerifyDealLogoDisplays() throws Exception {
        LocalOffers_LandingPage.clickEachAvailableDeal_VerifyDealLogoAndAddressDisplayed(city);
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        if (ClientDataRetrieval.isRedesignClient()) {
            Navigation.navigate_Logout();
        }
        Browser.closeBrowser();
    }
}
