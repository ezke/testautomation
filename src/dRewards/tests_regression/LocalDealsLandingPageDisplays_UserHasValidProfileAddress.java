package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.LocalOffers_LandingPage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.ConsumerService;
import dRewards.pages_Administration.ConsumerService_EditConsumerPage;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class LocalDealsLandingPageDisplays_UserHasValidProfileAddress extends BaseTest {

    private String city = "Boca Raton";
    private String country = "United States";
    private String address = "861 NW 51st ST";
    private String state = "FL";
    private String zipcode = "33487";
    private String username = "9362-qaauto22";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        //Navigate to client url and check if re-captcha is displayed on
        //login page
        Browser.openBrowser(ClientDataRetrieval.url);
        Browser.set_ImplicitWait_AndSaveValue(0);
        ConsumerService_EditConsumerPage.updateUserAddress_ForAARPRedesign(username, address, city,
                state, zipcode, country);

        //Navigate to client url and login with valid credentials
        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
        Navigation.navigate_LocalOffers_SearchPage();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_HeaderTitle() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_TitleText"));
        BaseUI.verifyElementHasExpectedText("localOffer_TitleText",
                "Deals near "+ city + ", "+ state);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_Breadcrumb() {
        String [] breadcrumb = {"Home", "Local Deals"};
        Navigation.verify_BreadcrumbList(breadcrumb);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_ChangeLocationLink() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_change_locationBtn"));
        BaseUI.verifyElementHasExpectedText("localOffer_change_locationBtn",
                "Change Location");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_DiningBtnDisplayed() {
        LocalOffers_LandingPage.verifyLocalDealSearchResults_DiningBtnDisplayed();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_EntertainmentBtnDisplayed() {
        LocalOffers_LandingPage.verifyLocalDealSearchResults_EntertainmentBtnDisplayed();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_aarp", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_ShoppingBtnDisplayed() {
        LocalOffers_LandingPage.verifyLocalDealSearchResults_ShoppingBtnDisplayed();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_ShowingLocations_TextDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ShowingLocation_ResultCount"));
        BaseUI.verifyElementHasExpectedPartialText("localOffer_ShowingLocation_ResultCount", "Showing Locations 1-8 of");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_Search_TextBoxDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SearchResult_SearchWithin_TextBox"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_SearchTextBox_GoBtnDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SearchResult_SearchWithin_Go_Button"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_SearchResultList() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SearchResult_SearchWithin_Go_Button"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_FirstLocalDeal() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ResultList_FirstDeal"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_RightPaginationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ResultList_RightPaginationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_LeftPaginationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_ResultList_LeftPaginationArrowDisabled"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4534_Step1_VerifyLocalDealSearchResults_MapDisplayed() {
        BaseUI.verifyElementAppears(Locator.lookupElement("localOffer_SearchResult_Map"));
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
