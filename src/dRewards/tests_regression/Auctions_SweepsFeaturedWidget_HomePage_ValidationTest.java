package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Auction_SearchSubAuction_Admin;
import dRewards.pages_Administration.Client_Info_Admin;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Sweepstakes_SearchSweepstakesProgramSearch_Admin;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class Auctions_SweepsFeaturedWidget_HomePage_ValidationTest extends BaseTest {

    private String status = "Active";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();
        Sweepstakes_SearchSweepstakesProgramSearch_Admin.searchActiveSweepstakesForGivenProgramID(status,
                Client_Info_Admin.return_ClientId(), Client_Info_Admin.return_ProgramIDs());
        String sweepstakes_SearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("searchSweepstakes_SearchResult_NoRecordsMsg"));

        Auction_SearchSubAuction_Admin.searchActiveAuctionsForGivenProgramID(status, Client_Info_Admin.return_ClientId());
        String auctions_SearchResult = BaseUI.getTextFromField(Locator.lookupRequiredElement("searchSubAuction_SearchResult_NoRecordsMsg"));

        if(sweepstakes_SearchResult.equals("No results found for search criteria you specified")
                && auctions_SearchResult.equals("No results found for search criteria you specified")) {
            BaseUI.log_AndFail("HomePage will Not display Sweepstakes and Auctions widget in 2nd and 3rd section.");
        } else {
            BaseUI.log_Status("HomePage will display Sweepstakes and Auctions widget in 2nd and 3rd Section");
        }

        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_VerifyLogo() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_VerifyFeaturedSweepstakesWidget_Image() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedSweepstakesWidget_Image"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_VerifyFeaturedSweepstakesWidget_SweepstakesHeader() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homePage_FeaturedSweepstakesWidget_Title"));
        BaseUI.verifyElementHasExpectedText("homePage_FeaturedSweepstakesWidget_Title", "Sweepstakes");
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_VerifyFeaturedAuctionsWidget_Image() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homePage_FeaturedAuctionsWidget_Image"));
    }

    @Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_VerifyFeaturedAuctionsWidget_AuctionsHeader() {
        BaseUI.verifyElementAppears(Locator.lookupElement("homePage_FeaturedAuctionsWidget_Title"));
        BaseUI.verifyElementHasExpectedText("homePage_FeaturedAuctionsWidget_Title", "Auctions");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_ClickFirstFeaturedDealWidget_VerifyLandingPage_Details() throws Exception {
        Homepage.clickFeaturedSweepstakesWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" } )
    public void PT4781_Step3_HomePage_ClickSecondFeaturedDealWidget_VerifyLandingPage_Details() throws Exception {
        Homepage.clickFeaturedAuctionsWidget_VerifyLandingPageDetails_MatchHomePageWidgetDetails();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.navigate_Home_viaHomeLink();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
