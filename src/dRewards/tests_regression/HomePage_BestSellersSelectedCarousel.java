package dRewards.tests_regression;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.GlobalVariables;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import dRewards.pages_Administration.Clients_SearchClients;
import dRewards.pages_Administration.LoginPage_Admin;
import dRewards.pages_Administration.Navigation_Admin;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import java.util.ArrayList;

public class HomePage_BestSellersSelectedCarousel extends BaseTest {

    private ArrayList<String> carouselImageList;
    private String programID;
    private String attribute = "13108";
    private String attributeValueSet = "True";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        //Navigate to Admin page
        Browser.openBrowser(GlobalVariables.dRewards_Admin_URL);
        Browser.set_ImplicitWait_AndSaveValue(0);
        LoginPage_Admin.login_Admin();
        //Check if the attribute value is set to True.
        Navigation_Admin.navigateExpandCategory_SelectClientProgramId_CheckAttributeSetToTrue(attribute, attributeValueSet);

        //Navigate to client web page.
        Browser.navigateTo(ClientDataRetrieval.url);
        LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4784_Step1_HomePage_VerifyLogo() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_HomeLogo"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4784_Step1_HomePage_VerifyBestSellerSelectedCarousel_Header() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BestSellerCarousel_Header"));
        BaseUI.verifyElementHasExpectedText("homepage_BestSellerCarousel_Header",
                "Best sellers selected especially for you");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4784_Step1_HomePage_VerifyBestSellerSelectedCarousel_ImagesDisplayed() {
        carouselImageList = Homepage.return_BestSellersCarousel_ImageList();
        BaseUI.verify_true_AndLog(carouselImageList.size() > 0, "Best Sellers Selected Carousel DISPLAYS IMAGES",
                "Best Sellers Selected Carousel DOES NOT DISPLAY IMAGES");
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4784_Step1_HomePage_VerifyBestSellerSelectedCarousel_RightNavigationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BestSellerCarousel_RightNavigationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4784_Step1_HomePage_VerifyBestSellerSelectedCarousel_LeftNavigationArrow() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("homepage_BestSellerCarousel_LeftNavigationArrow"));
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4784_Step1_HomePage_VerifyBestSellerSelectedCarousel_Clicking_RightArrow_Navigates_ToRight() throws Exception {
        Homepage.verify_ClickRightNavigationArrow_List_Of_VisibleBestSellersCarousel_ItemsChanged();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi" })
    public void PT4784_Step1_VerifyBestSellerSelectedCarousel_HomePage_Clicking_LeftArrow_Navigates_ToLeft() throws Exception {
        Homepage.verify_ClickLeftNavigationArrow_List_Of_VisibleBestSellersCarouselItemsChanged();
    }

    @Test(groups = { "regression_Tests", "all_tests", "not_redesign", "not_jeepwave", "not_smart", "not_tvc", "not_allstate",
            "not_allstateCash", "not_streetwise", "not_citi","not_mcafee" })
    public void PT4784_Step1_HomePage_VerifyBestSellerSelectedCarousel_Clicking_Item_NavigatesToProductLandingPage() throws Exception {
        Homepage.clickAndVerify_BestSellerItem_BestSellersSelectedCarousel_LandingPageDetails();
    }

    @AfterMethod(alwaysRun = true)
    public void testCleanup(ITestResult result) throws Exception {

        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.navigate_Home_viaLogo();
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            if (ClientDataRetrieval.isRedesignClient()) {
                Navigation.navigate_Logout();
            }
        } finally {
            Browser.closeBrowser();
        }
    }
}
