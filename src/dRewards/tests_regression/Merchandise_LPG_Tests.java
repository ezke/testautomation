package dRewards.tests_regression;

import dRewards.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import utils.Browser;
import utils.ResultWriter;

public class Merchandise_LPG_Tests extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "all_tests" ,"not_streetwise", "not_aarpredesign"})
	public void PT3243_Step15_PT4791_Step2_verify_Merchandise_LPG_Modal() throws Exception {
		Navigation.navigate_MerchandisePage_AndOpen_LPG_Modal();
		LPG_Modal.verify_Merchandise_LPG_Modal_Displayed();
	}

	@Test(groups = {"not_jeepwave", "not_tvc", "not_smart", "regression_Tests", "not_allstate", "not_allstateCash",
			"all_tests","not_streetwise", "not_aarpredesign"  })
	public void PT3243_Step15_PT4791_Step2_verify_Travel_LPG_Modal() throws Exception {
		Navigation.navigate_MerchandisePage_AndOpen_LPG_Modal();
		Homepage.verify_Travel_LPG_Modal();
	}

	@Test(groups = { "not_jeepwave","not_tvc", "not_smart","regression_Tests", "all_tests" ,"not_streetwise", "not_aarpredesign"})
	public void PT3243_Step15_PT4791_Step2_Navigate_LPG_TravelPage_Validate_TravelActive() throws Exception {
		Navigation.navigate_MerchandisePage_AndOpen_LPG_Modal();
		LPG_Modal.click_TravelTab();
		LPG_Modal.click_TravelLink();

		LPG_Page.click_TravelTab();
		LPG_Page.validate_Travel_Active();

		LPG_Page.verifyLPG_HeaderTitle();
	}

	@Test(groups = { "not_jeepwave","not_tvc", "not_smart", "regression_Tests", "all_tests", "not_streetwise"})
	public void PT3243_Step15_PT4791_Step2_Navigate_MerchandiseLPG_Page_AndValidateOnPage() throws Exception {
		Navigation.navigate_MerchandisePage_AndOpen_LPG_Modal();
		LPG_Modal.navigate_Merchandise_LPGMerchandisePage();

		LPG_Page.verifyLPG_HeaderTitle();
		LPG_Page.validate_Merchandise_Active();
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		
		ResultWriter.checkForFailureAndScreenshot(result);
		Navigation.navigate_Home_viaLogo();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}