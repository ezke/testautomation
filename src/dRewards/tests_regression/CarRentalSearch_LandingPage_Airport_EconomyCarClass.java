package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import dRewards.ClassObjects.CarRentalSearchCriteria;
import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.CarRentalSearch;
import dRewards.pages.CarRental_ModifySearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class CarRentalSearch_LandingPage_Airport_EconomyCarClass extends BaseTest {
	
	CarRentalSearchCriteria searchCriteria = new CarRentalSearchCriteria();

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		searchCriteria.airportCode_CityName = "JAX";
		searchCriteria.pickUpDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(60);
		searchCriteria.returnDateDisplayed = BaseUI.getDateAsString_InRelationToTodaysDate(62);
		searchCriteria.carClassOptionDisplayed = "Economy";
		searchCriteria.pickUpTimeDisplayed = "2:00 PM";
		searchCriteria.returnTimeDisplayed = "11:00 AM";

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
		Navigation.navigate_CarRental_Page();		
	}
	
	
	@Test(priority = 10, groups = { "not_jeepwave", "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_citi", "not_smart",
			"not_tvc", "not_streetwise" })
	public void PT3134_Step1_CarRentalSearch_LandingPage_VerifyCarRentalHeader() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("carRental_BreadcrumHeader"));
        BaseUI.verifyElementHasExpectedText("carRental_Header", "Car Rentals");
	}


    @Test(priority = 15, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests", "not_aarpredesign", "not_allstate", "not_allstateCash" })
	public void PT3134_Step1_CarRentalSearch_LandingPage_VerifyCarRentalImage() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupRequiredElement("nav_IntroBanner"));
	}


	@Test(priority = 20, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step2_CarRentalSearch_LandingPage_VerifyCarRental_SubHeader() throws Exception {

		CarRentalSearch.verify_CarRentalPage_Subheader();
	}


	@Test(priority = 30, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step3_CarRentalSearch_LandingPage_VerifyPickUpLocation_Default() throws Exception {

		CarRentalSearch.verify_DefaultLocation_SelectedFor_PickUp();
	}


	@Test(priority = 40, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step4_CarRentalSearch_LandingPage_VerifyPickUpReturnLocation_Default() throws Exception {

		CarRentalSearch.verify_Default_ReturnCarLocation_Selected();
	}


	@Test(priority = 50, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step5To9_CarRentalSearch_LandingPage_Enter_CityOrAirportCode_Economy() throws Exception {

		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
	}


	@Test(priority = 55, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step5To9_CarRentalSearch_LandingPage_Enter_CityOrAirportCode_NoPreference() throws Exception {

		searchCriteria.carClassOptionDisplayed = "No preference";
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
	}


	@Test(priority = 60, groups = { "regression_Tests", "not_jeepwave", "critical_Tests", "all_tests" })
	public void PT3134_Step10_CarRentalSearch_LandingPage_Click_NewSearch() throws Exception {
		
		searchCriteria.carClassOptionDisplayed = "No preference";
		CarRentalSearch.carRentalSearch_VerifyDisplayed_Details_ForSamePickUpLocation(searchCriteria);
		CarRental_ModifySearchResults.click_NewSearch_Link();
		BaseUI.verifyElementHasExpectedText("carRental_HeaderText", "Find Car Rentals. Reserve Now, Pay Later!");
		BaseUI.verify_true_AndLog((Locator.lookupRequiredElement("carRental_HeaderText").isDisplayed()), 
				"Find Car Rentals. Reserve Now, Pay Later! text is displayed", "Find Car Rentals. Reserve Now, Pay Later! is not displayed");
	}
		
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}
	

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			if (ClientDataRetrieval.isRedesignClient()) {
				Navigation.navigate_Home_viaHomeLink();
				Navigation.navigate_Logout();
			}
		} finally {
			Browser.closeBrowser();
		}
	}
}//End of class
