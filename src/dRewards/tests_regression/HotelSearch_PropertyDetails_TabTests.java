package dRewards.tests_regression;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.data.ClientDataRetrieval.client_Designation;
import dRewards.pages.HotelsAndCondos;
import dRewards.pages.HotelsAndCondos_PropertyDetailPage;
import dRewards.pages.HotelsAndCondos_SearchResults;
import dRewards.pages.LoginPage;
import dRewards.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class HotelSearch_PropertyDetails_TabTests extends BaseTest {
	String location = "Tampa, FL";
	String fromDate = BaseUI.getDateAsString_InRelationToTodaysDate(150);
	String fromDate_AccountHistoryFormat = BaseUI.getDateAsString_InRelationToTodaysDate(150, "yyyy-MM-dd");
	String toDate = BaseUI.getDateAsString_InRelationToTodaysDate(155);
	String toDate_AccountHistoryFormat = BaseUI.getDateAsString_InRelationToTodaysDate(155, "yyyy-MM-dd");
	String numberOfAdults = "2";
	String numberOfChildren = "2";
	String ageOfChildren = "3";
	String hotelLogoURL = "";
	String checkoutAddress = "";
	TableData hotelList = new TableData();
	
	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {
		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);

		Navigation.navigate_Hotel_Landingpage();
		HotelsAndCondos.hotel_Search_WithCriteria(location, fromDate, toDate, numberOfAdults, numberOfChildren,
				ageOfChildren);

		if (ClientDataRetrieval.client_Matches(client_Designation.Citi)) {
			HotelsAndCondos_SearchResults.sortBy("Your Price - Low to High");
		} else {
			HotelsAndCondos_SearchResults.sortBy("You Pay - Low to High");
		}

		//Added validation here to kill tests if Error present.
		HotelsAndCondos_SearchResults.verify_ErrorNotPresent();

		hotelList = HotelsAndCondos_SearchResults.return_hotelList_TableData();
		hotelLogoURL = HotelsAndCondos_SearchResults.return_ImageURL();

		HotelsAndCondos_SearchResults.click_First_BookItNow();
	}
	
	@Test(priority = 10, groups = { "regression_Tests", "all_tests" })
	public void PT3135_HotelsAndCondos_PropertyDetailsPage_HotelLogo_Accurate() throws Exception {
		hotelLogoURL = hotelLogoURL.replace("http:", "");
		HotelsAndCondos_PropertyDetailPage.verify_HotelLogo_Accurate(hotelLogoURL);
	}
	
	@Test(priority = 15, groups = { "regression_Tests", "all_tests" })
	public void PT3135_HotelsAndCondos_PropertyDetailsPage_PropertyFeatures_Tab() throws Exception {
		
		HotelsAndCondos_PropertyDetailPage.verify_PropertyFeatures_Tab();
	}
	
	@Test(priority = 20, groups = { "regression_Tests", "all_tests" })
	public void PT3135_HotelsAndCondos_PropertyDetailsPage_Photos_Tab() throws Exception {
		
		HotelsAndCondos_PropertyDetailPage.verify_Photos_Tab();
	}
	
	@Test(priority = 25, groups = { "regression_Tests", "all_tests", "not_XUAT" })
	public void PT3135_HotelsAndCondos_PropertyDetailsPage_MapDirections_Tab() throws Exception {
		
		HotelsAndCondos_PropertyDetailPage.verify_MapDirections_Tab(hotelList.data.get(0));
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try{
			
		}finally{
			Browser.closeBrowser();
		}
	}
}
