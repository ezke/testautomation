package dRewards.tests_regression;

import java.util.ArrayList;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dRewards.data.BaseTest;
import dRewards.data.ClientDataRetrieval;
import dRewards.pages.Homepage;
import dRewards.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class HomePage_LeftPanelNavigation extends BaseTest {

	@BeforeClass(alwaysRun = true)
	public void setup_method() throws Exception {

		Browser.openBrowser(ClientDataRetrieval.url);
		Browser.set_ImplicitWait_AndSaveValue(0);
		LoginPage.login(ClientDataRetrieval.userName, ClientDataRetrieval.password);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step7_Verify_MerchandiseLists_Match() throws Exception {
		String category = "Merchandise";
		String expectedCategoryHeader = "View All " + category;
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, expectedCategoryHeader);
	}

	@Test(groups = { "not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_tvc" ,"not_mcafee"})
	public void PT3243_Step7_Verify_AuctionsLists_Match() throws Exception {
		String category = "Auctions";
		String expectedCategoryHeader = "All " + category;
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, expectedCategoryHeader);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_redesign", "not_aarpredesign", "not_tvc", "not_jeepwave","not_mcafee" })
	public void PT3243_Step7_Verify_SweepstakesLists_Match() throws Exception {
		String category = "Sweepstakes";
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, null);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step7_Verify_TravelLists_Match() throws Exception {
		String category = "Travel";
		String expectedCategoryHeader = "View All " + category;
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, expectedCategoryHeader);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step7_Verify_LocalOffers_Lists_Match() throws Exception {
		String category = "Local Offers";
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, null);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step7_Verify_GiftCards_Lists_Match() throws Exception {
		String category = "Gift Cards";
		String expectedCategoryHeader = "View All " + category;
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, expectedCategoryHeader);
	}

	@Test(groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash", 
			"not_aarp", "not_redesign", "not_aarpredesign", "not_streetwise" ,"not_mcafee"})
	public void PT3243_Step7_Verify_FeaturedBrands_Lists_Match() throws Exception {
		String category = "Featured Brands";
		String expectedCategoryHeader = null;
		Homepage.verify_Category_Matches_BetweenTopList_AndLeftPanelNavigation(category, expectedCategoryHeader);
	}

	@Test(priority = 1, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3243_Step7_Merchandise_MaximizeLink() throws Exception {
		String category = "Merchandise";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 2, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3243_Step7_Merchandise_MinusLink() throws Exception {
		String category = "Merchandise";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 3, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_tvc","not_mcafee" })
	public void PT3243_Step7_Auctions_MaximizeLink() throws Exception {
		String category = "Auctions";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 4, groups = {"not_jeepwave", "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_tvc" ,"not_mcafee"})
	public void PT3243_Step7_Auctions_MinusLink() throws Exception {
		String category = "Auctions";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 5, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_tvc", "not_jeepwave","not_mcafee" })
	public void PT3243_Step7_Sweepstakes_MaximizeLink() throws Exception {
		String category = "Sweepstakes";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {

			if (Browser.currentBrowser.equals("internetexplorer")) {
				linkText = linkText.equals("Upcoming Sweepstakes") ? "Upcoming  Sweepstakes" : linkText;
			}

			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 6, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_tvc", "not_jeepwave" ,"not_mcafee"})
	public void PT3243_Step7_Sweepstakes_MinusLink() throws Exception {
		String category = "Sweepstakes";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 7, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3243_Step7_Travel_MaximizeLink() throws Exception {
		String category = "Travel";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 8, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3243_Step7_Travel_MinusLink() throws Exception {
		String category = "Travel";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 9, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step7_LocalOffers_MaximizeLink() throws Exception {
		String category = "Local Offers";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 10, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign","not_mcafee" })
	public void PT3243_Step7_LocalOffers_MinusLink() throws Exception {
		String category = "Local Offers";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 11, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3243_Step7_GiftCards_MaximizeLink() throws Exception {
		String category = "Gift Cards";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 12, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign" ,"not_mcafee"})
	public void PT3243_Step7_GiftCards_MinusLink() throws Exception {
		String category = "Gift Cards";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 13, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_streetwise" ,"not_mcafee"})
	public void PT3243_Step7_FeaturedBrands_MaximizeLink() throws Exception {
		String category = "Featured Brands";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.maximize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@Test(priority = 14, groups = { "regression_Tests", "all_tests", "not_citi", "not_allstate", "not_allstateCash",
			"not_aarp", "not_redesign", "not_aarpredesign", "not_streetwise" ,"not_mcafee"})
	public void PT3243_Step7_FeaturedBrands_MinusLink() throws Exception {
		String category = "Featured Brands";
		ArrayList<String> categoryList = Homepage.return_LeftPanel_CategoryList(category);
		BaseUI.verify_true_AndLog(categoryList.size() > 0, "Found Links", "Did not find links");

		Homepage.minimize_Category(category);
		for (String linkText : categoryList) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("homepage_LeftPanel_IndividualLinks", category, linkText));
		}
	}

	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		// Navigation.navigate_Home_viaHomeLink();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
