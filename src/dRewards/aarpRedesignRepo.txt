***Login page
loginPage_easyDeals_UserName    id=email
loginPage_easyDeals_Password    id=password
loginPage_easyDeals_SignOnButton    id=btn-login

loginPage_easyDeals_UserNameBpass    id=userId
loginPage_easyDeals_PasswordBpass    id=password
loginPage_easyDeals_SignOnButtonBpass   //input[@type='submit']
loginPage_NucaptchaIsPresent    //div[@id='nucaptcha-widget']//img[@id='nucaptcha-media']


***HomePage Search Button***
homepage_SearchBox	//input[@id='site-search-undefined-undefined-1693'] | //input[@name='site-search']
homepage_SearchButton	//span[@class='input-group-btn']/button
homepage_ClickUsername	//div[contains(@class,'header-nav-link header-nav-menu-label my-account-menu')]//span[@class='link-text'] | //div[@class='dropdown']//div[@id='welcome-arrow-image'] | //div[@class='dropdown']//div[@class='welcome-top']
nav_AccountLink	//span[./text()='My Rewards']
homepage_SearchIcon	//a[@class='header-nav-dropdown-search-icon']/span[@class='search-icon'] | //a[@class='header-nav-dropdown-search-icon']
Shp_ItemsInCart	//span[@class='number-of-items'] | //span[@class='shopping-cart-item-count']/strong

***homepage_lpgHeader	//div[@class='lpg-content']//p
nav_HomeLink    //ul[@class='navigationLinks']//a[@id='HL-Home'] | //a[./text()='Home'] | //ul[contains(@class,'top-level')]/li[@class='home']/a
nav_MyRewards   //span[contains(text(),'My Rewards')]

***HomePage Site-media Zone***
homepage_SitemediaZone1	//section[contains(@style,'sitemedia/coll1.png')]
homepage_SitemediaZone2	//section[contains(@style,'sitemedia/coll2.png')]
homePage_SiteMedia_Zone3	//section[contains(@style,'sitemedia/coll3.png')]
homepage_TravelHotel_Link	//h1[./text()='Book Your Next Trip Today']/following-sibling::button[./text()='Search Hotels']

***Account History
acct_SubHeader_Text	//h2[contains(text(),'My Rewards')] | //h1[contains(text(),'My Rewards')]

***Auction Checkout Order Review page
auctOrderRev_CompleteYourOrder_Button   (//input[@value='Claim My Auction'])[2]
auctShip_auctionInfo    //div[@class='claim-item-info']//div[@class='text-box']

***Auction Order Details page
myAuct_OrderAndShip_Header  //h2[@class='page-title'][./text()='Order Details'] | //h1[@class='page-title'][./text()='Order Details']
closed_Auctions_FirstAuction    //h2[@class='page-title'] | //h1[@class='page-title']

***Auction Details page, Auction Bidding page***
auctionBid_TimeLeft //div[contains(@class,'ends1')]/span[@class='value'] | (//span[@class='end-on'][./text()='Ended'])[1]
openAuct_ViewAllBids_FirstLink  //a[@href='javascript:;'][./text()='View Total Bids (']


***HomePage - Promo code***
nav_PromoCode_SubmitButton	//div[@class='header-nav-promo-code-entry-button-wrapper']/button
enterPromoCode_HelpText	//div[@class='header-nav-promo-code-entry-input-wrapper']//input[contains(@placeholder,'Enter Promo Code')]

***Daily Deal - Landing Page***
dailyDeal_reCaptchaVerificationModal_Text	//div[@id='recaptcha']/p
dailyDeal_reCaptchaVerificationModal_RecaptchaTxtbox	//div[@class='recaptcha-checkbox-checkmark']
dailyDeal_reCaptchaVerificationModal_RecaptchaContinueBtn	//input[contains(@value,'Continue')]
dailyDeals_AddToCart_PopUpModal_NoThanksHyperLink   //a[contains(text(),'No thanks')]
dailyDeal_reCaptchaVerificationModal_Recaptcha_ImNotARobotText  //label[@id='recaptcha-anchor-label'] | //div[@class='rc-anchor-content']//div[contains(@class,'rc-anchor-center-item')]
dailyDeal_reCaptchaVerificationModal_Recaptcha_Iframe   //iframe[@name='a-ex3mzkvdm7vp'] | //iframe[contains(@src,'https://www.google.com/recaptcha/api2/')]
dailyDeals_RecaptchaLogo    //div[@class='recaptcha-non-mobile']//img
dailyDeals_RecaptchaLabel   //div[@class='recaptcha-non-mobile']//label

***Admin ConsumerService***
consumer_UserID_Link_ByText //table[@class='horizontal']//td[3]/following-sibling::td[1]

***Navigation***
nav_Logout_Link	//span[./text()='Logout']

myAccount_carRental_ViewConfirmationPage_BookingDetail  //table[@class='DR-R4G-WrapTo100pc']//tr[4]/td[1]//tr[1]//td[@class='DR-R4G-SplitColumn']/p

**HomePage and other pages - Ads**
homePage_Ads_Iframe //iframe[contains(@id,'google_ads_iframe_')] | //iframe[contains(@id,'iframe_')] | //div[@class='GoogleActiveViewClass']/iframe[contains(@src,'/index.html')] | //iframe[contains(@id,'express_html_inpage_0.if')]
homePage_TopAds //div[@id='google_image_div']//img[contains(@class,'img_ad')] | //img[@alt='Advertisement'] | //gwd-google-ad[@id='gwd-ad']//gwd-page[@id='page1']//div[@id='gwd-frame6']/following-sibling::gwd-taparea[@id='gwd-taparea']
merchLandingPage_Ads_Iframe //iframe[contains(@id,'google_ads_iframe_')][@title='3rd party ad content'] | //iframe[contains(@id,'google_ad_')][@title='Advertisement'] | //div[@class='GoogleActiveViewClass']/iframe[contains(@id,'express_html_inpage_0.if')] | //div[@class='GoogleActiveViewClass']/iframe[contains(@src,'/index.html')] | //iframe[contains(@id,'google_ads_iframe_')] | //iframe[contains(@id,'iframe_')]
merchLandingPage_TopFooterAd_InsideSecondIframe  //gwd-google-ad[@id='gwd-ad']//gwd-page[@id='page1']//div[@id='gwd-frame6']/following-sibling::gwd-taparea[@id='gwd-taparea'] | //div[@id='google_image_div']//img[@class='img_ad'] | //div[contains(@class,'legal full-frame-object')]
eachMenuTab_LandingPage_TopAd   //canvas[@id='canvas'] | //img[contains(@src,'https://tpc')] | //div[contains(@class,' celtra-hotspot celtra-screen-object celtra-view touchable')]
eachMenuTab_LandingPage_TopAd_Iframe    //iframe[contains(@id,'google_ads_iframe')]
eachMenuTab_LandingPage_TopAd_InnerIframe    //iframe[contains(@id,'google_ad_')][@title='Advertisement']
eachMenuTab_LandingPage_TopAd_InsideFrame1  //img[@alt='Advertisement'] | //div[@id='google_image_div']//img[contains(@class,'img_ad')] | //div[@id='adContent']

***Homepage - Top Footer Links***
homepage_EarnMoreLink_LandingPage_Header    //span[@class='hidden-xs']/h1[@class='page-title'][./text()='Earn Points']
homepage_RewardsForGoodLink_LandingPage_Header    //span[@class='hidden-xs']/h1[@class='page-title'][./text()='Rewards for Good']

***Checkout page - Shipping Address***
checkout_NewAddress_Address1TxtBx   //label[contains(@for,'shippingAddress1')]/following-sibling::div/input[@id='shippingAddress1']
checkout_NewAddress_Address2TxtBx	id=address2 | //label[contains(@for,'shippingAddress2')]/following-sibling::div//input[@id='shippingAddress2']
checkout_NewAddress_CityTxtBx	//label[contains(@for,'shippingCity')]/following-sibling::div//input[@id='shippingCity']
checkout_NewAddress_StateDropdown	//label[contains(@for,'shippingState')]/following-sibling::div/select[@id='shippingState']
checkout_NewAddress_zipCodeTxtBx	//label[contains(@for,'shippingPostalCode')]/following-sibling::div/input[@id='shippingPostalCode']
checkout_NewAddress_CountryDropdown	//label[contains(@for,'shippingCountry')]/following-sibling::div/select[@id='shippingCountry']
checkout_NewAddress_phoneTxtBx	//label[contains(text(),'Phone Number')]/following-sibling::div[@class='input-group']/input[contains(@placeholder,'ex:')] | //label[contains(@for,'phoneNumber')]/following-sibling::div/input[@id='phoneNumber']

***Checkout page Step 2 ***Payment Information page***
checkout_newPayment_Address1    //input[@id='paymentAddress1']
checkout_newPayment_Country //select[@id='paymentCountry']
checkout_newPayment_City    //input[@id='paymentCity']
checkout_newPayment_State   //select[@id='paymentState']
checkout_newPayment_Zip //input[@id='paymentPostalCode']


***Merchandise Landing Page***
merch_FeaturedBrand_Header  //nav[@class='left-nav']//h2[./text()='Featured Brands']
merch_ActiveCategoryHeader	//h2[contains(text(),'Shop By Category')]
merch_ShopByCategoryLink_ByText //ul[@class='sub-nav  column-3']/li[2]/h2[./text()='Shop By Category']/following::ul/li[@class='merchandise']/a[./text()="{0}"]
merch_FeaturedBrands_ByText	//h2[./text()='Featured Brands']/following::ul/li[@class='merchandise']/a[./text()="{0}"]
merch_FeaturedShops_ByText	//h2[./text()='Featured Shops']/following::ul/li[@class='merchandise']/a[./text()="{0}"]
merchandisePage_Header	//h1[contains(text(),'Merchandise')]
merch_CategoryList  //h2[contains(text(),'Shop By Category')]/following-sibling::nav//li/a
merch_FeaturedBrandList //h2[contains(text(),'Featured Brands')]/following-sibling::nav//li/a
merch_FeaturedShopsList //h2[contains(text(),'Featured Shops')]/following-sibling::nav//li/a
merch_merchandiseActiveCategory	//h1[@class='page-title'][contains(text(),"{0}")] | //span[@class='dailydeal-header'][not(@class='dailydeal-start-time')]
merchLandingPage_Featured_Shop_CategoryCarousel_SubHeader   (//div[@class='carousel-container']//a[contains(@class,'merchandise-banner')]/div[@class='text']/h2)

***Auctions - Landing Page***
auctionsHeaderTitle //h1[@class='page-title'][contains(text(), '{0}')]
