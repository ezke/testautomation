package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import org.awaitility.Awaitility;
import utils.WhiteSeedWrapper.Application;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class QueueServicePage extends BasePage{

    private final XrayService _xrayService;

    public QueueServicePage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void wait_for_ConditionToMeet(Callable<Boolean> conditionToMeet, Duration timeToWait, Duration pollingInterval) {

        Awaitility.await().pollInterval(pollingInterval.toMillis(), TimeUnit.MILLISECONDS)
                .atMost(timeToWait.toMillis(), TimeUnit.MILLISECONDS).until(conditionToMeet);
    }

    public void wait_ForQueueService_Quit_Appears(String elementName, Duration timeToWaitInSeconds, String expectedText) {

        wait_for_ConditionToMeet(() -> {

                    String actualText = _xrayService.xray().elementByName(elementName).getText();
                    return actualText.equals(expectedText);
                },
                timeToWaitInSeconds, Duration.ofMillis(10000));
    }
}
