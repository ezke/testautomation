package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.BaseUI;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.time.Duration;

public class BatchQueryFilterPage extends BasePage {

    private final XrayService _xrayService;

    private static final String KEYINGJOB_FIELD = "tmp_keying_job";

    private static final String BATCHID_TEXTBOX = "batchID";

    public BatchQueryFilterPage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void filterByBatchID(String textToEnter) {

        _xrayService.xray().elementByName(BATCHID_TEXTBOX).typeText(textToEnter);
        click_BatchFilter_OkButton();
    }

    public void click_BatchFilter_OkButton() {
        _application.clickElement(ElementFilter.byText("OK"));
    }

    public void selectRowByBatchID(String batchID) throws Exception {

        _xrayService.xray().firstGrid().selectRowByColumnValue("BatchID", batchID);
    }

    public void wait_For_KeyingJob_ValueTobeUpdated(String columnValue, String expectedColumn, String expectedText, Duration timeToWaitInSeconds) {

        QueueServicePage _queueServicePage = new QueueServicePage(_application, _xrayService);
        _queueServicePage.wait_for_ConditionToMeet(() -> {

                    _application.typeOnKeyboardWithSpecialKeys("<F12></F12>");
                    String actualText = _xrayService.xray().firstGrid().getRowByColumnValue("ID", columnValue).get(expectedColumn);
                    return actualText.equals(expectedText);
                },
                timeToWaitInSeconds, Duration.ofMillis(10000));
    }

    public void verify_KeyingJob_Text(String expectedText) {

        String actualText = _xrayService.xray().elementByName(KEYINGJOB_FIELD).getText();
        BaseUI.baseStringCompare("KeyingJobField", expectedText, actualText);
    }

    public void verify_ColumnValue_ByColumnName(String batchIDValue, String expectedColumn, String expectedText) {

        String actualText = _xrayService.xray().firstGrid().getRowByColumnValue("BatchID", batchIDValue).get(expectedColumn);
        BaseUI.baseStringCompare(expectedColumn +" Column", expectedText, actualText);
    }
}
