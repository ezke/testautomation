package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

public class Navigation extends BasePage {

    public Navigation(Application application) {

        super(application);
    }

    public void nav_To_BatchFilterPage() {
        _application.typeOnKeyboard("2"); //Run ImageRPS
        _application.typeOnKeyboard("2"); //Data Maintenance menu
        _application.typeOnKeyboard("1"); //Batch Menu
        _application.typeOnKeyboard("1"); //View Batches by Batch ID
    }

    public void nav_To_SelectConsolidation() {

        _application.typeOnKeyboard("2"); //Run ImageRPS
        _application.typeOnKeyboard("2"); //Data Maintenance menu
        _application.typeOnKeyboard("4"); //Consolidations and Priorities
        _application.typeOnKeyboard("2"); //Start/Stop Consolidations
    }

    public void nav_To_QueueService() {

        _application.typeOnKeyboard("2"); //Run ImageRPS
        _application.typeOnKeyboard("4"); //Queue and Recognition Menu
        _application.typeOnKeyboard("1"); //Queue Service
    }

    public void pressTab() {
        _application.typeOnKeyboardWithSpecialKeys("<Tab />");
    }

    public void pressSpaceBar() {
        _application.typeOnKeyboardWithSpecialKeys("<Space />");
    }

    public void pressEnter() {
        _application.typeOnKeyboardWithSpecialKeys("<Enter />");
    }

    public void pressEscape() {
        _application.typeOnKeyboardWithSpecialKeys("<ESC />");
    }

    public void click_QueueAndRecognitionMenu() {
        _application.typeOnKeyboard("4"); //Queue and Recognition Menu in Main Menu
    }

    public void click_QueueService() {
        _application.typeOnKeyboard("1"); //Queue Service in Queue And Regonition Menu
    }

    public void click_QueueService_QuitButton() {
        _application.clickElement(ElementFilter.byText("Quit")); //Queue Service in Queue And Regonition Menu
    }

    public void click_StartStopConsolidations() {
        _application.typeOnKeyboard("2"); //Start/Stop Consolidations in Consolidation and Priorities
    }
}
