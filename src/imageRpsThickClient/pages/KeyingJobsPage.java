package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.BaseUI;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

public class KeyingJobsPage extends BasePage {

    private final XrayService _xrayService;

    private static final String CHKANDSTUBAMOUNT_TEXTBOX = "big_dol_amt";
    private static final String CHECKCOUNT_FIELD = "batchdb_tran.chk_count";

    private Navigation navigation = new Navigation(_application);

    public KeyingJobsPage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void click_Yes_InDialogBox() {
        _application.clickElement(ElementFilter.byText("Yes"));
    }

    public void click_No_InDialogBox() {
        _application.clickElement(ElementFilter.byText("No"));
    }

    public void verifyText_InDialogBox(String windowTitleName, String expectedDialogBoxText) throws InterruptedException, Exception {

        String[] windows = _application.getWindowList();

        if (windows.length >= 2) {
            _application.setCurrentWindow(ElementFilter.byText(windowTitleName));
            String windowActualText = _application.getElementText(ElementFilter.byText(expectedDialogBoxText));
            if (windowActualText.contains(expectedDialogBoxText)) {
                BaseUI.baseStringCompare("DialogBoxText", expectedDialogBoxText, windowActualText);
            }
        }
    }

    public void setFirstWindowToCurrent() {
        String[] windows = _application.getWindowList();
        if (windows.length == 1) {
            _application.setFirstWindowToCurrent();
        } else {
            BaseUI.log_AndFail("No windows found");
        }
    }

    public void balance_Amount_ForBAL() {

        String amountToEnter = "1";
        int maxCounter = 0;

        while (true) {
            if (maxCounter >= 50) {
                break;
            }
            enterText_IntoInputBox_ForBAL(amountToEnter);
            String[] windows = _application.getWindowList();

            if (windows.length >= 2) {
                _application.setCurrentWindow(ElementFilter.byText("Question (Press Help to view stack trace)"));
                if (_application.isElementPresent(ElementFilter.byText("End of Batch. Quit?"))) {
                    break;
                }
            }
            maxCounter++;
        }
    }

    private void enterText_IntoInputBox_ForBAL(String valueToEnter) {

        int maxCounter = 0;
        String checksCount = _xrayService.xray().elementByName(CHECKCOUNT_FIELD).getText();

        while(true){
            if(maxCounter >= Integer.valueOf(checksCount)){
                break;
            }
            _xrayService.xray().elementByName(CHKANDSTUBAMOUNT_TEXTBOX).typeText(valueToEnter);
            navigation.pressEnter();
            maxCounter++;
        }
        _xrayService.xray().elementByName(CHKANDSTUBAMOUNT_TEXTBOX).typeText(checksCount);
        navigation.pressEnter();
    }
}
