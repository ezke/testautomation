package imageRpsThickClient.pages;

import imageRpsThickClient.data.DcmLoginCredentials;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

public class LoginPage extends BasePage {

    private final DcmLoginCredentials _credentials;

    public LoginPage(Application application, DcmLoginCredentials credentials) {
        super(application);
        _credentials = credentials;
    }

    public void logIn() throws Exception {
        close_DialogBox_IfAppears();
        _application.putElementText(_credentials.loginName(), ElementFilter.byAutomationID("15"));
        _application.putElementText(_credentials.password(), ElementFilter.byAutomationID("17"));
        _application.clickElement(ElementFilter.byAutomationID("21"));
    }

    public void close_DialogBox_IfAppears() throws Exception {
        String[] windows = _application.getWindowList();
        if (windows.length >= 2) {
            click_DialogBox_YesButton();
        }
    }

    public void click_DialogBox_YesButton() throws Exception {

        _application.clickElement(ElementFilter.byText("Yes"));
    }
}
