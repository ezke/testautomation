package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;
import utils.WhiteSeedWrapper.WhiteSeedFault;

import java.time.Duration;

public class RecognitionStatsReportPage extends BasePage {

    private final XrayService _xrayService;
    private static final String STATSFROMDATE_TEXTBOX = "stat_date_from";
    private static final String THROUGH_TEXTBOX = "stat_date_thru";
    private static final String RUNNOW_BUTTON = "Run Now";
    private static final String RECO_VENDOR = "vendor_id";
    private static final String SAVE_TOFILE = "wk_output_file";

    public RecognitionStatsReportPage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void enterStatsFromDate(String date) throws Exception {

        _xrayService.xray().elementByName(STATSFROMDATE_TEXTBOX).typeText(date);
    }

    public void enterThroughDate(String date) throws Exception {

        _xrayService.xray().elementByName(THROUGH_TEXTBOX).typeText(date);
    }

    public void enterSaveToFilePath(String filePath) throws Exception {

        _xrayService.xray().elementByName(SAVE_TOFILE).typeText(filePath);
    }

    public void click_RunNow() throws WhiteSeedFault {

        _application.clickElement(ElementFilter.byText(RUNNOW_BUTTON));
    }

    public void click_Yes_InDialogBox_ToOverwrite_File() throws WhiteSeedFault {

        String[] windows = _application.getWindowList();
        if (windows.length >= 2) {
            _application.clickElement(ElementFilter.byText("Yes"));
        }
    }

    public void click_No_InDialogBoxTo_NotOpenFile() throws WhiteSeedFault {

        String[] windows = _application.getWindowList();
        if (windows.length >= 2) {
            _application.clickElement(ElementFilter.byText("No"));
        }
    }

    public void selectRecoVendor_FromDropdown_ByName(String name){

        _xrayService.xray().elementByName(RECO_VENDOR).click();
        _application.clickElement(ElementFilter.byText(name));

    }
}
