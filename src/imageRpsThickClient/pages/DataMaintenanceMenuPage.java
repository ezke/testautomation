package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;

public class DataMaintenanceMenuPage extends BasePage{

    public DataMaintenanceMenuPage(Application application) {
        super(application);
    }

    public void press_BatchMenu() {
        _application.typeOnKeyboard("1");
    }
}
