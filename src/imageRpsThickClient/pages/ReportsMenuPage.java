package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.WhiteSeedFault;

public class ReportsMenuPage extends BasePage{

    public ReportsMenuPage(Application application) {
        super(application);
    }

    public void pressKeyFor_StatisticsMenu() throws WhiteSeedFault {
        _application.typeOnKeyboard("1");
    }

    public void pressKeyFor_WorkFlowReportsMenu() throws WhiteSeedFault {
        _application.typeOnKeyboard("2");
    }

    public void pressKeyFor_ClientReportsMenu() throws WhiteSeedFault {
        _application.typeOnKeyboard("3");
    }

    public void pressKeyFor_SiteReportsMenu() throws WhiteSeedFault {
        _application.typeOnKeyboard("4");
    }

    public void pressKeyFor_ACHStatisticsMenu() throws WhiteSeedFault {
        _application.typeOnKeyboard("5");
    }

    public void pressKeyFor_RecievableWorksRXRescanReports() throws WhiteSeedFault {
        _application.typeOnKeyboard("6");
    }

    public void pressKeyFor_ConfiguredReports() throws WhiteSeedFault {
        _application.typeOnKeyboard("7");
    }

    public void pressKeyFor_ReportWizards() throws WhiteSeedFault {
        _application.typeOnKeyboard("8");
    }

}
