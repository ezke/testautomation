package imageRpsThickClient.pages;

import imageRpsThickClient.data.DcmLoginCredentials;
import imageRpsThickClient.data.ImageRpsThickClientConfig;
import imageRpsThickClient.programInfo.ElementType;
import imageRpsThickClient.programInfo.ProgramInfoDriver;
import imageRpsThickClient.xray.Xray;
import imageRpsThickClient.xray.XrayElement;
import imageRpsThickClient.xray.XrayService;
import r360.windows.WindowBase;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.io.IOException;

public class DCM extends WindowBase {

    private final DcmLoginCredentials _credentials;
    private final XrayService _xrayService;

    private DCM(Application application, DcmLoginCredentials credentials, XrayService xrayService) {
        _application = application;
        _credentials = credentials;
        _xrayService = xrayService;
    }

    private static DCM launch(String iniPath) throws IOException, InterruptedException {
        String whiteSeedUrl = ImageRpsThickClientConfig.getWhiteSeedUrl();
        DcmLoginCredentials credentials = ImageRpsThickClientConfig.getCredentials();

        int station = credentials.stationId();

        Application.launch(
                whiteSeedUrl, "cmd", "/c",
                "K:", "&",
                "cd", "\\cust1\\Test\\cmds", "&",
                "clntdvlp.cmd", iniPath, String.valueOf(station));
        Application application = Application.attach(whiteSeedUrl, "prowin32", 30.0);
        Thread.sleep(5000);
        return new DCM(application, credentials, new XrayService(application, new ProgramInfoDriver(station)));
    }

    public static DCM launchTemplateDB() throws IOException, InterruptedException {
        return launch("k:\\dcmcode\\test\\service\\CodeMgmt\\dcm-template.ini");
    }

    public static DCM launchRegularDB() throws IOException, InterruptedException {
        return launch("dcm.ini");
    }

    public LoginPage getLoginPage() {
        return new LoginPage(_application, _credentials);
    }

    public DevelopmentMenuPage getDevelopmentMenuPage() {
        return new DevelopmentMenuPage(_application);
    }

    public Navigation getNavigationPage() {
        return new Navigation(_application);
    }

    public ConsolidationPage getConsolidationPage() {
        return new ConsolidationPage(_application, _xrayService);
    }

    public QueueServicePage getQueueServicePage() {
        return new QueueServicePage(_application, _xrayService);
    }

    public MainMenuPage getMainMenuPage() {
        return new MainMenuPage(_application);
    }

    public MaintainQueuePage getMaintainQueuePage() {
        return new MaintainQueuePage(_application, _xrayService);
    }

    public QueueAndRecognitionMenuPage getQueueAndRecognitionMenuPage() {
        return new QueueAndRecognitionMenuPage(_application);
    }

    public ReportsMenuPage getMReportsMenuPage() {
        return new ReportsMenuPage(_application);
    }

    public ReportWizardsPage getReportWizardsPage() {
        return new ReportWizardsPage(_application, _xrayService);
    }

    public RecognitionStatsReportPage getRecognitionStatsReportPage() {
        return new RecognitionStatsReportPage(_application, _xrayService);
    }

    public BatchMenuPage getBatchMenuPage() {
        return new BatchMenuPage(_application);
    }

    public BatchQueryFilterPage getBatchQueryFilterPage() {
        return new BatchQueryFilterPage(_application, _xrayService);
    }

    public DataMaintenanceMenuPage getDataMaintenanceMenuPage() {
        return new DataMaintenanceMenuPage(_application);
    }

    public KeyingJobsPage getKeyingScanlinePage() {
        return new KeyingJobsPage(_application, _xrayService);
    }

    public SingleBatchOperationsPage getSingleBatchOperationsPage() {
        return new SingleBatchOperationsPage(_application);
    }

    public BrowseTransactionsPage getBrowseTransactionsPage() {
        return new BrowseTransactionsPage(_application, _xrayService);
    }

    public void click_ExitButton() throws Exception {

        int loopCount = 0;

        while (loopCount < 50) {
            _application.clickElement(ElementFilter.byText("Close"));

            String[] windows = _application.getWindowList();
            if (windows.length >= 2) {
                click_OkButton_ToExit();
                break;
            }

            loopCount++;
        }
    }

    public void click_OkButton_ToExit() throws Exception {

        _application.clickElement(ElementFilter.byText("OK"));
    }

    public MainMenuPage returnToMainMenuPage() {
        Xray xray = _xrayService.xray();

        boolean alreadyOnMainMenu = xray.getFrameTitles().equals(MainMenuPage.FRAME_TITLE);
        if (!alreadyOnMainMenu) {
            XrayElement backButtonDropdown = xray.elementByName("back_btn");
            backButtonDropdown.click();
            // Cursor to the first item in the menu, then select it
            _application.typeOnKeyboardWithSpecialKeys("<Down/><Enter/>");

            xray = _xrayService.xray();
        }

        xray.verifyFrameTitles(MainMenuPage.FRAME_TITLE);

        return getMainMenuPage();
    }
}
