package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;

public class BasePage {

    protected final Application _application;

    public BasePage(Application application) {
        _application = application;
    }
}
