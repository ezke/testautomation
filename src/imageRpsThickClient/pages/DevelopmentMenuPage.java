package imageRpsThickClient.pages;

import utils.BaseUI;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;
import whiteSeedProxy.ElementAttributes;

public class DevelopmentMenuPage extends BasePage {

    public DevelopmentMenuPage(Application application) {
        super(application);
    }

    public void verify_RunImageRPS_Text(String expectedText) throws Exception {

        String actualText = _application.getElementText(ElementFilter.byAutomationID("24")).trim();
        BaseUI.baseStringCompare("Development Menu", expectedText, actualText);
    }

    public void click_RunImageRPS() {
        _application.typeOnKeyboard("2");
    }

    public void run_ProcedureEditor() {

        _application.typeOnKeyboard("6");
        String[] windows = _application.getWindowList();
        if (windows.length >= 2) {
            _application.setCurrentWindow(ElementFilter.byText("Procedure Editor - Untitled:1"));
        }
        _application.typeOnKeyboardWithSpecialKeys("<F3></F3>");
        _application.typeOnKeyboard("Recostat_Client410.p");
        _application.typeOnKeyboardWithSpecialKeys("<Enter></Enter>");
        _application.typeOnKeyboardWithSpecialKeys("<F1></F1>");
        _application.clickElement(ElementFilter.byText("Close"));

        windows = _application.getWindowList();
        if (windows.length == 1) {
            _application.setFirstWindowToCurrent();
        } else {
            BaseUI.log_AndFail("No windows found");
        }
    }

    }
