package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.BaseUI;
import utils.WhiteSeedWrapper.Application;
import java.util.HashMap;

public class BrowseTransactionsPage extends BasePage{

    private final XrayService _xrayService;
    private static final String MULTIRESULTS_TEXTBOXVALUE = "batchdb_item.multi_results";
    private Navigation navigation = new Navigation(_application);

    public BrowseTransactionsPage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void selectRowByTransation(String transaction) throws Exception {

        _xrayService.xray().firstGrid().selectRowByColumnValue("Tran", transaction);
        navigation.pressEnter();
    }

    //Select an Item to Display
    public void selectRowBySequence(String sequence) throws Exception {

        _xrayService.xray().firstGrid().selectRowByColumnValue("Seq", sequence);
        navigation.pressEnter();
    }

    //Item Information Menu
    public void press_DollarAccountAuditPayeeAndBasicInfo() {
        _application.typeOnKeyboard("1");
    }


    public String return_MultiResults_Text(){

        return _xrayService.xray().elementByName(MULTIRESULTS_TEXTBOXVALUE).getText();
    }

    public void compare_MultiResultsValues_AgainstWORS_ResultFile(HashMap<String, String> worsValues) throws Exception {

        String multiResultsText = return_MultiResults_Text();
        String[] multiResultsTextArray = return_MultiResults_Text().split(",");
        if(worsValues.get("reserved").equals("")){
            BaseUI.baseStringCompare("MultiResults Value", "$1,0,-1,W1", multiResultsText);
        }else if(worsValues.get("LARrsValue").contains("Long")) {
            BaseUI.baseStringCompare("MultiResults Value", "$1,140000,100,W1", multiResultsText);
            BaseUI.baseStringCompare("MultiResults Value", worsValues.get("value"), multiResultsTextArray[1]);
        }else{
            BaseUI.baseStringCompare("MultiResults Value", worsValues.get("value"), multiResultsTextArray[1]);
            BaseUI.baseStringCompare("MultiResults LARrs Value", worsValues.get("LARrsValue"), multiResultsTextArray[5]);
            BaseUI.baseStringCompare("MultiResults LARcf Value", worsValues.get("LARcfValue"), multiResultsTextArray[6]);
        }
    }
}
