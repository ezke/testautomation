package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;

public class SingleBatchOperationsPage extends BasePage{

    public SingleBatchOperationsPage(Application application) {
        super(application);
    }

    public void press_GeneralBatchInformation() {
        _application.typeOnKeyboard("1");
    }

    public void press_BrowseTransactions() {
        _application.typeOnKeyboard("4");
    }

    public void press_KeyThisBatch() {
        _application.typeOnKeyboard("5");
    }
}
