package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.BaseUI;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import static org.hamcrest.Matchers.equalTo;

public class ConsolidationPage extends BasePage {

    private static final String FILTER_CLIENT_TEXTBOX = "tmp_client_id";
    private static final String NEW_CONSOLIDATION_DATE_TEXTBOX = "client.consol_date";

    private final XrayService _xrayService;


    public ConsolidationPage(Application application, XrayService xrayService) {

        super(application);
        _xrayService = xrayService;
    }

    public void click_Filter() {
        _application.clickElement(ElementFilter.byText("Filter"));
    }

    public void enterText_IntoInputBox(String textToEnter, long windowHandle) {
        _application.putElementText(textToEnter, ElementFilter.byWindowHandle(windowHandle));
    }

    public void click_Filter_Auto_RadioButton() {
        _application.clickElement(ElementFilter.byText("Auto"));
    }

    public void click_Filter_OkButton() {
        _application.clickElement(ElementFilter.byText("OK"));
    }

    public void click_ConsolDate_Popup_YesButton() {
        _application.clickElement(ElementFilter.byText("Yes"));
    }

    public void click_NewConsol_OkButton() {
        _application.clickElement(ElementFilter.byText("OK"));
    }

    public void click_NewConsol_Popup_SkipConsolidationDate() {
        _application.clickElement(ElementFilter.byText("Yes"));
    }

    public void verify_ElementByName_HasText(String elementName, String expectedText) {

        String actualText = _xrayService.xray().elementByName(elementName).getText();
        BaseUI.assertThat("Value from field " + elementName, actualText, equalTo(expectedText));
    }

    public void startNewConsolidation() {

        String date = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

        _xrayService.xray().elementByName(NEW_CONSOLIDATION_DATE_TEXTBOX).typeText(date);
        click_NewConsol_OkButton();
        String[] windows = _application.getWindowList();
        if (windows.length >= 2) {
            click_NewConsol_Popup_SkipConsolidationDate();
        }
    }

    public void filterByClient(String textToEnter) {

        click_Filter();
        _xrayService.xray().elementByName(FILTER_CLIENT_TEXTBOX).typeText(textToEnter);
        click_Filter_Auto_RadioButton();
        click_Filter_OkButton();
    }
}
