package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;

public class BatchMenuPage extends BasePage{

    public BatchMenuPage(Application application) {
        super(application);
    }

    public void press_ViewBatchesByBatchID() {
        _application.typeOnKeyboard("1");
    }
}
