package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.WhiteSeedWrapper.Application;

public class ReportWizardsPage extends BasePage{

    private final XrayService _xrayService;

    public ReportWizardsPage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void selectRowByDescription(String description) throws Exception {

        _xrayService.xray().firstGrid().selectRowByColumnValue("Description", description);
    }
}
