package imageRpsThickClient.pages;

import imageRpsThickClient.xray.XrayService;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

public class MaintainQueuePage extends BasePage {

    private final XrayService _xrayService;

    public MaintainQueuePage(Application application, XrayService xrayService) {
        super(application);
        _xrayService = xrayService;
    }

    public void selectRowByDescription(String description) throws Exception {

        _xrayService.xray().firstGrid().selectRowByColumnValue("Description", description);
    }

    public void unlock_Row() {
        _application.typeOnKeyboardWithSpecialKeys("<Alt>u</Alt>");
    }

    public void unlockRowByDescription(String description) throws Exception {
        selectRowByDescription(description);
        unlock_Row();
    }

    public void close_UnlockRow_PopupDialog_WithYes() {

        String[] windows = _application.getWindowList();
        if (windows.length >= 2) {
            click_UnlockRow_DialogBox_YesButton();
        }
    }

    public void click_UnlockRow_DialogBox_YesButton() {

        _application.clickElement(ElementFilter.byText("Yes"));
    }
}
