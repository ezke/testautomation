package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;

public class MainMenuPage extends BasePage {

    public static final String FRAME_TITLE = "Main Menu";

    public MainMenuPage(Application application) {
        super(application);
    }

    public void click_DataCompletionMenu() {
        _application.typeOnKeyboard("1");
    }

    public void click_DataMaintenanceMenu() {
        _application.typeOnKeyboard("2");
    }

    public void click_ReportsMenu() {
        _application.typeOnKeyboard("3");
    }

    public void click_QueueAndRecognitionMenu() {
        _application.typeOnKeyboard("4");
    }

    public void click_ConfigurationMenu() {
        _application.typeOnKeyboard("5");
    }

    public void click_MiscellaneousMenu() {
        _application.typeOnKeyboard("6");
    }

    public void click_ChangePasswordMenu() {
        _application.typeOnKeyboard("7");
    }

    public void click_DocumentCaptureMenu() {
        _application.typeOnKeyboard("8");
    }

    public void click_OpexCaptureConversion() {
        _application.typeOnKeyboard("9");
    }
}
