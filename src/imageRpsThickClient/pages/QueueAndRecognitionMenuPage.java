package imageRpsThickClient.pages;

import utils.WhiteSeedWrapper.Application;

public class QueueAndRecognitionMenuPage extends BasePage{

    public QueueAndRecognitionMenuPage(Application application) {
        super(application);
    }

    public void click_MaintainQueue() {
        _application.typeOnKeyboard("3");
    }
}
