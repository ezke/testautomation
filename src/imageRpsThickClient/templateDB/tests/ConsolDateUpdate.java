package imageRpsThickClient.templateDB.tests;

import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.pages.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;

public class ConsolDateUpdate extends BaseTest {

    private DCM dcm;
    private LoginPage loginPage;
    private Navigation navigation;
    private DevelopmentMenuPage developmentMenuPage;
    private ConsolidationPage consolidationPage;
    private QueueServicePage queueServicePage;

    @BeforeClass
    public void setup_method() throws Exception {
        dcm = DCM.launchTemplateDB();
        loginPage = dcm.getLoginPage();
        developmentMenuPage = dcm.getDevelopmentMenuPage();
        navigation = dcm.getNavigationPage();
        consolidationPage = dcm.getConsolidationPage();
        queueServicePage = dcm.getQueueServicePage();
    }

    @Test(priority = 1)
    public void openApplication() throws Exception {
        loginPage.logIn();
        developmentMenuPage.verify_RunImageRPS_Text("2. Run ImageRPS");
    }

    @Test(priority = 2)
    public void ConsolFilter_Verify_Client602_Date_Updated() throws Exception {

        navigation.nav_To_SelectConsolidation();
        consolidationPage.filterByClient("602");

        navigation.pressTab();
        navigation.pressTab();
        navigation.pressSpaceBar();
        navigation.pressEnter();
        consolidationPage.click_ConsolDate_Popup_YesButton();

        consolidationPage.startNewConsolidation();
        consolidationPage.verify_ElementByName_HasText("browser_f", "Select a Consolidation");
    }

    @Test(priority = 3)
    public void ConsolFilter_Verify_Client603_Date_Updated() throws Exception {

        navigation.pressEscape();
        navigation.click_StartStopConsolidations();
        consolidationPage.filterByClient("603");

        navigation.pressTab();
        navigation.pressTab();
        navigation.pressSpaceBar();
        navigation.pressEnter();
        consolidationPage.click_ConsolDate_Popup_YesButton();

        consolidationPage.startNewConsolidation();
        consolidationPage.verify_ElementByName_HasText("browser_f", "Select a Consolidation");
    }

    @Test(priority = 4)
    public void ConsolFilter_Verify_Client604_Date_Updated() throws Exception {

        navigation.pressEscape();
        navigation.click_StartStopConsolidations();
        consolidationPage.filterByClient("604");

        navigation.pressTab();
        navigation.pressTab();
        navigation.pressSpaceBar();
        navigation.pressEnter();
        consolidationPage.click_ConsolDate_Popup_YesButton();

        consolidationPage.startNewConsolidation();
        consolidationPage.verify_ElementByName_HasText("browser_f", "Select a Consolidation");
    }

    @Test(priority = 5)
    public void ConsolFilter_Verify_QueueService_Started_AndDone() throws Exception {

        navigation.pressEscape();
        navigation.pressEscape();
        navigation.pressEscape();
        navigation.click_QueueAndRecognitionMenu();
        navigation.click_QueueService();
        queueServicePage.wait_ForQueueService_Quit_Appears("quit-button", Duration.ofSeconds(180), "Quit");
        navigation.click_QueueService_QuitButton();

        consolidationPage.verify_ElementByName_HasText("menu_f", "Queue and Recognition Menu");
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        if (dcm != null) {
            dcm.click_ExitButton();
        }
    }
}
