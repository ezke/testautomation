package imageRpsThickClient.programInfo;

/**
 * <p>List of values returned by Element.elementType(), using names that
 * mean something to us (rather than using PROGRESS lingo).</p>
 *
 * <p>Use constants, rather than an enum, so we can get away with only
 * including the widget types we care about (while still making sure we
 * don't break if we encounter a widget type that's not in our list).</p>
 *
 * <p>A full list of PROGRESS widget types can be found at:
 * https://documentation.progress.com/output/ua/OpenEdge_latest/index.html#page/dvref%2Fwidget-reference.html%23
 * </p>
 */
public final class ElementType {

    public static final String BUTTON = "BUTTON";
    public static final String FRAME = "FRAME";
    public static final String GRID = "BROWSE";
    public static final String TEXT_BOX = "FILL-IN";

    private ElementType() {
    }
}
