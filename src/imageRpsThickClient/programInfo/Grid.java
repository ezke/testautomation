package imageRpsThickClient.programInfo;

import org.jetbrains.annotations.NotNull;
import utils.Parameter;
import utils.TableData;

import java.util.Collections;
import java.util.Map;

public class Grid {
    private final long _id;
    @NotNull
    private final String _name;
    private final Map<String, String> _currentRow;
    private final TableData _data;

    public Grid(long id, String name, Map<String, String> currentRow, TableData data) {
        _id = id;
        _name = name != null ? name : "";
        _currentRow = Collections.unmodifiableMap(currentRow);
        _data = data;
    }

    public Map<String, String> currentRow() {
        return _currentRow;
    }

    public TableData data() {
        return _data;
    }

    public long id() {
        return _id;
    }

    @NotNull
    public String name() {
        return _name;
    }
}
