
package imageRpsThickClient.programInfo.xmlTypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XmlHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XmlHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProgramHandle" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ProgramName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WinTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProgramStack" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PersistentProc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XmlHeader", propOrder = {
    "programHandle",
    "programName",
    "winTitle",
    "programStack",
    "persistentProc"
})
public class XmlHeader {

    @XmlElement(name = "ProgramHandle")
    protected long programHandle;
    @XmlElement(name = "ProgramName", required = true)
    protected String programName;
    @XmlElement(name = "WinTitle", required = true)
    protected String winTitle;
    @XmlElement(name = "ProgramStack", required = true)
    protected String programStack;
    @XmlElement(name = "PersistentProc", required = true)
    protected String persistentProc;

    /**
     * Gets the value of the programHandle property.
     * 
     */
    public long getProgramHandle() {
        return programHandle;
    }

    /**
     * Sets the value of the programHandle property.
     * 
     */
    public void setProgramHandle(long value) {
        this.programHandle = value;
    }

    /**
     * Gets the value of the programName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramName() {
        return programName;
    }

    /**
     * Sets the value of the programName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramName(String value) {
        this.programName = value;
    }

    /**
     * Gets the value of the winTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWinTitle() {
        return winTitle;
    }

    /**
     * Sets the value of the winTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWinTitle(String value) {
        this.winTitle = value;
    }

    /**
     * Gets the value of the programStack property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramStack() {
        return programStack;
    }

    /**
     * Sets the value of the programStack property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramStack(String value) {
        this.programStack = value;
    }

    /**
     * Gets the value of the persistentProc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersistentProc() {
        return persistentProc;
    }

    /**
     * Sets the value of the persistentProc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersistentProc(String value) {
        this.persistentProc = value;
    }

}
