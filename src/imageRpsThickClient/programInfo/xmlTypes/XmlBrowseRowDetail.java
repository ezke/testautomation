
package imageRpsThickClient.programInfo.xmlTypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XmlBrowseRowDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XmlBrowseRowDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WidgetID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ParentID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="WidgetHandle" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="WidgetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WidgetType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WidgetLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WidgetValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WindowsHandle" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BrowseRow" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XmlBrowseRowDetail", propOrder = {
    "widgetID",
    "parentID",
    "widgetHandle",
    "widgetName",
    "widgetType",
    "widgetLabel",
    "widgetValue",
    "windowsHandle",
    "browseRow"
})
public class XmlBrowseRowDetail {

    @XmlElement(name = "WidgetID")
    protected long widgetID;
    @XmlElement(name = "ParentID")
    protected long parentID;
    @XmlElement(name = "WidgetHandle")
    protected long widgetHandle;
    @XmlElement(name = "WidgetName", required = true)
    protected String widgetName;
    @XmlElement(name = "WidgetType", required = true)
    protected String widgetType;
    @XmlElement(name = "WidgetLabel", required = true)
    protected String widgetLabel;
    @XmlElement(name = "WidgetValue", required = true)
    protected String widgetValue;
    @XmlElement(name = "WindowsHandle")
    protected long windowsHandle;
    @XmlElement(name = "BrowseRow")
    protected int browseRow;

    /**
     * Gets the value of the widgetID property.
     * 
     */
    public long getWidgetID() {
        return widgetID;
    }

    /**
     * Sets the value of the widgetID property.
     * 
     */
    public void setWidgetID(long value) {
        this.widgetID = value;
    }

    /**
     * Gets the value of the parentID property.
     * 
     */
    public long getParentID() {
        return parentID;
    }

    /**
     * Sets the value of the parentID property.
     * 
     */
    public void setParentID(long value) {
        this.parentID = value;
    }

    /**
     * Gets the value of the widgetHandle property.
     * 
     */
    public long getWidgetHandle() {
        return widgetHandle;
    }

    /**
     * Sets the value of the widgetHandle property.
     * 
     */
    public void setWidgetHandle(long value) {
        this.widgetHandle = value;
    }

    /**
     * Gets the value of the widgetName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidgetName() {
        return widgetName;
    }

    /**
     * Sets the value of the widgetName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidgetName(String value) {
        this.widgetName = value;
    }

    /**
     * Gets the value of the widgetType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidgetType() {
        return widgetType;
    }

    /**
     * Sets the value of the widgetType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidgetType(String value) {
        this.widgetType = value;
    }

    /**
     * Gets the value of the widgetLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidgetLabel() {
        return widgetLabel;
    }

    /**
     * Sets the value of the widgetLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidgetLabel(String value) {
        this.widgetLabel = value;
    }

    /**
     * Gets the value of the widgetValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidgetValue() {
        return widgetValue;
    }

    /**
     * Sets the value of the widgetValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidgetValue(String value) {
        this.widgetValue = value;
    }

    /**
     * Gets the value of the windowsHandle property.
     * 
     */
    public long getWindowsHandle() {
        return windowsHandle;
    }

    /**
     * Sets the value of the windowsHandle property.
     * 
     */
    public void setWindowsHandle(long value) {
        this.windowsHandle = value;
    }

    /**
     * Gets the value of the browseRow property.
     * 
     */
    public int getBrowseRow() {
        return browseRow;
    }

    /**
     * Sets the value of the browseRow property.
     * 
     */
    public void setBrowseRow(int value) {
        this.browseRow = value;
    }

}
