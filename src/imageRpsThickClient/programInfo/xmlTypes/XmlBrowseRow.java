
package imageRpsThickClient.programInfo.xmlTypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XmlBrowseRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XmlBrowseRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WidgetID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ParentID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BrowseRow" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XmlBrowseRow", propOrder = {
    "widgetID",
    "parentID",
    "browseRow"
})
public class XmlBrowseRow {

    @XmlElement(name = "WidgetID")
    protected long widgetID;
    @XmlElement(name = "ParentID")
    protected long parentID;
    @XmlElement(name = "BrowseRow")
    protected int browseRow;

    /**
     * Gets the value of the widgetID property.
     * 
     */
    public long getWidgetID() {
        return widgetID;
    }

    /**
     * Sets the value of the widgetID property.
     * 
     */
    public void setWidgetID(long value) {
        this.widgetID = value;
    }

    /**
     * Gets the value of the parentID property.
     * 
     */
    public long getParentID() {
        return parentID;
    }

    /**
     * Sets the value of the parentID property.
     * 
     */
    public void setParentID(long value) {
        this.parentID = value;
    }

    /**
     * Gets the value of the browseRow property.
     * 
     */
    public int getBrowseRow() {
        return browseRow;
    }

    /**
     * Sets the value of the browseRow property.
     * 
     */
    public void setBrowseRow(int value) {
        this.browseRow = value;
    }

}
