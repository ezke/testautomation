
package imageRpsThickClient.programInfo.xmlTypes;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XmlProgInfoDs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XmlProgInfoDs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ttbHeader"/>
 *         &lt;element ref="{}ttbWidget" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}ttbBrowseRow" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}ttbBrowseRowDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XmlProgInfoDs", propOrder = {
    "ttbHeader",
    "ttbWidget",
    "ttbBrowseRow",
    "ttbBrowseRowDetail"
})
public class XmlProgInfoDs {

    @XmlElement(required = true)
    protected XmlHeader ttbHeader;
    protected List<XmlWidget> ttbWidget;
    protected List<XmlBrowseRow> ttbBrowseRow;
    protected List<XmlBrowseRowDetail> ttbBrowseRowDetail;

    /**
     * Gets the value of the ttbHeader property.
     * 
     * @return
     *     possible object is
     *     {@link XmlHeader }
     *     
     */
    public XmlHeader getTtbHeader() {
        return ttbHeader;
    }

    /**
     * Sets the value of the ttbHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link XmlHeader }
     *     
     */
    public void setTtbHeader(XmlHeader value) {
        this.ttbHeader = value;
    }

    /**
     * Gets the value of the ttbWidget property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ttbWidget property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTtbWidget().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlWidget }
     * 
     * 
     */
    public List<XmlWidget> getTtbWidget() {
        if (ttbWidget == null) {
            ttbWidget = new ArrayList<XmlWidget>();
        }
        return this.ttbWidget;
    }

    /**
     * Gets the value of the ttbBrowseRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ttbBrowseRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTtbBrowseRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlBrowseRow }
     * 
     * 
     */
    public List<XmlBrowseRow> getTtbBrowseRow() {
        if (ttbBrowseRow == null) {
            ttbBrowseRow = new ArrayList<XmlBrowseRow>();
        }
        return this.ttbBrowseRow;
    }

    /**
     * Gets the value of the ttbBrowseRowDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ttbBrowseRowDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTtbBrowseRowDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlBrowseRowDetail }
     * 
     * 
     */
    public List<XmlBrowseRowDetail> getTtbBrowseRowDetail() {
        if (ttbBrowseRowDetail == null) {
            ttbBrowseRowDetail = new ArrayList<XmlBrowseRowDetail>();
        }
        return this.ttbBrowseRowDetail;
    }

}
