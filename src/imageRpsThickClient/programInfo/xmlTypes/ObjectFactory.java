
package imageRpsThickClient.programInfo.xmlTypes;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the imageRpsThickClient.programInfo.xmlTypes package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TtbBrowseRow_QNAME = new QName("", "ttbBrowseRow");
    private final static QName _TtbBrowseRowDetail_QNAME = new QName("", "ttbBrowseRowDetail");
    private final static QName _TtbHeader_QNAME = new QName("", "ttbHeader");
    private final static QName _ProgInfoDS_QNAME = new QName("", "ProgInfo_DS");
    private final static QName _TtbWidget_QNAME = new QName("", "ttbWidget");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: imageRpsThickClient.programInfo.xmlTypes
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link XmlBrowseRow }
     * 
     */
    public XmlBrowseRow createXmlBrowseRow() {
        return new XmlBrowseRow();
    }

    /**
     * Create an instance of {@link XmlBrowseRowDetail }
     * 
     */
    public XmlBrowseRowDetail createXmlBrowseRowDetail() {
        return new XmlBrowseRowDetail();
    }

    /**
     * Create an instance of {@link XmlHeader }
     * 
     */
    public XmlHeader createXmlHeader() {
        return new XmlHeader();
    }

    /**
     * Create an instance of {@link XmlProgInfoDs }
     * 
     */
    public XmlProgInfoDs createXmlProgInfoDs() {
        return new XmlProgInfoDs();
    }

    /**
     * Create an instance of {@link XmlWidget }
     * 
     */
    public XmlWidget createXmlWidget() {
        return new XmlWidget();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XmlBrowseRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ttbBrowseRow")
    public JAXBElement<XmlBrowseRow> createTtbBrowseRow(XmlBrowseRow value) {
        return new JAXBElement<XmlBrowseRow>(_TtbBrowseRow_QNAME, XmlBrowseRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XmlBrowseRowDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ttbBrowseRowDetail")
    public JAXBElement<XmlBrowseRowDetail> createTtbBrowseRowDetail(XmlBrowseRowDetail value) {
        return new JAXBElement<XmlBrowseRowDetail>(_TtbBrowseRowDetail_QNAME, XmlBrowseRowDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XmlHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ttbHeader")
    public JAXBElement<XmlHeader> createTtbHeader(XmlHeader value) {
        return new JAXBElement<XmlHeader>(_TtbHeader_QNAME, XmlHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XmlProgInfoDs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProgInfo_DS")
    public JAXBElement<XmlProgInfoDs> createProgInfoDS(XmlProgInfoDs value) {
        return new JAXBElement<XmlProgInfoDs>(_ProgInfoDS_QNAME, XmlProgInfoDs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XmlWidget }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ttbWidget")
    public JAXBElement<XmlWidget> createTtbWidget(XmlWidget value) {
        return new JAXBElement<XmlWidget>(_TtbWidget_QNAME, XmlWidget.class, null, value);
    }

}
