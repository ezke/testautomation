package imageRpsThickClient.programInfo;

import imageRpsThickClient.programInfo.xmlTypes.XmlProgInfoDs;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.StringReader;

public final class ProgramInfoXmlParser {
    public static XmlProgInfoDs parseXml(Node xml) throws JAXBException {
        return parse(unmarshaller -> unmarshaller.unmarshal(xml));
    }

    public static XmlProgInfoDs parseXml(String xml) throws JAXBException {
        return parse(unmarshaller -> unmarshaller.unmarshal(new StringReader(xml)));
    }

    //#region Private implementation details

    private ProgramInfoXmlParser() {
    }

    @SuppressWarnings("SpellCheckingInspection")
    @XmlRootElement(name = "ProgInfo_DS")
    private static class MarshalableXmlProgInfoDs extends XmlProgInfoDs {
    }

    @FunctionalInterface
    private interface UnmarshalFunction {
        Object apply(Unmarshaller unmarshaller) throws JAXBException;
    }

    private static XmlProgInfoDs parse(UnmarshalFunction unmarshal) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(MarshalableXmlProgInfoDs.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (XmlProgInfoDs) unmarshal.apply(unmarshaller);
    }

    //#endregion
}
