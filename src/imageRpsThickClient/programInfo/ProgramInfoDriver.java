package imageRpsThickClient.programInfo;

import imageRpsThickClient.programInfo.xmlTypes.XmlProgInfoDs;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import utils.BaseUI;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

/**
 * <p>Reads the debug-info file from the ImageRPS prog_info utility.</p>
 *
 * <p>When Mike McMillan's "prog_info" plug-in is installed in a thick-client
 * environment, and we send a special keystroke to the thick client, prog_info
 * will write an XML file with details about the contents of the screen -
 * including grids, which aren't otherwise accessible from out-of-process.</p>
 *
 * <p>This class knows where to find that XML file, and knows how to wait for
 * the file to appear and then read its contents.</p>
 *
 * <p>Recommended usage pattern: call deleteXmlFile, then send the keystroke
 * to the thick client, then call loadXmlFile.</p>
 */
@SuppressWarnings("SpellCheckingInspection") // don't complain about "prog" in "prog_info" (the tool's actual name)
public class ProgramInfoDriver {
    private static final String XML_DIRECTORY = "\\\\Rps602Auto1Dcm\\K$\\DCMCode\\Test\\testingTools\\prog_info\\data\\STATION_%d";
    private static final String XML_FILE_NAME = "ProgInfo_DS.xml";

    private final Path _path;

    public ProgramInfoDriver(int stationNumber) {
        String directory = String.format(XML_DIRECTORY, stationNumber);
        _path = Paths.get(directory).resolve(XML_FILE_NAME);
    }

    public void deleteXmlFile() throws IOException {
        if (Files.exists(_path)) {
            // If we're unable to delete the file for some reason, fail with an exception
            Files.delete(_path);
        }
    }

    public Document loadXmlFile(Duration timeToWait) throws TimeoutException {
        LocalDateTime endTime = LocalDateTime.now().plus(timeToWait);
        while (LocalDateTime.now().isBefore(endTime)) {
            try {
                return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(_path.toFile());
            } catch (SAXException | ParserConfigurationException | IOException e) {
                // continue looping
            }
        }

        throw new TimeoutException("Thick client did not respond to screen-scrape request within the specified " +
                BaseUI.durationToString(timeToWait) + " timeout");
    }

    public ProgramInfo loadProgramInfo(Duration timeToWait) throws TimeoutException, JAXBException {
        Document xmlDocument = loadXmlFile(timeToWait);
        XmlProgInfoDs root = ProgramInfoXmlParser.parseXml(xmlDocument);
        ProgramInfoBuilder parser = new ProgramInfoBuilder(root.getTtbWidget(), root.getTtbBrowseRowDetail());
        return parser.createProgramInfo();
    }
}
