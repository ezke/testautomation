package imageRpsThickClient.programInfo;

import com.google.common.collect.Iterables;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProgramInfo {
    private final List<Element> _elements;
    private final List<Grid> _grids;

    public ProgramInfo(List<Element> elements, List<Grid> grids) {
        _elements = Collections.unmodifiableList(elements);
        _grids = Collections.unmodifiableList(grids);
    }

    public List<Element> elements() {
        return _elements;
    }

    public List<Grid> grids() {
        return _grids;
    }

    @NotNull
    public Element elementByLabel(String label) {
        return exactlyOne(elementsByLabel(label));
    }

    @NotNull
    public Element elementByName(String name) {
        return exactlyOne(elementsByName(name));
    }

    @NotNull
    public Grid gridByName(String name) {
        return exactlyOne(gridsByName(name));
    }

    @NotNull
    public List<Element> elementsByLabel(String label) {
        return filterList(_elements, e -> e.label().equals(label));
    }

    @NotNull
    public List<Element> elementsByName(String name) {
        return filterList(_elements, e -> e.name().equals(name));
    }

    @NotNull
    public List<Grid> gridsByName(String name) {
        return filterList(_grids, e -> e.name().equals(name));
    }

    //#region Private helper methods
    @NotNull
    private <E> E exactlyOne(List<E> list) {
        E value = Iterables.getOnlyElement(list);
        if (value == null)
            throw new IllegalStateException("List contained null value");
        return value;
    }

    private <E> List<E> filterList(List<E> list, Predicate<E> filter) {
        return list.stream().filter(filter).collect(Collectors.toList());
    }
    //#endregion
}
