package imageRpsThickClient.programInfo;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Element {
    private final long _id;
    @Nullable
    private final Long _parentId;
    private final long _windowHandle;
    @NotNull
    private final String _elementType;
    @NotNull
    private final String _name;
    @NotNull
    private final String _label;
    @NotNull
    private final String _value;

    public Element(long id, @Nullable Long parentId, long windowHandle, String elementType,
                   String name, String label, String value) {
        _id = id;
        _parentId = parentId;
        _windowHandle = windowHandle;
        _elementType = orEmpty(elementType);
        _name = orEmpty(name);
        _label = orEmpty(label);
        _value = orEmpty(value);
    }

    @NotNull
    private static String orEmpty(String value) {
        return value != null ? value : "";
    }

    public long id() {
        return _id;
    }

    @Nullable
    public Long parentId() {
        return _parentId;
    }

    public long windowHandle() {
        return _windowHandle;
    }

    /**
     * The element type, in PROGRESS lingo. The ElementType class has
     * constants (with human-readable names) for common element types.
     *
     * @return The element type
     */
    @NotNull
    public String elementType() {
        return _elementType;
    }

    @NotNull
    public String name() {
        return _name;
    }

    @NotNull
    public String label() {
        return _label;
    }

    @NotNull
    public String value() {
        return _value;
    }
}
