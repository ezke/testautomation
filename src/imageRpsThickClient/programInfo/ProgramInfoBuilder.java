package imageRpsThickClient.programInfo;

import imageRpsThickClient.programInfo.xmlTypes.XmlBrowseRowDetail;
import imageRpsThickClient.programInfo.xmlTypes.XmlWidget;
import utils.TableData;
import utils.TableDataBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class ProgramInfoBuilder {
    private final List<XmlWidget> _xmlWidgets;
    private final List<XmlBrowseRowDetail> _xmlBrowseRowDetails;

    public ProgramInfoBuilder(List<XmlWidget> xmlWidgets, List<XmlBrowseRowDetail> xmlBrowseRowDetails) {
        _xmlWidgets = xmlWidgets;
        _xmlBrowseRowDetails = xmlBrowseRowDetails;
    }

    private Element createElement(XmlWidget xmlWidget) {
        return new Element(
                xmlWidget.getWidgetID(),
                xmlWidget.getParentID(),
                xmlWidget.getWindowsHandle(),
                xmlWidget.getWidgetType(),
                xmlWidget.getWidgetName(),
                xmlWidget.getWidgetLabel(),
                xmlWidget.getWidgetValue());
    }

    private List<Element> createElements() {
        return _xmlWidgets.stream()
                .map(this::createElement)
                .collect(Collectors.toList());
    }

    private Grid createGrid(XmlWidget gridWidget) {
        long gridId = gridWidget.getWidgetID();
        String gridName = gridWidget.getWidgetName();
        Map<String, String> currentRow = createGridCurrentRow(gridId);
        TableData tableData = createTableData(gridId);

        return new Grid(gridId, gridName, currentRow, tableData);
    }

    private Map<String, String> createGridCurrentRow(long gridId) {
        List<XmlWidget> xmlWidgets = xmlWidgetsWithParentId(gridId);

        HashMap<String, String> result = new HashMap<>();
        for (XmlWidget xmlWidget : xmlWidgets) {
            result.put(xmlWidget.getWidgetLabel(), xmlWidget.getWidgetValue());
        }
        return result;
    }

    private List<Grid> createGrids() {
        return _xmlWidgets.stream()
                .filter(element -> Objects.equals(element.getWidgetType(), ElementType.GRID))
                .map(this::createGrid)
                .collect(Collectors.toList());
    }

    public ProgramInfo createProgramInfo() {
        List<Element> elements = createElements();
        List<Grid> grids = createGrids();
        return new ProgramInfo(elements, grids);
    }

    private TableData createTableData(long gridId) {
        List<XmlBrowseRowDetail> xmlBrowseRowDetails = _xmlBrowseRowDetails.stream()
                .filter(xmlBrowseRowDetail -> xmlBrowseRowDetail.getParentID() == gridId)
                .collect(Collectors.toList());

        TableDataBuilder builder = new TableDataBuilder();
        for (XmlBrowseRowDetail xmlBrowseRowDetail : xmlBrowseRowDetails) {
            int oneBasedRowIndex = xmlBrowseRowDetail.getBrowseRow();
            int rowIndex = oneBasedRowIndex - 1;
            String key = xmlBrowseRowDetail.getWidgetLabel();
            String value = xmlBrowseRowDetail.getWidgetValue();
            builder.put(rowIndex, key, value);
        }

        return builder.build();
    }

    private List<XmlWidget> xmlWidgetsWithParentId(long parentId) {
        return _xmlWidgets.stream()
                .filter(widget -> {
                    Long currentParentId = widget.getParentID();
                    return currentParentId != null && currentParentId == parentId;
                })
                .collect(Collectors.toList());
    }
}
