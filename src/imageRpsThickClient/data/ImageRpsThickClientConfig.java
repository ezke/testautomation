package imageRpsThickClient.data;

import org.jetbrains.annotations.NotNull;
import utils.DataBuilder;

import java.io.IOException;
import java.util.HashMap;

public class ImageRpsThickClientConfig {

    private static HashMap<String, String> loadConfigFile() throws IOException {
        return DataBuilder.GetConfig_DataFromFile("\\src\\imageRpsThickClient\\data\\Config.txt");
    }

    @NotNull
    public static DcmLoginCredentials getCredentials() throws IOException {
        HashMap<String, String> configData = loadConfigFile();
        return new DcmLoginCredentials(
                getValue(configData, "loginName"),
                getValue(configData, "loginPassword"),
                Integer.parseInt(getValue(configData, "station")));
    }

    public static String getWhiteSeedUrl() throws IOException {
        HashMap<String, String> configData = loadConfigFile();
        return getValue(configData, "whiteSeedUrl", "http://rps602auto1dcm:9995/Seed");
    }

    private static String getValue(HashMap<String, String> configData, String valueToGet) {
        return getValue(configData, valueToGet, null);
    }

    private static String getValue(HashMap<String, String> configData, String valueToGet, String defaultValue) {
        String value = "";
        value = System.getProperty(valueToGet);
        if (value == null) {
            value = configData.get(valueToGet);
        }
        if (value == null) {
            value = defaultValue;
        }

        if (value == null) {
            throw new IllegalStateException("Could not find configuration value '" + valueToGet + "'");
        }

        return value;
    }
}
