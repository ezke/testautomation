package imageRpsThickClient.data;

import utils.winiumXRay.XRayUtils;
import utils.DataBuilder;
import utils.Winium.WiniumBrowser;

import java.util.HashMap;

public class Data_Import {

	private static HashMap<String, String> config_Data;

	public static DcmLoginCredentials retrieve_EnvironmentData() throws Exception {
		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\imageRpsThickClient\\data\\Config.txt");

		DcmLoginCredentials loginCredentials = new DcmLoginCredentials(getValue("loginName"), getValue("loginPassword"),
				Integer.valueOf(getValue("station")));
//		GlobalVariables.loginName = getValue("loginName");
//		GlobalVariables.loginPassword = getValue("loginPassword");
//		GlobalVariables.station = getValue("station");

		GlobalVariables.winiumBrowserInfo = new WiniumBrowser(getValue("server"), Integer.valueOf(getValue("port")),
				"c:\\Selenium\\clntdvlp.lnk", "dcm.ini " + getValue("station"));


		//Browser.setWiniumServerString(getValue("server"), Integer.valueOf(getValue("port")));
		XRayUtils.setXrayLocation("\\\\rps602auto1dcm.qalabs.nwk\\k$\\DCMCode\\Test\\testingTools\\prog_info\\data\\STATION_"+ getValue("station") +"\\ProgInfo_DS.xml");

		return loginCredentials;
	}

	// Gets the specified value from first the System Property. If that is null it
	// reverts to config file.
	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}

		if (value != null) {
			System.setProperty(valueToGet, value);
		}
		return value;
	}

}// End of class
