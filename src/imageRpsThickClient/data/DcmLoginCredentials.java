package imageRpsThickClient.data;

public class DcmLoginCredentials {
    private final String _loginName;
    private final String _password;
    private final int _stationId;

    public DcmLoginCredentials(String loginName, String password, int stationId) {
        _loginName = loginName;
        _password = password;
        _stationId = stationId;
    }

    public String loginName() {
        return _loginName;
    }

    public String password() {
        return _password;
    }

    public int stationId() {
        return _stationId;
    }
}
