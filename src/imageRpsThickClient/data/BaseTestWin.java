package imageRpsThickClient.data;

import org.testng.annotations.BeforeTest;
import utils.Locator;

public abstract class BaseTestWin {

    public static DcmLoginCredentials _loginCredentials;

    @BeforeTest(alwaysRun = true)
    public void config_setup_method() throws Exception {
        _loginCredentials = Data_Import.retrieve_EnvironmentData();
        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\thickClientRepo.txt");
    }

//    public DcmLoginCredentials loadEnvironmentData() throws Exception {
//        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\thickClientRepo.txt");
//        return Data_Import.retrieve_EnvironmentData();
//    }

}
