package imageRpsThickClient.tests_prerelease;

import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRS_Winium.BaseTestWin_FRS;
import imageRpsThickClient.services.FRS_Winium.pages.FRSAdminApp_Page;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class FRS_AdvancedEnvirnomentSettings extends BaseTestWin_FRS {

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        FRS.windowsService().start();
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
    }

    @Test(groups = {"all_Tests", "regression_Tests"})
    public void PT1731_Verify_FRS_Advanced_CheckBox() throws Exception {
        BaseUI.checkCheckbox("advancedCheckBox");
        WebElement jobFileLabel = Locator.lookupRequiredElement("labelJobFilesPath");
        BaseUI.verifyElementHasExpectedAttributeValue(jobFileLabel, "Name","Job Files Path:");
        BaseUI.verifyElementAppears(jobFileLabel);
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("textBoxJobFilesPath"));
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
       new FRSAdminApp_Page().closeWindow();
    }

}//End of Class
