package imageRpsThickClient.tests_prerelease;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class ThickClient_Winium_Test extends BaseTestWin {
    @BeforeClass(alwaysRun=true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
    }

    @Test(enabled = false)
    public void test_comboBoxes() throws Exception {

        String lockboxName = "Shoptek2";
        String documentGroup = "Multiples";
        String workflow = "00000005 - Unstructured";

        DataCompletionOptions_Page dataCompletionOptions = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ConfigurationMenu()
                .navigate_WorkSetup()
                .navigate_ClientSetupMenu()
                .navigate_SelectAClient()
                .selectClient_AndNavigate_ClientSpecificConfigMenu(lockboxName)
                .navigate_DocumentGroupConfiguration()
                .selectAGroup_AndNavigate_DocumentSpecificGroupConfigurationMenu(documentGroup)
                .navigate_DataCompletionOptions();
        dataCompletionOptions._workflowComboBox.select_ComboBoxItem(workflow);
        dataCompletionOptions.exitApplication();
       // BaseUI.verify_true_AndLog(false, "Blah", "blah");
        Thread.sleep(100);
    }


    @Test(enabled = false)
    public void login_ErrorBox() throws Exception {

        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, 60);
        BaseUI.waitForElementToBeDisplayed("login_ID", null, null, 60);

        BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_ID"), _loginCredentials.loginName());
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_Password"), "dom");
        BaseUI.click(Locator.lookupElement("login_OK"));

        WebElement errorElement = Locator.lookupElement("login_ErrorMessage");
        String errorText = BaseUI.get_Attribute_FromField(errorElement, "Name");
        Thread.sleep(100);

    }


    @Test(enabled = true)
    public void test_RightClick_AndContextMenu() throws Exception {

        ViewReturns_byConsolidation_Page selectConsolidation =
                new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                        .navigate_RunImageRPS()
                        .navigate_DataMaintenanceMenu()
                        .navigate_ACH_MenuPage()
                        .navigate_ACH_MaintenanceMenuPage()
                        .navigate_ViewReturns_byConsolidation();


        WebElement consolidationPane = Locator.lookupElement("viewReturnsByConsol_SelectConsolidationFrame");

        Actions builder = new Actions(Browser.driver);
        builder.contextClick(consolidationPane).build().perform();

        BaseUI.click(Locator.lookupRequiredElement("viewReturnsByConsol_ContextMenu_ExportData"));


        Thread.sleep(100);
    }


    @Test(enabled = false)
    public void popupTest() throws Exception {

        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, 60);
        BaseUI.waitForElementToBeDisplayed("login_ID", null, null, 60);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.click(Locator.lookupRequiredElement("exitDialog_YesButton"));
        Thread.sleep(100);
    }




    @Test(enabled = false)
    public void test_Logout() throws Exception {

        ViewBatchesByBatchID_Page viewBatches =
                new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                        .navigate_RunImageRPS()
                        .navigate_DataMaintenanceMenu()
                        .navigate_BatchMenu()
                        .navigate_ViewBatchesByBatchID();


        viewBatches.exitApplication();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }
}
