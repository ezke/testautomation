package imageRpsThickClient.tests_prerelease;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DataCompletionOptions_Page;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.ViewBatchesByBatchID_Page;
import imageRpsThickClient.winiumPages.ViewReturns_byConsolidation_Page;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.Winium.WiniumBrowser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class ThickClient_AddMapping_FolderElement_Test {
    @BeforeClass(alwaysRun=true)
    public void setup_Method() throws Exception {
        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\thickClientRepo.txt");
        GlobalVariables.winiumBrowserInfo = new WiniumBrowser("rps602auto1sql.qalabs.nwk", 9999,
                "c:\\Selenium\\FRS\\RecoServiceAdmin.exe", null);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        Thread.sleep(100);
    }


    @Test(enabled = true)
    public void test_Folder_TextBox() throws Exception {


        BaseUI.waitForElementToBeDisplayed("environment", null, null);
        BaseUI.click(Locator.lookupRequiredElement("driveMappings_Tab"));
        BaseUI.click(Locator.lookupRequiredElement("addDriveMapping_Link"));
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("folderTextBox"), "folderTextBox");

        String textBoxText = Locator.lookupRequiredElement("folderTextBox").getText();

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }
}
