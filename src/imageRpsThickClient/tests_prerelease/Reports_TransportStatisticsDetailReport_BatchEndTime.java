package imageRpsThickClient.tests_prerelease;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_TransportStatisticsDetailReport_BatchEndTime extends BaseTestWin {

    private String _mainHandle;
    private UtilityMenu_Page _utilityMenu;
    private GenericTableBrowser _genericTableBrowser;

    private TransportDetailBySystemDate_Page _reportPage;

    private String _fileNameRPT = "strpt3.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "strpt3.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _fromDate = "01012014";

    private String _toDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");


    private String[] _reportExtractRPT;
    private TableData _rptTable;
    private TableData _csvData;
    private String[] _csvExtract;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password());
        _utilityMenu =    UtilityMenu_Page.navigate_UtilityMenu();

        _mainHandle = Locator.lookupRequiredElement("dcm_PageClassName").getAttribute("NativeWindowHandle");

        _genericTableBrowser = _utilityMenu.navigate_GenericTableBrowser();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2357_Validate_TableViewer_ColumnsPresent() throws Exception {
        String queryToPick = "stat_7780";
        _genericTableBrowser.enter_TableQuery(queryToPick);

        //Need some kind of validation to see inside of table and verify fields are present.
        //Currently blocked as I need Mike McMillan to fix issue where I can't see this table via XRay
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2357_Navigate_ToReport() throws Exception {
        _genericTableBrowser.exitApplication();

        _reportPage = _utilityMenu.navigate_BackTo_DevelopmentMenu()
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_TransportPerformanceStatisticsReports()
                .navigate_TransportDetailBySystemDate();
        _reportPage._outputAndRunOptions.remove_All_ButOption("Operator ID");
        _reportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table("Client ID");
        _reportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table("Begin Time");

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_Dates(_fromDate, _toDate);

        _reportPage.runReport();


        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");
        _rptTable = _reportPage.get_RPT_Table(_reportExtractRPT);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);


        //Need to Add tests here.  Currently not enough data to validate the criteria outlined in the test case.
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        //_reportPage.exitApplication();
    }
}
