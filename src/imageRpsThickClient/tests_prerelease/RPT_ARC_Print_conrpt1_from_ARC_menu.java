package imageRpsThickClient.tests_prerelease;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DataCompletionOptions_Page;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.PrintSummaryReport_byReturnDate_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class RPT_ARC_Print_conrpt1_from_ARC_menu extends BaseTestWin {

    private PrintSummaryReport_byReturnDate_Page _printSummaryPage;

    @BeforeClass(alwaysRun=true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
    }

    @Test(enabled = true)
    public void PT2200_Navigate_PrintSummaryReport_ByReturnDate_Validate_ErrorNotPresent() throws Exception {

        String consolidationToPick = "Consol Test 2";

        _printSummaryPage =  new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_DataMaintenanceMenu()
                .navigate_ACH_MenuPage()
                .navigate_ACH_MaintenanceMenuPage()
                .navigate_ViewReturns_byConsolidation()
                .select_ConsolidationByName_AndNavigate(consolidationToPick)
                .navigate_OtherReportsForConsolidation()
                .navigate_PrintSummaryReportByReturnDate();

        _printSummaryPage.verify_ErrorNotPresent();
        Thread.sleep(100);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }


    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        _printSummaryPage.exitApplication();
    }
}
