package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;


public class ViewBatchesByBatchID_Page extends DCMBasePage {

    private ViewBatchesByBatchID_SelectABatchPage selectBatchID = null;

    public void enter_BatchIDRange(int lowerRange, int upperRange) throws Exception {
        enter_BatchID_LowerRange(lowerRange);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("viewBatch_BatchIDUpperRange_TextBox"), String.valueOf(upperRange));
    }

    public void enter_BatchID_LowerRange(int batchID) throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("viewBatch_BatchIDLowerRange_TextBox"));
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("viewBatch_BatchIDLowerRange_TextBox"), String.valueOf(batchID));
        Thread.sleep(1500);
    }


    public Batch_QueryResults_Page navigate_BatchQueryResult() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.F1);
        BaseUI.wait_forPageToFinishLoading();
        BaseUI.waitForElementToBeDisplayed("viewBatch_BatchID_SelectABatch_WindowHeader", null, null, 50);
        Thread.sleep(3000);

        return new Batch_QueryResults_Page();
    }

    public ViewBatchesByBatchID_Page(){

        BaseUI.waitForElementToBeDisplayed("viewBatch_BatchIDLabel", null, null, 60);
    }

    public Batch_QueryResults_Page filldata_navigate_BatchQueryResult(String batchID) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batchID_TextBox"), batchID);

        BaseUI.send_KeyWithActionBuilder(Keys.F1);

        return new Batch_QueryResults_Page();
    }

    public void deleteCopiedFilesFromAllFolders(String p1DataFileCopied) throws Exception {
        enter_BatchID_LowerRange(Integer.valueOf(p1DataFileCopied.substring(3).replace(".dat", "")));
        navigate_BatchQueryResult();

        BaseUI.click(Locator.lookupRequiredElement("topMenuPanel_DeleteButton"));
        BaseUI.waitForElementToBeDisplayed("deleteBatchPage_DeleteBatchFromSystemOption", null, null, 40);
        BaseUI.click(Locator.lookupElement("deleteBatchPage_DeleteBatchFromSystemOption"));
        BaseUI.click(Locator.lookupElement("deleteBatchPage_OkBtn"));
        BaseUI.waitForElementToBeDisplayed("batchDeletePopUp_Header", null, null, 40);
        BaseUI.click(Locator.lookupElement("batchDeletePopUp_YesBtn"));
        BaseUI.waitForElementToNOTBeDisplayed("batchDeletePopUp_YesBtn", null, null);
    }
}
