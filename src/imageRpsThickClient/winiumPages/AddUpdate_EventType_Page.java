package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;

public class AddUpdate_EventType_Page {

    public EventTypeSetup_Page navigate_BackTo_EventTypeSetup() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new EventTypeSetup_Page();
    }

    public void check_KeepEventData_Checkbox() {
        BaseUI.checkCheckbox("addUpdateEventType_KeepEventData_Checkbox");
    }

    public AddUpdate_EventType_Page() throws Exception {
        BaseUI.waitForElementToBeDisplayed("addUpdateEventType_KeepEventData_Checkbox", null, null);
        Thread.sleep(500);
    }
}
