package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class BatchMenu_Page extends DCMBasePage {

    public ViewBatchesByBatchID_Page navigate_ViewBatchesByBatchID() throws Exception{
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "1");
        return new ViewBatchesByBatchID_Page();
    }


    public BatchMenu_Page(){

    }


}
