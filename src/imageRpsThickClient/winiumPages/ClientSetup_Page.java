package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ClientSetup_Page extends DCMBasePage {
    public SelectAClient_Page navigate_SelectAClient() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "1");
        return new SelectAClient_Page();
    }

    public ClientSetup_Page(){

    }
}
