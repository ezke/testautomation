package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class RecognitionStatsReport_Page extends DCMBasePage {

    public final ComboBoxControl _recoVendorComboBox = new ComboBoxControl("recognitionReport_RecoVendor", null, null);

    public final ComboBoxControl _documentTypeComboBox = new ComboBoxControl("recognitionReport_DocType", null, null);

    public RecognitionStatsReport_Page() {

    }

    public void enterStatsFromDate(String dateToEnter) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("recognitionReport_StatsFromDate_TextBox"), dateToEnter);
        Thread.sleep(500);
    }

    public void enterThroughDate(String dateToEnter) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("recognitionReport_Through_TextBox"), dateToEnter);
        Thread.sleep(500);
    }

    public void enter_SaveToFile(String textToEnter) {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("recognitionReport_SaveToFile_TextBox"), textToEnter);
    }

    public RecognitionStatsReport_Page run_Report() throws Exception {
        click_RunNow_DontViewReport();
        return new RecognitionStatsReport_Page();
    }

    public void click_RunNow_DontViewReport() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("recognitionReport_RunNow_Button"));
        BaseUI.waitForElementToBeDisplayed("recognitionReport_OpenReportModal", null, null, Duration.ofMinutes(3));
       //Clicks Yes- To overwrite existing file
        BaseUI.click(Locator.lookupRequiredElement("recognitionReport_DialogBox_YesButton"));
        BaseUI.waitForElementToBeDisplayed("recognitionReport_OpenReportModal", null, null, Duration.ofMinutes(3));
        //Clicks No - Not to View Report
        BaseUI.click(Locator.lookupRequiredElement("recognitionReport_DialogBox_NoButton"));
        BaseUI.waitForElementToNOTBeDisplayed("recognitionReport_DialogBox_NoButton", null, null);

    }
}
