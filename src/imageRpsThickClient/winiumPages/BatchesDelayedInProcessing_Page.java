package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class BatchesDelayedInProcessing_Page extends BatchReport_PageBase {

    public void enter_ReportBatchesThatTookMoreThan(String textToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batDelInProcRep_ReportBatchesThatTookMoreThan_TextBox"), textToEnter);
    }

    public void check_IncludeBatchesThatHaveReached_OUT(){
        BaseUI.checkCheckbox("batDelInProcRep_IncludeBatchesOUT_Checkbox");
    }

    public void select_BatchID_Radio(){
        BaseUI.click(Locator.lookupRequiredElement("batDelInProcRep_BatchID_Radio"));
    }

    public void select_BatchNumber_Radio(){
        BaseUI.click(Locator.lookupRequiredElement("batDelInProcRep_BatchNumber_Radio"));
    }

    public BatchesDelayedInProcessing_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchesDelayedInProcessing_Page();
    }

}
