package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

import java.util.HashMap;

public class GeneralBatchInformation_Page extends DCMBasePage {

    public TableData get_AuditTrailTable() throws Exception {

        TableData auditTrailTable = XRayUtils.return_Table(new String[]{"Job", "Op ID", "StnID", "Started", "Ended", "Secs", "Time Between Jobs"});

        return auditTrailTable;
    }

    //Needed different method for this as I needed the LAST matching entry versus the First matching entry.
    public HashMap<String, String> return_Row_BasedOn_1MatchingField(TableData tableToCheck, String key1, String expectedValue1) {
        HashMap<String, String> rowToReturn = new HashMap<>();
        for (HashMap<String, String> row : tableToCheck.data) {
            String value1 = row.get(key1);

            if (value1 != null && value1.equals(expectedValue1)) {
                rowToReturn = row;
            }
        }

        return rowToReturn;
    }

    public SingleBatchOperations_Page navigate_BackTo_SingleBatchOperations_Page() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new SingleBatchOperations_Page();
    }

    public GeneralBatchInformation_Page() throws Exception {
        waitForPageToLoad();
    }

}
