package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class QueueItem_Page extends DCMBasePage{

    public QueueItem_Page(){

    }

    public String return_ErrorMessage(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("queueItem_ErrorMessage"));
    }

    public void unCheck_QueueRecordIsLocked() throws Exception {
        BaseUI.uncheckCheckbox(Locator.lookupRequiredElement("queueItem_QueueRecordLocked_CheckBox"));
        Thread.sleep(100);
    }

    public void unCheck_ErrorsWhenRun() throws Exception {
        BaseUI.uncheckCheckbox(Locator.lookupRequiredElement("queueItem_ErrorsWhenRun_CheckBox"));
        Thread.sleep(100);
    }

    public void click_Ok_Button(){
        BaseUI.click(Locator.lookupRequiredElement("queueItem_OK_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("queueItem_OK_Button", null, null);
    }

    public MaintainQueue_Page navigate_BackTo_MaintainQueue(){
        click_Ok_Button();
        return new MaintainQueue_Page();
    }
}
