package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class MainMenu_Page extends DCMBasePage {

    public DataMaintenanceMenu_Page navigate_DataMaintenanceMenu() throws Exception {
        //BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "2");
        BaseUI.send_KeyWithActionBuilder("2");
        return new DataMaintenanceMenu_Page();
    }

    public ConfigurationMenu_Page navigate_ConfigurationMenu() throws Exception{
        //BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "5");
        BaseUI.send_KeyWithActionBuilder("5");
        return new ConfigurationMenu_Page();
    }

    public ReportsMenu_Page navigate_ReportsMenu() throws Exception{
        //BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "5");
        BaseUI.send_KeyWithActionBuilder("3");
        return new ReportsMenu_Page();
    }

    public QueueAndRecognition_MenuPage navigate_QueueAndRecognitionMenu(){
        BaseUI.send_KeyWithActionBuilder("4");
        return new QueueAndRecognition_MenuPage();
    }

    public MainMenu_Page(){
        BaseUI.click(Locator.lookupRequiredElement("dcm_PageClassName"));
        BaseUI.waitForElementToBeDisplayed("rpsMainMenu_DataCompletionMenu", null, null, 60);
    }

}
