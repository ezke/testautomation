package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class ConfigurationMenu_Page extends DCMBasePage {
    public WorkSetup_Page navigate_WorkSetup() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "1");
        return new WorkSetup_Page();
    }

    public Config_NotificationsAndEvents_Page navigate_NotificationsAndEvents() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "8");
        return new Config_NotificationsAndEvents_Page();
    }

    public Config_SystemSetup_Page navigate_SystemSetup() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "2");
        return new Config_SystemSetup_Page();
    }

    public MainMenu_Page navigate_BackTo_MainMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }

    public ConfigurationMenu_Page(){

    }
}
