package imageRpsThickClient.winiumPages;

import org.awaitility.Awaitility;
import org.openqa.selenium.WebElement;
import utils.winiumXRay.XRayUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

import java.io.InputStreamReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class DCMBasePage {

    public void exitApplication() throws Exception {

        int loopCount = 0;

        int handleCount = Browser.driver.getWindowHandles().size();

        while (loopCount < 50) {
            int currentHandleCount = Browser.driver.getWindowHandles().size();
            if (currentHandleCount > handleCount && BaseUI.elementAppears(Locator.lookupElement("exitDialog_ExitSystem"))) {
                BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
                break;
            }else if (currentHandleCount > handleCount){

                Actions builder = new Actions(Browser.driver);
                builder.keyDown(Keys.LEFT_ALT);

                BaseUI.send_KeyWithActionBuilder("n");
                builder.keyUp(Keys.LEFT_ALT);
            } else if (currentHandleCount < handleCount){
                handleCount = currentHandleCount;
            }
            BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);

            loopCount++;
        }

        BaseUI.wait_ForCondition_ToBeMet(()->{


                Process process = Runtime.getRuntime().exec("tasklist.exe");
                Scanner scanner = new Scanner(new InputStreamReader(process.getInputStream()));

                ArrayList<String> applicationList = new ArrayList<>();

                while (scanner.hasNext()) {
                    String scannerText = scanner.nextLine();
                    applicationList.add(scannerText);

                }
                scanner.close();

                return !arrayListContainsPartialText(applicationList, "Prowin32");




        }, Duration.ofSeconds(30),Duration.ofSeconds(1));
        Thread.sleep(1000);
    }

    private boolean arrayListContainsPartialText(ArrayList<String> listToCheck, String partialTextMatch)
    {
        for(String entry : listToCheck){
            if(entry.contains(partialTextMatch)){
                return true;
            }
        }
        return false;
    }

    public void verify_ErrorListBox_Empty() {
        WebElement errorListBox = Locator.lookupRequiredElement("dcm_ErrorList");
        BaseUI.verifyElementHasExpectedAttributeValue(errorListBox, "Name", "");
    }

    public void waitForPageToLoad() throws Exception {
        Thread.sleep(500);
        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, 40);
    }

    public void wait_for_ConditionToMeet(Callable<Boolean> conditionToMeet, Duration timeToWait, Duration pollingInterval) {

        Awaitility.await().pollInterval(pollingInterval.toMillis(), TimeUnit.MILLISECONDS)
                .atMost(timeToWait.toMillis(), TimeUnit.MILLISECONDS).until(conditionToMeet);
    }

    public void scroll_TopOfGrid() throws Exception {

        XRayUtils.invalidateXRay();
        Actions builder = new Actions(Browser.driver);
        builder.keyDown(Keys.CONTROL);
        builder.build().perform();
        builder.sendKeys(Keys.HOME).build().perform();
        builder.keyUp(Keys.CONTROL).build().perform();
        BaseUI.send_KeyWithActionBuilder(Keys.SPACE);
    }


    public DCMBasePage(){
        XRayUtils.invalidateXRay();
    }


}//End of Class
