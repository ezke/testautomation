package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.winiumXRay.XRayUtils;

import java.util.ArrayList;

public class GenericTableBrowser extends DCMBasePage {

    public ComboBoxControl _tableQueryComboBox = new ComboBoxControl("genericTableBrowser_QueryDropdown", null, null);

    public GenericTableBrowser() throws Exception {
        BaseUI.waitForElementToBeDisplayed("genericTableBrowser", null, null, 30);

    }

    public void enter_TableQuery(String textToEnter) throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("genericTableBrowser_QueryDropdown"));
        BaseUI.enterText_IntoInputBox(Browser.driver.switchTo().activeElement(), textToEnter);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
    }


    @Override
    public void exitApplication() throws Exception {
        WebElement genericTableBrowser = Locator.lookupRequiredElement("genericTableBrowser");
        BaseUI.click(genericTableBrowser.findElement(By.name("Close")));
        maximize_OriginalWindow();
        XRayUtils.invalidateXRay();

    }

    //Had issues targeting directly, had to resort to a workaround.
    private void maximize_OriginalWindow() throws Exception {
        ArrayList<WebElement> taskbarApps = new ArrayList<>(Locator.lookupRequiredElement("windowsTaskbar").findElements(By.xpath(".//*")));
        WebElement proWindow = null;
        for(WebElement taskBarApp : taskbarApps){
            String name = BaseUI.get_Attribute_FromField(taskBarApp, "Name");
            if(name.contains("prowin32.exe")){
                proWindow = taskBarApp;
                break;
            }
        }

        BaseUI.click(proWindow);
    }
}
