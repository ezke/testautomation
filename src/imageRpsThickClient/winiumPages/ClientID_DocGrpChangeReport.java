package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.RPT_Report;
import org.apache.commons.lang3.Range;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.HashMap;

public class ClientID_DocGrpChangeReport extends BatchReport_PageBase {


    public ClientID_DocGrpChangeReport runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ClientID_DocGrpChangeReport();
    }

    public void enter_Dates(String fromDate, String toDate){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("clientIDDocGrpChangeRep_FromEventDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("clientIDDocGrpChangeRep_ToEventDate"), toDate);
    }

    public TableData get_RPT_ReportTable(String[] rptExtract){
        TableData reportTable;
        RPT_Report report = new RPT_Report(rptExtract);


        HashMap<String, Range<Integer>> mappings = new HashMap<>();
        mappings.put("CapturedAsBatch", Range.between(16, 23));
        mappings.put("CapturedAsClnt", Range.between(23, 28));
        mappings.put("CapturedAsDG", Range.between(29,34));
        mappings.put("P1Seq", Range.between(36, 40));
        mappings.put("EventJob", Range.between(41, 45));
        mappings.put("Operator", Range.between(46, 54));
        mappings.put("DateTime", Range.between(55, 71));
        mappings.put("Stn", Range.between(73, 78));
        mappings.put("ChangedFromBatch", Range.between(79, 85));
        mappings.put("ChangedFromClnt", Range.between(87, 91));
        mappings.put("ChangedFromDG", Range.between(92, 95));
        mappings.put("**", Range.between(96, 98));
        mappings.put("ChangedToBatch", Range.between(99, 105));
        mappings.put("ChangedToClnt", Range.between(107, 111));
        mappings.put("ChangedToDG", Range.between(112, 114));
        mappings.put("CurrentBatch", Range.between(117, 123));
        mappings.put("CurrentTran", Range.between(125, 129));
        mappings.put("CurrentClnt", Range.between(132, 136));
        mappings.put("CurrentDG", Range.between(137, 140));
        mappings.put("CurrentJob", Range.between(142, 146));
        mappings.put("Value", Range.between(147, 160));
        mappings.put("Status", Range.between(162, 172));


        reportTable = report.get_Report_Table(Range.between(66,71), RPT_Report.RPT_Matcher.TIME, mappings);


        return reportTable;
    }

}
