package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;

public class QueueAndRecognition_MenuPage extends DCMBasePage{

    public QueueAndRecognition_MenuPage(){

    }

    public MaintainQueue_Page navigate_MaintainQueue(){
        BaseUI.send_KeyWithActionBuilder("3");
        return new MaintainQueue_Page();
    }

    public MainMenu_Page navigate_BackTo_MainMenuPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }

    public QueueStatusConsol_Page navigate_QueueStatusConsol(){
        BaseUI.send_KeyWithActionBuilder("2");
        return new QueueStatusConsol_Page();
    }
}
