package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ACH_MaintenanceMenu_Page extends DCMBasePage {

    public ViewReturns_byConsolidation_Page navigate_ViewReturns_byConsolidation() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "3");
        return new ViewReturns_byConsolidation_Page();
    }

    public ACH_MaintenanceMenu_Page(){

    }
}
