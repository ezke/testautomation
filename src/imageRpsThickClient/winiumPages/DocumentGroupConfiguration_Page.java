package imageRpsThickClient.winiumPages;

import utils.winiumXRay.XRayUtils;
import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;

public class DocumentGroupConfiguration_Page extends DCMBasePage {


    public void selectAGroup(String groupToSelect) throws Exception {
        String[] headers = {"ID", "Doc Group Name"};
        TableData selectClientTable = XRayUtils.return_Table(headers);
        XRayUtils.navigate_ToLocation_InTable(selectClientTable, "Doc Group Name", groupToSelect);
    }


    public DocumentSpecificGroupConfigurationMenu_Page selectAGroup_AndNavigate_DocumentSpecificGroupConfigurationMenu(String groupToSelect) throws Exception{
        selectAGroup(groupToSelect);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new DocumentSpecificGroupConfigurationMenu_Page();
    }

    public DocumentGroupConfiguration_Page(){

    }


}//End of Class
