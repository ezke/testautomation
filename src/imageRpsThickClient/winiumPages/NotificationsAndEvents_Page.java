package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class NotificationsAndEvents_Page extends DCMBasePage {

    public NotificationsAndEvents_Page(){

    }

    public ViewEvents_Page navigate_ViewEvents() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "1");
        return new ViewEvents_Page();
    }
}
