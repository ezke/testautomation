package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class ACH_ConsolidationMenu_OtherReports_Page extends DCMBasePage {

    public PrintSummaryReport_byReturnDate_Page navigate_PrintSummaryReportByReturnDate(){
        BaseUI.send_KeyWithActionBuilder("3");
        return new PrintSummaryReport_byReturnDate_Page();
    }

    public ACH_ConsolidationMenu_OtherReports_Page(){

    }

}
