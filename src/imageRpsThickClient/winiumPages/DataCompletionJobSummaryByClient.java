package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class DataCompletionJobSummaryByClient extends BatchReport_PageBase {

    public final ComboBoxControl _operatorFilterDropdown = new ComboBoxControl("dataCompJobSumByClientRep_OperatorFilter_Dropdown", null, null);


    public void enter_Dates(String fromDate, String throughDate){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("dataCompJobSumByClientRep_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("dataCompJobSumByClientRep_ThroughDate"), throughDate);
    }

    public DataCompletionJobSummaryByClient runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new DataCompletionJobSummaryByClient();
    }

    public void enter_OperatorFilter_TextBox(String textToEnter) throws Exception {
        WebElement textBox = Locator.lookupRequiredElement("dataCompJobSumByClientRep_OperatorFilter_TextBox");
        Actions select = new Actions(Browser.driver);
        select.doubleClick(textBox).build().perform();
        BaseUI.send_KeyWithActionBuilder(Keys.DELETE);
        BaseUI.enterText_IntoInputBox(textBox, textToEnter);
    }

    public OperatorPerformanceStatisticsReports_Page navigate_BackTo_OperatorPerformanceStatisticsReports() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new OperatorPerformanceStatisticsReports_Page();
    }

    public void check_FailIfNoData_Checkbox(){
        BaseUI.checkCheckbox("dataCompJobSumByClientRep_FailIfNoData_Checkbox");
    }

    public DataCompletionJobSummaryByClient click_OKOnError(){
      click_OK_OnError();
      return new DataCompletionJobSummaryByClient();
    }


    public void check_PrintDetail_Checkbox() throws Exception {
        BaseUI.checkCheckbox("dataCompJobSumByClientRep_PrintDetail_Checkbox");
    }

}
