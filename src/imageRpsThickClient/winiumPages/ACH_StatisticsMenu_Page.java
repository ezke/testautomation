package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class ACH_StatisticsMenu_Page extends DCMBasePage {

    public ACH_StatisticsReportsMenu_Page navigate_ACHStatisticsReportMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        return new ACH_StatisticsReportsMenu_Page();
    }

}
