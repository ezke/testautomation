package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import utils.BaseUI;
import utils.Locator;

public class BatchTimelineReport_Page extends BatchReport_PageBase {

    public final ComboBoxControl _reportDateType = new ComboBoxControl("batTimelineRep_ReportDateType_Dropdown", null, null);

    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batTimelineRep_ProcessDateFrom_Textbox"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batTimelineRep_ProcessDateTo_Textbox"), toDate);
    }

    public void enter_JobRange(String fromJob, String toJob) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batTimelineRep_JobFrom_Textbox"), fromJob);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batTimelineRep_JobTo_Textbox"), toJob);
    }

    //Date format: MM/dd/yy
    public void verify_DateRange_Line2(String[] reportExtract, String expectedDateType, String dateRangeFrom, String dateRangeTo) {
        String line2 = reportExtract[1].replaceAll("\\s+", " ");

        String expectedValue = "For " + expectedDateType + "(s): " + dateRangeFrom + " - " + dateRangeTo;
        BaseUI.baseStringPartialCompare("Date Range", expectedValue, line2);
    }

    public BatchTimelineReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchTimelineReport_Page();
    }
}//End of Class
