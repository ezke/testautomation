package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class EventTypeSetup_Page extends DCMBasePage {

    public AddUpdate_EventType_Page navigate_AddUpdate_EventType(String eventID) throws Exception {
        TableData reportTable = XRayUtils.return_Table(new String[]{"EventID"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "EventID", eventID);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);

        return new AddUpdate_EventType_Page();
    }

    public Config_NotificationsAndEvents_Page navigate_BackTo_NotificationsAndEvents() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new Config_NotificationsAndEvents_Page();
    }


    public EventTypeSetup_Page() throws Exception {
        waitForPageToLoad();
    }
}
