package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class Login_Page extends DCMBasePage {

    public DevelopmentMenu_Page login(String loginName, String password) throws Exception {

        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, 60);
        BaseUI.waitForElementToBeDisplayed("login_ID", null, null, 60);

        BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_ID"), loginName);
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_Password"), password);
        BaseUI.click(Locator.lookupElement("login_OK"));

        return new DevelopmentMenu_Page();
    }

    public Login_Page(){

    }

}
