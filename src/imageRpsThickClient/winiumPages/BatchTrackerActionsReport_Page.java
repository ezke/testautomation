package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class BatchTrackerActionsReport_Page extends BatchReport_PageBase {


    public void check_RejectsCheckbox(){
        BaseUI.checkCheckbox("batchTrackActsRep_RejectsCheckbox");
    }

    public String get_RPTLineContainingValue(String[] rptExtract, String valueToContain){
        for(String line : rptExtract){

            if(line.contains(valueToContain)){
                return line;
            }


        }
        return null;
    }

    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batchTrackActsRep_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batchTrackActsRep_ToDate"), toDate);
    }



    public BatchTrackerActionsReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchTrackerActionsReport_Page();
    }



    public BatchTrackerActionsReport_Page(){

    }

}
