package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class TransportSummaryBySystemDate_Report_Page extends BatchReport_PageBase {

    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("tranStatSumRep_FromProcessingDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("tranStatSumRep_ThroughProcessingDate"), toDate);
    }

    public TransportPerformanceStatisticsReports_Page navigate_BackTo_TransportPerformanceStatisticsReports() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new TransportPerformanceStatisticsReports_Page();
    }

    public TransportSummaryBySystemDate_Report_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new TransportSummaryBySystemDate_Report_Page();
    }
}//End of Class
