package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class DepositMaintenance_Page extends DCMBasePage {

    public SelectADeposit_Page click_OK_navigate_SelectADeposit(){
        BaseUI.click(Locator.lookupRequiredElement("dataMaint_OK_Button"));
        return new SelectADeposit_Page();
    }

}
