package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class SelectATransactionToDisplay_Page {

    private void selectOptionInList_ByDescription(String dataSelect) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Tran"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Tran", dataSelect);
    }

    public SelectAnItemToDisplay_Page navigate_SelectAnItemToDisplay_Page_BySelectingFirstEntry() throws Exception {
        selectOptionInList_ByDescription("1");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new SelectAnItemToDisplay_Page();
    }


}
