package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class SiteReportsMenu_Page extends DCMBasePage {

    public SiteDepositReport_Page navigate_SiteDepositReport(){
        BaseUI.send_KeyWithActionBuilder("1");
        return new SiteDepositReport_Page();
    }

    public SiteConsolidationReport_Page navigate_SiteConsolidationReport(){
        BaseUI.send_KeyWithActionBuilder("4");
        return new SiteConsolidationReport_Page();
    }

    public BatchCapture_ReconciliationReport_Page navigate_BatchCaptureReconciliation(){
        BaseUI.send_KeyWithActionBuilder("5");
        return new BatchCapture_ReconciliationReport_Page();
    }


}
