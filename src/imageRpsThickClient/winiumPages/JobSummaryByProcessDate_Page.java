package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class JobSummaryByProcessDate_Page extends BatchReport_PageBase {
    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("jobSumByProcDate_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("jobSumByProcDate_ToDate"), toDate);
    }

    public void enter_Job(String job){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("jobSumByProcDate_Job_TextBox"), job);
    }

    public JobSummaryByProcessDate_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new JobSummaryByProcessDate_Page();
    }

    public void check_ShowDailyDetail_Checkbox(){
        BaseUI.checkCheckbox("jobSumByProcDate_ShowDailyDetail_Checkbox");
    }


}
