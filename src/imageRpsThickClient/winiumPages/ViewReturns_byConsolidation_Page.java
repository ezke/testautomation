package imageRpsThickClient.winiumPages;

import utils.winiumXRay.XRayUtils;
import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;

public class ViewReturns_byConsolidation_Page extends DCMBasePage {

    public ACH_Consolidation_Menu_Page select_ConsolidationByName_AndNavigate(String consolidationToPick) throws Exception {
        TableData consolidationTable = XRayUtils.return_Table(new String[]{"Name"});
        XRayUtils.navigate_ToLocation_InTable(consolidationTable, "Name", consolidationToPick);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);

        return new ACH_Consolidation_Menu_Page();
    }


    public ViewReturns_byConsolidation_Page() {

        BaseUI.waitForElementToBeDisplayed("viewReturnsByConsol_PrintButton", null, null, 30);
    }
}
