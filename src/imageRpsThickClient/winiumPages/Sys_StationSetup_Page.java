package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class Sys_StationSetup_Page extends DCMBasePage {
    private void selectOptionInList_ByDescription(String id) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"ID"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "ID", id);
    }

    public SelectAStation_Page navigate_SelectAStation() throws Exception {
        selectOptionInList_ByDescription("26");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new SelectAStation_Page();
    }

    public MainMenu_Page navigate_MainPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }

}
