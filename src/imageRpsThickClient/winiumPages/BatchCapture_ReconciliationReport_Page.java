package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.*;
import utils.BaseUI;
import utils.Locator;

public class BatchCapture_ReconciliationReport_Page extends BatchReport_PageBase {

    public ComboBoxControl _reportDateType = new ComboBoxControl("batchCapReconcilRep_ReportDateType_Dropdown", null, null);
    public ComboBoxControl _typeOfReport = new ComboBoxControl("batchCapReconcilRep_TypeOfReport_Dropdown", null, null);
    public TextBox _dateToReport_TextBox = new TextBox("batchCapReconcilRep_DateToReport_Textbox", null, null);
    public Checkbox _failIfNoData_Checkbox = new Checkbox("depositSumReport_FailIfNoData_Checkbox", null, null);

    public Filter_ComboBoxControl _clientFilterDropdown = new Filter_ComboBoxControl("batchCapReconcilRep_ClientFilter_Dropdown", null, null);
    public Filter_ComboBoxControl _operatorFilterDropdown = new Filter_ComboBoxControl("batchCapReconcilRep_OperatorFilter_Dropdown", null, null);

    public Reports_OutputAndRunOptions _outputAndRunOptions = new Reports_OutputAndRunOptions("Batch Capture Reconciliation");


    public BatchCapture_ReconciliationReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchCapture_ReconciliationReport_Page();
    }
}
