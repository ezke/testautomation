package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class EventsResult_Page extends DCMBasePage{

    public EventsResult_Page() throws Exception {
        waitForPageToLoad();
    }

    public TableData get_EventIDAndEventOccuredDT() throws Exception {
        return XRayUtils.return_Table(new String[]{"EventID", "wkMsgString", "EventOccuredDT"});
    }

    public TableData get_EventSeqAndData1() throws Exception {
        return XRayUtils.return_Table(new String[]{"EventSeq", "EventID", "Data 1", "Data 2"});
    }

    public static void clickCancelButton() throws Exception {
        BaseUI.click(Locator.lookupElement("eventData_EventIDResultPage_CancelButton"));
        Thread.sleep(500);
    }

    public static void clickEnterKey_NavigateToEventID_WithFileConversionStartedPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_StartedStatus_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithFileConversionSuccessPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_SuccessStatus_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithFileConversionStartInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_BatchConversionStart_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithFileConversionStopInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_BatchConversionStop_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithFileConversionExitInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_BatchConversionExit_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithFileConversionFailureInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_BatchConversionFailure_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithXMLFileConversionFailureInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_XMLBatchConversionFailure_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithNoGrayImageInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_NoGrayImageStatus_TextBox", null, null, 40);
    }

    public static void clickEnterKey_NavigateToEventID_WithXMLBatchImportFileConversionFailureInstancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.waitForElementToBeDisplayed("eventData_EventIDResultPage_XMLBatchImportConversionFailure_TextBox",
                null, null, 40);
    }

    public static void verifyEventOccuredDate(String firstDateRange, String finalDateRange, String dateFormat) throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_OccuredDate"));
        String eventDateOccured = BaseUI.getTextFromField(Locator.lookupElement("eventData_EventIDResultPage_OccuredDate"));
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDateOccured, dateFormat);
    }

    public static void verifyEventLoggedDate(String firstDateRange, String finalDateRange, String dateFormat) throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_LoggedDate"));
        String eventDateLogged = BaseUI.getTextFromField(Locator.lookupElement("eventData_EventIDResultPage_LoggedDate"));
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDateLogged, dateFormat);
    }
}
