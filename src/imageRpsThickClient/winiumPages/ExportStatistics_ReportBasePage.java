package imageRpsThickClient.winiumPages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class ExportStatistics_ReportBasePage extends DCMBasePage {

    public void enterDate(String date) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("expDailTranStatFile_Date_TextBox"), date);
    }

    public void clickOK() {
        BaseUI.click(Locator.lookupRequiredElement("expDailTranStatFile_OK_Button"));
        BaseUI.waitForElementToBeDisplayed("expDailTranStatFile_StatExportModal", null, null, 20);
    }

    public void verify_StatExport_ModalAppears(String fileNameWithPath) {

        WebElement statExportModal = Locator.lookupRequiredElement("expDailTranStatFile_StatExportModal");
        BaseUI.verifyElementAppears(statExportModal);
        BaseUI.verifyElementHasExpectedAttributeValue(statExportModal, "Name", "Stat Export (Press HELP to view stack trace)");

        WebElement modalMessage = Locator.lookupRequiredElement("expDailTranStatFile_StatExportModal_Message");
        BaseUI.verifyElementAppears(modalMessage);
        BaseUI.verifyElementHasExpectedAttributeValue(modalMessage, "Name", "Export report, " +
                fileNameWithPath +", has been created.");
    }

    public ExportStatisticsMenu_Page click_OK_OnModal_Navigate_ExportStatisticsMenu(){
        BaseUI.click(Locator.lookupRequiredElement("expDailTranStatFile_StatExportModal_OK_Button"));

        return new ExportStatisticsMenu_Page();
    }

}
