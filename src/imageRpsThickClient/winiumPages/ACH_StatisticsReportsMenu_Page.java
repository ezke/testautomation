package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class ACH_StatisticsReportsMenu_Page extends DCMBasePage {

    public ACH_Reports_Page navigate_SummaryStatisticsReport() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        return new ACH_Reports_Page("ARC Summary Statistics Report");
    }

    public ACH_Reports_Page navigate_DailyStatisticsReport() throws Exception {
        BaseUI.send_KeyWithActionBuilder("2");
        return new ACH_Reports_Page("ARC Daily Statistics Report");
    }

    public ACH_Reports_Page navigate_ACHReasonStatisticsReport() throws Exception {
        BaseUI.send_KeyWithActionBuilder("3");
        return new ACH_Reports_Page("ARC Reason Statistics Report");
    }

    public ACH_Reports_Page navigate_ACHReasonByReturnCodeStatisticsRpt() throws Exception {
        BaseUI.send_KeyWithActionBuilder("4");
        return new ACH_Reports_Page("ARC Reason by Return Code Statistics Report");
    }

    public ACH_Reports_Page navigate_ReturnCodeStatisticsReport() throws Exception {
        BaseUI.send_KeyWithActionBuilder("5");
        return new ACH_Reports_Page("ARC Return Code Statistics Report");
    }


}
