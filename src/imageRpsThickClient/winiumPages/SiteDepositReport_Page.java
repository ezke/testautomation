package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Checkbox;
import imageRpsThickClient.pageControls.TextBox;

public class SiteDepositReport_Page extends BatchReport_PageBase {

    public TextBox _depositDate = new TextBox("depositSumReport_DepositDate_TextBox", null, null);

    public Checkbox _failIfNoData_Checkbox = new Checkbox("depositSumReport_FailIfNoData_Checkbox", null, null);

    public SiteDepositReport_Page click_OK_ForFailure(){
        click_OK_OnError();
        return new SiteDepositReport_Page();
    }

    public SiteDepositReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new SiteDepositReport_Page();
    }

}
