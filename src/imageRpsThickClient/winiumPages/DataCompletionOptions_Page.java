package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import utils.BaseUI;

public class DataCompletionOptions_Page extends DCMBasePage {

    public final ComboBoxControl _workflowComboBox = new ComboBoxControl("dataCompletionOptions_WorkFlow", null, null);


    public DataCompletionOptions_Page(){
        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, 40);

    }

}
