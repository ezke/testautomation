package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.RPT_Report;
import imageRpsThickClient.pageControls.TextBox;
import org.apache.commons.lang3.Range;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.HashMap;

public class ExceptionItemHitReport_Printer_TempFile_Page extends DCMBasePage {

    public ExceptionItemHitReport_Printer_TempFile_Page(){}

    public final TextBox outputTextBox = new TextBox("excptItmHitRepPrintTempFile_Output_Textbox", null, null);

    public void validate_ReportDate_Accurate(String[] rptReport) throws Exception {
        String dateFormat = "MM/dd/yy";
        String expectedDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        String line1 = rptReport[0];
        String[] line1Split = line1.split("\\s");

        String actualDate = line1Split[1];

        BaseUI.verify_Date_IsAcceptable_DateRange(expectedDate, actualDate, -3, 3, dateFormat);
    }

    public void clickOK_DontViewReport() {
        BaseUI.click(Locator.lookupRequiredElement("excptItmHitRepPrintTempFile_OK_Button"));

        int maxCount = 0;
        while (true) {
            if(maxCount == 5){
                break;
            }
            BaseUI.waitForElementToBeDisplayed("excptItmHitRepPrintTempFile_QuestionModal", null, null);
            if (BaseUI.elementExists("excptItmHitRepPrintTempFile_OverwriteMessage", null, null)) {
                BaseUI.click(Locator.lookupRequiredElement("excptItmHitRepPrintTempFile_YesButton"));

            }else{
                BaseUI.click(Locator.lookupRequiredElement("excptItmHitRepPrintTempFile_NoButton"));
                break;
            }
            maxCount++;
        }
    }

    public TableData getRPT_Table(String[] rptExtract){
        RPT_Report rptReport = new RPT_Report(rptExtract);

        HashMap<String, Range<Integer>> mappings = new HashMap<>();
        mappings.put("Sort Account Number", Range.between(0, 20));
        mappings.put("BatchID", Range.between(21, 28));
        mappings.put("Batch#", Range.between(29, 35));
        mappings.put("P1Seq#", Range.between(36, 42));
        mappings.put("Reject Reason", Range.between(42, 82));
        mappings.put("EIP Data", Range.between(83, 103));
        mappings.put("Scanline", Range.between(104, 130));

        return rptReport.get_Report_Table(mappings.get("Sort Account Number"), RPT_Report.RPT_Matcher.NUMBER, mappings);

    }

}
