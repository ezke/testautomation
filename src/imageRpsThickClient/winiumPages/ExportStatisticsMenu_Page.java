package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class ExportStatisticsMenu_Page extends DCMBasePage {

    public ExportDailyTransportStatisticsFileReport_Page navigate_ExportDailyTransportStatisticsFile() throws Exception {
        BaseUI.send_KeyWithActionBuilder("3");
        return new ExportDailyTransportStatisticsFileReport_Page();
    }

    public ExportDailyTotalItemsFile_Page navigate_ExportDailyTotalItemsFile() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        return new ExportDailyTotalItemsFile_Page();
    }

    public ExportDailyOperatorStatisticsFileReport_Page navigate_ExportDailyOperatorStatisticsFile() throws Exception {
        BaseUI.send_KeyWithActionBuilder("2");
        return new ExportDailyOperatorStatisticsFileReport_Page();
    }
}
