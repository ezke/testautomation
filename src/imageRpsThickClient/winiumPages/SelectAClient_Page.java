package imageRpsThickClient.winiumPages;

import utils.winiumXRay.XRayUtils;
import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;

public class SelectAClient_Page extends DCMBasePage {

    public ClientSpecificConfigurationMenu_Page selectClient_AndNavigate_ClientSpecificConfigMenu(String lockboxName) throws Exception{
        String[] headers = {"Clnt ID", "Lockbox", "Client Name", "Loc", "OrgID"};
        TableData selectClientTable = XRayUtils.return_Table(headers);
        XRayUtils.navigate_ToLocation_InTable(selectClientTable, "Lockbox", lockboxName);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);

        return new ClientSpecificConfigurationMenu_Page();
    }

    public SelectAClient_Page(){

    }


}
