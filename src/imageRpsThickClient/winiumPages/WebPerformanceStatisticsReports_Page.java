package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class WebPerformanceStatisticsReports_Page extends DCMBasePage {

    public WEBDCM_Operator_Reports_Page navigate_Operator_DetailBySystemDate(){
        BaseUI.send_KeyWithActionBuilder("1");
        return new WEBDCM_Operator_Reports_Page("Web Operator Stats Detail");
    }

    public WEBDCM_Operator_Reports_Page navigate_Operator_SummaryBySystemDate(){
        BaseUI.send_KeyWithActionBuilder("2");
        return new WEBDCM_Operator_Reports_Page("Web Operator Stats Summary");
    }

}
