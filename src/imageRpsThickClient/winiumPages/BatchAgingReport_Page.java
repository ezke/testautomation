package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.HashMap;

public class BatchAgingReport_Page extends BatchReport_PageBase {

    public final ComboBoxControl _reportDateType_ComboBox = new ComboBoxControl("batchAgingRep_ReportDateType_ComboBox", null, null);

    public String get_DaysOld(){

        return BaseUI.getTextFromField(Locator.lookupRequiredElement("batchAgingRep_DaysOld_TextBox"));

    }

    public void validate_AllDates_BeforeCutoff(TableData csvTableToCheck, int cutoffCount){
        SoftAssert softAssert = new SoftAssert();
        //DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");

        String dateFormat = "MM/dd/yy";
        String originDate = BaseUI.getDateAsString_InRelationToTodaysDate(-cutoffCount, dateFormat);

        DateTime cutoffDate = DateTime.parse(originDate, DateTimeFormat.forPattern(dateFormat));

        for(HashMap<String, String> row : csvTableToCheck.data){
            String batchDate = row.get("Batch Date");

            DateTime actualDate = DateTime.parse(batchDate, DateTimeFormat.forPattern(dateFormat));


            softAssert.assertTrue(actualDate.isBefore(cutoffDate), "Date " + actualDate.toString() + " was not before " + cutoffDate.toString());

        }
        softAssert.assertAll();
    }


    public BatchAgingReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchAgingReport_Page();
    }
    


    public BatchAgingReport_Page(){

    }

}
