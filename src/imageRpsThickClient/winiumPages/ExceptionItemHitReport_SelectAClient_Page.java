package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class ExceptionItemHitReport_SelectAClient_Page {


    private void selectOptionInList_ByColumnAndMatch(String column, String optionText) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Clnt ID", "Lockbox", "Client Name"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, column, optionText);

    }

    public ExceptionItemHitReport_DateEntry_Page navigate_Report_DateEntry(int clientID) throws Exception {
        selectOptionInList_ByColumnAndMatch("Clnt ID", String.valueOf(clientID));
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ExceptionItemHitReport_DateEntry_Page();
    }


}
