package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class DevelopmentMenu_Page extends DCMBasePage {

    private enum menuOptions{
        CompilePrograms(1),
        RunImageRPS(2),
        Dictionary(3),
        Quit(4),
        CreateNoClientsTxtLockout(5),
        ProcedureEditor(6),
        BackupConfiguration(7),
        RunAnyProgram(8),
        UpdateClientConfiguration(9),
        InstallationUtilities(10),
        LogInAsDifferentUser(11),
        Upgrade(12);

        private int value;

        public int getValue() {
            return value;
        }

        private menuOptions(final int val) {
            value = val;
        }

        @Override
        public String toString(){
            return String.valueOf(value);
        }
    }

    public MainMenu_Page navigate_RunImageRPS() throws Exception{

        BaseUI.waitForElementToBeDisplayed("developmentMenu_RunImageRPS", null, null,25);
        BaseUI.click(Locator.lookupRequiredElement("developmentMenu_RunImageRPS"));
        return new MainMenu_Page();
    }


    public DevelopmentMenu_Page run_ProcedureEditor() throws Exception {
        
        BaseUI.click(Locator.lookupRequiredElement("developmentMenu_ProcedureEditor"));
        BaseUI.send_KeyWithActionBuilder(Keys.F3);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("fileName_TextBox"), "Recostat_Client410.p");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        BaseUI.send_KeyWithActionBuilder(Keys.F1);
        BaseUI.click(Locator.lookupRequiredElement("procedure_Close_Button"));
        return new DevelopmentMenu_Page();
    }

    public DevelopmentMenu_Page() throws Exception {

        BaseUI.waitForElementToBeDisplayed("developmentMenu_CompilePrograms", null, null, 90);
        Thread.sleep(500);
    }
}
