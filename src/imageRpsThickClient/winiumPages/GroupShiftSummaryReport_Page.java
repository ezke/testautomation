package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class GroupShiftSummaryReport_Page extends BatchReport_PageBase {
    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("grpShiftSumRep_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("grpShiftSumRep_ToDate"), toDate);
    }

    public void enter_Job(String job){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("grpShiftSumRep_Job_TextBox"), job);
    }

    public GroupShiftSummaryReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new GroupShiftSummaryReport_Page();
    }

    public void check_PrintDetail_Checkbox(){
        BaseUI.checkCheckbox("grpShiftSumRep_PrintDetail_Checkbox");
    }


}
