package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class BatchStatusReport_Page extends BatchReport_PageBase {



    public void enter_Text_IntoReceiveDate(String dateToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batStatusRep_ReceiveDate_TextBox"), dateToEnter);
    }

    public void enter_Text_IntoProcessDate(String dateToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batStatusRep_ProcessDate_TextBox"), dateToEnter);
    }




    public BatchStatusReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchStatusReport_Page();
    }



    public BatchStatusReport_Page(){

    }

}
