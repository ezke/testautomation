package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class SelectAStation_Page extends DCMBasePage {

    public Sys_StationSetup_Page navigate_Sys_StationSetup() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("stationSetup_OK_Button"));
        return new Sys_StationSetup_Page();
    }
}
