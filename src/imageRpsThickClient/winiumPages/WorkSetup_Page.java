package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class WorkSetup_Page extends DCMBasePage {

    public ClientSetup_Page navigate_ClientSetupMenu() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "4");
        return new ClientSetup_Page();
    }

    public WorkSetup_Page() {

    }
}
