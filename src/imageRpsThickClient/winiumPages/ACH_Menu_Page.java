package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ACH_Menu_Page extends DCMBasePage {

    public ACH_MaintenanceMenu_Page navigate_ACH_MaintenanceMenuPage() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "4");
        return new ACH_MaintenanceMenu_Page();
    }

    public ACH_Menu_Page(){

    }


}
