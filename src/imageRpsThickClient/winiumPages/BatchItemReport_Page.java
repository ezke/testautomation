package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import utils.BaseUI;
import utils.Locator;

public class BatchItemReport_Page extends BatchReport_PageBase {

    public final ComboBoxControl reportType = new ComboBoxControl("batItemRep_ReportType_ComboBox", null, null);

    public final ComboBoxControl reportDateType = new ComboBoxControl("batItemRep_ReportDateType_ComboBox", null, null);

    public void enter_SystemDate_To(String textToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batItemRep_SystemDates_ToTextBox"), textToEnter);
    }

    public void enter_SystemDate_From(String textToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("batItemRep_SystemDates_FromTextBox"), textToEnter);
    }



    public BatchItemReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchItemReport_Page();
    }


//    public String get_Selected_ReportDateType_Option() throws Exception {
//        ComboBoxControl comboBoxControl = new ComboBoxControl("batchAgingRep_ReportDateType_ComboBox", null, null);
//
//
//        return comboBoxControl.getSelectedComboBoxItem();
//    }



    public BatchItemReport_Page(){

    }

}
