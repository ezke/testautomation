package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class Sys_SystemSettings_Page extends DCMBasePage {

    public MainMenu_Page navigate_MainPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }


    public CFGValueSetUp_Page navigate_CFGValueSetUp() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "8");
        return new CFGValueSetUp_Page();
    }
}
