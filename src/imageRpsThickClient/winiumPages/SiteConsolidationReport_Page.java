package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Checkbox;
import imageRpsThickClient.pageControls.TextBox;

public class SiteConsolidationReport_Page extends BatchReport_PageBase {

    public TextBox _consolFromDate = new TextBox("siteConsolRep_ConsolidationFromDate_Textbox", null, null);
    public TextBox _consolThruDate = new TextBox("siteConsolRep_ConsolidationThruDate_Textbox", null, null);

    public Checkbox _batchID_Radio = new Checkbox("siteConsolRep_BatchID_Radio", null, null);
    public Checkbox _failIfNoData_Checkbox = new Checkbox("depositSumReport_FailIfNoData_Checkbox", null, null);

    public SiteConsolidationReport_Page click_OK_ForFailure(){
        click_OK_OnError();
        return new SiteConsolidationReport_Page();
    }

    public SiteConsolidationReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new SiteConsolidationReport_Page();
    }

}
