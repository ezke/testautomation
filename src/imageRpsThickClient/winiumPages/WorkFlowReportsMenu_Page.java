package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class WorkFlowReportsMenu_Page extends DCMBasePage {

    public Pass2BatchesReport_Page navigate_Pass2BatchesReport_SortedByClient(){
        BaseUI.send_KeyWithActionBuilder("1");
        return new Pass2BatchesReport_Page();
    }

    public Pass2BatchesReport_Page navigate_Pass2BatchesReport_SortedByStation(){
        BaseUI.send_KeyWithActionBuilder("2");
        return new Pass2BatchesReport_Page();
    }

    public Pass2BatchesReport_Page navigate_Pass2BatchesReport_SortedByRCVDate(){
        BaseUI.send_KeyWithActionBuilder("3");
        return new Pass2BatchesReport_Page();
    }

    public BatchesDelayedInProcessing_Page navigate_BatchesDelayedInprocessing(){
        BaseUI.send_KeyWithActionBuilder("4");
        return new BatchesDelayedInProcessing_Page();
    }


}
