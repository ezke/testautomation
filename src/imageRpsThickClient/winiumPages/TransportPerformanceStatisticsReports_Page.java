package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class TransportPerformanceStatisticsReports_Page extends DCMBasePage {

    public TransportDetailBySystemDate_Page navigate_TransportDetailBySystemDate(){
        BaseUI.send_KeyWithActionBuilder("1");
        return new TransportDetailBySystemDate_Page();
    }

    public TransportSummaryBySystemDate_Report_Page navigate_TransportSummaryBySystemDate(){
        BaseUI.send_KeyWithActionBuilder("2");
        return new TransportSummaryBySystemDate_Report_Page();
    }

}
