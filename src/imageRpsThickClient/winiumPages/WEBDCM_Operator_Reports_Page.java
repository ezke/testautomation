package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class WEBDCM_Operator_Reports_Page extends BatchReport_PageBase {

    private final String _title;

    public WebPerformanceStatisticsReports_Page navigate_BackTo_WebPerformanceStatisticsReports_Page() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new WebPerformanceStatisticsReports_Page();
    }

    public void enter_Dates(String fromDate, String throughDate) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("webdcmOpReports_FromActivityDate_TextBox"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("webdcmOpReports_ThroughActivityDate_TextBox"), throughDate);
    }

    public void enter_Job(String jobToEnter) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("webdcmOpReports_Job_Textbox"), jobToEnter);
    }

    public void enter_Jobs(String[] jobsToEnter) {
        String outputString = jobsToEnter[0];
        for(int i = 1; i < jobsToEnter.length; i++){
            if(i != jobsToEnter.length) {
                outputString += ",";
            }
            outputString += jobsToEnter[i];

        }

        enter_Job(outputString);
    }

    public void check_PrintDetail_Checkbox() {
        BaseUI.checkCheckbox("webdcmOpReports_PrintDetail_Checkbox");
    }

    public WEBDCM_Operator_Reports_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new WEBDCM_Operator_Reports_Page(_title);
    }

    //Time format: MM/dd/yy hh:mm a
    public void verify_Line2Date(String[] rptExtract, String dateAndTimeFrom, String dateAndTimeThrough) {
        String line2 = rptExtract[1];
        String expectedBatchDateValue = "Start: " + dateAndTimeFrom + " - End: " + dateAndTimeThrough;

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    public void verify_ReportType(String[] rptExtract) {
        String line2 = rptExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: OpDetail";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    public void verify_JobIDs_OnlyContainJobs_FromSelected(TableData csvTable, String[] enteredJobs){
        SoftAssert softAssert = new SoftAssert();

        List<String> jobList = Arrays.asList(enteredJobs);

        for(HashMap<String, String> row : csvTable.data){
            String currentJob = row.get("Job ID");

            softAssert.assertTrue(jobList.contains(currentJob), "Expected Jobs did not contain " + currentJob);
        }

        softAssert.assertAll();
    }


    public WEBDCM_Operator_Reports_Page(String title) {
        _title = title;
    }
}//End of Class
