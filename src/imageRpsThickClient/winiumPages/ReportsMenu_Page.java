package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;

public class ReportsMenu_Page extends DCMBasePage {

    public ReportWizards_Page navigate_ReportWizards() throws Exception {
        BaseUI.send_KeyWithActionBuilder("8");
        return new ReportWizards_Page();
    }

    public MainMenu_Page navigate_BackTo_MainMenuPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }

    public StatisticsMenu_Page navigate_StatisticsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        return new StatisticsMenu_Page();
    }

    public ClientReportsMenu_Page navigate_ClientReportsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder("3");
        return new ClientReportsMenu_Page();
    }


    public SiteReportsMenu_Page navigate_SiteReportsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder("4");
        return new SiteReportsMenu_Page();
    }

    public WorkFlowReportsMenu_Page navigate_WorkFlowReportsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder("2");
        return new WorkFlowReportsMenu_Page();
    }

    public ACH_StatisticsMenu_Page navigate_ACHStatisticsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder("5");
        return new ACH_StatisticsMenu_Page();
    }

    public ReportsMenu_Page(){

    }
}
