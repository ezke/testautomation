package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Checkbox;
import imageRpsThickClient.pageControls.Filter_ComboBoxControl;
import imageRpsThickClient.pageControls.TextBox;
import org.openqa.selenium.Keys;
import utils.BaseUI;

public class ACH_Reports_Page extends BatchReport_PageBase {


    private String _pageTitle;

    public final TextBox _fromProcessingDate_TextBox = new TextBox("achReport_FromProcessingDate_TextBox", null, null);

    public final TextBox _throughProcessingDate_TextBox = new TextBox("achReport_ThroughProcessingDate_TextBox", null, null);

    public final Checkbox _failIfNoData_Checkbox = new Checkbox("achReport_FailIfNoData_Checkbox", null, null);

    public final Filter_ComboBoxControl _clientFilter_Dropdown = new Filter_ComboBoxControl("achReport_ClientFilter_Dropdown", null, null);

    public ACH_Reports_Page(String title){
        _pageTitle = title;
    }


    public void verify_RPT_Line2DateRange(String[] rptExtract, String fromDate, String throughDate){
        String line2 = rptExtract[1];
        String expectedBatchDateValue = fromDate + " - " + throughDate;

        BaseUI.baseStringPartialCompare("Date Range", expectedBatchDateValue, line2);
    }

    public void verify_RPT_Title(String[] extractRPT){
        BaseUI.baseStringPartialCompare("Report Title", _pageTitle, extractRPT[0]);
    }

    public void verify_ReportType(String[] rptExtract){
        String line2 = rptExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: ACHSumA";

        if(_pageTitle.equals("ARC Reason Statistics Report")) {
            expectedReportName = "Report: ACHSum2A";
        }

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    public ACH_Reports_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ACH_Reports_Page(_pageTitle);
    }

    public ACH_StatisticsReportsMenu_Page navigate_BackTo_ACHStatisticsReportsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new ACH_StatisticsReportsMenu_Page();
    }

}
