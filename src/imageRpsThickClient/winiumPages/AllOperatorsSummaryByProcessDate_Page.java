package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class AllOperatorsSummaryByProcessDate_Page extends BatchReport_PageBase {
    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("allOperSumByProcDate_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("allOperSumByProcDate_ToDate"), toDate);
    }

    public void enter_Job(String job){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("allOperSumByProcDate_Job_TextBox"), job);
    }

    public AllOperatorsSummaryByProcessDate_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new AllOperatorsSummaryByProcessDate_Page();
    }

    public void check_ShowDailyDetail_Checkbox(){
        BaseUI.checkCheckbox("allOperSumByProcDate_ShowDailyDetail_Checkbox");
    }


}
