package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class JobDetailBySystemDate_Page extends BatchReport_PageBase {

    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("jobDetBySysDate_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("jobDetBySysDate_ToDate"), toDate);
    }

    public void enter_Job(String job){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("jobDetBySysDate_Job_TextBox"), job);
    }

    public JobDetailBySystemDate_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new JobDetailBySystemDate_Page();
    }
}
