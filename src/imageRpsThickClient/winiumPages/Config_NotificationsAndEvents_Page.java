package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class Config_NotificationsAndEvents_Page extends DCMBasePage {

    public EventTypeSetup_Page navigate_EventTypeSetup() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "4");
        return new EventTypeSetup_Page();
    }

    public ConfigurationMenu_Page navigate_BackTo_ConfigurationMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new ConfigurationMenu_Page();
    }

}
