package imageRpsThickClient.winiumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class UtilityMenu_Page extends DCMBasePage {

    public static UtilityMenu_Page navigate_UtilityMenu() throws Exception {

        Actions action = new Actions(Browser.driver);
        action.keyDown(Keys.CONTROL);
        action.keyDown(Keys.SHIFT);
        action.build().perform();

        action.sendKeys(Keys.F6).build().perform();

        action.keyUp(Keys.SHIFT);
        action.keyUp(Keys.CONTROL);
        action.build().perform();

        return new UtilityMenu_Page();
    }


    public GenericTableBrowser navigate_GenericTableBrowser() throws Exception{
        WebElement originalWindow = Locator.lookupRequiredElement("dcm_PageClassName");

        TableData reportTable = XRayUtils.return_Table(new String[]{"Description"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Description", "Table Viewer");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);

        BaseUI.click(originalWindow.findElement(By.name("Minimize")));

        return new GenericTableBrowser();
    }


    public DevelopmentMenu_Page navigate_BackTo_DevelopmentMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new DevelopmentMenu_Page();
    }


    private UtilityMenu_Page() throws Exception {
        waitForPageToLoad();
    }
}
