package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Checkbox;
import imageRpsThickClient.pageControls.RPT_Report;
import org.openqa.selenium.Keys;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Locator;
import org.apache.commons.lang3.Range;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.HashMap;

public class TransportStatsSummarizedReport_Page extends BatchReport_PageBase {

    public final Checkbox printDetailCheckbox = new Checkbox("tranStatSumRep_PrintDetail_Checkbox", null, null);


    public TransportStatsSummarizedReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new TransportStatsSummarizedReport_Page();
    }

    public void enter_Dates(String fromDate, String throughDate) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("tranStatSumRep_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("tranStatSumRep_ThroughDate"), throughDate);
    }



    public void verify_InBatch_PagesPerHour(String[] reportExtract){
        RPT_Report rpt_report = new RPT_Report(reportExtract);
        rpt_report.verify_Totals_FarColumn(Range.between(141, 152), RPT_Report.RPT_Matcher.TIME, Range.between(5, 10));
    }

    public void verify_Actual_PagesPerHour(String[] reportExtract) {
        RPT_Report rpt_report = new RPT_Report(reportExtract);
        rpt_report.verify_Totals_FarColumn(Range.between(165, 176), RPT_Report.RPT_Matcher.TIME, Range.between(5, 10));
    }

    public TableData getRPTReportTable(String[] rptExtract){
        TableData newTable = new TableData();

        String currentDateValue = "";
        for(String line : rptExtract){
            if(line.contains("Date: ")){
                currentDateValue = line.split("Date:", 2)[1].trim().split("\\s", 2)[0];
            }

            if (line.length() >= 163) {
                HashMap<String, String> newLine = new HashMap<>();

                String matcherLocation = line.substring(2, 12).trim();
                //Determine if the row is a data Row
                if (matcherLocation.matches("\\d{1,2}:\\d{2}:\\d{2}[a,p]?m")) {
                    newLine.put("Start Time", line.substring(2,12).trim());
                    newLine.put("End Time", line.substring(13,23).trim());
                    newLine.put("In-Batch Time", line.substring(131,140).trim());
                    newLine.put("Actual Time", line.substring(153,162).trim());
                    newLine.put("Batch ID", line.substring(28,37).trim());
                    newLine.put("Date", currentDateValue);

                    newTable.data.add(newLine);
                }
            }
        }

        return newTable;
    }

    public TableData getRPTReportTable_WithWallClock(String[] rptExtract){
        TableData newTable = new TableData();

        String currentDateValue = "";
        int lineIndex = 0;
        for(String line : rptExtract){
            if(line.contains("Date: ")){
                currentDateValue = line.split("Date:", 2)[1].trim().split("\\s", 2)[0];
            }

            if (line.length() >= 176) {
                HashMap<String, String> newLine = new HashMap<>();

                String matcherLocation = line.substring(118, 127).trim();
                //Determine if the row is a data Row
                if (matcherLocation.matches("\\d{2}:\\d{2}:\\d{2}") && !rptExtract[lineIndex - 1].contains("Totals for")) {
                    newLine.put("Check Count", line.substring(58,64).trim());
                    newLine.put("Stub Count", line.substring(68,74).trim());
                    newLine.put("Item Count", line.substring(78,84).trim());
                    newLine.put("Tran Count", line.substring(88,94).trim());
                    newLine.put("Avg Trans Size", line.substring(96,101).trim());
                    newLine.put("Batch Count", line.substring(105,110).trim());
                    newLine.put("Avg Batch Size", line.substring(111,117).trim());
                    newLine.put("In-Batch Time", line.substring(118,127).trim());
                    newLine.put("In-Batch pg/hr", line.substring(129,137).trim());
                    newLine.put("Actual Time", line.substring(138,147).trim());
                    newLine.put("Actual pg/hr", line.substring(149,158).trim());
                    newLine.put("Wall Clock Time", line.substring(159,168).trim());
                    newLine.put("Wall Clock pg/hr", line.substring(169,177).trim());
                    newLine.put("Date", currentDateValue);

                    newTable.data.add(newLine);
                }
            }
            lineIndex++;
        }

        return newTable;
    }


    public void verify_TimeDurationsAccurate(TableData rptTable) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        BaseUI.verify_true_AndLog(rptTable.data.size() > 0, "Found Table Data", "Did NOT find table data");

        for(HashMap<String, String> row : rptTable.data){
            DateFormat formatter = new SimpleDateFormat("h:mm:ssa");
            Time startTime = new Time(formatter.parse(row.get("Start Time")).getTime());
            Time endTime = new Time(formatter.parse(row.get("End Time")).getTime());
            long startTimeLong = startTime.getTime();
            long endTimeLong = endTime.getTime();
            long timeDifference = endTimeLong - startTimeLong;

            java.time.Duration expectedTimeDifference = getDuration(row.get("Actual Time"));
            softAssert.assertTrue(expectedTimeDifference.toMillis()== timeDifference, "Times did NOT match");
        }
        softAssert.assertAll();

    }

    public void verify_ElapsedTime_Stat7780Table(TableData reportCSVTable) throws Exception{
        UtilityMenu_Page utilityMenu = UtilityMenu_Page.navigate_UtilityMenu();
        GenericTableBrowser genericTableBrowser = null;
        TableData stat7780Table;
        try {
            genericTableBrowser = utilityMenu.navigate_GenericTableBrowser();
            String queryToPick = "stat_7780";
            genericTableBrowser.enter_TableQuery(queryToPick);
            stat7780Table = XRayUtils.return_Table(new String[]{"Elapsed Time", "batch_id"});
        }finally{
            genericTableBrowser.exitApplication();
            BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
            waitForPageToLoad();
        }

        BaseUI.verify_true_AndLog(reportCSVTable.data.size() > 0, "Found table data", "Did NOT find table data");

        // SoftAssert softAssert = new SoftAssert();
        for(HashMap<String, String> row : reportCSVTable.data){
            String elapsedTime = String.valueOf(getDuration(row.get("In-Batch Time")).getSeconds());
            HashMap<String, String> rowToFind = new HashMap<>();
            rowToFind.put("Elapsed Time", elapsedTime);
            rowToFind.put("batch_id", row.get("Batch ID"));

            stat7780Table.verify_MatchingRow_Found(rowToFind);
        }
    }

    public void verify_WallClock_PagesPerHour(TableData wallclockTable) throws Exception {

        SoftAssert softAssert = new SoftAssert();

        for(HashMap<String, String> row : wallclockTable.data){
            Duration wallClockDuration = getDuration(row.get("Wall Clock Time"));

            double itemCount = Double.parseDouble(row.get("Item Count").replace(",", ""));
            double time = wallClockDuration.getSeconds();
            double batches = Double.parseDouble(row.get("Batch Count").replace(",",""));

            double expectedWallClockDouble = ((itemCount/time) * 3600);

            double firstMath = itemCount/time;
            firstMath = firstMath * 3600;

            int expectedWallClock = (int)Math.round(expectedWallClockDouble);

            int actualWallClock = Integer.parseInt(row.get("Wall Clock pg/hr").replace(",", ""));

            softAssert.assertTrue(expectedWallClock == actualWallClock, "Expected " + String.valueOf(expectedWallClock)
                    + " But seeing " + String.valueOf(actualWallClock));
        }

        softAssert.assertAll();
    }


    //timeToConvert would be in format hh:mm:ss
    private java.time.Duration getDuration(String timeToConvert) throws Exception {
        String[] tokens = timeToConvert.split(":");
        int hours = Integer.parseInt(tokens[0]);
        int minutes = Integer.parseInt(tokens[1]);
        int seconds = Integer.parseInt(tokens[2]);
        long duration = 3600 * hours + 60 * minutes + seconds;
        return java.time.Duration.ofSeconds(duration);
    }


}
