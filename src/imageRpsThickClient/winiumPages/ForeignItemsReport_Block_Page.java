package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import utils.BaseUI;
import utils.Locator;

public class ForeignItemsReport_Block_Page extends BatchReport_PageBase {

    public ComboBoxControl _reportType = new ComboBoxControl("foreignItemsReportBatch_ReportType", null, null);

    public String get_StartTime(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("foreignItemsReportBatch_StartTime"));
    }

    public String get_EndTime(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("foreignItemsReportBatch_EndTime"));
    }

    public void enter_StartDate(String dateToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("foreignItemsReportBatch_StartDate"), dateToEnter);
    }

    public void enter_EndDate(String dateToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("foreignItemsReportBatch_EndDate"), dateToEnter);
    }

    public void enter_DocID(String valueToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("foreignItemsReportBatch_DocID"), valueToEnter);
    }



    public ForeignItemsReport_Block_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ForeignItemsReport_Block_Page();
    }
}
