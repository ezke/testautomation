package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class Pass2BatchesReport_Page extends BatchReport_PageBase {

    public WorkFlowReportsMenu_Page navigate_BackTo_WorkFlowReportsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new WorkFlowReportsMenu_Page();
    }

    public Pass2BatchesReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new Pass2BatchesReport_Page();
    }

    public void check_ListIndividualBatches_Checkbox(){
        BaseUI.checkCheckbox("pass2BatchRep_ListIndividualBatches_Checkbox");
    }

    public void enter_ReceiveDate(String receiveDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("pass2BatchRep_BatchReceiveDate_TextBox"),receiveDate);
    }

    public void enter_ProcessDate(String processDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("pass2BatchRep_BatchProcessDate_TextBox"),processDate);
    }

    public void enter_Job(String jobToEnter) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("pass2BatchRep_Job_TextBox"),jobToEnter);
    }

    public void check_BatchNumber_Radio(){
        BaseUI.checkCheckbox("pass2BatchRep_BatchNumber_Radio");
    }

    public void check_BatchID_Radio(){
        BaseUI.checkCheckbox("pass2BatchRep_BatchID_Radio");
    }

    public void verify_RPT_Receive_And_ProcessDate_All(String[] rptReport){
        String line2 = rptReport[1];
        BaseUI.baseStringPartialCompare("Receive and Process Date", "Receive Date = ALL    Process Date = ALL", line2);
    }

    public void verify_RPT_Title(String[] rptReport){
        String line1 = rptReport[0];
        BaseUI.baseStringPartialCompare("Title", "Batch Status Report", line1);
    }

    public void verify_RPT_ReportType(String[] rptReport){
        String line2 = rptReport[1].replaceAll("\\s+", " ");
        String expectedType = "Report: batstat";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

}
