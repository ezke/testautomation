package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class PrintSummaryReport_byReturnDate_Page extends DCMBasePage {


    public void verify_ErrorNotPresent() throws Exception {
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("printSumReportByReturnDate_ErrorDialog"));
    }


    public PrintSummaryReport_byReturnDate_Page(){
        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, Duration.ofSeconds(30));
    }
}
