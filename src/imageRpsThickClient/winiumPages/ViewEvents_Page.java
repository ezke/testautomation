package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class ViewEvents_Page extends DCMBasePage {

    public ViewEvents_Page(){

    }

    public void enterText_EventIDLowerRange(String textToEnter) throws Exception {
        BaseUI.click(Locator.lookupElement("eventData_EventIDLowerRange_TextBox"));
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("eventData_EventIDLowerRange_TextBox"), textToEnter);
        Thread.sleep(500);
    }

    public void enterText_EventIDHigherRange(String textToEnter) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("eventData_EventIDHigherRange_TextBox"), textToEnter);
        Thread.sleep(500);
    }

    private void click_OKButton(){
        BaseUI.click(Locator.lookupRequiredElement("eventData_Ok_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("eventData_Ok_Button", null, null, 40);
    }

    public EventsResult_Page navigate_EventsResult() throws Exception {
        click_OKButton();
       return new EventsResult_Page();
    }

    public void enterText_EventID(String textToEnter) throws Exception {
        BaseUI.click(Locator.lookupElement("eventData_EventID_TextBox"));
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("eventData_EventID_TextBox"), textToEnter);
        Thread.sleep(500);
    }


    public MainMenu_Page navigate_MainPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }
}
