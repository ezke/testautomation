package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class Config_SystemSetup_Page extends DCMBasePage {

    public Sys_StationSetup_Page navigate_StationSetup() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "1");
        return new Sys_StationSetup_Page();
    }

    public Sys_SystemSettings_Page navigate_SystemSettings() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "2");
        return new Sys_SystemSettings_Page();
    }
}
