package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.RPT_Report;
import imageRpsThickClient.pageControls.Reports_OutputAndRunOptions;
import org.apache.commons.lang3.Range;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.HashMap;

public class TransportDetailBySystemDate_Page extends BatchReport_PageBase {

    public final Reports_OutputAndRunOptions _outputAndRunOptions =
            new Reports_OutputAndRunOptions("Transport Statistics Detail Report");


    public TransportDetailBySystemDate_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new TransportDetailBySystemDate_Page();
    }


    public void check_PrintDetail_Checkbox(){
        BaseUI.checkCheckbox("tranStatDetRep_PrintDetail_Checkbox");
    }

    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("tranStatDetRep_FromProcessingDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("tranStatDetRep_ThroughProcessingDate"), toDate);
    }

    public TableData get_RPT_Table(String[] rptExtract) {
        TableData reportTable;
        RPT_Report report = new RPT_Report(rptExtract);


        HashMap<String, Range<Integer>> mappings = new HashMap<>();
        mappings.put("Check Count", Range.between(45, 54));
        mappings.put("Stub Count", Range.between(55, 64));
        mappings.put("Item Count", Range.between(65, 74));
        mappings.put("Jam Count", Range.between(77, 81));
        mappings.put("Jam Count%", Range.between(82, 85));
        mappings.put("Elapsed Time", Range.between(87, 95));
        mappings.put("Hopper Time", Range.between(97, 105));
        mappings.put("Hopper Time%", Range.between(106, 109));
        mappings.put("Hand Drop Time", Range.between(111, 119));
        mappings.put("Hand Drop Time%", Range.between(120, 123));
        mappings.put("Jam Time", Range.between(125, 133));
        mappings.put("Jam Time%", Range.between(134, 137));
        mappings.put("ID Doc Time", Range.between(139, 147));
        mappings.put("ID Doc Time%", Range.between(148, 151));
        mappings.put("Unused Time", Range.between(153, 161));
        mappings.put("Unused Time%", Range.between(162, 165));
        mappings.put("Burst Speed", Range.between(166, 171));
        mappings.put("Wall Clock", Range.between(172, 177));

        reportTable = report.get_Report_Table(Range.between(90, 95), RPT_Report.RPT_Matcher.TIME, mappings);

        return reportTable;
    }


}
