package imageRpsThickClient.winiumPages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import utils.*;
import utils.winiumXRay.XRayUtils;

import java.time.Duration;
import java.util.HashMap;

public class MaintainQueue_Page extends DCMBasePage {

    public MaintainQueue_Page(){

    }

    public void selectBatch_ByDescription(String batchToSelect) throws Exception {
        XRayUtils.invalidateXRay();
        TableData selectQueueTable = XRayUtils.return_Table(new String[]{"Description"});
        XRayUtils.navigate_ToLocation_InTable(selectQueueTable, "Description", batchToSelect);
        BaseUI.send_KeyWithActionBuilder(Keys.SPACE);
    }

    public void unlockAndRunBatch_ByDescription(String batchToUnlock) throws Exception {
        selectBatch_ByDescription(batchToUnlock);
        Actions builder = new Actions(Browser.driver);

        builder.keyDown(Keys.LEFT_ALT);
        builder.build().perform();
        builder.sendKeys("u").build().perform();
        builder.keyUp(Keys.LEFT_ALT).build().perform();

        click_DialogBox_YesButton();
        BaseUI.send_KeyWithActionBuilder(Keys.SPACE);

        builder.keyDown(Keys.LEFT_ALT);
        builder.build().perform();
        builder.sendKeys("r").build().perform();
        builder.keyUp(Keys.LEFT_ALT).build().perform();
        click_DialogBox_YesButton();

    }

    public void runSelectedBatch(String batchToRun) throws Exception {

        BaseUI.send_KeyWithActionBuilder(Keys.SPACE);
        Actions builder = new Actions(Browser.driver);

        builder.keyDown(Keys.LEFT_ALT);
        builder.build().perform();
        builder.sendKeys("r").build().perform();
        builder.keyUp(Keys.LEFT_ALT).build().perform();
        click_DialogBox_YesButton();
    }

    private void click_DialogBox_YesButton(){
        BaseUI.click(Locator.lookupElement("maintainQueue_DialogBox_YesButton"));
        BaseUI.waitForElementToNOTBeDisplayed("maintainQueue_DialogBox_YesButton", null, null, 20);
    }

    public QueueAndRecognition_MenuPage navigate_BackTo_QueueAndRecognitionMenuPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new QueueAndRecognition_MenuPage();
    }

    public QueueItem_Page navigate_QueueItem_ByBatchDescription(String batchToSelect) throws Exception {
        waitForPageToLoad();
        scroll_TopOfGrid();
        TableData selectQueueTable = XRayUtils.return_Table(new String[]{"Description"});
        XRayUtils.navigate_ToLocation_InTable(selectQueueTable, "Description", batchToSelect);
        BaseUI.send_KeyWithActionBuilder(Keys.SPACE);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new QueueItem_Page();
    }

    public void wait_For_ERRColumn_ToBeUpdated(String batchName, Duration timeToWaitInSeconds, String expectedText) throws Exception {
        wait_for_ConditionToMeet(() -> {
            XRayUtils.invalidateXRay();
            TableData selectQueueTable;
            try {
                selectQueueTable = XRayUtils.return_Table(new String[]{"Description", "Err"});
            }catch(Exception e) {
                return false;
            }
            String actualText = selectQueueTable.return_Row_BasedOn_1MatchingField("Description", batchName).get("Err");
                    return actualText.equals(expectedText);
                },
                timeToWaitInSeconds, Duration.ofMillis(10000));
        Thread.sleep(5000);
        }

    public HashMap<String, String> getNodes(XMLParser parser, String initialTag, String[] nodesWeWant){
        HashMap<String, String> valueMap = new HashMap<>();
        NodeList nList = parser.getElementsByTagName(initialTag);
        Node nNode = nList.item(0);
        for(String nodeTag : nodesWeWant){
            if(nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){

                Element eElement = (Element)nNode;
                valueMap.put(nodeTag, eElement.getElementsByTagName(nodeTag).item(0).getTextContent());
            }
        }

        return valueMap;
    }

}
