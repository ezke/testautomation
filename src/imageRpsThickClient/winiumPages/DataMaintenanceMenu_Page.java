package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class DataMaintenanceMenu_Page extends DCMBasePage {

    public BatchMenu_Page navigate_BatchMenu() throws Exception{
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "1");
        return new BatchMenu_Page();
    }

    public ACH_Menu_Page navigate_ACH_MenuPage() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "6");
        return new ACH_Menu_Page();
    }

    public DepositMaintenance_Page navigate_DepositMaintenancePage() throws Exception {
        BaseUI.send_KeyWithActionBuilder("3");
        return new DepositMaintenance_Page();
    }

    public NotificationsAndEvents_Page navigate_NotificationsAndEvents() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "8");
        return new NotificationsAndEvents_Page();
    }

    public MainMenu_Page navigate_BackTo_MainMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }

    public DataMaintenanceMenu_Page(){

    }

}
