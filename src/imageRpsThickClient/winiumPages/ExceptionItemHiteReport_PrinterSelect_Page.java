package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class ExceptionItemHiteReport_PrinterSelect_Page extends DCMBasePage  {

    private void selectOptionInList(String optionText) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Name"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Name", optionText);
    }

    public ExceptionItemHitReport_Printer_TempFile_Page selectTempFile() throws Exception {
        selectOptionInList("Temp File");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ExceptionItemHitReport_Printer_TempFile_Page();
    }


}
