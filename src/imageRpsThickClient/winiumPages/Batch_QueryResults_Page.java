package imageRpsThickClient.winiumPages;

import org.apache.pdfbox.util.XMLUtil;
import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class Batch_QueryResults_Page extends DCMBasePage {

    public TableData get_BatchAndDates() throws Exception {
        return XRayUtils.return_Table(new String[]{"BatchID", "RecDate", "ProcDate", "ConsolDt", "Date"});

    }

    public BatchMenu_Page navigate_BackTo_BatchMenuPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new BatchMenu_Page();
    }

    private void selectOptionInList_ByDescription(String dataSelect) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"BatchID"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "BatchID", dataSelect);
    }

    public SingleBatchOperations_Page navigate_SingleBatchOperations_BySelectingFirstEntry() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new SingleBatchOperations_Page();
    }

    public SingleBatchOperations_Page navigate_SingleBatchOperations_BySelectingBatchID(String BatchID) throws Exception {
        selectOptionInList_ByDescription(BatchID);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new SingleBatchOperations_Page();
    }
    public Batch_QueryResults_Page() throws Exception{
        waitForPageToLoad();
    }
}
