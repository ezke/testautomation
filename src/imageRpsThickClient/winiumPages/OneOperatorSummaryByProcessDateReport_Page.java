package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class OneOperatorSummaryByProcessDateReport_Page extends BatchReport_PageBase {

    public void enter_Dates(String fromDate, String throughDate) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("oneOpSumByProcDateRep_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("oneOpSumByProcDateRep_ThroughDate"), throughDate);
    }

    public void check_ShowDailyDetail_Checkbox() throws Exception {
        BaseUI.checkCheckbox("oneOpSumByProcDateRep_ShowDailyDetail_Checkbox");
    }


    public OneOperatorSummaryByProcessDateReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new OneOperatorSummaryByProcessDateReport_Page();
    }


}
