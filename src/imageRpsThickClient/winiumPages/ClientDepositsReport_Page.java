package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Checkbox;
import imageRpsThickClient.pageControls.TextBox;

public class ClientDepositsReport_Page extends BatchReport_PageBase {

    public final TextBox _depositDate_Textbox = new TextBox("depositsSummaryReport_DepositDate_Textbox", null, null);

    public final Checkbox _failIfNoData_Checkbox = new Checkbox("achReport_FailIfNoData_Checkbox", null, null);

    public ClientDepositsReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ClientDepositsReport_Page();
    }

}
