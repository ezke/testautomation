package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class CFGValueSetUp_Page extends DCMBasePage{

    public CFGValueSetUp_Page() throws Exception {
        waitForPageToLoad();
    }

    public MainMenu_Page navigate_MainPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new MainMenu_Page();
    }

    private void selectConfigurationApplicationInList_ByDescription(String id) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Application ID"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Application ID", id);
    }

    public CFGValueSetUpPage_ConfiguredApplication selectConfigurationApplicationFromList(String applicationID) throws Exception {
        selectConfigurationApplicationInList_ByDescription(applicationID);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new CFGValueSetUpPage_ConfiguredApplication();
    }

    public CFGValueSetUpPage_DefinedInstances selectDefinedInstancesFromList(String stationID) throws Exception {
        BaseUI.click(Locator.lookupElement("cfgValueSetup_DefinedInstancesHeader"));
        Thread.sleep(500);
        selectDefinedInstancesInList_ByDescription(stationID);
        return new CFGValueSetUpPage_DefinedInstances();
    }

    private void selectDefinedInstancesInList_ByDescription(String id) throws Exception {
        TableData reportTable = XRayUtils.return_Table(new String[]{"Instance ID"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Instance ID", id);
    }

    private void selectInstanceValuesInList_ByDescription(String tagName) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Tag Name"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Tag Name", tagName);
    }

    public CFGValueSetUpPage_InstanceValues selectInstanceValuesFromList(String tagName) throws Exception {
        BaseUI.click(Locator.lookupElement("cfgValueSetup_InstancesValuesHeader"));
        Thread.sleep(500);
        selectInstanceValuesInList_ByDescription(tagName);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new CFGValueSetUpPage_InstanceValues();
    }

    public void selectInstanceValueCheckbox() {
        BaseUI.click(Locator.lookupElement("cfgSetUpValue_CfgTagValue_InstanceValueCheckbox"));
    }

    public void clickOkButton() {
        BaseUI.click(Locator.lookupElement("cfgSetUpValue_CfgTagValue_OkButton"));
        BaseUI.waitForElementToNOTBeDisplayed("cfgSetUpValue_CfgTagValue_InstanceValueCheckbox", null, null, 30);
    }

    public void selectAllValues_FromCfgValueSetUpPage(String applicationID, String instanceID, String tagName) throws Exception {
        selectConfigurationApplicationFromList(applicationID);
        selectDefinedInstancesFromList(instanceID);
        selectInstanceValuesFromList(tagName);
        if(! BaseUI.checkBoxStatus("cfgSetUpValue_CfgTagValue_InstanceValueCheckbox")) {
            selectInstanceValueCheckbox();
        }
        clickOkButton();
    }

    public void selectAllValues_FromCfgValueSetUpPage_InstanceValueUnchecked(String applicationID, String instanceID, String tagName) throws Exception {
        selectConfigurationApplicationFromList(applicationID);
        selectDefinedInstancesFromList(instanceID);
        selectInstanceValuesFromList(tagName);
        if(BaseUI.checkBoxStatus("cfgSetUpValue_CfgTagValue_InstanceValueCheckbox")) {
            selectInstanceValueCheckbox();
        }
        clickOkButton();
    }
}
