package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.RPT_Report;
import org.apache.commons.lang3.Range;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.text.DecimalFormat;
import java.util.HashMap;

public class OperatorStatsDetailReport_Page extends BatchReport_PageBase {

    public void enter_Dates(String fromDate, String throughDate){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("operStatsDetail_FromActivityDate_TextBox"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("operStatsDetail_ThroughActivityDate_TextBox"), throughDate);
    }

    public OperatorStatsDetailReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new OperatorStatsDetailReport_Page();
    }

    public TableData get_RPT_Table(String[] rptExtract) {
        TableData reportTable;
        RPT_Report report = new RPT_Report(rptExtract);


        HashMap<String, Range<Integer>> mappings = new HashMap<>();
        mappings.put("Begin Date", Range.between(29, 38));
        mappings.put("Batch Time", Range.between(38, 49));
        mappings.put("Batch", Range.between(49,56));
        mappings.put("Clnt", Range.between(57, 61));
        mappings.put("Doc Grp", Range.between(62, 65));
        mappings.put("Items", Range.between(66, 73));
        mappings.put("Rejs", Range.between(75, 81));
        mappings.put("Rej%", Range.between(82, 88));
        mappings.put("Good Items", Range.between(89, 96));

        reportTable = report.get_Report_Table(mappings.get("Begin Date"), RPT_Report.RPT_Matcher.DATE, mappings);

        return reportTable;
    }

    public void verify_RPT_RejsPercent_Accurate(TableData rptTable){
        Boolean foundRej = false;

        SoftAssert softAssert = new SoftAssert();
        for(HashMap<String, String> row : rptTable.data){
            double rejs = Double.valueOf(row.get("Rejs"));
            if(rejs > 0){
                foundRej = true;

                double items = Double.valueOf(row.get("Items"));

                DecimalFormat df = new DecimalFormat("#.00");
                String expectedPercent = df.format((rejs/items) * 100);

                softAssert.assertEquals(row.get("Rej%"), expectedPercent);
            }

        }
        softAssert.assertAll();

        BaseUI.verify_true_AndLog(foundRej, "Found at least 1 Rej", "Did not find any Rej.");
    }

}
