package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class ACH_Consolidation_Menu_Page  extends DCMBasePage {

    public ACH_ConsolidationMenu_OtherReports_Page navigate_OtherReportsForConsolidation(){
        BaseUI.send_KeyWithActionBuilder("4");
        return new ACH_ConsolidationMenu_OtherReports_Page();
    }

    public ACH_Consolidation_Menu_Page(){

    }

}
