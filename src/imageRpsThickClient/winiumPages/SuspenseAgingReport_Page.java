package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Reports_OutputAndRunOptions;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;

import java.util.ArrayList;

public class SuspenseAgingReport_Page extends BatchAgingReport_Page {

    public Reports_OutputAndRunOptions _outputAndRunOptions = new Reports_OutputAndRunOptions("Suspense Aging Report");

    public SuspenseAgingReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new SuspenseAgingReport_Page();
    }




    public void verify_BatchOrigID_Sorted_Descending(String[] reportExtract){
        ArrayList<Integer> batchOrigID = get_OrigBatchID(reportExtract);

        verify_SortedDescending(batchOrigID, "Batch Orig ID");
    }

    public void verify_BatchOrigID_Sorted_Ascending(String[] reportExtract){
        ArrayList<Integer> batchOrigID = get_OrigBatchID(reportExtract);

        verify_SortedAscending(batchOrigID, "Batch Orig ID");
    }


    public ArrayList<Integer> get_OrigBatchID(String[] rptExtract){
        return get_Numbers_FromRPT_Extract(rptExtract, 94, 103);
    }




    public SuspenseAgingReport_Page() throws Exception {
        Thread.sleep(500);
    }
}
