package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class DocumentSpecificGroupConfigurationMenu_Page extends DCMBasePage {

    public DataCompletionOptions_Page navigate_DataCompletionOptions() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "4");

        return new DataCompletionOptions_Page();
    }

    public DocumentSpecificGroupConfigurationMenu_Page(){

    }

}
