package imageRpsThickClient.winiumPages;

import utils.BaseUI;

public class ClientReportsMenu_Page extends DCMBasePage {

    public ClientDepositsReport_Page navigate_ClientDepositsReport() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        return new ClientDepositsReport_Page();
    }

    public ExceptionItemHitReport_SelectAClient_Page navigate_ExceptionItemHitReport_SelectAClient() throws Exception {
        BaseUI.send_KeyWithActionBuilder("3");
        return new ExceptionItemHitReport_SelectAClient_Page();
    }
}
