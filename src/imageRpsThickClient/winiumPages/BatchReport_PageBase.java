package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.Checkbox;
import imageRpsThickClient.pageControls.ChooseAPrinter_Popup;
import imageRpsThickClient.pageControls.TextBox;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.TableData;
import utils.winiumXRay.XRayUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

import java.time.Duration;
import java.util.*;

public class BatchReport_PageBase extends DCMBasePage {

    public Checkbox _printToPrinter_Checkbox = new Checkbox("batchRep_Print_Checkbox", null, null);

    public TextBox _printerNumber_TextBox = new TextBox("batchRep_Print_PrinterNumber_Textbox", null, null);

    public TextBox _printCopies_TextBox = new TextBox("batchRep_Print_PrintCopies_Textbox", null, null);

    public ChooseAPrinter_Popup openPrinterPopup() throws Exception {

        _printerNumber_TextBox.click();
        BaseUI.send_KeyWithActionBuilder(Keys.F2);

        ChooseAPrinter_Popup printerPopup = new ChooseAPrinter_Popup();
        printerPopup.wait.displayed(Duration.ofSeconds(15));
        return printerPopup;
    }


    protected ArrayList<Integer> get_Numbers_FromRPT_Extract(String[] rptExtract, int startIndex, int endIndex) {

        ArrayList<Integer> availableNumbers = new ArrayList<>();

        for (String line : rptExtract) {
            if (line.length() >= endIndex) {
                String numberLocation = line.substring(startIndex, endIndex).trim();
                //Didn't use BaseUI method because it would have resulted in thousand's of lines of console output.
                if (numberLocation.matches("^-?\\d+$")) {
                    availableNumbers.add(Integer.valueOf(numberLocation));
                }
            }
        }

        return availableNumbers;
    }

    protected void verify_SortedAscending(ArrayList<Integer> valuesToCheck, String nameOfField) {

        BaseUI.verify_true_AndLog(valuesToCheck.size() > 0, "Found " + nameOfField + "'s", "Did not find " + nameOfField + "'s");

        SoftAssert softAssert = new SoftAssert();
        for (int i = 0; i < valuesToCheck.size() - 1; i++) {
            softAssert.assertTrue(valuesToCheck.get(i) <= valuesToCheck.get(i + 1),
                    String.valueOf(valuesToCheck.get(i)) + " was NOT less than or equal to " + String.valueOf(valuesToCheck.get(i + 1)));
        }
        softAssert.assertAll();
    }

    public void verify_SortedDescending(ArrayList<Integer> valuesToCheck, String nameOfField) {

        BaseUI.verify_true_AndLog(valuesToCheck.size() > 0, "Found " + nameOfField + "'s", "Did not find " + nameOfField + "'s");

        SoftAssert softAssert = new SoftAssert();
        for (int i = 0; i < valuesToCheck.size() - 1; i++) {
            softAssert.assertTrue(valuesToCheck.get(i) >= valuesToCheck.get(i + 1),
                    String.valueOf(valuesToCheck.get(i)) + " was NOT greater than or equal to " + String.valueOf(valuesToCheck.get(i + 1)));
        }
        softAssert.assertAll();
    }

    public void click_RunNow_DontViewReport() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("batchRep_RunNow_Button"));

        Actions action = new Actions(Browser.driver);

        int maxCount = 0;
        while (true) {
            if (maxCount > 4) {
                break;
            }

            WebElement modal = BaseUI.waitForElementToBeDisplayed("batchRep_OpenReportModal", null, null, Duration.ofMinutes(3));

            if(BaseUI.get_Attribute_FromField(modal, "Name").contains("Error")){
                BaseUI.log_AndFail("Saw Error while waiting for report.");
            }

            WebElement messageElement = modal.findElement(By.xpath(".//*[@ClassName='Static'][2]"));
            if (BaseUI.get_Attribute_FromField(messageElement, "Name").contains("exists")) {
                WebElement yesButton = modal.findElement(By.name("Yes"));
                BaseUI.click(yesButton);
            } else {
                break;
            }
            maxCount++;
        }

        action.keyDown(Keys.ALT).build().perform();
        BaseUI.send_KeyWithActionBuilder("n");
        action.keyUp(Keys.ALT).build().perform();

        BaseUI.waitForElementToBeDisplayed("dcm_PageClassName", null, null, 40);
    }

    public void runReport_WhenNoReportPossible() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("batchRep_RunNow_Button"));
        BaseUI.waitForElementToBeDisplayed("batchRep_ErrorModal", null, null, 60);
    }

    public void verify_NoReportFound() {
        WebElement reportFailedMessageElement = Locator.lookupRequiredElement("batchRep_ErrorModal_Message");
        BaseUI.verifyElementAppears(reportFailedMessageElement);
        // String elementText = BaseUI.get_Attribute_FromField(reportFailedMessageElement, "Name");
        BaseUI.verifyElementHasExpectedAttributeValue(reportFailedMessageElement, "Name", "Report Failed\rNo Data Found!");
    }

    protected void click_OK_OnError() {
        BaseUI.click(Locator.lookupRequiredElement("batchRep_ErrorModal_OK_Button"));
    }

    public void validate_ReportDate_Accurate(String[] reportExtract) throws Exception {
        String dateFormat = "MM/dd/yy HH:mm:ss";
        String expectedDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        String line1 = reportExtract[0];
        String[] line1Split = line1.split("\\s");

        String actualDate = line1Split[1] + " " + line1Split[2];

        BaseUI.verify_Date_IsAcceptable_DateRange(expectedDate, actualDate, -3, 3, dateFormat);
    }

    public void check_SaveCSV_Checkbox() {
        BaseUI.checkCheckbox("batchRep_SaveCSV_Checkbox");
        XRayUtils.invalidateXRay();
    }

    public void enter_CSV_Name(String textToEnter) {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("batchRep_CSV_Name_TextBox"), textToEnter);
    }

    public void enter_SaveToFile(String textToEnter) {
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("batchRep_SaveToFile_TextBox"), textToEnter);
    }

    public void verify_ColumnHeaders_EachValueUnique(String[] columnHeaders) {

        Set<String> uniqueColumnHeaders = new HashSet<String>(Arrays.asList(columnHeaders));

        BaseUI.verify_true_AndLog(columnHeaders.length > 0, "Found Column Headers", "Did NOT find Column Headers");

        SoftAssert softAssert = new SoftAssert();

        for (String uniqueHeader : uniqueColumnHeaders) {
            int occurrencesCount = countOfOccurrences(columnHeaders, uniqueHeader);
            softAssert.assertEquals(occurrencesCount, 1, "Column " + uniqueHeader + " had " + String.valueOf(occurrencesCount) + " matches.");
        }

        softAssert.assertAll();
    }

    //Column row will be the first line of the csv as a String
    public void verify_ColumnHeaders_AllHaveValue(String columnRow) {
        String[] columnHeaders = columnRow.split("\\,");
        BaseUI.verify_true_AndLog(columnHeaders.length > 0, "Found Column Headers", "Did NOT find Column Headers");

        SoftAssert softAssert = new SoftAssert();

        for(String columnHeader : columnHeaders){
            softAssert.assertTrue(!columnHeader.trim().equals(""), "Column Header WAS empty.");
        }

        softAssert.assertAll();
    }


    private int countOfOccurrences(String[] arrayToCheck, String valueToSearchFor) {
        int count = 0;
        for (String item : arrayToCheck) {
            if (item.equals(valueToSearchFor)) {
                count++;
            }
        }

        return count;
    }

    public void validate_DatesFallInRange(TableData csvToCheck, String fromDate, String toDate, String[] columnsToCheck, String format) throws Exception {
        BaseUI.verify_true_AndLog(csvToCheck.data.size() > 0, "Found CSV Data.", "Did NOT find CSV Data.");

        for (HashMap<String, String> row : csvToCheck.data) {
            for (String column : columnsToCheck) {
                String dateToCheck = row.get(column);
                BaseUI.verify_Date_IsBetween_DateRange(fromDate, toDate, dateToCheck, format);
            }
        }
    }

    public void validate_DatesFallInRange(TableData csvToCheck, String fromDate, String toDate, String[] columnsToCheck) throws Exception {
        validate_DatesFallInRange(csvToCheck, fromDate, toDate, columnsToCheck, "MM/dd/yy");
    }

    public ReportWizards_Page navigate_BackTo_ReportWizardsPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new ReportWizards_Page();
    }

    public BatchReport_PageBase() {
        BaseUI.waitForElementToBeDisplayed("batchRep_RunNow_Button", null, null, 30);
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
        }
    }
}
