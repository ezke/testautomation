package imageRpsThickClient.winiumPages;


import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;


public class ExceptionItemHitReport_DateEntry_Page extends DCMBasePage {

    public ExceptionItemHiteReport_PrinterSelect_Page enterDate(String dateToEnter) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement(
                "excptItmHitRepDateEntry_DateEntry_Textbox", null, null), dateToEnter);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ExceptionItemHiteReport_PrinterSelect_Page();
    }

}
