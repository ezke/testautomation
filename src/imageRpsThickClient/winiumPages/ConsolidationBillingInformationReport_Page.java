package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ConsolidationBillingInformationReport_Page extends BatchReport_PageBase {

    public void enter_ConsolidationDate_From(String dateToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("consolidationReports_ConsolidationDates_From"), dateToEnter);
    }

    public void enter_ConsolidationDate_To(String dateToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("consolidationReports_ConsolidationDates_To"), dateToEnter);
    }




    public ConsolidationBillingInformationReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ConsolidationBillingInformationReport_Page();
    }

}
