package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;
import utils.winiumXRay.XRayUtils;

import java.time.Duration;

public class SelectAnItemToDisplay_Page extends DCMBasePage{

    public DataMaintenanceMenu_Page navigate_ItemInformationMenu_Page() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        return new DataMaintenanceMenu_Page();
    }

    public ItemInformationMenu_Page navigate_ItemInformationMenu_Page2() throws Exception {
        BaseUI.send_KeyWithActionBuilder("1");
        XRayUtils.takeXray(Duration.ofSeconds(60));
        return new ItemInformationMenu_Page();
    }
}
