package imageRpsThickClient.winiumPages;

import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class ViewBatchesByBatchID_SelectABatchPage extends DCMBasePage {

    public ViewBatchesByBatchID_SelectABatchPage() throws Exception {
        waitForPageToLoad();
    }

    public TableData get_BatchIDAndDate() throws Exception {
        return XRayUtils.return_Table(new String[]{"BatchID", "Date"});
    }
}
