package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;

public class StatisticsMenu_Page extends DCMBasePage{

    public ReportsMenu_Page navigate_BackTo_ReportsMenu_Page() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new ReportsMenu_Page();
    }

    public TransportPerformanceStatisticsReports_Page navigate_TransportPerformanceStatisticsReports()throws Exception {
        BaseUI.send_KeyWithActionBuilder("2");
        return new TransportPerformanceStatisticsReports_Page();
    }

    public ExportStatisticsMenu_Page navigate_ExportStatisticsMenu(){
        BaseUI.send_KeyWithActionBuilder("5");
        return new ExportStatisticsMenu_Page();
    }

    public OperatorPerformanceStatisticsReports_Page navigate_OperatorPerformanceStatisticsReports(){
        BaseUI.send_KeyWithActionBuilder("1");
        return new OperatorPerformanceStatisticsReports_Page();
    }

    public WebPerformanceStatisticsReports_Page navigate_WebPerformanceStatisticsReports(){
        BaseUI.send_KeyWithActionBuilder("3");
        return new WebPerformanceStatisticsReports_Page();
    }

}
