package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class SelectADeposit_Page extends DCMBasePage {

    public DataMaintenanceMenu_Page navigate_BackTo_DataMaintenanceMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new DataMaintenanceMenu_Page();
    }

    public TableData get_SelectADepositTable() throws Exception {
        return XRayUtils.return_Table(new String[]{"Client", "Lockbox", "Dep Date", "Endpoint", "Num", "Time", "Items", "Pkts", "Total", "Type", "Bank ID"});
    }
}
