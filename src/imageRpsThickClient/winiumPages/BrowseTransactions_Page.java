package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

public class BrowseTransactions_Page extends DCMBasePage {

    private void selectOptionInList_ByDescription(String dataSelect) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Tran"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Tran", dataSelect);
    }

    public SelectATransactionToDisplay_Page navigate_SelectATransactionToDisplay_Page_BySelectingFirstEntry() throws Exception {
        selectOptionInList_ByDescription("1");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new SelectATransactionToDisplay_Page();
    }
}
