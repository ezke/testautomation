package imageRpsThickClient.winiumPages;

import org.joda.time.Duration;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.HashMap;

public class BatchDelayReport_Page extends BatchReport_PageBase {

    public BatchDelayReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new BatchDelayReport_Page();
    }

    public String getAge() {
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("batchDelRep_BatchAge"));
    }

    public void validateTime_GreaterThanGivenDuration(TableData csvTable, String columnName, String expectedTime) {

        long expectedSeconds = getTotalSeconds(expectedTime);
        Duration expectedDuration = Duration.standardSeconds(expectedSeconds);

        SoftAssert softAssert = new SoftAssert();
        for (HashMap<String, String> row : csvTable.data) {
            long totalSeconds = getTotalSeconds(row.get(columnName));

            Duration fieldDuration = Duration.standardSeconds(totalSeconds);

            softAssert.assertTrue(fieldDuration.isLongerThan(expectedDuration) || fieldDuration.equals(expectedDuration),
                    "Field Duration of " + String.valueOf(totalSeconds) + " was NOT longer than expected Duration of" + String.valueOf(expectedSeconds));
        }

        softAssert.assertAll();
    }

    private long getTotalSeconds(String timeField) {
        long totalSeconds = 0;
        String[] timeFields = timeField.split("\\,");

        for (String time : timeFields) {
            time = time.replace("\"","");
            if (time.contains("yrs")) {
                totalSeconds += Long.valueOf(time.replace("yrs", "").trim()) * 31536000;
            } else if (time.contains("yr")) {
                totalSeconds += Long.valueOf(time.replace("yr", "").trim()) * 31536000;
            } else if (time.contains("mons")) {
                totalSeconds += Long.valueOf(time.replace("mons", "").trim()) * 2629746;
            } else if (time.contains("mon")) {
                totalSeconds += Long.valueOf(time.replace("mon", "").trim()) * 2629746;
            } else if (time.contains("days")) {
                totalSeconds += Long.valueOf(time.replace("days", "").trim()) * 86400;
            } else if (time.contains("day")) {
                totalSeconds += Long.valueOf(time.replace("day", "").trim()) * 86400;
            } else if (time.contains("hrs")) {
                totalSeconds += Long.valueOf(time.replace("hrs", "").trim()) * 3600;
            } else if (time.contains("hr")) {
                totalSeconds += Long.valueOf(time.replace("hr", "").trim()) * 3600;
            } else if (time.contains("min")) {
                totalSeconds += Long.valueOf(time.replace("min", "").trim()) * 60;
            } else if (time.contains("sec")) {
                totalSeconds += Long.valueOf(time.replace("sec", "").trim());
            }
        }

        return totalSeconds;
    }

    public BatchDelayReport_Page() {

    }
}
