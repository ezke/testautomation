package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ShiftSummaryReportForAllOperators_Page extends BatchReport_PageBase {
    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("shftSumRepAllOpers_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("shftSumRepAllOpers_ToDate"), toDate);
    }

    public void enter_Job(String job){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("shftSumRepAllOpers_Job_TextBox"), job);
    }

    public ShiftSummaryReportForAllOperators_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ShiftSummaryReportForAllOperators_Page();
    }

    public void check_PrintDetails_Checkbox(){
        BaseUI.checkCheckbox("shftSumRepAllOpers_ShowDailyDetail_Checkbox");
    }


}
