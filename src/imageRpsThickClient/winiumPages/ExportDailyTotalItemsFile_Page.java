package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ExportDailyTotalItemsFile_Page extends ExportStatistics_ReportBasePage {

    public ExportDailyTotalItemsFile_Page() throws Exception {
        waitForPageToLoad();
    }

    @Override
    public void enterDate(String date) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("expDailItemsFile_Date_Textbox"), date);
    }

}
