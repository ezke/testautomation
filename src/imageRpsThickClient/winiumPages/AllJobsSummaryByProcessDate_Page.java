package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.AllJobSummary_Job_RPT_Extract;
import imageRpsThickClient.pageControls.RPT_Report;
import imageRpsThickClient.pageControls.Reports_OutputAndRunOptions;
import org.apache.commons.lang3.Range;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.util.HashMap;

public class AllJobsSummaryByProcessDate_Page extends BatchReport_PageBase {

    public Reports_OutputAndRunOptions _outputAndRunOptions = new Reports_OutputAndRunOptions("Statistics Summary Report for All Jobs");

    public void enter_Dates(String fromDate, String toDate) throws Exception {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("allJobSumByProcDate_FromDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("allJobSumByProcDate_ToDate"), toDate);
    }

    public void enter_Job(String job){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("allJobSumByProcDate_Job_TextBox"), job);
    }

    public AllJobsSummaryByProcessDate_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new AllJobsSummaryByProcessDate_Page();
    }

    public void check_ShowDailyDetail_Checkbox(){
        BaseUI.checkCheckbox("allJobSumByProcDate_ShowDailyDetail_Checkbox");
    }


    public TableData get_RPT_Table(String[] rptExtract) throws Exception {
        AllJobSummary_Job_RPT_Extract rptTable = new AllJobSummary_Job_RPT_Extract(rptExtract);
        return rptTable.getRPT_ReportTable();
    }


    public void verify_ColumnName(String[] rptExtract, Range<Integer> columnRange, String expectedColumnName){
        String line4 = rptExtract[3];
        String line5 = rptExtract[4];

        String actualColumnName = line4.substring(columnRange.getMinimum(), columnRange.getMaximum()).trim();
        actualColumnName = actualColumnName.endsWith("/") ? actualColumnName +
                line5.substring(columnRange.getMinimum(), columnRange.getMaximum()).trim() : actualColumnName + " " +
                line5.substring(columnRange.getMinimum(), columnRange.getMaximum()).trim();

        BaseUI.baseStringCompare("Column Name", expectedColumnName, actualColumnName);
    }


}
