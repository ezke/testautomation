package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class QueueStatusConsol_Page extends DCMBasePage {

    public QueueStatusConsol_Page(){

    }

    public void check_NoQueues() throws Exception {
        BaseUI.checkCheckbox(Locator.lookupRequiredElement("queueStatus_NoQueues_CheckBox"));
        Thread.sleep(100);
    }

    public void unCheck_NoQueues() throws Exception {
        BaseUI.uncheckCheckbox(Locator.lookupRequiredElement("queueStatus_NoQueues_CheckBox"));
        Thread.sleep(100);
    }

    public QueueAndRecognition_MenuPage navigate_BackTo_QueueAndRecognitionMenuPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new QueueAndRecognition_MenuPage();
    }

}
