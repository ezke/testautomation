package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class SingleBatchOperations_Page extends DCMBasePage {

    public GeneralBatchInformation_Page navigate_GeneralBatchInformation() throws Exception {

        BaseUI.send_KeyWithActionBuilder("1");

        return new GeneralBatchInformation_Page();
    }

    public Batch_QueryResults_Page navigate_BackTo_BatchQueryResults() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);

        return new Batch_QueryResults_Page();
    }


    public SingleBatchOperations_Page() throws Exception {
        waitForPageToLoad();
    }

    public BrowseTransactions_Page navigate_BrowseTransactions() throws Exception {

        BaseUI.send_KeyWithActionBuilder("4");

        return new BrowseTransactions_Page();
    }

}
