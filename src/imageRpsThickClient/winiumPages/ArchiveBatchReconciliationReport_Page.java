package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import imageRpsThickClient.pageControls.Filter_ComboBoxControl;
import imageRpsThickClient.pageControls.Multiline_TextBox;
import imageRpsThickClient.pageControls.RPT_Report;
import org.apache.commons.lang3.Range;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.TableData;

import java.text.DecimalFormat;
import java.util.HashMap;

public class ArchiveBatchReconciliationReport_Page extends BatchReport_PageBase {

    private Range<Integer> _rptCheckTotalAmount_Range = Range.between(130, 147);

    public final ComboBoxControl _archiveSystem_ComboBox = new ComboBoxControl("archBatRecReport_ArchiveSystemDropdown", null, null);

    public final Filter_ComboBoxControl _stationFilter_ComboBox = new Filter_ComboBoxControl("archBatRecReport_StationFilter_Dropdown", null, null);

    public final Multiline_TextBox _stationFilter_TextBox = new Multiline_TextBox("archBatRecReport_StationFilter_TextBox", null, null);

    public void verify_RPT_ReportTitle(String[] rptExtract, String archiveSystem){
        String line1 = rptExtract[0];
        String expectedBatchDateValue = archiveSystem + " Batch Reconciliation Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedBatchDateValue, line1);
    }

    public void verify_RPT_ReportType(String[] rptExtract, String archiveSystem){
        String line2 = rptExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: " + archiveSystem + "reconc";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    //Use when we only have one set of grand totals.  Would need more complex solution if there were multiple.
    public void verify_GrandTotals_TotalCheckAmount(String[] rptExtract, TableData rptData) {
        DecimalFormat decimalFormat = new DecimalFormat("##.00");
        double expectedTotal = 0.0;

        for(HashMap<String, String> row : rptData.data){
            double lineTotal = Double.parseDouble(row.get("CheckTotalAmount").replace(",", ""));
            expectedTotal+= lineTotal;
        }

        String expectedTotalString = decimalFormat.format(expectedTotal);


        BaseUI.verify_true_AndLog(expectedTotal > 0, "Grand Total was greater than 0", "Grand Total was NOT greater than 0");

        double actualGrandTotal = getGrandTotals_GrandTotal(rptExtract);
        String actualGrandTotalString = decimalFormat.format(actualGrandTotal);

        BaseUI.verify_true_AndLog(expectedTotalString.equals(actualGrandTotalString),
                "Grand Totals matched value of " + expectedTotalString,
                "Grand Totals did NOT match, expected " + expectedTotalString + ", but seeing " + String.valueOf(actualGrandTotal));
    }

    //Use when we only have one set of grand totals.  Would need more complex solution if there were multiple.
    public void verify_GrandTotals_TotalEntries(String[] rptExtract, TableData rptData) {

        int expectedTotal = 0;

        for(HashMap<String, String> row : rptData.data){
            expectedTotal++;
        }

        BaseUI.verify_true_AndLog(expectedTotal > 0, "Grand Total was greater than 0", "Grand Total was NOT greater than 0");

        int actualGrandTotal = getGrandTotals_TotalEntries(rptExtract);

        BaseUI.verify_true_AndLog(expectedTotal == actualGrandTotal,
                "Grand Totals matched value of " + String.valueOf(expectedTotal),
                "Grand Totals did NOT match, expected " + String.valueOf(expectedTotal) + ", but seeing " + String.valueOf(actualGrandTotal));
    }



    private String getGrandTotalLine(String[] rptExtract){
        String grandTotalsLine = "";
        for(String line: rptExtract){
            if(line.trim().startsWith("Grand Totals")){
                grandTotalsLine = line;
                break;
            }
        }
        if(grandTotalsLine.equals("")){
            BaseUI.log_AndFail("Unable to find Grand Total Line.");
        }

        return grandTotalsLine;
    }

    private Integer getGrandTotals_TotalEntries(String[] rptExtract){
        String grandTotalsLine = getGrandTotalLine(rptExtract);
        int grandTotal = Integer.parseInt(
                grandTotalsLine.substring(57, 66).replace(",", "").trim());

        return grandTotal;
    }

    private Double getGrandTotals_GrandTotal(String[] rptExtract){
        String grandTotalsLine = getGrandTotalLine(rptExtract);
        double grandTotal = Double.parseDouble(
                grandTotalsLine.substring(_rptCheckTotalAmount_Range.getMinimum(), _rptCheckTotalAmount_Range.getMaximum()).replace(",", "").trim());

        return grandTotal;
    }



    public TableData get_RPT_ReportTable(String[] rptExtract){
        TableData reportTable;
        RPT_Report report = new RPT_Report(rptExtract);

        HashMap<String, Range<Integer>> mappings = new HashMap<>();
        mappings.put("CheckTotalAmount", _rptCheckTotalAmount_Range);
        mappings.put("BatchID", Range.between(57, 66));

        reportTable = report.get_Report_Table(Range.between(67,76), RPT_Report.RPT_Matcher.DATE, mappings);

        return reportTable;
    }


    public void verify_RPT_AllStations_HaveName(String[] rptExtract, String nameToHave){

        SoftAssert softAssert = new SoftAssert();
        boolean foundMatchingRow = false;
        for(String line : rptExtract){
            if(line.trim().startsWith("Totals for Capture Stn ID:")){
                foundMatchingRow = true;
                softAssert.assertTrue(line.contains(nameToHave), "Line did NOT contain " + nameToHave);
            }
        }

        BaseUI.verify_true_AndLog(foundMatchingRow, "Found a row that had Totals for Capture Stn ID:", "Did NOT find a Totals row.");
        softAssert.assertAll();

    }



    //archBatRecReport_StationFilter_TextBox

    public ArchiveBatchReconciliationReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new ArchiveBatchReconciliationReport_Page();
    }


}
