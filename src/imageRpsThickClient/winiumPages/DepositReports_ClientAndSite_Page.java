package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class DepositReports_ClientAndSite_Page extends BatchReport_PageBase {



    public void enter_DepositDate(String date){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("depositReport_DepositDate_TextBox"), date);
    }


    public DepositReports_ClientAndSite_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new DepositReports_ClientAndSite_Page();
    }



}
