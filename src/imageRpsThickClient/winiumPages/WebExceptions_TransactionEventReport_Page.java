package imageRpsThickClient.winiumPages;

import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.pageControls.RPT_Report;
import imageRpsThickClient.pageControls.Reports_OutputAndRunOptions;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;
import org.apache.commons.lang3.Range;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class WebExceptions_TransactionEventReport_Page extends BatchReport_PageBase {

    public Reports_OutputAndRunOptions _outputAndRunOptions = new Reports_OutputAndRunOptions("Web Exceptions - Transaction Event Report");

    public void verify_ReportDateType_Valid() {
        WebElement reportDateTypeElement = Locator.lookupRequiredElement("webExcTransReport_ReportDateType");
        String reportDateType = BaseUI.getTextFromField(reportDateTypeElement);
        BaseUI.verifyElementAppears(reportDateTypeElement);
        BaseUI.baseStringCompare("Report Date Type", "Event", reportDateType.trim());
    }

    public void verify_ClientID_Sorted_Ascending(String[] rptExtract) {
        ArrayList<Integer> clientIDs = get_ClientIDs(rptExtract);

        verify_SortedAscending(clientIDs, "Client ID");
    }

    public TableData get_RPT_Table(String[] rptExtract) {
        TableData reportTable;
        RPT_Report report = new RPT_Report(rptExtract);


        HashMap<String, Range<Integer>> mappings = new HashMap<>();
        mappings.put("ClientAndLockbox", Range.between(50, 62));
        mappings.put("Batch ID", Range.between(67, 74));
        mappings.put("Trans ID", Range.between(81,85));
        mappings.put("TimeStamp", Range.between(87, 105));
        mappings.put("Operator ID", Range.between(116, 125));
        mappings.put("Old State", Range.between(133, 145));
        mappings.put("New State", Range.between(150, 160));

        reportTable = report.get_Report_Table(mappings.get("ClientAndLockbox"), RPT_Report.RPT_Matcher.CLIENT_LOCKBOX, mappings);

        report.split_Column_ByDelimiter(reportTable, "ClientAndLockbox", "Client ID", "Lockbox ID",
               "\\-");


        return reportTable;
    }


    public String[] getPDF_Extract(String fileLocation) throws Exception {
        FileInputStream fs = new FileInputStream(fileLocation);
        String text = "";
        PDFParser parser = new PDFParser(fs);
        parser.parse();
        COSDocument cosDoc = parser.getDocument();
        PDFTextStripper pdfStripper = new PDFTextStripper();
        PDDocument pdDoc = new PDDocument(cosDoc);
        text = pdfStripper.getText(pdDoc);
        String[] docxLines = text.split(System.lineSeparator());

        return docxLines;
    }

    public TableData getPDF_Data(String[] extract) throws Exception {

        return get_RPT_Table(extract);
    }

    public void verify_ClientID_Sorted_Descending(String[] rptExtract) {
        ArrayList<Integer> clientIDs = get_ClientIDs(rptExtract);

        verify_SortedDescending(clientIDs, "Client ID");
    }

    private ArrayList<Integer> get_ClientIDs(String[] rptExtract) {
        int startIndex = 50;
        int endIndex = 62;

        ArrayList<Integer> availableNumbers = new ArrayList<>();

        for (String line : rptExtract) {
            if (line.length() >= endIndex) {
                String numberLocation = line.substring(startIndex, endIndex).trim();
                //Didn't use BaseUI method because it would have resulted in thousand's of lines of console output.
                if (numberLocation.matches("^\\d+-\\d+$")) {
                    availableNumbers.add(Integer.valueOf(numberLocation.split("\\-")[0]));
                }
            }
        }

        return availableNumbers;
    }

    public void enter_EventDates(String fromDate, String toDate) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("webExcTransReport_EventDate_From"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("webExcTransReport_EventDate_To"), toDate);
    }

    public WebExceptions_TransactionEventReport_Page click_OK_ForError() {
        click_OK_OnError();
        return new WebExceptions_TransactionEventReport_Page();
    }

    public WebExceptions_TransactionEventReport_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new WebExceptions_TransactionEventReport_Page();
    }
}
