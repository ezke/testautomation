package imageRpsThickClient.winiumPages;

import utils.winiumXRay.XRayUtils;
import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.TableData;

public class ReportWizards_Page extends DCMBasePage {

    private void selectOptionInList_ByDescription(String description) throws Exception{
        TableData reportTable = XRayUtils.return_Table(new String[]{"Description"});
        XRayUtils.navigate_ToLocation_InTable(reportTable, "Description", description);
    }

    public BatchTrackerActionsReport_Page navigate_BatchTrackerActionsReport() throws Exception {
        selectOptionInList_ByDescription("Batch Tracker Actions Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchTrackerActionsReport_Page();
    }

    public TransportStatsSummarizedReport_Page navigate_TransportStatsSummarizedReport() throws Exception {
        selectOptionInList_ByDescription("Transport Stats - Summarized Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new TransportStatsSummarizedReport_Page();
    }

    public BatchAgingReport_Page navigate_BatchAgingReport() throws Exception {
        selectOptionInList_ByDescription("Batch Aging Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchAgingReport_Page();
    }


    public ClientID_DocGrpChangeReport navigate_ClientID_DocGrpChangeReport() throws Exception {
        selectOptionInList_ByDescription("Client ID - Doc Grp Change Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ClientID_DocGrpChangeReport();
    }

    public BatchDelayReport_Page navigate_BatchDelayReport() throws Exception {
        selectOptionInList_ByDescription("Batch Delay Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchDelayReport_Page();
    }


    public BatchTimelineReport_Page navigate_BatchTimelineReport() throws Exception {
        selectOptionInList_ByDescription("Batch Timeline Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchTimelineReport_Page();
    }

    public ConsolidationBatchDetailReport_Page navigate_ConsolidationBatchDetailReport() throws Exception {
        selectOptionInList_ByDescription("Consolidation(s) Batch Detail");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ConsolidationBatchDetailReport_Page();
    }

    public DepositReports_ClientAndSite_Page navigate_DepositReports_ClientAndSite() throws Exception {
        selectOptionInList_ByDescription("Deposit Reports (Client and Site)");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new DepositReports_ClientAndSite_Page();
    }


    public ConsolidationBillingInformationReport_Page navigate_ConsolidationBillingInformationReport() throws Exception {
        selectOptionInList_ByDescription("Consolidation Billing Information");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ConsolidationBillingInformationReport_Page();
    }

    public ForeignItemsReport_Batch_Page navigate_ForeignItemsReport_BatchReport() throws Exception {
        selectOptionInList_ByDescription("Foreign Items Report (Batch)");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ForeignItemsReport_Batch_Page();
    }

    public ForeignItemsReport_Block_Page navigate_ForeignItemsReport_BlockReport() throws Exception {
        selectOptionInList_ByDescription("Foreign Items Report (Block)");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ForeignItemsReport_Block_Page();
    }

    public BatchStatusReport_Page navigate_BatchStatusReports() throws Exception {
        selectOptionInList_ByDescription("Batch Status Reports (Client and Site)");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchStatusReport_Page();
    }


    public BatchItemReport_Page navigate_BatchItemReports() throws Exception {
        selectOptionInList_ByDescription("Batch Item Reports (Client and Site)");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchItemReport_Page();
    }


    public IDM_Missing_ACK_Report_Page navigate_IDM_Missing_ACK_Report() throws Exception{
        selectOptionInList_ByDescription("IDM Missing ACK Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new IDM_Missing_ACK_Report_Page();
    }

    public BatchCapture_ReconciliationReport_Page navigate_BatchCaptureReconciliationReport() throws Exception{
        selectOptionInList_ByDescription("Batch Capture Reconciliation");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new BatchCapture_ReconciliationReport_Page();
    }

    public ArchiveBatchReconciliationReport_Page navigate_ArchiveBatchReconciliationReport() throws Exception{
        selectOptionInList_ByDescription("Archive Batch Reconciliation Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new ArchiveBatchReconciliationReport_Page();
    }

    public ReportsMenu_Page navigate_BackTo_ReportsMenuPage() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new ReportsMenu_Page();
    }

    public RecognitionStatsReport_Page navigate_RecognitionStatsReport() throws Exception {
        selectOptionInList_ByDescription("Recognition Stats Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new RecognitionStatsReport_Page();
    }

    public OperatorStatsDetailReport_Page navigate_OperatorStatsDetailReport() throws Exception {
        selectOptionInList_ByDescription("Operator Stats Detail Reports");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new OperatorStatsDetailReport_Page();
    }

    public SuspenseAgingReport_Page navigate_SuspenseAgingReport_Page() throws Exception {
        selectOptionInList_ByDescription("Suspense Aging Report");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new SuspenseAgingReport_Page();
    }

    public WebExceptions_TransactionEventReport_Page navigate_WebExceptions_TransactionEventReport() throws Exception {
        selectOptionInList_ByDescription("Web Exceptions - Transaction Event Repor");
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
        return new WebExceptions_TransactionEventReport_Page();
    }

    public ReportWizards_Page() throws Exception{
        waitForPageToLoad();

    }
}
