package imageRpsThickClient.winiumPages;

import imageRpsThickClient.pageControls.ComboBoxControl;
import imageRpsThickClient.pageControls.Reports_OutputAndRunOptions;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class OperatorDetailBySystemDate_Page extends BatchReport_PageBase {

    public ComboBoxControl _operatorFilter_Dropdown = new ComboBoxControl("opDetSysDatRep_OperatorFilter_Dropdown", null, null);

    public Reports_OutputAndRunOptions _outputAndRunOptions = new Reports_OutputAndRunOptions("Statistics Detail Report for an Operator");

    public void enter_OperatorFilter_TextBox(String textToEnter) throws Exception {
        WebElement textBox = Locator.lookupRequiredElement("opDetSysDatRep_OperatorFilter_TextBox");
        Actions select = new Actions(Browser.driver);
        select.doubleClick(textBox).build().perform();
        BaseUI.send_KeyWithActionBuilder(Keys.DELETE);
        BaseUI.enterText_IntoInputBox(textBox, textToEnter);
    }

    public void enter_DateRange(String fromDate, String toDate){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("opDetSysDatRep_FromActivityDate"), fromDate);
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("opDetSysDatRep_ToActivityDate"), toDate);
    }

    public OperatorDetailBySystemDate_Page click_OK_ForError(){
        click_OK_OnError();
        return new OperatorDetailBySystemDate_Page();
    }

    public OperatorDetailBySystemDate_Page runReport() throws Exception {
        click_RunNow_DontViewReport();
        return new OperatorDetailBySystemDate_Page();
    }

}
