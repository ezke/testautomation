package imageRpsThickClient.winiumPages;

import org.openqa.selenium.Keys;
import utils.BaseUI;

public class OperatorPerformanceStatisticsReports_Page {

    public StatisticsMenu_Page navigate_BackTo_StatisticsMenu() throws Exception {
        BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
        return new StatisticsMenu_Page();
    }

    public OperatorDetailBySystemDate_Page navigate_OperatorDetailBySystemDatePage(){
        BaseUI.send_KeyWithActionBuilder("1");
        return new OperatorDetailBySystemDate_Page();
    }


    public DataCompletionJobSummaryByClient navigate_DataCompletionJobSummaryByClient() {
        BaseUI.send_KeyWithActionBuilder("9");
        return new DataCompletionJobSummaryByClient();
    }

    public OneOperatorSummaryByProcessDateReport_Page navigate_OneOperatorSummaryByProcessDate(){
        BaseUI.send_KeyWithActionBuilder("2");
        return new OneOperatorSummaryByProcessDateReport_Page();

    }

    public JobDetailBySystemDate_Page navigate_JobDetailBySystemDate(){
        BaseUI.send_KeyWithActionBuilder("4");
        return new JobDetailBySystemDate_Page();
    }

    public JobSummaryByProcessDate_Page navigate_JobSummaryByProcessDate(){
        BaseUI.send_KeyWithActionBuilder("5");
        return new JobSummaryByProcessDate_Page();
    }

    public AllJobsSummaryByProcessDate_Page navigate_AllJobsSummaryByProcessDate(){
        BaseUI.send_KeyWithActionBuilder("6");
        return new AllJobsSummaryByProcessDate_Page();
    }

    public AllOperatorsSummaryByProcessDate_Page navigate_AllOperatorsSummaryByProcessDate(){
        BaseUI.send_KeyWithActionBuilder("3");
        return new AllOperatorsSummaryByProcessDate_Page();
    }

    public GroupShiftSummaryReport_Page navigate_GroupShiftSummaryReport(){
        BaseUI.send_KeyWithActionBuilder("8");
        return new GroupShiftSummaryReport_Page();
    }

    public ShiftSummaryReportForAllOperators_Page navigate_ShiftSummaryReportForAllOperators(){
        BaseUI.send_KeyWithActionBuilder("7");
        return new ShiftSummaryReportForAllOperators_Page();
    }

}
