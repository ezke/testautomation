package imageRpsThickClient.winiumPages;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class ExportDailyOperatorStatisticsFileReport_Page extends ExportStatistics_ReportBasePage {

    public ExportDailyOperatorStatisticsFileReport_Page() throws Exception {
        waitForPageToLoad();
    }

    @Override
    public void enterDate(String date) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("expDailOperStatFile_Date_TextBox"), date);
    }

}
