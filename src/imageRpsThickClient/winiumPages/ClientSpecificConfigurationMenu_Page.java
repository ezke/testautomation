package imageRpsThickClient.winiumPages;

import utils.BaseUI;
import utils.Locator;

public class ClientSpecificConfigurationMenu_Page extends DCMBasePage {

    public DocumentGroupConfiguration_Page navigate_DocumentGroupConfiguration() throws Exception {
        BaseUI.enterText_SendKeys_WithoutClearing(Locator.lookupElement("dcm_PageClassName"), "6");

        return new DocumentGroupConfiguration_Page();
    }

    public ClientSpecificConfigurationMenu_Page(){

    }

}// End of Class
