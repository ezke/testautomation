package imageRpsThickClient.pageControls;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.XMLParser;
import utils.winiumXRay.XRayUtils;

public class TextBox {

    private String _identifier;
    private String _identifier_Variable1;
    private String _identifier_Variable2;

    public TextBox(String identifier, String identifier_Variable1, String identifier_Variable2){
        _identifier = identifier;
        _identifier_Variable1 = identifier_Variable1;
        _identifier_Variable2 = identifier_Variable2;
    }

    public String getToolTip() throws Exception {
        click();
        String tooltip = XRayUtils.getTooltip(_identifier, _identifier_Variable1, _identifier_Variable2);

        return tooltip;
    }

    public void click(){
        BaseUI.click(getRequiredElement());
    }

    public void enterText(String textToEnter){
        BaseUI.enterText_IntoInputBox(getRequiredElement(), textToEnter);
    }

    private WebElement getRequiredElement(){
        return Locator.lookupRequiredElement(_identifier, _identifier_Variable1, _identifier_Variable2);
    }

}
