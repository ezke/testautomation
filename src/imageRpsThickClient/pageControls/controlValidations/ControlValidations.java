package imageRpsThickClient.pageControls.controlValidations;

import org.openqa.selenium.WebElement;
import utils.BaseUI;

public class ControlValidations {

    private boolean _controlExists = false;
    private WebElement _elementToValidate;

    private ControlValidations(){};

    public ControlValidations(WebElement controlElement){
        if(controlElement != null){
            _controlExists = true;
        }
        _elementToValidate = controlElement;
    }
    
    //Using this method internally to avoid unnecessary logging unless we call for it.
    private void elementExists(){
        if (!_controlExists) {
            BaseUI.log_AndFail("Unable to find element!");
        }
    }


    public ControlValidations exists(){
        BaseUI.verify_true_AndLog(_controlExists, "Element was found.", "Element was NOT found.");
        return new ControlValidations(_elementToValidate);
    }

   public ControlValidations notDisplayed(){
        BaseUI.verifyElementDoesNotAppear(_elementToValidate);
        return new ControlValidations(_elementToValidate);
   }

    public ControlValidations displayed(){
        elementExists();
        BaseUI.verifyElementAppears(_elementToValidate);
        return new ControlValidations(_elementToValidate);
    }

    public ControlValidations disabled(){
        elementExists();
        BaseUI.verifyElementDisabled(_elementToValidate);
        return new ControlValidations(_elementToValidate);
    }



}
