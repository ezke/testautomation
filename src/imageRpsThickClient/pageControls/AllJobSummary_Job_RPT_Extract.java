package imageRpsThickClient.pageControls;

import org.testng.asserts.SoftAssert;
import utils.DataBuilder;
import utils.TableData;

import java.util.HashMap;

public class AllJobSummary_Job_RPT_Extract {

    private TableData _jobDescriptionTable;
    private String[] _rptExtract;

    public AllJobSummary_Job_RPT_Extract(String[] rptExtract) throws Exception {
        _jobDescriptionTable = getJobDescriptionTable();
        _rptExtract = rptExtract;
    }

    private TableData getJobDescriptionTable() throws Exception {
        TableData jobAndDescriptionTable;

        jobAndDescriptionTable = DataBuilder.returnTableData_ForComparison("//src//imageRpsThickClient//data//jobs.csv", ",", true);
        jobAndDescriptionTable.remove_Character_FromKeys("\"");
        jobAndDescriptionTable.remove_Character_FromKeys(" ");
        jobAndDescriptionTable.remove_Character("\"");
        jobAndDescriptionTable.trim_column("Description");
        jobAndDescriptionTable.trim_column("Job");


        return jobAndDescriptionTable;
    }


    //Will splice in the job acronyms.  Used for verifications and to test sorting.
    public TableData getRPT_ReportTable(){
        TableData newTable = new TableData();

        String currentClientID = "";
        SoftAssert softAssert = new SoftAssert();
        for(String row : _rptExtract){
            if(isRowClientID(row)){
                currentClientID = getClientIDForRow(row);

            }else if(isRowJobLine(row)) {

                HashMap<String, String> newRow = new HashMap<>();
                newRow.put("Client ID", currentClientID);
                newRow.put("Job", getJobForRow(row));
                newRow.put("JobAcronym", get_JobAcronym(newRow.get("Job")));
                softAssert.assertTrue(!newRow.get("JobAcronym").equals(""), "Could not find acronym for " + newRow.get("Job"));
                newTable.data.add(newRow);
            }
        }
        softAssert.assertAll();;
        return newTable;
    }

    private String get_JobAcronym(String job){

        for(HashMap<String, String> row : _jobDescriptionTable.data){

            if(row.get("Description").contains(job)){
                return row.get("Job");
            }
        }

        return "";

    }


    private String getClientIDForRow(String row){
        return row.substring(10, row.length() - 1).split("\\-")[0].trim();
    }

    private String getJobForRow(String row){
        return row.substring(6, 23).trim();
    }

    private boolean isRowClientID(String row){

        return row.startsWith("Client ID:");
    }

    private boolean isRowJobLine(String row){

        return row.trim().startsWith("Job:");
    }

}
