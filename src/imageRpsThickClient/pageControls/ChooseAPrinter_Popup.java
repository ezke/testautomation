package imageRpsThickClient.pageControls;

import imageRpsThickClient.pageControls.controlValidations.ControlValidations;
import imageRpsThickClient.pageControls.controlWaits.WaitForControl;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

import java.time.Duration;

public class ChooseAPrinter_Popup {

    public ChooseAPrinter_Popup(){
        XRayUtils.invalidateXRay();
    }

    private final String _printerIdentifier = "choosePrinter_Window";

    private WebElement getElement(){
        return Locator.lookupElement(_printerIdentifier);
    }

    public ControlValidations verify() {
        return new ControlValidations(getElement());
    }

    public WaitForControl wait = new WaitForControl(_printerIdentifier, null, null);


    private boolean printerPopupAppears(){
        return BaseUI.elementAppears(Locator.lookupElement(_printerIdentifier));
    }

    public void exit_PrinterPopup() throws Exception {
        if(printerPopupAppears()){
            BaseUI.send_KeyWithActionBuilder(Keys.ESCAPE);
            wait.notDisplayed(Duration.ofSeconds(15));
        }
    }


    public TableData get_PrinterTableData() throws Exception {
        return XRayUtils.return_Table(new String[]{"Printer", "LPT", "UNC"});
    }


}
