package imageRpsThickClient.pageControls;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

import java.util.ArrayList;

public class ComboBoxControl {

    protected String _elementLocator;
    protected String _elementVariable1;
    protected String _elementVariable2;

    public ComboBoxControl(String locator, String variable1, String variable2) {
        _elementLocator = locator;
        _elementVariable1 = variable1;
        _elementVariable2 = variable2;
    }

    protected  ComboBoxControl(){};

    public void verify_ItemSelected(String expectedValue) throws Exception {
        String actualValue = getSelectedComboBoxItem();
        BaseUI.baseStringCompare(_elementLocator, expectedValue, actualValue);
    }

    public String getSelectedComboBoxItem() throws Exception {
        WebElement comboBoxElement = Locator.lookupElement(_elementLocator, _elementVariable1, _elementVariable2);

        BaseUI.click(comboBoxElement);
        Thread.sleep(200);

        ArrayList<WebElement> comboBoxListElements = new ArrayList<WebElement>();
        comboBoxListElements.addAll(comboBoxElement.findElements(By.xpath(".//*[@LocalizedControlType='list item']")));

        String selectedElementText = getTextOfSelectedElement(comboBoxListElements);
        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);

        return selectedElementText;
    }

    public void select_ComboBoxItem(String optionWeWant) throws Exception {

        WebElement comboBoxElement = Locator.lookupElement(_elementLocator, _elementVariable1, _elementVariable2);

        BaseUI.click(comboBoxElement);
        Thread.sleep(100);

        ArrayList<WebElement> comboBoxListElements = new ArrayList<WebElement>();
        comboBoxListElements.addAll(comboBoxElement.findElements(By.xpath(".//*[@LocalizedControlType='list item']")));

        String selectedElementText = getTextOfSelectedElement(comboBoxListElements);

        if(!selectedElementText.equals(optionWeWant)) {

            ArrayList<String> comboElementTexts = getValueList(comboBoxListElements);

            int originalIndex = comboElementTexts.indexOf(selectedElementText);

            int indexWeWant = comboElementTexts.indexOf(optionWeWant);

            if (originalIndex < indexWeWant) {
                for (int i = 0; i < indexWeWant - originalIndex; i++) {
                    BaseUI.send_KeyWithActionBuilder(Keys.DOWN);
                }
            } else if (originalIndex > indexWeWant) {
                {
                    for (int i = 0; i < originalIndex - indexWeWant; i++) {
                        BaseUI.send_KeyWithActionBuilder(Keys.UP);
                    }
                }
            }
        }

        BaseUI.send_KeyWithActionBuilder(Keys.ENTER);
    }

    //Combobox has to be open for this to work.
    protected String getTextOfSelectedElement( ArrayList<WebElement> comboBoxListElements){
        for(WebElement elementToCheck : comboBoxListElements){
            if(elementToCheck.isSelected()){
                return BaseUI.get_Attribute_FromField(elementToCheck, "Name");
            }
        }

        return null;
    }

    protected ArrayList<String> getValueList(ArrayList<WebElement> listElements) {
        ArrayList<String> textList = new ArrayList<>();
        for (WebElement listElement : listElements) {
            textList.add(listElement.getAttribute("Name"));
        }

        return textList;
    }
}
