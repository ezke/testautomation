package imageRpsThickClient.pageControls;

import imageRpsThickClient.pageControls.controlValidations.ControlValidations;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class Multiline_TextBox {

    private String _identifier;
    private String _identifier_Variable1;
    private String _identifier_Variable2;

    public Multiline_TextBox(String identifier, String identifier_Variable1, String identifier_Variable2){
        _identifier = identifier;
        _identifier_Variable1 = identifier_Variable1;
        _identifier_Variable2 = identifier_Variable2;
    }

    private WebElement getElement(){
        return Locator.lookupElement(_identifier, _identifier_Variable1, _identifier_Variable2);
    }

    public ControlValidations verify() {
       return new ControlValidations(getElement());
    }

    public void enterText(String textToEnter) throws Exception {
        WebElement textBox = Locator.lookupRequiredElement(_identifier, _identifier_Variable1, _identifier_Variable2);
        BaseUI.click(textBox);

        String textBoxText = BaseUI.getTextFromField(textBox);
        for(int i = 0; i < textBoxText.length(); i++){
            BaseUI.send_KeyWithActionBuilder(Keys.BACK_SPACE);
        }

        BaseUI.enterText_IntoInputBox(textBox, textToEnter);
    }

}
