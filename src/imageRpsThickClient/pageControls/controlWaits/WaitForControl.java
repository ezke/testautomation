package imageRpsThickClient.pageControls.controlWaits;

import utils.BaseUI;

import java.time.Duration;

public class WaitForControl {

    private String _identifier;
    private String _identifierVariable1;
    private String _identifierVariable2;

    public WaitForControl(String identifier, String identifierVariable1, String identifierVariable2){
        _identifier = identifier;
        _identifierVariable1 = identifierVariable1;
        _identifierVariable2 = identifierVariable2;
    }

    public void displayed(Duration timeToWait){
        BaseUI.waitForElementToBeDisplayed(_identifier, _identifierVariable1, _identifierVariable2, timeToWait);
    }

    public void notDisplayed(Duration timeToWait){
        BaseUI.waitForElementToNOTBeDisplayed(_identifier, _identifierVariable1, _identifierVariable2, (int)timeToWait.getSeconds());
    }


}
