package imageRpsThickClient.pageControls;

import org.apache.commons.lang3.Range;
import org.testng.asserts.SoftAssert;
import utils.TableData;

import java.text.DecimalFormat;
import java.util.HashMap;

public class RPT_Report {

    private String[] _reportExtract;

    public RPT_Report(String[] reportExtract) {
        _reportExtract = reportExtract;
    }

    public TableData get_Report_Table(Range<Integer> matchingRange, RPT_Matcher matcher, HashMap<String, Range<Integer>> rowsToGet) {
        TableData rptTable = new TableData();

        for (int i = 0; i < _reportExtract.length; i++) {

            if (_reportExtract[i].length() >= matchingRange.getMaximum()) {
                HashMap<String, String> newLine = new HashMap<>();

                String matcherLocation = _reportExtract[i].substring(matchingRange.getMinimum(), matchingRange.getMaximum()).trim();
                //Determine if the row is a data Row
                if (matcherLocation.matches(matcher.getValue())) {

                    for (String key : rowsToGet.keySet()) {

                        Range<Integer> currentRange = rowsToGet.get(key);

                        newLine.put(key, _reportExtract[i].substring(currentRange.getMinimum(), currentRange.getMaximum()).trim());
                    }
                    rptTable.data.add(newLine);
                }
            }
        }

        return rptTable;
    }

    public void split_Column_ByDelimiter(TableData rptTable, String originalColumnName, String newFirstColumnName, String newSecondColumnName, String delimiter) {
        for (HashMap<String, String> row : rptTable.data) {
            String[] splitValue = row.get(originalColumnName).split(delimiter, 2);

            row.put(newFirstColumnName, splitValue[0]);
            row.put(newSecondColumnName, splitValue[1]);
        }
    }

    public void verify_Totals_FarColumn(Range<Integer> rangeToGet, RPT_Matcher rowIsDataMatcher, Range<Integer> sectionToCheckIfRowIsData) {
        SoftAssert softAssert = new SoftAssert();
        boolean totalFound = false;
        String previousLine = "";

        int count = 0;
        int currentTotal = 0;
        for (String line : _reportExtract) {

            if (isTotal(previousLine)) {
                totalFound = true;

                int actualTotal = getIntValue(line.substring(rangeToGet.getMinimum(), rangeToGet.getMaximum()));
                double averageTotal = Double.valueOf(String.valueOf(currentTotal))/Double.valueOf(String.valueOf(count));
                DecimalFormat df = new DecimalFormat("###.");
                int averageTotalInt = Integer.valueOf(df.format(averageTotal).split("\\.")[0]);

                softAssert.assertTrue(actualTotal == averageTotalInt,
                        "Expected Total of " + String.valueOf(averageTotalInt) + " But seeing " + String.valueOf(actualTotal));

                count = 0;
                currentTotal = 0;
            } else if (isEntry(line, sectionToCheckIfRowIsData, rowIsDataMatcher)) {
                count++;
                currentTotal += getIntValue(line.substring(rangeToGet.getMinimum(), rangeToGet.getMaximum()));
            }

            previousLine = line;
        }
        softAssert.assertTrue(totalFound, "Did NOT find any totals.");

        softAssert.assertAll();
    }

    private int getIntValue(String stringToConvert){
        return Integer.valueOf(stringToConvert.replace(",", "").trim());
    }

    private boolean isEntry(String line, Range<Integer> matchingRange, RPT_Matcher matcher) {
        if (line.length() < matchingRange.getMaximum()) {
            return false;
        }

        String matcherLocation = line.substring(matchingRange.getMinimum(), matchingRange.getMaximum()).trim();
        //Determine if the row is a data Row
        if (matcherLocation.matches(matcher.getValue())) {
            return true;
        }

        return false;
    }

    private boolean isTotal(String previousLine) {
        if (previousLine.contains("Totals for Station ID")) {
            return true;
        }

        return false;
    }

    public enum RPT_Matcher {
        //ex: -5 or 52
        INTEGER("^-?\\d+$"),
        //ex: 50 or 5,000
        NUMBER("^\\d+,?\\d+$"),
        //ex: 20-2020
        CLIENT_LOCKBOX("^\\d+-\\d+$"),
        //ex: 12:00
        TIME("^\\d{2}:\\d{2}$"),
        //ex: 01/01/17
        DATE("^\\d{2}/\\d{2}/\\d{2}$");

        private String value;

        public String getValue() {
            return value;
        }

        private RPT_Matcher(final String val) {
            value = val;
        }

    }
}//End of Class
