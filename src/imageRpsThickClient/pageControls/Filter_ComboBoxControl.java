package imageRpsThickClient.pageControls;

import imageRpsThickClient.pageControls.controlValidations.ControlValidations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

import java.util.ArrayList;

public class Filter_ComboBoxControl extends ComboBoxControl {

    public Filter_ComboBoxControl(String locator, String variable1, String variable2) {
        _elementLocator = locator;
        _elementVariable1 = variable1;
        _elementVariable2 = variable2;
    }

    private WebElement getRequiredElement(){
        return Locator.lookupRequiredElement(_elementLocator, _elementVariable1, _elementVariable2);
    }

    private WebElement getNullableElement(){
        return Locator.lookupElement(_elementLocator, _elementVariable1, _elementVariable2);
    }


    @Override
    public void select_ComboBoxItem(String optionWeWant) throws Exception {

        WebElement comboBoxElement = getRequiredElement();

        BaseUI.click(comboBoxElement);
        Thread.sleep(300);

        ArrayList<WebElement> comboBoxListElements = new ArrayList<WebElement>();
        comboBoxListElements.addAll(comboBoxElement.findElements(By.xpath(".//*[@LocalizedControlType='list item']")));

        ArrayList<String> comboElementTexts = getValueList(comboBoxListElements);

        if(comboElementTexts.size() == 0){
            BaseUI.log_AndFail("Unable to get dropdown elements.");
        }

        for(int i = 0; i < comboElementTexts.size(); i++)
        {
            if(comboElementTexts.get(i).equals(optionWeWant)){
                BaseUI.click(comboBoxListElements.get(i));
                break;
            }
        }

    }


}
