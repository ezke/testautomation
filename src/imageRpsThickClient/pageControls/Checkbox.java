package imageRpsThickClient.pageControls;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class Checkbox {

    private String _identifier;
    private String _identifier_Variable1;
    private String _identifier_Variable2;

    public Checkbox(String identifier, String identifier_Variable1, String identifier_Variable2){
        _identifier = identifier;
        _identifier_Variable1 = identifier_Variable1;
        _identifier_Variable2 = identifier_Variable2;
    }

    private WebElement getRequiredElement(){
        return Locator.lookupRequiredElement(_identifier, _identifier_Variable1, _identifier_Variable2);
    }

    public void check(){
        BaseUI.checkCheckbox(getRequiredElement());
    }

    public void uncheck(){
        BaseUI.uncheckCheckbox(getRequiredElement());
    }

}
