package imageRpsThickClient.pageControls;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;
import utils.winiumXRay.XRayUtils;

import java.util.HashMap;

public class Reports_OutputAndRunOptions {

    private String _reportSortOrder_ColumnName = "Report Sort Order";
    private String _availableFields_ColumnName = "Available Fields";

    private String _reportPaneWindowName;

    public Reports_OutputAndRunOptions(String reportPaneWindowName) {
        _reportPaneWindowName = reportPaneWindowName;
    }

    public void add_Option_From_OutputAndRunOptions_Table(String optionToAdd) throws Exception {

        WebElement frame = Locator.lookupRequiredElement("outputAndRunOpts_AvailableOptions_Table", _reportPaneWindowName, null);

        Actions action = new Actions(Browser.driver);
        action.moveToElement(frame, 24, 5).click().build().perform();

        TableData outputTable = XRayUtils.return_Table(new String[]{_availableFields_ColumnName});
        XRayUtils.navigate_ToLocation_InTable(outputTable, _availableFields_ColumnName, optionToAdd);
        BaseUI.click(Locator.lookupRequiredElement("outputAndRunOpts_AddField"));
        XRayUtils.invalidateXRay();
    }

    public void remove_All_ButOption(String optionToKeep) throws Exception {
        TableData allReportSortOrderOptions = XRayUtils.return_Table(new String[]{_reportSortOrder_ColumnName});
        remove_PlusAndMinus_FromReportSortOrder(allReportSortOrderOptions);

        for (HashMap<String, String> row : allReportSortOrderOptions.data) {
            String option = row.get(_reportSortOrder_ColumnName);
            if (!option.equals(optionToKeep)) {

                remove_Option_From_ReportSortOrder(option);
            }
        }
    }

    //Need to have frame clicked before calling this.
    private void navigateToTop() throws Exception {

        for (int i = 0; i < 10; i++) {
            BaseUI.send_KeyWithActionBuilder(Keys.ARROW_UP);
        }
    }

    public void remove_Option_From_ReportSortOrder(String optionToRemove) throws Exception {
        WebElement frame = Locator.lookupRequiredElement("outputAndRunOpts_ReportSortOrder_Table", _reportPaneWindowName, null);

        Actions action = new Actions(Browser.driver);
        action.moveToElement(frame, 24, 5).click().build().perform();
        navigateToTop();

        TableData outputTable = XRayUtils.return_Table(new String[]{_reportSortOrder_ColumnName});
        remove_PlusAndMinus_FromReportSortOrder(outputTable);
        XRayUtils.navigate_ToLocation_InTable(outputTable, _reportSortOrder_ColumnName, optionToRemove);

        BaseUI.click(Locator.lookupRequiredElement("outputAndRunOpts_RemoveField"));

        XRayUtils.invalidateXRay();
    }

    public void click_Ascending_Or_Descending() {
        BaseUI.click(Locator.lookupRequiredElement("outputAndRunOpts_AscOrDesc_Button"));
    }

    public void verify_FailIfNoData_Checked() {
        BaseUI.verifyCheckboxStatus(Locator.lookupRequiredElement("outputAndRunOpts_FailIfNoData_Checkbox"), true);
    }

    private void remove_PlusAndMinus_FromReportSortOrder(TableData tableToFormat) {
        tableToFormat.remove_Character("+");
        tableToFormat.remove_Character("-");
        tableToFormat.trim_column(_reportSortOrder_ColumnName);
    }

    private TableData getOutputTable() throws Exception {
        TableData outputTable = XRayUtils.return_Table(new String[]{_reportSortOrder_ColumnName});

        return outputTable;
    }

    public void verify_ReportSortOrder_ContainsOptions(String[] expectedOptions) throws Exception {
        TableData outputTable = getOutputTable();
        remove_PlusAndMinus_FromReportSortOrder(outputTable);

        BaseUI.verify_true_AndLog(outputTable.data.size() == expectedOptions.length, "Table matched expected length.",
                "Table did not match expected size of " + expectedOptions);

        for (String option : expectedOptions) {
            outputTable.verify_Value_InColumn(_reportSortOrder_ColumnName, option);
        }
    }

    public void select_ReportSortOrder_Item(String optionToPick) throws Exception {
        WebElement frame = Locator.lookupRequiredElement("outputAndRunOpts_ReportSortOrder_Table", _reportPaneWindowName, null);

        Actions action = new Actions(Browser.driver);
        action.moveToElement(frame, 24, 5).click().build().perform();

        TableData outputTable = getOutputTable();

        remove_PlusAndMinus_FromReportSortOrder(outputTable);

        int indexOf_Option = outputTable.first_IndexOf(_reportSortOrder_ColumnName, optionToPick);
        XRayUtils.navigate_ToLocation_InTable(outputTable, _reportSortOrder_ColumnName, optionToPick);
    }

    public void click_Top() {
        BaseUI.click(Locator.lookupRequiredElement("outputAndRunOpts_TopButton"));
    }
}
