package imageRpsThickClient.unitTests;

import imageRpsThickClient.programInfo.ProgramInfoXmlParser;
import imageRpsThickClient.programInfo.xmlTypes.*;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;

import javax.xml.bind.JAXBException;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings("SpellCheckingInspection")
public class ProgramInfoXmlParserTests {
    private XmlProgInfoDs parse(String xml) throws JAXBException {
        return ProgramInfoXmlParser.parseXml(xml);
    }

    @Test
    public void empty() throws JAXBException {
        XmlProgInfoDs data = parse("<ProgInfo_DS/>");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(data.getTtbHeader(), null, "ttbHeader");
        softAssert.assertEquals(data.getTtbWidget().size(), 0, "ttbWidget count");
        softAssert.assertEquals(data.getTtbBrowseRow().size(), 0, "ttbBrowseRow count");
        softAssert.assertEquals(data.getTtbBrowseRowDetail().size(), 0, "ttbBrowseRowDetail count");
        softAssert.assertAll();
    }

    @Test
    public void emptyHeader() throws JAXBException {
        XmlHeader header = parse("<ProgInfo_DS>\n" +
                "  <ttbHeader />\n" +
                "</ProgInfo_DS>"
        ).getTtbHeader();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(header.getProgramHandle(), 0L, "ProgramHandle");
        softAssert.assertEquals(header.getProgramName(), null, "ProgramName");
        softAssert.assertEquals(header.getWinTitle(), null, "WinTitle");
        softAssert.assertEquals(header.getProgramStack(), null, "ProgramStack");
        softAssert.assertEquals(header.getPersistentProc(), null, "PersistentProc");
        softAssert.assertAll();
    }

    @Test
    public void header() throws JAXBException {
        final String title = "ImageRPS(R) DCM *TEST System* Site: WFS TEST SYSTEM  Station: 10  Operator: joe";
        final String programStack = "ProgInfoGet prog_info\\prog_info.p\nutility/prog_info_start.p";
        final String persistentProc = "PERSISTENT PROCEDURES: ...";
        XmlHeader header = parse("<ProgInfo_DS>\n" +
                "  <ttbHeader>\n" +
                "    <ProgramHandle>1900</ProgramHandle>\n" +
                "    <ProgramName>rptsetup/ch_rpt_descr.p</ProgramName>\n" +
                "    <WinTitle>" + title + "</WinTitle>\n" +
                "    <ProgramStack>" + programStack + "</ProgramStack>\n" +
                "    <PersistentProc>" + persistentProc + "</PersistentProc>\n" +
                "  </ttbHeader>\n" +
                "</ProgInfo_DS>"
        ).getTtbHeader();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(header.getProgramHandle(), 1900L, "ProgramHandle");
        softAssert.assertEquals(header.getProgramName(), "rptsetup/ch_rpt_descr.p", "ProgramName");
        softAssert.assertEquals(header.getWinTitle(), title, "WinTitle");
        softAssert.assertEquals(header.getProgramStack(), programStack, "ProgramStack");
        softAssert.assertEquals(header.getPersistentProc(), persistentProc, "PersistentProc");
        softAssert.assertAll();
    }

    @Test
    public void widget() throws JAXBException {
        List<XmlWidget> widgets = parse("<ProgInfo_DS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                "  <ttbWidget>\n" +
                "    <WidgetID>111</WidgetID>\n" +
                "    <ParentID>222</ParentID>\n" +
                "    <WidgetHandle>333</WidgetHandle>\n" +
                "    <WidgetName>report_descr.report_name</WidgetName>\n" +
                "    <WidgetType>Column (FILL-IN)</WidgetType>\n" +
                "    <WidgetLabel>Name</WidgetLabel>\n" +
                "    <WidgetValue>arc/conprt1</WidgetValue>\n" +
                "    <WindowsHandle>1115114</WindowsHandle>\n" +
                "  </ttbWidget>\n" +
                "</ProgInfo_DS>"
        ).getTtbWidget();
        BaseUI.assertThat("Widget count", widgets.size(), equalTo(1));
        XmlWidget widget = widgets.get(0);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(widget.getWidgetID(), 111L, "WidgetID");
        softAssert.assertEquals(widget.getParentID(), new Long(222L), "ParentID");
        softAssert.assertEquals(widget.getWidgetHandle(), 333L, "WidgetHandle");
        softAssert.assertEquals(widget.getWidgetName(), "report_descr.report_name", "WidgetName");
        softAssert.assertEquals(widget.getWidgetType(), "Column (FILL-IN)", "WidgetType");
        softAssert.assertEquals(widget.getWidgetLabel(), "Name", "WidgetLabel");
        softAssert.assertEquals(widget.getWidgetValue(), "arc/conprt1", "WidgetValue");
        softAssert.assertEquals(widget.getWindowsHandle(), 1115114L, "WindowsHandle");
        softAssert.assertAll();
    }

    @Test
    public void widgetWithEmptyElements() throws JAXBException {
        List<XmlWidget> widgets = parse("<ProgInfo_DS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                "  <ttbWidget>\n" +
                "    <WidgetLabel/>\n" +
                "    <WidgetValue/>\n" +
                "  </ttbWidget>\n" +
                "</ProgInfo_DS>"
        ).getTtbWidget();
        BaseUI.assertThat("Widget count", widgets.size(), equalTo(1));
        XmlWidget widget = widgets.get(0);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(widget.getWidgetLabel(), "", "WidgetLabel");
        softAssert.assertEquals(widget.getWidgetValue(), "", "WidgetValue");
        softAssert.assertAll();
    }

    @Test
    public void widgetWithNulls() throws JAXBException {
        List<XmlWidget> widgets = parse("<ProgInfo_DS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                "  <ttbWidget>\n" +
                "    <ParentID xsi:nil=\"true\"/>\n" +
                "    <WidgetLabel xsi:nil=\"true\"/>\n" +
                "  </ttbWidget>\n" +
                "</ProgInfo_DS>"
        ).getTtbWidget();
        BaseUI.assertThat("Widget count", widgets.size(), equalTo(1));
        XmlWidget widget = widgets.get(0);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(widget.getParentID(), null, "ParentID");
        softAssert.assertEquals(widget.getWidgetLabel(), null, "WidgetLabel");
        softAssert.assertAll();
    }

    @Test
    public void browseRow() throws JAXBException {
        List<XmlBrowseRow> rows = parse("<ProgInfo_DS>\n" +
                "  <ttbBrowseRow>\n" +
                "    <WidgetID>111</WidgetID>\n" +
                "    <ParentID>222</ParentID>\n" +
                "    <BrowseRow>333</BrowseRow>\n" +
                "  </ttbBrowseRow>\n" +
                "</ProgInfo_DS>"
        ).getTtbBrowseRow();
        BaseUI.assertThat("Row count", rows.size(), equalTo(1));
        XmlBrowseRow row = rows.get(0);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(row.getWidgetID(), 111L, "WidgetID");
        softAssert.assertEquals(row.getParentID(), 222L, "ParentID");
        softAssert.assertEquals(row.getBrowseRow(), 333L, "BrowseRow");
        softAssert.assertAll();
    }

    @Test
    public void browseRowDetail() throws JAXBException {
        List<XmlBrowseRowDetail> details = parse("<ProgInfo_DS>\n" +
                "  <ttbBrowseRowDetail>\n" +
                "    <WidgetID>111</WidgetID>\n" +
                "    <ParentID>222</ParentID>\n" +
                "    <WidgetHandle>333</WidgetHandle>\n" +
                "    <WidgetName>widget_name</WidgetName>\n" +
                "    <WidgetType>Column (FILL-IN)</WidgetType>\n" +
                "    <WidgetLabel>Name</WidgetLabel>\n" +
                "    <WidgetValue>value</WidgetValue>\n" +
                "    <WindowsHandle>444</WindowsHandle>\n" +
                "    <BrowseRow>1</BrowseRow>\n" +
                "  </ttbBrowseRowDetail>\n" +
                "</ProgInfo_DS>"
        ).getTtbBrowseRowDetail();
        BaseUI.assertThat("Row count", details.size(), equalTo(1));
        XmlBrowseRowDetail detail = details.get(0);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(detail.getWidgetID(), 111L, "WidgetID");
        softAssert.assertEquals(detail.getParentID(), 222L, "ParentID");
        softAssert.assertEquals(detail.getWidgetHandle(), 333L, "WidgetHandle");
        softAssert.assertEquals(detail.getWidgetName(), "widget_name", "WidgetName");
        softAssert.assertEquals(detail.getWidgetType(), "Column (FILL-IN)", "WidgetType");
        softAssert.assertEquals(detail.getWidgetLabel(), "Name", "WidgetLabel");
        softAssert.assertEquals(detail.getWidgetValue(), "value", "WidgetValue");
        softAssert.assertEquals(detail.getWindowsHandle(), 444L, "WindowsHandle");
        softAssert.assertEquals(detail.getBrowseRow(), 1L, "BrowseRow");
        softAssert.assertAll();
    }
}
