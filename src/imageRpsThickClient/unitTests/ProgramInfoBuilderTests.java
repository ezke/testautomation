package imageRpsThickClient.unitTests;

import imageRpsThickClient.programInfo.*;
import imageRpsThickClient.programInfo.xmlTypes.XmlBrowseRowDetail;
import imageRpsThickClient.programInfo.xmlTypes.XmlWidget;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.TableData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.hamcrest.Matchers.*;

public class ProgramInfoBuilderTests {
    private List<XmlWidget> xmlWidgets;
    private List<XmlBrowseRowDetail> xmlBrowseRowDetails;

    @BeforeMethod
    public void beforeMethod() {
        xmlWidgets = new ArrayList<>();
        xmlBrowseRowDetails = new ArrayList<>();
    }

    @SuppressWarnings("SameParameterValue")
    private void addBrowseRowDetail(long gridId, int oneBasedBrowseRow, String label, String value) {
        XmlBrowseRowDetail detail = new XmlBrowseRowDetail();
        detail.setParentID(gridId);
        detail.setBrowseRow(oneBasedBrowseRow);
        detail.setWidgetName("some/ugly/name/that/will/be/ignored");
        detail.setWidgetLabel(label);
        detail.setWidgetValue(value);
        xmlBrowseRowDetails.add(detail);
    }

    private void addWidget() {
        addWidget(widget -> {
        });
    }

    private void addWidget(Consumer<XmlWidget> action) {
        XmlWidget xmlWidget = new XmlWidget();
        action.accept(xmlWidget);
        xmlWidgets.add(xmlWidget);
    }

    private ProgramInfo build() {
        ProgramInfoBuilder programInfoBuilder = new ProgramInfoBuilder(xmlWidgets, xmlBrowseRowDetails);
        return programInfoBuilder.createProgramInfo();
    }

    private Element singleElement(ProgramInfo programInfo) {
        List<Element> elements = programInfo.elements();
        BaseUI.assertThat("Element count", elements.size(), equalTo(1));
        return elements.get(0);
    }

    private Grid singleGrid(ProgramInfo programInfo) {
        List<Grid> grids = programInfo.grids();
        BaseUI.assertThat("Element count", grids.size(), equalTo(1));
        return grids.get(0);
    }

    @Test
    public void empty() {
        ProgramInfo programInfo = build();

        BaseUI.assertThat("Element count", programInfo.elements().size(), equalTo(0));
        BaseUI.assertThat("Grid count", programInfo.grids().size(), equalTo(0));
    }

    @Test
    public void element() {
        final long id = 123L;
        final Long parentId = 234L; // can be null, so needs to be Long instead of long
        final long windowHandle = 345L;
        final String elementType = ElementType.TEXT_BOX;
        final String name = "Name";
        final String label = "Label";
        final String value = "Value";
        addWidget(w -> {
            w.setParentID(parentId);
            w.setWidgetID(id);
            w.setWindowsHandle(windowHandle);
            w.setWidgetType(elementType);
            w.setWidgetName(name);
            w.setWidgetLabel(label);
            w.setWidgetValue(value);
        });
        Element element = singleElement(build());

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(element.id(), id, "ID");
        softAssert.assertEquals(element.parentId(), parentId, "Parent ID");
        softAssert.assertEquals(element.windowHandle(), windowHandle, "Window handle");
        softAssert.assertEquals(element.elementType(), elementType, "Element type");
        softAssert.assertEquals(element.name(), name, "Name");
        softAssert.assertEquals(element.label(), label, "Label");
        softAssert.assertEquals(element.value(), value, "Value");
        softAssert.assertAll();
    }

    @Test
    public void multipleElements() {
        addWidget();
        addWidget();
        List<Element> elements = build().elements();
        BaseUI.assertThat("Element count", elements.size(), equalTo(2));
    }

    @Test
    public void gridCurrentRow() {
        final long gridId = 999L;
        addWidget(w -> {
            w.setWidgetID(gridId);
            w.setWidgetType(ElementType.GRID);
        });
        addWidget(w -> {
            w.setWidgetID(101L);
            w.setParentID(gridId);
            w.setWidgetName("some/ugly/name/that/will/be/ignored");
            w.setWidgetLabel("Column Header 1");
            w.setWidgetValue("Value 1");
        });
        addWidget(w -> {
            w.setWidgetID(102L);
            w.setParentID(gridId);
            w.setWidgetName("some/ugly/name/that/will/be/ignored");
            w.setWidgetLabel("Column Header 2");
            w.setWidgetValue("Value 2");
        });
        addWidget(w -> {
            w.setWidgetID(103L);
            w.setWidgetName("some/ugly/name/that/will/be/ignored");
            w.setWidgetLabel("Other control not in the grid");
            w.setWidgetValue("Value 3");
        });

        Map<String, String> currentRow = singleGrid(build()).currentRow();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(currentRow.size(), 2, "Count");
        softAssert.assertEquals(currentRow.get("Column Header 1"), "Value 1");
        softAssert.assertEquals(currentRow.get("Column Header 2"), "Value 2");
        softAssert.assertAll();
    }

    @Test
    public void gridDataForEmptyGrid() {
        addWidget(w -> {
            w.setWidgetID(999L);
            w.setWidgetType(ElementType.GRID);
        });
        TableData data = singleGrid(build()).data();
        BaseUI.assertThat("Row count", data.rowCount(), equalTo(0));
    }

    @Test
    public void gridData() {
        final long gridId = 999L;
        addWidget(w -> {
            w.setWidgetID(gridId);
            w.setWidgetType(ElementType.GRID);
        });
        // The real data file is in column-major order like this, with all rows
        // for one column, then all rows for the next column, etc.
        addBrowseRowDetail(gridId, 1, "Column 1", "R1C1");
        addBrowseRowDetail(gridId, 2, "Column 1", "R2C1");
        addBrowseRowDetail(gridId, 3, "Column 1", "R3C1");
        addBrowseRowDetail(gridId, 1, "Column 2", "R1C2");
        addBrowseRowDetail(gridId, 2, "Column 2", "R2C2");
        addBrowseRowDetail(gridId, 3, "Column 2", "R3C2");

        TableData data = singleGrid(build()).data();
        BaseUI.assertThat(data.toString(), equalTo("Column 1=R1C1\tColumn 2=R1C2\n" +
                "Column 1=R2C1\tColumn 2=R2C2\n" +
                "Column 1=R3C1\tColumn 2=R3C2"));
    }
}
