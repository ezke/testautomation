package imageRpsThickClient.xray;

import imageRpsThickClient.programInfo.Grid;
import utils.TableData;
import utils.WhiteSeedWrapper.Application;
import java.util.HashMap;

public class XrayGrid {

    private final Application _application;
    private final Grid _grid;

    public XrayGrid(Application application, Grid grid) {
        _application = application;
        _grid = grid;
    }

    public String name() {
        return _grid.name();
    }

    public TableData data() {
        return _grid.data();
    }

    public int currentRowIndex() {
        return _grid.data().return_RowIndex_BasedOn_AnyNumberOfMatchingFields(_grid.currentRow());
    }

    private int getRowIndexByColumnValue(String columnName, String columnValue) {
        return _grid.data().return_RowIndex_BasedOn_1MatchingFields(columnName, columnValue);
    }

    public HashMap<String, String> getRowByColumnValue(String columnName, String columnValue){
        return _grid.data().return_Row_BasedOn_1MatchingField(columnName,columnValue);
    }

    public void selectRowByColumnValue(String columnName, String columnValue) {

        int rowIndex = getRowIndexByColumnValue(columnName, columnValue);
        String keys = "";
        int currentRowIndex = currentRowIndex();
        if (currentRowIndex == rowIndex) {

        } else if (currentRowIndex < rowIndex) {
            int moves = rowIndex - currentRowIndex;
            for (int i = 0; i < moves; i++) {
                keys = keys + "<Down/>";
            }
        } else {
            int moves = currentRowIndex - rowIndex;
            for (int i = 0; i < moves; i++) {
                keys = keys + "<Up/>";
            }
        }
        _application.typeOnKeyboardWithSpecialKeys(keys + "<Space/>");
    }
}
