package imageRpsThickClient.xray;

import org.jetbrains.annotations.NotNull;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;
import utils.WhiteSeedWrapper.ElementProperties;

public class XrayElement {

    private final Application _application;
    private final long _windowHandle;
    private final String _name;
    private final String _label;
    private final String _elementType;
    private final String _initialValue;

    public XrayElement(Application application, long windowHandle, String name, String label,
                       String elementType, String initialValue) {
        _application = application;
        _windowHandle = windowHandle;
        _name = name;
        _label = label;
        _elementType = elementType;
        _initialValue = initialValue;
    }

    public long windowHandle() {
        return _windowHandle;
    }

    public String name() {
        return _name;
    }

    public String label() {
        return _label;
    }

    public String elementType() {
        return _elementType;
    }

    public String initialValue() {
        return _initialValue;
    }

    @NotNull
    private ElementFilter getElementFilter() {
        return ElementFilter.byWindowHandle(_windowHandle);
    }

    public String getText() {
        return _application.getElementText(getElementFilter());
    }

    public void typeText(String value) {
        _application.clearElement(getElementFilter());
        _application.putElementText(value, getElementFilter());
    }

    public void typeWithSpecialKeys(String value) {
        _application.putElementTextWithSpecialKeys(value, getElementFilter());
    }

    public void click() {
        _application.clickElement(getElementFilter());
    }

    private ElementProperties getProperties() {
        return _application.getElement(getElementFilter());
    }

    public boolean isEnabled() {
        return getProperties().isEnabled();
    }
}
