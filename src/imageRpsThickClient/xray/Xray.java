package imageRpsThickClient.xray;

import imageRpsThickClient.programInfo.ElementType;
import org.jetbrains.annotations.NotNull;
import utils.BaseUI;
import utils.Is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Xray {
    private final List<XrayElement> _elements;
    private final List<XrayGrid> _grids;

    public Xray(List<XrayElement> xrayElements, List<XrayGrid> xrayGrids) {
        _elements = new ArrayList<>(xrayElements);
        _grids = new ArrayList<>(xrayGrids);
    }

    @NotNull
    public XrayElement element(Predicate<XrayElement> filter) {
        return first(elements(filter));
    }

    @NotNull
    public List<XrayElement> elementsByElementType(String elementType) {
        return elements(element -> element.elementType().equals(elementType));
    }

    @NotNull
    public XrayElement elementByName(String name) {
        return element(element -> element.name().equals(name));
    }

    @NotNull
    public XrayElement elementByLabel(String label) {
        return element(element -> element.label().equals(label));
    }

    @NotNull
    public XrayGrid gridByName(String name) {
        return first(gridsByName(name));
    }

    @NotNull
    public XrayGrid firstGrid() {
        return first(_grids);
    }

    @NotNull
    public List<XrayGrid> gridsByName(String name) {
        return filterList(_grids, grid -> grid.name().equals(name));
    }

    @NotNull
    public List<XrayElement> elements(Predicate<XrayElement> filter) {
        return filterList(_elements, filter);
    }

    private <E> List<E> filterList(List<E> list, Predicate<E> filter) {
        return list.stream().filter(filter).collect(Collectors.toList());
    }

    @NotNull
    private <E> E first(List<E> list) {
        if (list.size() == 0) {
            throw new IllegalStateException("No match found");
        }
        // if there are multiple overlapping elements on screen, prog_info returns the top one first
        E value = list.get(0);
        if (value == null)
            throw new IllegalStateException("List contained null value");
        return value;
    }

    /**
     * Returns a string holding the titles of all the frames, sorted alphabetically and
     * separated by pipe characters ("|").
     *
     * @return The current page's frame titles, sorted and pipe-separated
     */
    public String getFrameTitles() {
        List<XrayElement> frames = elementsByElementType(ElementType.FRAME);
        return frames.stream()
                .map(frame -> frame.label().trim())
                .filter(title -> !title.isEmpty())
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.joining("|"));
    }

    public void verifyFrameTitles(String... expectedFrameTitles) {
        String actualTitlesToCompare = getFrameTitles();

        String expectedTitlesToCompare = Arrays.stream(expectedFrameTitles)
                .map(String::trim)
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.joining("|"));

        BaseUI.assertThat("Frame titles", actualTitlesToCompare, Is.equalTo(expectedTitlesToCompare));
    }
}
