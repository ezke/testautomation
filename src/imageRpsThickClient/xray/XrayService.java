package imageRpsThickClient.xray;

import imageRpsThickClient.programInfo.Element;
import imageRpsThickClient.programInfo.Grid;
import imageRpsThickClient.programInfo.ProgramInfo;
import imageRpsThickClient.programInfo.ProgramInfoDriver;
import utils.WhiteSeedWrapper.Application;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class XrayService {

    private final Application _application;
    private final ProgramInfoDriver _programInfoDriver;

    public XrayService(Application application, ProgramInfoDriver programInfoDriver) {
        _application = application;
        _programInfoDriver = programInfoDriver;
    }

    public Xray xray() {
        return xray(Duration.ofSeconds(60));
    }

    public Xray xray(Duration timeToWait) {
        ProgramInfo programInfo = retrieveProgramInfo(timeToWait);
        List<XrayElement> xrayElements = programInfo.elements().stream()
                .map(this::createXrayElement)
                .collect(Collectors.toList());
        List<XrayGrid> xrayGrids = programInfo.grids().stream()
                .map(this::createXrayGrid)
                .collect(Collectors.toList());
        return new Xray(xrayElements, xrayGrids);
    }

    private XrayElement createXrayElement(Element element) {
        return new XrayElement(
                _application,
                element.windowHandle(),
                element.name(),
                element.label(),
                element.elementType(),
                element.value());
    }

    private XrayGrid createXrayGrid(Grid grid) {
        return new XrayGrid(_application, grid);
    }

    private ProgramInfo retrieveProgramInfo(Duration timeToWait) {

        ProgramInfoDriver programInfoDriver = _programInfoDriver;
        try {
            programInfoDriver.deleteXmlFile();
            _application.typeOnKeyboardWithSpecialKeys("<Control><Alt>s</Alt></Control>");
            return programInfoDriver.loadProgramInfo(timeToWait);
        } catch (IOException | JAXBException | TimeoutException e) {
            throw new Error("Error retrieving program info: " + e.getMessage(), e);
        }
    }
}
