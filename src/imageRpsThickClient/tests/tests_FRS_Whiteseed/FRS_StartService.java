package imageRpsThickClient.tests.tests_FRS_Whiteseed;

import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRSAdminApp;
import imageRpsThickClient.data.BaseTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;


public class FRS_StartService extends BaseTest {
    private FRSAdminApp frs_service_ui_launch;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        FRS.windowsService().stop();
        frs_service_ui_launch = new FRSAdminApp();
    }

    @Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
    public void PT1728_Verify_FRS_Service_StopCheck() throws Exception {
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Stopped"), "Service Started Successfully", "Service Stop Failed");
    }

    @Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
    public void PT1728_Verify_FRS_Service_UI_Launch()throws Exception {
        frs_service_ui_launch.startService();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Running"), "Service Stopped Successfully", "Service Start Failed");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        frs_service_ui_launch.closeWindow();
    }

}//End of Class


