package imageRpsThickClient.tests.tests_FRS_Whiteseed;

import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRSAdminApp;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import imageRpsThickClient.data.BaseTest;
import org.testng.annotations.Test;
import utils.BaseUI;

public class FRS_AddDriveMapping extends BaseTest{
        private FRSAdminApp frs_service_ui_launch;

        @BeforeClass(alwaysRun = true)
        public void setup_method() throws Exception {
            FRS.windowsService().start();
            frs_service_ui_launch = new FRSAdminApp();
        }

        @Test(groups = {"all_Tests", "regression_Tests"}, priority = 1)
        public void PT1732_Verify_FRS_Click_DriveMappingTab() throws Exception {
            frs_service_ui_launch.openDriveMappingTab();
            BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_DriveMappingsTab().equals("Drive Mappings"), "Drive Mappings Tab Open", "Drive Mappings Tab not clicked");
        }

        @Test(groups = {"all_Tests", "regression_Tests"}, priority = 2)
        public void PT1732_Verify_FRS_Click_AddDriveMapping() throws Exception {
            frs_service_ui_launch.clickAddDriveMapping();
            BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_AddDriveMappingsPopUpWindow().equals("Drive:"), "Add Drive Mapping window Appears", " Add Drive Mapping Window failed to open");
        }

        @Test(groups = {"all_Tests", "regression_Tests"}, priority = 4)
        public void PT1732_Verify_FRS_FillFolderField() throws Exception {
            frs_service_ui_launch.fillFolder();
            frs_service_ui_launch.clickAddButton();
        }

        @Test(groups = {"all_Tests", "regression_Tests"}, priority = 9)
        public void PT1732_Verify_FRS_Click_SaveButton() throws Exception {
            frs_service_ui_launch.clickSaveButton();
            BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Running"), "Service Started Successfully", "Service Start Failed");
        }

        @AfterClass(alwaysRun = true)
        public void tearDown() throws Exception {
            frs_service_ui_launch.closeWindow();
        }
    }//End of Class
