package imageRpsThickClient.tests.tests_FRS_Whiteseed;

import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRSAdminApp;
import imageRpsThickClient.data.BaseTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.WindowsServiceState;

public class FRS_PauseService extends BaseTest {
    private FRSAdminApp frs_service_ui_launch;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        FRS.windowsService().start();
        frs_service_ui_launch = new FRSAdminApp();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 1)
    public void PT1729_Verfiy_FRS_Service_RunCheck() throws Exception {
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Running"), "Service Started Successfully", "Service Start Failed");
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 2)
    public void PT1729_Verfiy_FRS_ClickPauseService() throws Exception {
        frs_service_ui_launch.pauseService();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Paused"), "Service Paused Successfully", "Service Pause Failed");
        FRS.windowsService().validateWindowServiceState_MatchesExpected(WindowsServiceState.PAUSED);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        frs_service_ui_launch.closeWindow();
        FRS.windowsService().resume();
        FRS.windowsService().stop();
    }

}