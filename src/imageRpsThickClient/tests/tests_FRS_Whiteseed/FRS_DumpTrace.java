package imageRpsThickClient.tests.tests_FRS_Whiteseed;

import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRSAdminApp;
import imageRpsThickClient.data.BaseTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.FileOperations;

public class FRS_DumpTrace extends BaseTest {
    private FRSAdminApp frs_service_ui_launch;
    private String FRS_Log_Directory = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\logs\\";
    private String FRS_Log_File = "RecoInstance.1.log";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        FRS.windowsService().start();
        frs_service_ui_launch = new FRSAdminApp();
    }

    @Test(groups = {"all_Tests", "critical_Tests"}, priority = 1)
    public void PT1730_Verify_FRS_DiagnoticsTab_Click() throws Exception {
        Thread.sleep(5000);
        frs_service_ui_launch.openDiagnosticsTab();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_DiagnosticTab().equals("Trace File Path:"), "Diagnostic Tab Open", "Tab not clicked");
    }

    @Test(groups = {"all_Tests", "critical_Tests"}, priority = 2)
    public void PT1730_Verify_FRS_DiagnosticsSaveCheckBox() throws Exception {
        FileOperations.delete_SpecificFile(FRS_Log_Directory + FRS_Log_File);
        frs_service_ui_launch.saveCheckBoxChanges();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Running"), "Service Running Successfully", "Service Running Failed");
    }

    @Test(groups = {"all_Tests", "critical_Tests"}, priority = 3)
    public void PT1730_Verify_FRS_ClickDumpTrace() throws Exception {
        frs_service_ui_launch.clickDumpTrace();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getMessageBoxText().equals("Trace information has been successfully saved to K:\\cust1\\Test\\logs"), "Dialog box Popped up", "Dailog box Pop up Failed");
    }

    @Test(groups = {"all_Tests", "critical_Tests"}, priority = 4)
    public void PT1730_Verify_FRS_ClickOk() throws Exception {
        frs_service_ui_launch.clickOk();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Running"), "Service Running Successfully", "Service Running Failed");
        FileOperations.verify_FileFound(FRS_Log_Directory,FRS_Log_File );
    }
    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        frs_service_ui_launch.closeWindow();
    }
}//End of Class
