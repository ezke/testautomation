package imageRpsThickClient.tests.tests_FRS_Whiteseed;

import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRSAdminApp;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;

public class FRS_EditDriveMapping extends BaseTest {
    private FRSAdminApp frs_service_ui_launch;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        FRS.windowsService().start();
        frs_service_ui_launch = new FRSAdminApp();
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 10)
    public void PT1733_Verify_FRS_Click_DriveMappingTab() throws Exception {
        frs_service_ui_launch.openDriveMappingTab();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_DriveMappingsTab().equals("Drive Mappings"), "Drive Mappings Tab Open", "Drive Mappings Tab not clicked");
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 20)
    public void PT1733_Verify_FRS_EditDriveMapping() throws Exception {
        frs_service_ui_launch.editDriveMapping();
        // Due to WhiteSeed method limitation, Validation will be placed later. Keeping the method separate.
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 30)
    public void PT1733_Verify_FRS_ConfirmEditDriveMapping() throws Exception {
        frs_service_ui_launch.confirmEditDriveMapping();
        // Due to WhiteSeed method limitation, Validation will be placed later. Keeping the method separate.
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 40)
    public void PT1733_Verify_FRS_Click_SaveButton() throws Exception {
        frs_service_ui_launch.clickSaveButton();
        BaseUI.verify_true_AndLog(frs_service_ui_launch.getStatus_Service().equals("Service Status: Running"), "Service Started Successfully", "Service Start Failed");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
       frs_service_ui_launch.closeWindow();
    }
}
