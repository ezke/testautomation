package imageRpsThickClient.tests.tests_RCP;

import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;

public class RCP_StartServices {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
    }

    @Test(groups = {"all_Tests", "Regression_Tests"})
    public void PT4928_Verify_CentralSite_Running() throws Exception {
        RCP.rcpCentralWindowsService().start();
        String status = RCP.rcpCentralWindowsService().getState().toString();
        BaseUI.baseStringCompare("RCP Central Site Process Status", "Running", status);
    }

    @Test(groups = {"all_Tests", "Regression_Tests"})
    public void PT4929_Verify_RemoteSite_Running() throws Exception {
        RCP.rcpRemoteWindowsService().start();
        String status = RCP.rcpRemoteWindowsService().getState().toString();
        BaseUI.baseStringCompare("RCP Remote Site Process Status", "Running", status);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
    }
}
