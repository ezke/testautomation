package imageRpsThickClient.tests.tests_RCP;

import imageRPS.data.baseTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.FileOperations;
import wfsCommon.pages.DatePicker;
import imageRpsThickClient.services.RCP;

public class RCP_OXI_LZ_TraceLogExtraction extends baseTest {

    String test_Data2009 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-2009";
    String test_Data2008 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-2008";

    String remote_Input = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\LZ2";
    String central_OutputLZ = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\oxiLZ";
    String central_Logs_Location = "\\\\rps602auto1dcm.qalabs.nwk\\c$\\WausauFS\\RemoteCapturePush\\output\\logs";
    String fileNameStart = "rcp.trace.1.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        FileOperations.cleanup_PriorFiles(remote_Input);
        FileOperations.cleanup_PriorFiles(central_OutputLZ);
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 1)
    public void PT2009_Verify_Central_TransferTo_OutputLZ() throws Exception {
        RCP.rcpCentralWindowsService().start();
        FileOperations.copyFiles(test_Data2009,
                remote_Input);
        FileOperations.wait_ForFileCount_ToMatch(remote_Input, 9, 80);
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_OutputLZ, 9, 80);
        RCP.verify_FilesMatch_TwoFolders(remote_Input, central_OutputLZ);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 2)
    public void PT2009_Verify_Central_Unzip_Log() throws Exception {
        BaseUI.getTodaysDate_AsString();
        DatePicker datePicker = new DatePicker(null, null, null);
        String todaysDate = "Date = " + datePicker.getTodaysDate_AsString_WithFormat("M/d/yyyy");
        String[] fileExtract = FileOperations.getLatestFileExtract_FromFolder(central_Logs_Location, fileNameStart);
        String[] expectedMessages = {todaysDate, "Extracting 8 IBML Export Package file(s) from zip file", "OXIExportPackageSerializer: ExtractOXIExportPackage - Extracted 8 files Received the following IBML Export Package files from Remote Cust1Test Site:", "Extraction complete.", "Saving stream to C:\\WausauFS\\RemoteCapturePush\\output\\exportLZ1\\10001213.zip.", "TransferFile called for file=10001213.zip; type=512"};
        SoftAssert softAssert = new SoftAssert();
        for (String expectedMessage : expectedMessages) {
            softAssert.assertTrue(containsValue(fileExtract, expectedMessage), "Value of '" + expectedMessage + "' was not found in the log file");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 3)
    public void PT2008_Verify_Central_TransferTo_OutputLZ() throws Exception {
        RCP.setDataAndServices_PT2008();
        RCP.verify_FilesMatch_TwoFolders(remote_Input, central_OutputLZ);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 4)
    public void PT2008_Verify_Central_Unzip_Log() throws Exception {

        DatePicker datePicker = new DatePicker(null, null, null);
        String todaysDate = "Date = " + datePicker.getTodaysDate_AsString_WithFormat("M/d/yyyy");
        String[] fileExtract = FileOperations.getLatestFileExtract_FromFolder(central_Logs_Location, fileNameStart);
        String[] expectedMessages = {todaysDate, "Extracting 8 IBML Export Package file(s) from zip file", "OXIExportPackageSerializer: ExtractOXIExportPackage - Extracted 8 files Received the following IBML Export Package files from Remote Cust1Test Site:", "Extraction complete.", "Saving stream to C:\\WausauFS\\RemoteCapturePush\\output\\exportLZ1\\10001213.zip."};
        SoftAssert softAssert = new SoftAssert();
        for (String expectedMessage : expectedMessages) {
            softAssert.assertTrue(containsValue(fileExtract, expectedMessage), "Value of '" + expectedMessage + "' was not found in the log file");
        }
        softAssert.assertAll();
    }

    public boolean containsValue(String[] arrayToCheck, String textToMatch) {
        for (String value : arrayToCheck) {
            if (value.contains(textToMatch)) {
                return true;
            }
        }
        return false;
    }


    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
        FileOperations.cleanup_PriorFiles(remote_Input);
        FileOperations.cleanup_PriorFiles(central_OutputLZ);
    }
}
