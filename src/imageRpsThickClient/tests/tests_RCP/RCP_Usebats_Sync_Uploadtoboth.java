package imageRpsThickClient.tests.tests_RCP;

import imageRPS.data.baseTest;
import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.w3c.dom.Node;
import utils.FileOperations;
import utils.XMLParser;

public class RCP_Usebats_Sync_Uploadtoboth extends baseTest {
    static XMLParser parser;
    public static String xml_File_Location_Remote = "\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml";
    public static String xml_File_Location_Central = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml";
  //  String remote_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1969\\usebats\\";
    String remote_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1969";


    String remote_input = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    String central_Output_Usebats = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\usebats";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
        RCP.cleanup_RCP_1970_Files();
        parser = new XMLParser(xml_File_Location_Remote);
        Node synchDirectionNode = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(synchDirectionNode, "syncDirection", "Both");
        parser.setAttribute(synchDirectionNode, "minutesBetweenUpdates", "1");
        parser.setAttribute(synchDirectionNode, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
        parser = new XMLParser(xml_File_Location_Central);
        synchDirectionNode = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(synchDirectionNode, "syncDirection", "Both");
        parser.setAttribute(synchDirectionNode, "minutesBetweenUpdates", "1");
        parser.setAttribute(synchDirectionNode, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
        FileOperations.copyFiles(remote_Data,
                remote_input);
        FileOperations.wait_ForFileCount_ToMatch(remote_input, 6, 80);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"})
    public void PT1969_Verify_FileSync_RemoteToCentral() throws Exception {
        RCP.verify_FilesMatch_TwoFolders(remote_Data,remote_input);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_Usebats, 6, 80);
        RCP.verify_FilesMatch_TwoFolders(remote_input,central_Output_Usebats);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        FileOperations
                .deleteFiles_TopFolder(central_Output_Usebats);
        FileOperations
                .deleteFiles_TopFolder(remote_input);
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
    }
}// end of class





