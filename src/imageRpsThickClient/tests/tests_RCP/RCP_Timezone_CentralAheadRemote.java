package imageRpsThickClient.tests.tests_RCP;

import imageRPS.data.ImageRPS_Utilities;
import imageRPS.data.baseTest;
import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.FileOperations;

import java.io.File;

public class RCP_Timezone_CentralAheadRemote extends baseTest {



    String central_Queue_TestData = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-2006\\Remote_469102\\queue";
    String queue_fileToCopy = "46910261.P1";

    String central_p1Data_TestData = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-2006\\Remote_469102\\p1data";
    String p1data_fileToCopy = "46910261.p1";

    String central_p1images_TestData = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-2006\\Remote_469102\\p1images";
    String p1image_fileToCopy = "46910261.OPI";

    String remote_Input_Queue = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\queue";

    String remote_input_p1data = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1data";
    String remote_input_p1images = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1images";

    String central_Output_p1data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data";
    String central_Output_p1images = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images";

    String central_Output_Queue = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue";
    String central_Logs_Location = "\\\\rps602auto1dcm.qalabs.nwk\\c$\\WausauFS\\RemoteCapturePush\\output\\logs";

    String remote_InputQueue_WaitingForEncode = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\queue\\WaitingForEncode";

    String fileNameStart = "rcp.trace.1.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        fileNameStart += BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");

        ImageRPS_Utilities.change_CentralMachine_ToEasternTime();
        ImageRPS_Utilities.change_RemoteMachine_ToCentralTime();
        //Add wait time to see if the test doesn't fail intermittently
        Thread.sleep(30000);
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();

        FileOperations.cleanup_PriorFiles(remote_input_p1data);
        FileOperations.cleanup_PriorFiles(remote_Input_Queue);
        FileOperations.cleanup_PriorFiles(remote_input_p1images);
        FileOperations.cleanup_PriorFiles(central_Output_p1data);
        FileOperations.cleanup_PriorFiles(central_Output_p1images);
        FileOperations.cleanup_PriorFiles(central_Output_Queue);
        FileOperations.cleanup_PriorFiles(remote_InputQueue_WaitingForEncode);
        FileOperations.copyFile(central_Queue_TestData + "\\" + queue_fileToCopy,
                remote_Input_Queue + "\\" + queue_fileToCopy);
        FileOperations.copyFile(central_p1Data_TestData + "\\" + p1data_fileToCopy,
                remote_input_p1data + "\\" + p1data_fileToCopy);
        FileOperations.copyFile(central_p1images_TestData + "\\" + p1image_fileToCopy,
                remote_input_p1images + "\\" + p1image_fileToCopy);

        RCP.rcpCentralWindowsService().makeRunning();
        RCP.rcpRemoteWindowsService().makeRunning();
        // The sleep gives service a chance to move files.
        Thread.sleep(6000);

    }


    @Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
    public void PT2006_Verify_FilesMatch_p1data() throws Exception {
        RCP.verify_Match_For_p1Data(remote_input_p1data, central_Output_p1data);
    }


    @Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
    public void PT2005_Verify_FilesMatch_p1images() throws Exception {
        FileOperations.verify_AllFiles_FromFoldersMatch(remote_input_p1images, central_Output_p1images);
    }


    @Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
    public void PT2006_Verify_FilesMatch_TestData_And_Output_Queues_P1Files() throws Exception {
        FileOperations.verify_FileCount_Correct(central_Output_Queue, 2);
        File expectedfileToCheck = new File(central_Queue_TestData + "\\" + queue_fileToCopy);
        String file1_expectedPath = "k:\\kyris\\test601a\\p1data\\46910261.p1";

        File actualFileToCheck = new File(central_Output_Queue + "\\" + queue_fileToCopy);
        String actual_expectedPath = "C:\\WausauFS\\RemoteCapturePush\\output\\p1data\\46910261.p1";
        String expectedDate = "02/23/15";
        String expectedBatchNumber = "469102";

        // Pass these values in
        Integer expectedSkewTime = -60;

        ImageRPS_Utilities.verify_P1_file_Matches(expectedfileToCheck, file1_expectedPath, actualFileToCheck,
                actual_expectedPath, expectedBatchNumber, expectedSkewTime, expectedDate);
    }


    @Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
    public void PT2006_Verify_remote2_InputQueue_Deleted() throws Exception {
        FileOperations.verify_File_Matches(central_Queue_TestData + "\\" + queue_fileToCopy,
                remote_Input_Queue + "\\WaitingForEncode\\" + queue_fileToCopy);
    }

    @Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
    public void PT2006_Verify_remote1_logs() throws Exception {
        String[] fileExtract = FileOperations.getLatestFileExtract_FromFolder(central_Logs_Location, fileNameStart);
        String expectedSite = "Remote Cust1Test Site";
        Integer expectedClockSkew = -60;
        String dateFormat = "MM/dd/yy HH:mm:ss.SSS";
        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        currentDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,dateFormat,60);
        ImageRPS_Utilities.verify_TC2004_LogFileExtract_Matches_Expected(fileExtract, expectedSite, expectedClockSkew, currentDate);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
        ImageRPS_Utilities.change_RemoteMachine_ToCentralTime();
        ImageRPS_Utilities.change_CentralMachine_ToCentralTime();
        FileOperations.cleanup_PriorFiles(remote_Input_Queue);
        FileOperations.cleanup_PriorFiles(central_Output_Queue);
        FileOperations.cleanup_PriorFiles(remote_InputQueue_WaitingForEncode);
        FileOperations.cleanup_PriorFiles(remote_input_p1data);
        FileOperations.cleanup_PriorFiles(remote_input_p1images);
    }

}// end of class