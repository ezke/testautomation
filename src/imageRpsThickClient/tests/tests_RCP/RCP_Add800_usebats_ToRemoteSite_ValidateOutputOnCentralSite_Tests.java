package imageRpsThickClient.tests.tests_RCP;

import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class RCP_Add800_usebats_ToRemoteSite_ValidateOutputOnCentralSite_Tests {

	String remote2_TestData_UseBats = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\usebats";
	String remote2_Input_UseBats = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\usebats";

	String remote1_Output_UseBats = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\usebats";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.deleteFiles_TopFolder(remote1_Output_UseBats);
	
		FileOperations.copyFiles(remote2_TestData_UseBats, remote2_Input_UseBats);

		RCP.rcpCentralWindowsService().makeRunning();
		RCP.rcpRemoteWindowsService().makeRunning();
		// The sleep gives service a chance to move files.

		FileOperations.wait_ForFileCount_ToMatch(remote1_Output_UseBats, 800, 280);
		Thread.sleep(500);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1975_Verify_OutputMatchesInput() throws Exception {
		FileOperations.verify_AllFiles_FromFoldersMatch(remote2_TestData_UseBats, remote1_Output_UseBats);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1975_Verify_FileCounts() throws Exception {
		FileOperations.verify_FileCount_Correct(remote1_Output_UseBats, 800);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1975_Verify_InputEmpty() throws Exception {
		FileOperations.verify_NoFiles_Remaining(remote2_Input_UseBats);
	}



	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.deleteFiles_TopFolder(remote1_Output_UseBats);
	}

}// end of class
