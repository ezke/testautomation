package imageRpsThickClient.tests.tests_RCP;

import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class RCP_Add400_eip_ToRemoteSite_ValidateOutputOnCentralSite_Tests {

	String remote2_eip_remote = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\eip\\remote";
	String remote2_eip_Input = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\eip";

	String remote2_eip_central = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\eip\\central";
	String remote1_output_eip = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\eip";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.deleteFiles_TopFolder(remote1_output_eip);
		FileOperations.deleteFiles_TopFolder(remote2_eip_Input);

		FileOperations.copyFiles(remote2_eip_remote, remote2_eip_Input);
		FileOperations.copyFiles(remote2_eip_central, remote1_output_eip);

		RCP.rcpCentralWindowsService().start();
		RCP.rcpRemoteWindowsService().start();
		// The sleep gives service a chance to move files.

		FileOperations.wait_ForFileCount_ToMatch(remote1_output_eip, 400, 40);
		FileOperations.wait_ForFileCount_ToMatch(remote2_eip_Input, 400, 40);
		Thread.sleep(500);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1988_Verify_OutputMatchesInput() throws Exception {
		FileOperations.verify_AllFiles_FromFoldersMatch(remote1_output_eip, remote2_eip_Input);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1988_Verify_FileCounts() throws Exception {
		FileOperations.verify_FileCount_Correct(remote1_output_eip, 400);
		FileOperations.verify_FileCount_Correct(remote2_eip_Input, 400);
	}


	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.deleteFiles_TopFolder(remote1_output_eip);
		FileOperations.deleteFiles_TopFolder(remote2_eip_Input);
	}

}// end of class
