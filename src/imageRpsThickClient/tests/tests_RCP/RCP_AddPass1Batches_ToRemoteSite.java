package imageRpsThickClient.tests.tests_RCP;

import imageRPS.data.ImageRPS_Utilities;
import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class RCP_AddPass1Batches_ToRemoteSite {

	String remote1_P1Data_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data";
	String remote1_P1Images_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images";
	String remote1_P1Queue_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue";
	
	String remote2_testData_Queue = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\queue";
	String remote2_InputQueue = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\queue";
	String remote2_TestData_P1Data = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\p1data";
	String remote2_input_P1Data = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1data";
	String remote2_TestData_P1Images = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\p1images";
	String remote2_input_P1Images = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1images";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		RCP.rcpCentralWindowsService().stop();
		RCP.rcpRemoteWindowsService().stop();

		RCP.cleanup_RCP_1966_Files_AndResetServers();
	
		FileOperations.copyFiles(remote2_testData_Queue,
				remote2_InputQueue);

		FileOperations.copyFiles(remote2_TestData_P1Data,
				remote2_input_P1Data);

		FileOperations.copyFiles(remote2_TestData_P1Images,
				remote2_input_P1Images);

		RCP.rcpCentralWindowsService().makeRunning();
		RCP.rcpRemoteWindowsService().makeRunning();
		FileOperations.wait_ForFileCount_ToMatch(remote1_P1Data_Output, 30, 60);
		FileOperations.wait_ForFileCount_ToMatch(remote1_P1Images_Output, 30, 60);
		// The sleep gives service a chance to move files.
		Thread.sleep(1000);

	}

	//Step 5
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1966_Verify_p1Data_Matches() throws Exception {

		RCP.verify_Match_For_p1Data(
				remote2_input_P1Data,
				remote1_P1Data_Output);
	}

	//Step 5
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1966_Verify_P1images_Matches() throws Exception {
		FileOperations.verify_AllFiles_FromFoldersMatch(
				remote2_input_P1Images,
				remote1_P1Images_Output);
	}
	
	//Step 6
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1966_Verify_P1Files_Match() throws Exception {
		String expected_OriginalFilePath = "C:\\wfs\\RCP\\RemoteSite\\Kyris\\test601a\\p1data\\";
		String expected_NewFilePath = "C:\\WausauFS\\RemoteCapturePush\\output\\p1data\\";
		
		ImageRPS_Utilities.verify_P1_Files_Match_Expected(remote2_testData_Queue, remote1_P1Queue_Output, expected_OriginalFilePath, expected_NewFilePath);
	}
	
	
	//Step 7
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1966_Verify_NoFilesIn_InputQueue() throws Exception {
		FileOperations
				.verify_NoFiles_Remaining(remote2_InputQueue);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		FileOperations
				.deleteFiles_TopFolder(remote1_P1Data_Output);
		FileOperations
				.deleteFiles_TopFolder(remote1_P1Images_Output);
		FileOperations
				.deleteFiles_TopFolder(remote1_P1Queue_Output);

		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();
	}

}// end of class
