package imageRpsThickClient.tests.tests_RCP;

import imageRPS.data.baseTest;
import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class RCP_AddPass1Batch_ValidateCleanup extends baseTest {

    String central_P1Data_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data";
    String central_P1Images_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images";
    String central_P1Queue_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue";

    String remote1_TestData_P1data1962 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1962\\Remote1_Volume\\p1data";
    String remote1_TestData_P1Images1962 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1962\\Remote1_Volume\\p1images";
    String remote1_TestData_Queue1962 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1962\\Remote1_Volume\\queue";

    String remote1_TestData_P1data1963 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1963\\Remote1_Volume\\p1data";
    String remote1_TestData_P1Images1963 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1963\\Remote1_Volume\\p1images";
    String remote1_TestData_Queue1963 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1963\\Remote1_Volume\\queue";

    String remote1_TestData_P1data1964 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1964\\Remote1_Volume\\p1data";
    String remote1_TestData_P1Images1964 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1964\\Remote1_Volume\\p1images";
    String remote1_TestData_Queue1964 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1964\\Remote1_Volume\\queue";

    String remote1_TestData_P1data1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote1_Volume\\p1data";
    String remote1_TestData_P1Images1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote1_Volume\\p1images";
    String remote1_TestData_Queue1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote1_Volume\\queue";
    String remote2_TestData_P1data1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote2_Volume\\p1data";
    String remote2_TestData_P1Images1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote2_Volume\\p1images";
    String remote2_TestData_Queue1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote2_Volume\\queue";
    String remote3_TestData_P1data1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote3_Volume\\p1data";
    String remote3_TestData_P1Images1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote3_Volume\\p1images";
    String remote3_TestData_Queue1983 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1983\\Remote3_Volume\\queue";

    String remote1_TestData_P1data1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\p1data\\46910161.p1";
    String remote1_TestData_P1Images1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\p1images\\46910161.OPI";
    String remote1_TestData_Queue1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\queue\\46910161.p1";
    String remote2_TestData_P1data1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\p1data\\46910261.p1";
    String remote2_TestData_P1Images1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\p1images\\46910261.OPI";
    String remote2_TestData_Queue1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\queue\\46910261.p1";
    String remote3_TestData_P1data1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\p1data\\46910361.p1";
    String remote3_TestData_P1Images1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\p1images\\46910361.OPI";
    String remote3_TestData_Queue1967 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1967\\Remote1_Volume\\queue\\46910361.p1";

    String remote1_TestData_P1data1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote1_Volume\\p1data";
    String remote1_TestData_P1Images1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote1_Volume\\p1images";
    String remote1_TestData_Queue1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote1_Volume\\queue";
    String remote2_TestData_P1data1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote2_Volume\\p1data";
    String remote2_TestData_P1Images1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote2_Volume\\p1images";
    String remote2_TestData_Queue1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote2_Volume\\queue";
    String remote3_TestData_P1data1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote3_Volume\\p1data";
    String remote3_TestData_P1Images1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote3_Volume\\p1images";
    String remote3_TestData_Queue1968 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1968\\Remote3_Volume\\queue";

    String remote1_input_P1Data = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data";
    String remote1_input_P1Images = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images";
    String remote1_input_P1Queue = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue";
    String remote2_input_P1Data = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data";
    String remote2_input_P1Images = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images";
    String remote2_input_P1Queue = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue";
    String remote3_input_P1Data = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data";
    String remote3_input_P1Images = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images";
    String remote3_input_P1Queue = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue";

    String remote1_input_P1Data1967 = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data\\46910161.p1";
    String remote1_input_P1Images1967 = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images\\46910161.OPI";
    String remote1_input_P1Queue1967 = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue\\46910161.p1";
    String remote2_input_P1Data1967 = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data\\46910261.p1";
    String remote2_input_P1Images1967 = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images\\46910261.OPI";
    String remote2_input_P1Queue1967 = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue\\46910261.p1";
    String remote3_input_P1Data1967 = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data\\46910361.p1";
    String remote3_input_P1Images1967 = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images\\46910361.OPI";
    String remote3_input_P1Queue1967 = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue\\46910361.p1";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 1)
    public void PT1962_Verify_Central_Transfer_Data() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
        FileOperations.copyFiles(remote1_TestData_Queue1962,
                remote1_input_P1Queue);
        FileOperations.copyFiles(remote1_TestData_P1data1962,
                remote1_input_P1Data);
        FileOperations.copyFiles(remote1_TestData_P1Images1962,
                remote1_input_P1Images);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_P1Data_Output, 10, 80);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Images_Output, 10, 80);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Queue_Output, 11, 80);
        // The sleep gives service a chance to move files - needs a large amount of time for cleanup process to occur.
        RCP.verify_P1Data_FileMatch_By_Extension(remote1_TestData_P1data1962, central_P1Data_Output, "p1");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 2)
    public void PT1962_Verify_Central_Transfer_Images() throws Exception {
        FileOperations.verify_AllFiles_FromFoldersMatch(
                remote1_TestData_P1Images1962,
                central_P1Images_Output);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 3)
    public void PT1962_Verify_Central_Transfer_Queue() throws Exception {
        RCP.verify_P1Data_FileMatch_By_Extension(remote1_TestData_Queue1962, central_P1Queue_Output, "P1");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 4)
    public void PT1962_Verify_Remote_Cleared_Data() throws Exception {
        FileOperations.wait_ForFileCount_ToMatch(remote1_input_P1Data, 0, 2000);
        FileOperations
                .verify_NoFiles_Remaining(remote1_input_P1Data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 5)
    public void PT1962_Verify_Remote_Cleared_Images() throws Exception {
        FileOperations.wait_ForFileCount_ToMatch(remote1_input_P1Images, 0, 2000);
        FileOperations
                .verify_NoFiles_Remaining(remote1_input_P1Images);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 7)
    public void PT1962_Verify_Remote_Cleared_Queue() throws Exception {
        FileOperations.wait_ForFileCount_ToMatch(remote1_input_P1Queue, 3, 2000);
        FileOperations
                .verify_NoFiles_Remaining(remote1_input_P1Queue);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 8)
    public void PT1983_Verify_30_PassOneBatch_FileCount() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
        FileOperations.copyFiles(remote1_TestData_Queue1983,
                remote1_input_P1Queue);
        FileOperations.copyFiles(remote1_TestData_P1data1983,
                remote1_input_P1Data);
        FileOperations.copyFiles(remote1_TestData_P1Images1983,
                remote1_input_P1Images);
        FileOperations.copyFiles(remote2_TestData_Queue1983,
                remote2_input_P1Queue);
        FileOperations.copyFiles(remote2_TestData_P1data1983,
                remote2_input_P1Data);
        FileOperations.copyFiles(remote2_TestData_P1Images1983,
                remote2_input_P1Images);
        FileOperations.copyFiles(remote3_TestData_Queue1983,
                remote3_input_P1Queue);
        FileOperations.copyFiles(remote3_TestData_P1data1983,
                remote3_input_P1Data);
        FileOperations.copyFiles(remote3_TestData_P1Images1983,
                remote3_input_P1Images);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.wait_ForFileCount_ToMatch(central_P1Data_Output, 30, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Images_Output, 30, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Queue_Output, 31, 240);
        FileOperations.verify_FileCount_Correct(central_P1Data_Output, 30);
        FileOperations.verify_FileCount_Correct(central_P1Images_Output, 30);
        FileOperations.verify_FileCount_Correct(central_P1Queue_Output, 31);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 9)
    public void PT1983_Verify_30_PassOneBatch_FileMatch() throws Exception {
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_Queue1983, central_P1Queue_Output);
        RCP.verify_FilesMatch_TwoFolders(remote2_TestData_Queue1983, central_P1Queue_Output);
        RCP.verify_FilesMatch_TwoFolders(remote3_TestData_Queue1983, central_P1Queue_Output);
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_P1data1983, central_P1Data_Output);
        RCP.verify_FilesMatch_TwoFolders(remote2_TestData_P1data1983, central_P1Data_Output);
        RCP.verify_FilesMatch_TwoFolders(remote3_TestData_P1data1983, central_P1Data_Output);
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_P1Images1983, central_P1Images_Output);
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_P1Images1983, central_P1Images_Output);
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_P1Images1983, central_P1Images_Output);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 10)
    public void PT1968_Verify_30_PassOneBatch_FileCount() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.copyFiles(remote1_TestData_Queue1968,
                remote1_input_P1Queue);
        FileOperations.copyFiles(remote1_TestData_P1data1968,
                remote1_input_P1Data);
        FileOperations.copyFiles(remote1_TestData_P1Images1968,
                remote1_input_P1Images);
        FileOperations.copyFiles(remote2_TestData_Queue1968,
                remote2_input_P1Queue);
        FileOperations.copyFiles(remote2_TestData_P1data1968,
                remote2_input_P1Data);
        FileOperations.copyFiles(remote2_TestData_P1Images1968,
                remote2_input_P1Images);
        FileOperations.copyFiles(remote3_TestData_Queue1968,
                remote3_input_P1Queue);
        FileOperations.copyFiles(remote3_TestData_P1data1968,
                remote3_input_P1Data);
        FileOperations.copyFiles(remote3_TestData_P1Images1968,
                remote3_input_P1Images);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Data_Output, 30, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Images_Output, 30, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Queue_Output, 31, 240);
        FileOperations.verify_FileCount_Correct(central_P1Data_Output, 30);
        FileOperations.verify_FileCount_Correct(central_P1Images_Output, 30);
        FileOperations.verify_FileCount_Correct(central_P1Queue_Output, 31);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 11)
    public void PT1968_Verify_30_PassOneBatch_FileMatch() throws Exception {
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_Queue1968, central_P1Queue_Output);
        RCP.verify_FilesMatch_TwoFolders(remote2_TestData_Queue1968, central_P1Queue_Output);
        RCP.verify_FilesMatch_TwoFolders(remote3_TestData_Queue1968, central_P1Queue_Output);
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_P1data1968, central_P1Data_Output);
        RCP.verify_FilesMatch_TwoFolders(remote2_TestData_P1data1968, central_P1Data_Output);
        RCP.verify_FilesMatch_TwoFolders(remote3_TestData_P1data1968, central_P1Data_Output);
        RCP.verify_FilesMatch_TwoFolders(remote1_TestData_P1Images1968, central_P1Images_Output);
        RCP.verify_FilesMatch_TwoFolders(remote2_TestData_P1Images1968, central_P1Images_Output);
        RCP.verify_FilesMatch_TwoFolders(remote3_TestData_P1Images1968, central_P1Images_Output);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 12)
    public void PT1967_Verify_AllRemote_Single_PassOneBatch_FileCount() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.copyFile(remote1_TestData_Queue1967,
                remote1_input_P1Queue1967);
        FileOperations.copyFile(remote1_TestData_P1data1967,
                remote1_input_P1Data1967);
        FileOperations.copyFile(remote1_TestData_P1Images1967,
                remote1_input_P1Images1967);
        FileOperations.copyFile(remote2_TestData_Queue1967,
                remote2_input_P1Queue1967);
        FileOperations.copyFile(remote2_TestData_P1data1967,
                remote2_input_P1Data1967);
        FileOperations.copyFile(remote2_TestData_P1Images1967,
                remote2_input_P1Images1967);
        FileOperations.copyFile(remote3_TestData_Queue1967,
                remote3_input_P1Queue1967);
        FileOperations.copyFile(remote3_TestData_P1data1967,
                remote3_input_P1Data1967);
        FileOperations.copyFile(remote3_TestData_P1Images1967,
                remote3_input_P1Images1967);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Data_Output, 3, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Images_Output, 3, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Queue_Output, 4, 240);
        FileOperations.verify_FileCount_Correct(central_P1Data_Output, 3);
        FileOperations.verify_FileCount_Correct(central_P1Images_Output, 3);
        FileOperations.verify_FileCount_Correct(central_P1Queue_Output, 4);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 13)
    public void PT1967_Verify_AllRemote_Single_PassOneBatch_FileMatch() throws Exception {
        FileOperations.verify_FileFound(central_P1Queue_Output, "46910161.p1");
        FileOperations.verify_FileFound(central_P1Queue_Output, "46910261.p1");
        FileOperations.verify_FileFound(central_P1Queue_Output, "46910361.p1");
        FileOperations.verify_FileFound(central_P1Data_Output, "46910161.p1");
        FileOperations.verify_FileFound(central_P1Data_Output, "46910261.p1");
        FileOperations.verify_FileFound(central_P1Data_Output, "46910361.p1");
        FileOperations.verify_FileFound(central_P1Images_Output, "46910161.OPI");
        FileOperations.verify_FileFound(central_P1Images_Output, "46910261.OPI");
        FileOperations.verify_FileFound(central_P1Images_Output, "46910361.OPI");

    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 14)
    public void PT1963_Verify_Central_Transfer_Data_Win8() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
        FileOperations.copyFiles(remote1_TestData_Queue1963,
                remote3_input_P1Queue);
        FileOperations.copyFiles(remote1_TestData_P1data1963,
                remote3_input_P1Data);
        FileOperations.copyFiles(remote1_TestData_P1Images1963,
                remote3_input_P1Images);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.wait_ForFileCount_ToMatch(central_P1Data_Output, 10, 80);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Images_Output, 10, 80);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Queue_Output, 11, 80);
        RCP.verify_P1Data_FileMatch_By_Extension(remote1_TestData_P1data1963, central_P1Data_Output, "p1");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 15)
    public void PT1963_Verify_Central_Transfer_Images_Win8() throws Exception {
        FileOperations.verify_AllFiles_FromFoldersMatch(
                remote1_TestData_P1Images1963,
                central_P1Images_Output);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 16)
    public void PT1963_Verify_Central_Transfer_Queue_Win8() throws Exception {
        RCP.verify_P1Data_FileMatch_By_Extension(remote1_TestData_Queue1963, central_P1Queue_Output, "P1");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 17)
    public void PT1964_Verify_Central_Transfer_Data_Win7() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
        FileOperations.copyFiles(remote1_TestData_Queue1964,
                remote2_input_P1Queue);
        FileOperations.copyFiles(remote1_TestData_P1data1964,
                remote2_input_P1Data);
        FileOperations.copyFiles(remote1_TestData_P1Images1964,
                remote2_input_P1Images);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        FileOperations.wait_ForFileCount_ToMatch(central_P1Data_Output, 10, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Images_Output, 10, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_P1Queue_Output, 11, 240);
        RCP.verify_P1Data_FileMatch_By_Extension(remote1_TestData_P1data1964, central_P1Data_Output, "p1");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 18)
    public void PT1964_Verify_Central_Transfer_Images_Win7() throws Exception {
        FileOperations.verify_AllFiles_FromFoldersMatch(
                remote1_TestData_P1Images1964,
                central_P1Images_Output);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 19)
    public void PT1964_Verify_Central_Transfer_Queue_Win7() throws Exception {
        RCP.verify_P1Data_FileMatch_By_Extension(remote1_TestData_Queue1964, central_P1Queue_Output, "P1");
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        RCP.cleanup_RCP_1966_Files_AndResetServers();
    }
}// end of class





