package imageRpsThickClient.tests.tests_RCP;

import imageRpsThickClient.services.RCP;
import imageRPS.data.baseTest;

import imageRpsThickClient.services.WORS;
import org.testng.annotations.*;
import utils.BaseUI;
import utils.FileOperations;

import java.io.File;

public class RCP_Add_EIP_UploadBoth extends baseTest {
    public static String xml_File_Location_Remote = "\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml";
    public static String xml_File_Location_Remote2 = "\\\\qaautotest_81c.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml";
    public static String xml_File_Location_Remote3 = "\\\\qaautotest_7k.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Rcp.RemoteSite.Settings.xml";
    public static String xml_File_Location_Central = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml";
    String remote_A_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1976\\eip\\200A";
    String remote_D_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1976\\eip\\200D";

    String remote1_A_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200A";
    String remote2_B_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200B";
    String remote3_C_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200C";
    String central_D_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200D";

    String remote1_input1976 = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip";
    String remote1_input1977 = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip";
    String remote1_input1978 = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip";
    String central_Output_EIP = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\eip";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        RCP.set_XML_ConfigFile(xml_File_Location_Remote );
        RCP.set_XML_ConfigFile(xml_File_Location_Remote2);
        RCP.set_XML_ConfigFile(xml_File_Location_Remote3);
        RCP.set_XML_ConfigFile(xml_File_Location_Central);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 1)
    public void PT1976_Verify_EIP_RemoteToCentral_Server2016() throws Exception {
        clearDataAndResetServices();
        //load data
        FileOperations.copyFiles(remote_D_Data,
                remote1_input1976);
        FileOperations.copyFiles(remote_A_Data,
                remote1_input1976);
        //ensure data seeded
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1976, 400, 300);
        //verify file transfer
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_EIP, 400, 300);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1976, central_Output_EIP);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 2)
    public void PT1976_Verify_EIP_RemoteToCentral_Server2016_FileTypes() throws Exception {
        File[] file_Location_Files = FileOperations.return_NonHiddenFiles(new File(central_Output_EIP));
        int filetypes = RCP.getFileType_Count(file_Location_Files);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Central folder", "5 File types were not found in Central folder");
     }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 3)
    public void PT1977_Verify_EIP_RemoteToCentral_ServerWin7() throws Exception {
        clearDataAndResetServices();
        //load data
        FileOperations.copyFiles(remote_D_Data,
                remote1_input1977);
        FileOperations.copyFiles(remote_A_Data,
                remote1_input1977);
        //ensure data seeded
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1977, 400, 240);
        //verify file transfer
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_EIP, 400, 240);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1977, central_Output_EIP);

    }
    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 4)
    public void PT1977_Verify_EIP_RemoteToCentral_ServerWin7_FileTypes() throws Exception {
        File[] file_Location_Files = FileOperations.return_NonHiddenFiles(new File(central_Output_EIP));
        int filetypes = RCP.getFileType_Count(file_Location_Files);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Central folder", "5 File types were not found in Central folder");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 5)
    public void PT1978_Verify_EIP_RemoteToCentral_Win8() throws Exception {
        clearDataAndResetServices();
        //load data
        FileOperations.copyFiles(remote_D_Data,
                remote1_input1978);
        FileOperations.copyFiles(remote_A_Data,
                remote1_input1978);
        //ensure data seeded
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1978, 400, 240);
        //Verify file transfer
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_EIP, 400, 240);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1978, central_Output_EIP);

    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 6)
    public void PT1978_Verify_EIP_RemoteToCentral_Win8_FileTypes() throws Exception {
        File[] file_Location_Files = FileOperations.return_NonHiddenFiles(new File(central_Output_EIP));
        int filetypes = RCP.getFileType_Count(file_Location_Files);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Central folder", "5 File types were not found in Central folder");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 7)
    public void PT1979_Verify_EIP_UploadDownload800() throws Exception {
        clearDataAndResetServices();
        //load data
        FileOperations.copyFiles(remote1_A_Data,
                remote1_input1976);
        FileOperations.copyFiles(remote2_B_Data,
                remote1_input1977);
        FileOperations.copyFiles(remote3_C_Data,
                remote1_input1978);
        FileOperations.copyFiles(central_D_Data,
                central_Output_EIP);
        //ensure data seeded
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1976, 200, 240);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1977, 200, 240);
        FileOperations.wait_ForFileCount_ToMatch(remote3_C_Data, 200, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_D_Data, 200, 240);
        //start services
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        //verify file transfer
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1976, 800, 240);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1977, 800, 240);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1978, 800, 240);
        FileOperations.wait_ForFileCount_ToMatch(central_Output_EIP, 800, 240);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1976, central_Output_EIP);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1977, central_Output_EIP);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1978, central_Output_EIP);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 8)
    public void PT1979_Verify_EIP_UploadDownload800_5FileTypes() throws Exception {
        File[] file_Location_Files_Central = FileOperations.return_NonHiddenFiles(new File(central_Output_EIP));
        File[] file_Location_Files_Remote1 = FileOperations.return_NonHiddenFiles(new File(remote1_input1976));
        File[] file_Location_Files_Remote2 = FileOperations.return_NonHiddenFiles(new File(remote1_input1977));
        File[] file_Location_Files_Remote3 = FileOperations.return_NonHiddenFiles(new File(remote1_input1978));
        int filetypes = RCP.getFileType_Count(file_Location_Files_Central);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Central folder", "5 File types were not found in Central folder");
        filetypes = RCP.getFileType_Count(file_Location_Files_Remote1);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Remote1 folder", "5 File types were not found in Remote1 folder");
        filetypes = RCP.getFileType_Count(file_Location_Files_Remote2);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Remote2 folder", "5 File types were not found in Remote2 folder");
        filetypes = RCP.getFileType_Count(file_Location_Files_Remote3);
        BaseUI.verify_true_AndLog(filetypes == 5, "5 File types were found in Remote3 folder", "5 File types were not found in Remote3 folder");

    }

    public void clearDataAndResetServices() throws Exception {
        RCP.rcpRemoteWindowsService().makeStopped();
        RCP.rcpRemoteWindowsServiceWin7().makeStopped();
        RCP.rcpRemoteWindowsServiceWin8().makeStopped();
        RCP.rcpCentralWindowsService().makeStopped();
        RCP.cleanData_Add400_EIP();
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        clearDataAndResetServices();
    }

}// end of class





