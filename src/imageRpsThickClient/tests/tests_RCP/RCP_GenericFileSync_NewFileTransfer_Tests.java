package imageRpsThickClient.tests.tests_RCP;

import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class RCP_GenericFileSync_NewFileTransfer_Tests {

	String testDataLocation = "\\\\rps602auto1sql.qalabs.nwk\\c$\\Wausaufs\\RemoteCapturePush\\TestData\\GenericFileType";

	String remote2_Input_afpocr = "\\\\rps602auto1sql.qalabs.nwk\\c$\\Wausaufs\\RemoteCapturePush\\Input\\afpocr";
	String remote2_Input_mavro = "\\\\rps602auto1sql.qalabs.nwk\\c$\\Wausaufs\\RemoteCapturePush\\Input\\mavro";
	

	String remote1_Output_mavro = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\mavro";
	String remote1_Output_afpocr = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\afpocr";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.cleanup_PriorFiles(remote2_Input_mavro);
		FileOperations.cleanup_PriorFiles(remote2_Input_afpocr);
		FileOperations.cleanup_PriorFiles(remote1_Output_afpocr);
		FileOperations.cleanup_PriorFiles(remote1_Output_mavro);

		FileOperations.copyFiles(testDataLocation, remote2_Input_afpocr);
		FileOperations.copyFiles(testDataLocation, remote1_Output_mavro);

		RCP.rcpCentralWindowsService().makeRunning();
		RCP.rcpRemoteWindowsService().makeRunning();
		// The sleep gives service a chance to move files.

		FileOperations.wait_ForFileCount_ToMatch(remote2_Input_mavro, 3, 40);
		FileOperations.wait_ForFileCount_ToMatch(remote1_Output_afpocr, 3, 40);
		Thread.sleep(500);

	}

	//Step 4
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2013_Verify_FilesMatch_remote1OutputAfpocr_And_Remote2InputAfpocr() throws Exception {
		FileOperations.verify_AllFiles_FromFoldersMatch(testDataLocation, remote1_Output_afpocr);
		FileOperations.verify_AllFiles_FromFoldersMatch(remote1_Output_afpocr, remote2_Input_mavro);
		FileOperations.verify_FileCount_Correct(remote1_Output_afpocr, 3);
	}

	//Step 5
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2013_Verify_FilesMatch_remote1OutputMavro_And_Remote2InputMavro() throws Exception {
		FileOperations.verify_AllFiles_FromFoldersMatch(testDataLocation, remote1_Output_mavro);
		FileOperations.verify_AllFiles_FromFoldersMatch(remote1_Output_mavro, remote2_Input_afpocr);
		FileOperations.verify_FileCount_Correct(remote1_Output_mavro, 3);
	}


	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.cleanup_PriorFiles(remote2_Input_mavro);
		FileOperations.cleanup_PriorFiles(remote2_Input_afpocr);
		FileOperations.cleanup_PriorFiles(remote1_Output_afpocr);
		FileOperations.cleanup_PriorFiles(remote1_Output_mavro);
	}

}// end of class
