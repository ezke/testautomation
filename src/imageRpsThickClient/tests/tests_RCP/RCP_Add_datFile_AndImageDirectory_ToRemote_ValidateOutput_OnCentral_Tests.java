package imageRpsThickClient.tests.tests_RCP;

import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

import java.io.File;

public class RCP_Add_datFile_AndImageDirectory_ToRemote_ValidateOutput_OnCentral_Tests {

	String remote2_TestData_Archive = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\TestData\\archive";
	Integer expectedFileAndDirectoryCount = 66;
	Integer datFileCount = 3;
	String remote2_Input_Archive = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\archive";

	String remote1_Output_Archive = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\archive";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.cleanup_PriorFiles(remote2_Input_Archive);
		FileOperations.cleanup_PriorFiles(remote1_Output_Archive);

		FileOperations.copyFiles(remote2_TestData_Archive, remote2_Input_Archive);

		RCP.rcpCentralWindowsService().makeRunning();
		RCP.rcpRemoteWindowsService().makeRunning();
		// The sleep gives service a chance to move files.

		FileOperations.wait_ForFileCount_ToMatch(remote2_Input_Archive, datFileCount, 40);
		FileOperations.wait_ForFileCount_ToMatch(remote1_Output_Archive, expectedFileAndDirectoryCount, 40);

	}

	// Typo of "transfered" is confirmed as being how it's supposed to be.
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1990_Verify_InputFileNames_EndIn_Transfered() throws Exception {

		FileOperations.verify_FileNames_EndsWithExtension(remote2_Input_Archive, "transfered");
		FileOperations.verify_FileCount_Correct(remote2_Input_Archive, 3);
	}
	

	// Typo of "transfered" is confirmed as being how it's supposed to be.
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1990_Verify_FilesMatch_IgnoreExtension() throws Exception {
		File[] listOf_datFiles = FileOperations.return_Files_ThatEndInExtension(remote2_TestData_Archive, "dat");

		FileOperations.verify_AllFiles_FromFolder_MatchesExpectedFilesAndFileName_WithoutExtension(listOf_datFiles,
				remote2_Input_Archive);
	}
				
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT1990_Verify_OutputMatchesInput() throws Exception {
		FileOperations.verify_Files_CopiedCorrectly(remote2_TestData_Archive, remote1_Output_Archive,
				expectedFileAndDirectoryCount);

	}


	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.cleanup_PriorFiles(remote2_Input_Archive);
		FileOperations.cleanup_PriorFiles(remote1_Output_Archive);
	}

}// end of class
