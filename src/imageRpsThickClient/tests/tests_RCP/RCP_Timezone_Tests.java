package imageRpsThickClient.tests.tests_RCP;

import imageRPS.data.ImageRPS_Utilities;
import imageRpsThickClient.services.RCP;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.FileOperations;

import java.io.File;

public class RCP_Timezone_Tests {

	String remote2_queue_TestData = "\\\\rps602auto1sql.qalabs.nwk\\c$\\Wausaufs\\RemoteCapturePush\\TestData\\queue";
	String queue_fileToCopy = "46910061.P1";

	String remote2_p1Data_TestData = "\\\\rps602auto1sql.qalabs.nwk\\c$\\Wausaufs\\RemoteCapturePush\\TestData\\p1data";
	String p1data_fileToCopy = "46910061.p1";

	String remote2_p1images_TestData = "\\\\rps602auto1sql.qalabs.nwk\\c$\\Wausaufs\\RemoteCapturePush\\TestData\\p1images";
	String p1image_fileToCopy = "46910061.OPI";

	String remote2_Input_Queue = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\queue";

	String remote2_input_p1data = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1data";
	String remote2_input_p1images = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1images";

	String remote1_output_p1data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data";
	String remote1_output_p1images = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images";

	String remote1_Output_Queue = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue";
	String remote1_Logs_Location = "\\\\rps602auto1dcm.qalabs.nwk\\c$\\WausauFS\\RemoteCapturePush\\output\\logs";

	String remote2_InputQueue_WaitingForEncode = "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\queue\\WaitingForEncode";

	String fileNameStart = "rcp.trace.1.";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		fileNameStart += BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");
		ImageRPS_Utilities.change_RemoteMachine_ToPacificTime();
		ImageRPS_Utilities.change_CentralMachine_ToCentralTime();
		//Add wait time to see if the test doesn't fail intermittently
		Thread.sleep(30000);
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		FileOperations.cleanup_PriorFiles(remote2_Input_Queue);
		FileOperations.cleanup_PriorFiles(remote2_input_p1data);
		FileOperations.cleanup_PriorFiles(remote2_input_p1images);
		FileOperations.cleanup_PriorFiles(remote1_output_p1data);
		FileOperations.cleanup_PriorFiles(remote1_output_p1images);
		FileOperations.cleanup_PriorFiles(remote1_Output_Queue);
		FileOperations.cleanup_PriorFiles(remote2_InputQueue_WaitingForEncode);
		FileOperations.copyFile(remote2_queue_TestData + "\\" + queue_fileToCopy,
				remote2_Input_Queue + "\\" + queue_fileToCopy);
		FileOperations.copyFile(remote2_p1Data_TestData + "\\" + p1data_fileToCopy,
				remote2_input_p1data + "\\" + p1data_fileToCopy);
		FileOperations.copyFile(remote2_p1images_TestData + "\\" + p1image_fileToCopy,
				remote2_input_p1images + "\\" + p1image_fileToCopy);

		RCP.rcpCentralWindowsService().makeRunning();
		RCP.rcpRemoteWindowsService().makeRunning();
		// The sleep gives service a chance to move files.

		// FileOperations.wait_ForFileCount_ToMatch(remote2_Input_afpocr, 3, 40);
		// FileOperations.wait_ForFileCount_ToMatch(remote1_Output_afpocr, 3, 40);
		Thread.sleep(5000);

	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2004_Verify_FilesMatch_p1data() throws Exception {
		// FileOperations.verify_AllFiles_FromFoldersMatch(remote2_input_p1data,
		// remote1_output_p1data);

		RCP.verify_Match_For_p1Data(remote2_input_p1data, remote1_output_p1data);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2004_Verify_FilesMatch_p1images() throws Exception {
		FileOperations.verify_AllFiles_FromFoldersMatch(remote2_input_p1images, remote1_output_p1images);
	}

	// Step 7 and Step 10
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2004_Verify_FilesMatch_TestData_And_Output_Queues_P1Files() throws Exception {
		FileOperations.verify_FileCount_Correct(remote1_Output_Queue, 2);
		File expectedfileToCheck = new File(remote2_queue_TestData + "\\" + queue_fileToCopy);
		String file1_expectedPath = "C:\\wfs\\RCP\\RemoteSite\\Kyris\\test601a\\p1data\\46910061.p1";

		File actualFileToCheck = new File(remote1_Output_Queue + "\\" + queue_fileToCopy);
		String actual_expectedPath = "C:\\WausauFS\\RemoteCapturePush\\output\\p1data\\46910061.p1";
		String expectedDate = "02/23/15";
		String expectedBatch = "469100";

		// Pass these values in
		Integer expectedSkewTime = -119;

		ImageRPS_Utilities.verify_P1_file_Matches(expectedfileToCheck, file1_expectedPath, actualFileToCheck,
				actual_expectedPath, expectedBatch, expectedSkewTime, expectedDate);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2004_Verify_remote2_InputQueue_Deleted() throws Exception {
		FileOperations.verify_File_Matches(remote2_queue_TestData + "\\" + queue_fileToCopy,
				remote2_Input_Queue + "\\WaitingForEncode\\" + queue_fileToCopy);
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT2004_Verify_remote1_logs() throws Exception {
		String[] fileExtract = FileOperations.getLatestFileExtract_FromFolder(remote1_Logs_Location, fileNameStart);
		String expectedSite = "Remote Cust1Test Site";
		Integer expectedClockSkew = -120;

		String dateFormat = "MM/dd/yy HH:mm:ss.SSS";
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
	//	currentDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,dateFormat,expectedClockSkew);
		ImageRPS_Utilities.verify_TC2004_LogFileExtract_Matches_Expected(fileExtract, expectedSite, expectedClockSkew, currentDate);
	}

//	// Step 10
//	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
//	public void PT2004_Verify_remote1_OutputQueue_P1_MatchesExpected() throws Exception {
//		File fileToCheck = new File(remote1_Output_Queue + "\\" + queue_fileToCopy);
//		String expectedPath = "C:\\WausauFS\\RemoteCapturePush\\output\\p1data\\46910061.p1";
//		ImageRPS_Utilities.verify_P1_file_Matches(expectedfileToCheck, file1_expectedPath, actualFileToCheck, file2_expectedPath, expectedBatch, expectedSkewTime, expectedDate);
//	//	ImageRPS_Utilities.verify_P1_File_Matches_Expected(fileToCheck, expectedPath);
//
//	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		RCP.rcpCentralWindowsService().makeStopped();
		RCP.rcpRemoteWindowsService().makeStopped();

		ImageRPS_Utilities.change_RemoteMachine_ToCentralTime();

		FileOperations.cleanup_PriorFiles(remote2_Input_Queue);
		FileOperations.cleanup_PriorFiles(remote2_input_p1data);
		FileOperations.cleanup_PriorFiles(remote2_input_p1images);
		FileOperations.cleanup_PriorFiles(remote1_output_p1data);
		FileOperations.cleanup_PriorFiles(remote1_output_p1images);
		FileOperations.cleanup_PriorFiles(remote1_Output_Queue);
		FileOperations.cleanup_PriorFiles(remote2_InputQueue_WaitingForEncode);
	}

}// end of class
