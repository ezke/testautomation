package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.WFSEventCollector;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.winiumPages.*;
import org.openqa.selenium.Keys;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCS_Events_BatchFileConversionStarted_And_CompletedSuccessfully extends BaseTestWin {

    private String eventIDType;
    private String firstDateRange, finalDateRange;
    private ViewBatchesByBatchID_Page batchQueryFilterPage;
    private MainMenu_Page mainMenu_Page;

    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    private String p1DataFileToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1data\\30078101.dat";
    private  String p1ImageFileToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1images\\30078101.img";
    private String p1ImageFileWithSnpToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1images\\30078101.snp";

    private String p1DataFile_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\30078101.dat";
    private String p1ImagesFile_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\30078101.img";
    private String p1ImagesFileWithSnpExt_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\30078101.snp";
    private String p1DataFileCopiedInDestinationFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\OPEX\\30078101.dat";
    private String p1DataFileCopiedInP1DataFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\07810162.P1";
    private String p1DataFileCopiedInP1ImagesFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\07810162.OPI";
    private String p1DataFileCopiedFromP1ImagesFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\07810162.p1g";
    //private String p1DataFileCopiedFromUsedBatsFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\usedbats\\078101";
    private String p1DataFile = "30078101.dat";
    private String stationID = "62";
    TableData eventsResultData;
    private String eventSeq;
    private String eventResultData_SevInfo = "3-Information";

    private ViewEvents_Page viewEvents_page;
    private EventsResult_Page eventsResult_page;
    private String dateFormat = "MM/dd/yy HH:mm:ss";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {
        WFSEventCollector.windowsService().stop();
        BCS.windowsService().stop();
        WFSEventCollector.windowsService().start();

        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder);
        FileOperations.cleanup_PriorFile(p1DataFile_CopyDestination);
        FileOperations.cleanup_PriorFile(p1ImagesFile_CopyDestination);
        FileOperations.cleanup_PriorFile(p1ImagesFileWithSnpExt_CopyDestination);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInP1DataFolder);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInP1ImagesFolder);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedFromP1ImagesFolder);

        FileOperations.copyFile(p1DataFileToCopy, p1DataFile_CopyDestination);
        FileOperations.copyFile(p1ImageFileToCopy, p1ImagesFile_CopyDestination);
        FileOperations.copyFile(p1ImageFileWithSnpToCopy, p1ImagesFileWithSnpExt_CopyDestination);

        
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        viewEvents_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();
        BCS.windowsService().start();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT353_Step3_Verify_StartWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT353_Step6_Verify_BCS_EventOccuredDT_DateRange() throws Exception {

        String dateFormat = "MM/dd/yy HH:mm:ss";
        String eventID = "903104";

        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -3);
        finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        viewEvents_page.enterText_EventIDLowerRange(eventID);
        viewEvents_page.enterText_EventIDHigherRange(eventID);

        eventsResult_page = viewEvents_page.navigate_EventsResult();
        TableData eventsResultData = eventsResult_page.get_EventIDAndEventOccuredDT();

        String eventDate = eventsResultData.data.get(0).get("EventOccuredDT");
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDate, dateFormat);
    }

    @Test(priority = 25, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step6_Verify_BCS_EventData1_MatchP1DataFileCopied() throws Exception {
        eventsResultData = eventsResult_page.get_EventSeqAndData1();

        String eventData1 = eventsResultData.data.get(0).get("Data 1");
        BaseUI.baseStringCompare("p1Data file copied displayed on Event result page", p1DataFile, eventData1);
    }

    @Test(priority = 30, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step6_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartedStatus() throws Exception {
        eventSeq = eventsResultData.data.get(0).get("EventSeq");
        eventIDType = eventsResultData.data.get(0).get("EventID");
        EventsResult_Page.clickEnterKey_NavigateToEventID_WithFileConversionStartedPage();
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_StartedStatus_TextBox",
                "OE.BatchConversion.ConvertFile.Start");
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_EventSeq() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_SevInfo_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_EventSeq_TextBox",
                "     " + eventSeq.replace(",", ""));
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionSevInfo() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_SevInfo_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_SevInfo_TextBox",
                eventResultData_SevInfo);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionProcessStatus() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_ProcessTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_ProcessTextBox",
                "OE.BatchConversion.ConvertFile");
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_Data1() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data1_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data1_TextBox",
                p1DataFile);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_Data2() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data2_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data2_TextBox",
                stationID);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_EventIDType() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_TypeTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_TypeTextBox", eventIDType);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_EventStartedMessage() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_MessageTextBox",
                "Batch file conversion started for batch file: "+p1DataFile+" - Station Id: 62");
    }
    
    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_EventOccuredDate() throws Exception {
        EventsResult_Page.verifyEventOccuredDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_EventLoggedDate() throws Exception {
        EventsResult_Page.verifyEventLoggedDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 40, groups = {"regression_Tests"}, dependsOnMethods = {"PT353_Step6_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT353_Step7_Verify_ClickCancelButton() throws Exception {
        EventsResult_Page.clickCancelButton();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
    }

    @Test(priority = 50, groups = {"regression_Tests"})
    public void PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1() throws Exception {

        String eventID = "903105";

        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        String firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -3);
        String finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        mainMenu_Page = viewEvents_page.navigate_MainPage();
        viewEvents_page = mainMenu_Page.navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();

        //FileOperations.wait_ForFile_WithExtension_ToNOTExist(".dat", p1DataFileCopiedInDestinationFolder, 90);

        viewEvents_page.enterText_EventIDLowerRange(eventID);

        eventsResult_page = viewEvents_page.navigate_EventsResult();
        TableData eventsResultData = eventsResult_page.get_EventIDAndEventOccuredDT();

        String eventDate = eventsResultData.data.get(0).get("EventOccuredDT");
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDate, dateFormat);
    }

    @Test(priority = 55, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step6To7_Verify_BCS_EventData1_MatchP1DataFileCopied() throws Exception {
        eventsResultData = eventsResult_page.get_EventSeqAndData1();

        String eventData1 = eventsResultData.data.get(0).get("Data 1");
        BaseUI.baseStringCompare("p1Data file copied displayed on Event result page", 
                p1DataFile.substring(3).replace(".dat", ""), eventData1);
    }

    @Test(priority = 60, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step6To7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_SuccessStatus() throws Exception {
        eventSeq = eventsResultData.data.get(0).get("EventSeq");
        eventIDType = eventsResultData.data.get(0).get("EventID");
        EventsResult_Page.clickEnterKey_NavigateToEventID_WithFileConversionSuccessPage();
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_SuccessStatus_TextBox",
                "OE.BatchConversion.ConvertFile.Success");
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_EventSeq() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_EventSeq_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_EventSeq_TextBox",
                "     " + eventSeq.replace(",", ""));
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_SevInfo() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_SevInfo_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_SevInfo_TextBox",
                eventResultData_SevInfo);
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_ProcessStatus() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_ProcessTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_ProcessTextBox",
                "OE.BatchConversion.ConvertFile");
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_Data1() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data1_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data1_TextBox",
                p1DataFile.substring(3).replace(".dat", ""));
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_Data2() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data2_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data2_TextBox",
                p1DataFile);
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_EventIDType() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_TypeTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_TypeTextBox", eventIDType);
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_EventSuccessfulMessage() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_MessageTextBox",
                "Batch file conversion completed successfully - batch: "
                        +p1DataFile.replaceAll(".dat", "").substring(3)+" file: "+ p1DataFile +" - Station Id: ");
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_EventOccuredDate() throws Exception {
        EventsResult_Page.verifyEventOccuredDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Verify_BCS_EventID_EventDataResultsPage_FileConversionSuccess_EventLoggedDate() throws Exception {
        EventsResult_Page.verifyEventLoggedDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT354_Step6To7_Verify_BCS_EventSuccessfulOccuredDT_DateRange1"})
    public void PT354_Step8_Verify_ClickCancelButton_EventResultsPageDoesNotDisplay() throws Exception {
        EventsResult_Page.clickCancelButton();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            BCS.windowsService().stop();
            WORS.windowsService().stop();
            mainMenu_Page = viewEvents_page.navigate_MainPage();

            //Delete the p1Data and p1Images file from p1Data, p1Images and Usedbats folder
            batchQueryFilterPage = mainMenu_Page.navigate_DataMaintenanceMenu()
                    .navigate_BatchMenu()
                    .navigate_ViewBatchesByBatchID();
            batchQueryFilterPage.deleteCopiedFilesFromAllFolders(p1DataFile);
            batchQueryFilterPage.exitApplication();
        } finally {
            batchQueryFilterPage.exitApplication();
        }
    }
}
