package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.WFSEventCollector;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.winiumPages.EventsResult_Page;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.ViewEvents_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class BCS_Events extends BaseTestWin {

    private ViewEvents_Page viewEvents_page;
    private EventsResult_Page eventsResult_page;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WFSEventCollector.windowsService().start();
        BCS.windowsService().stop();

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        viewEvents_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();
        BCS.windowsService().start();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT352_Verify_BCS_EventOccuredDT_DateRange() throws Exception {

        String dateFormat = "MM/dd/yy HH:mm:ss";
        String eventID = "903103";

        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        String firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -3);
        String finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        viewEvents_page.enterText_EventIDLowerRange(eventID);
        viewEvents_page.enterText_EventIDHigherRange(eventID);

        eventsResult_page = viewEvents_page.navigate_EventsResult();
        TableData eventsResultData = eventsResult_page.get_EventIDAndEventOccuredDT();

        String eventDate = eventsResultData.data.get(0).get("EventOccuredDT");
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDate, dateFormat);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {

        WORS.windowsService().stop();
        eventsResult_page.exitApplication();
    }
}
