package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.BCS_Winium.BCSAdminApp;
import imageRpsThickClient.services.BCS_Winium.BaseTestWin_BCS;
import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCS_AdminApp_Tests extends BaseTestWin_BCS{

    private BCSAdminApp bcsAdminApp = new BCSAdminApp();
    @NotNull
    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);

    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT340_Precondition_Verify_WindowServiceStop() throws WindowsServiceException {
        windowsService().stop();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT340_Verify_StartWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().start();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT341_Verify_StopWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().stop();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
    }

    @Test(priority = 40, groups = {"regression_Tests"})
    public void PT344_Verify_BCS_AdminApp_Start() throws Exception {
        bcsAdminApp.startService();

        bcsAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 50, groups = {"regression_Tests"})
    public void PT345_Verify_BCS_AdminApp_Stop() throws Exception {
        bcsAdminApp.stopService();

        bcsAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Stopped"));
        windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        bcsAdminApp.closeWindow();
    }
}
