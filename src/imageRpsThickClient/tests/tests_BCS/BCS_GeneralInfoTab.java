package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.BCS_Winium.BCSAdminApp;
import imageRpsThickClient.services.BCS_Winium.BaseTestWin_BCS;
import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.Is;
import utils.ResultWriter;
import utils.WindowsService;

public class BCS_GeneralInfoTab extends BaseTestWin_BCS {

    private BCSAdminApp bcsAdminApp = new BCSAdminApp();
    @NotNull
    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        windowsService().stop();
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT371_Verify_GeneralInfo_Tab_DontUpdate() throws Exception {

        String updateAppDirectoryValue = "K:\\cust1\\Test\\service\\BCS1";
        bcsAdminApp.startService();
        bcsAdminApp.enter_AppDirectory(updateAppDirectoryValue);
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        bcsAdminApp.closeWindow();
    }
}
