package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.BCS_Winium.BCSAdminApp;
import imageRpsThickClient.services.BCS_Winium.BaseTestWin_BCS;
import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCS_StartTracing extends BaseTestWin_BCS{

    private BCSAdminApp bcsAdminApp = new BCSAdminApp();

    @NotNull
    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        windowsService().stop();
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);

    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT347_Verify_BCS_AdminApp_Start() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        String traceFilePath = bcsAdminApp.return_TraceFilePath();
        bcsAdminApp.check_TraceDirectlyToFile_CheckBox();
        bcsAdminApp.startService();
        BaseUI.assertThat("TraceFilePath is Equal to", "K:\\cust1\\Test\\logs", Is.equalTo(traceFilePath));
        bcsAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT347_Verify_BCS_Logs_Found() throws Exception {

        String remote2_Logs_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\logs";
        String batconvTestFile = "batconvTEST-62.log";
        String batconvsFile = "batconvs62.log";
        String ImageRPSSqlFile = "ImageRPSSql62.log";
        String RPSSql62File = "RPSSql62.log";
        String batconvmainFile = "BATCONVMAIN.LOG";
        String batchConversionServicdFile = "BatchConversionService.log";
        String batconverrsFile = "batconverrs.log";

        FileOperations.verify_FileFound(remote2_Logs_Folder, batconvTestFile);
        FileOperations.verify_FileFound(remote2_Logs_Folder, batconvsFile);
        FileOperations.verify_FileFound(remote2_Logs_Folder, ImageRPSSqlFile);
        FileOperations.verify_FileFound(remote2_Logs_Folder, RPSSql62File);
        FileOperations.verify_FileFound(remote2_Logs_Folder, batconvmainFile);
        FileOperations.verify_FileFound(remote2_Logs_Folder, batchConversionServicdFile);
        FileOperations.verify_FileFound(remote2_Logs_Folder, batconverrsFile);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        bcsAdminApp.closeWindow();
        windowsService().stop();
    }
}
