package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.services.BCS_Winium.BCSAdminApp;
import imageRpsThickClient.services.BCS_Winium.BaseTestWin_BCS;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCS_EditConfiguration_Tests extends BaseTestWin_BCS {

    private BCSAdminApp bcsAdminApp = new BCSAdminApp();
    private TableData tableColumns;
    private String siteName_Original;
    private String appDirectory_Original;
    private String updateSiteNameValue = "Cust1-Test1";
    private String updateAppDirectoryValue = "K:\\cust1\\Test\\service\\BCS1";
    private String stationID = "63";
    private String traceFilePath;
    private String traceQueueSize;
    private String updateTraceFilePathValue = "K:\\cust1\\Test\\logs1";
    private String updateTraceQueueSizeValue = "200001";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
        siteName_Original = bcsAdminApp.return_SiteName();
        appDirectory_Original = bcsAdminApp.return_ApplicationDirectory();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT343_Verify_GeneralInfo_Tab_Update() throws Exception {

        bcsAdminApp.enter_SiteName(updateSiteNameValue);
        bcsAdminApp.enter_AppDirectory(updateAppDirectoryValue);
        bcsAdminApp.click_SaveChanges_Button();
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT343_Verify_GeneralInfo_Tab_Updated_Value() throws Exception {

        String updated_SiteName = bcsAdminApp.return_SiteName();
        BaseUI.baseStringCompare("SiteName", updateSiteNameValue, updated_SiteName);
        BaseUI.assertThat("AppDirectory is not Equal to UpdatedValue", updateAppDirectoryValue, Is.not(Is.equalTo(appDirectory_Original)));
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT343_Verify_GeneralInfo_Tab_Update_OriginalValue() throws Exception {

        bcsAdminApp.enter_SiteName(siteName_Original);
        bcsAdminApp.enter_AppDirectory(appDirectory_Original);
        bcsAdminApp.click_SaveChanges_Button();
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 40, groups = {"regression_Tests"})
    public void PT343_Verify_GeneralInfo_Tab_Updated_OriginalValue() throws Exception {

        siteName_Original = bcsAdminApp.return_SiteName();
        appDirectory_Original = bcsAdminApp.return_ApplicationDirectory();
        BaseUI.assertThat("SiteName is not Equal to UpdatedValue", updateSiteNameValue, Is.not(Is.equalTo(siteName_Original)));
        BaseUI.assertThat("AppDirectory is not Equal to UpdatedValue", updateAppDirectoryValue, Is.not(Is.equalTo(appDirectory_Original)));
    }

    @Test(priority = 50, groups = {"regression_Tests"})
    public void PT343_Verify_Station_Tab_AddStation() throws Exception {

        String appServerFile = "K:\\cust1\\Test\\cmds\\dcm.ini";

        bcsAdminApp.click_StationsTab();
        bcsAdminApp.click_AddStation_Button();
        bcsAdminApp.enter_StationID(stationID);
        bcsAdminApp.enter_AppServerFile(appServerFile);
        bcsAdminApp.click_AddSationDialogSave_Button();
        bcsAdminApp.click_SaveChanges_Button();
        tableColumns = bcsAdminApp.tableExtractor_ByCell( new String[]{"Station ID", "App Server Ini File"});
        BaseUI.assertThat("TableColumns", tableColumns.data, Is.nonEmptyCollection());
        BaseUI.baseStringCompare("StationsID Column","63", tableColumns.dataRow(1).get("Station ID"));
    }

    @Test(priority = 60, groups = {"regression_Tests"})
    public void PT343_Verify_Station_Tab_RemoveStation() throws Exception {

        bcsAdminApp.click_StationsTab();
        int columnToClick = tableColumns.return_RowIndex_BasedOn_1MatchingFields("Station ID", stationID);
        bcsAdminApp.click_ColumnRow(columnToClick);
        bcsAdminApp.click_RemoveStation_Button();
        bcsAdminApp.click_SaveChanges_Button();
        tableColumns = bcsAdminApp.tableExtractor_ByCell( new String[]{"Station ID", "App Server Ini File"});
        BaseUI.assertThat("TableColumns", tableColumns.data, Is.nonEmptyCollection());
        tableColumns.verify_Value_NOT_InColumn("Station ID", stationID);
    }

    @Test(priority = 70, groups = {"regression_Tests"})
    public void PT343_Verify_Diagnostics_Tab_Update() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        traceFilePath = bcsAdminApp.return_TraceFilePath();
        traceQueueSize = bcsAdminApp.return_TraceQueueSize();
        bcsAdminApp.enter_TraceFilePath(updateTraceFilePathValue);
        bcsAdminApp.enter_TraceQueueSize(updateTraceQueueSizeValue);
        bcsAdminApp.unCheck_TraceDirectlyToFile_CheckBox();
        bcsAdminApp.click_SaveChanges_Button();
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 80, groups = {"regression_Tests"})
    public void PT343_Verify_Diagnostics_Tab_UpdatedValue() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        String updatedTraceFilePath = bcsAdminApp.return_TraceFilePath();
        String updatedTraceQueueSize = bcsAdminApp.return_TraceQueueSize();
        BaseUI.assertThat("TraceFilePath is not equal to UpdatedValue", updatedTraceFilePath, Is.not(Is.equalTo(traceFilePath)));
        BaseUI.assertThat("TraceQueueSize is not equal to UpdatedValue", updatedTraceQueueSize, Is.not(Is.equalTo(traceQueueSize)));
        bcsAdminApp.verify_TraceDirectlyToFile_UnChecked();
    }

    @Test(priority = 90, groups = {"regression_Tests"})
    public void PT343_Verify_Diagnostics_Tab_OriginalValue() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        bcsAdminApp.enter_TraceFilePath(traceFilePath);
        bcsAdminApp.enter_TraceQueueSize(traceQueueSize);
        bcsAdminApp.check_TraceDirectlyToFile_CheckBox();
        bcsAdminApp.click_SaveChanges_Button();
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 100, groups = {"regression_Tests"})
    public void PT343_Verify_Diagnostics_Tab_Updated_OriginalValue() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        traceFilePath = bcsAdminApp.return_TraceFilePath();
        traceQueueSize = bcsAdminApp.return_TraceQueueSize();
        BaseUI.assertThat("TraceFilePath is not equal to UpdatedValue", updateTraceFilePathValue, Is.not(Is.equalTo(traceFilePath)));
        BaseUI.assertThat("TraceQueueSize is not equal to UpdatedValue", updateTraceQueueSizeValue, Is.not(Is.equalTo(traceQueueSize)));
        bcsAdminApp.verify_TraceDirectlyToFile_Checked();
    }

    @Test(priority = 110, groups = {"regression_Tests"})
    public void PT343_Verify_TestConfiguration_Link() throws Exception {

        bcsAdminApp.click_TestConfigurationLink();
        String [] testingConfigurationText = bcsAdminApp.return_TestConfigurationModal_Text();
        bcsAdminApp.click_TestingConfig_CloseButton();
        BaseUI.baseStringCompare("Checking Config File", "Checking Configuration File", testingConfigurationText[0].trim());
        BaseUI.baseStringCompare("Testing SystemPath", "Testing System Paths", testingConfigurationText[1].trim());
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        bcsAdminApp.closeWindow();
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        bcsAdminApp.closeWindow();
    }
}
