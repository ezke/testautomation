package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.services.BCS_Winium.BCSAdminApp;
import imageRpsThickClient.services.BCS_Winium.BaseTestWin_BCS;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCS_ViewConfiguration_Tests extends BaseTestWin_BCS{

    private BCSAdminApp bcsAdminApp = new BCSAdminApp();
    private TableData getColumnValue;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT342_Verify_StationID_Row() throws Exception {

        bcsAdminApp.click_StationsTab();
        getColumnValue = bcsAdminApp.tableExtractor_ByCell( new String[]{"Station ID", "App Server Ini File"});
        BaseUI.baseStringCompare("StationsID Column","62", getColumnValue.dataRow(0).get("Station ID"));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT342_Verify_AppServerIniFile_Row() throws Exception {

        String expectedText = "K:\\cust1\\Test\\cmds\\dcm.ini";
        BaseUI.baseStringCompare("App Server Ini File Column",expectedText, getColumnValue.dataRow(0).get("App Server Ini File"));
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT342_Verify_Diagnostics_MSMQName() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        String msmqName = bcsAdminApp.return_MSMQName();
        String expectedText ="FormatName:DIRECT=OS:rps602auto1dcm.qalabs.nwk\\private$\\cust1test";
        BaseUI.baseStringCompare("MSMQ Name",expectedText, msmqName);
    }

    @Test(priority = 40, groups = {"regression_Tests"})
    public void PT342_Verify_Diagnostics_TraceFilePath() throws Exception {

        String traceFilePath = bcsAdminApp.return_TraceFilePath();
        String expectedText ="K:\\cust1\\Test\\logs";
        BaseUI.baseStringCompare("TraceFilePath",expectedText, traceFilePath);
    }

    @Test(priority = 50, groups = {"regression_Tests"})
    public void PT342_Verify_Diagnostics_TraceQueueSize() throws Exception {

        String traceQueueSize = bcsAdminApp.return_TraceQueueSize();
        String expectedText ="200000";
        BaseUI.baseStringCompare("TraceQueueSize",expectedText, traceQueueSize);
    }

    @Test(priority = 60, groups = {"regression_Tests"})
    public void PT342_Verify_Diagnostics_TraceDirectlyToFile() throws Exception {

        bcsAdminApp.verify_TraceDirectlyToFile_Checked();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        bcsAdminApp.closeWindow();
    }
}
