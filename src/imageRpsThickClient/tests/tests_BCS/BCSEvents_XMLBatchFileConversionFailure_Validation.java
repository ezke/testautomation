package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.WFSEventCollector;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.winiumPages.EventsResult_Page;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.ViewEvents_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCSEvents_XMLBatchFileConversionFailure_Validation extends BaseTestWin {

    private String eventIDType;
    private String firstDateRange, finalDateRange;

    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    private String p1DataFileToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1data\\00078103.XML";
    private String p1ImageFileToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1images\\00078103.img";

    private String p1DataFile_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\00078103.XML";
    private String p1ImagesFile_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\00078103.img";
    private String p1DataFileCopiedInDestination_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\00078103.wxl";
    private String p1DataFileCopiedInDestination_CopyDestination_p1_ErrExtension = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\07810362.p1-err";
    private String p1DataFileCopiedInDestinationFolder_ERRExtension = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\OPEX\\00078103.ERR";
    private String p1DataFileCopiedInDestinationFolder_DatExtension = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\OPEX\\00078103.dat";
    private String p1DataFileCopiedInDestinationFolder_WkfExtension = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\OPEX\\00078103.wkf";
    private String p1DataFileCopiedInDestinationFolder_WxlExtension = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\OPEX\\00078103.wxl";
    private String p1DataFile = "00078103.XML";
    private String stationID = "62";
    TableData eventsResultData;
    private String eventSeq;
    private String eventResultData_SevInfo = "1-Error";

    private ViewEvents_Page viewEvents_page;
    private EventsResult_Page eventsResult_page;
    private String dateFormat = "MM/dd/yy HH:mm:ss";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        WFSEventCollector.windowsService().stop();
        BCS.windowsService().stop();
        WFSEventCollector.windowsService().start();

        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_DatExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_ERRExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_WkfExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_WxlExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestination_CopyDestination);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestination_CopyDestination_p1_ErrExtension);
        FileOperations.cleanup_PriorFile(p1DataFile_CopyDestination);
        FileOperations.cleanup_PriorFile(p1ImagesFile_CopyDestination);

        FileOperations.copyFile(p1DataFileToCopy, p1DataFile_CopyDestination);
        FileOperations.copyFile(p1ImageFileToCopy, p1ImagesFile_CopyDestination);


        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        viewEvents_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();
        BCS.windowsService().start();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT357_Step3_Verify_StartWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT357_Step1_Verify_BCS_EventOccuredDT_DateRange() throws Exception {

        String eventID = "903003";

        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -3);
        finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        viewEvents_page.enterText_EventIDLowerRange(eventID);

        eventsResult_page = viewEvents_page.navigate_EventsResult();
        TableData eventsResultData = eventsResult_page.get_EventIDAndEventOccuredDT();

        String eventDate = eventsResultData.data.get(0).get("EventOccuredDT");
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDate, dateFormat);
    }

    @Test(priority = 25, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step1_Verify_BCS_EventData1_MatchP1DataFileCopied() throws Exception {
        eventsResultData = eventsResult_page.get_EventSeqAndData1();

        String eventData1 = eventsResultData.data.get(0).get("Data 1");
        BaseUI.baseStringCompare("Data file did not copy on Event result page",
                p1DataFile.replaceAll(".XML", "").substring(3), eventData1);
    }

    @Test(priority = 30, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step1_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_Status() throws Exception {
        eventSeq = eventsResultData.data.get(0).get("EventSeq");
        eventIDType = eventsResultData.data.get(0).get("EventID");
        EventsResult_Page.clickEnterKey_NavigateToEventID_WithXMLFileConversionFailureInstancePage();
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_XMLBatchConversionFailure_TextBox",
                "OE.Opexmain.ConvertFile.XMLFailure");
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_EventSeq() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_EventSeq_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_EventSeq_TextBox",
                "     " + eventSeq.replace(",", ""));
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_SevInfo() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_BatchConversionFailure_SevInfo_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_BatchConversionFailure_SevInfo_TextBox",
                eventResultData_SevInfo);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_ProcessStatus() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_BatchConversionFailure_ProcessTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_BatchConversionFailure_ProcessTextBox",
                "OE.BatchConversion.ConvertFile");
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_Data1() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data1_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data1_TextBox",
                p1DataFile.replaceAll(".XML", "").substring(3));
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversion_Data2() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data2_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data2_TextBox",
                p1DataFile.replaceAll(".XML", ".dat"));
    }


    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_EventIDType() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_TypeTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_TypeTextBox", eventIDType);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_EventStartedMessage() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
        String formattedP1DataFile = p1DataFile.replaceAll(".XML", "").substring(3);
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_MessageTextBox",
                "XML Batch file conversion failure for batch: " + formattedP1DataFile +" file: "+ p1DataFile.replaceAll(".XML", ".dat")+" - Station Id: ");
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_EventOccuredDate() throws Exception {
        EventsResult_Page.verifyEventOccuredDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 35, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_BCS_EventID_EventDataResultsPage_FileConversionStartInstance_EventLoggedDate() throws Exception {
        EventsResult_Page.verifyEventLoggedDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 40, groups = {"regression_Tests"}, dependsOnMethods = {"PT357_Step1_Verify_BCS_EventOccuredDT_DateRange"})
    public void PT357_Step7_Verify_ClickCancelButton_EventDataResultsPage_FileConversionStartInstanceWindowClosed() throws Exception {
        EventsResult_Page.clickCancelButton();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        BCS.windowsService().stop();
        WORS.windowsService().stop();
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_DatExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_ERRExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_WkfExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder_WxlExtension);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestination_CopyDestination);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestination_CopyDestination_p1_ErrExtension);
        FileOperations.cleanup_PriorFile(p1DataFile_CopyDestination);
        FileOperations.cleanup_PriorFile(p1ImagesFile_CopyDestination);
        eventsResult_page.exitApplication();
    }
}
