package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.BCS_Winium.BCSAdminApp;
import imageRpsThickClient.services.BCS_Winium.BaseTestWin_BCS;
import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCS_EditDiagnosticTab extends BaseTestWin_BCS {

    private BCSAdminApp bcsAdminApp = new BCSAdminApp();
    @NotNull
    private WindowsService windowsService() {
        return BCS.windowsService();
    }
    private String updateTraceFilePathValue = "K:\\cust1\\Test\\logs1";
    private String updateTraceQueueSizeValue = "200001";
    private String traceFilePath = "K:\\cust1\\Test\\logs";
    private String traceQueueSize = "200000";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        windowsService().stop();
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT370_Verify_Diagnostics_Tab_Update() throws Exception {

        bcsAdminApp.startService();
        bcsAdminApp.click_DiagnosticsTab();
        traceFilePath = bcsAdminApp.return_TraceFilePath();
        traceQueueSize = bcsAdminApp.return_TraceQueueSize();
        bcsAdminApp.enter_TraceFilePath(updateTraceFilePathValue);
        bcsAdminApp.enter_TraceQueueSize(updateTraceQueueSizeValue);
        bcsAdminApp.unCheck_TraceDirectlyToFile_CheckBox();
        bcsAdminApp.click_SaveChanges_Button();
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT343_Verify_BCS_ServiceRunning() throws Exception {

        bcsAdminApp.click_StationsTab();
        bcsAdminApp.wait_ForServiceText_TobeUpdated("Service Status: Running");
        bcsAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT343_Verify_Diagnostics_Tab_OriginalValue() throws Exception {

        bcsAdminApp.click_DiagnosticsTab();
        bcsAdminApp.enter_TraceFilePath(traceFilePath);
        bcsAdminApp.enter_TraceQueueSize(traceQueueSize);
        bcsAdminApp.check_TraceDirectlyToFile_CheckBox();
        bcsAdminApp.click_SaveChanges_Button();
        bcsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 40, groups = {"regression_Tests"})
    public void PT343_Verify_Diagnostics_Tab_Updated_OriginalValue() throws Exception {

        bcsAdminApp.click_StationsTab();
        bcsAdminApp.click_DiagnosticsTab();
        traceFilePath = bcsAdminApp.return_TraceFilePath();
        traceQueueSize = bcsAdminApp.return_TraceQueueSize();
        BaseUI.assertThat("TraceFilePath is not equal to UpdatedValue", updateTraceFilePathValue, Is.not(Is.equalTo(traceFilePath)));
        BaseUI.assertThat("TraceQueueSize is not equal to UpdatedValue", updateTraceQueueSizeValue, Is.not(Is.equalTo(traceQueueSize)));
        bcsAdminApp.verify_TraceDirectlyToFile_Checked();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        bcsAdminApp.closeWindow();
    }

    }
