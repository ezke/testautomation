package imageRpsThickClient.tests.tests_BCS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.WFSEventCollector;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class BCSEvents_BatchFileConversionWithCompressGrayChecked_GrayImageFileDoesNotExists extends BaseTestWin {

    private String eventIDType;
    private String firstDateRange, finalDateRange;
    private String applicationID = "opexc";
    private String tagName = "CompressGray";
    private String eventResultData_SevInfo = "2-Warning";
    private ViewBatchesByBatchID_Page batchQueryFilterPage;
    private MainMenu_Page mainMenu_Page;

    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    private String p1DataFileToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1data\\30078101.dat";
    private String p1ImageFileToCopy = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\wfs\\ST-Automation-TestBatches-Cust1\\p1images\\30078101.img";

    private String p1DataFile_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\30078101.dat";
    private String p1ImagesFile_CopyDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\30078101.img";
    private String p1DataFileCopiedInDestinationFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\OPEX\\30078101.dat";
    private String p1DataFileCopiedInP1DataFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\07810162.P1";
    private String p1ImagesFileCopiedInP1ImagesFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\07810162.OPI";
    private String p1DataFileCopiedFromP1ImagesFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\07810162.p1g";
    private String p1DataFile = "30078101.dat";
    private String stationID = "62";
    TableData eventsResultData;
    private String eventSeq;

    private ViewEvents_Page viewEvents_page;
    private CFGValueSetUp_Page cfgValueSetUpPage;
    private EventsResult_Page eventsResult_page;
    private String dateFormat = "MM/dd/yy HH:mm:ss";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        WFSEventCollector.windowsService().stop();
        BCS.windowsService().stop();
        WFSEventCollector.windowsService().start();

        //Delete the files copied to P1Data and P1Images folder.
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInDestinationFolder);
        FileOperations.cleanup_PriorFile(p1DataFile_CopyDestination);
        FileOperations.cleanup_PriorFile(p1ImagesFile_CopyDestination);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedInP1DataFolder);
        FileOperations.cleanup_PriorFile(p1ImagesFileCopiedInP1ImagesFolder);
        FileOperations.cleanup_PriorFile(p1DataFileCopiedFromP1ImagesFolder);

        //Set the Compress Gray value to 1, so 903009 event is not logged when gray image is copied
        //to p1Images folder.
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        cfgValueSetUpPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ConfigurationMenu()
                .navigate_SystemSetup()
                .navigate_SystemSettings()
                .navigate_CFGValueSetUp();

        cfgValueSetUpPage.selectAllValues_FromCfgValueSetUpPage(applicationID, stationID, tagName);
        mainMenu_Page = cfgValueSetUpPage.navigate_MainPage();

        //Copy the files
        FileOperations.copyFile(p1DataFileToCopy, p1DataFile_CopyDestination);
        FileOperations.copyFile(p1ImageFileToCopy, p1ImagesFile_CopyDestination);

        BCS.windowsService().start();

        viewEvents_page = mainMenu_Page.navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT349_Step1_Verify_StartWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange() throws Exception {
        String eventID = "903105";

        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -3);
        finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        viewEvents_page.enterText_EventIDLowerRange(eventID);
        viewEvents_page.enterText_EventIDHigherRange(eventID);

        eventsResult_page = viewEvents_page.navigate_EventsResult();
        TableData eventsResultData = eventsResult_page.get_EventIDAndEventOccuredDT();

        String eventDate = eventsResultData.data.get(0).get("EventOccuredDT");
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDate, dateFormat);
    }

    @Test(priority = 55, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step1_Verify_BCS_EventData1_MatchP1DataFileCopied() throws Exception {
        eventsResultData = eventsResult_page.get_EventSeqAndData1();

        String eventData1 = eventsResultData.data.get(0).get("Data 1");
        BaseUI.baseStringCompare("p1Data file copied displayed on Event result page",
                p1DataFile.substring(3).replace(".dat", ""), eventData1);
    }

    @Test(priority = 60, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step1_Verify_BCS_EventID_EventDataResultsPage_FileConversion_SuccessStatus() throws Exception {
        eventSeq = eventsResultData.data.get(0).get("EventSeq");
        eventIDType = eventsResultData.data.get(0).get("EventID");
        EventsResult_Page.clickEnterKey_NavigateToEventID_WithFileConversionSuccessPage();
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_SuccessStatus_TextBox",
                "OE.BatchConversion.ConvertFile.Success");
    }

    @Test(priority = 65, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step1_Verify_ClickCancelButton_EventResultsPageDoesNotDisplay() throws Exception {
        EventsResult_Page.clickCancelButton();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
    }

    @Test(priority = 66, groups = {"regression_Tests"})
    public void PT349_Step1_Verify_StopWindowsServiceDirectly() throws WindowsServiceException {
        BCS.windowsService().stop();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
    }

    @Test(priority = 70, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step1_Verify_BCS_EventGrayImageExists() throws Exception {

        String eventID = "903009";
        mainMenu_Page = viewEvents_page.navigate_MainPage();

        viewEvents_page = mainMenu_Page.navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();

        viewEvents_page.enterText_EventIDLowerRange(eventID);

        eventsResult_page = viewEvents_page.navigate_EventsResult();
        eventsResultData = eventsResult_page.get_EventIDAndEventOccuredDT();
        BaseUI.verify_true_AndLog(eventsResultData.data.size() > 0, "Events ID 903009 is logged because GRAY IMAGE DOES NOT EXISTS",
                "Event ID 903009 is not logged because GRAY IMAGE FILE EXISTS");
    }

    @Test(priority = 75, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step6_Verify_BCS_EventOccuredDT_DateRange() throws Exception {

        String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
        firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -4);
        finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        String eventDate = eventsResultData.data.get(0).get("EventOccuredDT");
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, eventDate, dateFormat);
    }
    
    @Test(priority = 80, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step6To7_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExistsStatus() throws Exception {
        eventSeq = eventsResultData.data.get(0).get("EventSeq");
        eventIDType = eventsResultData.data.get(0).get("EventID");
        EventsResult_Page.clickEnterKey_NavigateToEventID_WithNoGrayImageInstancePage();
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_NoGrayImageStatus_TextBox",
                "OE.BatchConversion.ConvertFile.NoGray");
    }

    @Test(priority = 86, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_EventSeq() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_EventSeq_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_EventSeq_TextBox",
                "     " + eventSeq.replace(",", ""));
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_SevInfo() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_NoGrayImageStatus_SevInfo_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_NoGrayImageStatus_SevInfo_TextBox",
                eventResultData_SevInfo);
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_ProcessStatus() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_NoGrayImageStatus_ProcessTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_NoGrayImageStatus_ProcessTextBox",
                "OE.BatchConversion.ConvertFile");
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_Data1() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data1_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data1_TextBox",
                p1DataFile.substring(3).replace(".dat", ""));
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_Data2() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_Data2_TextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_Data2_TextBox",
                stationID);
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_EventIDType() {
        BaseUI.verifyElementAppears(Locator.lookupElement("eventData_EventIDResultPage_TypeTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_TypeTextBox", eventIDType);
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_EventSuccessfulMessage() {
        BaseUI.verifyElementAppears(Locator.lookupRequiredElement("eventData_EventIDResultPage_MessageTextBox"));
        BaseUI.verifyElementHasExpectedText("eventData_EventIDResultPage_MessageTextBox",
                "No gray image file found for batch: "
                        +p1DataFile.replaceAll(".dat", "").substring(3)+" - Station Id: "+stationID);
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_EventOccuredDate() throws Exception {
        EventsResult_Page.verifyEventOccuredDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 85, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Verify_BCS_EventID_EventDataResultsPage_NoGrayFileExists_EventLoggedDate() throws Exception {
        EventsResult_Page.verifyEventLoggedDate(firstDateRange, finalDateRange, dateFormat);
    }

    @Test(priority = 90, groups = {"regression_Tests"}, dependsOnMethods = {"PT349_Step1_Verify_BCS_EventSuccessfulOccuredDT_DateRange"})
    public void PT349_Step8_Verify_ClickCancelButton_EventResultsPageDoesNotDisplay() throws Exception {
        EventsResult_Page.clickCancelButton();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("eventData_EventIDResultPage_MessageTextBox"));
    }

    @Test(priority = 95, groups = {"regression_Tests"})
    public void PT349_Step1_SetAndVerify_CFGInstanceValueToNotChecked() throws Exception {
        mainMenu_Page = viewEvents_page.navigate_MainPage();

        cfgValueSetUpPage = mainMenu_Page.navigate_ConfigurationMenu()
                .navigate_SystemSetup()
                .navigate_SystemSettings()
                .navigate_CFGValueSetUp();

        cfgValueSetUpPage.selectAllValues_FromCfgValueSetUpPage_InstanceValueUnchecked(applicationID, stationID, tagName);
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("cfgSetUpValue_CfgTagValue_InstanceValueCheckbox"));
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        try {
            BCS.windowsService().stop();
            WORS.windowsService().stop();
            mainMenu_Page = cfgValueSetUpPage.navigate_MainPage();

            batchQueryFilterPage = mainMenu_Page.navigate_DataMaintenanceMenu()
                    .navigate_BatchMenu()
                    .navigate_ViewBatchesByBatchID();
            batchQueryFilterPage.deleteCopiedFilesFromAllFolders(p1DataFile);
            batchQueryFilterPage.exitApplication();
        } finally {
            batchQueryFilterPage.exitApplication();
        }
    }
}
