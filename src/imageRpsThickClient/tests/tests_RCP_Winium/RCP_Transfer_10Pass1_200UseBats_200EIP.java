package imageRpsThickClient.tests.tests_RCP_Winium;

import imageRPS.data.baseTest;
import imageRpsThickClient.services.RCP;
import imageRpsThickClient.services.RCP_Winium.BaseTestWin_RCP;
import imageRpsThickClient.services.RCP_Winium.RCPAdminApp;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.*;

public class RCP_Transfer_10Pass1_200UseBats_200EIP extends BaseTestWin_RCP {

    public static String xml_File_Location_Central = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml";
    String remote1_TestData_P1data1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote1_Volume\\p1data";
    String remote1_TestData_P1Images1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote1_Volume\\p1images";
    String remote1_TestData_Queue1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote1_Volume\\queue";
    String remote2_TestData_P1data1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote2_Volume\\p1data";
    String remote2_TestData_P1Images1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote2_Volume\\p1images";
    String remote2_TestData_Queue1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote2_Volume\\queue";
    String remote3_TestData_P1data1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote3_Volume\\p1data";
    String remote3_TestData_P1Images1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote3_Volume\\p1images";
    String remote3_TestData_Queue1989 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1989\\Remote3_Volume\\queue";
    String central_P1Data_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data";
    String central_P1Images_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images";
    String central_P1Queue_Output = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue";
    String remote1_input_P1Data = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data";
    String remote1_input_P1Images = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images";
    String remote1_input_P1Queue = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue";
    String remote2_input_P1Data = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data";
    String remote2_input_P1Images = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images";
    String remote2_input_P1Queue = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue";
    String remote3_input_P1Data = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data";
    String remote3_input_P1Images = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images";
    String remote3_input_P1Queue = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue";

    String remote1_InputEIP = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip";
    String remote2_InputEIP = "\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip";
    String remote3_InputEIP = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip";
    String central_Output_EIP = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\eip";

    String remote1_A_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200A";
    String remote2_B_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200B";
    String remote3_C_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200C";
    String central_D_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1979\\eip\\200D";
    String central_R1_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1974\\usebats\\R1";
    String central_R2_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1974\\usebats\\R2";
    String central_R3_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1974\\usebats\\R3";
    String central_Output_Usebats = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\usebats";
  //  String central_Usebats_Input = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\usedbats\\Usebat_Testing";
    String remote_R1_Output = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    String remote_R2_Output = "\\\\qaautotest_7K.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    String remote_R3_Output = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    private imageRpsThickClient.services.RCP_Winium.RCPAdminApp RCPAdminApp = new RCPAdminApp();



    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        //parser = new XMLParser(xml_File_Location_Remote);
        RCP.set_XML_ConfigCentral_1989(xml_File_Location_Central);
        RCP.set_XML_ConfigRemotes_1989();
        RCP.cleanDataAll_ResetServices();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 1)
    public void PT1962_Verify_Central_Transfer_Data_Pass1() throws Exception {

        FileOperations.copyFiles(remote1_TestData_Queue1989,
                remote1_input_P1Queue);
        FileOperations.copyFiles(remote1_TestData_P1data1989,
                remote1_input_P1Data);
        FileOperations.copyFiles(remote1_TestData_P1Images1989,
                remote1_input_P1Images);
        FileOperations.copyFiles(remote2_TestData_Queue1989,
                remote2_input_P1Queue);
        FileOperations.copyFiles(remote2_TestData_P1data1989,
                remote2_input_P1Data);
        FileOperations.copyFiles(remote2_TestData_P1Images1989,
                remote2_input_P1Images);
        FileOperations.copyFiles(remote3_TestData_Queue1989,
                remote3_input_P1Queue);
        FileOperations.copyFiles(remote3_TestData_P1data1989,
                remote3_input_P1Data);
        FileOperations.copyFiles(remote3_TestData_P1Images1989,
                remote3_input_P1Images);
        FileOperations.copyFiles(remote1_A_Data,
                remote1_InputEIP);
        FileOperations.copyFiles(remote2_B_Data,
                remote2_InputEIP);
        FileOperations.copyFiles(remote3_C_Data,
                remote3_InputEIP);
        FileOperations.copyFiles(central_D_Data,
                central_Output_EIP);
        FileOperations.copyFiles(central_R1_Data,
                central_Output_Usebats);
        FileOperations.copyFiles(central_R2_Data,
                central_Output_Usebats);
        FileOperations.copyFiles(central_R3_Data,
                central_Output_Usebats);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.wait_ForFileCount_ToMatch(remote1_InputEIP, 800, 620);
        FileOperations.wait_ForFileCount_ToMatch(remote2_InputEIP, 800, 620);
        FileOperations.wait_ForFileCount_ToMatch(remote3_InputEIP, 800, 620);
        FileOperations.wait_ForFileCount_ToMatch(central_Output_EIP, 800, 620);
        FileOperations.verify_FileCount_Correct(central_P1Data_Output, 30);
        FileOperations.verify_FileCount_Correct(central_P1Images_Output, 30);
        FileOperations.verify_FileCount_Correct(central_P1Queue_Output, 31);

   }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 2)
    public void PT1962_PT4968_Verify_SynchDirection_Filter() throws Exception {
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
        BaseUI.click(Locator.lookupRequiredElement("synchFileTypesTab"));
        RCPAdminApp.verify_Filters_SynchDirections("eip", "*.bt1|*.btr|*.cd1|*.cdx|*.src*", "Both" );

    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 3)
    public void PT1962_Verify_Central_Transfer_Data_EIP() throws Exception {
        RCPAdminApp.closeWindow();
        FileOperations.verify_FileCount_Correct(remote1_InputEIP, 800);
        FileOperations.verify_FileCount_Correct(remote2_InputEIP, 800);
        FileOperations.verify_FileCount_Correct(remote3_InputEIP, 800);
        FileOperations.verify_FileCount_Correct(central_Output_EIP, 800);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 4)
    public void PT1962_Verify_Central_Transfer_Data_UseBats() throws Exception {
        FileOperations.verify_FileCount_Correct(central_Output_Usebats, 600);
        FileOperations.wait_ForFileCount_ToMatch(remote_R1_Output, 600, 220);
        FileOperations.verify_FileCount_Correct(remote_R1_Output, 600);
        FileOperations.verify_FileCount_Correct(remote_R2_Output, 600);
        FileOperations.verify_FileCount_Correct(remote_R3_Output,  600);
    }




    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        RCP.cleanDataAll_ResetServices();
    }
}
