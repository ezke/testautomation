package imageRpsThickClient.tests.tests_RCP_Winium;


import imageRpsThickClient.services.FRS_Winium.pages.FRSAdminApp_Page;
import imageRpsThickClient.services.RCP;
import imageRpsThickClient.services.RCP_Winium.BaseTestWin_RCP;
import imageRpsThickClient.services.RCP_Winium.RCPAdminApp;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;
import utils.Winium.WiniumBrowser;

public class RCP_Central_Installation extends BaseTestWin_RCP {
    private RCPAdminApp RCPAdminApp = new RCPAdminApp();
    String installFilesCentral = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1980";

    String installDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\service\\RCPInstallTest";
    WiniumBrowser rcpAppCentral = new WiniumBrowser("rps602auto1dcm.qalabs.nwk", 9009,
            "K:\\cust1\\Test\\service\\RCPInstallTest\\60200RC_20171018.CentralSite\\RcpCentralSiteAdmin.exe", null);

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\services\\RCP_Winium\\RCP_Repo.txt");
        RCP.makeDirectories1980();
        FileOperations.copyFiles(installFilesCentral,
                installDestination);
        Browser.openWiniumDriver(rcpAppCentral);
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 80)
    public void PT1980_Verify_FRSInstallation_InstallServiceLink()  {
        RCPAdminApp.enterInstallInformationRCP();
        RCPAdminApp.clickStartServiceLink();
        RCPAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));

    }

      @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }



    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        RCPAdminApp.stopServiceUI();
        RCPAdminApp.clickUninstallService();
        RCPAdminApp.closeWindow();

    }
}

