package imageRpsThickClient.tests.tests_RCP_Winium;

import imageRPS.data.baseTest;
import imageRpsThickClient.services.RCP;
import imageRpsThickClient.services.RCP_Winium.BaseTestWin_RCP;
import imageRpsThickClient.services.RCP_Winium.RCPAdminApp;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.*;
import utils.*;

public class RCP_Add_Usebats_UploadAndDownloadTests extends BaseTestWin_RCP{
    XMLParser parser;
    private String xml_File_Location_Remote = "\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml";
    private String xml_File_Location_Remote2 = "\\\\qaautotest_81c.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml";
    private String xml_File_Location_Remote3 = "\\\\qaautotest_7k.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Rcp.RemoteSite.Settings.xml";
    private String xml_File_Location_Central = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml";
    String remote_A_Data1970 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1970\\usebats\\Volume Testing\\200A";
    String remote_B_Data1970 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1970\\usebats\\Volume Testing\\200B";
    String remote_A_Data1972 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1972\\usebats\\Volume Testing\\200A";
    String remote_B_Data1972 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1972\\usebats\\Volume Testing\\200B";
    String remote_A_Data1971 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1971\\usebats\\Volume Testing\\200A";
    String remote_B_Data1971 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1971\\usebats\\Volume Testing\\200B";
    String central_R1_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1974\\usebats\\R1";
    String central_R2_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1974\\usebats\\R2";
    String central_R3_Data = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1974\\usebats\\R3";
    String central_Usebats_Input = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\usedbats\\Usebat_Testing";
    String remote_R1_Output = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats_R1";
    String remote_R2_Output = "\\\\qaautotest_7K.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats_R2";
    String remote_R3_Output = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats_R3";
    String remote1_input1970 = "\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    String remote1_input1971 = "\\\\qaautotest_7K.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    String remote1_input1972 = "\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats";
    String central_Output_Usebats = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\usebats";
    String A_Data1985 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1985\\usebats\\Volume Testing\\200A";
    String B_Data1985 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1985\\usebats\\Volume Testing\\200B";
    String C_Data1985 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1985\\usebats\\Volume Testing\\200C";
    String D_Data1985 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1985\\usebats\\Volume Testing\\200D";
    String A_Data1986 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1986\\usebats\\R1";
    String B_Data1986 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1986\\usebats\\R2";
    String C_Data1986 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1986\\usebats\\R3";
    String A_Data1987 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1987\\usebats\\R1";
    String B_Data1987 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1987\\usebats\\R2";
    String C_Data1987 = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1987\\usebats\\R3";
    private imageRpsThickClient.services.RCP_Winium.RCPAdminApp RCPAdminApp = new RCPAdminApp();
    @NotNull
    private WindowsService windowsServiceCentral() { return RCP.rcpCentralWindowsService();
    }

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        parser = new XMLParser(xml_File_Location_Remote);
        RCP.set_XML_ConfigFile(xml_File_Location_Remote);
        RCP.set_XML_ConfigFile(xml_File_Location_Remote2);
        RCP.set_XML_ConfigFile(xml_File_Location_Remote3);
        RCP.set_XML_ConfigFile(xml_File_Location_Central);
        cleanData_ResetServices();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 1)
    public void PT1970_Verify_FileSync_RemoteToCentral_Server2016() throws Exception {
        cleanData_ResetServices();
        FileOperations.copyFiles(remote_A_Data1970,
                remote1_input1970);
        FileOperations.copyFiles(remote_B_Data1970,
                remote1_input1970);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1970, 400, 80);
        RCP.verify_FilesMatch_TwoFolders(remote_A_Data1970, remote1_input1970);
        RCP.verify_FilesMatch_TwoFolders(remote_B_Data1970, remote1_input1970);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_Usebats, 400, 240);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1970, central_Output_Usebats);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 2)
    public void PT1971_Verify_FileSync_RemoteToCentral_Win7() throws Exception {
        cleanData_ResetServices();
        FileOperations.copyFiles(remote_A_Data1971,
                remote1_input1971);
        FileOperations.copyFiles(remote_B_Data1971,
                remote1_input1971);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1971, 400, 80);
        RCP.verify_FilesMatch_TwoFolders(remote_A_Data1971, remote1_input1971);
        RCP.verify_FilesMatch_TwoFolders(remote_B_Data1971, remote1_input1971);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_Usebats, 400, 440);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1971, central_Output_Usebats);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 3)
    public void PT1972_Verify_FileSync_RemoteToCentral_Win8() throws Exception {
        cleanData_ResetServices();
        FileOperations.copyFiles(remote_A_Data1972,
                remote1_input1972);
        FileOperations.copyFiles(remote_B_Data1972,
                remote1_input1972);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1972, 400, 80);
        RCP.verify_FilesMatch_TwoFolders(remote_A_Data1972, remote1_input1972);
        RCP.verify_FilesMatch_TwoFolders(remote_B_Data1972, remote1_input1972);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_Usebats, 400, 340);
        RCP.verify_FilesMatch_TwoFolders(remote1_input1972, central_Output_Usebats);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 4)
    public void PT1974_Verify_FileSync_600Usebats_UploadRemote() throws Exception {
        RCP.set_XML_Config_1986();
        cleanData_ResetServices();
        FileOperations.copyFiles(central_R1_Data,
                central_Usebats_Input);
        FileOperations.copyFiles(central_R2_Data,
                central_Usebats_Input);
        FileOperations.copyFiles(central_R3_Data,
                central_Usebats_Input);
        FileOperations.wait_ForFileCount_ToMatch(central_Usebats_Input, 600, 80);
        RCP.rcpRemoteWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpCentralWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(remote_R1_Output, 200, 340);
        FileOperations.wait_ForFileCount_ToMatch(remote_R2_Output, 200, 340);
        FileOperations.wait_ForFileCount_ToMatch(remote_R3_Output, 200, 340);
        RCP.verify_FilesMatch_TwoFolders(central_R1_Data, remote_R1_Output);
        RCP.verify_FilesMatch_TwoFolders(central_R2_Data, remote_R2_Output);
        RCP.verify_FilesMatch_TwoFolders(central_R3_Data, remote_R3_Output);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 5)
    public void PT1985_Verify_FileSync_800Usebats_UploadRemote() throws Exception {
        RCP.set_XML_Config_1985();
        cleanData_ResetServices();
        FileOperations.copyFiles(A_Data1985,
                central_Output_Usebats);
        FileOperations.copyFiles(B_Data1985,
                remote1_input1970);
        FileOperations.copyFiles(C_Data1985,
                remote1_input1971);
        FileOperations.copyFiles(D_Data1985,
                remote1_input1972);
        FileOperations.wait_ForFileCount_ToMatch(central_Output_Usebats, 200, 80);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1970, 200, 80);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1971, 200, 80);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1972, 200, 80);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Output_Usebats, 800, 340);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1970, 800, 340);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1971, 800, 340);
        FileOperations.wait_ForFileCount_ToMatch(remote1_input1972, 800, 340);
        FileOperations.verify_FileCount_Correct(central_Output_Usebats, 800);
        FileOperations.verify_FileCount_Correct(remote1_input1970, 800);
        FileOperations.verify_FileCount_Correct(remote1_input1971, 800);
        FileOperations.verify_FileCount_Correct(remote1_input1972, 800);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 6)
    public void PT1986_Verify_FileSync_800Usebats_UploadRemote() throws Exception {
        RCP.set_XML_Config_1986();
        cleanData_ResetServices();
        FileOperations.copyFiles(A_Data1986,
                central_Usebats_Input);
        FileOperations.copyFiles(B_Data1986,
                central_Usebats_Input);
        FileOperations.copyFiles(C_Data1986,
                central_Usebats_Input);
               RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(remote_R1_Output, 200, 240);
        FileOperations.verify_FileCount_Correct(central_Usebats_Input, 600);
        FileOperations.verify_FileCount_Correct(remote_R1_Output, 200);
        FileOperations.verify_FileCount_Correct(remote_R2_Output, 200);
        FileOperations.verify_FileCount_Correct(remote_R3_Output, 200);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 7)
    public void PT1986_PT4969_Verify_SynchDirection_Filter() throws Exception {
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
        BaseUI.click(Locator.lookupRequiredElement("synchFileTypesTab"));
        RCPAdminApp.verify_Filters_SynchDirections("usebats_R1", "*.r1*", "Upload" );
        RCPAdminApp.verify_Filters_SynchDirections("usebats_R2", "*.r2*", "Upload" );
        RCPAdminApp.verify_Filters_SynchDirections("usebats_R3", "*.r3*", "Upload" );


    }

    @Test(groups = {"all_Tests", "regression_Tests", "service_Tests"}, priority = 8)
    public void PT1987_Verify_FileSync_800Usebats_DownloadCentral() throws Exception {
        RCPAdminApp.closeWindow();
        RCP.set_XML_Config_1987();
        cleanData_ResetServices();
        FileOperations.copyFiles(A_Data1987,
                remote_R1_Output);
        FileOperations.copyFiles(B_Data1987,
                remote_R2_Output);
        FileOperations.copyFiles(C_Data1987,
                remote_R3_Output);
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsServiceWin7().start();
        RCP.rcpRemoteWindowsServiceWin8().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.wait_ForFileCount_ToMatch(central_Usebats_Input, 600, 240);
        FileOperations.verify_FileCount_Correct(central_Usebats_Input, 600);
        FileOperations.verify_FileCount_Correct(remote_R1_Output, 200);
        FileOperations.verify_FileCount_Correct(remote_R2_Output, 200);
        FileOperations.verify_FileCount_Correct(remote_R3_Output, 200);
    }


    @BeforeMethod(alwaysRun = true)
    public void cleanData_ResetServices() throws Exception {
        RCP.cleanData_Add400_Usebats_RestServices();
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        RCP.cleanData_Add400_Usebats_RestServices();
    }

}// end of class





