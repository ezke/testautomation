package imageRpsThickClient.tests.tests_RCP_Winium;

import imageRpsThickClient.services.RCP;
import imageRpsThickClient.services.RCP_Winium.BaseTestWin_RCP;
import imageRpsThickClient.services.RCP_Winium.RCPAdminApp;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;
import utils.Winium.WiniumBrowser;

public class RCP_Remote_Install_IncorrectCredentials extends BaseTestWin_RCP {
    private imageRpsThickClient.services.RCP_Winium.RCPAdminApp RCPAdminApp = new RCPAdminApp();

    String installFilesRemote = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-1960";
    String installDestination = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\service\\RCPInstallTest";

    WiniumBrowser rcpAppRemote = new WiniumBrowser("rps602auto1dcm.qalabs.nwk", 9009,
            "K:\\cust1\\Test\\service\\RCPInstallTest\\60200RC.RemoteSite\\RcpRemoteSiteAdmin.exe", null);

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\services\\RCP_Winium\\RCP_Repo.txt");
        RCP.makeDirectories1980();
        FileOperations.copyFiles(installFilesRemote,
                installDestination);
        Browser.openWiniumDriver(rcpAppRemote);
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 10)
    public void PT1960_Verify_RCPInstallationUS_ErrorDialog() {
        RCPAdminApp.enterIncorrectUserInformation_RemoteRCP();
        RCPAdminApp.verifyInstallationErrorText(Is.equalTo("An error occured while trying to install the service.  The service is not installed.  Please see the RcpCentralSiteService.InstallLog file for more details."));

    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 20)
    public void PT1961_Verify_RCPInstallation_InstallServiceLink() {
        RCPAdminApp.clickErrorDialogOk();
        RCPAdminApp.clickInstallServiceLink();
        RCPAdminApp.enterIncorrectPasswordInformation_RemoteRCP();
        RCPAdminApp.verifyInstallationErrorText(Is.equalTo("An error occured while trying to install the service.  The service is not installed.  Please see the RcpCentralSiteService.InstallLog file for more details."));

    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }



    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        RCPAdminApp.clickErrorDialogOk();
        RCPAdminApp.clickUninstallService();
        RCPAdminApp.closeWindow();

    }
}

