package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.ConsolidationBatchDetailReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_GenerateDCM_ConsolidationBatchDetail extends BaseTestWin {

    private String _reportType = "Consolidation(s) Batch Detail";

    private String _fileNameRPT = "conrpta.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "conrpta.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _consolidationDate_From = BaseUI.getDateAsString_InRelationToTodaysDate(-10, "MMddyyyy");
    private String _consolidationDate_From_RPTReport = BaseUI.getDateAsString_InRelationToTodaysDate(-10, "MM/dd/yy");
    private String _consolidationDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");
    private String _consolidationDate_To_RPTReport = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    private ConsolidationBatchDetailReport_Page _batchReportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_ConsolidationBatchDetailReport();

        _batchReportPage.enter_ConsolidationDate_From(_consolidationDate_From);
        _batchReportPage.enter_ConsolidationDate_To(_consolidationDate_To);

        _batchReportPage.check_SaveCSV_Checkbox();
        _batchReportPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
        _batchReportPage = _batchReportPage.runReport();


        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_ConsolidationNumber_DefaultValue() throws Exception {
        BaseUI.verifyElementHasExpectedText(Locator.lookupRequiredElement("consolidationReports_ConsolidationNumber"), "0");
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_batstat_Report_Date() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_Report_ConsolDates() throws Exception {
        String line3 = _reportExtract[2];
        String expectedBatchDateValue = "Consol Dates: " + _consolidationDate_From_RPTReport + " thru " + _consolidationDate_To_RPTReport;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line3);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_batstat_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+"," ");
        String expectedReportName = "Report: conrptb" ;

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_batstat_Report_Title() throws Exception {
        BaseUI.baseStringPartialCompare("Report Title", _reportType, _reportExtract[0]);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2331_ConsolBatchDetailReport_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found CSV Data.", "Did NOT find CSV Data.");

        for(HashMap<String, String> row : _csvData.data){
            String dateToCheck = row.get("Consol Date");
            BaseUI.verify_Date_IsBetween_DateRange(_consolidationDate_From_RPTReport, _consolidationDate_To_RPTReport, dateToCheck, "MM/dd/yy");
        }
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchReportPage != null) {
            _batchReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
