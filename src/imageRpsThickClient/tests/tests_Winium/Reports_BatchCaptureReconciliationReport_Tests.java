package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchCapture_ReconciliationReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.SiteConsolidationReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_BatchCaptureReconciliationReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "strptbr.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "strptbr.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private BatchCapture_ReconciliationReport_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _dateToReport = "02/08/2018";
    private String _dateToReport_RPT = "02/08/18";
    

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_SiteReportsMenu()
                .navigate_BatchCaptureReconciliation();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage._dateToReport_TextBox.enterText(_dateToReport);
        _reportPage._failIfNoData_Checkbox.check();

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    //Test was calling for these values to be set.  These values were already set this way as defaults.  Testing that these are still the defaults.
    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_ReportDateType_DefaultValue() throws Exception {
        _reportPage._reportDateType.verify_ItemSelected("System Date");
    }

    //Test was calling for these values to be set.  These values were already set this way as defaults.  Testing that these are still the defaults.
    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_TypeOfReport_DefaultValue() throws Exception {
        _reportPage._typeOfReport.verify_ItemSelected("Detail Report");
    }

    //Test was calling for these values to be set.  These values were already set this way as defaults.  Testing that these are still the defaults.
    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_ClientFilter_DefaultValue() throws Exception {
        _reportPage._clientFilterDropdown.verify_ItemSelected("All Clients");
    }

    //Test was calling for these values to be set.  These values were already set this way as defaults.  Testing that these are still the defaults.
    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_OperatorFilter_DefaultValue() throws Exception {
        _reportPage._operatorFilterDropdown.verify_ItemSelected("All Operators");
    }

    //Test was calling for these values to be set.  These values were already set this way as defaults.  Testing that these are still the defaults.
    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_ReportSortOrder_DefaultValue() throws Exception {
        _reportPage._outputAndRunOptions.verify_ReportSortOrder_ContainsOptions(new String[]{"Client Location", "P1 Station ID", "Client ID"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "ImageRPS Capture Batch Reconciliation Report", line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_RPT_Date_Range() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");

        String expectedValue = "Batch System Date = " + _dateToReport_RPT;
        BaseUI.baseStringPartialCompare("Batch System Date", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: strptbr";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_CSV_ConsolidationDatesAccurate() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Batch Date", _dateToReport_RPT, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2232_BatchReconciliationReport_Validate_NoError() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
