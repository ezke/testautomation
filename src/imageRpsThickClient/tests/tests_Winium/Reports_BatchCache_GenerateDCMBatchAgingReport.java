package imageRpsThickClient.tests.tests_Winium;


import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchAgingReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_BatchCache_GenerateDCMBatchAgingReport extends BaseTestWin {

    private String _fileNameRPT = "batage.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "batage.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;
    private int _expectedDateCount = 6;
    private BatchAgingReport_Page _batchAgePage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _batchAgePage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchAgingReport();

        _batchAgePage.check_SaveCSV_Checkbox();
        _batchAgePage.enter_CSV_Name("k:\\cust1\\Test\\reports\\batage.csv");

        _batchAgePage = _batchAgePage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_DaysOld_DefaultValue() throws Exception {
        String expectedDaysOld = "00" + String.valueOf(_expectedDateCount);
        String daysOld = _batchAgePage.get_DaysOld();

        BaseUI.baseStringCompare("Days Old", expectedDaysOld, daysOld);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_ReportDateType_DefaultValue() throws Exception {
        String expectedProcessDate = "Process Date";
        _batchAgePage._reportDateType_ComboBox.verify_ItemSelected(expectedProcessDate);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_batage_Report_Date() throws Exception {
        _batchAgePage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_batage_Report_BatchDate() throws Exception {

        String line2 = _reportExtractRPT[1];
        String expectedBatchDateValue = "Batch Date = " + String.valueOf(_expectedDateCount) + " days old or older";

        BaseUI.baseStringPartialCompare("Batch Date", expectedBatchDateValue, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_batage_Report_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+"," ");
        String expectedReportName = "Report: batage";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_batage_Report_Title() throws Exception {
        String expectedTitle = "ImageRPS Batch Aging Report";
        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, _reportExtractRPT[0]);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_batage_Report_CSV_EachEntry_OlderThanExpectedDays() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found Table Data", "Did NOT find table data.");
        _batchAgePage.validate_AllDates_BeforeCutoff(_csvData,_expectedDateCount);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2327_BatchAgingReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchAgePage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchAgePage != null) {
            _batchAgePage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
