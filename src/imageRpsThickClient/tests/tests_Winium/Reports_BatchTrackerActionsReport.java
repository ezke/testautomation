package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchTrackerActionsReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_BatchTrackerActionsReport extends BaseTestWin {

    private String _fileNameRPT = "btactrpt.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "btactrpt.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private BatchTrackerActionsReport_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _rptTable;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _eventDate_From = "01/01/2014";
    private String _eventDate_From_RPTFormat = "01/01/14";
    private String _eventDate_From_CSVFormat = "01/01/2014 00:00";

    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String get_eventDate_To_CSVFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy HH:mm");

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchTrackerActionsReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_Dates(_eventDate_From, _eventDate_To);
        _reportPage.check_RejectsCheckbox();

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        //Sort RPT and CSV Tables before we compare them
       // _rptTable = _reportPage.get_RPT_ReportTable(_reportExtractRPT);
       

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2362_BatchTrackerActionsReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2362_BatchTrackerActionsReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Batch Tracker Actions Report", line1);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2362_BatchTrackerActionsReport_Validate_RPT_Date_Range() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Start: " + _eventDate_From_RPTFormat + " - End: " + _eventDate_To_RPTFormat;
        BaseUI.baseStringPartialCompare("Date Range", expectedValue, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2362_BatchTrackerActionsReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: btActRpt";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2362_BatchTrackerActionsReport_Validate_CSV_ActionType_AllReject() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Action Type","REJECT", _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2362_BatchTrackerActionsReport_Validate_CSV_DatesFallInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"BT Action Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2362_BatchTrackerActionsReport_Validate_RPT_GrandTotal() throws Exception {
        String grandTotalLine = _reportPage.get_RPTLineContainingValue(_reportExtractRPT, "Grand Totals");
        grandTotalLine = grandTotalLine.replace("Grand Totals", "").trim();

        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found CSV Data", "Did NOT find CSV Data");

        BaseUI.verify_true_AndLog(_csvData.data.size() == Integer.valueOf(grandTotalLine),
                "Grand Totals matched value of " + String.valueOf(_csvData.data.size()),
                "Grand Totals: saw " + grandTotalLine + " but expecting" + String.valueOf(_csvData.data.size()));
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2362_BatchTrackerActionsReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
