package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.ForeignItemsReport_Batch_Page;
import imageRpsThickClient.winiumPages.ForeignItemsReport_Block_Page;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_GenerateDCM_ForeignItemsReport_Block extends BaseTestWin {

    private String _fileNameRPT = "foritmblk.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "foritmblk.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _startDate = "01012017";
    private String _startTime;
    private String _startDate_RPTReport = "01/01/17";
    private String _endDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");
    private String _endTime;
    private String _endDate_RPTReport = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    private String _docID = "1";

    private ForeignItemsReport_Block_Page _batchReportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _reportType_Run1 = "Beg Date";
    private String _reportType_AfterUpdate = "Consol Date";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_ForeignItemsReport_BlockReport();

        _batchReportPage.check_SaveCSV_Checkbox();

        _startTime = _batchReportPage.get_StartTime();
        _endTime = _batchReportPage.get_EndTime();

        _batchReportPage.enter_StartDate(_startDate);
        _batchReportPage.enter_EndDate(_endDate);
        _batchReportPage.enter_DocID(_docID);

        _batchReportPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
        _batchReportPage = _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"", "use_label");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_ReportType_DefaultValue() throws Exception {
        _batchReportPage._reportType.verify_ItemSelected(_reportType_Run1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_Report_Date() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_Report_Title() throws Exception {
        String line1 = _reportExtract[0];
        String expectedBatchDateValue = "Foreign Items Report by Block - Doc ID: " + _docID;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_Report_DateRange() throws Exception {
        String line2 = _reportExtract[1];
        String startTimeReformatted = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(_startTime, "hh:mm:ss a", "hh:mm a", 0);
        String endTimeReformatted = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(_endTime, "hh:mm:ss a", "hh:mm a", 0);

        String expectedBatchDateValue = _reportType_Run1 + " " + _startDate_RPTReport + " " + startTimeReformatted + " - " + _endDate_RPTReport + " " + endTimeReformatted;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: foritmblk";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _batchReportPage.validate_DatesFallInRange(_csvData, _startDate_RPTReport, _endDate_RPTReport, new String[]{"Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2334_ForeignItemsReportBlock_Validate_CSV_LabelColumn() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("use_label", _reportType_Run1, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 2)
    public void PT2334_ForeignItemsReportBlock_Validate_CSV_UpdateToReportType() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _batchReportPage._reportType.select_ComboBoxItem(_reportType_AfterUpdate);
        _batchReportPage = _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"", "use_label");
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("use_label", "Consolidation Date", _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 3)
    public void PT2334_ForeignItemsReportBlock_Validate_CSV_DateColumnFallsWithinRange_AfterUpdate() throws Exception {
        _batchReportPage.validate_DatesFallInRange(_csvData, _startDate_RPTReport, _endDate_RPTReport, new String[]{"Date"});
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 3)
    public void PT2334_ForeignItemsReportBlock_Validate_RPT_UpdateToReportType() throws Exception {
        String line2 = _reportExtract[1];

        String expectedBatchDateValue = "Consolidation Date" + " " + _startDate_RPTReport + " - " + _endDate_RPTReport;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line2);

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchReportPage != null) {
            _batchReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
