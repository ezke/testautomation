package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.OperatorStatsDetailReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;


public class Reports_OperatorStatsDetail extends BaseTestWin {

    private String _fileNameRPT = "opstatdet.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "opstatdet.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private OperatorStatsDetailReport_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _rptTable;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _activityDate_From = "01/01/2014";
    private String _activityDate_From_RPTFormat = "01/01/14";
    private String _activityTime_From;

    private String _activityDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String _activityDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _activityTime_To;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_OperatorStatsDetailReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_Dates(_activityDate_From, _activityDate_To);

        _activityTime_From = BaseUI.getTextFromField(Locator.lookupRequiredElement("operStatsDetail_FromActivityTime_TextBox"));
        _activityTime_To = BaseUI.getTextFromField(Locator.lookupRequiredElement("operStatsDetail_ThroughActivityTime_TextBox"));

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");
        _rptTable = _reportPage.get_RPT_Table(_reportExtractRPT);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Operator Stats Detail", line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_RPT_Date_Range() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String from_TimeReformatted = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(_activityTime_From, "hh:mm:ss a", "hh:mm a", 0);
        String to_TimeReformatted = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(_activityTime_To, "hh:mm:ss a", "hh:mm a", 0);

        String expectedValue = "Start: " + _activityDate_From_RPTFormat + " " + from_TimeReformatted + " - End: " + _activityDate_To_RPTFormat + " " + to_TimeReformatted;
        BaseUI.baseStringPartialCompare("Date Range", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: OpDetail";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_CSV_DatesFallInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData,_activityDate_From_RPTFormat, _activityDate_To_RPTFormat, new String[]{"Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2365_OperatorStatsDetail_Validate_RPT_Rej_Percent_Accurate() throws Exception {
        _reportPage.verify_RPT_RejsPercent_Accurate(_rptTable);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
