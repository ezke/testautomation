package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

public class Reports_QAASEvents_Tests extends BaseTestWin {

    private String _fileNameRPT = "strpt10.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "strpt10.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private DataCompletionJobSummaryByClient _reportPage;

    private AddUpdate_EventType_Page _addUpdateEventTypePage;
    private ViewEvents_Page _viewEventsPage;
    private EventsResult_Page _eventResultsPage;


    private String _activityDate_From = "01/01/2014";

    private String _eventID = "167326";

    private String _activityDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");

    private TableData _eventsTable;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _addUpdateEventTypePage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ConfigurationMenu()
                .navigate_NotificationsAndEvents()
                .navigate_EventTypeSetup()
                .navigate_AddUpdate_EventType(_eventID);

        _addUpdateEventTypePage.check_KeepEventData_Checkbox();

        _reportPage = _addUpdateEventTypePage.navigate_BackTo_EventTypeSetup()
                .navigate_BackTo_NotificationsAndEvents()
                .navigate_BackTo_ConfigurationMenu()
                .navigate_BackTo_MainMenu()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_OperatorPerformanceStatisticsReports()
                .navigate_DataCompletionJobSummaryByClient();

        _reportPage.check_FailIfNoData_Checkbox();
        _reportPage.runReport_WhenNoReportPossible();
        _reportPage = _reportPage.click_OKOnError();

        _reportPage.enter_Dates(_activityDate_From,_activityDate_To);
        _reportPage = _reportPage.runReport();

        _viewEventsPage = _reportPage.navigate_BackTo_OperatorPerformanceStatisticsReports()
                .navigate_BackTo_StatisticsMenu()
                .navigate_BackTo_ReportsMenu_Page()
                .navigate_BackTo_MainMenuPage()
                .navigate_DataMaintenanceMenu()
                .navigate_NotificationsAndEvents()
                .navigate_ViewEvents();

        _viewEventsPage.enterText_EventIDLowerRange(_eventID);
        _viewEventsPage.enterText_EventIDHigherRange(_eventID);

        _eventResultsPage = _viewEventsPage.navigate_EventsResult();

        _eventsTable = _eventResultsPage.get_EventIDAndEventOccuredDT();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT5273_QAAS_Events_Verify_WeFoundReportEvent() throws Exception {
        BaseUI.verify_true_AndLog(_eventsTable.data.size() > 0, "Found Table Data", "Did NOT find Table Data");

        HashMap<String, String> rowWeWant = _eventsTable.return_Row_BasedOn_1MatchingField("wkMsgString", "Report Created Statistics Summary Report for Clients");

        BaseUI.verify_true_AndLog(rowWeWant != null, "Found correct row", "Did not find the row we wanted");
        BaseUI.verify_true_AndLog(rowWeWant.get("EventOccuredDT").startsWith(_activityDate_To), "Row Event Date matched today's date", "Row Event Date did not match today's date");
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
