package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.WebExceptions_TransactionEventReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_GenerateDCM_WebExceptions_TransactionEventReport_SortByClient extends BaseTestWin {

    private String _fileNameRPT = "webtrarpt.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "webtrarpt.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _eventDate_From = "01012014";
    private String _eventDate_From_RPTFormat = "01/01/14";
    private String _eventDate_From_CSVFormat = "01/01/2014 01:01";
    private String _eventDate_From_NoReport = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");

    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _eventDate_To_CSVFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy HH:mm");
    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");


    private WebExceptions_TransactionEventReport_Page _batchReportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_WebExceptions_TransactionEventReport();

        _batchReportPage.check_SaveCSV_Checkbox();

        _batchReportPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2350_WebTransReport_Validate_ReportDateType_Value() throws Exception {
        _batchReportPage.verify_ReportDateType_Valid();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2350_WebTransReport_Validate_ReportSortOrder_DefaultValue() throws Exception {
        String[] defaultValues = {"Client ID"};
        _batchReportPage._outputAndRunOptions.verify_ReportSortOrder_ContainsOptions(defaultValues);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2350_WebTransReport_Validate_FailIfNoDataChecked() throws Exception {
        _batchReportPage._outputAndRunOptions.verify_FailIfNoData_Checked();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2350_WebTransReport_Validate_BadDates_Throws_ReportException() throws Exception {
        _batchReportPage.enter_EventDates(_eventDate_From_NoReport, _eventDate_To);
        _batchReportPage.runReport_WhenNoReportPossible();
        _batchReportPage.verify_NoReportFound();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 30)
    public void PT2350_WebTransReport_EnterValidDates_AndRun_Validate_Sorted_Ascending_ClientID() throws Exception {
        _batchReportPage = _batchReportPage.click_OK_ForError();
        _batchReportPage.enter_EventDates(_eventDate_From, _eventDate_To);
        _batchReportPage = _batchReportPage.runReport();
        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");

        _batchReportPage.verify_ClientID_Sorted_Ascending(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2350_WebTransReport_Validate_ReportDate() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2350_WebTransReport_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Web Exceptions - Transaction Event Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2350_WebTransReport_Validate_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: webtran";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2350_WebTransReport_Validate_DateOnLine2() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Event Date(s): " + _eventDate_From_RPTFormat + " thru " + _eventDate_To_RPTFormat;

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2350_WebTransReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2350_WebTransReport_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _batchReportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_CSVFormat, _eventDate_To_CSVFormat, new String[]{"Event Date"}, "MM/dd/yyyy HH:mm");
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 40)
    public void PT2350_WebTransReport_EnterValidDates_AndRun_Validate_Sorted_Descending_ClientID() throws Exception {
        _batchReportPage._outputAndRunOptions.click_Ascending_Or_Descending();
        _batchReportPage = _batchReportPage.runReport();
        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _batchReportPage.verify_ClientID_Sorted_Descending(_reportExtract);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        if(_batchReportPage != null) {
            _batchReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
