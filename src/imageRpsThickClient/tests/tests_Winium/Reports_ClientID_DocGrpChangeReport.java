package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.ClientID_DocGrpChangeReport;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_ClientID_DocGrpChangeReport extends BaseTestWin {

    private String _fileNameRPT = "batchgrpt.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "batchgrpt.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private ClientID_DocGrpChangeReport _reportPage;

    private String[] _reportExtractRPT;
    private TableData _rptTable;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _eventDate_From = "01/01/2014";
    private String _eventDate_From_RPTFormat = "01/01/14";
    private String _eventDate_From_CSVFormat = "01/01/2014 00:00";

    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String get_eventDate_To_CSVFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy HH:mm");

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_ClientID_DocGrpChangeReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_Dates(_eventDate_From, _eventDate_To);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        //Sort RPT and CSV Tables before we compare them
        _rptTable = _reportPage.get_RPT_ReportTable(_reportExtractRPT);
        _rptTable.replace_Empty_WithEmpty();
        _rptTable.replaceValue_ForColumn_WhenEqualTo("P1Seq", "", "0");
        _rptTable.replaceValue_ForColumn_WhenEqualTo("CurrentBatch", "", "0");
        _rptTable.replaceValue_ForColumn_WhenEqualTo("CurrentClnt", "", "0");
        _rptTable.replaceValue_ForColumn_WhenEqualTo("CurrentDG", "", "0");
        _rptTable.replaceValue_ForColumn_WhenEqualTo("CurrentTran", "", "0");


        _rptTable.sort_ByColumn_numeric_Ascending("CapturedAsBatch");
        _rptTable.sort_ByColumn_numeric_Ascending("CapturedAsClnt");
        _rptTable.sort_ByColumn_numeric_Ascending("CapturedAsDG");
        _rptTable.sort_ByColumn_numeric_Ascending("ChangedToBatch");
        _rptTable.sort_ByColumn_numeric_Ascending("ChangedToClnt");
        _rptTable.sort_ByColumn_numeric_Ascending("ChangedToDG");
        _rptTable.sort_ByColumn_numeric_Ascending("CurrentBatch");
        _rptTable.sort_ByColumn_numeric_Ascending("P1Seq");
        _rptTable.sort_ByColumn_Ascending("DateTime");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvData.remove_Character("\"");
        _csvData.sort_ByColumn_numeric_Ascending("Cap Batch ID");
        _csvData.sort_ByColumn_numeric_Ascending("Capture Client");
        _csvData.sort_ByColumn_numeric_Ascending("Capture Doc Group");
        _csvData.sort_ByColumn_numeric_Ascending("To Batch ID");
        _csvData.sort_ByColumn_numeric_Ascending("To Client");
        _csvData.sort_ByColumn_numeric_Ascending("To Doc Group");
        _csvData.sort_ByColumn_numeric_Ascending("Batch ID");
        _csvData.sort_ByColumn_numeric_Ascending("Cap P1Seq");
        _csvData.sort_ByColumn_Ascending("Date/Time");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2356_ClientID_DocGrpChangeReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2356_ClientID_DocGrpChangeReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Client ID - Doc Grp Change Report", line1);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2356_ClientID_DocGrpChangeReport_Validate_RPT_Date_Range() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "From Event Date = " + _eventDate_From_RPTFormat + " To Event Date = " + _eventDate_To_RPTFormat;
        BaseUI.baseStringPartialCompare("Date Range", expectedValue, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2356_ClientID_DocGrpChangeReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: batchgrpta";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CurrentClient() throws Exception {

        BaseUI.verify_TableColumns_Match("CurrentClnt", "Current Client", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CurrentDogGroup() throws Exception {

        BaseUI.verify_TableColumns_Match("CurrentDG", "Current Doc Group", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Populated() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Founc CSV Data", "Did NOT find CSV Data");

    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CurrentTran() throws Exception {

        BaseUI.verify_TableColumns_Match("CurrentTran", "Cur Tran Num", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CurrentJob() throws Exception {
        BaseUI.verify_TableColumns_Match("CurrentJob", "Cur Job", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CurrentBatchID() throws Exception {

        BaseUI.verify_TableColumns_Match("CurrentBatch", "Batch ID", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CapturedBatchID() throws Exception {
        BaseUI.verify_TableColumns_Match("CapturedAsBatch", "Cap Batch ID", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CaptureClient() throws Exception {
        BaseUI.verify_TableColumns_Match("CapturedAsClnt", "Capture Client", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CaptureDocGroup() throws Exception {
        BaseUI.verify_TableColumns_Match("CapturedAsDG", "Capture Doc Group", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_CaptureP1Seq() throws Exception {

        BaseUI.verify_TableColumns_Match("P1Seq", "Cap P1Seq", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_DateTime() throws Exception {
        BaseUI.verify_TableColumns_Match("DateTime", "Date/Time", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_Operator() throws Exception {
        BaseUI.verify_TableColumns_Match("Operator", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_EventJob() throws Exception {
        BaseUI.verify_TableColumns_Match("EventJob", "Event Job", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_Stn() throws Exception {
        BaseUI.verify_TableColumns_Match("Stn",  _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_FromBatchID() throws Exception {
        BaseUI.verify_TableColumns_Match("ChangedFromBatch", "From Batch ID",   _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_FromClient() throws Exception {
        BaseUI.verify_TableColumns_Match("ChangedFromClnt", "From Client",   _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_FromDocGroup() throws Exception {
        BaseUI.verify_TableColumns_Match("ChangedFromDG", "From Doc Group",   _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_DoubleAsterisk() throws Exception {
        BaseUI.verify_TableColumns_Match("**", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_ToBatchID() throws Exception {
        BaseUI.verify_TableColumns_Match("ChangedToBatch", "To Batch ID", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_ToClient() throws Exception {
        BaseUI.verify_TableColumns_Match("ChangedToClnt", "To Client", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_ToDocGroup() throws Exception {
        BaseUI.verify_TableColumns_Match("ChangedToDG", "To Doc Group", _rptTable.data, _csvData.data);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_Status() throws Exception {
        BaseUI.verify_TableColumns_Match("Status", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_Column_Matches_RPT_Value() throws Exception {

        _rptTable.remove_Character(".00", "Value");
        _rptTable.remove_Character(",", "Value");
        _rptTable.replace_Characters_FromBeginning("0.", ".", "Value");
        BaseUI.verify_TableColumns_Match("Value", _rptTable.data, _csvData.data);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2356_ClientID_DocGrpChangeReport_Validate_CSV_DatesFallInRange() throws Exception {
        for(HashMap<String, String> row : _csvData.data){
            String dateToCheck = row.get("Date/Time");
            BaseUI.verify_Date_IsBetween_DateRange(_eventDate_From_CSVFormat, get_eventDate_To_CSVFormat, dateToCheck, "MM/dd/yyyy HH:mm");
        }
    }



    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
