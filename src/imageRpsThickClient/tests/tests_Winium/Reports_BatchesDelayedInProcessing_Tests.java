package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchesDelayedInProcessing_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_BatchesDelayedInProcessing_Tests extends BaseTestWin {

    private String _fileNameRPT = "delay.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "delay.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private BatchesDelayedInProcessing_Page _reportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _reportBatchesThatTookMoreThan = "365d";
    private String _reportBatchesThatTookMoreThan_GetsUpdatedTo = "365days";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_WorkFlowReportsMenu()
                .navigate_BatchesDelayedInprocessing();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage.enter_ReportBatchesThatTookMoreThan(_reportBatchesThatTookMoreThan);
        _reportPage.select_BatchID_Radio();
        _reportPage.check_IncludeBatchesThatHaveReached_OUT();

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        BaseUI.baseStringPartialCompare("Report Title", "Batches Delayed in Processing", line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        BaseUI.baseStringPartialCompare("Report Type", "Report: batstat", line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_ReportSummary() throws Exception {
        String line2 = _reportExtract[1].replaceAll("//s+", " ");
        BaseUI.baseStringPartialCompare("Report Summary", "Batches that are incomplete or took more than: "
                        + _reportBatchesThatTookMoreThan_GetsUpdatedTo + " to complete", line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2223_BatchesDelayedInProcessing_Validate_BatchDelayColumn_AllContainYears() throws Exception {

        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found data in table.", "Did NOT find data in table.");

        SoftAssert softAssert = new SoftAssert();
        for (HashMap<String, String> row : _csvData.data) {
            String batchDelay = row.get("Batch Delay");
            //Check that timeframe has years, which would put it over 365 days.
            softAssert.assertTrue(batchDelay.contains("yr,") || batchDelay.contains("yrs"), "Batch Delay did not contain years, seeing " + batchDelay);
        }

        softAssert.assertAll();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
