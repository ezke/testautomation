package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_TransportStatsSummarizedReport extends BaseTestWin {

    private String _fileNameRPT = "tpsumda2.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "tpsumda2.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _startDate = "01012017";
    private String _startDate_RPTReport = "01/01/17";
    private String _endDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");
    private String _endDate_RPTReport = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");


    private TransportStatsSummarizedReport_Page _reportPage;

    private String[] _reportExtract;

    private TableData _reportRPTTable;

    private TableData _csvData;
    private String[] _csvExtract;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_TransportStatsSummarizedReport();

        _reportPage.check_SaveCSV_Checkbox();

        _reportPage.printDetailCheckbox.check();
        _reportPage.enter_Dates(_startDate, _endDate);

        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");
        _reportRPTTable = _reportPage.getRPTReportTable(_reportExtract);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");


    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2367_TransportStatsSummarizedReport_Validate_RPT_ActualTimeAccurate() throws Exception {
        _reportPage.verify_TimeDurationsAccurate(_reportRPTTable);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2367_TransportStatsSummarizedReport_Validate_Stat7780Table_ElapsedTimeAccurate() throws Exception {
        _reportPage.verify_ElapsedTime_Stat7780Table(_reportRPTTable);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_InBatchTotalPerHour_IsAverage() throws Exception {
        _reportPage.verify_InBatch_PagesPerHour(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_ActualPagesPerHour_IsAverage() throws Exception {
        _reportPage.verify_Actual_PagesPerHour(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_Report_Title() throws Exception {
        String line1 = _reportExtract[0];
        String expectedBatchDateValue = "Transport Stats Detail Report";

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_Report_DateRange() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = _startDate_RPTReport + " - " + _endDate_RPTReport;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: tpsum";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2368_TransportStatsSummarizedReport_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _startDate_RPTReport, _endDate_RPTReport, new String[]{"Begin Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT5178_TransportStatsSummarizedReport_VerifyWallClockPagesPerHour_Accurate() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _reportPage.printDetailCheckbox.uncheck();

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");
        _reportRPTTable = _reportPage.getRPTReportTable_WithWallClock(_reportExtract);

        _reportPage.verify_WallClock_PagesPerHour(_reportRPTTable);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
