package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_BatchTimelineReport_DateTypes extends BaseTestWin {

    private String _fileNameRPT = "battimeline.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "battimeline.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private BatchTimelineReport_Page _reportPage;
    private BatchMenu_Page _batchMenuPage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _dateFrom = BaseUI.getDateAsString_InRelationToTodaysDate(-160, "MM/dd/yyyy");
    private String _dateFrom_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(-160, "MM/dd/yy");
    private String _dateFrom_CSVFormat = BaseUI.getDateAsString_InRelationToTodaysDate(-160, "MM/dd/yyyy HH:mm:ss");

    private String _dateTo = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String _dateTo_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _dateTo_CSVFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy HH:mm:ss");;

    private String _fromJob = "p1";
    private String _toJob = "don";
    
    private String _systemDateType = "System Date";
    private String _receiveDateType = "Receive Date";
    private String _processDateType = "Process Date";
    private String _batchDateType = "Batch Date";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchTimelineReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_Dates(_dateFrom, _dateTo);
        _reportPage._reportDateType.select_ComboBoxItem(_systemDateType);


        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5114_BatchTimelineReport_Validate_SystemDate_ReportDate() throws Exception {
        _reportPage._reportDateType.verify_ItemSelected(_systemDateType);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5114_BatchTimelineReport_Validate_SystemDate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5114_BatchTimelineReport_Validate_SystemDate_RPT_Date_Range() throws Exception {
        _reportPage.verify_DateRange_Line2(_reportExtractRPT, _systemDateType, _dateFrom_RPTFormat, _dateTo_RPTFormat);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5114_BatchTimelineReport_Validate_SystemDate_CSV_DateColumns() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _dateFrom_CSVFormat, _dateTo_CSVFormat,
                new String[]{"Start Job Date and Time","End Job Date and Time"}, "MM/dd/yyyy HH:mm:ss");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=10)
    public void PT5114_BatchTimelineReport_Setup_ReceiveDate() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage._reportDateType.select_ComboBoxItem(_receiveDateType);
        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);
        _csvData.remove_Character("\"");

    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=11,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ReceiveDate")
    public void PT5114_BatchTimelineReport_Validate_ReceiveDate_ReportDate() throws Exception {
        _reportPage._reportDateType.verify_ItemSelected(_receiveDateType);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=11,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ReceiveDate")
    public void PT5114_BatchTimelineReport_Validate_ReceiveDate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=11,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ReceiveDate")
    public void PT5114_BatchTimelineReport_Validate_ReceiveDate_RPT_Date_Range() throws Exception {
        _reportPage.verify_DateRange_Line2(_reportExtractRPT, _receiveDateType, _dateFrom_RPTFormat, _dateTo_RPTFormat);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=11,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ReceiveDate")
    public void PT5114_BatchTimelineReport_Validate_ReceiveDate_CSV_DateColumns() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _dateFrom_CSVFormat, _dateTo_CSVFormat,
                new String[]{"Start Job Date and Time","End Job Date and Time"}, "MM/dd/yyyy HH:mm:ss");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=20)
    public void PT5114_BatchTimelineReport_Setup_ProcessDate() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage._reportDateType.select_ComboBoxItem(_processDateType);
        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);
        _csvData.remove_Character("\"");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=21,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ProcessDate")
    public void PT5114_BatchTimelineReport_Validate_ProcessDate_ReportDate() throws Exception {
        _reportPage._reportDateType.verify_ItemSelected(_processDateType);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=21,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ProcessDate")
    public void PT5114_BatchTimelineReport_Validate_ProcessDate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=21,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ProcessDate")
    public void PT5114_BatchTimelineReport_Validate_ProcessDate_RPT_Date_Range() throws Exception {
        _reportPage.verify_DateRange_Line2(_reportExtractRPT, _processDateType, _dateFrom_RPTFormat, _dateTo_RPTFormat);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=21,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_ProcessDate")
    public void PT5114_BatchTimelineReport_Validate_ProcessDate_CSV_DateColumns() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _dateFrom_CSVFormat, _dateTo_CSVFormat,
                new String[]{"Start Job Date and Time","End Job Date and Time"}, "MM/dd/yyyy HH:mm:ss");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=30)
    public void PT5114_BatchTimelineReport_Setup_BatchDate() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage._reportDateType.select_ComboBoxItem(_batchDateType);
        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);
        _csvData.remove_Character("\"");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=31,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_BatchDate")
    public void PT5114_BatchTimelineReport_Validate_BatchDate_ReportDate() throws Exception {
        _reportPage._reportDateType.verify_ItemSelected(_batchDateType);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=31,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_BatchDate")
    public void PT5114_BatchTimelineReport_Validate_BatchDate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=31,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_BatchDate")
    public void PT5114_BatchTimelineReport_Validate_BatchDate_RPT_Date_Range() throws Exception {
        _reportPage.verify_DateRange_Line2(_reportExtractRPT, _batchDateType, _dateFrom_RPTFormat, _dateTo_RPTFormat);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=31,
            dependsOnMethods = "PT5114_BatchTimelineReport_Setup_BatchDate")
    public void PT5114_BatchTimelineReport_Validate_BatchDate_CSV_DateColumns() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _dateFrom_CSVFormat, _dateTo_CSVFormat,
                new String[]{"Start Job Date and Time","End Job Date and Time"}, "MM/dd/yyyy HH:mm:ss");
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
