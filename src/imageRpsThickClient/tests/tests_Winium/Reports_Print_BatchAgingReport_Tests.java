package imageRpsThickClient.tests.tests_Winium;


import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.pageControls.ChooseAPrinter_Popup;
import imageRpsThickClient.winiumPages.BatchAgingReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_Print_BatchAgingReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "batage.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "batage.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;
    private BatchAgingReport_Page _batchAgePage;
    private ChooseAPrinter_Popup _printerPopup;


    private int _printerOption = 8;
    private int _printerCopies = 10;

    private String _expectedPrinterInsertion;
    private String[] _printerExtract;
    private String[] _reportExtractRPT;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        _expectedPrinterInsertion = "#l"+String.valueOf(_printerCopies);
      //  "\u001B&l"
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(GlobalVariables.default_PrinterReportLocation);
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _batchAgePage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchAgingReport();

        _batchAgePage.check_SaveCSV_Checkbox();
        _batchAgePage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _batchAgePage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _batchAgePage._printToPrinter_Checkbox.check();
        _batchAgePage._printerNumber_TextBox.enterText(String.valueOf(_printerOption));
        _batchAgePage._printCopies_TextBox.enterText(String.valueOf(_printerCopies));

        _batchAgePage = _batchAgePage.runReport();

        _printerExtract = DataBuilder.Get_FileData_AsString(
                new File(GlobalVariables.default_PrinterReportLocation)).split("\\n");

        _batchAgePage._printerNumber_TextBox.click();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" },priority=10)
    public void PT2358_PrinterReport_StartsWith_PrinterDataInsertion() throws Exception {
        String firstLine = _printerExtract[0];
        BaseUI.verify_true_AndLog(firstLine.startsWith(_expectedPrinterInsertion), "Print output contained correct output insertion.",
                "Print output did not contain correct print output, seeing " + firstLine);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" },priority=15)
    public void PT2358_PrinterReport_Matches_RPT() throws Exception {
        String printerLine1WithoutPrinterString  = _printerExtract[0].substring(_printerExtract[0].indexOf("Date"), _printerExtract[0].length());
        BaseUI.baseStringCompare("Line 1", _reportExtractRPT[0], printerLine1WithoutPrinterString);
        BaseUI.baseStringCompare("Line 1", _reportExtractRPT[1], _printerExtract[1]);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" },priority=17)
    public void PT2358_PrinterReport_PrinterNumber() throws Exception {
        String tooltip = _batchAgePage._printerNumber_TextBox.getToolTip();
        BaseUI.baseStringCompare("Printer Number", "Which Printer to use?  <F2> for lookup.", tooltip);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" },priority=18)
    public void PT2358_PrinterReport_PrinterCopies() throws Exception {
        String tooltip = _batchAgePage._printCopies_TextBox.getToolTip();
        BaseUI.baseStringCompare("Printer Copies", "Number of copies to print.", tooltip);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" },priority=20)
    public void PT2358_ChooseAPrinter_PopupAppears() throws Exception {
        _printerPopup = _batchAgePage.openPrinterPopup();
        TableData choosePrinterData = _printerPopup.get_PrinterTableData();
        HashMap<String, String> expectedPrinter = new HashMap<String, String>();
        expectedPrinter.put("Printer", "prtfile");
        expectedPrinter.put("LPT", "8");
        expectedPrinter.put("UNC", "report.txt");

        choosePrinterData.verify_MatchingRow_Found(expectedPrinter);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" },priority=25)
    public void PT2358_ChooseAPrinter_PopupCloses() throws Exception {
        _printerPopup.exit_PrinterPopup();
        _printerPopup.verify().notDisplayed();
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchAgePage != null) {
            _batchAgePage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
