package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchStatusReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_BatchCache_GenerateDCMBatchStatusReport extends BaseTestWin {

    private String _reportType = "Batch Status Report";

    private String _fileNameRPT = "batstatus.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "batstatus.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;


    private String _dateToEnter = "06122018";
    private String _dateToEnter_ReportFormat = "06/12/18";

    private BatchStatusReport_Page _batchReportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchStatusReports();

        _batchReportPage.enter_Text_IntoReceiveDate(_dateToEnter);
        _batchReportPage.enter_Text_IntoProcessDate(_dateToEnter);

        _batchReportPage.check_SaveCSV_Checkbox();
        _batchReportPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
        _batchReportPage = _batchReportPage.runReport();


        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }



    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_batstat_Report_Date() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_batstat_Report_ReceiveDate() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Receive Date = " + _dateToEnter_ReportFormat;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_batstat_Report_ProcessDate() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Process Date = " + _dateToEnter_ReportFormat;

        BaseUI.baseStringPartialCompare("Process Date", expectedBatchDateValue, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_batstat_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+"," ");
        String expectedReportName = "Report: batstat" ;

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_batstat_Report_Title() throws Exception {
        String expectedTitle = "Batch Status Reports";
        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, _reportExtract[0]);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2330_BatchStatusReport_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found CSV Data.", "Did NOT find CSV Data.");

        SoftAssert softAssert = new SoftAssert();

        for(HashMap<String, String> row : _csvData.data){
            String batchDate = row.get("Batch Date");
            String processDate = row.get("Process Date");
            softAssert.assertTrue(batchDate.equals(_dateToEnter_ReportFormat), "Seeing Batch Date of " + batchDate);
            softAssert.assertTrue(processDate.equals(_dateToEnter_ReportFormat), "Seeing Process Date of " + processDate);
        }

        softAssert.assertAll();
    }



    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_batchReportPage != null) {
            _batchReportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
