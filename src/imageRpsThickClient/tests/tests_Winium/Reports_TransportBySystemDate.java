package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.TransportDetailBySystemDate_Page;
import imageRpsThickClient.winiumPages.TransportSummaryBySystemDate_Report_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_TransportBySystemDate extends BaseTestWin {

    private String _fileNameRPT_Summary = "strpt6.rpt";
    private String _fileNameRPT_FileLocation_Summary = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT_Summary;
    private String _fileNameCSV_Summary = "strpt6.csv";
    private String _fileNameCSV_FileLocation_Summary = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV_Summary;

    private String _fileNameRPT_Detail = "strpt3.rpt";
    private String _fileNameRPT_FileLocation_Detail = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT_Detail;
    private String _fileNameCSV_Detail = "strpt3.csv";
    private String _fileNameCSV_FileLocation_Detail = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV_Detail;

    private String _eventDate_From = "01012014";
    private String _eventDate_From_RPTFormat = "01/01/14";

    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");

    private TransportSummaryBySystemDate_Report_Page _transportSummaryReportPage;
    private TransportDetailBySystemDate_Page _transportDetailsReportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation_Summary);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation_Summary);

        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation_Detail);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation_Detail);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _transportSummaryReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_TransportPerformanceStatisticsReports()
                .navigate_TransportSummaryBySystemDate();

        _transportSummaryReportPage.check_SaveCSV_Checkbox();
        _transportSummaryReportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT_Summary);
        _transportSummaryReportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV_Summary);

        _transportSummaryReportPage.enter_Dates(_eventDate_From, _eventDate_To);

        _transportSummaryReportPage = _transportSummaryReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation_Summary)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation_Summary, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation_Summary)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_ReportDate() throws Exception {
        _transportSummaryReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_NoErrorInListBox() throws Exception {
        _transportSummaryReportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Transport Statistics Summary Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: tpsum";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_DateOnLine2() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = _eventDate_From_RPTFormat + " - " + _eventDate_To_RPTFormat;

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _transportSummaryReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2212_TransportSummaryBySystemDate_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _transportSummaryReportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"Begin Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2211_TransportDetailBySystemDate_2nd_Setup() throws Exception {
        _transportDetailsReportPage = _transportSummaryReportPage.navigate_BackTo_TransportPerformanceStatisticsReports()
                .navigate_TransportDetailBySystemDate();

        _transportDetailsReportPage.check_SaveCSV_Checkbox();
        _transportDetailsReportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT_Detail);
        _transportDetailsReportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV_Detail);

        _transportDetailsReportPage.enter_Dates(_eventDate_From, _eventDate_To);
        _transportDetailsReportPage.check_PrintDetail_Checkbox();
        _transportDetailsReportPage = _transportDetailsReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation_Detail)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation_Detail, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation_Detail)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_ReportDate() throws Exception {
        _transportSummaryReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_NoErrorInListBox() throws Exception {
        _transportSummaryReportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Transport Statistics Detail Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: tpsum";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_DateOnLine2() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = _eventDate_From_RPTFormat + " - " + _eventDate_To_RPTFormat;

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _transportSummaryReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2211_TransportDetailBySystemDate_2nd_Setup")
    public void PT2211_TransportDetailBySystemDate_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _transportSummaryReportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"Begin Date"});
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_transportSummaryReportPage != null) {
            _transportSummaryReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
