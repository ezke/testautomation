package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.ACH_Reports_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_ACH_Statistics_Tests extends BaseTestWin {

    private String _fileNameRPT = "arcrpt2.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "arcrpt2.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;
    private int _expectedDateCount = 6;
    private ACH_Reports_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _fromDate_Website = "01/01/2001";
    private String _fromDate_RPT = "01/01/01";
    private String _throughDate_Website = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String _throughDate_RPT = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ACHStatisticsMenu()
                .navigate_ACHStatisticsReportMenu()
                .navigate_DailyStatisticsReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);

        _reportPage._failIfNoData_Checkbox.check();
        _reportPage._fromProcessingDate_TextBox.enterText(_fromDate_Website);
        _reportPage._throughProcessingDate_TextBox.enterText(_throughDate_Website);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_DefaultClientFilter() throws Exception {
        _reportPage._clientFilter_Dropdown.verify_ItemSelected("All Clients");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_NoErrorInErrorListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_Report_DateRange() throws Exception {
        _reportPage.verify_RPT_Line2DateRange(_reportExtractRPT, _fromDate_RPT, _throughDate_RPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_Report_CSV_EachEntry_FallsInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _fromDate_RPT, _throughDate_RPT, new String[]{"Stat Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2237_DailyStatisticsReport_Validate_CSV_AllColumnsHaveHeader() throws Exception {
        _reportPage.verify_ColumnHeaders_AllHaveValue(_csvExtract[0]);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2236_SummaryStatisticsReport_2ndSetup() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = _reportPage.navigate_BackTo_ACHStatisticsReportsMenu()
                .navigate_SummaryStatisticsReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);

        _reportPage._failIfNoData_Checkbox.check();
        _reportPage._fromProcessingDate_TextBox.enterText(_fromDate_Website);
        _reportPage._throughProcessingDate_TextBox.enterText(_throughDate_Website);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_DefaultClientFilter() throws Exception {
        _reportPage._clientFilter_Dropdown.verify_ItemSelected("All Clients");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_NoErrorInErrorListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_Report_DateRange() throws Exception {
        _reportPage.verify_RPT_Line2DateRange(_reportExtractRPT, _fromDate_RPT, _throughDate_RPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_Report_CSV_EachEntry_FallsInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _fromDate_RPT, _throughDate_RPT, new String[]{"Stat Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2236_SummaryStatisticsReport_2ndSetup")
    public void PT2236_SummaryStatisticsReport_Validate_CSV_AllColumnsHaveHeader() throws Exception {
        _reportPage.verify_ColumnHeaders_AllHaveValue(_csvExtract[0]);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 30)
    public void PT2240_ReturnCodeStatisticsReport_3rdSetup() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = _reportPage.navigate_BackTo_ACHStatisticsReportsMenu()
                .navigate_ReturnCodeStatisticsReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);

        _reportPage._failIfNoData_Checkbox.check();
        _reportPage._fromProcessingDate_TextBox.enterText(_fromDate_Website);
        _reportPage._throughProcessingDate_TextBox.enterText(_throughDate_Website);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_DefaultClientFilter() throws Exception {
        _reportPage._clientFilter_Dropdown.verify_ItemSelected("All Clients");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_NoErrorInErrorListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_Report_DateRange() throws Exception {
        _reportPage.verify_RPT_Line2DateRange(_reportExtractRPT, _fromDate_RPT, _throughDate_RPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_Report_CSV_EachEntry_FallsInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _fromDate_RPT, _throughDate_RPT, new String[]{"Stat Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2240_ReturnCodeStatisticsReport_3rdSetup")
    public void PT2240_ReturnCodeStatisticsReport_Validate_CSV_AllColumnsHaveHeader() throws Exception {
        _reportPage.verify_ColumnHeaders_AllHaveValue(_csvExtract[0]);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 40)
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = _reportPage.navigate_BackTo_ACHStatisticsReportsMenu()
                .navigate_ACHReasonByReturnCodeStatisticsRpt();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);

        _reportPage._failIfNoData_Checkbox.check();
        _reportPage._fromProcessingDate_TextBox.enterText(_fromDate_Website);
        _reportPage._throughProcessingDate_TextBox.enterText(_throughDate_Website);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_DefaultClientFilter() throws Exception {
        _reportPage._clientFilter_Dropdown.verify_ItemSelected("All Clients");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_NoErrorInErrorListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_Report_DateRange() throws Exception {
        _reportPage.verify_RPT_Line2DateRange(_reportExtractRPT, _fromDate_RPT, _throughDate_RPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_Report_CSV_EachEntry_FallsInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _fromDate_RPT, _throughDate_RPT, new String[]{"Stat Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2239_ACHReasonByReturnCodeStatisticsRpt_4thSetup")
    public void PT2239_ACHReasonByReturnCodeStatisticsRpt_Validate_CSV_AllColumnsHaveHeader() throws Exception {
        _reportPage.verify_ColumnHeaders_AllHaveValue(_csvExtract[0]);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 50)
    public void PT2238_ACHReasonStatisticsReport_5thSetup() throws Exception {
        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = _reportPage.navigate_BackTo_ACHStatisticsReportsMenu()
                .navigate_ACHReasonStatisticsReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);

        _reportPage._failIfNoData_Checkbox.check();
        _reportPage._fromProcessingDate_TextBox.enterText(_fromDate_Website);
        _reportPage._throughProcessingDate_TextBox.enterText(_throughDate_Website);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_DefaultClientFilter() throws Exception {
        _reportPage._clientFilter_Dropdown.verify_ItemSelected("All Clients");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_NoErrorInErrorListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_Report_DateRange() throws Exception {
        _reportPage.verify_RPT_Line2DateRange(_reportExtractRPT, _fromDate_RPT, _throughDate_RPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_Report_CSV_EachEntry_FallsInRange() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _fromDate_RPT, _throughDate_RPT, new String[]{"Stat Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 51,
            dependsOnMethods = "PT2238_ACHReasonStatisticsReport_5thSetup")
    public void PT2238_ACHReasonStatisticsReport_Validate_CSV_AllColumnsHaveHeader() throws Exception {
        _reportPage.verify_ColumnHeaders_AllHaveValue(_csvExtract[0]);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
