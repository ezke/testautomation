package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchDelayReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.*;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Reports_BatchCache_GenerateDCMBatchDelayReport extends BaseTestWin {

    private String _fileNameRPT = "batdelay.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "batdelay.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _hrsDelay = "2hrs";

    private BatchDelayReport_Page _batchDelayPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchDelayPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchDelayReport();

        _batchDelayPage.check_SaveCSV_Checkbox();
        _batchDelayPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\batdelay.csv");
        _batchDelayPage = _batchDelayPage.runReport();



        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_BatchAge_DefaultValue() throws Exception {
        String daysOld = _batchDelayPage.getAge();

        BaseUI.baseStringCompare("Batch Age", _hrsDelay, daysOld);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_batage_Report_Date() throws Exception {
        _batchDelayPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_batage_Report_Summary() throws Exception {
        int expectedDateCount = 6;
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Batches that are still incomplete after: " + _hrsDelay;

        BaseUI.baseStringPartialCompare("Report Summary", expectedBatchDateValue, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_batage_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+"," ");
        String expectedReportName = "Report: batdelay";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_batage_Report_Title() throws Exception {
        String expectedTitle = "Batch Delay Report";
        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, _reportExtract[0]);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchDelayPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" })
    public void PT2328_BatchDelayReport_Validate_CSV_DurationsWereGreaterThanExpectedAmount() throws Exception {
        _batchDelayPage.validateTime_GreaterThanGivenDuration(_csvData, "Batch Delay", _hrsDelay);
    }



    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchDelayPage != null) {
            _batchDelayPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
