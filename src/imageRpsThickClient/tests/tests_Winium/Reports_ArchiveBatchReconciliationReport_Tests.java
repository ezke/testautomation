package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.io.File;

public class Reports_ArchiveBatchReconciliationReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "archreconca.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "archreconca.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private ArchiveBatchReconciliationReport_Page _reportPage;

    private String[] _reportExtract;
    private TableData _reportTable;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _stationDropdownOption = "on Station ID";
    private String _stationID = "8";
    private String _archiveSystem = "R360";

    private String _locationName = "Processing Center";

    private int _stationCount_OnStation;
    private int _locationCount;

    private String _stationName = "*SC* 180e";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_ArchiveBatchReconciliationReport();

        _reportPage.check_SaveCSV_Checkbox();

        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage._archiveSystem_ComboBox.select_ComboBoxItem(_archiveSystem);
        _reportPage._stationFilter_ComboBox.select_ComboBoxItem(_stationDropdownOption);
        _reportPage._stationFilter_TextBox.enterText(_stationID);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _reportTable = _reportPage.get_RPT_ReportTable(_reportExtract);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _stationCount_OnStation = _csvData.data.size();
        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_ReportTitle(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_CSV_HasValues() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("cap_stn_id", _stationID, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_GrandTotals_TotalCheckAmount() throws Exception {
        _reportPage.verify_GrandTotals_TotalCheckAmount(_reportExtract, _reportTable);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2339_ArchiveBatchReconciliationReport_Validate_GrandTotals_TotalEntries() throws Exception {
        _reportPage.verify_GrandTotals_TotalEntries(_reportExtract, _reportTable);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2340_2ndSetup_ArchiveBatchReconciliationReport_Navigate() throws Exception {

        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _reportPage._stationFilter_ComboBox.select_ComboBoxItem("on Location");
        _reportPage._stationFilter_TextBox.enterText(_locationName);
        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _reportTable = _reportPage.get_RPT_ReportTable(_reportExtract);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");
        _locationCount = _csvData.data.size();

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2340_2ndSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2340_ArchiveBatchReconciliationReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2340_2ndSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2340_ArchiveBatchReconciliationReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_ReportTitle(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2340_2ndSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2340_ArchiveBatchReconciliationReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21,
            dependsOnMethods = "PT2340_2ndSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2340_ArchiveBatchReconciliationReport_Validate_CSV_HasValues() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("location_name", _locationName, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 30)
    public void PT2338_3rdSetup_ArchiveBatchReconciliationReport_Navigate() throws Exception {

        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _reportPage._stationFilter_ComboBox.select_ComboBoxItem("All Stations");
        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _reportTable = _reportPage.get_RPT_ReportTable(_reportExtract);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2338_3rdSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2338_ArchiveBatchReconciliationReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2338_3rdSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2338_ArchiveBatchReconciliationReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_ReportTitle(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2338_3rdSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2338_ArchiveBatchReconciliationReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2338_3rdSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2338_ArchiveBatchReconciliationReport_Validate_CSV_CountsHigherThanOtherTests() throws Exception {
        int currentCount = _csvData.data.size();
        BaseUI.verify_true_AndLog((currentCount > _locationCount && currentCount > _stationCount_OnStation),
                "Counts of All Stations filter was greater than previous tests.",
                "Counts of All Stations filter was NOT greater than previous tests, seeing " + String.valueOf(currentCount));
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2338_3rdSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2338_ArchiveBatchReconciliationReport_Validate_AllStations_TextBox_Disabled() throws Exception {
        _reportPage._stationFilter_TextBox.verify()
                .displayed()
                .disabled();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 40)
    public void PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate() throws Exception {

        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _reportPage._stationFilter_ComboBox.select_ComboBoxItem("on Name");
        _reportPage._stationFilter_TextBox.enterText(_stationName);
        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _reportTable = _reportPage.get_RPT_ReportTable(_reportExtract);

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2341_ArchiveBatchReconciliationReport_Validate_Report_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2341_ArchiveBatchReconciliationReport_Validate_Report_Title() throws Exception {
        _reportPage.verify_RPT_ReportTitle(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2341_ArchiveBatchReconciliationReport_Validate_Report_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract, _archiveSystem);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2341_ArchiveBatchReconciliationReport_Validate_RPT_AllStations_Match() throws Exception {
        _reportPage.verify_RPT_AllStations_HaveName(_reportExtract, _stationName);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2341_ArchiveBatchReconciliationReport_Validate_GrandTotals_TotalCheckAmount() throws Exception {
        _reportPage.verify_GrandTotals_TotalCheckAmount(_reportExtract, _reportTable);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 41,
            dependsOnMethods = "PT2341_4thSetup_ArchiveBatchReconciliationReport_Navigate")
    public void PT2341_ArchiveBatchReconciliationReport_Validate_GrandTotals_TotalEntries() throws Exception {
        _reportPage.verify_GrandTotals_TotalEntries(_reportExtract, _reportTable);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
