package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.SuspenseAgingReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.*;

import java.io.File;
import java.util.ArrayList;

public class Reports_GenerateDCM_SuspenseAgingReport extends BaseTestWin {

    private String _fileNameRPT = "susage.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "susage.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _startDate = "01012017";
    private String _startTime;
    private String _startDate_RPTReport = "01/01/17";
    private String _endDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");
    private String _endTime;
    private String _endDate_RPTReport = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    private String _docID = "1";
    private int _expectedDateCount = 6;

    private SuspenseAgingReport_Page _batchReportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _reportType_Run1 = "Process Date";
    private String _reportType_AfterUpdate = "Consol Date";

    private String _sortOption = "Orig Batch ID";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_SuspenseAgingReport_Page();

        _batchReportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table(_sortOption);
        _batchReportPage._outputAndRunOptions.select_ReportSortOrder_Item(_sortOption);
        _batchReportPage._outputAndRunOptions.click_Top();
        _batchReportPage.check_SaveCSV_Checkbox();

        _batchReportPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
        _batchReportPage = _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_DaysOld_DefaultValue() throws Exception {
        String expectedDaysOld = "00" + String.valueOf(_expectedDateCount);
        String daysOld = _batchReportPage.get_DaysOld();

        BaseUI.baseStringCompare("Days Old", expectedDaysOld, daysOld);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_ReportDateType_DefaultValue() throws Exception {
        String expectedProcessDate = "Process Date";
        _batchReportPage._reportDateType_ComboBox.verify_ItemSelected(expectedProcessDate);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_Report_Date() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_Report() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "ImageRPS Suspense Aging Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_DateOnLine2() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Batch Date = " + String.valueOf(_expectedDateCount) + " days old or older";

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: susage";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }


    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_Report_CSV_EachEntry_OlderThanExpectedDays() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found Table Data", "Did NOT find table data.");
        _batchReportPage.validate_AllDates_BeforeCutoff(_csvData, _expectedDateCount);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2337_SuspenseAgingReport_Validate_RPT_OrigNumber_SortedAscending() throws Exception {
        _batchReportPage.verify_BatchOrigID_Sorted_Ascending(_reportExtract);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 2)
    public void PT2337_SuspenseAgingReport_Validate_CSV_UpdateToReportType_SortedDescending() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _batchReportPage._outputAndRunOptions.select_ReportSortOrder_Item(_sortOption);
        _batchReportPage._outputAndRunOptions.click_Ascending_Or_Descending();
        _batchReportPage._outputAndRunOptions.click_Top();

        _batchReportPage = _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _batchReportPage.verify_BatchOrigID_Sorted_Descending(_reportExtract);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchReportPage != null) {
            _batchReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
