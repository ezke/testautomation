package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.MainMenu_Page;
import imageRpsThickClient.winiumPages.MaintainQueue_Page;
import imageRpsThickClient.winiumPages.RecognitionStatsReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.FileOperations;
import utils.ResultWriter;

public class WORS_Singles_CompareReadRates extends BaseTestWin {

    private String remote2_WORS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\Results";
    private MaintainQueue_Page maintainQueue_page;
    private RecognitionStatsReport_Page recognitionStatsReport_page;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);
        WORS.windowsService().start();

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        maintainQueue_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .run_ProcedureEditor()
                .navigate_RunImageRPS()
                .navigate_QueueAndRecognitionMenu()
                .navigate_MaintainQueue();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT2664_UnlockRow_Verify_WORS_FileFound_InResultsFolder() throws Exception {

        String batchToSelect = "Wausau Orbo Recognition (WOR) - Batch 410690";
        String worsr_File = "410690.WORSR";

        maintainQueue_page.unlockAndRunBatch_ByDescription(batchToSelect);

        FileOperations.wait_ForFileCount_ToMatch(remote2_WORS_Exports_Results, 1, 300);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_WORS_Exports_Results, worsr_File);
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT2664_Compare_WORS_Values_ResultsFolder_And_ReportsFolder() throws Exception {

        String remote2_WORS_Test_Reports = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\reports";
        String statsFromDate = "03/12/2015";
        String throughDate = "03/12/2015";
        String recoVendorName = "WORS";
        String saveFilePath = "k:\\cust1\\Test\\reports\\recostata_410690.rpt";

        MainMenu_Page mainMenu_Page = maintainQueue_page.navigate_BackTo_QueueAndRecognitionMenuPage()
                .navigate_BackTo_MainMenuPage();

        recognitionStatsReport_page = mainMenu_Page.navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_RecognitionStatsReport();
        recognitionStatsReport_page.enterStatsFromDate(statsFromDate);
        recognitionStatsReport_page.enterThroughDate(throughDate);
        recognitionStatsReport_page._recoVendorComboBox.select_ComboBoxItem(recoVendorName);
        recognitionStatsReport_page.enter_SaveToFile(saveFilePath);
        recognitionStatsReport_page.run_Report();

        String reportsFile = "recostata_410690.rpt";
        String resultsFile = "410690.WORSR";
        WORS.compare_WORS_Values_From_ResultFolder_And_ReportFolder_Singles(
                remote2_WORS_Exports_Results + "\\" + resultsFile,
                remote2_WORS_Test_Reports + "\\" + reportsFile);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {

        WORS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

        recognitionStatsReport_page.exitApplication();
    }
}
