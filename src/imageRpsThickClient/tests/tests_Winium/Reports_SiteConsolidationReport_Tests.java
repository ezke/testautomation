package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.pages.BasePage;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.SiteConsolidationReport_Page;
import imageRpsThickClient.winiumPages.SiteDepositReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_SiteConsolidationReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "conrpt5.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "conrpt5.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private SiteConsolidationReport_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _consolidationFromDate = "01/01/2001";
    private String _consolidationFromDate_RPT = "01/01/01";

    private String _consolidationThruDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
    private String _consolidationThruDate_RPT = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    private String _consolidationFailDate = "11/10/2018";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_SiteReportsMenu()
                .navigate_SiteConsolidationReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage._consolFromDate.enterText(_consolidationFailDate);
        _reportPage._consolThruDate.enterText(_consolidationFailDate);
        _reportPage._batchID_Radio.check();
        _reportPage._failIfNoData_Checkbox.check();

        _reportPage.runReport_WhenNoReportPossible();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2231_SiteConsolidationReport_Validate_ErrorWhenDateHasNoResults() throws Exception {
        _reportPage.verify_NoReportFound();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2231_SiteConsolidationReport_2ndSetup_ValidDate() throws Exception {
        _reportPage.click_OK_ForFailure();
        _reportPage._consolFromDate.enterText(_consolidationFromDate);
        _reportPage._consolThruDate.enterText(_consolidationThruDate);
        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2231_SiteConsolidationReport_2ndSetup_ValidDate")
    public void PT2231_SiteConsolidationReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2231_SiteConsolidationReport_2ndSetup_ValidDate")
    public void PT2231_SiteConsolidationReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Consolidation Total Items Report", line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2231_SiteConsolidationReport_2ndSetup_ValidDate")
    public void PT2231_SiteConsolidationReport_Validate_RPT_Date_Range() throws Exception {
        String line3 = _reportExtractRPT[2].replaceAll("\\s+", " ");

        String expectedValue = "Consol Dates: " + _consolidationFromDate_RPT + " thru " + _consolidationThruDate_RPT;
        BaseUI.baseStringPartialCompare("Consol Dates", expectedValue, line3);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2231_SiteConsolidationReport_2ndSetup_ValidDate")
    public void PT2231_SiteConsolidationReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: conrptb";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2231_SiteConsolidationReport_2ndSetup_ValidDate")
    public void PT2231_SiteConsolidationReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2231_SiteConsolidationReport_2ndSetup_ValidDate")
    public void PT2231_SiteConsolidationReport_Validate_CSV_ConsolidationDatesAccurate() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _consolidationFromDate_RPT, _consolidationThruDate_RPT, new String[]{"Consol Date"});
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
