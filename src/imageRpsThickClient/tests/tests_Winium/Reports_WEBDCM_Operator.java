package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.WEBDCM_Operator_Reports_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_WEBDCM_Operator extends BaseTestWin {

    private String _fileNameRPT_Summary = "strpt21.rpt";
    private String _fileNameRPT_FileLocation_Summary = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT_Summary;

    private String _fileNameCSV_Summary = "strpt21.csv";
    private String _fileNameCSV_FileLocation_Summary = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV_Summary;

    private String _fileNameRPT_Detail = "strpt11.rpt";
    private String _fileNameRPT_FileLocation_Detail = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT_Detail;

    private String _fileNameCSV_Detail = "strpt11.csv";
    private String _fileNameCSV_FileLocation_Detail = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV_Detail;

    private String _eventDate_From = "01012014";
    private String _eventDate_From_RPTFormat = "01/01/14";

    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");

    private WEBDCM_Operator_Reports_Page _reportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String[] _jobNames_1stRun = {"cca", "sca", "cc2", "ex$", "exc"};
    private String[] _jobName_Run2 = {"act"};

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation_Summary);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation_Summary);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_WebPerformanceStatisticsReports()
                .navigate_Operator_SummaryBySystemDate();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT_Summary);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV_Summary);

        _reportPage.check_PrintDetail_Checkbox();
        _reportPage.enter_Dates(_eventDate_From, _eventDate_To);
        _reportPage.enter_Jobs(_jobNames_1stRun);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation_Summary)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation_Summary, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation_Summary)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Web Operator Stats Summary";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_DateOnLine2() throws Exception {
        _reportPage.verify_Line2Date(_reportExtract, _eventDate_From_RPTFormat + " 12:00 AM",
                _eventDate_To_RPTFormat + " 11:59 PM");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _reportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"Date"});
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2214_WEBDCMOperatorSummary_Validate_AllJobs() throws Exception {
        _reportPage.verify_JobIDs_OnlyContainJobs_FromSelected(_csvData, _jobNames_1stRun);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2213_WEBDCMOperatorDetail_2ndSetup() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation_Detail);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation_Detail);

        _reportPage = _reportPage.navigate_BackTo_WebPerformanceStatisticsReports_Page()
                .navigate_Operator_DetailBySystemDate();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT_Detail);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV_Detail);

        _reportPage.check_PrintDetail_Checkbox();
        _reportPage.enter_Dates(_eventDate_From, _eventDate_To);
        _reportPage.enter_Jobs(_jobName_Run2);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation_Detail)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation_Detail, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation_Detail)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
        dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Web Operator Stats Detail";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_ReportType() throws Exception {
        _reportPage.verify_ReportType(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_DateOnLine2() throws Exception {
        _reportPage.verify_Line2Date(_reportExtract, _eventDate_From_RPTFormat + " 12:00 AM",
                _eventDate_To_RPTFormat + " 11:59 PM");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _reportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2213_WEBDCMOperatorDetail_2ndSetup")
    public void PT2213_WEBDCMOperatorDetail_Validate_AllJobs() throws Exception {
        _reportPage.verify_JobIDs_OnlyContainJobs_FromSelected(_csvData, _jobName_Run2);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation_Summary);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation_Summary);
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
