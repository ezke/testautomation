package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_GenerateDCM_BatchCaptureReconciliationReport extends BaseTestWin {

    private String _fileNameRPT = "strptbr.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "strptbr.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _dateToReport;
    private String _dateToReport_RPTFormat;

    private BatchCapture_ReconciliationReport_Page _batchReportPage;
    private BatchMenu_Page _batchMenuPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _reportDateType = "Process Date";
    private String _typeOfReport = "Detail Report";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        String day = BaseUI.getDateAsString_InRelationToTodaysDate(0, "E");
        int numberOfDasAgo_DateToUse = -1;
        if(day.equals("Mon")){
            numberOfDasAgo_DateToUse = -4;
        }
        _dateToReport = BaseUI.getDateAsString_InRelationToTodaysDate(numberOfDasAgo_DateToUse, "MMddyyyy");
        _dateToReport_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(numberOfDasAgo_DateToUse, "MM/dd/yy");

        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchCaptureReconciliationReport();

        _batchReportPage.check_SaveCSV_Checkbox();
        _batchReportPage._reportDateType.select_ComboBoxItem("Process Date");

        _batchReportPage._dateToReport_TextBox.enterText(_dateToReport);

        _batchReportPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
        _batchReportPage = _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_TypeOfReport_DefaultValue() throws Exception {
        _batchReportPage._typeOfReport.verify_ItemSelected(_typeOfReport);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_Report_Date() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_Report_Title() throws Exception {
        String line1 = _reportExtract[0];
        String expectedBatchDateValue = "ImageRPS Capture Batch Reconciliation Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedBatchDateValue, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_Report_Line2Date() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Batch " + _reportDateType + " = " + _dateToReport_RPTFormat;

        BaseUI.baseStringPartialCompare("Receive Date", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: strptbr";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchReportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2336_BatchReconciliationReport_Validate_CSV_HasValues() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found CSV data.", "Did NOT find CSV Data.");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 2)
    public void PT2336_2ndSetup_BatchCaptureReconciliationReport_Navigate_BatchMenu() throws Exception {
        _batchMenuPage = _batchReportPage.navigate_BackTo_ReportWizardsPage()
                .navigate_BackTo_ReportsMenuPage()
                .navigate_BackTo_MainMenuPage()
                .navigate_DataMaintenanceMenu()
                .navigate_BatchMenu();
    }

    @DataProvider(name = "test_Dates")
    public Object[][] GenerateTests_ForAutomations() {

        Object[][] scenariosToRun = new Object[_csvData.data.size()][1];

        int indexToPlace_ColumnHeader = 0;
        for (HashMap<String, String> row : _csvData.data) {
            scenariosToRun[indexToPlace_ColumnHeader][0] = Integer.parseInt(row.get("Batch ID"));
            indexToPlace_ColumnHeader++;
        }

        return scenariosToRun;
    }

    @Test(dataProvider = "test_Dates", alwaysRun = true, groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 3,
        dependsOnMethods = "PT2336_2ndSetup_BatchCaptureReconciliationReport_Navigate_BatchMenu")
    public void PT2336_BatchReconciliationReport_Validate_ProcDate_MatchesSelectedDate(int batchID) throws Exception {
        Batch_QueryResults_Page batchQueryResults = null;
        ViewBatchesByBatchID_Page viewBatches = _batchMenuPage.navigate_ViewBatchesByBatchID();
        try {

            viewBatches.enter_BatchID_LowerRange(batchID);
            batchQueryResults = viewBatches.navigate_BatchQueryResult();

            TableData batchQueryData = batchQueryResults.get_BatchAndDates();
            BaseUI.baseStringCompare(_reportDateType, _dateToReport_RPTFormat, batchQueryData.data.get(0).get("ProcDate"));
        } finally {
            _batchMenuPage = batchQueryResults.navigate_BackTo_BatchMenuPage();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchReportPage != null) {
            _batchReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
