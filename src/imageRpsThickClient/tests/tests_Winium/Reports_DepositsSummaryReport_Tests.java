package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.SiteDepositReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_DepositsSummaryReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "deprpt3.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "deprpt3.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private SiteDepositReport_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;

    private String _depositDate = "01/15/2018";
    private String _depositDateRPT = "01/15/18";

    private String _failDate = "11/13/2018";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_SiteReportsMenu()
                .navigate_SiteDepositReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage._depositDate.enterText(_failDate);
        _reportPage._failIfNoData_Checkbox.check();

        _reportPage.runReport_WhenNoReportPossible();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2229_DepositsSummaryReport_Validate_ErrorWhenDateHasNoResults() throws Exception {
        _reportPage.verify_NoReportFound();
    }



    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2229_DepositsSummaryReport_2ndSetup_ValidDate() throws Exception {
        _reportPage.click_OK_ForFailure();
        _reportPage._depositDate.enterText(_depositDate);
        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2229_DepositsSummaryReport_2ndSetup_ValidDate")
    public void PT2229_DepositsSummaryReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2229_DepositsSummaryReport_2ndSetup_ValidDate")
    public void PT2229_DepositsSummaryReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Deposits Summary Report", line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2229_DepositsSummaryReport_2ndSetup_ValidDate")
    public void PT2229_DepositsSummaryReport_Validate_RPT_Date_Range() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");

        String expectedValue = "Deposit Date: " + _depositDateRPT;
        BaseUI.baseStringPartialCompare("Date Range", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2229_DepositsSummaryReport_2ndSetup_ValidDate")
    public void PT2229_DepositsSummaryReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: deprpt";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2229_DepositsSummaryReport_2ndSetup_ValidDate")
    public void PT2229_DepositsSummaryReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2229_DepositsSummaryReport_2ndSetup_ValidDate")
    public void PT2229_DepositsSummaryReport_Validate_CSV_DepositDatesAccurate() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Deposit Date", _depositDateRPT, _csvData.data);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
