package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.AllJobsSummaryByProcessDate_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_AllJobsSummaryByProcessDate extends BaseTestWin {

//    private DcmLoginCredentials _loginCredentials;
    private String _fileNameRPT = "st20.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "st20.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _eventDate_From = "01012014";
    private String _eventDate_From_RPTFormat = "01/01/14";

    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");

    private AllJobsSummaryByProcessDate_Page _reportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private TableData _rptReportTable;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

//        _loginCredentials = loadEnvironmentData();
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_OperatorPerformanceStatisticsReports()
                .navigate_AllJobsSummaryByProcessDate();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage.check_ShowDailyDetail_Checkbox();
        _reportPage.enter_Dates(_eventDate_From, _eventDate_To);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Statistics Summary Report for All Jobs";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: OpSum2B";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_DateOnLine2() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = _eventDate_From_RPTFormat + " - " + _eventDate_To_RPTFormat;

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2207_AllJobsSummaryByProcessDate_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _reportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT5238_AllJobsSummaryByProcessDate_Validate_RPT_KilledCount_ColumnDisplayed() throws Exception {
        _reportPage.verify_ColumnName(_reportExtract, Range.between(131, 141), "Killed Stub Cnt");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT5238_AllJobsSummaryByProcessDate_Validate_RPT_KilledStubKeyed_ColumnDisplayed() throws Exception {
        _reportPage.verify_ColumnName(_reportExtract, Range.between(141, 149), "Killed/Keyed");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT5238_AllJobsSummaryByProcessDate_Validate_RPT_KilledCheckCount_ColumnDisplayed() throws Exception {
        _reportPage.verify_ColumnName(_reportExtract, Range.between(149, 159), "Killed Check Cnt");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT5238_AllJobsSummaryByProcessDate_Validate_RPT_KilledCheckKeyed_ColumnDisplayed() throws Exception {
        _reportPage.verify_ColumnName(_reportExtract, Range.between(159, 167), "Killed/Keyed");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT5238_AllJobsSummaryByProcessDate_Validate_RPT_KilledTranCount_ColumnDisplayed() throws Exception {
        _reportPage.verify_ColumnName(_reportExtract, Range.between(168, 177), "Killed Tran Cnt");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT5238_AllJobsSummaryByProcessDate_2ndSetup() throws Exception {
        _reportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table("Client ID");
        _reportPage._outputAndRunOptions.select_ReportSortOrder_Item("Client ID");
        _reportPage._outputAndRunOptions.click_Top();
        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");

        _rptReportTable = _reportPage.get_RPT_Table(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT5238_AllJobsSummaryByProcessDate_2ndSetup")
    public void PT5238_AllJobsSummaryByProcessDate_Validate_RPT_SortedCorrectly() throws Exception {
        TableData rptTableExpected = _rptReportTable.cloneTable();

        rptTableExpected.sort_ByColumn_Ascending("JobAcronym");
        rptTableExpected.sort_ByColumn_numeric_Ascending("Client ID");

        BaseUI.verify_TableColumns_Match("Job", rptTableExpected.data, _rptReportTable.data);
        BaseUI.verify_TableColumns_Match("Client ID", rptTableExpected.data, _rptReportTable.data);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
