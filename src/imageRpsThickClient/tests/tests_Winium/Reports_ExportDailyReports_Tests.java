package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

public class Reports_ExportDailyReports_Tests extends BaseTestWin {

    private ExportDailyTransportStatisticsFileReport_Page _dailyTransport_ReportPage;
    private ExportDailyOperatorStatisticsFileReport_Page _dailyOperator_ReportPage;
    private ExportStatisticsMenu_Page _exportStatisticsMenu_page;
    private ExportDailyTotalItemsFile_Page _dailyTotalItemsFile_ReportPage;

    private String _transport_SystemDate = "02032015";
    private String _transport_FileName = _transport_SystemDate + ".tpt";
    private String _transport_FileWithPath = GlobalVariables.default_Local_ReportsLocation + "\\" + _transport_FileName;

    private String _operator_ProcessDate = "06122017";
    private String _operator_FileName = _operator_ProcessDate + ".sts";
    private String _operator_FileWithPath = GlobalVariables.default_Local_ReportsLocation + "\\" + _operator_FileName;

    private String _dailyTotalItemsFile_Date = "11012018";
    private String _dailyTotalItemsFile_FileName = _dailyTotalItemsFile_Date + ".tot";
    private String _dailyTotalItemsFile_FileWithPath = GlobalVariables.default_Local_ReportsLocation + "\\" + _dailyTotalItemsFile_FileName;


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_transport_FileWithPath);
        FileOperations.delete_SpecificFile(_operator_FileWithPath);
        FileOperations.delete_SpecificFile(_dailyTotalItemsFile_FileWithPath);

        _dailyTransport_ReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_ExportStatisticsMenu()
                .navigate_ExportDailyTransportStatisticsFile();

        _dailyTransport_ReportPage.enterDate(_transport_SystemDate);
        _dailyTransport_ReportPage.clickOK();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2217_ExportDailyTransportStatisticsFile_Validate_ModalAccurate() throws Exception {
        _dailyTransport_ReportPage.verify_StatExport_ModalAppears(_transport_FileWithPath);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2217_ExportDailyTransportStatisticsFile_Validate_FileExists() throws Exception {
        FileOperations.verify_FileFound(GlobalVariables.default_ReportsLocation, _transport_FileName);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2216_ExportDailyOperatorStatisticsFile_Validate_ModalAccurate() throws Exception {
        _dailyOperator_ReportPage = _dailyTransport_ReportPage.click_OK_OnModal_Navigate_ExportStatisticsMenu()
                .navigate_ExportDailyOperatorStatisticsFile();
        _dailyOperator_ReportPage.enterDate(_operator_ProcessDate);
        _dailyOperator_ReportPage.clickOK();
        _dailyOperator_ReportPage.verify_StatExport_ModalAppears(_operator_FileWithPath);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2216_ExportDailyOperatorStatisticsFile_Validate_FileExists() throws Exception {
        FileOperations.verify_FileFound(GlobalVariables.default_ReportsLocation, _operator_FileName);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 30)
    public void PT2215_ExportDailyTotalItemsFile_Validate_ModalAccurate() throws Exception {
        _dailyTotalItemsFile_ReportPage = _dailyOperator_ReportPage.click_OK_OnModal_Navigate_ExportStatisticsMenu()
                .navigate_ExportDailyTotalItemsFile();
        _dailyTotalItemsFile_ReportPage.enterDate(_dailyTotalItemsFile_Date);
        _dailyTotalItemsFile_ReportPage.clickOK();
        _dailyTotalItemsFile_ReportPage.verify_StatExport_ModalAppears(_dailyTotalItemsFile_FileWithPath);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2215_ExportDailyTotalItemsFile_Validate_FileExists() throws Exception {
        FileOperations.verify_FileFound(GlobalVariables.default_ReportsLocation, _dailyTotalItemsFile_FileName);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        try {
            _exportStatisticsMenu_page = _dailyOperator_ReportPage.click_OK_OnModal_Navigate_ExportStatisticsMenu();
            _exportStatisticsMenu_page.exitApplication();
        }catch (Exception e){
            new DCMBasePage().exitApplication();
        }
    }
}
