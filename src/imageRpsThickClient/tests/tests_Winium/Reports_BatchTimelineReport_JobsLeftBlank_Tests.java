package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_BatchTimelineReport_JobsLeftBlank_Tests extends BaseTestWin {

    private String _fileNameRPT = "battimeline.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "battimeline.csv";
    private String _csv_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private BatchTimelineReport_Page _reportPage;
    private BatchMenu_Page _batchMenuPage;

    private String[] _reportExtractRPT;
    private TableData _csvData;
    private String[] _csvExtract;


    private String _dateFrom ="07/16/2018";
    private String _dateFrom_RPTFormat = "07/16/18";
    private String _dateFrom_CSVFormat = "07/16/2018 01:01:01";

    private String _dateTo = "07/17/2018";
    private String _dateTo_RPTFormat = "07/17/18";
    private String _dateTo_CSVFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "07/17/2018 23:00:00");


    private String _fromJob = "p1";
    private String _toJob = "don";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);
        FileOperations.delete_SpecificFile(_csv_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchTimelineReport();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);
        _reportPage.enter_Dates(_dateFrom, _dateTo);

        _reportPage = _reportPage.runReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _csv_CompletePath, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_csv_CompletePath)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5116_BatchTimelineReport_Validate_ReportDate() throws Exception {
        _reportPage._reportDateType.verify_ItemSelected("Process Date");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5116_BatchTimelineReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5116_BatchTimelineReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Batch Timeline Report", line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5116_BatchTimelineReport_Validate_RPT_Date_Range() throws Exception {
        _reportPage.verify_DateRange_Line2(_reportExtractRPT, "Process Date", _dateFrom_RPTFormat, _dateTo_RPTFormat);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5116_BatchTimelineReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report: btimeline";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=1)
    public void PT5116_BatchTimelineReport_Validate_CSV_DateColumns() throws Exception {
        _reportPage.validate_DatesFallInRange(_csvData, _dateFrom_CSVFormat, _dateTo_CSVFormat,
                new String[]{"Start Job Date and Time","End Job Date and Time"}, "MM/dd/yyyy HH:mm:ss");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority=2)
    public void PT5116_BatchTimelineReport_2ndSetup() throws Exception {
        _batchMenuPage = _reportPage.navigate_BackTo_ReportWizardsPage()
                .navigate_BackTo_ReportsMenuPage()
                .navigate_BackTo_MainMenuPage()
                .navigate_DataMaintenanceMenu()
                .navigate_BatchMenu();
    }


    @DataProvider(name = "test_Dates")
    public Object[][] GenerateTests_ForAutomations() throws Exception {

        Object[][] scenariosToRun = new Object[_csvData.data.size()][5];

        int indexToPlace_ColumnHeader = 0;
        for (HashMap<String, String> row : _csvData.data) {
            scenariosToRun[indexToPlace_ColumnHeader][0] = Integer.parseInt(row.get("Batch ID"));
            scenariosToRun[indexToPlace_ColumnHeader][1] = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(row.get("Start Job Date and Time"),
                    "MM/dd/yyyy HH:mm:ss",
                    "MM/dd/yy HH:mm:ss", 0);
            scenariosToRun[indexToPlace_ColumnHeader][2] = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(row.get("End Job Date and Time"),
                    "MM/dd/yyyy HH:mm:ss",
                    "MM/dd/yy HH:mm:ss", 0);
            scenariosToRun[indexToPlace_ColumnHeader][3] = row.get("Start Job");
            scenariosToRun[indexToPlace_ColumnHeader][4] = row.get("End Job");


            indexToPlace_ColumnHeader++;
        }

        return scenariosToRun;
    }

    @Test(dataProvider = "test_Dates", alwaysRun = true, groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 3,
            dependsOnMethods = "PT5116_BatchTimelineReport_2ndSetup")
    public void PT5116_BatchTimelineReport_Validate_DatesMatchReport(int batchID, String startDateTime, String endDateTime, String startJob, String endJob) throws Exception {

        GeneralBatchInformation_Page generalBatchInformation_page = null;
        ViewBatchesByBatchID_Page viewBatches = _batchMenuPage.navigate_ViewBatchesByBatchID();
        try {

            viewBatches.enter_BatchID_LowerRange(batchID);
            generalBatchInformation_page = viewBatches.navigate_BatchQueryResult()
                    .navigate_SingleBatchOperations_BySelectingFirstEntry()
                    .navigate_GeneralBatchInformation();


            TableData queryData = generalBatchInformation_page.get_AuditTrailTable();
            startJob = startJob.length() > 4 ? startJob.substring(0, 4) : startJob;
            endJob = endJob.length() > 4 ? endJob.substring(0,4) : endJob;
            
            String p1StartTime = queryData.return_Row_BasedOn_1MatchingField("Job", startJob).get("Started");
            String donEndTime = generalBatchInformation_page.return_Row_BasedOn_1MatchingField(queryData, "Job", endJob).get("Ended");

            //Currently a bug with bad data so these validations are going to fail.
            BaseUI.baseStringCompare("Start Date/Time", startDateTime, p1StartTime );
            BaseUI.baseStringCompare("End Date/Time", endDateTime, donEndTime);

        } finally {
            _batchMenuPage = generalBatchInformation_page
                    .navigate_BackTo_SingleBatchOperations_Page()
                    .navigate_BackTo_BatchQueryResults()
                    .navigate_BackTo_BatchMenuPage();
        }
    }



    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
