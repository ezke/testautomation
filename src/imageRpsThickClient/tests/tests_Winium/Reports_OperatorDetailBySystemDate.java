package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.OperatorDetailBySystemDate_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_OperatorDetailBySystemDate extends BaseTestWin {

    private String _fileNameRPT = "strpt1.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "strpt1.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _eventDate_From = "01012017";
    private String _eventDate_From_RPTFormat = "01/01/17";



    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");

    private OperatorDetailBySystemDate_Page _reportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _operatorID = "autotest";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_StatisticsMenu()
                .navigate_OperatorPerformanceStatisticsReports()
                .navigate_OperatorDetailBySystemDatePage();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage._operatorFilter_Dropdown.select_ComboBoxItem("on Op ID");
        _reportPage.enter_OperatorFilter_TextBox("dominic");
        _reportPage.enter_DateRange(_eventDate_To, _eventDate_To);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2202_OperatorDetailBySystemDate_Validate_FailIfNoDataChecked() throws Exception {
        _reportPage._outputAndRunOptions.verify_FailIfNoData_Checked();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2202_OperatorDetailBySystemDate_Validate_BadDates_Throws_ReportException() throws Exception {
        _reportPage.runReport_WhenNoReportPossible();
        _reportPage.verify_NoReportFound();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 30)
    public void PT2202_OperatorDetailBySystemDate_2ndSetup() throws Exception {
        _reportPage = _reportPage.click_OK_ForError();
        _reportPage.enter_DateRange(_eventDate_From, _eventDate_To);
        _reportPage.enter_OperatorFilter_TextBox(_operatorID);
        _reportPage = _reportPage.runReport();
        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_ReportTitle() throws Exception {
        String line1 = _reportExtract[0];
        String expectedTitle = "Statistics Detail Report for an Operator";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: OpDetail";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_DateOnLine2() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "Start: " + _eventDate_From_RPTFormat + " 12:00 AM - End: " + _eventDate_To_RPTFormat + " 11:59 PM";

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        _reportPage.validate_DatesFallInRange(
                _csvData, _eventDate_From_RPTFormat, _eventDate_To_RPTFormat, new String[]{"Date"});
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31,
            dependsOnMethods = "PT2202_OperatorDetailBySystemDate_2ndSetup")
    public void PT2202_OperatorDetailBySystemDate_Validate_CSV_AllEntriesMatchOperator() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Operator ID", _operatorID, _csvData.data);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
