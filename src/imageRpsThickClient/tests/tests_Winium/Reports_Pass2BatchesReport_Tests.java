package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.Pass2BatchesReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_Pass2BatchesReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "pass2batch.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;
    private String _fileNameCSV = "pass2batch.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;


    private String _eventDate_From = "01012014";
    private String _eventDate_From_RPTFormat = "01/01/14";

    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");
    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");


    private Pass2BatchesReport_Page _reportPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    private String _job = "p2";


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);


        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_WorkFlowReportsMenu()
                .navigate_Pass2BatchesReport_SortedByClient();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage.check_ListIndividualBatches_Checkbox();
        _reportPage.check_BatchID_Radio();
        _reportPage.enter_Job(_job);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_ReportTitle() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_DateOnLine2() throws Exception {
        _reportPage.verify_RPT_Receive_And_ProcessDate_All(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 1)
    public void PT2220_SortedByClient_Validate_KeyingJob() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Job", _job, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2221_SortByStation_Setup() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _reportPage = _reportPage.navigate_BackTo_WorkFlowReportsMenu()
                .navigate_Pass2BatchesReport_SortedByStation();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage.check_ListIndividualBatches_Checkbox();
        _reportPage.check_BatchID_Radio();
        _reportPage.enter_Job(_job);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
        dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_ReportTitle() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_DateOnLine2() throws Exception {
        _reportPage.verify_RPT_Receive_And_ProcessDate_All(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 11,
            dependsOnMethods = "PT2221_SortByStation_Setup")
    public void PT2221_SortedByStation_Validate_KeyingJob() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Job", _job, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2222_SortByRCVDate_Setup() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _reportPage = _reportPage.navigate_BackTo_WorkFlowReportsMenu()
                .navigate_Pass2BatchesReport_SortedByRCVDate();

        _reportPage.check_SaveCSV_Checkbox();
        _reportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _reportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameCSV);

        _reportPage.check_ListIndividualBatches_Checkbox();
        _reportPage.check_BatchID_Radio();
        _reportPage.enter_Job(_job);

        _reportPage = _reportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.remove_Character("\"");

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_ReportDate() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_NoErrorInListBox() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_ReportTitle() throws Exception {
        _reportPage.verify_RPT_Title(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_ReportType() throws Exception {
        _reportPage.verify_RPT_ReportType(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_DateOnLine2() throws Exception {
        _reportPage.verify_RPT_Receive_And_ProcessDate_All(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _reportPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 22,
            dependsOnMethods = "PT2222_SortByRCVDate_Setup")
    public void PT2222_SortedByRCVDate_Validate_KeyingJob() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("Job", _job, _csvData.data);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_reportPage != null) {
            _reportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
