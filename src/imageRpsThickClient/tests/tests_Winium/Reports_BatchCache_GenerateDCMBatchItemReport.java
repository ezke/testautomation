package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.BatchItemReport_Page;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.*;

import java.io.File;
import java.util.HashMap;

public class Reports_BatchCache_GenerateDCMBatchItemReport extends BaseTestWin {

    private String _reportType = "Batch Item Report";
    private String _reportDateType = "Process Date";

    private String _fileNameRPT = "batitm.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "batitm.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _hrsDelay = "2hrs";

    private String _systemDate_From = BaseUI.getDateAsString_InRelationToTodaysDate(-5, "MMddyyyy");
    private String _systemDate_From_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(-5, "MM/dd/yy");
    private String _systemDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");
    private String _systemDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    private BatchItemReport_Page _batchItemPage;

    private String[] _reportExtract;

    private TableData _csvData;
    private String[] _csvExtract;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchItemPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_BatchItemReports();

        _batchItemPage.enter_SystemDate_From(_systemDate_From);
        _batchItemPage.enter_SystemDate_To(_systemDate_To);

        _batchItemPage.check_SaveCSV_Checkbox();
        _batchItemPage.enter_SaveToFile("k:\\cust1\\Test\\reports\\" + _fileNameRPT);
        _batchItemPage.enter_CSV_Name("k:\\cust1\\Test\\reports\\" + _fileNameCSV);
        _batchItemPage = _batchItemPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);

        _csvExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameCSV_FileLocation)).split("\\n");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_Default_ReportType() throws Exception {
        _batchItemPage.reportType.verify_ItemSelected(_reportType);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_Default_ReportDateType() throws Exception {
        _batchItemPage.reportDateType.verify_ItemSelected(_reportDateType);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_batitm_Report_Date() throws Exception {
        _batchItemPage.validate_ReportDate_Accurate(_reportExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_batitm_Report_Summary() throws Exception {
        String line2 = _reportExtract[1];
        String expectedBatchDateValue = "For " + _reportDateType + "(s): " + _systemDate_From_RPTFormat + " - " + _systemDate_To_RPTFormat;

        BaseUI.baseStringPartialCompare("Report Summary", expectedBatchDateValue, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_batitm_Report_ReportType() throws Exception {
        String line2 = _reportExtract[1].replaceAll("\\s+", " ");
        String expectedReportName = "Report: batitm";

        BaseUI.baseStringPartialCompare("Report Type", expectedReportName, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_batitm_Report_Title() throws Exception {
        String expectedTitle = "Batch Items Report";
        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, _reportExtract[0]);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_CSV_AllColumnsUnique() throws Exception {
        String[] headerColumns = _csvExtract[0].split("\\,");

        _batchItemPage.verify_ColumnHeaders_EachValueUnique(headerColumns);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"})
    public void PT2329_BatchItemReport_Validate_CSV_DateColumnFallsWithinRange() throws Exception {
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found CSV Data.", "Did NOT find CSV Data.");

        for (HashMap<String, String> row : _csvData.data) {
            String dateToCheck = row.get(_reportDateType);
            BaseUI.verify_Date_IsBetween_DateRange(_systemDate_From_RPTFormat, _systemDate_To_RPTFormat, dateToCheck, "MM/dd/yy");
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_batchItemPage != null) {
            _batchItemPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
