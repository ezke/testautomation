package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;
import utils.winiumXRay.XRayUtils;

import java.io.File;
import java.util.HashMap;

public class Reports_ExceptionItemHitReport_Tests extends BaseTestWin {

    private String _fileNameRPT = "exceptionReport.rpt";
    private String _rpt_CompletePath = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private ExceptionItemHitReport_Printer_TempFile_Page _reportPage;

    private String[] _reportExtractRPT;
    private TableData _rptTable;
    private TableData _eipItemTable;

    private String _dateToEnter = "05/29/16";

    private int _clientID = 20;
    private String _clientName = "20 - Bevent Insurance";


    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        FileOperations.delete_SpecificFile(_rpt_CompletePath);

        _reportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ClientReportsMenu()
                .navigate_ExceptionItemHitReport_SelectAClient()
                .navigate_Report_DateEntry(_clientID)
                .enterDate(_dateToEnter)
                .selectTempFile();

        _reportPage.outputTextBox.enterText(_rpt_CompletePath);
        _reportPage.clickOK_DontViewReport();

        _reportExtractRPT = DataBuilder.Get_FileData_AsString(
                new File(_rpt_CompletePath)).split("\\n");
        _rptTable = _reportPage.getRPT_Table(_reportExtractRPT);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2226_ExceptionItemHitReport_Validate_RPT_Date() throws Exception {
        _reportPage.validate_ReportDate_Accurate(_reportExtractRPT);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2226_ExceptionItemHitReport_Validate_ErrorListEmpty() throws Exception {
        _reportPage.verify_ErrorListBox_Empty();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2226_ExceptionItemHitReport_Validate_RPT_Title() throws Exception {
        String line1 = _reportExtractRPT[0];
        BaseUI.baseStringPartialCompare("Title", "Exception Item Hit Report", line1);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2226_ExceptionItemHitReport_Validate_RPT_ClientName() throws Exception {
        String line2 = _reportExtractRPT[1];
        BaseUI.baseStringPartialCompare("Client Name", "Client: " + _clientName, line2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2226_ExceptionItemHitReport_Validate_RPT_Date_Line3() throws Exception {
        String line3 = _reportExtractRPT[2].replaceAll("\\s+", " ");
        String expectedValue = "Date: " + _dateToEnter;
        BaseUI.baseStringPartialCompare("Date Line 3", expectedValue, line3);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "report_Tests" }, priority = 1)
    public void PT2226_ExceptionItemHitReport_Validate_RPT_ReportType() throws Exception {
        String line2 = _reportExtractRPT[1].replaceAll("\\s+", " ");
        String expectedValue = "Report:eip1";
        BaseUI.baseStringPartialCompare("Report Type", expectedValue, line2);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_SortAccountNumber() throws Exception {
        GenericTableBrowser _genericTableBrowser = UtilityMenu_Page.navigate_UtilityMenu().navigate_GenericTableBrowser();
        _genericTableBrowser.enter_TableQuery("eip_item");
        _eipItemTable = XRayUtils.return_Table(new String[]{"Batch Date", "Batch ID", "Batch#", "client_id", "EIP Data", "Scanline", "rej_reason", "Sort Acct", "Seq"});
        _genericTableBrowser.exitApplication();

        BaseUI.verify_TableColumns_Match("Sort Account Number", "Sort Acct", _rptTable.data, _eipItemTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_And_ReportSize() throws Exception {
        BaseUI.verify_true_AndLog(_rptTable.data.size() == 1, "Found Report Data", "Did NOT find Report Data");
        BaseUI.verify_true_AndLog(_rptTable.data.size() == _eipItemTable.data.size(), "Tables Matched in size", "Tables did NOT match in size");
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_BatchID() throws Exception {
        BaseUI.verify_TableColumns_Match("BatchID", "Batch ID", _rptTable.data, _eipItemTable.data);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_BatchNumber() throws Exception {
        BaseUI.verify_TableColumns_Match("Batch#", "Batch#", _rptTable.data, _eipItemTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_P1SeqNumber() throws Exception {
        BaseUI.verify_TableColumns_Match("P1Seq#", "Seq", _rptTable.data, _eipItemTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_RejectReason() throws Exception {
        BaseUI.verify_TableColumns_Match("Reject Reason", "rej_reason", _rptTable.data, _eipItemTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_EIPData() throws Exception {
        BaseUI.verify_TableColumns_Match("EIP Data", "EIP Data", _rptTable.data, _eipItemTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 21)
    public void PT2226_ExceptionItemHitReport_Validate_EIP_Column_Matches_RPT_Scanline() throws Exception {
          BaseUI.verify_TableColumns_Match("Scanline", "Scanline", _rptTable.data, _eipItemTable.data);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
       
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if (_reportPage != null) {
            _reportPage.exitApplication();
        } else {
            new DCMBasePage().exitApplication();
        }
    }
}
