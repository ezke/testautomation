package imageRpsThickClient.tests.tests_Winium;

import imageRPS.data.ImageRPS_Utilities;
import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.ImageViewer_Winium.ImageViewerApp_Page;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.FileOperations;
import utils.ResultWriter;

import java.time.Duration;

public class WORS_RetryLogic_tmp_orbo_Folder extends BaseTestWin {

    private String remote2_tmp_orbo_Folder;
    private String kitbmp_File = "kitbmp.dll";
    private String hsfil32_File = "hsfil32.dll";
    private String hsoftw32_File = "hsoftw32.dll";
    private String iviewEXE_File = "iview.exe";
    private String batch = "Wausau Orbo Recognition (WOR) - Batch 410650";
    private QueueStatusConsol_Page queueStatusConsol_page;
    private QueueAndRecognition_MenuPage queueAndRecognition_menuPage;
    private MaintainQueue_Page maintainQueue_page;
    private QueueItem_Page queueItem_page;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WORS.windowsService().stop();
        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        queueStatusConsol_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_QueueAndRecognitionMenu()
                .navigate_QueueStatusConsol();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT2670_Verify_WORS_QueueItem_ErrorMessage() throws Exception {

        remote2_tmp_orbo_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\C$\\tmp\\" + "DCcust1Test" + _loginCredentials.stationId() + "\\tmp_orbo\\410650";
        String remote2_SystemFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\DCMCode\\Test\\system";

        FileOperations.cleanup_PriorFiles(remote2_tmp_orbo_Folder);

        queueStatusConsol_page.check_NoQueues();

        FileOperations.create_Folder(remote2_tmp_orbo_Folder);
        FileOperations.copyFile(remote2_SystemFolder + "\\" + iviewEXE_File,
                remote2_tmp_orbo_Folder + "\\" + iviewEXE_File);
        FileOperations.copyFile(remote2_SystemFolder + "\\" + kitbmp_File,
                remote2_tmp_orbo_Folder + "\\" + kitbmp_File);
        FileOperations.copyFile(remote2_SystemFolder + "\\" + hsfil32_File,
                remote2_tmp_orbo_Folder + "\\" + hsfil32_File);
        FileOperations.copyFile(remote2_SystemFolder + "\\" + hsoftw32_File,
                remote2_tmp_orbo_Folder + "\\" + hsoftw32_File);

        ImageRPS_Utilities.runCmdFile2(remote2_tmp_orbo_Folder + "\\iview.exe");
        queueAndRecognition_menuPage = queueStatusConsol_page.navigate_BackTo_QueueAndRecognitionMenuPage();
        maintainQueue_page = queueAndRecognition_menuPage.navigate_MaintainQueue();
        maintainQueue_page.unlockAndRunBatch_ByDescription(batch);
        maintainQueue_page.wait_For_ERRColumn_ToBeUpdated(batch, Duration.ofSeconds(300), "X");
        queueItem_page = maintainQueue_page.navigate_QueueItem_ByBatchDescription(batch);
        String errorMessage = queueItem_page.return_ErrorMessage();
        String expectedMessage = " Unable to delete directory C:\\TMP\\DCcust1Test" + _loginCredentials.stationId() + "\\tmp_orbo\\410650 after 3 attempts. Review log file for errors";
        BaseUI.baseStringCompare("ErrorMessage", expectedMessage, errorMessage);
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT2670_Verify_WORS_temp_orbo_Folders() throws Exception {

        FileOperations.wait_ForFileCount_ToMatch(remote2_tmp_orbo_Folder, 4, 90);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_tmp_orbo_Folder, kitbmp_File);
        FileOperations.verify_FileFound(remote2_tmp_orbo_Folder, hsfil32_File);
        FileOperations.verify_FileFound(remote2_tmp_orbo_Folder, hsoftw32_File);
        FileOperations.verify_FileFound(remote2_tmp_orbo_Folder, iviewEXE_File);
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT2670_Verify_WORS_temp_orbo_Folders_Empty() throws Exception {

        new ImageViewerApp_Page().kill_IviewProcess();
        queueItem_page.unCheck_ErrorsWhenRun();
        queueItem_page.unCheck_QueueRecordIsLocked();
        maintainQueue_page = queueItem_page.navigate_BackTo_MaintainQueue();
        maintainQueue_page.runSelectedBatch(batch);
        queueAndRecognition_menuPage = maintainQueue_page.navigate_BackTo_QueueAndRecognitionMenuPage();
        queueStatusConsol_page = queueAndRecognition_menuPage.navigate_QueueStatusConsol();
        queueStatusConsol_page.unCheck_NoQueues();
        FileOperations.verify_FolderNotFound(remote2_tmp_orbo_Folder);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {

        queueStatusConsol_page.exitApplication();
    }
}
