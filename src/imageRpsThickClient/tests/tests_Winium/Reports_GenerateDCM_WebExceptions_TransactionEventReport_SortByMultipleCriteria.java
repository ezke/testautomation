package imageRpsThickClient.tests.tests_Winium;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.DCMBasePage;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.WebExceptions_TransactionEventReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.io.File;

public class Reports_GenerateDCM_WebExceptions_TransactionEventReport_SortByMultipleCriteria extends BaseTestWin {

    private String _fileNameRPT = "webtrarpt.rpt";
    private String _fileNameRPT_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameRPT;

    private String _fileNameCSV = "webtrarpt.csv";
    private String _fileNameCSV_FileLocation = GlobalVariables.default_ReportsLocation + "\\" + _fileNameCSV;

    private String _fileNamePDF = "webrarpt.pdf";

    private String _eventDate_From = "01012014";
    private String _eventDate_From_RPTFormat = "01/01/14";

    private String _eventDate_To = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMddyyyy");
    private String _eventDate_To_RPTFormat = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yy");

    private WebExceptions_TransactionEventReport_Page _batchReportPage;

    private String[] _reportExtract;
    private TableData _rptTable;
    private TableData _csvData;

    private String[] _pdfExtract;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        _batchReportPage = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_WebExceptions_TransactionEventReport();

        _batchReportPage.check_SaveCSV_Checkbox();
        _batchReportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table("Batch ID");
        _batchReportPage.enter_EventDates(_eventDate_From, _eventDate_To);

        _batchReportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" + _fileNameRPT);
        _batchReportPage.enter_CSV_Name(GlobalVariables.default_Local_ReportsLocation + "\\" +  _fileNameCSV);
        _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _csvData = DataBuilder.returnTableData_ForComparison(
                _fileNameCSV_FileLocation, "\\,", false);
        _csvData.trim_column("Client Label");
        _csvData.remove_Character("\"");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 10)
    public void PT2351_WebTransReport_Validate_RPT_SortedAscending_ByBatchID_And_ClientID() throws Exception {
        _rptTable = _batchReportPage.get_RPT_Table(_reportExtract);
        BaseUI.verify_true_AndLog(_rptTable.data.size() > 0, "Found Report Data.", "Did NOT find Report Data.");

        TableData reportToCheckAgainst = _rptTable.cloneTable();
        reportToCheckAgainst.sort_ByColumn_numeric_Ascending("Batch ID");
        reportToCheckAgainst.sort_ByColumn_numeric_Ascending("Client ID");

        BaseUI.verify_TableColumns_Match("Client ID", reportToCheckAgainst.data, _rptTable.data);
        BaseUI.verify_TableColumns_Match("Batch ID", reportToCheckAgainst.data, _rptTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 15)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_TransID() throws Exception {
        _rptTable.sort_ByColumn_numeric_Ascending("Trans ID");

        _csvData.sort_ByColumn_numeric_Ascending("Batch ID");
        _csvData.sort_ByColumn_numeric_Ascending("Client ID");
        _csvData.sort_ByColumn_numeric_Ascending("Trans ID");

        BaseUI.verify_TableColumns_Match("Trans ID", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_BatchID() throws Exception {
        BaseUI.verify_TableColumns_Match("Batch ID", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_ClientID() throws Exception {
        BaseUI.verify_TableColumns_Match("Client ID", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_TimeStamp() throws Exception {
        BaseUI.verify_TableColumns_Match("TimeStamp", "Event Date", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_ClientAndLockbox() throws Exception {
        BaseUI.verify_TableColumns_Match("ClientAndLockbox", "Client Label", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_OldState() throws Exception {
        BaseUI.verify_TableColumns_Match("Old State", "oldState", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_NewState() throws Exception {
        BaseUI.verify_TableColumns_Match("New State", "newState", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 16)
    public void PT2351_WebTransReport_Validate_CSV_Column_Matches_RPT_Operator() throws Exception {
        BaseUI.verify_TableColumns_Match("Operator ID", "Operator", _rptTable.data, _csvData.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 20)
    public void PT2353_WebTransReport_Validate_SortBy_ClientID_And_Operator() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _batchReportPage._outputAndRunOptions.remove_Option_From_ReportSortOrder("Batch ID");
        _batchReportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table("Operator");

        _batchReportPage =  _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _rptTable = _batchReportPage.get_RPT_Table(_reportExtract);

        BaseUI.verify_true_AndLog(_rptTable.data.size() > 0, "Found Report Data.", "Did NOT find Report Data.");

        TableData reportToCheckAgainst = _rptTable.cloneTable();
        reportToCheckAgainst.sort_ByColumn_Ascending("Operator ID");
        reportToCheckAgainst.sort_ByColumn_numeric_Ascending("Client ID");

        BaseUI.verify_TableColumns_Match("Client ID", reportToCheckAgainst.data, _rptTable.data);
        BaseUI.verify_TableColumns_Match("Operator ID", reportToCheckAgainst.data, _rptTable.data);

    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 25)
    public void PT2352_WebTransReport_Validate_SortBy_ClientID_And_EventDate() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);

        _batchReportPage._outputAndRunOptions.remove_Option_From_ReportSortOrder("Operator");
        _batchReportPage._outputAndRunOptions.add_Option_From_OutputAndRunOptions_Table("Event Date");

        _batchReportPage =  _batchReportPage.runReport();

        _reportExtract = DataBuilder.Get_FileData_AsString(
                new File(_fileNameRPT_FileLocation)).split("\\n");

        _rptTable = _batchReportPage.get_RPT_Table(_reportExtract);

        BaseUI.verify_true_AndLog(_rptTable.data.size() > 0, "Found Report Data.", "Did NOT find Report Data.");

        TableData reportToCheckAgainst = _rptTable.cloneTable();
        reportToCheckAgainst.sort_ByColumn_Ascending("TimeStamp");
        reportToCheckAgainst.sort_ByColumn_numeric_Ascending("Client ID");

        BaseUI.verify_TableColumns_Match("Client ID", reportToCheckAgainst.data, _rptTable.data);
        BaseUI.verify_TableColumns_Match("TimeStamp", reportToCheckAgainst.data, _rptTable.data);

    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 30)
    public void PT2354_WebTransReport_Validate_PDF_Generated_AndSortedCorrectly() throws Exception {
        FileOperations.delete_SpecificFile(_fileNameRPT_FileLocation);
        FileOperations.delete_SpecificFile(_fileNameCSV_FileLocation);
        FileOperations.delete_SpecificFile(GlobalVariables.default_ReportsLocation + "\\" + _fileNamePDF);

        _batchReportPage.enter_SaveToFile(GlobalVariables.default_Local_ReportsLocation + "\\" +  _fileNamePDF);

        _batchReportPage =  _batchReportPage.runReport();
        _pdfExtract = _batchReportPage.getPDF_Extract(GlobalVariables.default_ReportsLocation + "\\" + _fileNamePDF);
        TableData pdfTable = _batchReportPage.getPDF_Data(_pdfExtract);

        BaseUI.verify_TableColumns_Match("Client ID", pdfTable.data, _rptTable.data);
        BaseUI.verify_TableColumns_Match("Operator ID", pdfTable.data, _rptTable.data);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2354_WebTransReport_Validate_PDF_ReportTitle() throws Exception {
        String line1 = _pdfExtract[0];
        String expectedTitle = "Web Exceptions - Transaction Event Report";

        BaseUI.baseStringPartialCompare("Report Title", expectedTitle, line1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2354_WebTransReport_Validate_PDF_Date() throws Exception {
        _batchReportPage.validate_ReportDate_Accurate(_pdfExtract);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2354_WebTransReport_Validate_PDF_ReportType() throws Exception {
        String line2 = _pdfExtract[1].replaceAll("\\s+", " ");
        String expectedType = "Report: webtran";

        BaseUI.baseStringPartialCompare("Report Type", expectedType, line2);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "report_Tests"}, priority = 31)
    public void PT2354_WebTransReport_Validate_PDF_DateOnLine2() throws Exception {
        String line2 = _pdfExtract[1];
        String expectedBatchDateValue = "Event Date(s): " + _eventDate_From_RPTFormat + " thru " + _eventDate_To_RPTFormat;

        BaseUI.baseStringPartialCompare("Date on Line 2", expectedBatchDateValue, line2);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(_batchReportPage != null) {
            _batchReportPage.exitApplication();
        }else{
            new DCMBasePage().exitApplication();
        }
    }
}
