package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.services.FRS;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class FRS_Singles_MultiResult_LAR_Enabled {

	private String remote2_FRSExports_TestData_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\job_Files\\TestJobs";
	private String fileNameToMove = "500331.frsj";

	private String remote2_FRS_Exports_jobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\job_Files";
	private String remote2_FRS_Export_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\Results";

	private String remote2_FRS_Queue_files = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\queuefiles";

	private String remote2_FRS_Export_TestAutomationBatches_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\FRS-TestAutomationBatches\\Test5201";

	private String frsr_File = "500331.FRSR";
	private String lar_xml_File = "LARUsage_01.xml";
	private String larr_File = "LARUsage_01.LARR";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		FRS.windowsService().stop();
		// WFS Reco ServiceTest-602

		FileOperations.cleanup_PriorFiles(remote2_FRS_Export_Results);
		FileOperations.copyFile(remote2_FRSExports_TestData_Folder + "\\" + fileNameToMove,
				remote2_FRS_Exports_jobFiles + "\\" + fileNameToMove);

		FRS.windowsService().start();

		FileOperations.wait_ForFileCount_ToMatch(remote2_FRS_Export_Results, 2, 50);
		Thread.sleep(500);

	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT5201_Verify_frsr_AND_xml_ExistIn_Remote2_FRS_Results() throws Exception {

		FileOperations.verify_FileCount_Correct(remote2_FRS_Export_Results, 2);
		FileOperations.verify_FileFound(remote2_FRS_Export_Results, frsr_File);
		FileOperations.verify_FileFound(remote2_FRS_Export_Results, lar_xml_File);
	}

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT5201_Verify_frsj_RemovedFrom_JobFiles() throws Exception {
		FileOperations.verify_FileName_NotFound(remote2_FRS_Exports_jobFiles, fileNameToMove);
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT5201_Verify_frsr_AND_xml_Found_In_FRS_queuefiles() throws Exception {
		FileOperations.verify_FileFound(remote2_FRS_Queue_files, frsr_File);
		FileOperations.verify_FileFound(remote2_FRS_Queue_files, larr_File);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests", "service_Tests" })
	public void PT5201_Verify_FRSR_Matches_BetweenResults_And_FRSTestAutomationBatches() throws Exception {
		FRS.verify_FRSR_FilesMatch(remote2_FRS_Export_TestAutomationBatches_Folder + "\\" + frsr_File,
				remote2_FRS_Export_Results + "\\" + frsr_File);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		FRS.windowsService().stop();

		FileOperations.cleanup_PriorFiles(remote2_FRS_Export_Results);

	}

}// end of class
