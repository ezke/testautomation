package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.RecognitionStatsReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class FRS_DCMReporting_ByDate extends BaseTestWin {

    private RecognitionStatsReport_Page recognitionStatsReport_page;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        recognitionStatsReport_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_RecognitionStatsReport();
    }

    @Test(groups = {"all_Tests", "regression_Tests"})
    public void PT1719_Validate_FRS_Reporting_ByDate() throws Exception {

        String statsFromDate = "03/16/2016";
        String throughDate = "03/16/2016";
        String recoVendorName = "FRS";

        recognitionStatsReport_page.enterStatsFromDate(statsFromDate);
        recognitionStatsReport_page.enterThroughDate(throughDate);
        recognitionStatsReport_page._recoVendorComboBox.select_ComboBoxItem(recoVendorName);
        recognitionStatsReport_page.run_Report();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        recognitionStatsReport_page.exitApplication();
    }

}//End of class
