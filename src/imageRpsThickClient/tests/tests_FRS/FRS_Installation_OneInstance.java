package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.services.FRS_Winium.BaseTestWin_FRS;
import imageRpsThickClient.services.FRS_Winium.pages.FRSAdminApp_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;
import java.io.File;

public class FRS_Installation_OneInstance extends BaseTestWin_FRS {

    private FRSAdminApp_Page frsAdminApp_page = new FRSAdminApp_Page();

    String frsCleanupLocation =  "\\\\rps602auto1web\\d$\\WausauFS\\FRS";

    String frsInstallationSouceLocation =  "\\\\rps602auto1dcm\\k$\\DCMCode\\Test\\service\\FRS";
    String frsInstallationDestinationLocation = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test";
    String frsInstallationZipFile1 = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2.zip";
    String frsInstallationUnZipFile1Location = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\";
    String frsInstallationZipFile2 = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\Fiserve2310.zip";
    String frsInstallationUnZipFile2Location = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\Fiserve2310\\";
    String frsInstallationZipFile3 = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\Profiles.zip";
    String frsInstallationUnZipFile3Location = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\Profiles\\";
    String frsInstallationConfigFilePath = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\RecoService.config.xml-default";
    String frsInstallationConfigFileRename = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\RecoService.config.xml";

    String logFileDirectory = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\Logs";

    String advancedJobFilesPath = "\\\\RPS602AUTO1DCM\\Data\\cust1\\Test\\exports\\frs\\Job_Files";
    String advancedResultFilesPath = "\\\\RPS602AUTO1DCM\\Data\\cust1\\Test\\exports\\frs\\Results";
    String advancedQueueFilesPath = "\\\\RPS602AUTO1DCM\\Data\\cust1\\Test\\Queue";

    String envRootPathLocation = "\\\\RPS602AUTO1DCM\\Data\\cust1\\Test";
    String serviceLoginUserName = "qalabs\\nextgenserviceuser";
    String serviceLoginUserPwd = "Wausau#1";
    String driveMappingFolderPath= "\\\\RPS602AUTO1DCM\\Data\\cust1\\Test";
    String diagnosticsTraceFilePath = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\Logs";

    String frsServiceServerName = "\\\\rps602auto1web";
    String frsServiceName ="WFS Reco Serviceauto20test";
    String frsInstallationPathWebServer = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\RecoServiceAdmin.exe";


    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        frsAdminApp_page.stopService(frsServiceServerName, frsServiceName);
        frsAdminApp_page.deleteService(frsServiceServerName, frsServiceName);

        FileOperations.cleanup_PriorFiles(frsCleanupLocation);
        FileOperations.copyFiles(frsInstallationSouceLocation, frsInstallationDestinationLocation);
        FileOperations.unZipFiles(frsInstallationZipFile1, frsInstallationUnZipFile1Location);
        FileOperations.unZipFiles(frsInstallationZipFile2, frsInstallationUnZipFile2Location);
        FileOperations.unZipFiles(frsInstallationZipFile3, frsInstallationUnZipFile3Location);

        File oldFile = new File(frsInstallationConfigFilePath);
        File newFile = new File(frsInstallationConfigFileRename);
        oldFile.renameTo(newFile);

        File file = new File(logFileDirectory);
        if (!file.exists()) {
            file.mkdir();
        }

        config_setup_method(frsInstallationPathWebServer);

        frsAdminApp_page.openWiniumDriver();
        frsAdminApp_page.enter_EnvTextBox("auto20test");
        frsAdminApp_page.saveSetEnvName();
        frsAdminApp_page.enter_EnvGeneralInfo("auto20test");
        frsAdminApp_page.enter_EnvRootPath(envRootPathLocation);
        frsAdminApp_page.clickSaveChanges();
        frsAdminApp_page.closeWindow();
        frsAdminApp_page.openWiniumDriver();
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 10)
    public void PT1714_Verify_FRSInstallation_AdvancedCheckBox() throws Exception {
        frsAdminApp_page.checkCheckBox();
        frsAdminApp_page.verifyLabelJobFilesPathTextVisible(Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 20)
    public void PT1714_Verify_FRSInstallation_AdvancedCheckBox_Configuration() throws Exception {

        frsAdminApp_page.enter_AdvJobFilesPath(advancedJobFilesPath);
        frsAdminApp_page.enter_AdvResultFilesPath(advancedResultFilesPath);
        frsAdminApp_page.enter_AdvQueueFilesPath(advancedQueueFilesPath);

        frsAdminApp_page.clickSaveChanges();
        frsAdminApp_page.verifySaveButtonGeneralInfoEnabled(Is.equalTo(false));
    }

     @Test(groups = {"all_Tests", "regression_Tests"}, priority = 30)
     public void PT1714_Verify_FRSInstallation_DriveMappingTab() throws Exception {

         frsAdminApp_page.clickDriveMappingsTab();
         frsAdminApp_page.verifyDriveMappingTabLinkVisible(Is.equalTo(true));
     }

     @Test(groups = {"all_Tests", "regression_Tests"}, priority = 40)
     public void PT1714_Verify_FRSInstallation_DriveMappingDriveLink() throws Exception {

         frsAdminApp_page.clickAddDriveMappingLink();
         frsAdminApp_page.verifyAddDriveMappingPopupVisible(Is.equalTo(true));
     }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 50)
    public void PT1714_Verify_FRSInstallation_Configuration() throws Exception {

        frsAdminApp_page.enter_DriveMappingDrive();
        frsAdminApp_page.enter_DriveMappingFolderPath(driveMappingFolderPath);

        frsAdminApp_page.clickAddDriveMappingBtn();
        frsAdminApp_page.verifySaveButtonVisible(Is.equalTo(true));
    }

      @Test(groups = {"all_Tests", "regression_Tests"}, priority = 60)
      public void PT1714_Verify_FRSInstallation_DiagnosticsTab() throws Exception {

          frsAdminApp_page.clickDiagnosticsTab();
          frsAdminApp_page.verifydiagnosticsTxtTraceFilePathLabelVisible(Is.equalTo(true));
      }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 70)
    public void PT1714_Verify_FRSInstallation_Diagnostics_Configuration() throws Exception {

        frsAdminApp_page.enter_DiagnosticsTraceFilePath(diagnosticsTraceFilePath);

        frsAdminApp_page.clickSaveChangesDiagnostics();
        frsAdminApp_page.verifySaveButtonGeneralInfoEnabled(Is.equalTo(false));
    }

     @Test(groups = {"all_Tests", "regression_Tests"}, priority = 80)
     public void PT1714_Verify_FRSInstallation_InstallServiceLink() throws Exception {

         frsAdminApp_page.clickInstallServiceLink();
         frsAdminApp_page.verifyServiceLoginUserWindowVisible(Is.equalTo(true));
     }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 90)
    public void PT1714_Verify_FRSInstallation_SetServiceLoginWindow() throws Exception {

        frsAdminApp_page.enter_SetServiceLoginUserName(serviceLoginUserName);
        frsAdminApp_page.enter_SetServiceLoginUserPwd(serviceLoginUserPwd);
        frsAdminApp_page.enter_SetServiceLoginUserPwdConfirm(serviceLoginUserPwd);

        frsAdminApp_page.clickSetServiceLoginOkBtn();
        frsAdminApp_page.verifyServiceStatusStopped(Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 100)
    public void PT1714_Verify_FRSInstallation_TestConfigurationLink() throws Exception {

        frsAdminApp_page.clickTestConfigurationLink();
        frsAdminApp_page.verifyConfigurationResultsBoxWindow_Displayed(Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 110)
    public void PT1714_Verify_FRSInstallation_CloseTestConfigurationWindow() throws Exception {

        frsAdminApp_page.clickCloseBtnTestConfigurationWindow();
        frsAdminApp_page.verifydiagnosticsTxtTraceFilePathLabelVisible(Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 120)
     public void PT1714_Verify_FRSInstallation_StartServiceLink() throws Exception {

        frsAdminApp_page.clickStartServiceLink();
        frsAdminApp_page.verifyServiceStatusRunning(Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 130)
    public void PT1714_Verify_FRSInstallation_DumpTraceLink() throws Exception {

        frsAdminApp_page.clickDumpTraceLink();
        frsAdminApp_page.verifyDumpTracePopup_Displayed(Is.equalTo(true));
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 140)
    public void PT1714_Verify_FRSInstallation_DumpTraceWindowOk() throws Exception {

        frsAdminApp_page.clickOkBtnDumpTrace();
        FileOperations.verify_FileFound(diagnosticsTraceFilePath, "RecoInstance.1.log");
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 150)
    public void PT1714_Verify_FRSInstallation_StopServiceLink() throws Exception {

        frsAdminApp_page.clickStopServiceLink();
        frsAdminApp_page.verifyServiceStatusStopped(Is.equalTo(true));
    }

        @AfterMethod(alwaysRun = true)
         public void tearDown(ITestResult result) throws Exception {
            ResultWriter.checkForFailureAndScreenshot(result);
        }

        @AfterClass(alwaysRun = true)
          public void tearDown() throws Exception {
            new FRSAdminApp_Page().closeWindow();
        }

}//End of Class
