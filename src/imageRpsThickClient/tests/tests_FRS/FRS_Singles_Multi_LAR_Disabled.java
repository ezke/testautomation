package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.FRS;
import imageRpsThickClient.winiumPages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;
import java.util.HashMap;

public class FRS_Singles_Multi_LAR_Disabled extends BaseTestWin {

    private String source_FRS_JobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\job_Files";
    private String backup_FRS_JobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\job_Files\\\\BackupJobFiles_Singles_Multi_Lar_Disabled";

    private String fileNameToMove = "410727.frsj";

    private String destination_FRS_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\Results";
    private String backup_FRS_TestAutomationBatches_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\FRS-AutomationTestBatches\\Test6202";

    private String frsr_File = "410727.FRSR";
    static XMLParser parser;

    private MaintainQueue_Page maintainQueue_page;
    private ItemInformationMenu_Page itemInformationMenu_Page;

    final String batchID = "410727";

    final String batch_id_expected = "410727";
    final String client_id_expected = "410";
    final String doc_grp_id_expected = "42";
    final String single_expected = "65";
    final String multi_expected = "0";
    final String lar_expected = "0";

    final String batch_id_results_expected = "00410727";
    final String name_expected ="M1" ;
    final String regxp_expected = "72";
    final String value_expected = "7200";
    final String conf_expected ="100" ;

    String batch_id_actual;
    String client_id_actual;
    String doc_grp_id_actual;
    String single_actual;
    String multi_actual;
    String lar_actual;

    String name_actual;
    String regxp_actual;
    String value_actual ;
    String conf_actual;

    final String docID_expected = "1";
    final String applied_expected = "72.00";
    final String auditTrail_expected = "$1,car,FRSRECO";
    final String multiResults_expected = "$1,7200,100,M1";

    String docID_actual;
    String applied_actual;
    String auditTrail_actual;
    String multiResults_actual;

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        FRS.windowsService().stop();

        FileOperations.copyFile(backup_FRS_JobFiles + "\\" + fileNameToMove,
                source_FRS_JobFiles + "\\" + fileNameToMove);

        FileOperations.cleanup_PriorFiles(destination_FRS_Results);
        FRS.windowsService().start();

        FileOperations.wait_ForFileCount_ToMatch(destination_FRS_Results, 2, 40);

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        maintainQueue_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_QueueAndRecognitionMenu()
                .navigate_MaintainQueue();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT1737_Validate_FRS_JobFiles_From_BackupJobFiles_Singles_Multi_Lar_Disabled() throws Exception {

        parser = new XMLParser(backup_FRS_JobFiles + "\\" + fileNameToMove);

        String[] nodes_Config = {"batch_id", "client_id","doc_grp_id"};
        HashMap<String, String> hashMapValues = maintainQueue_page.getNodes(parser, "config", nodes_Config);
        batch_id_actual = hashMapValues.get("batch_id");
        client_id_actual = hashMapValues.get("client_id");
        doc_grp_id_actual = hashMapValues.get("doc_grp_id");

        String[] nodes_Items = {"single", "multi", "lar"};
        hashMapValues = maintainQueue_page.getNodes(parser, "items", nodes_Items);
        single_actual = hashMapValues.get("single");
        multi_actual = hashMapValues.get("multi");
        lar_actual = hashMapValues.get("lar");

        BaseUI.baseStringCompare("Batch_Id", batch_id_expected, batch_id_actual);
        BaseUI.baseStringCompare("Client_Id", client_id_expected, client_id_actual);

        BaseUI.baseStringCompare("Doc_Group_Id", doc_grp_id_expected, doc_grp_id_actual);
        BaseUI.baseStringCompare("Single", single_expected, single_actual);
        BaseUI.baseStringCompare("Multi", multi_expected, multi_actual);
        BaseUI.baseStringCompare("Lar", lar_expected, lar_actual);
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT1737_Validate_FRS_Results_From_FRS_AutomationTestBatches() throws Exception {

        parser = new XMLParser(destination_FRS_Results + "\\" + frsr_File);

        String[] nodes_Config = {"batch_id", "client_id" };
        HashMap<String, String> hashMapValues = maintainQueue_page.getNodes(parser, "config", nodes_Config);
        batch_id_actual = hashMapValues.get("batch_id");
        client_id_actual = hashMapValues.get("client_id");

        String[] nodes_Results = {"name", "regxp", "value", "conf" };
        hashMapValues = maintainQueue_page.getNodes(parser, "results", nodes_Results);
        name_actual = hashMapValues.get("name");
        regxp_actual = hashMapValues.get("regxp");
        value_actual = hashMapValues.get("value");
        conf_actual = hashMapValues.get("conf");

        BaseUI.baseStringCompare("Batch_Id", batch_id_results_expected, batch_id_actual);
        BaseUI.baseStringCompare("Client_Id", client_id_expected, client_id_actual);

        BaseUI.baseStringCompare("Name", name_expected, name_actual);
        BaseUI.baseStringCompare("Regxp", regxp_expected, regxp_actual);
        BaseUI.baseStringCompare("Value", value_expected, value_actual);
        BaseUI.baseStringCompare("Conf", conf_expected, conf_actual);
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT1737_Validate_AuditTrail_MultiResults_From_FRS_Results_Multi_Lar_Disabled() throws Exception {

        MainMenu_Page mainMenu_Page = maintainQueue_page.navigate_BackTo_QueueAndRecognitionMenuPage()
                .navigate_BackTo_MainMenuPage();

        itemInformationMenu_Page = mainMenu_Page.navigate_DataMaintenanceMenu()
                .navigate_BatchMenu()
                .navigate_ViewBatchesByBatchID()
                .filldata_navigate_BatchQueryResult(batchID)
                .navigate_SingleBatchOperations_BySelectingBatchID(batchID)
                .navigate_BrowseTransactions()
                .navigate_SelectATransactionToDisplay_Page_BySelectingFirstEntry()
                .navigate_SelectAnItemToDisplay_Page_BySelectingFirstEntry()
                .navigate_ItemInformationMenu_Page2();

        docID_actual = BaseUI.getTextFromField(Locator.lookupRequiredElement("auditTrail_DocId_Label"));
        applied_actual = BaseUI.getTextFromField(Locator.lookupRequiredElement("auditTrail_Applied_Label"));

        auditTrail_actual = BaseUI.getTextFromField(Locator.lookupRequiredElement("auditTrail_Label"));
        auditTrail_actual = StringUtils.getOptions(auditTrail_actual, 3);

        multiResults_actual = BaseUI.getTextFromField(Locator.lookupRequiredElement("multiResults_Label"));
        multiResults_actual = StringUtils.getOptions(multiResults_actual, 4);

        BaseUI.baseStringCompare("DocID", docID_expected, docID_actual);
        BaseUI.baseStringCompare("Applied", applied_expected, applied_actual);
        BaseUI.baseStringCompare("Audit_Trail", auditTrail_expected, auditTrail_actual);
        BaseUI.baseStringCompare("Multi_Results", multiResults_expected, multiResults_actual);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
       ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
       FRS.windowsService().stop();
       itemInformationMenu_Page.exitApplication();
    }

}//end of class
