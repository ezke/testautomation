package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.services.FRS;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;
import utils.ResultWriter;

public class FRS_Singles_WithCAR_WithIQA_Tests {

    private String remote2_FRS_Export_QueueFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\queue";
    private String remote2_FRS_Exports_TestData_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\job_Files\\TestJobs";
    private String frsj_testFile = "410521.frsj";

    private String remote2_FRS_Exports_JobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\job_Files";
    private String remote2_FRS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\results";
    private String remote2_FRS_AutomationTestBatches = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\FRS\\FRS-AutomationTestBatches";
    private String frsr_File = "410521.FRSR";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        FRS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_FRS_Exports_Results);

        FileOperations.copyFile(remote2_FRS_Exports_TestData_Folder + "\\" + frsj_testFile,
                remote2_FRS_Exports_JobFiles + "\\" + frsj_testFile);

        FRS.windowsService().start();

        FileOperations.wait_ForFileCount_ToMatch(remote2_FRS_Exports_Results, 2, 180);
    }

    @Test(priority = 20, groups = {"all_Tests", "regression_Tests"})
    public void PT1716_Step3_Verify_FRSR_File_Exists_In_ResultsFolder() throws Exception {
        FileOperations.verify_FileFound(remote2_FRS_Exports_Results, frsr_File);
    }

    @Test(priority = 30, groups = {"all_Tests", "regression_Tests"})
    public void PT1716_Step4_Verify_FRSJ_File_NotFound_In_Exports_JobFiles() throws Exception {
        FileOperations.verify_FileName_NotFound(remote2_FRS_Exports_JobFiles, frsj_testFile);
    }

    @Test(priority = 10, groups = {"all_Tests", "regression_Tests"})
    public void PT1716_Step5_Verify_FRSR_Found_In_FRS_QueueFiles() throws Exception {
        FileOperations.verify_FileFound(remote2_FRS_Export_QueueFiles, frsr_File);
    }

    @Test(priority = 40, groups = {"all_Tests", "regression_Tests"})
    public void PT1716_Step6_Verify_FRSR_File_Matches_Between_Results_And_TestBatchesFolders() throws Exception {
        FRS.verify_FRSR_FilesMatch(remote2_FRS_AutomationTestBatches + "\\" + frsr_File,
                remote2_FRS_Exports_Results + "\\" + frsr_File);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        FRS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_FRS_Exports_Results);
    }

}//end of class