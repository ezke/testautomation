package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.services.FRS;
import imageRpsThickClient.services.FRS_Winium.BaseTestWin_FRS;
import imageRpsThickClient.services.FRS_Winium.pages.FRSAdminApp_Page;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class FRS_ChangeEnvironmentPath extends BaseTestWin_FRS{

    String frsinstallationPathDCMserver = "K:\\cust1\\Test\\service\\FRS\\RecoServiceAdmin.exe";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        config_setup_method(frsinstallationPathDCMserver);
        FRS.windowsService().start();
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
}

    @Test(groups = {"all_Tests", "regression_Tests"})
        public void PT1724_Verify_FRS_ChangeEnvRootPath() throws Exception {
        WebElement envRootpath = Locator.lookupRequiredElement("envRootpath");
        BaseUI.enterText_SendKeys_WithoutClearing(envRootpath, "\\\\RPS602AUTO1DCM\\vol1");
        FRSAdminApp_Page frsAdminAppPage = new FRSAdminApp_Page();
        frsAdminAppPage.clickSave();
        BaseUI.verify_true_AndLog(frsAdminAppPage.getStatus().equals("Service Status: Restarting"), "Service Started Successfully", "FRS Didn't Restart the Service");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        new FRSAdminApp_Page().closeWindow();
    }
}

