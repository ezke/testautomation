package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.MainMenu_Page;
import imageRpsThickClient.winiumPages.RecognitionStatsReport_Page;
import imageRpsThickClient.services.FRS;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class FRS_DCMReporting_ChecksOnly_FiservOnly extends BaseTestWin{

    private MainMenu_Page mainMenu_Page;
    private RecognitionStatsReport_Page recognitionStatsReport_page;

    private String remote_FRS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\Results";

    @BeforeClass(alwaysRun = true)
    public void setup_Method() throws Exception {

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);

        mainMenu_Page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_ConfigurationMenu()
                .navigate_SystemSetup()
                .navigate_StationSetup()
                .navigate_SelectAStation()
                .navigate_Sys_StationSetup()
                .navigate_MainPage();
    }
    @Test(groups = {"all_Tests", "regression_Tests"})
    public void PT1736_Verify_FRS_Reporting_Checks_Only_Fiserv_Only_Compare_Values_Results_And_Reports() throws Exception {

        String remote_FRS_Test_Reports = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\reports";
        String saveFilePath = "k:\\cust1\\Test\\reports\\recostata_410715.rpt";

        String statsFromDate = "11/29/2018";
        String throughDate = "11/29/2018";

        String documentTypeName = "CHECK";
        String recoVendorName = "FRS";

        recognitionStatsReport_page = mainMenu_Page.navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_RecognitionStatsReport();

        recognitionStatsReport_page.enterStatsFromDate(statsFromDate);
        recognitionStatsReport_page.enterThroughDate(throughDate);
        recognitionStatsReport_page._documentTypeComboBox.select_ComboBoxItem(documentTypeName);
        recognitionStatsReport_page._recoVendorComboBox.select_ComboBoxItem(recoVendorName);
        recognitionStatsReport_page.enter_SaveToFile(saveFilePath);
        recognitionStatsReport_page.run_Report();

        String reportsFile = "recostata_410692.rpt";
        String resultsFile = "410715.FRSR";

        FRS.compare_FRS_Values_From_ResultFolder_And_ReportFolder_DCMReporting(
                remote_FRS_Exports_Results + "\\" + resultsFile,
                remote_FRS_Test_Reports + "\\" + reportsFile);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        //recognitionStatsReport_page.exitApplication();
    }

}//End of class

