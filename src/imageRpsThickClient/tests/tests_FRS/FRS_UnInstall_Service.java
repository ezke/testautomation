package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.services.FRS_Winium.BaseTestWin_FRS;
import imageRpsThickClient.services.FRS_Winium.pages.FRSAdminApp_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Is;
import utils.ResultWriter;

public class FRS_UnInstall_Service extends BaseTestWin_FRS {
    private FRSAdminApp_Page frsAdminApp_page = new FRSAdminApp_Page();

    String frsServiceServerName = "\\\\rps602auto1web";
    String frsServiceName ="WFS Reco Serviceauto20test";
    String frsInstallationPathWebServer = "\\\\rps602auto1web\\d$\\WausauFS\\FRS\\Test\\60200FRS-20180404.2\\RecoServiceAdmin.exe";

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        config_setup_method(frsInstallationPathWebServer);

        frsAdminApp_page.openWiniumDriver();
    }

    @Test(groups = {"all_Tests", "regression_Tests"})
    public void verify_FRSUnInstallation() throws Exception {
        frsAdminApp_page.clickUnInstallServiceLink();
        frsAdminApp_page.verifyServiceStatusNotInstalled(Is.equalTo(true));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        new FRSAdminApp_Page().closeWindow();
    }
}
