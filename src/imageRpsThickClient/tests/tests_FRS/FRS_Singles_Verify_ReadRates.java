package imageRpsThickClient.tests.tests_FRS;

import imageRpsThickClient.data.BaseTestWin;
import imageRpsThickClient.data.GlobalVariables;
import imageRpsThickClient.services.FRS;
import imageRpsThickClient.winiumPages.Login_Page;
import imageRpsThickClient.winiumPages.MainMenu_Page;
import imageRpsThickClient.winiumPages.MaintainQueue_Page;
import imageRpsThickClient.winiumPages.RecognitionStatsReport_Page;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.FileOperations;
import utils.ResultWriter;

public class FRS_Singles_Verify_ReadRates extends BaseTestWin {

    private MaintainQueue_Page maintainQueue_page;
    private String remote2_FRS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\frs\\Results";
    private RecognitionStatsReport_Page recognitionStatsReport_page;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        FRS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_FRS_Exports_Results);

        FRS.windowsService().start();

        Browser.openWiniumDriver(GlobalVariables.winiumBrowserInfo);
        maintainQueue_page = new Login_Page().login(_loginCredentials.loginName(), _loginCredentials.password())
                .navigate_RunImageRPS()
                .navigate_QueueAndRecognitionMenu()
                .navigate_MaintainQueue();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT1716_UnlockRow_Verify_FRS_FileFound_InResultsFolder() throws Exception {

        String batchToSelect = "FISERV Recognition (CAR) - Batch 410520";
        String frsr_File = "410520.FRSR";

        maintainQueue_page.unlockAndRunBatch_ByDescription(batchToSelect);

        FileOperations.wait_ForFileCount_ToMatch(remote2_FRS_Exports_Results, 1, 300);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_FRS_Exports_Results, frsr_File);
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT1716_Compare_FRS_Values_ResultsFolder_And_ReportsFolder() throws Exception {

        String remote2_FRS_Test_Reports = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\reports";

        String statsFromDate = "03/16/2016";
        String throughDate = "03/16/2016";
        String recoVendorName = "FRS";
        String saveFilePath = "k:\\cust1\\Test\\reports\\recostata_410520.rpt";

        MainMenu_Page mainMenu_Page = maintainQueue_page.navigate_BackTo_QueueAndRecognitionMenuPage()
                .navigate_BackTo_MainMenuPage();

        recognitionStatsReport_page = mainMenu_Page.navigate_ReportsMenu()
                .navigate_ReportWizards()
                .navigate_RecognitionStatsReport();
        recognitionStatsReport_page.enterStatsFromDate(statsFromDate);
        recognitionStatsReport_page.enterThroughDate(throughDate);
        recognitionStatsReport_page._recoVendorComboBox.select_ComboBoxItem(recoVendorName);
        recognitionStatsReport_page.enter_SaveToFile(saveFilePath);
        recognitionStatsReport_page.run_Report();

        String reportsFile = "recostata.rpt";
        String resultsFile = "410520.FRSR";

        FRS.compare_FRS_Values_From_ResultFolder_And_ReportFolder_Singles(
       remote2_FRS_Exports_Results + "\\" + resultsFile,
        remote2_FRS_Test_Reports + "\\" + reportsFile);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        FRS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_FRS_Exports_Results);
    }

}//End of class

