package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.services.WORSAdminApp;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Is;
import utils.WindowsService;
import utils.WindowsServiceException;
import utils.WindowsServiceState;

import java.io.IOException;

public class WORS_ChangeConfiguration_PortNumber extends BaseTest{

    private String remote2_WORS_Service_ConfigFile = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\wors";
    private String configFile = "WORService.config.xml";
    private String actualPortNumber;
    private WORSAdminApp worsAdminApp;
    private WindowsService windowsService() {
        return WORS.windowsService();
    }

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.startService();
    }

    @Test(priority = 10)
    public void PT2668_Verify_WORS_Running_FromAdminApp() throws WindowsServiceException {

        worsAdminApp.verifyStartServiceLinkVisibility(Is.equalTo(false));
        worsAdminApp.verifyStopServiceLinkVisibility(Is.equalTo(true));
        worsAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 20)
    public void PT2667_Verify_WORS_PortNumber_Updates(){

        worsAdminApp.openOrbographTab();
        actualPortNumber = worsAdminApp.return_PortNumber();
        worsAdminApp.enterLastDigitIntoPort_TextBox("1");
        worsAdminApp.clickSaveChangesButton();
        worsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 30)
    public void PT2667_Verify_WORS_PortNumber_Updates_ToNewValue() throws Exception {

        worsAdminApp.close();
        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.openOrbographTab();
        int updatedValue = Integer.valueOf(actualPortNumber)+ 1;
        worsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
        worsAdminApp.verify_PortNumber_ConfigFile(remote2_WORS_Service_ConfigFile + "\\" + configFile, String.valueOf(updatedValue));
    }

    @Test(priority = 40)
    public void PT2667_Verify_WORS_PortNumber_UpdatesToOriginalID() throws IOException {

        worsAdminApp.close();
        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.openOrbographTab();
        worsAdminApp.enterTextIntoPort_TextBox(actualPortNumber);
        worsAdminApp.clickSaveChangesButton();
        worsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 50)
    public void PT2667_Verify_WORS_PortNumber() throws Exception {

        worsAdminApp.close();
        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.openOrbographTab();
        worsAdminApp.verify_PortNumber_ConfigFile(remote2_WORS_Service_ConfigFile + "\\" + configFile, actualPortNumber);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        if(worsAdminApp != null){
            worsAdminApp.close();
        }

    }

    }
