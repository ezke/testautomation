package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.pages.*;
import imageRpsThickClient.services.WORS;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

import java.time.Duration;
import java.util.HashMap;

public class WORS_CourtesyAmountAndLegalLineComplete extends BaseTest {

    private String resultsFile = "410680.WORSR";
    private String remote2_WORS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\Results";

    private DCM dcm;
    private LoginPage loginPage;
    private DevelopmentMenuPage developmentMenuPage;
    private MainMenuPage mainMenuPage;
    private QueueAndRecognitionMenuPage queueAndRecognitionMenuPage;
    private MaintainQueuePage maintainQueuePage;
    private DataMaintenanceMenuPage dataMaintenanceMenuPage;
    private BatchMenuPage batchMenuPage;
    private BatchQueryFilterPage batchQueryFilterPage;
    private Navigation navigation;
    private SingleBatchOperationsPage singleBatchOperationsPage;
    private BrowseTransactionsPage browseTransactionsPage;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);
        WORS.windowsService().start();

        dcm = DCM.launchRegularDB();
        loginPage = dcm.getLoginPage();
        developmentMenuPage = dcm.getDevelopmentMenuPage();
        mainMenuPage = dcm.getMainMenuPage();
        maintainQueuePage = dcm.getMaintainQueuePage();
        queueAndRecognitionMenuPage = dcm.getQueueAndRecognitionMenuPage();
        navigation = dcm.getNavigationPage();
        dataMaintenanceMenuPage = dcm.getDataMaintenanceMenuPage();
        batchMenuPage = dcm.getBatchMenuPage();
        batchQueryFilterPage = dcm.getBatchQueryFilterPage();
        singleBatchOperationsPage = dcm.getSingleBatchOperationsPage();
        browseTransactionsPage = dcm.getBrowseTransactionsPage();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT2665_UnlockRow_Verify_WORS_FileFound_InResultsFolder() throws Exception {

        String rowDescription = "Wausau Orbo Recognition (WOR) - Batch 410680";
        String worsr_File = "410680.WORSR";

        loginPage.logIn();
        developmentMenuPage.click_RunImageRPS();
        mainMenuPage.click_QueueAndRecognitionMenu();
        queueAndRecognitionMenuPage.click_MaintainQueue();
        maintainQueuePage.unlockRowByDescription(rowDescription);
        maintainQueuePage.close_UnlockRow_PopupDialog_WithYes();

        FileOperations.wait_ForFileCount_ToMatch(remote2_WORS_Exports_Results, 1, 300);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_WORS_Exports_Results, worsr_File);
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT2665_Verify_WORS_KeyingJobAtCCA() throws Exception {

        navigation.pressEscape();
        navigation.pressEscape();
        mainMenuPage.click_DataMaintenanceMenu();
        dataMaintenanceMenuPage.press_BatchMenu();
        batchMenuPage.press_ViewBatchesByBatchID();
        batchQueryFilterPage.filterByBatchID("410680");
        batchQueryFilterPage.wait_For_KeyingJob_ValueTobeUpdated("410", "Job", "cca", Duration.ofSeconds(300));
        batchQueryFilterPage.verify_ColumnValue_ByColumnName("410680", "Job", "cca");
    }

    @Test(priority = 30, groups = {"regression_Tests"})
    public void PT2665_Compare_MultiResultsValues_AgainstResultFile_Sequence2() throws Exception {

        HashMap<String, String> worsValuesSeq2 = WORS.return_WORSValues_BySequence("2", remote2_WORS_Exports_Results + "\\" + resultsFile);
        navigation.pressEnter();
        singleBatchOperationsPage.press_BrowseTransactions();
        browseTransactionsPage.selectRowByTransation("1");
        browseTransactionsPage.selectRowBySequence("2");
        browseTransactionsPage.press_DollarAccountAuditPayeeAndBasicInfo();
        browseTransactionsPage.compare_MultiResultsValues_AgainstWORS_ResultFile(worsValuesSeq2);
    }

    @Test(priority = 40, groups = {"regression_Tests"})
    public void PT2665_Compare_MultiResultsValues_AgainstResultFile_Sequence8_BadImage() throws Exception {

        HashMap<String, String> worsValuesSeq8 = WORS.return_WORSValues_BySequence("8", remote2_WORS_Exports_Results + "\\" + resultsFile);
        navigation.pressEscape();
        navigation.pressEscape();
        navigation.pressEscape();
        browseTransactionsPage.selectRowByTransation("4");
        browseTransactionsPage.selectRowBySequence("8");
        browseTransactionsPage.press_DollarAccountAuditPayeeAndBasicInfo();
        browseTransactionsPage.compare_MultiResultsValues_AgainstWORS_ResultFile(worsValuesSeq8);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {

        WORS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

        if (dcm != null) {
            dcm.click_ExitButton();
        }
    }
}
