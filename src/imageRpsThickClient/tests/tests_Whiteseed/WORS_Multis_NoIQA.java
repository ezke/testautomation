package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.services.WORS;
import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.pages.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

import java.time.Duration;

public class WORS_Multis_NoIQA extends BaseTest{

    private String remote2_WORS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\results";

    private DCM dcm;
    private LoginPage loginPage;
    private DevelopmentMenuPage developmentMenuPage;
    private MainMenuPage mainMenuPage;
    private QueueAndRecognitionMenuPage queueAndRecognitionMenuPage;
    private MaintainQueuePage maintainQueuePage;
    private Navigation navigation;
    private DataMaintenanceMenuPage dataMaintenanceMenuPage;
    private BatchMenuPage batchMenuPage;
    private BatchQueryFilterPage batchQueryFilterPage;
    private KeyingJobsPage keyingJobsPage;
    private SingleBatchOperationsPage singleBatchOperationsPage;

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);
        WORS.windowsService().start();

        dcm = DCM.launchRegularDB();
        loginPage = dcm.getLoginPage();
        developmentMenuPage = dcm.getDevelopmentMenuPage();
        mainMenuPage = dcm.getMainMenuPage();
        maintainQueuePage = dcm.getMaintainQueuePage();
        queueAndRecognitionMenuPage = dcm.getQueueAndRecognitionMenuPage();
        navigation = dcm.getNavigationPage();
        dataMaintenanceMenuPage = dcm.getDataMaintenanceMenuPage();
        batchMenuPage = dcm.getBatchMenuPage();
        batchQueryFilterPage = dcm.getBatchQueryFilterPage();
        keyingJobsPage = dcm.getKeyingScanlinePage();
        singleBatchOperationsPage = dcm.getSingleBatchOperationsPage();
    }

    @Test(priority = 10 , groups = {"regression_Tests"})
    public void PT2655_UnlockRow_Verify_WORS_FileFound_InResultsFolder() throws Exception {

        final String ROWDESCRIPTION = "Wausau Orbo Recognition (WOR) - Batch 410641";
        String worsr_File = "410641.WORSR";

        loginPage.logIn();
        developmentMenuPage.click_RunImageRPS();
        mainMenuPage.click_QueueAndRecognitionMenu();
        queueAndRecognitionMenuPage.click_MaintainQueue();
        maintainQueuePage.unlockRowByDescription(ROWDESCRIPTION);
        maintainQueuePage.close_UnlockRow_PopupDialog_WithYes();

        FileOperations.wait_ForFileCount_ToMatch(remote2_WORS_Exports_Results, 1, 300);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_WORS_Exports_Results, worsr_File);
    }

    @Test(priority = 20 , groups = {"regression_Tests"})
    public void PT2655_Verify_WORS_FileNotFound_InJobFilesFolder() throws Exception {

        String worsj_testFile = "410641.worsj";
        String remote2_WORS_Exports_JobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\job_Files";
        FileOperations.verify_FileName_NotFound(remote2_WORS_Exports_JobFiles, worsj_testFile);
    }

    @Test(priority = 30 , groups = {"regression_Tests"})
    public void PT2655_Verify_Batch_DialogBoxText() throws Exception {

        navigation.pressEscape();
        navigation.pressEscape();
        mainMenuPage.click_DataMaintenanceMenu();
        dataMaintenanceMenuPage.press_BatchMenu();
        batchMenuPage.press_ViewBatchesByBatchID();
        batchQueryFilterPage.filterByBatchID("410641");
        navigation.pressEnter();
        singleBatchOperationsPage.press_KeyThisBatch();
        keyingJobsPage.balance_Amount_ForBAL();
        keyingJobsPage.verifyText_InDialogBox("Question (Press Help to view stack trace)",
                "End of Batch. Quit?");
    }

    @Test(priority = 40 , groups = {"regression_Tests"})
    public void PT2655_Verify_Batch_CompletesTo_don() throws Exception {

        keyingJobsPage.click_Yes_InDialogBox();
        keyingJobsPage.setFirstWindowToCurrent();
        navigation.pressEscape();
        batchQueryFilterPage.wait_For_KeyingJob_ValueTobeUpdated("410","Job","don", Duration.ofSeconds(300));
        navigation.pressEnter();
        singleBatchOperationsPage.press_GeneralBatchInformation();
        batchQueryFilterPage.verify_KeyingJob_Text("don");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {

        WORS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

        if (dcm != null) {
            dcm.click_ExitButton();
        }
    }
}
