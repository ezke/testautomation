package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.services.WORS;
import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.pages.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class WORS_Singles_NoIQA extends BaseTest {

    private String remote2_WORS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\results";

    private DCM dcm;
    private LoginPage loginPage;
    private DevelopmentMenuPage developmentMenuPage;
    private MainMenuPage mainMenuPage;
    private QueueAndRecognitionMenuPage queueAndRecognitionMenuPage;
    private MaintainQueuePage maintainQueuePage;

    @BeforeClass
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);
        WORS.windowsService().start();

        dcm = DCM.launchRegularDB();
        loginPage = dcm.getLoginPage();
        developmentMenuPage = dcm.getDevelopmentMenuPage();
        mainMenuPage = dcm.getMainMenuPage();
        maintainQueuePage = dcm.getMaintainQueuePage();
        queueAndRecognitionMenuPage = dcm.getQueueAndRecognitionMenuPage();
    }

    @Test(priority = 10 , groups = {"regression_Tests"})
    public void PT2654_Step6_UnlockRow_Verify_WORS_FileFound_InResultsFolder() throws Exception {

        final String ROWDESCRIPTION = "Wausau Orbo Recognition (WOR) - Batch 410600";
        String worsr_File = "410600.WORSR";

        loginPage.logIn();
        developmentMenuPage.click_RunImageRPS();
        mainMenuPage.click_QueueAndRecognitionMenu();
        queueAndRecognitionMenuPage.click_MaintainQueue();
        maintainQueuePage.unlockRowByDescription(ROWDESCRIPTION);
        maintainQueuePage.close_UnlockRow_PopupDialog_WithYes();

        FileOperations.wait_ForFileCount_ToMatch(remote2_WORS_Exports_Results, 1, 300);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_WORS_Exports_Results, worsr_File);
    }

    @Test(priority = 20 , groups = {"regression_Tests"})
    public void PT2654_Step6_Verify_WORS_FileNotFound_InJobFilesFolder() throws Exception {

        String worsj_testFile = "410600.worsj";
        String remote2_WORS_Exports_JobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\job_Files";
        FileOperations.verify_FileName_NotFound(remote2_WORS_Exports_JobFiles, worsj_testFile);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {

        WORS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

        if (dcm != null) {
            dcm.click_ExitButton();
        }
    }
}
