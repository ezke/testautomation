package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.services.WORS;
import imageRpsThickClient.services.WORSAdminApp;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Is;
import utils.WindowsService;
import utils.WindowsServiceException;
import utils.WindowsServiceState;
import java.io.IOException;

public class WORS_ChangeConfiguration_StationID extends BaseTest {

    private String remote2_WORS_Service_ConfigFile = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\wors";
    private String configFile = "WORService.config.xml";
    private WORSAdminApp worsAdminApp;
    private String actualStationID;
    private WindowsService windowsService() {
        return WORS.windowsService();
    }

    @BeforeClass(alwaysRun = true)
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.startService();
    }

    @Test(priority = 10)
    public void PT2667_Verify_WORS_Running_FromAdminApp() throws WindowsServiceException {

        worsAdminApp.verifyStartServiceLinkVisibility(Is.equalTo(false));
        worsAdminApp.verifyStopServiceLinkVisibility(Is.equalTo(true));
        worsAdminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 20)
    public void PT2667_Verify_WORS_StationID_Updates(){

        actualStationID = worsAdminApp.return_StationID();
        worsAdminApp.enterTextIntoStationID_TextBox(actualStationID + "1");
        worsAdminApp.clickSaveChangesButton();
        worsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 30)
    public void PT2667_Verify_WORS_StationID_OriginalID() throws Exception {

        worsAdminApp.close();
        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.verifyStationIDText(actualStationID + "1");
        worsAdminApp.verify_StationID_ConfigFile(remote2_WORS_Service_ConfigFile + "\\" + configFile, actualStationID + "1");
    }

    @Test(priority = 40)
    public void PT2667_Verify_WORS_StationID_UpdatesToOriginalID() throws IOException {

        worsAdminApp.close();
        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.enterTextIntoStationID_TextBox(actualStationID);
        worsAdminApp.clickSaveChangesButton();
        worsAdminApp.verifySaveChangesButtonEnabled(Is.equalTo(false));
    }

    @Test(priority = 50)
    public void PT2667_Verify_WORS_StationID() throws Exception {

        worsAdminApp.close();
        worsAdminApp = WORSAdminApp.launchAndAttach();
        worsAdminApp.verifyStationIDText(actualStationID);
        worsAdminApp.verify_StationID_ConfigFile(remote2_WORS_Service_ConfigFile + "\\" + configFile, actualStationID);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
       if(worsAdminApp != null){
           worsAdminApp.close();
       }

    }
}
