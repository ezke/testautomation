package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.services.BCS;
import imageRpsThickClient.services.BCSAdminApp;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.Test;
import utils.Is;
import utils.WindowsService;
import utils.WindowsServiceException;
import utils.WindowsServiceState;

import java.io.IOException;

public class BCS_AdminTests {

    @NotNull
    private WindowsService windowsService() {
        return BCS.windowsService();
    }

    @Test(priority = 1)
    public void PT340_Precondition() throws WindowsServiceException {
        // Precondition: Have the BCS service stopped or not started yet.
        windowsService().stop();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
    }

    @Test(priority = 101)
    public void PT340_CanStartWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().start();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
    }

    @Test(priority = 102)
    public void PT341_CanStopWindowsServiceDirectly() throws WindowsServiceException {
        windowsService().stop();
        windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
    }

    @Test(priority = 201)
    public void PT344_CanStartWindowsServiceFromAdminApp() throws IOException, WindowsServiceException {
        try (BCSAdminApp adminApp = BCSAdminApp.launchAndAttach()) {
            // Click on the Start Service link in the Available Service Tasks frame.
            adminApp.startService();

            // The Start Service link will change to Stop Service.
            adminApp.verifyStartServiceLinkVisibility(Is.equalTo(false));
            adminApp.verifyStopServiceLinkVisibility(Is.equalTo(true));

            // The Service Status in the upper left corner will change to Running.
            adminApp.verifyServiceStatusText(Is.equalTo("Service Status: Running"));

            // Bonus: verify that the Windows service is actually running.
            windowsService().verifyState(Is.equalTo(WindowsServiceState.RUNNING));
        }
    }

    @Test(priority = 202)
    public void PT345_CanStopWindowsServiceFromAdminApp() throws IOException, WindowsServiceException {
        try (BCSAdminApp adminApp = BCSAdminApp.launchAndAttach()) {
            // Click on the Stop Service link in the Available Service Tasks frame.
            adminApp.stopService();

            // The Stop Service link will change to Start Service.
            adminApp.verifyStopServiceLinkVisibility(Is.equalTo(false));
            adminApp.verifyStartServiceLinkVisibility(Is.equalTo(true));

            // The Service Status in the upper left corner will change to stopped.
            adminApp.verifyServiceStatusText(Is.equalTo("Service Status: Stopped"));

            // Bonus: verify that the Windows service is actually stopped.
            windowsService().verifyState(Is.equalTo(WindowsServiceState.STOPPED));
        }
    }
}
