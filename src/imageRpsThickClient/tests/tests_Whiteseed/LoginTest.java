package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.pages.DCM;
import imageRpsThickClient.pages.DevelopmentMenuPage;
import imageRpsThickClient.pages.LoginPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    private DCM dcm;
    private LoginPage loginPage;
    private DevelopmentMenuPage developmentMenuPage;

    @BeforeClass
    public void setup_method() throws Exception {
        dcm = DCM.launchRegularDB();
        loginPage = dcm.getLoginPage();
        developmentMenuPage = dcm.getDevelopmentMenuPage();
    }

    @Test(priority = 1)
    public void openApplication() throws Exception {
        loginPage.logIn();
        developmentMenuPage.verify_RunImageRPS_Text("2. Run ImageRPS");
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {
        if (dcm != null) {
            dcm.click_ExitButton();
        }
    }
}
