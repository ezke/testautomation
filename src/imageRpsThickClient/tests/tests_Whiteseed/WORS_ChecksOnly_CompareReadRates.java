package imageRpsThickClient.tests.tests_Whiteseed;

import imageRpsThickClient.services.WORS;
import imageRpsThickClient.data.BaseTest;
import imageRpsThickClient.pages.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class WORS_ChecksOnly_CompareReadRates extends BaseTest {

    private String remote2_WORS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\Results";

    private DCM dcm;
    private LoginPage loginPage;
    private DevelopmentMenuPage developmentMenuPage;
    private MainMenuPage mainMenuPage;
    private QueueAndRecognitionMenuPage queueAndRecognitionMenuPage;
    private MaintainQueuePage maintainQueuePage;
    private ReportsMenuPage reportsMenuPage;
    private ReportWizardsPage reportWizardsPage;
    private RecognitionStatsReportPage recognitionStatsReportPage;
    private Navigation navigation;

    @BeforeClass
    public void setup_method() throws Exception {

        WORS.windowsService().stop();

        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);
        WORS.windowsService().start();

        dcm = DCM.launchRegularDB();
        loginPage = dcm.getLoginPage();
        developmentMenuPage = dcm.getDevelopmentMenuPage();
        mainMenuPage = dcm.getMainMenuPage();
        maintainQueuePage = dcm.getMaintainQueuePage();
        queueAndRecognitionMenuPage = dcm.getQueueAndRecognitionMenuPage();
        navigation = dcm.getNavigationPage();
        reportsMenuPage = dcm.getMReportsMenuPage();
        reportWizardsPage = dcm.getReportWizardsPage();
        recognitionStatsReportPage = dcm.getRecognitionStatsReportPage();
    }

    @Test(priority = 10, groups = {"regression_Tests"})
    public void PT2663_UnlockRow_Verify_WORS_FileFound_InResultsFolder() throws Exception {

        String rowDescription = "Wausau Orbo Recognition (WOR) - Batch 410620";
        String worsr_File = "410620.WORSR";

        loginPage.logIn();
        developmentMenuPage.click_RunImageRPS();
        mainMenuPage.click_QueueAndRecognitionMenu();
        queueAndRecognitionMenuPage.click_MaintainQueue();
        maintainQueuePage.unlockRowByDescription(rowDescription);
        maintainQueuePage.close_UnlockRow_PopupDialog_WithYes();

        FileOperations.wait_ForFileCount_ToMatch(remote2_WORS_Exports_Results, 1, 300);
        Thread.sleep(500);
        FileOperations.verify_FileFound(remote2_WORS_Exports_Results, worsr_File);
    }

    @Test(priority = 20, groups = {"regression_Tests"})
    public void PT2663_Compare_WORS_Values_ResultsFolder_And_ReportsFolder() throws Exception {

        String remote2_WORS_Test_Reports = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\reports";
        String rowDescription = " Recognition Stats Report";
        String statsFromDate = "03/02/2015";
        String throughDate = "03/02/2015";
        String saveFilePath = "k:\\cust1\\Test\\reports\\recostata_410620.rpt";
        navigation.pressEscape();
        navigation.pressEscape();

        mainMenuPage.click_ReportsMenu();
        reportsMenuPage.pressKeyFor_ReportWizards();
        reportWizardsPage.selectRowByDescription(rowDescription);
        navigation.pressEnter();
        recognitionStatsReportPage.enterStatsFromDate(statsFromDate);
        recognitionStatsReportPage.enterThroughDate(throughDate);
        recognitionStatsReportPage.enterSaveToFilePath(saveFilePath);
        recognitionStatsReportPage.click_RunNow();
        recognitionStatsReportPage.click_Yes_InDialogBox_ToOverwrite_File();
        recognitionStatsReportPage.click_No_InDialogBoxTo_NotOpenFile();

        String reportsFile = "recostata_410620.rpt";
        String resultsFile = "410620.WORSR";
        WORS.compare_WORS_Values_From_ResultFolder_And_ReportFolder(
                remote2_WORS_Exports_Results + "\\" + resultsFile,
                remote2_WORS_Test_Reports + "\\" + reportsFile);
    }

    @AfterClass(alwaysRun = true)
    public void TearDown() throws Exception {

        WORS.windowsService().stop();
        FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

        if (dcm != null) {
            dcm.click_ExitButton();
        }
    }
}
