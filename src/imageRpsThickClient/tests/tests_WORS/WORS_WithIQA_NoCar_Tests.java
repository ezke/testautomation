package imageRpsThickClient.tests.tests_WORS;

import imageRpsThickClient.services.WORS;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.FileOperations;

public class WORS_WithIQA_NoCar_Tests {

	private String remote2_WORS_Exports_TestData_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\job_Files\\TestJobs";
	private String worsj_testFile = "998877.worsj";

	private String remote2_WORS_Exports_JobFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\job_Files";

	private String remote2_WORS_Exports_Results = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\results";

	private String remote2_FRS_Export_QueueFiles = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\queue";

	private String remote2_WORS_TestAutomationBatches = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\exports\\wors\\WORS-TestAutomationBatches";

	private String worsr_File = "998877.WORSR";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		WORS.windowsService().stop();

		FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

		FileOperations.copyFile(remote2_WORS_Exports_TestData_Folder + "\\" + worsj_testFile,
				remote2_WORS_Exports_JobFiles + "\\" + worsj_testFile);

		WORS.windowsService().start();

		FileOperations.wait_ForFileCount_ToMatch(remote2_WORS_Exports_Results, 1, 180);
		Thread.sleep(500);

	}

	// Step 3
	@Test(priority = 20,groups = { "all_Tests", "critical_Tests" })
	public void PT2637_Verify_WORSR_File_Exists_InResultsFolder() throws Exception {
		FileOperations.verify_FileFound(remote2_WORS_Exports_Results, worsr_File);
		// FileOperations.verify_FileCount_Correct(remote2_FRS_Export_Results, 2);

	}

	// Step 4
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests" })
	public void PT2637_worsj_File_NotFound_In_Exports_JobFiles() throws Exception {
		FileOperations.verify_FileName_NotFound(remote2_WORS_Exports_JobFiles, worsj_testFile);
	}

	// Step 5
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests" })
	public void PT2637_Verify_worsr_Found_In_FRS_queuefiles() throws Exception {
		FileOperations.verify_FileFound(remote2_FRS_Export_QueueFiles, worsr_File);
	}

	// Step 6
	@Test(priority = 40,groups = { "all_Tests", "critical_Tests" })
	public void PT2637_Verify_WORSR_File_Matches_Between_Results_And_TestBatchesFolders() throws Exception {
		WORS.verify_WORSR_FilesMatch(remote2_WORS_TestAutomationBatches + "\\" + worsr_File,
				remote2_WORS_Exports_Results + "\\" + worsr_File);
		
		//ImageRPS_Utilities.verify_P1_file_Matches(expectedfileToCheck, file1_expectedPath, actualFileToCheck, file2_expectedPath, expectedBatch, expectedSkewTime, expectedDate);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		WORS.windowsService().stop();

		FileOperations.cleanup_PriorFiles(remote2_WORS_Exports_Results);

	}

}// end of class
