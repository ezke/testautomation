package imageRpsThickClient.services;

import imageRPS.data.ImageRPS_Utilities;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import org.w3c.dom.Node;
import utils.*;
import utils.XMLParser;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;

public class RCP {

    private static final String rcpCentralServerName = "10.27.80.155";
    private static final String rcpCentralWindowsServiceName = "WfsRcpCentralSrvCust1-Test";
    private static final String rcpRemoteServerName = "10.27.80.156";
    private static final String rcpRemoteServerNameWin8 = "10.27.82.84";
    private static final String rcpRemoteServerNameWin7 = "10.27.82.87";
    private static final String rcpRemoteWindowsServiceName = "WfsRcpRemoteSrvcust1Test";
    public static File queue = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File preencd = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File logs = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File landingzone = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File p1data = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File p1images = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File p2data = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File p2images = new File("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    public static File[] fileDirs = {queue, preencd, logs, landingzone, p1data, p1images, p2data, p2images};

    public static void cleanup_RCP_1966_Files_AndResetServers() throws Exception {
        RCP.rcpRemoteWindowsServiceWin8().makeStopped();
        RCP.rcpRemoteWindowsService().makeStopped();
        RCP.rcpRemoteWindowsServiceWin7().makeStopped();
        RCP.rcpCentralWindowsService().makeStopped();
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1data");
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\p1images");
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue");

        FileOperations.cleanup_PriorFiles("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data");
        FileOperations.cleanup_PriorFiles("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images");
        FileOperations.cleanup_PriorFiles("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");

        FileOperations.cleanup_PriorFiles("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data");
        FileOperations.cleanup_PriorFiles("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images");
        FileOperations.cleanup_PriorFiles("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");

        FileOperations.cleanup_PriorFiles("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1data");
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\p1images");
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\queue");
    }

    public static void cleanup_RCP_1970_Files() throws Exception {
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\usebats");
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
    }

    public static WindowsService rcpCentralWindowsService() {
        return new RemoteWindowsService(rcpCentralServerName, rcpCentralWindowsServiceName,
                Duration.ofSeconds(30), Duration.ofSeconds(30));
    }

    public static WindowsService rcpRemoteWindowsService() {
        return new RemoteWindowsService(rcpRemoteServerName, rcpRemoteWindowsServiceName,
                Duration.ofSeconds(30), Duration.ofSeconds(30));
    }

    public static WindowsService rcpRemoteWindowsServiceWin8() {
        return new RemoteWindowsService(rcpRemoteServerNameWin8, rcpRemoteWindowsServiceName,
                Duration.ofSeconds(30), Duration.ofSeconds(30));
    }

    public static WindowsService rcpRemoteWindowsServiceWin7() {
        return new RemoteWindowsService(rcpRemoteServerNameWin7, rcpRemoteWindowsServiceName,
                Duration.ofSeconds(30), Duration.ofSeconds(30));
    }

    public static void verify_Match_For_p1Data(String expectedFolderLocation, String actualFolderLocation)
            throws Exception {
        File expectedFolder = new File(expectedFolderLocation);
        File actualFolder = new File(actualFolderLocation);

        File[] listOfExpectedFiles = FileOperations.return_NonHiddenFiles(expectedFolder);
        File[] listOfActualFiles = FileOperations.return_NonHiddenFiles(actualFolder);

        ArrayList<String> listOf_ExpectedFile_Outputs = new ArrayList<String>();
        ArrayList<String> listOf_ActualFile_Outputs = new ArrayList<String>();

        for (File file : listOfExpectedFiles) {
            String fileOutput = DataBuilder.Get_FileData_AsString(file);
            listOf_ExpectedFile_Outputs.add(fileOutput);
        }

        for (File file : listOfActualFiles) {
            String fileOutput = DataBuilder.Get_FileData_AsString(file);
            listOf_ActualFile_Outputs.add(fileOutput);
        }

        // Cycle through and test our files.
        for (int i = 0; i < listOf_ExpectedFile_Outputs.size(); i++) {
            // Test first row of Expected File where the cells are different.
            ImageRPS_Utilities.verify_FirstRow_CellDifferences(listOfExpectedFiles[i], listOf_ExpectedFile_Outputs.get(i),
                    "k:\\kyris\\test601a\\");
            // Test first row of ActualFile where the cells are different.
            ImageRPS_Utilities.verify_FirstRow_CellDifferences(listOfActualFiles[i], listOf_ActualFile_Outputs.get(i),
                    "C:\\WausauFS\\RemoteCapturePush\\output\\");

            // Excluding paths, let's verify that the first row matches between our 2 files.
            ImageRPS_Utilities.verify_FirstRowMatches_ExceptForOurPathIndexes(listOfExpectedFiles[i], listOf_ExpectedFile_Outputs.get(i),
                    listOf_ActualFile_Outputs.get(i));

            String[] expectedFileOutput = listOf_ExpectedFile_Outputs.get(i).split("\\n");
            String[] actualFileOutput = listOf_ActualFile_Outputs.get(i).split("\\n");

            BaseUI.verify_true_AndLog(expectedFileOutput.length > 3, "Found file lines.", "Did NOT find file lines.");
            BaseUI.verify_true_AndLog(expectedFileOutput.length == actualFileOutput.length,
                    "Outputs matched in line count.", "Outputs did not match in line count.");

            // Check the remaining lines to make sure they match. Spacing may differ, so we
            // trimmed them.
            String output = "";
            for (int j = 1; j < expectedFileOutput.length; j++) {
                Boolean linesMatched = expectedFileOutput[j].trim().equals(actualFileOutput[j].trim());
                if (!linesMatched) {
                    output += "\n" + "Expecting " + expectedFileOutput[j] + "\n" + "But seeing " + actualFileOutput[j];
                }
            }
            BaseUI.verify_true_AndLog(output.equals(""), "Lines matched for file " + listOfActualFiles[i].getName(),
                    output);
        }

        // Verifies that we found the files and our previous tests weren't just doing
        // empty loops.
        BaseUI.verify_true_AndLog(listOfExpectedFiles.length > 0, "Found files.", "Did NOT find files.");
        BaseUI.verify_true_AndLog(listOfExpectedFiles.length == listOfActualFiles.length,
                "Number of Files matched between expected and actual file arrays.",
                "Expected " + String.valueOf(listOfExpectedFiles.length) + " files, but seeing "
                        + String.valueOf(listOfActualFiles.length) + " files.");
    }

    public static void verify_P1Data_FileMatch_By_Extension(String fileLocation1, String fileLocation2, String extension)
            throws Exception {

        File[] listOf_Files_Location = FileOperations.return_Files_ThatEndInExtension(fileLocation1, extension);
        File[] listOf_Files_Location2 = FileOperations.return_Files_ThatEndInExtension(fileLocation2, extension);

        String[] file1files = new String[listOf_Files_Location.length];
        for (int i = 0; i < listOf_Files_Location.length; i++) {
            file1files[i] = listOf_Files_Location[i].getName().toString();
        }

        String[] file2files = new String[listOf_Files_Location2.length];
        for (int i = 0; i < listOf_Files_Location2.length; i++) {
            file2files[i] = listOf_Files_Location2[i].getName();
        }
        BaseUI.verify_true_AndLog(fileLocation1.length() > 0, "Files are greater than zero", "File size not greater than zero");
        BaseUI.verify_true_AndLog(Arrays.equals(file1files, file2files), "Files match with extension type " + extension, "Files do not match with extension type " + extension);
    }

    //files from folder location 1 exists within file location 2
    public static void verify_FilesMatch_TwoFolders(String fileLocation1, String fileLocation2) {
        File[] file_Location1_Files = FileOperations.return_NonHiddenFiles(new File(fileLocation1));
        File[] file_Location2_Files = FileOperations.return_NonHiddenFiles(new File(fileLocation2));
        String[] location1_files = new String[file_Location1_Files.length];
        for (int i = 0; i < file_Location1_Files.length; i++) {
            location1_files[i] = file_Location1_Files[i].getName();
        }
        String[] location2_files = new String[file_Location2_Files.length];
        for (int i = 0; i < file_Location2_Files.length; i++) {
            location2_files[i] = file_Location2_Files[i].getName();
        }
        System.out.println("file one length" + fileLocation1.length());
        System.out.println("file one length" + fileLocation2.length());
        BaseUI.verify_true_AndLog(fileLocation1.length() > 0, "Files are greater than zero ", "File size not greater than zero");
        SoftAssert softAssert = new SoftAssert();
        for (String location1file : location1_files) {
            softAssert.assertTrue(Arrays.asList(location2_files).contains(location1file), "Value of '" + location1file + "' was not found in the " + fileLocation2 + " folder");
        }
        softAssert.assertAll();
    }

    public static void cleanData_Add400_Usebats_RestServices() throws Exception {
        RCP.rcpRemoteWindowsServiceWin8().stop();
        RCP.rcpRemoteWindowsService().stop();
        RCP.rcpRemoteWindowsServiceWin7().stop();
        RCP.rcpCentralWindowsService().stop();
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\usebats");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\usedbats\\Usebat_Testing");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\usebats_R3");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\usebats_R2");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\usebats_R1");
    }

    public static void cleanDataAll_ResetServices() throws Exception {
        RCP.rcpRemoteWindowsServiceWin8().stop();
        RCP.rcpRemoteWindowsService().stop();
        RCP.rcpRemoteWindowsServiceWin7().stop();
        RCP.rcpCentralWindowsService().stop();

        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1data");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\p1images");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\queue");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\usebats");
    }

    public static void cleanData_Add400_EIP() {
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_81c.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\qaautotest_7k.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip");
        FileOperations
                .deleteFiles_TopFolder("\\\\rps602auto1sql.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\Input\\eip");
    }

    //send file location
    public static int getFileType_Count(File[] file_Location_Files) {
        ArrayList<String> file_Extensions = new ArrayList<String>();
        for (int i = 0; i < file_Location_Files.length; i++) {
            String filename = file_Location_Files[i].getName();
            String extension = FilenameUtils.getExtension(filename);
            if (!(file_Extensions).contains(extension)) {
                file_Extensions.add(extension);
            }
        }
        int filetypes = file_Extensions.size();
        return filetypes;
    }

    public static void set_XML_ConfigFile(String xml_File_Location_Remote) throws Exception {
        XMLParser parser;
        parser = new XMLParser(xml_File_Location_Remote);
        Node useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Both");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
    }

    public static void set_XML_ConfigFile_Value(String xml_File_Location_Remote, String attributeToChange, String valueToSetTo) throws Exception {
        XMLParser parser;
        parser = new XMLParser(xml_File_Location_Remote);
        Node node = parser.findNode("//SyncFileTypes//SyncFileType[2]");
        //    Node node = parser.findNode("//SyncFileTypes//SyncFileType[@name= '"+fileType+"']");
        parser.setAttribute(node, attributeToChange, valueToSetTo);
        parser.applyUpdates_ToXML();
    }

    public static void set_XML_ConfigRemotes_1989() throws Exception {
        XMLParser parser;
        parser = new XMLParser("\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        Node useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Download");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_7k.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Download");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_81c.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Download");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        Node useEIP_Remote = parser.findNode("//SyncFileTypes//SyncFileType[2]");
        parser.setAttribute(useEIP_Remote, "syncDirection", "Both");
        parser.setAttribute(useEIP_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useEIP_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
    }

    public static void set_XML_ConfigCentral_1989(String xml_File_Location_Remote) throws Exception {
        XMLParser parser;
        parser = new XMLParser(xml_File_Location_Remote);
        Node useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Upload");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        Node useEIP_Remote = parser.findNode("//SyncFileTypes//SyncFileType[2]");
        parser.setAttribute(useEIP_Remote, "syncDirection", "Both");
        parser.setAttribute(useEIP_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useEIP_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
    }

    public static void set_XML_Config_1987() throws Exception {
        XMLParser parser;
        parser = new XMLParser("\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml");
        Node useBats_Cenral = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Cenral, "syncDirection", "Upload");
        parser.setAttribute(useBats_Cenral, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Cenral, "deleteAfterUpload", "false");
        Node Central_R1 = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(Central_R1, "syncDirection", "Download");
        parser.setAttribute(Central_R1, "minutesBetweenUpdates", "1");
        parser.setAttribute(Central_R1, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
        Node Central_R2 = parser.findNode("//SyncFileTypes//SyncFileType[6]");
        parser.setAttribute(Central_R2, "syncDirection", "Download");
        parser.setAttribute(Central_R2, "minutesBetweenUpdates", "1");
        parser.setAttribute(Central_R2, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
        Node Central_R3 = parser.findNode("//SyncFileTypes//SyncFileType[7]");
        parser.setAttribute(Central_R3, "syncDirection", "Download");
        parser.setAttribute(Central_R3, "minutesBetweenUpdates", "1");
        parser.setAttribute(Central_R3, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        Node useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Upload");
        parser.setAttribute(useBats_Remote, "filter", ".r1");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_7k.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Upload");
        parser.setAttribute(useBats_Remote, "filter", ".r2");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_81c.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Upload");
        parser.setAttribute(useBats_Remote, "filter", ".r3");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
    }

    public static void set_XML_Config_1986() throws Exception {
        XMLParser parser;
        parser = new XMLParser("\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml");
        Node useBats_Cenral = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Cenral, "syncDirection", "Upload");
        parser.setAttribute(useBats_Cenral, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Cenral, "deleteAfterUpload", "false");
        Node Central_R1 = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(Central_R1, "syncDirection", "Upload");
        parser.setAttribute(Central_R1, "minutesBetweenUpdates", "1");
        parser.setAttribute(Central_R1, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
        Node Central_R2 = parser.findNode("//SyncFileTypes//SyncFileType[6]");
        parser.setAttribute(Central_R2, "syncDirection", "Upload");
        parser.setAttribute(Central_R2, "minutesBetweenUpdates", "1");
        parser.setAttribute(Central_R2, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
        Node Central_R3 = parser.findNode("//SyncFileTypes//SyncFileType[7]");
        parser.setAttribute(Central_R3, "syncDirection", "Upload");
        parser.setAttribute(Central_R3, "minutesBetweenUpdates", "1");
        parser.setAttribute(Central_R3, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        Node useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Download");
        parser.setAttribute(useBats_Remote, "filter", ".r1");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_7k.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Download");
        parser.setAttribute(useBats_Remote, "filter", ".r2");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_81c.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[5]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Download");
        parser.setAttribute(useBats_Remote, "filter", ".r3");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
    }

    public static void set_XML_Config_1985() throws Exception {
        XMLParser parser;
        parser = new XMLParser("\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\Test\\service\\RCP\\Rcp.CentralSite.Settings.xml");
        Node useBats_Cenral = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Cenral, "syncDirection", "Both");
        parser.setAttribute(useBats_Cenral, "filter", "*.*");
        parser.setAttribute(useBats_Cenral, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Cenral, "deleteAfterUpload", "false");

        parser = new XMLParser("\\\\rps602auto1sql.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        Node useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Both");
        parser.setAttribute(useBats_Remote, "filter", "*.*");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_7k.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Both");
        parser.setAttribute(useBats_Remote, "filter", "*.*");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();

        parser = new XMLParser("\\\\qaautotest_81c.qalabs.nwk\\D$\\cust1\\test\\service\\RCP\\Remote\\Rcp.RemoteSite.Settings.xml");
        useBats_Remote = parser.findNode("//SyncFileTypes//SyncFileType[1]");
        parser.setAttribute(useBats_Remote, "syncDirection", "Both");
        parser.setAttribute(useBats_Remote, "filter", "*.*");
        parser.setAttribute(useBats_Remote, "minutesBetweenUpdates", "1");
        parser.setAttribute(useBats_Remote, "deleteAfterUpload", "false");
        parser.applyUpdates_ToXML();
    }

    public static void setDataAndServices_PT2008() throws Exception {
        RCP.rcpCentralWindowsService().stop();
        RCP.rcpRemoteWindowsService().stop();
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\LZ2");
        FileOperations.cleanup_PriorFiles("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\oxiLZ");
        RCP.rcpCentralWindowsService().start();
        RCP.rcpRemoteWindowsService().start();
        FileOperations.copyFiles("\\\\rps602auto1dcm.qalabs.nwk\\C$\\RCP Test Data - Test Set 533\\553-2008",
                "\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\LZ2");
        FileOperations.wait_ForFileCount_ToMatch("\\\\rps602auto1sql.qalabs.nwk\\C$\\Wausaufs\\RemoteCapturePush\\Input\\LZ2", 10, 80);
        FileOperations.wait_ForFileCount_ToMatch("\\\\rps602auto1dcm.qalabs.nwk\\C$\\WausauFS\\RemoteCapturePush\\output\\oxiLZ", 9, 80);
    }





    public static void makeDirectories1980() {
        for (int i = 0; i < fileDirs.length; i++) {
            if (!fileDirs[i].exists()) {
                fileDirs[i].mkdir();
            }
        }
    }
}//End of Class
