package imageRpsThickClient.services.RCP_Winium;

import imageRpsThickClient.data.GlobalVariables;
import org.hamcrest.Matcher;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.*;
import utils.Winium.WiniumBrowser;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

public class RCPAdminApp {

    public static HashMap<String, String> config_Data;

    public static void enterInstallInformationRCP() {
        enter_EnvTextBox("Cust1-InstallTest Central Site");
        RCPAdminApp.saveSetEnvName();
        RCPAdminApp.clickInstallServiceLink();
        enter_SetServiceLoginUserName("qalabs\\nextgenserviceuser");
        enter_SetServiceLoginUserPwd("Wausau#1");
        enter_SetServiceLoginUserPwdConfirm("Wausau#1");
        clickSetServiceLoginOkBtn();

    }

    public static void enterIncorrectUserInformation_RemoteRCP() {
        enter_EnvTextBox("Cust1-InstallTest Remote Site");
        RCPAdminApp.saveSetEnvName();
        RCPAdminApp.clickInstallServiceLink();
        enter_SetServiceLoginUserName("Test");
        enter_SetServiceLoginUserPwd("Wausau#1");
        enter_SetServiceLoginUserPwdConfirm("Wausau#1");
        clickSetServiceLoginOkBtn();
    }

    public static void enterIncorrectPasswordInformation_RemoteRCP() {
        RCPAdminApp.clickInstallServiceLink();
        enter_SetServiceLoginUserName("qalabs\\nextgenserviceuser");
        enter_SetServiceLoginUserPwd("123");
        enter_SetServiceLoginUserPwdConfirm("123");
        clickSetServiceLoginOkBtn();
        clickCancelBtn();
    }


    public static void clickSetServiceLoginOkBtn() {
        WebElement webElement = Locator.lookupElement("setServiceLoginOkBtn");
        webElement.click();
    }

    public static void clickCancelBtn() {
        WebElement webElement = Locator.lookupElement("cancelBtn");
        webElement.click();
    }

    public static void enter_SetServiceLoginUserName(String serviceLoginUserName) {
        WebElement loginUserName = Locator.lookupElement("setServiceLoginUser");
        loginUserName.sendKeys(serviceLoginUserName);
    }

    public static void enter_SetServiceLoginUserPwd(String serviceLoginPwd) {
        WebElement setServiceLoginUserPwd = Locator.lookupElement("setServiceLoginPwd");
        setServiceLoginUserPwd.sendKeys(serviceLoginPwd);
    }

    public static void enter_SetServiceLoginUserPwdConfirm(String serviceLoginPwd) {
        WebElement setServiceLoginUserPwdConfirm = Locator.lookupElement("setServiceLoginConfirmPwd");
        setServiceLoginUserPwdConfirm.sendKeys(serviceLoginPwd);
        BaseUI.waitForElementToBeDisplayed("setServiceLoginOkBtn", null, null);

    }

    public static void enter_EnvTextBox(String evnValue) {
        WebElement envTextBox = Locator.lookupElement("installationSetEnvName");
        envTextBox.sendKeys(evnValue);
    }

    public static void saveSetEnvName() {
        WebElement setEnvSave = Locator.lookupElement("btnEnvSave");
        setEnvSave.click();
    }

    public static void clickInstallServiceLink() {
        WebElement webElement = Locator.lookupElement("installService_Link");
        webElement.click();
    }

    public static void closeWindow() {
        BaseUI.click(Locator.lookupRequiredElement("closeServiceWindow"));
    }

    public static void verifyServiceStatusText(Matcher<String> condition) {
        BaseUI.assertThat("Service Status text", getServiceStatusText(), condition);
    }

    public static void verifyInstallationErrorText(Matcher<String> condition) {
        BaseUI.assertThat("Installation Error Message", getInstallationErrorText(), condition);
    }

    public static void clickStartServiceLink() {
        WebElement webElement = Locator.lookupElement("startService_Link");
        webElement.click();
    }

    public void verifyFilterUsebats(Matcher<String> condition) {
        BaseUI.assertThat("Service Status text", getServiceStatusText(), condition);
    }

    public static String getServiceStatusText() {
        return BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("serviceStatus_Text"), "Name");
    }

    public static String getInstallationErrorText() {
        return BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("installationError"), "Name");
    }

    public static TableData parse_SynchDirections_Table() {
        WebElement table = Locator.lookupRequiredElement("syncFileTypesTable");
        String[] headers = {"Name", "Directory", "Filter", "Direction", "Update Frequency"};
        ArrayList<WebElement> cells = new ArrayList<>(table.findElements(By.xpath("./*[2]/following-sibling::*/*")));
        ArrayList<String> cellExtracts = new ArrayList<String>();
        for (WebElement cell : cells) {
            cellExtracts.add(BaseUI.get_Attribute_FromField(cell, "Name"));
        }

        TableData newTable = new TableData();
        Integer cellCount = cellExtracts.size();
        Integer headerCount = headers.length;
        Integer listSize = cellCount / headerCount;
        // add Hashmaps to our list for each of the rows
        for (int i = 0; i < listSize; i++) {
            newTable.data.add(new HashMap<String, String>());
        }

        Integer dataIndex = 0;
        for (int i = 0; i < cellCount; ) {
            if (dataIndex >= newTable.data.size()) {
                break;
            } else {

            }

            for (int j = 0; j < headerCount; j++) {
                newTable.data.get(dataIndex).put(headers[j], cellExtracts.get(i));

                i++;
            }
            dataIndex++;
        }

        return newTable;
    }

    public static void verify_Filters_SynchDirections(String Name, String expectedFilter, String expectedDirection) {

        TableData parse_SynchDirections_Table = parse_SynchDirections_Table();
        HashMap<String, String> rowWeWant = parse_SynchDirections_Table.return_Row_BasedOn_1MatchingField("Name", Name);
        String filter = rowWeWant.get("Filter");
        String direction = rowWeWant.get("Direction");
        BaseUI.assertThat(Name + " Direction", direction, (Is.equalTo(expectedDirection)));
        BaseUI.assertThat(Name + " Filter", filter, (Is.equalTo(expectedFilter)));
    }

    public static String getValue(String valueToGet) {
        String value = "";
        value = System.getProperty(valueToGet);
        if (value == null) {
            value = config_Data.get(valueToGet);
        }

        if (value != null) {
            System.setProperty(valueToGet, value);
        }
        return value;
    }

    public void stopServiceUI() {
        WebElement stopService_Link = Locator.lookupElement("stopService_Link");
        if (BaseUI.elementAppears(stopService_Link)) {
            stopService_Link.click();
        }
    }

    public  void clickUninstallService(){
        WebElement unInstallService_Link = Locator.lookupElement("unInstallService_Link");
        unInstallService_Link.click();
        BaseUI.waitForElementToBeDisplayed("installService_Link", null, null);

    }

    public static void clickErrorDialogOk(){
        WebElement unInstallService_Link = Locator.lookupElement("errorDialogOkBtn");
        unInstallService_Link.click();
    }

}
