package imageRpsThickClient.services;

import imageRpsThickClient.data.ImageRpsThickClientConfig;
import org.hamcrest.Matcher;
import utils.BaseUI;
import utils.Is;
import utils.Wait;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.io.IOException;
import java.time.Duration;

public final class BCSAdminApp implements AutoCloseable {

    @SuppressWarnings("SpellCheckingInspection") // IntelliJ's spellchecker doesn't like "cust"
    private static final String ADMIN_EXE_PATH = "K:\\cust1\\Test\\service\\BCS\\BCSServiceAdmin.exe";
    private static final ElementFilter FILTER_SERVICE_STATUS_LABEL = ElementFilter.byAutomationID("lblStatus");
    private static final ElementFilter FILTER_START_SERVICE_LINK = ElementFilter.byAutomationID("cmdStartServer");
    private static final ElementFilter FILTER_STOP_SERVICE_LINK = ElementFilter.byAutomationID("cmdStopServer");

    private final Application _application;

    private BCSAdminApp(Application application) {
        _application = application;
    }

    public static BCSAdminApp launchAndAttach() throws IOException {
        Application application = Application.launchAndAttach(
                ImageRpsThickClientConfig.getWhiteSeedUrl(),
                ADMIN_EXE_PATH);
        return new BCSAdminApp(application);
    }

    @Override
    public void close() {
        _application.close();
    }

    private String getServiceStatusText() {
        return _application.getElementText(FILTER_SERVICE_STATUS_LABEL);
    }

    private boolean isStartServiceLinkVisible() {
        return _application.isElementPresent(FILTER_START_SERVICE_LINK);
    }

    private boolean isStopServiceLinkVisible() {
        return _application.isElementPresent(FILTER_STOP_SERVICE_LINK);
    }

    private void controlService(ElementFilter elementToClick, Duration timeout, String expectedServiceStatusText) {
        // Click the link. The app will respond to messages just long enough to
        // repaint the status label, and then freeze until the service starts/stops.
        // So this step might return immediately, but it usually takes all the time.
        _application.clickElement(elementToClick);

        // In case White did return right away after the click, make sure we wait
        // the specified timeout for the service to start/stop.
        waitUntilServiceStatusLabel(Is.equalTo(expectedServiceStatusText), timeout);
    }

    public void startService() {
        controlService(FILTER_START_SERVICE_LINK, BCS.windowsServiceStartTimeout(), "Service Status: Running");
    }

    public void stopService() {
        controlService(FILTER_STOP_SERVICE_LINK, BCS.windowsServiceStopTimeout(), "Service Status: Stopped");
    }

    public void verifyServiceStatusText(Matcher<String> condition) {
        BaseUI.assertThat("Service Status text", getServiceStatusText(), condition);
    }

    public void verifyStartServiceLinkVisibility(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the 'Start Service' hyperlink visible?", isStartServiceLinkVisible(), condition);
    }

    public void verifyStopServiceLinkVisibility(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the 'Stop Service' hyperlink visible?", isStopServiceLinkVisible(), condition);
    }

    private void waitUntilServiceStatusLabel(Matcher<String> condition, Duration timeout) {
        Wait.atMost(timeout).until("Service Status label",
                () -> _application.getElementText(FILTER_SERVICE_STATUS_LABEL),
                condition);
    }
}
