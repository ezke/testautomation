package imageRpsThickClient.services.BCS_Winium;

import imageRpsThickClient.data.GlobalVariables;
import org.testng.annotations.BeforeTest;
import utils.DataBuilder;
import utils.Locator;
import utils.Winium.WiniumBrowser;

import java.util.HashMap;

public class BaseTestWin_BCS {

    private HashMap<String, String> config_Data;

    @BeforeTest(alwaysRun = true)
    public void config_setup_method() throws Exception {

        config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\imageRpsThickClient\\data\\Config.txt");
        GlobalVariables.winiumBrowserInfo = new WiniumBrowser(getValue("server"), Integer.valueOf(getValue("port")),
                "K:\\cust1\\Test\\service\\BCS\\BCSServiceAdmin.exe", null);

        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\services\\BCS_Winium\\BCS_Repo.txt");
    }

    private String getValue(String valueToGet) {
        String value = "";
        value = System.getProperty(valueToGet);
        if (value == null) {
            value = config_Data.get(valueToGet);
        }

        if (value != null) {
            System.setProperty(valueToGet, value);
        }
        return value;
    }
}
