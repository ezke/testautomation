package imageRpsThickClient.services.BCS_Winium;

import org.hamcrest.Matcher;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

public class BCSAdminApp {

    public void startService() throws Exception {

        BaseUI.waitForElementToBeDisplayed("startService_Link", null, null);
        BaseUI.click(Locator.lookupRequiredElement("startService_Link"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals("Service Status: Running"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(90), Duration.ofSeconds(10));
    }

    public void stopService() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("stopService_Link"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals("Service Status: Stopped"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(60), Duration.ofSeconds(10));
    }

    private String getServiceStatusText() {

        return BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("serviceStatus_Text"), "Name");
    }

    public void verifyServiceStatusText(Matcher<String> condition) {
        BaseUI.assertThat("Service Status text", getServiceStatusText(), condition);
    }

    public void closeWindow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("closeBCSServiceInstallationWindow"));
    }

    public void click_StationsTab() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("stations_Tab"));
        BaseUI.waitForElementToBeDisplayed("stations_Tab", null, null);

    }

    public void click_DiagnosticsTab(){
        BaseUI.click(Locator.lookupRequiredElement("diagnostics_Tab"));
        BaseUI.waitForElementToBeDisplayed("diagnostics_Tab", null, null);
    }

    public String return_MSMQName(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("diagnostics_MSMQName_TextBox"));
    }

    public String return_TraceFilePath(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("diagnostics_TraceFilePath_TextBox"));
    }

    public String return_TraceQueueSize(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("diagnostics_TraceQueueSize_TextBox"));
    }

    public String return_SiteName(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("generalInfo_SiteName"));
    }

    public String return_ApplicationDirectory(){
        return BaseUI.getTextFromField(Locator.lookupRequiredElement("generalInfo_AppDirectory"));
    }

    public void enter_SiteName(String textToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("generalInfo_SiteName"),textToEnter);
    }

    public void enter_AppDirectory(String diectoryToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("generalInfo_AppDirectory"),diectoryToEnter);
    }

    public void click_SaveChanges_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("savesChanges_Button"));
        Thread.sleep(300);
        click_BatchCoversionOK_Button();
    }

    private void click_BatchCoversionOK_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("batchConversionService_OKButton"));
        BaseUI.waitForElementToNOTBeDisplayed("batchConversionService_OKButton", null, null);
    }

    public void click_AddStation_Button(){
        BaseUI.click(Locator.lookupRequiredElement("stations_AddStation_Button"));
        BaseUI.waitForElementToBeDisplayed("addStation_StationID_TextBox", null, null);
    }

    public void click_RemoveStation_Button() throws InterruptedException {
        BaseUI.click(Locator.lookupRequiredElement("stations_RemoveStation_Button"));
        Thread.sleep(300);
    }

    public void enter_StationID(String diectoryToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("addStation_StationID_TextBox"),diectoryToEnter);
    }

    public void enter_AppServerFile(String appServerFileToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("addStation_AppServerFile_TextBox"),appServerFileToEnter);
    }

    public void click_AddSationDialogSave_Button() throws InterruptedException {
        BaseUI.click(Locator.lookupRequiredElement("addStation_save_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("addStation_save_Button", null, null);
    }

    public void enter_TraceFilePath(String traceFilePathToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("diagnostics_TraceFilePath_TextBox"),traceFilePathToEnter);
    }

    public void enter_TraceQueueSize(String traceQueueSizeToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("diagnostics_TraceQueueSize_TextBox"),traceQueueSizeToEnter);
    }

    public void unCheck_TraceDirectlyToFile_CheckBox(){
        BaseUI.uncheckCheckbox(Locator.lookupRequiredElement("diagnostics_TraceDirectlyToFile_CheckBox"));
    }
    public void check_TraceDirectlyToFile_CheckBox(){
        BaseUI.checkCheckbox(Locator.lookupRequiredElement("diagnostics_TraceDirectlyToFile_CheckBox"));
    }

    public void click_TestConfigurationLink(){
        BaseUI.click(Locator.lookupRequiredElement("testConfiguration_Link"));
        BaseUI.waitForElementToBeDisplayed("testingConfig_CheckingConfigFile", null, null);
    }

    public String[] return_TestConfigurationModal_Text(){

        return new String[] {BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("testingConfig_CheckingConfigFile"), "Name"),
                BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("testingConfig_TestingSystemPath"), "Name")};
    }

    public void click_TestingConfig_CloseButton(){
        BaseUI.click(Locator.lookupRequiredElement("testingConfig_CloseButton"));
        BaseUI.waitForElementToNOTBeDisplayed("testingConfig_CloseButton", null, null);
    }

    public void verify_TraceDirectlyToFile_Checked() {
        BaseUI.verifyCheckboxStatus(Locator.lookupRequiredElement("diagnostics_TraceDirectlyToFile_CheckBox"), true);
    }

    public void verify_TraceDirectlyToFile_UnChecked() {
        BaseUI.verifyCheckboxStatus(Locator.lookupRequiredElement("diagnostics_TraceDirectlyToFile_CheckBox"), false);
    }

    public void verifySaveChangesButtonEnabled(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the 'Save Changes' button enabled?", isSaveChangesButtonEnabled(), condition);
    }

    private boolean isSaveChangesButtonEnabled() {
        WebElement element = Locator.lookupRequiredElement("savesChanges_Button");
        boolean enabled;
        enabled = element.isEnabled();
        return enabled;
    }

    public void wait_ForServiceText_TobeUpdated(String serviceText) throws Exception {

        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals(serviceText));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(60), Duration.ofSeconds(10));
    }

    public TableData tableExtractor_ByCell(String[] headers) {
        TableData newTable = new TableData();
        ArrayList<WebElement> cells = getColumnValues();
        ArrayList<String> cellExtracts = new ArrayList<String>();

        for (WebElement cell : cells) {
            cellExtracts.add(cell.getText());
        }

        Integer cellCount = cellExtracts.size();
        Integer headerCount = headers.length;
        Integer listSize = cellCount / headerCount;
        // add Hashmaps to our list for each of the rows
        for (int i = 0; i < listSize; i++) {
            newTable.data.add(new HashMap<>());
        }

        Integer dataIndex = 0;
        for (int i = 0; i < cellCount; ) {
            if (dataIndex >= newTable.data.size()) {
                break;
            } else {

            }

            for (int j = 0; j < headerCount; j++) {
                newTable.data.get(dataIndex).put(headers[j], cellExtracts.get(i));

                i++;
            }
            dataIndex++;
        }
        // cells are split up by /t/t/t/t and then /n for line.

        return newTable;
    }

    private ArrayList<WebElement> getColumnValues() {
         WebElement tableElement = return_StationTableElement();
        return new ArrayList<>(tableElement.findElements(By.xpath("./*[2]/following-sibling::*/*[1]/following-sibling::*")));
    }

    private WebElement return_StationTableElement(){
        return Locator.lookupRequiredElement("stations_Table");
    }

    public void click_ColumnRow(int indexToClick){
        WebElement tableElement = return_StationTableElement();
        WebElement rowToClick =  tableElement.findElements(By.xpath("./*[2]/following-sibling::*/*[1]")).get(indexToClick);
        BaseUI.click(rowToClick);
    }
}
