package imageRpsThickClient.services;

import imageRPS.data.ImageRPS_Utilities;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import utils.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static utils.BaseUI.assertThat;

public class WORS {

	private static final String serverName = "10.27.80.155";
	private static final String windowsServiceName = "WFS WOR SERVICECust 1 - WORS";

	public static WindowsService windowsService() {
		return new RemoteWindowsService(serverName, windowsServiceName,
				Duration.ofSeconds(30), Duration.ofSeconds(30));
	}

	public static void verify_WORSR_FilesMatch(String source_Directory_AndFile, String end_Directory_AndFile)
			throws Exception {
		
		String[] sourceExtract = DataBuilder.Get_FileData_AsString(source_Directory_AndFile).split("\\n");
		String[] endFileExtract = DataBuilder.Get_FileData_AsString(end_Directory_AndFile).split("\\n");

		String output = "";
		BaseUI.verify_true_AndLog(sourceExtract.length == endFileExtract.length, "Files matched in line count.",
				"Files did NOT match in line count.");

		String dateFormat = "M/d/yyyy h:mm:ss a";
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
		String dateRange1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat, dateFormat,
				-3);
		String dateRange2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat, dateFormat,
				3);

		String elapsedTimeFormat = "HH:mm:ss.SSSSSSS";
		
		
		for (int i = 0; i < sourceExtract.length; i++) {

			if (endFileExtract[i].contains("<startTime>")) {
				String endFile_startTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "startTime");

				BaseUI.verify_Date_IsBetween_DateRange(dateRange1, dateRange2, endFile_startTime, dateFormat);

				String sourceFile_StartTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "startTime");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(sourceFile_StartTime, dateFormat),
						"Time format matched.",
						"Line " + sourceFile_StartTime + " did not match format of " + dateFormat);
			} else if (endFileExtract[i].contains("<endTime>")) {
				String endFile_endTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "endTime");
				BaseUI.verify_Date_IsBetween_DateRange(dateRange1, dateRange2, endFile_endTime, dateFormat);

				String sourceFile_endTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "endTime");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(sourceFile_endTime, dateFormat),
						"Time format matched.",
						"Line " + sourceFile_endTime + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<elapsedTime>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "elapsedTime");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "elapsedTime");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<timeAvg>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "timeAvg");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "timeAvg");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<timeMax>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "timeMax");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "timeMax");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<timeMin>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "timeMin");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "timeMin");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);

			} else if (sourceExtract[i].contains("<reserved>")) {
				//Skip any validations for reserved.  They won't match.
				
			} else if (!sourceExtract[i].equals(endFileExtract[i])) {
				output += "\n" + "Source File had line:\n" + sourceExtract[i] + "\n" + "But End file had line:\n"
						+ endFileExtract[i];
			}

		}

		BaseUI.verify_true_AndLog(output.equals(""),
				"File " + source_Directory_AndFile + " matched file " + end_Directory_AndFile, output);
		
		
	}

    private static int return_AverageConfidenceValue(String sourceFolderLocation) throws Exception {

        String[] sourceFileExtract = DataBuilder.Get_FileData_AsString(sourceFolderLocation).split("\\n");

        int totalValue = 0;
        int loopCount = 0;
        int averageConfValue = 0;
        for (String aSourceFileExtract : sourceFileExtract) {

            if (aSourceFileExtract.contains("<conf>")) {

                String sourceConfValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aSourceFileExtract, "conf");
                if (Integer.valueOf(sourceConfValue) >= 65) {
                    totalValue = totalValue + Integer.valueOf(sourceConfValue);
                    loopCount++;
                    averageConfValue = totalValue / loopCount;
                }

            }
        }
        return averageConfValue;
    }

    private static int return_ConfidenceValueCount_LessThan_SixtyFive(String sourceFolderLocation) throws Exception {

        String[] sourceFileExtract = DataBuilder.Get_FileData_AsString(sourceFolderLocation).split("\\n");
        int loopCountForConfValue = 0;
        for (String aSourceFileExtract : sourceFileExtract) {
            if (aSourceFileExtract.contains("<conf>")) {

                String sourceConfValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aSourceFileExtract, "conf");
                if (Integer.valueOf(sourceConfValue) != 0 && Integer.valueOf(sourceConfValue) < 65) {
                    loopCountForConfValue++;
                }
            }
        }
        return loopCountForConfValue;
    }

    private static int return_AverageConfidenceValue_ByDocID(String docID, String resultFolderLocation) throws Exception {

        XMLParser parser;
        int totalValue = 0;
        int loopCount = 0;
        int averageConfValue;

        String confXpath = "//doc_id[./text()='"+ docID + "']/following-sibling::fields//results//conf";
        parser = new XMLParser(resultFolderLocation);
        NodeList test_ConfNode = parser.findNodes(confXpath);

        for (int i = 0; i < test_ConfNode.getLength(); i++) {
            String confValue = "";
            if (test_ConfNode.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) test_ConfNode.item(i);
                if (el.getTagName().equals("conf")) {
                    confValue = el.getTextContent();
                }
                if (Integer.valueOf(confValue) >= 65) {
                    totalValue += Integer.valueOf(confValue);
                    loopCount++;

                }
            }
        }
        averageConfValue = totalValue / loopCount;
        return averageConfValue;
    }

    private static String return_NextLine_FromFile_ByLineText(String folderLocation, String lineText) throws IOException {

        FileReader file = new FileReader(folderLocation);
        BufferedReader bufferedReader = new BufferedReader(file);
        String lines;

        String nextLine = "";
        while ((lines = bufferedReader.readLine()) != null) {
            if (lines.startsWith(lineText)) {
                nextLine = bufferedReader.readLine();
            }
        }
        return nextLine;
    }

    //Compares AverageConf, DOCID, ItemCount and ItemsRead from Results folder is greater or equal to
    // AverageConf, FieldsSubmitted, FieldsOkCounts in Report Folder
    // Compares difference between ItemCount and ItemsRead from Results Folder is equal to Confidence Fail Count in Reports Folder
    public static void compare_WORS_Values_From_ResultFolder_And_ReportFolder(String resultFolderLocation, String reportFolderLocation) throws Exception {

        String resultItemCountValue = "";

        int averageConfForResultFile = return_AverageConfidenceValue(resultFolderLocation);
        String[] resultFileExtract = DataBuilder.Get_FileData_AsString(resultFolderLocation).split("\\n");

        String nextLineFromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "Capture Stn ID: 62-BCS2");
        String avgConfForReportFile = nextLineFromReportFile.substring(117, 125).split("\\.")[0].trim();
        String docIDReportFile = nextLineFromReportFile.substring(54, 57).trim();
        String fieldsSubmittedValue = nextLineFromReportFile.substring(93, 106).trim();
        String fieldsOkCntValue = nextLineFromReportFile.substring(107, 116).trim();
        String confidenceFailCntValueReportFile = nextLineFromReportFile.substring(135, 141).trim();

        for (String aResultFileExtract : resultFileExtract) {

            if (aResultFileExtract.contains("<doc_id>")) {

                String resultDocIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "doc_id");
                assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
                        resultDocIDValue, equalTo(docIDReportFile));
            } else if (aResultFileExtract.contains("<itemCount>")) {

                resultItemCountValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemCount");
                assertThat("Compare ItemCount Value from Result File is equal to FieldsSubmitted value of ReportFile",
                        resultItemCountValue, equalTo(fieldsSubmittedValue));
            } else if (aResultFileExtract.contains("<itemsRead>")) {

               String  resultItemReadsValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemsRead");
                assertThat("Compare ItemsRead Value from Result File is equal to FieldsOKCnt value of ReportFile",
                        resultItemReadsValue, equalTo(fieldsOkCntValue));

                int confidenceFailCntResultFile = Integer.valueOf(resultItemCountValue) - Integer.valueOf(resultItemReadsValue);
                assertThat("Compare difference between ItemCount and ItemsRead in Result File is equal to Confidence Fail Count value of ReportFile",
                        confidenceFailCntResultFile, equalTo(Integer.valueOf(confidenceFailCntValueReportFile)));
            }
        }
        assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
                averageConfForResultFile, greaterThanOrEqualTo(Integer.valueOf(avgConfForReportFile)));
    }

    public static void compare_WORS_Values_From_ResultFolder_And_ReportFolder_Multis(String resultFolderLocation, String reportFolderLocation) throws Exception {

        String resultItemCountValue = "";

        int averageConfForResultFileDocID11 = return_AverageConfidenceValue_ByDocID("11",resultFolderLocation);
        int averageConfForResultFileDocID7 = return_AverageConfidenceValue_ByDocID("7",resultFolderLocation);
        int countOfConfValueLestThan65 = return_ConfidenceValueCount_LessThan_SixtyFive(resultFolderLocation);

        String[] resultFileExtract = DataBuilder.Get_FileData_AsString(resultFolderLocation).split("\\n");

        String nextLineDocID11_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "Capture Stn ID: 61-BCS1");
        String nextLineDocID7_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "  Doc ID: 11-Bevent Stub");

        String docIDReportFile_ForDocID11 = nextLineDocID11_FromReportFile.substring(54, 57).trim();
        String fieldsSubmittedValue_ForDocID11 = nextLineDocID11_FromReportFile.substring(93, 106).trim();
        String fieldsOkCntValue_ForDocID11 = nextLineDocID11_FromReportFile.substring(107, 116).trim();
        String avgConfForReportFile_ForDocID11 = nextLineDocID11_FromReportFile.substring(117, 125).split("\\.")[0].trim();
        String confidenceFailCntValueReportFile_ForDocID11 = nextLineDocID11_FromReportFile.substring(135, 141).trim();

        String docIDReportFile_ForDocID7 = nextLineDocID7_FromReportFile.substring(54, 57).trim();
        String fieldsSubmittedValue_ForDocID7 = nextLineDocID7_FromReportFile.substring(93, 106).trim();
        String fieldsOkCntValue_ForDocID7 = nextLineDocID7_FromReportFile.substring(107, 116).trim();
        String avgConfForReportFile_ForDocID7 = nextLineDocID7_FromReportFile.substring(117, 125).split("\\.")[0].trim();
        String confidenceFailCntValueReportFile_ForDocID7 = nextLineDocID7_FromReportFile.substring(135, 141).trim();

        for (String aResultFileExtract : resultFileExtract) {

            if (aResultFileExtract.contains("<doc_id>")) {

                String resultDocIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "doc_id");
                if (resultDocIDValue.equals("7")) {
                    assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
                            resultDocIDValue, equalTo(docIDReportFile_ForDocID7));
                } else {
                    assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
                            resultDocIDValue, equalTo(docIDReportFile_ForDocID11));
                }
            } else if (aResultFileExtract.contains("<itemCount>")) {

                resultItemCountValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemCount");
                assertThat("Compare ItemCount Value from Result File is equal to FieldsSubmitted value of ReportFile",
                        Integer.valueOf(resultItemCountValue), equalTo(Integer.valueOf(fieldsSubmittedValue_ForDocID11) + Integer.valueOf(fieldsSubmittedValue_ForDocID7)));
            } else if (aResultFileExtract.contains("<itemsRead>")) {

                String resultItemReadsValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemsRead");
                assertThat("Compare ItemsRead Value from Result File is equal to FieldsOKCnt value of ReportFile",
                        Integer.valueOf(resultItemReadsValue), equalTo(Integer.valueOf(fieldsOkCntValue_ForDocID11) + Integer.valueOf(fieldsOkCntValue_ForDocID7)
                                + countOfConfValueLestThan65));

                int confidenceFailCntResultFile = Integer.valueOf(resultItemCountValue) - Integer.valueOf(resultItemReadsValue) + countOfConfValueLestThan65;
                assertThat("Compare difference between ItemCount and ItemsRead in Result File is equal to Confidence Fail Count value of ReportFile",
                        confidenceFailCntResultFile, equalTo(Integer.valueOf(confidenceFailCntValueReportFile_ForDocID11) + Integer.valueOf(confidenceFailCntValueReportFile_ForDocID7)));
            }
        }

        BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
                String.valueOf(averageConfForResultFileDocID11), greaterThanOrEqualTo(avgConfForReportFile_ForDocID11));
        BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
                String.valueOf(averageConfForResultFileDocID7), greaterThanOrEqualTo(avgConfForReportFile_ForDocID7));
    }

    public static void compare_WORS_Values_From_ResultFolder_And_ReportFolder_Singles(String resultFolderLocation, String reportFolderLocation) throws Exception {

        String resultItemCountValue = "";

        int averageConfForResultFileDocID11 = return_AverageConfidenceValue_ByDocID("11",resultFolderLocation);
        int averageConfForResultFileDocID8 = return_AverageConfidenceValue_ByDocID("8",resultFolderLocation);
        int countOfConfValueLessThan65 = return_ConfidenceValueCount_LessThan_SixtyFive(resultFolderLocation);

        String[] resultFileExtract = DataBuilder.Get_FileData_AsString(resultFolderLocation).split("\\n");

        String nextLineDocID11_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "Capture Stn ID: 62-BCS2");
        String nextLineDocID8_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "  Doc ID: 11-Bevent Stub");

        String docIDReportFile_ForDocID11 = nextLineDocID11_FromReportFile.substring(54, 57).trim();
        String fieldsSubmittedValue_ForDocID11 = nextLineDocID11_FromReportFile.substring(93, 106).trim();
        String fieldsOkCntValue_ForDocID11 = nextLineDocID11_FromReportFile.substring(107, 116).trim();
        String avgConfForReportFile_ForDocID11 = nextLineDocID11_FromReportFile.substring(117, 125).split("\\.")[0].trim();
        String confidenceFailCntValueReportFile_ForDocID11 = nextLineDocID11_FromReportFile.substring(135, 141).trim();

        String docIDReportFile_ForDocID8 = nextLineDocID8_FromReportFile.substring(54, 57).trim();
        String fieldsSubmittedValue_ForDocID8 = nextLineDocID8_FromReportFile.substring(93, 106).trim();
        String fieldsOkCntValue_ForDocID8 = nextLineDocID8_FromReportFile.substring(107, 116).trim();
        String avgConfForReportFile_ForDocID8 = nextLineDocID8_FromReportFile.substring(117, 125).split("\\.")[0].trim();
        String confidenceFailCntValueReportFile_ForDocID8 = nextLineDocID8_FromReportFile.substring(135, 141).trim();

        for (String aResultFileExtract : resultFileExtract) {

            if (aResultFileExtract.contains("<doc_id>")) {

                String resultDocIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "doc_id");
                if (resultDocIDValue.equals("8")) {
                    assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
                            resultDocIDValue, equalTo(docIDReportFile_ForDocID8));
                } else {
                    assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
                            resultDocIDValue, equalTo(docIDReportFile_ForDocID11));
                }
            } else if (aResultFileExtract.contains("<itemCount>")) {

                resultItemCountValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemCount");
                assertThat("Compare ItemCount Value from Result File is equal to FieldsSubmitted value of ReportFile",
                        Integer.valueOf(resultItemCountValue), equalTo(Integer.valueOf(fieldsSubmittedValue_ForDocID11) + Integer.valueOf(fieldsSubmittedValue_ForDocID8)));
            } else if (aResultFileExtract.contains("<itemsRead>")) {

                String resultItemReadsValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemsRead");
                assertThat("Compare ItemsRead Value from Result File is equal to FieldsOKCnt value of ReportFile",
                        Integer.valueOf(resultItemReadsValue), equalTo(Integer.valueOf(fieldsOkCntValue_ForDocID11) + Integer.valueOf(fieldsOkCntValue_ForDocID8)
                                + countOfConfValueLessThan65));

                int confidenceFailCntResultFile = Integer.valueOf(resultItemCountValue) - Integer.valueOf(resultItemReadsValue) + countOfConfValueLessThan65;
                assertThat("Compare difference between ItemCount and ItemsRead in Result File is equal to Confidence Fail Count value of ReportFile",
                        confidenceFailCntResultFile, equalTo(Integer.valueOf(confidenceFailCntValueReportFile_ForDocID11) + Integer.valueOf(confidenceFailCntValueReportFile_ForDocID8)));
            }
        }
        BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
                String.valueOf(averageConfForResultFileDocID11), greaterThanOrEqualTo(avgConfForReportFile_ForDocID11));
        BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
                String.valueOf(averageConfForResultFileDocID8), greaterThanOrEqualTo(avgConfForReportFile_ForDocID8));
    }


    public static HashMap<String, String> return_WORSValues_BySequence(String seqnumber, String resultFolderLocation) throws Exception {
        XMLParser parser;

        HashMap<String, String> worsValues = new HashMap<>();

        String valueXpath = "//p1_seq_num[./text()='"+ seqnumber+ "']/following-sibling::fields//results//value";
        String confXpath = "//p1_seq_num[./text()='"+ seqnumber + "']/following-sibling::fields//results//conf";
        String reservedXpath = "//p1_seq_num[./text()='"+ seqnumber +"']/following-sibling::fields//results//reserved";
        parser = new XMLParser(resultFolderLocation);

        Node test_ValueNode = parser.findNode(valueXpath);
        Node test_ConfNode = parser.findNode(confXpath);
        Node test_ReservedNode = parser.findNode(reservedXpath);

        worsValues.put("value", test_ValueNode.getTextContent());
        worsValues.put("conf", test_ConfNode.getTextContent());
        worsValues.put("reserved", test_ReservedNode.getTextContent());
        worsValues.put("LARrsValue", return_WORSReservedValue(worsValues.get("reserved"), "LARrs"));
        worsValues.put("LARcfValue", return_WORSReservedValue(worsValues.get("reserved"), "LARcf"));

        return worsValues;
    }

    private static String return_WORSReservedValue(String reservedOutput, String actualReservedValue){

	    String[] reservedArray = reservedOutput.split("\\s");
	    String larValue ="";
	    for(String value: reservedArray){
	        if(value.contains(actualReservedValue)){
                larValue = value.split("=")[1];
	            break;
            }
        }
        return larValue;
    }
	
}//End of Class
