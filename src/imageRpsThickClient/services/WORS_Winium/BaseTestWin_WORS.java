package imageRpsThickClient.services.WORS_Winium;

import imageRpsThickClient.data.GlobalVariables;
import org.testng.annotations.BeforeSuite;
import utils.DataBuilder;
import utils.Locator;
import utils.Winium.WiniumBrowser;

import java.util.HashMap;

public class BaseTestWin_WORS{

    private HashMap<String, String> config_Data;

    @BeforeSuite(alwaysRun = true)
    public void config_setup_method() throws Exception {

        config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\imageRpsThickClient\\data\\Config.txt");
        GlobalVariables.winiumBrowserInfo = new WiniumBrowser(getValue("server"), Integer.valueOf(getValue("port")),
                "K:\\cust1\\Test\\service\\wors\\WORServiceAdmin.exe", null);

        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\services\\WORS_Winium\\WORS_Repo.txt");
    }

    private String getValue(String valueToGet) {
        String value = "";
        value = System.getProperty(valueToGet);
        if (value == null) {
            value = config_Data.get(valueToGet);
        }

        if (value != null) {
            System.setProperty(valueToGet, value);
        }
        return value;
    }
}
