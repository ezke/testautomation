package imageRpsThickClient.services.WORS_Winium;

import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class WORSAdminApp {

    public void startService() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("startService_Link"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals("Service Status: Running"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    public void stopService() throws Exception {

        BaseUI.click(Locator.lookupRequiredElement("stopService_Link"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals("Service Status: Stopped"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    private String getServiceStatusText() {

        return BaseUI.getTextFromField(Locator.lookupRequiredElement("serviceStatus_Text"));
    }

    public void exitApplication(){
        BaseUI.click(Locator.lookupRequiredElement("closeServiceWindow"));
    }
}
