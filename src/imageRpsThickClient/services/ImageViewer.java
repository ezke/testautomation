package imageRpsThickClient.services;

import imageRpsThickClient.data.ImageRpsThickClientConfig;
import r360.windows.WindowBase;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;

import java.io.IOException;
import java.time.Duration;

public class ImageViewer extends WindowBase{

    private static final String IMAGEVIEWER_EXEPATH = "K:\\DCMCode\\Test\\system\\iview.exe";

    public ImageViewer() throws IOException {
        _application = Application.launchAndAttach(
                ImageRpsThickClientConfig.getWhiteSeedUrl(),
                IMAGEVIEWER_EXEPATH,"k:\\cust1\\Test\\p1images\\70223108.p1");
    }

    public void clickImage() throws InterruptedException {
        _application.clickElement(ElementFilter.byText("Image"));
        Thread.sleep(1000);
    }

    public void select_ExtractAllImages() {
        _application.waitForElement(Duration.ofSeconds(25), ElementFilter.byText("Extract All Images to Files"));
        _application.clickElement(ElementFilter.byText("Extract All Images to Files"));
    }

    public void select_Folder() {

        _application.clickElement(ElementFilter.byText("This PC"));
        _application.clickElement(ElementFilter.byText("Data (K:)"));
        _application.clickElement(ElementFilter.byText("cust1"));
        _application.clickElement(ElementFilter.byText("Test"));
        _application.clickElement(ElementFilter.byText("p1images"));
        _application.clickElement(ElementFilter.byText("70223108"));
        _application.clickElement(ElementFilter.byText("OK"));
        _application.clickElement(ElementFilter.byText("OK"));

    }
}
