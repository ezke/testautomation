package imageRpsThickClient.services;

import utils.RemoteWindowsService;
import utils.WindowsService;

import java.time.Duration;

/**
 * BCS (Batch Conversion Service) module
 */
public class BCS {

    private static final String SERVER_NAME = "Rps602Auto1Dcm.qalabs.nwk";
    private static final String WINDOWS_SERVICE_NAME = "WFS BCS SERVICECust1-Test";

    private BCS() {
    }

    public static WindowsService windowsService() {
        return new RemoteWindowsService(SERVER_NAME, WINDOWS_SERVICE_NAME,
                windowsServiceStartTimeout(), windowsServiceStopTimeout());
    }

    public static Duration windowsServiceStartTimeout() {
        return Duration.ofSeconds(120);
    }

    public static Duration windowsServiceStopTimeout() {
        return Duration.ofSeconds(30);
    }
}
