package imageRpsThickClient.services.FRS_Winium.pages;

import org.hamcrest.Matcher;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class FRSAdminApp_Page {

    public void closeWindow() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("closeServiceWindow"));
    }

    public String getStatus() {
        return BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("serviceStatus"), "Name");
    }

    public void clickSave() throws Exception {
        WebElement btnSave = Locator.lookupRequiredElement("btnSave");
        BaseUI.click(btnSave);
        Thread.sleep(500);
    }

    public void openWiniumDriver() throws Exception {
        Browser.openWiniumDriver(imageRpsThickClient.data.GlobalVariables.winiumBrowserInfo);
    }

    public void enter_EnvTextBox(String evnValue) throws Exception {
        WebElement envTextBox = Locator.lookupElement("installationSetEnvName");
        envTextBox.sendKeys(evnValue);
    }

    public void saveSetEnvName() throws Exception {
        WebElement setEnvSave = Locator.lookupElement("btnEnvSave");
        setEnvSave.click();
    }

    public void enter_EnvGeneralInfo(String envGenValue) throws Exception {
        WebElement envGeneralInfo = Locator.lookupElement("envtxtSiteName");
        envGeneralInfo.sendKeys(envGenValue);
    }

    public void enter_EnvRootPath(String envRootPathLocation) throws Exception {
        WebElement envRootPath = Locator.lookupElement("envRootPathLocation");
        envRootPath.sendKeys(envRootPathLocation);
    }

    public void saveGeneralInfo() throws Exception {
        WebElement generalInfoSave = Locator.lookupElement("generalInfoBtnSave");
        generalInfoSave.click();
    }

    public void clickInstallServiceLink() throws Exception {
        BaseUI.waitForElementToBeDisplayed("installService_Link", null, null);
        WebElement webElement = Locator.lookupElement("installService_Link");
        webElement.click();
    }

    public void enter_SetServiceLoginUserName(String serviceLoginUserName) throws Exception {
        WebElement loginUserName = Locator.lookupElement("setServiceLoginUser");
        loginUserName.sendKeys(serviceLoginUserName);
    }

    public void enter_SetServiceLoginUserPwd(String serviceLoginPwd) throws Exception {
        WebElement setServiceLoginUserPwd = Locator.lookupElement("setServiceLoginPwd");
        setServiceLoginUserPwd.sendKeys(serviceLoginPwd);
    }

    public void enter_SetServiceLoginUserPwdConfirm(String serviceLoginPwd) throws Exception {
        WebElement setServiceLoginUserPwdConfirm = Locator.lookupElement("setServiceLoginConfirmPwd");
        setServiceLoginUserPwdConfirm.sendKeys(serviceLoginPwd);
    }

    public void clickSetServiceLoginOkBtn() throws Exception {
        BaseUI.waitForElementToBeDisplayed("setServiceLoginOkBtn", null, null);
        WebElement webElement = Locator.lookupElement("setServiceLoginOkBtn");
        webElement.click();
    }

    public void checkCheckBox() throws Exception {
        WebElement webElement = Locator.lookupElement("advancedCheckBox");
        webElement.click();
    }

    public void enter_AdvJobFilesPath(String advancedJobFilesPath) throws Exception {
        WebElement jobFilesPath = Locator.lookupElement("txtBoxJobFilesPath");
        jobFilesPath.sendKeys(advancedJobFilesPath);
    }

    public void enter_AdvResultFilesPath(String advancedResultFilesPath) throws Exception {
        WebElement resultFilesPath = Locator.lookupElement("txtBoxResultFilesPath");
        resultFilesPath.sendKeys(advancedResultFilesPath);
    }

    public void enter_AdvQueueFilesPath(String advancedQueueFilesPath) throws Exception {
        WebElement queueFilesPath = Locator.lookupElement("txtBoxQueueFilesPath");
        queueFilesPath.sendKeys(advancedQueueFilesPath);
    }

    public void clickSaveChanges() throws Exception {
        WebElement advCheckBoxSaveChanges = Locator.lookupElement("generalInfoBtnSave");
        advCheckBoxSaveChanges.click();
    }

    public void clickDriveMappingsTab() throws Exception {
        BaseUI.waitForElementToBeDisplayed("tabDriveMappings", null, null);
        WebElement driveMappingsTab = Locator.lookupElement("tabDriveMappings");
        driveMappingsTab.click();
    }

    public void clickAddDriveMappingLink() throws Exception {
        WebElement webElement = Locator.lookupElement("addDriveMapping_Link");
        webElement.click();
    }

    public void enter_DriveMappingDrive() throws Exception {
        WebElement addDriveMappingsDrive = Locator.lookupElement("addDriveMappingsDrive");
        addDriveMappingsDrive.sendKeys("K:");
        addDriveMappingsDrive.click();
    }

    public void enter_DriveMappingFolderPath(String driveMappingFolderPath) throws Exception {
        WebElement addDriveMappingsTxtFolder = Locator.lookupElement("addDriveMappingsTxtFolder");
        addDriveMappingsTxtFolder.sendKeys(driveMappingFolderPath);
    }

    public void clickAddDriveMappingBtn() throws Exception {
        WebElement addDriveMappingsClickAdd = Locator.lookupElement("addDriveMappingsClickAdd");
        addDriveMappingsClickAdd.click();
    }

    public void clickInstancesTab() throws Exception {
        WebElement instancesTab = Locator.lookupElement("tabInstances");
        instancesTab.click();
    }

    public void clickAddInstancesLink() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("addInstances_Link"));
    }

    public void clickDiagnosticsTab() throws Exception {
        WebElement diagnosticstab = Locator.lookupElement("tabDiagnostics");
        diagnosticstab.click();
    }

    public void enter_DiagnosticsTraceFilePath(String diagnosticsTraceFilePath) throws Exception {
        WebElement diagnosticsTxtTraceFilePath = Locator.lookupElement("diagnosticsTxtTraceFilePath");
        diagnosticsTxtTraceFilePath.sendKeys(diagnosticsTraceFilePath);
    }

    public void clickSaveChangesDiagnostics() throws Exception {
        WebElement diagnosticsSaveChanges = Locator.lookupElement("diagnosticsSaveChangesBtn");
        diagnosticsSaveChanges.click();
    }

    public void clickTestConfigurationLink() throws Exception {
        WebElement webElement = Locator.lookupElement("testConfiguration_Link");
        webElement.sendKeys();
    }

    public void clickCloseBtnTestConfigurationWindow() throws Exception {
        WebElement webElement = Locator.lookupElement("testConfigurationCloseBtn");
        webElement.click();
    }

    public void clickStartServiceLink() throws Exception {
        WebElement webElement = Locator.lookupElement("startService_Link");
        webElement.click();
    }

    public void clickDumpTraceLink() throws Exception {
        WebElement webElement = Locator.lookupElement("dumpTrace_Link");
        webElement.click();
    }

    public void clickOkBtnDumpTrace() throws Exception {
        WebElement dumpTraceOkBtn = Locator.lookupElement("dumpTraceOkBtn");
        dumpTraceOkBtn.click();
    }

    public void deleteService(String serverName, String serviceName) throws Exception {
        BaseUI.log_Status("Deleting service= " + serviceName + "; on Server=" + serverName);
        String[] command = {"cmd.exe", "/c", "sc", serverName, "delete", serviceName};
        Process process = new ProcessBuilder(command).start();
    }

    public void stopService(String serverName, String serviceName) throws Exception {
        BaseUI.log_Status("Stopping service= " + serviceName + "; on Server=" + serverName);
        String[] command = {"cmd.exe", "/c", "sc", serverName, "stop", serviceName};
        Process process = new ProcessBuilder(command).start();
    }

    public void verifySaveButtonGeneralInfoEnabled(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the save button enabled?", isSaveButtonGeneralInfoEnabled(), condition);
    }

    private boolean isSaveButtonGeneralInfoEnabled() {
        WebElement webElement = Locator.lookupElement("generalInfoBtnSave");
        return webElement.isEnabled();
    }

    public void verifyDriveMappingTabLinkVisible(Matcher<Boolean> condition) {
        BaseUI.waitForElementToBeDisplayed("addDriveMapping_Link", null, null);
        WebElement webElement = Locator.lookupElement("addDriveMapping_Link");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is the Add Drive Mapping available to Click?", visible, condition);
    }

    public void verifyAddDriveMappingPopupVisible(Matcher<Boolean> condition) {
        BaseUI.waitForElementToBeDisplayed("addDriveMappingsDrive", null, null);
        WebElement webElement = Locator.lookupElement("addDriveMappingsDrive");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Add Drive Mapping popup visible?", visible, condition);
    }

    public void verifySaveButtonVisible(Matcher<Boolean> condition) {
        BaseUI.waitForElementToBeDisplayed("generalInfoBtnSave", null, null);
        WebElement webElement = Locator.lookupElement("generalInfoBtnSave");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is save button visible?", visible, condition);
    }

    public void verifyConfigurationResultsBoxWindow_Displayed(Matcher<Boolean> condition) {
        WebElement webElement = Locator.lookupElement("testConfigurationResults");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is test configuration results visible?", visible, condition);
    }

    public void closeWinium()
    {
        WebElement webElement = Locator.lookupElement("btnClose");
        webElement.click();
    }

    public void clickStopServiceLink() throws Exception {
        WebElement webElement = Locator.lookupElement("stopService_Link");
        webElement.click();
    }

    public void verifyDumpTracePopup_Displayed(Matcher<Boolean> condition) {
        WebElement webElement = Locator.lookupElement("dumpTracePopup");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Dump Trace Popup visible?", visible, condition);
    }
    public void verifyLabelJobFilesPathTextVisible(Matcher<Boolean> condition){
        BaseUI.waitForElementToBeDisplayed("labelJobFilesPath", null, null);
        WebElement webElement = Locator.lookupElement("labelJobFilesPath");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Label Job Files visible?", visible, condition);
    }
    public void verifyServiceStatusStopped(Matcher<Boolean> condition) {
        BaseUI.waitForElementToBeDisplayed("labelServiceStatus", null, null);
        WebElement webElement = Locator.lookupElement("labelServiceStatus");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Service Status Stopped?", visible, condition);
    }
    public void verifyServiceStatusRunning(Matcher<Boolean> condition) {
        BaseUI.waitForElementToBeDisplayed("labelServiceStatus", null, null);
        WebElement webElement = Locator.lookupElement("labelServiceStatus");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Service Status Running?", visible, condition);
    }
    public void verifydiagnosticsTxtTraceFilePathLabelVisible(Matcher<Boolean> condition) {
        WebElement webElement = Locator.lookupElement("diagnosticsTxtTraceFilePathLabel");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is diagnostics Txt Trace File Path Label visible?", visible, condition);
    }
    public void verifyServiceLoginUserWindowVisible(Matcher<Boolean> condition) {
        WebElement webElement = Locator.lookupElement("setServiceLoginUser");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Service Login User Popup visible?", visible, condition);
    }

    public void clickUnInstallServiceLink() throws Exception {

        WebElement serviceStatusRunning = Locator.lookupElement("labelServiceStatusRunning");
        if (BaseUI.elementAppears(serviceStatusRunning)) {
            stopServiceUI();
        }

        WebElement serviceStatusStopped = Locator.lookupElement("labelServiceStatusStopped");
        if (BaseUI.elementAppears(serviceStatusStopped)) {
            unInstallServiceUI();
        }
    }

    private void stopServiceUI() {
        WebElement stopService_Link = Locator.lookupElement("stopService_Link");
        if (BaseUI.elementAppears(stopService_Link)) {
            stopService_Link.click();
        }
    }

    private void unInstallServiceUI() {
        WebElement unInstallService_Link = Locator.lookupElement("unInstallService_Link");
        if (BaseUI.elementAppears(unInstallService_Link)) {
            unInstallService_Link.click();
        }
    }

    public void verifyServiceStatusNotInstalled(Matcher<Boolean> condition) {
        BaseUI.waitForElementToBeDisplayed("labelServiceStatus", null, null);
        WebElement webElement = Locator.lookupElement("labelServiceStatus");
        boolean visible = webElement.isDisplayed();
        BaseUI.assertThat("Is Service Status Not Installed?", visible, condition);
    }
}

