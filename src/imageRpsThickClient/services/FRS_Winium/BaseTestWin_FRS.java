package imageRpsThickClient.services.FRS_Winium;

import imageRpsThickClient.data.Data_Import;
import imageRpsThickClient.data.DcmLoginCredentials;
import imageRpsThickClient.data.GlobalVariables;
import org.testng.annotations.BeforeTest;
import utils.DataBuilder;
import utils.Locator;
import utils.Winium.WiniumBrowser;

import java.util.HashMap;

public class BaseTestWin_FRS {

    private HashMap<String, String> config_Data;

    public DcmLoginCredentials _loginCredentials;

    public void config_setup_method(String applicationPath) throws Exception {
        _loginCredentials = Data_Import.retrieve_EnvironmentData();
        config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\imageRpsThickClient\\data\\Config.txt");
        GlobalVariables.winiumBrowserInfo = new WiniumBrowser(getValue("server"), Integer.valueOf(getValue("port")),
                applicationPath, null);

        Locator.loadObjectRepository("\\src\\imageRpsThickClient\\services\\FRS_Winium\\FRS_Repo.txt");
    }

    private String getValue(String valueToGet) {
        String value = "";
        value = System.getProperty(valueToGet);
        if (value == null) {
            value = config_Data.get(valueToGet);
        }

        if (value != null) {
            System.setProperty(valueToGet, value);
        }
        return value;
    }


}
