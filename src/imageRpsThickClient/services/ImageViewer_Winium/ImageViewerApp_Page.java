package imageRpsThickClient.services.ImageViewer_Winium;

import utils.BaseUI;
import utils.Locator;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class ImageViewerApp_Page {
    
    public void close_Application() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("close_ImageViewer"));
        Thread.sleep(300);
    }

    public void kill_IviewProcess() throws IOException {
        Process process = Runtime.getRuntime().exec("TASKKILL /F /IM iview.exe");
        Scanner scanner = new Scanner(new InputStreamReader(process.getInputStream()));
        while (scanner.hasNext()) {
            String scannerText = scanner.nextLine();
            System.out.println(scannerText);

        }
        scanner.close();
    }
}
