package imageRpsThickClient.services;

import utils.RemoteWindowsService;
import utils.WindowsService;

import java.time.Duration;

public class WFSEventCollector {

    private static final String serverName = "10.27.80.155";
    private static final String windowsServiceName = "Wfs.ImageRps.EventCollector_Cust1Test";

    public static WindowsService windowsService() {
        return new RemoteWindowsService(serverName, windowsServiceName,
                windowsServiceStartTimeout(), windowsServiceStopTimeout());
    }

    public static Duration windowsServiceStartTimeout() {
        return Duration.ofSeconds(120);
    }

    public static Duration windowsServiceStopTimeout() {
        return Duration.ofSeconds(30);
    }
}
