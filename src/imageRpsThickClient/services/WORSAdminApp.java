package imageRpsThickClient.services;

import imageRPS.data.ImageRPS_Utilities;
import imageRpsThickClient.data.ImageRpsThickClientConfig;
import org.hamcrest.Matcher;
import utils.BaseUI;
import utils.DataBuilder;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;
import utils.WhiteSeedWrapper.ElementProperties;
import utils.WhiteSeedWrapper.WhiteSeedFault;
import java.io.IOException;
import java.time.Duration;

import static org.hamcrest.Matchers.equalTo;

public class WORSAdminApp {

    private static final String WORS_ADMIN_EXE_PATH = "K:\\cust1\\Test\\service\\wors\\WORServiceAdmin.exe";
    private static final ElementFilter FILTER_SERVICE_STATUS_LABEL = ElementFilter.byAutomationID("lblStatus");
    private static final ElementFilter FILTER_START_SERVICE_LINK = ElementFilter.byAutomationID("cmdStartServer");
    private static final ElementFilter FILTER_STOP_SERVICE_LINK = ElementFilter.byAutomationID("cmdStopServer");
    private static final ElementFilter FILTER_SERVICE_ORBOGRAPH_TAB = ElementFilter.byText("Orbograph");
    private static final ElementFilter FILTER_SERVICE_GENERALINFORMATION_STATIONID_TEXTBOX = ElementFilter.byAutomationID("txtStationId");
    private static final ElementFilter FILTER_SERVICE_SAVECHANGES_BUTTON = ElementFilter.byText("Save Changes");
    private static final ElementFilter FILTER_SERVICE_ORBOGRAPH_PORT_TEXTBOX = ElementFilter.byAutomationID("txtOrboPort");

    private final Application _application;

    private WORSAdminApp(Application application) {
        _application = application;
    }

    public static WORSAdminApp launchAndAttach() throws IOException {
        Application application = Application.launchAndAttach(
                ImageRpsThickClientConfig.getWhiteSeedUrl(),
                WORS_ADMIN_EXE_PATH);
        return new WORSAdminApp(application);
    }

    public void stopService() throws WhiteSeedFault, Exception {
        _application.clickElement(ElementFilter.byText("Stop Service"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals("Service Status: Stopped"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    public void startService() throws WhiteSeedFault, Exception {
        _application.clickElement(ElementFilter.byText("Start Service"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getServiceStatusText().equals("Service Status: Running"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    public void close() {
        _application.close();
    }

    public void openOrbographTab() throws WhiteSeedFault {
        _application.clickElement(FILTER_SERVICE_ORBOGRAPH_TAB);
    }

    public void enterLastDigitIntoPort_TextBox(String lastDigit) {
        _application.focusElement(FILTER_SERVICE_ORBOGRAPH_PORT_TEXTBOX);
        _application.clickElement(FILTER_SERVICE_ORBOGRAPH_PORT_TEXTBOX);
        _application.typeOnKeyboardWithSpecialKeys("<Backspace></Backspace>");
        _application.typeOnKeyboard(lastDigit);
    }

    public void enterTextIntoPort_TextBox(String text) {
        _application.putElementText(text, FILTER_SERVICE_ORBOGRAPH_PORT_TEXTBOX);
    }

    public String return_PortNumber(){
        return _application.getElementText(FILTER_SERVICE_ORBOGRAPH_PORT_TEXTBOX);
    }

    public void enterTextIntoStationID_TextBox(String text) {
        _application.putElementText(text, FILTER_SERVICE_GENERALINFORMATION_STATIONID_TEXTBOX);
    }

    public String return_StationID() {
        return _application.getElementText(FILTER_SERVICE_GENERALINFORMATION_STATIONID_TEXTBOX);
    }

    private String getServiceStatusText() {
        return _application.getElementText(FILTER_SERVICE_STATUS_LABEL);
    }

    private boolean isStartServiceLinkVisible() {
        return _application.isElementPresent(FILTER_START_SERVICE_LINK);
    }

    private boolean isStopServiceLinkVisible() {
        return _application.isElementPresent(FILTER_STOP_SERVICE_LINK);
    }

    public void clickSaveChangesButton() throws WhiteSeedFault {
        _application.clickElement(FILTER_SERVICE_SAVECHANGES_BUTTON);
    }

    public void verifyServiceStatusText(Matcher<String> condition) {
        BaseUI.assertThat("Service Status text", getServiceStatusText(), condition);
    }

    public void verifyStartServiceLinkVisibility(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the 'Start Service' hyperlink visible?", isStartServiceLinkVisible(), condition);
    }

    public void verifyStopServiceLinkVisibility(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the 'Stop Service' hyperlink visible?", isStopServiceLinkVisible(), condition);
    }

    public void verifySaveChangesButtonEnabled(Matcher<Boolean> condition) {
        BaseUI.assertThat("Is the 'Save Changes' button enabled?", isSaveChangesButtonEnabled(), condition);
    }

    public void verifyStationIDText(String expectedText) {
        String actualStationID = return_StationID();
        BaseUI.baseStringCompare("StationID", expectedText, actualStationID);
    }

    private boolean isSaveChangesButtonEnabled() {

        ElementProperties elementProperties = _application.getElement(FILTER_SERVICE_SAVECHANGES_BUTTON);
        boolean enabled;
        enabled = elementProperties.isEnabled();
        return enabled;
    }

    public void verify_StationID_ConfigFile(String configFolderLocation, String expectedStationID) throws Exception {

        String[] configFileExtract = DataBuilder.Get_FileData_AsString(configFolderLocation).split("\\n");
        for (String aconfigFileExtract : configFileExtract) {
            if (aconfigFileExtract.contains("<stnId>")) {

                String stationIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aconfigFileExtract, "stnId");
                BaseUI.assertThat("Compare StationID Value from config File is equal to StationID Value in AdminApp",
                        stationIDValue, equalTo(expectedStationID));
            }
        }
    }

    public void verify_PortNumber_ConfigFile(String configFolderLocation, String expectedPortNumber) throws Exception {

        String[] configFileExtract = DataBuilder.Get_FileData_AsString(configFolderLocation).split("\\n");
        for (String aconfigFileExtract : configFileExtract) {
            if (aconfigFileExtract.contains("<port>")) {

                String portValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aconfigFileExtract, "port");
                BaseUI.assertThat("Compare PortNumber Value from config File is equal to PortNumber Value in AdminApp",
                        portValue, equalTo(expectedPortNumber));
            }
        }
    }
}
