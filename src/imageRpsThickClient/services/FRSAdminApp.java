package imageRpsThickClient.services;

import imageRpsThickClient.data.ImageRpsThickClientConfig;
import utils.BaseUI;
import utils.WhiteSeedWrapper.Application;
import utils.WhiteSeedWrapper.ElementFilter;
import utils.WhiteSeedWrapper.ElementProperties;
import utils.WhiteSeedWrapper.WhiteSeedFault;
import whiteSeedProxy.CheckState;
import java.io.IOException;
import java.net.MalformedURLException;
import java.time.Duration;
import java.util.List;

public class FRSAdminApp {
    private static final String FRS_EXE_PATH = "K:\\cust1\\Test\\service\\FRS\\RecoServiceAdmin.exe";
    private static final String FRS_DRIVEMAPPING_PATH = "\\\\RPS602AUTO1DCM\\data";
    private static final String FRS_EDITDRIVEMAPPING_PATH = "\\\\RPS602AUTO1DCM\\vol1";
    protected Application application;

    private static final ElementFilter FILTER_CLOSE_SERVICE_RECOSERVICEWINDOW = ElementFilter.byText("Close");
    private static final ElementFilter FILTER_OPEN_SERVICE_DIAGNOSTICTAB = ElementFilter.byText("Diagnostics");
    private static final ElementFilter FILTER_CLICK_SERVICE_DUMPTRACE = ElementFilter.byText("Dump Trace");
    private static final ElementFilter FILTER_CLICK_SERVICE_OK = ElementFilter.byText("OK");
    private static final ElementFilter FILTER_ADD_SERVICE_DRIVEMAPPINGTAB = ElementFilter.byText("Add Drive Mapping");
    private static final ElementFilter FILTER_OPEN_SERVICE_DRIVEMAPPINGTAB = ElementFilter.byText("Drive Mappings");
    private static final ElementFilter FILTER_SAVE_SERVICE_DRIVEMAPPING = ElementFilter.byAutomationID("cmdSave");
    private static final ElementFilter FILTER_CLICK_SERVICE_ADDBUTTON = ElementFilter.byAutomationID("cmdOK");
    private static final ElementFilter FILTER_FILL_SERVICE_FOLDERPATH = ElementFilter.byAutomationID("txtFolder");
    private static final ElementFilter FILTER_DELETE_SERVICE_DRIVEMAPPINGTAB = ElementFilter.byText("Delete Drive Mapping");
    private static final ElementFilter FILTER_CLICKYES_SERVICE_DELETEDRIVE = ElementFilter.byText("Yes");
    private static final ElementFilter FILTER_EDIT_SERVICE_DRIVEMAPPING = ElementFilter.byText("Edit Drive Mapping");
    private static final ElementFilter FILTER_CHECKBOX_SERVICE_ADVANCED = ElementFilter.byAutomationID("chkAdvanced");
    private static final ElementFilter FILTER_CHECK_SERVICE_JOBFILESPATH = ElementFilter.byText("Job Files Path:");

    public FRSAdminApp() throws IOException, WhiteSeedFault {
        application = Application.launchAndAttach(
                ImageRpsThickClientConfig.getWhiteSeedUrl(),
                FRS_EXE_PATH);
    }

    public void stopService() throws MalformedURLException, WhiteSeedFault, Exception {
        application.clickElement(ElementFilter.byText("Stop Service"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getStatus_Service().equals("Service Status: Stopped"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    public void startService() throws MalformedURLException, WhiteSeedFault, Exception {
        application.clickElement(ElementFilter.byText("Start Service"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getStatus_Service().equals("Service Status: Running"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    public void pauseService() throws MalformedURLException, WhiteSeedFault, Exception {
        application.clickElement(ElementFilter.byText("Pause Service"));
        BaseUI.wait_ForCondition_ToBeMet(() -> {
            try {
                return (getStatus_Service().equals("Service Status: Paused"));
            } catch (Exception e) {
                return false;
            }
        }, Duration.ofSeconds(10), Duration.ofSeconds(1));
    }

    public void closeWindow() throws MalformedURLException, WhiteSeedFault {
        application.clickElement(FILTER_CLOSE_SERVICE_RECOSERVICEWINDOW);
    }

    public void openDiagnosticsTab() throws MalformedURLException, WhiteSeedFault {
        application.clickElement(FILTER_OPEN_SERVICE_DIAGNOSTICTAB);
    }

    public void openDriveMappingTab() throws WhiteSeedFault {
        application.clickElement(FILTER_OPEN_SERVICE_DRIVEMAPPINGTAB);
    }

    public void clickAddDriveMapping() throws WhiteSeedFault {
        application.clickElement(FILTER_ADD_SERVICE_DRIVEMAPPINGTAB);
    }

    public void clickAddButton() throws WhiteSeedFault {
        application.clickElement(FILTER_CLICK_SERVICE_ADDBUTTON);
    }

    public void deleteDriveMapping() throws WhiteSeedFault {
        application.clickElement(FILTER_DELETE_SERVICE_DRIVEMAPPINGTAB);
    }

    public void editDriveMapping() throws WhiteSeedFault {
        application.clickElement(FILTER_EDIT_SERVICE_DRIVEMAPPING);
    }

    public void clickYesDeleteDrive() throws WhiteSeedFault {
        List<ElementProperties> elementProperties = application.getElements(FILTER_CLICKYES_SERVICE_DELETEDRIVE);
        int size = elementProperties.size();
        boolean isEnabled = isEnabled(elementProperties);
        if (size!=0 && isEnabled)
        {
            application.clickElement(FILTER_CLICKYES_SERVICE_DELETEDRIVE);
        }
    }

    public void clickSaveButton() throws WhiteSeedFault, Exception {
        List<ElementProperties> elementProperties = application.getElements(FILTER_SAVE_SERVICE_DRIVEMAPPING);
        int size = elementProperties.size();
        boolean isEnabled = isEnabled(elementProperties);

        if (size != 0 && isEnabled) {
            application.clickElement(FILTER_SAVE_SERVICE_DRIVEMAPPING);
            BaseUI.wait_ForCondition_ToBeMet(() -> {
                try {
                    return (getStatus_Service().equals("Service Status: Running"));
                } catch (Exception e) {
                    return false;
                }
            }, Duration.ofSeconds(15), Duration.ofSeconds(1));
        }
    }

    public boolean clickEditSave() throws WhiteSeedFault, Exception {
        List<ElementProperties> elementProperties = application.getElements(FILTER_CLICK_SERVICE_ADDBUTTON);
        int size = elementProperties.size();
        boolean isEnabled = isEnabled(elementProperties);

        if (size != 0 && isEnabled)
        {
            return true;
        }
        return false;
    }

    private boolean isEnabled(List<ElementProperties> elementProperties)
    {
        for (ElementProperties eProperty : elementProperties)
        {
            if (eProperty.isEnabled())
            {
                return true;
            }
        }
        return false;
    }

    public void confirmEditDriveMapping() throws MalformedURLException, WhiteSeedFault, InterruptedException, Exception {
    boolean truefalse = clickEditSave();
        if (truefalse) {
            fillEditFolder();
            clickAddButton();
        }
    }


    public void saveCheckBoxChanges() throws MalformedURLException, WhiteSeedFault, InterruptedException, Exception {
        List<ElementProperties> listElements = application.getElements();
        if (isCheckBoxChecked(listElements, "chkTraceToFile")) {
            application.clickElement(ElementFilter.byAutomationID("chkTraceToFile"));
            application.clickElement(ElementFilter.byText("Save Changes"));
            BaseUI.wait_ForCondition_ToBeMet(() -> {
                try {
                    return (getStatus_Service().equals("Service Status: Running"));
                } catch (Exception e) {
                    return false;
                }
            }, Duration.ofSeconds(20), Duration.ofSeconds(1));
        }
    }

    public void clickDumpTrace() throws MalformedURLException, WhiteSeedFault, InterruptedException, Exception {
        application.clickElement(FILTER_CLICK_SERVICE_DUMPTRACE);
    }

    public void clickOk() throws MalformedURLException, WhiteSeedFault, InterruptedException, Exception {
        application.clickElement(FILTER_CLICK_SERVICE_OK);
    }

    public boolean isCheckBoxChecked(List<ElementProperties> elements, String checkBoxName) {
        for (ElementProperties element : elements) {
            if (element.id().equals(checkBoxName)) {
                if (element.initialCheckState() == CheckState.CHECKED) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getStatus_Service() throws MalformedURLException, WhiteSeedFault {
        return (application.getElementText(ElementFilter.byAutomationID("lblStatus")));
    }

    public String getStatus_DiagnosticTab() throws MalformedURLException, WhiteSeedFault {
        // label9 = "Trace File Path:"
        return (application.getElementText(ElementFilter.byAutomationID("label9")));
    }

    public String getMessageBoxText() throws MalformedURLException, WhiteSeedFault {
        return application.getElementText(ElementFilter.byAutomationID("65535"));
    }

    public String getStatus_DriveMappingsTab() throws WhiteSeedFault {
        // label4 = "Drive Mappings"
        return (application.getElementText(ElementFilter.byAutomationID("label4")));
    }

    public String getStatus_AddDriveMappingsPopUpWindow() throws WhiteSeedFault {
        return (application.getElementText(ElementFilter.byAutomationID("cmbDrive")));
    }

    public void fillFolder() throws WhiteSeedFault {
        application.putElementText(FRS_DRIVEMAPPING_PATH, FILTER_FILL_SERVICE_FOLDERPATH);
    }

    public void fillEditFolder() throws WhiteSeedFault {
        application.putElementText(FRS_EDITDRIVEMAPPING_PATH, FILTER_FILL_SERVICE_FOLDERPATH);
    }

    public void advancedCheckBox() throws WhiteSeedFault {
        application.clickElement(FILTER_CHECKBOX_SERVICE_ADVANCED);
    }

    public boolean checkJobFilesPath() throws MalformedURLException, WhiteSeedFault {
        return application.isElementPresent(FILTER_CHECK_SERVICE_JOBFILESPATH);
    }
}
