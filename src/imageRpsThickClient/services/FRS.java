package imageRpsThickClient.services;

import imageRPS.data.ImageRPS_Utilities;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import utils.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static utils.BaseUI.assertThat;

public class FRS {

	private static final String serverName = "10.27.80.155";
	private static final String windowsServiceName = "WFS Reco ServiceCust1-Test";

	public static WindowsService windowsService() {
		return new RemoteWindowsService(serverName, windowsServiceName,
				Duration.ofSeconds(30), Duration.ofSeconds(30));
	}

	public static void verify_FRSR_FilesMatch(String source_Directory_AndFile, String end_Directory_AndFile)
			throws Exception {

		String[] sourceExtract = DataBuilder.Get_FileData_AsString(source_Directory_AndFile).split("\\n");
		String[] endFileExtract = DataBuilder.Get_FileData_AsString(end_Directory_AndFile).split("\\n");

		String output = "";
		BaseUI.verify_true_AndLog(sourceExtract.length == endFileExtract.length, "Files matched in line count.",
				"Files did NOT match in line count.");

		String dateFormat = "M/d/yyyy h:mm:ss a";
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
		String dateRange1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat, dateFormat,
				-1);
		String dateRange2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat, dateFormat,
				1);

		String elapsedTimeFormat = "HH:mm:ss.SSSSSSS";

		for (int i = 0; i < sourceExtract.length; i++) {

			if (endFileExtract[i].contains("<startTime>")) {
				String endFile_startTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "startTime");

				BaseUI.verify_Date_IsBetween_DateRange(dateRange1, dateRange2, endFile_startTime, dateFormat);

				String sourceFile_StartTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "startTime");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(sourceFile_StartTime, dateFormat),
						"Time format matched.",
						"Line " + sourceFile_StartTime + " did not match format of " + dateFormat);
			} else if (endFileExtract[i].contains("<endTime>")) {
				String endFile_endTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "endTime");
				BaseUI.verify_Date_IsBetween_DateRange(dateRange1, dateRange2, endFile_endTime, dateFormat);

				String sourceFile_endTime = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "endTime");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(sourceFile_endTime, dateFormat),
						"Time format matched.",
						"Line " + sourceFile_endTime + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<elapsedTime>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "elapsedTime");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "elapsedTime");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<timeAvg>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "timeAvg");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "timeAvg");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<timeMax>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "timeMax");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "timeMax");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);
			} else if (sourceExtract[i].contains("<timeMin>")) {
				String source_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(sourceExtract[i], "timeMin");

				String endFile_Time = ImageRPS_Utilities.return_Line_WithoutXMLTags(endFileExtract[i], "timeMin");

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(source_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + source_Time + " did not match format of " + dateFormat);

				BaseUI.verify_true_AndLog(BaseUI.time_MatchesFormat(endFile_Time, elapsedTimeFormat),
						"Time format matched.", "Line " + endFile_Time + " did not match format of " + dateFormat);

			} else if (!sourceExtract[i].equals(endFileExtract[i])) {
				output += "\n" + "Source File had line:\n" + sourceExtract[i] + "\n" + "But End file had line:\n"
						+ endFileExtract[i];
			}

		}

		BaseUI.verify_true_AndLog(output.equals(""),
				"File " + source_Directory_AndFile + " matched file " + end_Directory_AndFile, output);

	}

	public static void compare_FRS_Values_From_ResultFolder_And_ReportFolder_Multis(String resultFolderLocation, String reportFolderLocation) throws Exception {

		String resultItemCountValue = "";

		int averageConfForResultFileDocID1 = return_AverageConfidenceValue_ByDocID("1",resultFolderLocation);

		String[] resultFileExtract = DataBuilder.Get_FileData_AsString(resultFolderLocation).split("\\n");

		String nextLineDocID1_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "  Batch ID: 410692");

		String docIDReportFile_ForDocID1 = nextLineDocID1_FromReportFile.substring(54, 57).trim();
		String fieldsSubmittedValue_ForDocID1 = nextLineDocID1_FromReportFile.substring(93, 106).trim();
		String avgConfForReportFile_ForDocID1 = nextLineDocID1_FromReportFile.substring(117, 125).split("\\.")[0].trim();

		for (String aResultFileExtract : resultFileExtract) {

			if (aResultFileExtract.contains("<doc_id>")) {

				String resultDocIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "doc_id");
				if (resultDocIDValue.equals("1")) {
					assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
							resultDocIDValue, equalTo(docIDReportFile_ForDocID1));
				}
			} else if (aResultFileExtract.contains("<itemCount>")) {

				resultItemCountValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemCount");
				assertThat("Compare ItemCount Value from Result File is equal to FieldsSubmitted value of ReportFile",
						Integer.valueOf(resultItemCountValue), equalTo(Integer.valueOf(fieldsSubmittedValue_ForDocID1)));
			}
		}

		BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
				String.valueOf(averageConfForResultFileDocID1), greaterThanOrEqualTo(avgConfForReportFile_ForDocID1));

	}

	public static void compare_FRS_Values_From_ResultFolder_And_ReportFolder_Singles(String resultFolderLocation, String reportFolderLocation) throws Exception {

		String resultItemCountValue = "";

		int averageConfForResultFileDocID1 = return_AverageConfidenceValue_ByDocID("1",resultFolderLocation);

		String[] resultFileExtract = DataBuilder.Get_FileData_AsString(resultFolderLocation).split("\\n");

		String nextLineDocID1_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "Batch ID: 410520");

		String docIDReportFile_ForDocID1 = nextLineDocID1_FromReportFile.substring(54, 57).trim();
		String fieldsSubmittedValue_ForDocID1 = nextLineDocID1_FromReportFile.substring(93, 106).trim();
		String avgConfForReportFile_ForDocID1 = nextLineDocID1_FromReportFile.substring(117, 125).split("\\.")[0].trim();

		for (String aResultFileExtract : resultFileExtract) {

			if (aResultFileExtract.contains("<doc_id>")) {

				String resultDocIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "doc_id");
				if (resultDocIDValue.equals("1")) {
					assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
							resultDocIDValue, equalTo(docIDReportFile_ForDocID1));
				}
			} else if (aResultFileExtract.contains("<itemCount>")) {

				resultItemCountValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemCount");
				assertThat("Compare ItemCount Value from Result File is equal to FieldsSubmitted value of ReportFile",
						Integer.valueOf(resultItemCountValue), equalTo(Integer.valueOf(fieldsSubmittedValue_ForDocID1)));
			}
		}

		BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
				String.valueOf(averageConfForResultFileDocID1), greaterThanOrEqualTo(avgConfForReportFile_ForDocID1));
	}

	public static void compare_FRS_Values_From_ResultFolder_And_ReportFolder_DCMReporting(String resultFolderLocation, String reportFolderLocation) throws Exception {

		String resultItemCountValue = "";

		int averageConfForResultFileDocID1 = return_AverageConfidenceValue_ByDocID("1",resultFolderLocation);

		String[] resultFileExtract = DataBuilder.Get_FileData_AsString(resultFolderLocation).split("\\n");

		String nextLineDocID1_FromReportFile = return_NextLine_FromFile_ByLineText(reportFolderLocation, "Batch ID: 410692");

		String docIDReportFile_ForDocID1 = nextLineDocID1_FromReportFile.substring(54, 57).trim();
		String fieldsSubmittedValue_ForDocID1 = nextLineDocID1_FromReportFile.substring(93, 106).trim();
		String avgConfForReportFile_ForDocID1 = nextLineDocID1_FromReportFile.substring(117, 125).split("\\.")[0].trim();

		for (String aResultFileExtract : resultFileExtract) {

			if (aResultFileExtract.contains("<doc_id>")) {

				String resultDocIDValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "doc_id");
				if (resultDocIDValue.equals("1")) {
					assertThat("Compare DocID Value from Result File is equal to DocID Value in ReportFile",
							resultDocIDValue, equalTo(docIDReportFile_ForDocID1));
				}
			} else if (aResultFileExtract.contains("<itemCount>")) {

				resultItemCountValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aResultFileExtract, "itemCount");
				assertThat("Compare ItemCount Value from Result File is equal to FieldsSubmitted value of ReportFile",
						Integer.valueOf(resultItemCountValue), equalTo(Integer.valueOf(fieldsSubmittedValue_ForDocID1)));
			}
		}

		BaseUI.assertThat("Compare Average Confidence Value from Result file is greater or equal to Average Confidence Value in ReportFile",
				String.valueOf(averageConfForResultFileDocID1), greaterThanOrEqualTo(avgConfForReportFile_ForDocID1));
	}



	private static int return_AverageConfidenceValue_ByDocID(String docID, String resultFolderLocation) throws Exception {

		XMLParser parser;
		int totalValue = 0;
		int loopCount = 0;
		int averageConfValue = 0;

		String confXpath = "//doc_id[./text()='"+ docID + "']/following-sibling::fields//results//conf";
		parser = new XMLParser(resultFolderLocation);
		NodeList test_ConfNode = parser.findNodes(confXpath);

		for (int i = 0; i < test_ConfNode.getLength(); i++) {
			String confValue = "";
			if (test_ConfNode.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element) test_ConfNode.item(i);
				if (el.getTagName().equals("conf")) {
					confValue = el.getTextContent();
				}
				if (Integer.valueOf(confValue) >= 65) {
					totalValue += Integer.valueOf(confValue);
					loopCount++;

				}
			}
		}
		if (loopCount>0) {
			averageConfValue = totalValue / loopCount;
		}
		return averageConfValue;
	}

	private static int return_ConfidenceValueCount_LessThan_SixtyFive(String sourceFolderLocation) throws Exception {

		String[] sourceFileExtract = DataBuilder.Get_FileData_AsString(sourceFolderLocation).split("\\n");
		int loopCountForConfValue = 0;
		for (String aSourceFileExtract : sourceFileExtract) {
			if (aSourceFileExtract.contains("<conf>")) {

				String sourceConfValue = ImageRPS_Utilities.return_Line_WithoutXMLTags(aSourceFileExtract, "conf");
				if (Integer.valueOf(sourceConfValue) != 0 && Integer.valueOf(sourceConfValue) < 65) {
					loopCountForConfValue++;
				}
			}
		}
		return loopCountForConfValue;
	}

	private static String return_NextLine_FromFile_ByLineText(String folderLocation, String lineText) throws IOException {

		FileReader file = new FileReader(folderLocation);
		BufferedReader bufferedReader = new BufferedReader(file);
		String lines;

		String nextLine = "";
		while ((lines = bufferedReader.readLine()) != null) {
			if (lines.startsWith(lineText)) {
				nextLine = bufferedReader.readLine();
			}
		}
		return nextLine;
	}

}//end of class
