package imageRPS.tests_Smoke;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Alerts;
import imageRPS.pages.AlertsEvents;
import imageRPS.pages.Dashboard;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_BatchTracker;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Consolidations_Filters;
import imageRPS.pages.DataMaintenance_ItemSearch_Filters;
import imageRPS.pages.Events;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.Help;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.ReportsOutput;
import imageRPS.pages.ReportsOutput_ConfiguredReports;
import imageRPS.pages.ReportsOutput_Reformatter;
import imageRPS.pages.Utilities;
import imageRPS.pages.Utilities_ConcurrentUsers;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.Browser;
import utils.ResultWriter;

public class Navigation_TopMenu extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 2)
	public void PT5319_Dashboard() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		Dashboard.verify_Dashboard_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_DataCompletion() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion.verify_DataCompletionPage_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_DataMaintenance_Batches() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Batches");
		DataMaintenance_Batch_Filters.verify_Batches_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_DataMaintenance_Consolidations() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");
		DataMaintenance_Consolidations_Filters.verify_DataMaint_ConsolidationsPage_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_DataMaintenance_ItemSearch() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Item Search");
		DataMaintenance_ItemSearch_Filters.verify_DataMaint_ItemSearchFilters_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_DataMaintenance_BatchTracker() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Batch Tracker");
		DataMaintenance_BatchTracker.verify_DataMaint_BatchTracker_Loaded();
		DataMaintenance_BatchTracker.verify_BatchTracker_ActivitySelected("Process Batch");
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_ReportsOutput_Reformatter() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.ReportsOutput);
		ReportsOutput.navigate_SubTab("Reformatter");
		ReportsOutput_Reformatter.verify_Reformatter_TabLoadedSuccessfully();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_ReportsOutput_ConfiguredReports() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.ReportsOutput);
		ReportsOutput.navigate_SubTab("Configured Reports");
		ReportsOutput_ConfiguredReports.verify_ConfiguredReports_TabActive();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_Utilities_default_Print_Ticket() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities.navigate_SubTab("Print Ticket");
		Utilities_PrintTicket_BatchTicketFilter.verify_PrintTicket_TabLoadedSuccessfully();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_Utilities_ConcurrentUsers() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities.navigate_SubTab("Concurrent Users");
		Utilities_ConcurrentUsers.verify_TabLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_AlertsEvents_Alerts() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.AlertsEvents);
		AlertsEvents.navigate_SubTab("Alerts");
		Alerts.verify_Alerts_SubTabActive();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_AlertsEvents_Events() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.AlertsEvents);
		AlertsEvents.navigate_SubTab("Events");
		Events.verify_Events_SubTabActive();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5319_Help() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.Help);
		Help.verify_default_HelpPageLoaded();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		ReportsOutput_Reformatter.click_OK_OnError_IfExists();
		Navigation.click_OK_OnError_IfExists();

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}// End of Class
