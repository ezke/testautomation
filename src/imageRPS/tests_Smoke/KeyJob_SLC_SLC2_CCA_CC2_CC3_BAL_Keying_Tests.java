package imageRPS.tests_Smoke;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Account;
import imageRPS.pages.DataCompletion_Balancing;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.DataCompletion_Scanline;
import imageRPS.pages.DataCompletion_bal;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_SLC_SLC2_CCA_CC2_CC3_BAL_Keying_Tests extends baseTest {

	String docType = "";
	String keyingJob = "slc";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String fieldName = "";
	String genericValue = "1";
	String clientID;
	String docID;
	

	Integer minValue;
	Integer maxValue;
	Integer index_Of_Field = null;

	TableData batchStuff;

	HashMap<String, String> jobInfo;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchStuff = Navigation.return_ConfigData();

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		batch = BaseUI.random_NumberAsString(800001, 800010);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
		DataCompletion.verify_CorrectJob(keyingJob);

		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion.return_DocID();
		jobInfo = batchStuff.return_Row_BasedOn_2MatchingFields("client_id", clientID, "doc_id", docID);
		maxValue = Integer.parseInt(jobInfo.get("field_len"));

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}
//slc job
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
	public void PT5071_Key_Job_MaxValues() throws Exception {
		// DataCompletion_Scanline.key_allFields_UntilDone(maxValue);
		String valueToKey = "750568958664759112022015000200000482551";
		DataCompletion_Scanline.key_allFields_UntilDone(valueToKey);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 2)
	public void PT5071_Key_Job_Confirm_Takes_To_sl2() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_CorrectJob("act");

	}

//act job

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 3)
	public void PT5071_Key_Job_MaxValues_act() throws Exception {

		DataCompletion_Account.key_allFields_UntilDone(clientID);
		fieldName = DataCompletion_Account.return_first_fieldName();
		genericValue = DataCompletion_Account.return_first_textField();
		// DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 4)
	public void PT5071_Key_Job_Confirm_Takes_To_cca() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_CorrectJob("cca");

	}

// cca job
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 5)
	public void PT5071_CCA_KeyTo_CC2() throws Exception {
		DataCompletion_Balancing.key_through_Batch("1");
		DataCompletion.click_Yes_ToConfirm();
		keyingJob = "cc2";
		DataCompletion.verify_CorrectJob(keyingJob);

	}
//cc2 job
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 6)
	public void PT5071_CC2_KeyTo_BAL() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion_Balancing.key_through_Batch("2");
		DataCompletion.click_Yes_ToConfirm();
		keyingJob = "bal";
		DataCompletion.verify_CorrectJob(keyingJob);
	}

	// bal
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 50)
	public void PT5071_Balance_StubsTooHigh() throws Exception {
		seqNumber = DataCompletion_bal.return_firstSeqNumber();
		String amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_StbAmount"));
		amountToEnter = amountToEnter.replace(",", "");
		Double amountToEnter_Double = Double.parseDouble(amountToEnter);
		amountToEnter_Double = amountToEnter_Double - .01;
		amountToEnter = amountToEnter_Double.toString();
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.verify_StubsTooHigh("0.01");
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 100)
	public void PT5071_Balance_FirstItem() throws Exception {
		DataCompletion_bal.balance_OneAmount();
		DataCompletion_bal.balance_OneAmount();
		DataCompletion_bal.verify_Balanced();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 200)
	public void PT5071_Balance_RemainingItems() throws Exception {
		DataCompletion_bal.key_Through_AllBalanceItems();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 300)
	public void PT5071_Balance_ConfirmModal_ClickYes() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_bal.wait_ForAlert_ToAppear();
		DataCompletion_bal.verify_Alert_DialogPresent();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 400)
	public void PT5071_Balance_Click_OK_OnAlert_TakesOutOfJob() throws Exception {
		DataCompletion_bal.click_OK_ForAlertModal();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 500)
	public void PT5071_Balance_VerifyAuditTrail() throws Exception {

		Navigation.navigate_to_Batch_ViaCommand(batch);
		DataMaintenance.verify_Bi_Keying_Job();

		DataMaintenance_Batch.click_ViewItems();
		index_Of_Field = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				seqNumber);

		DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToRight();
		HashMap<String, String> viewItemRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(index_Of_Field);
		BaseUI.baseStringPartialCompare("Audit Trail", keyingJob + "," + GlobalVariables.return_UserName_Formatted() + ",",
				viewItemRow.get("Audit Trail"));
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
