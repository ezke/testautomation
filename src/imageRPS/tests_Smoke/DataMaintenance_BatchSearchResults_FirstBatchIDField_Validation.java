package imageRPS.tests_Smoke;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

import java.util.ArrayList;

public class DataMaintenance_BatchSearchResults_FirstBatchIDField_Validation extends baseTest {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
    }

    @Test(groups = { "all_Tests", "smoke_Tests" , "module_DataMaintenance_BatchFilter" })
    public void PT4452_DataMaint_BatchResults_firstBatchID() throws Exception {
        Integer batchToSelect = 7;
        ArrayList<String> batchIDList;
        DataMaintenance_Batch_Filters.click_Submit();

        batchIDList = DataMaintenance_Batch_SearchResults.return_ColumnTextList_ByHeader("Batch ID");
        DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
        DataMaintenance_Batch_Filters.enter_BatchID_First_And_Second(batchIDList.get(batchToSelect), null);
        DataMaintenance_Batch_Filters.click_Submit();

        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Batch ID",
                batchIDList.get(batchToSelect));
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Batch ID",
                batchIDList.get(batchToSelect));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

        DataMaintenance_Batch_Filters.click_Cancel_OnModal_IfAppears();

        if (BaseUI.elementExists("dataMaint_BatchFilter_Link", null, null)) {
            DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
            DataMaintenance_Batch_Filters.click_ResetFilters();
        }

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
