package imageRPS.tests;

import java.util.HashMap;
import java.util.Objects;

import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.*;

public class DataMaintenance_Consolidations_ValidateBatch extends baseTest {

	String keyingJob = "bal";

	TableData searchResults;

	private String client_ID = "602";
    private String consolAmountColumn = "Consol Amount";
    private String consolChecksColumn = "Consol Checks";
    private String consolBatchesColumn = "Consol Batches";
    private String openAmountColumn = "Open Amount";
    private String openChecksColumn = "Open Checks";
    private String openBatchesColumn = "Open Batches";

    private String expectedConsolAmountValue = "0";
    private String expectedConsolChecksValue ="0";
    private String expectedConsolBatchesValue = "0";
    private String expectedConsolAmountValueAfterProcess = "$37,000.00";
    private String expectedConsolChecksValueAfterProcess ="20";
    private String expectedConsolBatchesValueAfterProcess = "2";

    private int index_OfSeq11;
    private HashMap<String, String> viewItemRow11;
    private String consolCloseDateValue;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

	    Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");

		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(client_ID);
		DataMaintenance_Consolidations_Filters.click_Submit();

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 10)
	public void PT4541_DataMaintenance_Verify_ConsolAmount() throws Exception {

		String consolAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn).replace("$", "").split("\\.")[0];
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValue, consolAmountValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 15)
	public void PT4541_DataMaintenance_Verify_ConsolChecksAmt() throws Exception {

		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumnValues", expectedConsolChecksValue, consolChecksValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 20)
	public void PT4541_DataMaintenance_Verify_ConsolBatch() throws Exception {

		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchesColumnValues", expectedConsolBatchesValue, consolBatchesValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 25)
	public void PT4541_DataMaintenance_Verify_ConsolOpenAmt() throws Exception {

        String expectedOpenAmountValue = "$55,500.00";
	    String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValues", expectedOpenAmountValue, openAmountValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 30)
	public void PT4541_DataMaintenance_Verify_ConsolOpenChks() throws Exception {

        String expectedOpenChecksValue = "30";
	    String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openChecksColumn);
		BaseUI.baseStringCompare("OpenChecksColumnValues", expectedOpenChecksValue, openChecksValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 35)
	public void PT4541_DataMaintenance_Verify_ConsolOpenBatch() throws Exception {

        String expectedOpenBatchesValue = "3";
	    String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn);
		BaseUI.baseStringCompare("OpenBatchesColumnValues", expectedOpenBatchesValue, openBatchesValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 40)
	public void PT4541_Verify_NavigateToBalJob() throws Exception {

		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_OpenBatches();
		DataMaintenance_Consolidations_SearchResults.select_Row_ByIndex(1);
		DataMaintenance_Consolidations_SearchResults.click_KeyBatch();
		DataCompletion.verify_CorrectJob(keyingJob);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 45)
	public void PT4541_Balance_KeyFirstBatch() throws Exception {

		String amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 50)
	public void PT4541_VerifyAlertMsg() throws Exception {
		DataCompletion_bal.click_Yes_ToConfirm_AndWaitForAlert();
		DataCompletion_bal.verify_Alert_DialogPresent();
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 55)
	public void PT4541_Balance_Balance_KeySecoundBatch() throws Exception {

		DataCompletion_bal.click_OK_ForAlertModal();
		String batch = "700228";
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		String amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 60)
	public void PT4541_VerifyAlertMsg2Batch() throws Exception {
		DataCompletion_bal.click_Yes_ToConfirm_AndWaitForAlert();
		DataCompletion_bal.verify_Alert_DialogPresent();

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 65)
	public void PT4541_DataMaintenance_Verify_ConsolAmount1() throws Exception {

		DataCompletion_bal.click_OK_ForAlertModal();
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");
		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(client_ID);
		DataMaintenance_Consolidations_Filters.click_Submit();
		DataMaintenance_Consolidations_SearchResults.refresh_And_Wait_ForColumnValueToBeUpdated(consolAmountColumn, expectedConsolAmountValueAfterProcess, 120);
		String consolAmountValueAfterProcess = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn);
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValueAfterProcess, consolAmountValueAfterProcess);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 75)
	public void PT4541_DataMaintenance_Verify_ConsolChecksAmt1() throws Exception {

		String consolChecksValueAfterProcess = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumnValues", expectedConsolChecksValueAfterProcess, consolChecksValueAfterProcess);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 80)
	public void PT4541_DataMaintenance_Verify_ConsolBatch1() throws Exception {

		String consolBatchesValueAfterProcess = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchesColumnValues", expectedConsolBatchesValueAfterProcess, consolBatchesValueAfterProcess);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 85)
	public void PT4541_DataMaintenance_Verify_ConsolOpenAmt1() throws Exception {

        String expectedOpenAmountValueAfterProcess = "$18,500.00";
	    String openAmountValueAfterProcess = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValues", expectedOpenAmountValueAfterProcess, openAmountValueAfterProcess);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations" }, priority = 90)
	public void PT4541_DataMaintenance_Verify_ConsolOpenChks1() throws Exception {

        String expectedOpenChecksValueAfterProcess = "10";
	    String openChecksValueAfterProcess = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openChecksColumn);
		BaseUI.baseStringCompare("OpenChecksColumnValues", expectedOpenChecksValueAfterProcess, openChecksValueAfterProcess);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 100)
	public void PT4541_DataMaintenance_Verify_ConsolOpenBatch1() throws Exception {

        String expectedOpenBatchesValueAfterProcess = "1";
	    String openBatchesValueAfterProcess = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn);
		BaseUI.baseStringCompare("OpenBatchesColumnValues", expectedOpenBatchesValueAfterProcess, openBatchesValueAfterProcess);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 105)
	public void PT4541_DataMaintenance_Verify_ConsolBatchExists() throws Exception {

		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_ConsolBatches_Button();
		String checkTotalColumnValueIndex1 = DataMaintenance_Batch_SearchResults
				. return_Cell_ByColumnHeader_AndIndex("Check Total", 1);
		String checkTotalColumnValueIndex2 = DataMaintenance_Batch_SearchResults
				. return_Cell_ByColumnHeader_AndIndex("Check Total", 2);
		BaseUI.baseStringCompare("CheckTotalColumnValue", "$18,500.00", checkTotalColumnValueIndex1);
		BaseUI.baseStringCompare("CheckTotalColumnValue", "$18,500.00", checkTotalColumnValueIndex2);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 110)
	public void PT4542_DataMaintenance_Verify_ConsolBatchExists_BatchInfo() throws Exception {

//		Step 17
		String consolDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy") + " #1";
		DataMaintenance_Batch_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_ConsolBatches.click_BatchInfo();
		DataMaintenance_Batch.expand_Section(DataMaintenance_Batch.collapsible_SectionName.Pass2Info);
		DataMaintenance_Batch.verify_Pass2Info_Value_ByText("Consolidation:", consolDate);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 115)
	public void PT4545_DataMaintenance_Verify_ConsolBatchExists_ViewItems() throws Exception {

//		Step 16
		DataMaintenance_Consolidations_SearchResults.click_ConsolResult_BreadcrumbLink();
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_ConsolBatches_Button();
		DataMaintenance_Batch_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_ConsolBatches.click_ViewItems();
		index_OfSeq11 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				"11");
		int index_OfSeq12 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				"12");
		viewItemRow11 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(index_OfSeq11);
		DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
		HashMap<String, String> viewItemRow12 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(index_OfSeq12);
		BaseUI.baseStringCompare("AppliedAmountColumnValue", "$500.00", viewItemRow11.get("Applied Amount"));
		BaseUI.baseStringCompare("AppliedAmountColumnValue", "$500.00", viewItemRow12.get("Applied Amount"));
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 120)
	public void PT4545_DataMaintenance_Verify_ConsolBatchExists_ViewItems_TransInfo() throws Exception {

//		Step 17
		DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
		DataMaintenance_Consolidations_SearchResults.select_Row_ByIndex(index_OfSeq11);
		DataMaintenance_Batch_ViewItems.click_TransInfo_Button();
		DataMaintenance_TransInfo.verify_Check_And_Stub_Totals(viewItemRow11.get("Applied Amount"));
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 125)
	public void PT4546_DataMaintenance_Verify_ConsolBatchExists_ViewItems_ItemInfo() throws Exception {

//		Step 16
		DataMaintenance_TransInfo.click_GoBack();
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		DataMaintenance_ItemInfo.verify_P1SeqNumber(viewItemRow11.get("P1 Seq Number"));
		DataMaintenance_ItemInfo.verify_AppliedAmount(viewItemRow11.get("Applied Amount"));
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 130)
	public void PT4593_DataMaintenance_Verify_ConsolBatchExists_ViewAutoConsol_BasicInfo() throws Exception {

//		Step 6
		DataMaintenance_Consolidations_SearchResults.click_ConsolResult_BreadcrumbLink();
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_ViewAutoConsol();
		DataMaintenance_Consolidations_SearchResults.verify_ViewAutoConsol_BasicInfo(client_ID,"Consol, Test", "1", "No", "No" );

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 135)
	public void PT4593_DataMaintenance_Verify_ConsolBatchExists_ViewAutoConsol_EventInfo() throws Exception {

//		Step 6
		DataMaintenance_Consolidations_SearchResults.verify_ViewAutoConsol_EventInfo();

	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 140)
	public void PT4593_DataMaintenance_Verify_ConsolBatchExists_ViewAutoConsol_DepositAndReportInfo() throws Exception {

//		Step 6
		DataMaintenance_Consolidations_SearchResults.verify_ViewAutoConsol_DepositAndReportInfo();
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 145)
	public void PT4577_DataMaintenance_Verify_ConsolBatch_ChangeAutoConsolTime_AdjustTimeByAnOffset() throws Exception {

//		Step 7
		String newHour = "1";
		String newMinute = "30";
		DataMaintenance_Consolidations_SearchResults.click_ConsolResult_BreadcrumbLink();
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_AdjustByHrs"), newHour);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_AdjustByMns"), newMinute);
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime_OkBtn();
//		Auto Consol Close Date time is set to 9:00 PM for this client
		String expectedConsolCloseDateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0) + " 10:30 PM";
		String consolCloseDateTime = DataMaintenance_Consolidations_SearchResults
				. return_ConsolSearchResults("Consol Close Date Time");
		BaseUI.baseStringCompare("ConsolCloseDateTimeValue", expectedConsolCloseDateTime, consolCloseDateTime);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 155)
	public void PT4615_DataMaintenance_Verify_ConsolBatch_ChangeAutoConsolTime_DatesTimesToNewValue() throws Exception {

//		Step 15
		String newDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime();
		DataMaintenance_Consolidations_SearchResults.click_ChageDatesTimesToNewValue();

		DataMaintenance_Consolidations_SearchResults.enter_ChangeAutoConsolidationTime_NewDate(newDate);
		String newTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "hh:mm a");
		newTime = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(newTime, "hh:mm a", "hh:mm a", 1);

		DataMaintenance_Consolidations_SearchResults.enter_ChangeAutoConsolidationTime_NewTime(newTime);
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime_OkBtn();

		String dateFormat = "MM/dd/yyyy hh:mm a";
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
		String firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
				dateFormat, -3);
		String finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
				dateFormat, 3);

		String consolCloseDateTime = DataMaintenance_Consolidations_SearchResults
				. return_ConsolSearchResults("Consol Close Date Time");
		BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, consolCloseDateTime, dateFormat);

	}


	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 160)
	public void PT4554_DataMaintenance_Verify_StopConsol_NextConsolidationConfirmationModal_ConsolDate() throws Exception {

//		Step 17
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_StopConsol_Auto();
		TableData nextConsolidationDateConfirmation_Results = DataMaintenance_Consolidations_SearchResults
				.return_NextConsolidationDateConfirmation_Modal_Table();
		HashMap<String, String> nextConsolidationRow = nextConsolidationDateConfirmation_Results
				.return_Row_BasedOn_1MatchingField("Client ID", client_ID);
		BaseUI.baseStringCompare("StopConsolDate", currentDate, Objects.requireNonNull(nextConsolidationRow).get("Consol Date") );
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 161)
	public void PT4554_DataMaintenance_Verify_ConsolAmount_StopConsol() throws Exception {

		DataMaintenance_Consolidations_SearchResults.click_StopConsol_MsgAlert_ConfirmBtn();
		String consolAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn).replace("$", "").split("\\.")[0];
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValue, consolAmountValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_Consolidations" }, priority = 165)
	public void PT4554_DataMaintenance_Verify_ConsolChecksAmt_StopConsol() throws Exception {

//		Step 17
		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumnValues", expectedConsolChecksValue, consolChecksValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 170)
	public void PT4554_DataMaintenance_Verify_ConsolBatch_StopConsol() throws Exception {

//		Step 17
		consolCloseDateValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults("Consol Close Date Time");
		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchesColumnValues", expectedConsolBatchesValue, consolBatchesValue);

	}


	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 175)
	public void PT4554_DataMaintenance_Verify_ConsolBatch_NoBatchExist() throws Exception {

//		Step 18
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_ConsolBatches_Button();
		DataMaintenance_Consolidations_SearchResults.verify_dataMaint_ConsolBatches_NoBatchAvaliable();

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 180)
	public void PT4555_DataMaintenance_Verify_ConsolBatch_OneBatchExist() throws Exception {

//		Step 9
		DataMaintenance_Consolidations_SearchResults.click_ConsolResult_BreadcrumbLink();
		DataMaintenance_Consolidations_SearchResults.select_FirstRow();
		DataMaintenance_Consolidations_SearchResults.click_OpenBatches();

		String batchCount = "1-1";
		String newBatchcount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ConsolBatches_BatchCount"));
		BaseUI.baseStringCompare("Batch_Count", batchCount, newBatchcount);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 185)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ClientID() throws Exception {

//		Step 14
		DataMaintenance_Consolidations_SearchResults.click_ConsolFilter_BreadcrumbLink();
		DataMaintenance_Consolidations_Filters.click_ShowCurrent();
		DataMaintenance_Consolidations_Filters.click_ShowPast();
		DataMaintenance_Consolidations_Filters.click_Submit();

		String clientIDValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue("Client ID", "1");
		BaseUI.baseStringCompare("ClientIDColumnValue", client_ID, clientIDValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 190)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ConsolDate() throws Exception {

//		Step 14
		String consolDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
		String consolDateValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue("Consol Date", "1");
		BaseUI.baseStringCompare("ConsolDateColumnValue", consolDate, consolDateValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 195)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ConsolAmount() throws Exception {

//		Step 14
		String consolAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue(consolAmountColumn, "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValueAfterProcess, consolAmountValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 200)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ConsolChecks() throws Exception {

//		Step 14
		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue(consolChecksColumn, "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolChecksValueAfterProcess, consolChecksValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 205)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ConsolBatches() throws Exception {

//		Step 14
		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue(consolBatchesColumn, "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolBatchesValueAfterProcess, consolBatchesValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 210)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_OpenAmount() throws Exception {

//		Step 14
		String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue(openAmountColumn, "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", "0", openAmountValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 215)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_OpenChecks() throws Exception {

//		Step 14
		String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue(openChecksColumn, "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", "0", openChecksValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 220)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_OpenBatches() throws Exception {

//		Step 14
		String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue(openBatchesColumn, "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", "0", openBatchesValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 225)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ConsolCutoffDateTime() throws Exception {

//		Step 14
		String consolDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
		String consolCutoffDateTimeValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue("Consol Cutoff Date Time", "1");
		BaseUI.baseStringCompare("ConsolAmountColumnValue", consolDate + " 09:00 PM", consolCutoffDateTimeValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 230)
	public void PT4620_DataMaintenance_Verify_ConsolBatch_StopConsolidation_PastConsolidation_ConsolCloseDateTime() throws Exception {

//		Step 14
		String consolCloseDateTimeValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResultsByRowsIndexValue("Consol Close Date Time", "1").substring(0, 10);
		BaseUI.baseStringCompare("ConsolAmountColumnValue", consolCloseDateValue.substring(0,10), consolCloseDateTimeValue);

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 235)
	public void PT4635_DataMaintenance_Verify_ConsolBatch_StopConsolidation_ReExportConsol() throws Exception {

//		Step 30
        String remote2_Cust1_Archive_IMS = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\test\\archive\\IMS";
        String datFileName1 = "02032015_0602_700227_02032015_01.dat";
        String datFileName2 = "02032015_0602_700228_02032015_01.dat";

		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ByIndex(1);
		DataMaintenance_Consolidations_SearchResults.click_ReExportConsol_Button();
		DataMaintenance_Consolidations_SearchResults.click_AllReExport_CheckBoxes();
		DataMaintenance_Consolidations_SearchResults.click_ReExportConsol_OKButton();
		FileOperations.wait_ForFile_WithExtension_ToExist(".dat", remote2_Cust1_Archive_IMS, 120);
		FileOperations.verify_FileFound(remote2_Cust1_Archive_IMS, datFileName1);
		FileOperations.verify_FileFound(remote2_Cust1_Archive_IMS, datFileName2);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

        String datFile1 = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\test\\archive\\IMS\\02032015_0602_700227_02032015_01.dat";
        String datFile2 = "\\\\rps602auto1dcm.qalabs.nwk\\k$\\cust1\\test\\archive\\IMS\\02032015_0602_700228_02032015_01.dat";
	    Browser.closeBrowser();
		FileOperations.cleanup_PriorFile(datFile1);
		FileOperations.cleanup_PriorFile(datFile2);
	}

}// End of Class
