package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_ex;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class KeyJob_ex_Keying_Tests extends baseTest {

	String keyingJob = "ex$";
	String batch = "";
	// String valueToEnter = "";
	String seqNumber = "";

	String basePayment = "20000";
	String feesBackInterest = "10000";
	String escrow = "823";
	String paycodeInput = "1";
	String donation = "500";
	String addPrincipal = "";


	Integer index_Of_Field = null;


	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		// 700131-700152
		batch = BaseUI.random_NumberAsString(700206, 700216);
		batch = "700209";

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3704_Balance_verify_AppliedAmont_Msg_ToAppear() throws Exception {
		DataCompletion_ex.verify_AppliedAmont_Msg_ToAppear();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3704_Balance_StubsTooHigh() throws Exception {

		DataCompletion_ex.key_Through_AllBatch(basePayment, feesBackInterest, escrow, paycodeInput, donation);
		DataCompletion_ex.wait_ForConfirm_ToAppear();
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Modal_Message"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3704_Balance_ConfirmModal_ClickYes() throws Exception {
		
		DataCompletion.click_Yes_ToConfirm();
		// DataCompletion_bal.wait_ForAlert_ToAppear();
		DataCompletion_ex.verify_Alert_DialogPresent();
		DataCompletion_ex.verify_AppliedAmont_Msg_Not_ToAppear();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT3704_Balance_ConfirmModal_ClickOK() throws Exception {
		
		DataCompletion_ex.click_OK_ToConfirm();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);
		// DataCompletion.click_Yes_ToConfirm();

		// DataCompletion_ex.verify_Alert_DialogPresent();
	}


	

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
