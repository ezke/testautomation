package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_CC3_KeyDifferentValues_Accept extends baseTest {

    private String keyingJob = "cca";
    private String batch = "702759";
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3759_Step6_To_7_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3759_Step8_To_9_CC2_KeyTo_CC3_Verify_Job() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3759_Step10_CC3_VerifySeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3759_Step11_CC3_VerifySeqAgain_WithDifferentValue_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("4");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3759_Step12_CC3_VerifySeqAgain_WithSameValue_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");

    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3759_Step13_CC3_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3759_Step14_CC3_VerifySeq_Accept() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_NOT_Rejected(1);
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
        
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3759_Step15_CC3_VeifyFourthSeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 90)
    public void PT3759_Step16_CC3_Verify_FourthSeqAgain_WithDifferentValue_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("4");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 100)
    public void PT3759_Step17_CC3_VerifyConfirmMessage_FourthSeq() throws Exception {

        String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.04).\n" +
                "Please reject the item if you can" + openApostropheUnicode + "t read it.";
        DataCompletion_Balancing.enterText_Into_LastInputBox("4");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 110)
    public void PT3759_Step18_CC3_VerifyFourthSeq_Accept() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_NOT_Rejected(1);
        DataCompletion_Balancing.verify_Seq_ByIndex(3, "6");

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
