package imageRPS.tests;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataMaintenance_BatchSearchResults_FilterTests extends baseTest {

	// Integer count_WithoutFilters;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		
	}

	
	
	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_BatchFilter"  })
	public void PT4483_DataMaint_BatchResults_WorkTypeList() throws Exception {
		WorkType workTypeToUse = WorkType.Single;

		DataMaintenance_Batch_Filters.select_WorkType(workTypeToUse);
		DataMaintenance_Batch_Filters.click_Submit();
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Work Type",
				workTypeToUse.getWorkType_Index().toString());

		DataMaintenance_Batch_SearchResults.select_FirstRow();
		DataMaintenance_Batch_SearchResults.click_BatchInfo();
		DataMaintenance_Batch.verify_WorkType(workTypeToUse);

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4480_DataMaint_BatchResults_ConsolDate_AndNumber() throws Exception {
		String consol_Date = "11/08/2016";
		String number = "1";

		DataMaintenance_Batch_Filters.enter_ConsolidationDate_AndNumber(consol_Date, number);
		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Consol Date", consol_Date);
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Number", number);
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Consol Date", consol_Date);
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Consol Num", number);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_BatchFilter"  })
	public void PT4467_DataMaint_BatchResults_LocationID() throws Exception {
		Integer index = 2;

		String location1 = DataMaintenance_Batch_Filters.select_LocationID_From_FirstFilterBox_ByIndex(index);
		String locationText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_LocationID_firstInputBox"));
		BaseUI.baseStringCompare("First Location ID", location1, locationText);

		String location2Text = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_LocationID_secondInputBox"));
		BaseUI.baseStringCompare("Second Location ID", "", location2Text);
		DataMaintenance_Batch_Filters.click_Submit();
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Location ID", location1);
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Location ID", location1);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4461_AND_PT4517_DataMaint_BatchResults_ClientID_And_LockboxID_EnterClientID() throws Exception {
		Integer rowIndex = 5;
		HashMap<String, String> tableRow;

		DataMaintenance_Batch_Filters.click_Launch_SelectClient_Modal();
		tableRow = DataMaintenance_Batch_Filters.return_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_Filters.cancel_OutOf_Select_ClientModal();
		DataMaintenance_Batch_Filters.enter_ClientID(tableRow.get("Client ID"));

		String lockboxText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_lockboxID_TextBox"));
		BaseUI.baseStringCompare("Lockbox ID", tableRow.get("Lockbox ID"), lockboxText);

		DataMaintenance_Batch_Filters.click_Submit();
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Lockbox ID", tableRow.get("Lockbox ID"));
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Lockbox ID",
				tableRow.get("Lockbox ID"));
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Client ID", tableRow.get("Client ID"));
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Client ID", tableRow.get("Client ID"));
		
		//231077 or 4517
		DataMaintenance_Batch_SearchResults.click_GoBack();
		DataMaintenance_Batch_Filters.click_ResetFilters();
		lockboxText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_lockboxID_TextBox"));
		BaseUI.baseStringCompare("Lockbox ID", lockboxText, "");
		String clientIDText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_clientID_TextBox"));
		BaseUI.baseStringCompare("Client ID", clientIDText, "");
		
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4460_DataMaint_BatchResults_ClientID_And_LockboxID_SelectFromPopup() throws Exception {
		Integer rowIndex = 5;
		HashMap<String, String> tableRow;

		DataMaintenance_Batch_Filters.click_Launch_SelectClient_Modal();
		tableRow = DataMaintenance_Batch_Filters.return_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_Filters.select_Client_FromSelectClient_Modal(rowIndex);
		DataMaintenance_Batch_Filters.click_SelectClient();

		String lockboxText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_lockboxID_TextBox"));
		BaseUI.baseStringCompare("Lockbox ID", tableRow.get("Lockbox ID"), lockboxText);

		String clientText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_clientID_TextBox"));
		BaseUI.baseStringCompare("Client ID", tableRow.get("Client ID"), clientText);

		DataMaintenance_Batch_Filters.click_Submit();
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Lockbox ID", tableRow.get("Lockbox ID"));
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Lockbox ID",
				tableRow.get("Lockbox ID"));
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Client ID", tableRow.get("Client ID"));
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Client ID", tableRow.get("Client ID"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4454_DataMaint_BatchResults_first_And_second_BatchID() throws Exception {

		ArrayList<String> batchIDList;
		DataMaintenance_Batch_Filters.click_Submit();

		batchIDList = DataMaintenance_Batch_SearchResults.return_ColumnTextList_ByHeader("Batch ID");
		DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
		DataMaintenance_Batch_Filters.enter_BatchID_First_And_Second(batchIDList.get(5), batchIDList.get(12));
		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_Column_Values_AreBetweenRange("Batch ID",
				Integer.parseInt(batchIDList.get(5)), Integer.parseInt(batchIDList.get(12)));
		DataMaintenance_Batch_SearchResults.verify_Filter_Range_Text_ContainsFilter("Batch ID", batchIDList.get(5),
				batchIDList.get(12));
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4452_DataMaint_BatchResults_firstBatchID() throws Exception {
		Integer batchToSelect = 7;
		ArrayList<String> batchIDList;
		DataMaintenance_Batch_Filters.click_Submit();

		batchIDList = DataMaintenance_Batch_SearchResults.return_ColumnTextList_ByHeader("Batch ID");
		DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
		DataMaintenance_Batch_Filters.enter_BatchID_First_And_Second(batchIDList.get(batchToSelect), null);
		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Batch ID",
				batchIDList.get(batchToSelect));
		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Batch ID",
				batchIDList.get(batchToSelect));
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4482_DataMaint_BatchResults_DocGroupID() throws Exception {
		String docGroup = "1";

		DataMaintenance_Batch_Filters.enter_DocGroupID(docGroup);
		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Doc Group", docGroup);
		DataMaintenance_Batch_SearchResults.select_FirstRow();
		DataMaintenance_Batch_SearchResults.click_BatchInfo();
		DataMaintenance_Batch.verify_DocGroupID(docGroup);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4511_DataMaint_BatchResults_KeyingJobs() throws Exception {
		String[] jobs = { "doc", "act" };

		DataMaintenance_Batch_Filters.select_Key_Jobs_FromModal(jobs);
		String keyJob_Textbox_Value = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_KeyJobs_Textbox"));
		BaseUI.baseStringPartialCompare("Key Job", jobs[0], keyJob_Textbox_Value);
		BaseUI.baseStringPartialCompare("Key Job", jobs[1], keyJob_Textbox_Value);

		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Key Job", jobs[0] + "," + jobs[1]);
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValues("Keying Job", jobs);

	}

	//Source WF or Source Job.
	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4512_DataMaint_BatchResults_SourceWF() throws Exception {
		String[] jobs = { "done", "dcm", "shrd" };

		DataMaintenance_Batch_Filters.select_Select_Source_Jobs_FromModal(jobs);
		String sourceJob_Textbox_Value = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_WF_Textbox"));
		BaseUI.baseStringPartialCompare("Source Job", jobs[0], sourceJob_Textbox_Value);
		BaseUI.baseStringPartialCompare("Source", jobs[1], sourceJob_Textbox_Value);
		BaseUI.baseStringPartialCompare("Source", jobs[2], sourceJob_Textbox_Value);

		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Source WF", jobs[0] + "," + jobs[1] + "," + jobs[2]);
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValues("Source Job", jobs);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		
		DataMaintenance_Batch_Filters.click_Cancel_OnModal_IfAppears();

		if (BaseUI.elementExists("dataMaint_BatchFilter_Link", null, null)) {
			DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
			DataMaintenance_Batch_Filters.click_ResetFilters();
		}

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
