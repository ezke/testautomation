package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_SLC_PostItemPopup extends baseTest {

    private String batchID = "702005";
    private String keyingJob = "slc";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3831_Step14_SLC_OCR_Veify_Question_ModalBox() throws Exception {

        String expectedMessage ="Do you want to process multiple fields?";
        DataCompletion_Scanline.click_OCR();
        DataCompletion_Scanline.pressEnter_KeyingField();
        DataCompletion.verify_QuestionModal_Appears(expectedMessage);

    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3831_Step15_SLC_OCR_Verify_F1key() throws Exception {

        DataCompletion_Scanline.pressF1_QuestionOrConfirm_Modal();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Done_Confirm_YesButton"));
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3831_Step16_SLC_OCR_Key_To_SL2() throws Exception {

        DataCompletion_Scanline.key_allFields_UntilDone_OCR();
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "sl2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
