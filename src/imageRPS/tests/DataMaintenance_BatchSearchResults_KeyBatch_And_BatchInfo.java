package imageRPS.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;


public class DataMaintenance_BatchSearchResults_KeyBatch_And_BatchInfo extends baseTest {

	// Integer rowToSelect = 7;
	// TableData viewItemData;
	String batchNumber = "9460";
	String keyingJob = "";
	HashMap<String, String> tableRow;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance_Batch_Filters.enter_BatchID_AndSearch(batchNumber, "");

		keyingJob = DataMaintenance_Batch_SearchResults.return_columnValues_ByColumnHeaderName("Keying Job").get(0);
		DataMaintenance_Batch_SearchResults.select_Row_ByIndex(1);
		DataMaintenance_Batch_SearchResults.click_KeyBatch();
		Thread.sleep(100);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 10)
	public void PT4439_DataMaint_BatchResults_KeyJob() throws Exception {
		DataCompletion.verify_CorrectJob(keyingJob);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_ClientData_BatchId"), batchNumber);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 20)
	public void PT4439_DataMaint_BatchResults_Done_Returns_ToBatchResults() throws Exception {
		DataCompletion.end_DataCompletion();
		DataMaintenance_Batch_SearchResults.verify_On_BatchSearchResults_Page();
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 30)
	public void PT4438_DataMaint_BatchResults_BatchInformation() throws Exception {
		DataMaintenance_Batch_SearchResults.click_BatchInfo();
		DataMaintenance_Batch.verify_On_BatchInformation_Page();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 31)
	public void PT4438_DataMaint_BatchResults_BatchInformation_Non_CollapsibleSections_Displayed() throws Exception {
		DataMaintenance_Batch.verify_Non_Collapsible_Sections_Displayed();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 31)
	public void PT4438_DataMaint_BatchResults_BatchInformation_CollapsibleSections_NOT_Displayed() throws Exception {
		DataMaintenance_Batch.verify_Collapsible_Sections_NOT_Displayed();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 40)
	public void PT4439_DataMaint_BatchResults_BatchInformation_ExpandAll() throws Exception {
		DataMaintenance_Batch.expand_All();
		DataMaintenance_Batch.verify_Non_Collapsible_Sections_Displayed();
		DataMaintenance_Batch.verify_Collapsible_Sections_Displayed();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 50)
	public void PT4438_DataMaint_BatchResults_BatchInformation_CollapseAll() throws Exception {
		DataMaintenance_Batch.collapse_All();
		DataMaintenance_Batch.verify_Non_Collapsible_Sections_Displayed();
		DataMaintenance_Batch.verify_Collapsible_Sections_NOT_Displayed();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		tableRow = null;
		Browser.closeBrowser();
	}

}// End of Class
