package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.Browser;
import utils.ResultWriter;

//Runs too slowly in IE, website was timing out.  So I split the tests in half to run from 2 different classes.
public class DataMaintenance_BatchSearchResults_SortTests2 extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance_Batch_Filters.click_Submit();

	}

	@DataProvider(name = "SortTests")
	public Object[][] return_Tests_ObjectArray() {

		// Object[][] linksToNavigate = new Object[dataSet.size()][1];

		Object[][] tests_ToRun = { { "Block Num", "ascending_numeric" },
				{ "Block Num", "descending_numeric" }, /*{ "Pass2 Date", "ascending_date" },
				{ "Pass2 Date", "descending_date" }, */{ "Priority", "ascending_numeric" },
				{ "Priority", "descending_numeric" }, { "Location ID", "ascending_numeric" },
				{ "Location ID", "descending_numeric" }, { "P1 Stn ID", "ascending_numeric" },
				{ "P1 Stn ID", "descending_numeric" }, { "Receive Date", "ascending_date" },
				{ "Receive Date", "descending_date" }, { "Process Date", "ascending_date" },
				{ "Process Date", "descending_date" }, { "Consol Num", "ascending_numeric" },
				{ "Consol Num", "descending_numeric" }, { "SourceID", "ascending_numeric" },
				{ "SourceID", "descending_numeric" }, { "Source Job", "ascending_alpha" },
				{ "Source Job", "descending_alpha" } };

		return tests_ToRun;
	}

	@Test(dataProvider = "SortTests", groups = { "all_Tests", "regression_Tests" })
	public void PT4436_DataMaint_BatchResults_Sort(String headerText, String testType) throws Exception {
		DataMaintenance_Batch_SearchResults.run_SortTest_DataDriven(headerText, testType);
		
	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
