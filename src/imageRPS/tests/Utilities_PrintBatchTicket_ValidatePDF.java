package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_PrintBatchTicket_ValidatePDF extends baseTest {

	String clientID = "601";
	String docGroup = "98";
	WorkType workType = WorkType.Single;
	 TableData searchResults = new TableData();
	TableData pdfResults = new TableData();

	String expectedLockbox = "601601";
	String expectedDocGropup = "98 - Singles (Print Batch Ticket)";
	Integer batchRangeLower = 999900;
	Integer batchRangeUpper = 999906;

	Integer indexToCheck = 1;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(clientID, null, docGroup, workType);
		//searchResults = Utilities_PrintTicket_SearchResults.return_SearchResults2();
	
	}
	
	@DataProvider(name = "pdfResults")
	public Object[][] createData_PDFResults() throws Exception {
		Utilities_PrintTicket_SearchResults.check_Checkbox_ByIndex(indexToCheck);
		Utilities_PrintTicket_SearchResults.launch_PrintBatchTicket_PDF();
		pdfResults = Utilities_PrintTicket_SearchResults.return_BatchTicket_PDF_data();
		
		BaseUI.verify_true_AndLog(pdfResults.data.size() == 10, "Found 10 Search Results.", "Did NOT find 10 search results.");

		Object[][] rowObject = new Object[pdfResults.data.size()][2];

		for (Integer i = 0; i < pdfResults.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = pdfResults.data.get(i);
		}

		return rowObject;
	}

	@Test(dataProvider = "pdfResults", groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3349_Verify_Each_PDF_Entry_Matches_ExpectedResults(Integer index, HashMap<String, String> pdfEntry)
			throws Throwable {
		Utilities_PrintTicket_SearchResults.verify_BatchTicket_PDFEntry_AgainstExpectedResults(pdfEntry,
				expectedLockbox, expectedDocGropup, clientID, batchRangeLower, batchRangeUpper);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		pdfResults = null;

		Browser.closeBrowser();
	}
}// End of Class
