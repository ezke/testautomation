package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_Balancing;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.Help;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_CCA_CC2_CC3_Key_And_JobAction_Tests extends baseTest {

	String keyingJob = "cca";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String genericValue = "2000";
	String docType = "";

	String clientID;
	String docID;

	Integer index_Of_Field = null;
	TableData batchStuff;
	HashMap<String, String> jobInfo;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		// Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		// 700177-700180
		batch = BaseUI.random_NumberAsString(700162, 700170);

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

		DataCompletion.verify_CorrectJob(keyingJob);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3685_CCA_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3685_CCA_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT3685_CCA_Img2() throws Exception {
		DataCompletion_Balancing.verify_Img2_Button_ChangedImageZoom();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3685_CCA_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT3685_CCA_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT3685_CCA_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT3685_CCA_GrayButton_Displayed() throws Exception {
		DataCompletion.verify_Gray_ButtonEnabled();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public void PT3685_CCA_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
	public void PT3685_CCA_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 90)
	public void PT3685_CCA_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 100)
	public void PT3685_CCA_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 110)
	public void PT3685_CCA_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 120)
	public void PT3685_CCA_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 130)
	public void PT3685_CCA_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 140)
	public void PT3685_CCA_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 150)
	public void PT3685_CCA_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 151)
	public void PT3685_CCA_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 152)
	public void PT3685_CCA_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 153)
	public void PT3685_CCA_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 154)
	public void PT3685_CCA_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 155)
	public void PT3685_CCA_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 160)
	public void PT3685_CCA_Done_ModalVisible() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 161)
	public void PT3685_CCA_Done_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 170)
	public void PT3685_CCA_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct("cca");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 171)
	public void PT3686_CCA_KeyTo_CC2() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion_Balancing.key_through_Batch("1");
		DataCompletion.click_Yes_ToConfirm();
		keyingJob = "cc2";
		DataCompletion.verify_CorrectJob(keyingJob);
	}

	//
	// Run same suite of tests on CC2
	//

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 210)
	public void PT3686_CC2_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 220)
	public void PT3686_CC2_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 225)
	public void PT3686_CC2_Img2() throws Exception {
		DataCompletion_Balancing.verify_Img2_Button_ChangedImageZoom();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 230)
	public void PT3686_CC2_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 240)
	public void PT3686_CC2_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 250)
	public void PT3686_CC2_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 260)
	public void PT3686_CC2_GrayButton_Displayed() throws Exception {
		DataCompletion.verify_Gray_ButtonEnabled();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 270)
	public void PT3686_CC2_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 280)
	public void PT3686_CC2_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 290)
	public void PT3686_CC2_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 300)
	public void PT3686_CC2_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 310)
	public void PT3686_CC2_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 320)
	public void PT3686_CC2_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 330)
	public void PT3686_CC2_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 340)
	public void PT3686_CC2_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 350)
	public void PT3686_CC2_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 351)
	public void PT3686_CC2_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 352)
	public void PT3686_CC2_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 353)
	public void PT3686_CC2_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 354)
	public void PT3686_CC2_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 355)
	public void PT3686_CC2_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 360)
	public void PT3686_CC2_Done_ModalVisible() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 361)
	public void PT3686_CC2_Done_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 370)
	public void PT3686_CC2_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct("cca");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 380)
	public void PT3686_CC2_KeyTo_CC3() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion_Balancing.key_through_Batch("2");
		DataCompletion.click_Yes_ToConfirm();
		keyingJob = "cc3";
		DataCompletion.verify_CorrectJob(keyingJob);
	}

	//
	// Run same suite of tests on CC3
	//

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 410)
	public void PT3687_CC3_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 420)
	public void PT3687_CC3_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 425)
	public void PT3687_CC3_Img2() throws Exception {
		DataCompletion_Balancing.verify_Img2_Button_ChangedImageZoom();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 430)
	public void PT3687_CC3_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 440)
	public void PT3687_CC3_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 450)
	public void PT3687_CC3_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 460)
	public void PT3687_CC3_GrayButton_Displayed() throws Exception {
		DataCompletion.verify_Gray_ButtonEnabled();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 470)
	public void PT3687_CC3_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 480)
	public void PT3687_CC3_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 490)
	public void PT3687_CC3_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 500)
	public void PT3687_CC3_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 510)
	public void PT3687_CC3_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 520)
	public void PT3687_CC3_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 530)
	public void PT3687_CC3_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 540)
	public void PT3687_CC3_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 550)
	public void PT3687_CC3_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 551)
	public void PT3687_CC3_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 552)
	public void PT3687_CC3_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 553)
	public void PT3687_CC3_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 554)
	public void PT3687_CC3_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 555)
	public void PT3687_CC3_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 560)
	public void PT3687_CC3_Done_ModalVisible() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 561)
	public void PT3687_CC3_Done_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 570)
	public void PT3687_CC3_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct("cca");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 580)
	public void PT3729_Key_CC3_To_BAL() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion_Balancing.key_through_Batch("2");
		DataCompletion.click_Yes_ToConfirm();
		keyingJob = "sca";
		DataCompletion.verify_CorrectJob(keyingJob);
	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			batchStuff = null;
			jobInfo = null;
			
			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();
		} finally {
			Browser.closeBrowser();
		}
	}

}// End of Class
