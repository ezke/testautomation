package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataMaintenance_Batch.collapsible_SectionName;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.Help;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class KeyJob_DOC_Action_Tests extends baseTest {

	
	String keyingJob = "doc";
	String batchID = "";
	String timestamp = "";
	
	Integer index_Of_Field = null;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		batchID = BaseUI.random_NumberAsString(700217, 700219);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		
		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT3725_DOC_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT3725_DOC_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
	public void PT3725_DOC_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 40)
	public void PT3725_DOC_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 50)
	public void PT3725_DOC_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 60)
	public void PT3725_DOC_GrayButton_Displayed() throws Exception {
		DataCompletion.verify_Gray_ButtonEnabled();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 70)
	public void PT3725_DOC_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 80)
	public void PT3725_DOC_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
		String newBatch = DataCompletion.return_BatchID();
		BaseUI.baseStringCompare("Batch ID", batchID, newBatch);
	}


	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 90)
	public void PT3725_DOC_Lock_Modal_Yes_LocksBatch() throws Exception {
		DataCompletion.click_LockButton();
		timestamp = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy hh:mm a").toLowerCase();
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_DataCompletionPage_Loaded();
	//	DataCompletion.verify_BatchID_Changed(batchID);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 100)
	public void PT3725_DOC_verify_BatchLocked() throws Exception {
		Navigation.enter_Text_Into_CommandBox_AndNavigate("batch " + batchID);
		DataMaintenance_Batch.expand_Section(collapsible_SectionName.BatchStatus);
		DataMaintenance_Batch.verify_LockStatus_Locked("*", "Intentional by ", timestamp);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 110)
	public void PT3725_DOC_DataMaint_BatchResults_LockBatch_UnLocked_OnBatchPage() throws Exception {

		// Unlock
		DataMaintenance_Batch.unlockBatch();
		DataMaintenance_Batch.expand_Section(collapsible_SectionName.BatchStatus);
		// Validate unlocked
		DataMaintenance_Batch.verify_LockStatus_NOT_Locked();

	}
		
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 120)
	public void PT3725_DOC_Back_to_original_Batch() throws Exception {
		//Get us back to the original keying job.
		//Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		//DataCompletion.end_DataCompletion();
		//DataCompletion.click_Abbreviation(keyingJob);
		//String newBatch = DataCompletion.return_BatchID();
		//Make sure we're back to original batch.
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		String newBatch = DataCompletion.return_BatchID();
		BaseUI.baseStringCompare("Batch ID", batchID, newBatch);
	}
		
		
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 130)
	public void PT3725_DOC_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 140)
	public void PT3725_DOC_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 150)
	public void PT3725_DOC_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 151)
	public void PT3725_DOC_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 160)
	public void PT3725_DOC_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 170)
	public void PT3725_DOC_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 180)
	public void PT3725_DOC_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 181)
	public void PT3725_DOC_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 182)
	public void PT3725_DOC_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 183)
	public void PT3725_DOC_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 184)
	public void PT3725_DOC_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 190)
	public void PT3725_DOC_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}
	

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 200)
	public void PT3725_DOC_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct(keyingJob);
	}
	
	
		
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
