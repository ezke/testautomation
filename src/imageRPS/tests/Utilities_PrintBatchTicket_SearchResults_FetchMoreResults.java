package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_PrintBatchTicket_SearchResults_FetchMoreResults extends baseTest {

	String clientID = "10";
	String lockboxID = "Shoptek2";
	String docGroup = "4";
	// Might switch to enum
	WorkType workType = WorkType.NSF;
	
	
	Integer formerUpperAmount = null;
	Integer newUpperAmount = null;


	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(null, null, null, null);
		
		formerUpperAmount = Utilities_PrintTicket_SearchResults.return_UpperCountAmount();
		
		//Utilities_PrintTicket_SearchResults.sort_Column_Descending("Client Name");
		
	}


	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_ClickFetchMore_AmountIncreases() throws Exception {
		Utilities_PrintTicket_SearchResults.click_FetchMore();
		
		newUpperAmount = Utilities_PrintTicket_SearchResults.return_UpperCountAmount();
		BaseUI.verify_true_AndLog(newUpperAmount > formerUpperAmount, "Upper Amount increased.", "Upper Amount did NOT increase.");
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT3314_Utilities_PrintBatchTicketFilter_ClickFetchMore_ExportList_Equals_NewAmount() throws Exception {
		TableData exportList = Utilities_PrintTicket_SearchResults.return_ExportList_TableData();
	
		BaseUI.verify_true_AndLog(exportList.data.size() == newUpperAmount, "Size of table matched Count on page.", "Size of table did NOT match Count on page.");
	}
	
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
