package imageRPS.tests;

import imageRPS.data.ImageRPS_Utilities;
import imageRPS.data.baseTest;
import imageRPS.pages.Alerts;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

import java.time.Duration;

public class SessionManagement_Login_AllowSessionExpire extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ImageRPS_Utilities.updateWebConfigTimeout(Duration.ofMinutes(1));
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"})
    public void PT4414_TopMenuSecurity_Login_CheckDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
        Alerts.verify_SessionTimeOut_Alert();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try{
            ImageRPS_Utilities.setDefaultWebServerTimeout();
        }finally {
            Browser.closeBrowser();
        }
    }

}//End of Class



