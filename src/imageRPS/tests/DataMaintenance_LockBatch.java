package imageRPS.tests;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch.collapsible_SectionName;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_Filters.LockStatus;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DataMaintenance_LockBatch extends baseTest {

	// Integer count_WithoutFilters;
	Integer indexToPick = 5;
	String batchID = "";
	String timestamp = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT4437_DataMaint_BatchResults_LockBatch_Removed_FromUnlockedFiltered() throws Exception {

		DataMaintenance_Batch_Filters.select_LockStatus(LockStatus.UnlockedBatches);
		DataMaintenance_Batch_Filters.click_Submit();

		DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Lock Status", "U");
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT4437_DataMaint_BatchResults_LockBatch_Removed_FromUnlockedFiltered_Results() throws Exception {

		batchID = DataMaintenance_Batch_SearchResults.return_Cell_ByColumnHeader_AndIndex("Batch ID", indexToPick);
		DataMaintenance_Batch_SearchResults.select_Row_ByIndex(indexToPick);

		timestamp = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy hh:mm a").toLowerCase();
		DataMaintenance_Batch_SearchResults.lock_Batch();

		// Verify batch no longer present due to Filter
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_DoesNOTContain_Value("Batch ID", batchID);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT4437_DataMaint_BatchResults_LockBatch_Results_ShowingLocked() throws Exception {

		// Navigate back and switch filter to be batch ID
		DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
		DataMaintenance_Batch_Filters.click_ResetFilters();
		DataMaintenance_Batch_Filters.enter_BatchID_First_And_Second(batchID, null);

		// Submit and then pick batch from list
		DataMaintenance_Batch_Filters.click_Submit();

		// Verify it's locked.
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Locked", "Yes");

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
	public void PT4437_DataMaint_BatchResults_LockBatch_Locked_OnBatchPage() throws Exception {

		DataMaintenance_Batch_SearchResults.select_FirstRow();
		// Batch Info
		DataMaintenance_Batch_SearchResults.click_BatchInfo();
		// Validate Lock
		DataMaintenance_Batch.expand_Section(collapsible_SectionName.BatchStatus);
		DataMaintenance_Batch.verify_LockStatus_Locked("*", "Automation Test", timestamp);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 40)
	public void PT4437_DataMaint_BatchResults_LockBatch_UnLocked_OnBatchPage() throws Exception {

		// Unlock
		DataMaintenance_Batch.unlockBatch();
		DataMaintenance_Batch.expand_Section(collapsible_SectionName.BatchStatus);
		// Validate unlocked
		DataMaintenance_Batch.verify_LockStatus_NOT_Locked();

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 50)
	public void PT4437_DataMaint_BatchResults_LockBatch_UnLocked_OnResultsPage() throws Exception {

		// Go Back
		DataMaintenance_Batch.click_GoBack();

		// Verify Unlocked in grid
		DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Locked", "No");

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
