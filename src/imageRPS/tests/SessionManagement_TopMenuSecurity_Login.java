package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SessionManagement_TopMenuSecurity_Login extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
	public void PT4402_TopMenuSecurity_Login() throws Exception {
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    // * PT4402 - Excluding the validation for " The Entity ImageRPSTest should be selected by default." - Out of scope as per produ`ct Management
    // * PivotalTracker Bug ID 158183286

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
        public void PT4402_TopMenuSecurity_Login_ValidateDataCompletionDisappear() throws Exception {
        BaseUI.verifyElementDoesNotAppear(
                Locator.lookupElement("navigation_Link_ByText", "Data Completion", null));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
    public void PT4402_TopMenuSecurity_Login_ValidateDataMaintenanceDisappear() throws Exception {
        BaseUI.verifyElementDoesNotAppear(
                Locator.lookupElement("navigation_Link_ByText", "Data Maintenance", null));

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
    public void PT4402_TopMenuSecurity_Login_ValidateReportsDisappear() throws Exception {
        BaseUI.verifyElementDoesNotAppear(
                Locator.lookupElement("navigation_Link_ByText", "Reports/Output", null));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
    public void PT4402_TopMenuSecurity_Login_ValidateUtilitiesDisappear() throws Exception {
        BaseUI.verifyElementDoesNotAppear(
                Locator.lookupElement("navigation_Link_ByText", "Utilities", null));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
    public void PT4402_TopMenuSecurity_Login_ValidateAlertsDisappear() throws Exception {
        BaseUI.verifyElementDoesNotAppear(
                Locator.lookupElement("navigation_Link_ByText", "Alerts/Events", null));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" })
    public void PT4402_TopMenuSecurity_Login_ValidateHelpDisappear() throws Exception {
		BaseUI.verifyElementDoesNotAppear(
				Locator.lookupElement("navigation_Link_ByText", "Help", null));
	}

	@AfterMethod(alwaysRun = true)
	    public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		if (BaseUI.elementAppears(Locator.lookupElement("navigate_SignOut"))) {
			Navigation.signOut();
		}
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}//End of Class
