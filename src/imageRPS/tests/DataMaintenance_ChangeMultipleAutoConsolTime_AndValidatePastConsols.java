package imageRPS.tests;

import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_bal;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.DataMaintenance_Consolidations_Filters;
import imageRPS.pages.DataMaintenance_Consolidations_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.Navigation.TopNavigationTab;
import org.testng.asserts.SoftAssert;
import utils.*;

import java.time.Duration;
import java.util.HashMap;
import java.util.Objects;

public class DataMaintenance_ChangeMultipleAutoConsolTime_AndValidatePastConsols extends baseTest{


	private String clientIDColumn = "Client ID";
	private String consolAmountColumn = "Consol Amount";
	private String consolChecksColumn = "Consol Checks";
	private String consolBatchesColumn = "Consol Batches";
	private String openAmountColumn = "Open Amount";
	private String openChecksColumn = "Open Checks";
	private String openBatchesColumn = "Open Batches";

	private String expectedConsolAmountValue = "0";
	private String expectedConsolChecksValue ="0";
	private String expectedConsolBatchesValue = "0";
	private String expectedConsolAmountValueAfterProcess = "$18,500.00";
	private String expectedConsolChecksValueAfterProcess ="10";
	private String expectedConsolBatchesValueAfterProcess = "1";
	private String expectedOpenAmountValue = "$18,500.00";
	private String expectedOpenChecksValue = "10";
	private String expectedOpenBatchesValue = "1";
	private String expectedOpenAmountValueAfterProcess = "0";
	private String expectedOpenChecksValueAfterProcess = "0";
	private String expectedOpenBatchesValueAfterProcess = "0";

	private String Client_ID = "603";
	private String Client_ID1 = "604";

	TableData searchResults;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");
		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID);
		DataMaintenance_Consolidations_Filters.click_Submit();

	}

	// Navigate to Data Maintenance | Consolidations | and validate the "Open
	// Amount", "Open Checks" and "Open Batches" for Consol 603 and 604 =
	// $18,500.00, 10, and 1
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4623_DataMaintenance_Verify_ConsolAmount() throws Exception {

		String consolAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn).replace("$", "").split("\\.")[0];
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValue, consolAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4623_DataMaintenance_Verify_ConsolChecksAmt() throws Exception {

		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumn", expectedConsolChecksValue, consolChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4623_DataMaintenance_Verify_ConsolBatch() throws Exception {

		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchColumnValue", expectedConsolBatchesValue, consolBatchesValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4623_DataMaintenance_Verify_OpenAmount() throws Exception {

		String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValue", expectedOpenAmountValue, openAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4623_DataMaintenance_Verify_OpenChecks() throws Exception {

		String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openChecksColumn);
		BaseUI.baseStringCompare("OpenChecksColumnValue", expectedOpenChecksValue, openChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4623_DataMaintenance_Verify_OpenBatches() throws Exception {

		String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn);
		BaseUI.baseStringCompare("OpenBatchesColumnValue", expectedOpenBatchesValue, openBatchesValue);
	}


	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4623_DataMaintenance_Verify_ConsolAmount604() throws Exception {

		DataMaintenance.navigate_to_SubTab("Consolidations");
		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID1);
		DataMaintenance_Consolidations_Filters.click_Submit();
		String consolAmountValueDifferentClient = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn).replace("$", "").split("\\.")[0];
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValue, consolAmountValueDifferentClient);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4623_DataMaintenance_Verify_ConsolChecksAmt604() throws Exception {

		String consolChecksValueDifferentClient = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumnValue", expectedConsolChecksValue, consolChecksValueDifferentClient);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4623_DataMaintenance_Verify_ConsolBatch604() throws Exception {

		String consolBatchesValueDifferentClient = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchColumnValue", expectedConsolBatchesValue, consolBatchesValueDifferentClient);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4623_DataMaintenance_Verify_OpenAmount604() throws Exception {

		String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValue", expectedOpenAmountValue, openAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4623_DataMaintenance_Verify_OpenChecks604() throws Exception {

		String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openChecksColumn);
		BaseUI.baseStringCompare("OpenChecksColumnValue", expectedOpenChecksValue, openChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4623_DataMaintenance_Verify_OpenBatches604() throws Exception {

		String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn );
		BaseUI.baseStringCompare("OpenBatchesColumnValue", expectedOpenBatchesValue, openBatchesValue);
	}

	// Go to Batch information for 700351, and key bal (900)

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4623_DataMaintenance_Verify_ConsolOpenAmt() throws Exception {

		String batch = "700351";
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		String amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 40)
	public void PT4623_DataMaintenance_Verify_ConsolOpenChks() throws Exception {

		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_bal.wait_ForAlert_ToAppear();
		DataCompletion_bal.click_OK_ForAlertModal();

		String batch = "700352";
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		String amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 50)
	public void PT4623_DataMaintenance_Verify_ConsolOpenAmountkeyedJob() throws Exception {

		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_bal.wait_ForAlert_ToAppear();
		DataCompletion_bal.click_OK_ForAlertModal();
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");

		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID);
		DataMaintenance_Consolidations_Filters.click_Submit();
		DataMaintenance_Consolidations_SearchResults.refresh_And_Wait_ForColumnValueToBeUpdated(openAmountColumn, "$0.00");

		String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn).replace("$", "").split("\\.")[0];
		BaseUI.baseStringCompare("OpenAmountColumnValue", expectedOpenAmountValueAfterProcess, openAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 60)
	public void PT4623_DataMaintenance_Verify_OpenCheckkeyedJob() throws Exception {

		String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults( openChecksColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValue", expectedOpenChecksValueAfterProcess, openChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 61)
	public void PT4623_DataMaintenance_Verify_OpenBatchkeyedJob() throws Exception {

		String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn);
		BaseUI.baseStringCompare("OpenBatchColumnValue", expectedOpenBatchesValueAfterProcess, openBatchesValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 62)
	public void PT4623_DataMaintenance_Verify_ConsolAmountkeyedJob() throws Exception {

		String consolAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn);
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValueAfterProcess, consolAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 63)
	public void PT4623_DataMaintenance_Verify_ConsolCheckskeyedJob() throws Exception {

		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumnValue", expectedConsolChecksValueAfterProcess, consolChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 64)
	public void PT4623_DataMaintenance_Verify_ConsolBatchkeyedJob() throws Exception {

		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchesColumnValue", expectedConsolBatchesValueAfterProcess, consolBatchesValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 65)
	public void PT4623_DataMaintenance_Verify_OpenCheckkeyedJob604() throws Exception {

		DataMaintenance_Consolidations_SearchResults.click_GoBack_Button();
		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID1);
		DataMaintenance_Consolidations_Filters.click_Submit();
		String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn);
		String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openChecksColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValue", expectedOpenAmountValueAfterProcess, openAmountValue);
		BaseUI.baseStringCompare("OpenChecksColumnValue", expectedOpenChecksValueAfterProcess, openChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 66)
	public void PT4623_DataMaintenance_Verify_OpenBatchkeyedJob604() throws Exception {

		String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn);
		BaseUI.baseStringCompare("OpenBatchesColumnValue", expectedOpenBatchesValueAfterProcess, openBatchesValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 67)
	public void PT4623_DataMaintenance_Verify_ConsolAmountkeyedJob604() throws Exception {

		String consoleAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn);
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValueAfterProcess, consoleAmountValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 68)
	public void PT4623_DataMaintenance_Verify_ConsolCheckskeyedJob604() throws Exception {

		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolValues", expectedConsolChecksValueAfterProcess, consolChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 69)
	public void PT4623_DataMaintenance_Verify_ConsolBatchkeyedJob604() throws Exception {

		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolValues", expectedConsolBatchesValueAfterProcess, consolBatchesValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 70)
	public void PT4623_DataMaintenance_VerifyNewTime_AdjustByHours_And_Minutes_Disabled() throws Exception {

		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");
		DataMaintenance_Consolidations_Filters.click_ConsolType_Auto();
		DataMaintenance_Consolidations_Filters.click_Submit();
		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ClientID(Client_ID);
		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ClientID(Client_ID1);
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime();
		DataMaintenance_Consolidations_SearchResults.click_ChageDatesTimesToNewValue();
		BaseUI.verifyElementDisabled(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_AdjustByHrs"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_AdjustByMns"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 75)
	public void PT4623_DataMaintenance_Verify_ConsolCloseDateTime() throws Exception {

		// Select todays date in the date picker (step 22)
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
		BaseUI.enterText(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewDate"),
				currentDate);
		BaseUI.tabThroughField(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewDate"));

		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, "hh:mm a");
		String newTime = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime, "hh:mm a", "hh:mm a", 1);
		DataMaintenance_Consolidations_SearchResults.enter_ChangeAutoConsolidationTime_NewTime(newTime);
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime_OkBtn();
		String newDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(currentDate, "MM/dd/yyyy", "MM/dd/yyyy", 1);
		String consolCloseColumn = "Consol Close Date Time";
		String closeTime = "09:00 PM";

		DataMaintenance_Consolidations_SearchResults.refresh_And_Wait_ForColumnValueToBeUpdated(
				Client_ID, consolCloseColumn, newDate + " " + closeTime, Duration.ofSeconds(240));
		searchResults = DataMaintenance_Consolidations_SearchResults.return_SearchResults_ForConsolidation();
		BaseUI.baseStringCompare(consolCloseColumn, newDate + " " + closeTime, searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID ).get(consolCloseColumn));

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 76)
	public void PT4623_DataMaintenance_Verify_Consol_And_OpenColumns() throws Exception {

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(Objects.requireNonNull(searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID)).get(consolAmountColumn), "$0.00",
				"The value displayed on screen " + consolAmountColumn + " " + searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID).get(consolAmountColumn) + " does not match expected value : $0.00\n");
		softAssert.assertEquals(Objects.requireNonNull(searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID)).get(consolChecksColumn), expectedConsolChecksValue,
				"The value displayed on screen " + consolChecksColumn + " " + searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID).get(consolChecksColumn) + " does not match expected value : " + expectedConsolChecksValue + "\n");
		softAssert.assertEquals(Objects.requireNonNull(searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID)).get(consolBatchesColumn), expectedConsolBatchesValue,
				"The value displayed on screen " + consolBatchesColumn + " " + searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID).get(consolBatchesColumn) + " does not match expected value : " + expectedConsolBatchesValue + "\n");
		softAssert.assertEquals(Objects.requireNonNull(searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID)).get(openAmountColumn).replace("$", "").split("\\.")[0], expectedOpenAmountValueAfterProcess,
				"The value displayed on screen " + openAmountColumn + " " + searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID).get(openAmountColumn) + " does not match expected value : " + expectedOpenAmountValueAfterProcess + "\n");
		softAssert.assertEquals(Objects.requireNonNull(searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID)).get(openChecksColumn), expectedOpenChecksValueAfterProcess,
				"The value displayed on screen " + openChecksColumn + " " + searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID).get(openChecksColumn) + " does not match expected value : " + expectedOpenChecksValueAfterProcess + "\n");
		softAssert.assertEquals(Objects.requireNonNull(searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID)).get(openBatchesColumn), expectedOpenBatchesValueAfterProcess,
				"The value displayed on screen " + openBatchesColumn + " " + searchResults.return_Row_BasedOn_1MatchingField(clientIDColumn, Client_ID).get(openBatchesColumn) + " does not match expected value : " + expectedOpenBatchesValueAfterProcess + "\n");
		softAssert.assertAll();
	}


	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 86)
	public void PT4623_DataMaintenance_Verify_PastConsolidationExist() throws Exception {

		DataMaintenance_Batch_SearchResults.click_GoBack();
		DataMaintenance_Consolidations_Filters.click_Reset();
		DataMaintenance_Consolidations_Filters.click_ShowCurrent();
		DataMaintenance_Consolidations_Filters.click_ShowPast();
		DataMaintenance_Consolidations_Filters.click_Submit();

		String consolDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy");
		consolDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(consolDate, "MM/dd/yyyy", "MM/dd/yyyy", 0);

		searchResults = DataMaintenance_Consolidations_SearchResults.return_SearchResults();
		HashMap<String, String> searchResultsRowClient= searchResults.return_Row_BasedOn_1MatchingField("Client ID",
				searchResults.data.get(searchResults.first_IndexOf(clientIDColumn, "603")).get(clientIDColumn));

		BaseUI.baseStringCompare("Client ID", Client_ID, searchResultsRowClient != null ? searchResultsRowClient.get("Client ID") : null);
		BaseUI.baseStringCompare("Consol Date", consolDate, searchResultsRowClient != null ? searchResultsRowClient.get("Consol Date") : null);

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 90)
	public void PT4623_DataMaintenance_Verify_ConsolAmountkeyedJobPast() throws Exception {

		String consolAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolAmountColumn);
		BaseUI.baseStringCompare("ConsolAmountColumnValue", expectedConsolAmountValueAfterProcess, consolAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 91)
	public void PT4623_DataMaintenance_Verify_ConsolCheckskeyedJobPast() throws Exception {

		String consolChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolChecksColumn);
		BaseUI.baseStringCompare("ConsolChecksColumnValue", expectedConsolChecksValueAfterProcess, consolChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 92)
	public void PT4623_DataMaintenance_Verify_ConsolBatchkeyedJobPast() throws Exception {

		String consolBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(consolBatchesColumn);
		BaseUI.baseStringCompare("ConsolBatchesColumnValue", expectedConsolBatchesValueAfterProcess, consolBatchesValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 93)
	public void PT4623_DataMaintenance_Verify_ConsolOpenAmountPast() throws Exception {

		String openAmountValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openAmountColumn);
		BaseUI.baseStringCompare("OpenAmountColumnValue", expectedOpenAmountValueAfterProcess, openAmountValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 94)
	public void PT4623_DataMaintenance_Verify_OpenChecksPast() throws Exception {

		String openChecksValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openChecksColumn);
		BaseUI.baseStringCompare("OpenChecksColumnValue", expectedOpenChecksValueAfterProcess, openChecksValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 95)
	public void PT4623_DataMaintenance_Verify_OpenBatchJobPast() throws Exception {

		String openBatchesValue = DataMaintenance_Consolidations_SearchResults
				.return_ConsolSearchResults(openBatchesColumn);
		BaseUI.baseStringCompare("OpenBatchesColumnValue", expectedOpenBatchesValueAfterProcess, openBatchesValue);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
