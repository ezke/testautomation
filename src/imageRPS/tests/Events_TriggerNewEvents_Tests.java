package imageRPS.tests;

import org.jetbrains.annotations.NotNull;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.ImageRPS_Utilities;
import imageRPS.data.baseTest;
import imageRPS.pages.AlertsEvents;
import imageRPS.pages.Events;
import imageRPS.pages.Events_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.*;

public class Events_TriggerNewEvents_Tests extends baseTest {

	String eventsText_forData2 = "cmdtest";
	TableData expectedResults;
	@NotNull
	TableData searchResults;
	String time_EventsExecuted = "";
	String time_EventsExecuted_Minus1Minute = "";
	String time_EventsExecuted_Plus1Minute = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedResults = Events.return_ExpectedResults_ForEventTesterCMD();

		ImageRPS_Utilities.run_Events2();
		time_EventsExecuted = BaseUI.getDateAsString_InRelationToTodaysDate(0, Events_SearchResults.timeFormat);
		time_EventsExecuted_Minus1Minute = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(-1,
				Events_SearchResults.timeFormat);
		time_EventsExecuted_Plus1Minute = BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(1,
				Events_SearchResults.timeFormat);

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		Navigation.navigate_toTab(TopNavigationTab.AlertsEvents);
		AlertsEvents.navigate_SubTab("Events");
		Events.click_Reset();
		Events.enter_Text_Into_Data2(eventsText_forData2);
		Events.click_Submit();
	}

	// Added this test to deal with defect that we were seeing. List would not
	// populate fast enough.
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3038_EventsListPopulates() throws Exception {

		searchResults = Events_SearchResults.return_SearchResults();
		BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Found Events.", "Did NOT find Events.");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3038_Events_Event1_Matches_CMD_GeneratedEvent() throws Exception {

		// Added additional wait to deal with issue where list was not populating within
		// acceptable time period.
		if (searchResults == null || searchResults.data.size() == 0) {
			Events_SearchResults.wait_For_RangeToPopulate(60);
			searchResults = Events_SearchResults.return_SearchResults();
		}

		Integer eventToPick = 0;
		Events_SearchResults.validate_EventRow(eventToPick, expectedResults, searchResults,
				time_EventsExecuted_Minus1Minute, time_EventsExecuted_Plus1Minute);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT3038_Events_Event2_Matches_CMD_GeneratedEvent() throws Exception {
		Integer eventToPick = 1;
		Events_SearchResults.validate_EventRow(eventToPick, expectedResults, searchResults,
				time_EventsExecuted_Minus1Minute, time_EventsExecuted_Plus1Minute);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 11)
	public void PT3038_Events_Event3_Matches_CMD_GeneratedEvent() throws Exception {
		Integer eventToPick = 2;
		Events_SearchResults.validate_EventRow(eventToPick, expectedResults, searchResults,
				time_EventsExecuted_Minus1Minute, time_EventsExecuted_Plus1Minute);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		expectedResults = null;

		Browser.closeBrowser();
	}

}// End of Class
