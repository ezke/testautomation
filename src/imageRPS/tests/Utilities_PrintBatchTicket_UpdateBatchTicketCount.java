package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.Browser;
import utils.ResultWriter;

public class Utilities_PrintBatchTicket_UpdateBatchTicketCount extends baseTest {

	String clientID = "601";
	String docGroup = "97";


	

	Integer indexToCheck = 1;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(clientID, null, docGroup, null);
		//searchResults = Utilities_PrintTicket_SearchResults.return_SearchResults2();
	
	}

	@Test(priority=10, groups = { "all_Tests", "critical_Tests" })
	public void PT3349_TicketCount_Defaults_To_1()
			throws Throwable {
		Utilities_PrintTicket_SearchResults.verify_TicketCount(indexToCheck, 1);
	}
	
	@Test(priority=20, groups = { "all_Tests", "critical_Tests" })
	public void PT3349_Update_TicketCount_To_10()
			throws Throwable {
		
		Integer ticketCount = 10;
		Utilities_PrintTicket_SearchResults.check_Checkbox_ByIndex(indexToCheck);
		Utilities_PrintTicket_SearchResults.update_TicketCount(ticketCount);
		
		Utilities_PrintTicket_SearchResults.verify_TicketCount(indexToCheck, ticketCount);
	}
	
	@Test(priority=30, groups = { "all_Tests", "critical_Tests" })
	
	public void PT3349_Update_TicketCount_To_1()
			throws Throwable {
		Integer ticketCount = 1;
		Utilities_PrintTicket_SearchResults.check_Checkbox_ByIndex(indexToCheck);
		Utilities_PrintTicket_SearchResults.update_TicketCount(ticketCount);

		Utilities_PrintTicket_SearchResults.verify_TicketCount(indexToCheck, ticketCount);
	}
	
	

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
	

		Browser.closeBrowser();
	}
}// End of Class
