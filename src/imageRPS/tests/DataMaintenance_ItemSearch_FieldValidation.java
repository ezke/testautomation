package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;


public class DataMaintenance_ItemSearch_FieldValidation extends baseTest {

    TableData searchResults;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
        DataMaintenance.navigate_to_SubTab("Item Search");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_AccountNumber_Char_40() throws Exception {

//      Step5
        String textToVerify= "1234567890123456789012345678901234567890";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Account Number:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_AccountNumber_Length_DoesNot_Exceeds_40Char() throws Exception {

//      Step5
        String textToVerify= "123456789012345678901234567890123456789012";
        int maxLength = 40;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Account Number:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_DollarAmount() throws Exception {

//      Step6
        String textToVerify= "12345678";
        DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox("Dollar Amount:", textToVerify);
        BaseUI.tabThroughField(Locator.lookupElement("dataMaint_ItemSearch_TextBox_ByLabelText", "Dollar Amount:", null));
        String dollarAmountInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_ItemSearch_TextBox_ByLabelText", "Dollar Amount:", null)).replace("," ,"");
        BaseUI.baseStringCompare("DollarAmountValue", textToVerify + ".00", dollarAmountInputText);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_StubCheck() throws Exception {

//      Step7
        String textToVerify= "S";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Stub/Check:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_StubCheck_LengthDoesNotExceed_1Char() throws Exception {

//      Step7
        String textToVerify= "SS";
        int maxLength = 1;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Stub/Check:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_DocID() throws Exception {

//      Step8
        String textToVerify= "123";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Doc ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_DocID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step8
        String textToVerify= "1234";
        int maxLength = 3;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Doc ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_ClientID() throws Exception {

//      Step9
        String textToVerify= "1234";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Client ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_ClientID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step9
        String textToVerify= "12345";
        int maxLength = 4;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Doc ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_LockBoxID() throws Exception {

//      Step10
        String textToVerify= "1234567890";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Lockbox ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_LockBoxID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step10
        String textToVerify= "1234567890125";
        int maxLength = 10;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Lockbox ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_DocGroupID() throws Exception {

//      Step11
        String textToVerify= "123";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Doc Group:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_DocGroupID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step11
        String textToVerify= "1234";
        int maxLength = 3;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Doc Group:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_WorkType() throws Exception {

//      Step12
        String textToVerify= "12";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Work Type:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_WorkType_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step12
        String textToVerify= "123";
        int maxLength = 2;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Work Type:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_BatchID() throws Exception {

//      Step13
        String textToVerify= "123456";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Batch ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_BatchID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step13
        String textToVerify= "1234567";
        int maxLength = 6;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Batch ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_BatchNumber() throws Exception {

//      Step14
        String textToVerify= "123456";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Batch Number:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_BatchNumber_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step14
        String textToVerify= "1234567";
        int maxLength = 6;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Batch Number:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_ConsolNumber() throws Exception {

//      Step15
        String textToVerify= "12";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Consol Number:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_ConsolNumber_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step15
        String textToVerify= "123";
        int maxLength = 2;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Consol Number:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_BlockNumber() throws Exception {

//      Step16
        String textToVerify= "123456";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Block#:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_BlockNumber_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step16
        String textToVerify= "1234567";
        int maxLength = 6;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Block#:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceClientID() throws Exception {

//      Step17
        String textToVerify= "1234";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Source Client ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceClientID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step17
        String textToVerify= "1234567";
        int maxLength = 4;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Source Client ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceLockbox() throws Exception {

//      Step18
        String textToVerify= "1234567890";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Source Lockbox ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceLockbox_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step18
        String textToVerify= "123456789012";
        int maxLength = 10;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Source Lockbox ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceDocGroup() throws Exception {

//      Step19
        String textToVerify= "123";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Source Doc Group:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceDocGroup_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step19
        String textToVerify= "12345";
        int maxLength = 4;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Source Doc Group:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceP1StattionID() throws Exception {

//      Step20
        String textToVerify= "1234";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Source P1 Station ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceP1StationID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step20
        String textToVerify= "12345";
        int maxLength = 4;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Source P1 Station ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceP1SeqNumber() throws Exception {

//      Step21
        String textToVerify= "123456";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Source P1 Seq#:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceP1SeqNumber_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step21
        String textToVerify= "12345678";
        int maxLength = 6;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Source P1 Seq#:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceBatchID() throws Exception {

//      Step22
        String textToVerify= "123456";
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Value("Source Batch ID:", textToVerify);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4798_DataMaintenance_ItemSearch_SourceBatchID_LengthDoesNot_Exceed_Maxlength() throws Exception {

//      Step22
        String textToVerify= "12345678";
        int maxLength = 6;
        DataMaintenance_ItemSearch_Filters.verify_FieldName_Length_DoesNOTExceed_MaxLength("Source Batch ID:", textToVerify, maxLength);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 20)
    public void PT4798_DataMaintenance_ItemSearch_VerifyResults() throws Exception {

//      Step23
        DataMaintenance_ItemSearch_Filters.click_Submit();
        searchResults = DataMaintenance_ItemSearch_Results.return_SearchResults2();
        searchResults.verify_TableSize(0);
        BaseUI.verifyElementHasExpectedText("dataMaint_ItemSearchResults_NoData_Text","No data to display");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
