package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class ItemKeyingCount_DataCompletion_Tests extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3558_ItemKeyingCount_DataCompletion_slc() throws Exception {
		String name = "slc";

		Dashboard.click_ItemKeyingCount_BatchBar_ByName(name);
		DataCompletion.verify_CorrectJob(name);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3564_ItemKeyingCount_DataCompletion_cca() throws Exception {
		String name = "cca";

		Dashboard.click_ItemKeyingCount_BatchBar_ByName(name);
		DataCompletion.verify_CorrectJob(name);
		BaseUI.verifyElementHasFocus(Locator.lookupElement("dataComplete_CheckAmount_TextBox"));

	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3588_ItemKeyingCount_DataCompletion_afe() throws Exception {
		String name = "afe";

		Dashboard.click_ItemKeyingCount_BatchBar_ByName(name);
		DataCompletion.verify_CorrectJob(name);

		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CurrentThumbnail_Seq_Number"))),
				"Seq was a number.", "Seq was NOT a number.");

		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"))),
				"Batch was a number.", "Batch was NOT a number.");
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3569_ItemKeyingCount_DataCompletion_sl2() throws Exception {
		String name = "sl2";

		Dashboard.click_ItemKeyingCount_BatchBar_ByName(name);
		DataCompletion.verify_CorrectJob(name);
		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(BaseUI.getSelectedOptionFromDropdown("dataComplete_SLC_SeqDropdown")),
				"Seq was a number.", "Seq was NOT a number.");

		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"))),
				"Batch was a number.", "Batch was NOT a number.");

		BaseUI.verifyElementHasFocus(Locator.lookupElement("dataComplete_CheckKeyField"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3571_ItemKeyingCount_DataCompletion_act() throws Exception {
		String name = "act";
		
		Dashboard.click_ItemKeyingCount_BatchBar_ByName(name);
		DataCompletion.verify_CorrectJob(name);
		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Last_Seq"))),
				"Seq was a number.", "Seq was NOT a number.");

		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"))),
				"Batch was a number.", "Batch was NOT a number.");
		BaseUI.verifyElementHasFocus(Locator.lookupElement("dataComplete_Field_TextBox"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3578_ItemKeyingCount_DataCompletion_bal() throws Exception {
		String name = "bal";

		Dashboard.click_ItemKeyingCount_BatchBar_ByName(name);
		DataCompletion.verify_CorrectJob(name);
		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Visible_Tran_Number"))),
				"Tran was a number.", "Tran was NOT a number.");

		BaseUI.verify_true_AndLog(
				BaseUI.string_isNumeric(
						BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"))),
				"Batch was a number.", "Batch was NOT a number.");

		BaseUI.verifyElementHasFocus(Locator.lookupElement("dataComplete_Visible_BalanceAmount_Textbox"));

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (Navigation.isTabActive(TopNavigationTab.DataCompletion)) {
			DataCompletion.end_DataCompletion();
			Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		}

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
}
