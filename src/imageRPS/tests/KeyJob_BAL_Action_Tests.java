package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_bal;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.Help;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_BAL_Action_Tests extends baseTest{
	String keyingJob = "bal";
	String batchID = "";
	String timestamp = "";

	Integer maxValue;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	String clientID;
	String docID;
	String seq;

	Integer index_Of_Field = null;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchID = BaseUI.random_NumberAsString(700182, 700192);
		Browser.openBrowser(GlobalVariables.baseURL);
		batchStuff = Navigation.return_ConfigData();

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		DataCompletion.verify_CorrectJob(keyingJob);

		clientID = DataCompletion.return_ClientID();
		batchID = DataCompletion.return_BatchID();
		jobInfo = batchStuff.return_Row_BasedOn_2MatchingFields("client_id", clientID, "batch_ID", batchID);
		//maxValue = Integer.parseInt(jobInfo.get("field_len"));

		

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 1)
	public void PT5080_veify_BAL_Toggle_ToolbarIcon() throws Exception {
		DataCompletion_bal.veify_BAL_Toggle_ToolbarIcon();

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT5080_BAL_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion_bal.click_BAL_ZoomIn_And_ZoomOut_Works();
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT5080_BAL_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
	public void PT5080_BAL_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 40)
	public void PT5080_BAL_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 50)
	public void PT5080_BAL_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 60)
	public void PT5080_BAL_Gray_Button() throws Exception {
		DataCompletion.verify_Grey_Button();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 70)
	public void PT5080_BAL_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 80)
	public void PT5080_BAL_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 130)
	public void PT5080_BAL_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 140)
	public void PT5080_BAL_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 150)
	public void PT5080_BAL_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 151)
	public void PT5080_BAL_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 160)
	public void PT5080_BAL_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 170)
	public void PT5080_BAL_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 180)
	public void PT5080_BAL_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 181)
	public void PT5080_BAL_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 182)
	public void PT5080_BAL_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 183)
	public void PT5080_BAL_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 184)
	public void PT5080_BAL_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 190)
	public void PT5080_BAL_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 260)
	public void PT5080_BAL_Done_ModalVisible() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 261)
	public void PT5080_BAL_Done_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 300)
	public void PT5080_BAL_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct("bal");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}
