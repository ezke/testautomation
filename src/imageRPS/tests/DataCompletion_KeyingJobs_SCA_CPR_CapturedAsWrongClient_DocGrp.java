package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_SCA_CPR_CapturedAsWrongClient_DocGrp extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private String cprOperatorNotesText = "123Abc!?.,";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702226, 702228);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3672_Step6_To_8_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3672_Step9_To_10_CC2_KeyTo_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "sca";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3672_Step11_SCA_CPR_ModalVisible() throws Exception {
        DataCompletion.click_CPR_Button();
        DataCompletion.verify_CPR_Modal_Visible();

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3672_Step11_SCA_CPR_SelectReason_VerifyBatch() throws Exception {

        DataCompletion.modalDialog_Click_RepairDescription_CaptureAsWrongClient();
        DataCompletion.enterText_InTo_CPR_OperatorNotes(cprOperatorNotesText);
        DataCompletion.modalDialog_Click_SelectReason_Button();
        DataCompletion.modalDialog_RepairDescription_SendBatchTo_CPR();
        String actualBatchID = BaseUI.getTextFromField(Locator.lookupRequiredElement("dataComplete_BatchID_ByIndex", "1", null));
        BaseUI.baseStringPartialCompare("dataComplete_BatchId", batch, actualBatchID);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3672_Step11_SCA_CPR_Verify_BatchReasonMessage() throws Exception {

        String actualReasonMessage = BaseUI.getTextFromField(Locator.lookupRequiredElement("dataComplete_ReasonMsg_ByIndex", "1", null));
        BaseUI.baseStringPartialCompare("dataComplete_ReasonMsg",
                "Batch Captured as Wrong Client/Doc Grp;" + cprOperatorNotesText + "(" + GlobalVariables.return_UserName_Formatted()
                , actualReasonMessage);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3672_Step12_SCA_CPR_Verify_CheckIn_UpdateBatch() throws Exception {

        DataCompletion_CPR.verify_CheckIn_UpdateBatch();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 70)
    public void PT3672_Step12_SCA_CPR_Verify_CheckIn_CloseDialog() throws Exception {

        DataCompletion_CPR.verify_CheckIn_CloseDialog();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3672_Step12_SCA_CPR_Verify_CheckIn_AlertDialog() throws Exception {

        DataCompletion_CPR.clickOk_CheckIn_AlertDialog();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_AlertDialogBox_IsPresent"));

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 90)
    public void PT3672_Step13_To_14_KeyTo_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message("This batch is ready for wait and does not require keying.");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 100)
    public void PT3672_Step14_Verify_DataCompletionPage_Loads() throws Exception {

        DataCompletion.click_OK_on_Alert();
        DataCompletion.verify_DataCompletionPage_Loaded();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
