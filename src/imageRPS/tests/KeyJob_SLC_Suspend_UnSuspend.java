package imageRPS.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Scanline;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_SLC_Suspend_UnSuspend extends baseTest {

	String keyingJob = "slc";
	String batchID;
	String timestamp = "";

	Integer maxValue;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	String clientID;
	String docID;
	// secondSeq will be rejected and then unrejected.
	String secondSeq;
	String firstSeq;

	Integer index_Of_Field = null;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchID = BaseUI.random_NumberAsString(700261, 700265);

		Browser.openBrowser(GlobalVariables.baseURL);
		batchStuff = Navigation.return_ConfigData();

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		DataCompletion.verify_CorrectJob(keyingJob);

		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion.return_DocID();
		jobInfo = batchStuff.return_Row_BasedOn_2MatchingFields("client_id", clientID, "doc_id", docID);
		maxValue = Integer.parseInt(jobInfo.get("field_len"));

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}

		firstSeq = DataCompletion_Scanline.return_Current_SeqNumber();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT5076_SLC_KeyToSecondSeq() throws Exception {
		firstSeq = DataCompletion_Scanline.return_Current_SeqNumber();
		//DataCompletion_Scanline.set_Msrd_On_Or_Off(false);
		DataCompletion_Scanline.key_With_MaxValue_GenericValue(maxValue);
		DataCompletion_Scanline.verify_Seq_Changed(firstSeq);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT5076_SLC_SuspendItem_ModalDisplayed() throws Exception {
		// Update our seq number.
		secondSeq = DataCompletion_Scanline.return_Current_SeqNumber();

		DataCompletion.click_Susp();
		
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT5076_SLC_SuspendItem_SelectReasonAndSuspend_NextSeq_Displayed() throws Exception {
		DataCompletion.select_SuspendReason_ByIndex(1);
		DataCompletion.click_Susp_SelectReason();
		DataCompletion_Scanline.verify_Seq_Changed(secondSeq);
		DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT5076_SLC_FirstSeq_NotSuspended() throws Exception {
		DataCompletion_Scanline.select_SeqNumber_FromDropdown(firstSeq);
		DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT5076_SLC_SuspendedItem_Select_SuspendedSeq_SusLabel() throws Exception {
		DataCompletion_Scanline.select_SeqNumber_FromDropdown(secondSeq);
		DataCompletion_Scanline.verify_CurrentSeq_Suspended();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT5076_SLC_SuspendItem_UnSuspend_Item() throws Exception {
		DataCompletion.click_UnSusp();
		DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT5076_SLC_SuspendUnSuspend_Item_AuditTrail() throws Exception {
		String header = "Audit Trail";
		Integer rowIndex = null;
		HashMap<String, String> viewItemRow = new HashMap<String, String>();

		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", secondSeq);
		// DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		viewItemRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(rowIndex);
		BaseUI.baseStringPartialCompare(header,
				"UnSuspend,slc," + GlobalVariables.return_UserName_Formatted() + ",,Sus" + batchID + ",slc," + GlobalVariables.return_UserName_Formatted() + ",,",
				viewItemRow.get(header));

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchStuff = null;
		jobInfo = null;
		Browser.closeBrowser();
	}

}// End of Class
