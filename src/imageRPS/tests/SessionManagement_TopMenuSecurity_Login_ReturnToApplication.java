package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LogOutPage;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SessionManagement_TopMenuSecurity_Login_ReturnToApplication extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 10)
    public void PT4405_TopMenuSecurity_Login_CheckDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 20)
    public void PT4405_TopMenuSecurity_Login_SignOut() throws Exception {
            Navigation.signOut();
            BaseUI.wait_ForElement_ToContainText("signOutPage_Message", 100, "You are now signed out.");
            LogOutPage.verifyLogOutPageMsg();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 30)
    public void PT4405_TopMenuSecurity_Login_GoBackLoginPage() throws Exception {
        BaseUI.click(Locator.lookupElement("signOutPage_ReturnToApplication"));
        BaseUI.wait_ForElement_ToContainText("loginPageMainMessage", 100, "Login");
        LoginPage.verifyLoginPageMainMessage();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity" }, priority = 40)
    public void PT4405_TopMenuSecurity_Login_ValidateDataCompletionDisappear() throws Exception {
        BaseUI.verifyElementDoesNotAppear(
                Locator.lookupElement("navigation_Link_ByText", "Dashboard", null));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        //Browser.closeBrowser();
    }

}//End of Class

