package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_bal;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class KeyJob_BAL_Keying_Tests extends baseTest {

	String keyingJob = "bal";
	String batch = "";
	// String valueToEnter = "";
	String seqNumber = "";
	// String fieldName = "";
	// String genericValue = "1";
	// String docType = "";

	// String clientID;
	// String docID;
	//
	Integer index_Of_Field = null;
	// TableData batchStuff;
	// HashMap<String, String> jobInfo;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		// 700131-700152
		batch = BaseUI.random_NumberAsString(700112, 700120);

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

		seqNumber = DataCompletion_bal.return_firstSeqNumber();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT5073_Balance_StubsTooHigh() throws Exception {
		String amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_StbAmount"));
		amountToEnter = amountToEnter.replace(",", "");
		Double amountToEnter_Double = Double.parseDouble(amountToEnter);
		amountToEnter_Double = amountToEnter_Double - .01;
		amountToEnter = amountToEnter_Double.toString();
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.verify_StubsTooHigh("0.01");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT5073_Balance_FirstItem() throws Exception {
		DataCompletion_bal.balance_OneAmount();
		DataCompletion_bal.balance_OneAmount();
		DataCompletion_bal.verify_Balanced();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT5073_Balance_RemainingItems() throws Exception {
		DataCompletion_bal.key_Through_AllBalanceItems();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT5073_Balance_ConfirmModal_ClickYes() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_bal.wait_ForAlert_ToAppear();
		DataCompletion_bal.verify_Alert_DialogPresent();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT5073_Balance_Click_OK_OnAlert_TakesOutOfJob() throws Exception {
		DataCompletion_bal.click_OK_ForAlertModal();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT5073_Balance_VerifyAuditTrail() throws Exception {
		Navigation.navigate_to_Batch_ViaCommand(batch);
		DataMaintenance_Batch.click_ViewItems();
		index_Of_Field = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				seqNumber);
		
		DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToRight();
		HashMap<String, String> viewItemRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(index_Of_Field);
		BaseUI.baseStringPartialCompare("Audit Trail", keyingJob + "," + GlobalVariables.return_UserName_Formatted() + ",",
				viewItemRow.get("Audit Trail"));
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
