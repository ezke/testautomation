package imageRPS.tests;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities;
import imageRPS.pages.Utilities_ConcurrentUsers;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_ConcurrentUsers_Tests extends baseTest {

	WebDriver firstUser;
	WebDriver secondUser;
	TableData availableUsers;
	
	String userName2_ExpectedValue = "autote_1";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities.navigate_ConcurrentUsers();

		firstUser = Browser.driver;

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.SecondLoginName, GlobalVariables.SecondLoginPassword);
	

		secondUser = Browser.driver;

	
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT3388_Utilities_ConcurrentUsers_firstUser_DoesntSee_SecondUser_UntilRefresh() throws Exception {

		Browser.changeDriver(firstUser, Browser.currentBrowser);
		availableUsers = Utilities_ConcurrentUsers.return_userList();
		availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.return_UserName_Formatted());
		availableUsers.verify_Value_NOT_InColumn("Operator ID", userName2_ExpectedValue);
		
		
		Utilities_ConcurrentUsers.click_Refresh();
		availableUsers = Utilities_ConcurrentUsers.return_userList();
		availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.return_UserName_Formatted());
		availableUsers.verify_Value_InColumn("Operator ID", userName2_ExpectedValue);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 11)
	public void PT3388_Utilities_ConcurrentUsers_firstUser_And_SecondUser_InTable() throws Exception {
		Browser.changeDriver(secondUser, Browser.currentBrowser);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities.navigate_ConcurrentUsers();
		availableUsers = Utilities_ConcurrentUsers.return_userList();
		availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.return_UserName_Formatted());
		availableUsers.verify_Value_InColumn("Operator ID", userName2_ExpectedValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT3388_Utilities_ConcurrentUsers_secondUser_Disconnects_FirstUser_TableUpdates() throws Exception {
		Utilities_ConcurrentUsers.disconnect_User(GlobalVariables.return_UserName_Formatted(), availableUsers);
		availableUsers = Utilities_ConcurrentUsers.return_userList();
		availableUsers.verify_Value_NOT_InColumn("Operator ID", GlobalVariables.return_UserName_Formatted());
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
	public void PT3388_Utilities_ConcurrentUsers_secondUser_Disconnects_FirstUser_FirstSession_Disconnected()
			throws Exception {
		Browser.changeDriver(firstUser, Browser.currentBrowser);
		BaseUI.waitForElementToBeDisplayed("navigation_SessionEnded_Title", null, null,60);
		Navigation.verify_UserDisconnected();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		
		availableUsers = null;
		try {
			Browser.changeDriver(firstUser, Browser.currentBrowser);
			Browser.closeBrowser();
		} finally {
			Browser.changeDriver(secondUser, Browser.currentBrowser);
			Navigation.signOut();
			Browser.closeBrowser();
		}
	}

}// End of Class
