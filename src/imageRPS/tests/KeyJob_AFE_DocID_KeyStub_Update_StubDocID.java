package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_AFE_DocID_KeyStub_Update_StubDocID extends baseTest {

	String batchID;
	Integer rowIndex;
	TableData auditInfo;
	TableData expectedTable;
	String expectedUsername;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		expectedUsername = GlobalVariables.loginName.length() >= 8 ? GlobalVariables.loginName.substring(0, 8)
				: GlobalVariables.loginName;
		batchID = BaseUI.random_NumberAsString(700289, 700290);

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_KeyBatch();
		DataCompletion.verify_CorrectJob("afe");

	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4200_AFE_Tran1_VerifyDefaults() {
		Integer expectedDoc = 3;
		Integer expectedSeq = 3;
		String fieldWithFocus = "Courtesy Amount ($1)";
		Integer fixCount = 47;
		Integer errorCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(expectedDoc);
		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(errorCount,
				fieldWithFocus, fixCount);

	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4200_AFE_Tran1_Seq1_DefaultsAccurate() throws Exception {
		Integer docID = 24;
		Integer seq = 1;
		Integer errorCount = 15;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(seq);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(docID);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4200_AFE_Tran1_Unselect_AutoUpdate() throws Exception {
		DataCompletion_fbl_AllFieldsBalancing.deselect_AutoUpdate();
		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Disabled();
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4200_AFE_Tran1_Seq1_KeyAccountNumberA1() throws Exception {
		Integer fixCount = 47;
		Integer errorCount = 15;
		Integer seq = 1;

		String fieldToKey = "Account Number (A1)";
		String textToEnter = "132465789";
		String textBoxToHaveFocus = "BuildACT Edit Field (A2)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(textBoxToHaveFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(seq);
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 21)
	public void PT4200_AFE_Tran1_Seq1_KeyA2() throws Exception {
		String fieldToKey = "(A2)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(A3)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 22)
	public void PT4200_AFE_Tran1_Seq1_KeyA3() throws Exception {
		String fieldToKey = "(A3)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(A4)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 23)
	public void PT4200_AFE_Tran1_Seq1_KeyA4() throws Exception {
		String fieldToKey = "(A4)";
		String textToEnter = "12345";
		String fieldToHaveFocus = "(A5)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 12
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 24)
	public void PT4200_AFE_Tran1_Seq1_KeyA5() throws Exception {
		String fieldToKey = "(A5)";
		String textToEnter = "2";
		String fieldToHaveFocus = "(A6)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 13
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4200_AFE_Tran1_Seq1_KeyA6() throws Exception {
		String fieldToKey = "(A6)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(A7)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 14
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 26)
	public void PT4200_AFE_Tran1_Seq1_KeyA7() throws Exception {
		String fieldToKey = "(A7)";
		String textToEnter = "a";
		String fieldToHaveFocus = "(A8)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 15
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 27)
	public void PT4200_AFE_Tran1_Seq1_KeyA8() throws Exception {
		String fieldToKey = "(A8)";
		String textToEnter = "@";
		String fieldToHaveFocus = "(A9)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 16
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 28)
	public void PT4200_AFE_Tran1_Seq1_KeyA9() throws Exception {
		String fieldToKey = "(A9)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(A10)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 17
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 29)
	public void PT4200_AFE_Tran1_Seq1_KeyA10() throws Exception {
		String fieldToKey = "(A10)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(O1)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 18
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4200_AFE_Tran1_Seq1_KeyO1() throws Exception {
		String fieldToKey = "(O1)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(O2)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT4200_AFE_Tran1_Seq1_KeyO2() throws Exception {
		String fieldToKey = "(O2)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(O3)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 20
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 32)
	public void PT4200_AFE_Tran1_Seq1_KeyO3() throws Exception {
		String fieldToKey = "(O3)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(O4)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 21
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 33)
	public void PT4200_AFE_Tran1_Seq1_KeyO4() throws Exception {
		String fieldToKey = "(O4)";
		String textToEnter = "1";
		String fieldToHaveFocus = "(O5)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 22
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 34)
	public void PT4200_AFE_Tran1_Seq1_KeyO5() throws Exception {
		String fieldToKey = "(O5)";
		String textToEnter = "1";
		String fieldToHaveFocus = "Dol 5 ($5)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 23
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT4200_AFE_Tran1_Seq1_Dol5() throws Exception {
		String fieldToKey = "Dol 5 ($5)";
		String textToEnter = "100";
		Integer seq = 2;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(seq);
	}

	// Step 24
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4200_AFE_Tran1_Seq1_Click_DocIDButton() throws Exception {
		Integer defaultDocID = 24;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.click_DocId_Button();
		DataCompletion_fbl_AllFieldsBalancing.verify_DocID_Modal_GridRow_Selected(defaultDocID);
	}

	// Step 25
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT4200_AFE_Tran1_Seq1_DocID_Modal_Select25() throws Exception {
		Integer docID = 25;
		Integer fixCount = 47;
		Integer errorCount = 15;
		String fieldToHaveFocus = "(A1)";

		String tranBal = "$1.00";
		String stubBal = "$1.00";

		DataCompletion_fbl_AllFieldsBalancing.select_DocID_Modal_Document_ByDocID(docID);
		DataCompletion_fbl_AllFieldsBalancing.click_DocID_Modal_SelectDocument_Button();

		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(docID);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);

		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(tranBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(stubBal);

	}

	// Step 25
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 46)
	public void PT4200_AFE_Tran1_Seq1_DocID_Updated_TextboxesCorrect() throws Exception {
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A1)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A2)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A3)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A4)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A5)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A6)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A7)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A8)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A9)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(A10)", "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(O1)", "0.00");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(O2)", "0.00");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(O3)", "0.00");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(O4)", "0.00");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("(O5)", "0.00");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue("Dol 5", "1.00");
		DataCompletion_fbl_AllFieldsBalancing.verify_Error_NOT_Visible_ForTextField("Dol 5");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			auditInfo = null;
			expectedTable = null;

			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();

		} finally {
			Browser.closeBrowser();
		}
	}
}
