package imageRPS.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.DataMaintenance_TransInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class DataMaintenance_BatchSearchResults_ViewItems extends baseTest {

	Integer rowToSelect = 7;
	TableData viewItemData;
	HashMap<String, String> tableRow;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		
	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 10)
	public void PT4440_DataMaint_BatchResults_ViewItems_DataFound() throws Exception {
		
		DataMaintenance_Batch_Filters.click_Submit();
		DataMaintenance_Batch_SearchResults.select_Row_ByIndex(rowToSelect);
		DataMaintenance_Batch_SearchResults.click_ViewItems();
		//viewItemData = DataMaintenance_Batch_ViewItems.return_ViewItem_TableData();
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ViewItems_BatchItemGrid"));
		DataMaintenance_Batch_ViewItems.verify_Headers_Match();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 20)
	public void PT4440_DataMaint_BatchResults_ReviewItems_SelectChkItem() throws Exception {
		//Added 1 since XPath indexes start at 1 instead of 0.
		Integer itemToSelect = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("Doc Type", "2");
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(itemToSelect);
		tableRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(itemToSelect);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 30)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_VerifyBatchID() throws Exception {
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		DataMaintenance_ItemInfo.verify_BatchID(tableRow.get("Batch ID"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_AppliedAmount() throws Exception {
		DataMaintenance_ItemInfo.verify_AppliedAmount(tableRow.get("Applied Amount"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_RejectJob() throws Exception {
		DataMaintenance_ItemInfo.verify_RejectJob(tableRow.get("Reject Job"));
	}
	
	//Not sure what value would map to this.
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_SourceBatchID() throws Exception {
		DataMaintenance_ItemInfo.verify_SourceBatchID(tableRow.get("Source ID"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_P1SeqNumber() throws Exception {
		DataMaintenance_ItemInfo.verify_P1SeqNumber(tableRow.get("P1 Seq Number"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_BalancingDoc() throws Exception {
		DataMaintenance_ItemInfo.verify_BalancingDoc(tableRow.get("Balancing Doc"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_TranRejectJob() throws Exception {
		DataMaintenance_ItemInfo.verify_TranRejectJob(tableRow.get("Tran Reject Job"));
	}
	
	//Don't know what maps to this.
//	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 40)
//	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_SourceP1SeqNumber() throws Exception {
//		DataMaintenance_ItemInfo.verify_SourceP1SeqNumber(tableRow.get("Tran Reject Job"));
//	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_DocID() throws Exception {
		DataMaintenance_ItemInfo.verify_DocID(tableRow.get("Doc ID"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_TranNumber() throws Exception {
		DataMaintenance_ItemInfo.verify_TranNumber(tableRow.get("Tran Number"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_DocTypeName() throws Exception {
		DataMaintenance_ItemInfo.verify_DocTypeName(tableRow.get("Doc Type Name"));
	}
	
	//Item Job and Batch Job have same value.  Not sure which one to call here.
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_CurrentJob() throws Exception {
		DataMaintenance_ItemInfo.verify_CurrentJob(tableRow.get("Item Job"));
	}
	

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_TranSeqNumber() throws Exception {
		DataMaintenance_ItemInfo.verify_TranSeqNumber(tableRow.get("Tran Seq Number"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4440_DataMaint_BatchResults_ReviewItems_ItemInfo_CheckImageAppears() throws Exception {
		//DataMaintenance_ItemInfo.verify_AppliedAmount(tableRow.get("Applied Amount"));
		DataMaintenance_ItemInfo.verify_CheckImage_Displayed(tableRow.get("Batch ID"));
	}
	
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_BatchID() throws Exception {
		DataMaintenance_ItemInfo.click_GoBack();
		DataMaintenance_Batch_ViewItems.click_TransInfo_Button();
		DataMaintenance_TransInfo.verify_BatchID(tableRow.get("Batch ID"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_TranNumber() throws Exception {
		DataMaintenance_TransInfo.verify_TranNumber(tableRow.get("Tran Number"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_Balanced() throws Exception {
		DataMaintenance_TransInfo.verify_Balanced(tableRow.get("Balancing Doc"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_CheckAndStubTotal() throws Exception {
		DataMaintenance_TransInfo.verify_Check_And_Stub_Totals(tableRow.get("Applied Amount"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_SourceBatchID() throws Exception {
		DataMaintenance_TransInfo.verify_SourceBatchID(tableRow.get("Source ID"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_Job() throws Exception {
		DataMaintenance_TransInfo.verify_Job(tableRow.get("Tran Job"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_KeyingJob() throws Exception {
		DataMaintenance_TransInfo.verify_KeyingJob(tableRow.get("Batch Job"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4440_DataMaint_BatchResults_ReviewItems_TransInfo_RejectJob() throws Exception {
		DataMaintenance_TransInfo.verify_RejectJob(tableRow.get("Reject Job"));
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
//
//		if (BaseUI.elementExists("dataMaint_BatchFilter_Link", null, null)) {
//			DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
//			DataMaintenance_Batch_Filters.click_ResetFilters();
//		}

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		tableRow = null;
		Browser.closeBrowser();
	}

}// End of Class
