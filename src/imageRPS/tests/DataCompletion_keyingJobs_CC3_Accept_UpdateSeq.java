package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

public class DataCompletion_keyingJobs_CC3_Accept_UpdateSeq extends baseTest {

    private String keyingJob = "cca";
    private String batch = "702763";
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3763_Step7_To_8_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3763_Step9_To_10_CC2_KeyTo_CC3_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3763_Step11_CC3_VeifySeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        String keyingField = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"));
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
        BaseUI.verify_true_AndLog(keyingField.length() == 0, "Keying Field was empty", "Keying Field was NOT empty");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3763_Step12_CC3_VeifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3763_Step13_CC3_Accept_VerifySequence() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
        DataCompletion_Balancing.verify_NOT_Rejected(1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 60)
    public void PT3763_Step14_CC3_Verify_DifferentValue() throws Exception {

        DataCompletion_Balancing.enterText_Into_InputBox_ByIndex("2", 1);
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
        DataCompletion_Balancing.verify_NOT_Rejected(1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 70)
    public void PT3763_Step15_To_20_CC3_Accept_Verify_ViewItems_ItemInfo_FieldValue() throws Exception {

        DataCompletion.end_DataCompletion();

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        int indexOfViewItemsSeq2 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        DataMaintenance_Batch_ViewItems.select_Row_ByIndex(indexOfViewItemsSeq2);
        DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();

        TableData viewItemsTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
        BaseUI.verify_true_AndLog(viewItemsTable.data.size() > 0, "Found rows to test.", "Did not find rows to test.");
        HashMap<String, String> sequense$5Row = viewItemsTable.return_Row_BasedOn_1MatchingField("Type Sequence", "$ 5");
        BaseUI.baseStringCompare("Field Value", "0.02 ---> Applied", sequense$5Row.get("Field Value"));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
