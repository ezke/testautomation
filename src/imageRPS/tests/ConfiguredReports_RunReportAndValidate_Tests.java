package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.ReportsOutput;
import imageRPS.pages.ReportsOutput_Reformatter;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.ReportsOutput_ConfiguredReports;
import utils.Browser;
import utils.ResultWriter;

public class ConfiguredReports_RunReportAndValidate_Tests extends baseTest {

	String batch = "";
	Integer id_ToSelect = 12;

	Integer index_Of_Field = null;
	String[] reportLines;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		Navigation.navigate_toTab(TopNavigationTab.ReportsOutput);
		Navigation.click_OK_OnError_IfExists();
		ReportsOutput_Reformatter.click_OK_OnError_IfExists();
		ReportsOutput.navigate_SubTab("Configured Reports");
		ReportsOutput_ConfiguredReports.sort_Column_Ascending("ID");
		ReportsOutput_ConfiguredReports.select_Row_By_IDColumnText(id_ToSelect);
		ReportsOutput_ConfiguredReports.click_ViewReport();
		reportLines = ReportsOutput_ConfiguredReports.return_TextReport_Lines();
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3281_Verify_Report_HasExpected_Header() throws Exception {
		String expectedReportText = "Deposit Reports - Replacement DEPRPT1";
		ReportsOutput_ConfiguredReports.verify_Report_Title_MatchesExpected(reportLines, expectedReportText);
	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
