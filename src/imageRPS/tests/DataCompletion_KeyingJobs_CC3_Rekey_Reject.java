package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_CC3_Rekey_Reject extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private String rejectReason;
    private int indexOfViewItemsSeq2;
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = "702767";
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3770_Step6_To_7_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3770_Step8_To_9_CC2_KeyTo_CC3_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3770_Step10_CC3_VerifySeq() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3770_Step11_CC3_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3770_Step12_CC3_Rekey_VerifySequence() throws Exception {

        DataCompletion.click_ConfirmModal_Rekey_Button();
        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 60)
    public void PT3770_Step13_To_14_CC3_Rekey_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 70)
    public void PT3770_Step15_CC3_Rekey_Reject_VerifyRejected() throws Exception {

        rejectReason = "CC3 Could not identify amount";
        DataCompletion.click_ConfirmModal_Reject_Button();
        DataCompletion.select_RejectReason(rejectReason);
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Balancing.verify_Rejected(keyingJob, 1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 80)
    public void PT3770_Step16_To_18_CC3_Reject_VerifyViewItems() throws Exception {

        HashMap<String, String> viewItemRowSeq2 = new HashMap<String, String>();
        SoftAssert softAssert = new SoftAssert();

        DataCompletion.end_DataCompletion();

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        indexOfViewItemsSeq2 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");
        viewItemRowSeq2 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq2);
        softAssert.assertEquals(viewItemRowSeq2.get("Reject Job"), keyingJob,
                "The value " + viewItemRowSeq2.get("Reject Job") + " on the screen is NOT matching expected value" + keyingJob);
        softAssert.assertEquals(viewItemRowSeq2.get("Tran Reject Job"), keyingJob,
                "The value " + viewItemRowSeq2.get("Reject Job") + " on the screen is NOT matching expected value " + keyingJob);
        softAssert.assertEquals(viewItemRowSeq2.get("Reject Reason"), rejectReason + " seq# 2",
                "The value " + viewItemRowSeq2.get("Reject Job") + " on the screen is NOT matching expected value " + rejectReason + " seq# 2");
        softAssert.assertTrue(viewItemRowSeq2.get("Audit Trail").contains("Reject,cc3," + GlobalVariables.return_UserName_Formatted()),
                "The value " + viewItemRowSeq2.get("Reject Job") + " on the screen does NOT contain expected value " + "Reject,cc3," + GlobalVariables.return_UserName_Formatted());

        softAssert.assertAll();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 90)
    public void PT3770_Step19_CC3_Reject_VerifyItemInfo() throws Exception {

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        DataMaintenance_Batch_ViewItems.select_Row_ByIndex(indexOfViewItemsSeq2);
        DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
        TableData viewItemsTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
        BaseUI.verify_true_AndLog(viewItemsTable.data.size() > 0, "Found rows to test.", "Did not find rows to test.");
        HashMap<String, String> sequense$5Row = viewItemsTable.return_Row_BasedOn_1MatchingField("Type Sequence", "$ 5");
        BaseUI.baseStringCompare("Field Value", "0.03 ---> Applied", sequense$5Row.get("Field Value"));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
