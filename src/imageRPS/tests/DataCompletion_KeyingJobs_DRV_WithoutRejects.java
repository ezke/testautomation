package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_DRV_WithoutRejects extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702815 ,702817);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_KeyBatch();
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3794_Step8_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3794_Step9_To_10_KeyTo_CC2_Verify_Alert_Dup() throws Exception {

        String expectedMessage = "This batch is ready for dup and does not require keying.";
        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "drv";
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3794_Step12_DRV_Verify_Duplicate_AlertPopup() throws Exception {

        String expectedMessage = "This batch has a high percentage of duplicates detected.\nThe batch Counts are listed below." +
                "\nYou may lock the batch, or use the \"Reject Batch\" button to reject the batch.";
        DataCompletion.click_OK_on_Alert();
        DataMaintenance_Batch.refresh_And_Wait_ForJobList_ColumnValueToBeUpdated("Description",
                "Duplicate Review (drv)",1,120);
        DataMaintenance_Batch_SearchResults.click_KeyBatch();
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3794_Step13_To_26_DRV_Click_Through_All_DRV_VerifyAlertMessage() throws Exception {

        String expectedMessage = "This batch is ready for pp2 and does not require keying.";
        DataCompletion.click_OK_on_Alert();
        DataCompletion_DRV.click_through_DRV(false);
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3794_Step17_To_18_DRV_Compare_BatchSearchResults_BatchID() throws Exception {

        DataCompletion.click_OK_on_Alert();
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_ExpectedValue("Batch ID", batch);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 60)
    public void PT3794_Step18_DRV_Compare_BatchSearchResults_RejectJob() throws Exception {

        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_DoesNOTContain_Value("Reject Job", keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 70)
    public void PT3794_Step18_DRV_Compare_BatchSearchResults_TranRejectJob() throws Exception {

        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_DoesNOTContain_Value("Tran Reject Job", keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 80)
    public void PT3794_Step17_To_18_DRV_Compare_BatchSearchResults_BatchJob() throws Exception {

        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_ExpectedValue("Batch Job", "p2");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}
