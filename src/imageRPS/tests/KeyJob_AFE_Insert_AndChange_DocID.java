package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class KeyJob_AFE_Insert_AndChange_DocID extends baseTest {

	String batchID;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		batchID = BaseUI.random_NumberAsString(700301, 700305);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);

	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4158_AFE_Tran1_VerifyDefaults() {
		Integer expectedTran = 1;
		Integer expectedSeq = 2;
		Integer expectedFixCount = 7;
		Integer expectedErrorCount = 1;
		String textBoxThatShouldHaveFocus = "Courtesy Amount ($1)";
		Integer expectedDefDoc = 25;
		Integer expectedWorkItems = 4;

		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_DefDoc_Button_HasCorrectNumber(expectedDefDoc);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_workItem_Count(expectedWorkItems);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedErrorCount, textBoxThatShouldHaveFocus, expectedFixCount);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4158_Tran1_AFE_SelectSeq1_ClickInsert_Inserts_NewItem() throws Exception {
		Integer expectedWorkItems = 5;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.click_InsertButton();
		DataCompletion_fbl_AllFieldsBalancing.verify_workItem_Count(expectedWorkItems);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4158_Tran1_AFE_SelectSeq10_VerifyNewStub_Defaults() throws Exception {
		Integer expectedSeq = 10;
		Integer docID = 25;
		Integer expectedFixCount = 23;
		Integer expectedErrorCount = 16;
		String statusError = "Transaction 1 has fields needing attention.\n" + 
				"Field is too short (current length = 0). Minimum length = 1.";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(5);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(docID);
		DataCompletion_fbl_AllFieldsBalancing.verify_Current_Thumbnail_BackgroundPink();
		DataCompletion_fbl_AllFieldsBalancing.verify_INS_Present_ForCurrentlySelection();
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(expectedErrorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_StatusError(statusError);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4158_Tran1_AFE_ClickDocID_Select22() throws Exception {
		Integer newDocID = 22;
		Integer fixCount = 9;
		Integer errorCount = 2;
		String statusError = "Transaction 1 has fields needing attention.\n" + 
				"Field 'Account Number' (A1) requires operator action in afe.";
		
		DataCompletion_fbl_AllFieldsBalancing.click_DocId_Button();
		DataCompletion_fbl_AllFieldsBalancing.select_DocID_Modal_Document_ByDocID(newDocID);
		DataCompletion_fbl_AllFieldsBalancing.click_DocID_Modal_SelectDocument_Button();
		DataCompletion_fbl_AllFieldsBalancing.verify_Current_Thumbnail_BackgroundYellow();
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(newDocID);
		DataCompletion_fbl_AllFieldsBalancing.verify_INS_Present_ForCurrentlySelection();
		DataCompletion_fbl_AllFieldsBalancing.verify_NB_Present_ForCurrentlySelection();
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_AllTextBoxLabels_DontContainValue("$");
		DataCompletion_fbl_AllFieldsBalancing.verify_StatusError(statusError);
	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();

		} finally {
			Browser.closeBrowser();
		}
	}
}
