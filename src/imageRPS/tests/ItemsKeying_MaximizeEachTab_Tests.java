package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.Dashboard.itemKeying_Link;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class ItemsKeying_MaximizeEachTab_Tests extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3615_JobsTab_Maximized() throws Exception {
		Dashboard.verify_ItemsKeying_TabMaximized(itemKeying_Link.Jobs);
		Dashboard.verify_ItemsKeying_Tab_UnMaximized();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3615_ClientsTab_Maximized() throws Exception {
		Dashboard.verify_ItemsKeying_TabMaximized(itemKeying_Link.Clients);
		Dashboard.verify_ItemsKeying_Tab_UnMaximized();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3615_DatesTab_Maximized() throws Exception {
		Dashboard.verify_ItemsKeying_TabMaximized(itemKeying_Link.Dates);
		Dashboard.verify_ItemsKeying_Tab_UnMaximized();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3615_CutoffsTab_Maximized() throws Exception {
		Dashboard.verify_ItemsKeying_TabMaximized(itemKeying_Link.Cutoffs);
		Dashboard.verify_ItemsKeying_Tab_UnMaximized();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3615_LocationsTab_Maximized() throws Exception {
		Dashboard.verify_ItemsKeying_TabMaximized(itemKeying_Link.Locations);
		Dashboard.verify_ItemsKeying_Tab_UnMaximized();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void STRY585_Refresh() throws Exception {
		Dashboard.click_ItemKeying_Link(itemKeying_Link.Refresh);
		BaseUI.verifyElementHasExpectedText("dshbrd_ItemKeying_LastRefreshedLabel", "(a few seconds since last update)");
		Thread.sleep(70000);
		BaseUI.verifyElementHasExpectedText("dshbrd_ItemKeying_LastRefreshedLabel", "(a minute since last update)");
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Dashboard.un_maximize_ItemKeying();
		Dashboard.click_ItemKeying_Link(itemKeying_Link.Jobs);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
