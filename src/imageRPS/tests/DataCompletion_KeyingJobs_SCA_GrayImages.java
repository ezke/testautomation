package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import imageRpsThickClient.services.ImageViewer;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.FileOperations;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_SCA_GrayImages extends baseTest {

    private String remote2_p1images_Folder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\70223108";
    private String remote2_p1images_Save_SourceFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\SAVE";
    private String remote2_p1images_DestinationFolder = "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images";
    private String fileNameToMove = "70223108.P1";
    private String keyingJob = "cca";
    private String batch = "702231";
    ImageViewer imageViewer;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        FileOperations.delete_SpecificFile("\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\70223108.P1");
        FileOperations.copyFile(remote2_p1images_Save_SourceFolder + "\\" + fileNameToMove,
                    remote2_p1images_DestinationFolder + "\\" + fileNameToMove);

        FileOperations.cleanup_PriorFiles(remote2_p1images_Folder);

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3662_Step9_To_10_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3662_Step11_To_12_CC2_KeyTo_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "sca";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3662_Step13_SCA_Gray_Button() throws Exception {

        DataCompletion.verify_Gray_Button_ChangesImage();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3662_Step14_SCA_BW_Button() throws Exception {

        DataCompletion.verify_BW_Button_ChangesImage();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3662_SCA_Step14_To_16_Sequence_Three_GrayButton() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("1");
        DataCompletion.verify_Gray_Button_ChangesImage();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3662_SCA_Step17_To_20_Sequence_Nine_GrayButton() throws Exception {

        DataCompletion_Balancing.key_through_Batch_BySequence("1", "9");
        DataCompletion.verify_Gray_Button_ChangesImage();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3662_Step21_To_22_CC2_KeyThrough_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message("This batch is ready for wait and does not require keying.");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3662_Step23_Verify_DataCompletionPage_Loads() throws Exception {

        DataCompletion.click_OK_on_Alert();
        DataCompletion.verify_DataCompletionPage_Loaded();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3662_Step24_To_26_Verify_ImageViewer_GrayImage() throws Exception {

        imageViewer = new ImageViewer();
        imageViewer.clickImage();
        imageViewer.select_ExtractAllImages();
        imageViewer.select_Folder();
        imageViewer.close();
        String[] filename = {"\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\70223108\\70223108-00346888.tif",
                             "\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\70223108\\70223108-00363189.tif"};
        FileOperations.verify_FileMetadata_BitSize(filename, 8);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();

    }


}
