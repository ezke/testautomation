package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_CCA_CC2_CC3_RejectITem extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702261,702262);

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3750_Step6_CCA_Sequence_Two() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("123");
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3750_Step8_To_9_CCA_Sequence_Two_RejectItem() throws Exception {

        DataCompletion_Balancing.select_InputBox_ByIndex(1);
        DataCompletion.reject_Item();
        DataCompletion_Balancing.verify_Rejected(keyingJob, 1);
        String keyingFieldValue = DataCompletion_Balancing.return_KeyingFieldText_ByIndex(1);
        BaseUI.baseStringCompare("KeyingTextFeild", "1.23", keyingFieldValue);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3751_Step5_To_7_CCA_Key_TO_CC2() throws Exception {

        DataCompletion.end_DataCompletion();
        batch = BaseUI.random_NumberAsString(702263,702264);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3751_Step8_CC2_Sequence_Two() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("123");
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3751_Step9_To_10_CCA_Sequence_Two_RejectItem() throws Exception {

        DataCompletion_Balancing.select_InputBox_ByIndex(1);
        DataCompletion.reject_Item();
        DataCompletion_Balancing.verify_Rejected(keyingJob, 1);
        String keyingFieldValue = DataCompletion_Balancing.return_KeyingFieldText_ByIndex(1);
        BaseUI.baseStringCompare("KeyingTextFeild", "1.23", keyingFieldValue);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3752_Step5_To_7_CCA_Key_TO_CC2() throws Exception {

        DataCompletion.end_DataCompletion();
        batch = BaseUI.random_NumberAsString(702265,702266);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3752_Step8_CC2_Key_TO_CC3() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3752_Step9_CC2_Sequence_Two() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("123");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 90)
    public void PT3752_Step10_To_11_CCA_Sequence_Two_RejectItem() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("123");
        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.select_InputBox_ByIndex(1);
        DataCompletion.reject_Item();
        DataCompletion_Balancing.verify_Rejected(keyingJob, 1);
        String keyingFieldValue = DataCompletion_Balancing.return_KeyingFieldText_ByIndex(1);
        BaseUI.baseStringCompare("KeyingTextFeild", "1.23", keyingFieldValue);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}
