package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_Account;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_INS_Keying_Tests extends baseTest {

	
	String keyingJob = "ins";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String fieldName = "";
	String genericValue = "1";
	String docType = "";
	
	String clientID;
	String docID;

	Integer index_Of_Field = null;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		//700177-700180
		batch = BaseUI.random_NumberAsString(700177, 700180);
		
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion_Account.return_last_doc();
		seqNumber = DataCompletion_Account.return_last_seq();
		
		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 1)
	public void PT3714_Key_Job_MaxValues() throws Exception {
		
		DataCompletion_Account.key_allFields_UntilDone(clientID);
		fieldName = DataCompletion_Account.return_first_fieldName();
		genericValue = DataCompletion_Account.return_first_textField();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 2)
	public void PT3714_Key_Job_Confirm_JobChanges() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_BatchID_DidNOTChange(batch);
		DataCompletion.verify_JobChanged(keyingJob);
	}
	
	

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 3)
	public void PT3714_INS_Navigate_ItemInfo_AndValidate_FieldName() throws Exception {
		String header = "Field Name";
		Integer rowIndex = null;
		//fieldName = fieldName.replace("Account", "Acct");
		docType = DataCompletion_Account.stripOut_DocumentType_FromFieldName(fieldName);

		
		Navigation.navigate_to_Batch_ViaCommand(batch);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", seqNumber);
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		
		index_Of_Field = DataMaintenance_ItemInfo.return_FirstRowIndex_OfColumn_WithValue(header, fieldName);
		DataMaintenance_ItemInfo.verify_Field_By_Header_AndIndex(header, index_Of_Field, fieldName);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 4)
	public void PT3714_INS_ItemInfo_Validate_AuditInfo() throws Exception {
		String header = "Audit Info";
		String expectedAuditValue = GlobalVariables.return_UserName_Formatted() + "," + keyingJob + ",";
		DataMaintenance_ItemInfo.verify_Field_By_Header_AndIndex(header, index_Of_Field, expectedAuditValue);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 4)
	public void PT3714_INS_ItemInfo_Validate_FieldValue() throws Exception {
		String header = "Field Value";
		DataMaintenance_ItemInfo.verify_Field_By_Header_AndIndex(header, index_Of_Field, genericValue);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 4)
	public void PT3714_INS_ItemInfo_Validate_CurrentJob() throws Exception {
		DataMaintenance_ItemInfo.verify_CurrentJob_NOT_OldJob(keyingJob);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 5)
	public void PT3714_INS_Navigate_BackTo_DataCompletionACT_UnlockBack() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchStuff = null;
		jobInfo = null;
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
