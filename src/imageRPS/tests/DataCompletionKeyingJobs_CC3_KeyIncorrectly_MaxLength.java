package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletionKeyingJobs_CC3_KeyIncorrectly_MaxLength extends baseTest {

    private String keyingJob = "cca";
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($12,345,678.90,$98,765,432.10,$9,999,999.99).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        String batch = "702766";
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3766_Step6_To_7_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1234567890");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3766_Step8_To_9_CC2_KeyTo_CC3_Verify_Job() throws Exception {

        DataCompletion_Balancing.key_through_Batch("9876543210");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3766_Step10_CC3_VerifySeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("999999999");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
        DataCompletion_Balancing.verify_KeyingField_Empty();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3766_Step11_CC3_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("999999999");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3766_Step12_CC3_Verify_DifferentValue_Accept() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
        DataCompletion_Balancing.verify_NOT_Rejected(1);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
