package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_CPR;
import imageRPS.pages.DataCompletion_DOC;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_CPR_BatchCaptured_Split_Transactions extends baseTest {

	String docType = "";

	String tranList = "Transaction List";

	String docGrp = "1";
	String jobId = "CPR";
	String date = "02/10/2015";
	String newClientid1 = "405";
	String newDocGrp1 = "9";
	String newClientId = "405 - ClientSRTest405";
	String newDocGrp = "9 - Singles - Wait - Skip P2 - X9";
	String dialogBoxclientId = "20 - Bevent Insurance";
	String dialogBoxDocGrpId = "1 - Singles - Skip P2 - P&E to 77";
	String dialogBoxDocWorkType = "1 - Singles";

	String keyingJob = "doc";
	String batch = "";
	String clientID;
	String seqNum;
	String tranNun = "0";
	String transNum2 = "2";
	String tranCount3 = "3";
	String StbCount0 = "0";
	String StbCount1 = "1";

	String reasonMsg = "T-1 PiggyBack Transaction";
	String seqNum1 = "1";
	String seqNum2 = "2";
	String seqNum4 = "4";
	String seqNum5 = "5";
	String seqNum6 = "6";
	String seqNum7 = "7";
	String seqNum8 = "8";
	String seqNum9 = "9";
	String seqNum3 = "3";
	String chkCount6 = "6";
	String chkCount3 = "3";
	String chkCount2 = "2";
	String trans = "1";
	String dialogBoxDocId = "11 - Bevent Stub";

	String checkTotal = "0.00";
	String itemCount = "9";
	String sourceItemCount = "9";
	String checCount = "6";
	String rejectCheckCount = "0";
	String rejectStubCount = "0";
	String stubCount = "3";
	String stubTotal = "0.00";
	String tranCount = "3";
	String sourceTranCount = "1";
	String GoodBalanceStubCount = "3";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		batch = BaseUI.random_NumberAsString(700241, 700245);
		// batch = "700245";

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		// seqNum = DataCompletion_DOC.return_first_Seq();

		clientID = DataCompletion.return_ClientID();
		batch = DataCompletion.return_BatchID();
		seqNum = DataCompletion_DOC.return_first_Seq();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3843_CPR_Verify_DocJob_IsPresent() throws Exception {
		DataCompletion.verify_CorrectJob(keyingJob);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3843_CPR_Verify_ClientId_IsPresent() throws Exception {
		String expextedSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
		BaseUI.baseStringCompare("dataComplete_DOC_first_Seq", seqNum, expextedSeqNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 10)
	public void PT3843_CCA_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 36)
	public void PT3843_Verify_ConfirmDialogBoxMsg() throws Exception {

		DataCompletion.modalDialog_Click_RepairDescription_piggyBackTransaction();
		DataCompletion.modalDialog_Click_SelectReason_Button();
		DataCompletion.dataComplete_VerifyCPR_Doc_ConfirmDialogBoxMsg();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 50)
	public void PT3843_Verify_RepairDescription() throws Exception {
		DataCompletion.dataComplete_CPR_Doc_ClickNo_ConfirmDialogBox();
		String expectecBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchId"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchId", batch, expectecBatchId);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 51)
	public void PT3843_Verify_tranNun() throws Exception {

		String expectecTranNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum"));
		BaseUI.baseStringPartialCompare("dataComplete_TranNum", tranNun, expectecTranNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 52)
	public void PT3843_Verify_tranList() throws Exception {

		String expectecTranList = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranList"));
		BaseUI.baseStringPartialCompare("dataComplete_TranList", tranList, expectecTranList);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 53)
	public void PT3843_Verify_reasonMsg() throws Exception {

		String expectecReasonMsg = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ReasonMsg_PiggyBack"));
		BaseUI.baseStringPartialCompare("dataComplete_ReasonMsg_PiggyBack", reasonMsg, expectecReasonMsg);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 54)
	public void PT3843_Verify_JobItem() throws Exception {

		DataCompletion_CPR.verify_JobItem(clientID, batch, date, jobId, docGrp);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT3843_Verify_SplitBtn_IsPresent() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Split_Btn"));

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 60)
	public void PT3843_Step8_Verify_SeqNum4_IsPresent() throws Exception {

		DataCompletion_CPR.click_dataComplete_UnknownChk4();
		String NewSeqNum4 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Pass1SequenceNumber"));
		BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum4, NewSeqNum4);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 70)
	public void PT3843_Step8_Verify_SplitTran_Values() throws Exception {

		DataCompletion_CPR.click_dataComplete_SplitBtn();

		String expectedtran1 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Tran1"));
		BaseUI.baseStringPartialCompare("dataComplete_Tran1", trans, expectedtran1);

		String expectedtran2 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum2"));
		BaseUI.baseStringPartialCompare("dataComplete_TranNum2", transNum2, expectedtran2);

		String newChkCount6 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CheckCount"));
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount6, newChkCount6);

		String newStbCount0 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_StubCount0"));
		BaseUI.baseStringCompare("dataComplete_StubCount0", StbCount0, newStbCount0);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 71)
	public void PT3843_Step8_Verify_TranNum1_Img1() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img1();
		String NewSeqNum1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum1, NewSeqNum1);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum1,
		// NewSeqNum1);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 72)
	public void PT3843_Step8_Verify_TranNum1_Img2() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img1();
		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img2();
		String NewSeqNum2 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum2, NewSeqNum2);

		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum2,
		// NewSeqNum2);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 73)
	public void PT3843_Step8_Verify_TranNum1_Img3() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img2();
		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img3();
		String NewSeqNum3 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum3, NewSeqNum3);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum3,
		// NewSeqNum3);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 80)
	public void PT3843_Step8_Verify_Tran2_Img4() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img3();
		DataCompletion_CPR.click_dataComplete_Tran2_Img1();
		String NewSeqNum4 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum4, NewSeqNum4);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum4,
		// NewSeqNum4);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 81)
	public void PT3843_Step8_Verify_Tran2_Img5() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img1();
		DataCompletion_CPR.click_dataComplete_Tran2_Img2();
		String NewSeqNum5 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum5, NewSeqNum5);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum5,
		// NewSeqNum5);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 82)
	public void PT3843_Step8_Verify_Tran2_Img6() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img2();
		DataCompletion_CPR.click_dataComplete_Tran2_Img3();
		String NewSeqNum6 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum6, NewSeqNum6);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum6,
		// NewSeqNum6);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 83)
	public void PT3843_Step8_Verify_Tran2_Img7() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img3();
		DataCompletion_CPR.click_dataComplete_Tran2_Img4();
		String NewSeqNum7 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum7, NewSeqNum7);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum7,
		// NewSeqNum7);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 84)
	public void PT3843_Step8_Verify_Tran2_Img8() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img4();
		DataCompletion_CPR.click_dataComplete_Tran2_Img5();
		String NewSeqNum8 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum8, NewSeqNum8);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum8,
		// NewSeqNum8);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 85)
	public void PT3843_Step8_Verify_Tran2_Img9() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img5();
		DataCompletion_CPR.click_dataComplete_Tran2_Img6();
		String NewSeqNum9 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum9, NewSeqNum9);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum9,
		// NewSeqNum9);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 90)
	public void PT3843_Verify_SeqNum7_IsPresemt() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img6();
		BaseUI.click(Locator.lookupElement("dataComplete_UnknownChk4"));
		Navigation.wait_For_Page_ToLoad();
		String NewSeqNum7 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum7, NewSeqNum7);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum7,
		// NewSeqNum7);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 95)
	public void PT3843_Verify_SplitBtn2ndTime_ValuesUpdated() throws Exception {

		DataCompletion_CPR.click_dataComplete_SplitBtn();
		String newTranCount3 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranCount2ndTime"));
		BaseUI.baseStringCompare("dataComplete_TranCount2ndTime", tranCount3, newTranCount3);

		String newChkCount3 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ChekCount2ndTime"));
		BaseUI.baseStringCompare("dataComplete_ChekCount2ndTime", chkCount3, newChkCount3);

		String newStbCount0 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_StbCount2ndTime"));
		BaseUI.baseStringCompare("dataComplete_StbCount2ndTime", StbCount0, newStbCount0);

	}

	// verify tran#:1
	// img1 = seq1
	// img2 = seq2
	// img3 = seq3
	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 100)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_Img1() throws Exception {
		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img1();
		String tran1Img1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum1, tran1Img1);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum1,
		// tran1Img1);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 101)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_Img2() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img1();
		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img2();
		String tran1Img2 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum2, tran1Img2);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum2,
		// tran1Img2);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 102)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_Img3() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img2();
		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img3();
		String tran1Img3 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum3, tran1Img3);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum3,
		// tran1Img3);

	}

	// verify tran#:2
	// img1 = seq4
	// img2 = seq5
	// img3 = seq6
	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 103)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_tran2Img1() throws Exception {

		DataCompletion_CPR.click_dataCompleteCPR_Tran1_Img3();
		DataCompletion_CPR.click_dataComplete_Tran2_Img1();
		String tran2Img1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum4, tran2Img1);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum4,
		// tran2Img1);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 104)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_tran2Img2() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img1();
		DataCompletion_CPR.click_dataComplete_Tran2_Img2();
		String tran2Img2 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum5, tran2Img2);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum5,
		// tran2Img2);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 105)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_tran2Img3() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img2();
		DataCompletion_CPR.click_dataComplete_Tran2_Img3();
		String tran2Img3 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum6, tran2Img3);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum6,
		// tran2Img3);

	}

	// verify tran#:3
	// img1 = seq7
	// img2 = seq8
	// img3 = seq9
	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 106)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_tran3Img1() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran2_Img3();
		DataCompletion_CPR.click_dataComplete_Tran3_Img1();
		String tran3Img1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum7, tran3Img1);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum7,
		// tran3Img1);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 107)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_tran3Img2() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran3_Img1();
		DataCompletion_CPR.click_dataComplete_Tran3_Img2();
		String tran3Img2 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum8, tran3Img2);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum8,
		// tran3Img2);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 108)
	public void PT3843_Step8_Verify_NewSeqNewTranCount3_tran3Img3() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran3_Img2();
		DataCompletion_CPR.click_dataComplete_Tran3_Img3();
		String tran3Img3 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum9, tran3Img3);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum9,
		// tran3Img3);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 109)
	public void PT3843_Step8_Verify_DocIdUnknownImg4_7() throws Exception {

		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_UnknownChk4Text"), "Unknown - Chk");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_UnknownChk7Text"), "Unknown - Chk");

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 110)
	public void PT3843_Verify_SeqNum4_IsSelected() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran3_Img3();
		DataCompletion_CPR.click_dataComplete_Tran2_Img1();
		String tran2Img1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum4, tran2Img1);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum4,
		// tran2Img1);
		/// DataCompletion_CPR.click_dataComplete_Tran2_Img1();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 130)
	public void PT3843_Verify_DocId_UpdatedTran_Values() throws Exception {
		DataCompletion_CPR.click_dataComplete_DocId_Img();
		DataCompletion_CPR.select_dataComplete_DocIdDialogBox_Option(dialogBoxDocId);
		DataCompletion_CPR.dataComplete_DocId_DialogBox_OkBtn();
		String newChkCount = DataCompletion_CPR.return_CheckCount();
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount2, newChkCount);
		String newStbCount = DataCompletion_CPR.return_StubCount();
		BaseUI.baseStringCompare("dataComplete_StubCount0", StbCount1, newStbCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 140)
	public void PT3843_Verify_SeqNum7_IsSelected() throws Exception {

		DataCompletion_CPR.click_dataComplete_Tran3_Img1();
		String tran3Img1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum7, tran3Img1);
		// BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum7,
		// tran3Img1);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 170)
	public void PT3843_Verify_DocId_UpdatedTran_Values_seq7() throws Exception {

		DataCompletion_CPR.click_dataComplete_DocId_Img();
		DataCompletion_CPR.select_dataComplete_DocIdDialogBox_Option(dialogBoxDocId);
		DataCompletion_CPR.dataComplete_DocId_DialogBox_OkBtn();
		String newChkCount = DataCompletion_CPR.return_CheckCount();
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount2, newChkCount);
		String newStbCount = DataCompletion_CPR.return_StubCount();
		BaseUI.baseStringCompare("dataComplete_StubCount0", StbCount1, newStbCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 180)
	public void PT3843_Step8_Verify_DocI_BeventStubImg4_7() throws Exception {

		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BeventStub4Text"), "Bevent Stub - Stb");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BeventStub7Text"), "Bevent Stub - Stb");

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 200)
	public void PT3843_Step11_Verify_CheckIn_UpdateBatch() throws Exception {

		DataCompletion_CPR.verify_CheckIn_UpdateBatch();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 210)
	public void PT3843_Verify_CheckIn_CloseDialog() throws Exception {

		DataCompletion_CPR.verify_CheckIn_CloseDialog();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 220)
	public void PT3843_CPR_Verify_SLC_Job_IsPresent() throws Exception {

		DataCompletion_CPR.clickOk_CheckIn_CloseDialog();
		String newJOb = "cca";
		DataCompletion.verify_CorrectJob(newJOb);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 220)
	public void PT3843_CPR_Verify_SeqNum_IsPresent() throws Exception {

		String expextedSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
		BaseUI.baseStringCompare("dataComplete_DOC_first_Seq", seqNum2, expextedSeqNum);
		// String expextedSeqNum =
		// BaseUI.getTextFromField(Locator.lookupElement("dataComplete_SLC_SeqDropdown"));
		// BaseUI.baseStringCompare("dataComplete_SLC_SeqDropdown", seqNum,
		// expextedSeqNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 230)
	public void PT3843_DataCompletionPage_KeyBatch() throws Exception {

		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 234)
	public void PT3843_DataCompletionPage_KeyBatch_Information() throws Exception {
		Navigation.enter_Text_Into_CommandBox_AndNavigate("Batch " + batch);
		String expectedchkTotal = DataCompletion_CPR.return_CountInfo_checkTotal();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ChkTotal", checkTotal, expectedchkTotal);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_ItemCount() throws Exception {

		String expectedItemCount = DataCompletion_CPR.return_CountInfo_ItemCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ItemCount", itemCount, expectedItemCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_sourceItemCount() throws Exception {

		String expectedsourceItemCount = DataCompletion_CPR.return_CountInfo_SourceItemCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_sourceItemCount", sourceItemCount, expectedsourceItemCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_checCount() throws Exception {

		String expectedchecCount = DataCompletion_CPR.return_CountInfo_ChecCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ChkCount", checCount, expectedchecCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_rejectCheckCount() throws Exception {

		String expectedrejectCheckCount = DataCompletion_CPR.return_CountInfo_RejectCheckCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_rejectCheckCount", rejectCheckCount, expectedrejectCheckCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_rejectStubCount() throws Exception {

		String expectedrejectStubCount = DataCompletion_CPR.return_CountInfo_RejectStubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_rejectStubCount", rejectStubCount, expectedrejectStubCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_stubCount() throws Exception {

		String expectedstubCount = DataCompletion_CPR.return_CountInfo_StubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_stubCount", stubCount, expectedstubCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_StubTotal() throws Exception {

		String expectedstubTotal = DataCompletion_CPR.return_CountInfo_StubTotal();
		BaseUI.baseStringCompare("dataMaint_CountInfo_stubTotal", stubTotal, expectedstubTotal);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_TranCount() throws Exception {

		String expectedtranCount = DataCompletion_CPR.return_CountInfo_TranCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", tranCount, expectedtranCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_SourceTranCount() throws Exception {

		String expectedsourceTranCount = DataCompletion_CPR.return_CountInfo_SourceTranCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", sourceTranCount, expectedsourceTranCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3843_DataCompletionPage_CountInfo_GoodBalanceStubCount() throws Exception {

		String expectedGoodBalanceStubCount = DataCompletion_CPR.return_CountInfo_GoodBalanceStubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", GoodBalanceStubCount, expectedGoodBalanceStubCount);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 500)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq1() throws Exception {

		DataMaintenance_Batch.click_ViewItems();
		String[] seqNum1 = { "1", "1" };
		String seq1 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(1, "P1 Seq Number");
		String transeq1 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(1, "Tran Number");
		String[] expectedNum1 = { seq1, transeq1 };
		BaseUI.baseStringCompareArray(seqNum1, expectedNum1);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 501)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq2() throws Exception {

		String[] seqNum2 = { "2", "1" };
		String seq2 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(2, "P1 Seq Number");
		String transeq2 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(2, "Tran Number");
		String[] expectedNum2 = { seq2, transeq2 };
		BaseUI.baseStringCompareArray(seqNum2, expectedNum2);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 502)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq3() throws Exception {

		String[] seqNum3 = { "3", "1" };
		String seq3 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(3, "P1 Seq Number");
		String transeq3 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(3, "Tran Number");
		String[] expectedNum3 = { seq3, transeq3 };
		BaseUI.baseStringCompareArray(seqNum3, expectedNum3);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 503)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq4() throws Exception {

		String[] seqNum4 = { "4", "2" };
		String seq4 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(4, "P1 Seq Number");
		String transeq4 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(4, "Tran Number");
		String[] expectedNum4 = { seq4, transeq4 };
		BaseUI.baseStringCompareArray(seqNum4, expectedNum4);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 504)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq5() throws Exception {

		String[] seqNum5 = { "5", "2" };
		String seq5 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(5, "P1 Seq Number");
		String transeq5 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(5, "Tran Number");
		String[] expectedNum5 = { seq5, transeq5 };
		BaseUI.baseStringCompareArray(seqNum5, expectedNum5);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 505)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq6() throws Exception {

		String[] seqNum6 = { "6", "2" };
		String seq6 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(6, "P1 Seq Number");
		String transeq6 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(6, "Tran Number");
		String[] expectedNum6 = { seq6, transeq6 };
		BaseUI.baseStringCompareArray(seqNum6, expectedNum6);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 506)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq7() throws Exception {

		String[] seqNum7 = { "7", "3" };
		String seq7 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(7, "P1 Seq Number");
		String transeq7 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(7, "Tran Number");
		String[] expectedNum7 = { seq7, transeq7 };
		BaseUI.baseStringCompareArray(seqNum7, expectedNum7);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 507)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq8() throws Exception {

		String[] seqNum8 = { "8", "3" };
		String seq8 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(8, "P1 Seq Number");
		String transeq8 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(8, "Tran Number");
		String[] expectedNum8 = { seq8, transeq8 };
		BaseUI.baseStringCompareArray(seqNum8, expectedNum8);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 508)
	public void PT3843_DataCompletionPage_Verify_BatchItem_Seq9() throws Exception {

		String[] seqNum9 = { "9", "3" };
		String seq9 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(9, "P1 Seq Number");
		String transeq9 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(9, "Tran Number");
		String[] expectedNum9 = { seq9, transeq9 };
		BaseUI.baseStringCompareArray(seqNum9, expectedNum9);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}
