package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.sql.SQLException;
import java.util.HashMap;

public class DataMaintenance_BatchSearchResults_BatchEvents extends baseTest {

    String downloadsFolder = Browser.downloadLocation;
    String batchID = "11991";
    TableData searchResults;
    TableData exportResults;
    TableData batchEventsResults;

    //Tests are hardcoded to run in Chrome, cannot download files through IE.
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

//      Step 1-6 and 9
        Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
        DataMaintenance_Batch_Filters.enter_BatchID_AndSearch(batchID, "");
        DataMaintenance_Batch_SearchResults.select_Row_ByIndex(1);
        searchResults = DataMaintenance_Batch_SearchResults.return_TableRows_ViaColumn();
        DataMaintenance_Batch_SearchResults.click_BatchEvents();

        batchEventsResults = DataMaintenance_Batch_BatchEvents.return_TableRows_ViaColumn();
        batchEventsResults.trim_column("Event Data1");

        exportResults = DataMaintenance_Batch_BatchEvents.return_ExportList_TableData();
        exportResults.remove_Character(",", "Batch ID");
        exportResults.remove_Character(",", "Event Seq");
        exportResults.remove_Character(",", "Source ID");
        exportResults.remove_Character(",", "Tran ID");
        exportResults.remove_Character(",", "Event Station ID");
        exportResults.trim_column("Event Data1");


    }
    @DataProvider(name = "searchResults")
    public Object[][] createData_SortTests() throws ClassNotFoundException, SQLException {
        BaseUI.verify_true_AndLog(batchEventsResults.data.size() > 0, "Found Search Results.", "Did NOT find search results.");

        Object[][] rowObject = new Object[batchEventsResults.data.size()][2];

        for (Integer i = 0; i <batchEventsResults.data.size(); i++) {
            rowObject[i][0] = i;
            rowObject[i][1] = batchEventsResults.data.get(i);
        }

        return rowObject;
    }

    @Test(dataProvider = "searchResults", priority = 3, groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" })
    public void PT4442_Compare_ExportList_Against_BatchEventsSearchResults(Integer index, HashMap<String, String> tableRow)
            throws Throwable {
        //Step 9
        BaseUI.verify_TableRow_Matches(index.toString(), tableRow, exportResults.data.get(index));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter"  }, priority = 3)
    public void PT4442_ExportList_Validate_FileName() throws Exception {
        String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");
        String expectedFileText = "eventList" + todaysDate;
        String fileName = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);

        BaseUI.baseStringPartialCompare("File Name", expectedFileText, fileName);

    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 4)
    public void PT4442_Validate_ClickGoBackButton_NavigatesToBatchFilterPage() throws Exception {
        // Step 10
        DataMaintenance_Batch_BatchEvents.click_GoBack();
        BaseUI.verify_TableColumn_Contains_GivenValue("Batch ID", batchID,
               searchResults.data);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        searchResults = null;
        exportResults = null;
        batchEventsResults = null;
        Browser.closeBrowser();
    }
}
