package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.DataMaintenance_Consolidations_Filters;
import imageRPS.pages.DataMaintenance_Consolidations_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class DataMaintenance_Consolidations_FilterTests extends baseTest {

	// Integer count_WithoutFilters;
	TableData searchResults;
	//
	HashMap<String, String> searchValues = new HashMap<String, String>();
	TableData exportResults;

	String Consol = "2";
	String Autoconsol = "Yes";

	String CloseDateFrom = "01/31/2017";
	String CloseTimeFrom = "4:00pm";
	String CloseDateTo = "03/07/2017";
	String CloseTimeTo = "2:00pm";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL,"chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"})
	public void PT4684_DataMaint_ConsolidationColum_Results() throws Exception {

		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextbox(Consol);
		DataMaintenance_Consolidations_Filters.click_Submit();
		exportResults = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Consol#", "2", exportResults.data);
		DataMaintenance_Consolidations_SearchResults.verify_ColumnMatches_ExpectedValue("Consol#", "2");
		DataMaintenance_Consolidations_SearchResults.verify_FilterText_ContainsFilter("Consol#", "2");

	}

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations"}, priority = 230)
	public void PT4686_DataMaintenance_Verify_ConsolBatchFilter_CosolNumber() throws Exception {


		DataMaintenance_Consolidations_Filters.enterText_IntoConsolNumber_FromAndTo_TextBox("2", "3");
		DataMaintenance_Consolidations_Filters.click_Submit();
		DataMaintenance_Consolidations_SearchResults.verify_Column_Values_AreBetweenNumericRange("Consol#",
				2, 3);
		DataMaintenance_Consolidations_SearchResults.verify_Filter_Range_Text_ContainsFilter("Consol#", "2", "3");

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"})
	public void PT4687_DataMaint_ConsolidationColum_Auto() throws Exception {
		DataMaintenance_Consolidations_Filters.click_ConsolType_Auto();
		DataMaintenance_Consolidations_Filters.click_Submit();
		exportResults = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Consol Type", "Auto", exportResults.data);

		DataMaintenance_Consolidations_SearchResults.verify_ColumnMatches_ExpectedValue("Consol Type", "Auto");
		DataMaintenance_Consolidations_SearchResults.verify_FilterText_ContainsFilter("Consol Type", "A");
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" })
	public void PT4688_DataMaint_ConsolidationColum_Manual() throws Exception {
		DataMaintenance_Consolidations_Filters.click_ConsolType_Manual();
		DataMaintenance_Consolidations_Filters.click_Submit();
		exportResults = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Consol Type", "Manual", exportResults.data);

		DataMaintenance_Consolidations_SearchResults.verify_ColumnMatches_ExpectedValue("Consol Type", "Manual");
		DataMaintenance_Consolidations_SearchResults.verify_FilterText_ContainsFilter("Consol Type", "M");
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "regression_Tests", "module_DataMaintenance_Consolidations"})
	public void PT4693_DataMaint_ConsolidationColum_Results_Filterby_ConsolCloseDateTimeRange() throws Exception {

		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextbox_Filterby_ConsolCloseDateTimeRange(
				CloseDateFrom, CloseTimeFrom, CloseDateTo, CloseTimeTo);
		DataMaintenance_Consolidations_Filters.click_Submit();
		exportResults = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
		// searchResults.remove_Character(" ", "Applied Amount");
		BaseUI.verify_true_AndLog(exportResults.data.size() > 0, "Data was found it", "Data was Not found it");
		for (HashMap<String, String> tableRow : exportResults.data) {

			BaseUI.verify_Date_IsBetween_DateRange("01/31/2017 04:00 PM", "03/07/2017 2:00 PM",
					tableRow.get("Consol Close Date Time"), "MM/dd/yyyy h:mm a");
			BaseUI.verify_Date_IsBetween_DateRange("01/31/2017 04:00 PM", "03/07/2017 2:00 PM", 
					BaseUI.return_Date_AsDifferentFormat(tableRow.get("Consol Date"), "MM/dd/yyyy", "MM/dd/yyyy h:mm a"),
					"MM/dd/yyyy h:mm a");

		}

	}

    @Test(groups = { "all_Tests", "critical_Tests" , "regression_Tests", "module_DataMaintenance_Consolidations"})
    public void PT4699_DataMaint_ConsolidationColum_Results_Filterby_ConsolCutoffTimeRange() throws Exception {

        DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextbox_FilterBy_ConsolCutoffDateTimeRange(
                CloseDateFrom, CloseTimeFrom, CloseDateTo, CloseTimeTo);
        DataMaintenance_Consolidations_Filters.click_Submit();
        exportResults = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
        // searchResults.remove_Character(" ", "Applied Amount");
        BaseUI.verify_true_AndLog(exportResults.data.size() > 0, "Data was found it", "Data was Not found it");
        for (HashMap<String, String> tableRow : exportResults.data) {

            BaseUI.verify_Date_IsBetween_DateRange("01/31/2017 04:00 PM", "03/07/2017 2:00 PM",
                    tableRow.get("Consol Cutoff Date Time"), "MM/dd/yyyy h:mm a");
            BaseUI.verify_Date_IsBetween_DateRange("01/31/2017 04:00 PM", "03/07/2017 2:00 PM",
                    BaseUI.return_Date_AsDifferentFormat(tableRow.get("Consol Date"), "MM/dd/yyyy", "MM/dd/yyyy h:mm a"),
                    "MM/dd/yyyy h:mm a");

        }

    }

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
		DataMaintenance.navigate_to_SubTab("Consolidations");
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		exportResults = null;
		Browser.closeBrowser();
	}

}// End of Class
