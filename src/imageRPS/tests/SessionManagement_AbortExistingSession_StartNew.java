package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SessionManagement_AbortExistingSession_StartNew extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(priority = 10, groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"})
    public void PT4413_TopMenuSecurity_Login_CheckDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @Test(priority = 20, groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"})
    public void PT4413_TopMenuSecurity_ValidateSecondBrowserSession() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
        BaseUI.wait_for_PageSource_ToContainText("You are currently logged in as", 1);
        LoginPage.login_DontNavigatePastPriorSession(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
        BaseUI.verifyElementAppears(Locator.lookupElement("navigation_SessionEnded_Title"));
        BaseUI.verify_true_AndLog(BaseUI.pageSourceContainsString("Click here to abort the existing session and start a new ImageRPS Session."), "Text found", "Text not found");
    }

    @Test(priority = 30, groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"})
    public void PT4413_TopMenuSecurity_ValidateNewBrowserSession() throws Exception {
        LoginPage.navigate_past_Preexisting_SessionMessage();
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}//End of Class
