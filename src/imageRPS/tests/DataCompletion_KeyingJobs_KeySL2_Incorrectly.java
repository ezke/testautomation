package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_KeySL2_Incorrectly extends baseTest {

    private String keyingJob = "sl2";
    private String batch = "702004";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3717_SL2_Step8_Verify_WarningError_IncorrectLength() throws Exception {

        String expectedValue = "123";
        String expectedError = "Warning! Input too short";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_SL2_ErrorMessage(expectedError);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3717_SL2_Step9_Verify_WarningError_TooLong() throws Exception {

        String expectedValue = "1234567890132465789012345678901234567890123456789012345678901";
        String expectedError = "Warning! Input too long";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_SL2_ErrorMessage(expectedError);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3717_SL2_Step10_Verify_WarningError_InvalidFormat() throws Exception {

        String expectedValue = "d043307265d 36144851c 4322";
        String expectedError = "Warning! Field has invalid format, value: at position: 12, Not in ID keys allowed: -abcdefghijklmnopqrstuvwxyz0123456789";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_SL2_ErrorMessage(expectedError);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3717_SL2_Step11_Verify_WarningError_RT_IncorrectLength() throws Exception {

        String expectedValue = "1111111111";
        String expectedError = "Warning! Field: RT, Err: Incorrect Length";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_SL2_ErrorMessage(expectedError);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3717_SL2_Step12_Verify_ValidKey() throws Exception {

        String secondSequence= "2";
        String expectedValue = "d043307265d36144851c4322";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion_Scanline.verify_Seq_Changed(secondSequence);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
