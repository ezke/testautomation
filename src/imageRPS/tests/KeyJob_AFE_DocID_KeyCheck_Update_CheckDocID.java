package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_AFE_DocID_KeyCheck_Update_CheckDocID extends baseTest {

	String batchID;
	Integer rowIndex;
	TableData auditInfo;
	TableData expectedTable;
	String expectedUsername;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		expectedUsername = GlobalVariables.loginName.length() >= 8 ? GlobalVariables.loginName.substring(0, 8)
				: GlobalVariables.loginName;
		batchID = BaseUI.random_NumberAsString(700286, 700288);
		

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_KeyBatch();
		DataCompletion.verify_CorrectJob("afe");

	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4199_AFE_Tran1_VerifyDefaults() {
		Integer expectedDoc = 3;
		Integer expectedSeq = 3;
		String fieldWithFocus = "Courtesy Amount ($1)";
		Integer fixCount = 47;
		Integer errorCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(expectedDoc);
		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(errorCount,
				fieldWithFocus, fixCount);

	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4199_AFE_Tran1_UpdateAccountNumber() throws Exception {
		String fieldToKey = "Account Number (A2)";
		String textToEnter = "124567890";
		String fieldWithFocus = "Courtesy Amount ($1)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldWithFocus);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4199_AFE_Tran1_KeyCourtesyAmount_GoTo_NextSeq() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "2000";
		String fieldWithFocus = "BuildACT Edit Field (A2)";
		Integer fixCount = 46;
		String expectedTranBal = "$-20.00";
		String expectedCheck = "$20.00";
		String expectedStb = "$0.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldWithFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedCheck);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(expectedStb);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 16)
	public void PT4199_AFE_Tran1_KeyCourtesyAmount_Seq3_NoErrors() throws Exception {
		Integer expectedErrorCount = 0;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(expectedErrorCount);
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4199_AFE_Tran1_Seq3_Click_DocIDButton() throws Exception {
		Integer expectedDocID = 3;

		DataCompletion_fbl_AllFieldsBalancing.click_DocId_Button();
		DataCompletion_fbl_AllFieldsBalancing.verify_DocID_Modal_GridRow_Selected(expectedDocID);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4199_AFE_Tran1_Seq3_DocIDModal_SelectDoc2_PageUpdated() throws Exception {
		Integer docID_ToSelect = 2;
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		String expectedCourtesyAmount = "0.00";
		Integer fixCount = 47;
		Integer errorCount = 1;
		String expectedChk = "$0.00";
		String expectedTranBal = "$0.00";

		String elementToBeDisabled = "Account Number (A2)";

		DataCompletion_fbl_AllFieldsBalancing.select_DocID_Modal_Document_ByDocID(docID_ToSelect);
		DataCompletion_fbl_AllFieldsBalancing.click_DocID_Modal_SelectDocument_Button();

		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(docID_ToSelect);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue(fieldToHaveFocus,
				expectedCourtesyAmount);
		
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasExpectedValue(elementToBeDisabled, "");
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_Disabled(elementToBeDisabled);
		
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
		
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(errorCount, fieldToHaveFocus, fixCount);
		

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			auditInfo = null;
			expectedTable = null;

			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();

		} finally {
			Browser.closeBrowser();
		}
	}
}
