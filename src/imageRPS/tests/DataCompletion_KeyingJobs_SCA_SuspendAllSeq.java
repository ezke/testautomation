package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_SCA_SuspendAllSeq extends baseTest{
	
	private String keyingJob = "cca";
    private String batch = "";
    private String suspendReason = "ShopTek Suspense Traveler's Check";
    private TableData tableRows;
    
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702131, 702135);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3666_Step5_To_8_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3666_Step9_To_10_CC2_KeyTo_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "sca";
        DataCompletion.verify_CorrectJob(keyingJob);
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3666_Step11_To_12_CC3_KeyTo_SCA() throws Exception {

        tableRows = DataCompletion_Balancing.suspend_All_Sequence(suspendReason);
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message("This batch is ready for wait and does not require keying.");
        BaseUI.verify_true_AndLog(tableRows.data.size()> 0, "Found rows to test.", "Did not find rows to test.");
    }
    
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3666_Step13_Verify_DataCompletionPage_Loads() throws Exception {

    	DataCompletion.click_OK_on_Alert();
    	DataCompletion.verify_DataCompletionPage_Loaded();
    }
    
    @DataProvider(name = "keyFieldsRows")
    public Object[][] createData_KeyFieldsScanline() {
        Object[][] linksToNavigate = new Object[tableRows.data.size()][2];

        int indexToPlace_ColumnHeader = 0;
        for (HashMap<String, String> row : tableRows.data) {
            linksToNavigate[indexToPlace_ColumnHeader][1] = row;
            linksToNavigate[indexToPlace_ColumnHeader][0] = indexToPlace_ColumnHeader;
            indexToPlace_ColumnHeader++;
        }

        return linksToNavigate;
    }
    
    @Test(dataProvider = "keyFieldsRows", alwaysRun = true, priority = 50, groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"})
    public void PT3666_Step14_Compare_DataCompletion_KeyFieldsSequence_Against_ItemInfoTable(int lineIndex, HashMap<String, String> row) throws Exception {

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        int viewItemsRowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", row.get("Seq"));
       
        HashMap<String, String> viewItemsRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(viewItemsRowIndex);
        BaseUI.baseStringCompare("RejectReason",  "SUS" + "-" + suspendReason + " bank# 07020003", viewItemsRow.get("Reject Reason"));
        BaseUI.baseStringPartialCompare("AuditTrail", "Sus"+ batch +","+ "sca"+ "," + GlobalVariables.return_UserName_Formatted()+",,", viewItemsRow.get("Audit Trail") );
       
    }
    
    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
    
}
