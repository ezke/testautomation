package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_SCA_OperPref extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702229, 702230);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3673_Step8_To_9_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3673_Step10_To_11_CC2_KeyTo_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "sca";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3673_Step12_To_14_SCA_OperPref_KeyAhead_Checkbox_FiveCharacters() throws Exception {

        String valueToEnter = "12345";
        DataCompletion.verify_OperPref_KeyAhead_FiveCharacterDropdown_Works(valueToEnter);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3673_Step15_To_18_SCA_OperPref_DecimalPoint_CheckBox() throws Exception {

        String valueToEnter = "678";
        DataCompletion.click_OperPref_Button();
        DataCompletion.check_Or_uncheck_OperPref_KeyAhead_Checkbox(false);
        DataCompletion.check_Or_uncheck_OperPref_AllowDecimalPoint_Checkbox(true);
        DataCompletion.click_OperPref_OK_Button();
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"), valueToEnter);
        String keyingFieldValue = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"));
        BaseUI.baseStringCompare("KeyingTextFeild", "678.00", keyingFieldValue);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3673_Step19_To_22_SCA_OperPref_DefaultToZoom_CheckBox() throws Exception {

        HashMap<String, String> zoomedIn_StyleMap;
        HashMap<String, String> zoomedOut_StyleMap;
        int zoomedWidth;
        int zoomedHeight;
        int zoomedOutWidth;
        int zoomedOutHeight;

        zoomedOut_StyleMap = DataCompletion.return_Image_Style_Info();
        zoomedOutWidth = Integer.parseInt(zoomedOut_StyleMap.get("width").replace("px", ""));
        zoomedOutHeight = Integer.parseInt(zoomedOut_StyleMap.get("height").replace("px", ""));

        DataCompletion.click_OperPref_Button();
        DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
        DataCompletion.click_OperPref_OK_Button();
        DataCompletion.end_DataCompletion();
        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());

        zoomedIn_StyleMap = DataCompletion.return_Image_Style_Info();
        zoomedWidth = Integer.parseInt(zoomedIn_StyleMap.get("width").replace("px", ""));
        zoomedHeight = Integer.parseInt(zoomedIn_StyleMap.get("height").replace("px", ""));

        BaseUI.verify_true_AndLog(zoomedHeight > zoomedOutHeight,
                "Zoomed In height " + String.valueOf(zoomedHeight) + " is Greater than " + String.valueOf(zoomedOutHeight),
                "Zoomed In height " + String.valueOf(zoomedHeight) + " was NOT Greater than " + String.valueOf(zoomedOutHeight));

        BaseUI.verify_true_AndLog(zoomedWidth > zoomedOutWidth,
                "Zoomed In width " + String.valueOf(zoomedWidth) + " is Greater than " + String.valueOf(zoomedOutWidth),
                "Zoomed In width " + String.valueOf(zoomedWidth) + " was NOT Greater than " + String.valueOf(zoomedOutWidth));

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3673_Step23_SCA_OperPref_StubImage_And_DecimalPoint() throws Exception {

        String valueToEnter = "123";
        String previousCheckImage = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("dataComplete_Img1_Image"),"src");
        DataCompletion.click_OperPref_Button();
        DataCompletion.check_Or_uncheck_OperPref_KeyAhead_Checkbox(true);
        DataCompletion.click_OperPref_OK_Button();
        BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"), valueToEnter);
        String newCheckImage = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("dataComplete_Img1_Image"),"src");
        BaseUI.verify_false_AndLog(previousCheckImage.equals(newCheckImage),
                "Image changed from previousCheckImage to newCheckImage",
                "Image did Not change from previousCheckImage to newCheckImage");
        String keyingFieldValue = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"));
        BaseUI.baseStringCompare("KeyingTextFeild", "123.00", keyingFieldValue);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            DataCompletion.click_OperPref_Button();
            DataCompletion.check_Or_uncheck_OperPref_KeyAhead_Checkbox(false);
            DataCompletion.check_Or_uncheck_OperPref_AllowDecimalPoint_Checkbox(false);
            DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
            DataCompletion.click_OperPref_OK_Button();
        }finally{
            Browser.closeBrowser();
        }

    }

}
