package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Consolidations_Filters;
import imageRPS.pages.DataMaintenance_Consolidations_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Stop_ManualConsolidations_Tests extends baseTest {

	TableData searchResults;
	TableData searchResults_Updated;
	TableData nextConsolidationDateConfirmation_Results;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");
		DataMaintenance_Consolidations_Filters.click_ConsolType_Manual();
		DataMaintenance_Consolidations_Filters.click_Submit();
		DataMaintenance_Consolidations_SearchResults.sort_SearchResults_Ascending("Client ID");
		searchResults = DataMaintenance_Consolidations_SearchResults.return_SearchResults();

		String searchResults_401_ConsolNum = searchResults.return_Row_BasedOn_1MatchingField("Client ID", "401")
				.get("Consol#");
		if (!searchResults_401_ConsolNum.equals("2")) {
			DataMaintenance_Consolidations_SearchResults
					.check_ConsolidationCheckbox_ByIndex(searchResults.first_IndexOf("Client ID", "401") + 1);
			DataMaintenance_Consolidations_SearchResults.click_StopConsol();
			DataMaintenance_Consolidations_SearchResults.click_StopConsol_OK_Button();
			DataMaintenance_Consolidations_SearchResults.click_NextConsolidationDateConfirmation_ConfirmButton();
			DataMaintenance_Consolidations_SearchResults.sort_SearchResults_Ascending("Client ID");
			searchResults = DataMaintenance_Consolidations_SearchResults.return_SearchResults();
		}

	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 10)
	public void PT4629_Consolidations_Select_First3_SelectedCountIncreases() throws Exception {
		// XPath indexing starts at 1.
		DataMaintenance_Consolidations_SearchResults
				.check_ConsolidationCheckbox_ByIndex(searchResults.first_IndexOf("Client ID", "401") + 1);
		DataMaintenance_Consolidations_SearchResults
				.check_ConsolidationCheckbox_ByIndex(searchResults.first_IndexOf("Client ID", "2") + 1);
		// DataMaintenance_Consolidations_SearchResults.check_ConsolidationCheckbox_ByIndex(3);
		DataMaintenance_Consolidations_SearchResults.verify_SelectedRowCount(2);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 20)
	public void PT4629_Consolidations_Click_StopConsol_DefaultsTo_SystemGeneratedDate() throws Exception {
		DataMaintenance_Consolidations_SearchResults.click_StopConsol();
		DataMaintenance_Consolidations_SearchResults.verify_StopGeneratedDate_Selected();
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_Consolidations"}, priority = 30)
	public void PT4629_Consolidations_Click_StopConsol_OK_NextConsolidationDateConfirmation_MatchesExpected_ConsolNumIncremented()
			throws Exception {
		DataMaintenance_Consolidations_SearchResults.click_StopConsol_OK_Button();
		nextConsolidationDateConfirmation_Results = DataMaintenance_Consolidations_SearchResults
				.return_NextConsolidationDateConfirmation_Modal_Table();

		// DataMaintenance_Consolidations_SearchResults
		// .verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNumIncremented(searchResults,
		// nextConsolidationDateConfirmation_Results,
		// searchResults.first_IndexOf("Client ID", "401"));

		DataMaintenance_Consolidations_SearchResults
				.verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNum_401_Decremented(
						searchResults, nextConsolidationDateConfirmation_Results,
						searchResults.first_IndexOf("Client ID", "401"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 31)
	public void PT4629_Consolidations_Click_StopConsol_OK_NextConsolidationDateConfirmation_MatchesExpected_ConsolNum_didNOTincrement()
			throws Exception {
		DataMaintenance_Consolidations_SearchResults
				.verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNum_NOT_incremented(
						searchResults, nextConsolidationDateConfirmation_Results,
						searchResults.first_IndexOf("Client ID", "2"), 1);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 40)
	public void PT4629_Consolidations_ClickConfirm_NextConsolidationDateConfirmation_MatchesExpected_401()
			throws Exception {
		DataMaintenance_Consolidations_SearchResults.click_NextConsolidationDateConfirmation_ConfirmButton();
		DataMaintenance_Consolidations_SearchResults.sort_SearchResults_Ascending("Client ID");
		searchResults_Updated = DataMaintenance_Consolidations_SearchResults.return_SearchResults();
		DataMaintenance_Consolidations_SearchResults
				.verify_UPDATED_Match_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows(
						searchResults_Updated, nextConsolidationDateConfirmation_Results,
						searchResults.first_IndexOf("Client ID", "401"));

	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 41)
	public void PT4629_Consolidations_NextConsolidationDateConfirmation_MatchesExpected_2() throws Exception {
		DataMaintenance_Consolidations_SearchResults
				.verify_UPDATED_Match_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows(
						searchResults_Updated, nextConsolidationDateConfirmation_Results,
						searchResults.first_IndexOf("Client ID", "2"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 42)
	public void PT4629_Consolidations_NextConsolidationDateConfirmation_StopBatch_401_NextConsolidationDateConfirmation_Increments()
			throws Exception {
		nextConsolidationDateConfirmation_Results = DataMaintenance_Consolidations_SearchResults
				.select_401_AndNavigateTo_NextConsolidationDateConfirmation_ReturnTableResults(
						searchResults_Updated.first_IndexOf("Client ID", "401") + 1);

		DataMaintenance_Consolidations_SearchResults
				.verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNumIncremented(
						searchResults_Updated, nextConsolidationDateConfirmation_Results,
						searchResults_Updated.first_IndexOf("Client ID", "401"));
		BaseUI.baseStringCompare("New Consol Time", "02:53 pm",
				nextConsolidationDateConfirmation_Results.data.get(0).get("New Consol Time"));

		// Verify new info from updated TC

	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 43)
	public void PT4629_Consolidations_NextConsolidationDateConfirmation_StopBatch_401_SearchResults_Increments()
			throws Exception {
		DataMaintenance_Consolidations_SearchResults.click_NextConsolidationDateConfirmation_ConfirmButton();
		DataMaintenance_Consolidations_SearchResults.sort_SearchResults_Ascending("Client ID");
		searchResults_Updated = DataMaintenance_Consolidations_SearchResults.return_SearchResults();
		DataMaintenance_Consolidations_SearchResults
				.verify_UPDATED_Match_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows(
						searchResults_Updated, nextConsolidationDateConfirmation_Results,
						searchResults_Updated.first_IndexOf("Client ID", "401"));

	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 44)
	public void PT4629_Consolidations_NextConsolidationDateConfirmation_StopBatch_401_SearchResults_Decreases_2ndTime_nextConsolidationDateConfirmation()
			throws Exception {

		nextConsolidationDateConfirmation_Results = DataMaintenance_Consolidations_SearchResults
				.select_401_AndNavigateTo_NextConsolidationDateConfirmation_ReturnTableResults(
						searchResults_Updated.first_IndexOf("Client ID", "401") + 1);

		DataMaintenance_Consolidations_SearchResults
				.verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNum_401_Decremented(
						searchResults_Updated, nextConsolidationDateConfirmation_Results,
						searchResults_Updated.first_IndexOf("Client ID", "401"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_Consolidations" }, priority = 45)
	public void PT4629_Consolidations_NextConsolidationDateConfirmation_StopBatch_401_SearchResults_Decreases_2ndTime_SearchResults_Updated()
			throws Exception {

		DataMaintenance_Consolidations_SearchResults.click_NextConsolidationDateConfirmation_ConfirmButton();

		searchResults_Updated = DataMaintenance_Consolidations_SearchResults.return_SearchResults();

		// Verify new info from updated TC
		DataMaintenance_Consolidations_SearchResults
				.verify_UPDATED_Match_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows(
						searchResults_Updated, nextConsolidationDateConfirmation_Results,
						searchResults_Updated.first_IndexOf("Client ID", "401"));

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		searchResults_Updated = null;
		nextConsolidationDateConfirmation_Results = null;

		Browser.closeBrowser();
	}
}// End of Class
