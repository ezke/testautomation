package imageRPS.tests;

import java.sql.SQLException;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class DataMaintenance_BatchSearchResults_ExportAndPreview_Lists extends baseTest {

	String downloadsFolder = Browser.downloadLocation;
	String clientID = "21";
	
	
	TableData searchResults;
	TableData exportResults;
	TableData previewListResults;

	//Tests are hardcoded to run in Chrome, cannot download files through IE.
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance_Batch_Filters.enter_ClientID(clientID);
		DataMaintenance_Batch_Filters.click_Submit();
		Thread.sleep(100);
		
		searchResults = DataMaintenance_Batch_SearchResults.return_TableRows_ViaColumn();
		searchResults.remove_Character("$", "Check Total");
		
		exportResults = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
		exportResults.remove_Character(" ", "PbC");
		exportResults.remove_Character(",", "Batch Num");
		exportResults.remove_Character(",", "Batch ID");
		exportResults.format_Number_forCurrency("Check Total");
	
		DataMaintenance_Batch_SearchResults.click_PreviewList();
		previewListResults = DataMaintenance_Batch_SearchResults.return_PreviewList_SearchResults();
		previewListResults.replace_Empty_WithEmpty();
		previewListResults.remove_Character(" ", "Locked By");
		previewListResults.remove_Character(",", "Batch ID");
		previewListResults.remove_Character(",", "Batch Num");
		previewListResults.format_Number_forCurrency("Check Total");
	}
	
	@DataProvider(name = "searchResults")
	public Object[][] createData_SortTests() throws ClassNotFoundException, SQLException {
		BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Found Search Results.", "Did NOT find search results.");
		
		Object[][] rowObject = new Object[searchResults.data.size()][2];

		for (Integer i = 0; i < searchResults.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = searchResults.data.get(i);
		}

		return rowObject;
	}

	@Test(dataProvider = "searchResults", priority = 3, groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4443_Compare_ExportList_Against_SearchResults(Integer index, HashMap<String, String> tableRow)
			throws Throwable {

		BaseUI.verify_TableRow_Matches(index.toString(), tableRow, exportResults.data.get(index));
	}

	@Test(dataProvider = "searchResults", priority = 3, groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_BatchFilter" })
	public void PT4444_Compare_PreviewList_Against_SearchResults(Integer index, HashMap<String, String> tableRow)
			throws Throwable {

		BaseUI.verify_TableRow_Matches(index.toString(), tableRow, previewListResults.data.get(index));
	}
	
	

	@Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_BatchFilter" }, priority = 3)
	public void PT4443_ExportList_Validate_FileName() throws Exception {
		//batchTicketList20170519
		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");
		String expectedFileText = "batchList" + todaysDate;
		String fileName = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);
		
		BaseUI.baseStringPartialCompare("File Name", expectedFileText, fileName);
		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		exportResults = null;
		previewListResults = null;

		Browser.closeBrowser();
	}

}// End of Class
