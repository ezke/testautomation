package imageRPS.tests;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DataMaintenance_act_Validating_Signing_out_browserAlert extends baseTest {

	String ItemK = "Item Keying Count Graph";
	String alertG = "Alert Count Graph";
	String Consolidations = "Consolidations by Cutoff Time Graph";
	String keyingJob = "act";
	String linkToCheck = "ImageRpsTest";
	
	String expextedmsg = "Data Completion(act): Please finish the batch in act.";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4407_verify_EntityImageRPSTest_IsSelected() throws Exception {

		Navigation.verify__toolbarMenu_atTheTopOfThePage(linkToCheck);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4407_verify_TopNavigationTab_IsActive() throws Exception {

		Navigation.verify_Page_Active(TopNavigationTab.Dashboard);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4407_verifytable_TablePreferences() throws Exception {

		Dashboard.verifytable(ItemK, alertG, Consolidations);
		//Dashboard.return_preferencesList();
		
		
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4407_verify_DashboardWidget_arePresnt() throws Exception {

		Dashboard.verify_DashboardWidget_arePresnt();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4407_navigateToDAtqMaitenance() throws Exception {

		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion.click_Abbreviation(keyingJob);
		DataCompletion.verify_CorrectJob(keyingJob);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4407_verify_SingOut_Alertpopup() throws Exception {

		Navigation.signOut();
//		 String testtoget = BaseUI.switchToAlert_GetText_AndDismiss();
//		 String alert = "";
//		BaseUI.baseStringCompare(alert, expextedmsg, testtoget);
		BaseUI.verify_AlertPresent_AndDismiss();
		

	}

	@Test(groups = { "all_Test", "critical_Tests" }, priority = 40)
	public void PT4407_verify_seecionIs_Active() throws Exception {

		BaseUI.switch_ToDefaultContent();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
