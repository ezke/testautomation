package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.sql.SQLException;
import java.util.HashMap;

public class DataMaintenance_Consolidations_ExportList extends baseTest {

    String downloadsFolder = Browser.downloadLocation;
    String clientID = "602";

    TableData searchResults;
    TableData exportResults;

    // Tests are hardcoded to run in Chrome, cannot download files through IE.
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
        DataMaintenance.navigate_to_SubTab("Consolidations");
        DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(clientID);
        DataMaintenance_Consolidations_Filters.click_Submit();
        searchResults = DataMaintenance_Consolidations_SearchResults.return_TableRows_ViaColumn();
        searchResults.remove_Character("$", "Consol Amount");
        searchResults.remove_Character("$", "Consol Batches");
        searchResults.remove_Character("$", "Open Amount");
        searchResults.remove_Character(",", "Open Amount");
        searchResults.split_column("Consol Batches", "\\.", 0);
        searchResults.split_column("Open Amount", "\\.", 0);

        exportResults = DataMaintenance_Consolidations_SearchResults.return_ExportList_TableData();
        exportResults.remove_Character("$", "Consol Amount");
        exportResults.remove_Character("$", "Open Amount");
        exportResults.remove_Character(",", "Open Amount");
        exportResults.remove_Character(",", "Lockbox ID");


    }

    @DataProvider(name = "searchResults")
    public Object[][] createData_SortTests() throws ClassNotFoundException, SQLException {
        BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Found Search Results.",
                "Did NOT find search results.");

        Object[][] rowObject = new Object[searchResults.data.size()][2];

        for (Integer i = 0; i < searchResults.data.size(); i++) {
            rowObject[i][0] = i;
            rowObject[i][1] = searchResults.data.get(i);
        }

        return rowObject;
    }

    @Test(dataProvider = "searchResults", priority = 3, groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations" })
    public void PT4594_Compare_ExportList_Against_SearchResults(Integer index, HashMap<String, String> tableRow)
            throws Throwable {

        BaseUI.verify_TableRow_Matches(index.toString(), tableRow, exportResults.data.get(index));
    }

    @Test(groups = { "all_Tests", "regression_Tests" , "module_DataMaintenance_Consolidations" }, priority = 4)
    public void PT4594_ExportList_Validate_FileName() throws Exception {
        //consolidationList20170519
        String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");
        String expectedFileText = "consolidationList" + todaysDate;
        String fileName = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);

        BaseUI.baseStringPartialCompare("File Name", expectedFileText, fileName);

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        searchResults = null;
        exportResults = null;

        Browser.closeBrowser();
    }

}
