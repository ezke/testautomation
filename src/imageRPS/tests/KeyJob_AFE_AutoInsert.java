package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class KeyJob_AFE_AutoInsert extends baseTest {

	String batchID;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		batchID = BaseUI.random_NumberAsString(700296, 700300);

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		DataCompletion_fbl_AllFieldsBalancing.deselect_AutoInsert();
	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4160_AFE_Tran1_VerifyDefaults() {
		Integer expectedSeq = 2;
		Integer expectedDoc = 3;
		Integer expectedFixCount = 7;
		Integer expectedErrorCount = 1;
		String textBoxThatShouldHaveFocus = "Courtesy Amount ($1)";

		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(textBoxThatShouldHaveFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(expectedErrorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_AutoInsertButton_EnabledORDisabled(false);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(expectedDoc);
	}

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4160_Tran1_AFE_AutoInsert_Click_Enables_Button() throws Exception {
		DataCompletion_fbl_AllFieldsBalancing.click_AutoInsert();
		DataCompletion_fbl_AllFieldsBalancing.verify_AutoInsertButton_EnabledORDisabled(true);
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4160_AFE_Tran1_Seq2_to_Seq3_CourtesyAmount_AndEnter_NextSeq() throws Exception {
		String textToEnter = "100";
		String textBoxToEnterTextTo = "Courtesy Amount ($1)";
		Integer errorCount = 1;
		Integer fixCount = 6;
		String tranBalance = "$-1.00";
		String chkBal = "$1.00";
		String stbBal = "$0.00";
		Integer expectedSeq = 3;
		String textBoxToHaveFocus = "Courtesy Amount ($1)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(
				tranBalance, chkBal, stbBal, textBoxToHaveFocus, errorCount, fixCount, expectedSeq);
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4160_AFE_Tran1_Seq2_ErrorCleared() throws Exception {
		Integer errorCount = 0;
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4160_AFE_Tran1_Seq3_CourtesyAmount_ErrorIconVisible_And_TextCorrect() throws Exception {
		String fieldToCheck = "Courtesy Amount ($1)";
		String expectedError = "Field 'Courtesy Amount' ($1) requires operator action in afe.";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Error_Visible_ForTextField_AndTitleMatches(fieldToCheck,
				expectedError);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4160_AFE_Tran1_Seq3_to_Seq4_CourtesyAmount_Key200_AndHitEnter_NextSeq() throws Exception {
		String textToEnter = "200";
		String textBoxToEnterTextTo = "Courtesy Amount ($1)";
		Integer errorCount = 1;
		Integer fixCount = 5;
		String tranBalance = "$-3.00";
		String chkBal = "$3.00";
		String stbBal = "$0.00";
		Integer expectedSeq = 4;
		String textBoxToHaveFocus = "Courtesy Amount ($1)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(
				tranBalance, chkBal, stbBal, textBoxToHaveFocus, errorCount, fixCount, expectedSeq);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT4160_AFE_Tran1_Seq3_ErrorCleared() throws Exception {
		Integer errorCount = 0;
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 32)
	public void PT4160_AFE_Tran1_Seq4_CourtesyAmount_ErrorIconVisible_And_TextCorrect() throws Exception {
		String fieldToCheck = "Courtesy Amount ($1)";
		String expectedError = "Field 'Courtesy Amount' ($1) requires operator action in afe.";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.verify_Error_Visible_ForTextField_AndTitleMatches(fieldToCheck,
				expectedError);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4160_AFE_Tran1_Seq4_to_Seq1_CourtesyAmount_Key300_AndHitEnter_NextSeq() throws Exception {
		String textToEnter = "300";
		String textBoxToEnterTextTo = "Courtesy Amount ($1)";
		Integer errorCount = 4;
		Integer fixCount = 4;
		String tranBalance = "$-6.00";
		String chkBal = "$6.00";
		String stbBal = "$0.00";
		Integer expectedSeq = 1;
		String textBoxToHaveFocus = "Account Number";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(
				tranBalance, chkBal, stbBal, textBoxToHaveFocus, errorCount, fixCount, expectedSeq);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT4160_AFE_Tran1_Seq4_ErrorCleared() throws Exception {
		Integer errorCount = 0;
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 42)
	public void PT4160_AFE_Tran1_Seq1_AccountNumber_ErrorIconVisible_And_TextCorrect() throws Exception {
		String fieldToCheck = "Account Number";
		String expectedError = "Field is too short (current length = 0). Minimum length = 1.";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.verify_Error_Visible_ForTextField_AndTitleMatches(fieldToCheck,
				expectedError);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT4160_AFE_Tran1_Seq1_KeyA1_O1_O2() throws Exception {
		String textToEnter1 = "300";
		String textBoxToEnterTextTo1 = "Account Number";
		String textToEnter2 = "1";
		String textBoxToEnterTextTo2 = "O1";
		String textToEnter3 = "1";
		String textBoxToEnterTextTo3 = "O2";

		Integer errorCount = 1;
		Integer fixCount = 1;
		String tranBalance = "$-6.00";
		String chkBal = "$6.00";
		String stbBal = "$0.00";
		Integer expectedSeq = 1;
		String textBoxToHaveFocus = "Dol 5 ($5)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo1, textToEnter1);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo2, textToEnter2);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo3, textToEnter3);
		DataCompletion_fbl_AllFieldsBalancing.verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(
				tranBalance, chkBal, stbBal, textBoxToHaveFocus, errorCount, fixCount, expectedSeq);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 46)
	public void PT4160_AFE_Tran1_Seq1_Dol5_ErrorIconVisible_And_TextCorrect() throws Exception {
		String fieldToCheck = "Dol 5 ($5)";
		String expectedError = "Field 'Dol 5' ($5) requires operator action in afe.";

		DataCompletion_fbl_AllFieldsBalancing.verify_Error_Visible_ForTextField_AndTitleMatches(fieldToCheck,
				expectedError);
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT4160_AFE_Seq1_to_Seq10_Dol5_KeyAndEnter() throws Exception {
		String textToEnter = "200";
		String textBoxToEnterTextTo = "Dol 5 ($5)";
		Integer errorCount = 16;
		Integer fixCount = 16;
		String tranBalance = "$-4.00";
		String chkBal = "$6.00";
		String stbBal = "$2.00";
		Integer expectedSeq = 10;
		Integer expectedDoc = 25;
		String textBoxToHaveFocus = "Account Number (A1)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(
				tranBalance, chkBal, stbBal, textBoxToHaveFocus, errorCount, fixCount, expectedSeq);


		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Doc(expectedDoc);
		DataCompletion_fbl_AllFieldsBalancing.verify_INS_Present_ForCurrentlySelection();
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 51)
	public void PT4160_AFE_Seq1_NoErrors() throws Exception {
		Integer errorCount = 0;
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);

	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 52)
	public void PT4160_AFE_Seq10_SelectDefaultInsertDocument_AndVerify_ButtonMatches() throws Exception {
		Integer docID_ToUse = 24;
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(5);
		DataCompletion_fbl_AllFieldsBalancing.update_DefaultInsertDocument(docID_ToUse);
		DataCompletion_fbl_AllFieldsBalancing.verify_DefDoc_Button_HasCorrectNumber(docID_ToUse);

	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT4160_AFE_Tran1_Seq10_KeyToLastField() throws Exception {
		HashMap<String, String> fieldsToKey = new HashMap<String, String>();
		fieldsToKey.put("A1", "1234567890");
		fieldsToKey.put("A2", "1");
		fieldsToKey.put("A3", "1");
		fieldsToKey.put("A4", "12345");
		fieldsToKey.put("A5", "1");
		fieldsToKey.put("A6", "1");
		fieldsToKey.put("A7", "a");
		fieldsToKey.put("A8", "@");
		fieldsToKey.put("A9", "1");
		fieldsToKey.put("A10", "1");
		fieldsToKey.put("O1", "1");
		fieldsToKey.put("O2", "1");
		fieldsToKey.put("O3", "1");
		fieldsToKey.put("O4", "1");
		fieldsToKey.put("O5", "1");

		Integer errorCount = 1;
		Integer fixCount = 1;

		String fieldToCheck = "Dol 5 ($5)";
		String expectedError = "Field 'Dol 5' ($5) requires operator action in afe.";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoFields(fieldsToKey);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);

		DataCompletion_fbl_AllFieldsBalancing.verify_Error_Visible_ForTextField_AndTitleMatches(fieldToCheck,
				expectedError);
	}

	// Step 12
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4160_AFE_Tran1_Seq10_to_Seq11_Dol5_Key300_AndHitEnter_NextSeq() throws Exception {
		String textToEnter = "300";
		String textBoxToEnterTextTo = "Dol 5 ($5)";
		Integer errorCount = 16;
		Integer fixCount = 16;
		String tranBalance = "$-1.00";
		String chkBal = "$6.00";
		String stbBal = "$5.00";
		Integer expectedSeq = 11;
		String textBoxToHaveFocus = "A1";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(textBoxToEnterTextTo, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(
				tranBalance, chkBal, stbBal, textBoxToHaveFocus, errorCount, fixCount, expectedSeq);
	}

	// Step 12
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 61)
	public void PT4160_AFE_Seq10_NoErrors() throws Exception {
		Integer errorCount = 0;
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(5);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
	}

	// Step 13
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 65)
	public void PT4160_AFE_Seq11_KeySeq_ToLastField() throws Exception {
		HashMap<String, String> fieldsToKey = new HashMap<String, String>();
		fieldsToKey.put("A1", "1234567890");
		fieldsToKey.put("A2", "1");
		fieldsToKey.put("A3", "1");
		fieldsToKey.put("A4", "12345");
		fieldsToKey.put("A5", "1");
		fieldsToKey.put("A6", "1");
		fieldsToKey.put("A7", "a");
		fieldsToKey.put("A8", "@");
		fieldsToKey.put("A9", "1");
		fieldsToKey.put("A10", "1");
		fieldsToKey.put("O1", "1");
		fieldsToKey.put("O2", "1");
		fieldsToKey.put("O3", "1");
		fieldsToKey.put("O4", "1");
		fieldsToKey.put("O5", "1");

		Integer errorCount = 1;
		Integer fixCount = 1;

		String fieldToCheck = "Dol 5 ($5)";
		String expectedError = "Field 'Dol 5' ($5) requires operator action in afe.";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(6);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoFields(fieldsToKey);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);

		DataCompletion_fbl_AllFieldsBalancing.verify_Error_Visible_ForTextField_AndTitleMatches(fieldToCheck,
				expectedError);

	}

	// Step 14
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public void PT4160_AFE_Seq11_LastField_GoesToTran2() throws Exception {
		Integer expectedTranNum = 2;
		String fieldToKey = "Dol 5 ($5)";
		String textToEnter = "100";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTranNum);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();
			
		} finally {
			Browser.closeBrowser();
		}
	}
}
