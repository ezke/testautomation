package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_PrintBatchTicket_3PrintBatchTicket_ValidatePDF extends baseTest {

	String clientID = "601";
	String docGroup = "98";
	WorkType workType = WorkType.Single;
	TableData searchResults = new TableData();
	TableData pdfResults = new TableData();

	String expectedLockbox = "601601";
	String expectedDocGropup = "98 - Singles (Print Batch Ticket)";
	Integer batchRangeLower = 999900;
	Integer batchRangeUpper = 999906;

	Integer indexToCheck = 1;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "firefox");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(clientID, null, docGroup, workType);
		Utilities_PrintTicket_SearchResults.check_Checkbox_ByIndex(indexToCheck);
		Utilities_PrintTicket_SearchResults.launch_Print3PartTicket_PDF();
		pdfResults = Utilities_PrintTicket_SearchResults.return_BatchTicket_PDF_data();

		// searchResults = Utilities_PrintTicket_SearchResults.return_SearchResults2();

	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3367_Verify_Each_PDF_Entry_Matches_ExpectedResults() throws Throwable {
		BaseUI.verify_true_AndLog(pdfResults.data.size() == 20, "Found 20 Search Results.",
				"Did NOT find 20 search results.");

	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3367_Verify_Each_PDF_Entry_Matches_ExpectedResults1() throws Throwable {
		for (Integer i = 0; i < pdfResults.data.size(); i = i + 2) {
			//batches on the same page should have same batch number.
			String firstBatch_PageNumber = pdfResults.data.get(i).get("pageNumber");
			String secondBatch_PageNumber = pdfResults.data.get(i + 1).get("pageNumber");
			BaseUI.verify_true_AndLog(firstBatch_PageNumber.equals(secondBatch_PageNumber),
					"Page Number " + firstBatch_PageNumber + " matched.",
					"Page Number " + firstBatch_PageNumber + " did not match " + secondBatch_PageNumber);
			
			
			String firstBatch_BatchID = pdfResults.data.get(i).get("batchID");
			String secondBatch_BatchID = pdfResults.data.get(i + 1).get("batchID");
			BaseUI.verify_true_AndLog(firstBatch_BatchID.equals(secondBatch_BatchID),
					"Batch ID " + firstBatch_BatchID + " matched.",
					"Batch ID " + firstBatch_PageNumber + " did not match " + secondBatch_BatchID);

			//Next page should have different page number.
			if ((i + 2) < pdfResults.data.size()) {
				String thirdBatch_PageNumber = pdfResults.data.get(i + 2).get("pageNumber");

				BaseUI.verify_true_AndLog(!firstBatch_PageNumber.equals(thirdBatch_PageNumber),
						"Next entry had a different page number " + thirdBatch_PageNumber,
						"Next entry was SUPPOSED to have a different page number.");
				
				
				//Batch Number should change from page to page.
				String thirdBatch_BatchID = pdfResults.data.get(i + 2).get("batchID");
				BaseUI.verify_true_AndLog(!firstBatch_BatchID.equals(thirdBatch_BatchID),
						"Next entry had a different Batch ID " + thirdBatch_PageNumber,
						"Next entry was SUPPOSED to have a different Batch ID.");
				
			}

		}

	}

	@DataProvider(name = "pdfResults")
	public Object[][] createData_PDFResults() throws Exception {
//		Utilities_PrintTicket_SearchResults.check_Checkbox_ByIndex(indexToCheck);
//		Utilities_PrintTicket_SearchResults.launch_Print3PartTicket_PDF();
//		pdfResults = Utilities_PrintTicket_SearchResults.return_PrintBatchTicket_PDF_data();

		Object[][] rowObject = new Object[pdfResults.data.size()][2];

		for (Integer i = 0; i < pdfResults.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = pdfResults.data.get(i);
		}

		return rowObject;
	}

	@Test(dataProvider = "pdfResults", groups = { "all_Tests", "critical_Tests" })
	public void PT3367_Verify_Each_PDF_Entry_Matches_ExpectedResults(Integer index, HashMap<String, String> pdfEntry)
			throws Throwable {
		Utilities_PrintTicket_SearchResults.verify_BatchTicket_PDFEntry_AgainstExpectedResults(pdfEntry,
				expectedLockbox, expectedDocGropup, clientID, batchRangeLower, batchRangeUpper);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		pdfResults = null;

		Browser.closeBrowser();
	}
}// End of Class
