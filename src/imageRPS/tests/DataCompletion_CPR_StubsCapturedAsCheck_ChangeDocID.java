package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_CPR;
import imageRPS.pages.DataCompletion_DOC;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_CPR_StubsCapturedAsCheck_ChangeDocID extends baseTest {

	String docType = "";

	String tranList = "Transaction List";

	String docGrp = "1";
	String jobId = "CPR";
	String date = "02/03/2015";
	String newClientid1 = "405";
	String newDocGrp1 = "9";
	String newClientId = "405 - ClientSRTest405";
	String newDocGrp = "9 - Singles - Wait - Skip P2 - X9";
	String dialogBoxclientId = "20 - Bevent Insurance";
	String dialogBoxDocGrpId = "1 - Singles - Skip P2 - P&E to 77";
	String dialogBoxDocWorkType = "1 - Singles";

	String keyingJob = "cca";
	String batch = "";
	String clientID;
	String seqNum;
	String tranNun = "0";
	String transNum2 = "2";
	String StbCount0 = "0";
	String StbCount1 = "1";

	String reasonMsg = "I-2 Stub Captured as Check";

	String seqNum2 = "2";
	String seqNum7 = "7";
	String seqNum7row = "14";
	String seqNum11 = "11";
	String seqNum11row = "20";
	String seqNum3 = "3";
	String seqNum3row = "8";
	String chkCount1 = "1";
	String chkCount3 = "3";
	String chkCount2 = "2";
	String trans = "1";
	String dialogBoxDocId = "11 - Bevent Stub";
	String seqLabel = "Bevent Stub - Stb";

	String checkTotal = "0.00";
	String itemCount = "20";
	String sourceItemCount = "20";
	String checCount = "10";
	String rejectCheckCount = "0";
	String rejectStubCount = "0";
	String stubCount = "10";
	String stubTotal = "0.00";
	String tranCount = "10";
	String sourceTranCount = "10";
	String GoodBalanceStubCount = "10";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		//batch = BaseUI.random_NumberAsString(700336, 700338);
		batch = "700337";

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		// seqNum = DataCompletion_DOC.return_first_Seq();

		clientID = DataCompletion.return_ClientID();
		batch = DataCompletion.return_BatchID();
		seqNum = DataCompletion_DOC.return_first_Seq();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3835_CPR_Verify_DocJob_IsPresent() throws Exception {
		DataCompletion.verify_CorrectJob(keyingJob);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3835_CPR_Verify_ClientId_IsPresent() throws Exception {
		String expextedSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
		BaseUI.baseStringCompare("dataComplete_DOC_first_Seq", seqNum, expextedSeqNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3835_CCA_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT3835_Verify_ConfirmDialogBoxMsg() throws Exception {

		DataCompletion_CPR.modalDialog_Click_StubCapturedAsCheck();
		DataCompletion.modalDialog_Click_SelectReason_Button();
		DataCompletion.dataComplete_VerifyCPR_Doc_ConfirmDialogBoxMsg();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT3835_dataComplete_CPR_Doc_ClickNo_ConfirmDialogBox() throws Exception {
		DataCompletion.dataComplete_CPR_Doc_ClickNo_ConfirmDialogBox();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT3835_Verify_RepairDescription() throws Exception {
		String expectecBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchId"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchId", batch, expectecBatchId);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 51)
	public void PT3835_Verify_tranNun() throws Exception {

		String expectecTranNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum"));
		BaseUI.baseStringPartialCompare("dataComplete_TranNum", tranNun, expectecTranNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 52)
	public void PT3835_Verify_tranList() throws Exception {

		BaseUI.wait_forPageToFinishLoading();
		String expectecTranList = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranList"));
		BaseUI.baseStringPartialCompare("dataComplete_TranList", tranList, expectecTranList);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 53)
	public void PT3835_Verify_reasonMsg() throws Exception {

		String expectecReasonMsg = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ReasonMsg_PiggyBack"));
		BaseUI.baseStringPartialCompare("dataComplete_ReasonMsg_PiggyBack", reasonMsg, expectecReasonMsg);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 54)
	public void PT3835_Verify_JobItem() throws Exception {

		DataCompletion_CPR.verify_JobItem(clientID, batch, date, jobId, docGrp);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT3835_Step8_Verify_SeqNum4_IsPresent() throws Exception {

		String tranNum = "3";
		DataCompletion_CPR.click_BatchBy_BatchNumAndTran_Img4(batch, tranNum);

		String NewSeqNum3 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Pass1SequenceNumber"));
		BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum3, NewSeqNum3);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 65)
	public void PT3835_Step8_Verify_Trans_Values() throws Exception {

		DataCompletion_CPR.click_dataComplete_DocId_Img();

		String expectedtran1 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Tran1"));
		BaseUI.baseStringPartialCompare("dataComplete_Tran1", trans, expectedtran1);
	}
	
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 66)
		public void PT3835_Step8_Verify_TranNum_Values() throws Exception {
		String expectedtran2 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum2"));
		BaseUI.baseStringPartialCompare("dataComplete_TranNum2", transNum2, expectedtran2);
		}
		
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 66)
		public void PT3835_Step8_Verify_CheckCount_Values() throws Exception {
		String newChkCount6 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CheckCount"));
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount2, newChkCount6);
		
		}
		
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 66)
		public void PT3835_Step8_Verify_StubCount_Values() throws Exception {
		String stb0 = "0";
		String newStbCount1 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_StubCount0"));
		BaseUI.baseStringCompare("dataComplete_StubCount0", stb0, newStbCount1);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 71)
	public void PT3835_Verify_DocId_UpdatedTran_ValuesSeq2ChkCount() throws Exception {

		DataCompletion_CPR.select_dataComplete_DocIdDialogBox_Option(dialogBoxDocId);
		DataCompletion_CPR.dataComplete_DocId_DialogBox_OkBtn();
		String newChkCount = DataCompletion_CPR.return_CheckCount();
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount1, newChkCount);
	}
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 72)
		public void PT3835_Verify_DocId_UpdatedTran_ValuesSeq2StbCount() throws Exception {	
		
		String newStbCount = DataCompletion_CPR.return_StubCount();
		BaseUI.baseStringCompare("dataComplete_StubCount0", StbCount1, newStbCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 73)
	public void PT3835_Verify_SeqNum7_IsSelected() throws Exception {

		String tranNum = "7";
		DataCompletion_CPR.click_BatchBy_BatchNumAndTran_Img4(batch, tranNum);
		String NewSeqNum2 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Pass1SequenceNumber"));
		BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum7, NewSeqNum2);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 74)
	public void PT3835_Verify_DocId_UpdatedTran_ValuesSeq7() throws Exception {


		DataCompletion_CPR.Verify_DocId_UpdatedTran_ValuesSeq( dialogBoxDocId, chkCount1, StbCount1);	

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 75)
	public void PT3835_Step8_Verify_DocIdSeq3_3() throws Exception {

		DataCompletion_CPR.Verify_DocIdSeqsLabel(seqNum3row, seqLabel);
		

	}
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 75)
	public void PT3835_Step8_Verify_DocIdSeq3_7() throws Exception {
		
		DataCompletion_CPR.Verify_DocIdSeqsLabel(seqNum7row, seqLabel);

	}
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 76)
	public void PT3835_Verify_SeqNum11_IsSelected() throws Exception {

		String tranNum = "11";
		DataCompletion_CPR.click_BatchBy_BatchNumAndTran_Img4(batch, tranNum);
		String NewSeqNum2 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Pass1SequenceNumber"));
		BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", seqNum11, NewSeqNum2);
		
	}
	
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 77)
		public void PT3835_Verify_DocId_UpdatedTran_ValuesSeq11() throws Exception {
		
		DataCompletion_CPR.Verify_DocId_UpdatedTran_ValuesSeq( dialogBoxDocId, chkCount1, StbCount1);
		

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 81)
	public void PT3835_Step8_Verify_DocIdSwq11() throws Exception {

		
		DataCompletion_CPR.Verify_DocIdSeqsLabel(seqNum11row, seqLabel);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 200)
	public void PT3835_Step11_Verify_CheckIn_UpdateBatch() throws Exception {

		DataCompletion_CPR.verify_CheckIn_UpdateBatch();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 210)
	public void PT3835_Verify_CheckIn_CloseDialog() throws Exception {

		DataCompletion_CPR.verify_CheckIn_CloseDialog();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 220)
	public void PT3835_CPR_Verify_SLC_Job_IsPresent() throws Exception {

		DataCompletion_CPR.clickOk_CheckIn_CloseDialog();
		String newJOb = "cca";
		DataCompletion.verify_CorrectJob(newJOb);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 221)
	public void PT3835_CPR_Verify_SeqNum_IsPresent() throws Exception {

		String expextedSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
		BaseUI.baseStringCompare("dataComplete_DOC_first_Seq", seqNum2, expextedSeqNum);
		
	}
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 222)
		public void PT3835_CPR_Verify_DoneDialogBox() throws Exception {
			
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 234)
	public void PT3835_DataCompletionPage_KeyBatch_Information() throws Exception {
		
		
		Navigation.enter_Text_Into_CommandBox_AndNavigate("Batch " + batch);

		String expectedchkTotal = DataCompletion_CPR.return_CountInfo_checkTotal();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ChkTotal", checkTotal, expectedchkTotal);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_ItemCount() throws Exception {

		String expectedItemCount = DataCompletion_CPR.return_CountInfo_ItemCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ItemCount", itemCount, expectedItemCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_sourceItemCount() throws Exception {

		String expectedsourceItemCount = DataCompletion_CPR.return_CountInfo_SourceItemCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_sourceItemCount", sourceItemCount, expectedsourceItemCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_checCount() throws Exception {

		String expectedchecCount = DataCompletion_CPR.return_CountInfo_ChecCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ChkCount", checCount, expectedchecCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_rejectCheckCount() throws Exception {

		String expectedrejectCheckCount = DataCompletion_CPR.return_CountInfo_RejectCheckCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_rejectCheckCount", rejectCheckCount, expectedrejectCheckCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_rejectStubCount() throws Exception {

		String expectedrejectStubCount = DataCompletion_CPR.return_CountInfo_RejectStubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_rejectStubCount", rejectStubCount, expectedrejectStubCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_stubCount() throws Exception {

		String expectedstubCount = DataCompletion_CPR.return_CountInfo_StubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_stubCount", stubCount, expectedstubCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_StubTotal() throws Exception {

		String expectedstubTotal = DataCompletion_CPR.return_CountInfo_StubTotal();
		BaseUI.baseStringCompare("dataMaint_CountInfo_stubTotal", stubTotal, expectedstubTotal);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_TranCount() throws Exception {

		String expectedtranCount = DataCompletion_CPR.return_CountInfo_TranCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", tranCount, expectedtranCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_SourceTranCount() throws Exception {

		String expectedsourceTranCount = DataCompletion_CPR.return_CountInfo_SourceTranCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", sourceTranCount, expectedsourceTranCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 235)
	public void PT3835_DataCompletionPage_CountInfo_GoodBalanceStubCount() throws Exception {

		String expectedGoodBalanceStubCount = DataCompletion_CPR.return_CountInfo_GoodBalanceStubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", GoodBalanceStubCount, expectedGoodBalanceStubCount);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}
