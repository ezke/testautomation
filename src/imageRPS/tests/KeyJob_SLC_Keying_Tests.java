package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_Scanline;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_SLC_Keying_Tests extends baseTest {

	
	String keyingJob = "slc";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String fieldName = "";
	String genericValue = "1";
	
	String clientID;
	String docID;
	
	Integer minValue;
	Integer maxValue;
	
	Integer index_Of_Field = null;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchStuff = Navigation.return_ConfigData();

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		batch = "700140";
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch.toString());
		DataCompletion.verify_CorrectJob(keyingJob);

		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion.return_DocID();
		jobInfo = batchStuff.return_Row_BasedOn_2MatchingFields("client_id", clientID, "doc_id", docID);
		maxValue = Integer.parseInt(jobInfo.get("field_len"));
		
		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3705_Key_Job_MaxValues() throws Exception {
		DataCompletion_Scanline.key_allFields_UntilDone(maxValue);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT3705_Key_Job_Confirm_Takes_To_sl2() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_CorrectJob("sl2");
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchStuff = null;
		jobInfo = null;
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
