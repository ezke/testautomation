package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;


public class DataCompletion_KeyingJobs_KeyCC3_Incorrectly extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private String seq = "2";
    int indexOfViewItemsSeq2;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702120, 702125);

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);

        if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
            DataCompletion.click_Yes_ToConfirm();
        }
    }


    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3792_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3792_CC2_KeyTo_CC3() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3792_CC3_VeifySeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
        DataCompletion_Balancing.verify_KeyingField_Empty();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3792_Batch_ViewItems_Veify_ItemJob_Value() throws Exception {

        HashMap<String, String> viewItemRowSeq2 = new HashMap<String, String>();
        String itemJobHeader = "Item Job";
        DataCompletion.end_DataCompletion();
        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch.click_ViewItems();
        indexOfViewItemsSeq2 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", seq);
        viewItemRowSeq2 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq2);
        BaseUI.baseStringPartialCompare(itemJobHeader, keyingJob, viewItemRowSeq2.get(itemJobHeader));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3792_Batch_ItemInfo_Veify_Sequence$5_FieldValue() throws Exception {

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        DataMaintenance_Batch_ViewItems.select_Row_ByIndex(indexOfViewItemsSeq2);
        DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
        TableData viewItemsTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
        BaseUI.verify_true_AndLog(viewItemsTable.data.size()> 0, "Found rows to test.", "Did not find rows to test.");
        HashMap<String, String> sequense$5Row = viewItemsTable.return_Row_BasedOn_1MatchingField("Type Sequence", "$ 5");
        BaseUI.verify_true_AndLog(sequense$5Row.get("Field Value").length()== 0, "Field value was empty", "Field value was Not empty");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3792_Verify_KeyCC3_Appears() throws Exception {

        DataMaintenance_Batch.click_GoBack();
        DataMaintenance_Batch.click_GoBack();
        DataMaintenance_Batch.click_KeyBatch();
        DataCompletion.verify_CorrectJob(keyingJob);
        DataCompletion_Balancing.verify_Seq_ByIndex(1, seq);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
