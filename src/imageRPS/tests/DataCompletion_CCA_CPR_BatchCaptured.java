package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_CPR;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_CCA_CPR_BatchCaptured extends baseTest {

	String keyingJob = "cca";

	String docType = "";
	String batch = "";
	String tranNun = "0";
	String tranList = "Transaction List";
	String reasonMsg = "Batch Captured as Wrong Client";
	String clientID;
	String docGrp = "1";
	String jobId = "CPR";
	String date = "02/03/2015";
	String newClientid1 = "405";
	String newDocGrp1 = "9";
	String newClientId = "405 - ClientSRTest405";
	String newDocGrp = "9 - Singles - Wait - Skip P2 - X9";
	String dialogBoxclientId = "20 - Bevent Insurance";
	String dialogBoxDocGrpId = "1 - Singles - Skip P2 - P&E to 77";
	String dialogBoxDocWorkType = "1 - Singles";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		batch = BaseUI.random_NumberAsString(700236, 700240);

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

		DataCompletion.verify_CorrectJob(keyingJob);
		clientID = DataCompletion.return_ClientID();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 130)
	public void PT3685_CCA_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 140)
	public void PT3837_click_RepairDescription_CaptureAsWrongClient() throws Exception {

		DataCompletion.modalDialog_Click_RepairDescription_CaptureAsWrongClient();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 141)
	public void PT3837_click_SelectReason_Button() throws Exception {

		DataCompletion.modalDialog_Click_SelectReason_Button();
		DataCompletion.modalDialog_RepairDescription_SendBatchTo_CPR();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 161)
	public void PT3837_Step8_Verify_RepairDescription() throws Exception {
		String expectecBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchId"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchId", batch, expectecBatchId);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 162)
	public void PT3837_Step8_Verify_tranNun() throws Exception {
		
		String expectecTranNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum"));
		BaseUI.baseStringPartialCompare("dataComplete_TranNum", tranNun, expectecTranNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 163)
	public void PT3837_Step8_Verify_tranList() throws Exception {
		
		BaseUI.wait_forPageToFinishLoading();
		String expectecTranList = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranList"));
		BaseUI.baseStringPartialCompare("dataComplete_TranList", tranList, expectecTranList);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 164)
	public void PT3837_Step8_Verify_reasonMsg() throws Exception {
		
		
		String expectecReasonMsg = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ReasonMsg"));
		BaseUI.baseStringPartialCompare("dataComplete_ReasonMsg", reasonMsg, expectecReasonMsg);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 165)
	public void PT3837_Step8_Verify_JobItem() throws Exception {
		
		DataCompletion_CPR.verify_JobItem(clientID, batch, date, jobId, docGrp);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 170)

	public void PT3837_Step8_Verify_NewBatch_IsPresent() throws Exception {
		
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_NewBatchPresent"));

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 175)
	public void PT3837_Step8_Verify_BatchTcket_IsSelected() throws Exception {
		BaseUI.wait_forPageToFinishLoading();
		DataCompletion_CPR.verify_BatchTcket_IsSelected();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 180)
	public void PT3837_Step9_Verify_EditBatch_IsPresent() throws Exception {
		
	
		DataCompletion_CPR.verify_EditBatch_IsPresent();
		DataCompletion_CPR.click_EditBatch_DialogBox();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 181)
	public void PT3837_Step9_Verify_EditBatch_DialogBox() throws Exception {
		
		DataCompletion_CPR.verify_EditBatch_DialogBox();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 185)
	public void PT3837_Step9_Verify_EditBatch_BatchId() throws Exception {
		
		String expectedbatchId = BaseUI
				.getTextFromInputBox(Locator.lookupElement("dataComplete_EditBatchDialogBox_BatchId"));
		BaseUI.baseStringPartialCompare("dataComplete_EditBatchDialogBox_BatchId", batch, expectedbatchId);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 185)
	public void PT3837_Step9_Verify_EditBatch_ClientId() throws Exception {
		
		String expectedClientId = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_EditBatchDialogBox_ClientId"));
		BaseUI.baseStringPartialCompare("dataComplete_EditBatchDialogBox_ClientId", dialogBoxclientId,
				expectedClientId);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 185)
	public void PT3837_Step9_Verify_EditBatch_DocGrpId() throws Exception {
		
		String expectedDocGrpId = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_EditBatchDialogBox_DocGrpId"));
		BaseUI.baseStringPartialCompare("dataComplete_EditBatchDialogBox_DocGrpId", dialogBoxDocGrpId,
				expectedDocGrpId);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 185)
	public void PT3837_Step9_Verify_EditBatch_WorkType() throws Exception {
		
		String expectedWorkType = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_EditBatchDialogBox_WorkType"));
		BaseUI.baseStringPartialCompare("dataComplete_EditBatchDialogBox_WorkType", dialogBoxDocWorkType,
				expectedWorkType);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 190)
	public void PT3837_Step9_10_EditBatchDialogBox_UpdateBatch() throws Exception {
		
		DataCompletion_CPR.editBatchDialogBox_UpdateBatch(newClientId, newDocGrp);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 195)
	public void PT3837_Step9_10_verify_UpdateBatch() throws Exception {
		
		DataCompletion_CPR.verify_UpdateBatch(newClientid1, newDocGrp1);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 200)
	public void PT3837_Step11_Verify_CheckIn_UpdateBatch() throws Exception {
		
		DataCompletion_CPR.verify_CheckIn_UpdateBatch();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 210)
	public void PT3837_Step11_Verify_CheckIn_CloseDialog() throws Exception {
	
		DataCompletion_CPR.verify_CheckIn_CloseDialog();


	}

//	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 211)
//	public void PT3837_Step11_clickOk_CheckIn_CloseDialog() throws Exception {
//		BaseUI.wait_forPageToFinishLoading();
//	//	DataCompletion_CPR.clickOk_CheckIn_CloseDialog();


//	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 215)
	public void PT3837_Step11_Verify_CheckIn_AlertDialog() throws Exception {
		
		
			
		DataCompletion_CPR.clickOk_CheckIn_AlertDialog();
		DataCompletion_CPR.verify_CheckIn_AlertDialog();

	}
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 216)
	public void PT3837_Step11_verify_DataCompletionPage_IsPresent() throws Exception {
		
	Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);
	
	}
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
			
		Browser.closeBrowser();
	}

}// End of Class
