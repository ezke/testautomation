package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class SessionMangement_Login_StartNewImageRPSSession extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 10)
    public void PT4412_TopMenuSecurity_Login_CheckDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto2_users, GlobalVariables.passvord_SMAuto2_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 20)
    public void PT4412_TopMenuSecurity_ValidateOpenNewLoginSession() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
        BaseUI.wait_for_PageSource_ToNoLongerContainText("You are currently logged in as", 1);
        LoginPage.login_DontNavigatePastPriorSession(GlobalVariables.entity, GlobalVariables.login_SMAuto2_users, GlobalVariables.passvord_SMAuto2_users);
        BaseUI.verify_true_AndLog(BaseUI.pageSourceContainsString("Click here to abort the existing session and start a new ImageRPS Session."), "Text found", "Text not found");
        BaseUI.verifyElementAppears(Locator.lookupElement("navigation_SessionEnded_Title"));
        BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_ExistingSession_StartNewSession"));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}//End of Class

