package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

public class DataMaintenance_BatchSearchResults_MoreFilterTests extends baseTest {

    String invalidlockBoxID = "99";
    String lockBoxID = "26";
    TableData searchResults;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);

        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 10)
    public void PT4450_DataMaint_BatchResults_Verify_LockBox_Invalid() throws Exception {
//      Step4-5
        String expectedText = "Unknown Lockbox ID: '99'";
        DataMaintenance_Batch_Filters.enter_LockBoxID_AndSearch(invalidlockBoxID);
        DataMaintenance_Batch_Filters.wait_ForAlert_ToAppear();
        String alertText = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_Alert_Text"));
        BaseUI.baseStringCompare("AlertText", expectedText, alertText);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 20)
    public void PT4462_DataMaint_BatchResults_Verify_Enter_ValidLockBox_And_CliendIDBox_Empty() throws Exception {

//      Step 5
        DataMaintenance_Batch_Filters.click_OK_IfAlertModalAppears();
        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.enter_LockBoxID(lockBoxID);
        String clientID = BaseUI
                .getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_clientID_TextBox"));
        BaseUI.verify_true_AndLog(BaseUI.stringEmpty(clientID), "ClientID was empty", "ClientID was Not empty");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 30)
    public void PT4462_DataMaint_BatchResults_Verify_LockBox_Valid_InSearchResultsPage() throws Exception {

//      Step6
        HashMap<String, String> tableRow;
        Integer rowIndex = 6;
        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.click_Launch_SelectClient_Modal();
        tableRow = DataMaintenance_Batch_Filters.return_Row_ByIndex(rowIndex);
        DataMaintenance_Batch_Filters.cancel_OutOf_Select_ClientModal();
        DataMaintenance_Batch_Filters.enter_LockBoxID( tableRow.get("Lockbox ID"));
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Lockbox ID", tableRow.get("Lockbox ID"));
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Lockbox ID",
                tableRow.get("Lockbox ID"));

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 40)
    public void PT4492_DataMaint_BatchResults_Verify_SourceWF() throws Exception {

//      Step4
        String[] jobs = {"dcm"};
        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.select_Select_Source_Jobs_FromModal(jobs);
        String sourceJob_Textbox_Value = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_WF_Textbox"));
        BaseUI.baseStringPartialCompare("Source Job", jobs[0], sourceJob_Textbox_Value);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 50)
    public void PT4492_DataMaint_BatchResults_Verify_SourceWF_SearchResultPage() throws Exception {

//      Step 5
        String[] jobs = {"dcm"};
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Source WF", jobs[0] );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValues("Source Job", jobs);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 60)
    public void PT4492_DataMaint_BatchResults_Verify_SourceWF_Updates_Pull() throws Exception {

//      Step 6-8
        String[] jobs = {"dcm", "pull"};
        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.select_Select_Source_Jobs_FromModal(jobs);
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Source WF", jobs[1] );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Source Job", jobs[1]);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 70)
    public void PT4492_DataMaint_BatchResults_Verify_SourceWF_Updates_Hold() throws Exception {

//      Step 9,11,12
        String[] jobs = {"pull", "hold"};
        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.select_Select_Source_Jobs_FromModal(jobs);
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Source WF", jobs[1] );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Source Job", jobs[1]);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 80)
    public void PT4508_DataMaint_BatchResults_Verify_WFComplete_NotComplete() throws Exception {

        String wfNotComplete= "1" ;
        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.select_WFComplete(DataMaintenance_Batch_Filters.WFComplete.NotComplete);
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("WF Complete", wfNotComplete );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_DoesNOTContain_Value("Keying Job", "don");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 90)
    public void PT4509_DataMaint_BatchResults_Verify_WFComplete_Complete() throws Exception {

        String wfComplete= "2" ;
        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.select_WFComplete(DataMaintenance_Batch_Filters.WFComplete.WFComplete);
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("WF Complete", wfComplete );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Keying Job", "don");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 100)
    public void PT4513_DataMaint_BatchResults_Verify_LockBOxID_With_LowClientIDValue() throws Exception {

        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.enter_ClientID("20");
        String locboxID = BaseUI
                .getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_lockboxID_TextBox"));
        BaseUI.baseStringCompare("LockBox ID", "202020",locboxID );

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 110)
    public void PT4513_DataMaint_BatchResults_Verify_LockBOxID_With_HighClientIDValue() throws Exception {

        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.enter_ClientID("2000");
        String locboxID = BaseUI
                .getTextFromInputBox(Locator.lookupElement("dataMaint_Filters_lockboxID_TextBox"));
        BaseUI.baseStringCompare("LockBox ID", "2000",locboxID );

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 120)
    public void PT4515_DataMaint_BatchResults_Verify_PurgeOK_NotReady() throws Exception {

        String purgeNotReady ="1";
        DataMaintenance_Batch_Filters.click_ResetFilters();
        DataMaintenance_Batch_Filters.select_PurgeOK(DataMaintenance_Batch_Filters.PurgeOK.NotReady);
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Purge OK", purgeNotReady );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Purge OK", "No");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_BatchFilter" }, priority = 130)
    public void PT4516_DataMaint_BatchResults_Verify_PurgeOK_Ready() throws Exception {

        String purgeNotReady ="2";
        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_Batch_Filters.select_PurgeOK(DataMaintenance_Batch_Filters.PurgeOK.Ready);
        DataMaintenance_Batch_Filters.click_Submit();
        DataMaintenance_Batch_SearchResults.verify_FilterText_ContainsFilter("Purge OK", purgeNotReady );
        DataMaintenance_Batch_SearchResults.verify_ColumnMatches_ExpectedValue("Purge OK", "Yes");

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
