package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.DataMaintenance_ItemSearch_Filters;
import imageRPS.pages.DataMaintenance_ItemSearch_Results;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class DataMaintenance_ItemSearch_FilterTests extends baseTest {

	// Integer count_WithoutFilters;
	TableData searchResults;
	//
	HashMap<String, String> searchValues = new HashMap<String, String>();

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		searchValues.put("Account Number", "1111*");
		searchValues.put("Dollar Amount", "10.00");
		searchValues.put("Doc ID", "26");
		searchValues.put("Client ID", "601");
		searchValues.put("Lockbox ID", "601601");
		searchValues.put("Doc Group", "2");
		searchValues.put("Work Type", "2");
		searchValues.put("Batch ID", "469291");
		searchValues.put("Stub/Check", "S");
		searchValues.put("Receive Date", "06/17/2015");
		searchValues.put("Process Date", "06/16/2015");
		searchValues.put("Batch Date", "06/17/2015");
		searchValues.put("Block#", "469291");

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Item Search");

		DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox_ForGivenFields(searchValues);
		DataMaintenance_ItemSearch_Filters.click_Submit();

		//TableData searchResults2 = DataMaintenance_ItemSearch_Results.return_SearchResults2();
		
		searchResults = DataMaintenance_ItemSearch_Results.return_SearchResults2();
		searchResults.remove_Character("$", "Applied Amount");
		searchResults.remove_Character("$", "Save Amount");
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_ClientID() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Client ID", searchValues.get("Client ID"),
				searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_BatchID() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Batch ID", searchValues.get("Batch ID"),
				searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT_4755_DataMaint_ItemSearchResults_BatchDate() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Batch Date", searchValues.get("Batch Date"),
				searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests" , "module_DataMaintenance_ItemSearch"})
	public void PT4755_DataMaint_ItemSearchResults_StubCheck() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Stub/Check", "Stb", searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT_4755_DataMaint_ItemSearchResults_AccountNumber() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_STARTWithString("Account Number",
				searchValues.get("Account Number").replace("*", ""), searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_AppliedAmount() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Applied Amount", searchValues.get("Dollar Amount"),
				searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_SavedAmount() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Save Amount", searchValues.get("Dollar Amount"),
				searchResults.data);
	}

	// These values should not change and if this test fails, ask Jonathan why
	// it failed.
	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_AmtField() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Amt Field", "4", searchResults.data);
	}

	// These values should not change and if this test fails, ask Jonathan why
	// it failed.
	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_AcctField() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("Acct Field", "1", searchResults.data);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" })
	public void PT4755_DataMaint_ItemSearchResults_VerifyFiltesrs() throws Exception {
		DataMaintenance_ItemSearch_Results.verify_Filter_InFilterResultsList("All Amount", "true");
		DataMaintenance_ItemSearch_Results.verify_Filters_InFilterResultsList(searchValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "module_DataMaintenance_ItemSearch" }, priority = 1)
	public void PT4797_DataMaint_ItemSearchResults_Verify_ResetErrorPopup() throws Exception {

		DataMaintenance_Batch_SearchResults.click_GoBack();
		DataMaintenance_ItemSearch_Filters.click_Reset();
		DataMaintenance_ItemSearch_Filters.click_Submit();
		DataMaintenance_ItemSearch_Filters.verify_Alert_DialogPresent();
	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementExists("dataMaint_BatchFilter_Link", null, null)) {
			DataMaintenance_Batch_SearchResults.click_BatchFilter_BreadcrumbLink();
			DataMaintenance_Batch_Filters.click_ResetFilters();
		}

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		Browser.closeBrowser();
	}

}// End of Class
