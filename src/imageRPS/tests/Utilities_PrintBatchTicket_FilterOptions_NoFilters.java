package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_PrintBatchTicket_FilterOptions_NoFilters extends baseTest {

	String clientID = "10";
	String lockboxID = "Shoptek2";
	String docGroup = "4";
	// Might switch to enum
	WorkType workType = WorkType.NSF;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_FiltersVisible_clientID() throws Exception {
		BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_clientID_Textbox"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_FiltersVisible_lockboxID() throws Exception {
		BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_lockboxID_Textbox"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_FiltersVisible_docGroup() throws Exception {
		BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_docGroup_Textbox"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_FiltersVisible_SubmitButton() throws Exception {
		BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_SubmitButton"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_FiltersVisible_ResetButton() throws Exception {
		BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_ResetButton"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3314_Utilities_PrintBatchTicketFilter_FiltersVisible_WorkType_DefaultValue() throws Exception {
		BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_workType_Dropdown"));
		BaseUI.verifySelectedItemInDropdown(Locator.lookupElement("util_PrintBatchFilter_workType_Dropdown"),
				WorkType.SelectWorkType.getValue());
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT3314_Utilities_PrintBatchTicketFilter_ClickSubmit_NoFilters() throws Exception {

		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(null, null, null, null);
		Utilities_PrintTicket_SearchResults.verify_FilterText_Empty();
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT3314_Utilities_PrintBatchTicket_NoFilter_SearchResults_SearchResults_Controls_Present_AndEnabledOrDisabled_Correctly() throws Exception {
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(true);
	}



	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT3314_Utilities_PrintBatchTicket_NoFilter_SearchResults_ColumnsDisplayed() throws Exception {
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		TableData results = Utilities_PrintTicket_SearchResults.return_SearchResults2();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Results in list.", "Did not find results in list.");
	}

	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
