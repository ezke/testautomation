package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.DataCompletion.ConfirmAction;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_AFE_KeyBatch extends baseTest {

	String keyingJobAbbreviation = "fbl";
	// TableData fbl_StubAndCheck_Data;
	TableData itemInfoTable;
	Integer index_Of_Item_ToReject;

	Integer previousNumberToFix = 0;
	Integer itemsThatWillbeFixed = 0;

	String batchID;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		batchID = BaseUI.random_NumberAsString(700276, 700280);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4228_AFE_Tran1_Seq2_Selected_ByDefault() {
		Integer expectedSeq = 2;
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4228_AFE_Tran1_Seq2_AutoUpdate_Defaults_ToToggled() {
		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4228_AFE_Tran1_Seq2_Cursor_Defaults_To_X1() {
		String fieldText = "X1";
		Integer expectedNumberItemsToFix = 3;
		Integer expectedFixCount = 17;
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedNumberItemsToFix, fieldText,
				expectedFixCount);
	}


	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 6)
	public void PT4228_AFE_Tran1_Seq2_KeyX1Field_X2HasFocus() throws Exception {
		String fieldLabelText = "X1";
		String textToEnter = "123";
		Integer expectedErrorCount = 2;
		String newTextBoxToHaveFocus = "X2";
		String expectedBalance = "$0.00";
		Integer expectedFixCount = 16;
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldLabelText, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedErrorCount, newTextBoxToHaveFocus,
				expectedFixCount);

		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedBalance);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4228_AFE_Tran1_Seq2_KeyX2Field_HasFocus() throws Exception {
		String fieldLabelText = "X2";
		String textToEnter = "456";
		Integer expectedErrorCount = 1;
		String expectedBalance = "$0.00";
		Integer expectedFixCount = 15;
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldLabelText, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedErrorCount, fieldToHaveFocus,
				expectedFixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedBalance);
	}



	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4228_AFE_Tran1_Seq3_CourtesyAmount_KeyThrough_NextSeqDisplayed() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToKey = "100";
		Integer expectedSeq = 3;
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToKey);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 16)
	public void PT4228_AFE_Tran1_Seq2_Courtesy_FixCount14() throws Exception {
		Integer expectedFixCount = 14;
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 17)
	public void PT4228_AFE_Tran1_Seq2_Courtesy_TransactionBalance() throws Exception {
		String expectedValue = "$-1.00";
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 17)
	public void PT4228_AFE_Tran1_Seq2_Courtesy_ChkAmount() throws Exception {
		String expectedValue = "$1.00";
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 17)
	public void PT4228_AFE_Tran1_Seq3_Courtesy_ChkAmount() throws Exception {
		String expectedField = "X1";
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(expectedField);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 18)
	public void PT4228_AFE_Tran1_Seq2_NoErrors() throws Exception {
		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(0);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4228_AFE_Tran1_Seq3_KeyX1_ValuesMatch() throws Exception {
		String fieldToKey = "X1";
		String textToEnter = "678";
		Integer fixCount = 13;
		Integer fieldsToCorrectCount = 2;
		String fieldToHaveFocus = "X2";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4228_AFE_Tran1_Seq3_KeyX2_ValuesMatch() throws Exception {
		String fieldToKey = "X2";
		String textToEnter = "890";
		Integer fixCount = 12;
		Integer fieldsToCorrectCount = 1;
		String fieldToHaveFocus = "Courtesy Amount ($1)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4228_AFE_Tran1_Seq3_KeyCourtesyAmount_TakesToNextSeq() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "200";
		Integer fixCount = 11;
		Integer fieldsToCorrectCount = 3;
		String fieldToHaveFocus = "X1";
		Integer expectedSeq = 4;
		String expectedChk = "$3.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT4228_AFE_Tran1_Seq3_ValuesAccurate_FromCompletion() throws Exception {
		// String fieldToKey = "Courtesy Amount ($1)";
		// String textToEnter = "200";
		Integer fieldsToCorrectCount = 0;
		Integer expectedSeq = 3;
		String expectedTranBalance = "$-3.00";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBalance);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
	}

	// Step 12
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT4228_AFE_Tran1_Seq4_KeyX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 10;
		String textToEnter = "100";
		Integer fieldsToCorrectCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 13
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT4228_AFE_Tran1_Seq4_KeyX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		Integer fixCount = 9;
		String textToEnter = "222";
		Integer fieldsToCorrectCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 14
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4228_AFE_Tran1_Seq4_KeyCourtesyAmount_TakesToNextSeq() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "300";
		Integer fixCount = 8;
		Integer fieldsToCorrectCount = 8;
		String fieldToHaveFocus = "A1";
		Integer expectedSeq = 1;
		String expectedChk = "$6.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
	}

	// Step 14
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT4228_AFE_Tran1_Seq4_ValuesAccurate_FromCompletion() throws Exception {
//		String fieldToKey = "Courtesy Amount ($1)";
//		String textToEnter = "200";
		Integer fieldsToCorrectCount = 0;
		Integer expectedSeq = 4;
		String expectedTranBalance = "$-6.00";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBalance);
	}

	// Step 15
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 43)
	public void PT4228_AFE_Tran1_Seq1_KeyA1() throws Exception {
		String fieldToKey = "A1";
		String fieldToHaveFocus = "A7";
		Integer fixCount = 7;
		String textToEnter = "1234567890";
		Integer fieldsToCorrectCount = 7;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 16
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 45)
	public void PT4228_AFE_Tran1_Seq1_KeyA7() throws Exception {
		String fieldToKey = "A7";
		String fieldToHaveFocus = "A10";
		Integer fixCount = 6;
		String textToEnter = "";
		Integer fieldsToCorrectCount = 6;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 16
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 46)
	public void PT4228_AFE_Tran1_Seq1_KeyA10() throws Exception {
		String fieldToKey = "A10";
		String fieldToHaveFocus = "X1";
		Integer fixCount = 5;
		String textToEnter = "";
		Integer fieldsToCorrectCount = 5;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 18
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT4228_AFE_Tran1_Seq1_KeX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 4;
		String textToEnter = "333";
		Integer fieldsToCorrectCount = 4;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT4228_AFE_Tran1_Seq1_KeX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "O1";
		Integer fixCount = 3;
		String textToEnter = "444";
		Integer fieldsToCorrectCount = 3;

		// DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 20
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT4228_AFE_Tran1_Seq1_KeyO1() throws Exception {
		String fieldToKey = "O1";
		String fieldToHaveFocus = "O2";
		Integer fixCount = 2;
		String textToEnter = "1";
		Integer fieldsToCorrectCount = 2;

		// DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 21
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 65)
	public void PT4228_AFE_Tran1_Seq1_KeyO2() throws Exception {
		String fieldToKey = "O2";
		String fieldToHaveFocus = "Balance Amt";
		Integer fixCount = 1;
		String textToEnter = "2";
		Integer fieldsToCorrectCount = 1;

		// DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 22
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public void PT4228_AFE_Tran1_Seq1_BalanceAmt() throws Exception {
		String fieldToKey = "Balance Amt";
		String fieldToHaveFocus = "X1";

		Integer fixCount = 14;
		String textToEnter = "600";
		Integer fieldsToCorrectCount = 3;
		String currentTranBal = "$0.00";
		Integer currentSeq = 6;
		Integer currentTran = 2;
		String currentChk = "$0.00";
		String currentStb = "$0.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(currentTranBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(currentSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(currentChk);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(currentStb);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(currentTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 23
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 75)
	public void PT4228_AFE_Tran2_Seq6_KeyX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 13;
		String textToEnter = "111";
		Integer fieldsToCorrectCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 24
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
	public void PT4228_AFE_Tran2_Seq6_KeyX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "Courtesy Amount";
		Integer fixCount = 12;
		String textToEnter = "222";
		Integer fieldsToCorrectCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 25
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 85)
	public void PT4228_AFE_Tran2_Seq6_KeyCourtesyAmount_TakesToNextSeq() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "100";
		Integer fixCount = 11;
		Integer fieldsToCorrectCount = 3;
		String fieldToHaveFocus = "X1";
		Integer expectedSeq = 7;
		String expectedChk = "$1.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
	}

	// Step 25
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 86)
	public void PT4228_AFE_Tran2_Seq6_ValuesAccurate_FromCompletion() throws Exception {
		Integer fieldsToCorrectCount = 0;
		Integer expectedSeq = 6;
		String expectedTranBalance = "$-1.00";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBalance);
	}

	// Step 26
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 90)
	public void PT4228_AFE_Tran2_Seq7_KeyX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 10;
		String textToEnter = "111";
		Integer fieldsToCorrectCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 27
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 95)
	public void PT4228_AFE_Tran2_Seq7_KeyX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		Integer fixCount = 9;
		String textToEnter = "222";
		Integer fieldsToCorrectCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 28
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 100)
	public void PT4228_AFE_Tran2_Seq7_KeyCourtesyAmount_TakesToNextSeq() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "200";
		Integer fixCount = 8;
		Integer fieldsToCorrectCount = 8;
		String fieldToHaveFocus = "A1";
		Integer expectedSeq = 5;
		String expectedChk = "$3.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
	}

	// Step 28
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 101)
	public void PT4228_AFE_Tran2_Seq7_ValuesAccurate_FromCompletion() throws Exception {
		Integer fieldsToCorrectCount = 0;
		Integer expectedSeq = 7;
		String expectedTranBalance = "$-3.00";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBalance);
	}

	// Step 29
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 105)
	public void PT4228_AFE_Tran2_Seq5_KeyA1() throws Exception {
		String fieldToKey = "A1";
		String fieldToHaveFocus = "A7";
		Integer fixCount = 7;
		String textToEnter = "1234567890";
		Integer fieldsToCorrectCount = 7;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);

	}

	// Step 30
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 110)
	public void PT4228_AFE_Tran2_Seq5_KeyA7() throws Exception {
		String fieldToKey = "A7";
		String fieldToHaveFocus = "A10";
		Integer fixCount = 6;
		String textToEnter = "";
		Integer fieldsToCorrectCount = 6;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 31
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 115)
	public void PT4228_AFE_Tran2_Seq5_KeyA10() throws Exception {
		String fieldToKey = "A10";
		String fieldToHaveFocus = "X1";
		Integer fixCount = 5;
		String textToEnter = "";
		Integer fieldsToCorrectCount = 5;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 32
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 120)
	public void PT4228_AFE_Tran2_Seq5_KeyX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 4;
		String textToEnter = "333";
		Integer fieldsToCorrectCount = 4;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 33
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 125)
	public void PT4228_AFE_Tran2_Seq5_KeyX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "O1";
		Integer fixCount = 3;
		String textToEnter = "444";
		Integer fieldsToCorrectCount = 3;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 34
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 130)
	public void PT4228_AFE_Tran2_Seq5_KeyO1() throws Exception {
		String fieldToKey = "O1";
		String fieldToHaveFocus = "O2";
		Integer fixCount = 2;
		String textToEnter = "1";
		Integer fieldsToCorrectCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 35
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 135)
	public void PT4228_AFE_Tran2_Seq5_KeyO2() throws Exception {
		String fieldToKey = "O2";
		String fieldToHaveFocus = "Balance Amt";
		Integer fixCount = 1;
		String textToEnter = "2";
		Integer fieldsToCorrectCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 36
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 140)
	public void PT4228_AFE_Tran2_Seq5_BalanceAmt_NextTran() throws Exception {
		String fieldToKey = "Balance Amt";
		String fieldToHaveFocus = "X1";

		Integer fixCount = 11;
		String textToEnter = "300";
		Integer fieldsToCorrectCount = 3;
		String currentTranBal = "$0.00";
		Integer currentSeq = 9;
		Integer currentTran = 3;
		String currentChk = "$0.00";
		String currentStb = "$0.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(currentTranBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(currentSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(currentChk);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(currentStb);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(currentTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 37
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 145)
	public void PT4228_AFE_Tran3_Seq9_KeyX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 10;
		String textToEnter = "111";
		Integer fieldsToCorrectCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 38
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 145)
	public void PT4228_AFE_Tran3_Seq9_KeyX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "Courtesy Amount";
		Integer fixCount = 9;
		String textToEnter = "222";
		Integer fieldsToCorrectCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 39
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 150)
	public void PT4228_AFE_Tran3_Seq9_KeyCourtesyAmount_TakesToNextSeq() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "100";
		Integer fixCount = 8;
		Integer fieldsToCorrectCount = 8;
		String fieldToHaveFocus = "A1";
		Integer expectedSeq = 8;
		String expectedChk = "$1.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
	}

	// Step 39
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 151)
	public void PT4228_AFE_Tran3_Seq9_ValuesAccurate_FromCompletion() throws Exception {
		Integer fieldsToCorrectCount = 0;
		Integer expectedSeq = 9;
		String expectedTranBalance = "$-1.00";

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranBalance);
	}

	// Step 40
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 155)
	public void PT4228_AFE_Tran3_Seq8_KeyA1() throws Exception {
		String fieldToKey = "A1";
		String fieldToHaveFocus = "A7";
		Integer fixCount = 7;
		String textToEnter = "1234567890";
		Integer fieldsToCorrectCount = 7;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 41
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 166)
	public void PT4228_AFE_Tran3_Seq8_KeyA7() throws Exception {
		String fieldToKey = "A7";
		String fieldToHaveFocus = "A10";
		Integer fixCount = 6;
		String textToEnter = "";
		Integer fieldsToCorrectCount = 6;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 42
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 170)
	public void PT4228_AFE_Tran3_Seq8_KeyA10() throws Exception {
		String fieldToKey = "A10";
		String fieldToHaveFocus = "X1";
		Integer fixCount = 5;
		String textToEnter = "";
		Integer fieldsToCorrectCount = 5;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 43
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 175)
	public void PT4228_AFE_Tran3_Seq8_KeyX1() throws Exception {
		String fieldToKey = "X1";
		String fieldToHaveFocus = "X2";
		Integer fixCount = 4;
		String textToEnter = "333";
		Integer fieldsToCorrectCount = 4;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 44
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 180)
	public void PT4228_AFE_Tran3_Seq8_KeyX2() throws Exception {
		String fieldToKey = "X2";
		String fieldToHaveFocus = "O1";
		Integer fixCount = 3;
		String textToEnter = "555";
		Integer fieldsToCorrectCount = 3;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 45
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 185)
	public void PT4228_AFE_Tran3_Seq8_KeyO1() throws Exception {
		String fieldToKey = "O1";
		String fieldToHaveFocus = "O2";
		Integer fixCount = 2;
		String textToEnter = "1";
		Integer fieldsToCorrectCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 46
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 190)
	public void PT4228_AFE_Tran3_Seq8_KeyO2() throws Exception {
		String fieldToKey = "O2";
		String fieldToHaveFocus = "Balance Amt";
		Integer fixCount = 1;
		String textToEnter = "2";
		Integer fieldsToCorrectCount = 1;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(fieldsToCorrectCount, fieldToHaveFocus,
				fixCount);
	}

	// Step 47
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 195)
	public void PT4228_AFE_Tran3_Seq8_KeyBalanceAmt() throws Exception {
		String fieldToKey = "Balance Amt";
		Integer fixCount = 0;
		String textToEnter = "100";
		Integer fieldsToCorrectCount = 0;
		String tranBalance = "$0.00";
		String chkAmount = "$1.00";
		String stbAmount = "$1.00";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(tranBalance);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(chkAmount);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(stbAmount);
	}

	// Step 47
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 196)
	public void PT4228_AFE_ConfirmModalVisible() throws Exception {
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	// Step 48
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 197)
	public void PT4228_AFE_ConfirmModal_ClickYes_BringsUpAlert() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_AlertModal_Message("This batch is ready for pp2 and does not require keying.");
	}

	// Step 49
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 198)
	public void PT4228_AFE_AlertModal_Click_OK() throws Exception {
		DataCompletion.click_OK_on_Alert();
		DataCompletion.verify_DataCompletionPage_Loaded();

	}

	// Step 50
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 199)
	public void PT4228_AFE_P1SeqNumber1_AlertModal_verify_AuditInfo() throws Exception {

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_ViewItems();
		Integer rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				"1");
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();

		itemInfoTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
		String[] typeSequences_ForAuditInfoCheck = { "$ 5", "A 1", "X 1", "X 2" };

		DataMaintenance_ItemInfo.verify_AuditInfo_AFE(itemInfoTable,
				typeSequences_ForAuditInfoCheck);
	}

	// Step 50
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 200)
	public void PT4228_AFE_P1SeqNumber1_AlertModal_verify_FieldValues() throws Exception {

		HashMap<String, String> expected_FieldValues = new HashMap<String, String>();
		expected_FieldValues.put("$ 4", "");
		expected_FieldValues.put("$ 5", "6.00 ---> Applied");
		expected_FieldValues.put("A 1", "1234567890");
		expected_FieldValues.put("A 4", "12345");
		expected_FieldValues.put("A 7", "abc");
		expected_FieldValues.put("A10", "1");
		expected_FieldValues.put("O 1", "0.01");
		expected_FieldValues.put("O 2", "0.02");
		expected_FieldValues.put("S 1", "7505234438876021111120020004100005268");
		expected_FieldValues.put("SR1", "7505234438876021111120020004100005268");
		expected_FieldValues.put("X 1", "333");
		expected_FieldValues.put("X 2", "444");
		DataMaintenance_ItemInfo.verify_FieldValue(itemInfoTable, expected_FieldValues);

	}

	// Step 51
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 205)
	public void PT4228_AFE_P1SeqNumber2_AlertModal_verify_AuditInfo() throws Exception {

		DataMaintenance_ItemInfo.click_GoBack();
		Integer rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				"2");
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();

		itemInfoTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
		String[] typeSequences_ForAuditInfoCheck = { "$ 1", "X 1", "X 2" };

		DataMaintenance_ItemInfo.verify_AuditInfo_AFE(itemInfoTable,
				typeSequences_ForAuditInfoCheck);
	}

	// Step 51
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 206)
	public void PT4228_AFE_P1SeqNumber2_AlertModal_verify_FieldValues() throws Exception {

		HashMap<String, String> expected_FieldValues = new HashMap<String, String>();
		expected_FieldValues.put("$ 1", "1.00 ---> Applied");
		expected_FieldValues.put("$ 2", "");
		expected_FieldValues.put("A 1", "256073373");
		expected_FieldValues.put("A 2", "077010572365");
		expected_FieldValues.put("A 3", "1621");
		expected_FieldValues.put("A 4", "");
		expected_FieldValues.put("A 6", "");
		expected_FieldValues.put("S 4", "d256073373d077010572365c1621");
		expected_FieldValues.put("SR4", "d256073373d077010572365c1621");
		expected_FieldValues.put("X 1", "123");
		expected_FieldValues.put("X 2", "456");
		DataMaintenance_ItemInfo.verify_FieldValue(itemInfoTable, expected_FieldValues);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		itemInfoTable = null;
		Browser.closeBrowser();
	}
}
