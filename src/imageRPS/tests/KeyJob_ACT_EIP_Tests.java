package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Account;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_ACT_EIP_Tests extends baseTest {

	
	String keyingJob = "act";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String fieldName = "";
	String genericValue = "1";
	
	String clientID;
	String docID;
	
	Integer minValue;
	Integer maxValue;
	
	Integer index_Of_Field = null;
	TableData batchStuff;
	TableData docInfo;
	HashMap<String, String> jobInfo;
	String valueToKey = "12345";
	String expectedMessage = "Reject item because of EIP?\nField Value: 12345\nField Name: Account Number 2\n12345 was not accepted. It was found in EIP file: dummy2";
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchStuff = Navigation.return_ConfigData();
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		//Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		//369101-369110
		batch = BaseUI.random_NumberAsString(369101, 369110);
		
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch.toString());
		DataCompletion.verify_CorrectJob(keyingJob);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3680_ACT_EIP_Key_Job_MaxValues() throws Exception {
//		Reject item because of EIP?
//				Field Value: 12345
//				Field Name: Account Number 2
//				12345 was not accepted. It was found in EIP file: dummy2
		
		
		
		DataCompletion_Account.enter_Text_IntoKeyingField(valueToKey, true);
		BaseUI.waitForElementToBeDisplayed("dataComplete_Confirm_Modal", null, null);
		DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT3680_ACT_EIP_Click_No_ToCancel() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
		docInfo = DataCompletion_Account.return_document_Info();
		
		BaseUI.verify_true_AndLog(docInfo.data.size() == 1, "One row returned.", "Row count was off.");
		BaseUI.baseStringCompare("Rej", "---", docInfo.data.get(0).get("Rej"));
		BaseUI.baseStringCompare("Field", valueToKey, docInfo.data.get(0).get("Field"));
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT3680_ACT_EIP_YesToConfirm_RejectsFirstLine() throws Exception {
		DataCompletion_Account.enter_Text_IntoKeyingField(valueToKey, true);
		BaseUI.waitForElementToBeDisplayed("dataComplete_Confirm_Modal", null, null);
		DataCompletion.click_Yes_ToConfirm();
		docInfo = DataCompletion_Account.return_document_Info();
		
		BaseUI.verify_true_AndLog(docInfo.data.size() == 2, "Two rows returned.", "Row count was off.");
		BaseUI.baseStringCompare("Rej", keyingJob, docInfo.data.get(0).get("Rej"));
		BaseUI.baseStringCompare("Field", valueToKey, docInfo.data.get(0).get("Field"));		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT3680_ACT_EIP_Key_Job_Done_Yes_toUnlockJob() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchStuff = null;
		docInfo = null;
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
