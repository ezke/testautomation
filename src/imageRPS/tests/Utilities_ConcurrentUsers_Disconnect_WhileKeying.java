package imageRPS.tests;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch.collapsible_SectionName;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities;
import imageRPS.pages.Utilities_ConcurrentUsers;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_ConcurrentUsers_Disconnect_WhileKeying extends baseTest {

	WebDriver firstUser;
	WebDriver secondUser;
	TableData availableUsers;
	TableData searchResults;

	String batchID;
	String userName2_ExpectedValue = "autote_1";
	String timeStamp;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		firstUser = Browser.driver;

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.SecondLoginName, GlobalVariables.SecondLoginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		// User starts keying job.
		DataCompletion.click_Abbreviation("act");
		batchID = DataCompletion.return_BatchID();
		//DataCompletion.lock_Job();
		secondUser = Browser.driver;

		Browser.changeDriver(firstUser, Browser.currentBrowser);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities.navigate_ConcurrentUsers();

		availableUsers = Utilities_ConcurrentUsers.return_userList();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3391_Utilities_ConcurrentUsers_firstUser_And_SecondUser_InTable() throws Exception {
		availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.return_UserName_Formatted());
		availableUsers.verify_Value_InColumn("Operator ID", userName2_ExpectedValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 2)
	public void PT3391_Utilities_ConcurrentUsers_secondUser_Disconnects_FirstUser_TableUpdates() throws Exception {
		// Browser.changeDriver(firstUser, Browser.currentBrowser);
		timeStamp = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy hh:mm a").toLowerCase();
		Utilities_ConcurrentUsers.disconnect_User(userName2_ExpectedValue, availableUsers);
		availableUsers = Utilities_ConcurrentUsers.return_userList();
		availableUsers.verify_Value_NOT_InColumn("Operator ID", userName2_ExpectedValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 3)
	public void PT3391_Utilities_ConcurrentUsers_secondUser_Disconnects_FirstUser_FirstSession_Disconnected()
			throws Exception {
		Browser.changeDriver(secondUser, Browser.currentBrowser);
		Navigation.wait_ForSessionEnded();
		Navigation.verify_UserDisconnected();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 4)
	public void PT3391_Verify_BatchLocked() throws Exception {
		Browser.changeDriver(firstUser, Browser.currentBrowser);

		// Navigate to Batch ID from Setup
		// Verify it is locked and accurate.
		Navigation.navigate_to_Batch_ViaCommand(batchID);

		DataMaintenance_Batch.expand_Section(collapsible_SectionName.BatchStatus);
		DataMaintenance_Batch.verify_LockStatus_Locked(userName2_ExpectedValue, "Job: act", timeStamp);


	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3391_Verify_UnlockBatch_AndVerify_Unlocked() throws Exception {
		DataMaintenance_Batch.unlockBatch();
		DataMaintenance_Batch.verify_LockStatus_NOT_Locked();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			availableUsers = null;
			searchResults = null;

			Browser.changeDriver(firstUser, Browser.currentBrowser);
			Browser.closeBrowser();
		} finally {
			Browser.changeDriver(secondUser, Browser.currentBrowser);
			Browser.closeBrowser();
		}
	}

}// End of Class
