package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Balancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_SCA_Reject_UnReject extends baseTest {

	String keyingJob = "sca";

	String batch = "";
	String timestamp = "";

	Integer maxValue;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	String clientID;
	String docID;
	// secondSeq will be rejected and then unrejected.
	String secondSeq;
	String firstSeq;
	String thirdSeq;

	Integer index_Of_Field = null;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batch = "700171";

		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

		// Key batch through to SCA.
		DataCompletion_Balancing.key_through_to_SCA("1");

		DataCompletion.verify_CorrectJob(keyingJob);

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 5)
	public void PT3667_SCA_KeyToSecondSeq() throws Exception {
		DataCompletion_Balancing.enterText_Into_LastInputBox("1");
		DataCompletion_Balancing.enterText_Into_LastInputBox("1");

		firstSeq = DataCompletion_Balancing.return_Seq_ByIndex(1);
		secondSeq = DataCompletion_Balancing.return_Seq_ByIndex(2);
		thirdSeq = DataCompletion_Balancing.return_Seq_ByIndex(3);

		DataCompletion_Balancing.select_InputBox_ByIndex(2);
		DataCompletion.reject_Item();

		DataCompletion_Balancing.verify_NOT_Rejected(1);
		DataCompletion_Balancing.verify_Rejected(keyingJob, 2);
		DataCompletion_Balancing.verify_NOT_Rejected(3);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT3667_SCA_UnRejectItem() throws Exception {
		DataCompletion_Balancing.select_InputBox_ByIndex(2);
		DataCompletion.click_reject();

		DataCompletion_Balancing.verify_NOT_Rejected(1);
		DataCompletion_Balancing.verify_NOT_Rejected(2);
		DataCompletion_Balancing.verify_NOT_Rejected(3);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT3667_SCA_RejectedUnRejected_Item_AuditTrail() throws Exception {
		String header = "Audit Trail";
		Integer rowIndex = null;
		HashMap<String, String> viewItemRow = new HashMap<String, String>();

		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();

		Navigation.navigate_to_Batch_ViaCommand(batch);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", secondSeq);
		// DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		viewItemRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(rowIndex);
		BaseUI.baseStringPartialCompare(header,
				"unReject,sca," + GlobalVariables.return_UserName_Formatted() + ",,Reject,sca," + GlobalVariables.return_UserName_Formatted() + ",,",
				viewItemRow.get(header));

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			batchStuff = null;
			jobInfo = null;
			Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
			DataCompletion.end_DataCompletion();
		} finally {
			Browser.closeBrowser();
		}
	}

}// End of Class
