package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_AFE_Rej_UnRej extends baseTest {

	String batchID;
	Integer rowIndex;
	TableData auditInfo;
	TableData expectedTable;
	String expectedUsername;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		expectedUsername = GlobalVariables.loginName.length() >= 8 ? GlobalVariables.loginName.substring(0, 8)
				: GlobalVariables.loginName;
		batchID = BaseUI.random_NumberAsString(700306, 700310);

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_KeyBatch();
		DataCompletion.verify_CorrectJob("afe");

	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4178_AFE_Tran1_VerifyDefaults() {
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		Integer expectedFixCount = 7;
		Integer expectedErrorCount = 1;
		Integer expectedSeq = 2;
		Integer expectedTran = 1;

		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedErrorCount,
				fieldToHaveFocus, expectedFixCount);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4178_AFE_Tran1_ClickRej_VerifyRejModalAppears() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
		DataCompletion.verify_RejectModal_SelectReasonButton_Enabled_OR_Disabled(false);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4178_AFE_Tran1_RejModal_SelectAReason_SelectReasonButtonEnabled() throws Exception {
		String rejectReason = "test";

		DataCompletion.select_RejectReason_Default();
		DataCompletion.select_RejectReason(rejectReason);
		DataCompletion.verify_RejectModal_SelectReasonButton_Enabled_OR_Disabled(true);
	}

	// Steps 8-10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4178_AFE_Tran1_RejModal_ClickSelectReason_ModalDisappears() throws Exception {
		Integer expectedFixCount = 6;
		Integer expectedTranNumber = 1;
		Integer expectedSeq = 3;

		DataCompletion.click_SelectReason_ForReject();
		DataCompletion.verify_Reject_Modal_NOT_Visible();
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTranNumber);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_WasRejected_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(4);
	}

	// Steps 8-10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4178_AFE_Tran1_UnReject_FirstSeq() throws Exception {
		Integer expectedFixCount = 7;

		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(1);
		DataCompletion.click_reject();
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(1);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(2);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(3);
		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(4);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4178_AFE_Reject_Unreject_Item_AuditTrail() throws Exception {
		String itemInfoHeader = "Item Audit Trail";
		expectedTable = DataMaintenance_ItemInfo.return_PT4178_ExpectedTestData(expectedUsername, itemInfoHeader);

		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();

		Navigation.enter_Text_Into_CommandBox_AndNavigate("batch " + batchID);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");

		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		DataMaintenance_ItemInfo.expandSectionHeader_ByText(itemInfoHeader);
		auditInfo = DataMaintenance_ItemInfo.return_ItemAuditTrail_Table();

		for (Integer i = 0; i < expectedTable.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), expectedTable.data.get(i), auditInfo.data.get(i));
		}

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			auditInfo = null;
			expectedTable = null;
		} finally {
			Browser.closeBrowser();
		}
	}
}
