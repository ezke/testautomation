package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class DataCompletion_KetingJobs_KeyCC3_Incorrectly_Rekey_Accept extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private int indexOfViewItemsSeq2;
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702223, 702225);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3772_Step6_To_7_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3772_Step8_To_9_CC2_KeyTo_CC3() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3772_Step10_CC3_VeifySeq() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3772_Step11_CC3_VeifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3772_Step12_To_13_CC3_Rekey_VerifySequence() throws Exception {

        DataCompletion.click_ConfirmModal_Rekey_Button();
        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3772_Step14_CC3_Rekey_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3772_Step15_CC3_Accept_VerifySequence() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3772_Step16_To_18_CC3_Accept_VerifyViewItems() throws Exception {

        DataCompletion.end_DataCompletion();
        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        indexOfViewItemsSeq2 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");
        DataMaintenance_Batch_ViewItems.select_Row_ByIndex(indexOfViewItemsSeq2);
        DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
        TableData viewItemsTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
        BaseUI.verify_true_AndLog(viewItemsTable.data.size()> 0, "Found rows to test.", "Did not find rows to test.");
        HashMap<String, String> sequense$5Row = viewItemsTable.return_Row_BasedOn_1MatchingField("Type Sequence", "$ 5");
        BaseUI.baseStringCompare("$5 Field Value", "0.03 ---> Applied", sequense$5Row.get("Field Value"));

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}
