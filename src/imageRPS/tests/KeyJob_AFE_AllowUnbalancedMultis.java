package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_AFE_AllowUnbalancedMultis extends baseTest {

	String batchID;
	Integer rowIndex;
	TableData auditInfo;
	TableData expectedTable;
	String expectedUsername;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		expectedUsername = GlobalVariables.loginName.length() >= 8 ? GlobalVariables.loginName.substring(0, 8)
				: GlobalVariables.loginName;
		batchID = BaseUI.random_NumberAsString(700291, 700295);
		//batchID = "700292";

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_KeyBatch();
		DataCompletion.verify_CorrectJob("afe");

	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4179_AFE_Tran1_VerifyDefaults() {
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		Integer expectedFixCount = 7;
		Integer expectedErrorCount = 1;
		Integer expectedSeq = 2;
		Integer expectedTran = 1;

		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedErrorCount,
				fieldToHaveFocus, expectedFixCount);
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4179_AFE_Tran1_KeyToSeq3() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "100";

		String expectedTranTotal = "$-1.00";
		String expectedChkTotal = "$1.00";
		Integer expectedSeq = 3;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBoxFocus_TranBal_CurrentChk_SelectedSeq(fieldToKey,
				expectedTranTotal, expectedChkTotal, expectedSeq);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4179_AFE_Tran1_KeyToSeq4() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "100";

		String expectedTranTotal = "$-2.00";
		String expectedChkTotal = "$2.00";
		Integer expectedSeq = 4;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBoxFocus_TranBal_CurrentChk_SelectedSeq(fieldToKey,
				expectedTranTotal, expectedChkTotal, expectedSeq);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4179_AFE_Tran1_KeyToSeq1() throws Exception {
		String fieldToKey = "Courtesy Amount ($1)";
		String textToEnter = "100";

		String fieldToHaveFocus = "Account Number (A1)";
		String expectedTranTotal = "$-3.00";
		String expectedChkTotal = "$3.00";
		Integer expectedSeq = 1;

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBoxFocus_TranBal_CurrentChk_SelectedSeq(fieldToHaveFocus,
				expectedTranTotal, expectedChkTotal, expectedSeq);
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4179_AFE_Tran1_KeyToSeq1_A1_O1_and_O2() throws Exception {
		String a1Text = "123456789";
		String o1Text = "1";
		String o2Text = "2";
		Integer expectedErrors = 1;
		Integer expectedFix = 1;
		String fieldToHaveFocus = "Dol 5 ($5)";

		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField("A1", a1Text);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField("O1", o1Text);
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField("O2", o2Text);

		DataCompletion_fbl_AllFieldsBalancing.verify_Field_HasFocus_AndFix_AndError_CountsCorrect(expectedErrors,
				fieldToHaveFocus, expectedFix);

	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4179_AFE_Tran1_KeySeq1_ToTran2() throws Exception {
		String fieldToKey = "Dol 5 ($5)";
		String textToEnter = "100";
		Integer expectedTran = 2;
		String fieldToHaveFocus = "Courtesy Amount ($1)";
		DataCompletion_fbl_AllFieldsBalancing.enterTextIntoField(fieldToKey, textToEnter);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT4179_AFE_Tran1_ValuesMatchExpected() throws Exception {
		String expectedTran = "$-2.00";
		String expectedChk = "$3.00";
		String expectedStb = "$1.00";
		Integer expectedFixCount = 0;

		DataCompletion_fbl_AllFieldsBalancing.expand_TransactionList();
		DataCompletion_fbl_AllFieldsBalancing.click_Tran_InTranList_ByTranNubmer(1);

		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTran);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChk);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(expectedStb);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			auditInfo = null;
			expectedTable = null;

			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();

		} finally {
			Browser.closeBrowser();
		}
	}
}
