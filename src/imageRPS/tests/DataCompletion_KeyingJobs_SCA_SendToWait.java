package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Balancing;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_SCA_SendToWait extends baseTest{

	private String keyingJob = "cca";
    private String batch = "";
    
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702126, 702130);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3676_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2000");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3676_CC2_KeyTo_CC3() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2000");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "sca";
        DataCompletion.verify_CorrectJob(keyingJob);
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "11");
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3676_CC3_KeyTo_SCA() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2000");
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message("This batch is ready for wait and does not require keying.");
    }
    
    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3676_Verify_DataCompletionPage_Loads() throws Exception {

    	DataCompletion.click_OK_on_Alert();
    	DataCompletion.verify_DataCompletionPage_Loaded();
    }
    
    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
    
}
