package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class TopMenuSecurity_LoginTests extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT4334_Login_DashboardOnly() throws Exception {
		LoginPage.login(GlobalVariables.entity, GlobalVariables.login_autotest_dashonly,
				GlobalVariables.password_autotest_dashonly);
		Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT4340_Login_DataCompletion() throws Exception {
		LoginPage.login(GlobalVariables.entity, GlobalVariables.login_autotest_datacomponly,
				GlobalVariables.password_autotest_datacomponly);
		Navigation.verify_Only_ProvidedHeader_Appears("Data Completion");
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT4344_Login_AlertsEvents() throws Exception {
		LoginPage.login(GlobalVariables.entity, GlobalVariables.login_autotest_alrtonly,
				GlobalVariables.password_autotest_alrtonly);
		Navigation.verify_Only_ProvidedHeader_Appears("Alerts/Events");
	}

	@AfterMethod(alwaysRun = true)
	public void testTearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementAppears(Locator.lookupElement("login_UnableToLogIn_Error"))) {
			Navigation.clickOK_ForErrorModal();
		}

		if (BaseUI.elementAppears(Locator.lookupElement("navigate_SignOut"))) {
			Navigation.signOut();
		}

		Browser.navigateTo(GlobalVariables.baseURL);
	}

	@AfterClass(alwaysRun = true)
	public void finalTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
