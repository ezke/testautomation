package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Balancing;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.DataMaintenance_Consolidations_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Dashboard_Consolidations_List_Tests extends baseTest {

	TableData consolCutoffTimeList_old;
	HashMap<String, String> oldRow;
	HashMap<String, String> newRow;
	TableData consolCutoffTimeList_new;
	// 700353-700357
	String batchNumber = "700353";
	String cutoffTimeToSelect = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchNumber = BaseUI.random_NumberAsString(700353, 700357);
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		cutoffTimeToSelect = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy") + " 7:00pm";
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3421_Dashboard_ToggleCutoffs_ListOn() throws Exception {
		Dashboard.toggle_ConsolidationsByCutoff_List(true);
		Dashboard.verify_ConsolidationsByCutoffTime_ListButton_ToggledOn();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3421_Dashboard_AbleToDoubleClick_AndNavigateThrough_ToBatch() throws Exception {
		String clientID = "605";
		consolCutoffTimeList_old = Dashboard.return_ConsolidationsByCutoffTime_List_TableData();

		String expectedJob = "cc2";
		//Double clicking one of the lines takes us to Consolidation Results page.
		Dashboard.doubleclick_ConsolidationsByCutoffTime_Row_By_MatchingTime(consolCutoffTimeList_old,
				cutoffTimeToSelect);
		//These steps will takes us to a specific batch
		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ClientID(clientID);
		DataMaintenance_Consolidations_SearchResults.click_OpenBatches();
		//We'll pick the batch and then from there we'll key it.
		DataMaintenance_Batch_SearchResults.select_Row_ByBatchID(batchNumber);
		DataMaintenance_Batch_SearchResults.click_KeyBatch();

		DataCompletion.verify_CorrectJob(expectedJob);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3421_Key_Through_ToPP2() throws Exception {
		String valueToKey = "90000";

		DataCompletion_Balancing.key_through_Batch(valueToKey);
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_Balancing.key_through_Batch(valueToKey);
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_AlertModal_Message("This batch is ready for pp2 and does not require keying.");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT3421_Verify_ConsolidationCutoffs_List_Updated_CompletedItems() throws Exception {
		DataCompletion.click_OK_on_Alert();
		Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		//So after we get to Dashboard page, we need to wait a minute. I opted to initially refresh the chart,
		Dashboard.click_ConsolidationsByCutoff_RefreshButton();
		//Then I wait for the chart label to tell me that a minute has passed.
		Dashboard.wait_for_Consolidations_by_CutoffTime_ToBe_1Minute();
		//Then I refresh again and that should refresh the list so it's accurate and up to date.
		Dashboard.click_ConsolidationsByCutoff_RefreshButton();
		consolCutoffTimeList_new = Dashboard.return_ConsolidationsByCutoffTime_List_TableData();

		oldRow = consolCutoffTimeList_old.return_Row_BasedOn_1MatchingField("Time", cutoffTimeToSelect);
		newRow = consolCutoffTimeList_new.return_Row_BasedOn_1MatchingField("Time", cutoffTimeToSelect);

		Integer expected_completedItems = Integer.parseInt(oldRow.get("Completed Items")) + 10;
		Integer actual_completedItems = Integer.parseInt(newRow.get("Completed Items"));
		BaseUI.verify_true_AndLog(expected_completedItems.equals(actual_completedItems),
				"Actual Completed Items matches Expected Completed Items",
				"Expected " + expected_completedItems.toString() + " Completed Items, but seeing "
						+ actual_completedItems.toString() + " actual Completed Items.");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT3421_Verify_ConsolidationCutoffs_List_Updated_CompletedAmount() throws Exception {
		Double expected_CompletedAmount = Double.parseDouble(oldRow.get("Completed Amount")) + 18500.00;
		expected_CompletedAmount = BaseUI.round_Double_ToPassedInDecimalPlaces(expected_CompletedAmount, 2);
		Double actual_CompletedAmount = Double.parseDouble(newRow.get("Completed Amount"));

		BaseUI.verify_true_AndLog(expected_CompletedAmount.equals(actual_CompletedAmount),
				"Expected and actual Completed Amounts were equivalent.", "Expected Completed Amount of "
						+ expected_CompletedAmount.toString() + ", but seeing " + actual_CompletedAmount.toString());
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT3421_Verify_ConsolidationCutoffs_List_Updated_OpenItems() throws Exception {
		Integer expected_openItems = Integer.parseInt(oldRow.get("Open Items")) - 10;
		Integer actual_openItems = Integer.parseInt(newRow.get("Open Items"));
		BaseUI.verify_true_AndLog(expected_openItems.equals(actual_openItems),
				"Actual Open Items matches Expected Completed Items", "Expected " + expected_openItems.toString()
						+ " Open Items, but seeing " + actual_openItems.toString() + " actual Open Items.");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT3421_Verify_ConsolidationCutoffs_List_Updated_OpenAmount() throws Exception {
		Double expected_OpenAmount = Double.parseDouble(oldRow.get("Open Amount")) - 18500.00;
		expected_OpenAmount = BaseUI.round_Double_ToPassedInDecimalPlaces(expected_OpenAmount, 2);
		Double actual_OpenAmount = Double.parseDouble(newRow.get("Open Amount"));

		BaseUI.verify_true_AndLog(expected_OpenAmount.equals(actual_OpenAmount),
				"Expected and actual Open Amounts were equivalent.", "Expected Completed Amount of "
						+ expected_OpenAmount.toString() + ", but seeing " + actual_OpenAmount.toString());
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		consolCutoffTimeList_old = null;

		Browser.closeBrowser();
	}

}// End of Class
