package imageRPS.tests;

import imageRPS.data.baseTest;

import imageRPS.pages.*;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import r360.pages.AdvancedSearch;
import utils.*;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

public class TopMenuSecurity_MenuOptionsTests extends baseTest {

    @NotNull
    TableData searchResults;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);

    }

    @Test(groups = {"all_Tests", "regression_Tests"})
    public void PT4366_Command() throws Exception {
        LoginPage.login(GlobalVariables.entity, "Sect4366", GlobalVariables.loginPassword);
        BaseUI.click(Locator.lookupElement("navigation_Command_TextBox"));
        String[] knownValues = {"alerts graph", "alt . (Command Input)", "consol graph", "endsession", "jobs graph"};
        (Locator.lookupElement("navigation_Command_TextBox")).sendKeys(org.openqa.selenium.Keys.ARROW_DOWN);
        Thread.sleep(500);
        ArrayList<WebElement> elementsList = Locator.lookup_multipleElements("navigation_Command_TextBox_ListOptions", null, null);
        ArrayList<String> strValues = new ArrayList<>();

        for (WebElement element : elementsList) {
            strValues.add(element.getText());
        }

        for (String KnownValue : knownValues) {
            BaseUI.verify_true_AndLog(strValues.contains(KnownValue), "Value of '" + KnownValue + "' was found in the Command Dropdown", "Value of '" + KnownValue + "' was not found in the Command Dropdown");
        }
        Navigation.enter_Text_Into_CommandBox_AndNavigate("jobs graph");
        BaseUI.verifyElementHasExpectedText("dshbrd_ItemKeying_LastRefreshedLabel", "(a few seconds since last update)");

    }

    @Test(groups = {"all_Tests", "regression_Tests"})
    public void PT4365_Help() throws Exception {
        LoginPage.login(GlobalVariables.entity, "Sect4365", GlobalVariables.loginPassword);
        Help.verify_default_HelpPageLoaded();

    }

    //disabled due to test being incomplete
    @Test(groups = {"all_Tests", "regression_Tests"}, enabled = false)
    public void PT4364_Alerts_Events() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, "Sect4364", GlobalVariables.loginPassword);
        BaseUI.verify_true_AndLog(Navigation.isTabActive(Navigation.TopNavigationTab.AlertsEvents), "Alerts/Events page is loaded by default", "Alerts/Events page is not loaded by default");
        Navigation.verify_Only_ProvidedHeader_Appears("Alerts/Events");
        AlertsEvents.navigate_SubTab("Events");
        Events.click_Submit();
        searchResults = Events_SearchResults.return_SearchResults();
        ArrayList<String> baseRowData = (Events_SearchResults.get_RowData_ByIndex(0, searchResults));
        ArrayList<String> nextRowData = (Events_SearchResults.get_RowData_ByIndex(1, searchResults));

        BaseUI.click(Locator.lookupElement("eventsResults_SequenceHeader"));
        searchResults = Events_SearchResults.return_SearchResults();
        ArrayList<String> lastRowData = (Events_SearchResults.get_RowData_ByIndex(0, searchResults));
        BaseUI.click(Locator.lookupElement("eventsResults_SequenceHeader"));
        BaseUI.click(Locator.lookupElement("eventsResults_SequenceHeader"));
        BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Found Events.", "Did NOT find Events.");
        Events_SearchResults.select_FirstRow();

        BaseUI.click(Locator.lookupElement("events_DetailsButton"));
        Thread.sleep(2000);

        String headerSequenceText = (Locator.lookupElement("eventDetails_HeaderSequenceNumber").getAttribute("innerText"));
        System.out.println("header text= " + headerSequenceText);
        Events_SearchResults.validate_Events_Details(baseRowData);
        BaseUI.click(Locator.lookupElement("eventDetails_NextItemButton"));
        Events_SearchResults.validate_Events_Details(nextRowData);
        BaseUI.click(Locator.lookupElement("eventDetails_LastItemButton"));
        Events_SearchResults.validate_Events_Details(lastRowData);
        BaseUI.click(Locator.lookupElement("eventDetails_FirstItemButton"));
        Events_SearchResults.validate_Events_Details(baseRowData);


    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
        Navigation.signOut();
        Browser.closeBrowser();
        Browser.openBrowser(GlobalVariables.baseURL);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        Browser.closeBrowser();
    }
}





