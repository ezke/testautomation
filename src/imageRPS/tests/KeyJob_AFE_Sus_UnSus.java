package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.DataCompletion.ConfirmAction;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_AFE_Sus_UnSus extends baseTest {

	String batchID;
	Integer rowIndex;
	TableData auditInfo;
	TableData expectedTable;
	String expectedUsername;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		expectedUsername =  GlobalVariables.loginName.length() >= 8 ? GlobalVariables.loginName.substring(0, 8) :  GlobalVariables.loginName;
		batchID = BaseUI.random_NumberAsString(700281, 700285);

		Navigation.navigate_to_Batch_ViaCommand(batchID);
		DataMaintenance_Batch.click_KeyBatch();
		DataCompletion.verify_CorrectJob("afe");

	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT4202_AFE_Tran1_VerifyDefaults() {
		String fieldToHaveFocus = "Account Number (A2)";
		Integer expectedFixCount = 18;
		Integer expectedErrorCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.verify_AutoUpdate_Enabled();
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_SurroundedBy_PinkBorder(fieldToHaveFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(expectedFixCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(expectedErrorCount);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4202_AFE_Tran1_ClickSusp_VerifySuspModalAppears() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT4202_AFE_Tran1_SuspModal_Select_ThinClientSuspense_AndClick_SelectReasonButton_Tran2Active_AndModalDisappears()
			throws Exception {
		String suspenseReason = "Thin Client Suspense";
		Integer expectedTranNumber = 2;

		DataCompletion.select_SuspendReason_BySuspReasonText(suspenseReason);
		DataCompletion.click_Susp_SelectReason();
		DataCompletion.verify_Susp_Modal_NOT_Visible();
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(expectedTranNumber);
	}

	// Step 9
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4202_AFE_Tran_Dropdown_Tran1_Susp() throws Exception {
		Integer tranNumber = 1;

		DataCompletion_fbl_AllFieldsBalancing.expand_TransactionList();
		DataCompletion_fbl_AllFieldsBalancing.verify_Tran_Suspended_InList_ByTranNumber(tranNumber);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT4202_AFE_Tran_Dropdown_ClickTran1_WorkItems_Susp() throws Exception {
		Integer tranNumber = 1;
		Integer workItemCount = 2;

		DataCompletion_fbl_AllFieldsBalancing.click_Tran_InTranList_ByTranNubmer(tranNumber);
		DataCompletion_fbl_AllFieldsBalancing.verify_TransactionNumber(tranNumber);
		DataCompletion_fbl_AllFieldsBalancing.verify_AllItems_ForCurrentTran_Suspended(workItemCount);
	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4202_AFE_Tran_Dropdown_ClickTran1_WorkItems_Unsusp_ConfirmMessageAppears() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.UnSusp);
	}

	// Step 12
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT4202_AFE_Tran_Dropdown_ClickTran1_WorkItems_Unsusp_Confirm_ClickYes_WorkItems_Unsusp()
			throws Exception {
		Integer workItemCount = 2;
		Integer expectedErrorCount = 18;

		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_fbl_AllFieldsBalancing.verify_AllItems_ForCurrentTran_NOT_Suspended(workItemCount,
				expectedErrorCount);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 40)
	public void PT4202_AFE_SuspUnsusp_Item_AuditTrail() throws Exception {
		String itemInfoHeader = "Item Audit Trail";
		expectedTable = DataMaintenance_ItemInfo.return_PT4202_ExpectedTestData(expectedUsername, batchID);

	
		
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();

		Navigation.enter_Text_Into_CommandBox_AndNavigate("batch " + batchID);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");
		
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		DataMaintenance_ItemInfo.expandSectionHeader_ByText(itemInfoHeader);
		auditInfo = DataMaintenance_ItemInfo.return_ItemAuditTrail_Table();
		

		for (Integer i = 0; i < expectedTable.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), expectedTable.data.get(i), auditInfo.data.get(i));
		}


	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			auditInfo = null;
			expectedTable = null;
		} finally {
			Browser.closeBrowser();
		}
	}
}
