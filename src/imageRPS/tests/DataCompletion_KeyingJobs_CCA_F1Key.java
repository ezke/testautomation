package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_CCA_F1Key extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);

        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataCompletion);

        batch = BaseUI.random_NumberAsString(702241, 702245);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);

        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3830_Step7_To_16_CCA_Veify_Question_ModalBox_F1Key_Works() throws Exception {

        keyingJob = "cc2";
        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion_Scanline.pressF1_QuestionOrConfirm_Modal();
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Done_Confirm_YesButton"));
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
