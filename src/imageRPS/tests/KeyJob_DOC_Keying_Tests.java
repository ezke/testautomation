package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_DOC;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class KeyJob_DOC_Keying_Tests extends baseTest {

	
	String keyingJob = "doc";
	String batchID = "";
	String seqNumber ="";
	String timestamp = "";
	
	Integer index_Of_Field = null;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		batchID = BaseUI.random_NumberAsString(700220, 700221);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		
		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
	public void PT3679_DOC_KeyJob() throws Exception {
		seqNumber = DataCompletion_DOC.return_first_Seq();
		DataCompletion_DOC.key_through_DOC_With_FirstDocID();
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
	public void PT3679_DOC_KeyJob_ConfirmTakesYou_ToNewBatch() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_CorrectJob("slc");
		//DataCompletion.verify_BatchID_Changed(batchID);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 21)
	public void PT3679_DOC_Click_Done_ToUnlockBatch() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
	public void PT3679_DOC_KeyJob_ConfirmedJob_In_SLC() throws Exception {
		Integer rowIndex = null;
		
		Navigation.enter_Text_Into_CommandBox_AndNavigate("batch " + batchID);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", seqNumber);
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		
		DataMaintenance_ItemInfo.verify_CurrentJob_NOT_OldJob(keyingJob);

	}
	
		
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
