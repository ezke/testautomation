package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Account;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_ACT_Reject_UnReject extends baseTest {

	String keyingJob = "act";
	// 700154-700161
	// String batchID = "700152";
	String batch = "";
	String timestamp = "";

	// Integer maxValue;
	Integer minValue;
	TableData batchInfo;
	HashMap<String, String> jobInfo;

	TableData docInfo;

	String clientID;
	String docID;

	Integer index_Of_Field = null;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		batch = BaseUI.random_NumberAsString(700154, 700161);

		Browser.openBrowser(GlobalVariables.baseURL);
		batchInfo = Navigation.return_ConfigData();

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch.toString());
		DataCompletion.verify_CorrectJob(keyingJob);

		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion_Account.return_first_doc();

		jobInfo = batchInfo.return_Row_BasedOn_2MatchingFields("client_id", clientID, "doc_id", docID);
		// maxValue = Integer.parseInt(jobInfo.get("field_len"));

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3708_ACT_Verify_NothingRejected_Yet() throws Exception {
		// Should get us to the third row.
		minValue = DataCompletion_Account.return_CurrentMinValue(clientID, batchInfo);
		DataCompletion_Account.key_With_MinValue_GenericValue(minValue);
		DataCompletion_Account.key_With_MinValue_GenericValue(minValue);

		docInfo = DataCompletion_Account.return_document_Info();
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 0);
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 1);
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 2);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3708_ACT_RejectSecondRow_NoOtherRow_Rejected() throws Exception {
		DataCompletion_Account.click_On_KeyField_ByIndex(1);
		DataCompletion.reject_Item();

		// Refresh docInfo
		docInfo = DataCompletion_Account.return_document_Info();
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 0);
		DataCompletion_Account.verify_Row_Rejected(docInfo, 1);
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 2);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3708_ACT_UnReject_SecondRow() throws Exception {
		DataCompletion_Account.click_On_KeyField_ByIndex(1);
		DataCompletion.click_reject();

		docInfo = DataCompletion_Account.return_document_Info();
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 0);
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 1);
		DataCompletion_Account.verify_Row_NotRejected_OR_Susp(docInfo, 2);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT3708_ACT_RejectedUnRejected_Item_AuditTrail() throws Exception {
		String header = "Audit Trail";

		Integer rowIndex = null;
		HashMap<String, String> viewItemRow = new HashMap<String, String>();

		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();

		Navigation.navigate_to_Batch_ViaCommand(batch);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				docInfo.data.get(1).get("Seq"));
		viewItemRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(rowIndex);
		String expectedLoginName = GlobalVariables.return_UserName_Formatted();
		
		BaseUI.baseStringPartialCompare(header,
				"unReject,act," + expectedLoginName + ",,Reject,act," + expectedLoginName + ",,",
				viewItemRow.get(header));

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchInfo = null;
		jobInfo = null;
		docInfo = null;
		Browser.closeBrowser();
	}

}// End of Class
