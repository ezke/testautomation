package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_SLC_RejectItem extends baseTest {

    private String batchID = "";
    private String keyingJob = "slc";
    private String rejectReason = "test";
    private String seq1 = "1";
    private String seq3 = "3";
    private String seq5 = "5";
    String auditHeader = "Audit Trail";
    String rejectReasonHeader = "Reject Reason";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batchID = BaseUI.random_NumberAsString(702016, 702021);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3694_SLC_Verify_RejectModal_Appears() throws Exception {

//      Step8
        DataCompletion.click_reject();
        DataCompletion.verify_Reject_Modal_Visible();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3694_SLC_Verify_SeqChanged_AfterSelecting_RejectReason() throws Exception {

//      Step9
        DataCompletion.select_RejectReason(rejectReason);
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Scanline.verify_Seq_Changed(seq1);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3694_SLC_Verify_SeqText_PreviousSeqNumber() throws Exception {

        DataCompletion_Scanline.select_SeqNumber_FromDropdown(seq1);
        DataCompletion_Scanline.verify_CurrentSeq_Rejected_bySLC();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3694_SLC_Verify_Seq3_Changes_Seq5() throws Exception {

//      Step10
        String expectedValue = "750526679548154007102002000200000906489";
        DataCompletion_Scanline.select_SeqNumber_FromDropdown(seq3);
        DataCompletion_Scanline.set_Msrd_On_Or_Off(false);
        DataCompletion_Scanline.enter_Text_IntoKeyingField("", false);
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
        DataCompletion_Scanline.verify_Seq_Changed(seq3);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3694_SLC_Verify_Seq5_Changes_Seq7() throws Exception {

//      Step11-12
        DataCompletion.click_reject();
        DataCompletion.select_RejectReason(rejectReason);
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Scanline.verify_Seq_Changed(seq5);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3694_SLC_Verify_SeqText_OfSeq5() throws Exception {

        DataCompletion_Scanline.select_SeqNumber_FromDropdown(seq5);
        DataCompletion_Scanline.verify_CurrentSeq_Rejected_bySLC();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3694_SLC_Verify_Rejected_Item_AuditTrail_RejectReason_Seq1() throws Exception {

//      Step 13-15
        HashMap<String, String> viewItemRowSeq1 = new HashMap<String, String>();

        DataCompletion.click_DoneButton();
        DataCompletion.click_Yes_ToConfirm();
        Navigation.navigate_to_Batch_ViaCommand(batchID);
        DataMaintenance_Batch.click_ViewItems();
        int indexOfViewItemsSeq1 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", seq1);
        viewItemRowSeq1 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq1);
        BaseUI.baseStringPartialCompare(auditHeader,
                "Reject,slc," + GlobalVariables.return_UserName_Formatted() + ",,", viewItemRowSeq1.get(auditHeader));
        BaseUI.baseStringPartialCompare(rejectReasonHeader,
                rejectReason + " seq# " + seq1 , viewItemRowSeq1.get(rejectReasonHeader));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3694_SLC_Verify_Rejected_Item_AuditTrail_RejectReason_Seq3() throws Exception {

//      Step 13-15
        HashMap<String, String> viewItemRowSeq5 = new HashMap<String, String>();
        int indexOfViewItemsSeq5 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", seq5);
        viewItemRowSeq5 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq5);
        BaseUI.baseStringPartialCompare(auditHeader,
                "Reject,slc," + GlobalVariables.return_UserName_Formatted() + ",,", viewItemRowSeq5.get(auditHeader));
        BaseUI.baseStringPartialCompare(rejectReasonHeader,
                rejectReason + " seq# " + seq5 , viewItemRowSeq5.get(rejectReasonHeader));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }


}
