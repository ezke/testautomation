package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_DRV_RejectEntireBatch extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private String batchID;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702818, 702820);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_KeyBatch();
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3793_Step6_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3793_Step7_To_9_KeyTo_CC2_Verify_Alert_Dup() throws Exception {

        String expectedMessage = "This batch is ready for dup and does not require keying.";
        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "drv";
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3793_Step11_DRV_Verify_Duplicate_AlertPopup() throws Exception {

        String expectedMessage = "This batch has a high percentage of duplicates detected.\nThe batch Counts are listed below." +
                "\nYou may lock the batch, or use the \"Reject Batch\" button to reject the batch.";
        DataCompletion.click_OK_on_Alert();
        DataMaintenance_Batch.refresh_And_Wait_ForJobList_ColumnValueToBeUpdated("Description",
                "Duplicate Review (drv)", 1, 120);
        DataMaintenance_Batch_SearchResults.click_KeyBatch();
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3793_Step12_To_14_DRV_Click_Through_Sequence_Two_VerifySequence() throws Exception {

        String secondSequence = DataCompletion_DRV.return_Current_SeqNumber();
        DataCompletion.click_OK_on_Alert();
        DataCompletion_DRV.click_through_ShowNextMatch();
        DataCompletion_DRV.click_NotDuplicate_Button();
        DataCompletion_DRV.verify_Seq_Changed(secondSequence);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3793_Step15_DRV_Click_Through_Sequence_Four_VerifySequence() throws Exception {

        String fourthSequence = DataCompletion_DRV.return_Current_SeqNumber();
        batchID = DataCompletion_DRV.return_BatchID();
        DataCompletion_DRV.click_YesDuplicate_Button();
        DataCompletion_DRV.verify_Seq_Changed(fourthSequence);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 60)
    public void PT3793_Step16_To_17_DRV_Reject_Batch_Verify_AlertPopupMessage() throws Exception {

        String expectedMessage = "This batch is ready for pp2 and does not require keying.";
        DataCompletion_DRV.click_RejectBatch_Button();
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 70)
    public void PT3793_Step17_To_18_DRV_Compare_BatchSearchResults_BatchID() throws Exception {

        DataCompletion.click_OK_on_Alert();
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_ExpectedValue("Batch ID", batch);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 80)
    public void PT3793_Step19_DRV_Compare_BatchSearchResults_TranRejectJob() throws Exception {

        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_ExpectedValue("Tran Reject Job", keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 90)
    public void PT3793_Step19_DRV_Compare_BatchSearchResults_RejectJob() throws Exception {

        String[] jobs = {"drv", "---"};
        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_ExpectedValues("Reject Job", jobs);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 100)
    public void PT3793_Step19_DRV_Compare_BatchSearchResults_AppliedAmount() throws Exception {

        String[] amounts = {"$0.01", "$0.00"};
        DataMaintenance_Batch_ViewItems.verify_ColumnMatches_ExpectedValues("Applied Amount", amounts);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 110)
    public void PT3793_Step16_DRV_Compare_BatchSearchResults_RejectReason() throws Exception {

        String[] rejectReasons ={"MicrDup Entire Batch seq", "micrdup batseq "+ batchID + "_4 seq# 4"};
        DataMaintenance_Batch_ViewItems.verify_ColumnContains_ExpectedValues("Reject Reason", rejectReasons );
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 120)
    public void PT3793_Step19_DRV_Compare_BatchSearchResults_AuditTrail() throws Exception {

        String[] auditTrails ={"$5,cc2," + GlobalVariables.return_UserName_Formatted(), "Reject,drv," + GlobalVariables.return_UserName_Formatted()};
        DataMaintenance_Batch_ViewItems.verify_ColumnContains_ExpectedValues("Audit Trail", auditTrails );
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}