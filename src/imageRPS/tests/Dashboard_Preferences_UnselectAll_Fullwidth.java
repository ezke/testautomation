package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;

public class Dashboard_Preferences_UnselectAll_Fullwidth extends baseTest {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);

        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 10)
    public void PT3408_UnselectGraphs_FullDisplay_UserB() throws Exception {
        Dashboard.select_Preferences_Dropdown("full");
        Dashboard.unselect_All_Dashboard_Graphs();
        Dashboard.click_Preferences_Save();
        Dashboard.signout_Relogin_Second_User();
        Dashboard.verify_All_Dashboard_Charts_Display();
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 20)
    public void PT3408_UnselectGraphs_FullDisplay_UserA() throws Exception {
        Dashboard.signout_Relogin_Primary_User();
        Dashboard.verify_All_Dashboard_Charts_DoNot_Display();
        Dashboard.select_All_Dashboard_Graphs();
        Dashboard.verify_Full_Chart_Width();
        Dashboard.verify_All_Dashboard_Charts_Display();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            Dashboard.select_Preferences_Dropdown("1/2");
            Dashboard.click_Preferences_Save();
        } finally {
            Browser.closeBrowser();
        }
    }
}
