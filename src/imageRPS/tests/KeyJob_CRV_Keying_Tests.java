package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.DataMaintenance_ItemInfo;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_Account;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class KeyJob_CRV_Keying_Tests extends baseTest {

	
	String keyingJob = "crv";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String fieldName = "";
	String genericValue = "1";
	
	Integer index_Of_Field = null;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		batch = "700226";
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
	
		

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 1)
	public void PT3699_CRV_KeyMissingFields_Verify_ConfirmPresent() throws Exception {
		batch = DataCompletion.return_BatchID();
		seqNumber = DataCompletion_Account.return_first_Seq();
		fieldName = DataCompletion_Account.return_first_FieldName();
		
		DataCompletion_Account.key_Job_With_GenericValues(genericValue);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 2)
	public void PT3699_CRV_Confirm_Takes_ToNextBatch() throws Exception {
		DataCompletion.click_Yes_ToConfirm();
		//DataCompletion.verify_BatchID_Changed(batch);
		DataCompletion.verify_CorrectJob("cca");
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 3)
	public void PT3699_CRV_Navigate_ItemInfo_AndValidate_FieldName() throws Exception {
		String header = "Field Name";
		Integer rowIndex = null;
		
		Navigation.navigate_to_Batch_ViaCommand(batch);
		DataMaintenance_Batch.click_ViewItems();
		rowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", seqNumber);
		DataMaintenance_Batch_ViewItems.select_Row_ByIndex(rowIndex);
		DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
		
		index_Of_Field = DataMaintenance_ItemInfo.return_FirstRowIndex_OfColumn_WithValue(header, fieldName);
		DataMaintenance_ItemInfo.verify_Field_By_Header_AndIndex(header, index_Of_Field, fieldName);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 4)
	public void PT3699_CRV_ItemInfo_Validate_AuditInfo() throws Exception {
		String header = "Audit Info";
		String expectedAuditValue = GlobalVariables.return_UserName_Formatted() + "," + keyingJob + ",";
		DataMaintenance_ItemInfo.verify_Field_By_Header_AndIndex(header, index_Of_Field, expectedAuditValue);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 4)
	public void PT3699_CRV_ItemInfo_Validate_FieldValue() throws Exception {
		String header = "Field Value";
		DataMaintenance_ItemInfo.verify_Field_By_Header_AndIndex(header, index_Of_Field, genericValue);
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
