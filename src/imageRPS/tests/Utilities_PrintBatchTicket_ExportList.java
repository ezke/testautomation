package imageRPS.tests;

import java.sql.SQLException;
import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Utilities_PrintTicket_SearchResults_PreviewList;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_PrintBatchTicket_ExportList extends baseTest {

	String downloadsFolder = Browser.downloadLocation;
	String clientID = "10";
	String lockboxID = "Shoptek2";
	String docGroup = "4";
	// Might switch to enum
	WorkType workType = WorkType.NSF;

	TableData searchResults;
	TableData exportResults;
	TableData previewListResults;

	// Tests are hardcoded to run in Chrome, cannot download files through IE.
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(clientID, null, null, null);

		searchResults = Utilities_PrintTicket_SearchResults.return_SearchResults2();
		exportResults = Utilities_PrintTicket_SearchResults.return_ExportList_TableData();

		Utilities_PrintTicket_SearchResults.click_PreviewList();
		previewListResults = Utilities_PrintTicket_SearchResults_PreviewList.return_SearchResults2();

	}

	@DataProvider(name = "searchResults")
	public Object[][] createData_SortTests() throws ClassNotFoundException, SQLException {
		BaseUI.verify_true_AndLog(searchResults.data.size() > 0, "Found Search Results.",
				"Did NOT find search results.");

		Object[][] rowObject = new Object[searchResults.data.size()][2];

		for (Integer i = 0; i < searchResults.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = searchResults.data.get(i);
		}

		return rowObject;
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, dataProvider = "searchResults", priority = 3)
	public void PT3326_Compare_ExportList_Against_SearchResults(Integer index, HashMap<String, String> tableRow)
			throws Throwable {

		BaseUI.verify_TableRow_Matches(index.toString(), tableRow, exportResults.data.get(index));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, dataProvider = "searchResults", priority = 3)
	public void PT3327_Compare_PreviewList_Against_SearchResults(Integer index, HashMap<String, String> tableRow)
			throws Throwable {

		BaseUI.verify_TableRow_Matches(index.toString(), tableRow, previewListResults.data.get(index));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 3)
	public void PT3326_Utilities_PrintBatchTicket_ExportList_Validate_FileName() throws Exception {
		// batchTicketList20170519
		String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "yyyyMMdd");
		String expectedFileText = "batchTicketList" + todaysDate;
		String fileName = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);

		BaseUI.baseStringPartialCompare("File Name", expectedFileText, fileName);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		searchResults = null;
		exportResults = null;
		previewListResults = null;
		Browser.closeBrowser();
	}

}// End of Class
