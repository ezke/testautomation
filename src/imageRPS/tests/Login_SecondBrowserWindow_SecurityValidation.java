package imageRPS.tests;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.Navigation.TopNavigationTab;
//import imageRPS.pages.SignOut_Page;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class Login_SecondBrowserWindow_SecurityValidation extends baseTest {
	String original_Browser;
	WebDriver originalDriver;
	WebDriver secondDriver;
	String linkToCheck = "ImageRpsTest";
	String ItemK = "Item Keying Count Graph";
	String alertG = "Alert Count Graph";
	String Consolidations = "Consolidations by Cutoff Time Graph";

	String apptime2 = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4410_verify_EntityImageRPSTest_IsSelected() throws Exception {

		Navigation.verify__toolbarMenu_atTheTopOfThePage(linkToCheck);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4410_verify_TopNavigationTab_IsActive() throws Exception {

		Navigation.verify_Page_Active(TopNavigationTab.Dashboard);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT4410_verifytable_TablePreferences() throws Exception {

		Dashboard.verifytable(ItemK, alertG, Consolidations);
		// Dashboard.return_preferencesList();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT4410_verify_DashboardWidget_arePresnt() throws Exception {

		Dashboard.verify_DashboardWidget_arePresnt();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT4410_verify_NewSession() throws Exception {
		originalDriver = Browser.driver;
		Browser.driver = null;
		if (Browser.currentBrowser.equals("internetexplorer")) {
			Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");
		} else {

			Browser.openBrowser(GlobalVariables.baseURL, "internetexplorer");
		}

		secondDriver = Browser.driver;
		LoginPage.login_DontNavigatePastPriorSession(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

		LoginPage.verify_NewSessionLinksAre_Present();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT4410_verify_SecoundBrowserMsg() throws Exception {
		LoginPage.navigate_past_Preexisting_SessionMessage();

		Browser.close_CurrentBrowserWindow();
		Browser.driver = originalDriver;
		BaseUI.waitForElementToBeDisplayed("loginPage_presentTime_ExistingSessionEnd", null, null, 90);
		
		LoginPage.verify_SecoundBrowserMsg();

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		// Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		// Dashboard.click_ItemKeying_Link(itemKeying_Link.Jobs);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		// Browser.closeBrowser();
		if (originalDriver != null) {
			Browser.driver = originalDriver;
			Browser.closeBrowser();
		} else {
			Browser.closeBrowser();
		}
		if (secondDriver != null) {
			Browser.driver = secondDriver;
			Browser.closeBrowser();
		}
	}
}// End of Class