package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_SLC extends baseTest {

    private String batchID = "";
    private String keyingJob = "slc";
    private String expectedRej = "---";
    private String expectedSeqDropdownValue = "";
    private String expectedDocId = "21";
    private String expectedOvertypeMode = "Overtype";
    private String expectedMisreadMode = "Misread";
    TableData tableRows;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batchID = BaseUI.random_NumberAsString(702011, 702015);

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3832_SLC_Verify_KeyingRows() throws Exception {

        expectedSeqDropdownValue = "1";
        DataCompletion_Scanline.verify_KeyingRows(expectedRej, expectedDocId, expectedSeqDropdownValue,
                expectedOvertypeMode, expectedMisreadMode);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3832_SLC_Gray_Button() throws Exception {
        DataCompletion.verify_Gray_Button_ChangesImage();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3832_SLC_Gray_Button_Flip_Works() throws Exception {
        DataCompletion.verify_Flip_Back_And_Front_Works();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3832_SLC_BW_Button() throws Exception {
        DataCompletion.verify_BW_Button_ChangesImage();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3832_SLC_BW_Button_Flip_Works() throws Exception {
        DataCompletion.verify_Flip_Back_And_Front_Works();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 60)
    public void PT3832_SLC_OCR_Changes_KeyingTextBox() throws Exception {

        tableRows = DataCompletion_Scanline.key_All_Fields_UntilDone();
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.end_DataCompletion();
        BaseUI.verify_true_AndLog(tableRows.data.size()> 0, "Found rows to test.", "Did not find rows to test.");
        DataCompletion.verify_DataCompletionPage_Loaded();

    }

    @DataProvider(name = "keyFieldsRows")
    public Object[][] createData_KeyFieldsScanline() {
        Object[][] linksToNavigate = new Object[tableRows.data.size()][2];

        int indexToPlace_ColumnHeader = 0;
        for (HashMap<String, String> row : tableRows.data) {
            linksToNavigate[indexToPlace_ColumnHeader][1] = row;
            linksToNavigate[indexToPlace_ColumnHeader][0] = indexToPlace_ColumnHeader;
            indexToPlace_ColumnHeader++;
        }

        return linksToNavigate;
    }

    @Test(dataProvider = "keyFieldsRows", alwaysRun = true, priority = 70, groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"})
    public void PT3832_Compare_DataCompletion_KeyFieldsScanline_Against_ItemInfoTable(int lineIndex, HashMap<String, String> row) throws Exception {

        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
        DataMaintenance.navigate_to_SubTab("Batches");
        DataMaintenance_Batch_Filters.enter_BatchID_AndSearch(batchID, "");
        DataMaintenance_Batch_SearchResults.select_Row_ByIndex(1);
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        int indexOfViewItemsSeqRow = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
                row.get("Seq"));
        DataMaintenance_Batch_ViewItems.select_Row_ByIndex(indexOfViewItemsSeqRow);
        DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
        TableData viewItemsTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
        BaseUI.verify_true_AndLog(viewItemsTable.data.size()> 0, "Found rows to test.", "Did not find rows to test.");
        HashMap<String, String> finalScanlineRow = viewItemsTable.return_Row_BasedOn_1MatchingField("Field Name", "Final Scanline");
        BaseUI.baseStringCompare("FinalScanlineRow", row.get("FinalScalineText"), finalScanlineRow.get("Field Value"));

        HashMap<String, String> rawScanlineRow = viewItemsTable.return_Row_BasedOn_1MatchingField("Field Name", "Raw Scanline");
        BaseUI.baseStringCompare("RawScanlineRow", row.get("RawScalineText").replace("*", "!"), rawScanlineRow.get("Field Value"));

        BaseUI.baseStringCompare("AuditInfo", GlobalVariables.return_UserName_Formatted()+",slc,", finalScanlineRow.get("Audit Info") );
        BaseUI.baseStringCompare("ScanComplete", "yes", finalScanlineRow.get("Scan Complete") );
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
