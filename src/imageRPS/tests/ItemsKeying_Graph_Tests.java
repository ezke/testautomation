package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.Dashboard.itemKeying_BarColor;
import imageRPS.pages.Dashboard.itemKeying_Link;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.Browser;
import utils.ResultWriter;

public class ItemsKeying_Graph_Tests extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3609_ItemKeying_ItemsToKey_ClientTab() throws Exception {
		Integer barToClick = 1;
		itemKeying_BarColor barColor = itemKeying_BarColor.Red;

		Dashboard.verify_ItemKeying_Clients_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3620_ItemKeying_ItemsToKey_CutoffsTab() throws Exception {
		Integer barToClick = 1;
		itemKeying_BarColor barColor = itemKeying_BarColor.Red;
		
		Dashboard.verify_ItemKeying_Cutoffs_BatchNavigation(barColor, barToClick);
	}

	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3608_ItemKeying_Batch_ClientTab() throws Exception {
		Integer barToClick = 1;
		itemKeying_BarColor barColor = itemKeying_BarColor.Green;

		Dashboard.verify_ItemKeying_Clients_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3619_ItemKeying_Batch_CutoffsTab() throws Exception {
		Integer barToClick = 1;
		itemKeying_BarColor barColor = itemKeying_BarColor.Green;
		
		Dashboard.verify_ItemKeying_Cutoffs_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3610_ItemKeying_ItemsInSystem_ClientTab() throws Exception {
		Integer barToClick = 3;
		itemKeying_BarColor barColor = itemKeying_BarColor.Blue;

		Dashboard.verify_ItemKeying_Clients_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3620_ItemKeying_ItemsInSystem_CutoffsTab() throws Exception {
		Integer barToClick = 3;
		itemKeying_BarColor barColor = itemKeying_BarColor.Blue;
		
		Dashboard.verify_ItemKeying_Cutoffs_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3598_ItemKeying_Batch_DatesTab() throws Exception {
		Integer barToClick = 5;
		itemKeying_BarColor barColor = itemKeying_BarColor.Green;
		
		Dashboard.verify_ItemKeying_Dates_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3599_ItemKeying_ItemsToKey_DatesTab() throws Exception {
		Integer barToClick = 1;
		itemKeying_BarColor barColor = itemKeying_BarColor.Red;
		
		Dashboard.verify_ItemKeying_Dates_BatchNavigation(barColor, barToClick);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" })
	public void PT3600_ItemKeying_ItemsInSystem_DatesTab() throws Exception {
		Integer barToClick = 6;
		itemKeying_BarColor barColor = itemKeying_BarColor.Blue;
		
		Dashboard.verify_ItemKeying_Dates_BatchNavigation(barColor, barToClick);
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		Dashboard.click_ItemKeying_Link(itemKeying_Link.Jobs);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
