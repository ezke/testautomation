package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.Browser;
import utils.ResultWriter;

public class DataMaintenance_BatchSearchResults_SortTests extends baseTest {

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance_Batch_Filters.click_Submit();

	}

	@DataProvider(name = "SortTests")
	public Object[][] return_Tests_ObjectArray() {

		// Object[][] linksToNavigate = new Object[dataSet.size()][1];

		Object[][] tests_ToRun = { { "Client ID", "ascending_numeric" }, { "Client ID", "descending_numeric" },
				{ "Batch ID", "ascending_numeric" }, { "Batch ID", "descending_numeric" },
				{ "Batch Date", "ascending_date" }, { "Batch Date", "descending_date" },
				{ "Balanced", "ascending_alpha" }, { "Balanced", "descending_alpha" }, { "PbC", "ascending_alpha" },
				{ "PbC", "descending_alpha" }, { "Lockbox ID", "ascending_alpha" },
				{ "Lockbox ID", "descending_alpha" }, { "Batch Num", "ascending_numeric" },
				{ "Batch Num", "descending_numeric" }, { "Doc Grp", "ascending_numeric" },
				{ "Doc Grp", "descending_numeric" }, { "Locked By", "ascending_alpha" },
				{ "Locked By", "descending_alpha" }, { "Keying Job", "ascending_alpha" },
				{ "Keying Job", "descending_alpha" }, { "Item Count", "ascending_numeric" },
				{ "Item Count", "descending_numeric" }, { "Check Count", "ascending_numeric" },
				{ "Check Count", "descending_numeric" }, { "Reject Checks", "ascending_numeric" },
				{ "Reject Checks", "descending_numeric" }, { "Check Total", "ascending_numeric" },
				{ "Check Total", "descending_numeric" } };

		return tests_ToRun;
	}

	@Test(dataProvider = "SortTests", groups = { "all_Tests", "regression_Tests" })
	public void PT4436_DataMaint_BatchResults_Sort(String headerText, String testType) throws Exception {
		DataMaintenance_Batch_SearchResults.run_SortTest_DataDriven(headerText, testType);

	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
