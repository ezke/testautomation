package imageRPS.tests;

import imageRPS.data.ImageRPS_Utilities;
import imageRPS.data.baseTest;
import imageRPS.pages.Alerts;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

import java.time.Duration;

public class SessionMangement_Login_SessionExpireChooseOK extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ImageRPS_Utilities.updateWebConfigTimeout(Duration.ofMinutes(1));
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(priority = 10, groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"})
    public void PT4417_TopMenuSecurity_Login_VerifyDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto2_users, GlobalVariables.passvord_SMAuto2_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @Test(priority = 20, groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"})
    public void PT4417_TopMenuSecurity_ValidateAllowThinClientSessionToExpire() throws Exception {
        Alerts.verify_SessionTimeOut_Alert();
        BaseUI.click(Locator.lookupElement("dshbrd_Alert_ButtonOK"));
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try{
            ImageRPS_Utilities.setDefaultWebServerTimeout();
        }finally {
            Browser.closeBrowser();
        }
    }
}//End of Class


