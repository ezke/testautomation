package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.jsoup.Connection;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import utils.BaseUI;
import utils.Locator;
import utils.ResultWriter;

public class SessionManagement_DataCompletionRefreshPage extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 10)
    public void PT4416_TopMenuSecurity_Login_CheckDashBoardDataCompletion() throws Exception {

        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SECt4416_users, GlobalVariables.passvord_SECt4416_users);

        Navigation.verify_ToolBarHeader_Appears("Dashboard");
        Navigation.verify_ToolBarHeader_Appears("Data Completion");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 20)
    public void PT4416_TopMenuSecurity_Login_VerifyDataCompletionPage() throws Exception {

        DataCompletion.click_DataCompletion();
        DataCompletion.verify_DataCompletionPage_Loaded();
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 30)
    public void PT4416_DataCompletion_ValidateThinClientRefreshPage() throws Exception {

        Browser.driver.navigate().refresh();
        BaseUI.waitForElementToBeDisplayed("dshbrd_dashboard", null, null);

        Navigation.verify_ToolBarHeader_Appears("Dashboard");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 40)
    public void PT4416_DataCompletion_ValidateSignOutMessage() throws Exception {

        Navigation.signOut();

        BaseUI.wait_ForElement_ToContainText("signOutPage_Message", 100, "You are now signed out.");
        LogOutPage.verifyLogOutPageMsg();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
         Browser.closeBrowser();
    }

}//End of Class
