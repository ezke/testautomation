package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataMaintenance_Batch_Filters;
import imageRPS.pages.DataMaintenance_Batch_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class DataMaintenance_BatchSearchResults_FetchMoreResults extends baseTest {

	TableData exportList;
	Integer formerUpperAmount = null;
	Integer newUpperAmount = null;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chromeHeadless");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance_Batch_Filters.click_Submit();
		
		formerUpperAmount = DataMaintenance_Batch_SearchResults.return_UpperCountAmount();
	}


	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 1)
	public void PT4447_Utilities_PrintBatchTicketFilter_ClickFetchMore_AmountIncreases() throws Exception {
		DataMaintenance_Batch_SearchResults.click_FetchMore();
		
		newUpperAmount = DataMaintenance_Batch_SearchResults.return_UpperCountAmount();
		BaseUI.verify_true_AndLog(newUpperAmount > formerUpperAmount, "Upper Amount increased.", "Upper Amount did NOT increase.");
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 2)
	public void PT4447_Utilities_PrintBatchTicketFilter_ClickFetchMore_ExportList_Equals_NewAmount() throws Exception {
		exportList = DataMaintenance_Batch_SearchResults.return_ExportList_TableData();
	
		BaseUI.verify_true_AndLog(exportList.data.size() == newUpperAmount, "Size of table matched Count on page.", "Size of table did NOT match Count on page.");
	}
	
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		exportList = null;
		Browser.closeBrowser();
	}

}// End of Class
