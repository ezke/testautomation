package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_DRV_Duplicate_Reject extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private String fourthSequence;
    private String eighthSequence;
    private String batchID;
    private String expectedAppliedAmountValue = "0.00";
    private String expectedAuditTrailValue = "Reject,drv," + GlobalVariables.return_UserName_Formatted();


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(3216, 3218);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_KeyBatch();
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3703_Step8_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3703_Step9_To_10_KeyTo_CC2_Verify_Alert_Dup() throws Exception {

        String expectedMessage = "This batch is ready for dup and does not require keying.";
        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "drv";
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3703_Step12_DRV_Verify_Duplicate_AlertPopup() throws Exception {

        String expectedMessage = "This batch has a high percentage of duplicates detected.\nThe batch Counts are listed below." +
                "\nYou may lock the batch, or use the \"Reject Batch\" button to reject the batch.";
        DataCompletion.click_OK_on_Alert();
        DataMaintenance_Batch.refresh_And_Wait_ForJobList_ColumnValueToBeUpdated("Description",
                "Duplicate Review (drv)",1,120);
        DataMaintenance_Batch_SearchResults.click_KeyBatch();
        DataCompletion.verify_AlertModal_Message(expectedMessage);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3703_Step13_To_15_DRV_Click_Through_Sequence_Two_VerifySequence() throws Exception {

        String secondSequence = DataCompletion_DRV.return_Current_SeqNumber();
        DataCompletion.click_OK_on_Alert();
        DataCompletion_DRV.click_through_ShowNextMatch();
        DataCompletion_DRV.click_NotDuplicate_Button();
        DataCompletion_DRV.verify_Seq_Changed(secondSequence);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3703_Step16_DRV_Click_Through_Sequence_Four_VerifySequence() throws Exception {

        batchID = DataCompletion_DRV.return_BatchID();
        fourthSequence = DataCompletion_DRV.return_Current_SeqNumber();
        DataCompletion_DRV.click_YesDuplicate_Button();
        DataCompletion_DRV.verify_Seq_Changed(fourthSequence);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3703_Step17_To_18_DRV_Click_Through_Sequence_Six_Verify_Sequence() throws Exception {

        String sixthSequence = DataCompletion_DRV.return_Current_SeqNumber();
        DataCompletion_DRV.click_through_ShowNextMatch();
        DataCompletion_DRV.click_NotDuplicate_Button();
        DataCompletion_DRV.verify_Seq_Changed(sixthSequence);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3703_Step19_To_20_DRV_Click_Through_Sequence_Eight_VerifySequence() throws Exception {

        eighthSequence = DataCompletion_DRV.return_Current_SeqNumber();
        DataCompletion_DRV.click_YesDuplicate_Button();
        DataCompletion_DRV.verify_Seq_Changed(eighthSequence);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 90)
    public void PT3703_Step22_To_27_DRV_Click_Through_All_DRV_VerifyAlertMessage() throws Exception {

        String expectedMessage = "This batch is ready for pp2 and does not require keying.";
        DataCompletion_DRV.click_through_DRV(false);
        DataCompletion.click_No_ToConfirm();
        DataCompletion_DRV.click_NotDuplicate_Button();
        DataCompletion.click_Yes_ToConfirm();
        DataCompletion.verify_AlertModal_Message(expectedMessage);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 100)
    public void PT3703_Step28_To_31_DRV_Compare_BatchSearchResults_Sequence_Four() throws Exception {

        DataCompletion.click_OK_on_Alert();
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        int viewItemsRowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", fourthSequence);
        HashMap<String, String> viewItemsRowSeq4 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(viewItemsRowIndex);
        SoftAssert softAssert = new SoftAssert();

        try{
            softAssert.assertEquals(viewItemsRowSeq4.get("Reject Job"), keyingJob,
                    "The value displayed for Reject Job on screen " + viewItemsRowSeq4.get("Reject Job")+ " does not contain the expected value of " + keyingJob);
            softAssert.assertEquals(viewItemsRowSeq4.get("Applied Amount").replace("$", ""), expectedAppliedAmountValue,
                    "The value displayed for Applied Amount on screen " + viewItemsRowSeq4.get("Applied Amount") + " does not contain the expected value of " + expectedAppliedAmountValue);
            softAssert.assertEquals(viewItemsRowSeq4.get("Reject Reason"),"micrdup batseq " + batchID + "_" + fourthSequence + " seq# " + fourthSequence,
                    "The value "+ viewItemsRowSeq4.get("Reject Reason")+ " should match " + "micrdup batseq " + batchID + "_" + fourthSequence + " seq# " + fourthSequence);
            softAssert.assertTrue(viewItemsRowSeq4.get("Audit Trail").contains(expectedAuditTrailValue),
                    "The value displayed for Audit Trail on screen " + viewItemsRowSeq4.get("Audit Trail") +
                            " does not contain the expected value of " + expectedAuditTrailValue);
        }finally{
            softAssert.assertAll();
        }
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 110)
    public void PT3703_Step31_DRV_Compare_BatchSearchResults_Sequence_Eight() throws Exception {

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        int viewItemsRowIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", eighthSequence);
        HashMap<String, String> viewItemsRowSeq10 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(viewItemsRowIndex);
        SoftAssert softAssert = new SoftAssert();

        try{
            softAssert.assertEquals(viewItemsRowSeq10.get("Reject Job"), keyingJob,
                    "The value " + viewItemsRowSeq10.get("Reject Job")+ " should match " + keyingJob);
            softAssert.assertEquals(viewItemsRowSeq10.get("Applied Amount").replace("$", ""), expectedAppliedAmountValue,
                    "The value " + viewItemsRowSeq10.get("Applied Amount") + " should match " + expectedAppliedAmountValue);
            softAssert.assertEquals(viewItemsRowSeq10.get("Reject Reason"),"micrdup batseq " + batchID + "_" + eighthSequence + " seq# " + eighthSequence,
                    "The value "+ viewItemsRowSeq10.get("Reject Reason")+ " should match " + "micrdup batseq " + batchID + "_" + eighthSequence + " seq# " + fourthSequence);
            softAssert.assertTrue(viewItemsRowSeq10.get("Audit Trail").contains(expectedAuditTrailValue),
                    "The value displayed for Audit Trail on screen " + viewItemsRowSeq10.get("Audit Trail") +
                            " does not contain the expected value of " + expectedAuditTrailValue);
        }finally{
            softAssert.assertAll();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
