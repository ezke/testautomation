package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.*;

import java.util.HashMap;

public class DataMaintenance_ItemSearch_FilterSuspenseItems extends baseTest {

    TableData searchResults;
    private TableData searchResultsBeforeCPR;
    private TableData searchResultsAfterCPR;
    private String clientID ="";
    private String batch = "";
    private String date = "02/08/2018";
    private String jobId = "CPR";
    private String docGrp = "8";
    private String suspenseBatchClientID = "20 - Bevent Insurance";
    private String suspenseBatchDocGroupID = "1 - Singles - Skip P2 - P&E to 77";

    HashMap<String, String> searchValues = new HashMap<String, String>();

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
        DataMaintenance.navigate_to_SubTab("Item Search");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 10)
    public void PT4748_DataMaintenance_ItemSearch_VerifySuspenseItemsResults() throws Exception {

        searchValues.put("Doc ID", "22");
        searchValues.put("Source Client ID", "601");
        searchValues.put("Source Batch ID", "169271");
        searchValues.put("Source Doc Group", "8");
        searchValues.put("Source Receive Date", "02/03/2015");
        searchValues.put("Source Process Date", "02/03/2015");
        searchValues.put("Source P1 Station ID", "61");

        DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox_ForGivenFields(searchValues);
        DataMaintenance_ItemSearch_Filters.click_Submit();
        searchResults = DataMaintenance_ItemSearch_Results.return_SearchResults();
        searchResults.verify_TableSize(12);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 15)
    public void PT4748_DataMaintenance_ItemSearch_Verify_ClientIDValue_ColumnCount() throws Exception {

        searchResults.verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(searchResults, "Client ID", "601", 10);
        searchResults.verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(searchResults, "Client ID", "5", 2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 20)
    public void PT4748_DataMaintenance_ItemSearch_Verify_BatchIDValue_ColumnCount() throws Exception {

        searchResults.verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(searchResults, "Batch ID", "169271", 10);
        searchResults.verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(searchResults, "Batch ID", "1000", 2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 25)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_BeforeCPR() throws Exception {

        searchValues.put("Account Number", "*");
        searchValues.put("Doc ID", "");
        searchValues.put("Source Client ID", "601");
        searchValues.put("Source Batch ID", "469262");
        searchValues.put("Source Doc Group", "8");
        searchValues.put("Source Receive Date", "");
        searchValues.put("Source Process Date", "");
        searchValues.put("Source P1 Station ID", "");

        DataMaintenance_Batch_SearchResults.click_GoBack();
        DataMaintenance_ItemSearch_Filters.click_Reset();
        DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox_ForGivenFields(searchValues);
        DataMaintenance_ItemSearch_Filters.click_Submit();
        searchResultsBeforeCPR = DataMaintenance_ItemSearch_Results.return_SearchResults();

        searchResultsBeforeCPR.verify_TableSize(20);
        searchResultsBeforeCPR.verify_Value_NOT_InColumn("Client ID", "20");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 30)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_CCAJob() throws Exception {

//      Step 5
        batch = "469262";
        String keyingJob = "cca";
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 35)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_CCA_CPR_ModalVisible() throws Exception {

//      Step6
        DataCompletion.click_CPR_Button();
        DataCompletion.verify_CPR_Modal_Visible();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 40)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_CPR_SelectStubCaptured() throws Exception {

//      Step6
        DataCompletion.modalDialog_Click_Stub_CaptureAsCheck();
        DataCompletion.modalDialog_Click_SelectReason_Button();
        DataCompletion.dataComplete_VerifyCPR_Doc_ConfirmDialogBoxMsg();

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 45)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_JobItems() throws Exception {

//      Step6
        DataCompletion.dataComplete_CPR_Doc_ClickNo_ConfirmDialogBox();
        String expectecBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchId"));
        BaseUI.baseStringPartialCompare("dataComplete_BatchId", batch, expectecBatchId);
        DataCompletion_CPR.verify_JobItem(clientID, batch, date, jobId, docGrp);
        BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Suspense_Btn"));

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 50)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_Select4thStub_Suspense() throws Exception {

//      Step7-8
        DataCompletion_CPR.click_DataComplete_Tran4_Img1();
        DataCompletion_CPR.click_DataComplete_SuspenseBtn();
        DataCompletion_CPR.updateSuspenseBatch(suspenseBatchClientID, suspenseBatchDocGroupID);
        DataCompletion_CPR.verify_SuspenseBatch("Batch Ticket", "Shoptek Stub (all keying Jobs) - Stb", "Per/Bus Check (all keying Jobs - Chk");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 55)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_SelectNewBatch_SetDocID_Stub() throws Exception {

//      Step9
        DataCompletion_CPR.click_DataComplete_BatchStub_Img();
        DataCompletion_CPR.click_dataComplete_DocId_Img();
        DataCompletion_CPR.update_DataComplete_DocID("21 - WFS Remit Stub");
        DataCompletion_CPR.verify_DataComplete_DocID_Stub("WFS Remit Stub - Stb");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 60)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_SelectNewBatch_SetDocID_Chk() throws Exception {

//      Step10
        DataCompletion_CPR.click_DataComplete_BatchChk_Img();
        DataCompletion_CPR.click_dataComplete_DocId_Img();
        DataCompletion_CPR.update_DataComplete_DocID("1 - Per/Bus Check");
        DataCompletion_CPR.verify_DataComplete_DocID_Chk( "Per/Bus Check - Chk");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 65)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_SelectNewBatch_CheckIn() throws Exception {

//      Step11
        DataCompletion_CPR.verify_CheckIn_UpdateBatch();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 70)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_SelectNewBatch_CheckIn_CloseDialogBox() throws Exception {

//      Step11
        DataCompletion_CPR.verify_CheckIn_CloseDialog();
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 80)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Transaction_SelectNewBatch_CCA_Done() throws Exception {

//      Step12
        DataCompletion_CPR.clickOk_CheckIn_AlertDialog();
        DataCompletion.click_DoneButton();
        DataCompletion.click_Yes_ToConfirm();
        BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_KeyingJobs_Header"));
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 85)
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Results() throws Exception {

//      Step13-18
        searchValues.put("Account Number", "*");
        searchValues.put("Doc ID", "");
        searchValues.put("Source Client ID", "601");
        searchValues.put("Source Batch ID", "469262");
        searchValues.put("Source Doc Group", "8");
        searchValues.put("Source Receive Date", "");
        searchValues.put("Source Process Date", "");
        searchValues.put("Source P1 Station ID", "");

        Navigation.navigate_toTab(Navigation.TopNavigationTab.DataMaintenance);
        DataMaintenance.navigate_to_SubTab("Item Search");
        DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox_ForGivenFields(searchValues);
        DataMaintenance_ItemSearch_Filters.click_Submit();
        DataMaintenance_ItemSearch_Results.refresh_And_Wait_ForColumnValueToBeUpdated("Client ID", "20", 1, 300);
        searchResultsAfterCPR = DataMaintenance_ItemSearch_Results.return_SearchResults();
        searchResultsAfterCPR.remove_Character("$", "Applied Amount");
        searchResultsAfterCPR.remove_Character(",", "Applied Amount");
        searchResultsAfterCPR.verify_TableSize(20);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 90,
            dependsOnMethods = {"PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Results" })
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseBatch_ClientIDValue_ColumnCount() throws Exception {

//      Step18
        searchResultsAfterCPR.verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(searchResultsAfterCPR, "Client ID", "601", 18);
        searchResultsAfterCPR.verify_CountOfMatches_ThatHave_ColumnName_AndCellValue(searchResultsAfterCPR, "Client ID", "20", 2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 95,
            dependsOnMethods = {"PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Results" })
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseBatch_Items_In_SearchResultPage() throws Exception {

//      Step18
        HashMap<String, String> suspensItems1RowExpected = new HashMap<String, String>();
        suspensItems1RowExpected.put("Client ID", "20");
        suspensItems1RowExpected.put("P1 Seq Number", "1");
        suspensItems1RowExpected.put("Stub/Check", "STB");
        suspensItems1RowExpected.put("Applied Amount", "4000.00");

        HashMap<String, String> suspensItems2RowExpected = new HashMap<String, String>();
        suspensItems1RowExpected.put("Client ID", "20");
        suspensItems1RowExpected.put("P1 Seq Number", "2");
        suspensItems1RowExpected.put("Stub/Check", "Chk");
        suspensItems1RowExpected.put("Applied Amount", "4000.00");

        searchResultsAfterCPR.verify_MatchingRow_Found(suspensItems1RowExpected);
        searchResultsAfterCPR.verify_MatchingRow_Found(suspensItems2RowExpected);

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataMaintenance_ItemSearch" }, priority = 100,
            dependsOnMethods = {"PT4799_DataMaintenance_ItemSearch_Verify_SuspenseItems_Results" })
    public void PT4799_DataMaintenance_ItemSearch_Verify_SuspenseBatch_BatchIDColumn_IsNotEmpty() throws Exception {

        searchResultsAfterCPR.verify_Column_NotEmptyOrNull("Batch ID");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
