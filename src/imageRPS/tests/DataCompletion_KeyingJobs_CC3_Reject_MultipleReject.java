package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.Browser;
import utils.ResultWriter;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_CC3_Reject_MultipleReject extends baseTest {

    private String keyingJob = "cca";
    private String batch = "702757";
    private String firstRejectReason;
    private String secondRejectReason;
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3757_Step6_To_7_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3757_Step8_To_9_CC2_KeyTo_CC3_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3757_Step10_CC3_VerifySeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3757_Step11_CC3_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3757_Step12_CC3_Reject_VerifySecondSeq_Rejected() throws Exception {

        firstRejectReason = "Global Test";
        DataCompletion.click_ConfirmModal_Reject_Button();
        DataCompletion.select_RejectReason(firstRejectReason);
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Balancing.verify_Rejected(keyingJob, 1);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3757_Step14_CC3_VerifyFourthSeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_KeyingField_Empty();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "verify");

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3757_Step14_CC3_VerifyConfirmMessage_FourthSeq() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3757_Step15_CC3_Reject_VerifyFourthSeq_Rejected() throws Exception {

        secondRejectReason = "CC3 Could not identify amount";
        DataCompletion.click_ConfirmModal_Reject_Button();
        DataCompletion.select_RejectReason(secondRejectReason);
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Balancing.verify_Rejected(keyingJob, 2);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 90)
    public void PT3757_Step16_To_18_CC3_Reject_Verify_ViewItems_SecondSeq() throws Exception {

        HashMap<String, String> viewItemRowSeq2 = new HashMap<String, String>();
        SoftAssert softAssert = new SoftAssert();
        DataCompletion.end_DataCompletion();

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_ViewItems();

        int indexOfViewItemsSeq2 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");
        viewItemRowSeq2 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq2);

        try{
            softAssert.assertEquals(viewItemRowSeq2.get("Reject Job"), keyingJob);
            softAssert.assertEquals(viewItemRowSeq2.get("Tran Reject Job"), keyingJob);
            softAssert.assertEquals(viewItemRowSeq2.get("Reject Reason"),firstRejectReason + " seq# 2");
            softAssert.assertTrue(viewItemRowSeq2.get("Audit Trail").contains("Reject,cc3," + GlobalVariables.return_UserName_Formatted()));

        }finally{
            softAssert.assertAll();
        }
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 100)
    public void PT3757_Step18_CC3_Reject_Verify_ViewItems_FourthSeq() throws Exception {

        HashMap<String, String> viewItemRowSeq4 = new HashMap<String, String>();
        SoftAssert softAssert = new SoftAssert();

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        int indexOfViewItemsSeq4 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "4");
        viewItemRowSeq4 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq4);

        try{
            softAssert.assertEquals(viewItemRowSeq4.get("Reject Job"), keyingJob);
            softAssert.assertEquals(viewItemRowSeq4.get("Tran Reject Job"), keyingJob);
            softAssert.assertEquals(viewItemRowSeq4.get("Reject Reason"),secondRejectReason + " seq# 4");
            softAssert.assertTrue(viewItemRowSeq4.get("Audit Trail").contains("Reject,cc3," + GlobalVariables.return_UserName_Formatted()));

        }finally{
            softAssert.assertAll();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }


}
