package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_KeyCC3_Incorrectly_Rekey_Reject_Cancel extends baseTest {

    private String keyingJob = "cca";
    private String batch = "";
    private int indexOfViewItemsSeq2;
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batch = BaseUI.random_NumberAsString(702220, 702222);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch.toString());
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3790_Step9_To_10_CCA_KeyTo_CC2() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3790_Step11_To_13_CC2_KeyTo_CC3() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3790_Step13_CC3_VeifySeq() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }


    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3790_Step14_CC3_VeifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3790_Step15_To_16_CC3_Rekey_VerifySequence() throws Exception {

        DataCompletion.click_ConfirmModal_Rekey_Button();
        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 60)
    public void PT3790_Step17_CC3_Rekey_VerifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 70)
    public void PT3790_Step18_To_22_CC3_Reject_VerifyViewItems() throws Exception {

        String rejectReason = "CC3 Could not identify amount";
        HashMap<String, String> viewItemRowSeq2 = new HashMap<String, String>();
        SoftAssert softAssert = new SoftAssert();
        DataCompletion.click_ConfirmModal_Reject_Button();
        DataCompletion.select_RejectReason(rejectReason);
        DataCompletion.click_Cancel_Reject();
        DataCompletion.end_DataCompletion();

        Navigation.navigate_to_Batch_ViaCommand(batch);
        DataMaintenance_Batch_SearchResults.click_ViewItems();
        indexOfViewItemsSeq2 = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", "2");
        viewItemRowSeq2 = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(indexOfViewItemsSeq2);

        try{
            softAssert.assertNotEquals(viewItemRowSeq2.get("Reject Job"), keyingJob,
                    "The value " + viewItemRowSeq2.get("Reject Job")+ " should Not matches " + keyingJob);
            softAssert.assertNotEquals(viewItemRowSeq2.get("Tran Reject Job"), keyingJob,
                    "The value " + viewItemRowSeq2.get("Tran Reject Job") + " should Not matches " + keyingJob);
            softAssert.assertNotEquals(viewItemRowSeq2.get("Reject Reason"),rejectReason + " seq# 2",
                    "The value "+ viewItemRowSeq2.get("Reject Reason")+ " should Not matches " + rejectReason+"seq# 2");
            softAssert.assertNotEquals(viewItemRowSeq2.get("Audit Trail"),"Reject,cc3," + GlobalVariables.return_UserName_Formatted(),
                    "The value "+ viewItemRowSeq2.get("Audit Trail")+ " should Not matches " + "Reject,cc3," + GlobalVariables.return_UserName_Formatted());

        }finally{
            softAssert.assertAll();
        }

    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 80)
    public void PT3790_Step23_CC3_Reject_VerifyItemInfo() throws Exception {

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        DataMaintenance_Batch_ViewItems.select_Row_ByIndex(indexOfViewItemsSeq2);
        DataMaintenance_Batch_ViewItems.click_ItemInfo_Button();
        TableData viewItemsTable = DataMaintenance_ItemInfo.return_ItemInfoTable();
        BaseUI.verify_true_AndLog(viewItemsTable.data.size()> 0, "Found rows to test.", "Did not find rows to test.");
        HashMap<String, String> sequense$5Row = viewItemsTable.return_Row_BasedOn_1MatchingField("Type Sequence", "$ 5");
        BaseUI.verify_true_AndLog(sequense$5Row.get("Field Value").length()== 0, "Field value was empty", "Field value was Not empty");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 90)
    public void PT3790_Step24_Verify_KeyCC3_Appears() throws Exception {

        DataMaintenance_Batch.click_GoBack();
        DataMaintenance_Batch.click_GoBack();
        DataMaintenance_Batch.click_KeyBatch();
        DataCompletion.verify_CorrectJob(keyingJob);
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "2");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}
