package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

import java.util.HashMap;

public class DataCompletion_KeyingJobs_SL2_Reject_UnReject extends baseTest {

    private String keyingJob = "sl2";
    private String batchID;
    private String secondSequence;
    private String fourthSequence;
    private String sixthSequence;
    private String eightSequence;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batchID = BaseUI.random_NumberAsString(123467, 123479);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
    public void PT3696_Step8_To_9_SL2_RejectSecondSeq() throws Exception {

        secondSequence = DataCompletion_Scanline.return_Current_SeqNumber();
        DataCompletion.click_reject();
        DataCompletion.select_RejectReason_Default();
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Scanline.verify_Seq_Changed(secondSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
    public void PT3696_Step10_SL2_KeyFourthSeq() throws Exception {

        fourthSequence = DataCompletion_Scanline.return_Current_SeqNumber();
        DataCompletion_Scanline.enter_Text_IntoKeyingField("d051404260d5231746547c03487", true);
        DataCompletion_Scanline.verify_Seq_Changed(fourthSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
    public void PT3696_Step_10_SL2_FourthSeq_NotRejected() throws Exception {

        DataCompletion_Scanline.select_SeqNumber_FromDropdown(fourthSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 40)
    public void PT3696_Step10_SL2_Reject_FourthSeq() throws Exception {

        DataCompletion.click_reject();
        DataCompletion.select_RejectReason_Default();
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Scanline.verify_Seq_Changed(fourthSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 50)
    public void PT3696_Step11_To_12_SL2_Reject_SixthSeq() throws Exception {

        sixthSequence = DataCompletion_Scanline.return_Current_SeqNumber();
        DataCompletion.click_reject();
        DataCompletion.select_RejectReason_Default();
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Scanline.verify_Seq_Changed(sixthSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 60)
    public void PT3696_Step13_To_14_SL2_KeyEightSeq() throws Exception {

        eightSequence = DataCompletion_Scanline.return_Current_SeqNumber();
        DataCompletion_Scanline.enter_Text_IntoKeyingField("d256073373d107010920913c1263", true);
        DataCompletion_Scanline.verify_Seq_Changed(fourthSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 70)
    public void PT3696_Step13_To_14_SL2_EightSeq_NotRejected() throws Exception {

        DataCompletion_Scanline.select_SeqNumber_FromDropdown(eightSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 80)
    public void PT3696_Step13_To_14_SL2_Reject_EightSeq() throws Exception {

        DataCompletion.click_reject();
        DataCompletion.select_RejectReason_Default();
        DataCompletion.click_SelectReason_ForReject();
        DataCompletion_Scanline.verify_Seq_Changed(eightSequence);
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 90)
    public void PT3696_Step15_SL_RejectItem_Select_RejectedFourthSeq_RejLabel() throws Exception {
        DataCompletion_Scanline.select_SeqNumber_FromDropdown(fourthSequence);
        DataCompletion_Scanline.verify_CurrentSeq_Rejected_BySL2();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 100)
    public void PT3696_Step15_SL2_Reject_UnReject_FourthSequence() throws Exception {
        DataCompletion_Scanline.unReject_Job_IfRejected();
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 110)
    public void PT3696_Step16_SL2_RejectItem_Select_RejectedEightSeq_RejLabel() throws Exception {
        DataCompletion_Scanline.select_SeqNumber_FromDropdown(eightSequence);
        DataCompletion_Scanline.verify_CurrentSeq_Rejected_BySL2();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 120)
    public void PT3696_Step16_SL2_Reject_UnReject_EightSequence() throws Exception {
        DataCompletion_Scanline.unReject_Job_IfRejected();
        DataCompletion_Scanline.verify_CurrentSeq_NotRejected();
    }

    @Test(groups = { "all_Tests", "regression_Tests" }, priority = 130)
    public void PT3696_Step17_To_18_SL2_RejectedUnRejected_Item_AuditTrail() throws Exception {
        String header = "Audit Trail";
        int fourthSeqIndex;
        int eightSeqIndex;
        HashMap<String, String> viewFourthSeqRow = new HashMap<String, String>();
        HashMap<String, String> viewEightSeqRow = new HashMap<String, String>();

        DataCompletion.click_DoneButton();
        DataCompletion.click_Yes_ToConfirm();

        Navigation.navigate_to_Batch_ViaCommand(batchID);
        DataMaintenance_Batch.click_ViewItems();
        fourthSeqIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", fourthSequence);
        viewFourthSeqRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(fourthSeqIndex);

        DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToLeft();
        eightSeqIndex = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number", eightSequence);
        viewEightSeqRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(eightSeqIndex);
        BaseUI.baseStringPartialCompare(header,
                "unReject,sl2," + GlobalVariables.return_UserName_Formatted() + ",,Reject,sl2," + GlobalVariables.return_UserName_Formatted() + ",,",
                viewFourthSeqRow.get(header));
        BaseUI.baseStringPartialCompare(header,
                "unReject,sl2," + GlobalVariables.return_UserName_Formatted() + ",,Reject,sl2," + GlobalVariables.return_UserName_Formatted() + ",,",
                viewEightSeqRow.get(header));

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
