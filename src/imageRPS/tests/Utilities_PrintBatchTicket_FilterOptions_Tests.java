package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter;
import imageRPS.pages.Utilities_PrintTicket_SearchResults;
import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_PrintBatchTicket_FilterOptions_Tests extends baseTest {

	String clientID = "10";
	String lockboxID = "Shoptek2";
	String docGroup = "4";
	// Might switch to enum
	WorkType workType = WorkType.NSF;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.Utilities);
		Thread.sleep(100);
	}


	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3315_Utilities_PrintBatchTicket_ClientID_SearchResults() throws Exception {
		String filterName = "Client ID";
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(clientID, null, null, null);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue(filterName, clientID);
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter(filterName, clientID);
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(false);

	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3321_lockboxID() throws Exception {
		String filterName = "Lockbox ID";
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(null, lockboxID, null, null);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue(filterName, lockboxID);
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter(filterName, lockboxID);
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(false);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3321_DocGroup() throws Exception {
		String filterName = "Doc Group";
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(null, null, docGroup, null);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue(filterName, docGroup);
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter(filterName, docGroup);
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(false);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3321_WorkType() throws Exception {
		String filterName = "Work Type Description";
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(null, null, null, workType);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue(filterName, workType.getValue());
		// Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter(filterName,
		// workType.getValue());
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(false);
	}

	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3321_Utilities_PrintBatchTicket_ClientID_And_DocGroup_SearchResults() throws Exception {
		
		String client_ID = "6";
		String doc_group = "4";
		
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(client_ID, null, doc_group, null);
		
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter("Client ID",client_ID);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue("Client ID", client_ID);
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter("Doc Group", doc_group);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue("Doc Group", doc_group);
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(false);
		
	}
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3323_Utilities_PrintBatchTicket_AllFilters_SearchResults() throws Exception {
		
		String client_ID = "10";
		String lockbox_ID = "Shoptek2";
		String doc_group = "9";
		WorkType work_type = WorkType.Multiples;
		
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(client_ID, lockbox_ID, doc_group, work_type);
		
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter("Client ID",client_ID);
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter("Doc Group", doc_group);
		Utilities_PrintTicket_SearchResults.verify_FilterText_ContainsFilter("Lockbox ID", lockbox_ID);
		
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue("Client ID", client_ID);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue("Lockbox ID", lockbox_ID);
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue("Work Type Description", work_type.getValue());
		Utilities_PrintTicket_SearchResults.verify_ColumnMatches_ExpectedValue("Doc Group", doc_group);
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(false);
		
		Utilities_PrintTicket_SearchResults.click_GoBack_Button();
		Utilities_PrintTicket_BatchTicketFilter.search_ByCriteria(null, null, null, null);
		Utilities_PrintTicket_SearchResults.verify_SearchResults_Controls_AsExpected(true);
		Utilities_PrintTicket_SearchResults.verify_Headers_Appear();
		TableData results = Utilities_PrintTicket_SearchResults.return_SearchResults2();
		BaseUI.verify_true_AndLog(results.data.size() > 0, "Found Results in list.", "Did not find results in list.");
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests" })
	public void PT3321_Utilities_PrintBatchTicket_ClientID_Updates_LockboxID() throws Exception {
		String clientID = "10";
		String expected_LockboxID = "Shoptek2";
		
		Utilities_PrintTicket_BatchTicketFilter.verify_Entering_ClientID_Updates_LockboxID(clientID, expected_LockboxID);
	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (!BaseUI.elementAppears(Locator.lookupElement("util_PrintBatchFilter_Title"))) {
			Utilities_PrintTicket_SearchResults.click_GoBack_Button();
		}
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
