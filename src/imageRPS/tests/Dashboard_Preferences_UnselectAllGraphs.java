package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.Dashboard;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import org.testng.annotations.BeforeClass;
import utils.BaseUI;
import utils.Browser;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.Test;
import utils.Locator;
import utils.ResultWriter;

public class Dashboard_Preferences_UnselectAllGraphs extends baseTest {

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);

        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 10)
    public void PT3402_Unselect_All_Graphs_UserB_Displays() throws Exception {
        Dashboard.unselect_All_Dashboard_Graphs();
        Dashboard.click_Preferences_Save();
        Dashboard.signout_Relogin_Second_User();
        Dashboard.verify_All_Dashboard_Charts_Display();
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 20)
    public void PT3402_Unselect_All_Graphs_UserA_NoDisplay() throws Exception {
        Dashboard.signout_Relogin_Primary_User();
        Dashboard.verify_All_Dashboard_Charts_DoNot_Display();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        try {
            Dashboard.select_All_Dashboard_Graphs();
            Dashboard.click_Preferences_Save();

        } finally {
            Browser.closeBrowser();
        }
    }
}










