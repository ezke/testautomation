package imageRPS.tests;

import java.util.HashMap;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.DataCompletion_Account;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.Help;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_INS_Action_Tests extends baseTest {

	
	String keyingJob = "ins";
	//123456-123460
	//String batchID = "700152";
	String batch = "";
	String timestamp = "";
	
	
	Integer maxValue;
	TableData batchInfo;
	HashMap<String, String> jobInfo;
	String clientID;
	String docID;
	String seq;
	
	
	Integer index_Of_Field = null;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batch = BaseUI.random_NumberAsString(700316, 700320);

		Browser.openBrowser(GlobalVariables.baseURL);
		batchInfo = Navigation.return_ConfigData();

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		
		DataCompletion.verify_CorrectJob(keyingJob);

		
		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion_Account.return_first_doc();
		seq = DataCompletion_Account.return_first_seq();
		batch = DataCompletion.return_BatchID();
		
		
		
		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT3684_INS_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT3684_INS_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
	public void PT3684_INS_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 40)
	public void PT3684_INS_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 50)
	public void PT3684_INS_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 60)
	public void PT3684_INS_GrayButton_Displayed() throws Exception {
		DataCompletion.verify_Gray_ButtonEnabled();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 70)
	public void PT3684_INS_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 80)
	public void PT3684_INS_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 130)
	public void PT3684_INS_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 140)
	public void PT3684_INS_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 150)
	public void PT3684_INS_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 151)
	public void PT3684_INS_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 160)
	public void PT3684_INS_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 170)
	public void PT3684_INS_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 180)
	public void PT3684_INS_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 181)
	public void PT3684_INS_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 182)
	public void PT3684_INS_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);
		
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 183)
	public void PT3684_INS_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 184)
	public void PT3684_INS_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 190)
	public void PT3684_INS_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}
	
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 206)
	public void PT3684_INS_RedactButton_Displayed() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_RedactButton"));
		BaseUI.verifyElementEnabled(Locator.lookupElement("dataComplete_RedactButton"));
	}
	
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 220)
	public void PT3684_INS_NoCD_GoesToNextSeq() throws Exception {
		String previousSeq;
		String newSeq;
		previousSeq = DataCompletion_Account.return_last_seq();
		
		Integer minValue = DataCompletion_Account.return_CurrentMinValue(clientID, batchInfo);
		DataCompletion_Account.key_With_MinValue_GenericValue_DontHitEnter(minValue);
		DataCompletion.click_NoCD();
		newSeq = DataCompletion_Account.return_last_seq();
		
		
		BaseUI.verify_true_AndLog(Integer.parseInt(previousSeq) < Integer.parseInt(newSeq),
				"New Sequence number is larger than old Sequence Number.", 
				"New Sequence number of " + newSeq + " was NOT larger than " + previousSeq);
	}
	

//	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 230)
//	public void PT3684_INS_Over_ToggleOff() throws Exception {
//		DataCompletion_SLC.set_Over_On_Or_Off(false);
//		DataCompletion.verify_Button_NOT_Toggled(Locator.lookupElement("dataComplete_OverButton"));
//	}
//	
//	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 231)
//	public void PT3684_INS_Over_ToggleOn() throws Exception {
//		DataCompletion_SLC.set_Over_On_Or_Off(true);
//		DataCompletion.verify_Button_Toggled(Locator.lookupElement("dataComplete_OverButton"));
//	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 240)
	public void PT3684_INS_Cycle_ToggleOn_Cycle_SectionAppears() throws Exception {
		DataCompletion.set_Cycle_On_Or_Off(true);
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_SLC_CycleSection"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 241)
	public void PT3684_INS_Cycle_ToggleOn_Cycle_CurrentDoc_InList() throws Exception {
		TableData cycleRows = DataCompletion.return_CycleSection_Table();
	
		Integer tableRowThatMatches = null;
		seq = DataCompletion_Account.return_last_seq();
		tableRowThatMatches = cycleRows.first_IndexOf_DoubleMatch("Seq", seq, "Doc ID", docID);
		BaseUI.verify_true_AndLog(tableRowThatMatches != null, "Found a matching row.", "Could not find a matching row.");
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 242)
	public void PT3684_INS_Cycle_ToggleOff_CycleSection_Disappears() throws Exception {
		DataCompletion.set_Cycle_On_Or_Off(false);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_SLC_CycleSection"));
	}
	
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 260)
	public void PT3684_INS_Done_ModalVisible() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 261)
	public void PT3684_INS_Done_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}
	
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 300)
	public void PT3684_INS_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct("act");
	}
	
	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 301)
	public void PT3684_NavigateBack_AndUnlock_Batch() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
	}
	
		
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchInfo = null;
		jobInfo = null;
		Browser.closeBrowser();
	}
	
	
	
	
	
}//End of Class
