package imageRPS.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_CPR;
import imageRPS.pages.DataCompletion_DOC;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Batch;
import imageRPS.pages.DataMaintenance_Batch_ViewItems;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_CPR_BatchCaptured_RejectItemAndValidate extends baseTest {

	// string for story 680/////
	String docGrp = "1";
	String jobId = "CPR";
	String date = "02/03/2015";
	String tranList = "Transaction List";
	String batch = "";
	String clientID;
	String seqNum;
	String reasonMsg = "Batch Captured as Wrong Client";
	String seqNum1 = "11";
	String tranNun = "0";
	String chkCount = "0";
	String StbCount1 = "1";
	String keyingJob = "cca";
	String rejectOptn = "test";

	String checkTotal = "0.00";
	String itemCount = "20";
	String sourceItemCount = "20";
	String checCount = "10";
	String rejectCheckCount = "1";
	String rejectStubCount = "1";
	String stubCount = "10";
	String stubTotal = "0.00";
	String tranCount = "10";
	String sourceTranCount = "10";
	String GoodBalanceStubCount = "9";

	String batchNumber = "9";
	String docGroup = "1";
	String docGpsName = "Singles Skip P2 P&E to 77";
	String lockBoxId = "202020";
	String clientName = "Bevent Insurance";
	String soursebatchId = "";
	String batchType = "Normal";
	String batchDate = "02/03/2015 01:48 pm";
	String workType = "1 Singles";
	String workFlow = "CPR Testing";
	String balance = "No";
	String priority = "8";
	String cutoffDate = "";
	String trayId = "";

	String[] seqNum5 = { "11", "test seq# 11" };
	String[] seqNum6 = { "12", "test seq# 11" };

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);
		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		batch = BaseUI.random_NumberAsString(700246, 700250);
		// batch = "700249";

		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		// seqNum = DataCompletion_DOC.return_first_Seq();

		clientID = DataCompletion.return_ClientID();
		batch = DataCompletion.return_BatchID();
		seqNum = DataCompletion_DOC.return_first_Seq();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3845_CPR_Verify_DocJob_IsPresent() throws Exception {
		DataCompletion.verify_CorrectJob(keyingJob);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 1)
	public void PT3845_CPR_Verify_ClientId_IsPresent() throws Exception {
		String expextedSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
		BaseUI.baseStringCompare("dataComplete_DOC_first_Seq", seqNum, expextedSeqNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 10)
	public void PT3845_CCA_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 36)
	public void PT3845_Verify_ConfirmDialogBoxMsg() throws Exception {

		DataCompletion.modalDialog_Click_RepairDescription_CaptureAsWrongClient();
		DataCompletion.modalDialog_Click_SelectReason_Button();
		DataCompletion.modalDialog_RepairDescription_SendBatchTo_CPR();

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 50)
	public void PT3845_Verify_RepairDescription() throws Exception {
		String expectecBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchId"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchId", batch, expectecBatchId);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 51)
	public void PT3845_Verify_tranNun() throws Exception {

		String expectecTranNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum"));
		BaseUI.baseStringPartialCompare("dataComplete_TranNum", tranNun, expectecTranNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 52)
	public void PT3845_Verify_tranList() throws Exception {

		String expectecTranList = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranList"));
		BaseUI.baseStringPartialCompare("dataComplete_TranList", tranList, expectecTranList);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 53)
	public void PT3845_Verify_reasonMsg() throws Exception {

		String expectecReasonMsg = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ReasonMsg"));
		BaseUI.baseStringPartialCompare("dataComplete_ReasonMsg", reasonMsg, expectecReasonMsg);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 54)
	public void PT3845_Verify_JobItem() throws Exception {

		DataCompletion_CPR.verify_JobItem(clientID, batch, date, jobId, docGrp);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT3845_Verify_RejectBtn_IsPresent() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Reject_Btn"));

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public void PT3845_Step8_Verify_Reject_MsgBox_Values() throws Exception {

		String expectedtran2 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TranNum"));
		String transNum = "0";
		BaseUI.baseStringPartialCompare("dataComplete_TranNum2", transNum, expectedtran2);

		String ChkCount = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CheckCountRejectBatch"));
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount, ChkCount);

		String StbCount = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CheckCountRejectBatch_Stb"));
		BaseUI.baseStringCompare("dataComplete_StubCount0", StbCount1, StbCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 71)
	public void PT3845_Step8_Verify_TranNum6_Img1() throws Exception {

		String tranNum6 = "6";
		DataCompletion_CPR.click_BatchBy_BatchNumAndTran(batch, tranNum6);
		// DataCompletion_CPR.click_dataCompleteCPR_Tran6_Img1();
		String NewSeqNum1 = DataCompletion_CPR.return_SeqID();
		DataCompletion_CPR.verify_dataComplete_Pass1SequenceNumber(seqNum1, NewSeqNum1);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 170)
	public void PT3845_Verify_RejectItem_UpdatedTran() throws Exception {

		DataCompletion_CPR.click_dataComplete_RejectBtn();
		DataCompletion_CPR.select_RejectItem_IsSelected();
		DataCompletion_CPR.select_dataComplete_RejectDialogBox_Option(rejectOptn);
		DataCompletion_CPR.click_DataComplete_Reject_DialogBox_OkBtn();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 200)
	public void PT3845_Step11_Verify_CheckIn_UpdateBatch() throws Exception {

		DataCompletion_CPR.verify_CheckIn_UpdateBatch();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 210)
	public void PT3845_Verify_CheckIn_CloseDialog() throws Exception {

		DataCompletion_CPR.verify_CheckIn_CloseDialog();

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 220)
	public void PT3845_CPR_Verify_SLC_Job_IsPresent() throws Exception {

		DataCompletion_CPR.clickOk_CheckIn_CloseDialog();
		DataCompletion.verify_CorrectJob(keyingJob);
		String expextedSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
		BaseUI.baseStringCompare("dataComplete_DOC_first_Seq", seqNum, expextedSeqNum);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 220)
	public void PT3845_CPR_Verify_SeqNum_IsPresent() throws Exception {

		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);

	}

	@Test(groups = { "all_Tests", "critical_Tests", "smoke_Tests" }, priority = 230)
	public void PT3845_DataCompletionPage_BasicInfo_BatchNumber_TextPresent() throws Exception {

		Navigation.enter_Text_Into_CommandBox_AndNavigate("Batch " + batch);
		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_BatchNumber();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_BatchNumber_TextPresent", batchNumber, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_BatchId_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_BatchId();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_BatchId_TextPresent", batch, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_DocGroupId_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_DocGroupId();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_DocGroupId_TextPresent", docGroup, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_ClientId_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_ClientId();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_ClientId_TextPresent", clientID, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_LockboxId_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_LockboxId();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_LockboxId_TextPresent", lockBoxId, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_ClientName_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_ClientName();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_ClientName_TextPresent", clientName, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_SourceBatchId_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_SourceBatchId();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_SourceBatchId_TextPresent", batch, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_BatchType_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_BatchType();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_BatchType_TextPresent", batchType, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_BatchDate_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_BatchDate();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_BatchDate_TextPresent", batchDate, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_WorkFlow_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_WorkFlow();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_WorkFlow_TextPresent", workFlow, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_Balanced_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_Balanced();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_Balanced_TextPresent", balance, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_KeyingJob_TextPresent() throws Exception {
		String newkeyingJob = "cca";
		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_KeyingJob();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_KeyingJob_TextPresent", newkeyingJob, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_Priority_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_Priority();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_Priority_TextPresent", priority, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_CutoffDateTime_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_CutoffDateTime();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_CutoffDateTime_TextPresent", cutoffDate, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 236)
	public void PT3845_DataCompletionPage_BasicInfo_TrayId_TextPresent() throws Exception {

		String expecteValue = DataMaintenance.return_dataMaint_BasicInfo_TrayId();
		BaseUI.baseStringCompare("dataMaint_BasicInfo_TrayId_TextPresent", trayId, expecteValue);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_KeyBatch_Information() throws Exception {

		String expectedchkTotal = DataCompletion_CPR.return_CountInfo_checkTotal();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ChkTotal", checkTotal, expectedchkTotal);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_ItemCount() throws Exception {

		String expectedItemCount = DataCompletion_CPR.return_CountInfo_ItemCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ItemCount", itemCount, expectedItemCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_sourceItemCount() throws Exception {

		String expectedsourceItemCount = DataCompletion_CPR.return_CountInfo_SourceItemCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_sourceItemCount", sourceItemCount, expectedsourceItemCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_checCount() throws Exception {

		String expectedchecCount = DataCompletion_CPR.return_CountInfo_ChecCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_ChkCount", checCount, expectedchecCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_rejectCheckCount() throws Exception {

		String expectedrejectCheckCount = DataCompletion_CPR.return_CountInfo_RejectCheckCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_rejectCheckCount", rejectCheckCount, expectedrejectCheckCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_rejectStubCount() throws Exception {

		String expectedrejectStubCount = DataCompletion_CPR.return_CountInfo_RejectStubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_rejectStubCount", rejectStubCount, expectedrejectStubCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_stubCount() throws Exception {

		String expectedstubCount = DataCompletion_CPR.return_CountInfo_StubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_stubCount", stubCount, expectedstubCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_StubTotal() throws Exception {

		String expectedstubTotal = DataCompletion_CPR.return_CountInfo_StubTotal();
		BaseUI.baseStringCompare("dataMaint_CountInfo_stubTotal", stubTotal, expectedstubTotal);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_TranCount() throws Exception {

		String expectedtranCount = DataCompletion_CPR.return_CountInfo_TranCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", tranCount, expectedtranCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_SourceTranCount() throws Exception {

		String expectedsourceTranCount = DataCompletion_CPR.return_CountInfo_SourceTranCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", sourceTranCount, expectedsourceTranCount);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 237)
	public void PT3845_DataCompletionPage_CountInfo_GoodBalanceStubCount() throws Exception {

		String expectedGoodBalanceStubCount = DataCompletion_CPR.return_CountInfo_GoodBalanceStubCount();
		BaseUI.baseStringCompare("dataMaint_CountInfo_tranCount", GoodBalanceStubCount, expectedGoodBalanceStubCount);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 500)
	public void PT3845_DataCompletionPage_Verify_BatchItem_RejectJob11() throws Exception {

		DataMaintenance_Batch.click_ViewItems();

		String[] seqNum1 = { "11", "cpr" };
		String seq1 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(11, "P1 Seq Number");
		String transeq1 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(11, "Reject Job");
		String[] expectedNum1 = { seq1, transeq1 };
		BaseUI.baseStringCompareArray(seqNum1, expectedNum1);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 501)
	public void PT3845_DataCompletionPage_Verify_BatchItem_RejectJob12() throws Exception {

		String[] seqNum2 = { "12", "cpr" };
		String seq2 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(12, "P1 Seq Number");
		String transeq2 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(12, "Reject Job");
		String[] expectedNum2 = { seq2, transeq2 };
		BaseUI.baseStringCompareArray(seqNum2, expectedNum2);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 502)
	public void PT3845_DataCompletionPage_Verify_BatchItem_TranRejectJob11() throws Exception {

		String[] seqNum3 = { "11", "cpr" };
		String seq3 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(11, "P1 Seq Number");
		String transeq3 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(11,
				"Tran Reject Job");
		String[] expectedNum3 = { seq3, transeq3 };
		BaseUI.baseStringCompareArray(seqNum3, expectedNum3);
	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 503)
	public void PT3845_DataCompletionPage_Verify_BatchItem_TranRejectJob12() throws Exception {

		String[] seqNum4 = { "12", "cpr" };
		String seq4 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(12, "P1 Seq Number");
		String transeq4 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(12,
				"Tran Reject Job");
		String[] expectedNum4 = { seq4, transeq4 };
		BaseUI.baseStringCompareArray(seqNum4, expectedNum4);

	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 504)
	public void PT3845_DataCompletionPage_Verify_BatchItem_RejectReason11() throws Exception {

		String seq5 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(11, "P1 Seq Number");
		String seq6 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(12, "P1 Seq Number");
		DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToRight();
		String transeq5 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(11,
				"Reject Reason");
		String[] expectedVal1 = { seq5, transeq5 };
		BaseUI.baseStringCompareArray(seqNum5, expectedVal1);

		String transeq6 = DataMaintenance_Batch_ViewItems.return_CellText_ByRowNumber_And_HeaderText(12,
				"Reject Reason");
		String[] expectedVal2 = { seq6, transeq6 };
		BaseUI.baseStringCompareArray(seqNum6, expectedVal2);

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}
