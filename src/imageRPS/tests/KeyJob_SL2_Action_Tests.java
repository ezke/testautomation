package imageRPS.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.DataCompletion_Scanline;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.Help;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_SL2_Action_Tests extends baseTest {
	String keyingJob = "sl2";
	String batchID = "";
	String timestamp = "";

	Integer maxValue;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	String clientID;
	String docID;
	String seq;

	Integer index_Of_Field = null;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		batchID = BaseUI.random_NumberAsString(123467, 123479);
		batchID = "123471";

		Browser.openBrowser(GlobalVariables.baseURL);
		batchStuff = Navigation.return_ConfigData();

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
		DataCompletion.verify_CorrectJob(keyingJob);

		clientID = DataCompletion.return_ClientID();
		docID = DataCompletion.return_DocID();
		jobInfo = batchStuff.return_Row_BasedOn_2MatchingFields("client_id", clientID, "doc_id", docID);
		maxValue = Integer.parseInt(jobInfo.get("field_len"));

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();
		}
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 10)
	public void PT3726_SL2_ZoomIn_And_ZoomOut_Works() throws Exception {
		DataCompletion.verify_ZoomIn_And_ZoomOut_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 20)
	public void PT3726_SL2_Flip_Works() throws Exception {
		DataCompletion.verify_Flip_Back_And_Front_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 30)
	public void PT3726_SL2_Rotate_Clockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_Clockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 40)
	public void PT3726_SL2_Rotate_CounterClockwise_90Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_CounterClockwise90Degrees_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 50)
	public void PT3726_SL2_Rotate_180Degrees_Works() throws Exception {
		DataCompletion.verify_Rotate_180Degrees_Works();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 60)
	public void PT3726_SL2_Gray_Button() throws Exception {
		DataCompletion.verify_Gray_Button_ChangesImage();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 61)
	public void PT3726_SL2_BW_Button() throws Exception {
		DataCompletion.verify_BW_Button_ChangesImage();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 70)
	public void PT3726_SL2_Lock_ModalAppears() throws Exception {
		DataCompletion.click_LockButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Lock);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 80)
	public void PT3726_SL2_Lock_No_DoesNotLock() throws Exception {
		DataCompletion.click_No_ToConfirm();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 130)
	public void PT3726_SL2_Reject_ModalDisplayed() throws Exception {
		DataCompletion.click_reject();
		DataCompletion.verify_Reject_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 140)
	public void PT3726_SL2_Reject_Cancel_ModalNotDisplayed() throws Exception {
		DataCompletion.click_Cancel_Reject();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 150)
	public void PT3726_SL2_Susp_ModalVisible() throws Exception {
		DataCompletion.click_Susp();
		DataCompletion.verify_Susp_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 151)
	public void PT3726_SL2_Susp_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_Susp();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 160)
	public void PT3726_SL2_CPR_ModalVisible() throws Exception {
		DataCompletion.click_CPR_Button();
		DataCompletion.verify_CPR_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 170)
	public void PT3726_SL2_CPR_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_CPR();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_CPR_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 180)
	public void PT3726_SL2_OperPref_ModalVisible() throws Exception {
		DataCompletion.click_OperPref_Button();
		DataCompletion.verify_OperPref_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 181)
	public void PT3726_SL2_OperPref_AbleTo_Check_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(true);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(true);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 182)
	public void PT3726_SL2_OperPref_AbleTo_Uncheck_DefaultToZoomed_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(false);
		DataCompletion.verify_DefaultZoomed_Is_Checked_OR_Unchecked(false);

	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 183)
	public void PT3726_SL2_OperPref_AbleTo_Uncheck_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(false);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(false);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 184)
	public void PT3726_SL2_OperPref_AbleTo_Check_SavePreferences_Checkbox() throws Exception {
		DataCompletion.check_Or_uncheck_OperPref_SavePreferences_Checkbox(true);
		DataCompletion.verify_SavePreferences_Is_Checked_OR_Unchecked(true);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 190)
	public void PT3726_SL2_OperPref_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_OperPref();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_OperPref_Dialog"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 200)
	public void PT3726_SL2_OCR_Changes_KeyingTextBox() throws Exception {
		DataCompletion.rotate_Image_ToNoRotation();
		DataCompletion_Scanline.verify_OCR_Button_Works();
		// DataCompletion_SLC.verify_OCR_Button_DoesNOTWork("ocr: region width zero (0)
		// or height is zero (0)");
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 205)
	public void PT3726_SL2_Rope_Appears() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Rope_Button"));

		// DataCompletion.click_Rope();
		// DataCompletion.verify_Button_Toggled(Locator.lookupElement("dataComplete_Rope_Button"));
		// DataCompletion.verify_Button_NOT_Toggled(Locator.lookupElement("dataComplete_OCR_Button"));
		// DataCompletion.verify_Button_NOT_Toggled(Locator.lookupElement("dataComplete_Pan_Button"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 206)
	public void PT3726_SL2_Pan_Appears() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Pan_Button"));

		// DataCompletion.click_Pan();
		// DataCompletion.verify_Button_Toggled(Locator.lookupElement("dataComplete_Pan_Button"));
		// DataCompletion.verify_Button_NOT_Toggled(Locator.lookupElement("dataComplete_OCR_Button"));
		// DataCompletion.verify_Button_NOT_Toggled(Locator.lookupElement("dataComplete_Rope_Button"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 210)
	public void PT3726_SL2__Msrd_ToggleOn_UnableToKey() throws Exception {
		String expectedValue = "";

		DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
		DataCompletion_Scanline.set_Msrd_On_Or_Off(true);
		DataCompletion_Scanline.enter_Text_IntoKeyingField("12345", true);
		DataCompletion_Scanline.verify_KeyingField_Text_MatchesExpected(expectedValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 211)
	public void PT3726_SL2_Msrd_ToggleOff_AbleToKey() throws Exception {
		String expectedValue = "1234567";

		DataCompletion_Scanline.set_Msrd_On_Or_Off(false);
		DataCompletion_Scanline.enter_Text_IntoKeyingField("", true);
		DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
		DataCompletion_Scanline.verify_KeyingField_Text_MatchesExpected(expectedValue);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 219)
	public void PT3726_SL2_NoCD_KeyWith_InvalidValues() throws Exception {

		DataCompletion_Scanline.key_With_MaxValue_GenericValue_DONT_HitEnter(maxValue);

		DataCompletion.click_NoCD();
		DataCompletion.verify_ErrorMessage("Field: RT, Err: Incorrect Length");
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 220)
	public void PT3726_SL2_NoCD_GoesToNextSeq() throws Exception {
		String previousSeq;
		String newSeq;
		String valueToKey = "d120697445d97656304c4393";
		DataCompletion_Scanline.enter_Text_IntoKeyingField(valueToKey, false);

		previousSeq = DataCompletion_Scanline.return_Current_SeqNumber();
		DataCompletion.click_NoCD();
		newSeq = DataCompletion_Scanline.return_Current_SeqNumber();

		BaseUI.verify_true_AndLog(Integer.parseInt(previousSeq) < Integer.parseInt(newSeq),
				"New Sequence number is larger than old Sequence Number.",
				"New Sequence number of " + newSeq + " was NOT larger than " + previousSeq);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 230)
	public void PT3726_SL2_Over_ToggleOff() throws Exception {
		DataCompletion_Scanline.set_Over_On_Or_Off(false);
		DataCompletion.verify_Button_NOT_Toggled(Locator.lookupElement("dataComplete_OverButton"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 231)
	public void PT3726_SL2_Over_ToggleOn() throws Exception {
		DataCompletion_Scanline.set_Over_On_Or_Off(true);
		DataCompletion.verify_Button_Toggled(Locator.lookupElement("dataComplete_OverButton"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 240)
	public void PT3726_SL2_Cycle_ToggleOn_Cycle_SectionAppears() throws Exception {
		DataCompletion.set_Cycle_On_Or_Off(true);
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_SLC_CycleSection"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 241)
	public void PT3726_SL2_Cycle_ToggleOn_Cycle_CurrentDoc_InList() throws Exception {
		TableData cycleRows = DataCompletion.return_CycleSection_Table();

		Integer tableRowThatMatches = null;
		seq = DataCompletion_Scanline.return_Current_SeqNumber();
		tableRowThatMatches = cycleRows.first_IndexOf_DoubleMatch("Seq", seq, "Doc ID", docID);
		BaseUI.verify_true_AndLog(tableRowThatMatches != null, "Found a matching row.",
				"Could not find a matching row.");
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 242)
	public void PT3726_SL2_Cycle_ToggleOff_CycleSection_Disappears() throws Exception {
		DataCompletion.set_Cycle_On_Or_Off(false);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_SLC_CycleSection"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 250)
	public void PT3726_SL2_DocId_ModalVisible() throws Exception {
		DataCompletion.click_DocId();
		DataCompletion.verify_DocId_Modal_Visible();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 251)
	public void PT3726_SL2_DocId_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_Cancel_DocId();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_DocId_Modal"));
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 260)
	public void PT3726_SL2_Done_ModalVisible() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 261)
	public void PT3726_SL2_Done_Cancel_Modal_NOT_Visible() throws Exception {
		DataCompletion.click_No_ToCancel();
		DataCompletion.verify_ConfirmModal_NotDisplayed();
	}

	@Test(groups = { "all_Tests", "regression_Tests" }, priority = 300)
	public void PT3726_SL2_HelpButton_Works() throws Exception {
		DataCompletion.click_HelpButton();
		Help.verify_HelpTopic_Correct("slc");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchStuff = null;
		jobInfo = null;

		try {
			Navigation.navigate_toTab(TopNavigationTab.DataCompletion);
			DataCompletion.click_DoneButton();
			DataCompletion.click_Yes_ToConfirm();
		} finally {

			Browser.closeBrowser();
		}
	}

}// End of Class
