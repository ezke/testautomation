package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class SessionManagement_Login_CloseWebBrowserChrome extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 10)
    public void PT4406_TopMenuSecurity_Login_CheckDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 20)
    public void PT4406_TopMenuSecurity_ValidateCloseBrowserObserveLoginPgMsg() throws Exception {
        Browser.closeBrowser();
        Browser.openBrowser(GlobalVariables.baseURL);
        BaseUI.verify_true_AndLog(BaseUI.pageSourceNotContainsString("You are currently logged in as SM Auto1, but you requested a resource that you were not authorized for. Either provide credentials that do not have access or contact your administrator to grant you access."), "Text not found", "Text found");

    }
    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}//End of Class

