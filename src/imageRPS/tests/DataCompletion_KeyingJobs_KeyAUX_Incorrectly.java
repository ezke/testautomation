package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.openqa.selenium.Keys;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_KeyAUX_Incorrectly extends baseTest {

    private String batchID = "";
    private String keyingJob = "aux";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        batchID = BaseUI.random_NumberAsString(702201, 702211);
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
        Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batchID);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 10)
    public void PT3719_AUX_Verify_WarningError() throws Exception {

//      Step8
        String expectedValue = "123456789012345678900";
        String expectedError = "Input too long";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_ErrorMessage(expectedError);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 20)
    public void PT3719_AUX_Verify_Seq_And_FieldName_Text() throws Exception {

//      Step9
        String expectedValue = "1";
        String expectedFieldName = "Auxiliary On-Us Completion";
        String expectedSeq = "verify";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_Aux_FieldName_ByIndex(1, expectedFieldName);
        DataCompletion.verify_Aux_Seq_ByIndex(1, expectedSeq);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 30)
    public void PT3719_AUX_Verify_Keying_DoesNot_Match_ErrorMessage() throws Exception {

//      Step10
        String expectedValue = "2";
        String expectedError = "Account number does not match first keying. Press '/' to rekey.";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_ErrorMessage(expectedError);
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 40)
    public void PT3719_AUX_Verify_Keying_ForwardSlash_Clears_Field() throws Exception {

//      Step11
        BaseUI.enterKey_DontUseClear(Locator.lookupElement("dataComplete_SLC_visibleEntryField"), Keys.DIVIDE);
        String keyingField = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_SLC_visibleEntryField"));
        BaseUI.verify_true_AndLog(keyingField.length()== 0, "Keying Field was empty", "Keying Field was NOT empty");
    }

    @Test(groups = { "all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs" }, priority = 50)
    public void PT3719_AUX_Verify_Keying_Match_And_NavigatesTo_NextSeq() throws Exception {

//      Step12-13
        String expectedValue = "1";
        String expectedSeq3 = "3";
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion_Scanline.enter_Text_IntoKeyingField(expectedValue, true);
        DataCompletion.verify_Aux_Seq_ByIndex(2, expectedSeq3);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);

    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }


}
