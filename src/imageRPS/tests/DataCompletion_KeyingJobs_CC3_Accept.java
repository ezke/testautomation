package imageRPS.tests;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class DataCompletion_KeyingJobs_CC3_Accept extends baseTest {

    private String keyingJob = "cca";
    private String batch = "702755";
    private String openApostropheUnicode = "\u2019";
    private String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.03).\n" +
            "Please reject the item if you can" + openApostropheUnicode + "t read it.";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);

        Navigation.enter_Text_Into_CommandBox_AndNavigate("Key " + batch);
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 10)
    public void PT3755_Step6_To_7_CCA_KeyTo_CC2_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("1");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc2";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 20)
    public void PT3755_Step8_To_9_CC2_KeyTo_CC3_VerifyJob() throws Exception {

        DataCompletion_Balancing.key_through_Batch("2");
        DataCompletion.click_Yes_ToConfirm();
        keyingJob = "cc3";
        DataCompletion.verify_CorrectJob(keyingJob);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 30)
    public void PT3755_Step10_CC3_VeifySeq_And_KeyingField() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        String keyingField = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"));
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
        BaseUI.verify_true_AndLog(keyingField.length() == 0, "Keying Field was empty", "Keying Field was NOT empty");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 40)
    public void PT3755_Step11_CC3_VeifyConfirmMessage() throws Exception {

        DataCompletion_Balancing.enterText_Into_LastInputBox("3");
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
        DataCompletion.verify_ConfirmModal_Buttons_Appears();
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3755_Step12_CC3_Accept_VerifySequence() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
        DataCompletion_Balancing.verify_NOT_Rejected(1);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3762_Step13_CC3_Verify_DifferentValue() throws Exception {

        DataCompletion_Balancing.enterText_Into_InputBox_ByIndex("4", 1);
        DataCompletion_Balancing.verify_Seq_ByIndex(1, "verify");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3762_Step14_CC3_Verify_DifferentValue_ConfirmMessage() throws Exception {

        String expectedMessage = "This item has been keyed with 3 different values ($0.01,$0.02,$0.04).\n" +
                "Please reject the item if you can" + openApostropheUnicode + "t read it.";
        DataCompletion_Balancing.enterText_Into_InputBox_ByIndex("4", 1);
        DataCompletion.verify_ConfirmModal_Appears(expectedMessage);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_DataCompletion_KeyingJobs"}, priority = 50)
    public void PT3762_Step15_CC3_Verify_DifferentValue_Accept() throws Exception {

        DataCompletion.click_ConfirmModal_Accept_Button();
        DataCompletion_Balancing.verify_Seq_ByIndex(2, "4");
        DataCompletion_Balancing.verify_NOT_Rejected(1);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}
