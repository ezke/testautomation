package imageRPS.tests;

import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.data.baseTest;
import imageRPS.pages.Alerts;
import imageRPS.pages.Dashboard;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_Account;
import imageRPS.pages.DataCompletion_Balancing;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class Dashboard_AlertCountGraph_KeyBatch_ValidateAlertInGraph_Tests extends baseTest {

	String keyingJob = "cca";
	String batch = "";
	String valueToEnter = "";
	String seqNumber = "";
	String genericValue = "2000";
	String docType = "";

	String clientID;
	String docID;

	Integer index_Of_Field = null;
	TableData batchStuff;
	HashMap<String, String> jobInfo;
	String alertLabelText = "More than 'x' Items require Job 'y'";

	String timeOfAlert = "";
	Integer id = 101;
	String alert = "ManyItem";
	String category = "Compl";
	String level = "Alert";
	Double alertCurrent = 10.00;
	Double alertThreshold = 1.00;
	String warningTime = "";
	Double warningCurrent = 0.00;
	Double warningThreshold = 500.00;
	Integer average = 0;
	Boolean maxTrigger = true;
	String message = "Job pyr has too many items";

	// Integer events_SequenceNumber
	// Integer events_severity = 1;
	// Integer events_StationId = 9004;
	// String events_OperatorId = "DynOp004";
	String events_Message = "Warning: More than 1 Items for Job=pyr. (Rule:101-Job pyr has too many items)";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		batch = BaseUI.random_NumberAsString(700311, 700315);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3514_Verify_AlertGraph_NoXYAlerts() throws Exception {
		Dashboard.verify_Alert_Bars_Empty_By_BarLabelText(alertLabelText);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3514_TakenTo_CCA() throws Exception {
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		DataCompletion.verify_CorrectJob(keyingJob);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT3514_Key_CCA_To_PYR() throws Exception {
		timeOfAlert = BaseUI.getDateAsString_InRelationToTodaysDate(0, Alerts.alertsTimeFormat);
		DataCompletion_Balancing.key_through_Batch("2000");
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_CorrectJob("pyr");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT3514_Verify_Alert_NotEmpty_For_MoreThanXItemsRequireJobY() throws Exception {
		DataCompletion.click_DoneButton();
		DataCompletion.click_Yes_ToConfirm();
		Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		Dashboard.refresh_And_Wait_ForREDAlertGraph_ValueToBeGreaterThan0(alertLabelText);
		Dashboard.verify_AlertGraph_AlertGreaterThan0(alertLabelText, "RED");
		Dashboard.verify_Alert_Bar_Empty_By_BarLabelText_AndColor(alertLabelText, "YELLOW");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 25)
	public void PT3514_Dashboard_ClickOn_Alert_AndVerifyTable() throws Exception {
		Dashboard.click_AlertGraphBar_ByLabelText_AndColor(alertLabelText, "RED");
		TableData alertsList = Alerts.return_AlertList_Table();
		alertsList.verify_TableSize(1);
		Alerts.verify_Level_Alert_ColorRED_ByIndex(1);
		Alerts.verify_alerts_FirstRow(alertsList, id, alert, category, level, timeOfAlert, alertCurrent, alertThreshold,
				warningTime, warningCurrent, warningThreshold, average, maxTrigger, message);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 26)
	public void PT3008_Alerts_DetailsButtonDisabled() throws Exception {
		Alerts.select_AlertsListTable_FirstRow();
		Navigation.verify_ButtonDisabled(Locator.lookupElement("alerts_DetailsButton"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 27)
	public void PT3008_Alerts_RelatedEvents() throws Exception {
		Alerts.click_RelatedEvents_Button();
		TableData eventsList = Alerts.return_EventsList_TableData();
		String reformattedTime = BaseUI.return_Date_AsDifferentFormat(timeOfAlert, Alerts.alertsTimeFormat,
				Alerts.eventsListFormat);

		Alerts.verify_EventsList_Table_FirstRow(eventsList, reformattedTime, events_Message);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT3514_KeyToBal() throws Exception {
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		String clientID = DataCompletion.return_ClientID();
		DataCompletion_Account.key_allFields_UntilDone(clientID);
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion.verify_CorrectJob("bal");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 35)
	public void PT3514_VerifyAlertGraph_EmptyFor_MoreThanXItemsRequireJobY() throws Exception {
		Navigation.navigate_toTab(TopNavigationTab.Dashboard);
		Dashboard.refresh_And_Wait_ForREDAlertGraph_ToBeEmpty(alertLabelText);
		Dashboard.verify_Alert_Bars_Empty_By_BarLabelText(alertLabelText);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		try {
			batchStuff = null;
			jobInfo = null;

		} finally {
			Browser.closeBrowser();
		}
	}

}// End of Class
