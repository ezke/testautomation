package imageRPS.tests_ignored;

import imageRPS.data.baseTest;
import imageRPS.pages.*;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Utilities_ConcurrentUsers_Refresh extends baseTest {

    WebDriver userone;
    WebDriver userTwo;
    TableData availableUsers;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);

        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_concurrentonly, GlobalVariables.password_concurrentonly);
        Navigation.navigate_toTab(Navigation.TopNavigationTab.Utilities);
        Utilities.navigate_ConcurrentUsers();
        userone = Browser.driver;
        Browser.openBrowser(GlobalVariables.baseURL);
        LoginPage.login(GlobalVariables.entity, GlobalVariables.secondLogin_Concurrentonly, GlobalVariables.secondpassword_Concurrentonly);
        userTwo = Browser.driver;
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 10)
    public void PT3382_Utilities_ConcurrentUsers_SecondUser_NotDisplayed() throws Exception {

        Browser.changeDriver(userone, Browser.currentBrowser);
        availableUsers = Utilities_ConcurrentUsers.return_userList();
        availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.login_concurrentonly);
        availableUsers.verify_Value_NOT_InColumn("Operator ID", GlobalVariables.secondLogin_Concurrentonly);
    }

    @Test(groups = {"all_Tests", "regression_Tests"}, priority = 20)
    public void PT3382_Utilities_ConcurrentUsers_SecondUser_Displayed_After_Reset() throws Exception {
        Utilities_ConcurrentUsers.click_Refresh();
        availableUsers = Utilities_ConcurrentUsers.return_userList();
        availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.login_concurrentonly);
        availableUsers.verify_Value_InColumn("Operator ID", GlobalVariables.secondLogin_Concurrentonly);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {

        availableUsers = null;
        try {
            Browser.changeDriver(userTwo, Browser.currentBrowser);
            Navigation.signOut();
            Browser.closeBrowser();
        } finally {
            Browser.changeDriver(userone, Browser.currentBrowser);
            Browser.closeBrowser();
        }
    }
}

