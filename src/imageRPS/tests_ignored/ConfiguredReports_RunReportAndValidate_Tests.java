package imageRPS.tests_ignored;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.data.baseTest;
import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_bal;
import imageRPS.pages.DataMaintenance;
import imageRPS.pages.DataMaintenance_Consolidations_Filters;
import imageRPS.pages.DataMaintenance_Consolidations_SearchResults;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.ReportsOutput;
import imageRPS.pages.ReportsOutput_Reformatter;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.Navigation.TopNavigationTab;
import imageRPS.pages.ReportsOutput_ConfiguredReports;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class ConfiguredReports_RunReportAndValidate_Tests extends baseTest {
	
	
	//NO NEED THIS tc CAN BE DELATED AS 01/19/2018

	String batch = "";
	Integer id_ToSelect = 12;

	Integer index_Of_Field = null;
	String[] reportLines;
	
	
String keyingJob = "bal";
	
	TableData searchResults;
	HashMap<String, String> searchValues = new HashMap<String, String>();
	

	
	//String consolAmtValue0 = "$0.00";
	
	
	//11/28/2018  block due to   https://www.pivotaltracker.com/n/projects/1614171/stories/153220813  
	
	
	
	String consolOpenAmt = "Open Amount";
	String consolOpenAmtsValue = "$55,500.00";
	
	String consolOpenChks = "Open Checks";
	String consolOpenChksValue = "30";
	
	
	
	

	String consolAmount1 = "Consol Amount";
	
	
	String consolChks1 = "Consol Checks";
	
	
	String consolBatch1 = "Consol Batches";
	
	String consolBatchValue2 = "2";
	
	
	String consolOpenAmtsValue2 = "$18,500.00";
	

	
	String consolOpenBatch1 = "Open Batches";

	
//////////////new story variable 530
	
	String consolOpenAmt1 = "Open Amount";
	String consolOpenAmtsValue1 = "$18,500.00";
	String	Client_ID = "603";
	String consolAmount = "Consol Amount";
	String consolAmtValue = "$18,500.00";
	String consolChks = "Consol Checks";
	String consolChksValue = "0";
	String consolChksValue603 = "10";
	String consolOpenBatch = "Open Batches";
	String consolOpenBatchValue = "0";
	
	String consolOpenChks1 = "Open Checks";
	String consolOpenChksValue1 = "0";
	
	String consolBatch = "Consol Batches";
	String consolBatchValue = "0";
	String consolChksValue1 = "10";
	
	String consolAmtValue1 = "0";
	String consolBatchValue1 = "1";
	
	
	String	Client_ID1 = "604";
	
	String firstDate = "";
	
	TableData batchInfo;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");	
	
		DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID);
		DataMaintenance_Consolidations_Filters.click_Submit();	
//
//		Navigation.navigate_toTab(TopNavigationTab.ReportsOutput);
//		Navigation.click_OK_OnError_IfExists();
//		ReportsOutput_Reformatter.click_OK_OnError_IfExists();
//		ReportsOutput.navigate_SubTab("Configured Reports");
//		ReportsOutput_ConfiguredReports.sort_Column_Ascending("ID");
//		ReportsOutput_ConfiguredReports.select_Row_By_IDColumnText(id_ToSelect);
//		ReportsOutput_ConfiguredReports.click_ViewReport();
//		reportLines = ReportsOutput_ConfiguredReports.return_TextReport_Lines();
	}

	//Navigate to Data Maintenance | Consolidations | and validate the "Open Amount", "Open Checks" and "Open Batches" for Consol 603 and 604 = $18,500.00, 10, and 1
		@Test(groups = { "all_Tests", "smoke_Tests" },priority = 1)
		public void PT4554_DataMaintenance_Verify_ConsolAmount() throws Exception {
			
				
			String expectedOpenAmount  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenAmt1);		
			BaseUI.baseStringCompare("consolOpenAmtsValue1", consolOpenAmtsValue1, expectedOpenAmount);
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 10)
		public void PT4554_DataMaintenance_Verify_ConsolChecksAmt() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolChks);		
			BaseUI.baseStringCompare("ConsolValues", consolChksValue, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 11)
		public void PT4554_DataMaintenance_Verify_ConsolBatch() throws Exception {
					
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolBatchValue, expectedconsolValue);
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" },priority = 12)
		public void PT4554_DataMaintenance_Verify_ConsolAmount604() throws Exception {
			
			//Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
			DataMaintenance.navigate_to_SubTab("Consolidations");
			DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID1);
			DataMaintenance_Consolidations_Filters.click_Submit();	
			String expectedconsolAmount  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenAmt1);		
			BaseUI.baseStringCompare("consolOpenAmtsValue1", consolOpenAmtsValue1, expectedconsolAmount);
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 13)
		public void PT4554_DataMaintenance_Verify_ConsolChecksAmt604() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolChks);		
			BaseUI.baseStringCompare("ConsolValues", consolChksValue, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 14)
		public void PT4554_DataMaintenance_Verify_ConsolBatch604() throws Exception {
					
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolBatchValue, expectedconsolValue);
			
		}
		
		//Go to Batch information for 700351, and key bal (900)
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 30)
		public void PT4554_DataMaintenance_Verify_ConsolOpenAmt() throws Exception {
			
			String batch = "700351";
			Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
			String amountToEnter =  BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
			amountToEnter = amountToEnter.replace(".", "");
			DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
			DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
			DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
			
			
			DataCompletion.click_Yes_ToConfirm();
			DataCompletion_bal.wait_ForAlert_ToAppear();
			DataCompletion_bal.click_OK_ForAlertModal();
			//DataCompletion_bal.verify_Alert_DialogPresent();
			
			
		//	String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenAmt);		
		//	BaseUI.baseStringCompare("ConsolValues", consolOpenAmtsValue, expectedconsolValue);
			
		}
		//Validate the "Keying Job" states "don" (will need to wait a min or two until QaaS processes -   Do the same thing for batch 700352
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 40)
		public void PT4554_DataMaintenance_Verify_ConsolOpenChks() throws Exception {
			
			
			
			String batch = "700352";
			Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
			String amountToEnter =  BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
			amountToEnter = amountToEnter.replace(".", "");
			DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
			DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
			DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 50)
		public void PT4554_DataMaintenance_Verify_ConsolOpenAmountkeyedJob() throws Exception {	
			
			
			DataCompletion.click_Yes_ToConfirm();
			DataCompletion_bal.wait_ForAlert_ToAppear();
			DataCompletion_bal.click_OK_ForAlertModal();
			Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
			DataMaintenance.navigate_to_SubTab("Consolidations");
			
			//need to wait for Qaas processes to completed 
			Thread.sleep(100000);
			
			DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID);
			DataMaintenance_Consolidations_Filters.click_Submit();
			String expectedconsolAmount  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenAmt1);		
			BaseUI.baseStringCompare("ConsolChecksValue", consolAmtValue1, expectedconsolAmount);
				
			//String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenBatch);		
			//BaseUI.baseStringCompare("ConsolValues", consolOpenBatchValue, expectedconsolValue);
				
		}	
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 60)
		public void PT4554_DataMaintenance_Verify_OpenCheckkeyedJob() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenChks1);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenChksValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 61)
		public void PT4554_DataMaintenance_Verify_OpenBatchkeyedJob() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenBatchValue, expectedconsolValue);
			
		}
		
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 62)
		public void PT4554_DataMaintenance_Verify_ConsolAmountkeyedJob() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolAmount);		
			BaseUI.baseStringCompare("ConsolValues", consolAmtValue, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 63)
		public void PT4554_DataMaintenance_Verify_ConsolCheckskeyedJob() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolChks);		
			BaseUI.baseStringCompare("ConsolValues", consolChksValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 64)
		public void PT4554_DataMaintenance_Verify_ConsolBatchkeyedJob() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolBatchValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 65)
		public void PT4554_DataMaintenance_Verify_OpenCheckkeyedJob604() throws Exception {		
			
			DataMaintenance_Consolidations_SearchResults.click_GoBack_Button();
			DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID1);
			DataMaintenance_Consolidations_Filters.click_Submit();
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenChks1);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenChksValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 66)
		public void PT4554_DataMaintenance_Verify_OpenBatchkeyedJob604() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenBatchValue, expectedconsolValue);
			
		}
		
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 67)
		public void PT4554_DataMaintenance_Verify_ConsolAmountkeyedJob604() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolAmount);		
			BaseUI.baseStringCompare("ConsolValues", consolAmtValue, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 68)
		public void PT4554_DataMaintenance_Verify_ConsolCheckskeyedJob604() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolChks);		
			BaseUI.baseStringCompare("ConsolValues", consolChksValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 69)
		public void PT4554_DataMaintenance_Verify_ConsolBatchkeyedJob604() throws Exception {		
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolBatchValue1, expectedconsolValue);
		
		}
		
		//Select Consol 603 and 604 and select "Change Auto Consol Time" (step 19-20)
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 70)
		public void PT4554_DataMaintenance_VerifyNewTimeIsDesiable() throws Exception {
			
			 DataMaintenance_Consolidations_SearchResults.verify_NewTime(Client_ID,Client_ID1);
			
		}	
		//Select Change dates/times to new value radio (btn) (The New Date and New Time should enable, and the hours and minutes should disable) (step 21)
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 75)
		public void PT4554_DataMaintenance_SelectNewDate() throws Exception {
			
			//Select todays date in the date picker (step 22)
			String firstDate =  BaseUI.getDateAsString_InRelationToTodaysDate(0 , "MM/dd/yyyy");
			BaseUI.enterText(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewDate"), firstDate);		
			BaseUI.tabThroughField(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewDate"));
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 76)
		public void PT4554_DataMaintenance_SelectNewTime() throws Exception {
			//Select New Time and select 1 min from now and select OK (btn) (step 23)
			String firstTime =  BaseUI.getDateAsString_InRelationToTodaysDate(0, "hh:mm a");
			firstTime = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(firstTime, "hh:mm a", "hh:mm a", 1);
			BaseUI.enterText(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewTime"), firstTime);	
			
			//Thread.sleep(10000);
			BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_OkBtn"));
		
			//Wait 1min and select the Refresh (btn) to refresh the consolidation list. It should highlight red, and the Late Count should increment +2. (step 24)
			Thread.sleep(100000);
		DataMaintenance_Consolidations_SearchResults.click_RefreshBtn();
//			String   lateCount = "5";
//			 DataMaintenance_Consolidations_SearchResults.verify_LateCount(lateCount);
			 
			 //11) Wait 1 min and select the Refresh (btn) to refresh the consolidation list. 
			//     The Consolidation for todays date should no longer display. Instead a Consolidation for Tomorrows date should be open.
			 DataMaintenance_Consolidations_SearchResults.click_RefreshBtn();
			 String firstDate =  BaseUI.getDateAsString_InRelationToTodaysDate(0 , "MM/dd/yyyy");
			 firstDate =  BaseUI.getDate_WithFormat_X_Days_FromInitialString(firstDate, "MM/dd/yyyy", "MM/dd/yyyy", 1);
			 
			 Thread.sleep(10000);	 
			 //Consol Amount should be 0.00, Consol Checks should be 0, Consol Batches should be 0, 
			 //Open Amount should remain 0, Open Checks should remain 0, and Open Batches should remain 0. (step 25)
			 
			 
	// 12) Select Go Back (btn) | Select Reset (btn) | Select Show Past checkbox | Unselect Show Current checkbox | Select Submit (btn). 
	 //    The past Consolidations for 603 and 604 should exist. A Consol Date of todays date should exist. 
	 //	   The Consol Amount should be 18,500.00, Consol Checks should be 10, Consol Batches should be 1, 
//		   Open Amount should be 0.00, Open Checks should be 0, and Open Batches should be 0. (step 26)
			 
			 
			 
			 
			 
			 
			 
			 
			 
			String  inde603 = "4";
			String  inde604 = "5";
			String clientID = "Client ID";
			

		//	String firstDate =  BaseUI.getDateAsString_InRelationToTodaysDate(0 , "MM/dd/yyyy");
			String consolClose = "Consol Close Date Time";
			String firstDate1 = firstDate +  " " + firstTime;
			String[] headerArray = { Client_ID, firstDate1};
			
			
		
		String clientValue = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResultsByRowsIndexValue(clientID,inde603 );	
		DataMaintenance_Consolidations_SearchResults.scroll_Horizontally_Until_CellVisible_ByHeader(consolClose);
		String firstDateValue = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResultsByRowsIndexValue(consolClose, inde603);
		String[] newValueMatch = { clientValue, firstDateValue};
		BaseUI.baseStringCompareArray(headerArray, newValueMatch);
		Thread.sleep(1000);	
			
		//	DataMaintenance_Consolidations_SearchResults.scroll_Horizontally_Until_CellVisible_ByHeader(Columto);
			//List<String> closedDate = DataMaintenance_Consolidations_SearchResults.return_ColumnTextList_ByHeader(Columto);
		//	List<String> clienttoCheck = DataMaintenance_Consolidations_SearchResults.return_ColumnTextList_ByHeader(clientID);				
			//String rowValue = DataMaintenance_Consolidations_SearchResults.return_Cell_ByColumnHeader_AndIndex(clientID, 1);
		

		}
		
		//will need to refresh db and run to see
		
		
		
		

		
		
		
		
		
		
		/*
		
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
		public void PT5073_Verify_NavigateToBalJob() throws Exception {
			
			DataMaintenance_Batch_SearchResults.select_FirstRow();
			DataMaintenance_Batch_SearchResults.click_OpenBatches();
			DataMaintenance_Batch_SearchResults.select_Row_ByIndex(1);
			DataMaintenance_Batch_SearchResults.click_KeyBatch();		
			DataCompletion.verify_CorrectJob(keyingJob);
		
		}
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 21)
		public void PT5073_Balance_KeyFirstBatch() throws Exception {
			
			
			String amountToEnter =  BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
			amountToEnter = amountToEnter.replace(".", "");
			DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
			DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
			DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
		}
			@Test(groups = { "all_Tests", "critical_Tests" }, priority = 22)
			public void PT5073_VerifyAlertMsg() throws Exception {	
				
			DataCompletion.click_Yes_ToConfirm();
			DataCompletion_bal.wait_ForAlert_ToAppear();
			DataCompletion_bal.verify_Alert_DialogPresent();
			
		}
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
		public void PT5073_Balance__Balance_KeySecoundBatch() throws Exception {
		
		DataCompletion_bal.click_OK_ForAlertModal();
		String batch = "700228";
		Navigation.enter_Text_Into_CommandBox_AndNavigate("key " + batch);
		String amountToEnter =  BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChksAmount"));
		amountToEnter = amountToEnter.replace(".", "");
		DataCompletion_bal.enterValue_IntoInputBox(amountToEnter);
		DataCompletion_bal.key_Through_AllBalanceItemsBy_enterValue(amountToEnter);
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
		
		
		}
		@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
		public void PT5073_VerifyAlertMsg2Batch() throws Exception {	
		
		DataCompletion.click_Yes_ToConfirm();
		DataCompletion_bal.wait_ForAlert_ToAppear();
		DataCompletion_bal.verify_Alert_DialogPresent();
		
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" },priority = 39)
		public void PT4554_DataMaintenance_Verify_ConsolAmount1() throws Exception {
			
			DataCompletion_bal.click_OK_ForAlertModal();
			Browser.closeBrowser();
			Browser.openBrowser(GlobalVariables.baseURL);
			LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
			
			
			//Navigation.navigate_toTab(TopNavigationTab.Dashboard);
			Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
			DataMaintenance.navigate_to_SubTab("Consolidations");
			DataMaintenance_Consolidations_Filters.enterText_IntoFilterTextboxClientId(Client_ID);
			DataMaintenance_Consolidations_Filters.click_Submit();
			String expectedconsolAmount  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolAmount);		
			BaseUI.baseStringCompare("ConsolChecksValue", consolAmtValue1, expectedconsolAmount);
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 40)
		public void PT4554_DataMaintenance_Verify_ConsolChecksAmt1() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolChks);		
			BaseUI.baseStringCompare("ConsolValues", consolChksValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 41)
		public void PT4554_DataMaintenance_Verify_ConsolBatch1() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolBatchValue2, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 42)
		public void PT4554_DataMaintenance_Verify_ConsolOpenAmt1() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenAmt);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenAmtsValue2, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 43)
		public void PT4554_DataMaintenance_Verify_ConsolOpenChks1() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenChks);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenChksValue1, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 44)
		public void PT4554_DataMaintenance_Verify_ConsolOpenBatch1() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolOpenBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolOpenBatchValue1, expectedconsolValue);
			
			
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" },priority = 50)
		public void PT4554_DataMaintenance_Verify_ConsolAmount_StopConsol() throws Exception {
			
			DataMaintenance_Batch_SearchResults.select_FirstRow();
			DataMaintenance_Batch_SearchResults.click_StopConsol();		
			DataMaintenance_Batch_SearchResults.click_StopConsol_MsgAlert_ConfirmBtn();		
			String expectedconsolAmount  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolAmount);		
			BaseUI.baseStringCompare("ConsolChecksValue", consolAmtValue0, expectedconsolAmount);
			
		}
		
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 51)
		public void PT4554_DataMaintenance_Verify_ConsolChecksAmt_StopConsol() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolChks);		
			BaseUI.baseStringCompare("ConsolValues", consolChksValue, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 52)
		public void PT4554_DataMaintenance_Verify_ConsolBatch_StopConsol() throws Exception {
			
			
			String expectedconsolValue  = DataMaintenance_Consolidations_SearchResults.return_ConsolSearchResults(consolBatch);		
			BaseUI.baseStringCompare("ConsolValues", consolBatchValue, expectedconsolValue);
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 60)
		public void PT4554_DataMaintenance_Verify_ConsolBatch_NoBatchExist() throws Exception {
			
			DataMaintenance_Batch_SearchResults.select_FirstRow();
			DataMaintenance_Batch_SearchResults.click_ConsolBatches_Button();
			DataMaintenance_Batch_SearchResults.verify_dataMaint_ConsolBatches_NoBatchAvaliable();
			
		}
		
		@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 70)
		public void PT4554_DataMaintenance_Verify_ConsolBatch_OneBatchExist() throws Exception {
			
			
			
			DataMaintenance_Batch_SearchResults.click_GoBack();
			DataMaintenance_Batch_SearchResults.click_OpenBatches();
			DataMaintenance_Batch_SearchResults.select_FirstRow();
			DataMaintenance_Batch_SearchResults.click_OpenBatches();
			
			String batchCount  = "1-1";
			String newBatchcount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ConsolBatches_BatchCount"));
			BaseUI.baseStringCompare("Batch_Cout", batchCount, newBatchcount);
			
		}
		*/

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
