package imageRPS.tests_ignored;

import imageRPS.data.baseTest;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import imageRPS.pages.DataCompletion;
import imageRPS.pages.DataCompletion_fbl_AllFieldsBalancing;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.DataCompletion.ConfirmAction;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class FBL_Workflow_Tests extends baseTest {

	String keyingJobAbbreviation = "fbl";
	TableData fbl_StubAndCheck_Data;
	Integer index_Of_Item_ToReject;

	Integer previousNumberToFix = 0;
	Integer itemsThatWillbeFixed = 0;
	
	String batchID;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL);

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataCompletion);

		DataCompletion.click_Abbreviation(keyingJobAbbreviation);

		if (BaseUI.pageSourceContainsString("No more items to key in this batch. OK to move to next?")) {
			DataCompletion.click_Yes_ToConfirm();

		}

		fbl_StubAndCheck_Data = DataCompletion_fbl_AllFieldsBalancing.return_StubAndCheck_Rows();
//		index_Of_Item_ToReject = DataCompletion_fbl_AllFieldsBalancing.return_An_Index_ToReject(fbl_StubAndCheck_Data);
		previousNumberToFix = DataCompletion_fbl_AllFieldsBalancing.return_ItemsToFix_Count();

		// if our item to reject is already rejected we unreject the item.
//		if (!BaseUI.getTextFromField(
//				Locator.lookupElement("dataMaint_FBL_RejectReason_ByIndex", index_Of_Item_ToReject.toString(), null))
//				.equals("---")) {
//			DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(index_Of_Item_ToReject);
//			DataCompletion.click_reject();
//		}
		
		batchID = DataCompletion.return_BatchID();
	}

//	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 1)
//	public void PT3632_FBL_Reject() throws Exception {
//		itemsThatWillbeFixed = Integer
//				.parseInt(fbl_StubAndCheck_Data.data.get(index_Of_Item_ToReject - 1).get("Correction Count"));
//
//		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(index_Of_Item_ToReject);
//		DataCompletion.reject_Item();
//
//		DataCompletion_fbl_AllFieldsBalancing.verify_Item_WasRejected_ByIndex(index_Of_Item_ToReject);
//		BaseUI.verify_true_AndLog(
//				DataCompletion_fbl_AllFieldsBalancing.return_ItemsToFix_Count() == previousNumberToFix
//						- itemsThatWillbeFixed,
//				"Items to fix was as expected.", "Items to fix did not decrease properly.");
//	}
//
//	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 2)
//	public void PT3632_FBL_Un_Reject() throws Exception {
//		DataCompletion_fbl_AllFieldsBalancing.select_WorkItem_ByIndex(index_Of_Item_ToReject);
//		DataCompletion.click_reject();
//		DataCompletion_fbl_AllFieldsBalancing.verify_Item_Was_NOT_Rejected_ByIndex(index_Of_Item_ToReject);
//		BaseUI.verify_true_AndLog(
//				DataCompletion_fbl_AllFieldsBalancing.return_ItemsToFix_Count() == previousNumberToFix,
//				"Items to fix was as expected.", "Items to fix did not reset properly.");
//	}

	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 3)
	public void PT3632_FBL_KeyAllFields() throws Exception {

		DataCompletion_fbl_AllFieldsBalancing.clear_Errors_AllTransactions();
		DataCompletion.verify_ConfirmModal_Appears(ConfirmAction.Done);
	}
	
	@Test(groups = { "all_Tests", "smoke_Tests" }, priority = 4)
	public void PT3632_FBL_Confirm_Shows_NewBatch() throws Exception {

		DataCompletion.click_DoneButton();
		DataCompletion.verify_BatchID_Changed(batchID);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}
}
