package imageRPS.tests_ignored;

import java.util.HashMap;

import imageRPS.data.baseTest;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class KeyJob_ACT_Reject_UnReject extends baseTest {

	String keyingJob = "act";
	// 700154-700161
	// String batchID = "700152";
	String batch = "";
	String timestamp = "";

	// Integer maxValue;
	Integer minValue;
	TableData batchInfo;
	HashMap<String, String> jobInfo;

	TableData docInfo;

	String clientID;
	String docID;

	Integer index_Of_Field = null;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.baseURL, "chrome");

		LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 5)
	public void PT3708_ACT_Verify_NothingRejected_Yet() throws Exception {
		BaseUI.verify_true_AndLog(true, "", "");
		BaseUI.log_Warning("WARNING");
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT3708_ACT_RejectSecondRow_NoOtherRow_Rejected() throws Exception {
		BaseUI.verify_true_AndLog(false, "", "Test Failed.");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		batchInfo = null;
		jobInfo = null;
		docInfo = null;
		Browser.closeBrowser();
	}

}// End of Class
