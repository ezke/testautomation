package imageRPS.tests_prerelease;

import imageRPS.data.baseTest;
import imageRPS.pages.GlobalVariables;
import imageRPS.pages.LoginPage;
import imageRPS.pages.Navigation;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class SessionMangement_Login_RefreshPage extends baseTest {
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        Browser.openBrowser(GlobalVariables.baseURL);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 10)
    public void PT4415_TopMenuSecurity_Login_CheckDashBoard() throws Exception {
        LoginPage.login(GlobalVariables.entity, GlobalVariables.login_SMAuto1_users, GlobalVariables.passvord_SMAuto1_users);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }

    @Test(groups = {"all_Tests", "regression_Tests", "module_SessionManagement_TopMenuSecurity"}, priority = 20)
    public void PT4415_TopMenuSecurity_Login_RefreshPage() throws Exception {
        Browser.driver.navigate().refresh();
        BaseUI.waitForElementToBeDisplayed("dshbrd_dashboard", null, null);
        Navigation.verify_Only_ProvidedHeader_Appears("Dashboard");
    }
    
    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }

}//End of Class



