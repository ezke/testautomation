package imageRPS.pages;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Locator;

public class DataMaintenance_Batch_Filters {

	// Batch Filters page.
	public static void enter_BatchID_AndSearch(String batchFrom, String batchTo) throws Exception {

		if (batchFrom != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_batchID_FromTextBox"), batchFrom);
		}

		if (batchTo != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_batchID_ToTextBox"), batchTo);
		}

		click_Submit();
	}

	public static void click_Submit() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Submit_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	// consol_Date needs to be in format MM/dd/yyyy
	public static void enter_ConsolidationDate_AndNumber(String consol_date, String consol_number) {
		if (consol_date != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_consolDate_Textbox"), consol_date);
		}

		if (consol_number != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_Number_Textbox"), consol_number);
		}

	}

	public static void enter_LockBoxID(String lockBoxID) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_lockboxID_TextBox"),lockBoxID);
		Thread.sleep(100);
	}

	public static void enter_LockBoxID_AndSearch(String lockBoxID) throws Exception {
		enter_LockBoxID(lockBoxID);
		click_Submit();
	}

	public static void wait_ForAlert_ToAppear() {
		BaseUI.waitForElementToBeDisplayed("dataMaint_Alert_Dialog", null, null);
	}

	public static void click_OK_IfAlertModalAppears() throws Exception {
		if(BaseUI.elementAppears(Locator.lookupElement("dataMaint_Alert_Dialog"))){
			BaseUI.click(Locator.lookupElement("dataMaint_Alert_OK_Button"));
			BaseUI.waitForElementToNOTBeDisplayed("dataMaint_Alert_Dialog", null, null, 10);
			BaseUI.wait_forPageToFinishLoading();
			Navigation.wait_For_Page_ToLoad();
		}

	}

	public static void verify_Batches_PageLoaded() throws Exception {
		DataMaintenance.verify_SubTab_Active("Batches");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_Batches_HeaderElement"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void click_Cancel_OnModal_IfAppears() throws Exception {
		WebElement modalCancelButton = Locator.lookupElement("dataMaint_Filters_GenericCancelButton");
		if (modalCancelButton != null && BaseUI.elementAppears(modalCancelButton)) {
			BaseUI.click(modalCancelButton);
			Thread.sleep(1500);
		}

	}

	// Index starts at 1
	public static String select_LocationID_From_FirstFilterBox_ByIndex(Integer index) throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_LocationID_first_FilterIcon"));
		BaseUI.waitForElementToBeDisplayed("dataMain_LocationIDModal_Modal", null, null);
		Thread.sleep(400);

		WebElement lastLocationID = Locator.lookupElement("dataMain_LocationIDModal_Row_ByIndex", index.toString(),
				null);
		String idToReturn = BaseUI.getTextFromField(lastLocationID);
		BaseUI.click(lastLocationID);
		Thread.sleep(200);

		click_LocationIDModal_SelectLocationButton();

		return idToReturn;
	}

	public static void click_LocationIDModal_SelectLocationButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMain_LocationIDModal_SelectLocation_Button"));
		Thread.sleep(3000);
	}

	public static void enter_LocationID_First_And_Second(String firstLocation, String secondLocation) {
		if (firstLocation != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_LocationID_firstInputBox"), firstLocation);
		}

		if (secondLocation != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_LocationID_secondInputBox"), secondLocation);
		}

	}

	public static void enter_DocGroupID(String docGroupID) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_DocGroup_TextBox"), docGroupID);

	}

	public static void enter_BatchID_First_And_Second(String firstBatch, String secondBatch) {
		if (firstBatch != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_BatchID_From_TextBox"), firstBatch);
		}

		if (secondBatch != null) {
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_BatchID_To_TextBox"), secondBatch);
		}

	}

	public static void verify_AtLeast_1RowPresent() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_FirstRow_FirstCell"));
	}

	public static void select_WorkType(WorkType workType) throws Exception {

		String valueToUse = workType.getValue();
		valueToUse = valueToUse.equals("Single") ? "Singles" : valueToUse;

		if (workType != null) {
			BaseUI.selectValueFromDropDown(Locator.lookupElement("dataMaint_WorkType_Dropdown"), valueToUse);
		}
	}

	public static void select_Key_Jobs_FromModal(String[] jobNames) throws Exception {
		click_KeyJobs_FilterButton();

		for (String job : jobNames) {
			BaseUI.click(Locator.lookupElement("dataMaint_SelectKeyJobs_JobName_ByText", job, null));
		}

		Thread.sleep(100);
		BaseUI.click(Locator.lookupElement("dataMaint_SelectKeyJobs_SelectKeyJobs_Button"));
		Thread.sleep(1000);

	}

	public static void click_KeyJobs_FilterButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_KeyJobs_FilterButton"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_SelectKeyJobs_Modal", null, null);
		Thread.sleep(200);
	}

	public static void select_Select_Source_Jobs_FromModal(String[] jobNames) throws Exception {
		click_SourceWF_FilterButton();

		for (String job : jobNames) {
			BaseUI.click(Locator.lookupElement("dataMaint_SelectSourceJobs_JobNameCell_ByText", job, null));
		}

		Thread.sleep(100);
		BaseUI.click(Locator.lookupElement("dataMaint_SelectSourceJobs_Modal_SubmitButton"));
		Thread.sleep(1000);

	}

	public static void click_SourceWF_FilterButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_WF_FilterButton"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_SelectSourceJobs_Modal", null, null);
		Thread.sleep(400);
	}

	public static void click_ResetFilters() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Filters_ResetButton"));
		Thread.sleep(500);
	}

	public static void enter_ClientID(String clientID) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Filters_clientID_TextBox"), clientID);
		Thread.sleep(100);
	}

	public static void click_Launch_SelectClient_Modal() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_launch_SelectClient_Modal_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_SelectClientModal_ModalTable", null, null);
		// Seeing strange exception, added additional 200 millisecond sleep here
		// to give modal a chance to be fully done loading.
		Thread.sleep(200);
	}

	public static void click_SelectClient() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SelectClientModal_SelectClientButton"));
		Thread.sleep(3000);
	}

	public static void select_LockStatus(LockStatus lockStatus) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataMaint_Filters_LockStatus_Dropdown"),
				lockStatus.getValue());
	}

	public enum LockStatus {
		SelectLockStatus("Select Lock Status"), AllBatches("All Batches"), UnlockedBatches(
				"Unlocked Batches"), LockedBatches(
						"Locked Batches"), SystemLock("System Lock"), OperatorLock("Operator Lock");

		private String value;

		private LockStatus(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	/// Select Client Modal
	//
	//
	//

	// Index starts at 1.
	public static HashMap<String, String> return_Row_ByIndex(Integer index) {
		WebElement rowToReturn = Locator.lookupElement("dataMaint_SelectClientModal_ModalTable_RowByIndex",
				index.toString(), null);

		HashMap<String, String> rowValues = new HashMap<String, String>();
		rowValues.put("Client ID", BaseUI.getTextFromField(rowToReturn.findElement(By.xpath("./div[1]/div"))));
		rowValues.put("Lockbox ID", BaseUI.getTextFromField(rowToReturn.findElement(By.xpath("./div[2]/div"))));
		rowValues.put("Client Name", BaseUI.getTextFromField(rowToReturn.findElement(By.xpath("./div[3]/div"))));
		rowValues.put("Location ID", BaseUI.getTextFromField(rowToReturn.findElement(By.xpath("./div[4]/div"))));
		rowValues.put("Location", BaseUI.getTextFromField(rowToReturn.findElement(By.xpath("./div[5]/div"))));

		return rowValues;
	}

	// index starts at 1.
	public static void select_Client_FromSelectClient_Modal(Integer index) throws Exception {
		WebElement rowToReturn = Locator.lookupElement("dataMaint_SelectClientModal_ModalTable_RowByIndex",
				index.toString(), null);

		BaseUI.click(rowToReturn);
		Thread.sleep(200);
	}

	public static void cancel_OutOf_Select_ClientModal() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SelectClientModal_CancelButton"));
		Thread.sleep(3000);
	}

	public static void select_WFComplete(WFComplete wfComplete) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataMaint_WFComplete_Dropdown"),
				wfComplete.getValue());
	}

	public enum WFComplete {
		SelectWorkType("Select WF Complete"), Any("Any"), NotComplete("Not Complete"), WFComplete(
				"Complete");

		private String value;

		private WFComplete(final String val) {
			value = val;

		}

		public String getValue() {
			return value;
		}


		@Override
		public String toString() {
			return getValue();
		}
	}

	public static void select_PurgeOK(PurgeOK purgeOK) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataMaint_PurgeOK_Dropdown"),
				purgeOK.getValue());
	}


	public enum PurgeOK {
		SelectPurgeOK("Select Purge OK"), Any("Any"), NotReady("Not Ready"), Ready(
				"Ready");

		private String value;

		private PurgeOK(final String val) {
			value = val;

		}

		public String getValue() {
			return value;
		}


		@Override
		public String toString() {
			return getValue();
		}
	}


}// End of Class
