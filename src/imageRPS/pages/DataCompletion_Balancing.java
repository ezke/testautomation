package imageRPS.pages;

import java.util.HashMap;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

//sca, cca,cc2,cc3
public class DataCompletion_Balancing {

	public static void key_through_to_SCA(String valueToKeyWith) throws Exception {

		Integer maxCounter = 0;

		while (true) {

			if (maxCounter >= 4) {
				break;
			}

			String dataCompletionText = BaseUI
					.getTextFromField(Locator.lookupElement("navigation_Link_ByText", "Data Completion", null));

			if (dataCompletionText.contains("sca")) {
				break;
			}

			key_through_Batch(valueToKeyWith);

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				DataCompletion.click_Yes_ToConfirm();
			}

			maxCounter++;
		}
	}

	public static void key_through_Batch(String valueToKey) throws Exception {

		Integer maxCounter = 0;

		while (true) {
			if (maxCounter >= 50) {
				break;
			}

			enterText_Into_LastInputBox(valueToKey);
			//
			// BaseUI.waitForElementToContain_AttributeMatch("dataComplete_Balancing_LastKeyingTextField",
			// null, null,
			// "value", "0", 3);

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			maxCounter++;

		}

	}

	public static void key_through_Batch_BySequence(String valueToKey, String sequenceNumber) throws Exception {

		int maxCounter = 0;
		String seqNumber = "";

		while (true) {
			if (maxCounter >= 50) {
				break;
			}

			enterText_Into_LastInputBox(valueToKey);
			seqNumber = return_last_seq();
			if (seqNumber.equals(sequenceNumber)) {
				break;
			}

			maxCounter++;

		}

	}

	public static void enterText_Into_LastInputBox(String valueToKey) throws Exception {
		WebElement lastKeyingTextBox = Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField");
		BaseUI.enterText_IntoInputBox(lastKeyingTextBox, valueToKey);
		BaseUI.enterKey(lastKeyingTextBox, Keys.ENTER);
		Thread.sleep(300);
		// Needed something custom that wouldn't throw an exception.
		try {
//			Predicate<WebDriver> myPredicate = x -> x
//					.findElement(Locator.lookupBy("dataComplete_Balancing_LastKeyingTextField", null, null))
//					.getAttribute("value").equals("0");

			WebDriverWait wait = (WebDriverWait) new WebDriverWait(Browser.driver, 3);
			wait.ignoring(NullPointerException.class).until(new Function<WebDriver, Boolean>() {
		        public Boolean apply(WebDriver driver) {
		            return driver.findElement(Locator.lookupBy("dataComplete_Balancing_LastKeyingTextField", null, null))
							.getAttribute("value").equals("0"); 
		        }});
		} catch (Exception e) {
			BaseUI.log_Status("No longer seeing new input fields.");
		}
	}

	public static void enterText_Into_InputBox_ByIndex(String valueToKey, int index) throws Exception {

		select_InputBox_ByIndex(index);
		WebElement textBox = Locator.lookupElement("dataComplete_Balancing_KeyingTextField_ByIndex", String.valueOf(index), null);
		BaseUI.enterText_IntoInputBox(textBox, valueToKey);
		BaseUI.enterKey(textBox, Keys.ENTER);
		Thread.sleep(300);
	}

	public static String return_last_seq() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Last_Seq"));
	}

	// Index starts at 1.
	public static String return_Seq_ByIndex(Integer index) {
		String seqValue = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_Seq_ByIndex", index.toString(), null));
		return seqValue;
	}

	public static void verify_Seq_ByIndex(int seqIndex, String expectedMessage){
		WebElement seq = Locator.lookupElement("dataComplete_Seq_ByIndex", String.valueOf(seqIndex), null);
		BaseUI.verifyElementAppears(seq);
		BaseUI.verifyElementHasExpectedText(seq, expectedMessage);

	}

	public static void verify_KeyingField_Empty() throws Exception{
		String keyingField = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"));
		BaseUI.verify_true_AndLog(keyingField.length()== 0, "Keying Field was empty", "Keying Field was NOT empty");
	}

	// Index starts at 1.
	public static void select_InputBox_ByIndex(Integer index) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Balancing_KeyingTextField_ByIndex", index.toString(), null));
		Thread.sleep(100);
	}

	public static String return_KeyingFieldText_ByIndex(int index){
		String keyingFieldText = BaseUI.get_NonVisible_TextFromField(
				Locator.lookupElement("dataComplete_Balancing_KeyingTextField_ByIndex", String.valueOf(index), null));
		return keyingFieldText;
	}

	// Index starts at 1.
	public static String return_Rej_ByIndex(Integer index) {
		String rejectReason = BaseUI.getTextFromField(
				Locator.lookupElement("dataComplete_Balancing_RejReason_ByIndex", index.toString(), null));
		return rejectReason;
	}

	public static void verify_Rejected(String keyingJob, Integer indexOfRejectReason) {
		BaseUI.verifyElementHasExpectedText(
				Locator.lookupElement("dataComplete_Balancing_RejReason_ByIndex", indexOfRejectReason.toString(), null),
				keyingJob);
	}

	public static void verify_Susp(Integer indexOfRejectReason) {
		BaseUI.verifyElementHasExpectedText(
				Locator.lookupElement("dataComplete_Balancing_RejReason_ByIndex", indexOfRejectReason.toString(), null),
				"sus");
	}

	public static void verify_NOT_Rejected(Integer indexOfRejectReason) {
		BaseUI.verifyElementHasExpectedText(
				Locator.lookupElement("dataComplete_Balancing_RejReason_ByIndex", indexOfRejectReason.toString(), null),
				"---");
	}

	public static void click_Img2_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Img2_Button"));
		Thread.sleep(700);
	}

	public static void verify_Img2_Button_ChangedImageZoom() throws Exception {
		HashMap<String, String> img2StyleInfo_Before = return_Image2_Style_Info();
		click_Img2_Button();
		HashMap<String, String> img2StyleInfo_After = return_Image2_Style_Info();

		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_Before.get("top").trim()),
				"Found style attribute 'top'", "Did NOT find style attribute 'top'");
		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_Before.get("left").trim()),
				"Found style attribute 'left'", "Did NOT find style attribute 'left'");
//		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_Before.get("width").trim()),
//				"Found style attribute 'width'", "Did NOT find style attribute 'width'");
//		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_Before.get("height").trim()),
//				"Found style attribute 'height'", "Did NOT find style attribute 'height'");
		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_After.get("top").trim()),
				"Found style attribute 'top'", "Did NOT find style attribute 'top'");
		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_After.get("left").trim()),
				"Found style attribute 'left'", "Did NOT find style attribute 'left'");
//		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_After.get("width").trim()),
//				"Found style attribute 'width'", "Did NOT find style attribute 'width'");
//		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(img2StyleInfo_After.get("height").trim()),
//				"Found style attribute 'height'", "Did NOT find style attribute 'height'");

		BaseUI.verify_true_AndLog(!img2StyleInfo_Before.get("top").trim().equals(img2StyleInfo_After.get("top").trim()),
				"'top' style changed.", "'top' style did NOT change.");
		BaseUI.verify_true_AndLog(
				!img2StyleInfo_Before.get("left").trim().equals(img2StyleInfo_After.get("left").trim()),
				"'left' style changed.", "'left' style did NOT change.");
//		BaseUI.verify_true_AndLog(
//				!img2StyleInfo_Before.get("width").trim().equals(img2StyleInfo_After.get("width").trim()),
//				"'width' style changed.", "'width' style did NOT change.");
//		BaseUI.verify_true_AndLog(
//				!img2StyleInfo_Before.get("height").trim().equals(img2StyleInfo_After.get("height").trim()),
//				"'height' style changed.", "'height' style did NOT change.");

	}

	public static HashMap<String, String> return_Image2_Style_Info() {
		HashMap<String, String> styleMap = new HashMap<String, String>();
		String imageStyle = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_Img2_Image"), "style");
		String[] styles = imageStyle.split("\\;");

		for (String style : styles) {
			String[] styleInfo = style.split("\\:");
			styleMap.put(styleInfo[0].trim(), styleInfo[1].trim());
		}

		return styleMap;
	}
	
	public static HashMap<String, String> return_KeyField_RowsInfo(String rejectReason, int index) throws Exception {

		WebElement tableRow = Locator.lookupElement("dataComplete_SCA_KeyingRows_ByIndex", String.valueOf(index), null);

		HashMap<String, String> rowData = new HashMap<String, String>();
		DataCompletion.reject_Item(rejectReason);
		rowData.put("Rej", BaseUI.getTextFromField(tableRow.findElement(By.xpath(".//div/span[contains(@data-bind,'html:rej')]"))));
		rowData.put("Seq", BaseUI.getTextFromField(tableRow.findElement(By.xpath("./div[@data-bind='html:seq']"))));
			
		return rowData;
	}
	
	public static TableData reject_All_Sequence(String rejectReason) throws Exception {

		TableData keyFieldRowsTableData = new TableData();

		int maxCounter = 0;
		int index = 0;
		while (true) {
			if (maxCounter >= 50) {
				break;
			}
			index++;
			keyFieldRowsTableData.data.add(return_KeyField_RowsInfo(rejectReason, index));

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			maxCounter++;
		}
		return keyFieldRowsTableData;
	}


	public static HashMap<String, String> return_KeyField_RowsInfo_ForSuspend(String suspendReason, int index) throws Exception {

		WebElement tableRow = Locator.lookupElement("dataComplete_SCA_KeyingRows_ByIndex", String.valueOf(index), null);

		HashMap<String, String> rowData = new HashMap<String, String>();
		DataCompletion.suspend_BySuspReasonText(suspendReason);
		rowData.put("Rej", BaseUI.getTextFromField(tableRow.findElement(By.xpath(".//div/span[contains(@data-bind,'html:rej')]"))));
		rowData.put("Seq", BaseUI.getTextFromField(tableRow.findElement(By.xpath("./div[@data-bind='html:seq']"))));
			
		return rowData;
	}
	
	public static TableData suspend_All_Sequence(String suspendReason) throws Exception {

		TableData keyFieldRowsTableData = new TableData();

		int maxCounter = 0;
		int index = 0;
		while (true) {
			if (maxCounter >= 50) {
				break;
			}
			index++;
			keyFieldRowsTableData.data.add(return_KeyField_RowsInfo_ForSuspend(suspendReason, index));

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			maxCounter++;
		}
		return keyFieldRowsTableData;
	}
}
