package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class DataMaintenance_Consolidations_ConsolBatches {

    public static void click_BatchInfo() throws Exception {
        BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_BatchInfo_Button"));
        Thread.sleep(200);
        BaseUI.waitForElementToBeDisplayed("dataMaint_Batch_BatchInfo_Header", null, null, 2);
    }

    public static void click_ViewItems() throws Exception {
        BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_ViewItems_Button"));
        Thread.sleep(200);
        BaseUI.waitForElementToBeDisplayed("dataMaint_ViewItems_Header", null, null, 2);
    }
}
