package imageRPS.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class DataMaintenance_Batch_ViewItems extends DataMaintenance_Batch_SearchResults {

	static String[] headers_ViewItem = { "Batch ID", "P1 Seq Number", "Source ID", "Balanced Error", "Tran Number",
			"Par P1 Seq", "Doc Type", "Doc Type Name", "Tran Seq Number", "Reject Job", "Item Job", "Batch Job",
			"Tran Reject Job", "Tran Job", "Doc ID", "Applied Amount", "P2 Seq Number", "P2 Pocket Seq Num",
			"PocketCut ID", "Reject Reason", "ARC Reason", "P2 Amount", "P2 Changed", "Bank ID", "Bank Type",
			"Bank Name", "Audit Trail", "Balancing Doc" };

	public static TableData return_ViewItem_TableData() throws Exception {
		TableData viewItem_Data = new TableData();
		viewItem_Data.data = table_Extractor(headers_ViewItem, "dataMaint_ViewItems_ColumnValues_ByHeaderText");

		return viewItem_Data;
	}

	public static ArrayList<HashMap<String, String>> table_Extractor(String[] headerArray, String elementLocator_byHeaderText) throws Exception {
		ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();

		for (String header : headerArray) {

			scroll_Horizontally_Until_CellVisible_ByHeader(header);

			ArrayList<WebElement> columnCellList = Locator.lookup_multipleElements(elementLocator_byHeaderText, header,
					null);

			for (Integer i = 0; i < columnCellList.size(); i++) {
				HashMap<String, String> tableRow = null;

				if (table_Values.size() == 0 || table_Values.size() <= i) {
					tableRow = new HashMap<String, String>();
					tableRow.put(header, BaseUI.getTextFromField(columnCellList.get(i)));
					table_Values.add(tableRow);
				} else {
					table_Values.get(i).put(header, BaseUI.getTextFromField(columnCellList.get(i)));
				}
			}

		}

		return table_Values;

	}

	// Index has to start at 1
	public static String return_CellText_ByRowNumber_And_HeaderText(Integer rowNumber, String headerText) {
		String cellValue = BaseUI.getTextFromField(Locator.lookupElement(
				"dataMaint_ViewItems_ColumnCell_ByRowNumber_AndHeaderText", rowNumber.toString(), headerText));

		return cellValue;
	}

	// Loops through a table and just verifies that each column has at least one
	// value in it.
	// Not a valid test.
	// public static void verify_Columns_eachContain_A_Value(TableData
	// tableToVerify) {
	//
	// BaseUI.verify_true_AndLog(tableToVerify.data.size() > 0 , "Found Rows in
	// the table.", "Table had no rows.");
	//
	// for (String key : tableToVerify.data.get(0).keySet()) {
	// Boolean hasValue = false;
	// for (HashMap<String, String> dataRow : tableToVerify.data) {
	//
	// if (!BaseUI.stringEmpty(dataRow.get(key)) && !dataRow.get(key).equals("
	// ")) {
	// hasValue = true;
	// }
	//
	// }
	//
	// BaseUI.verify_true_AndLog(hasValue, "Found a value in column " + key,
	// "Could not find any values for column " + key);
	// }
	//
	// }

	// Index must start with 1.
	public static HashMap<String, String> return_Row_ByIndex(Integer rowToPick) throws Exception {
		HashMap<String, String> row = new HashMap<String, String>();

		// Get first 16 items.
		for (int i = 0; i < 17; i++) {
			String cellValue = BaseUI
					.getTextFromField(Locator.lookupElement("dataMaint_ViewItems_ColumnValues_ByHeaderText_AndIndex",
							headers_ViewItem[i], rowToPick.toString()));

			row.put(headers_ViewItem[i], cellValue);
		}

		scroll_horizontally_AllTheWay_ToRight();

		for (int i = 17; i < headers_ViewItem.length; i++) {
			String cellValue = BaseUI
					.getTextFromField(Locator.lookupElement("dataMaint_ViewItems_ColumnValues_ByHeaderText_AndIndex",
							headers_ViewItem[i], rowToPick.toString()));

			row.put(headers_ViewItem[i], cellValue);
		}

		// Scroll all the way to the right.

		// for (String header : headers_ViewItem) {
		// scroll_Horizontally_Until_CellVisible_ByHeader(header);
		//
		// String cellValue = BaseUI.getTextFromField(Locator.lookupElement(
		// "dataMaint_ViewItems_ColumnValues_ByHeaderText_AndIndex", header,
		// rowToPick.toString()));
		//
		// row.put(header, cellValue);
		//
		// }

		return row;
	}

	public static void scroll_horizontally_AllTheWay_ToRight() throws Exception {
		WebElement right_Scrollbar = Locator.lookupElement("dataMaint_ViewItems_ScrollRightHorizontal");
		for (int i = 0; i < 45; i++) {
			BaseUI.click_js(right_Scrollbar);
		}
	}

	public static void scroll_horizontally_AllTheWay_ToLeft() throws Exception {
		WebElement right_Scrollbar = Locator.lookupElement("dataMaint_ViewItems_ScrollLeftHorizontal");
		for (int i = 0; i < 45; i++) {
			BaseUI.click_js(right_Scrollbar);
		}
	}

	// Might need to add scrolling if we at some point need this for values that
	// are too far over to the right.
	public static Integer return_FirstRowIndex_OfColumn_WithValue(String header, String expectedValue)
			throws Exception {
		ArrayList<WebElement> columnElementList = Locator
				.lookup_multipleElements("dataMaint_ViewItems_ColumnValues_ByHeaderText", header, null);
		// ArrayList<String> columnList = new ArrayList<String>();
		Integer indexToReturn = null;
		for (Integer i = 0; i < columnElementList.size(); i++) {
			String cellValue = BaseUI.get_NonVisible_TextFromField(columnElementList.get(i));
			if (cellValue.equals(expectedValue)) {
				indexToReturn = i + 1;
				break;
			}
		}

		return indexToReturn;
	}

	public static void verify_Headers_Match() throws Exception {
		ArrayList<WebElement> headerElementList = Locator.lookup_multipleElements("dataMaint_ViewItems_HeaderList",
				null, null);
		ArrayList<String> headerList = new ArrayList<String>();

		for (WebElement headerElement : headerElementList) {
			String header = BaseUI.get_NonVisible_TextFromField(headerElement);
			headerList.add(header);
		}

		BaseUI.verify_true_AndLog(headerList.size() == headers_ViewItem.length, "Header count was as expected.",
				"Did not find all expected headers.");

		for (String header : headers_ViewItem) {
			BaseUI.verify_true_AndLog(headerList.contains(header), "Found header " + header,
					"Did NOT find header " + header);
		}

	}

	public static void click_ItemInfo_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ViewItems_ItemInfo_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
		BaseUI.waitForElementToBeDisplayed("dataMaint_ItemInfo_ItemFieldsTable_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void click_TransInfo_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ViewItems_TransInfo_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static ArrayList<String> return_ColumnTextList_ByHeader(String headerText) throws Exception {
		scroll_Horizontally_Until_CellVisible_ByHeader(headerText);

		ArrayList<WebElement> columnElementList = Locator.lookup_multipleElements("dataMaint_ViewItems_ColumnValues_ByHeaderText",
				headerText, null);
		ArrayList<String> columnTextList = new ArrayList<String>();

		for (WebElement columnCell : columnElementList) {

			columnTextList.add(BaseUI.get_NonVisible_TextFromField(columnCell));
		}

		return columnTextList;
	}

	public static void verify_ColumnMatches_ExpectedValue(String columnName, String expectedValue) throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			BaseUI.baseStringCompare(columnName, expectedValue, value);
		}

	}

	public static void scroll_Horizontally_Until_CellVisible_ByHeader(String headerText) throws Exception {

		int maxCount = 0;

		while (!BaseUI.elementAppears(Locator.lookupElement("dataMaint_ViewItems_ColumnValues_ByHeaderText", headerText, null))) {
			if (maxCount > 50) {
				break;
			}

			BaseUI.click_js(Locator.lookupElement("dataMaint_ViewItems_ScrollRightHorizontal"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_ViewItems_ScrollRightHorizontal"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_ViewItems_ScrollRightHorizontal"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_ViewItems_ScrollRightHorizontal"));

			maxCount++;
		}

	}

	public static void verify_ColumnMatches_ExpectedValues(String columnName, String[] expectedValues)
			throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			Boolean valueMatches = false;
			for (String expectedValue : expectedValues) {
				if (expectedValue.equals(value)) {
					valueMatches = true;
				}

			}

			BaseUI.verify_true_AndLog(valueMatches, "Column text was " + value, "Was NOT expecting text " + value);
		}

	}

	public static void verify_ColumnContains_ExpectedValues(String columnName, String[] expectedValues)
			throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			Boolean valueMatches = false;
			for (String expectedValue : expectedValues) {
				if (value.contains(expectedValue)) {
					valueMatches = true;
				}

			}

			BaseUI.verify_true_AndLog(valueMatches, "Column text was " + value, "Was NOT expecting text " + value);
		}

	}

	public static void verify_ColumnMatches_DoesNOTContain_Value(String columnName, String expectedValue)
			throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		Boolean columnContainsValue = false;
		for (String value : columnTextList) {
			if (value.equals(expectedValue)) {
				columnContainsValue = true;
			}
		}

		BaseUI.verify_false_AndLog(columnContainsValue, "Value " + expectedValue + " was NOT found.",
				"Found value " + expectedValue);
	}
}// End of Class
