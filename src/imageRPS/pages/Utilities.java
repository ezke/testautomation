package imageRPS.pages;

import imageRPS.pages.Navigation;
import utils.BaseUI;
import utils.Locator;

public class Utilities {

	public static void verify_SubTab_Active(String buttonText) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("util_SubHeader_Active_ByText", buttonText, null));
	}

	public static void navigate_SubTab(String tabToClick) throws Exception {

		BaseUI.click(Locator.lookupElement("util_SubHeader_ByText", tabToClick, null));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}
	
	
	public static void navigate_ConcurrentUsers() throws Exception {
		BaseUI.waitForElementToBeDisplayed("util_ConcurrentUsers_Button", null, null);
		BaseUI.click(Locator.lookupElement("util_ConcurrentUsers_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}
	
}//End of Class
