package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class ReportsOutput {

	public static void verify_SubTab_Active(String buttonText) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("reports_SubHeader_Active_ByText", buttonText, null));
	}

	public static void navigate_SubTab(String tabToClick) throws Exception {

		BaseUI.click(Locator.lookupElement("reports_SubHeader_ByText", tabToClick, null));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

}
