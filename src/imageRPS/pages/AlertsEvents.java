package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class AlertsEvents {

	public static void verify_SubTab_Active(String buttonText) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("alertsEvents_SubHeader_Active_ByText", buttonText, null));
	}

	public static void navigate_SubTab(String tabToClick) throws Exception {
		BaseUI.click(Locator.lookupElement("alertsEvents_SubHeader_ByText", tabToClick, null));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}
	
	
}//end of class
