package imageRPS.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class DataCompletion_fbl_AllFieldsBalancing {

	public static TableData return_StubAndCheck_Rows() {
		TableData stubAndCheck_Data = new TableData();

		HashMap<String, String> cellMappings = new HashMap<String, String>();
		cellMappings.put("Seq Number", ".//span[./text()='Seq:']/following-sibling::span[1]");
		cellMappings.put("Payment Type", ".//span[./text()='Seq:']/following-sibling::span[2]");
		cellMappings.put("Reject", ".//span[./text()='Rej:']/following-sibling::span[1]/span");
		cellMappings.put("Amount", ".//span[./text()='$']/following-sibling::span[1]");
		cellMappings.put("Correction Count", ".//span[./text()='Seq:']/following-sibling::span[3]");

		stubAndCheck_Data.data = BaseUI.tableExtractor("dataMaint_FBL_StubAndCheck_Rows", cellMappings);
		// Correction Count will target Rej: element when no Correction Count
		// exists. So updating this value with 0.
		stubAndCheck_Data.replace_Character("Rej:", "0", "Correction Count");

		return stubAndCheck_Data;
	}

	public static void verify_TextBox_HasExpectedValue(String identifyingText, String expectedValue) {
		String inputBoxActualValue = BaseUI.getTextFromInputBox(
				Locator.lookupElement("dataComplete_AllFields_FieldTextBox_ByLabelText", identifyingText, null));
		BaseUI.baseStringCompare(identifyingText, expectedValue, inputBoxActualValue);
	}

	public static void verify_TextBox_Disabled(String identifyingText) {
		BaseUI.verifyElementDisabled(
				Locator.lookupElement("dataComplete_AllFields_FieldTextBox_ByLabelText", identifyingText, null));

	}

	public static void verify_Current_Thumbnail_BackgroundPink() {
		BaseUI.verifyElementHasExpected_CSSAttribute(Locator.lookupElement("dataComplete_AllFields_CurrrentThumbnail"),
				"background-color", "rgba(238, 130, 238, 1)");
	}

	public static void verify_Current_Thumbnail_BackgroundYellow() {
		BaseUI.verifyElementHasExpected_CSSAttribute(Locator.lookupElement("dataComplete_AllFields_CurrrentThumbnail"),
				"background-color", "rgba(255, 255, 0, 1)");
	}

	public static void verify_workItem_Count(Integer expectedCount) {
		ArrayList<WebElement> workItemList = Locator.lookup_multipleElements("dataComplete_AllFields_WorkItem_List",
				null, null);
		BaseUI.verify_true_AndLog(workItemList.size() == expectedCount,
				"Work Item list matched " + expectedCount.toString(), "Expected " + expectedCount.toString()
						+ " work items, but seeing " + String.valueOf(workItemList.size()));

	}

	public static void click_InsertButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_InsertButton"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	// Indexes for Xpaths start with 1
	// This method will return an index of a Work Item that has more than 1 work
	// item and has issues that need to be resolved.
	public static Integer return_An_Index_ToReject(TableData availableOptions) {

		Integer checkCount = availableOptions.countOf_Matches_ThatHave_ColumnName_AndCellValue("Payment Type", "C");
		Integer stubCount = availableOptions.countOf_Matches_ThatHave_ColumnName_AndCellValue("Payment Type", "S");

		Integer indexToUse = null;

		if (stubCount > 1) {

			for (HashMap<String, String> row : availableOptions.data) {
				if (row.get("Payment Type").equals("S") && Integer.parseInt(row.get("Correction Count")) > 0) {
					indexToUse = availableOptions.data.indexOf(row) + 1;
				}
			}
		}

		// If we couldn't find a match from Checks let's try to find a match
		// from Stubs
		if (indexToUse == null && checkCount > 1) {
			for (HashMap<String, String> row : availableOptions.data) {
				if (row.get("Payment Type").equals("C") && Integer.parseInt(row.get("Correction Count")) > 0) {
					indexToUse = availableOptions.data.indexOf(row) + 1;
				}
			}
		}

		BaseUI.verify_true_AndLog(indexToUse != null, "Found an index to use for testing.",
				"Could not find an appropriate index to use for testing.");

		return indexToUse;
	}

	public static Integer return_ItemsToFix_Count() {
		String elementText = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_FBL_Visible_NumberOfItems_ToFix"));

		elementText = elementText.replace("Fix:", "").trim();

		return Integer.parseInt(elementText);
	}

	public static void verify_AutoInsertButton_EnabledORDisabled(Boolean shouldBeEnabled) {

		WebElement autoInsertButton = Locator.lookupElement("dataComplete_AllFields_AutoInsert_Button");

		if (shouldBeEnabled) {
			BaseUI.verify_true_AndLog(
					BaseUI.elementHasExpectedAttribute(autoInsertButton, "class", "btn toggledButton"),
					"Auto Insert was enabled.", "Auto Insert was NOT enabled.");

		} else {
			BaseUI.verify_true_AndLog(BaseUI.elementHasExpectedAttribute(autoInsertButton, "class", "btn"),
					"Auto Insert was NOT enabled.", "Auto Insert was enabled.");
		}

	}

	public static void click_DocId_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_DocId_Button"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_AllFields_DocId_Modal", null, null);
		Thread.sleep(500);
	}

	public static void verify_DocID_Modal_GridRow_Selected(Integer docID) {
		WebElement docIDCell = Locator.lookupElement("dataComplete_AllFields_DocId_DocIDGridCell_ByText",
				docID.toString(), null);
		WebElement documentNameCell = Locator.lookupElement("dataComplete_AllFields_DocId_DocumentNameGridCell_ByText",
				docID.toString(), null);

		BaseUI.verifyElementHasExpectedPartialAttributeValue(docIDCell, "class", "jqx-grid-cell-selected");
		BaseUI.verifyElementHasExpectedPartialAttributeValue(documentNameCell, "class", "jqx-grid-cell-selected");
	}

	public static void select_DocID_Modal_Document_ByDocID(Integer docID) throws InterruptedException {
		BaseUI.click(
				Locator.lookupElement("dataComplete_AllFields_DocId_DocIDGridCell_ByText", docID.toString(), null));
		Thread.sleep(300);
	}

	public static void click_DocID_Modal_SelectDocument_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_DocId_Modal_SelectDocumentButton"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_AllFields_DocId_Modal", null, null, 20);
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();
	}

	// Indexes of Xpaths start with 1
	public static void select_WorkItem_ByIndex(Integer index) throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_FBL_SeqLabel_ByIndex", index.toString(), null));
		Thread.sleep(1500);
		// Navigation.wait_For_Page_ToLoad();
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void select_1st_Check() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_FBL_First_Check"));
		Thread.sleep(1500);
		// Navigation.wait_For_Page_ToLoad();
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void select_1st_Stub() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_FBL_First_Stub"));
		Thread.sleep(1500);
		// Navigation.wait_For_Page_ToLoad();
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void verify_INS_Present_ForCurrentlySelection() {
		WebElement insElement = Locator.lookupElement("dataComplete_AllFields_Current_INS_Field");
		BaseUI.verifyElementAppears(insElement);
		BaseUI.verifyElementHasExpectedPartialTextByElement(insElement, "Ins");
	}

	public static void verify_NB_Present_ForCurrentlySelection() {
		WebElement insElement = Locator.lookupElement("dataComplete_AllFields_Current_NB_Field");
		BaseUI.verifyElementAppears(insElement);
		BaseUI.verifyElementHasExpectedPartialTextByElement(insElement, "NB");
	}

	public static void click_DefDoc_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_DefDoc_Button"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_AllFields_DefDoc_Modal", null, null, 10);
		Thread.sleep(500);
	}

	public static void select_DefDoc_Reason(Integer docIDToSelect) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_DefDoc_Modal_Row_ByDocID", docIDToSelect.toString(),
				null));
		Thread.sleep(200);
	}

	public static void click_DefDoc_SelectDefaultInsertDocument_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_DefDoc_Modal_SelectDefault_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_AllFields_DefDoc_Modal", null, null, 10);
		Thread.sleep(300);
	}

	public static void update_DefaultInsertDocument(Integer docIDToSelect) throws Exception {
		click_DefDoc_Button();
		select_DefDoc_Reason(docIDToSelect);
		click_DefDoc_SelectDefaultInsertDocument_Button();
	}

	public static void verify_DefDoc_Button_HasCorrectNumber(Integer expectedDocID) {
		BaseUI.verifyElementHasExpectedText("dataComplete_AllFields_DefDoc_Button",
				"Def Doc (" + expectedDocID.toString() + ")");
	}

	public static void clear_Current_Errors_WithGenericValues() throws Exception {
		ArrayList<WebElement> fieldList = Locator.lookup_multipleElements("dataMaint_FBL_ListOfFields", null, null);

		for (Integer i = 0; i < fieldList.size(); i++) {

			WebElement refreshedFieldElement = Locator.lookupElement("dataMaint_FBL_Field_ByIndex",
					String.valueOf(i + 1), null);

			WebElement firstSpan = refreshedFieldElement.findElement(By.xpath("./span[1]"));

			if (!BaseUI.stringEmpty(BaseUI.getTextFromField(firstSpan))) {

				String fieldName = BaseUI.getTextFromField(
						refreshedFieldElement.findElement(By.xpath("./div[@data-bind='text: FieldName']")));

				WebElement textBox = refreshedFieldElement.findElement(By.xpath("./input"));

				if (fieldName.contains("Amt") || fieldName.contains("Amount")) {
					BaseUI.enterText_IntoInputBox_DontUseClear(textBox, "500");
				} else if (fieldName.contains("Number")) {
					String textBox_Text = BaseUI.getTextFromInputBox(textBox);
					if (BaseUI.stringEmpty(textBox_Text)) {
						BaseUI.enterText_IntoInputBox_DontUseClear(textBox, "12345");
					} else {
						BaseUI.click(textBox);
					}
				} else if (fieldName.contains("Name")) {
					BaseUI.enterText_IntoInputBox_DontUseClear(textBox, "GeorgioAny");
				} else if (fieldName.contains("MI")) {
					BaseUI.enterText_IntoInputBox_DontUseClear(textBox, "Od");
				} else {
					BaseUI.verify_true_AndLog(false, "",
							"Unrecognized field " + fieldName + ", unable to clear error.");
				}

				BaseUI.enterKey(textBox, Keys.ENTER);
				Thread.sleep(500);
				BaseUI.wait_forPageToFinishLoading();

			}

		}

	}

	public static void equalize_Stub_And_Check_Amounts() throws Exception {

		String chkString = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_FBL_CheckAmount"));
		String stubString = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_FBL_StubAmount"));

		Double checkAmount = Double.parseDouble(chkString.replace("$", ""));
		Double stubAmount = Double.parseDouble(stubString.replace("$", ""));

		if (checkAmount > stubAmount) {
			select_1st_Stub();

			Double amountToAdd = checkAmount - stubAmount;

			Double amountPresent = Double.parseDouble(
					BaseUI.get_NonVisible_TextFromField(Locator.lookupElement("dataMaint_FBL_BalanceAmt_Textbox")));

			Double newAmountToEnter = amountToAdd + amountPresent;
			String amountToEnterAsTring = BaseUI.convertDouble_ToString_ForCurrency(newAmountToEnter).replace(".", "");

			BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement("dataMaint_FBL_BalanceAmt_Textbox"),
					amountToEnterAsTring);

		} else if (stubAmount > checkAmount) {
			select_1st_Check();

			Double amountToAdd = stubAmount - checkAmount;

			String amountPresent_AsString = BaseUI
					.getTextFromInputBox(Locator.lookupElement("dataMaint_FBL_BalanceAmt_Textbox"));
			// String amountPresent_AsString =
			// BaseUI.get_NonVisible_TextFromField(Locator.lookupElement("dataMaint_FBL_BalanceAmt_Textbox"));
			if (!BaseUI.string_IsDouble(amountPresent_AsString)) {
				Thread.sleep(100);
			}
			Double amountPresent = Double.parseDouble(amountPresent_AsString);

			Double newAmountToEnter = amountToAdd + amountPresent;
			String amountToEnterAsTring = BaseUI.convertDouble_ToString_ForCurrency(newAmountToEnter).replace(".", "");

			BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_FBL_BalanceAmt_Textbox"),
					amountToEnterAsTring);
		}

	}

	public static void clear_Errors_CurrentTransaction() throws Exception {
		TableData stubsAndChecks = return_StubAndCheck_Rows();

		// Loop through our data
		for (Integer i = 0; i < stubsAndChecks.data.size(); i++) {
			Integer correctionCount = Integer.parseInt(stubsAndChecks.data.get(i).get("Correction Count"));
			// Where there's a correction Count > 0, we'll clear out the
			// corrections.
			if (correctionCount > 0) {
				select_WorkItem_ByIndex(i + 1);
				clear_Current_Errors_WithGenericValues();
			}
		}

		equalize_Stub_And_Check_Amounts();

	}

	public static void clear_Errors_CurrentTransaction(TableData stubsAndChecks) throws Exception {
		// Loop through our data
		for (Integer i = 0; i < stubsAndChecks.data.size(); i++) {
			Integer correctionCount = Integer.parseInt(stubsAndChecks.data.get(i).get("Correction Count"));
			// Where there's a correction Count > 0, we'll clear out the
			// corrections.
			if (correctionCount > 0) {
				select_WorkItem_ByIndex(i + 1);
				clear_Current_Errors_WithGenericValues();
			}
		}
	}

	public static void expand_TransactionList() throws Exception {

		WebElement arrowElement = Locator.lookupElement("dataMaint_FBL_Expand_TransactionList_Arrow");

		if (!BaseUI.get_Attribute_FromField(arrowElement, "class")
				.equals("jqx-icon-arrow-down jqx-icon jqx-icon-arrow-down-selected")) {
			BaseUI.click(arrowElement);
			Thread.sleep(500);
		}
	}

	public static void click_Tran_InTranList_ByTranNubmer(Integer tranNumber) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_Transaction_FromTranList_ByTranNumber",
				tranNumber.toString(), null));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void verify_AllItems_ForCurrentTran_Suspended(Integer expectedWorkItemCount) {
		ArrayList<WebElement> suspendedList = Locator
				.lookup_multipleElements("dataComplete_AllFields_RejectReason_AllWorkItems", null, null);

		BaseUI.verify_true_AndLog(suspendedList.size() > 0, "Found work items reject reason fields.",
				"Did NOT find reject reason fields.");

		BaseUI.verify_true_AndLog(suspendedList.size() == expectedWorkItemCount,
				"Work Item Count matched " + expectedWorkItemCount.toString(), "Work Item Count was "
						+ String.valueOf(suspendedList.size()) + " But expected " + expectedWorkItemCount.toString());

		verify_Count_ErrorSymbols_CurrentTran(0);

		for (WebElement rejReason : suspendedList) {
			BaseUI.verifyElementHasExpected_CSSAttribute(rejReason, "background-color", "rgba(255, 0, 0, 1)");
			BaseUI.verifyElementHasExpectedText(rejReason, "sus");
		}
	}

	public static void verify_AllItems_ForCurrentTran_NOT_Suspended(Integer expectedWorkItemCount,
			Integer expectedErrorCount) {
		ArrayList<WebElement> suspendedList = Locator
				.lookup_multipleElements("dataComplete_AllFields_RejectReason_AllWorkItems", null, null);

		BaseUI.verify_true_AndLog(suspendedList.size() > 0, "Found work items reject reason fields.",
				"Did NOT find reject reason fields.");

		BaseUI.verify_true_AndLog(suspendedList.size() == expectedWorkItemCount,
				"Work Item Count matched " + expectedWorkItemCount.toString(), "Work Item Count was "
						+ String.valueOf(suspendedList.size()) + " But expected " + expectedWorkItemCount.toString());

		verify_Count_ErrorSymbols_CurrentTran(expectedErrorCount);

		for (WebElement rejReason : suspendedList) {
			if (Browser.currentBrowser.equals("internetexplorer")) {
				BaseUI.verifyElementHasExpected_CSSAttribute(rejReason, "background-color", "transparent");
			} else {
				BaseUI.verifyElementHasExpected_CSSAttribute(rejReason, "background-color", "rgba(0, 0, 0, 0)");
			}
			BaseUI.verifyElementHasExpectedText(rejReason, "---");
		}
	}

	public static void verify_Tran_Suspended_InList_ByTranNumber(Integer tranNumber) {
		BaseUI.verifyElementHasExpectedPartialTextByElement(Locator.lookupElement(
				"dataComplete_AllFields_Transaction_FromTranList_ByTranNumber", tranNumber.toString(), null), "Sus");

	}

	public static void click_TransactionList_ByTranNumber(Integer tranNumber) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_Transaction_FromTranList_ByTranNumber",
				tranNumber.toString(), null));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void clear_Errors_AllTransactions() throws Exception {
		expand_TransactionList();
		ArrayList<WebElement> transactionList = Locator.lookup_multipleElements("dataMaint_FBL_Transaction_FixList",
				null, null);

		// ArrayList<String, String> transactionFix_text = new ArrayList<String,
		// String>();

		for (Integer i = 0; i < transactionList.size(); i++) {
			expand_TransactionList();

			WebElement transaction_FixElement = Locator.lookupElement("dataMaint_FBL_Transaction_ByIndex",
					String.valueOf(i + 1), null);

			Integer fixCount = Integer
					.parseInt(BaseUI.get_NonVisible_TextFromField(transaction_FixElement).replace("Fix:", "").trim());
			if (fixCount > 0) {
				BaseUI.click(transaction_FixElement);
				Thread.sleep(500);
				BaseUI.wait_forPageToFinishLoading();

				clear_Errors_CurrentTransaction();
			}
		}
	}

	// Xpath Indexes start at 1
	public static void verify_Item_WasRejected_ByIndex(Integer indexToVerify) {
		String nameOfJob = BaseUI
				.getTextFromField(Locator.lookupElement("navigation_Link_ByText", "Data Completion", null));
		nameOfJob = nameOfJob.substring(nameOfJob.indexOf("(") + 1, nameOfJob.length() - 1);

		WebElement rejReason = Locator.lookupElement("dataMaint_FBL_RejectReason_ByIndex", indexToVerify.toString(),
				null);

		BaseUI.verifyElementHasExpectedText(rejReason, nameOfJob);
		BaseUI.verifyElementHasExpected_CSSAttribute(rejReason, "background-color", "rgba(255, 0, 0, 1)");

	}

	// Xpath Indexes start at 1
	public static void verify_Item_Was_NOT_Rejected_ByIndex(Integer indexToVerify) {
		BaseUI.verifyElementHasExpectedText(
				Locator.lookupElement("dataMaint_FBL_RejectReason_ByIndex", indexToVerify.toString(), null), "---");

	}

	public static void verify_Selected_Seq(Integer expectedSeq) {
		String currentSeq = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_AllFields_CurrentSeqNumber"));
		BaseUI.baseStringCompare("Current Seq Number", expectedSeq.toString(), currentSeq);

	}

	public static void click_AutoInsert() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_AutoInsert_Button"));
		Thread.sleep(300);
	}

	public static void deselect_AutoInsert() throws Exception {
		WebElement autoInsertButton = Locator.lookupElement("dataComplete_AllFields_AutoInsert_Button");
		if (BaseUI.elementHasExpectedAttribute(autoInsertButton, "class", "btn toggledButton")) {
			click_AutoInsert();
			BaseUI.waitForElementToContain_AttributeMatch("dataComplete_AllFields_AutoInsert_Button", null, null,
					"class", "btn", 5);
		}

	}

	public static void verify_Selected_Doc(Integer expectedDoc) {
		String currentDoc = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_AllFields_CurrentDocNumber"));
		BaseUI.baseStringCompare("Current Doc Number", expectedDoc.toString(), currentDoc);

	}

	public static void verify_AutoUpdate_Enabled() {
		BaseUI.verifyElementHasExpectedPartialAttributeValue(
				Locator.lookupElement("dataComplete_AllFields_AutoUpdate_Button"), "class", "btn toggledButton");
	}

	public static void verify_AutoUpdate_Disabled() {
		BaseUI.verify_true_AndLog(
				BaseUI.elementHasExpectedAttribute(Locator.lookupElement("dataComplete_AllFields_AutoUpdate_Button"),
						"class", "btn"),
				"Auto Update was NOT enabled.", "Auto Update was enabled.");
	}

	public static void click_AutoUpdate() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AllFields_AutoUpdate_Button"));
		Thread.sleep(300);
	}

	public static void deselect_AutoUpdate() throws Exception {
		WebElement autoUpdateButton = Locator.lookupElement("dataComplete_AllFields_AutoUpdate_Button");
		if (BaseUI.elementHasExpectedAttribute(autoUpdateButton, "class", "btn toggledButton")) {
			click_AutoUpdate();
			BaseUI.waitForElementToContain_AttributeMatch("dataComplete_AllFields_AutoUpdate_Button", null, null,
					"class", "btn", 5);
		}

	}

	public static void verify_TextBox_HasFocus(String expectedTextBox_LabelText) {
		// BaseUI.verifyElementHasExpectedAttributeValue(
		// Locator.lookupElement("dataComplete_AllFields_FieldContainer_ByText",
		// expectedTextBox_LabelText, null),
		// "class", "fieldlistitem current\r\n" + "");
		BaseUI.verifyElementHasFocus(Locator.lookupElement("dataComplete_AllFields_FieldTextBox_ByLabelText",
				expectedTextBox_LabelText, null));
	}

	public static void verify_TextBoxFocus_TranBal_CurrentChk_SelectedSeq(String fieldToKey, String expectedTranTotal,
			String expectedChkTotal, Integer expectedSeq) {
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToKey);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(expectedTranTotal);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(expectedChkTotal);
		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);
	}

	public static void verify_AllTextBoxLabels_DontContainValue(String valueWeShouldntSee) {
		ArrayList<WebElement> labelList = Locator
				.lookup_multipleElements("dataComplete_AllFields_AllCurrentTextBoxLabels", null, null);

		BaseUI.verify_true_AndLog(labelList.size() > 0, "Found Labels.", "Did NOT find labels.");

		for (WebElement labelElement : labelList) {
			BaseUI.verifyElementDoesNotHaveExpectedText(labelElement, valueWeShouldntSee);
		}

	}

	public static void verify_TextBox_SurroundedBy_PinkBorder(String textBoxLabelText) {
		WebElement textBoxContainer = Locator.lookupElement("dataComplete_AllFields_FieldContainer_ByText",
				textBoxLabelText, null);

		String expectedColor = "rgba(255, 112, 255, 1)";
		String expectedBorderWidth = "2px";

		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-top-color", expectedColor);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-bottom-color", expectedColor);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-top-color", expectedColor);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-left-color", expectedColor);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-right-color", expectedColor);

		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-top-width", expectedBorderWidth);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-bottom-width", expectedBorderWidth);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-left-width", expectedBorderWidth);
		BaseUI.verifyElementHasExpected_CSSAttribute(textBoxContainer, "border-right-width", expectedBorderWidth);
	}

	public static void verify_TranFix_NumberAndColor_Correct(Integer expectedNumber) {
		WebElement fixCount = Locator.lookupElement("dataComplete_AllFields_CurrentTranFixCount");

		String fixText = BaseUI.getTextFromField(fixCount);
		fixText = fixText.split("\\:")[1];

		BaseUI.baseStringCompare("Transaction Fix Count", expectedNumber.toString(), fixText);
		if (expectedNumber > 0) {
			BaseUI.verifyElementHasExpectedAttributeValue(fixCount, "style", "color: red;");
		} else {
			BaseUI.verifyElementHasExpectedAttributeValue(fixCount, "style", "");
		}

	}

	public static void verify_Balances_TextBoxHasFocus_ErrorCount_FixCount_and_CorrectSeq(String tranBalance,
			String chkBal, String stbBal, String textBoxToHaveFocus, Integer errorCount, Integer fixCount,
			Integer expectedSeq) {
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrentTransactionBalance(tranBalance);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenChk(chkBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_CurrenStb(stbBal);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(textBoxToHaveFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(errorCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);

		DataCompletion_fbl_AllFieldsBalancing.verify_Selected_Seq(expectedSeq);

	}

	public static void verify_Count_ErrorSymbols(Integer expectedCount) {
		ArrayList<WebElement> errorSymbolList;
		if (Browser.currentBrowser.startsWith("chrome")) {
			errorSymbolList = Locator
					.lookup_multipleElements("dataComplete_AllFields_ErrorSymbols", null, null);

		} else {
			 errorSymbolList = Locator
					.lookup_multipleElements("dataComplete_AllFields_ErrorSymbols_Ie", null, null);
		}
		BaseUI.verify_true_AndLog(errorSymbolList.size() == expectedCount,
				"Expected Error Count matched " + expectedCount.toString(), "Expected Error Count: "
						+ expectedCount.toString() + " but seeing: " + String.valueOf(errorSymbolList.size()));
	}

	public static void verify_StatusError(String expectedErrorText) {
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_AllFields_Status_ErrorMessage"),
				expectedErrorText);
	}

	public static void verify_Count_ErrorSymbols_CurrentTran(Integer expectedCount) {
		ArrayList<WebElement> errorSymbolList = Locator
				.lookup_multipleElements("dataComplete_AllFields_Thumbnail_ErrorSymbols", null, null);

		Integer totalErrors = 0;

		for (WebElement errorSymbol : errorSymbolList) {
			totalErrors += Integer.parseInt(BaseUI.getTextFromField(errorSymbol));
		}

		BaseUI.verify_true_AndLog(totalErrors == expectedCount,
				"Expected Error Count matched " + expectedCount.toString(),
				"Expected Error Count: " + expectedCount.toString() + " but seeing: " + totalErrors.toString());
	}

	public static void verify_Error_Visible_ForTextField_AndTitleMatches(String fieldToCheck, String expectedMessage) {
		// if (Browser.currentBrowser.equals("chrome")){
		WebElement errorElement = Locator.lookupElement("dataComplete_AllFields_ErrorIcon_ByFieldText", fieldToCheck,
				null);

		// }else{

		// WebElement errorElement =
		// Locator.lookupElement("dataComplete_AllFields_ErrorIcon_ByFieldText_Ie",
		// fieldToCheck,
		// null);

		BaseUI.verifyElementAppears(errorElement);
		BaseUI.verifyElementHasExpectedAttributeValue(errorElement, "title", expectedMessage);

	}

	// }
	public static void verify_Error_NOT_Visible_ForTextField(String fieldToCheck) {
		WebElement errorElement = Locator.lookupElement("dataComplete_AllFields_ErrorIcon_ByFieldText", fieldToCheck,
				null);
		BaseUI.verifyElementDoesNotAppear(errorElement);
	}

	public static void enterTextIntoFields(HashMap<String, String> fieldsToKey) throws Exception {
		for (String field : fieldsToKey.keySet()) {
			enterTextIntoField(field, fieldsToKey.get(field));

		}

	}

	public static void enterTextIntoField(String fieldIdentifyingText, String textToEnter) throws Exception {
		BaseUI.enterText_IntoInputBox_DontUseClear(
				Locator.lookupElement("dataComplete_AllFields_FieldTextBox_ByLabelText", fieldIdentifyingText, null),
				textToEnter);

		BaseUI.enterKey(
				Locator.lookupElement("dataComplete_AllFields_FieldTextBox_ByLabelText", fieldIdentifyingText, null),
				Keys.ENTER);
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();

		if (BaseUI.pageSourceContainsString("Please rekey this field")) {
			BaseUI.enterText_IntoInputBox_DontUseClear(Locator.lookupElement(
					"dataComplete_AllFields_FieldTextBox_ByLabelText", fieldIdentifyingText, null), textToEnter);

			BaseUI.enterKey(Locator.lookupElement("dataComplete_AllFields_FieldTextBox_ByLabelText",
					fieldIdentifyingText, null), Keys.ENTER);
			Thread.sleep(1000);
			Navigation.wait_For_Page_ToLoad();

		}

	}

	public static void verify_CurrentTransactionBalance(String expectedText) {
		WebElement chk_Balance = Locator.lookupElement("dataComplete_AllFields_CurrentTransactionBalance");

		BaseUI.verifyElementHasExpectedText(chk_Balance, expectedText);

		if (expectedText.equals("$0.00")) {
			String chkBalance_Style = BaseUI.get_Attribute_FromField(chk_Balance, "style");
			BaseUI.verify_true_AndLog(chkBalance_Style.equals(""), "Color style was not set, text is black.",
					"Was expecting style attribute to not be set, which means element should have been black, but seeing "
							+ chkBalance_Style);
		} else {
			BaseUI.verifyElementHasExpectedPartialAttributeValue(chk_Balance, "style", "color: red;");
		}
	}

	public static void verify_CurrenChk(String expectedText) {
		WebElement chk_Balance = Locator.lookupElement("dataComplete_AllFields_CurrentChk");

		BaseUI.verifyElementHasExpectedText(chk_Balance, expectedText);

		// if (expectedText.equals("$0.00")) {
		// BaseUI.verifyElementHasExpectedPartialAttributeValue(TransactionBalance,
		// "style", null);
		// } else {
		// BaseUI.verifyElementHasExpectedPartialAttributeValue(TransactionBalance,
		// "style", "color:red");
		// }
	}

	public static void verify_CurrenStb(String expectedText) {
		WebElement stb_Balance = Locator.lookupElement("dataComplete_AllFields_CurrentStub");

		BaseUI.verifyElementHasExpectedText(stb_Balance, expectedText);

		// if (expectedText.equals("$0.00")) {
		// BaseUI.verifyElementHasExpectedPartialAttributeValue(TransactionBalance,
		// "style", null);
		// } else {
		// BaseUI.verifyElementHasExpectedPartialAttributeValue(TransactionBalance,
		// "style", "color:red");
		// }
	}

	public static void verify_TransactionNumber(Integer expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataComplete_AllFields_CurrentTranNumber",
				"Trn#" + expectedValue.toString());
	}

	// Use this method when you key a field and it goes to the next field.
	public static void verify_Field_HasFocus_AndFix_AndError_CountsCorrect(Integer fieldsToCorrectCount,
			String fieldToHaveFocus, Integer fixCount) {
		DataCompletion_fbl_AllFieldsBalancing.verify_Count_ErrorSymbols(fieldsToCorrectCount);
		DataCompletion_fbl_AllFieldsBalancing.verify_TextBox_HasFocus(fieldToHaveFocus);
		DataCompletion_fbl_AllFieldsBalancing.verify_TranFix_NumberAndColor_Correct(fixCount);
	}

}// End Of Class
