package imageRPS.pages;

import org.openqa.selenium.Keys;
import utils.BaseUI;
import utils.Locator;

public class DataCompletion_bal {

	public static void balance_OneAmount() throws Exception {

		String inputLabel = return_PaymentType();
		String amountToEnter = "";
		if (inputLabel.equals("Chk")) {
			amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_StbAmount"));
		} else {
			amountToEnter = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChkAmount"));
		}

		if (amountToEnter.equals("0.00")) {
			amountToEnter = "1";
		}

		amountToEnter = amountToEnter.replace(".", "");

		enterValue_IntoInputBox(amountToEnter);

	}

	public static void enterValue_IntoInputBox(String valueToEnter) throws Exception {
		
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_BAL_VisibleInputBox"), valueToEnter);
		BaseUI.enterKey(Locator.lookupElement("dataComplete_BAL_VisibleInputBox"), Keys.ENTER);
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void key_Through_AllBalanceItems() throws Exception {

		Integer loopCount = 0;

		while (loopCount < 50) {
			balance_OneAmount();

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}
			loopCount++;
		}

	}
	
	public static void key_Through_AllBalanceItemsBy_enterValue(String valueToEnter) throws Exception {

		Integer loopCount = 0;

		while (loopCount < 50) {
			//balance_OneAmount();
			enterValue_IntoInputBox(valueToEnter);

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}
			loopCount++;
		}

	}
	
	public static void click_Yes_ToConfirm_AndWaitForAlert() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Confirm_YesButton"));
		wait_ForAlert_ToAppear();

	}

	public static void wait_ForAlert_ToAppear() {
		BaseUI.waitForElementToBeDisplayed("dataComplete_BAL_Alert_Dialog", null, null);
	}

	public static void verify_Alert_DialogPresent() {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BAL_Alert_Dialog"));
		BaseUI.verifyElementHasExpectedText("dataComplete_BAL_Alert_Text",
				"This batch is ready for pp2 and does not require keying.");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BAL_Alert_OK_Button"));
	}

	public static void click_OK_ForAlertModal() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_BAL_Alert_OK_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_BAL_Alert_Dialog", null, null, 10);
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void verify_Balanced() {
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BAL_Amount"), "0.00");

		BaseUI.verifyElementHasExpected_CSSAttribute(Locator.lookupElement("dataComplete_BAL_Amount"),
				"background-color", "rgba(0, 136, 0, 1)");

		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BAL_StatusLabel"), "Balanced!");

		String inputLabel = return_PaymentType();
		String inputBoxAmount = "";
		String checkAmount = "";
		if (inputLabel.equals("Chk")) {
			inputBoxAmount = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_BAL_VisibleInputBox"));
			checkAmount = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_ChkAmount"));
		} else {
			inputBoxAmount = BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_BAL_VisibleInputBox"));
			checkAmount = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_StbAmount"));
		}
		BaseUI.baseStringCompare("Amount", inputBoxAmount, checkAmount);
	}

	public static void verify_StubsTooHigh(String expectedAmountUnbalanced) {
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BAL_StatusLabel"), "Stubs too high");
		BaseUI.verifyElementHasExpected_CSSAttribute(Locator.lookupElement("dataComplete_BAL_Amount"),
				"background-color", "rgba(136, 0, 0, 1)");

		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BAL_Amount"), expectedAmountUnbalanced);
	}

	public static String return_PaymentType() {
		String inputLabel = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_VisibleInput_Label"));
		return inputLabel;
	}

	public static String return_firstSeqNumber() {
		String seqNumber = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BAL_FirstSeqNumber"));
		return seqNumber;
	}

	public static void veify_BAL_Toggle_ToolbarIcon() throws Exception {
		click_BAL_ZoomIn_And_ZoomOut_Works();
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BAL_Toggle_ToolbarIcon"));
		click_BAL_ZoomIn_And_ZoomOut_Works();
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_BAL_Toggle_ToolbarIcon"));
		
	}
	
	public static void click_BAL_ZoomIn_And_ZoomOut_Works() throws Exception {
		
		BaseUI.click(Locator.lookupElement("dataComplete_BAL_Toggle_btn"));
		Thread.sleep(500);
		
	}
	
	
	
}// End of class
