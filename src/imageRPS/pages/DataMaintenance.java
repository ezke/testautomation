package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class DataMaintenance {

	public static void navigate_to_SubTab(String buttonText) throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SubTab_ByText", buttonText, null));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	
	public static String return_Bi_Keying_Job() {
		String Keying_Job = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_KeyingJob_TextPresent"));

		return Keying_Job;
	}
	
	public static void verify_Bi_Keying_Job() throws Exception {
		
		String[] expectedAnyArray = { "pp2", "p2", "dow", "out" };
		
		String expected_Jobs = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_KeyingJob_TextPresent"));

		BaseUI.return_indexOf_PartialMatchInArray(expectedAnyArray, expected_Jobs);
		
	}
	
	public static void verify_SubTab_Active(String buttonText) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_SubTab_Active_ByText", buttonText, null));
	}
	
	public static String return_dataMaint_BasicInfo_BatchId() {
		String batchId = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_BatchId_TextPresent"));

		return batchId;
	}
	
	public static String return_dataMaint_BasicInfo_BatchNumber() {
		String batchNumber = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_BatchNumber_TextPresent"));

		return batchNumber;
	}
	
	public static String return_dataMaint_BasicInfo_DocGroupId() {
		String docGroupId = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_DocGroupId_TextPresent"));

		return docGroupId;
	}
	
	
	public static String return_dataMaint_BasicInfo_DocGroupName() {
		String docGroupName = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_DocGroupName_TextPresent"));

		return docGroupName;
	}
	
	public static String return_dataMaint_BasicInfo_ClientId() {
		String clientId = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_ClientId_TextPresent"));

		return clientId;
	}
	
	public static String return_dataMaint_BasicInfo_LockboxId() {
		String lockboxId = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_LockboxId_TextPresent"));

		return lockboxId;
	}	
	
	public static String return_dataMaint_BasicInfo_ClientName() {
		String clientName = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_ClientName_TextPresent"));

		return clientName;
	}
	
	public static String return_dataMaint_BasicInfo_SourceBatchId() {
		String sourceBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_SourceBatchId_TextPresent"));

		return sourceBatchId;
	}
	
	public static String return_dataMaint_BasicInfo_BatchType() {
		String batchType = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_BatchType_TextPresent"));

		return batchType;
	}
	
	public static String return_dataMaint_BasicInfo_BatchDate() {
		String batchDate = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_BatchDate_TextPresent"));

		return batchDate;
	}
	
	public static String return_dataMaint_BasicInfo_WorkType() {
		String workType = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_WorkType_TextPresent"));

		return workType;
	}
	
	public static String return_dataMaint_BasicInfo_WorkFlow() {
		String workFlow = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_WorkFlow_TextPresent"));

		return workFlow;
	}
	
	public static String return_dataMaint_BasicInfo_Balanced() {
		String valanced = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_Balanced_TextPresent"));

		return valanced;
	}
	
	public static String return_dataMaint_BasicInfo_KeyingJob() {
		String keyingJob = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_KeyingJob_TextPresent"));

		return keyingJob;
	}
	
	public static String return_dataMaint_BasicInfo_Priority() {
		String priority = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_Priority_TextPresent"));

		return priority;
	}
	
	public static String return_dataMaint_BasicInfo_CutoffDateTime() {
		String cutoffDateTime = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_CutoffDateTime_TextPresent"));

		return cutoffDateTime;
	}
	
	public static String return_dataMaint_BasicInfo_TrayId() {
		String trayId = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BasicInfo_TrayId_TextPresent"));

		return trayId;
	}
}// End of Class













