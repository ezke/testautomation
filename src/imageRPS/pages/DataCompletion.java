package imageRPS.pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class DataCompletion {

	public static void verify_DataCompletionPage_Loaded() {
		Navigation.verify_Page_Active(TopNavigationTab.DataCompletion);
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_KeyingJobs_Header"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_NonKeyingJobs_Header"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void click_DoneButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_DoneButton"));
		Thread.sleep(1000);
	}

	public static void click_Abbreviation(String keyingJobAbbreviation) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_ClientSelectionTable_ByClientAbbreviation",
				keyingJobAbbreviation, null));

		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void end_DataCompletion() throws Exception {
		click_DoneButton();
		click_Yes_ToConfirm();
	}

	public static void click_Yes_ToConfirm() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Confirm_YesButton"));
		Thread.sleep(3000);

		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_OK_on_Alert() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_AlertModal_OKButton"));
		Thread.sleep(3000);

		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_AlertModal_Message(String expectedText) {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_AlertModal"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_AlertModal_Message"), expectedText);
	}

	public static void click_HelpButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_HelpButton"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_No_ToConfirm() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Confirm_NoButton"));
		Thread.sleep(3000);

		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_No_ToCancel() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Confirm_NoButton"));
		Thread.sleep(3000);

		Navigation.wait_For_Page_ToLoad();

	}

	public enum ConfirmAction {
		Done("Done", "No more items to key in this batch. OK to move to next?",
				"Done button pressed. OK to exit?"), Lock("Lock", "Lock this batch. OK to move to next?",
						"Lock this batch. OK to exit?"), UnSusp("UnSuspend",
								"Are you sure you want to unsuspend this transaction?");

		private String value;
		private String modalMessage;
		private String modalMessage2;

		private ConfirmAction(final String val, final String message) {
			value = val;
			modalMessage = message;
		}

		private ConfirmAction(final String val, final String message, final String message2) {
			value = val;
			modalMessage = message;
			modalMessage2 = message2;
		}

		public String getValue() {
			return value;
		}

		public String getModalMessage() {
			return modalMessage;
		}

		public String getModalMessage2() {
			return modalMessage2;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	public static void verify_ConfirmModal_Appears(ConfirmAction confirmAction) {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"));

		String possibleMessage1 = confirmAction.getModalMessage();
		String possibleMessage2 = confirmAction.getModalMessage2();

		String actualMessage = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Confirm_Modal_Message"));
		BaseUI.verify_true_AndLog(actualMessage.equals(possibleMessage1) || actualMessage.equals(possibleMessage2),
				"Modal message matched one of the passing messages.",
				"Modal message of '" + actualMessage + "' was not expected.");

	}

	public static void verify_ConfirmModal_Appears(String confirmMessage) {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"));

		String actualMessage = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Confirm_Modal_Message"));
		BaseUI.verify_true_AndLog(confirmMessage.equals(actualMessage),
				"Modal message matched one of the passing messages.",
				"Modal message of '" + actualMessage + "' was not expected. Expected Message was '" + confirmMessage + " '");

	}

	public static void verify_ConfirmModal_Buttons_Appears(){

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_ConfirmModal_Accept_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_ConfirmModal_Reject_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_ConfirmModal_Rekey_Button"));
	}

	public static void verify_ConfirmModal_NotDisplayed() {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Confirm_Modal"));
	}

	public static void click_ConfirmModal_Accept_Button(){
		BaseUI.click(Locator.lookupElement("dataComplete_ConfirmModal_Accept_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Confirm_Modal", null, null, 25);
	}

	public static void click_ConfirmModal_Reject_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_ConfirmModal_Reject_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Confirm_Modal", null, null, 25);
		BaseUI.waitForElementToBeDisplayed("dataComplete_Reject_Dialog", null, null,25);
		//Just the wait method was not working so had add sleep
		Thread.sleep(1500);
	}

	public static void click_ConfirmModal_Rekey_Button(){
		BaseUI.click(Locator.lookupElement("dataComplete_ConfirmModal_Rekey_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Confirm_Modal", null, null, 25);
	}

	public static void verify_QuestionModal_Appears(String questionMessage) {

		BaseUI.waitForElementToBeDisplayed("dataComplete_Question_Modal", null,null, 30);
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Question_Modal"));

		String actualMessage = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Question_Modal_Message"));
		BaseUI.verify_true_AndLog(questionMessage.equals(actualMessage),
				"Modal message matched one of the passing messages.",
				"Modal message of '" + actualMessage + "' was not expected. Expected Message was '" + questionMessage + " '");

	}

	public static void click_Reset() throws Exception {
		BaseUI.waitForElementToBeClickable("dataComplete_ResetButton", null, null);
		BaseUI.click(Locator.lookupElement("dataComplete_ResetButton"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_CorrectJob(String jobName) {
		String elementVariable = "Data Completion(" + jobName + ")";
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Link_ByText", elementVariable, null));

		String jobText = BaseUI.get_NonVisible_TextFromField(Locator.lookupElement("dataComplete_ClientData_JobName"));
		BaseUI.baseStringCompare("Job", jobName, jobText);
		// BaseUI.verifyElementHasExpectedText("dataComplete_ClientData_JobName",
		// jobName);

	}

	public static void verify_JobChanged(String jobName) {
		String dataCompletionText = BaseUI
				.getTextFromField(Locator.lookupElement("navigation_Link_ByText", "Data Completion", null));
		BaseUI.verify_true_AndLog(!dataCompletionText.contains(jobName),
				"Data Completion title did not contain " + jobName, "Data Completion title DID contain " + jobName);

		String jobText = BaseUI.get_NonVisible_TextFromField(Locator.lookupElement("dataComplete_ClientData_JobName"));
		BaseUI.baseStringCompareStringsAreDifferent("Job", jobName, jobText);
	}

	public static void select_RejectReason_Default() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_RejectModal_SelectReason_ByIndex", "1", null));
		// BaseUI.click(Locator.lookupElement("dataComplete_RejectReason_TestReject"));
		Thread.sleep(200);
	}
	

	public static void select_RejectReason(String reasonToSelect) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_RejectModal_SelectReason_ByText", reasonToSelect, null));
		// BaseUI.click(Locator.lookupElement("dataComplete_RejectReason_TestReject"));
		Thread.sleep(200);
	}

	public static void click_reject() throws Exception {
		WebElement rejectButton = BaseUI.waitForElementToBeClickable("dataComplete_Reject_Button", null, null);
		BaseUI.click(rejectButton);
		Thread.sleep(1000);
		// BaseUI.waitForElementToBeDisplayed("dataComplete_Reject_Dialog",
		// null, null);
	}

	public static void click_SelectReason_ForReject() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Reject_SelectReason_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Reject_Dialog", null, null);
		Thread.sleep(1000);
//		Thread.sleep(2000);
//		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_Cancel_Reject() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Reject_Cancel_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Reject_Dialog", null, null, 5);
		Thread.sleep(500);
	}

	public static void reject_Item() throws Exception {
		click_reject();
		BaseUI.waitForElementToBeDisplayed("dataComplete_Reject_Dialog", null, null);
		select_RejectReason_Default();
		click_SelectReason_ForReject();
	}

	public static void reject_Item(String rejectReason) throws Exception {
		click_reject();
		BaseUI.waitForElementToBeDisplayed("dataComplete_Reject_Dialog", null, null);
		select_RejectReason(rejectReason);
		click_SelectReason_ForReject();
	}

	
	
	public static void verify_Reject_Modal_Visible() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Reject_Dialog"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Reject_SelectReason_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_RejectModal_HelpButton"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Reject_Cancel_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_RejectModal_SelectReason_ByIndex", "1", null));

	}

	public static void verify_Reject_Modal_NOT_Visible() {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Reject_Dialog"));
	}

	public static void verify_RejectModal_SelectReasonButton_Enabled_OR_Disabled(Boolean buttonEnabled) {
		if (buttonEnabled) {
			BaseUI.verifyElementHasExpectedAttributeValue(
					Locator.lookupElement("dataComplete_Reject_SelectReason_Button"), "class", "btn btn-primary");
		} else {
			BaseUI.verifyElementHasExpectedAttributeValue(
					Locator.lookupElement("dataComplete_Reject_SelectReason_Button"), "class",
					"btn btn-primary disabled");
		}

	}

	public static void click_Susp() throws Exception {
		WebElement susp_Button = BaseUI.waitForElementToBeClickable("dataComplete_Susp_Button", null, null);
		BaseUI.click(susp_Button);
		BaseUI.waitForElementToBeDisplayed("dataComplete_Susp_OR_ConfirmDialog", null, null);
		Thread.sleep(500);
	}

	// Unsusps the selected row.
	public static void click_UnSusp() throws Exception {
		click_Susp();
		click_Yes_ToConfirm();
	}

	public static void click_Cancel_Susp() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Susp_Cancel_Button"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Susp_Dialog", null, null, 5);
	}

	public static void click_Cancel_Susp_Error() throws Exception {
		// The continue button technically cancels the modal.
		BaseUI.click(Locator.lookupElement("dataComplete_Susp_Error_ContinueButton"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Susp_Dialog_OR_Error", null, null, 5);
	}
	

	public static void click_OK_Susp_Alert() throws Exception {
		// The continue button technically cancels the modal.
		BaseUI.click(Locator.lookupElement("dataComplete_Susp_Alert_OKButton"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Susp_Dialog_OR_Error", null, null, 5);
	}

	public static void verify_Susp_Modal_NOT_Visible() {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_Susp_Dialog"));
	}

	public static void verify_Susp_Modal_Visible() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_Dialog"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_SelectReason_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_Cancel_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_SelectReason_ByIndex", "1", null));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_Header_BankId"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_Header_SuspenseReason"));
	}

	public static void suspend_With_DefaultValue() throws Exception {
		click_Susp();
		select_SuspendReason_ByIndex(1);
		click_Susp_SelectReason();
	}
	
	public static void suspend_BySuspReasonText(String textToSelect) throws Exception {
		click_Susp();
		select_SuspendReason_BySuspReasonText(textToSelect);
		click_Susp_SelectReason();
	}

	public static void select_SuspendReason_BySuspReasonText(String textToSelect) throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Susp_SelectReason_ByReasonText", textToSelect, null));
		Thread.sleep(300);
	}

	public static void click_Susp_SelectReason() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Susp_SelectReason_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_Susp_Dialog_OR_Error", null, null, 5);
		Thread.sleep(300);
		Navigation.wait_For_Page_ToLoad();
	}

	// Index starts at 1.
	public static void select_SuspendReason_ByIndex(Integer index) throws InterruptedException {
		WebElement suspendReason = Locator.lookupElement("dataComplete_Susp_SelectReason_ByIndex", index.toString(),
				null);
		BaseUI.click(suspendReason);
		Thread.sleep(100);
	}

	public static void verify_Susp_Modal_Visible_PYE_MultiplesAlertDialog() {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_AlertDialog"));
		WebElement errorMessage = Locator.lookupElement("dataComplete_Susp_Alert_Message");
		BaseUI.verifyElementAppears(errorMessage);
		BaseUI.verifyElementHasExpectedText(errorMessage,
				"Multiples cannot be suspended for this Doc Group. They must be rejected.");

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Susp_Alert_OKButton"));
	}

	public static void click_DocId() throws Exception {
		WebElement susp_Button = BaseUI.waitForElementToBeClickable("dataComplete_DocId_Button", null, null);
		BaseUI.click(susp_Button);
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("dataComplete_DocId_Modal", null, null);
	}

	public static void click_Cancel_DocId() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_DocId_Cancel_Button"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_DocId_Modal", null, null, 5);
	}

	public static void verify_DocId_Modal_Visible() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_DocId_Modal"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_DocId_ColumnHeader_DocId"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_DocId_ColumnHeader_DocName"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_DocId_SelectDocument_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_DocId_Cancel_Button"));
	}

	public static void click_CPR_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_Button"));
		Thread.sleep(2000);
		BaseUI.waitForElementToBeDisplayed("dataComplete_CPR_Dialog", null, null);
	}

	public static void click_Cancel_CPR() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_Cancel_Button"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_CPR_Dialog", null, null, 5);
	}

	public static void verify_CPR_Modal_Visible() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_Dialog"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_SelectReason_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_Cancel_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_SelectReason_ByIndex", "1", null));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_Header_SortOrder"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_Header_RepairType"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_Header_RepairDescription"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CPR_OperatorNotes_TextArea"));
	}

	public static void enterText_InTo_CPR_OperatorNotes(String valueToEnter) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_CPR_OperatorNotes_TextArea"),valueToEnter);
		Thread.sleep(300);
	}

	public static void modalDialog_Click_Stub_CaptureAsCheck() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_SelectReason_ByIndex", "2", null));
		Navigation.wait_For_Page_ToLoad();

	}

	public static void modalDialog_Click_RepairDescription_piggyBackTransaction() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_SelectReason_ByIndex", "3", null));
		Thread.sleep(3000);

	}

	public static void modalDialog_Click_RepairDescription_CaptureAsWrongClient() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_SelectReason_ByIndex", "4", null));
		Navigation.wait_For_Page_ToLoad();

	}

	public static void modalDialog_Click_SelectReason_Button() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_CPR_SelectReason_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void modalDialog_RepairDescription_SendBatchTo_CPR() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Dialog"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Done_Alert_SendBatchTo_CPR"));
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Alert_OkButton"));
		BaseUI.waitForElementToBeDisplayed("navigation_LoadingSpinner", null, null);
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void dataComplete_VerifyCPR_Doc_ConfirmDialogBoxMsg() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Dialog"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_ConfirmDialogBoxMsg"));

	}

	public static void dataComplete_CPR_Doc_ClickNo_ConfirmDialogBox() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_ConfirmDialogBox_No_Btn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	// public static String return_CPRValue() {
	// String Colum1 =
	// BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CPRvalue"));
	// return Colum1;
	//
	// }

	public static void check_Or_uncheck_OperPref_KeyAhead_Checkbox(Boolean check_Checkbox) {
		if (check_Checkbox) {
			BaseUI.checkCheckbox("dataComplete_OperPref_KeyAhead_Checkbox");
		} else {
			BaseUI.uncheckCheckbox("dataComplete_OperPref_KeyAhead_Checkbox");
		}
	}

	public static void select_KeyAhead_Characters(String charatersToSelect) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_OperPref_KeyAhead_Dropdown"), charatersToSelect);
		Thread.sleep(200);
	}

	public static void check_Or_uncheck_OperPref_AllowDecimalPoint_Checkbox(Boolean check_Checkbox) {
		if (check_Checkbox) {
			BaseUI.checkCheckbox("dataComplete_OperPref_AllowDecimalPoint_Checkbox");
		} else {
			BaseUI.uncheckCheckbox("dataComplete_OperPref_AllowDecimalPoint_Checkbox");
		}
	}

	public static void check_Or_uncheck_OperPref_DefaultZoomed_Checkbox(Boolean check_Checkbox) {
		if (check_Checkbox) {
			BaseUI.checkCheckbox("dataComplete_OperPref_DefaultZoomed_Checkbox");
		} else {
			BaseUI.uncheckCheckbox("dataComplete_OperPref_DefaultZoomed_Checkbox");
		}
	}

	public static void verify_OperPref_KeyAhead_FiveCharacterDropdown_Works(String valueToKey) throws Exception {
		String keyAheadDropdownSelection = "5 characters";
		String previousCheckImage = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("dataComplete_Img1_Image"),"src");
		click_OperPref_Button();
		check_Or_uncheck_OperPref_KeyAhead_Checkbox(true);
		select_KeyAhead_Characters(keyAheadDropdownSelection);
		click_OperPref_OK_Button();
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_Balancing_LastKeyingTextField"), valueToKey);
		String newCheckImage = BaseUI.get_Attribute_FromField(Locator.lookupRequiredElement("dataComplete_Img1_Image"),"src");
		BaseUI.verify_false_AndLog(previousCheckImage.equals(newCheckImage),
				"Image changed from previousCheckImage to newCheckImage",
				"Image did Not change from previousCheckImage to newCheckImage");

	}

	public static void verify_DefaultZoomed_Is_Checked_OR_Unchecked(Boolean checkboxChecked) {
		// String valueToPass = checkboxChecked ? "checked" : "unchecked";
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataComplete_OperPref_DefaultZoomed_Checkbox"),
				checkboxChecked);

	}

	public static void check_Or_uncheck_OperPref_SavePreferences_Checkbox(Boolean check_Checkbox) {
		if (check_Checkbox) {
			BaseUI.checkCheckbox("dataComplete_OperPref_SavePreferences_Checkbox");
		} else {
			BaseUI.uncheckCheckbox("dataComplete_OperPref_SavePreferences_Checkbox");
		}
	}

	public static void verify_SavePreferences_Is_Checked_OR_Unchecked(Boolean checkboxChecked) {
		// String valueToPass = checkboxChecked ? "checked" : "unchecked";

		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataComplete_OperPref_SavePreferences_Checkbox"),
				checkboxChecked);
	}

	public static void click_OperPref_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_OperPref_Button"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_OperPref_Dialog", null, null);
		Thread.sleep(500);
	}

	public static void click_OperPref_OK_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_OperPref_OK_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_OperPref_Dialog", null, null, 5);
		Thread.sleep(300);
	}

	public static void click_Cancel_OperPref() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_OperPref_Cancel_Button"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataComplete_OperPref_Dialog", null, null, 5);
	}

	public static void verify_OperPref_Modal_Visible() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_OperPref_Dialog"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_OperPref_DefaultZoomed_Checkbox"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_OperPref_SavePreferences_Checkbox"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_OperPref_OK_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_OperPref_Cancel_Button"));
	}

	public static String return_ColumUpdatedByRejJob(String seqNumToCheck) throws Exception {
		
		
		String rejJobToget = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Reject_ColumUpdatedBySeqNum",seqNumToCheck, null));

		return rejJobToget;
		
	//BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Reject_ColumUpdatedBySeqNum"),seqNumToCheck);
	//BaseUI.verifyElementAppearsByString(Locator.lookupBy("dataComplete_Reject_ColumUpdatedBySeqNum", seqNumToCheck, null));
	//	BaseUI.elementExists(Locator.lookupElement("dataComplete_Reject_ColumUpdatedBySeqNum", seqNumToCheck, null));
	
	
}
	public static String return_BatchID() {
		String batchID = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"));

		return batchID;
	}
	
	
	public static String return_RejSeqNumByIndex(String indexNum) {
		String secondSeqNum = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Rej_SeqNumByIndex",indexNum, null));

		return secondSeqNum;
	}
	

	public static String return_ClientID() {
		String jobInfo = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_JobInfo"));
		jobInfo = jobInfo.replace("Client Id ", "");
		String clientID = jobInfo.split("\\s")[0];

		return clientID;
	}

	public static String return_DocID() {
		String docID = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DocID"));
		return docID;
	}

	public static void verify_BatchID_Changed(String previousBatch) {
		String newBatchID = return_BatchID();
		BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(newBatchID), "New Batch was Numeric",
				"Batch ID of " + newBatchID + " was supposed to be numeric.");
		BaseUI.verify_false_AndLog(BaseUI.stringEmpty(newBatchID), "Found Batch ID.", "Did NOT find Batch ID.");
		BaseUI.verify_true_AndLog(!newBatchID.equals(previousBatch), "New Batch ID found.",
				"Batch ID did not change from " + previousBatch);

	}

	public static void verify_BatchID_DidNOTChange(String expectedBatch) {
		String newBatchID = return_BatchID();
		BaseUI.baseStringCompare("Batch remained the same: " + expectedBatch, expectedBatch, newBatchID);
	}

	public static void lock_Job() throws Exception {
		click_LockButton();
		click_Yes_ToConfirm();
	}

	public static void click_LockButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_LockButton"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_Done_Confirm_YesButton", null, null);
	}

	public static void click_90Degrees_Clockwise() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Rotate90Degrees_Clockwise_Button"));
		Thread.sleep(300);
	}

	public static void click_90Degress_Counter_Clockwise() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Rotate90Degrees_CounterClockwise_Button"));
		Thread.sleep(300);
	}

	public static void click_180Degress() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Rotate180Degrees_Button"));
		Thread.sleep(300);
	}

	public static void click_FlipImage() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_FlipImage_Button"));
		Thread.sleep(300);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_Zoom() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_ZoomImage_Button"));
		Thread.sleep(1000);
	}

	public static void click_Grayscale() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_GrayImage_Button"));
		Thread.sleep(300);
	}

	// Actions
	//
	//
	//
	public static void zoomIn() throws Exception {
		String zoomButtonClass = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_ZoomImage_Button"),
				"class");
		if (zoomButtonClass.equals("btn wfs-icon-zoomIn")) {
			DataCompletion.click_Zoom();
		}
	}

	public static void zoomOut() throws Exception {
		String zoomButtonClass = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_ZoomImage_Button"),
				"class");
		if (!zoomButtonClass.equals("btn wfs-icon-zoomIn")) {
			DataCompletion.click_Zoom();
		}
	}

	public static void flip_ToBack() throws Exception {

		String flipButtonClass = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_FlipImage_Button"),
				"class");
		if (flipButtonClass.equals("btn wfs-icon-flip")) {
			DataCompletion.click_FlipImage();
		}
	}

	public static void flip_ToFront() throws Exception {

		String flipButtonClass = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_FlipImage_Button"),
				"class");
		if (flipButtonClass.equals("btn toggledButton wfs-icon-flipInv")) {
			DataCompletion.click_FlipImage();
		}
	}

	public static void verify_Flip_Back_And_Front_Works() throws Exception {
		String imageInfoBack;
		String imageInfoFront;

		flip_ToBack();
		imageInfoBack = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"), "src");

		flip_ToFront();
		imageInfoFront = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"), "src");

		BaseUI.verify_false_AndLog(imageInfoFront.equals(imageInfoBack), "Image SRC changed after clicking Flip.",
				"Image SRC did NOT change after clicking Flip.");

	}

	public static void verify_FlipButton_Displayed() {
		WebElement flip = Locator.lookupElement("dataComplete_FlipImage_Button");
		BaseUI.verifyElementAppears(flip);
		Navigation.verify_ButtonEnabled(flip);
	}

	public static void verify_Gray_ButtonEnabled() {
		WebElement greyButton = Locator.lookupElement("dataComplete_GrayImage_Button");
		Navigation.verify_ButtonEnabled(greyButton);
		BaseUI.verifyElementAppears(greyButton);
	}

	public static void verify_Gray_Button_ChangesImage() throws Exception {
		String imageBefore = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"),
				"src");
		String imageAfter;

		click_Grey_Button();
		imageAfter = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"), "src");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BW_Button"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_GrayImage_Button"));
		BaseUI.verify_false_AndLog(imageBefore.equals(imageAfter), "Image SRC changed after clicking Grey.",
				"Image SRC did NOT change after clicking Grey.");

	}

	public static void click_Grey_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_GrayImage_Button"));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("dataComplete_DOC_ImageElement", null, null);
	}

	public static void verify_BW_Button_ChangesImage() throws Exception {
		String imageBefore = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"),
				"src");
		String imageAfter;

		click_BW_Button();
		imageAfter = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"), "src");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_GrayImage_Button"));
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_BW_Button"));
		BaseUI.verify_false_AndLog(imageBefore.equals(imageAfter), "Image SRC changed after clicking Grey.",
				"Image SRC did NOT change after clicking Grey.");

	}

	public static void verify_Grey_Button() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_GrayImage_Button"));

	}

	public static void click_Rope() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Rope_Button"));
		Thread.sleep(300);
	}

	public static void click_Pan() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Pan_Button"));
		Thread.sleep(300);
	}

	public static void verify_Button_Toggled(WebElement button) {
		BaseUI.verifyElementHasExpectedAttributeValue(button, "class", "btn toggledButton");
	}

	public static void verify_Button_NOT_Toggled(WebElement button) {
		BaseUI.verifyElementHasExpectedAttributeValue(button, "class", "btn");
	}

	public static void click_BW_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_BW_Button"));
		Thread.sleep(500);
	}

	public static void verify_Rotate_Clockwise90Degrees_Works() throws Exception {
		Integer previousDegree;
		Integer nextDegree;

		previousDegree = return_ImageDegree();
		DataCompletion.click_90Degrees_Clockwise();
		nextDegree = return_ImageDegree();

		verify_Rotation(90, previousDegree, nextDegree);
	}

	public static void verify_Rotate_CounterClockwise90Degrees_Works() throws Exception {
		Integer previousDegree;
		Integer nextDegree;

		previousDegree = return_ImageDegree();
		DataCompletion.click_90Degress_Counter_Clockwise();
		nextDegree = return_ImageDegree();

		verify_Rotation(-90, previousDegree, nextDegree);
	}

	public static void verify_Rotate_180Degrees_Works() throws Exception {
		Integer previousDegree;
		Integer nextDegree;

		previousDegree = return_ImageDegree();
		DataCompletion.click_180Degress();
		nextDegree = return_ImageDegree();

		verify_Rotation(180, previousDegree, nextDegree);
	}

	public static void verify_Rotation(Integer expectedDegreeChange, Integer previousDegree, Integer currentDegree) {
		Integer maxDegree = 360;
		Integer expectedValue;

		// Degrees go up to 360 (if it's 360 it becomes 0), so wrote logic to
		// handle this.
		if (expectedDegreeChange + previousDegree >= maxDegree) {
			expectedValue = expectedDegreeChange + previousDegree - maxDegree;

		} else if (expectedDegreeChange + previousDegree < 0) {
			expectedValue = maxDegree - Math.abs(expectedDegreeChange + previousDegree);
		} else {
			expectedValue = previousDegree + expectedDegreeChange;
		}

		BaseUI.verify_true_AndLog(expectedValue.equals(currentDegree), "Expected " + expectedValue + " matched.",
				"Expected " + expectedValue + " but seeing " + currentDegree);
	}

	public static void rotate_Image_ToNoRotation() throws Exception {
		Integer maxCount = 0;

		while (true) {

			if (maxCount > 20) {
				break;
			}

			Integer degree = return_ImageDegree();
			if (degree == 0) {
				break;
			} else {
				click_90Degrees_Clockwise();
			}
			maxCount++;
		}

	}

	public static Integer return_ImageDegree() {
		HashMap<String, String> styleInfo = return_Image_Style_Info();
		String transformValue = styleInfo.get("transform");

		Integer degree;
		if (transformValue == null) {
			degree = 0;
		} else {
			degree = Integer
					.parseInt(transformValue.substring(transformValue.indexOf("(") + 1, transformValue.indexOf("d")));
		}

		return degree;
	}

	public static void verify_ZoomIn_And_ZoomOut_Works() throws Exception {
		HashMap<String, String> zoomedIn_StyleMap;
		Integer zoomedWidth;
		Integer zoomedHeight;

		HashMap<String, String> zoomedOut_StyleMap;
		Integer zoomedOutWidth;
		Integer zoomedOutHeight;

		zoomIn();
		zoomedIn_StyleMap = return_Image_Style_Info();
		zoomedWidth = Integer.parseInt(zoomedIn_StyleMap.get("width").replace("px", ""));
		zoomedHeight = Integer.parseInt(zoomedIn_StyleMap.get("height").replace("px", ""));

		zoomOut();
		zoomedOut_StyleMap = return_Image_Style_Info();
		zoomedOutWidth = Integer.parseInt(zoomedOut_StyleMap.get("width").replace("px", ""));
		zoomedOutHeight = Integer.parseInt(zoomedOut_StyleMap.get("height").replace("px", ""));

		BaseUI.verify_true_AndLog(zoomedHeight > zoomedOutHeight,
				"Zoomed In height " + zoomedHeight.toString() + " is Greater than " + zoomedOutHeight.toString(),
				"Zoomed In height " + zoomedHeight.toString() + " was NOT Greater than " + zoomedOutHeight.toString());

		BaseUI.verify_true_AndLog(zoomedWidth > zoomedOutWidth,
				"Zoomed In width " + zoomedWidth.toString() + " is Greater than " + zoomedOutWidth.toString(),
				"Zoomed In width " + zoomedWidth.toString() + " was NOT Greater than " + zoomedOutWidth.toString());
	}

	public static HashMap<String, String> return_Image_Style_Info() {
		HashMap<String, String> styleMap = new HashMap<String, String>();
		String imageStyle = BaseUI.get_Attribute_FromField(Locator.lookupElement("dataComplete_DOC_ImageElement"),
				"style");
		String[] styles = imageStyle.split("\\;");

		for (String style : styles) {
			String[] styleInfo = style.split("\\:");
			styleMap.put(styleInfo[0].trim(), styleInfo[1].trim());
		}

		return styleMap;
	}

	public static void click_NoCD() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_NoCD_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_Cycle_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_SLC_CycleButton"));
		Thread.sleep(500);
	}

	public static void set_Cycle_On_Or_Off(Boolean toggled_On) throws Exception {
		WebElement button = Locator.lookupElement("dataComplete_SLC_CycleButton");
		String button_ClassValue = BaseUI.get_Attribute_FromField(button, "class");

		if (toggled_On && button_ClassValue.equals("btn toggledButton")) {

		} else if (!toggled_On && button_ClassValue.equals("btn toggledButton")) {
			click_Cycle_Button();
		} else if (!toggled_On && !button_ClassValue.equals("btn toggledButton")) {
		} else if (toggled_On && !button_ClassValue.equals("btn toggledButton")) {
			click_Cycle_Button();
		}
	}

	public static TableData return_CycleSection_Table() {
		TableData cycleSectionTable = new TableData();
		HashMap<String, String> cellMappings = new HashMap<String, String>();
		cellMappings.put("Seq", "./div[@role='gridcell'][1]/div");
		cellMappings.put("Doc ID", "./div[@role='gridcell'][2]/div");
		cellMappings.put("Document Type", "./div[@role='gridcell'][3]/div");

		cycleSectionTable.data = BaseUI.tableExtractor("dataComplete_SLC_CycleSection_Rows", cellMappings);

		return cycleSectionTable;
	}

	public static void verify_ErrorMessage(String expectedMessage) {
		WebElement errorMessage = Locator.lookupElement("dataComplete_ErrorMessage");
		BaseUI.verifyElementAppears(errorMessage);
		BaseUI.verifyElementHasExpectedText(errorMessage, expectedMessage);
	}

	public static void verify_Aux_FieldName_ByIndex(int fieldnameIndex, String expectedMessage){
		WebElement fieldName = Locator.lookupElement("dataComplete_AUX_First_FieldName_ByIndex", String.valueOf(fieldnameIndex), null);
		BaseUI.verifyElementAppears(fieldName);
		BaseUI.verifyElementHasExpectedText(fieldName, expectedMessage);

	}

	public static void verify_Aux_Seq_ByIndex(int seqIndex, String expectedMessage){
		WebElement seq = Locator.lookupElement("dataComplete_AUX_First_Seq_ByIndex", String.valueOf(seqIndex), null);
		BaseUI.verifyElementAppears(seq);
		BaseUI.verifyElementHasExpectedText(seq, expectedMessage);

	}

	public static void verify_SL2_ErrorMessage(String expectedMessage) {
		WebElement errorMessage = Locator.lookupRequiredElement("dataComplete_ErrorMsg");
		BaseUI.verifyElementAppears(errorMessage);
		BaseUI.verifyElementHasExpectedText(errorMessage, expectedMessage);
	}

	public static void click_DataCompletion() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Text"));
		Thread.sleep(1000);
	}

}// End of Class
