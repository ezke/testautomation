package imageRPS.pages;

import org.openqa.selenium.WebElement;

import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Locator;

public class Help {

	public static void verify_HelpTopic_Correct(String topic) {
		WebElement helpTopic = Locator.lookupElement("help_Topic");

		BaseUI.verifyElementAppears(helpTopic);
		BaseUI.verifyElementHasExpectedText(helpTopic, topic);
	}

	public static void verify_default_HelpPageLoaded() throws Exception {
		Navigation.verify_Page_Active(TopNavigationTab.Help);
		verify_HelpTopic_Correct("toc");
		BaseUI.verifyElementAppears(Locator.lookupElement("help_Header"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

}// End of Class
