package imageRPS.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.Locator;
import utils.TableData;

public class Navigation {

	public static void wait_ForSessionEnded() throws Exception {
		BaseUI.waitForElementToBeDisplayed("navigation_SessionEnded_Title", null, null, 20);
	}

	public static void signOut() throws InterruptedException {
		BaseUI.click(Locator.lookupElement("navigate_SignOut"));
		Thread.sleep(500);

	}

	public static void navigate_Past_CertificateError() throws Exception {
		if (BaseUI.pageSourceContainsString("Continue to this website (not recommended).")) {
			Browser.driver.get("javascript:document.getElementById('overridelink').click();");
			// BaseUI.click(Locator.lookupElement("ie_ContinueToWebsite"));
			Thread.sleep(3500);
		}
	}

	public static void verify_InternalServiceErrorNotPresent() {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("navigation_InternalServiceError"));

	}

	public static void clickOK_ForErrorModal() throws Exception {
		BaseUI.click(Locator.lookupElement("navigation_InternalServiceError_OKButton"));
		BaseUI.waitForElementToNOTBeDisplayed("navigation_InternalServiceError_OKButton", null, null, 5);
	}

	public static void click_OK_OnError_IfExists() throws Exception {
		if (BaseUI.elementAppears(Locator.lookupElement("navigation_InternalServiceError"))) {
			ReportsOutput_Reformatter.clickOK_ForErrorModal();
		}

	}

	public static void navigate_toTab(TopNavigationTab tab) throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click_js(Locator.lookupElement("navigation_Link_ByText", tab.getValue(), null));
		} else {
			BaseUI.click(Locator.lookupElement("navigation_Link_ByText", tab.getValue(), null));
		}
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
		// Thread.sleep(2000);
		// BaseUI.wait_forPageToFinishLoading();
		// Thread.sleep(500);

		// If the Data Completion tab does not contain the "(", which means
		// nothing is being keyed, then we'll click Reset.
		if (tab.equals(TopNavigationTab.DataCompletion)
				&& !BaseUI.getTextFromField(Locator.lookupElement("navigation_Link_ByText", tab.getValue(), null))
						.contains("(")) {
			DataCompletion.click_Reset();

		}
	}

	public static Boolean isTabActive(TopNavigationTab tabName) {
		Boolean tabActive;
		WebElement tabToCheck = Locator.lookupElement("navigation_ActiveStatus_ByText", tabName.getValue(), null);

		tabActive = BaseUI.get_Attribute_FromField(tabToCheck, "class").equals("active") ? true : false;

		return tabActive;
	}

	public enum TopNavigationTab {
		Dashboard("Dashboard"), DataCompletion("Data Completion"), DataMaintenance("Data Maintenance"), ReportsOutput(
				"Reports/Output"), Utilities("Utilities"), AlertsEvents("Alerts/Events"), Help("Help");

		private String value;

		private TopNavigationTab(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	public static TableData return_ConfigData() throws Exception {
		TableData tabledata = DataBuilder.returnTableData_ForComparison("\\src\\imageRPS\\data\\dbsetup.bkp", "\\s",
				true);

		tabledata.remove_Character("\"");

		return tabledata;
	}

	public static void enter_Text_Into_CommandBox_AndNavigate(String commandToEnter) throws Exception {
		WebElement command_TextBox = Locator.lookupElement("navigation_Command_TextBox");

		BaseUI.enterText_IntoInputBox(command_TextBox, commandToEnter);
		BaseUI.enterKey(command_TextBox, Keys.ENTER);
		Thread.sleep(1500);
		wait_For_Page_ToLoad();
	}

	public static void navigate_to_Batch_ViaCommand(String batchNumber) throws Exception {
		// enter_Text_Into_CommandBox_AndNavigate("batch " + batchNumber);
		String commandToEnter = "batch " + batchNumber;

		WebElement command_TextBox = Locator.lookupElement("navigation_Command_TextBox");

		BaseUI.enterText_IntoInputBox(command_TextBox, commandToEnter);
		BaseUI.enterKey(command_TextBox, Keys.ENTER);
		BaseUI.waitForElementToBeDisplayed("dataMaint_JobListTable_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void verify_UserDisconnected() {
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_SessionEnded_Title"));
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_UserSession_NotValid_Title"));
	}

	public static void wait_For_Page_ToLoad() throws Exception {
		// Thread.sleep(1000);
		BaseUI.waitForElementToContain_PartialAttributeMatch("navigation_LoadingSpinner", null, null, "style",
				"opacity: 0;", 75);
		BaseUI.waitForElementToNOTBeDisplayed("navigation_LoadingSpinner", null, null, 35);
		Thread.sleep(1000);
	}

	public static void wait_For_SpinnerToAppear() throws Exception {
		BaseUI.waitForElementToBeDisplayed_ButDontFail("navigation_LoadingSpinner", null, null, 40);
		// BaseUI.waitForElementToBeDisplayed("navigation_LoadingSpinner", null, null,
		// 40);
	}

	public static Boolean button_IsEnabled(WebElement buttonToCheck) {

		String classvalue = BaseUI.get_Attribute_FromField(buttonToCheck, "class");
		if (classvalue.equals("btn")) {
			return true;
		} else if (classvalue.equals("btn disabled")) {
			return false;
		} else {
			return BaseUI.element_isEnabled(buttonToCheck);
		}

	}

	// Use this for lower bar buttons that have btn disabled as their class when
	// they're disabled.
	public static void verify_ButtonEnabled(WebElement button) {
		BaseUI.verify_true_AndLog(button_IsEnabled(button), "Button was enabled.", "Button was Disabled.");
	}

	public static void verify_ButtonDisabled(WebElement button) {
		BaseUI.verify_true_AndLog(!button_IsEnabled(button), "Button was disabled.", "Button was enabled.");
	}

	// Don't use for Data Completion page when job is being keyed, as that page
	// will have the job type in the title.
	public static void verify_Page_Active(TopNavigationTab navTab) {
		String classValue = BaseUI.get_Attribute_FromField(
				Locator.lookupElement("navigation_ActiveStatus_ByText", navTab.getValue(), null), "class");
		BaseUI.baseStringCompare(navTab.getValue() + " class was active?", "active", classValue);

	}

	public static void verify__toolbarMenu_atTheTopOfThePage(String link) {

		BaseUI.verifyElementExists("toolbarMenu_atTheTopOfThePage", link, null);

	}

	// Use for User permission tests where only the provided Top Navigation link
	// should appear.
	public static void verify_Only_ProvidedHeader_Appears(String navigationLinkWeShouldSee) throws Exception {
		String[] otherHeaderLinks = { "Dashboard", "Data Completion", "Data Maintenance", "Reports/Output", "Utilities",
				"Alerts/Events", "Help" };

		for (String otherHeaderLink : otherHeaderLinks) {
			if (!otherHeaderLink.equals(navigationLinkWeShouldSee)) {
				BaseUI.verifyElementDoesNotAppear(
						Locator.lookupElement("navigation_Link_ByText", otherHeaderLink, null));
			}
		}

		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Link_ByText", navigationLinkWeShouldSee, null));
	}

	public static void verify_ToolBarHeader_Appears(String navigationLinkWeShouldSee) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_Link_ByText", navigationLinkWeShouldSee, null));
	}

	//
	// public static void close_ToastError_If_ItAppears() throws Exception {
	// if (BaseUI.pageSourceContainsString("toast-item toast-type-error")
	// && Locator.lookupElement("error_MessagePopup").isDisplayed()) {
	//
	// BaseUI.click(Locator.lookupElement("error_CloseButton"));
	// Thread.sleep(500);
	// }
	// }

}// end of class
