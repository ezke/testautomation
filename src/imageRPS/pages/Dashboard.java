package imageRPS.pages;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class Dashboard {

    public static WebElement return_BatchBar_ByName(String name) {

        ArrayList<WebElement> listOfBatches = Locator.lookup_multipleElements("dshbrd_ItemKeyingCount_System_List",
                null, null);
        ArrayList<String> listOfBatches_Names = new ArrayList<String>();
        for (WebElement batch : listOfBatches) {
            String batchName = BaseUI.getTextFromField(batch);
            if (batchName.startsWith("C:")) {
                batchName = batchName.replace("C:", "").trim();
            }

            listOfBatches_Names.add(batchName);
        }

        Integer indexOfName = listOfBatches_Names.indexOf(name) + 1;
        WebElement batchBar = Locator.lookupElement("dshbrd_ItemKeyingCount_Batch_ByIndex", indexOfName.toString(),
                itemKeying_BarColor.Green.getValue());

        listOfBatches = null;
        listOfBatches_Names = null;

        return batchBar;
    }

    public static void click_ConsolidationsByCutoff_RefreshButton() throws Exception {
        String injectedText = "Unique Automation Text that will disappear with refresh.";
        BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("dshbrd_ConsolidationsByCutoff_LastUpdated_Label"), injectedText);
        BaseUI.click(Locator.lookupElement("dshbrd_ConsolidationsByCutoff_RefreshButton"));
        BaseUI.waitForElementToNOTContain_PartialAttributeMatch("dshbrd_ConsolidationsByCutoff_LastUpdated_Label", null, null, "innerText", injectedText, 10);
        BaseUI.waitForElementToContain_AttributeMatch("dshbrd_ConsolidationsByCutoff_LastUpdated_Label", null, null, "innerText", "(a few seconds since last update)", 10);
    }

    public static void wait_for_Consolidations_by_CutoffTime_ToBe_1Minute() {
        BaseUI.waitForElementToContain_AttributeMatch("dshbrd_ConsolidationsByCutoff_LastUpdated_Label", null, null,
                "innerText", "(a minute since last update)", 70);
    }

    // Index starts at 1.
    public static String return_BatchName_ForBar_ByIndex(itemKeying_BarColor barColor, itemKeying_Link keyingType,
                                                         Integer barIndex) {

        WebElement batchBar = Locator.lookupElement("dshbrd_ItemKeying_Visible_ByIndex_ByColor", barIndex.toString(),
                barColor.getValue());

        String bar_xCoordinate = BaseUI.get_Attribute_FromField(batchBar, "x");
        Double x_Coordinate_fromBar = Double.parseDouble(bar_xCoordinate);

        // Set this to 10000.0 Should ensure that one of the values pulled is
        // less than it.
        Double x_Coordinate_fromBestMatch = 10000.0;
        String bestMatchingText = "";

        ArrayList<WebElement> listOfBatches = Locator
                .lookup_multipleElements("dshbrd_ItemKeyingCount_System_CoordinateList", null, null);
        try {

            for (WebElement batch : listOfBatches) {

                Double x_Coordinate_forBatch = 0.0;

                String batch_text = BaseUI.getTextFromField(batch.findElement(
                        By.xpath("./*[local-name() = 'g']/*[local-name() = 'text'][@class='jqx-chart-axis-text']")));

                if (batch_text.startsWith("C:")) {
                    batch_text = batch_text.replace("C:", "").trim();
                }

                if (keyingType.equals(itemKeying_Link.Cutoffs)) {
                    batch_text += " " + BaseUI.getTextFromField(batch.findElement(By.xpath(
                            "./*[local-name() = 'g']/*[local-name() = 'text'][@class='jqx-chart-axis-text'][2]")));
                }

                String xCoordinate_str = BaseUI.get_Attribute_FromField(batch, "transform");
                xCoordinate_str = xCoordinate_str.replace("translate(", "");

                // Element looks slightly different in IE.
                String splitCharacter = Browser.currentBrowser.equals("internetexplorer") ? "\\s" : "\\,";

                xCoordinate_str = xCoordinate_str.split(splitCharacter)[0].trim();
                x_Coordinate_forBatch = Double.parseDouble(xCoordinate_str);

                Double newAmount_Difference = Math.abs(x_Coordinate_fromBar - x_Coordinate_forBatch);
                // Coordinate difference that we allow.
                if (newAmount_Difference < x_Coordinate_fromBestMatch) {
                    x_Coordinate_fromBestMatch = newAmount_Difference;
                    bestMatchingText = batch_text;
                }
            }
        } finally {

            batchBar = null;
            listOfBatches = null;
        }

        return bestMatchingText;
    }

    // This method is defunct, found a better way to compare.
    public static Double return_expected_X_Coordinate_Difference(itemKeying_Link itemType) {
        Double coordinateDiff = null;

        if (itemType.equals(itemKeying_Link.Clients)) {
            return itemKeying_Maximized() ? 15.0 : 30.0;
        } else if (itemType.equals(itemKeying_Link.Cutoffs)) {
            return itemKeying_Maximized() ? 105.0 : 35.0;
        }

        return coordinateDiff;
    }

    public static Double return_batch_X_Coordinates(String name) {
        ArrayList<WebElement> listOfBatches = Locator
                .lookup_multipleElements("dshbrd_ItemKeyingCount_System_CoordinateList", null, null);
        Double x_Coordinate = 0.0;
        try {

            for (WebElement batch : listOfBatches) {
                String batchName = BaseUI.getTextFromField(batch.findElement(
                        By.xpath("./*[local-name() = 'g']/*[local-name() = 'text'][@class='jqx-chart-axis-text']")));

                if (batchName.startsWith("C:")) {
                    batchName = batchName.replace("C:", "").trim();
                }

                if (name.equals(batchName)) {
                    String xCoordinate_str = BaseUI.get_Attribute_FromField(batch, "transform");
                    xCoordinate_str = xCoordinate_str.replace("translate(", "");
                    xCoordinate_str = xCoordinate_str.split("\\,")[0].trim();
                    x_Coordinate = Double.parseDouble(xCoordinate_str);
                }
            }
        } finally {
            listOfBatches = null;
        }
        return x_Coordinate;
    }

    public static WebElement return_BatchBar_ByName_UsingCoordinates(String name, itemKeying_BarColor barColor) {

        Double x_Coordinate = return_batch_X_Coordinates(name);

        ArrayList<WebElement> listOfBars = Locator.lookup_multipleElements("dshbrd_ItemKeying_Visible_ByColor",
                barColor.getValue(), null);
        for (WebElement bar : listOfBars) {
            String bar_xCoordinate = BaseUI.get_Attribute_FromField(bar, "x");
            Double bar_xCoordinate_AsDouble = Double.parseDouble(bar_xCoordinate);

            // Coordinate difference that we allow.
            Double coordinateDifference = itemKeying_Maximized() ? 20.0 : 9.0;

            if (Math.abs(x_Coordinate - bar_xCoordinate_AsDouble) < coordinateDifference) {
                return bar;
            }
        }

        return null;
    }

    public static void click_ItemKeyingCount_BatchBar_ByName(String name) throws Exception {
        WebElement batchBarToClick = return_BatchBar_ByName(name);

        if (Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.click_js(batchBarToClick);
        } else {
            BaseUI.click(batchBarToClick);
        }
        Thread.sleep(2000);
        Navigation.wait_For_Page_ToLoad();

        //
        // Thread.sleep(1000);
        // BaseUI.wait_forPageToFinishLoading();
    }

    public static void click_ItemKeyingCount_BatchBar_ByIndex(Integer index, itemKeying_BarColor barColor)
            throws Exception {
        WebElement batchBar = Locator.lookupElement("dshbrd_ItemKeying_Visible_ByIndex_ByColor", index.toString(),
                barColor.getValue());

        // if (Browser.currentBrowser.equals("internetexplorer")) {
        BaseUI.click_js(batchBar);
        // } else {
        // BaseUI.click(batchBar);
        // }
        // Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
        Navigation.wait_For_Page_ToLoad();
        Thread.sleep(200);
    }

    public static void click_ItemKeyingCount_BatchBar_ByName_usingCoordinates(String name, itemKeying_BarColor barColor)
            throws Exception {
        WebElement batchBarToClick = return_BatchBar_ByName_UsingCoordinates(name, barColor);

        BaseUI.click(batchBarToClick);
        Thread.sleep(1000);
        BaseUI.wait_forPageToFinishLoading();
    }

    public static void click_ItemKeying_Link(itemKeying_Link linkText) throws Exception {
        String linkTextValue = linkText.getValue();

        WebElement linkToclick = Locator.lookupElement("dshbrd_ItemKeying_Link_ByText", linkTextValue, null);

        if (button_IsEnabled(linkToclick)) {
            BaseUI.click(linkToclick);
            Thread.sleep(400);
        }

        String linkText_AsSingular = linkTextValue.substring(linkTextValue.length() - 1, linkTextValue.length())
                .equals("s") ? linkTextValue.substring(0, linkTextValue.length() - 1) : linkText.getValue();

        linkText_AsSingular = linkText_AsSingular.equals("Date") ? "Processing Date" : linkText_AsSingular;

        if (!linkTextValue.equals("Refresh") && !linkTextValue.equals("Maximize")) {
            Thread.sleep(200);
            BaseUI.waitForElementToContain_PartialAttributeMatch("dshbrd_ItemKeyingCount_ByLabel", null, null,
                    "textContent", "by " + linkText_AsSingular);
        } else {
            Thread.sleep(1000);
        }
    }

    public static Boolean button_IsEnabled(WebElement linkToCheck) {
        Boolean buttonEnabled = !BaseUI.get_Attribute_FromField(linkToCheck, "class").equals("btn disabled") ? true
                : false;

        return buttonEnabled;
    }

    public static Boolean itemKeying_Maximized() {
        String maximize_Class = BaseUI.get_Attribute_FromField(
                Locator.lookupElement("dshbrd_ItemKeying_Link_ByText", itemKeying_Link.Maximize.getValue(), null),
                "class");

        return maximize_Class.equals("btn toggledButton") ? true : false;
    }

    public static void maximize_ItemKeying() throws Exception {
        if (!itemKeying_Maximized()) {
            click_ItemKeying_Link(itemKeying_Link.Maximize);
        }
    }

    public static void un_maximize_ItemKeying() throws Exception {
        if (itemKeying_Maximized()) {
            click_ItemKeying_Link(itemKeying_Link.Maximize);
        }
    }

    public enum itemKeying_Link {
        Jobs("Jobs"), Clients("Clients"), Dates("Dates"), Cutoffs("Cutoffs"), Locations("Locations"), Refresh(
                "Refresh"), Maximize("Maximize");

        private String value;

        private itemKeying_Link(final String val) {
            value = val;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return getValue();
        }
    }

    public enum itemKeying_BarColor {
        // Green is Batches, Items in System in blue, Items to Key is red.
        Green("#7B9447"), Red("#993F3D"), Blue("#2770C6");

        private String value;

        private itemKeying_BarColor(final String val) {
            value = val;
        }

        public String getValue() {
            return value = Browser.currentBrowser.equals("internetexplorer") ? value.toLowerCase() : value;
        }

        @Override
        public String toString() {
            return getValue();
        }
    }

    public static void verify_ItemKeying_Clients_BatchNavigation(itemKeying_BarColor barColor, Integer barIndexToClick)
            throws Exception {
        Dashboard.click_ItemKeying_Link(itemKeying_Link.Clients);
        Dashboard.maximize_ItemKeying();

        String barText = Dashboard.return_BatchName_ForBar_ByIndex(barColor, itemKeying_Link.Clients, barIndexToClick);
        Dashboard.click_ItemKeyingCount_BatchBar_ByIndex(barIndexToClick, barColor);

        String clientID = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_clientID"));

        BaseUI.verify_true_AndLog(clientID.startsWith(barText),
                "Client ID " + clientID + "started with value " + barText,
                "Was expecting Client ID " + clientID + "to start with value " + barText);
    }

    public static void verify_ItemKeying_Cutoffs_BatchNavigation(itemKeying_BarColor barColor, Integer barIndexToClick)
            throws Exception {
        Dashboard.click_ItemKeying_Link(itemKeying_Link.Cutoffs);
        Dashboard.maximize_ItemKeying();

        String barText = Dashboard.return_BatchName_ForBar_ByIndex(barColor, itemKeying_Link.Cutoffs, barIndexToClick);
        String dateRange = return_ItemKeying_CutoffsDateRange(barText);
        String priorDate = dateRange.split("\\-")[0];
        priorDate = BaseUI.return_Date_AsDifferentFormat(priorDate.toUpperCase(), "hh:mma MM/dd/yyyy",
                "MM/dd/yyyy hh:mm:ss a");
        String nextDate = dateRange.split("\\-")[1];
        nextDate = BaseUI.return_Date_AsDifferentFormat(nextDate, "hh:mma MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss a");

        Dashboard.click_ItemKeyingCount_BatchBar_ByIndex(barIndexToClick, barColor);

        String cutoffDate = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_cutoffDate"));
        cutoffDate = cutoffDate.split("\\(")[0].trim();
        cutoffDate = BaseUI.return_Date_AsDifferentFormat(cutoffDate, "EEE, MMM d, yyyy h:mm a",
                "MM/dd/yyyy hh:mm:ss a");

        BaseUI.verify_Date_IsBetween_DateRange(priorDate, nextDate, cutoffDate, "MM/dd/yyyy hh:mm:ss a");
    }

    public static void verify_ItemKeying_Dates_BatchNavigation(itemKeying_BarColor barColor, Integer barIndexToClick)
            throws Exception {
        click_ItemKeying_Link(itemKeying_Link.Dates);
        maximize_ItemKeying();

        String barText = return_BatchName_ForBar_ByIndex(barColor, itemKeying_Link.Dates, barIndexToClick);
        String dateRange = return_ItemKeying_DateRange(barText);
        String priorDate = dateRange.split("\\-")[0];
        priorDate = BaseUI.return_Date_AsDifferentFormat(priorDate, "MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss a");
        String nextDate = dateRange.split("\\-")[1];
        nextDate = BaseUI.return_Date_AsDifferentFormat(nextDate, "MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss a");

        Dashboard.click_ItemKeyingCount_BatchBar_ByIndex(barIndexToClick, barColor);

        String batchProcessDate = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchProcessDate"));
        batchProcessDate = batchProcessDate.split("\\(")[0].trim();
        batchProcessDate = BaseUI.return_Date_AsDifferentFormat(batchProcessDate, "EEE, MMM d, yyyy h:mm a",
                "MM/dd/yyyy hh:mm:ss a");

        BaseUI.verify_Date_IsBetween_DateRange(priorDate, nextDate, batchProcessDate, "MM/dd/yyyy hh:mm:ss a");
    }

    // date must be in format MM/dd/yyyy
    public static String return_ItemKeying_DateRange(String date) {
        ArrayList<WebElement> dateList = Locator.lookup_multipleElements("dshbrd_ItemKeyingCount_System_List", null,
                null);
        ArrayList<String> dateList_Text = new ArrayList<String>();

        for (WebElement dateElement : dateList) {
            dateList_Text.add(BaseUI.getTextFromField(dateElement));
        }

        Integer indexOfDate = dateList_Text.indexOf(date);

        String previousDate = indexOfDate == 0 ? date : dateList_Text.get(indexOfDate - 1);
        String nextDate = indexOfDate == dateList_Text.size() ? date : dateList_Text.get(indexOfDate + 1);

        return previousDate + "-" + nextDate;
    }

    // date must be in format MM/dd/yyyy
    public static String return_ItemKeying_CutoffsDateRange(String date) {
        ArrayList<WebElement> dateList = Locator.lookup_multipleElements("dshbrd_ItemKeyingCount_System_List", null,
                null);
        ArrayList<String> dateList_Text = new ArrayList<String>();

        for (Integer i = 0; i < dateList.size(); i++) {
            String newDate = BaseUI.getTextFromField(dateList.get(i));
            i++;
            newDate = newDate + " " + BaseUI.getTextFromField(dateList.get(i));
            dateList_Text.add(newDate);
        }

        Integer indexOfDate = dateList_Text.indexOf(date);

        String previousDate = indexOfDate == 0 ? date : dateList_Text.get(indexOfDate - 1);
        String nextDate = indexOfDate == dateList_Text.size() ? date : dateList_Text.get(indexOfDate + 1);

        return previousDate + "-" + nextDate;
    }

    public static void verify_ItemsKeying_TabMaximized(itemKeying_Link tabToCheck) throws Exception {
        click_ItemKeying_Link(tabToCheck);
        click_ItemKeying_Link(itemKeying_Link.Maximize);
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dshbrd_AlertsChart"));
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dshbrd_ConsolidationsChart"));
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dshbrd_Preferences"));
    }

    public static void verify_ItemsKeying_Tab_UnMaximized() throws Exception {
        un_maximize_ItemKeying();
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_AlertsChart"));
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_ConsolidationsChart"));
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_Preferences"));
    }

    // Hover not working, no way to use this method unless we can come up with a
    // solution.
    public static void hoverOver_BatchBar_ByName(String name) throws Exception {
        WebElement batchBar = return_BatchBar_ByName(name);

        // BaseUI.elementHover(batchBar);
        // Mouse mouse = ((HasInputDevices) Browser.driver).getMouse();
        // mouse.mouseMove(((Locatable)
        // Locator.lookupElement("Navigation_Link_ByText", "Data Completion",
        // null)).getCoordinates());
        // mouse.mouseMove(((Locatable) batchBar).getCoordinates());

        // BaseUI.elementHover(Locator.lookupElement("Navigation_Link_ByText",
        // "Data Completion", null));
        // BaseUI.elementAppears(batchBar);

        BaseUI.element_ClickAndHold(batchBar);
    }

    // Hover not working, no way to use this method unless we can come up with a
    // solution.
    public static HashMap<String, String> return_ToolTip_Text() {
        HashMap<String, String> toolTip_Mapping = new HashMap<String, String>();

        ArrayList<WebElement> tooltip_Rows = Locator.lookup_multipleElements("dshbrd_ToolTip_Rows", null, null);

        for (WebElement tooltipRow : tooltip_Rows) {
            String rowTitle = BaseUI.getTextFromField(tooltipRow.findElement(By.xpath("./td[1]")));
            String rowValue = BaseUI.getTextFromField(tooltipRow.findElement(By.xpath("./td[2]")));

            toolTip_Mapping.put(rowTitle, rowValue);
        }

        return toolTip_Mapping;
    }

    public static ArrayList<String> return_preferencesList() {
        ArrayList<String> preferences_List = new ArrayList<String>();
        ArrayList<WebElement> preferencesElements = Locator.lookup_multipleElements("dshbrd_prefs_Table", null, null);

        for (WebElement preferencesElements1 : preferencesElements) {
            preferences_List.add(BaseUI.get_Attribute_FromField(preferencesElements1, "innerText"));
        }

        return preferences_List;
    }

    public static void verify_Dashboard_Loaded() throws Exception {
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_AlertsChart_ChartPart"));
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_ConsolidationsChart_ChartPart"));
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_ItemKeyingCountChart_ChartPart"));
        Navigation.verify_Page_Active(TopNavigationTab.Dashboard);
        Navigation.verify_InternalServiceErrorNotPresent();
    }

    public static void verifytable(String ItemKeyingCountGraph, String AlertCountraph,
                                   String ConsolidationsbyCutoffTimeGraph) {

        ArrayList<String> preferencesList = return_preferencesList();
        System.out.println(preferencesList.size());
        for (int i = 0; i < preferencesList.size(); i++) {

            BaseUI.verify_true_AndLog(
                    preferencesList.get(i).contains(ItemKeyingCountGraph)
                            | (preferencesList.get(i).contains(AlertCountraph)
                            | (preferencesList.get(i).contains(ConsolidationsbyCutoffTimeGraph))),
                    "preferencesList match" + preferencesList.get(i),
                    "preferencesList Not match" + preferencesList.get(i));
        }
    }

    public static void verify_Alert_Bars_Empty_By_BarLabelText(String barLabelText) throws Exception {
        verify_Alert_Bar_Empty_By_BarLabelText_AndColor(barLabelText, "RED");
        verify_Alert_Bar_Empty_By_BarLabelText_AndColor(barLabelText, "YELLOW");
    }

    public static void verify_Alert_Bar_Empty_By_BarLabelText_AndColor(String barLabelText, String color)
            throws Exception {
        WebElement barToCheck = return_Alert_BarElement_ByLabelText_AndColor(barLabelText, color);
        String barWidth = BaseUI.get_Attribute_FromField(barToCheck, "width");
        BaseUI.baseStringCompare("Bar Width", "0.5", barWidth);
    }

    public static void click_AlertGraphBar_ByLabelText_AndColor(String labelText, String color) throws Exception {
        WebElement barToClick = return_Alert_BarElement_ByLabelText_AndColor(labelText, "RED");
        if (Browser.currentBrowser.equals("internetexplorer")) {
            BaseUI.click_js(barToClick);
        } else {
            BaseUI.click(barToClick);
        }
        BaseUI.wait_forPageToFinishLoading();
        Navigation.wait_For_Page_ToLoad();
        BaseUI.waitForElementToBeDisplayed("alerts_AlertListTable_TableCells", null, null, 20);
        Thread.sleep(500);
    }

    public static void refresh_And_Wait_ForREDAlertGraph_ValueToBeGreaterThan0(String labelText) throws Exception {
        BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
            public Boolean call() throws Exception {
                click_AlertGraph_RefreshButton();
                WebElement barGraph = return_Alert_BarElement_ByLabelText_AndColor(labelText, "RED");
                String alertWidth = BaseUI.get_Attribute_FromField(barGraph, "width");

                return Double.parseDouble(alertWidth) > 0.5;
            }
        }, Duration.ofSeconds(120), Duration.ofMillis(1000));
    }

    public static void refresh_And_Wait_ForREDAlertGraph_ToBeEmpty(String labelText) {
        BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
            public Boolean call() throws Exception {
                click_AlertGraph_RefreshButton();
                WebElement barGraph = return_Alert_BarElement_ByLabelText_AndColor(labelText, "RED");
                String alertWidth = BaseUI.get_Attribute_FromField(barGraph, "width");

                return Double.parseDouble(alertWidth) == 0.5;
            }
        }, 70, 1000);
    }

    public static void verify_AlertGraph_AlertGreaterThan0(String labelText, String color) throws Exception {
        WebElement barToCheckElement = return_Alert_BarElement_ByLabelText_AndColor(labelText, color);
        Double width = Double.parseDouble(BaseUI.get_Attribute_FromField(barToCheckElement, "width"));
        BaseUI.verify_true_AndLog(width > 0.5, "Width was greater than 0.5",
                "Width did not indicate there was an alert for color " + color);
    }

    public static void click_AlertGraph_RefreshButton() throws Exception {
        String injectedText = "Unique Automation Text that will disappear with refresh.";
        BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("dshbrd_Alerts_ChartDivElement"), injectedText);
        BaseUI.click(Locator.lookupElement("dshbrd_Alerts_RefreshButton"));

        BaseUI.waitForElementToNOTContain_PartialAttributeMatch("dshbrd_Alerts_ChartDivElement", null, null, "innerText", injectedText, 10);
        BaseUI.waitForElementToContain_AttributeMatch("dshbrd_Alerts_LastUpdateLabel", null, null, "innerText", "(a few seconds since last update)", 10);
    }

    // Don't include new line characters in labelText.
    // Color should be "RED" or "YELLOW"
    public static WebElement return_Alert_BarElement_ByLabelText_AndColor(String labelText, String color)
            throws InterruptedException {
        String barElementLocatorToPick_ByIndex = "";
        if (color.equals("RED")) {
            barElementLocatorToPick_ByIndex = "dshbrd_AlertsChart_RedBar_ByIndex";
        } else if (color.equals("YELLOW")) {
            barElementLocatorToPick_ByIndex = "dshbrd_AlertsChart_YellowBar_ByIndex";
        } else {
            BaseUI.log_AndFail("Color passed in is not recognized.");
        }

        ArrayList<WebElement> listOfLabelElements = Locator
                .lookup_multipleElements("dshbrd_AlertsChart_ListOF_ChartBarLabels", null, null);
        for (Integer i = 0; i < listOfLabelElements.size(); i++) {

            String elementText = BaseUI.getTextFromField(listOfLabelElements.get(i));
            if (elementText.contains("\n")) {
                String[] elementLines = elementText.split("\\n");
                elementText = elementLines[1].trim() + " " + elementLines[0].trim();
            }
            if (elementText.equals(labelText)) {
                // Indexing for xpaths starts at 1.
                i += 1;
                return Locator.lookupElement(barElementLocatorToPick_ByIndex, i.toString(), null);
            }
        }
        return null;
    }

    public WebElement return_RED_Alert_Bar_ByIndex(Integer index) {
        return Locator.lookupElement("dshbrd_AlertsChart_RedBar_ByIndex", index.toString(), null);
    }

    public static void verify_DashboardWidget_arePresnt() {
        BaseUI.verifyElementExists("dshbrd_ConsolidationsChart", null, null);
        BaseUI.verifyElementExists("dshbrd_ItemKeyingCountChart", null, null);
        BaseUI.verifyElementExists("dshbrd_AlertsChart", null, null);
    }

    public static void toggle_ConsolidationsByCutoff_List(Boolean turnListOn) throws Exception {
        WebElement listButton = Locator.lookupElement("dshbrd_ConsolidationsByCutoff_ListButton");
        String listClass = BaseUI.get_Attribute_FromField(listButton, "class");

        String expectedClass = turnListOn ? "btn toggledButton" : "btn";

        if (!listClass.equals(expectedClass)) {
            BaseUI.click(listButton);
            BaseUI.waitForElementToBeDisplayed("dshbrd_ConsolidationsByCutoff_TimeCell_ByRowIndex", "1", null);
            Thread.sleep(500);
        }
    }

    public static void verify_ConsolidationsByCutoffTime_ListButton_ToggledOn() {
        BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupElement("dshbrd_ConsolidationsByCutoff_ListButton"),
                "class", "btn toggledButton");
    }

    public static TableData return_ConsolidationsByCutoffTime_List_TableData() {
        String[] headers = {"Time", "Completed Items", "Completed Amount", "Open Items", "Open Amount"};

        TableData cutoffTable = BaseUI.tableExtractor_ByCell("dshbrd_ConsolidationsByCutoff_TableCells", headers);
        cutoffTable.trim_column("Time");
        cutoffTable.remove_Character("$");
        cutoffTable.remove_Character(",");

        return cutoffTable;
    }

    public static void doubleclick_ConsolidationsByCutoffTime_Row_By_MatchingTime(TableData rowData,
                                                                                  String timeStringToMatch) throws Exception {
        Integer indexToPick = rowData.first_IndexOf("Time", timeStringToMatch) + 1;

        WebElement cellToClick = Locator.lookupElement("dshbrd_ConsolidationsByCutoff_TimeCell_ByRowIndex",
                indexToPick.toString(), null);
        BaseUI.double_click(cellToClick);
        BaseUI.wait_forPageToFinishLoading();
        Navigation.wait_For_Page_ToLoad();
    }

    public static void select_Preferences_Dropdown(String ratio) throws Exception {
        BaseUI.selectValueFromDropDown((Locator.lookupElement("dshbrd_Preferences_Width_Dropdown")), ratio);
        Thread.sleep(1000);
    }

    public static void signout_Relogin_Second_User() throws Exception {
        click_SignOut();
        click_ReturnToApplicationLink();
        LoginPage.login(GlobalVariables.entity, GlobalVariables.SecondLoginName, GlobalVariables.SecondLoginPassword);
    }

    public static void signout_Relogin_Primary_User() throws Exception {
        click_SignOut();
        BaseUI.waitForElementToBeDisplayed("returnToApplicationLink", null, null, 10);
        click_ReturnToApplicationLink();
        LoginPage.login(GlobalVariables.entity, GlobalVariables.loginName, GlobalVariables.loginPassword);
    }

    public static void click_Preferences_Save() throws Exception {
        BaseUI.click(Locator.lookupElement("dshbrd_Preferences_SaveButton"));
        Thread.sleep(1000);
    }

    public static void click_SignOut() throws Exception {
        BaseUI.click(Locator.lookupElement("navigate_SignOut"));
        BaseUI.waitForElementToBeDisplayed("returnToApplicationLink", null, null, 10);
    }

    public static void verify_Half_Chart_Width() {
        String dshbrd_ConsolidationsChart_ChartPart = String.valueOf(Locator.lookupElement("dshbrd_ConsolidationsChart_ChartPart").getSize().getWidth());
        String dshbrd_AlertsChart_ChartPart = String.valueOf(Locator.lookupElement("dshbrd_AlertsChart_ChartPart").getSize().getWidth());
        String dshbrd_ItemKeyingCountChart_ChartPart = String.valueOf(Locator.lookupElement("dshbrd_ItemKeyingCountChart_ChartPart").getSize().getWidth());
        BaseUI.baseStringCompare("Consolidation Chart", "879", dshbrd_ConsolidationsChart_ChartPart);
        BaseUI.baseStringCompare("Alerts Chart", "879", dshbrd_AlertsChart_ChartPart);
        BaseUI.baseStringCompare("Item Keying Chart", "879", dshbrd_ItemKeyingCountChart_ChartPart);
    }

    public static void verify_Full_Chart_Width() {

        int expectedSize = 1800;
        int dshbrd_ConsolidationsChart_ChartPart = Locator.lookupElement("dshbrd_ConsolidationsChart_ChartPart").getSize().getWidth();
        int dshbrd_AlertsChart_ChartPart = Locator.lookupElement("dshbrd_AlertsChart_ChartPart").getSize().getWidth();
        int dshbrd_ItemKeyingCountChart_ChartPart = Locator.lookupElement("dshbrd_ItemKeyingCountChart_ChartPart").getSize().getWidth();

        BaseUI.verify_true_AndLog( (expectedSize - 30) <= dshbrd_ConsolidationsChart_ChartPart && expectedSize + 30 >= dshbrd_ConsolidationsChart_ChartPart,
                "Consolidation Chart fell within acceptable range for widgth.",
                "Consolidation Chart width of " + String.valueOf(dshbrd_ConsolidationsChart_ChartPart) + " did not fall within acceptable width range.");


        BaseUI.verify_true_AndLog( (expectedSize - 30) <= dshbrd_AlertsChart_ChartPart && expectedSize + 30 >= dshbrd_AlertsChart_ChartPart,
                "Alerts Chart fell within acceptable range for widgth.",
                "Alerts Chart width of " + String.valueOf(dshbrd_AlertsChart_ChartPart) + " did not fall within acceptable width range.");


        BaseUI.verify_true_AndLog( (expectedSize - 30) <= dshbrd_ItemKeyingCountChart_ChartPart && expectedSize + 30 >= dshbrd_ItemKeyingCountChart_ChartPart,
                "Item Keying Count Chart fell within acceptable range for widgth.",
                "Item Keying Count Chart width of " + String.valueOf(dshbrd_ItemKeyingCountChart_ChartPart) + " did not fall within acceptable width range.");
    }

    public static void verify_Third_Chart_Width() {
        String dshbrd_ConsolidationsChart_ChartPart = String.valueOf(Locator.lookupElement("dshbrd_ConsolidationsChart_ChartPart").getSize().getWidth());
        String dshbrd_AlertsChart_ChartPart = String.valueOf(Locator.lookupElement("dshbrd_AlertsChart_ChartPart").getSize().getWidth());
        String dshbrd_ItemKeyingCountChart_ChartPart = String.valueOf(Locator.lookupElement("dshbrd_ItemKeyingCountChart_ChartPart").getSize().getWidth());
        BaseUI.baseStringCompare("Consolidation Chart", "571", dshbrd_ConsolidationsChart_ChartPart);
        BaseUI.baseStringCompare("Alerts Chart", "571", dshbrd_AlertsChart_ChartPart);
        BaseUI.baseStringCompare("Item Keying Chart", "571", dshbrd_ItemKeyingCountChart_ChartPart);
    }

    public static void unselect_All_Dashboard_Graphs() throws Exception {
        BaseUI.uncheckCheckbox("dshbrd_Preferences_AlertCountGraph_Checkbox");
        BaseUI.uncheckCheckbox("dshbrd_Preferences_ItemKeyingCountGraph_Checkbox");
        BaseUI.uncheckCheckbox("dshbrd_Preferences_ConsolidationsByCutoffTimeGraph_Checkbox");
        Thread.sleep(1000);
    }

    public static void select_All_Dashboard_Graphs() throws Exception {
        BaseUI.checkCheckbox("dshbrd_Preferences_AlertCountGraph_Checkbox");
        BaseUI.checkCheckbox("dshbrd_Preferences_ItemKeyingCountGraph_Checkbox");
        BaseUI.checkCheckbox("dshbrd_Preferences_ConsolidationsByCutoffTimeGraph_Checkbox");
        Thread.sleep(1000);
    }

    public static void verify_All_Dashboard_Charts_Display() {
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_ConsolidationsChart_ChartPart"));
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_AlertsChart_ChartPart"));
        BaseUI.verifyElementAppears(Locator.lookupElement("dshbrd_ItemKeyingCountChart_ChartPart"));
    }

    public static void verify_All_Dashboard_Charts_DoNot_Display() {
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dshbrd_ConsolidationsChart_ChartPart"));
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dshbrd_AlertsChart_ChartPart"));
        BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dshbrd_ItemKeyingCountChart_ChartPart"));
    }

    public static void click_ReturnToApplicationLink() {
        BaseUI.click(Locator.lookupElement("returnToApplicationLink"));
        WebElement entityTextBox = Locator.lookupElement("loginPage_txt_EntityName");
        BaseUI.waitForElementToBeDisplayed("loginPage_txt_EntityName", null, null, 10);

    }
}// End of Class

