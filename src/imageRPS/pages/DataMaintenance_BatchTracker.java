package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class DataMaintenance_BatchTracker {

	public static void verify_DataMaint_BatchTracker_Loaded() throws Exception {
		DataMaintenance.verify_SubTab_Active("Batch Tracker");
		//Activity defaults to Process Batch.
		verify_BatchTracker_ActivitySelected("Process Batch");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_BatchTracker_ProcessBatch_Table"));
	}

	public static void verify_BatchTracker_ActivitySelected(String activity) {
		String activeStatus = BaseUI.get_Attribute_FromField(
				Locator.lookupElement("dataMaint_BatchTracker_SubTab_liElement_ByText", activity, null), "class");

		Boolean activityActive = null;
		if (activeStatus != null && activeStatus.equals("active")) {
			activityActive = true;
		} else {
			activityActive = false;
		}
		
		BaseUI.verify_true_AndLog(activityActive, "Activity " + activity + " was active.",  "Activity " + activity + " was  NOT active.");
		
		Navigation.verify_InternalServiceErrorNotPresent();
	}

}// End of Class
