package imageRPS.pages;

import java.util.HashMap;
import utils.BaseUI;
import utils.Locator;

public class DataCompletion_CPR {

	public static void verify_JobItem(String clientID, String batch, String date, String jobId, String docGrp)
			throws Exception {

		String expectecClientID = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientId_Present"));
		BaseUI.baseStringPartialCompare("dataComplete_ClientId_Present", clientID, expectecClientID);
		String expectecBatchId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchId2ndTear_Present"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchId2ndTear_Present", batch, expectecBatchId);
		String expectecDate = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Date_Present"));
		BaseUI.baseStringPartialCompare("dataComplete_Date_Present", date, expectecDate);
		String expectecJobId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Job_Present"));
		BaseUI.baseStringPartialCompare("dataComplete_Job_Present", jobId, expectecJobId);
		String expectecDocGrpId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DocGrp_Present"));
		BaseUI.baseStringPartialCompare("dataComplete_DocGrp_Present", docGrp, expectecDocGrpId);

	}

	// new meth...
	public static void click_NewtBatch_DialogBox() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_Click_NewBatchImg"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_EditBatch_IsPresent() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_EditBatchPresent"));

		// String bgColor = "0, 0, 255";
		// BaseUI.get_Attribute_FromField(fieldToGet, attribute)
		// BaseUI.verifyElementHasExpected_CSSAttribute(elementToVerify,
		// cssAttribute, bgColor);

	}

	public static void click_EditBatch_DialogBox() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_Click_EditBatchImg"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_EditBatch_DialogBox() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_EditBatchDialogBox_IsPresent"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_EditBatchDialogBox_Img_IsPresent"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_EditBatchDialogBox_OK_Btn"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_EditBatchDialogBox_Cancel_Btn"));

	}

	public static void verify_NewBatch_DialogBoxTextMsg() throws Exception {

		String textMsg = "Are you sure you want to create a new Batch?";
		String expectedMsg = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_NewBatchDialogBoxMsg"));
		BaseUI.baseStringCompare("dataComplete_NewBatchDialogBoxMsg", textMsg, expectedMsg);

	}

	public static void verify_NewBatch_DialogBox() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_NewBatchDialogBox_IsPresent"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_NewBatchDialogBox_CancelBtn"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_NewBatchDialogBox_ContinuelBtn"));

	}

	public static void click_GhostStub_DialogBox() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_ghoststubBtnImg"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_NewBatch_DialogBox() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_NewBatchDialogBox_ContinuelBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_CheckIn_UpdateBatch() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_Click_CheckInImg"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CheckInDialogBox_IsPresent"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CheckInDialogBox_TextMsg"));

	}

	public static void verify_CheckIn_CloseDialog() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_CheckInDialogBox_ContinueBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CheckInDialogBox_IsPresent"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_CheckInDialog2ndBox_TextMsg"));

	}

	public static void clickOk_CheckIn_CloseDialog() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_EditBatchDialogBox_OK_Btn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void dataComplete_DocId_DialogBox_OkBtn() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_DocId_DialogBox_OkBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_DataComplete_Reject_DialogBox_OkBtn() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_Reject_OK_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_BatchTcket_IsSelected() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_BatchTicket_ClickCheckbox"));
		Thread.sleep(3000);
	}

	public static void select_RejectItem_IsSelected() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_Reject_RejectSelectedItemTran"));
		Thread.sleep(3000);
	}

	public static void newBatchDialogBox_UpdateBatch(String newBatchId, String newClientId, String newDocGrp,
			String dialogBoxDocWorkType) throws Exception {

		BaseUI.enterText(Locator.lookupElement("dataComplete_NewBatchDialogBox_BatchId"), newBatchId);
		// BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_NewBatchDialogBox_BatchId"),
		// valueToEnter);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_NewBatchDialogBox_ClientId"), newClientId);
		Thread.sleep(2000);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_NewBatchDialogBox_DocGrpId"), newDocGrp);
		Thread.sleep(2000);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_NewBatchDialogBox_WorkType"),
				dialogBoxDocWorkType);
		Thread.sleep(2000);
		BaseUI.click(Locator.lookupElement("dataComplete_EditBatchDialogBox_OK_Btn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void editBatchDialogBox_UpdateBatch(String newClientId, String newDocGrp) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_EditBatchDialogBox_ClientId"), newClientId);
		Thread.sleep(2000);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_EditBatchDialogBox_DocGrpId"), newDocGrp);
		Thread.sleep(2000);
		BaseUI.click(Locator.lookupElement("dataComplete_EditBatchDialogBox_OK_Btn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void verify_UpdateBatch(String newClientid1, String newDocGrp1) throws Exception {

		String expectecNewClientID = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientId_Present"));
		// String newClientid1 = null;
		BaseUI.baseStringPartialCompare("dataComplete_ClientId_Present", newClientid1, expectecNewClientID);
		Thread.sleep(1000);
		String expectecNewDocGrpId = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DocGrp_Present"));
		// String newDocGrp1 = null;
		BaseUI.baseStringPartialCompare("dataComplete_DocGrp_Present", newDocGrp1, expectecNewDocGrpId);
		Thread.sleep(1000);
	}

	public static void verify_CheckIn_AlertDialog() throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_AlertDialogBox_IsPresent"));
		// BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_AlertDialog2ndBox_TextMsg"));

	}

	public static void clickOk_CheckIn_AlertDialog() throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_EditBatchDialogBox_OK_Btn"));
		Thread.sleep(5000);

	}

	public static void click_dataComplete_UnknownChk4() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_UnknownChk4"));
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_dataComplete_SplitBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Click_SplitImg"));
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_DataComplete_SuspenseBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_SuspenseImg"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_SuspenseBatch_Modal", null, null);
	}

	public static void updateSuspenseBatch(String newClientId, String newDocGrp) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_SuspenseBatch_ClientId"), newClientId);
		Thread.sleep(300);
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_SuspenseBatch_DocGrpId"), newDocGrp);
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("dataComplete_SuspenseBatch_OK_Btn"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_BatchTicket_Text", null, null, 35);
	}

	public static void verify_SuspenseBatch(String batchTicketText, String batchStubText, String batchChkText) throws Exception {
		String actualBatchTicketText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchTicket_Text"));
		String actualBatchStubText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchStub_Text"));
		String actualBatchChkText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_BatchChk_Text"));
		BaseUI.baseStringPartialCompare("dataComplete_NewBatchTicket_TextPresent", batchTicketText, actualBatchTicketText);
		BaseUI.baseStringPartialCompare("dataComplete_BatchStub_TextPresent", batchStubText, actualBatchStubText);
		BaseUI.baseStringPartialCompare("dataComplete_BatchStub_TextPresent", batchChkText, actualBatchChkText);
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BatchTicket_Img"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BatchStub_Img"));
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BatchChk_Img"));
	}


	public static void click_dataComplete_RejectBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Click_RejectImg"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_UnRejectBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Click_UnRejectImg"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_DocId_Img() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_DocId_Img"));
		BaseUI.waitForElementToBeDisplayed("dataComplete_SetDocID_Modal", null, null);
		BaseUI.wait_forPageToFinishLoading();

	}

	public static void update_DataComplete_DocID(String dropdownValue) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_SetDocID_Dropdown"), dropdownValue);
		Thread.sleep(500);
		BaseUI.click(Locator.lookupElement("dataComplete_SetDocID_OK_Btn"));
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void verify_DataComplete_DocID_Stub(String batchStubText){

		String actualBatchStubText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_NewBatchStub_Text"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchStub_TextPresent", batchStubText, actualBatchStubText);

	}

	public static void verify_DataComplete_DocID_Chk(String batchChkText){

		String actualBatchChkText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_NewBatchChk_Text"));
		BaseUI.baseStringPartialCompare("dataComplete_BatchStub_TextPresent", batchChkText, actualBatchChkText);
	}

	public static void select_dataComplete_DocIdDialogBox_Option(String newDocId) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_DocId_DialogBox_SelectItem"), newDocId);
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void select_dataComplete_RejectDialogBox_Option(String rejectOptn) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_Reject_RejectSelect_DdowList"), rejectOptn);
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static String return_SeqID() {
		// String batchID =
		// BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"));
		String seqID = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Pass1SequenceNumber"));

		return seqID;
	}

	public static String return_RejectJobItem(String indexNum) {
		// String batchID =
		// BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"));
		String jobItem = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_ItemSelectedByIndex", indexNum, null));

		return jobItem;
	}

	public static void click_dataCompleteCPR_Tran6_Img1() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran6_Img1"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataCompleteCPR_Tran1_Img1() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran1_Img1"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataCompleteCPR_Tran1_Img2() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran1_Img2"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataCompleteCPR_Tran1_Img3() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran1_Img3"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran2_Img1() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran2_Img1"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran2_Img2() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran2_Img2"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran2_Img3() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran2_Img3"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran2_Img4() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran2_Img4"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran2_Img5() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran2_Img5"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran2_Img6() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran2_Img6"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran3_Img1() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran3_Img1"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran3_Img2() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran3_Img2"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_dataComplete_Tran3_Img3() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran3_Img3"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_DataComplete_Tran4_Img1() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Tran4_Img1"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_DataComplete_BatchStub_Img() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_BatchStub_Img"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_DataComplete_BatchChk_Img() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_BatchChk_Img"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}


	public static String return_CheckCount() {

		String newChkCount6 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_CheckCount"));

		return newChkCount6;

	}

	public static String return_StubCount() {

		String newChkCount6 = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_StubCount0"));

		return newChkCount6;

	}

	public static void verify_dataComplete_Pass1SequenceNumber(String val1, String Val2) throws Exception {

		BaseUI.baseStringCompare("dataComplete_Pass1SequenceNumber", val1, Val2);

	}

	public static String return_CountInfo_checkTotal() {

		String checkTotal = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_ChkTotal"));

		return checkTotal;
	}

	public static String return_CountInfo_ItemCount() {

		String ItemCount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_ItemCount"));

		return ItemCount;
	}

	public static String return_CountInfo_SourceItemCount() {

		String ItemCount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_sourceItemCount"));

		return ItemCount;
	}

	public static String return_CountInfo_ChecCount() {

		String checCount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_ChkCount"));

		return checCount;
	}

	public static String return_CountInfo_RejectCheckCount() {

		String rejectCheckCount = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_rejectCheckCount"));

		return rejectCheckCount;
	}

	public static String return_CountInfo_RejectStubCount() {

		String rejectrejectStubCount = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_rejectStubCount"));

		return rejectrejectStubCount;
	}

	public static String return_CountInfo_StubCount() {

		String stubCount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_stubCount"));

		return stubCount;
	}

	public static String return_CountInfo_StubTotal() {

		String stubTotal = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_stubTotal"));

		return stubTotal;
	}

	public static String return_CountInfo_TranCount() {

		String tranCount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_tranCount"));

		return tranCount;
	}

	public static String return_CountInfo_SourceTranCount() {

		String sourceTranCount = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_sourceTranCount"));

		return sourceTranCount;
	}

	public static String return_CountInfo_GoodBalanceStubCount() {

		String goodBalanceStubCount = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_GoodBalanceStubCount"));

		return goodBalanceStubCount;
	}

	public static String return_BasicInfo_BatchId() {

		String goodBalanceStubCount = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_CountInfo_GoodBalanceStubCount"));

		return goodBalanceStubCount;
	}

	public static void click_BatchBy_BatchNumAndTran(String batchNum, String tranNum) throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_BatchBy_BatchNumAndTran", batchNum, tranNum));
		Thread.sleep(3000);

	}

	public static void click_BatchBy_IndexRejectTran(String indexNum) throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_BatchBy_RejectTran", indexNum, null));
		Thread.sleep(3000);

	}

	public static void verify_BatchBy_IndexRejectTran(String indexNum) throws Exception {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BatchBy_RejectTran", indexNum, null));

	}

	public static void unreject_AndVerify_IndexRejectTranIsNOTPresent(String index) throws Exception {

		click_BatchBy_IndexRejectTran(index);
		click_dataComplete_UnRejectBtn();
		verify_BatchBy_IndexRejectTranIsNOtPresent(index);
	}

	public static void verify_BatchBy_IndexRejectTranIsNOtPresent(String indexNum) throws Exception {

		// BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BatchBy_RejectTran",
		// indexNum,null));
		// Thread.sleep(3000);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_BatchBy_RejectTran", indexNum, null));

	}

	public static void click_BatchBy_BatchNumAndTran_Img3(String batchNum, String tranNum) throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_BatchBy_BatchNumAndTran_Img3", batchNum, tranNum));
		Thread.sleep(3000);

	}

	public static void click_BatchBy_BatchNumAndTran_Img4(String batchNum, String tranNum) throws Exception {

		BaseUI.click(Locator.lookupElement("dataComplete_BatchBy_BatchNumAndTran_Img4", batchNum, tranNum));
		Thread.sleep(3000);

	}

	public static void click_dataComplete_GhostStubDialogBox_OK_Btn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_GhostStubDialogBox_OK_Btn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	}

	public static void select_dataComplete_GhostStubDialogBox(String newDocId) throws Exception {

		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_GhoststubDialogBox_SelectDocId"), newDocId);
		Thread.sleep(3000);

	}

	public static void modalDialog_Click_CheckCapturedAsStub() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_SelectReason_ByIndex", "1", null));
		Thread.sleep(3000);

	}

	public static void modalDialog_Click_StubCapturedAsCheck() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_CPR_SelectReason_ByIndex", "2", null));
		Thread.sleep(3000);

	}

	public static String return_Seq_Label_ByIndexr(String rowNum) {

		String seqLabel = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_BeventStubSeqTextByRowNum", rowNum, null));
		return seqLabel;

	}

	public static void Verify_DocIdSeqsLabel(String seqNum3, String seqLabel) throws Exception {

		String seqtext = DataCompletion_CPR.return_Seq_Label_ByIndexr(seqNum3);
		BaseUI.baseStringCompare("dataComplete_BeventStubSeqTextByRowNum", seqLabel, seqtext);
	}

	public static void Verify_DocId_UpdatedTran_ValuesSeq(String dialogBoxDocId, String chkCount1, String StbCount1)
			throws Exception {

		DataCompletion_CPR.click_dataComplete_DocId_Img();
		DataCompletion_CPR.select_dataComplete_DocIdDialogBox_Option(dialogBoxDocId);
		DataCompletion_CPR.dataComplete_DocId_DialogBox_OkBtn();
		String newChkCount = DataCompletion_CPR.return_CheckCount();
		BaseUI.baseStringCompare("dataComplete_CheckCount", chkCount1, newChkCount);
		String newStbCount = DataCompletion_CPR.return_StubCount();
		BaseUI.baseStringCompare("dataComplete_StubCount0", StbCount1, newStbCount);

	}

	public static void verify_RejSeq2ItemUpdatedRejJob(String indexToReturn, String rejJob) throws Exception {

		String actualValue = DataCompletion_CPR.return_RejectJobItem(indexToReturn);
		BaseUI.baseStringCompare("keyJob", rejJob, actualValue);
	}

	public static void click_AND_verify_RejSeq2ItemUpdatedRejJob(String index1ToClick, String index2ToClick,
			String indexToReturn, String rejJob) throws Exception {
		click_BatchBy_IndexRejectTran(index1ToClick);
		click_BatchBy_IndexRejectTran(index2ToClick);
		verify_RejSeq2ItemUpdatedRejectReason(indexToReturn, rejJob);
	}

	public static void verify_RejSeq2ItemUpdatedRejectReason(String indexToReturn, String rejReason) throws Exception {

		String actualValueReason = DataCompletion_CPR.return_RejectJobItem(indexToReturn);
		BaseUI.baseStringCompare("RejectReason", rejReason, actualValueReason);

	}

	public static void verify_BatchItem_RejectJobUnreject(Integer index_Of_Field, String seqNumber8, String unReject,
			String indexpart) throws Exception {

		// String seqNumber8 = "8";
		index_Of_Field = DataMaintenance_Batch_ViewItems.return_FirstRowIndex_OfColumn_WithValue("P1 Seq Number",
				seqNumber8);
		DataMaintenance_Batch_ViewItems.scroll_horizontally_AllTheWay_ToRight();
		HashMap<String, String> viewItemRow = DataMaintenance_Batch_ViewItems.return_Row_ByIndex(index_Of_Field);
		BaseUI.baseStringPartialCompare("Audit Trail",
				unReject + "," + GlobalVariables.return_UserName_Formatted() + ",," + indexpart + GlobalVariables.return_UserName_Formatted() + ",,",
				viewItemRow.get("Audit Trail"));

	}

}// End of Class
