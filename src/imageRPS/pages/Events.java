package imageRPS.pages;

import java.util.HashMap;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class Events {

	public static void verify_Events_SubTabActive() throws Exception {
		AlertsEvents.verify_SubTab_Active("Events");
		BaseUI.verifyElementAppears(Locator.lookupElement("events_Header"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void enter_Text_Into_Data2(String textToEnter) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("events_Data2_InputBox"), textToEnter);
	}

	public static void click_Submit() throws Exception {
		BaseUI.click(Locator.lookupElement("events_SubmitButton"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
		//This is the acceptable timeframe for the events to appear.
		Events_SearchResults.wait_For_RangeToPopulate(15);
	}

	
	

	public static void click_Reset() throws Exception {
		BaseUI.click(Locator.lookupElement("events_ResetButton"));
		Thread.sleep(300);
	}

	public static TableData return_ExpectedResults_ForEventTesterCMD() {
		TableData expectedResults = new TableData();
		expectedResults.data.add(new HashMap<String, String>());
		expectedResults.data.add(new HashMap<String, String>());
		expectedResults.data.add(new HashMap<String, String>());
		expectedResults.data.get(0).put("Message", "FRS instances configured, CMDTest instances started");
		expectedResults.data.get(0).put("Station Id", "00");
		expectedResults.data.get(0).put("Severity", "3");
		expectedResults.data.get(0).put("Operator Id", "FRS203800");

		expectedResults.data.get(1).put("Message",
				"Dual Reformatter started host program REF client id = CMDTest consolidation number = consolidation date = 0");
		expectedResults.data.get(1).put("Station Id", "00");
		expectedResults.data.get(1).put("Severity", "3");
		expectedResults.data.get(1).put("Operator Id", "REF305100");

		expectedResults.data.get(2).put("Message", "Keep Alive - Instance Id: %1");
		expectedResults.data.get(2).put("Station Id", "00");
		expectedResults.data.get(2).put("Severity", "3");
		expectedResults.data.get(2).put("Operator Id", "WORS204099");

		return expectedResults;
	}

}// End of Class
