package imageRPS.pages;

import java.util.HashMap;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class Utilities_ConcurrentUsers {

	public static TableData return_userList() {

		TableData userData = new TableData();

		HashMap<String, String> cellMappings = new HashMap<String, String>();

		cellMappings.put("Operator Name", "./div[@role='gridcell'][1]/div");
		cellMappings.put("Operator ID", "./div[@role='gridcell'][2]/div");
		cellMappings.put("Last Logon", "./div[@role='gridcell'][3]/div");

		userData.data = BaseUI.tableExtractor("util_Users_ConcurrentUsers_TableRows", cellMappings);

		return userData;
	}

	public static void disconnect_User(String userID, TableData userList) throws Exception {
		selectUser_ByUserID(userID, userList);
		click_Disconnect_AndAccept();
	}

	public static void click_Disconnect_AndAccept() throws Exception {
		click_Disconnect();
		click_Yes_ForDisconnectModal();
	}

	public static void verify_TabLoaded() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("util_Users_Header"));
		Utilities.verify_SubTab_Active("Concurrent Users");
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void click_Disconnect() throws Exception {
		BaseUI.click(Locator.lookupElement("util_Users_Disconnect_Button"));
		BaseUI.waitForElementToBeDisplayed("util_Users_ConfirmDisconnect_Modal", null, null);

	}

	public static void click_Yes_ForDisconnectModal() throws Exception {
		BaseUI.click(Locator.lookupElement("util_Users_ConfirmDisconnect_Yes_Button"));
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void selectUser_ByUserID(String userID, TableData userList) throws Exception {
		Integer userIndex = userList.first_IndexOf("Operator ID", userID);

		BaseUI.click(Locator.lookupElement("util_Users_UserID_ByIndex", Integer.toString(userIndex + 1), null));
		Thread.sleep(100);

	}

	public static void click_Refresh() throws Exception {
		BaseUI.click(Locator.lookupElement("util_Users_Refresh_Button"));
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

}// End of Class
