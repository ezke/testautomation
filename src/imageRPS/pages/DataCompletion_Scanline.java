package imageRPS.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;
import java.util.HashMap;

//slc and sl2 jobs.
public class DataCompletion_Scanline {

	public static void key_allFields_UntilDone(Integer entryLength) throws Exception {

		Integer entryCount = 0;

		while (true) {
			if (entryCount > 20) {
				break;
			}

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			key_With_MaxValue_GenericValue(entryLength);
			entryCount++;
		}
	}

	public static void key_allFields_UntilDone(String valueToKey) throws Exception {

		Integer entryCount = 0;

		while (true) {
			if (entryCount > 20) {
				break;
			}

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			enter_Text_IntoKeyingField(valueToKey, true);
			entryCount++;
		}
	}

	public static void pressF1_QuestionOrConfirm_Modal() throws Exception {

		WebElement modalButton = Locator.lookupRequiredElement("dataComplete_Done_Confirm_YesButton");
		BaseUI.enterKey(modalButton, Keys.F1);
		Thread.sleep(300);
		Navigation.wait_For_Page_ToLoad();
	}

	//Note: It Press F1 to confirm Question Modal
	public static void key_allFields_UntilDone_OCR() throws Exception {

		int entryCount = 0;

		while (true) {
			if (entryCount > 20) {
				break;
			}

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			click_OCR();
			pressEnter_KeyingField();
			pressF1_QuestionOrConfirm_Modal();
			entryCount++;
		}
	}

	public static void key_With_MaxValue_GenericValue(Integer maxValue) throws Exception {
		String valueToEnter = "";
		while (valueToEnter.length() != maxValue) {
			valueToEnter += "1";
		}

		enter_Text_IntoKeyingField(valueToEnter, true);
	}

	public static void key_With_MaxValue_GenericValue_DONT_HitEnter(Integer maxValue) throws Exception {
		String valueToEnter = "";
		while (valueToEnter.length() != maxValue) {
			valueToEnter += "1";
		}

		enter_Text_IntoKeyingField(valueToEnter, false);
	}

	public static String return_Current_SeqNumber() {
		String currentDropdownValue = BaseUI.getSelectedOptionFromDropdown("dataComplete_SLC_SeqDropdown");

		return currentDropdownValue;
	}

	public static void verify_Seq_Changed(String previousSeq) {

		String currentSeq = return_Current_SeqNumber();
		BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(currentSeq), currentSeq + " was a number.",
				currentSeq + " was NOT a number.");
		BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(previousSeq), previousSeq + " was a number.",
				previousSeq + " was NOT a number.");
		BaseUI.verify_false_AndLog(previousSeq.equals(currentSeq), currentSeq + " does NOT match " + previousSeq,
				currentSeq + " did NOT change.");

	}

	public static void select_SeqNumber_FromDropdown(String seq) throws Exception {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("dataComplete_SLC_SeqDropdown"), seq);
		Thread.sleep(1000);
	}

	public static void verify_CurrentSeq_NotRejected() {
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_CurrentRejStatus", "---");
	}

	public static void verify_CurrentSeq_Rejected_bySLC() {
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_CurrentRejStatus", "slc");
	}

	public static void verify_CurrentSeq_Rejected_BySL2() {
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_CurrentRejStatus", "sl2");
	}

	public static void verify_CurrentSeq_Suspended() {
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_CurrentRejStatus", "sus");
	}

	public static void unReject_Job_IfRejected() throws Exception {
		String rejectText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_SLC_CurrentRejStatus"));

		if (!rejectText.equals("---")) {
			DataCompletion.click_reject();
		}
	}

	public static void verify_KeyingField_Text_MatchesExpected(String expectedValue) {
		WebElement keyingTextBox = Locator.lookupElement("dataComplete_SLC_visibleEntryField");
		String keyingText = BaseUI.getTextFromInputBox(keyingTextBox);
		BaseUI.baseStringCompare("Keying Text", expectedValue, keyingText);
	}

	public static void verify_No_InputError() {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_SLC_InputErrorElement"));
	}

	public static void enter_Text_IntoKeyingField(String valueToEnter, Boolean hitEnter) throws Exception {
		WebElement keyingTextBox = Locator.lookupElement("dataComplete_SLC_visibleEntryField");
		BaseUI.enterText_IntoInputBox(keyingTextBox, valueToEnter);
		if (hitEnter) {
			BaseUI.enterKey(keyingTextBox, Keys.ENTER);
			Thread.sleep(300);
			Navigation.wait_For_Page_ToLoad();
		}
	}

	public static void enter_Text_IntoKeyingField_WithoutClearing(String valueToEnter, Boolean hitEnter)
			throws Exception {
		WebElement keyingTextBox = Locator.lookupElement("dataComplete_SLC_visibleEntryField");
		BaseUI.click(keyingTextBox);
		keyingTextBox.sendKeys(valueToEnter);
		// BaseUI.enterText_IntoInputBox_DontUseClear(keyingTextBox,
		// valueToEnter);
		if (hitEnter) {
			BaseUI.enterKey(keyingTextBox, Keys.ENTER);
			Thread.sleep(300);
			Navigation.wait_For_Page_ToLoad();
		}
	}

	public static void click_OCR() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_OCR_Button"));
		Thread.sleep(1000);
	}

	public static void verify_OCR_Button_Works() throws Exception {
		String previousKeyingFieldText = BaseUI
				.getTextFromInputBox(Locator.lookupElement("dataComplete_SLC_visibleEntryField"));
		click_OCR();
		String newKeyingFieldText = BaseUI
				.getTextFromInputBox(Locator.lookupElement("dataComplete_SLC_visibleEntryField"));

		// Check that Error Element exists but does not appear. Error indicates
		// OCR did not work properly.
		BaseUI.verifyElementExists("dataComplete_SLC_WarningError", null, null);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("dataComplete_SLC_WarningError"));

		BaseUI.verify_false_AndLog(previousKeyingFieldText.equals(newKeyingFieldText),
				"Input text change from " + previousKeyingFieldText + " to " + newKeyingFieldText,
				"Input text did not change from " + previousKeyingFieldText);
		DataCompletion.verify_Button_Toggled(Locator.lookupElement("dataComplete_OCR_Button"));
	}

	public static void verify_OCR_Button_DoesNOTWork(String errorMessage) throws Exception {
		// String previousKeyingFieldText = BaseUI
		// .getTextFromInputBox(Locator.lookupElement("dataComplete_SLC_visibleEntryField"));
		click_OCR();
		// String newKeyingFieldText = BaseUI
		// .getTextFromInputBox(Locator.lookupElement("dataComplete_SLC_visibleEntryField"));

		DataCompletion.verify_ErrorMessage(errorMessage);
	}

	public static void click_Msrd_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_SLC_Msrd_Button"));
		Thread.sleep(500);
	}

	public static void set_Msrd_On_Or_Off(Boolean msrd_ShouldBeOn) throws Exception {
		WebElement msrdButton = Locator.lookupElement("dataComplete_SLC_Msrd_Button");
		String msrd_ClassValue = BaseUI.get_Attribute_FromField(msrdButton, "class");

		if (msrd_ShouldBeOn && msrd_ClassValue.equals("btn toggledButton")) {

		} else if (!msrd_ShouldBeOn && msrd_ClassValue.equals("btn toggledButton")) {
			click_Msrd_Button();
		} else if (!msrd_ShouldBeOn && !msrd_ClassValue.equals("btn toggledButton")) {
		} else if (msrd_ShouldBeOn && !msrd_ClassValue.equals("btn toggledButton")) {
			click_Msrd_Button();
		}
	}

	public static void click_Over_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_OverButton"));
		Thread.sleep(500);
	}

	public static void set_Over_On_Or_Off(Boolean over_ShouldBeOn) throws Exception {
		WebElement overButton = Locator.lookupElement("dataComplete_OverButton");
		String over_ClassValue = BaseUI.get_Attribute_FromField(overButton, "class");

		if (over_ShouldBeOn && over_ClassValue.equals("btn toggledButton")) {

		} else if (!over_ShouldBeOn && over_ClassValue.equals("btn toggledButton")) {
			click_Over_Button();
		} else if (!over_ShouldBeOn && !over_ClassValue.equals("btn toggledButton")) {
		} else if (over_ShouldBeOn && !over_ClassValue.equals("btn toggledButton")) {
			click_Over_Button();
		}
	}


	public static void verify_KeyingRows(String expectedRej, String expectedDocID, String seqDropdownValue,
										 String expectedOvertypeMode, String expectedMisreadMode){

		String cursorPosText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_SLC_CursorPos_Text"));
		String tranText = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_SLC_Tran_Text"));
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_CurrentRejStatus", expectedRej);
		BaseUI.verify_String_Not_EmptyOrNull(tranText);
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_DocId_Text", expectedDocID);
		BaseUI.verify_Value_Selected_InDropdown(Locator.lookupElement("dataComplete_SLC_SeqDropdown"), seqDropdownValue);
		BaseUI.verify_String_Not_EmptyOrNull(cursorPosText);
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_OvertypeMode_Text", expectedOvertypeMode);
		BaseUI.verifyElementHasExpectedText("dataComplete_SLC_MisreadMode_Text", expectedMisreadMode);

	}

	public static void pressEnter_KeyingField() throws Exception {

		WebElement keyingTextBox = Locator.lookupElement("dataComplete_SLC_visibleEntryField");
		BaseUI.enterKey(keyingTextBox, Keys.ENTER);
		Thread.sleep(300);
		Navigation.wait_For_Page_ToLoad();
	}

	public static HashMap<String, String> return_KeyField_RowsInfo() throws Exception {

		WebElement tableRow = Locator.lookupElement("dataComplete_SLC_KeyingRows");

			HashMap<String, String> rowData = new HashMap<String, String>();
			rowData.put("Seq", BaseUI.getTextFromInputBox(tableRow.findElement(By.xpath("./div[4]//select[@data-bind='options:$parent.displayedSeqNums, value:$parent.selectedSeqNum']"))));
			rowData.put("RawScalineText", BaseUI.getTextFromInputBox(tableRow.findElement(By.xpath("./div/div/input"))));
			String previousKeyingFieldText = BaseUI
					.getTextFromInputBox(Locator.lookupElement("dataComplete_SLC_visibleEntryField"));
			click_OCR();
			BaseUI.waitForElementToNOTContain_PartialAttributeMatch("dataComplete_SLC_visibleEntryField", null, null,
					"value", previousKeyingFieldText,15);
			rowData.put("FinalScalineText", BaseUI.getTextFromInputBox(tableRow.findElement(By.xpath("./div/div/input"))));

		return rowData;
	}

	public static TableData key_All_Fields_UntilDone() throws Exception {

		TableData keyFieldRowsTableData = new TableData();

		int entryCount = 0;

		while (true) {
			if (entryCount > 20) {
				break;
			}

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			keyFieldRowsTableData.data.add(return_KeyField_RowsInfo());
			pressEnter_KeyingField();
			entryCount++;
		}
		return keyFieldRowsTableData;
	}


}// End of Class
