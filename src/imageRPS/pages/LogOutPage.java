package imageRPS.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;


public class LogOutPage {
    public static void verifyLogOutPageMsg() {
        BaseUI.verifyElementAppears(Locator.lookupElement("signOutPage_Message"));
    }

}
