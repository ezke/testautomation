package imageRPS.pages;

import java.util.HashMap;

import utils.BaseUI;
import utils.Locator;

public class DataMaintenance_ItemSearch_Filters {

	public static void enterText_IntoFilterTextbox_ForGivenFields(HashMap<String, String> fieldsToEnter) {

	
		for (String field : fieldsToEnter.keySet()) {
			enterText_IntoFilterTextbox(field, fieldsToEnter.get(field));
		}
	}

	public static void enterText_IntoFilterTextbox(String filterName, String textToEnter) {
		BaseUI.enterText_IntoInputBox(
				Locator.lookupElement("dataMaint_ItemSearch_TextBox_ByLabelText", filterName, null), textToEnter);
	}


	public static void click_Submit() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ItemSearch_SubmitButton"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}


	public static void click_Reset() throws Exception {
		BaseUI.click_js(Locator.lookupElement("dataMaint_ItemSearch_ResetButton"));
		Thread.sleep(500);
	}

	public static void verify_DataMaint_ItemSearchFilters_Loaded() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ItemSearchFilters_Header"));
		DataMaintenance.verify_SubTab_Active("Item Search");
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void verify_FieldName_Value(String fieldName, String textToVerify) throws Exception {

		DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox(fieldName, textToVerify);
		String fieldNameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_ItemSearch_TextBox_ByLabelText", fieldName, null));
		BaseUI.baseStringCompare(fieldName + " Value", textToVerify, fieldNameInputText);

	}

	public static void verify_FieldName_Length_DoesNOTExceed_MaxLength(String fieldName, String textToVerify, int maxLength) throws Exception {

		DataMaintenance_ItemSearch_Filters.enterText_IntoFilterTextbox(fieldName, textToVerify);
		String fieldnameInputText = BaseUI.getTextFromInputBox(Locator.lookupElement("dataMaint_ItemSearch_TextBox_ByLabelText", fieldName, null));
		BaseUI.verify_true_AndLog(fieldnameInputText.length()<= maxLength, fieldName + " was less or equal to " + maxLength , fieldName + " was NOT less or eqaul to " + maxLength);
	}

	public static void verify_Alert_DialogPresent() {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ItemSearchFilters_AlertDialogBox"));
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemSearchFilters_AlertDialogBox_Text",
				"Account Number, Dollar Amount, or Doc ID must be filled in!");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ItemSearchFilters_Alert_OK_Button"));
	}
	
}// End of class
