package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class Utilities_PrintTicket_BatchTicketFilter {

	public static void search_ByCriteria(String clientID, String lockboxID, String docGroup, WorkType workType)
			throws Exception {

		BaseUI.waitForElementToBeClickable("util_PrintBatchFilter_clientID_Textbox", null, null);
		click_ResetFilters();

		if (clientID != null) {
			BaseUI.enterText(Locator.lookupElement("util_PrintBatchFilter_clientID_Textbox"), clientID);
		}

		if (lockboxID != null) {
			BaseUI.enterText(Locator.lookupElement("util_PrintBatchFilter_lockboxID_Textbox"), lockboxID);
		}

		if (docGroup != null) {
			BaseUI.enterText(Locator.lookupElement("util_PrintBatchFilter_docGroup_Textbox"), docGroup);
		}

		if (workType != null) {
			BaseUI.selectValueFromDropDown(Locator.lookupElement("util_PrintBatchFilter_workType_Dropdown"),
					workType.getValue());
		}

		click_Submit();
	}
	
	public static void verify_PrintTicket_TabLoadedSuccessfully() throws Exception {
		Utilities.verify_SubTab_Active("Print Ticket");
		BaseUI.verifyElementAppears(Locator.lookupElement("util_PrintBatchFilter_Header"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void verify_Entering_ClientID_Updates_LockboxID(String clientID, String expected_LockboxID)
			throws Exception {
		click_ResetFilters();

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("util_PrintBatchFilter_clientID_Textbox"), clientID);
	
		Thread.sleep(200);
		String lockbox_text = BaseUI
				.getTextFromInputBox(Locator.lookupElement("util_PrintBatchFilter_lockboxID_Textbox"));

		BaseUI.baseStringCompare("Lockbox ID", expected_LockboxID, lockbox_text);
	}

	public static void click_ResetFilters() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintBatchFilter_ResetButton"));
		Thread.sleep(500);
	}

	public static void click_Submit() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintBatchFilter_SubmitButton"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();

	}

	public enum WorkType {
		SelectWorkType("Select Work Type", 0), Single("Single", 1), Multiples("Multiples", 2), StubsOnly(
				"Stubs Only", 3), ChecksOnly("Checks Only", 4), NSF("NSF", 5);

		private String value;
		private Integer index;

		private WorkType(final String val, final Integer associatedNumber) {
			value = val;
			index = associatedNumber;
		}

		public String getValue() {
			return value;
		}

		public Integer getWorkType_Index() {
			return index;
		}
		
		@Override
		public String toString() {
			return getValue();
		}
	}

}// End of Class
