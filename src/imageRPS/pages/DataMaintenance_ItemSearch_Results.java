package imageRPS.pages;

import java.time.Duration;
import java.util.HashMap;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class DataMaintenance_ItemSearch_Results {

	public static TableData return_SearchResults() {
		TableData searchResults = new TableData();

		String[] columnHeaders = { "Client ID", "Batch ID", "Batch Number", "Batch Date", "P1 Seq Number", "Stub/Check",
				"Acct Field", "Account Number", "Amt Field", "Save Amount", "Applied Amount", "Tran Number" };

		searchResults.data = BaseUI.tableExtractor(columnHeaders, "dataMaint_Item_Search_ColumnValues_ByColumnHeader");

		return searchResults;
	}

	public static TableData return_SearchResults2() {
		TableData searchResults = new TableData();

		String[] columnHeaders = { "Client ID", "Batch ID", "Batch Number", "Batch Date", "P1 Seq Number", "Stub/Check",
				"Acct Field", "Account Number", "Amt Field", "Save Amount", "Applied Amount", "Tran Number" };

		if (Browser.currentBrowser.equals("chrome")) {
			searchResults = BaseUI.tableExtractorV2("dataMaint_TableBody", columnHeaders);
		} else {

			searchResults = BaseUI.tableExtractor_ByCell("dataMaint_TableCell", columnHeaders);

		}

		return searchResults;
	}

	public static void verify_Filter_InFilterResultsList(String filterName, String filterValue) {
		String filter = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ItemSearch_FilterList"));
		BaseUI.baseStringPartialCompare("Filter " + filterName, filterName + "=" + filterValue, filter);
	}

	public static void verify_Filters_InFilterResultsList(HashMap<String, String> filtersToCheck) {
		String filter = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ItemSearch_FilterList"));

		BaseUI.verify_true_AndLog(filtersToCheck.keySet().size() > 0, "Found keys to test.",
				"Did not find keys to test.");

		for (String field : filtersToCheck.keySet()) {
			BaseUI.baseStringPartialCompare("Filter " + field, field + "=" + filtersToCheck.get(field), filter);
		}
	}

	public static void refresh_And_Wait_ForColumnValueToBeUpdated(String columnName, String expectedColumnValue,int indexValue, int timeToWaitInSeconds) throws Exception {

		BaseUI.wait_ForCondition_ToBeMet(()->{
					click_RefreshBtn();
					sort_Column_Ascending(columnName);
					String columnValue = return_ItemSearchResultsByRowsIndexValue(columnName, indexValue);
					return columnValue.equals(expectedColumnValue);},
				Duration.ofSeconds(timeToWaitInSeconds), Duration.ofMillis(5000));
		Thread.sleep(500);
	}

	public static void scroll_Horizontally_Until_CellVisible_ByHeader(String headerText) throws Exception {

		int maxCount = 0;

		while (!BaseUI.elementAppears(Locator.lookupElement("dataMaint_Item_Search_ColumnValues_ByColumnHeader", headerText, null))) {
			if (maxCount > 50) {
				break;
			}

			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));

			maxCount++;
		}

	}

	public static void sort_Column_Ascending(String columnName) throws Exception {

		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);

		for (int i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("ascending")) {
				BaseUI.click_js(
						Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("ascending")) {
				break;
			}
		}
	}


	public static void click_RefreshBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ItemSearchResults_Refresh_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static String return_ItemSearchResultsByRowsIndexValue(String rowTitle, int indexValue) {
		String SearchResults = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ItemSearchResults_ItemList_RowsIndexValue",rowTitle
				,String.valueOf(indexValue)));

		return SearchResults;

	}

}// end of class
