package imageRPS.pages;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import imageRPS.pages.Navigation.TopNavigationTab;
import utils.*;

public class DataMaintenance_Consolidations_SearchResults {
	
	
	
	public static String return_Cell_ByColumnHeader_AndIndex(String headerText, Integer index) {
		String cellValue = BaseUI.getTextFromField(
				Locator.lookupElement("dataMaint_Consolidation_SearchResults_ColumnCell_ByHeaderText_ByIndex", headerText, index.toString()));

		System.out.println("Returning value " + cellValue);
		Reporter.log("Returning value " + cellValue);

		return cellValue;
	}
	static String[] headerArray = { "Client ID", "Lockbox ID", "Client Name", "Consol Date", "Day", "Consol#",
			"Consol Amount", "Consol Checks", "Consol Batches", "Open Amount", "Open Checks", "Open Batches", "Consol Cutoff Date Time", "Consol Close Date Time",
			"Complete Date", "Complete", "Deposited", "Auto Consol", "Consol Type", "Warn Window", "Capture Window",
			"XAG Window", "Auto Exception" };


	public static TableData return_TableRows_ViaColumn() throws Exception {

		TableData tableRows = new TableData();

		tableRows.data = table_Extractor(headerArray, "dataMaint_Consolidation_ColumnValue_ByHeaderText");

		return tableRows;
	}

	public static ArrayList<HashMap<String, String>> table_Extractor(String[] headerArray, String elementLocator_byHeaderText) throws Exception {
		ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();

		for (String header : headerArray) {

			scroll_Horizontally_Until_CellVisible_ByHeader(header);

			ArrayList<WebElement> columnCellList = Locator.lookup_multipleElements(elementLocator_byHeaderText, header,
					null);

			for (int i = 0; i < columnCellList.size(); i++) {
				HashMap<String, String> tableRow = null;

				if (table_Values.size() == 0 || table_Values.size() <= i) {
					tableRow = new HashMap<String, String>();
					tableRow.put(header, BaseUI.getTextFromField(columnCellList.get(i)));
					table_Values.add(tableRow);
				} else {
					table_Values.get(i).put(header, BaseUI.getTextFromField(columnCellList.get(i)));
				}
			}

		}

		return table_Values;

	}


	public static void scroll_Horizontally_Until_CellVisible_ByHeader(String headerText) throws Exception {

		Integer maxCount = 0;

		while (!BaseUI.elementAppears(Locator.lookupElement("dataMaint_Consolidation_ColumntableconsolResultGrid", headerText, null))) {
			if (maxCount > 50) {
				break;
			}
	
			BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_HorizontalScrollBar_MoveRight"));
			BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_HorizontalScrollBar_MoveRight"));
			BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_HorizontalScrollBar_MoveRight"));
			BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_HorizontalScrollBar_MoveRight"));

			maxCount++;
		}

	}

	public static ArrayList<String> return_ColumnTextList_ByHeader(String headerText) {
		ArrayList<WebElement> columnElementList = Locator
				.lookup_multipleElements("dataMaint_Consolidation_ColumntableconsolResultGrid", headerText, null);
		ArrayList<String> columnTextList = new ArrayList<String>();

		for (WebElement columnCell : columnElementList) {
			columnTextList.add(BaseUI.get_NonVisible_TextFromField(columnCell));
		}

		return columnTextList;
	}

	public static void verify_ColumnMatches_ExpectedValue(String columnName, String expectedValue) {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			BaseUI.baseStringCompare(columnName, expectedValue, value);
		}

	}

	public static void verify_Column_Values_AreBetweenNumericRange(String columnText, int expectedValueFrom, int expectedValueTo)
			throws Exception {
		ArrayList<String> columnList_ForComparison = return_ColumnTextList_ByHeader(columnText);

		BaseUI.verify_true_AndLog(columnList_ForComparison.size() > 0, "Column Data found.", "Column Data NOT found.");

		for (String cellValue : columnList_ForComparison) {
			int cellInt = Integer.parseInt(cellValue);
			BaseUI.verify_true_AndLog(cellInt >= expectedValueFrom && cellInt <= expectedValueTo,
					"Value " + cellValue + " is greater or equal to " + String.valueOf(expectedValueFrom) + " and " + String.valueOf(expectedValueTo),
					"Value " + cellValue + " was NOT greater or equal to " + String.valueOf(expectedValueFrom)+
							" and " + String.valueOf(expectedValueTo));
		}

	}

	public static void verify_Filter_Range_Text_ContainsFilter(String filterName, String first_FilterValue, String second_FilterValue) {
		String filterText = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_FiltersBar")).split("\\Hide")[0].trim();

		String expectedText = filterName + "=" + first_FilterValue + " thru " + second_FilterValue;
		BaseUI.baseStringPartialCompare("Filter Contains", expectedText, filterText);
	}

	public static void click_ReExportConsol_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_SearchResults_ReExportConsol_Button"));
		Thread.sleep(300);
		BaseUI.waitForElementToBeDisplayed("dataMaint_Consolidation_ReExportConsol_Modal", null, null, 25);
	}

	public static void click_AllReExport_CheckBoxes() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ReExportConsol_ReExportBatches_CheckBox"));
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ReExportConsol_ReExportArchived_CheckBox"));
		Thread.sleep(300);
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ReExportConsol_ReExportReject_CheckBox"));
		Thread.sleep(300);
	}

	public static void click_ReExportConsol_OKButton() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ReExportConsol_OK_Button"));
		Thread.sleep(300);
		BaseUI.waitForElementToNOTBeDisplayed("dataMaint_Consolidation_ReExportConsol_Modal", null, null, 25);
	}

	public static void verify_FilterText_ContainsFilter(String filterName, String filterValue) {
		String filterText = BaseUI
				.getTextFromField(Locator.lookupElement("dataMaint_Consolidation_SearchResults_Filters"));
		String expectedText = filterName + "=" + filterValue;
		BaseUI.baseStringPartialCompare("Filter Contains", expectedText, filterText);
	}

	// Indexing starts at 1.
	public static void click_ConsolidationCheckbox_ByIndex(Integer indexToCheck) throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_consolidationList_checkboxByIndex",
				indexToCheck.toString(), null));
		Thread.sleep(300);

	}

	// Indexing starts at 1.
	public static void click_ConsolidationCheckbox_ClientID(String clientID) throws Exception {
		ArrayList<String> cutoff_clientIDList = new ArrayList<String>();
		cutoff_clientIDList = DataMaintenance_Consolidations_SearchResults.return_ColumnTextList_ByHeader("Client ID");
		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ByIndex(cutoff_clientIDList.indexOf(clientID) + 1);
	}
	
	
	public static void click_OpenBatches() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_OpenBatches_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_BatchListTable_FirstCell", null, null, 40);
		Navigation.wait_For_Page_ToLoad();
	}
	
	public static void click_RefreshBtn() throws Exception {
		
		
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_Refresh_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void refresh_And_Wait_ForColumnValueToBeUpdated(String columnName, String expectedColumnValue) {

		BaseUI.wait_ForCondition_ToBeMet(()->{
			click_RefreshBtn();
			String columnValue = return_ConsolSearchResults(columnName);
			return columnValue.equals(expectedColumnValue);},70, 5000);

	}

	public static void refresh_And_Wait_ForColumnValueToBeUpdated(String columnName, String expectedColumnValue, int timeToWaitInSeconds) throws Exception {

		BaseUI.wait_ForCondition_ToBeMet(()->{
			click_RefreshBtn();
			String columnValue = return_ConsolSearchResults(columnName);
			return columnValue.equals(expectedColumnValue);},
				Duration.ofSeconds(timeToWaitInSeconds), Duration.ofMillis(5000));

	}

	public static void refresh_And_Wait_ForColumnValueToBeUpdated(String columnValue,String expectedColumn, String expectedValue, Duration timeToWaitInSeconds) throws Exception {
		BaseUI.wait_ForCondition_ToBeMet(() -> {
			click_RefreshBtn();
			TableData data = return_SearchResults_ForConsolidation();
			String dataCell = data.return_Row_BasedOn_1MatchingField("Client ID" , columnValue).get(expectedColumn);
			if (dataCell.contains(expectedValue)) {
				return true;
			}
			return false;
		}, timeToWaitInSeconds, Duration.ofSeconds(25));
	}
	
	public static void click_ChangeAutoConsolTime_OkBtn() throws Exception {
		
		
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_OkBtn"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();

	// Wait for the qasss process to completed require at list 2 minute
	}
	
	// XPath indexing starts at 1.
	public static void check_ConsolidationCheckbox_ByIndex(Integer indexToCheck) throws Exception {
		WebElement checkboxStatus_Element = Locator.lookupElement(
				"dataMaint_ConsolidationResults_consolidationList_checkboxByIndex_Status", indexToCheck.toString(),
				null);
		String checkboxStatus = BaseUI.get_Attribute_FromField(checkboxStatus_Element, "class");
		if (checkboxStatus != null && checkboxStatus.equals("jqx-checkbox-check-checked")) {
		} else {
			click_ConsolidationCheckbox_ByIndex(indexToCheck);
		}
	}

	public static void uncheck_ConsolidationCheckbox_ByIndex(Integer indexToCheck) throws Exception {
		WebElement checkboxStatus_Element = Locator.lookupElement(
				"dataMaint_ConsolidationResults_consolidationList_checkboxByIndex_Status", indexToCheck.toString(),
				null);
		String checkboxStatus = BaseUI.get_Attribute_FromField(checkboxStatus_Element, "class");
		if (checkboxStatus != null && checkboxStatus.equals("jqx-checkbox-check-checked")) {
			click_ConsolidationCheckbox_ByIndex(indexToCheck);
		}
	}

	public static void verify_SelectedRowCount(Integer expectedRowCount) throws Exception {
		BaseUI.verifyElementHasExpectedText("dataMaint_ConsolidationResults_consolidationList_SelectedRowCount",
				expectedRowCount.toString());
	}
	public static void verify_LateCount(String expectedRowCount) throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ConsolidationResults_consolidationList_LateCountBg"));
		BaseUI.verifyElementHasExpectedText("dataMaint_ConsolidationResults_consolidationList_LateCount",
				expectedRowCount.toString());
	}
	
	public static void click_StopConsol() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_StopConsol_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_ConsolidationResults_StopConsol_Manual_Modal", null, null, 20);
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_StopConsol_Auto() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_StopConsol_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_ConsolidationResults_StopConsol_Auto_Modal", null, null, 20);
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_StopConsol_MsgAlert_ConfirmBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_StopConsol_MsgAlert_ConfirmBtn"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_ChangeAutoConsolTime() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_ChangeAutoConsolTime_Button"));
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();

	}
	
	public static void click_GoBack_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_GoBack_Button"));
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_ViewAutoConsol() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_ViewAutoConsol_Button"));
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_ViewAutoConsol_BasicInfo(String clientID, String clientName, String consolidationNo, String closed, String completed){

		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Client ID:", null), clientID);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Client Name:", null), clientName);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Consolidation Date:", null), currentDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Consolidation#:", null), consolidationNo);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Process Date:", null), currentDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Receive Date:", null), currentDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Closed:", null), closed);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_BasicInfo_ByText", "Completed:", null), completed);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_DeletedEvent_CheckBox"), false);
	}

	public static void verify_ViewAutoConsol_EventInfo(){

		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_EventOccursOn_Date"), currentDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_EventOccursOn_Time"), "09:00 pm");
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_PerformConsolidation_CheckBox"), true);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_WaitForBatches_CheckBox"), false);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_KeepTrying_CheckBox"), true);
		BaseUI.verifyCheckboxStatus(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_PermitEmptyConsolidation_CheckBox"), true);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_SendWarning"), "0");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_NewBatchesToNextEvent"), "0");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_CloseXAGBatches"), "0");
	}

	public static void verify_ViewAutoConsol_DepositAndReportInfo(){
		String currentDate = BaseUI.getDateAsString_InRelationToTodaysDate(0);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_DepositDate"), currentDate);
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_DepositTime"), "09:00 pm");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_Deposited"), "No");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_DepositType"), "Consolidation");
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_ReportType"), "None");
		String reportName = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_Consolidation_ViewAutoConsol_ReportName"));
		BaseUI.verify_true_AndLog(BaseUI.stringEmpty(reportName), "ReportName Text is empty", "ReportName Text is not empty");
	}

	public static void click_ConsolFilter_BreadcrumbLink() throws Exception {
		BaseUI.click_js(Locator.lookupElement("dataMaint_Consolidation_SearchResults_ConsolFilter_Link", null, null));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_ConsolResult_BreadcrumbLink() throws Exception {
		BaseUI.click_js(Locator.lookupElement("dataMaint_Consolidation_SearchResults_ConsolResult_Link", null, null));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}
	
	
	public static void click_StopConsol_OK_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_StopConsol_Modal_OK_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_ConsolidationResults_NextConsolidationDateConfirmation_Modal",
				null, null);
		Thread.sleep(400);
	}

	public static void click_KeyBatch() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_KeyBatch_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_ConsolBatches_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolBatches_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void verify_dataMaint_ConsolBatches_NoBatchAvaliable() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ConsolBatches_NoBatchAvaliable"));

	}

	public static void select_FirstRow() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_FirstRow_FirstCell"));
		Thread.sleep(300);
	}

	public static void verify_StopGeneratedDate_Selected() {
		BaseUI.verifyCheckboxStatus(
				Locator.lookupElement("dataMaint_ConsolidationResults_StopConsol_Modal_SystemGeneratedDate_Radio"),
				true);
		BaseUI.verifyElementDisabled(
				Locator.lookupElement("dataMaint_ConsolidationResults_StopConsol_Modal_ConsolDate_Box"));
		BaseUI.verifyElementDisabled(
				Locator.lookupElement("dataMaint_ConsolidationResults_StopConsol_Modal_ConsolNumber_TextBox"));
	}
	
	

	
	public static String return_ConsolSearchResults(String rowTitle) {
		// String batchID =
		// BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"));
		String SearchResults = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ConsolidationResults_consolidationList_RowsValue",rowTitle,null));

		return SearchResults;
		
	}
	
	public static String return_ConsolSearchResultsByRowsIndexValue(String rowTitle, String IndevValue) {
		// String batchID =
		// BaseUI.getTextFromField(Locator.lookupElement("dataComplete_ClientData_BatchId"));
		String SearchResults = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ConsolidationResults_consolidationList_RowsIndexValue",rowTitle,IndevValue));

		return SearchResults;
		
	}


	// index starts at 1.
	public static void select_Row_ByIndex(Integer index) throws Exception {

		String cellElementIdentifier = "";
		if (BaseUI.elementAppears(Locator.lookupElement("dataMaint_SearchResults_BatchMenuGrid"))) {
			cellElementIdentifier = "dataMaint_FirstCell_ByIndex";
		} else {
			cellElementIdentifier = "dataMaint_ViewItems_FirstCell_ByRowIndex";
		}

		WebElement cellElement = Locator.lookupElement(cellElementIdentifier, index.toString(), null);

		BaseUI.click(cellElement);
		Thread.sleep(300);
		Navigation.wait_For_Page_ToLoad();
	}

	// Set this method to only return certain columns that I'm using in Next
	// Consolidation Date Confirmation Modal

	public static TableData return_SearchResults() {
		TableData searchResults = new TableData();
		String[] headersToPull = { "Client ID", "Client Name", "Consol Date", "Day", "Consol#" };

		searchResults.data = BaseUI.tableExtractor(headersToPull,
				"dataMaint_ConsolidationResults_consolidationList_ColumnElements_ByHeaderText");
		
		
		return searchResults;
	}


	public static TableData return_SearchResults_ForConsolidation() {
		TableData searchResults = new TableData();
		String[] headersToPull = { "Client ID", "Consol Amount", "Consol Checks", "Consol Batches", "Open Amount", "Open Checks", "Open Batches", "Consol Close Date Time" };

		searchResults.data = BaseUI.tableExtractor(headersToPull,
				"dataMaint_ConsolidationResults_consolidationList_ColumnElements_ByHeaderText");


		return searchResults;
	}


	public static TableData return_NextConsolidationDateConfirmation_Modal_Table() {
		TableData tableResults = new TableData();

		String[] tableHeaders = { "Client ID", "Client", "Consol Date", "Consol Num", "Consol Time", "New Consol Date",
				"New Consol Day", "New Consol Num", "New Consol Time" };

		if (Browser.currentBrowser.equals("Chrome")) {
			tableResults = BaseUI.tableExtractorV2(
					"dataMaint_ConsolidationResults_NextConsolidationDateConfirmation_Modal_TableBody", tableHeaders);

		} else {
			tableResults = BaseUI.tableExtractor_ByCell(
					"dataMaint_ConsolidationResults_NextConsolidationDateConfirmation_Modal_TableCells", tableHeaders);
		}

		return tableResults;
	}

	public static void click_NextConsolidationDateConfirmation_ConfirmButton() throws Exception {
		BaseUI.click(Locator
				.lookupElement("dataMaint_ConsolidationResults_NextConsolidationDateConfirmation_ConfirmButton"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void sort_SearchResults_Ascending(String headerText) throws Exception {
		for (int i = 0; i < 3; i++) {

			String sortedAttribute = BaseUI.get_Attribute_FromField(Locator.lookupElement(
					"dataMaint_ConsolidationResults_consolidationList_Header_SortAttribute_ByText", headerText, null),
					"aria-sort");

			if (sortedAttribute != null && sortedAttribute.equals("ascending")) {
				break;
			} else {

				BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_consolidationList_Header_ByText",
						headerText, null));
				Thread.sleep(500);
			}
		}

	}

	public static void sort_SearchResults_Descending(String headerText) throws Exception {
		for (int i = 0; i < 3; i++) {

			String sortedAttribute = BaseUI.get_Attribute_FromField(Locator.lookupElement(
					"dataMaint_ConsolidationResults_consolidationList_Header_SortAttribute_ByText", headerText, null),
					"aria-sort");

			if (sortedAttribute == null || sortedAttribute.equals("descending")) {
				break;
			} else {

				BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResults_consolidationList_Header_ByText",
						headerText, null));
				Thread.sleep(500);
			}
		}

	}

	public static void verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNumIncremented(
			TableData searchResults, TableData nextConsolidationDateResults, Integer rowIndex) throws Exception {

		// Next Consolidation Date Results comes back in different order from
		// Consolidation List, opted to find match based on Client ID
		HashMap<String, String> nextConsolidationRow = nextConsolidationDateResults
				.return_Row_BasedOn_1MatchingField("Client ID", searchResults.data.get(rowIndex).get("Client ID"));

		// Validate Client ID
		BaseUI.baseStringCompare("Client ID", searchResults.data.get(rowIndex).get("Client ID"),
				nextConsolidationRow.get("Client ID"));

		// Validate Client
		BaseUI.baseStringCompare("Client", searchResults.data.get(rowIndex).get("Client Name"),
				nextConsolidationRow.get("Client"));

		// Validate Consol Date
		BaseUI.baseStringCompare("Consol Date", searchResults.data.get(rowIndex).get("Consol Date"),
				nextConsolidationRow.get("Consol Date"));

		// Validate Consol Num (should match exactly)
		BaseUI.baseStringCompare("Consol Num", searchResults.data.get(rowIndex).get("Consol#"),
				nextConsolidationRow.get("Consol Num"));

		// Validate New Consol Date matches old Consol Date
		BaseUI.baseStringCompare("New Consol Date", nextConsolidationRow.get("Consol Date"),
				nextConsolidationRow.get("New Consol Date"));

		// Validate New Consol Day matches Day, need to do a date format conversion.
		String newConsolDay = nextConsolidationRow.get("New Consol Day");
		String searchResultsDay = searchResults.data.get(rowIndex).get("Day");
		searchResultsDay = BaseUI.return_Date_AsDifferentFormat(searchResultsDay, "EEE", "EEEE");

		BaseUI.baseStringCompare("Day", newConsolDay, searchResultsDay);

		// Consol Num should increment by 1.
		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(nextConsolidationRow.get("New Consol Num")),
				nextConsolidationRow.get("New Consol Num") + " was an Integer.",
				nextConsolidationRow.get("New Consol Num") + " was NOT an Integer.");
		BaseUI.verify_true_AndLog(
				Integer.parseInt(nextConsolidationRow.get(
						"New Consol Num")) == (Integer.parseInt(searchResults.data.get(rowIndex).get("Consol#")) + 1),
				"New Consol Num increased by 1.", "New Consol Num did NOT increase by 1.");

		// New Consol Time (Format)
		BaseUI.verify_true_AndLog(
				BaseUI.time_MatchesFormat(nextConsolidationRow.get("New Consol Time").toUpperCase(), "hh:mm a"),
				"Time was in acceptable format.", "Time was NOT in acceptable format.");

	}

	public static void verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNum_NOT_incremented(
			TableData searchResults, TableData nextConsolidationDateResults, Integer rowIndex,
			Integer expectedConsolNum) throws Exception {

		// Next Consolidation Date Results comes back in different order from
		// Consolidation List, opted to find match based on Client ID
		HashMap<String, String> nextConsolidationRow = nextConsolidationDateResults
				.return_Row_BasedOn_1MatchingField("Client ID", searchResults.data.get(rowIndex).get("Client ID"));

		// Validate Client ID
		BaseUI.baseStringCompare("Client ID", searchResults.data.get(rowIndex).get("Client ID"),
				nextConsolidationRow.get("Client ID"));

		// Validate Client
		BaseUI.baseStringCompare("Client", searchResults.data.get(rowIndex).get("Client Name"),
				nextConsolidationRow.get("Client"));

		// Validate Consol Date
		BaseUI.baseStringCompare("Consol Date", searchResults.data.get(rowIndex).get("Consol Date"),
				nextConsolidationRow.get("Consol Date"));

		// Validate Consol Num (should match exactly)
		BaseUI.baseStringCompare("Consol Num", searchResults.data.get(rowIndex).get("Consol#"),
				nextConsolidationRow.get("Consol Num"));

		// Validate New Consol Date matches old Consol Date
		BaseUI.verify_false_AndLog(
				nextConsolidationRow.get("Consol Date").equals(nextConsolidationRow.get("New Consol Date")),
				"Consol Date does not match New Consol Date.",
				"Consol Date was NOT suppposed to match New Consol Date.");

		// Validate New Consol Day (should be a day of week)
		BaseUI.verify_true_AndLog(BaseUI.string_IsDayOfWeek(nextConsolidationRow.get("New Consol Day")),
				nextConsolidationRow.get("New Consol Day") + " was a day of the week.",
				nextConsolidationRow.get("New Consol Day") + " was NOT a day of the week.");

		// Consol Num should equal our expected Num.
		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(nextConsolidationRow.get("New Consol Num")),
				nextConsolidationRow.get("New Consol Num") + " was an Integer.",
				nextConsolidationRow.get("New Consol Num") + " was NOT an Integer.");
		BaseUI.verify_true_AndLog(Integer.parseInt(nextConsolidationRow.get("New Consol Num")) == expectedConsolNum,
				"New Consol Num matched expected num.",
				"New Consol Num was " + nextConsolidationRow.get("New Consol Num") + " we were expecting "
						+ expectedConsolNum.toString());

		// New Consol Time (Format)
		BaseUI.verify_true_AndLog(
				BaseUI.time_MatchesFormat(nextConsolidationRow.get("New Consol Time").toUpperCase(), "hh:mm a"),
				"Time was in acceptable format.", "Time was NOT in acceptable format.");

	}

	public static void verify_UPDATED_Match_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows(
			TableData searchResults, TableData nextConsolidationDateResults, Integer rowIndex) throws Exception {
		// for (Integer rowIndex : rowsToPick) {
		// Next Consolidation Date Results comes back in different order from
		// Consolidation List, opted to find match based on Client ID
		HashMap<String, String> nextConsolidationRow = nextConsolidationDateResults
				.return_Row_BasedOn_1MatchingField("Client ID", searchResults.data.get(rowIndex).get("Client ID"));

		// Validate Client ID
		BaseUI.baseStringCompare("Client ID", nextConsolidationRow.get("Client ID"),
				searchResults.data.get(rowIndex).get("Client ID"));

		// Validate Client
		BaseUI.baseStringCompare("Client", nextConsolidationRow.get("Client"),
				searchResults.data.get(rowIndex).get("Client Name"));

		// Validate Consol Date
		BaseUI.baseStringCompare("Consol Date", nextConsolidationRow.get("New Consol Date"),
				searchResults.data.get(rowIndex).get("Consol Date"));

		// Validate Consol Num (should match exactly)
		BaseUI.baseStringCompare("Consol Num", nextConsolidationRow.get("New Consol Num"),
				searchResults.data.get(rowIndex).get("Consol#"));

		// Validate New Consol Day matches Day, need to do a date format conversion.
		String newConsolDay = nextConsolidationRow.get("New Consol Day");
		String searchResultsDay = searchResults.data.get(rowIndex).get("Day");
		searchResultsDay = BaseUI.return_Date_AsDifferentFormat(searchResultsDay, "EEE", "EEEE");

		BaseUI.baseStringCompare("Day", newConsolDay, searchResultsDay);

		// New Consol Num should match
		BaseUI.baseStringCompare("Consol Num", nextConsolidationRow.get("New Consol Num"),
				searchResults.data.get(rowIndex).get("Consol#"));

		// Validate New Consol Date (format)
		BaseUI.baseStringCompare("Consol Date", nextConsolidationRow.get("New Consol Date"),
				searchResults.data.get(rowIndex).get("Consol Date"));
		// }

	}

	// Due to the complexity of Consolidation nonsense, I've had to hard code what
	// we're expecting here for this specific Client ID.
	// If this changes, run a database refresh. If that doesn't help, blame
	// Jonathan.
	public static void verify_SearchTableRows_Match_NextConsolidationDateConfirmation_Rows_ConsolNum_401_Decremented(
			TableData searchResults, TableData nextConsolidationDateResults, Integer rowIndex) throws Exception {

		// Next Consolidation Date Results comes back in different order from
		// Consolidation List, opted to find match based on Client ID
		HashMap<String, String> nextConsolidationRow = nextConsolidationDateResults
				.return_Row_BasedOn_1MatchingField("Client ID", "401");

		// Validate Client ID
		BaseUI.baseStringCompare("Client ID", "401", nextConsolidationRow.get("Client ID"));

		// Validate Client
		BaseUI.baseStringCompare("Client", "Stop Manual Consol Test (Schedule)", nextConsolidationRow.get("Client"));

		// Validate Consol Date
		BaseUI.baseStringCompare("Consol Date", searchResults.data.get(rowIndex).get("Consol Date"),
				nextConsolidationRow.get("Consol Date"));

		// Validate Consol Num (should match exactly)
		BaseUI.baseStringCompare("Consol Num", "2", nextConsolidationRow.get("Consol Num"));

		BaseUI.baseStringCompare("Consol Time", "", nextConsolidationRow.get("Consol Time"));

		// Validate New Consol Date matches old Consol Date
		BaseUI.verify_false_AndLog(
				nextConsolidationRow.get("Consol Date").equals(nextConsolidationRow.get("New Consol Date")),
				"Consol Date does not match New Consol Date.",
				"Consol Date was NOT suppposed to match New Consol Date.");

		// Validate New Consol Day (should be a day of week)
		BaseUI.verify_true_AndLog(BaseUI.string_IsDayOfWeek(nextConsolidationRow.get("New Consol Day")),
				nextConsolidationRow.get("New Consol Day") + " was a day of the week.",
				nextConsolidationRow.get("New Consol Day") + " was NOT a day of the week.");

		// Consol Num should equal our expected Num.
		BaseUI.verify_true_AndLog(nextConsolidationRow.get("New Consol Num").equals("1"),
				"New Consol Num matched expected num.",
				"New Consol Num was " + nextConsolidationRow.get("New Consol Num") + " we were expecting " + "1");

		// New Consol Time (Format)
		BaseUI.verify_true_AndLog(
				BaseUI.time_MatchesFormat(nextConsolidationRow.get("New Consol Time").toUpperCase(), "hh:mm a"),
				"Time was in acceptable format.", "Time was NOT in acceptable format.");

		BaseUI.verify_true_AndLog(nextConsolidationRow.get("New Consol Time").equals("11:00 am"),
				"Time matched 11:00 am", "Time was NOT 11:00 am.");

	}

	// index starts at 1.
	public static TableData select_401_AndNavigateTo_NextConsolidationDateConfirmation_ReturnTableResults(
			Integer indexOf401_OnResultsPage) throws Exception {
		TableData nextConsolidationDateConfirmation_Results = new TableData();
		DataMaintenance_Consolidations_SearchResults.check_ConsolidationCheckbox_ByIndex(indexOf401_OnResultsPage);
		DataMaintenance_Consolidations_SearchResults.click_StopConsol();
		DataMaintenance_Consolidations_SearchResults.click_StopConsol_OK_Button();
		nextConsolidationDateConfirmation_Results = DataMaintenance_Consolidations_SearchResults
				.return_NextConsolidationDateConfirmation_Modal_Table();

		return nextConsolidationDateConfirmation_Results;

	}
	
	public static void verify_NewTime(String clientId1, String clientId2) throws Exception {
		
		Navigation.navigate_toTab(TopNavigationTab.DataMaintenance);
		DataMaintenance.navigate_to_SubTab("Consolidations");	
		DataMaintenance_Consolidations_Filters.click_ConsolType_Auto();
		DataMaintenance_Consolidations_Filters.click_Submit();	
		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ClientID(clientId1);
		DataMaintenance_Consolidations_SearchResults.click_ConsolidationCheckbox_ClientID(clientId2);
		Thread.sleep(500);
		DataMaintenance_Consolidations_SearchResults.click_ChangeAutoConsolTime();	
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsol_ChangeDateTimeToNewValue"));
		Thread.sleep(1000);	
		BaseUI.verifyElementDisabled(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_AdjustByHrs"));
		BaseUI.verifyElementDisabled(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_AdjustByMns"));
		
		
		
	}

	public static void click_ChageDatesTimesToNewValue() throws Exception{

		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsol_ChangeDateTimeToNewValue"));
		Thread.sleep(1000);
	}

	public static void enter_ChangeAutoConsolidationTime_NewDate(String newDate) throws Exception {

		BaseUI.enterText(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewDate"),
				newDate);
		BaseUI.tabThroughField(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewDate"));
	}

	public static void enter_ChangeAutoConsolidationTime_NewTime(String newTime) throws Exception {

		BaseUI.enterText(Locator.lookupElement("dataMaint_ConsolidationResultsChangeAutoConsolTime_NewTime"),
				newTime);
		Thread.sleep(200);
	}

	public static void click_ExportList() throws Exception {
		String downloadsFolder = Browser.downloadLocation;
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_SearchResults_ExportList_Button"));
		DataBuilder.wait_ForFile_WithExtension_ToExist(".csv", downloadsFolder, 5);
	}

	// This will also delete any prior existing csv files from Downloads Folder.
	public static TableData return_ExportList_TableData() throws Exception {
		// String downloadsFolder = DataBuilder.return_Downloads_Folder();
		String downloadsFolder = Browser.downloadLocation;

		String downloadFolderLog = "Download folder set to " + downloadsFolder;
		BaseUI.log_Status(downloadFolderLog);

		TableData exportResults = new TableData();

		// DataBuilder.delete_Files_FromDownloads_withExtension(".csv");
		DataBuilder.delete_Files_WithExtention_FromLocation(".csv", downloadsFolder);
		click_ExportList();

		String locate_File = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);
		String locatedFileLog = "Located File " + locate_File;
		BaseUI.log_Status(locatedFileLog);

		exportResults = DataBuilder.returnTableData_ForComparison(locate_File, ",", false);
		exportResults.remove_Character_FromKeys("\"");
		exportResults.remove_Character("\"");

		return exportResults;
	}




}// End of class
