package imageRPS.pages;


import utils.BaseUI;
import utils.Locator;

public class DataMaintenance_TransInfo {

	public static void verify_BatchID(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_BatchID", expectedValue);
	}


	
	public static void verify_SourceBatchID(String sourceID) {

		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_SourceBatchID", sourceID);
	}
	
	public static void verify_Balanced(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_Balanced", expectedValue);
	}

	public static void verify_Check_And_Stub_Totals(String expectedValue) {
		String expectedAmount = expectedValue;
		expectedAmount = expectedAmount.replace("$", "");
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_CheckTotal", expectedAmount);
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_StubTotal", expectedAmount);
	}

	public static void verify_RejectJob(String expectedValue) {
		String rejectJob = expectedValue;
		rejectJob = rejectJob.equals("---") ? "--- -" : rejectJob;
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_RejectJob", rejectJob);
	}

	public static void verify_TranNumber(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_TranNumber", expectedValue);
	}


	public static void verify_Job(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_Job", expectedValue);
	}

	public static void verify_KeyingJob(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_TransInfo_KeyingJob", expectedValue);
	}

	public static void click_GoBack() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_GoBack_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

}// End of Class
