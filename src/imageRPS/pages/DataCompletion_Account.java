package imageRPS.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

//Use for act, aux, crv, dda, ins, pye
public class DataCompletion_Account {

	public static void key_Job_With_GenericValues(String genericValue) throws Exception {

		Integer loopCount = 0;
		while (true) {
			if (loopCount >= 20) {
				BaseUI.verify_true_AndLog(false, "",
						"Loop took too many attempts to key crv job, failing test to avoid infinite loop. Please check screenshot.");
			}

			String textValue = BaseUI
					.getTextFromInputBox(Locator.lookupElement("dataComplete_crv_Last_FieldToCorrect"));
			if (!textValue.equals("")) {
				break;
			} else {
				enter_TheValue(genericValue);
			}

			loopCount++;

		}

		// DataCompletion.end_DataCompletion();
	}

	public static String return_first_FieldName() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_crv_first_FieldName"));
	}

	public static String return_first_Seq() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_crv_first_Seq"));
	}

	public static void enter_TheValue(String valueToEnter) throws Exception {

		WebElement textField = Locator.lookupElement("dataComplete_crv_Last_FieldToCorrect");

		BaseUI.enterText_IntoInputBox(textField, valueToEnter);
		BaseUI.enterKey(textField, Keys.ENTER);
		Thread.sleep(500);
	}

	public static void key_allFields_UntilDone(String clientID) throws Exception {

		Integer entryCount = 0;
		TableData batchInfo = Navigation.return_ConfigData();

		try {

			while (true) {
				if (entryCount > 20) {
					break;
				}

				if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
					break;
				}

				// TableData newTable =
				// batchInfo.return_Table_BasedOn_AnyNumberOfMatchingFields(fieldsToMatch);
				Integer minValue = return_CurrentMinValue(clientID, batchInfo);

				key_With_MinValue_GenericValue(minValue);
				entryCount++;
			}
		} finally {
			batchInfo = null;
		}
	}

	public static Integer return_CurrentMinValue(String clientID, TableData batchInfo) throws Exception {
		String docID = return_last_doc();
		String lastFieldName = return_last_fieldName();

		HashMap<String, String> fieldsToMatch = new HashMap<String, String>();
		fieldsToMatch.put("client_id", clientID);
		fieldsToMatch.put("doc_id", docID);
		fieldsToMatch.put("field_name", lastFieldName);

		HashMap<String, String> jobInfo = batchInfo.return_Row_BasedOn_AnyNumberOfMatchingFields(fieldsToMatch);
		Integer minValue = null;
		String minimumFieldLength = jobInfo == null ? "0" : jobInfo.get("Minimum_Field_Length");

		minValue = minimumFieldLength == null || minimumFieldLength.equals("") || minimumFieldLength.equals("\"\"") ? 0
				: Integer.parseInt(minimumFieldLength);

		return minValue;
	}

	public static void key_With_MinValue_GenericValue(Integer minValue) throws Exception {
		String valueToEnter = "";

		if (minValue != 0) {
			while (valueToEnter.length() != minValue) {
				valueToEnter += "1";
			}
		} else {
			valueToEnter = "1";
		}

		enter_Text_IntoKeyingField_AndClearUpVerify(valueToEnter, true);
	}

	public static void key_With_MinValue_GenericValue_DontHitEnter(Integer minValue) throws Exception {
		String valueToEnter = "";

		if (minValue != 0) {
			while (valueToEnter.length() != minValue) {
				valueToEnter += "1";
			}
		} else {
			valueToEnter = "1";
		}

		enter_Text_IntoKeyingField(valueToEnter, false);
	}

	public static void enter_Text_IntoKeyingField(String valueToEnter, Boolean hitEnter) throws Exception {
		WebElement keyingTextBox = Locator.lookupElement("dataComplete_Field_Last_TextBox");
		BaseUI.enterText_IntoInputBox(keyingTextBox, valueToEnter);
		if (hitEnter) {
			BaseUI.enterKey(keyingTextBox, Keys.ENTER);
			Thread.sleep(500);
//			BaseUI.wait_forPageToFinishLoading();
			Navigation.wait_For_Page_ToLoad();
		}
	}

	public static void enter_Text_IntoKeyingField_AndClearUpVerify(String valueToEnter, Boolean hitEnter)
			throws Exception {
		enter_Text_IntoKeyingField(valueToEnter, hitEnter);

		String lastSeq = BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Last_Seq"));
		if (lastSeq.equals("verify")) {
			enter_Text_IntoKeyingField(valueToEnter, hitEnter);
		}
	}

	public static String return_first_textField() {
		return BaseUI.getTextFromInputBox(Locator.lookupElement("dataComplete_Field_First_TextBox"));
	}

	public static String return_first_doc() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_First_DocID"));
	}

	public static String return_last_doc() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Last_DocID"));
	}

	public static String return_first_seq() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_First_Seq"));
	}

	public static String return_last_seq() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Last_Seq"));
	}

	public static String return_first_fieldName() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_First_FieldName"));
	}

	public static String return_last_fieldName() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Last_FieldName"));
	}

	// Index starts at 0. Wanted index to start the same for readability
	// purposes.
	public static void click_On_KeyField_ByIndex(Integer indexToClick) throws Exception {
		// Had to add 1 to account for index difference in xpath.
		indexToClick++;

		BaseUI.click(Locator.lookupElement("dataComplete_Field_TextBox_ByIndex", indexToClick.toString(), null));
		Thread.sleep(500);
	}

	public static String stripOut_DocumentType_FromFieldName(String fieldName) {
		String docType = fieldName;
		if (fieldName.contains("(")) {
			docType = fieldName.substring(0, fieldName.indexOf("(") + 1);
			docType = docType.replace(")", "");
		}
		return docType;
	}

	public static void verify_Seq_Changed(String previousSeq) {

		String currentSeq = return_last_seq();
		BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(currentSeq), currentSeq + " was a number.",
				currentSeq + " was NOT a number.");
		BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(previousSeq), previousSeq + " was a number.",
				previousSeq + " was NOT a number.");
		BaseUI.verify_false_AndLog(previousSeq.equals(currentSeq), currentSeq + " does NOT match " + previousSeq,
				currentSeq + " did NOT change.");
	}

	public static TableData return_document_Info() {
		TableData docInfo = new TableData();

		ArrayList<WebElement> tableRows = Locator.lookup_multipleElements("dataComplete_act_docRows", null, null);

		for (WebElement row : tableRows) {
			HashMap<String, String> rowData = new HashMap<String, String>();
			rowData.put("Doc", BaseUI.getTextFromField(row.findElement(By.xpath("./div[@data-bind='html:docId']"))));
			rowData.put("Seq", BaseUI.getTextFromField(row.findElement(By.xpath("./div[@data-bind='html:seq']"))));
			rowData.put("Rej",
					BaseUI.getTextFromField(row.findElement(By.xpath("./div/span[contains(@data-bind,'html:rej')]"))));
			rowData.put("Field", BaseUI.getTextFromInputBox(row.findElement(By.xpath("./div/input"))));
			rowData.put("Field Name",
					BaseUI.getTextFromField(row.findElement(By.xpath("./div[@data-bind='html:FieldName']"))));

			docInfo.data.add(rowData);
		}

		return docInfo;
	}

	// Index starts at 0.
	public static void verify_Row_NotRejected_OR_Susp(TableData docInfo, Integer rowIndex) {
		BaseUI.baseStringCompare("Should not be Reject or Susp", "---", docInfo.data.get(rowIndex).get("Rej"));
	}

	// Index starts at 0.
	public static void verify_Row_Rejected(TableData docInfo, Integer rowIndex) {
		String dataCompletionType = BaseUI
				.get_NonVisible_TextFromField(Locator.lookupElement("dataComplete_ClientData_JobName"));

		BaseUI.baseStringCompare("Reject Reason", dataCompletionType, docInfo.data.get(rowIndex).get("Rej"));
	}

	// Index starts at 0.
	public static void verify_Row_IsSusp(TableData docInfo, Integer rowIndex) {
		BaseUI.baseStringCompare("Susp", "sus", docInfo.data.get(rowIndex).get("Rej"));
	}

}// End of Class
