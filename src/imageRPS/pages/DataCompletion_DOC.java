package imageRPS.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class DataCompletion_DOC {

	public static void key_through_DOC_With_FirstDocID() throws Exception {

		Integer keyCount = 0;
		while (true) {
			if (keyCount > 20) {
				break;
			}

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
				break;
			}

			key_lastTextBox_WithFirstDocID();

		}

	}

	public static void key_lastTextBox_WithFirstDocID() throws Exception {
		String docID_ToKey = BaseUI
				.getTextFromField(Locator.lookupElement("dataComplete_DOC_SelectADocument_FirstDocID"));

		WebElement lastKeyingTextBox = Locator.lookupElement("dataComplete_DOC_Last_Keying_TextBox");

		BaseUI.enterText_IntoInputBox(lastKeyingTextBox, docID_ToKey);
		BaseUI.enterKey(lastKeyingTextBox, Keys.ENTER);
		Thread.sleep(500);

	}
	
	public static String return_first_Seq() {
		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_DOC_first_Seq"));
	}


	
}// End of Class
