package imageRPS.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import imageRPS.pages.Navigation.TopNavigationTab;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.Locator;
import utils.TableData;

public class DataMaintenance_Batch_SearchResults {

	static String[] headerArray = { "Client ID", "Lockbox ID", "Batch ID", "Batch Num", "Batch Date", "Balanced",
			"Doc Grp", "Locked", "Locked By", "Read Only", "Keying Job", "Item Count", "Check Count", "Reject Checks",
			"Check Total", "Block Num", "Pass2 Date", "PbC", "Priority", "Location ID", "P1 Stn ID",
			"Receive Date", "Process Date", "Cutoff Date", "Consol Date", "Consol Num", "SourceID", "Source Job",
			"Archived", "MICR Loaded", "NSF Loaded", "Purge OK", "CUS Tag", "CU2 Tag" };

	public static TableData return_TableRows_ViaColumn() throws Exception {

		TableData tableRows = new TableData();

		tableRows.data = table_Extractor(headerArray, "dataMaint_ColumnValues_ByHeaderText");

		return tableRows;
	}

	public static TableData return_TableRows_ViaColumnV2() throws Exception {

		TableData tableRows = new TableData();

		tableRows = BaseUI.tableExtractorV2("dataMaint_Generic_TableBody", headerArray);

		return tableRows;
	}
	
	public static void run_SortTest_DataDriven(String headerText, String testType) throws Exception
	{
		switch (testType) {
		case "ascending_numeric":
			DataMaintenance_Batch_SearchResults.sort_Column_Ascending(headerText);
			DataMaintenance_Batch_SearchResults.verify_Column_Sorted_Numeric_Ascending(headerText);
			break;
		case "descending_numeric":
			DataMaintenance_Batch_SearchResults.sort_Column_Descending(headerText);
			DataMaintenance_Batch_SearchResults.verify_Column_Sorted_Numeric_Descending(headerText);
			break;
		case "ascending_date":
			DataMaintenance_Batch_SearchResults.sort_Column_Ascending(headerText);
			DataMaintenance_Batch_SearchResults.verify_Column_Sorted_DATE_Ascending(headerText);
			break;
		case "descending_date":
			DataMaintenance_Batch_SearchResults.sort_Column_Descending(headerText);
			DataMaintenance_Batch_SearchResults.verify_Column_Sorted_DATE_Descending(headerText);
			break;
		case "ascending_alpha":
			DataMaintenance_Batch_SearchResults.sort_Column_Ascending(headerText);
			DataMaintenance_Batch_SearchResults.verify_Column_Sorted_Ascending(headerText);
			break;
		case "descending_alpha":
			DataMaintenance_Batch_SearchResults.sort_Column_Descending(headerText);
			DataMaintenance_Batch_SearchResults.verify_Column_Sorted_Descending(headerText);
			break;

		default:
			BaseUI.verify_true_AndLog(false, "", "Unrecognized test type " + testType);

		}
		
		
		
	}
	
	
	
	
	public static void click_KeyBatch() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_KeyBatch_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_FetchMore() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_FetchMore_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	// This will also delete any prior existing csv files from Downloads Folder.
	public static TableData return_ExportList_TableData() throws Exception {
		// String downloadsFolder = DataBuilder.return_Downloads_Folder();
		String downloadsFolder = Browser.downloadLocation;

		String downloadFolderLog = "Download folder set to " + downloadsFolder;
		System.out.println(downloadFolderLog);
		Reporter.log(downloadFolderLog);

		TableData exportResults = new TableData();

		// DataBuilder.delete_Files_FromDownloads_withExtension(".csv");
		DataBuilder.delete_Files_WithExtention_FromLocation(".csv", downloadsFolder);
		click_ExportList();

		String locate_File = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);
		String locatedFileLog = "Located File " + locate_File;
		System.out.println(locatedFileLog);
		Reporter.log(locatedFileLog);

		exportResults = DataBuilder.returnTableData_ForComparison(locate_File, ",", false);
		exportResults.remove_Character_FromKeys("\"");
		exportResults.remove_Character("\"");

		return exportResults;
	}

	public static void click_PreviewList() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("dataMaint_SearchResults_PreviewList_Button"), false, 1000);
	}

	public static TableData return_PreviewList_SearchResults() {

		TableData searchResults = new TableData();
		searchResults.data = BaseUI.tableExtractor(headerArray, "dataMaint_PreviewList_ColumnCells_ByHeader");

		return searchResults;

	}

	public static void click_ExportList() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_ExportList_Button"));
		Thread.sleep(5000);
	}

	// Needed to add custom table extractor due to issues scrolling
	// horizontally.
	public static ArrayList<HashMap<String, String>> table_Extractor(String[] headerArray,
			String elementLocator_byHeaderText) throws Exception {
		ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();

		for (String header : headerArray) {

			scroll_Horizontally_Until_CellVisible_ByHeader(header);

			ArrayList<WebElement> columnCellList = Locator.lookup_multipleElements(elementLocator_byHeaderText, header,
					null);

			for (Integer i = 0; i < columnCellList.size(); i++) {
				HashMap<String, String> tableRow = null;

				if (table_Values.size() == 0 || table_Values.size() <= i) {
					tableRow = new HashMap<String, String>();
					tableRow.put(header, BaseUI.getTextFromField(columnCellList.get(i)));
					table_Values.add(tableRow);
				} else {
					table_Values.get(i).put(header, BaseUI.getTextFromField(columnCellList.get(i)));
				}
			}

		}

		return table_Values;

	}

	public static void click_ViewItems() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_ViewItems_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void lock_Batch() throws Exception {
		click_LockBatch_Button();

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_LockBatchModal_ReasonTextBox"),
				"Automation Test");

		click_OK_ToLockBatch();
	}

	public static void click_BatchEvents() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_BatchEvents_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}


	public static void click_OK_ToLockBatch() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_LockBatchModal_OK_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_LockBatch_Button() {
		BaseUI.click(Locator.lookupElement("dataMaint_LockBatch_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_LockBatchModal_Modal", null, null);
	}

	public static String return_Cell_ByColumnHeader_AndIndex(String headerText, Integer index) {
		String cellValue = BaseUI.getTextFromField(
				Locator.lookupElement("dataMaint_ColumnCell_ByHeaderText_ByIndex", headerText, index.toString()));

		System.out.println("Returning value " + cellValue);
		Reporter.log("Returning value " + cellValue);

		return cellValue;
	}

	public static void verify_RowCount_WentDown(Integer previousCount, Integer newCount) {
		BaseUI.verify_true_AndLog(previousCount > newCount, "Entry Count went down after applying filter.",
				"Entry Count did NOT go down after applying filter.");

	}

	public static void select_FirstRow() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Batch_FirstRow_FirstCell"));
		Thread.sleep(300);
	}

	// index starts at 1.
	public static void select_Row_ByIndex(Integer index) throws Exception {

		
		
		String cellElementIdentifier = "";
		if (BaseUI.elementAppears(Locator.lookupElement("dataMaint_SearchResults_BatchMenuGrid"))) {
			cellElementIdentifier = "dataMaint_FirstCell_ByIndex";
		} else {
			cellElementIdentifier = "dataMaint_ViewItems_FirstCell_ByRowIndex";
		}

		WebElement cellElement = Locator.lookupElement(cellElementIdentifier, index.toString(), null);
		
		BaseUI.click(cellElement);
		Thread.sleep(300);
		Navigation.wait_For_Page_ToLoad();
	}
	
	public static void select_Row_ByBatchID(String batchID) throws Exception {
		ArrayList<String> batchList_BatchIDs = new ArrayList<String>();
		batchList_BatchIDs = DataMaintenance_Batch_SearchResults.return_ColumnTextList_ByHeader("Batch ID");
		select_Row_ByIndex(batchList_BatchIDs.indexOf(batchID) + 1);
	}
	

	
	public static ArrayList<String> return_ColumnTextList_ByHeader_SearchResult(String headerText) throws Exception {
		////scroll_Horizontally_Until_CellVisible_ByHeader(headerText);
		// BaseUI.scroll_to_element_NOT_Javascript(first_columnCell);

		ArrayList<WebElement> columnElementList = Locator.lookup_multipleElements("dataMaint_Consolidation_ColumntableconsolResultGrid",
				headerText, null);
		ArrayList<String> columnTextList = new ArrayList<String>();

		for (WebElement columnCell : columnElementList) {

			columnTextList.add(BaseUI.get_NonVisible_TextFromField(columnCell));
		}

		return columnTextList;
	}
	
	public static void click_BatchInfo() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_BatchInfo_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	
	public static void verify_On_BatchSearchResults_Page() {
		Navigation.verify_Page_Active(TopNavigationTab.DataMaintenance);
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_BatchList_Header"));
	}

	public static void verify_Column_Values_AreBetweenRange(String columnText, Integer value_from, Integer value_to)
			throws Exception {
		ArrayList<String> columnList_ForComparison = return_ColumnTextList_ByHeader(columnText);

		BaseUI.verify_true_AndLog(columnList_ForComparison.size() > 0, "Column Data found.", "Column Data NOT found.");

		for (String cellValue : columnList_ForComparison) {
			Integer cellInt = Integer.parseInt(cellValue);
			BaseUI.verify_true_AndLog(cellInt >= value_from && cellInt <= value_to,
					"Value " + cellValue + " fell between " + value_from.toString() + " and " + value_to.toString(),
					"Value " + cellValue + " did NOT fall between " + value_from.toString() + " and "
							+ value_to.toString());
		}

	}

	public static void verify_FilterText_ContainsFilter(String filterName, String filterValue) {
		String filterText = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_FiltersBar"));

		String expectedText = filterName + "=" + filterValue;
		BaseUI.baseStringPartialCompare("Filter Contains", expectedText, filterText);
	}

	public static void verify_Filter_Range_Text_ContainsFilter(String filterName, String first_FilterValue,
			String second_FilterValue) {
		String filterText = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_FiltersBar"));

		String expectedText = filterName + "=" + first_FilterValue + " thru " + second_FilterValue;
		BaseUI.baseStringPartialCompare("Filter Contains", expectedText, filterText);
	}

	public static void click_BatchFilter_BreadcrumbLink() throws Exception {
		BaseUI.click_js(Locator.lookupElement("dataMaint_BatchFilter_Link", null, null));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_GoBack() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_GoBack_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}
	
	public static void sort_Column_Ascending(String columnName) throws Exception {

		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);
		// BaseUI.scroll_to_element_NOT_Javascript(
		// Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText",
		// columnName, null));

		for (Integer i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("ascending")) {
				BaseUI.click_js(
						Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("ascending")) {
				break;
			}
		}
	}

	public static void sort_Column_Descending(String columnName) throws Exception {
		//
		// BaseUI.scroll_to_element_NOT_Javascript(
		// Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText",
		// columnName, null));

		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);

		for (Integer i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("descending")) {
				BaseUI.click_js(
						Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("descending")) {
				break;
			}
		}
	}

	public static ArrayList<String> return_columnValues_ByColumnHeaderName(String headerText) {
		ArrayList<WebElement> columnCells = Locator.lookup_multipleElements("dataMaint_ColumnValues_ByHeaderText",
				headerText, null);
		ArrayList<String> columnValues = new ArrayList<String>();

		for (WebElement cell : columnCells) {

			columnValues.add(BaseUI.get_NonVisible_TextFromField(cell));
		}

		BaseUI.verify_true_AndLog(columnValues.size() > 0, "Found column values.", "Did NOT find column values.");

		return columnValues;

	}

	public static ArrayList<String> return_columnValues_ByColumnHeaderName_FormattedForNumbers(String headerText) {

		ArrayList<WebElement> columnCells = Locator.lookup_multipleElements("dataMaint_ColumnValues_ByHeaderText",
				headerText, null);
		ArrayList<String> columnValues = new ArrayList<String>();

		for (WebElement cell : columnCells) {

			String cellText = BaseUI.get_NonVisible_TextFromField(cell);
			cellText = cellText.replace("$", "");
			cellText = cellText.replace(",", "");
			cellText = cellText.equals("") ? "0" : cellText;

			columnValues.add(cellText.trim());
		}

		BaseUI.verify_true_AndLog(columnValues.size() > 0, "Found column values.", "Did NOT find column values.");

		return columnValues;

	}

	public static Integer return_UpperCountAmount() {
		String batchText = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_BatchList_Header"));

		batchText = batchText.substring(batchText.indexOf("-") + 1, batchText.indexOf(";"));

		return Integer.parseInt(batchText);
	}

	public static void verify_Column_Sorted_Numeric_Descending(String columnName) throws Exception {
		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);
		ArrayList<String> columnList = return_columnValues_ByColumnHeaderName_FormattedForNumbers(columnName);

		for (Integer i = 1; i < columnList.size(); i++) {

			Double previousDouble = Double.parseDouble(columnList.get(i - 1));
			Double currentDouble = Double.parseDouble(columnList.get(i));

			BaseUI.verify_true_AndLog(previousDouble >= currentDouble,
					MessageFormat.format("Row {0}: {1} is greater or equal to {2}", i.toString(),
							previousDouble.toString(), currentDouble.toString()),
					MessageFormat.format("Row {0}: {1} is NOT greater than or equal to {2}", i.toString(),
							previousDouble.toString(), currentDouble.toString()));
		}
	}

	public static void verify_Column_Sorted_Numeric_Ascending(String columnName) throws Exception {
		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);
		ArrayList<String> columnList = return_columnValues_ByColumnHeaderName_FormattedForNumbers(columnName);

		for (Integer i = 1; i < columnList.size(); i++) {

			Double previousDouble = Double.parseDouble(columnList.get(i - 1));
			Double currentDouble = Double.parseDouble(columnList.get(i));

			BaseUI.verify_true_AndLog(previousDouble <= currentDouble,
					MessageFormat.format("Row {0}: {1} is less than or equal to {2}", i.toString(),
							previousDouble.toString(), currentDouble.toString()),
					MessageFormat.format("Row {0}: {1} is NOT less than or equal to {2}", i.toString(),
							previousDouble.toString(), currentDouble.toString()));
		}
	}

	public static void verify_Column_Sorted_Descending(String columnName) {
		ArrayList<String> columnList = return_columnValues_ByColumnHeaderName(columnName);

		for (Integer i = 1; i < columnList.size(); i++) {

			String previousValue = columnList.get(i - 1);
			String currentValue = columnList.get(i);

			Integer comparison = previousValue.compareToIgnoreCase(currentValue);

			BaseUI.verify_true_AndLog(comparison >= 0,
					MessageFormat.format("Row {0}: {1} is greater or equal to {2}", i.toString(), previousValue,
							currentValue),
					MessageFormat.format("Row {0}: {1} is NOT greater than or equal to {2}", i.toString(),
							previousValue, currentValue));
		}
	}

	public static void verify_Column_Sorted_Ascending(String columnName) {
		ArrayList<String> columnList = return_columnValues_ByColumnHeaderName(columnName);

		for (Integer i = 1; i < columnList.size(); i++) {

			String previousValue = columnList.get(i - 1);
			String currentValue = columnList.get(i);

			Integer comparison = previousValue.compareToIgnoreCase(currentValue);

			BaseUI.verify_true_AndLog(comparison <= 0,
					MessageFormat.format("Row {0}: {1} is greater or equal to {2}", i.toString(), previousValue,
							currentValue),
					MessageFormat.format("Row {0}: {1} is NOT greater than or equal to {2}", i.toString(),
							previousValue, currentValue));
		}
	}

	public static void verify_Column_Sorted_DATE_Descending(String columnName) throws Exception {
		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);

		ArrayList<String> columnList = return_columnValues_ByColumnHeaderName(columnName);

		for (Integer i = 1; i < columnList.size(); i++) {

			String previousValue = columnList.get(i - 1);
			String currentValue = columnList.get(i);

			BaseUI.verify_FirstDate_IsBefore_OrEQUALto_SecondDate(currentValue, previousValue, "MM/dd/yyyy");

		}
	}

	public static void verify_Column_Sorted_DATE_Ascending(String columnName) throws Exception {
		scroll_Horizontally_Until_CellVisible_ByHeader(columnName);

		ArrayList<String> columnList = return_columnValues_ByColumnHeaderName(columnName);

		for (Integer i = 1; i < columnList.size(); i++) {

			String previousValue = columnList.get(i - 1);
			String currentValue = columnList.get(i);

			BaseUI.verify_FirstDate_IsBefore_OrEQUALto_SecondDate(previousValue, currentValue, "MM/dd/yyyy");
		}
	}

	public static void scroll_Horizontally_Until_CellVisible_ByHeader(String headerText) throws Exception {

		Integer maxCount = 0;

		// String scrollBarToUse = "";
		// String headerElementToUse = "";
		// if
		// (BaseUI.elementAppears(Locator.lookupElement("dataMaint_SearchResults_BatchMenuGrid")))
		// {
		// headerElementToUse = "dataMaint_ColumnValues_ByHeaderText";
		// scrollBarToUse = "dataMaint_HorizontalScrollBar_MoveRight";
		// } else {
		// headerElementToUse = "dataMaint_ViewItems_ColumnValues_ByHeaderText";
		// scrollBarToUse = "dataMaint_ViewItems_ScrollRightHorizontal";
		// }

		while (!BaseUI.elementAppears(Locator.lookupElement("dataMaint_ColumnValues_ByHeaderText", headerText, null))) {
			if (maxCount > 50) {
				break;
			}

			// WebElement myElement = (new WebDriverWait(Browser.driver,
			// 30)).until(ExpectedConditions
			// .elementToBeClickable(By.xpath(Locator.map.get("dataMaint_HorizontalScrollBar"))));
			// myElement.click();
			// Actions move = new Actions(Browser.driver);
			// move.moveToElement(myElement).clickAndHold();
			// move.moveByOffset(5000, 0);
			// move.release();
			// move.perform();

			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));
			BaseUI.click_js(Locator.lookupElement("dataMaint_HorizontalScrollBar_MoveRight"));

			maxCount++;
		}

	}

	public static ArrayList<String> return_ColumnTextList_ByHeader(String headerText) throws Exception {
		scroll_Horizontally_Until_CellVisible_ByHeader(headerText);
		// BaseUI.scroll_to_element_NOT_Javascript(first_columnCell);

		ArrayList<WebElement> columnElementList = Locator.lookup_multipleElements("dataMaint_ColumnValues_ByHeaderText",
				headerText, null);
		ArrayList<String> columnTextList = new ArrayList<String>();

		for (WebElement columnCell : columnElementList) {

			columnTextList.add(BaseUI.get_NonVisible_TextFromField(columnCell));
		}

		return columnTextList;
	}

	public static void verify_ColumnMatches_ExpectedValue(String columnName, String expectedValue) throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			BaseUI.baseStringCompare(columnName, expectedValue, value);
		}

	}

	public static void verify_ColumnMatches_DoesNOTContain_Value(String columnName, String expectedValue)
			throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		Boolean columnContainsValue = false;
		for (String value : columnTextList) {
			if (value.equals(expectedValue)) {
				columnContainsValue = true;
			}
		}

		BaseUI.verify_false_AndLog(columnContainsValue, "Value " + expectedValue + " was NOT found.",
				"Found value " + expectedValue);
	}

	public static void verify_ColumnMatches_ExpectedValues(String columnName, String[] expectedValues)
			throws Exception {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			Boolean valueMatches = false;
			for (String expectedValue : expectedValues) {
				if (expectedValue.equals(value)) {
					valueMatches = true;
				}

			}

			BaseUI.verify_true_AndLog(valueMatches, "Column text was " + value, "Was NOT expecting text " + value);
		}

	}

	// public static TableData return_TableRows()
	// {
	// TableData tableRows = new TableData();
	// HashMap<String, String> cellMappings = new HashMap<String, String>();
	// cellMappings.put("Client ID", "./div[1]/div");
	// cellMappings.put("Lockbox ID", "./div[2]/div");
	// cellMappings.put("Batch ID", "./div[3]/div");
	// cellMappings.put("Batch Num", "./div[4]/div");
	// cellMappings.put("Batch Date", "./div[5]/div");
	// cellMappings.put("Balanced", "./div[6]/div");
	// cellMappings.put("Doc Grp", "./div[7]/div");
	// cellMappings.put("Locked", "./div[8]/div");
	// cellMappings.put("Locked By", "./div[9]/div");
	// cellMappings.put("Read Only", "./div[10]/div");
	// cellMappings.put("Keying Job", "./div[11]/div");
	// cellMappings.put("Item Count", "./div[12]/div");
	// cellMappings.put("Check Count", "./div[13]/div");
	// cellMappings.put("Reject Checks", "./div[14]/div");
	// cellMappings.put("Check Total", "./div[15]/div");
	// cellMappings.put("Block Num", "./div[16]/div");
	// cellMappings.put("Pass2 Date", "./div[17]/div");
	// cellMappings.put("PbC", "./div[18]/div");
	// cellMappings.put("Priority", "./div[19]/div");
	// cellMappings.put("Location ID", "./div[20]/div");
	// cellMappings.put("P1 Stn ID", "./div[21]/div");
	// cellMappings.put("Receive Date", "./div[22]/div");
	// cellMappings.put("Process Date", "./div[23]/div");
	// cellMappings.put("Cutoff Date", "./div[24]/div");
	// cellMappings.put("Consol Date", "./div[25]/div");
	// cellMappings.put("SourceID", "./div[26]/div");
	// cellMappings.put("Source Job", "./div[27]/div");
	// cellMappings.put("Archived", "./div[28]/div");
	// cellMappings.put("MICR Loaded", "./div[29]/div");
	// cellMappings.put("NSF Loaded", "./div[30]/div");
	// cellMappings.put("Purge OK", "./div[31]/div");
	// cellMappings.put("CUS Tag", "./div[32]/div");
	// cellMappings.put("CU2 Tag", "./div[33]/div");
	//
	// tableRows.data = BaseUI.tableExtractor("dataMaint_SearchResults_Rows",
	// cellMappings);
	// return tableRows;
	//
	// }

}// End of Class
