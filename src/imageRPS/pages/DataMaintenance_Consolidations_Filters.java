package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class DataMaintenance_Consolidations_Filters {

	// public static void
	// enterText_IntoFilterTextbox_ForGivenFields(HashMap<String, String>
	// fieldsToEnter) {
	//
	//
	// for (String field : fieldsToEnter.keySet()) {
	// enterText_IntoFilterTextbox(field, fieldsToEnter.get(field));
	// }
	// }

	public static void verify_DataMaint_ConsolidationsPage_Loaded() throws Exception {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ConsolidationFilters_Header"));
		DataMaintenance.verify_SubTab_Active("Consolidations");
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void enterText_IntoFilterTextbox(String textToEnter) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_TextBox"), textToEnter);
	}

	public static void enterText_IntoConsolNumber_FromAndTo_TextBox(String textToEnterIntoFrom, String textToEnterIntoTo){
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolNumberFrom_TextBox"), textToEnterIntoFrom);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolNumberTo_TextBox"), textToEnterIntoTo);
	}

	public static void enterText_IntoFilterTextboxClientId(String textToEnter) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_TextBoxClientId"), textToEnter);
	}
	
	public static void enterText_IntoFilterTextbox_Filterby_ConsolCloseDateTimeRange(String textToEnter,
			String textToEnter1, String textToEnter2, String textToEnter3) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCloseDateFrom"),
				textToEnter);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCloseTimeFrom"),
				textToEnter1);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCloseDateTo"),
				textToEnter2);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCloseTimeTo"),
				textToEnter3);
	}

	public static void enterText_IntoFilterTextbox_FilterBy_ConsolCutoffDateTimeRange(String textToEnter,
			String textToEnter1, String textToEnter2, String textToEnter3) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCutOffDateFrom"),
				textToEnter);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCutOffTimeFrom"),
				textToEnter1);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCutOffDateTo"),
				textToEnter2);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Consolidation_ConsolidationCutOffTimeTo"),
				textToEnter3);

	}

	public static void enterText_IntoFilterTextbox_ConsolidationCloseDateTo(String textToEnter) {

	}

	public static void enterText_IntoFilterTextbox_ConsolidationCloseTimeTo(String textToEnter) {

	}

	public static void click_ConsolType_Auto() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ConsolType_Auto"));
		Thread.sleep(500);
	}

	public static void click_ConsolType_Manual() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ConsolType_Manual"));
		Thread.sleep(500);
	}

	public static void click_Submit() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_SubmitButton"));
		Thread.sleep(1000);
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_ShowPast() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_ConsolidationFilters_Click_ShowPast"));
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_ShowCurrent() throws Exception {
		BaseUI.click_js(Locator.lookupElement("dataMaint_ConsolidationFilters_Click_ShowCurrent"));
		Thread.sleep(500);
		Navigation.wait_For_Page_ToLoad();
	}
	

	public static void click_Reset() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Consolidation_ResetButton"));
		Thread.sleep(500);
	}

}// End of class
