package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class ReportsOutput_ConfiguredReports {

	public static void verify_ConfiguredReports_TabActive() throws Exception {
		ReportsOutput.verify_SubTab_Active("Configured Reports");
		BaseUI.verifyElementAppears(Locator.lookupElement("configuredReports_Header"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void sort_Column_Ascending(String columnName) throws Exception {

		for (Integer i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("ascending")) {
				BaseUI.click(
						Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("ascending")) {
				break;
			}
		}
	}

	public static void sort_Column_Descending(String columnName) throws Exception {

		for (Integer i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("descending")) {
				BaseUI.click(
						Locator.lookupElement("dataMaint_ColumnHeader_SortElement_ByHeaderText", columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("descending")) {
				break;
			}
		}
	}

	public static void select_Row_By_IDColumnText(Integer id_ToPick) throws Exception {
		BaseUI.click(Locator.lookupElement("configuredReports_SelectID_ColumnCell_ByText", id_ToPick.toString(), null));
		Thread.sleep(500);
	}

	public static void click_ViewReport() {
		BaseUI.click(Locator.lookupElement("configuredReports_ViewReportButton"));
		BaseUI.waitForElementToBeDisplayed("configuredReports_Report_FileTitle", null, null);
	}

	public static String[] return_TextReport_Lines() throws Exception {
		String[] reportLines;
		try {
			BaseUI.switch_ToIframe(0);
			String reportText = BaseUI.getTextFromField(Locator.lookupRequiredElement("configuredReports_ReportBody"));
			reportLines = reportText.split("\\n");
		} finally {
			BaseUI.switch_ToDefaultContent();
		}

		return reportLines;
	}

	public static void verify_Report_Title_MatchesExpected(String[] reportLines, String expectedText) {
		BaseUI.verify_true_AndLog(reportLines[0].contains(expectedText),
				"Found title " + expectedText + " in first row of report.",
				"Did NOT find title " + expectedText + " in first row of report.");

	}

}
