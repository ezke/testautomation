package imageRPS.pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.Locator;
import utils.TableData;

public class Utilities_PrintTicket_SearchResults {

	static String[] headers = { "Client ID", "Lockbox ID", "Client Name", "Doc Group", "DocGroup Name", "Work Type",
			"Work Type Description", "Ticket Count", "Ticket" };

	public static TableData return_SearchResults_Top10Results() {
		HashMap<String, String> tableCellMappings = new HashMap<String, String>();
		tableCellMappings.put("Client ID", ".//div[@role='gridcell'][2]/div");
		tableCellMappings.put("Lockbox ID", ".//div[@role='gridcell'][3]/div");
		tableCellMappings.put("Client Name", ".//div[@role='gridcell'][4]/div");
		tableCellMappings.put("Doc Group", ".//div[@role='gridcell'][5]/div");
		tableCellMappings.put("Doc Group Name", ".//div[@role='gridcell'][6]/div");
		tableCellMappings.put("Work Type", ".//div[@role='gridcell'][7]/div");
		tableCellMappings.put("Work Type Description", ".//div[@role='gridcell'][8]/div");
		tableCellMappings.put("Ticket Count", ".//div[@role='gridcell'][9]/div");
		tableCellMappings.put("Ticket", ".//div[@role='gridcell'][10]/div");

		TableData searchResults = new TableData();
		searchResults.data = BaseUI.tableExtractor(10, "util_PrintTicketSearchResults_TableRows", tableCellMappings);

		return searchResults;
	}

	public static TableData return_SearchResults2() {

		TableData searchResults = new TableData();
		// searchResults.data = BaseUI.tableExtractor(headers,
		// "util_PrintTicketSearchResults_TableColumn_ByHeaderText");
		if (Browser.currentBrowser.startsWith("chrome")) {
			searchResults = BaseUI.tableExtractorV2("util_PrintTicketSearchResults_TableBody", headers);
		} else {
			searchResults = BaseUI.tableExtractor_ByCell("util_PrintTicketSearchResults_TableCells", headers);
		}

		return searchResults;

	}

	// This will also delete any prior existing csv files from Downloads Folder.
	public static TableData return_ExportList_TableData() throws Exception {
		// String downloadsFolder = DataBuilder.return_Downloads_Folder();
		String downloadsFolder = Browser.downloadLocation;

		String downloadFolderLog = "Download folder set to " + downloadsFolder;
		System.out.println(downloadFolderLog);
		Reporter.log(downloadFolderLog);

		TableData exportResults = new TableData();

		// DataBuilder.delete_Files_FromDownloads_withExtension(".csv");
		DataBuilder.delete_Files_WithExtention_FromLocation(".csv", downloadsFolder);
		click_ExportList();

		String locate_File = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);
		//Added redundancy in case file doesn't appear immediately.
		if (locate_File == null) {
			Thread.sleep(10000);
			locate_File = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);
		}
		String locatedFileLog = "Located File " + locate_File;
		System.out.println(locatedFileLog);
		Reporter.log(locatedFileLog);

		exportResults = DataBuilder.returnTableData_ForComparison(locate_File, ",", false);
		exportResults.remove_Character_FromKeys("\"");
		exportResults.remove_Character("\"");

		return exportResults;
	}

	public static void click_PrintBatchTicket() {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_PrintBatchTicket_Button"));
		BaseUI.waitForElementToBeDisplayed("util_PrintTicketSearchResults_Alert_OK_Button", null, null, 40);
	}

	public static void click_Print3PartTicket() {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_Print3PartTicket_Button"));
		BaseUI.waitForElementToBeDisplayed("util_PrintTicketSearchResults_Alert_OK_Button", null, null, 420);
	}

	public static void click_OK_on_Alert() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_Alert_OK_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void launch_PrintBatchTicket_PDF() throws Exception {
		click_PrintBatchTicket();
		click_OK_on_Alert();
		BaseUI.waitForElementToBeDisplayed("util_PrintTicketSearchResults_PDF_iFrame", null, null, 20);
	}

	public static void launch_Print3PartTicket_PDF() throws Exception {
		click_Print3PartTicket();
		click_OK_on_Alert();
		BaseUI.waitForElementToBeDisplayed("util_PrintTicketSearchResults_PDF_iFrame", null, null, 20);
	}

	public static Integer return_UpperCountAmount() {
		String batchText = BaseUI
				.getTextFromField(Locator.lookupElement("util_PrintTicketSearchResults_BatchTicket_Header"));

		batchText = batchText.substring(batchText.indexOf("-") + 1, batchText.indexOf(";"));

		return Integer.parseInt(batchText);
	}

	public static void click_PreviewList() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("util_PrintTicketSearchResults_PreviewList"), false, 1000);
		Browser.driver.manage().window().maximize();
	}

	public static void click_ExportList() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_ExportList_Button"));
		Thread.sleep(5000);
	}

	public static void click_FetchMore() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_FetchMore_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void sort_Column_Ascending(String columnName) throws Exception {
		for (Integer i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("util_PrintTicketSearchResults_HeaderElement_WithSortIcon", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("ascending")) {
				BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_HeaderElement_WithSortIcon",
						columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("ascending")) {
				break;
			}
		}
	}

	public static void sort_Column_Descending(String columnName) throws Exception {
		for (Integer i = 0; i < 4; i++) {
			String ascendingValue = BaseUI.get_Attribute_FromField(
					Locator.lookupElement("util_PrintTicketSearchResults_HeaderElement_WithSortIcon", columnName, null),
					"aria-sort");

			if (ascendingValue == null || !ascendingValue.equals("descending")) {
				BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_HeaderElement_WithSortIcon",
						columnName, null));
				Thread.sleep(100);
			} else if (ascendingValue.equals("descending")) {
				break;
			}
		}
	}

	public static void verify_Headers_Appear() {
		for (String header : headers) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("util_PrintTicketSearchResults_HeaderElement", header, null));
		}
	}

	public static void verify_FilterText_ContainsFilter(String filterName, String filterValue) {
		String filterText = BaseUI.getTextFromField(Locator.lookupElement("util_PrintTicketSearchResults_Filters"));

		String expectedText = filterName + "=" + filterValue;
		BaseUI.baseStringPartialCompare("Filter Contains", expectedText, filterText);
	}

	public static void verify_FilterText_Empty() {
		String filterText = BaseUI.getTextFromField(Locator.lookupElement("util_PrintTicketSearchResults_Filters"));

		String expectedText = "Filters:";
		BaseUI.baseStringCompare("Filter", expectedText, filterText);
	}

	public static ArrayList<String> return_ColumnTextList_ByHeader(String headerText) {
		ArrayList<WebElement> columnElementList = Locator
				.lookup_multipleElements("util_PrintTicketSearchResults_TableColumn_ByHeaderText", headerText, null);
		ArrayList<String> columnTextList = new ArrayList<String>();

		for (WebElement columnCell : columnElementList) {
			columnTextList.add(BaseUI.get_NonVisible_TextFromField(columnCell));
		}

		return columnTextList;
	}

	public static void verify_ColumnMatches_ExpectedValue(String columnName, String expectedValue) {

		ArrayList<String> columnTextList = return_ColumnTextList_ByHeader(columnName);

		BaseUI.verify_true_AndLog(columnTextList.size() > 0, "Found column text.", "Could NOT find column text.");

		for (String value : columnTextList) {
			BaseUI.baseStringCompare(columnName, expectedValue, value);
		}

	}

	public static void verify_SearchResults_Controls_AsExpected(Boolean ableToFetchMore) {
		BaseUI.verifyCheckboxStatus(
				Locator.lookupElement("util_PrintTicketSearchResults_SkipExistingBatch_IDs_Checkbox"), false);
		WebElement fetchMoreButton = Locator.lookupElement("util_PrintTicketSearchResults_FetchMore_Button");
		if (ableToFetchMore) {
			Navigation.verify_ButtonEnabled(fetchMoreButton);
		} else {
			Navigation.verify_ButtonDisabled(fetchMoreButton);
		}
		BaseUI.verifyElementAppears(fetchMoreButton);

		// Refresh Button - Our Selenium code is having issues determining
		// whether or not this is displayed, so that validation was left out.
		WebElement refreshButton = Locator.lookupElement("util_PrintTicketSearchResults_Refresh_Button");
		Navigation.verify_ButtonEnabled(refreshButton);

		WebElement clearSelectedButton = Locator.lookupElement("util_PrintTicketSearchResults_ClearSelection_Button");
		Navigation.verify_ButtonDisabled(clearSelectedButton);
		BaseUI.verifyElementAppears(clearSelectedButton);

		WebElement goBackButton = Locator.lookupElement("util_PrintTicketSearchResults_GoBack_Button");
		Navigation.verify_ButtonEnabled(goBackButton);
		BaseUI.verifyElementAppears(goBackButton);

		WebElement updateTicketCountButton = Locator
				.lookupElement("util_PrintTicketSearchResults_UpdateTicketCount_Button");
		Navigation.verify_ButtonDisabled(updateTicketCountButton);
		BaseUI.verifyElementAppears(updateTicketCountButton);

		WebElement printBatchTicketButton = Locator
				.lookupElement("util_PrintTicketSearchResults_PrintBatchTicket_Button");
		Navigation.verify_ButtonDisabled(printBatchTicketButton);
		BaseUI.verifyElementAppears(printBatchTicketButton);

		WebElement print3PartTicketButton = Locator
				.lookupElement("util_PrintTicketSearchResults_Print3PartTicket_Button");
		Navigation.verify_ButtonDisabled(print3PartTicketButton);
		BaseUI.verifyElementAppears(print3PartTicketButton);

		WebElement exportListButton = Locator.lookupElement("util_PrintTicketSearchResults_ExportList_Button");
		Navigation.verify_ButtonEnabled(exportListButton);
		BaseUI.verifyElementAppears(exportListButton);

		WebElement previewListButton = Locator.lookupElement("util_PrintTicketSearchResults_PreviewList");
		Navigation.verify_ButtonEnabled(previewListButton);
		BaseUI.verifyElementAppears(previewListButton);

	}

	public static void click_GoBack_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_GoBack_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_Checkbox_ByIndex(Integer indexToCheck) throws Exception {
		BaseUI.click(
				Locator.lookupElement("util_PrintTicketSearchResults_checkboxByIndex", indexToCheck.toString(), null));
		Thread.sleep(300);

	}

	// XPath indexing starts at 1.
	public static void check_Checkbox_ByIndex(Integer indexToCheck) throws Exception {
		WebElement checkboxStatus_Element = Locator
				.lookupElement("util_PrintTicketSearchResults_checkboxByIndex_Status", indexToCheck.toString(), null);
		String checkboxStatus = BaseUI.get_Attribute_FromField(checkboxStatus_Element, "class");
		if (checkboxStatus != null && checkboxStatus.equals("jqx-checkbox-check-checked")) {
		} else {
			click_Checkbox_ByIndex(indexToCheck);
		}
	}

	public static void uncheck_Checkbox_ByIndex(Integer indexToCheck) throws Exception {
		WebElement checkboxStatus_Element = Locator
				.lookupElement("util_PrintTicketSearchResults_checkboxByIndex_Status", indexToCheck.toString(), null);
		String checkboxStatus = BaseUI.get_Attribute_FromField(checkboxStatus_Element, "class");
		if (checkboxStatus != null && checkboxStatus.equals("jqx-checkbox-check-checked")) {
			click_Checkbox_ByIndex(indexToCheck);
		}
	}

	public static void switch_PDF_Page(Integer pageToPick) throws Exception {
		WebElement pageInputBox = Locator.lookupElement("util_PrintTicketSearchResults_BatchTicketPDF_PageNumberInput");

		// BaseUI.enterText_IntoInputBox(pageInputBox, pageToPick.toString());
		Actions select = new Actions(Browser.driver);
		select.doubleClick(pageInputBox).build().perform();

		Thread.sleep(200);
		BaseUI.enterKey(pageInputBox, Keys.DELETE);
		pageInputBox.sendKeys(pageToPick.toString());

		BaseUI.enterKey(pageInputBox, Keys.ENTER);
		Thread.sleep(300);

	}

	public static TableData return_BatchTicket_PDF_data() throws Exception {
		TableData pdfData = new TableData();
		// BaseUI.switch_ToIframe_ByTagname("frame");
		BaseUI.switch_ToIframe(0);

		String numPages = BaseUI.getTextFromField(
				Locator.lookupElement("util_PrintTicketSearchResults_BatchTicketPDF_Page_NumberOfPages"));
		Integer pageCount = Integer.parseInt(numPages.split("\\s")[1]);

		for (Integer pageNum = 1; pageNum <= pageCount; pageNum++) {
			// BaseUI.switch_ToDefaultContent();
			switch_PDF_Page(pageNum);

			ArrayList<WebElement> clientList = Locator.lookup_multipleElements(
					"util_PrintTicketSearchResults_BatchTicketPDF_ClientAndLockboxCells", pageNum.toString(), null);
			ArrayList<WebElement> batchList = Locator.lookup_multipleElements(
					"util_PrintTicketSearchResults_BatchTicketPDF_Value_ByPage_ByText", pageNum.toString(),
					"Batch ID:");
			ArrayList<WebElement> docGroupList = Locator.lookup_multipleElements(
					"util_PrintTicketSearchResults_BatchTicketPDF_Value_ByPage_ByText", pageNum.toString(),
					"Document Group:");
			ArrayList<WebElement> barCodeList = Locator.lookup_multipleElements(
					"util_PrintTicketSearchResults_BatchTicketPDF_BarCode_ByPage", pageNum.toString(), null);

			ArrayList<WebElement> printList = Locator.lookup_multipleElements(
					"util_PrintTicketSearchResults_BatchTicketPDF_Value_ByPage_ByText", pageNum.toString(),
					"Print Date:");

			for (Integer batchNum = 0; batchNum < clientList.size(); batchNum++) {
				HashMap<String, String> currentRow = new HashMap<String, String>();

				currentRow.put("pageNumber", pageNum.toString());

				// BaseUI.switch_ToIframe_ByTagname("frame");

				String client_And_Lockbox = BaseUI.getTextFromField(clientList.get(batchNum));
				String client = client_And_Lockbox.substring(0, client_And_Lockbox.indexOf(" "));
				client = client.split("\\:")[1];
				currentRow.put("client", client);

				currentRow.put("printDate", BaseUI.getTextFromField(printList.get(batchNum)));
				String lockbox = client_And_Lockbox
						.substring(client_And_Lockbox.indexOf(" "), client_And_Lockbox.length()).trim();
				lockbox = lockbox.split("\\:")[1];
				currentRow.put("lockbox", lockbox);
				currentRow.put("batchID", BaseUI.getTextFromField(batchList.get(batchNum)));
				currentRow.put("docGroup", BaseUI.getTextFromField(docGroupList.get(batchNum)));
				currentRow.put("barCode", BaseUI.getTextFromField(barCodeList.get(batchNum)));
				// util_PrintTicketSearchResults_BatchTicketPDF_Value_ByText_ByIndex
				pdfData.data.add(currentRow);
			}
		}

		return pdfData;
	}

	public static void verify_BatchTicket_PDFEntry_AgainstExpectedResults(HashMap<String, String> rowToCheck,
			String expectedLockbox, String expecteddocGroup, String expectedClient, Integer batchLowerRange,
			Integer batchUpperRange) {

		BaseUI.baseStringCompare("Lockbox", expectedLockbox, rowToCheck.get("lockbox"));
		BaseUI.baseStringCompare("Client", expectedClient, rowToCheck.get("client"));
		BaseUI.baseStringCompare("Doc Group", expecteddocGroup, rowToCheck.get("docGroup"));

		Integer batchIDInt = Integer.parseInt(rowToCheck.get("batchID"));

		BaseUI.verify_true_AndLog(batchLowerRange <= batchIDInt && batchUpperRange >= batchIDInt,
				"Batch ID: " + rowToCheck.get("batchID") + " fell within range of " + batchLowerRange.toString() + "-"
						+ batchUpperRange.toString(),
				"Batch ID: " + rowToCheck.get("batchID") + " did NOT fall within range of " + batchLowerRange.toString()
						+ "-" + batchUpperRange.toString());

		String expectedBarCode = MessageFormat.format("#06019811# !4444$4444! #{0}#", rowToCheck.get("batchID"));
		BaseUI.verify_true_AndLog(expectedBarCode.equals(rowToCheck.get("barCode")),
				"Barcode matched expected value of " + expectedBarCode,
				"Barcode - was expecting " + expectedBarCode + " but saw value " + rowToCheck.get("barCode"));

		String expected_todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMM dd, yyyy");
		BaseUI.verify_true_AndLog(expected_todaysDate.equals(rowToCheck.get("printDate")),
				"Print Date matched " + expected_todaysDate,
				"Expected Print Date " + expected_todaysDate + " Seeing " + rowToCheck.get("printDate"));
	}

	public static void launch_UpdateTicketCount_Modal() {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_UpdateTicketCount_Button"));
		BaseUI.closeAlertIfPresent();
		BaseUI.waitForElementToBeDisplayed("util_PrintTicketSearchResults_UpdateTicketModal", null, null, 20);

	}

	public static void update_TicketCount_TextBox(Integer numberToEnter) throws Exception {
		BaseUI.enterText_IntoInputBox(
				Locator.lookupElement("util_PrintTicketSearchResults_UpdateTicketModal_BatchTicketCount_Textbox"),
				numberToEnter.toString());
		Thread.sleep(100);
		BaseUI.checkCheckbox(
				Locator.lookupElement("util_PrintTicketSearchResults_UpdateTicketModal_SaveBatchTicketCount_Checkbox"));
		Thread.sleep(100);
	}

	public static void update_TicketCount(Integer numberToEnter) throws Exception {
		launch_UpdateTicketCount_Modal();
		update_TicketCount_TextBox(numberToEnter);
		click_OK_UpdateTicketCount_Modal();

	}

	public static void click_OK_UpdateTicketCount_Modal() throws Exception {
		BaseUI.click(Locator.lookupElement("util_PrintTicketSearchResults_UpdateTicketModal_OK_Button"));
		BaseUI.waitForElementToNOTBeDisplayed("util_PrintTicketSearchResults_UpdateTicketModal", null, null, 10);
		Thread.sleep(3000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void verify_TicketCount(Integer indexToCheck, Integer expectedTicketCount) {
		TableData searchResults = new TableData();
		searchResults = return_SearchResults2();
		try {
			BaseUI.verify_true_AndLog(
					searchResults.data.get(0).get("Ticket Count").equals(expectedTicketCount.toString()),
					"Found Ticket Count of " + expectedTicketCount.toString(),
					"Did NOT find Ticket Count of " + expectedTicketCount.toString());
		} finally {
			searchResults = null;
		}
	}

}// End of Class
