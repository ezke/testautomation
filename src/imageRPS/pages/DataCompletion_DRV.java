package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class DataCompletion_DRV {


    public static void click_through_ShowNextMatch() throws Exception {

        int maxCounter = 0;

        while (true) {
            if (maxCounter >= 250) {
                break;
            }

            if(BaseUI.elementAppears(Locator.lookupElement("dataComplete_DRV_ShowNextMatch_Active_Button"))) {
                click_ShowNextMatch_Button();
            }

            if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_DRV_ShowNextMatch_Disabled_Button"))) {
                break;
            }

            maxCounter++;
        }
    }

    //clicks through "Show the Next match" and click "Yes, this is a duplicate" or "This is not a duplicate" until confirm dialog box appears
    public static void click_through_DRV(Boolean buttonToClick) throws Exception {

        int maxCounter = 0;

        while (true) {
            if (maxCounter >= 50) {
                break;
            }

            if(BaseUI.elementAppears(Locator.lookupElement("dataComplete_DRV_ShowNextMatch_Active_Button"))) {
                click_through_ShowNextMatch();
            }
            click_YesDuplicate_Or_NotDuplicate_Button(buttonToClick);
            if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal"))) {
                break;
            }

            maxCounter++;
        }
    }


    public static void click_ShowNextMatch_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("dataComplete_DRV_ShowNextMatch_Active_Button"));
        Thread.sleep(300);
    }

    public static void click_YesDuplicate_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("dataComplete_DRV_YesDuplicate_Button"));
        Thread.sleep(300);
    }

    public static void click_NotDuplicate_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("dataComplete_DRV_NotDuplicate_Button"));
        Thread.sleep(300);
    }

    public static void click_RejectBatch_Button() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("dataComplete_DRV_RejectBatch_Button"));
        Thread.sleep(300);
    }

    public static String return_Current_SeqNumber() {
        String currentSeqValue = BaseUI.getTextFromField(Locator.lookupRequiredElement("dataComplete_DRV_Sequence"));

        return currentSeqValue;
    }

    public static String return_BatchID() {
        String currentBatchValue = BaseUI.getTextFromField(Locator.lookupRequiredElement("dataComplete_DRV_BatchID"));

        return currentBatchValue;
    }

    public static void verify_Seq_Changed(String previousSeq) {

        String currentSeq = return_Current_SeqNumber();
        BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(currentSeq), currentSeq + " was a number.",
                currentSeq + " was NOT a number.");
        BaseUI.verify_true_AndLog(BaseUI.string_isNumeric(previousSeq), previousSeq + " was a number.",
                previousSeq + " was NOT a number.");
        BaseUI.verify_false_AndLog(previousSeq.equals(currentSeq), currentSeq + " does NOT match " + previousSeq,
                currentSeq + " did NOT change.");

    }

    public static void click_YesDuplicate_Or_NotDuplicate_Button(Boolean buttonToClick) throws Exception {
        if (buttonToClick) {
            click_YesDuplicate_Button();
        } else {
            click_NotDuplicate_Button();
        }
    }
}
