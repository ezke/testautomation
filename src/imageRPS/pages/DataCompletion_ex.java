package imageRPS.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class DataCompletion_ex {

	public static void enterValue_IntoInputBox(String valueToEnter, String valueToEnter1, String valueToEnter2,
			String valueToEnter3, String valueToEnter4) throws Exception {

		WebElement BasePayment = Locator.lookupElement("dataComplete_BasePayment_TextBox");
		BaseUI.enterText_IntoInputBox(BasePayment, valueToEnter);
		BaseUI.enterKey(BasePayment, Keys.ENTER);
		Thread.sleep(300);

		WebElement FeesBackInterest = Locator.lookupElement("dataComplete_FeesBackInterest_TextBox");
		BaseUI.enterText_IntoInputBox(FeesBackInterest, valueToEnter1);
		BaseUI.enterKey(FeesBackInterest, Keys.ENTER);
		Thread.sleep(300);

		WebElement Escrow = Locator.lookupElement("dataComplete_Escrow_TextBox");
		BaseUI.enterText_IntoInputBox(Escrow, valueToEnter2);
		BaseUI.enterKey(Escrow, Keys.ENTER);
		Thread.sleep(300);

		WebElement PaycodeInput = Locator.lookupElement("dataComplete_PaycodeInput_TextBox");
		BaseUI.enterText_IntoInputBox(PaycodeInput, valueToEnter3);
		BaseUI.enterKey(PaycodeInput, Keys.ENTER);
		Thread.sleep(300);

		WebElement Donation = Locator.lookupElement("dataComplete_Donation_TextBox");
		BaseUI.enterText_IntoInputBox(Donation, valueToEnter4);
		BaseUI.enterKey(Donation, Keys.ENTER);
		Thread.sleep(300);

	}

	public static void enterValue_AddPrincipa(String AddPrincipa1) throws Exception {

		// BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataComplete_AddPrincipa_TextBox"),
		// AddPrincipa);

		WebElement AddPrincipa = Locator.lookupElement("dataComplete_AddPrincipa_TextBox");
		BaseUI.enterText_IntoInputBox(AddPrincipa, AddPrincipa1);
		Thread.sleep(200);
		//Color changes for a short time frame, cannot reliably run a validation here.
//		BaseUI.verifyElementHasExpected_CSSAttribute(Locator.lookupElement("dataComplete_Differnce"),
//				"background-color", "rgba(0, 136, 0, 1)");
		BaseUI.enterKey(AddPrincipa, Keys.ENTER);
		Thread.sleep(3000);

	}

	public static void key_Through_AllBatch(String basePayment, String feesBackInterest, String escrow,
			String paycodeInput, String donation) throws Exception {

		Integer loopCount = 0;

		while (loopCount < 20) {
			// balance_OneAmount();

			DataCompletion_ex.enterValue_IntoInputBox(basePayment, feesBackInterest, escrow, paycodeInput, donation);
			Thread.sleep(1000);
			String getprincipal = DataCompletion_ex.getValue_FromDifference();
			BaseUI.remove_ExtraSpaces(getprincipal.replaceAll(". ,", " "));
			getprincipal = getprincipal.replace(".", "");
			getprincipal = getprincipal.toString();
			getprincipal = getprincipal.replace(",", "");
			DataCompletion_ex.enterValue_AddPrincipa(getprincipal);
			Thread.sleep(1000);

			if (BaseUI.elementAppears(Locator.lookupElement("dataComplete_Confirm_Modal_Message"))) {
				break;
			}
			loopCount++;
		}

	}

	public static String getValue_FromDifference() {

		return BaseUI.getTextFromField(Locator.lookupElement("dataComplete_Differnce"));

	}

	public static void getValue_FromAppliedAmount() {

		BaseUI.getTextFromField(Locator.lookupElement("dataComplete_AppliedAmount"));
	}

	public static void getValue_FromTotalOtherAmounts() {

		BaseUI.getTextFromField(Locator.lookupElement("dataComplete_TotalOtherAmounts"));
	}

	public static void verify_Balanced() {
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataComplete_BAL_Amount"), "0.00");

		BaseUI.verifyElementHasExpected_CSSAttribute(Locator.lookupElement("dataComplete_BAL_Amount"),
				"background-color", "rgba(0, 136, 0, 1)");

	}

	public static void click_Yes_ToConfirm() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Confirm_YesButton"));
		Thread.sleep(3000);

		Navigation.wait_For_Page_ToLoad();

	}

	public static void click_OK_ToConfirm() throws Exception {
		BaseUI.click(Locator.lookupElement("dataComplete_Done_Alert_OkButton"));
		Thread.sleep(3000);

		Navigation.wait_For_Page_ToLoad();

	}

	public static void verify_Confirm_DialogPresent() {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Dialog"));

		BaseUI.verifyElementHasExpectedText("dataComplete_BAL_Alert_Text",
				"This batch is ready for pp2 and does not require keying.");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BAL_Alert_OK_Button"));
	}

	public static void verify_Alert_DialogPresent() {

		// BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_Confirm_Dialog"));
		BaseUI.verifyElementHasExpectedText("dataComplete_BAL_Alert_Text",
				"This batch is ready for wait and does not require keying.");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_BAL_Alert_OK_Button"));
	}

	public static void wait_ForConfirm_ToAppear() {
		BaseUI.waitForElementToBeDisplayed("dataComplete_Confirm_Dialog", null, null);
	}

	public static void verify_AppliedAmont_Msg_ToAppear() {

		BaseUI.verifyElementAppears(Locator.lookupElement("dataComplete_AppliedAmont_Msg"));

	}

	public static void verify_AppliedAmont_Msg_Not_ToAppear() {
		BaseUI.verifyElementDoesNOTExist("dataComplete_AppliedAmont_Msg", null, null);

	}

}

// End of class
