package imageRPS.pages;

import utils.BaseUI;
import utils.Locator;

public class ReportsOutput_Reformatter {

	public static void verify_Reformatter_TabLoadedSuccessfully() throws Exception {
		ReportsOutput.verify_SubTab_Active("Reformatter");
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("reformatter_NoConsolidations_ErrorModal"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static void clickOK_ForErrorModal() throws Exception {
		BaseUI.click(Locator.lookupElement("reformatter_NoConsolidations_ErrorModal_OKButton"));
		BaseUI.waitForElementToNOTBeDisplayed("reformatter_NoConsolidations_ErrorModal_OKButton", null, null, 5);
	}

	public static void click_OK_OnError_IfExists() throws Exception {
		if (BaseUI.elementAppears(Locator.lookupElement("reformatter_NoConsolidations_ErrorModal"))) {
			ReportsOutput_Reformatter.clickOK_ForErrorModal();
		}

	}

}// end of class
