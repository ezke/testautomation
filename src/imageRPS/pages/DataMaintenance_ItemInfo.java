package imageRPS.pages;

import java.util.ArrayList;
import java.util.HashMap;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class DataMaintenance_ItemInfo {

	// public static void verify_BasicInformation(HashMap<String, String>
	// basicInfo) {
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_BatchID",
	// basicInfo.get("Batch ID"));
	//
	// String appliedAmount = basicInfo.get("Applied Amount");
	// appliedAmount = appliedAmount.replace("$", "");
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_AppliedAmount",
	// appliedAmount);
	//
	// String rejectJob = basicInfo.get("Reject Job");
	// rejectJob = rejectJob.equals("---") ? "--- -" : rejectJob;
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_RejectJob",
	// rejectJob);
	//
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_P1SeqNumber",
	// basicInfo.get("P1 Seq Number"));
	//
	// String tranRejectJob = basicInfo.get("Tran Reject Job");
	// tranRejectJob = tranRejectJob.equals("---") ? "-" : tranRejectJob;
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_TranRejectJob",
	// tranRejectJob);
	//
	// // Don't know what this maps to
	// //
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_SourceP1SeqNumber",
	// // basicInfo.get(""));
	//
	// String docID = basicInfo.get("Doc ID");
	// String newDocID =
	// BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ItemInfo_DocID"));
	// BaseUI.verify_true_AndLog(newDocID.startsWith(docID + " - "),
	// "Doc ID from prior page: " + docID + " found in: " + newDocID,
	// "Doc ID from prior page: " + docID + " NOT found in: " + newDocID);
	//
	// // Could not find a corresponding field on previous page.
	// // BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_InsertFrom",
	// // basicInfo.get(""));
	//
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_TranNumber",
	// basicInfo.get("Tran Number"));
	//
	// // Could not find a corresponding field on previous page.
	// //
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_SourceTranNumber",
	// // basicInfo.get(""));
	//
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_DocType",
	// basicInfo.get("Doc Type Name"));
	//
	// // Item Job and Batch Job have same value. Not sure which one to call
	// // here.
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_CurrentJob",
	// basicInfo.get("Item Job"));
	//
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_TranSeqNumber",
	// basicInfo.get("Tran Seq Number"));
	//
	// // showing as Yes in previous page, blank on this page.
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_BalancingDoc",
	// basicInfo.get("Balancing Doc"));
	//
	// // Not sure what this should map to
	// BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_SourceBatchID",
	// basicInfo.get("Source ID"));
	//
	// }

	public static TableData return_PT4202_ExpectedTestData(String expectedUserName, String batchID) {
		TableData expectedTable = new TableData();
		expectedTable.data.add(new HashMap<String, String>());
		expectedTable.data.add(new HashMap<String, String>());
		expectedTable.data.get(0).put("Sequence", "1");
		expectedTable.data.get(0).put("Type Sequence", "UnSuspend");
		expectedTable.data.get(0).put("Operator ID", expectedUserName);
		expectedTable.data.get(0).put("Keying Job", "afe");
		expectedTable.data.get(0).put("Other", "");
		expectedTable.data.get(1).put("Sequence", "2");
		expectedTable.data.get(1).put("Type Sequence", "Sus" + batchID);
		expectedTable.data.get(1).put("Operator ID", expectedUserName);
		expectedTable.data.get(1).put("Keying Job", "afe");
		expectedTable.data.get(1).put("Other", "");

		return expectedTable;

	}
	
	public static TableData return_PT4178_ExpectedTestData(String expectedUserName, String batchID) {
		 TableData expectedTable = new TableData();
		 expectedTable.data.add(new HashMap<String, String>());
		 expectedTable.data.add(new HashMap<String, String>());
		 expectedTable.data.get(0).put("Sequence", "1");
		 expectedTable.data.get(0).put("Type Sequence", "unReject");
		 expectedTable.data.get(0).put("Operator ID", expectedUserName);
		 expectedTable.data.get(0).put("Keying Job", "afe");
		 expectedTable.data.get(0).put("Other", "");
		 expectedTable.data.get(1).put("Sequence", "2");
		 expectedTable.data.get(1).put("Type Sequence", "Reject");
		 expectedTable.data.get(1).put("Operator ID", expectedUserName);
		 expectedTable.data.get(1).put("Keying Job", "afe");
		 expectedTable.data.get(1).put("Other", "");

		return expectedTable;

	}

	public static void expandSectionHeader_ByText(String linkText) throws Exception {
		WebElement sectionExpander = Locator
				.lookupElement("dataMaint_ItemInfo_ExandableSectionHeader_ElementThatExpands", linkText, null);

		WebElement sectionLink = Locator.lookupElement("dataMaint_ItemInfo_ExandableSectionHeader_Link_ByText",
				linkText, null);

		String expanded = BaseUI.get_Attribute_FromField(sectionExpander, "aria-expanded");

		if (expanded == null || expanded.equals("false")) {
			BaseUI.click(sectionLink);
			BaseUI.waitForElementToContain_AttributeMatch(
					"dataMaint_ItemInfo_ExandableSectionHeader_ElementThatExpands", linkText, null, "aria-expanded",
					"true", 5);
			Thread.sleep(500);
		}

	}

	public static TableData return_ItemAuditTrail_Table() throws Exception {
		String[] headers = { "Sequence", "Type Sequence", "Operator ID", "Keying Job", "Other" };

		TableData auditTable = new TableData();

		// if (Browser.currentBrowser.equals("chrome")) {
		//
		// auditTable = BaseUI.tableExtractorV2("dataMaint_ItemInfo_AuditTrail_Body",
		// headers);
		//
		// } else {
		BaseUI.scroll_to_element(Locator.lookupElement("dataMaint_ItemInfo_AuditTrail_Cells"));
		
		auditTable = BaseUI.tableExtractor_ByCell("dataMaint_ItemInfo_AuditTrail_Cells", headers);
		// }

		auditTable.remove_Character("\\n");
		auditTable.trim_column("Sequence");
		auditTable.trim_column("Type Sequence");
		auditTable.trim_column("Operator ID");
		auditTable.trim_column("Keying Job");
		auditTable.trim_column("Other");
		return auditTable;

	}

	// Might need to add scrolling if we at some point need this for values that
	// are too far over to the right.
	public static Integer return_FirstRowIndex_OfColumn_WithValue(String header, String expectedValue)
			throws Exception {
		ArrayList<WebElement> columnElementList = Locator
				.lookup_multipleElements("dataMaint_ItemInfo_Table_ColumnValues_ByHeaderText", header, null);
		// ArrayList<String> columnList = new ArrayList<String>();
		Integer indexToReturn = null;
		for (Integer i = 0; i < columnElementList.size(); i++) {
			String cellValue = BaseUI.get_NonVisible_TextFromField(columnElementList.get(i));
			if (cellValue.equals(expectedValue)) {
				indexToReturn = i + 1;
				break;
			}
		}

		return indexToReturn;
	}

	// Xpath indexes start at 1.
	public static String return_Value_ByHeader_AndRowIndex(String header, Integer index) {
		String textToReturn = BaseUI.getTextFromField(
				Locator.lookupElement("dataMaint_ItemInfo_TableCell_ByHeader_AndIndex", header, index.toString()));

		return textToReturn;
	}

	public static TableData return_ItemInfoTable() {
		String[] headers = { "Type Sequence", "Field Name", "Field Value", "Audit Info", "Scan Complete", "Field Alias",
				"Zone Id" };

		TableData itemInfo = new TableData();
		// if (Browser.currentBrowser.equals("chrome")) {
		// itemInfo = BaseUI.tableExtractorV2("dataMaint_ItemInfo_Table_Body", headers);
		// } else {
		// itemInfo = BaseUI.tableExtractor_ByCell("dataMaint_ItemInfo_TableCells",
		// headers);
		// }
		itemInfo.data = BaseUI.tableExtractor(headers, "dataMaint_ItemInfo_Table_ColumnValues_ByHeaderText");

		return itemInfo;
	}

	// rowsToCheck should be an array of the Type Sequence values to match up.
	public static void verify_AuditInfo(TableData itemInfoData, String expectedValue, String[] rowsToCheck) {
		for (String rowValueToMatch : rowsToCheck) {
			HashMap<String, String> row = itemInfoData.dataRow("Type Sequence", rowValueToMatch);
			BaseUI.baseStringCompare("Audit Trail", expectedValue, row.get("Audit Info"));
		}
	}

	public static void verify_AuditInfo_AFE(TableData itemInfoData, String[] rowsToCheck) {
		String nameFormatted = GlobalVariables.loginName;
		if (nameFormatted.length() > 8) {
			nameFormatted = nameFormatted.substring(0, 8);
		}

		verify_AuditInfo(itemInfoData, nameFormatted + ",afe,", rowsToCheck);

	}

	// rowsToCheck should be an array of the Type Sequence values to match up.
	public static void verify_FieldValue(TableData itemInfoData, HashMap<String, String> expectedValues) {
		for (String key : expectedValues.keySet()) {
			HashMap<String, String> row = itemInfoData.dataRow("Type Sequence", key);
			BaseUI.baseStringCompare("Field Value", expectedValues.get(key), row.get("Field Value"));
		}
	}

	public static void verify_Field_By_Header_AndIndex(String header, Integer index, String expectedValue) {
		String actualValue = DataMaintenance_ItemInfo.return_Value_ByHeader_AndRowIndex(header, index);
		BaseUI.baseStringCompare(header, expectedValue, actualValue);
	}

	public static void verify_Field_By_Header_AndIndex_PartialMatch(String header, Integer index,
			String expectedValue) {
		String actualValue = DataMaintenance_ItemInfo.return_Value_ByHeader_AndRowIndex(header, index);
		BaseUI.baseStringPartialCompare(header, expectedValue, actualValue);
	}

	public static void verify_CheckImage_Displayed(String batchID) {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_ItemInfo_CheckImage_ByBatch", batchID, null));
	}

	public static void verify_BatchID(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_BatchID", expectedValue);
	}

	public static void verify_AppliedAmount(String expectedValue) {
		String appliedAmount = expectedValue;
		appliedAmount = appliedAmount.replace("$", "");
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_AppliedAmount", appliedAmount);
	}

	public static void verify_RejectJob(String expectedValue) {
		String rejectJob = expectedValue;
		rejectJob = rejectJob.equals("---") ? "--- -" : rejectJob;
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_RejectJob", rejectJob);
	}

	public static void verify_SourceBatchID(String sourceID) {

		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_SourceBatchID", sourceID);
	}

	public static void verify_P1SeqNumber(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_P1SeqNumber", expectedValue);
	}

	// showing as Yes in previous page, blank on this page.
	public static void verify_BalancingDoc(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_BalancingDoc", expectedValue);
	}

	public static void verify_TranRejectJob(String expectedValue) {
		String tranRejectJob = expectedValue;
		tranRejectJob = tranRejectJob.equals("---") ? "-" : tranRejectJob;
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_TranRejectJob", tranRejectJob);
	}

	// Don't know what this maps to
	public static void verify_SourceP1SeqNumber(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_SourceP1SeqNumber", expectedValue);
	}

	public static void verify_DocID(String expectedValue) {
		String docID = expectedValue;
		String newDocID = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ItemInfo_DocID"));
		BaseUI.verify_true_AndLog(newDocID.startsWith(docID + " - "),
				"Doc ID from prior page: " + docID + " found in: " + newDocID,
				"Doc ID from prior page: " + docID + " NOT found in: " + newDocID);
	}

	public static void verify_TranNumber(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_TranNumber", expectedValue);
	}

	public static void verify_DocTypeName(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_DocType", expectedValue);
	}

	// Item Job and Batch Job have same value. Not sure which one to call here.
	public static void verify_CurrentJob(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_CurrentJob", expectedValue);
	}

	// Item Job and Batch Job have same value. Not sure which one to call here.
	public static void verify_CurrentJob_NOT_OldJob(String expectedValue) {

		String currentJobText = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_ItemInfo_CurrentJob"));

		BaseUI.verify_false_AndLog(BaseUI.stringEmpty(currentJobText) && currentJobText.length() == 3,
				"Found current job.", "Did NOT find current job.");

		BaseUI.verifyElementDoesNotHaveExpectedText("dataMaint_ItemInfo_CurrentJob", expectedValue);
	}

	public static void verify_TranSeqNumber(String expectedValue) {
		BaseUI.verifyElementHasExpectedText("dataMaint_ItemInfo_TranSeqNumber", expectedValue);
	}

	public static void click_GoBack() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_GoBack_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

}// End of Class
