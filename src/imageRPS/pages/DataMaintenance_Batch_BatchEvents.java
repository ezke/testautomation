package imageRPS.pages;

import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import utils.*;

import java.util.ArrayList;
import java.util.HashMap;

public class DataMaintenance_Batch_BatchEvents {

    static String[] header_BatchEvents = {"Batch ID", "Client ID", "Source ID", "Tran ID", "Event Date", "Event Seq",
            "Event Process", "Event Type", "Event Result", "Event By", "Event Station ID", "Event Data1", "Event Data2"};


    public static TableData return_TableRows_ViaColumn() throws Exception {

        TableData tableRows = new TableData();

        tableRows.data = table_Extractor(header_BatchEvents, "dataMaint_BatchEvents_ColumnValue_ByHeaderText");

        return tableRows;
    }

    public static ArrayList<HashMap<String, String>> table_Extractor(String[] headerArray, String elementLocator_byHeaderText) throws Exception {
        ArrayList<HashMap<String, String>> table_Values = new ArrayList<HashMap<String, String>>();

        for (String header : headerArray) {

            scroll_Horizontally_Until_CellVisible_ByHeader(header);

            ArrayList<WebElement> columnCellList = Locator.lookup_multipleElements(elementLocator_byHeaderText, header,
                    null);

            for (int i = 0; i < columnCellList.size(); i++) {
                HashMap<String, String> tableRow = null;

                if (table_Values.size() == 0 || table_Values.size() <= i) {
                    tableRow = new HashMap<String, String>();
                    tableRow.put(header, BaseUI.getTextFromField(columnCellList.get(i)));
                    table_Values.add(tableRow);
                } else {
                    table_Values.get(i).put(header, BaseUI.getTextFromField(columnCellList.get(i)));
                }
            }

        }

        return table_Values;

    }

    public static void scroll_Horizontally_Until_CellVisible_ByHeader(String headerText) throws Exception {

        int maxCount = 0;

        while (!BaseUI.elementAppears(Locator.lookupElement("dataMaint_BatchEvents_ColumnValue_ByHeaderText", headerText, null))) {
            if (maxCount > 50) {
                break;
            }
            BaseUI.click_js(Locator.lookupElement("dataMaint_BatchEvents_HorizontalScrollBar_MoveRight"));
            BaseUI.click_js(Locator.lookupElement("dataMaint_BatchEvents_HorizontalScrollBar_MoveRight"));
            BaseUI.click_js(Locator.lookupElement("dataMaint_BatchEvents_HorizontalScrollBar_MoveRight"));
            BaseUI.click_js(Locator.lookupElement("dataMaint_BatchEvents_HorizontalScrollBar_MoveRight"));

            maxCount++;
        }
    }

    public static void click_ExportList() throws Exception {
        BaseUI.click(Locator.lookupElement("dataMaint_BatchEvents_ExportList_Button"));
        Thread.sleep(5000);
    }

    public static void click_GoBack() throws Exception {
        BaseUI.click(Locator.lookupElement("dataMaint_BatchEvents_GoBack_Button"));
        Thread.sleep(2000);
    }

    // This will also delete any prior existing csv files from Downloads Folder.
    public static TableData return_ExportList_TableData() throws Exception {
        String downloadsFolder = Browser.downloadLocation;

        String downloadFolderLog = "Download folder set to " + downloadsFolder;
        System.out.println(downloadFolderLog);
        Reporter.log(downloadFolderLog);

        TableData exportResults = new TableData();

        DataBuilder.delete_Files_WithExtention_FromLocation(".csv", downloadsFolder);
        click_ExportList();

        String locate_File = DataBuilder.return_File_WithExtension(".csv", downloadsFolder);
        String locatedFileLog = "Located File " + locate_File;
        System.out.println(locatedFileLog);
        Reporter.log(locatedFileLog);

        exportResults = DataBuilder.returnTableData_ForComparison(locate_File, ",", false);
        exportResults.remove_Character_FromKeys("\"");
        exportResults.remove_Character("\"");
        exportResults.replace_Character("\r", " ", "Event Data1");
        exportResults.replace_Character("  ", " ", "Event Data1");
        return exportResults;
    }
}
