package imageRPS.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class LoginPage {

	public static void login(String entityName, String login, String password) throws Exception {
		login_DontNavigatePastPriorSession(entityName, login, password);
		navigate_past_Preexisting_SessionMessage();
	}

	public static void navigate_past_Preexisting_SessionMessage() throws Exception {
		if (BaseUI.pageSourceContainsString(
				"Click here to abort the existing session and start a new ImageRPS Session.")) {
			BaseUI.click(Locator.lookupElement("loginPage_ExistingSession_ExitPreviousSessionLink"));
			Thread.sleep(2000);
		}

		wait_homepage_Loaded();
	}

	public static void click_ExistingSession_StartNewSession(String headerToCheck) throws Exception {
		BaseUI.click(Locator.lookupElement("loginPage_ExistingSession_StartNewSession"));
		BaseUI.waitForElementToBeDisplayed("navigation_Link_ByText", headerToCheck, null);
		Thread.sleep(1500);
	}


	public static void wait_homepage_Loaded() throws Exception {

		BaseUI.waitForElementToBeDisplayed("navigation_ActiveTabLink", null, null, 30);

		if (BaseUI.getTextFromField(Locator.lookupElement("navigation_ActiveTabLink")).equals("Dashboard")) {
			BaseUI.waitForElementToBeDisplayed("dshbrd_Preferences_AlertCountGraph_Checkbox", null, null,55);
			BaseUI.waitForElementToBeDisplayed("dshbrd_Preferences_ItemKeyingCountGraph_Checkbox", null, null);
			BaseUI.waitForElementToBeDisplayed("dshbrd_Preferences_ConsolidationsByCutoffTimeGraph_Checkbox", null, null);
			if (BaseUI.checkBoxStatus("dshbrd_Preferences_AlertCountGraph_Checkbox"))  {
				BaseUI.waitForElementToBeDisplayed("dshbrd_AlertsChart_ChartPart", null, null,55);
			}
			if (BaseUI.checkBoxStatus("dshbrd_Preferences_ItemKeyingCountGraph_Checkbox"))  {
				BaseUI.waitForElementToBeDisplayed("dshbrd_ItemKeyingCountChart_ChartPart", null, null);
			}
			if (BaseUI.checkBoxStatus("dshbrd_Preferences_ConsolidationsByCutoffTimeGraph_Checkbox"))  {
				BaseUI.waitForElementToBeDisplayed("dshbrd_ConsolidationsChart_ChartPart", null, null);
			}
			if (BaseUI.elementExists("navigation_Command_TextBox", null, null)) {
				BaseUI.waitForElementToBeClickable("navigation_Command_TextBox", null, null);
			}
		}
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void login_DontNavigatePastPriorSession(String entityName, String login, String password)
			throws Exception {
		// This will allow pages to finish loading before doing things.
		Browser.set_ImplicitWait_AndSaveValue(0);
		Browser.driver.manage().window().maximize();

		Thread.sleep(500);
		Navigation.navigate_Past_CertificateError();

		WebElement entityTextBox = Locator.lookupElement("loginPage_txt_EntityName");
		BaseUI.click(entityTextBox);
		Thread.sleep(200);
		BaseUI.log_Status("Entering text into Entity Textbox.");
		entityTextBox.sendKeys(entityName);

		WebElement userNameTextBox = Locator.lookupElement("loginPage_txt_UserName");
		BaseUI.click(userNameTextBox);
		Thread.sleep(200);
		BaseUI.log_Status("Entering text into User Name Textbox.");
		userNameTextBox.sendKeys(login);

		if (BaseUI.elementAppears(Locator.lookupElement("loginPage_txt_Password_masked"))) {
			BaseUI.tabThroughField(Locator.lookupElement("LoginPage_txt_UserName"));
			WebElement textField = Browser.driver.switchTo().activeElement();
			Thread.sleep(100);
			BaseUI.log_Status("Entering text into Password Textbox.");
			textField.sendKeys(password);
		} else {
			BaseUI.enterText(Locator.lookupElement("loginPage_txt_Password"), password);
		}

		if(Browser.currentBrowser.equals("internetexplorer")){
			BaseUI.click_js(Locator.lookupElement("loginPage_btn_SignIn"));
		}else{
			BaseUI.click(Locator.lookupElement("loginPage_btn_SignIn"));
		}
		Thread.sleep(500);
		// BaseUI.wait_forPageToFinishLoading();
		BaseUI.waitForElementToBeDisplayed("login_ElemenToLookForOnLogin", null, null, 55);
		// Fail test and don't go any further if we don't see this element. Means the
		// login was unsuccessful.
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("login_UnableToLogIn_Error"));
	}

	public static void verify_NewSession() throws Exception {

		// String apptime2 =
		// BaseUI.getTextFromField(Locator.lookupElement("loginPage_presentTime_Session2"));
		// String apptime2_formated =
		// BaseUI.getDate_WithFormat_X_Days_FromInitialString(apptime2,
		// "hh:mm:ss", "hh:mm", 1);
		verify_NewSessionLinksAre_Present();

	}

	public static void verify_SecoundBrowserMsg() throws Exception {

		// Thread.sleep(20000);
		String apptime1 = BaseUI.getTextFromField(Locator.lookupElement("loginPage_presentTime_ExistingSessionEnd"));
		// String apptime1_formated = apptime1.substring(0, apptime1.indexOf("
		// ")).trim();
		String apptime1_formated = BaseUI.getDate_WithFormat_X_Days_FromInitialString(apptime1, "hh:mm:ss a", "hh:mm a",
				0);
		apptime1_formated = BaseUI.getTodaysDate_AsString() + " " + apptime1_formated;
		BaseUI.verify_Date_IsBetween_DateRange(
				BaseUI.getTodaysDate_AsString() + " "
						+ BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(-1, "hh:mm a"),
				BaseUI.getTodaysDate_AsString() + " "
						+ BaseUI.getTimeAsString_InRelationToCurrentTime_ByMinutes(1, "hh:mm a"),
				apptime1_formated, "MM/dd/yyyy hh:mm a");

		// BaseUI.baseStringPartialCompare("loginPage_presentTime_ExistingSessionEnd",
		// apptime1_formated,
		// apptime2_formated);
		// validating second does not match
		// BaseUI.baseStringCompareStringsAreDifferent("loginPage_presentTime_ExistingSessionEnd",
		// apptime1, apptime2);
		verify_AbortThe_ExistingSessionLinksAre_Present();
	}

	public static void verify_NewSessionLinksAre_Present() {

		BaseUI.pageSourceContainsString("Click here to abort the existing session and start a new ImageRPS Session.");
		BaseUI.verifyElementAppears(Locator.lookupElement("navigation_SessionEnded_Title"));
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_ExistingSession_StartNewSession"));
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_ExistingSession_StartNewSession_Operator"));

	}

	public static void verify_AbortThe_ExistingSessionLinksAre_Present() {

		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_ExistingSession_SessionEnded"));
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_presentTime_ExistingSessionEnd"));
		// BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_ExistingSession_StartNewSession_HasBeenEnded"));
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPage_ExistingSession_StartNewSession"));

	}

	public static void clearLoginFields() throws Exception {
		WebElement entityNametextField = Locator.lookupElement("loginPage_txt_EntityName");
		entityNametextField.sendKeys(Keys.CONTROL + "a");
		entityNametextField.sendKeys(Keys.DELETE);
		BaseUI.tabThroughField(Locator.lookupElement("loginPage_txt_EntityName"));
		WebElement userNameTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText_IntoInputBox(userNameTextField, "");
		BaseUI.tabThroughField(Locator.lookupElement("loginPage_txt_UserName"));
		WebElement passwordTextField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(passwordTextField, "");

	}

	public static void clearPassword() throws Exception {
		BaseUI.tabThroughField(Locator.lookupElement("loginPage_txt_UserName"));
		WebElement textField = Browser.driver.switchTo().activeElement();
		BaseUI.enterText(textField, "");

	}

	public static void verifyLoginPageError() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageErrorvalidation"));
	}

	public static void verifyLoginPageErrorDoesNotAppear() {
		BaseUI.verifyElementDisabled(Locator.lookupElement("loginPageErrorvalidation"));
	}

	public static void changePassword(String oldPassword, String newPassword) throws Exception {
		BaseUI.enterText(Locator.lookupElement("loginPageOldPasswordText"), oldPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageNewPasswordText"), newPassword);
		BaseUI.enterText(Locator.lookupElement("loginPageConfirmPasswordText"), newPassword);
		BaseUI.tabThroughField(Locator.lookupElement("loginPageConfirmPasswordText"));
		Thread.sleep(3000);

	}

	public static void clickChangePasswordSaveButton() {
		BaseUI.click(Locator.lookupElement("loginPageChangePasswordSaveButton"));
	}

	public static void clickChangePasswordCancelButton() {
		BaseUI.click(Locator.lookupElement("loginPageChangePasswordCancelButton"));
	}

	public static void verifyLoginPageChangePasswordConfirmMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageChangePasswordConfirmMessage"));
	}

	public static void verifyLoginPageChangePasswordErrorMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageChangePasswordErrorMessage"));
	}

	public static void clickForgotPasswordLink() throws Exception {
		if (Browser.currentBrowser.equals("internetexplorer")) {
			BaseUI.click(Locator.lookupElement("loginPageForgotPasswordLink"));
			Thread.sleep(2000);
		} else {
			BaseUI.click(BaseUI.waitForElementToBeClickable("loginPageForgotPasswordLink", null, null));
			Thread.sleep(2000);
		}
	}

	public static void resetForgotPasswordForm(String entity, String userName, String emailSubjectLine)
			throws InterruptedException {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEntityText"), entity);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordLoginNameText"), userName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEmailSubjectLineText"),
				emailSubjectLine);
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordSubmitButton"));
		Thread.sleep(2000);
	}

	public static void verifyForgotPasswordModalAppears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPasswordModal"));
	}

	public static void enterForgotPasswordEntity(String valueToSet) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEntityText"), valueToSet);
	}

	public static void enterForgotPasswordLoginName(String valueToSet) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordLoginNameText"), valueToSet);
	}

	public static void enterForgotPasswordEmailSubjectLine(String valueToSet) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("loginPageForgotPasswordEmailSubjectLineText"), valueToSet);
	}

	public static void clickForgotPasswordSubmitButton() {
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordSubmitButton"));
	}

	public static void clickForgotPasswordCancelButton() {
		BaseUI.click(Locator.lookupElement("loginPageForgotPasswordCancelButton"));
	}

	public static void verifyForgotPasswordEmailConfirmationMessage() {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPasswordEmailConfirmationMessage"));
	}

	public static void verifyForgotPasswordErrorMesage(String expectedValue) {
		BaseUI.verifyElementAppears(Locator.lookupElement("loginPageForgotPaswordErrorMessage"));
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("loginPageForgotPaswordErrorMessage"), expectedValue);
	}

    public static void verifyLoginPageMainMessage() {
        BaseUI.verifyElementAppears(Locator.lookupElement("loginPageMainMessage"));
    }

}// end of class
