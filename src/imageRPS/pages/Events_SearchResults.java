package imageRPS.pages;

import org.jetbrains.annotations.NotNull;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

import java.util.*;

public class Events_SearchResults {

    public static String timeFormat = "MM/dd/yyyy h:mm:ss a";

    public static TableData return_SearchResults() {

        String[] tableHeaders = {"Sequence", "Occurred Time", "Severity", "Station Id", "Operator Id", "Message"};

        TableData searchResults = new TableData();

        if (Browser.currentBrowser.equals("chrome")) {
            searchResults = BaseUI.tableExtractorV2("eventsResults_SearchResults_TableBody", tableHeaders);
        } else {
            searchResults = BaseUI.tableExtractor_ByCell("eventsResults_SearchResults_TableCells", tableHeaders);
        }

        return searchResults;
    }

    public static void wait_For_RangeToPopulate(Integer timeToWait) {
        BaseUI.waitForElementToContain_PartialAttributeMatch("eventsResults_Range1Text", null, null, "innerText",
                BaseUI.getDateAsString_InRelationToTodaysDate(0, "MM/dd/yyyy"), timeToWait);
    }

    public static void validate_EventRow(Integer rowToEvaluate, TableData expectedData, TableData searchResultData,
                                         String firstDate, String secondDate) throws Exception {
        String eventTime = searchResultData.data.get(rowToEvaluate).get("Occurred Time");
        BaseUI.verify_Date_IsBetween_DateRange(firstDate, secondDate, eventTime, timeFormat);
        BaseUI.baseStringCompare("Message", expectedData.data.get(rowToEvaluate).get("Message"),
                searchResultData.data.get(rowToEvaluate).get("Message"));
        BaseUI.baseStringCompare("Station Id", expectedData.data.get(rowToEvaluate).get("Station Id"),
                searchResultData.data.get(rowToEvaluate).get("Station Id"));
        BaseUI.baseStringCompare("Severity", expectedData.data.get(rowToEvaluate).get("Severity"),
                searchResultData.data.get(rowToEvaluate).get("Severity"));
        BaseUI.baseStringCompare("Operator Id", expectedData.data.get(rowToEvaluate).get("Operator Id"),
                searchResultData.data.get(rowToEvaluate).get("Operator Id"));
    }

    public static void select_FirstRow() throws Exception {
        BaseUI.click(Locator.lookupElement("eventsResults_SearchResults_FirstCell"));
        Thread.sleep(500);
    }

    public static ArrayList<String> get_RowData_ByIndex(Integer rowToEvaluate, TableData searchResultData) {
        ArrayList<String> rowData = new ArrayList<String>();
        rowData.add(searchResultData.data.get(rowToEvaluate).get("Sequence"));
        rowData.add(searchResultData.data.get(rowToEvaluate).get("Occurred Time"));
        rowData.add(searchResultData.data.get(rowToEvaluate).get("Severity"));
        rowData.add(searchResultData.data.get(rowToEvaluate).get("Station Id"));
        rowData.add(searchResultData.data.get(rowToEvaluate).get("Operator Id"));
        rowData.add(searchResultData.data.get(rowToEvaluate).get("Message"));
        return rowData;
    }

    //rowdata acquired through (Events_SearchResults.get_RowData_ByIndex(0, searchResults));
    public static void validate_Events_Details(ArrayList<String> rowData) {

        //don't pass table data, pass the arraylist of tabledata from the test istself'
        String sequence = rowData.get(0);
        String severity = rowData.get(2);
        String stationId = rowData.get(3);
        String operatorId = rowData.get(4);
        String message = rowData.get(5);

        String details_SequenceText = (Locator.lookupElement("eventDetails_HeaderSequenceNumber").getAttribute("innerText"));
        String details_Severity = (Locator.lookupElement("eventDetails_Severity").getAttribute("innerText"));
        String details_StationID = (Locator.lookupElement("eventDetails_StationID").getAttribute("innerText"));
        String details_OperatorID = (Locator.lookupElement("eventDetails_OperatorID").getAttribute("innerText"));
        String details_Message = (Locator.lookupElement("eventDetails_Message").getAttribute("innerText"));
        BaseUI.baseStringCompare("Events Header Sequence", sequence, details_SequenceText);
        BaseUI.baseStringCompare("Severity", severity, details_Severity);
        BaseUI.baseStringCompare("Station ID", stationId, details_StationID);
        BaseUI.baseStringCompare("Operator ID", operatorId, details_OperatorID);
        BaseUI.baseStringCompare("Message", message, details_Message);
    }
}// End of Class
