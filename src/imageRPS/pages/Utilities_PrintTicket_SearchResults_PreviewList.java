package imageRPS.pages;

import java.util.HashMap;

import utils.BaseUI;
import utils.TableData;

public class Utilities_PrintTicket_SearchResults_PreviewList {

	public static TableData return_SearchResults_Top10Results() {
		HashMap<String, String> tableCellMappings = new HashMap<String, String>();
		tableCellMappings.put("Client ID", ".//td[1]");
		tableCellMappings.put("Lockbox ID", ".//td[2]");
		tableCellMappings.put("Client Name", ".//td[3]");
		tableCellMappings.put("Doc Group", ".//td[4]");
		tableCellMappings.put("DocGroup Name", ".//td[5]");
		tableCellMappings.put("Work Type", ".//td[6]");
		tableCellMappings.put("Work Type Description", ".//td[7]");
		tableCellMappings.put("Ticket Count", ".//td[8]");
		tableCellMappings.put("Ticket", ".//td[9]");

		TableData searchResults = new TableData();
		searchResults.data = BaseUI.tableExtractor(10, "util_PrintTicket_PreviewList_TableRows", tableCellMappings);

		return searchResults;
	}

	public static TableData return_SearchResults2() {
		String[] headers = { "Client ID", "Lockbox ID", "Client Name", "Doc Group", "DocGroup Name", "Work Type",
				"Work Type Description", "Ticket Count", "Ticket" };

		TableData searchResults = new TableData();
		searchResults.data = BaseUI.tableExtractor(headers, "util_PrintTicket_PreviewList_TableColumn_ByHeaderText");

		return searchResults;

	}
	
	
	
}//End of Class
