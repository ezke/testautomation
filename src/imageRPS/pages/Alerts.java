package imageRPS.pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class Alerts {

	public static String alertsTimeFormat = "MM/dd/yyyy HH:mm:ss.SSS";
	public static String eventsListFormat = "MM/dd/yyy h:mm:ss a";

	public static void verify_Alerts_SubTabActive() throws Exception {
		AlertsEvents.verify_SubTab_Active("Alerts");
		BaseUI.verifyElementAppears(Locator.lookupElement("alerts_Header"));
		Navigation.verify_InternalServiceErrorNotPresent();
	}

	public static TableData return_AlertList_Table() {
		TableData alertsListTable;
		String[] headers = { "Id", "Alert", "Category", "Level", "Alert Time", "Alert Current", "Alert Threshold",
				"Warning Time", "Warning Current", "Warning Threshold", "Average", "Max Trigger", "Message" };

		alertsListTable = BaseUI.tableExtractor_ByCell("alerts_AlertListTable_TableCells", headers);

		return alertsListTable;
	}

	public static void verify_alerts_FirstRow(TableData alertList, Integer id, String alert, String category,
			String level, String alertTime, Double alertCurrent, Double alertThreshold, String warningTime,
			Double warningCurrent, Double warningThreshold, Integer average, Boolean maxTrigger, String message)
			throws Exception {
		HashMap<String, String> firstRow = alertList.data.get(0);
		BaseUI.verify_true_AndLog(id == Integer.parseInt(firstRow.get("Id")), "Id matched value of " + id.toString(),
				"Expected Id of " + id.toString() + ". But seeing value " + firstRow.get("Id"));
		BaseUI.baseStringCompare("Alert", alert, firstRow.get("Alert"));
		BaseUI.baseStringCompare("Category", category, firstRow.get("Category"));
		BaseUI.baseStringCompare("Level", level, firstRow.get("Level"));

		// Need to change Alert Time to use range comparison.
		verify_Dates(alertTime, firstRow.get("Alert Time"), alertsTimeFormat);

		BaseUI.verify_true_AndLog(alertCurrent == Double.parseDouble(firstRow.get("Alert Current")),
				"Alert Current matched value of " + alertCurrent.toString(), "Expected Alert Current of "
						+ alertCurrent.toString() + ". But seeing value " + firstRow.get("Alert Current"));
		BaseUI.verify_true_AndLog(alertThreshold.equals(Double.parseDouble(firstRow.get("Alert Threshold"))),
				"Alert Threshold matched value of " + alertThreshold.toString(), "Expected Alert Threshold of "
						+ alertThreshold.toString() + ". But seeing value " + firstRow.get("Alert Threshold"));

		// Need to change Warning Time to use range comparison.
		verify_Dates(warningTime, firstRow.get("Warning Time"), alertsTimeFormat);

		BaseUI.verify_true_AndLog(warningCurrent.equals(Double.parseDouble(firstRow.get("Warning Current"))),
				"Warning Current matched value of " + warningCurrent.toString(), "Expected Warning Current of "
						+ warningCurrent.toString() + ". But seeing value " + firstRow.get("Warning Current"));
		BaseUI.verify_true_AndLog(warningThreshold == Double.parseDouble(firstRow.get("Warning Threshold")),
				"Warning Threshold matched value of " + warningThreshold.toString(), "Expected Warning Threshold of "
						+ warningThreshold.toString() + ". But seeing value " + firstRow.get("Warning Threshold"));
		BaseUI.verify_true_AndLog(average == Integer.parseInt(firstRow.get("Average")),
				"Average matched value of " + average.toString(),
				"Expected Average of " + average.toString() + ". But seeing value " + firstRow.get("Average"));
		BaseUI.baseStringCompare("Max Trigger", maxTrigger.toString(), firstRow.get("Max Trigger"));
		BaseUI.baseStringCompare("Message", message, firstRow.get("Message"));
	}

	public static void verify_Dates(String expectedDate, String actualDate, String format) throws Exception {
		if (expectedDate.equals("")) {
			BaseUI.baseStringCompare("Date Field", expectedDate, actualDate);
		} else {
			String firstDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(expectedDate, format, format, -1);
			String secondDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(expectedDate, format, format, 4);

			BaseUI.verify_Date_IsBetween_DateRange(firstDate, secondDate, actualDate.trim(), format);
		}
	}

	// Xpath indexing starts at 1.
	public static void verify_Level_Alert_ColorRED_ByIndex(Integer rowToCheck) {
		String expectedColor = "rgba(255, 192, 192, 1)";

		BaseUI.verifyElementHasExpected_CSSAttribute(
				Locator.lookupElement("alerts_AlertListTable_AlertCell_ByIndex", rowToCheck.toString(), null),
				"background-color", expectedColor);

	}

	public static void click_RelatedEvents_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_RelatedEvents_Button"));
		BaseUI.waitForElementToBeDisplayed("alerts_EventsList_Title", null, null);
		BaseUI.waitForElementToBeDisplayed("alerts_EventsList_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static TableData return_EventsList_TableData() {
		TableData eventsListTable = new TableData();

		String[] headers = { "Sequence", "Occurred Time", "Severity", "Station Id", "Operator Id", "Message" };

		eventsListTable = BaseUI.tableExtractor_ByCell("alerts_EventsList_Cells", headers);

		return eventsListTable;
	}

	public static void select_AlertsListTable_FirstRow() throws Exception {
		BaseUI.click(Locator.lookupElement("alerts_AlertListTable_FirstCell"));
		Thread.sleep(200);

	}

	public static void verify_EventsList_Table_FirstRow(TableData tableExtract, String expectedOccurredTime,
			String expectedMessage) throws Exception {

		String eventsListFormat = "MM/dd/yyy h:mm:ss a";

		HashMap<String, String> rowToCheck = tableExtract.data.get(0);

		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(rowToCheck.get("Sequence")), "Sequence was an Integer",
				"Sequence " + rowToCheck.get("Sequence") + " was NOT an Integer.");

		verify_Dates(expectedOccurredTime, rowToCheck.get("Occurred Time"), eventsListFormat);

		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(rowToCheck.get("Severity")), "Severity was an Integer",
				"Severity " + rowToCheck.get("Severity") + " was NOT an Integer.");

		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(rowToCheck.get("Station Id")), "Station Id was an Integer",
				"Station Id " + rowToCheck.get("Station Id") + " was NOT an Integer.");


		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(rowToCheck.get("Operator Id").trim()),
				"Operator Id was not an Empty String",
				"Operator Id " + rowToCheck.get("Operator Id") + " was an empty String.");

		BaseUI.baseStringCompare("Message", expectedMessage, rowToCheck.get("Message"));
	}

	public static void verify_SessionTimeOut_Alert() throws Exception {

		BaseUI.waitForElementToBeDisplayed("loginPage_SessionTimeout", "", "Session Expiring", 600);
		WebElement divElement = Locator.lookupElement("loginPage_SessionTimeout");

		if (divElement != null) {
			String style = BaseUI.get_Attribute_FromField(divElement, "style");
			BaseUI.verify_true_AndLog(style.contains("display: block"), "Popup appeared", "Popup did not appear");
			}
		else
			{
			BaseUI.verify_true_AndLog(false, "", "div missing");
		    }
	}

	public static void verify_SessionTimeOut_Alert(String element) throws Exception {

		BaseUI.waitForElementToBeDisplayed(element, "", "Session Expiring", 600);
		WebElement divElement = Locator.lookupElement(element);

		if (divElement != null) {
			String style = BaseUI.get_Attribute_FromField(divElement, "style");
			BaseUI.verify_true_AndLog(style.contains("display: block"), "Popup appeared", "Popup did not appear");
		}
		else
		{
			BaseUI.verify_true_AndLog(false, "", "div missing");
		}
	}

}// End of Class
