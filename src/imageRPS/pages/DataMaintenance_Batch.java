package imageRPS.pages;

import org.openqa.selenium.WebElement;

import imageRPS.pages.Utilities_PrintTicket_BatchTicketFilter.WorkType;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.time.Duration;

public class DataMaintenance_Batch {

	static String[] non_Collapsible_Sections = { "Basic Information", "Pass1 Capture Information", "Count Information",
			"Job List" };

	// String[] collapsible_Sections = {"Pass2 Information", "After Pass2
	// Status", "Batch Status", "Batch Audit Trail", "Misc Information"};

	public enum collapsible_SectionName {
		Pass2Info("Pass2 Information"), AfterPass2Status("After Pass2 Status"), BatchStatus(
				"Batch Status"), BatchAuditTrail("Batch Audit Trail"), MiscInformation("Misc Information");

		private String value;

		private collapsible_SectionName(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	public static void click_KeyBatch() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_KeyBatch_Button"));
		Thread.sleep(1000);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void click_ViewItems() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_SearchResults_ViewItems_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_ViewItems_FirstCellOfFields", null, null, 20);
		Thread.sleep(500);
//		Thread.sleep(1500);
//		Navigation.wait_For_Page_ToLoad();

	}

	public static void expand_All() throws Exception {
		WebElement expand_collapse_Button = Locator.lookupElement("dataMaint_Batch_ExpandAll_CollapseAll_Button");
		String buttonText = BaseUI.getTextFromField(expand_collapse_Button);
		if (buttonText.equals("Expand All")) {
			click_ExpandAll_CollapseAll_Button();
		}
	}

	public static void click_ExpandAll_CollapseAll_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Batch_ExpandAll_CollapseAll_Button"));
		Thread.sleep(1000);

	}

	public static void collapse_All() throws Exception {
		WebElement expand_collapse_Button = Locator.lookupElement("dataMaint_Batch_ExpandAll_CollapseAll_Button");
		String buttonText = BaseUI.getTextFromField(expand_collapse_Button);
		if (buttonText.equals("Collapse All")) {
			click_ExpandAll_CollapseAll_Button();
		}
	}

	public static void verify_Non_Collapsible_Sections_Displayed() {
		for (String section : non_Collapsible_Sections) {
			BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_Batch_SectionHeader_ByText", section, null));
		}
	}

	public static void verify_Collapsible_Sections_NOT_Displayed() {
		for (collapsible_SectionName section : collapsible_SectionName.values()) {
			BaseUI.verifyElementDoesNotAppear(
					Locator.lookupElement("dataMaint_Batch_SectionHeader_ByText", section.getValue(), null));
		}
	}

	public static void verify_Collapsible_Sections_Displayed() {
		for (collapsible_SectionName section : collapsible_SectionName.values()) {
			BaseUI.verifyElementAppears(
					Locator.lookupElement("dataMaint_Batch_SectionHeader_ByText", section.getValue(), null));
		}
	}

	public static void collapse_Section(collapsible_SectionName section) throws Exception {
		if (BaseUI.elementAppears(
				Locator.lookupElement("dataMaint_Batch_collapsibleSection_ByText", section.getValue(), null))) {
			click_collapsible_SectionHeader(section);
			BaseUI.waitForElementToNOTBeDisplayed("dataMaint_Batch_collapsibleSection_ByText", section.getValue(),
					null);
		}
	}

	public static void expand_Section(collapsible_SectionName section) throws Exception {
		if (!BaseUI.elementAppears(
				Locator.lookupElement("dataMaint_Batch_collapsibleSection_ByText", section.getValue(), null))) {
			click_collapsible_SectionHeader(section);
			BaseUI.waitForElementToBeDisplayed("dataMaint_Batch_collapsibleSection_ByText", section.getValue(), null);
		}
	}

	public static void verify_On_BatchInformation_Page() {
		BaseUI.verifyElementAppears(Locator.lookupElement("dataMaint_Batch_BatchInfo_Header"));
	}

	public static void verify_DocGroupID(String docGroupID) {
		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Batch_DocGroupID_Value"), docGroupID);

	}

	public static void verify_WorkType(WorkType worktype) {

		String valueToUse = worktype.getValue();
		valueToUse = valueToUse.equals("Single") ? "Singles" : valueToUse;

		BaseUI.verifyElementHasExpectedText("dataMaint_Batch_WorkType_Value",
				worktype.getWorkType_Index().toString() + " - " + valueToUse);

	}

	public static void unlockBatch() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Batch_UnlockBatch_Button"));
		BaseUI.waitForElementToBeDisplayed("dataMaint_Batch_UnlockBatch_Modal", null, null);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataMaint_Batch_UnlockBatch_ReasonTextBox"),
				"Automation Test");
		BaseUI.click(Locator.lookupElement("dataMaint_Batch_UnlockBatch_OK_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	// lockTimestamp must be in format "MM/dd/yyyy hh:mm a"
	// lockReason example "Job: act"
	public static void verify_LockStatus_Locked(String lockOperator, String lockReason, String lockTimestamp) throws Exception {
		String dateFormat = "MM/dd/yyyy hh:mm a";

		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Batch_isLocked"), "Yes");
		BaseUI.verifyElementHasExpectedText("dataMaint_Batch_lockOperatorID", lockOperator);
		BaseUI.verifyElementHasExpectedPartialText("dataMaint_Batch_lockReason", lockReason);

		lockTimestamp = lockTimestamp.toUpperCase();
		String firstDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(lockTimestamp, dateFormat, dateFormat,
				-1);
		String secondDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(lockTimestamp, dateFormat,
				dateFormat, 1);

		BaseUI.verify_Date_IsBetween_DateRange(firstDate, secondDate, lockTimestamp, dateFormat);
	//	BaseUI.verifyElementHasExpectedText("dataMaint_Batch_lockTimestamp", lockTimestamp);

	}

	public static void click_GoBack() throws Exception {
		BaseUI.click_js(Locator.lookupElement("dataMaint_GoBack_Button"));
		Thread.sleep(1500);
		Navigation.wait_For_Page_ToLoad();
	}

	public static void verify_LockStatus_NOT_Locked() {

		BaseUI.verifyElementHasExpectedText(Locator.lookupElement("dataMaint_Batch_isLocked"), "No");
		BaseUI.verifyElementHasExpectedText("dataMaint_Batch_lockOperatorID", "");
		BaseUI.verifyElementHasExpectedText("dataMaint_Batch_lockReason", "");
		BaseUI.verifyElementHasExpectedText("dataMaint_Batch_lockTimestamp", "");

	}

	public static void click_collapsible_SectionHeader(collapsible_SectionName section) throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Batch_collapseLink_ByText", section.getValue(), null));
		Thread.sleep(300);
	}

	public static void verify_Pass2Info_Value_ByText(String text, String expectedValue){
		String actualValue = BaseUI.getTextFromField(Locator.lookupElement("dataMaint_Batch_Pass2Info_Value_ByText", text, null));
		BaseUI.baseStringCompare("Pass2Info text Value", expectedValue, actualValue);
	}

	public static TableData return_JobListTableInfo() {
		String[] headers = { "Job ID", "Job Name", "Description", "Job Type", "Count"};

		TableData itemInfo = new TableData();

		itemInfo.data = BaseUI.tableExtractor(headers, "dataMaint_Batch_JobListTable_ColumnValue_ByHeaderText");

		return itemInfo;
	}

	// Xpath indexes start at 1.
	public static String return_JobListValue_ByHeader_AndRowIndex(String header, int index) {
		String textToReturn = BaseUI.getTextFromField(
				Locator.lookupElement("dataMaint_Batch_JobListTable_ColumnValue_ByHeaderText_ByIndex", header, String.valueOf(index)));

		return textToReturn;
	}

	public static void click_RefreshBtn() throws Exception {
		BaseUI.click(Locator.lookupElement("dataMaint_Batch_Refresh_Button"));
		BaseUI.wait_forPageToFinishLoading();
		Navigation.wait_For_Page_ToLoad();
	}

	public static void refresh_And_Wait_ForJobList_ColumnValueToBeUpdated(String columnName, String expectedColumnValue,int indexValue, int timeToWaitInSeconds) throws Exception {

		BaseUI.wait_ForCondition_ToBeMet(()->{
					click_RefreshBtn();
					String columnValue = return_JobListValue_ByHeader_AndRowIndex(columnName, indexValue);
					return columnValue.equals(expectedColumnValue);},
				Duration.ofSeconds(timeToWaitInSeconds), Duration.ofMillis(5000));

	}


}// End of Class
