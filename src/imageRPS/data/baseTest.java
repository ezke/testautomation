package imageRPS.data;

import org.testng.annotations.BeforeTest;
import utils.BaseUI;
import utils.Locator;
import utils.Selenium;


public abstract class baseTest {
	@BeforeTest(alwaysRun=true)
	public void config_setup_method() throws Throwable {
		Selenium.verifySeleniumIsRunning();
		Locator.loadObjectRepository("\\src\\imageRPS\\imageRPS_Repo.txt");
		Data_Import.retrieve_EnvironmentData();
		try {
			ImageRPS_Utilities.setDefaultWebServerTimeout();
		}catch(Exception e){
			BaseUI.log_Warning("Encountered error while setting web server timeout " + e.getMessage());
		}
	}
}
