package imageRPS.data;

import java.io.File;
import java.util.HashMap;
import imageRPS.pages.GlobalVariables;
import utils.Browser;
import utils.DataBuilder;
import utils.FileOperations;
import utils.TableData;

public class Data_Import {

	private static TableData availableEnvironments;
	private static HashMap<String, String> selectedEnvironment;
	private static HashMap<String, String> config_Data;;

	public static void retrieve_EnvironmentData() throws Exception {
		availableEnvironments = DataBuilder.returnTableData_ForComparison("\\src\\imageRPS\\data\\environmentData.csv",
				"\\|", true);

		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\imageRPS\\data\\Config.txt");

		// loads the user config data
		GlobalVariables.entity = getValue("entity");
		GlobalVariables.environment = getValue("environment");
		GlobalVariables.loginName = getValue("loginName");
		GlobalVariables.loginPassword = getValue("loginPassword");
		Browser.currentBrowser = getValue("browser");

		// loads our environment data.
		selectedEnvironment = availableEnvironments.return_Row_BasedOn_1MatchingField("environment",
				GlobalVariables.environment);
		String filePathWithVersionInfo = selectedEnvironment.get("versionFile");
		GlobalVariables.baseURL = selectedEnvironment.get("url");
		GlobalVariables.environmentWebConfigFileLocation = selectedEnvironment.get("webConfigFile");

		File tmpFile = new File(filePathWithVersionInfo);
		if (tmpFile.exists()) {
			GlobalVariables.versionInfo = FileOperations.getVersionInfo(filePathWithVersionInfo);
		} else {
			GlobalVariables.versionInfo = "Unable to find File with path " + filePathWithVersionInfo + " might not be accessible from the vm this was run on.";
		}
	}

	// Gets the specified value from first the System Property. If that is null it
	// reverts to config file.
	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}

		if (value != null) {
			System.setProperty(valueToGet, value);
		}
		return value;
	}

}// End of class
