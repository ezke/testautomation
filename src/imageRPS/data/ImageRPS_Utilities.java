package imageRPS.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.Arrays;

import imageRPS.pages.GlobalVariables;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import utils.BaseUI;
import utils.DataBuilder;
import utils.FileOperations;
import utils.XMLParser;

import javax.xml.parsers.DocumentBuilderFactory;

public class ImageRPS_Utilities {

    public static String return_Line_WithoutXMLTags(String line, String htmlTags) {
        String output = line.substring(line.indexOf("<" + htmlTags + ">") + htmlTags.length() + 2,
                line.indexOf("</" + htmlTags + ">"));

        return output;
    }

    public static void setDefaultWebServerTimeout() throws Exception {
        updateWebConfigTimeout(Duration.ofMinutes(10));
    }

    public static void updateWebConfigTimeout(Duration timeToSet) throws Exception {

        Document configDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(GlobalVariables.environmentWebConfigFileLocation));
        XMLParser parser = XMLParser.getXMLParserWithContent(GlobalVariables.environmentWebConfigFileLocation, configDoc);

        Node timeoutNode = parser.findNode("//add[@key='SessionTimeout']");
        parser.setAttribute(timeoutNode, "value", String.valueOf(timeToSet.toMillis()));

        parser.applyUpdates_ToXML();
    }

    public static void cleanup_CPR_Files() throws Exception {
        FileOperations.cleanup_PriorFile("\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1data\\12300061.P1");
        FileOperations.cleanup_PriorFile("\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\p1images\\12300061.OPI");
        FileOperations.cleanup_PriorFile("\\\\rps602auto1dcm.qalabs.nwk\\K$\\cust1\\Test\\usedbats\\123000");
    }

    public static void run_Events() throws Exception {
        String fileLocation = "C:\\Selenium\\ImageRPS_Event\\";
        String fileName = "EventTester.cmd";

        runCmdFile(fileName, fileLocation);
    }

    public static void run_Events2() throws Exception {
        String fileNameAndLocation = "C:\\Selenium\\ImageRPS_Event\\EventTester.cmd";

        runCmdFile2(fileNameAndLocation);
    }

    public static void runCmdFile(String cmdFileName, String fileLocation) throws Exception {
        String commandLine = MessageFormat.format("cmd /c cd {0} && {1} && exit", fileLocation, cmdFileName);

        try {
            BaseUI.log_Status("Executing script " + commandLine);
            Process proc = Runtime.getRuntime().exec(commandLine);
            BufferedReader r = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String tmp;
            while ((tmp = r.readLine()) != null) {
                System.out.println("> " + tmp);
            }
            proc.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BaseUI.log_Status("Script done.");
    }

    public static void change_RemoteMachine_ToPacificTime() throws Exception {
        runCmdFile2("\\src\\imageRPS\\data\\RCP_Timezone_Bats\\Change_Remote_To_PacificTime.bat");
    }

    public static void change_CentralMachine_ToCentralTime() throws Exception {
        runCmdFile2("\\src\\imageRPS\\data\\RCP_Timezone_Bats\\Change_Central_To_CentralTime.bat");
    }

    public static void change_RemoteMachine_ToCentralTime() throws Exception {
        runCmdFile2("\\src\\imageRPS\\data\\RCP_Timezone_Bats\\Change_Remote_To_CentralTime.bat");
    }

    public static void change_CentralMachine_ToEasternTime() throws Exception {
        runCmdFile2("\\src\\imageRPS\\data\\RCP_Timezone_Bats\\Change_Central_To_EasternTime.bat");
    }

    public static void change_CentralMachine_ToMountainTime() throws Exception {
        runCmdFile2("\\src\\imageRPS\\data\\RCP_Timezone_Bats\\Change_Central_To_MountainTime.bat");
    }

    public static String runCmdFile2(String cmdFileNameWithLocation) throws Exception {
        String relative_Path = DataBuilder.Get_FileData_Path(cmdFileNameWithLocation);
        String commandLine = MessageFormat.format("cmd.exe /c start {0}", relative_Path);

        String output = "";

        try {
            BaseUI.log_Status("Executing script " + commandLine);
            Process proc = Runtime.getRuntime().exec(commandLine);
            BufferedReader r = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String tmp;

            while (r.ready()) {
                tmp = r.readLine();
                output += tmp;
                BaseUI.log_Status("> " + tmp);

            }

        } catch (Exception e) {

            e.printStackTrace();
        }
        BaseUI.log_Status("Script done.");
        return output;
    }

    // remote1_expectedPath
    // Skips the cell 1 check as we might not know what path should be there.
    public static void verify_P1_file_Matches(File expectedfileToCheck, String file1_expectedPath,
                                              File actualFileToCheck, String file2_expectedPath, String expectedBatch, Integer expectedSkewTime,
                                              String expectedDate) throws Exception {

        String expected_fileOutput = DataBuilder.Get_FileData_AsString(expectedfileToCheck);
        String actual_FileOutput = DataBuilder.Get_FileData_AsString(actualFileToCheck);

        String[] expectedFileOutput = expected_fileOutput.split("\\s(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");
        String[] actualFileOutput = actual_FileOutput.split("\\s(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

        BaseUI.verify_true_AndLog(expectedFileOutput.length > 3, "Found file lines.", "Did NOT find file lines.");
        BaseUI.verify_true_AndLog(expectedFileOutput.length == actualFileOutput.length,
                "Outputs matched in line count.", "Outputs did not match in line count.");

        // Validate skew time.
        Integer expectedTime = Integer.parseInt(expectedFileOutput[expectedFileOutput.length - 2]);
        Integer actualTime = Integer.parseInt(actualFileOutput[actualFileOutput.length - 2]);

        Integer skewTime = (expectedTime - actualTime) / 60;
        Integer range1 = expectedSkewTime - 3;
        Integer end_range = expectedSkewTime + 3;
        BaseUI.verify_true_AndLog(skewTime >= range1 && skewTime <= end_range,
                "Clock Skew Amount was close to expected skew amount.", "Expected clock skew range of "
                        + range1.toString() + "-" + end_range.toString() + " But seeing " + skewTime.toString());

        // Validate dates.
        String file1Date = expectedFileOutput[expectedFileOutput.length - 3];
        String file2Date = actualFileOutput[actualFileOutput.length - 3];

        file1Date = convert_P1Date_ToProperFormat(file1Date);

        file2Date = convert_P1Date_ToProperFormat(file2Date);

        BaseUI.baseStringCompare("File 1 Date ", expectedDate, file1Date);
        BaseUI.baseStringCompare("File 2 Date ", expectedDate, file2Date);

        // Validate "unattend/loadbat"
        String file1_unattend = expectedFileOutput[expectedFileOutput.length - 4];
        String file2_unattend = actualFileOutput[actualFileOutput.length - 4];

        BaseUI.baseStringCompare("File 1 unattend/loadbat", "\"unattend/loadbat\"", file1_unattend);
        BaseUI.baseStringCompare("File 2 unattend/loadbat", "\"unattend/loadbat\"", file2_unattend);

        BaseUI.baseStringCompare("Batch Info", "\"Load P1 Batch " + expectedBatch + "\"",
                expectedFileOutput[expectedFileOutput.length - 1]);
        BaseUI.baseStringCompare("Batch Info", "\"Load P1 Batch " + expectedBatch + "\"",
                actualFileOutput[actualFileOutput.length - 1]);
        BaseUI.verify_true_AndLog(expectedFileOutput.length == actualFileOutput.length, "P1 fields matched in length.",
                "P1 fields did NOT match in length.");

        // Validate file 1 path correct
        BaseUI.baseStringCompare("Field 1 expected path", file1_expectedPath, expectedFileOutput[0]);
//		// Validate file 2 path correct
        BaseUI.baseStringCompare("Field 2 expected path", file2_expectedPath, actualFileOutput[0]);
    }

    public static void verify_P1_Files_Match_Expected(String originalPath_ForFiles, String newPath_ForFiles,
                                                      String expectedOriginalPath, String expectedNewPath) throws Exception {
        File[] originalFiles = FileOperations
                .return_NonHidden_Files_AndSubFiles_excludeDirectories(originalPath_ForFiles);
        File[] newFiles = FileOperations.return_NonHidden_Files_AndSubFiles_excludeDirectories(newPath_ForFiles);

        BaseUI.verify_true_AndLog(originalFiles.length > 0, "Found files.", "Did NOT find files.");
        BaseUI.verify_true_AndLog(originalFiles.length == newFiles.length, "File Lists matched in count.",
                "File lists did NOT match in count.");

        String output = "";
        for (int i = 0; i < originalFiles.length; i++) {
            String originalFileExtract = DataBuilder.Get_FileData_AsString(originalFiles[i]);
            String newFileExtract = DataBuilder.Get_FileData_AsString(newFiles[i]);

            String[] originalFileOutput = originalFileExtract.split("\\s(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");
            String[] newFileOutput = newFileExtract.split("\\s(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

            if (!BaseUI.string_IsInteger(originalFileOutput[originalFileOutput.length - 2])) {
                output += "\n File " + originalFiles[i].getPath() + "\n: Time was not an Integer.";
            }
            if (!BaseUI.string_IsInteger(newFileOutput[newFileOutput.length - 2])) {
                output += "\n File " + newFiles[i].getPath() + "\n: Time was not an Integer.";
            }

            String originalDate = convert_P1Date_ToProperFormat(originalFileOutput[originalFileOutput.length - 3]);
            String newDate = convert_P1Date_ToProperFormat(newFileOutput[newFileOutput.length - 3]);

            if (!originalDate.equals(newDate)) {
                output += "\n File - " + originalFiles[i].getPath() + "\nOriginal File date " + originalDate
                        + " did not match New File Date " + newDate;
            }

            if (!BaseUI.time_MatchesFormat(originalDate, "MM/dd/yy")) {
                output += "\n File - " + originalFiles[i].getPath() + "\nDate did not match format MM/dd/yy. Seeing " + originalDate;
            }

            String expectedUnattend_LoadbatString = "\"unattend/loadbat\"";
            if (!originalFileOutput[originalFileOutput.length - 4].equals(expectedUnattend_LoadbatString)) {
                output += "\n File - " + originalFiles[i].getPath() + "\n unattend/loadbat did not match.";
            }
            if (!newFileOutput[newFileOutput.length - 4].equals(expectedUnattend_LoadbatString)) {
                output += "\n File - " + newFiles[i].getPath() + "\n unattend/loadbat did not match.";
            }

            String original_loadP1Batch = originalFileOutput[originalFileOutput.length - 1];
            String new_loadP1Batch = newFileOutput[newFileOutput.length - 1];

            if (!original_loadP1Batch.equals(new_loadP1Batch)) {
                output += "\n File - " + originalFiles[i].getPath() + "\n P1 Batch Field did not match \n File - "
                        + newFiles[i].getPath() + "\n P1 Batch field.";
            }
            String expectedP1BatchString = "\"Load P1 Batch " + originalFiles[i].getName().substring(0, 6) + "\"";
            if (!expectedP1BatchString.equals(original_loadP1Batch)) {
                output += "\n File - " + originalFiles[i].getPath()
                        + "\n File Load P1 Batch string did not match expected value of \n" + expectedP1BatchString;
            }
            String originalPath = originalFileOutput[0];
            String newPath = newFileOutput[0];

            // Validate file 1 path correct
            String expectedOriginalFullPath = expectedOriginalPath + originalFiles[i].getName().toLowerCase();

            if (!expectedOriginalFullPath.equals(originalPath)) {
                output += "\n File - " + originalFiles[i].getPath() + "\n In File path did NOT match "
                        + expectedOriginalFullPath;
            }

            // Validate file 2 path correct. Left this with Original File Name because they
            // should be the same.
            String expectednewFullPath = expectedNewPath + originalFiles[i].getName().toLowerCase();

            if (!expectednewFullPath.equals(newPath)) {
                output += "\n File - " + newFiles[i].getPath() + "\n In File path did NOT match " + expectednewFullPath;
            }

            if (originalFileOutput.length != newFileOutput.length) {

                output += "File - " + originalFiles[i].getPath()
                        + "\n number of fields in file did not match new file.";
            }
        }

        BaseUI.verify_true_AndLog(output.equals(""), "Files matched.", output);
    }

    public static String convert_P1Date_ToProperFormat(String date) throws Exception {
        if (date.length() > 8) {
            date = BaseUI.return_Date_AsDifferentFormat(date, "MM/dd/yyyy", "MM/dd/yy");
        }

        return date;
    }

    public static void verify_TC2004_LogFileExtract_Matches_Expected(String[] fileExtract, String expectedSite,
                                                                     Integer expectedClockSkew, String currentDate) throws Exception {
        String connectingLine = "";
        Integer indexOfconnectingLine = -1;

        for (int i = 0; i < fileExtract.length; i++) {
            if (fileExtract[i].contains("Site 'Remote Cust1Test Site' is connecting")) {
                connectingLine = fileExtract[i].replace("\n", "");
                indexOfconnectingLine = i;
                break;
            }
        }
        String callerLine = fileExtract[indexOfconnectingLine + 1].replace("\n", "");
        String connectSuccessfulLine = fileExtract[indexOfconnectingLine + 2].replace("\n", "");

        // Verify this line and the following 2 lines match the proper date format.
        String connectingLineDate = connectingLine.substring(0, connectingLine.indexOf(": "));
        String callerLineDate = callerLine.substring(0, callerLine.indexOf(": "));
        String connectSuccessfulDate = connectSuccessfulLine.substring(0, connectSuccessfulLine.indexOf(": "));
        String dateFormat = "MM/dd/yy HH:mm:ss.SSS";
        String firstDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, -3);
        String finalDateRange = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentDate, dateFormat,
                dateFormat, 3);

        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, connectingLineDate, dateFormat);
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, callerLineDate, dateFormat);
        BaseUI.verify_Date_IsBetween_DateRange(firstDateRange, finalDateRange, connectSuccessfulDate, dateFormat);

        BaseUI.baseStringCompare("Connection Line", "Site '" + expectedSite + "' is connecting",
                connectingLine.split("\\:\\s")[1]);
        BaseUI.baseStringCompare("Caller Line", "Adding Caller, Site '" + expectedSite + "'",
                callerLine.split("\\:\\s")[1]);
        
        BaseUI.baseStringCompare("Connection Successful", "Site '" + expectedSite + "' connected successfully",
                connectSuccessfulLine.substring(connectSuccessfulLine.indexOf(": ") + 2,
                        connectSuccessfulLine.indexOf(". ")));

        Integer clockSkewAmount = Integer.parseInt(connectSuccessfulLine.split("\\=")[1].split("\\s")[0]);

        Integer lowerClockSkewRange = expectedClockSkew - 3;
        Integer higherClockSkewRange = expectedClockSkew + 3;

        BaseUI.verify_true_AndLog(lowerClockSkewRange <= clockSkewAmount && higherClockSkewRange >= clockSkewAmount,
                "Clock Skew Amount was close to expected skew amount.",
                "Expected clock skew range of " + lowerClockSkewRange.toString() + "-" + higherClockSkewRange.toString()
                        + " But seeing " + clockSkewAmount.toString());
    }

    // Path will vary by folder, so we had to test these cells separately.
    public static void verify_FirstRowMatches_ExceptForOurPathIndexes(File expectedfileToCheck,
                                                                      String expectedfileOutput, String actualfileOuput) {
        String fileName = expectedfileToCheck.getName();
        String fileNameWithoutExtension = fileName.split("\\.")[0];

        String headerRow_Expected = expectedfileOutput.split("\\n")[0];
        String[] headerRow_Expected_Delimited = headerRow_Expected.replaceAll("\\s+", " ").trim().split("\\s(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

        String headerRow_Actual = actualfileOuput.split("\\n")[0];
        String[] headerRow_Actual_Delimited = headerRow_Actual.replaceAll("\\s+", " ").trim().split("\\s(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

        Integer opiIndex = Arrays.asList(headerRow_Expected_Delimited)
                .indexOf("\"" + fileNameWithoutExtension + ".OPI\"") + 1;
        Integer p1Index = Arrays.asList(headerRow_Expected_Delimited).indexOf("\"" + fileNameWithoutExtension + ".P1\"")
                + 1;
        Integer p1gIndex = Arrays.asList(headerRow_Expected_Delimited)
                .indexOf("\"" + fileNameWithoutExtension + ".P1G\"") + 1;

        BaseUI.verify_true_AndLog(headerRow_Expected_Delimited.length == headerRow_Actual_Delimited.length,
                "First Row had same number of cells.", "Cell count for first row did NOT match.");

        BaseUI.verify_true_AndLog(headerRow_Expected_Delimited.length > 0, "Found Header row.",
                "Did not find header row.");

        String output = "";

        for (int i = 0; i < headerRow_Expected_Delimited.length; i++) {

            if (i != opiIndex && i != p1Index && i != p1gIndex) {
                Boolean cellsMatched = headerRow_Expected_Delimited[i].equals(headerRow_Actual_Delimited[i]);
                if (!cellsMatched) {
                    output += "\n" + "Expected cell value of " + headerRow_Expected_Delimited[i] + " But seeing "
                            + headerRow_Actual_Delimited[i];
                }
            }
        }
        BaseUI.verify_true_AndLog(output.equals(""), "First Rows matched.", output);
    }

    public static void verify_FirstRow_CellDifferences(File fileToCheck, String fileOutput, String expectedPath) {
        String fileName = fileToCheck.getName();
        String fileNameWithoutExtension = fileName.split("\\.")[0];

        String headerRow_Expected = fileOutput.split("\\n")[0];
        String[] headerRow_Expected_Delimited = headerRow_Expected.split("\\s");

        String opi_FileNameLocation = headerRow_Expected_Delimited[Arrays.asList(headerRow_Expected_Delimited)
                .indexOf("\"" + fileNameWithoutExtension + ".OPI\"") + 1];

        BaseUI.baseStringCompare("OPI file Location", ("\"" + expectedPath + "p1images\"").toLowerCase(),
                opi_FileNameLocation.toLowerCase());

        String p1_FileNameLocation = headerRow_Expected_Delimited[Arrays.asList(headerRow_Expected_Delimited)
                .indexOf("\"" + fileNameWithoutExtension + ".P1\"") + 1];

        BaseUI.baseStringCompare("P1 file Location", ("\"" + expectedPath + "p1data\"").toLowerCase(),
                p1_FileNameLocation.toLowerCase());

        String p1g_FileNameLocation = headerRow_Expected_Delimited[Arrays.asList(headerRow_Expected_Delimited)
                .indexOf("\"" + fileNameWithoutExtension + ".P1G\"") + 1];

        BaseUI.baseStringCompare("P1G file Location", ("\"" + expectedPath + "p1images\"").toLowerCase(),
                p1g_FileNameLocation.toLowerCase());
    }
}// End of Class
