package inteliData.data;

import java.io.File;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.testng.annotations.Test;

import com.mailosaur.model.Email;

import utils.BaseUI;
import utils.DataBuilder;
import utils.Email_API;
import utils.FileOperations;
import utils.TableData;
import utils.FTP.SFTP;

public class DailyImport {



	private String internalPrograms_Source = "\\\\plvfile02\\appdev\\upload\\automation\\Files for Program and Suppression Automations";

	


	@Test(groups = { "dailyImport" })
	public void Upload_FTPFiles() throws Exception {
		File[] filesToUpload = FileOperations.return_NonHiddenFiles(new File(GlobalVariables.ftp_FilesToUploadLocation));
		SFTP.uploadFiles(GlobalVariables.FTP_CLIENT, filesToUpload, "/automations/");
	}

	@Test(groups = { "dailyImport" })
	public void MovePROD_Internal_ProgramsAndSuppressions_Files() throws Exception {
		FileOperations.copyFiles(internalPrograms_Source, GlobalVariables.prodDestination);
	}

	@Test(groups = { "dailyImport" })
	public void MoveQA_Internal_ProgramsAndSuppressions_Files() throws Exception {
		FileOperations.copyFiles(internalPrograms_Source, GlobalVariables.qaDestination);
	}

	@Test(groups = { "dailyImport" })
	public void Clear_OldEmails() throws Exception {
		Email_API.delete_Emails_ByRecipient(GlobalVariables.programAutomationQA_Email);
		Email_API.delete_Emails_ByRecipient(GlobalVariables.suppressionAutomationQA_Email);
		Email_API.delete_Emails_ByRecipient(GlobalVariables.programAutomationPROD_Email);
		Email_API.delete_Emails_ByRecipient(GlobalVariables.suppressionAutomationPROD_Email);

		TableData emailData = DataBuilder
				.returnTableData_ForComparison("\\src\\inteliData\\data\\emails_automations.csv", "\\,", true);
//
//		final LocalDateTime programsAcceptableDate = LocalDateTime.now().minusHours(12);
//
//		final LocalDateTime suppressionsAcceptableDate = LocalDateTime.now().minusHours(1);
//
//		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

		emailData.forEach_Row(a -> {
			try {

				Email[] allEmailsPerRecipient = Email_API.return_AllEmailsByRecipient(a.get("email"));

				for (Email email : allEmailsPerRecipient) {
			//		String creationTime = email.creationdate.toString();

//					LocalDateTime creationDate = LocalDateTime.parse(creationTime.replaceAll("Z$", "+0000"), format);
//
//					if ((a.get("type").equals("Programs") && creationDate.isBefore(programsAcceptableDate))
//							|| (a.get("type").equals("Suppressions")
//									&& creationDate.isBefore(suppressionsAcceptableDate))) {
						Email_API.delete_Email_ByEmailID(email.id);
			//		}

				}
			} catch (Exception e) {
				BaseUI.log_Warning(e.getMessage());
			}
		});

	}

}
