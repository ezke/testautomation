package inteliData.data;

import org.testng.annotations.BeforeSuite;

import utils.BaseUI;
import utils.Email_API;
import utils.Locator;
import utils.Selenium;

public abstract class baseTest {

	@BeforeSuite(alwaysRun=true)
	public void config_setup_method() throws Throwable {
		//Selenium.verifySeleniumIsRunning();
		Locator.loadObjectRepository("\\src\\inteliData\\inteliData_Repo.txt");
		Data_Import.retrieve_EnvironmentData();
		try {
			Email_API.delete_Emails_ByRecipient(GlobalVariables.accountEmail);
		} catch (Exception e){
			BaseUI.log_Warning("Encountered issue trying to delete emails: " + e.getMessage());
		}
	}
	
	
	
	
	
}//End of Class
