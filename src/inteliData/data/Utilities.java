package inteliData.data;

import java.text.MessageFormat;
import java.util.HashMap;

import com.mailosaur.model.Email;

import utils.BaseUI;
import utils.DatabaseConnection;
import utils.Email_API;
import utils.Locator;
import utils.TableData;

public class Utilities {

	public static String emailTimeFormat = "M/d/yyyy h:mm:ss a";

	public static void verify_Email_Automations(Email email, String expectedSubject, String expectectedAutomationName,
			String expectedFileName, String expectedDate, String successMessage) throws Exception {
		String subject = Email_API.return_EmailSubject_FromEmail(email);
		BaseUI.baseStringCompare("Email Subject", expectedSubject, subject);

		BaseUI.verifyElementHasExpectedText("email_SuccessMessage", successMessage);
		BaseUI.verifyElementHasExpectedText("email_AutomationName_Value", expectectedAutomationName);
		BaseUI.verifyElementHasExpectedText("email_FileName_Value", expectedFileName);

		String messageDate = BaseUI.getTextFromField(Locator.lookupElement("email_MessageDate"));
		messageDate = messageDate.split("\\=")[1];

		BaseUI.verify_Date_IsAcceptable_DateRange(expectedDate, messageDate, -720, 1020, emailTimeFormat);
	}

	public static void verify_Email_AndValuesMatch(String addressToUse, String expectedSubject,
			String expectectedAutomationName, String expectedFileName, String expectedDate,
			String expectedSuccessMessage, boolean expecting1email) throws Exception {

		Email[] allEmailsForRecipient = Email_API.return_AllEmailsByRecipient(addressToUse);

		Email_API.verify_EmailCount(allEmailsForRecipient, 1);
		Email emailToUse = allEmailsForRecipient[0];
		Email_API.open_EmailHTML_In_NewBrowserWindow(emailToUse);

		Utilities.verify_Email_Automations(emailToUse, expectedSubject, expectectedAutomationName, expectedFileName,
				expectedDate, expectedSuccessMessage);

	}

	public static void verify_Email_Successful_Automations(Email email, String expectedSubject,
			String expectectedAutomationName, String expectedFileName, String expectedDate) throws Exception {
		verify_Email_Automations(email, expectedSubject, expectectedAutomationName, expectedFileName, expectedDate,
				"The following file automation was successful.");
	}

	public static void verify_Email_Successful_NoRecords_Automations(Email email, String expectedSubject,
			String expectectedAutomationName, String expectedFileName, String expectedDate, int automationID)
			throws Exception {

		verify_Email_Automations(email, expectedSubject, expectectedAutomationName, expectedFileName, expectedDate,
				"There is no data in the file for automation " + String.valueOf(automationID));
	}

	// Use for our emails_automations file
	public static String return_EmailAddress(TableData emails, String scenario, String type, String location)
			throws Exception {
		HashMap<String, String> lineToMatch = new HashMap<String, String>();
		lineToMatch.put("location", location);
		lineToMatch.put("scenario", scenario);
		lineToMatch.put("type", type);
		lineToMatch.put("environment", GlobalVariables.environment);

		HashMap<String, String> rowMatch = emails.return_Row_BasedOn_AnyNumberOfMatchingFields(lineToMatch);

		if (rowMatch == null) {
			BaseUI.log_AndFail(MessageFormat.format(
					"Could not find email for location {0} with scenario of {1}, type of {2} and environment of {3}",
					location, scenario, type, GlobalVariables.environment));
		}

		return rowMatch.get("email");
	}

	public static TableData return_fileStatusInfo(int automationID) throws Exception {

		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;

		String query = "DECLARE @automationid int = " + String.valueOf(automationID) + "\r\n" + "\r\n"
				+ "select f.fileid, f.filestatustype as 'filestatus', fs.filestatustype as 'filestatusinfo' from automationfiles af\r\n"
				+ "join vs_files f on af.fileid = f.fileid\r\n" + "join vs_filestatuses fs on f.fileid = fs.fileid\r\n"
				+ "where automationid = @automationid\r\n" + "and f.crdate > getdate() -1";

		TableData queryData = DatabaseConnection.runSQLServerQuery(query);

		return queryData;
	}

	public static String returnFileId(int automationID) throws Exception {
		TableData queryData = return_fileStatusInfo(automationID);

		return queryData.data.get(0).get("fileid");
	}

	public static void verify_Database_FileNoDataFailure(int automationID) throws Exception {
		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;

		TableData queryData = return_fileStatusInfo(automationID);

		queryData.verify_MatchingRow_Found(new HashMap<String, String>() {
			{
				put("filestatus", "Failed");
				put("filestatusinfo", "File Found");
			}
		});
		queryData.verify_MatchingRow_Found(new HashMap<String, String>() {
			{
				put("filestatus", "Failed");
				put("filestatusinfo", "File Moved");
			}
		});
		queryData.verify_MatchingRow_Found(new HashMap<String, String>() {
			{
				put("filestatus", "Failed");
				put("filestatusinfo", "File Layout Does Not Match Automation");
			}
		});

		queryData.verify_TableSize(3);

	}

	private static TableData return_PackageData(int packageID) throws Exception {
		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;
		String query = "DECLARE @packageid int = " + String.valueOf(packageID) + "\r\n" + "\r\n"
				+ "select package.packageid, groupid, transactiondate from packageitemtransactions trans\r\n"
				+ "join packageitems items on trans.packageitemid = items.packageitemid\r\n"
				+ "join packages package on items.packageid = package.packageid\r\n"
				+ "where package.packageid = @packageid\r\n" + "and transactiondate > getdate() -1\r\n"
				+ "and packageitemtransactionstatustypeid = 3";

		TableData queryData = DatabaseConnection.runSQLServerQuery(query);

		return queryData;

	}

	public static void verify_Package_Completed_2RecordsFound(int packageID) throws Exception {
		TableData queryData = return_PackageData(packageID);

		queryData.verify_TableSize(2);
	}

	public static void verify_Package_Completed_NoRecordsFound(int packageID) throws Exception {
		TableData queryData = return_PackageData(packageID);
		queryData.verify_TableSize(0);
	}

	public static void verify_Database_DuplicateFileFound(int automationID) throws Exception {
		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;

		TableData queryData = return_fileStatusInfo(automationID);

		queryData.verify_MatchingRow_Found(new HashMap<String, String>() {
			{
				put("filestatus", "Failed");
				put("filestatusinfo", "Duplicate Files Found");
			}
		});

		queryData.verify_TableSize(1);

	}

	public static TableData return_ElmaErrorData(int automationID) throws Exception {
		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;

		String query = "	\r\n" + "	select Message,\r\n" + "	CONVERT(datetime, \r\n"
				+ "SWITCHOFFSET(CONVERT(datetimeoffset, ELMAH_Error.TimeUtc), \r\n"
				+ "DATENAME(TzOffset, SYSDATETIMEOFFSET())))\r\n" + "AS LocalTime \r\n"
				+ "	from elmah.dbo.elmah_error \r\n" + "	where message like '% automation "
				+ String.valueOf(automationID) + "%' \r\n" + "	and CONVERT(datetime, \r\n"
				+ "SWITCHOFFSET(CONVERT(datetimeoffset, ELMAH_Error.TimeUtc), \r\n"
				+ "DATENAME(TzOffset, SYSDATETIMEOFFSET()))) > getdate()-1\r\n" + "	order by timeUTC desc";

		TableData queryData = DatabaseConnection.runSQLServerQuery(query);

		return queryData;

	}

	public static void verify_NoElmaError(int automationID) throws Exception {
		TableData queryData = return_ElmaErrorData(automationID);

		queryData.verify_TableSize(0);
	}

	// only checks for today's date.
	public static void verify_Database_Found1ElmaError(int automationID, String expectedError) throws Exception {

		TableData queryData = return_ElmaErrorData(automationID);

		queryData.verify_TableSize(1);

		BaseUI.baseStringCompare("Elmah Error", expectedError, queryData.data.get(0).get("Message"));

	}

}// End of Class
