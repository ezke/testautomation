package inteliData.data;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.Suppressions;
import utils.Browser;
import utils.ResultWriter;

public class Cleanup_OldDNS_Suppressions extends baseTest {

	// Not to be used as a test.
	// This is simply to clean up old DNS Suppressions based on matching specific
	// criteria.

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_Suppressions();

	}

	@Test
	public void Cleanup_Old_Suppressions() throws Exception {
		Suppressions.cleanup_AndRemove_OldDNSSuppressions();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of class
