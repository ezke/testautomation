package inteliData.data;

import java.io.File;

import org.testng.annotations.Test;

import utils.FileOperations;
import utils.FTP.SFTP;

public class Delete_FTP_Files {

    private String[] _fileNamesToDelete = {"RegressionInternal-DuplicateFiles1.csv", "RegressionInternal-HeaderWith1Record.csv", "RegressionInternal-HeaderWithRecords.csv", "RegressionInternal-DuplicateFiles2.csv",
            "RegressionInternalSuppression-DuplicateFiles1.csv", "RegressionInternalSuppression-DuplicateFiles2.csv", "RegressionProgramInternal.csv"};



    @Test(groups = {"dailyDelete"})
    public void Delete_FTPFiles() throws Exception {
        File[] filesToDeleteFromFTP = FileOperations
                .return_NonHiddenFiles(new File(GlobalVariables.ftp_FilesToUploadLocation));

        String[] fileNames = new String[filesToDeleteFromFTP.length];
        for (int i = 0; i < filesToDeleteFromFTP.length; i++) {
            fileNames[i] = filesToDeleteFromFTP[i].getName();

        }

        SFTP.delete_Files(GlobalVariables.FTP_CLIENT, fileNames, "/automations/");
    }


    @Test(groups = {"dailyDelete"})
    public void Delete_QAFiles() throws Exception {
        for (String fileName : _fileNamesToDelete) {
            FileOperations.delete_SpecificFile(GlobalVariables.qaDestination + fileName);
        }
    }

    @Test(groups = {"dailyDelete"})
    public void Delete_PRODFiles() throws Exception {
        for (String fileName : _fileNamesToDelete) {
            FileOperations.delete_SpecificFile(GlobalVariables.prodDestination + fileName);
        }
    }
}
