package inteliData.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import utils.*;
import utils.FTP.SFTP;

public class Data_Import {

	private static TableData passwords;
	private static TableData availableEnvironments;
	private static HashMap<String, String> selectedEnvironment;
	private static HashMap<String, String> config_Data;

	private static String suppressionFile = "\\\\plvfile02.datamyx.com\\appdev\\upload\\automation\\Suppressions.txt";
	private static String passwordsFile = Browser.downloadLocation + "\\" + "Passwords.txt";

	public static void retrieve_EnvironmentData() throws Exception {
		availableEnvironments = DataBuilder
				.returnTableData_ForComparison("\\src\\inteliData\\data\\environmentData.csv", "\\|", true);

		config_Data = DataBuilder.GetConfig_DataFromFile("\\src\\inteliData\\data\\Config.txt");

		// loads the user config data
		GlobalVariables.environment = getValue("environment");
		GlobalVariables.userName = getValue("userName");
		GlobalVariables.password = getValue("password");
		GlobalVariables.default_UserDesignation = "Test Automation";
		Browser.currentBrowser = getValue("browser");

		suppressionFile = get_SuppressionFileLocation(GlobalVariables.environment);
		// loads our environment data.
		selectedEnvironment = availableEnvironments.return_Row_BasedOn_1MatchingField("environment",
				GlobalVariables.environment);
		GlobalVariables.url = selectedEnvironment.get("url");

		String dbPrefix = selectedEnvironment.get("dbPrefix");
		GlobalVariables.environment_Abbreviation = dbPrefix;
		GlobalVariables.suppression_Mart1 = database_mart1_suppression_ConnectionString(dbPrefix);
		GlobalVariables.suppression_Mart2 = database_mart2_suppression_ConnectionString(dbPrefix);
		GlobalVariables.suppression_Ful1 = database_ful1_suppression_ConnectionString(dbPrefix);
		GlobalVariables.trzis_tran01 = database_tran01_trzis_ConnectionString(dbPrefix);
		GlobalVariables.firstPartyDBConnectionString = database_FirstParty_suppression_ConnectionString(dbPrefix);
		// GlobalVariables.defaultSuppression = selectedEnvironment.get("suppression");

		try {
			SFTP.downloadFile(GlobalVariables.FTP_CLIENT, new File("/notpasswords/Passwords.txt"), Browser.downloadLocation);

			passwords = DataBuilder.returnTableData_ForComparison(passwordsFile, "\\|", false);

		} catch (java.io.FileNotFoundException e) {
			BaseUI.log_Warning("Unable to find file, encountered error:\n" + e.getMessage());
		} catch (Exception e1){
			BaseUI.log_Warning("Ran into error trying to get passwords file:\n" + e1.getMessage());
		}

		try {
			HashMap<String, String> suppressionFile_Data = DataBuilder.GetConfig_DataFromFile(suppressionFile);
			GlobalVariables.defaultSuppression = suppressionFile_Data.get(GlobalVariables.environment);
		} catch (Exception e) {
			BaseUI.log_Warning("Unable to retrieve suppression file.  Exception " + e.getMessage());
		}

		//Only want this to be switched off for DEV environment
		if(!GlobalVariables.environment.equals("DEV")){
			GlobalVariables.run_DateTimeChecks_Dev = true;
		}
	}


	private static String get_SuppressionFileLocation(String environment){
		if(environment.equals("DEV")){
			return "c:\\DAF\\Intelidata\\Suppressions.txt";
		}else{
			return "\\\\plvfile02.datamyx.com\\appdev\\upload\\automation\\Suppressions.txt";
		}
	}

	public static void update_PasswordFile(String user, String newPassword) throws Exception {
		HashMap<String, String> userUpdateInfo = new HashMap<String, String>();
		userUpdateInfo.put("user", user);
		userUpdateInfo.put("environment", GlobalVariables.environment);

		Consumer<HashMap<String, String>> updatePassword;
		updatePassword = x -> {
			if (x.get("user").equals(user) && x.get("environment").equals(GlobalVariables.environment)) {
				x.put("password", newPassword);
			}
		};
		DataBuilder.update_CSVFile(passwordsFile, false, "\\|", updatePassword);

		passwords = DataBuilder.returnTableData_ForComparison(passwordsFile, "\\|", false);
		File[] filesToUpload = new File[]{new File(passwordsFile)};
		SFTP.uploadFiles(GlobalVariables.FTP_CLIENT, filesToUpload, "/notpasswords/");
	}

	public static String returnPassword(String user) {
		HashMap<String, String> userRow = passwords.return_Row_BasedOn_2MatchingFields("user", user, "environment",
				GlobalVariables.environment);

		return userRow.get("password");
	}

	public static void updateSuppressionFile(String orderID) throws Exception {
		if (orderID == null || orderID.trim().equals("")) {
			BaseUI.log_AndFail("Order ID was null, unable to update suppression file.");
		}

		if (BaseUI.string_IsInteger(orderID)) {
			HashMap<String, String> suppressionFile_Data;
			try {
				suppressionFile_Data = DataBuilder.GetConfig_DataFromFile(suppressionFile);
			}catch (FileNotFoundException e){
				FileOperations.copyFile(System.getProperty("user.dir") + "\\src\\inteliData\\data\\Suppressions.txt", suppressionFile);
				suppressionFile_Data = DataBuilder.GetConfig_DataFromFile(suppressionFile);
			}
			String environmentLine = suppressionFile_Data.get(GlobalVariables.environment);

			String fileImport = DataBuilder.Get_FileData_AsString(suppressionFile);
			String[] fileAsArray = fileImport.split("\\r?\\n");
			List<String> finalArray = new ArrayList<String>(Arrays.asList(fileAsArray));

			// If we found the environment line matching our current environment. We'll just
			// update that line.
			if (environmentLine != null) {

				for (int i = 0; i < fileAsArray.length; i++) {
					String lineEnvironment = fileAsArray[i].split("\\|")[0];
					if (lineEnvironment.equals(GlobalVariables.environment)) {
						fileAsArray[i] = GlobalVariables.environment + "|" + orderID;
						finalArray = Arrays.asList(fileAsArray);
						break;
					}

				}
				// Else we'll add that line to the end of the list.
			} else {

				finalArray.add(GlobalVariables.environment + "|" + orderID);
			}

			Path newFilePath = Paths.get(suppressionFile);
			Files.write(newFilePath, finalArray, Charset.forName("UTF-8"));
		}

	}

	// jdbc:jtds:sqlserver://slvsqlmart01/suppression;domain=MYX;user=TestAutomation;password=tHabr!gUj7NE
	public static String database_mart1_suppression_ConnectionString(String dbPrefix) {
		String dbConnectionString;
		if(GlobalVariables.environment.equals("DEV")){
			dbConnectionString = "jdbc:sqlserver://DWA00920.deluxe.com:60000;user=trzisappuser;password=QgoR9!vM47;databaseName=SUPPRESSION";
		}else {
			dbConnectionString = "jdbc:jtds:sqlserver://" + dbPrefix
					+ "sqlmart01.datamyx.com/suppression;domain=MYX;user=TestAutomation;password=tHabr!gUj7NE";
		}
		return dbConnectionString;
	}

	//This one wasn't working for DEV, need to check on it
	public static String database_FirstParty_suppression_ConnectionString(String dbPrefix) {
		String dbConnectionString;
		if(GlobalVariables.environment.equals("DEV")){
			dbConnectionString = "jdbc:jtds:sqlserver://DWA00916.deluxe.com:60000/FirstParty;domain=deluxe;user=WAUIESQL1;password=Texsup#1";
			//dbConnectionString = "jdbc:sqlserver://DWA00916.deluxe.com:60000;user=trzisappuser;password=QgoR9!vM47;databaseName=FirstParty";
		}else {
			dbConnectionString = "jdbc:jtds:sqlserver://" + dbPrefix
					+ "sqlfpd01.datamyx.com/FirstParty;domain=MYX;user=TestAutomation;password=tHabr!gUj7NE";
		}
		return dbConnectionString;
	}

	public static String database_mart2_suppression_ConnectionString(String dbPrefix) {
		String dbConnectionString;
		if(GlobalVariables.environment.equals("DEV")){
			dbConnectionString = "jdbc:sqlserver://DWA00915.deluxe.com:60000;user=trzisappuser;password=QgoR9!vM47;databaseName=SUPPRESSION";
		}else {
			dbConnectionString = "jdbc:jtds:sqlserver://" + dbPrefix
					+ "sqlmart02.datamyx.com/suppression;domain=MYX;user=TestAutomation;password=tHabr!gUj7NE";
		}
		return dbConnectionString;
	}

	public static String database_ful1_suppression_ConnectionString(String dbPrefix) {
		String dbConnectionString;
		if(GlobalVariables.environment.equals("DEV")){
			dbConnectionString = "jdbc:sqlserver://DWA00921.deluxe.com:60000;user=trzisappuser;password=QgoR9!vM47;databaseName=SUPPRESSION";
		}else {
			dbConnectionString = "jdbc:jtds:sqlserver://" + dbPrefix
					+ "sqlful01.datamyx.com/suppression;domain=MYX;user=TestAutomation;password=tHabr!gUj7NE";
		}
		return dbConnectionString;
	}

	public static String database_tran01_trzis_ConnectionString(String dbPrefix) {
		String dbConnectionString;
		if(GlobalVariables.environment.equals("DEV")){
			dbConnectionString = "jdbc:sqlserver://DWA00918.deluxe.com:60000;user=trzisappuser;password=QgoR9!vM47;databaseName=TRZIS";
		}else {
			dbConnectionString = "jdbc:jtds:sqlserver://" + dbPrefix
					+ "sqltran01.datamyx.com/trzis;domain=MYX;user=TestAutomation;password=tHabr!gUj7NE";
		}
		return dbConnectionString;
	}

	// Gets the specified value from first the System Property. If that is null it
	// reverts to config file.
	public static String getValue(String valueToGet) {
		String value = "";
		value = System.getProperty(valueToGet);
		if (value == null) {
			value = config_Data.get(valueToGet);
		}

		if (value != null) {
			System.setProperty(valueToGet, value);
		}
		return value;
	}

}// End of class
