package inteliData.data;

import utils.FTP.FTP_ClientInfo;

public class GlobalVariables {

	public static String userName = "";
	public static String password = "";

	public static String default_UserDesignation = "";

	public static String user_PasswordReset = "autouserplus2";
	
	public static String url = "";
	public static String environment = "";
	
	//slv for QA, plv for PROD, etc
	public static String environment_Abbreviation = "";
	
	//db connection strings go here
	public static String suppression_Mart1 = "";
	public static String suppression_Mart2 = "";
	public static String suppression_Ful1 = "";
	public static String trzis_tran01 = "";
	public static String firstPartyDBConnectionString = "";
	
	
	public static String defaultMarketer = "1";
	public static String defaultSuppression = "";
	
	public static String accountEmail = "inteliauto1.j4pxicxw@mailosaur.io";
	
	public static String programAutomationQA_Email = "progAutQA.j4pxicxw@mailosaur.io";
	public static String programAutomationPROD_Email = "progAutPROD.j4pxicxw@mailosaur.io";
	public static String suppressionAutomationQA_Email = "suppressAutQA.j4pxicxw@mailosaur.io";
	public static String suppressionAutomationPROD_Email = "suppressAutPROD.j4pxicxw@mailosaur.io";
	
	public static String ftp_FilesToUploadLocation = "\\\\plvfile02.datamyx.com\\appdev\\upload\\automation\\FTP Files\\";
	public static final FTP_ClientInfo FTP_CLIENT = new FTP_ClientInfo("ftp.bankersdashboard.com", 22, "Carlene", "72X2LTvO");
	
	public static String suppressionSampleFile = System.getProperty("user.dir") + "\\src\\inteliData\\data\\Name Address and Phone - 248.csv";
	public static String suppressionSampleFile_Name = "Name Address and Phone - 248.csv";
	
	public static String appendSampleFile = System.getProperty("user.dir") + "\\src\\inteliData\\data\\Append 10K Test.csv";
	public static String appendSampleFile_Name = "Append 10K Test.csv";
	
	//Includes 52.  The other 2 are for Puerto Rico and District of Columbia
	public static String[] stateAbbreviations_52 = {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY", "PR", "DC"};

	//Dev environment currently having issues with Date/Time.  Need to be able to turn off those validations.
	//This gets set to true via the Data_Import class for other environments so those validations will still run.
	public static boolean run_DateTimeChecks_Dev = false;

	public static String qaDestination = "\\\\slvftp01\\ftp\\Users\\test_internal\\";
	public static  String prodDestination = "\\\\plvftp01\\ftp\\Users\\test_internal\\";



}
