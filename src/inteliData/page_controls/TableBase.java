package inteliData.page_controls;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class TableBase {
	public String table_ID;

	public String tableBodyElement = "table_tableBody_ByTableID";
	public String tableHeaderElement = "table_headerList_ByTableID";

	public TableBase(String tableID) {
		table_ID = tableID;
	}

	public TableBase(String table_BodyElement, String table_HeadersElement) {
		tableBodyElement = table_BodyElement;
		tableHeaderElement = table_HeadersElement;
	}

	public TableBase() {
	}

	public void verify_TablesMatch(TableData expectedTable) throws Exception {
		TableData actualTable = return_TableData();

		actualTable.verify_TableSize(expectedTable.data.size());

		for(int i = 0; i < expectedTable.data.size(); i++){
			expectedTable.verify_MatchingRow_Found(actualTable.data.get(i));
		}
	}

	public TableData return_TableData() {
		TableData table_Data = new TableData();

		String[] tableHeaders = return_TableHeaders();

		table_Data = BaseUI.tableExtractorV2(tableBodyElement, table_ID, null, tableHeaders);

		return table_Data;
	}

	public String[] return_TableHeaders() {
		ArrayList<WebElement> tableHeaderElements = Locator.lookup_multipleElements(tableHeaderElement, table_ID, null);
		ArrayList<String> tableHeaders = new ArrayList<String>();
		for (WebElement headerElement : tableHeaderElements) {
			String headerText = BaseUI.get_Attribute_FromField(headerElement, "innerText").trim();
			if (!headerText.equals("")) {
				tableHeaders.add(headerText.trim());
			}
		}

		return tableHeaders.toArray(new String[0]);
	}

	public void sort_Header_Ascending(String headerToSortBy) throws Exception {

		for (int i = 0; i < 2; i++) {
			WebElement headerWeWant = Locator.lookupElement("table_headerList_ByTableID_WithText", table_ID, null);
			String headerClass = BaseUI.get_Attribute_FromField(headerWeWant, "class");
			if (headerClass.equals("sorting_asc")) {
				break;
			} else {
				BaseUI.click(headerWeWant);
				Thread.sleep(500);
			}

		}
	}

	public void sort_Header_Descending(String headerToSortBy) throws Exception {

		for (int i = 0; i < 2; i++) {
			WebElement headerWeWant = Locator.lookupElement("table_headerList_ByTableID_WithText", table_ID,
					headerToSortBy);
			String headerClass = BaseUI.get_Attribute_FromField(headerWeWant, "class");
			if (headerClass.equals("sorting_desc")) {
				break;
			} else {
				BaseUI.click(headerWeWant);
				Thread.sleep(500);
			}

		}
	}

	public void verify_NoDataInTable() {
		WebElement tableBody = Locator.lookupElement(tableBodyElement, table_ID, null);
		WebElement noDataElement = tableBody.findElement(By.xpath(".//td[@class='dataTables_empty']"));
		BaseUI.verifyElementAppears(noDataElement);

		String cellText = BaseUI.getTextFromField(noDataElement);
		BaseUI.verify_true_AndLog(
				cellText.equals("No data available in table") || cellText.equals("No matching records found"),
				"Found appropriate text to indicate no rows found.",
				"Did NOT find appropriate text to indicate no rows found.");
	}

	public Boolean tableWasEmpty() {
		WebElement tableBody = Locator.lookupRequiredElement(tableBodyElement, table_ID, null);
		WebElement noDataElement = null;
		try {
			noDataElement = tableBody.findElement(By.xpath(".//td[@class='dataTables_empty']"));
		} catch (org.openqa.selenium.NoSuchElementException e) {
			//If Element was not found stating table was empty, we'll return false.
			return false;
		}

		return BaseUI.elementAppears(noDataElement);
	}

}// End of Class
