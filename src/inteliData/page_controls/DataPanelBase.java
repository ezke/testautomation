package inteliData.page_controls;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

//Use for the data panels as found on Create Audience > Review And Run page.

public class DataPanelBase {

	public String headerText;

	public DataPanelBase(String expectedHeaderText) {
		headerText = expectedHeaderText;
	}

	public DataPanelBase() {

	}

	// headerText variable returns match on partial text. This validation would be
	// to ensure that header is complete match with expected text.
	public void verify_HeaderText_Matches(String textToMatch) {
		WebElement headerElement = return_HeaderElement();
		BaseUI.verifyElementHasExpectedText(headerElement, textToMatch);
	}

	public void verify_ValueMatches(String header, String expectedValue) {
		HashMap<String, String> returnedData = return_PanelData();
		BaseUI.baseStringCompare(header, expectedValue, returnedData.get(header));
	}

	public WebElement return_HeaderElement() {
		return Locator.lookupElement("dataPanel_Header_ByHeaderText", headerText, null);
	}

	public void verify_ExpectedData_Matches_ReturnedData(HashMap<String, String> expectedData) {
		HashMap<String, String> returnedData = return_PanelData();

		BaseUI.verify_true_AndLog(returnedData.keySet().size() == expectedData.keySet().size(),
				"Key sets matched in size.", "Keysets did NOT match in size.");

		for (String key : expectedData.keySet()) {
			BaseUI.baseStringCompare(key, expectedData.get(key), returnedData.get(key));
		}

	}

	public ArrayList<WebElement> returnRowHeaders() {
		return Locator.lookup_multipleElements("dataPanel_CellHeaders_ByHeaderText", headerText, null);
	}

	public ArrayList<WebElement> returnRowCells() {
		return Locator.lookup_multipleElements("dataPanel_CellValues_ByHeaderText", headerText, null);
	}

	public String return_SpecificValue(String cellHeaderText) {
		String textToReturn = BaseUI.getTextFromField(Locator
				.lookupElement("dataPanel_SpecificValue_ByHeaderText_CellHeaderText", headerText, cellHeaderText));
		textToReturn = textToReturn.replace("= ", "").trim();
		return textToReturn;
	}

	public HashMap<String, String> return_PanelData() {
		ArrayList<WebElement> rowHeaders = returnRowHeaders();
		ArrayList<WebElement> rowCells = returnRowCells();
		HashMap<String, String> panelData = new HashMap<String, String>();
		for (int i = 0; i < rowHeaders.size(); i++) {
			panelData.put(BaseUI.get_Attribute_FromField(rowHeaders.get(i), "innerText"),
					BaseUI.get_Attribute_FromField(rowCells.get(i), "innerText"));
		}

		// Do some formatting on original HashMap

		return format_Data(panelData);
	}

	public HashMap<String, String> format_Data(HashMap<String, String> unformattedMappings) {
		HashMap<String, String> panelData_Formatted = new HashMap<String, String>();

		for (String key : unformattedMappings.keySet()) {
			String newKey = "";
			if (key.endsWith(":")) {
				newKey = key.replace(":", "");
				panelData_Formatted.put(newKey, unformattedMappings.get(key).trim());
			} else if (key.contains("\n")) {
				newKey = key.split("\n")[0].trim();
				if (newKey.endsWith(":")) {
					newKey = newKey.replace(":", "").trim();
				}
				panelData_Formatted.put(newKey, unformattedMappings.get(key).trim());
			} else {
				newKey = key;
				panelData_Formatted.put(newKey, unformattedMappings.get(key).trim());
			}
			if (unformattedMappings.get(key).startsWith("= ")) {
				panelData_Formatted.put(newKey, unformattedMappings.get(key).replace("= ", "").trim());
			}

		}
		return panelData_Formatted;
	}
}// End of Class
