package inteliData.page_controls;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;
import java.util.ArrayList;
import java.util.Arrays;

public class TextBox_WithLinkWrapper {

    protected String _identifyingText = null;

    protected String _searchTextBoxElement = "txtLinkWrap_SearchTextBox";
    protected String _listOfEntriesElement = "txtLinkWrap_ListOfEntries";

    protected TextBox_WithLinkWrapper(){}

    protected TextBox_WithLinkWrapper(String identifyingText) {
        _identifyingText = identifyingText;
    }

    protected TextBox_WithLinkWrapper(String searchTextBox, String listOfEntriesElement) {
        _searchTextBoxElement = searchTextBox;
        _listOfEntriesElement = listOfEntriesElement;
    }

    public static TextBox_WithLinkWrapper getTextBox_ByIdentifyingText(String identifyingText) {

        return new TextBox_WithLinkWrapper(identifyingText);
    }

    public static TextBox_WithLinkWrapper getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier(String searchTextBox, String listOfEntriesElement) {

        return new TextBox_WithLinkWrapper(searchTextBox, listOfEntriesElement);
    }

    public void enterText(String textToInput) throws Exception {
        WebElement elementToEnterTextInto = get_ElementToEnterTextInto();
        BaseUI.enterText_IntoInputBox(elementToEnterTextInto, textToInput);
        BaseUI.enterKey(elementToEnterTextInto, Keys.ENTER);
    }

    public WebElement get_ElementToEnterTextInto() {
        return Locator.lookupElement(_searchTextBoxElement, _identifyingText, null);
    }


    public ArrayList<WebElement> getOptionsElements() {
        return Locator.lookup_multipleElements(_listOfEntriesElement,
                _identifyingText, null);
    }

    public void enterText(String[] valuesToEnter) throws Exception {
        for (String value : valuesToEnter) {
            enterText(value);
        }
    }

    public String[] getEnteredOptions() throws Exception {

        ArrayList<WebElement> enteredOptions = getOptionsElements();

        ArrayList<String> enteredOptions_Text = new ArrayList<String>();
        for (WebElement option : enteredOptions) {
            enteredOptions_Text.add(BaseUI.getTextFromField(option));
        }

        return enteredOptions_Text.toArray(new String[0]);
    }

    public void verify_OptionsSelected(String[] expectedOptions) throws Exception {
        String[] actualValues = getEnteredOptions();

        BaseUI.verify_true_AndLog(expectedOptions.length == actualValues.length,
                "Expected values did matched actual values in count.",
                "Expected values did not match actual values in count.");

        for (String expectedOption : expectedOptions) {
            BaseUI.verify_true_AndLog(Arrays.asList(actualValues).contains(expectedOption),
                    "Found expected value of " + expectedOption, "Did NOT find expected value of " + expectedOption);
        }

    }

}// End of Class
