package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

//Checkboxes where you click the label
public class Checkbox_ClickLabel_WithAlerts extends CheckboxBase {

	public String alert_Message;

	// Not all Checkboxes of this type will have an Alert Message.
	public Checkbox_ClickLabel_WithAlerts(String labelText, String alertMessage) {
		label_text = labelText;
		elementWithClassToCheck = "chckboxLabel_div_Element_By_LabelText";
		elementWithLabelToClick = "chckboxLabel_ClickableLabel_By_LabelText";
		alert_Message = alertMessage;
	}
	
	@Override
	public void wait_ForCheckboxAction() throws Exception {
		Thread.sleep(2000);
		BaseUI.wait_forPageToFinishLoading();
	}

	@Override
	public Boolean checkboxSelected(WebElement checkboxElementWithSelectedClass) {
		String classValue = BaseUI.get_Attribute_FromField(checkboxElementWithSelectedClass, "class");
		if (!classValue.contains("with-success-tooltip")) {
			return false;
		} else {
			return true;
		}
	}

	public void verify_AlertMessage_Appears_AndCorrect() {
		String value = BaseUI.get_Attribute_FromField(Locator.lookupElement("chckboxLabel_input_ByLabelText", label_text, null), "value");
		WebElement alert = Locator.lookupElement("chckboxLabel_Alert_By_Value", value, null);
		BaseUI.verifyElementAppears(alert);
		BaseUI.verifyElementHasExpectedText(alert, alert_Message);
	}
	
	public void verify_AlertMessage_DoesNOTAppear() {
		String value = BaseUI.get_Attribute_FromField(Locator.lookupElement("chckboxLabel_input_ByLabelText", label_text, null), "value");
		WebElement alert = Locator.lookupElement("chckboxLabel_Alert_By_Value", value, null);
		BaseUI.verifyElementDoesNotAppear(alert);
	}


}// End of Class
