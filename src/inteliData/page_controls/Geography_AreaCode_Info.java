package inteliData.page_controls;

import org.apache.commons.lang3.Range;

public class Geography_AreaCode_Info {

    private String _level;
    private Range<Integer> _ficoRange_Initial;
    private String[] _areaCodes_AfterExclusion;

    public Geography_AreaCode_Info(String level, Range<Integer> fico_initial, String[] areaCodes_afterExclusion){
        _level = level;
        _ficoRange_Initial = fico_initial;
        _areaCodes_AfterExclusion = areaCodes_afterExclusion;

    }


    public String getLevel() {
        return _level;
    }

    public Range<Integer> getFicoRange_Initial() {
        return _ficoRange_Initial;
    }


    public String[] get_areaCodes_AfterExclusion() {
        return _areaCodes_AfterExclusion;
    }




}
