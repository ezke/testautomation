package inteliData.page_controls;

import utils.BaseUI;
import utils.Locator;

public class ShowEntries_Dropdown {

	String dropdownName;

	public ShowEntries_Dropdown(String dropdown_Name) {
		dropdownName = dropdown_Name;
	}

	public void select_DropdownOption(showEntriesOptions optionWeWant) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("showEntriesDrop_dropdown_ByName", dropdownName, null),
				optionWeWant.getValue());
	}

	public enum showEntriesOptions {
		option_10("10"), option_25("25"), option_50("50"), option_100("100"), option_ViewAll("View All");

		private String value;

		private showEntriesOptions(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

}// End of class
