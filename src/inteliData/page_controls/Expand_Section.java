package inteliData.page_controls;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class Expand_Section {

	public String headerText = "";
	public String sectionExpandedText = "panel-collapse collapse in";
	public String sectionCollapsedText = "panel-collapse collapse";
	public String attributeToCheck = "class";
	public String elementIdentifier_ElementToCheck = "expandSec_ElementToCheckExpandedState";

	public Expand_Section(String header_Text) {
		headerText = header_Text;
	}

	public Expand_Section() {

	}

	public WebElement elementToClick() {
		return Locator.lookupElement("expandSec_linkToExpand", headerText, null);
	}
	
//	public String elementToCheckIdentifier() {
//		return "expandSec_ElementToCheckExpandedState";
//	}

	public void expand_Section() throws Exception {
		if (sectionExpanded()) {
			BaseUI.log_Status("Section " + headerText + " already expanded, no action needed.");
			BaseUI.wait_forPageToFinishLoading();
		} else {
			BaseUI.log_Status("Expanding section " + headerText);
			((JavascriptExecutor)Browser.driver).executeScript("window.scrollTo(0, 0)");
			BaseUI.click(elementToClick());
			BaseUI.waitForElementToContain_AttributeMatch(elementIdentifier_ElementToCheck, headerText, null,
					attributeToCheck, sectionExpandedText, 5);
			Thread.sleep(200);
		}
	}

	public void collapse_Section() throws Exception {
		if (!sectionExpanded()) {
			BaseUI.log_Status("Section " + headerText + " already collapsed, no action needed.");
		} else {
			BaseUI.log_Status("Collapsing section " + headerText);
			BaseUI.click(elementToClick());
			BaseUI.waitForElementToContain_AttributeMatch(elementIdentifier_ElementToCheck, headerText, null,
					attributeToCheck, sectionCollapsedText, 5);
			Thread.sleep(200);
		}
	}

	private Boolean sectionExpanded() {
		String expandElementToCheckClass = BaseUI.get_Attribute_FromField(
				Locator.lookupElement(elementIdentifier_ElementToCheck, headerText, null), attributeToCheck);

		if (expandElementToCheckClass.equals(sectionExpandedText)) {
			return true;
		} else if (expandElementToCheckClass.equals(sectionCollapsedText)) {
			return false;
		} else {
			BaseUI.log_AndFail("Unrecognized expander class value of " + expandElementToCheckClass);
		}

		return null;
	}

}// End of class
