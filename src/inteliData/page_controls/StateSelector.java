package inteliData.page_controls;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utils.BaseUI;
import utils.Locator;

//Control to select states on Global Criteria and Define Levels pages
public class StateSelector {

	
	private String _id;
	
	public StateSelector(String id) {
		_id = id;
	}

	public void select_States(String[] statesToSelect) throws Exception {
		BaseUI.log_Status("Attempting to select states: " + statesToSelect.toString());

		WebElement stateSelector = Locator.lookupElement("createAud_GlobalCrit_StateSelector", _id, null);
		BaseUI.scroll_to_element(stateSelector);

		Select stateSelect = new Select(stateSelector);
		for (String state : statesToSelect) {
			stateSelect.selectByVisibleText(state.toUpperCase());
		}
	}

	public void click_AllStates() throws Exception {
		WebElement allstates_Control = Locator.lookupElement("createAud_GlobalCrit_AllStates_Checkbox", _id, null);
		BaseUI.scroll_to_element(allstates_Control);
		BaseUI.click(allstates_Control);
		Thread.sleep(200);
	}

	public void verify_StatesSelected(String[] expectedStates) {
		ArrayList<String> expectedStatesList = new ArrayList<>(Arrays.asList(expectedStates));

		ArrayList<WebElement> selectedStateElements = Locator.lookup_multipleElements("createAud_GlobalCrit_StatesDropdown_SelectedStates", _id, null);

		BaseUI.verify_true_AndLog(expectedStates.length == selectedStateElements.size(), "Found same number of states.",
				"Returned State elements did not match expected states in size.");

		for (WebElement stateElement : selectedStateElements) {
			String stateText = BaseUI.getTextFromField(stateElement);
			BaseUI.verify_true_AndLog(expectedStatesList.contains(stateText),
					"Found State " + stateText + " in expected results.",
					"Did NOT find State " + stateText + " in expected results.");

		}
	}

}
