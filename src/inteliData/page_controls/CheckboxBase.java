package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class CheckboxBase {
	// Element we check to see if it's checked.
	public String elementWithClassToCheck = "chckboxLabel_div_Element_By_LabelText";
	// Element we click on to check and uncheck.
	public String elementWithLabelToClick = "chckboxLabel_ClickableLabel_By_LabelText";
	public String label_text;

	public CheckboxBase() {

	}

	public CheckboxBase(String labelText) {
		label_text = labelText;

	}

	protected void click_Checkbox(WebElement elementToClick){
		BaseUI.click(elementToClick);
	}

	public void check_Checkbox() throws Exception {
		WebElement checkboxSelectedLI = elementTo_SeeIfChecked();

		if (!checkboxSelected(checkboxSelectedLI)) {
			click_Checkbox(clickElement());
			wait_ForCheckboxAction();
		}

	}

	public void wait_ForCheckboxAction() throws Exception {
		Thread.sleep(200);
	}

	public WebElement elementTo_SeeIfChecked() {
		return Locator.lookupElement(elementWithClassToCheck, label_text, null);
	}

	public WebElement clickElement() {
		return Locator.lookupElement(elementWithLabelToClick, label_text, null);
	}

	public void uncheck_Checkbox() throws Exception {
		WebElement checkboxSelectedLI = elementTo_SeeIfChecked();

		if (checkboxSelected(checkboxSelectedLI)) {
			click_Checkbox(clickElement());
			wait_ForCheckboxAction();
		}

	}

	public Boolean checkboxSelected(WebElement checkboxElementWithSelectedClass) {
		String classValue = BaseUI.get_Attribute_FromField(checkboxElementWithSelectedClass, "class");
		if (classValue != null && !classValue.contains("checked") && !classValue.contains("selected")) {
			return false;
		} else {
			return true;
		}
	}

	public void verify_CheckboxChecked() {
		boolean checkboxSelected = checkboxSelected(elementTo_SeeIfChecked());
		BaseUI.verify_true_AndLog(checkboxSelected, "Checkbox was checked.", "Checkbox was NOT checked");
	}
	
	public void verify_CheckboxNOTChecked() {
		boolean checkboxSelected = checkboxSelected(elementTo_SeeIfChecked());
		BaseUI.verify_true_AndLog(!checkboxSelected, "Checkbox was NOT checked.", "Checkbox was checked");
	}
}
