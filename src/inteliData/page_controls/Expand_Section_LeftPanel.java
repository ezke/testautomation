package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Expand_Section_LeftPanel extends Expand_Section {

	public Expand_Section_LeftPanel(String header_Text) {
		headerText = header_Text;
		sectionExpandedText = "active";
		sectionCollapsedText = "";
		elementIdentifier_ElementToCheck = "expandSecNav_ElementToCheckExpandedState";
	}

	@Override
	public WebElement elementToClick() {
		return Locator.lookupElement("expandSecNav_linkToClick", headerText, null);
	}

	@Override
	public void collapse_Section() throws Exception {
		BaseUI.log_AndFail(
				"Cannot directly collapse Left Panel section.  Would have to select other section for this section to collapse.");
	}

}
