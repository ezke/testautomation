package inteliData.page_controls;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class Table_PurchaseRecords_PricingInfo extends TableBase {

	public Table_PurchaseRecords_PricingInfo() {

	}

	@Override
	public TableData return_TableData() {
		TableData table_Data = new TableData();

		String[] tableHeaders = return_TableHeaders();
		// table_Data = BaseUI.tableExtractorV2(tableBodyElement, table_ID, null,
		// tableHeaders);
		table_Data = BaseUI.tableExtractor_ByCell("createAud_PurcRec_PricingInfo_TableCells_ExcludingFooterRow",
				tableHeaders);

		return table_Data;
	}

	@Override
	public String[] return_TableHeaders() {
		String[] tableHeaders = { "Commitment", "Supplier", "Expiration Date", "Commitment Amount", "Commitment Paid",
				"Available Balance", "Supplier Quantity", "Total Price", "Amount to Apply", "Amount to Pay" };
		return tableHeaders;
	}

	public String return_TotalAmount() {
		String totalAmount = BaseUI.get_Attribute_FromField(Locator.lookupElement("createAud_PurcRec_PricingInfo_TotalAmount"), "innerText");
		return totalAmount.trim();
	}
	
}
