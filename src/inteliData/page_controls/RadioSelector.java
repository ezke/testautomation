package inteliData.page_controls;

import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public  class RadioSelector {


    private String _identifyingText;

    public RadioSelector(String identifyingText){
        _identifyingText = identifyingText;
    }

    public <E extends Enum<E>> void click_Option(Enum<E> option) throws Exception {
        WebElement optionElement = Locator.lookupElement("radSelect_Option", _identifyingText, option.toString());
        BaseUI.scroll_element_to_middleOfScreen(optionElement);
        BaseUI.click(optionElement);
    }


    public <E extends Enum<E>> void verify_Option_Selected(Enum<E> option) throws Exception {
        WebElement optionElement = Locator.lookupElement("radSelect_Option", _identifyingText, option.toString());
        BaseUI.verifyElementHasExpectedAttributeValue(optionElement,"class","checked");
    }
}
