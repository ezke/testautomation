package inteliData.page_controls;

import org.apache.commons.lang3.Range;

public class Geography_ZipCode_Info {

    private String _level;
    private Range<Integer> _ficoRange;
    private String[] _zips;

    public Geography_ZipCode_Info(String level, Range<Integer> ficoRange, String[] zips) {
        _level = level;
        _ficoRange = ficoRange;
        _zips = zips;
    }

    public String getLevel() {
        return _level;
    }

    public Range<Integer> get_ficoRange() {
        return _ficoRange;
    }

    public String[] get_zips() {
        return _zips;
    }




}
