package inteliData.page_controls;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

//Table is custom to Suppression Map, will require some additional work.
public class Table_SuppressionMap extends TableBase {

	private String setDestinationDropdownElement = "suppressionMap_TableDestination_ByIndex";
	private String getTableRowsElement = "table_tableRows";
	private String checkboxElementToCheckByIndex = "chckbox_Tables_ElementToCheck_ByIndex";
	private String checkboxElementToClick = "suppressionMap_SelectionCheckbox_ByIDAndRow";

	private Table_SuppressionMap(String tableID) {
		table_ID = tableID;
	}

	// Use this overload if you don't have a table ID
	private Table_SuppressionMap(String setDestinationdDropdown_Element, String getTableRows_Element,
			String checkbox_ElementToCheckByIndex, String checkbox_ElementToClick) {
		setDestinationDropdownElement = setDestinationdDropdown_Element;
		getTableRowsElement = getTableRows_Element;
		checkboxElementToCheckByIndex = checkbox_ElementToCheckByIndex;
		checkboxElementToClick = checkbox_ElementToClick;
	}

	public static Table_SuppressionMap get_Table_SuppressionMap_byID(String idOfTable) {
		return new Table_SuppressionMap(idOfTable);

	}

	public static Table_SuppressionMap get_Table_SuppressionMap_NoID(String setDestinationdDropdown_Element,
			String getTableRows_Element, String checkbox_ElementToCheckByIndex, String checkbox_ElementToClick) {

		return new Table_SuppressionMap(setDestinationdDropdown_Element, getTableRows_Element,
				checkbox_ElementToCheckByIndex, checkbox_ElementToClick);

	}

	// Index starts at 1.
	public Checkbox_ForTable checkbox_ByIndex(Integer index) {
		Checkbox_ForTable tableCheckbox = null;

		if (table_ID != null) {
			tableCheckbox = new Checkbox_ForTable(table_ID, index.toString(), checkboxElementToCheckByIndex,
					checkboxElementToClick);
		} else {
			tableCheckbox = new Checkbox_ForTable(index.toString(), checkboxElementToCheckByIndex,
					checkboxElementToClick);
		}
		return tableCheckbox;
	}

	public void verify_AllCheckboxes_Checked(TableData tableExtract) {
		BaseUI.verify_true_AndLog(tableExtract.data.size() > 0, "Found data in the table to loop through.", "Did NOT find data in the table to loop through.");
		
		for (int i = 0; i < tableExtract.data.size(); i++) {
			checkbox_ByIndex(i + 1).verify_CheckboxChecked();
		}
	}

	// Checks the checkbox to select a specific row.
	public void selectRow_BySource(TableData tableData, String sourceToSelect) throws Exception {
		Integer indexToSelect = null;

		for (int i = 0; i < tableData.data.size(); i++) {
			if (tableData.data.get(i).get("Source").equals(sourceToSelect)) {
				indexToSelect = i + 1;
				break;
			}
		}

		checkbox_ByIndex(indexToSelect).check_Checkbox();
	}

	public void setDestination(TableData tableData, String sourceToMatch, String destinationToPick) {
		Integer indexToSelect = tableData.first_IndexOf("Source", sourceToMatch) + 1;

		BaseUI.selectValueFromDropDown(
				Locator.lookupElement(setDestinationDropdownElement, indexToSelect.toString(), null),
				destinationToPick);

	}

	public ArrayList<WebElement> returnTableRows() {
		return Locator.lookup_multipleElements(getTableRowsElement, table_ID, null);
	}

	// I'll have to extract data by row since there is a combination of normal
	// cells, checkboxes and dropdowns.
	@Override
	public TableData return_TableData() {
		
		BaseUI.log_Status("Attempting to pull data from Mapping table.");
		
		TableData tableData = new TableData();

//		ArrayList<WebElement> sourceColumn = Locator.lookup_multipleElements("suppressionTable_SourceColumn", null, null);
//		ArrayList<WebElement> sampleColumn = Locator.lookup_multipleElements("suppressionTable_SampleColumn", null, null);
//		ArrayList<WebElement> destinationColumn = Locator.lookup_multipleElements("suppressionTable_DestinationColumn", null, null);
//		
//		for(int i = 0; i <sourceColumn.size() ; i++)
//		{
//			//Chose not to use BaseUI methods as this would blow up the console output.
//			HashMap<String, String> newRow = new HashMap<String,String>();
//			newRow.put("Source", sourceColumn.get(i).getAttribute("innerText"));
//			newRow.put("Sample", sampleColumn.get(i).getAttribute("innerText"));
//			
//			Select destination = new Select(destinationColumn.get(i));
//			newRow.put("Destination",destination.getFirstSelectedOption().getAttribute("innerText"));
//			
//			tableData.data.add(newRow);
//		}
//		
		//table/tbody/tr/td[2]
		
		ArrayList<WebElement> tableRows = returnTableRows();
		for (WebElement tableRow : tableRows) {
			HashMap<String, String> newRow = new HashMap<String, String>();
			newRow.put("Source", BaseUI.getTextFromField(tableRow.findElement(By.xpath("./td[2]"))));
			newRow.put("Sample", BaseUI.getTextFromField(tableRow.findElement(By.xpath("./td[3]"))));
			Select destination = new Select(tableRow.findElement(By.xpath("./td[4]//select")));
			newRow.put("Destination", BaseUI.getTextFromField(destination.getFirstSelectedOption()));

			tableData.data.add(newRow);
		}

		return tableData;
	}

}// end of class
