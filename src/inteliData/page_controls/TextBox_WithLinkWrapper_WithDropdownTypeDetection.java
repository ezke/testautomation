package inteliData.page_controls;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;

public class TextBox_WithLinkWrapper_WithDropdownTypeDetection extends TextBox_WithLinkWrapper {

    protected TextBox_WithLinkWrapper_WithDropdownTypeDetection(String identifyingText) {
        _identifyingText = identifyingText;
    }

    protected TextBox_WithLinkWrapper_WithDropdownTypeDetection(String searchTextBox, String listOfEntriesElement) {
        _searchTextBoxElement = searchTextBox;
        _listOfEntriesElement = listOfEntriesElement;
    }


    public static TextBox_WithLinkWrapper_WithDropdownTypeDetection getTextBox_ByIdentifyingText(String identifyingText) {

        return new TextBox_WithLinkWrapper_WithDropdownTypeDetection(identifyingText);
    }

    public static TextBox_WithLinkWrapper_WithDropdownTypeDetection getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier(String searchTextBox, String listOfEntriesElement) {

        return new TextBox_WithLinkWrapper_WithDropdownTypeDetection(searchTextBox, listOfEntriesElement);
    }

    //Pass in Strings in format of "CITY, ST"
    //Example "ACKWORTH, IA"
    @Override
    public void enterText(String textToInput) throws Exception {
        String city =  textToInput.split("\\,")[0];
        String state =  textToInput.split("\\,")[1].trim();

        WebElement elementToEnterTextInto = get_ElementToEnterTextInto();
        BaseUI.enterText_IntoInputBox(elementToEnterTextInto, city);
        WebElement itemToPick = BaseUI.waitForElementToBeDisplayed("txtLinkWrapDrop_ListItem", city, state);
        BaseUI.click(itemToPick);
        Thread.sleep(500);
    }


}
