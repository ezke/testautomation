package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.BaseUI;

public class Checkbox_InDraggableContainer extends CheckboxBase{

//	public String elementWithClassToCheck = "chckboxLabel_div_Element_By_LabelText";
//	public String elementWithLabelToClick = "chckboxLabel_ClickableLabel_By_LabelText";
	
//	private String label_text;
//
	public Checkbox_InDraggableContainer(String labelText) {
		label_text = labelText;
		elementWithClassToCheck = "dragCheck_li_Element_By_LabelText";
		elementWithLabelToClick = "dragCheck_ClickableLabel_By_LabelText";
	}

	@Override
	public void wait_ForCheckboxAction() throws Exception {
		Thread.sleep(2000);
		BaseUI.wait_forPageToFinishLoading();
	}
	
//	public void check_Checkbox() throws Exception {
//		WebElement checkboxSelectedLI = Locator.lookupElement("dragCheck_li_Element_By_LabelText", label_text, null);
//
//		if (!checkboxSelected(checkboxSelectedLI)) {
//			BaseUI.click(Locator.lookupElement("dragCheck_ClickableLabel_By_LabelText", label_text, null));
//			Thread.sleep(2000);
//			BaseUI.wait_forPageToFinishLoading();
//		}
//
//	}

	@Override
	public Boolean checkboxSelected(WebElement checkboxElementWithSelectedClass) {
		String classValue = BaseUI.get_Attribute_FromField(checkboxElementWithSelectedClass, "class");
		if (classValue == null || classValue.equals("")) {
			return false;
		} else {
			return true;
		}
	}
}// End of Class