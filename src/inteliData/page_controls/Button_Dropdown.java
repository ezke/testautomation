package inteliData.page_controls;

import java.util.ArrayList;

import utils.BaseUI;
import utils.Locator;

public class Button_Dropdown {

	// private String create_ID = "";
	public ArrayList<Button_Dropdown_ListItem> dropdownItems;
	private String dropdownButtonText = "";
	
	public Button_Dropdown(ArrayList<Button_Dropdown_ListItem> dropdown_Items, String buttonText) {
		dropdownItems = dropdown_Items;
		dropdownButtonText = buttonText;
		// create_ID = elementID;
	}

	public void click_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("createBtn_CreateButton", dropdownButtonText, null));
		BaseUI.waitForElementToBeDisplayed("createBtn_FirstDropdownLink", dropdownButtonText, null);
		Thread.sleep(200);
	}

	public void click_DropdownLink(String linkToClick) throws Exception {
		Button_Dropdown_ListItem itemToUse = null;
		for (Button_Dropdown_ListItem listItem : dropdownItems) {
			if (listItem.linkText.equals(linkToClick)) {
				itemToUse = listItem;
			}
		}
		if (itemToUse == null) {
			BaseUI.log_AndFail("Unable to find link " + linkToClick + " for this page.");
		}
		itemToUse.click_DropdownLink(dropdownButtonText);

	}

	public void wait_ForPageToLoad() throws Exception {

	}

}
