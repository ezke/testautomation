package inteliData.page_controls;

import java.text.MessageFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class Table_WithLinks extends TableBase {



	public Table_WithLinks(String idOfTable) {
		table_ID = idOfTable;
	}

	@Override
	public TableData return_TableData() {
		TableData table_Data = new TableData();

		String[] tableHeaders = return_TableHeaders();
		table_Data = BaseUI.tableExtractor_ByCell("table_tableCells_ByTableID", table_ID, null, tableHeaders);
		table_Data.remove_Character("\n");
		table_Data.remove_Character("\t");
		return table_Data;
	}

	// Indexing starts at 1.
	public void clickLink_ByRowIndex_ColumnName(Integer index, String columnName) throws Exception {
		String xpath = "//*[@id=\"{0}\"]//tbody/tr[{1}]/td[count(//*[@id=\"{0}\"]//thead/tr/th[./text()=\"{2}\"]/preceding-sibling::th) + 1]//a";
		xpath = MessageFormat.format(xpath, table_ID, index.toString(), columnName);
		By elementLocatorBy = By.xpath(xpath);
		WebElement linkElement = Locator.findElement(elementLocatorBy);
		BaseUI.click(linkElement);
		wait_AfterClick();
	}

	public void wait_AfterClick() throws Exception {
	}

}// End of Class
