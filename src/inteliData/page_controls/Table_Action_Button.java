package inteliData.page_controls;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Table_Action_Button {

	private String tableID = "";
	private Integer rowNumber = null;
	private String actionToPick = "";

	// rowNumber starts at 1.
	public Table_Action_Button(String table_ID, Integer row_Number, String action_ToPick) {
		tableID = table_ID;
		rowNumber = row_Number;
		actionToPick = action_ToPick;
	}

	// perform the entire chain of events including getting the action dropdown to
	// appear.
	public void click_ActionButton() throws Exception {
		hover_OverRow_UntilActionButtonAppears();
		click_ActionButton_ToDisplayDropdown();
		click_Action_ListItem();
	}

	//Indexing starts at 1
	private void hover_OverRow_UntilActionButtonAppears() throws Exception {
		BaseUI.elementHover(Locator.lookupElement("table_tableRow_ByTableID_AndIndex", tableID, rowNumber.toString()));
		BaseUI.waitForElementToBeDisplayed("actionButton_ByTableID_AndRowIndex", tableID, rowNumber.toString(), 7);
		Thread.sleep(200);
	}

	private void click_ActionButton_ToDisplayDropdown() throws Exception {
		BaseUI.click(Locator.lookupElement("actionButton_ByTableID_AndRowIndex", tableID, rowNumber.toString()));
		BaseUI.waitForElementToBeDisplayed("actionButton_FirstListItem", tableID, rowNumber.toString());
		Thread.sleep(200);

	}

	private void click_Action_ListItem() {
		WebElement elementWeClick = null;
		ArrayList<WebElement> availableListItems = Locator.lookup_multipleElements("actionButton_AllListItems", tableID,
				rowNumber.toString());
		for (WebElement availableItem : availableListItems) {
			if (BaseUI.getTextFromField(availableItem).contains(actionToPick)) {
				elementWeClick = availableItem;
				break;
			}
		}

		BaseUI.click(elementWeClick);
		// We'll implement a wait from the class we call this class from.
	}

}// End of Class
