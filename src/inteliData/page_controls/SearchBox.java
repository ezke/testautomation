package inteliData.page_controls;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

public class SearchBox {

	private String elementID = "";

	public SearchBox(String element_ID) {
		elementID = element_ID;
	}

	public void search_WithCriteria(String textToSearchFor) throws Exception {
		WebElement searchTextBox = Locator.lookupElement("searchBox_textbox", elementID, null);
		BaseUI.enterText_IntoInputBox(searchTextBox, textToSearchFor);
		BaseUI.enterKey(searchTextBox, Keys.TAB);
		wait_ForResultToPopulate(textToSearchFor);
	}

	public void wait_ForResultToPopulate(String textToSearchFor) throws Exception {
		Thread.sleep(750);
	}

}// End of Class
