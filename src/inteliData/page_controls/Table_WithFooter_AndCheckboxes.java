package inteliData.page_controls;

import java.util.HashMap;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

//Table has footer, checkboxes and all checkbox
public class Table_WithFooter_AndCheckboxes extends TableBase {

	public Checkbox_ForTable all_Checkbox = new Checkbox_ForTable(table_ID, "ALL") {
		@Override
		public WebElement elementTo_SeeIfChecked() {
			return Locator.lookupElement("createAud_AndSum_Records_SelectAll_Checkbox_ElementToCheck");
		}
		
		@Override
		public WebElement clickElement() {
			return Locator.lookupElement("createAud_AndSum_Records_SelectAll_Checkbox_ElementToClick");
		}
		
	};

	public Table_WithFooter_AndCheckboxes(String tableID) {
		tableBodyElement = "tableWithFooters_CellIdentifier";
		table_ID = tableID;
	}

	// Index starts at 1.
	public Checkbox_ForTable checkbox_ByIndex(Integer index) {
		return new Checkbox_ForTable(table_ID, index.toString()) {
			@Override
			public void wait_ForCheckboxAction() throws Exception {
				Thread.sleep(1500);
			}
		};
	}

	// Assumes there will only be 1 row so it just returns the first row.
	public HashMap<String, String> return_footerRow() {
		TableData footerRows = new TableData();

		String[] headers = return_TableHeaders();

		footerRows = BaseUI.tableExtractor_ByCell("tableFootAndCheck_footerCells", table_ID, null, headers);

		return footerRows.data.get(0);
	}

	// Index starts at 1.
	// Assumes there will only ever be one Input text box.
	public WebElement textBox_ByRowIndex(Integer indexToPick) {
		return Locator.lookupElement("table_textbox_ByTableID_And_Index", table_ID, indexToPick.toString());
	}

	@Override
	public TableData return_TableData() {
		TableData table_Data = new TableData();

		String[] tableHeaders = return_TableHeaders();
		table_Data = BaseUI.tableExtractor_ByCell(tableBodyElement, table_ID, null, tableHeaders);

		return table_Data;
	}

}// end of class
