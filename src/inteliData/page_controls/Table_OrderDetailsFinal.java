package inteliData.page_controls;

import utils.BaseUI;
import utils.TableData;

//This table has checkboxes, though not the All checkbox.  
//Also has some custom xpathing and no table id.
public class Table_OrderDetailsFinal extends TableBase {

	private String checkbox_ElementToClick = "createAud_OrderDetailsFinal_Table_Chkbox_ElementToClick";
	String checkbox_ElementToCheck = "createAud_OrderDetailsFinal_Table_Chkbox_ElementToCheck";

	public Table_OrderDetailsFinal(String tableID) {
		tableBodyElement = "createAud_OrderDetailsFinal_TableCells";
		tableHeaderElement = "createAud_OrderDetailsFinal_TableHeaders";
		table_ID = tableID;
	}

	@Override
	public TableData return_TableData() {
		TableData table_Data;

		String[] tableHeaders = return_TableHeaders();
		table_Data = BaseUI.tableExtractor_ByCell(tableBodyElement, table_ID, null, tableHeaders);

		return table_Data;
	}


	// Index starts at 1.
	public Checkbox_ForTable checkbox_ByIndex(Integer index) {
		return new Checkbox_ForTable(table_ID, index.toString(), checkbox_ElementToCheck, checkbox_ElementToClick);
	}

	public void check_Checkbox_ByColumnHeader_AndValueMatch(String header, String valueToMatch) throws Exception {
		TableData tableData = return_TableData();
		Integer indexOfMatch = tableData.first_IndexOf(header, valueToMatch);

		checkbox_ByIndex(indexOfMatch + 1).check_Checkbox();
	}

	public void uncheck_Checkbox_ByColumnHeader_AndValueMatch(String header, String valueToMatch) throws Exception {
		TableData tableData = return_TableData();
		Integer indexOfMatch = tableData.first_IndexOf(header, valueToMatch);

		checkbox_ByIndex(indexOfMatch + 1).uncheck_Checkbox();
	}

}// End of Class
