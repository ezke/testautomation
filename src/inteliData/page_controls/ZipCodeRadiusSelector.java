package inteliData.page_controls;

import utils.BaseUI;
import utils.Locator;

public class ZipCodeRadiusSelector {

	public void enter_ZipCodeInfo(String zipCode, int zipMin, int zipMax) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("zipRadius_ZipCode_TextBox"), zipCode);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("zipRadius_ZipCode_Min_TextBox"), String.valueOf(zipMin));
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("zipRadius_ZipCode_Max_TextBox"), String.valueOf(zipMax));
	}

	public void select_ZipCode_InclusionOrExclusion(ZipRadiusInclusionLevel zipInclusionLevel) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("zipRadius_IncludeZips_Dropdown"),
				zipInclusionLevel.getValue());
	}

	public void verify_ZipRadiusFields(String zipCode, int zipMin, int zipMax, ZipRadiusInclusionLevel inclusionLevel) {
		String zipCodeActualText = BaseUI.getTextFromInputBox(Locator.lookupElement("zipRadius_ZipCode_TextBox"));
		String zipCode_Min_ActualText = BaseUI
				.getTextFromInputBox(Locator.lookupElement("zipRadius_ZipCode_Min_TextBox"));
		String zipCode_Max_ActualText = BaseUI
				.getTextFromInputBox(Locator.lookupElement("zipRadius_ZipCode_Max_TextBox"));

		BaseUI.baseStringCompare("Zip Code", zipCode, zipCodeActualText);
		BaseUI.baseStringCompare("Zip Code Min Radius", String.valueOf(zipMin), zipCode_Min_ActualText);
		BaseUI.baseStringCompare("Zip Code Max Radius", String.valueOf(zipMax), zipCode_Max_ActualText);
		BaseUI.verify_Value_Selected_InDropdown(Locator.lookupElement("zipRadius_IncludeZips_Dropdown"),
				inclusionLevel.getValue());
	}

	public enum ZipRadiusInclusionLevel {
		INCLUDE_ALL_ZIPS("Include all zips and states from this radius"), INCLUDE_ONLY_THE_STATE(
				"Include only the state from the entered zip code"), EXCLUDE_THE_STATE(
						"Exclude the state from the entered zip code"),;

		private String value;

		private ZipRadiusInclusionLevel(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

}// End of Class
