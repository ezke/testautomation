package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Multiselect_ButtonDropdown {

	String label_Identifier;

	public Multiselect_ButtonDropdown(String labelIdentifier) {
		label_Identifier = labelIdentifier;
	}

	public WebElement dropdown_Button() {
		return Locator.lookupElement("multiselect_DropdownClickElement", label_Identifier, null);
	}

	public void verify_DropdownButton_Text_Accurate(String expectedText) {
		String dropdownButtonText = BaseUI.getTextFromField(dropdown_Button()).trim();
		BaseUI.baseStringCompare("Multiselect with label " + label_Identifier, expectedText, dropdownButtonText);
	}

}// End of Class
