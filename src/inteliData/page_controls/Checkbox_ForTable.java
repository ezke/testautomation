package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.Locator;

public class Checkbox_ForTable extends CheckboxBase {

	private String table_ID;
	private String chckbox_identifier;

	// identifier could either be the row index or "ALL" to indicate it's the "ALL"
	// checkbox
	public Checkbox_ForTable(String tableID, String identifier) {
		chckbox_identifier = identifier;
		table_ID = tableID;

		if (identifier.equals("ALL")) {
			elementWithClassToCheck = "chckbox_Tables_ElementToCheck_ALL";
			elementWithLabelToClick = "chckbox_Tables_ElementToClick_ALL";
		} else {
			elementWithClassToCheck = "chckbox_Tables_ElementToCheck_ByIndex";
			elementWithLabelToClick = "chckbox_Tables_ElementToClick_ByIndex";
		}
	}

	// This overload is for passing in the element identifiers, this one doesn't
	// allow for an ALL, could be added to a different overload.
	public Checkbox_ForTable(String tableID, String identifier, String elementToCheckIdentifier,
			String elementToClickIdentifier) {
		chckbox_identifier = identifier;
		table_ID = tableID;

		elementWithClassToCheck = elementToCheckIdentifier;
		elementWithLabelToClick = elementToClickIdentifier;
	}

	// Table's without ID's will use this overload
	public Checkbox_ForTable(String identifier, String elementToCheckIdentifier, String elementToClickIdentifier) {
		chckbox_identifier = identifier;

		elementWithClassToCheck = elementToCheckIdentifier;
		elementWithLabelToClick = elementToClickIdentifier;
	}

	@Override
	public WebElement elementTo_SeeIfChecked() {
		if (table_ID != null) {
			return Locator.lookupElement(elementWithClassToCheck, table_ID, chckbox_identifier);
		} else {
			return Locator.lookupElement(elementWithClassToCheck, chckbox_identifier, null);
		}
	}

	@Override
	public WebElement clickElement() {
		if (table_ID != null) {
			return Locator.lookupElement(elementWithLabelToClick, table_ID, chckbox_identifier);
		} else {
			return Locator.lookupElement(elementWithLabelToClick, chckbox_identifier, null);
		}
	}

}
