package inteliData.page_controls;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class DataPanel_MainContent extends DataPanelBase {

	private String headerElementLocator = "dataPanel_main_CellHeaders_ByMainHeader";
	private String cellElementLocator = "dataPanel_main_CellTexts_ByMainHeader";
	private String specificCellTextLocator = "dataPanel_main_CellTexts_ByMainHeader_AndCellHeader";

	public DataPanel_MainContent(String header_Text) {
		headerText = header_Text;
	}

	// Treats the headerText like it was an Id. Currently being used by First Party
	// Data Sources since they decided to have a new hybrid data panel type here.
	public DataPanel_MainContent(String id, String header_ElementLocator, String cell_ElementLocator,
			String specific_CellTextLocator) {
		headerText = id;
		headerElementLocator = header_ElementLocator;
		cellElementLocator = cell_ElementLocator;
		specificCellTextLocator = specific_CellTextLocator;

	}

	// reference this method from page object and add wait logic there.
	public void click_Link_ByHeader(String cellHeaderText) {
		BaseUI.click(Locator.lookupElement("dataPanel_main_CellLink_ByMainHeader_AndCellHeader", headerText,
				cellHeaderText));
	}

	@Override
	public ArrayList<WebElement> returnRowHeaders() {
		return Locator.lookup_multipleElements(headerElementLocator, headerText, null);
	}

	@Override
	public ArrayList<WebElement> returnRowCells() {
		return Locator.lookup_multipleElements(cellElementLocator, headerText, null);
	}

	@Override
	public String return_SpecificValue(String cellheaderText) {
		String textToReturn = BaseUI
				.getTextFromField(Locator.lookupElement(specificCellTextLocator, headerText, cellheaderText));

		textToReturn = textToReturn.split("\\n")[1];
		return textToReturn.trim();
	}

	// @Override
	// public HashMap<String, String> return_PanelData() {
	// ArrayList<WebElement> rowHeaders =
	// Locator.lookup_multipleElements("dataPanel_main_CellHeaders",
	// null, null);
	// ArrayList<WebElement> rowCells =
	// Locator.lookup_multipleElements("dataPanel_main_CellTexts",
	// null, null);
	//
	// HashMap<String, String> panelData = new HashMap<String, String>();
	// for (int i = 0; i < rowHeaders.size(); i++) {
	// panelData.put(BaseUI.getTextFromField(rowHeaders.get(i)),
	// BaseUI.getTextFromField(rowCells.get(i)).split("\\n")[1].trim());
	// }
	//
	// return format_Data(panelData);
	// }
	//
	@Override
	public HashMap<String, String> format_Data(HashMap<String, String> unformattedMappings) {
		HashMap<String, String> panelData_Formatted = new HashMap<String, String>();

		for (String key : unformattedMappings.keySet()) {
			String keyValue = "";
			try {
				keyValue = unformattedMappings.get(key).split("\\n")[1].trim();
			} catch (java.lang.ArrayIndexOutOfBoundsException e) {
				keyValue = "";
			}
			panelData_Formatted.put(key, keyValue);
		}

		return panelData_Formatted;
	}

}
