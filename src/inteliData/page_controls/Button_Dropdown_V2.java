package inteliData.page_controls;

import utils.BaseUI;
import utils.Locator;

//Trying out a different design.  Plan on phasing out/replacing original
public class Button_Dropdown_V2 {

	private String dropdownElementIdentifier;
	private String listItemIdentifier;
	private String listItemText;

	public Button_Dropdown_V2(String dropdownIdentifier, String list_ItemIdentifier, String list_ItemText) {
		dropdownElementIdentifier = dropdownIdentifier;
		listItemIdentifier = list_ItemIdentifier;
		listItemText = list_ItemText;

	}

	public void click_listItem() {
		BaseUI.click(Locator.lookupElement(listItemIdentifier, listItemText, null));
	}

	public void openList() throws Exception {
		BaseUI.click(Locator.lookupElement(dropdownElementIdentifier));
		BaseUI.waitForElementToBeDisplayed(listItemIdentifier, listItemText, null, 5);
		Thread.sleep(200);
	}

	public void open_List_AndClickItem() throws Exception {
		openList();
		click_listItem();
	}

}// End of class
