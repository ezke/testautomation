package inteliData.page_controls;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import utils.BaseUI;

public class DataPanel_NoCellHeaders extends DataPanelBase {

	private String key_text;

	// keyText will be the beginning of the key text, additional keys will have
	// numbers added.
	// will look something like format0, format1, format2, etc.
	public DataPanel_NoCellHeaders(String expectedHeaderText, String keyText) {
		headerText = expectedHeaderText;
		key_text = keyText;
	}

	public HashMap<String, String> return_PanelData() {
		ArrayList<WebElement> rowCells = returnRowCells();
		HashMap<String, String> panelData = new HashMap<String, String>();
		for (Integer i = 0; i < rowCells.size(); i++) {
			panelData.put(key_text + i.toString(),
					BaseUI.get_Attribute_FromField(rowCells.get(i), "innerText"));
		}
		
		return panelData;
	}

}// End of Class
