package inteliData.page_controls;

import utils.BaseUI;
import utils.TableData;

public class Table_SuppressionsPage extends TableBase {

	public Table_SuppressionsPage(String tableID) {
		table_ID = tableID;
	}

	// Index starts at 1.
	public Checkbox_ForTable checkbox_ByIndex(Integer index) {
		return new Checkbox_ForTable(table_ID, index.toString());
	}

	// rowToPick indexing starts at 1.
	public void click_View_ForRow(Integer rowToPick) throws Exception {
		Table_Action_Button newButton = new Table_Action_Button(table_ID, rowToPick, "View");
		newButton.click_ActionButton();
		wait_ForView_PageToLoad();
	}

	public void wait_ForView_PageToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("suppressionDisplay_IdCell", null, null, 20);
		Thread.sleep(200);
	}

	// rowToPick indexing starts at 1.
	public void click_Delete_ForRow(Integer rowToPick) throws Exception {
		Table_Action_Button newButton = new Table_Action_Button(table_ID, rowToPick, "Delete");
		newButton.click_ActionButton();
		BaseUI.waitForElementToBeDisplayed("suppressions_DeleteItemModal_OK_Button", null, null, 40);
		Thread.sleep(1500);


	}

	@Override
	public TableData return_TableData() {
		TableData table_Data = new TableData();

		String[] tableHeaders = return_TableHeaders();

		table_Data = BaseUI.tableExtractor_ByCell("suppressionDisplay_TableCells", table_ID, null, tableHeaders);

		return table_Data;
	}

}// End of Class
