package inteliData.page_controls;

import utils.BaseUI;
import utils.Locator;

//Class for dealing with tabs at the top of page
public class TabSwitch {

	String tabText;


	public TabSwitch(String tab_Text) {
		tabText = tab_Text;
	}

	public void switchTab() throws Exception {
		BaseUI.click(Locator.lookupElement("tabSwitch_ElementToClick_ByText", tabText, null));
		//BaseUI.waitForElementToBeDisplayed(elementIdentifierToWaitFor, null, null);
		wait_ForTabSwitch();
	}


	public void wait_ForTabSwitch() throws Exception {
		BaseUI.waitForElementToContain_PartialAttributeMatch("tabSwitch_ElementToWaitFor", tabText, null, "class", "active", 20);
		Thread.sleep(500);
	}
}
