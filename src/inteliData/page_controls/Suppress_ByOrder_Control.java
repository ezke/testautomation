package inteliData.page_controls;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Suppress_ByOrder_Control {

	private String controlText = "Suppress by order";

	public CheckboxBase supressByOrderCheckbox = new CheckboxBase(controlText);

	public Suppress_ByOrder_Control(String control_Text) {
		controlText = control_Text;
	}

	// Scrolling on this page is wonky, I recommend entering search text first
	// before checking this checkbox.
	public void check_Checkbox() throws Exception {
		WebElement checkbox = Locator.lookupElement("suppressByOrder_Checkbox", controlText, null);
		BaseUI.checkCheckbox(checkbox);
	}

	public void enter_Text_IntoSearchBox(String textToEnter) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("suppressByOrder_Searchbox", controlText, null),
				textToEnter);
	}

	public void search_for_Text(String textToEnter) throws Exception {
		enter_Text_IntoSearchBox(textToEnter);
		click_SearchButton();
	}

	public void search_For_And_Add_FirstSelection_ToSelectedList(String textToEnter) throws Exception {
		search_for_Text(textToEnter);
		select_First_Option_InSelectionList();
		click_Add_Button();
	}

	public void click_Add_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("suppressByOrder_AddButton", controlText, null));
		Thread.sleep(200);
	}

	public void select_First_Option_InSelectionList() {
		BaseUI.selectValueFromDropDownByIndex(Locator.lookupElement("suppressByOrder_SelectionBox", controlText, null),
				0);
	}

	public String return_First_Option_InSelectedList() {
		String selectedText = BaseUI
				.getTextFromField(Locator.lookupElement("suppressByOrder_SelectedBox_FirstOption", controlText, null));
		return selectedText;
	}

	// Assumes there will be an option after searching
	public void click_SearchButton() throws Exception {
		BaseUI.click(Locator.lookupElement("suppressByOrder_SearchButton", controlText, null));
		Thread.sleep(500);
		BaseUI.waitForElementToBeDisplayed("suppressByOrder_SelectionBox_FirstOption", controlText, null, 120);
		Thread.sleep(500);
	}

}// End of class
