package inteliData.page_controls;

import utils.BaseUI;
import utils.Locator;

public class Button_Dropdown_ListItem {

	public String linkText;

	public Button_Dropdown_ListItem(String link_Text) {
		linkText = link_Text;
	}

	public void click_DropdownLink(String dropdownText) throws Exception {
		BaseUI.click(Locator.lookupElement("createBtn_DropdownLink_ByText", dropdownText, linkText));
		wait_ForPageToLoad();
	}
	
	public void wait_ForPageToLoad() throws Exception {

	}

}
