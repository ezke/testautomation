package inteliData.page_controls;


import org.openqa.selenium.*;

import utils.BaseUI;

import utils.Locator;

import java.io.File;


public class ZipCodes_ChooseFile {

    private String _chooseFileElementLocator;

    public ZipCodes_ChooseFile(String chooseFileElementLocator) {
        _chooseFileElementLocator = chooseFileElementLocator;
    }

    public void chooseFile(String fileNameAndLocation) throws Exception {

        if (fileNameAndLocation.startsWith("\\src")) {
            fileNameAndLocation = System.getProperty("user.dir") + fileNameAndLocation;
        }

        WebElement chooseFileButton = Locator.lookupRequiredElement(_chooseFileElementLocator);

        BaseUI.scroll_element_to_middleOfScreen(chooseFileButton);

        BaseUI.DropFile(new File(fileNameAndLocation),chooseFileButton, 0, 0);

        wait_ForModalToAppear();
    }

    private void wait_ForModalToAppear() throws Exception {
        BaseUI.waitForElementToBeDisplayed("zipCodeChooseFile_ModalErrorMessage", null, null, 20);
        Thread.sleep(500);
    }

    public void click_OK() throws Exception {
        BaseUI.click(Locator.lookupElement("zipCodeChooseFile_ModalOKButton"));
        BaseUI.waitForElementToNOTBeDisplayed("zipCodeChooseFile_ModalOKButton", null, null, 20);
        Thread.sleep(1000);
    }

    public void verify_ErrorTextDisplayedAndAccurate(String expectedText) {
        WebElement messageErrorElement = Locator.lookupRequiredElement("zipCodeChooseFile_ModalErrorMessage", null, null);
        BaseUI.verifyElementAppears(messageErrorElement);
        BaseUI.verifyElementHasExpectedText(messageErrorElement, expectedText);
    }


}//End of class
