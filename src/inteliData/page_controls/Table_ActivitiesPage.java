package inteliData.page_controls;

import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_SearchReview;
import utils.BaseUI;

public class Table_ActivitiesPage extends TableBase {

	public Table_ActivitiesPage(String tableID) {
		table_ID = tableID;
	}

	// Index starts at 1.
	public Checkbox_ForTable checkbox_ByIndex(Integer index) {
		return new Checkbox_ForTable(table_ID, index.toString());
	}

	// rowToPick indexing starts at 1.
	public void click_RecreateAudience_ForRow(Integer rowToPick) throws Exception {
		Table_Action_Button newButton = new Table_Action_Button(table_ID, rowToPick, "Recreate Audience");
		newButton.click_ActionButton();
		CreateAudience_SearchReview.wait_ForSearchReviewPage_ToLoad();
	}

	// rowToPick indexing starts at 1.
	public void click_View_ForRow(Integer rowToPick) throws Exception {
		Table_Action_Button newButton = new Table_Action_Button(table_ID, rowToPick, "View");
		newButton.click_ActionButton();
		CreateAudience_DownloadRecords_Page.wait_ForPageToLoad();
	}

	// rowToPick indexing starts at 1.
	public void click_ViewAudience_ForRow(Integer rowToPick) throws Exception {
		Table_Action_Button newButton = new Table_Action_Button(table_ID, rowToPick, "View Audience");
		newButton.click_ActionButton();
		CreateAudience_SearchOrderRecords.wait_ForPage_ToLoad();
	}

}// End of Class
