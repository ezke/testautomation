package inteliData.page_controls;

import java.util.HashMap;

import utils.BaseUI;
import utils.TableData;

//Table has footer
public class Table_WithFooter extends TableBase {

	String tableFooterElement = "tableFootAndCheck_footerCells";

	public Table_WithFooter(String tableID) {
		table_ID = tableID;
	}
	
	public Table_WithFooter(String table_BodyElement, String table_HeaderElement, String table_FooterElement) {
		tableBodyElement = table_BodyElement;
		tableHeaderElement = table_HeaderElement;
		tableFooterElement = table_FooterElement;
	}

	// Assumes there will only be 1 row so it just returns the first row.
	public HashMap<String, String> return_footerRow() {
		TableData footerRows = new TableData();

		String[] headers = return_TableHeaders();

		footerRows = BaseUI.tableExtractor_ByCell(tableFooterElement, table_ID, null, headers);

		return footerRows.data.get(0);
	}


}// end of class
