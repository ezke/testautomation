package inteliData.tests;

import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.pages.Activities;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.Programs;
import inteliData.pages.SearchDisplay;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Running_A_Program_Tests extends baseTest {

	String programID = "11125266";

	String audienceID = "";

	String mart = "Daily Triggers";
	String account = "ACME";
	String marketer = "1";

	String name = "QA Automation Test - DO NOT MODIFY";
	String[] states = { "RI" };

	String timeInitiated = "";

	Integer expectedRecords = 100;

	String poNumber = "QA Automation Program ";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		if(GlobalVariables.environment.equals("DEV")){
			expectedRecords = 4;
		}

		poNumber += BaseUI.getDateAsString_InRelationToTodaysDate(0, "Mdyyyy");

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_Programs();
		Programs.allAcquisitionTab.switchTab();
		Programs.acquisitionQuickSearchBox.search_WithCriteria(programID);
		Programs.allAcquisitionTable.click_View_ForRow(1);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT268_ProgramDisplay_Validate_AutoOrderSettings() throws Exception {
		HashMap<String, String> autoOrderExpectedValues = new HashMap<String, String>();
		autoOrderExpectedValues.put("Select results", "Nth all search results to 100 records");
		autoOrderExpectedValues.put("Auto ship", "Yes");
		autoOrderExpectedValues.put("PO number format", "QA Automation Program !date");
		autoOrderExpectedValues.put("File format type", "CSV");
		autoOrderExpectedValues.put("Outputs for auto order", "FICO");

		ProgramDisplay.autoOrderSettings_Panel.verify_ExpectedData_Matches_ReturnedData(autoOrderExpectedValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT268_ProgramDisplay_Validate_GeneralInformationPanel() throws Exception {
		HashMap<String, String> generalInformationExpectedValues = new HashMap<String, String>();
		generalInformationExpectedValues.put("One Per Household", "Yes");
		generalInformationExpectedValues.put("Unique Phones Only", "No");
		generalInformationExpectedValues.put("Supplier (s)", "TransUnion (1), Experian (2)");
		generalInformationExpectedValues.put("Usage Types", "Direct Mail");

		ProgramDisplay.generalInformation_Panel
				.verify_ExpectedData_Matches_ReturnedData(generalInformationExpectedValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT268_ProgramDisplay_Validate_Level1Panel() throws Exception {
		HashMap<String, String> level1ExpectedValues = new HashMap<String, String>();
		level1ExpectedValues.put("Geography", "Use global geography");
		level1ExpectedValues.put("Mortgage trigger", "Yes");
		level1ExpectedValues.put("FICO risk score (FICO)", "From 500 To 525");

		ProgramDisplay.level1_Panel.verify_ExpectedData_Matches_ReturnedData(level1ExpectedValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT268_ProgramDisplay_Validate_Level2Pane1() throws Exception {
		HashMap<String, String> level2ExpectedValues = new HashMap<String, String>();
		level2ExpectedValues.put("Geography", "Use global geography");
		level2ExpectedValues.put("Automotive trigger", "Yes");
		level2ExpectedValues.put("FICO risk score (FICO)", "From 600 To 625");

		ProgramDisplay.level2_Panel.verify_ExpectedData_Matches_ReturnedData(level2ExpectedValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 20)
	public void PT268_ProgramDisplay_RunProgramAlertAppears() throws Exception {
		timeInitiated = BaseUI.getDateAsString_InRelationToTodaysDate(0,
				CreateAudience_DownloadRecords_Page.dateFormat);
		ProgramDisplay.click_RunProgram();
		audienceID = ProgramDisplay.return_AudienceID_From_RunProgramAlert();
		ProgramDisplay.verify_RunProgramAlert(audienceID);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 30)
	public void PT268_ProgramDisplay_ClickView_ForAudience_VerifyAudienceSummary_OnDownloadRecordsPage()
			throws Exception {
		TableData audiences = ProgramDisplay.audiencesTable.return_TableData();
		int indexWeWant = audiences.first_IndexOf("Id", audienceID) + 1;
		ProgramDisplay.click_Audiences_ViewLink_ByIndex(indexWeWant);

		if (SearchDisplay.atSearchDisplayPage()) {
			SearchDisplay.refreshUntil_AtLeast1RowPresent();
			
			String orderID = SearchDisplay.return_OrderId_ForFirstRow();
			Navigation.navigate_Activities();
			Activities.click_Tab_AllOrders();
			Activities.allOrders_SearchBox.search_WithCriteria(orderID);
			Activities.allOrders_ActivitiesTable.click_View_ForRow(1);
		}

		CreateAudience_DownloadRecords_Page.verify_AudienceSummaryPanel(Integer.parseInt(audienceID), name, mart,
				account, marketer, timeInitiated);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT268_DownloadRecordsPage_VerifyOrderDetailsPanel() throws Exception {
		CreateAudience_DownloadRecords_Page.refresh_Until_Shipped();
		CreateAudience_DownloadRecords_Page.verify_OrderDetailsPanel(poNumber, timeInitiated,
				expectedRecords.toString(), null, true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 31)
	public void PT268_DownloadRecords_VerifyCSVFile() throws Exception {
		String typeName = "CSV (Order File)";
		String fileName = "";
		HashMap<String, Range<Integer>> levelRanges = new HashMap<String, Range<Integer>>();
		levelRanges.put("1", Range.between(500, 525));
		levelRanges.put("2", Range.between(600, 625));

		TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
		fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
		TableData csvTable = CreateAudience_DownloadRecords_Page
				.download_File_FromDownloadRecords(indexOfLineToDownload + 1, fileName);
		CreateAudience_DownloadRecords_Page.verify_RunProgram_CSV_MatchesExpected(csvTable, states, levelRanges);
		csvTable.verify_TableSize(expectedRecords);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
