package inteliData.tests;

import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.DataSource_Modal;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.DataSourceImport;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.FirstParty_DataSources_Display;
import inteliData.pages.FirstParty_Mappings;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.Programs;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class FirstPartyData_ProductTemplate_Tests extends baseTest {

	String name = "Automation - DataSource";
	String account = "ACME";
	String marketer = "1";
	String expectedMarketer = "1 (15899)";
	String template = "Product";
	int expectedRecords = 17;
	int expectedErrors = 9;

	String file = "\\src\\inteliData\\data\\ProductsTest1.csv";
	String fileName = "ProductsTest1.csv";

	HashMap<String, String> expectedMappings;

	String id = "";
	String marketer_TableKey = "";

	TableData products;
	TableData productPassThroughs;
	TableData productValidationErrors;

	TableData csvToCheckAgainst;
	TableData csvToCheckAgainst_reformatted;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings = new HashMap<String, String>();
		expectedMappings.put("ProductGroup", "Product Group");
		expectedMappings.put("Level1", "Level 1");
		expectedMappings.put("Level2", "Level 2");
		expectedMappings.put("Level3", "Level 3");
		expectedMappings.put("ProductDescription", "Product Description");
		expectedMappings.put("Active", "Active Indicator");
		expectedMappings.put("PT1", "");
		expectedMappings.put("PT2", "");

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.click_CreateButton();
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer, template);
		DataSourceImport.chooseFile(file, true);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 10)
	public void PT272_FirstPartyMappings_VerifyDetailsPanel() throws Exception {
		id = FirstParty_Mappings.data_Panel.return_SpecificValue("Id");
		FirstParty_Mappings.verify_detailsPanel_FirstPartyData(name, fileName, expectedMarketer, expectedRecords);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 15)
	public void PT272_FirstPartyMappings_VerifyDefaultMappings() throws Exception {
		TableData mappingsTable = FirstParty_Mappings.mapTable.return_TableData();
		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 16)
	public void PT272_FirstPartyMappings_ClickMapRemainingAsPassThrough_VerifyTableUpdated() throws Exception {
		expectedMappings.put("PT1", "Pass Through");
		expectedMappings.put("PT2", "Pass Through");
		FirstParty_Mappings.click_MapRemainingAsPassThrough();
		TableData mappingsTable = FirstParty_Mappings.mapTable.return_TableData();

		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
		FirstParty_Mappings.mapTable.checkbox_ByIndex(mappingsTable.first_IndexOf("Source", "PT1") + 1)
				.verify_CheckboxChecked();
		FirstParty_Mappings.mapTable.checkbox_ByIndex(mappingsTable.first_IndexOf("Source", "PT2") + 1)
				.verify_CheckboxChecked();
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 20)
	public void PT272_ConfirmMappings_VerifyConfirmMappingTable() throws Exception {
		FirstParty_Mappings.click_Map_Button();
		ConfirmMappedFields.verify_Mappings(expectedMappings);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 21)
	public void PT272_ConfirmMappings_Verify_DetailsPanel() throws Exception {
		ConfirmMappedFields.verify_DetailsPanel_ForFirstPartyData_MatchesExpected(id, name, fileName, expectedRecords,
				expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 30)
	public void PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		FirstParty_DataSources_Display.refresh_Until_Mapped();
		marketer_TableKey = FirstParty_DataSources_Display.return_MarketerKey();
		FirstParty_DataSources_Display.verify_DetailsPanel(id, name, fileName, expectedRecords, expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 31, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT272_FirstPartyDataSourcesDisplay_VerifyErrorsPanel() throws Exception {
		String addressStandards = "N/A";
		String latLongMatch = "N/A";

		// populate DB's
		products = FirstParty_DataSources_Display.return_products(id, marketer_TableKey);
		productPassThroughs = FirstParty_DataSources_Display.return_productPassThroughs(id, marketer_TableKey);
		productValidationErrors = FirstParty_DataSources_Display.return_productValidationErrors(id, marketer_TableKey);
		csvToCheckAgainst = FirstParty_DataSources_Display.return_CSVFile_ToCheckAgainst(file);
		csvToCheckAgainst_reformatted = FirstParty_DataSources_Display
				.format_CSVFile_ToMatchDB_ForProducts(csvToCheckAgainst.cloneTable());

		FirstParty_DataSources_Display.verify_ErrorsPanel(expectedErrors, addressStandards, latLongMatch);
	}

	// Step 11
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT272_VerifyDatabase_csv_CorrectCount_OfEntries() throws Exception {
		csvToCheckAgainst_reformatted.verify_TableSize(expectedRecords);
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_PriorLevels_ArePopulated_if_CurrentLevelPopulated() throws Exception {
		FirstParty_DataSources_Display.verify_PriorLevelsPopulated_Error(products, productValidationErrors);
	}

	// Step 4
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_RowsWithNoProblems_DidntThrowErrors() throws Exception {
		String[] requiredFields = { "ProductGroupId", "Level1", "ProductDescription" };
		FirstParty_DataSources_Display.verify_products_RowsWithoutErrors_ThrewNoExceptions(products,
				productValidationErrors, csvToCheckAgainst, requiredFields);
	}

	// Step 5
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_ProductGroupId_Required_ThrowsError() throws Exception {
		FirstParty_DataSources_Display.verify_Field_Required_Null(products, productValidationErrors, "ProductGroupId",
				"GroupId");
	}

	// Step 6
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_Level1_Required_ThrowsError() throws Exception {
		FirstParty_DataSources_Display.verify_WhenFieldRequired_IfNullOrEmpty_ErrorInErrorTable(products,
				productValidationErrors, "Level1", "Level1", "ProductId");
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_ProductDescription_Required_ThrowsError() throws Exception {
		FirstParty_DataSources_Display.verify_WhenFieldRequired_IfNullOrEmpty_ErrorInErrorTable(products,
				productValidationErrors, "ProductDescription", "Description", "ProductId");
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_ErrorTable_VerifyCountOfUniqueProductIds() throws Exception {
		productValidationErrors.verify_CountOfUniqueColumnValues(expectedErrors, "ProductId");
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_ErrorTable_CorrectCountOfEntries() throws Exception {
		productValidationErrors.verify_TableSize(18);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT273_VerifyDatabase_ActiveColumn_ThrowsError_ForUnrecognizedValue() throws Exception {
		String incorrectValue = "blah";
		FirstParty_DataSources_Display.verify_product_Active_Option_ThatIsntAcceptable_ThrowsException(products,
				productValidationErrors, csvToCheckAgainst, incorrectValue, "ProductId");
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductsTable_Matches_CSV_for_ProductGroupColumn() throws Exception {
		BaseUI.verify_TableColumns_Match("ProductGroup", csvToCheckAgainst_reformatted.data, products.data);
	}

	// Step 8
	// Expecting 2 passthroughs per record.
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_PassThroughs_CorrectNumberOfEntries() throws Exception {
		productPassThroughs.verify_TableSize(expectedRecords * 2);
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductsTable_Level1_Matches_CSV() throws Exception {
		BaseUI.verify_TableColumns_Match("Level1", csvToCheckAgainst_reformatted.data, products.data);
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductsTable_Level2_Matches_CSV() throws Exception {
		BaseUI.verify_TableColumns_Match("Level2", csvToCheckAgainst_reformatted.data, products.data);
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductsTable_Level3_Matches_CSV() throws Exception {
		BaseUI.verify_TableColumns_Match("Level3", csvToCheckAgainst_reformatted.data, products.data);
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductsTable_ProductDescription_Matches_CSV() throws Exception {
		BaseUI.verify_TableColumns_Match("ProductDescription", csvToCheckAgainst_reformatted.data, products.data);
	}

	// Step 3
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductsTable_Active_Matches_CSV() throws Exception {
		BaseUI.verify_TableColumns_Match("Active", csvToCheckAgainst_reformatted.data, products.data);
	}

	// Step 5
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_productPassThroughs_PT_1_MatchesCSV() throws Exception {
		String passThrough = "PT1";
		FirstParty_DataSources_Display.verify_PassThrough_Value(products, productPassThroughs,
				csvToCheckAgainst_reformatted, passThrough, "ProductId");

	}

	// Step 6
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_productPassThroughs_PT_2_MatchesCSV() throws Exception {

		String passThrough = "PT2";
		FirstParty_DataSources_Display.verify_PassThrough_Value(products, productPassThroughs,
				csvToCheckAgainst_reformatted, passThrough, "ProductId");

	}

	// At some point they will be moving Product Groups table, this test will
	// probably fail when that change occurs.
	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT272_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT274_VerifyDatabase_ProductGroup_ProductGroupId_ShouldBeEmpty_WhenProductGroupEmpty()
			throws Exception {
		FirstParty_DataSources_Display.verify_ProductGroupIdEmpty_WhenProductGroupEmpty(products);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
