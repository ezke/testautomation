package inteliData.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.pages.Navigation;
import inteliData.data.Data_Import;
import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.pages.Account_ChangePassword;
import inteliData.pages.Account_ChangePassword.PasswordChangeError;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;

public class ChangePassword_Tests extends baseTest {

	String _originalPassword = "";
	String _newPassword;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.url);
		_originalPassword = Data_Import.returnPassword(GlobalVariables.user_PasswordReset);
		LoginPage.login(GlobalVariables.user_PasswordReset, _originalPassword);
		Navigation.navigate_ChangePassword();
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT294_UpdatePassword_Successfully() throws Exception {
		_newPassword = "Wausau#" + BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMdd")
				+ BaseUI.random_NumberAsString(1000, 3000);
		Account_ChangePassword.changePassword(_newPassword, _newPassword);

		Account_ChangePassword.verify_PasswordChange_Successful_AndIfSuccessful_UpdatePWFile(
				GlobalVariables.user_PasswordReset, _newPassword);
		LoginPage.login(GlobalVariables.user_PasswordReset, _newPassword);
		HomePage.verify_HomePage_Loaded();
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT294_UpdatePassword_Successfully_ShorterPassword() throws Exception {
		_newPassword = "Wausau#" + BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMdd")
				+ BaseUI.random_NumberAsString(100, 300);
		Account_ChangePassword.changePassword(_newPassword, _newPassword);

		Account_ChangePassword.verify_PasswordChange_Successful_AndIfSuccessful_UpdatePWFile(
				GlobalVariables.user_PasswordReset, _newPassword);
		LoginPage.login(GlobalVariables.user_PasswordReset, _newPassword);
		HomePage.verify_HomePage_Loaded();
	}



	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests"})
	public void PT295_UpdatePassword_CannotBeSameAs_CurrentPassword() throws Exception {
		String currentPassword = Data_Import.returnPassword(GlobalVariables.user_PasswordReset);
		Account_ChangePassword.changePassword(currentPassword, currentPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.CannotBeSameAsCurrent);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT296_UpdatePassword_CannotBeSameAs_OldPassword() throws Exception {
		_newPassword = "Wausau#" + BaseUI.getDateAsString_InRelationToTodaysDate(0, "MMdd")
				+ BaseUI.random_NumberAsString(0, 99);
		Account_ChangePassword.changePassword(_newPassword, _newPassword);
		Account_ChangePassword.verify_PasswordChange_Successful_AndIfSuccessful_UpdatePWFile(
				GlobalVariables.user_PasswordReset, _newPassword);
		LoginPage.login(GlobalVariables.user_PasswordReset, _newPassword);
		Navigation.navigate_ChangePassword();
		Account_ChangePassword.changePassword(_originalPassword, _originalPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.CannotBeSameAsLast5);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT297_UpdatePassword_CannotBe_LessThan8Characters() throws Exception {
		String newPassword = "Waus#12";
		Account_ChangePassword.changePassword(newPassword, newPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.CannotBeLessThan8Chars);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT299_UpdatePassword_MissingUppercaseLetter() throws Exception {
		String newPassword = "password#aa7";
		Account_ChangePassword.changePassword(newPassword, newPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.PasswordMustContain);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT300_UpdatePassword_Missing1digit() throws Exception {
		String newPassword = "Password#aa";
		Account_ChangePassword.changePassword(newPassword, newPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.PasswordMustContain);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT301_UpdatePassword_NoSpecialChars() throws Exception {
		String newPassword = "Passwordaa7";
		Account_ChangePassword.changePassword(newPassword, newPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.PasswordMustContain);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT302_UpdatePassword_NoLowerCaseLetters() throws Exception {
		String newPassword = "PASSWORD#7";
		Account_ChangePassword.changePassword(newPassword, newPassword);
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.PasswordMustContain);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT303_UpdatePassword_Password_DoesntMatch_ConfirmPassword() throws Exception {
		Account_ChangePassword.changePassword("Wausau#1", "Wausau#2");
		Account_ChangePassword.verify_PasswordChange_AlertError(PasswordChangeError.PasswordMustMatchConfirmPassword);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "password_Tests" })
	public void PT304_UpdatePassword_Password_And_ConfirmPassword_FieldsRequired() throws Exception {
		Account_ChangePassword.click_SavePassword();
		Account_ChangePassword.verify_PasswordFieldRequiredError();
		Account_ChangePassword.verify_ConfirmPasswordFieldRequiredError();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (BaseUI.elementAppears(Locator.lookupElement("changePass_AlertError"))) {
			Browser.clickBrowserBackButton();
			Browser.clickBrowserBackButton();
			HomePage.wait_ForHomePageToLoad();

		} else if (BaseUI.elementAppears(Locator.lookupElement("changePass_SavePassword_Button"))) {
			Browser.clickBrowserBackButton();
			HomePage.wait_ForHomePageToLoad();

		} else if (BaseUI.elementAppears(Locator.lookupElement("login_Username"))) {
			LoginPage.login(GlobalVariables.user_PasswordReset,
					Data_Import.returnPassword(GlobalVariables.user_PasswordReset));
		}

		Navigation.navigate_ChangePassword();
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}
