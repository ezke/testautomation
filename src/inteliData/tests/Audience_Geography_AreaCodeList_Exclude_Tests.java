package inteliData.tests;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.page_controls.Geography_AreaCode_Info;
import inteliData.pages.*;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.ArrayList;
import java.util.HashMap;

public class Audience_Geography_AreaCodeList_Exclude_Tests extends baseTest {

    private final String _poNumber = "Area Code List Exclude Geo-Automation PO";

    private final String _audienceName = "Area Code List Exclude - Automation";
    private final String _audienceMart = "Auto Mart";
    private final String _audienceAccount = "ACME";

    private final String _marketer = GlobalVariables.defaultMarketer;
    private String _review_CreationTime = "";

    private int _expectedTotal;
    private int _audienceID;

    // Global Criteria variables
    private final String[] _areaCodes_Global = {"201", "301", "401", "501", "601", "701", "801", "901"};
    private final String[] _areaCodes_Global_AfterExclude = {"301", "401", "501", "601", "701", "801", "901"};
    private final String[] _areaCodes_Exclude = {"201", "202"};


    // Level 1 variables
    private final String _level1_Name = "auto level 1";
    private final Range<Integer> _ficoRange_Level1 = Range.between(500, 510);


    // Level 2 variables
    private final String _level2_Name = "auto level 2";
    private final Range<Integer> _ficoRange_Level2 = Range.between(515, 520);

    private final String[] _areaCodes_Level2 = {"202", "302", "402", "502", "602", "702", "802", "903"};
    private final String[] _areaCodes_Level2_AfterExclude = {"302", "402", "502", "602", "702", "802", "903"};

    private HashMap<String, String> _expectedGlobalGeography;
    private HashMap<String, String> _level1ExpectedValues;
    private HashMap<String, String> _level2ExpectedValues;
    private HashMap<String, String> _expectedGlobalRejects;

    //   private TableData _zip_ResultsTable;
    private TableData _csvTable;
    private TableData _areaCode_ResultsTable;
//    private final ArrayList<HashMap<String, Object>> _customTableObject = new ArrayList<HashMap<String, Object>>();
    private ArrayList<Geography_AreaCode_Info> _areaCodeInfo;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        _areaCodeInfo = new ArrayList<Geography_AreaCode_Info>();
        _areaCodeInfo.add(new Geography_AreaCode_Info("1", _ficoRange_Level1, _areaCodes_Global_AfterExclude));
        _areaCodeInfo.add(new Geography_AreaCode_Info("2", _ficoRange_Level2, _areaCodes_Level2_AfterExclude));

        _expectedGlobalGeography = new HashMap<String, String>();
        _expectedGlobalGeography.put("Area code list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_areaCodes_Global));

        _level1ExpectedValues = new HashMap<String, String>();
        _level1ExpectedValues.put("Geography", "Use global geography");
        _level1ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level1.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level1.getMaximum()));
        _level1ExpectedValues.put("Phone option", "100% phones");

        _level2ExpectedValues = new HashMap<String, String>();
        _level2ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level2.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level2.getMaximum()));
        _level2ExpectedValues.put("Area code list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_areaCodes_Level2));
        _level2ExpectedValues.put("Phone option", "100% phones");

        _expectedGlobalRejects = new HashMap<String, String>();
        _expectedGlobalRejects.put("Area code list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_areaCodes_Exclude));

        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(_audienceName, _audienceMart, _audienceAccount,
                null, null);
        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();

        CreateAudience_GlobalCriteria.include_AreaCodes_TextBox.enterText(_areaCodes_Global);
        CreateAudience_GlobalCriteria.click_Exclude();
        CreateAudience_GlobalCriteria.exclude_AreaCodes_TextBox.enterText(_areaCodes_Exclude);

        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(_level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());
        CreateAudience_DefineLevels.phoneOptionSelector.click_Option(CreateAudience_DefineLevels.phone_options.ALL_PHONES);

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
        CreateAudience_DefineLevels.phoneOptionSelector.click_Option(CreateAudience_DefineLevels.phone_options.ALL_PHONES);
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();

        CreateAudience_DefineLevels.include_AreaCodes_TextBox.enterText(_areaCodes_Level2);

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
    }

    // Step 25
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT384_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 25
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT384_SearchReview_Verify_GlobalRejectsPanel() throws Exception {
        CreateAudience_SearchReview.globalRejects_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalRejects);
    }

    // Step 25
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT384_SearchReview_Verify_Level1Panel() throws Exception {
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 25
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT384_SearchReview_Verify_Level2Panel() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }


    // Step 26
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 20)
    public void PT384_SearchReview_Click_EditGlobal_Verify_AreaCodesRetained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.include_AreaCodes_TextBox.verify_OptionsSelected(_areaCodes_Global);
    }

    // Step 27
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 25)
    public void PT384_GlobalCriteria_ClickExclude_Verify_AreaCodesRetained() throws Exception {
        CreateAudience_GlobalCriteria.click_Exclude();
        CreateAudience_GlobalCriteria.exclude_AreaCodes_TextBox.verify_OptionsSelected(_areaCodes_Exclude);
    }

    // Step 28
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 30)
    public void PT384_SearchReview_GlobalGeography_Verify_AreaCodesAreStillAccurate() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 28
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 31)
    public void PT384_SearchReview_GlobalGeography_Verify_GlobalRejects_AreaCodesAreStillAccurate() throws Exception {
        CreateAudience_SearchReview.globalRejects_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalRejects);
    }

    // Step 31
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 40)
    public void PT384_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());

    }

    // Step 31
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 41)
    public void PT384_SearchReview_EditLevels_Level1_PhoneOptionSelection_StillValid() throws Exception {
        CreateAudience_DefineLevels.phoneOptionSelector.verify_Option_Selected(CreateAudience_DefineLevels.phone_options.ALL_PHONES);
    }

    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 45)
    public void PT384_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
    }

    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 46)
    public void PT384_SearchReview_EditLevels_Level2_PhoneOptionSelection_StillValid() throws Exception {
        CreateAudience_DefineLevels.phoneOptionSelector.verify_Option_Selected(CreateAudience_DefineLevels.phone_options.ALL_PHONES);
    }

    // Step 35
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 50)
    public void PT384_SearchReview_EditLevels_Level2_AreaCodes_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.include_AreaCodes_TextBox.verify_OptionsSelected(_areaCodes_Level2);
    }

    // Step 37
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 64)
    public void PT384_SearchReview_Level1_StillValid() throws Exception {

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 37
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 65)
    public void PT384_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 70)
    public void PT384_AbleToGetTo_SearchOrderRecords() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.verify_AtPage();

    }

    // Step 39
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 75)
    public void PT384_SearchReview_BuildAudience_Level1_And2_AreaCodesFoundInTable() throws Exception {
        CreateAudience_SearchOrderRecords.retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing();
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("Area Code");
        _areaCode_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();


        _areaCode_ResultsTable.verify_TableSize(_areaCodes_Global_AfterExclude.length + _areaCodes_Level2_AfterExclude.length);

        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_areaCode_ResultsTable, "1",
                _areaCodes_Global_AfterExclude, "Area Code");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_areaCode_ResultsTable, "2",
                _areaCodes_Level2_AfterExclude, "Area Code");
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 80)
    public void PT384_SearchOrderRecords_PickRecords_FooterRequested_MatchesTotalWePick() throws Exception {
        CreateAudience_SearchOrderRecords.telemarketing_Checkbox.check_Checkbox();
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(_areaCode_ResultsTable, 10,
                10);
        _expectedTotal = _areaCode_ResultsTable.return_Sum_ofColumn("Requested").intValueExact();
        _audienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));
        CreateAudience_SearchOrderRecords.verify_Footer_Requested(_expectedTotal);
    }

    // Step 47
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 90)
    public void PT384_PurchaseRecords_VerifyAudienceSummary() throws Exception {
        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(_poNumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_AddAdditionalOutputs();
        CreateAudience_Order_OutputOptions.additionalOptionsModal.checkbox_ByText("AREACODE").check_Checkbox();
        CreateAudience_Order_OutputOptions.additionalOptionsModal.click_SaveAndAddFields();
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(_audienceID, _audienceMart, _marketer,
                _review_CreationTime, _expectedTotal, _poNumber);
    }

    // Step 47
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT384_PurchaseRecords_Results_FooterAccurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(_expectedTotal),
                String.valueOf(_expectedTotal));
    }

    // Step 47
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT384_PurchaseRecords_OutputFields_Accurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.outputFields_Table.verify_TablesMatch(
                CreateAudience_PurchaseRecordsPage.return_OutputFields_ExpectedData_ForAreaCodeTests());
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 105)
    public void PT384_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 49
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 110)
    public void PT384_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";
        _csvTable = CreateAudience_DownloadRecords_Page.return_CSV_Data(typeName);
        _csvTable.verify_TableSize(_expectedTotal);
    }

    // Step 49
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT384_DownloadRecords_Verify_CSV_Lines_FICO_AndState() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_FICO_And_AreaCodes(_csvTable, _areaCodeInfo);
    }

    // Step 49
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT384_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificStates() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_RequestedCounts_MatchCSVCounts(_areaCode_ResultsTable, _csvTable, "Area Code", "AREACODE");
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();

    }
}
