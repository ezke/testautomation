package inteliData.tests;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.*;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class Audience_Geography_CountyList_Exclusion_Tests extends baseTest {

    private final String _poNumber = "County List Exclude Geo-Automation PO";

    private final String _audienceName = "County List Exclude - Automation";
    private final String _audienceMart = "Auto Mart";
    private final String _audienceAccount = "ACME";

    private final String _marketer = GlobalVariables.defaultMarketer;
    private String _review_CreationTime = "";

    private int _expectedTotal;
    private int _audienceID;

    // Global Criteria variables
    private final String[] _counties_Global = {"AUTAUGA, AL", "DISTRICT OF COLUMBIA, DC", "BARBER, KS", "DOUGLAS, NE",
            "MURRAY, OK", "WYOMING, WV", "CAROLINA, PR"};
    private final String[] _counties_Global_Exclude = {"EL DORADO, CA", "DOUGLAS, NE"};
    private final String[] _counties_Global_AfterExclude = {"AUTAUGA, AL", "DISTRICT OF COLUMBIA, DC", "BARBER, KS",
            "MURRAY, OK", "WYOMING, WV", "CAROLINA, PR"};


    // Level 1 variables
    private final String _level1_Name = "auto level 1";
    private final Range<Integer> _ficoRange_Level1 = Range.between(600, 610);

    // Level 2 variables
    private final String _level2_Name = "auto level 2";
    private final Range<Integer> _ficoRange_Level2 = Range.between(615, 620);
    private final String[] _counties_Level2 = {"EL DORADO, CA", "MIAMI-DADE, FL", "ASCENSION, LA", "CLARK, NV",
            "PHILADELPHIA, PA", "PIERCE, WA", "GUAYNABO, PR"};
    private final String[] _counties_Level2_AfterExclude = {"MIAMI-DADE, FL", "ASCENSION, LA", "CLARK, NV",
            "PHILADELPHIA, PA", "PIERCE, WA", "GUAYNABO, PR"};

    private HashMap<String, String> _expectedGlobalGeography;
    private HashMap<String, String> _level1ExpectedValues;
    private HashMap<String, String> _level2ExpectedValues;
    private HashMap<String, String> _expectedGlobalRejects;

    private TableData _county_ResultsTable;
    private TableData _csvTable;
    private TableData _countyQueryData;

    private final TableData expectedRows = new TableData();

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        _expectedGlobalGeography = new HashMap<String, String>();
        _expectedGlobalGeography.put("County list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_counties_Global, true));
        _expectedGlobalRejects = new HashMap<String, String>();
        _expectedGlobalRejects.put("County list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_counties_Global_Exclude, true));
        _level1ExpectedValues = new HashMap<String, String>();
        _level1ExpectedValues.put("Geography", "Use global geography");
        _level1ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level1.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level1.getMaximum()));

        _level2ExpectedValues = new HashMap<String, String>();
        _level2ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level2.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level2.getMaximum()));
        _level2ExpectedValues.put("County list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_counties_Level2, true));

        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(_audienceName, _audienceMart, _audienceAccount,
                null, null);
        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();

        CreateAudience_GlobalCriteria.include_County_TextBox.enterText(_counties_Global);
        CreateAudience_GlobalCriteria.click_Exclude();
        CreateAudience_GlobalCriteria.exclude_Counties_TextBox.enterText(_counties_Global_Exclude);

        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(_level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();

        CreateAudience_DefineLevels.include_County_TextBox.enterText(_counties_Level2);

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT386_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT386_SearchReview_Verify_GlobalRejectsPanel() throws Exception {
        CreateAudience_SearchReview.globalRejects_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalRejects);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT386_SearchReview_Verify_Level1Panel() throws Exception {
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT386_SearchReview_Verify_Level2Panel() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }


    // Step 24
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 20)
    public void PT386_SearchReview_Click_EditGlobal_Verify_CountyRetained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.include_County_TextBox.verify_OptionsSelected(_counties_Global);
    }

    // Step 25
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 25)
    public void PT386_GlobalCriteria_ClickExclude_Verify_CountyRetained() throws Exception {
        CreateAudience_GlobalCriteria.click_Exclude();
        CreateAudience_GlobalCriteria.exclude_Counties_TextBox.verify_OptionsSelected(_counties_Global_Exclude);
    }

    // Step 26
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 30)
    public void PT386_SearchReview_GlobalGeography_Verify_CountyStillAccurate() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 26
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 31)
    public void PT386_SearchReview_GlobalReject_Verify_CountyStillAccurate() throws Exception {
        CreateAudience_SearchReview.globalRejects_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalRejects);
    }


    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 40)
    public void PT386_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());

    }

    // Step 32
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 41)
    public void PT386_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());

    }

    // Step 33
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 42)
    public void PT386_SearchReview_EditLevels_Level2_County_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.include_County_TextBox.verify_OptionsSelected(_counties_Level2);
    }

    // Step 35
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 64)
    public void PT386_SearchReview_Level1_StillValid() throws Exception {

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 35
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 65)
    public void PT386_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }


    // Step 37
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 75)
    public void PT386_SearchReview_BuildAudience_Level1_And2_CountyAndStateFoundInTable() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("County");
        _county_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();

        for (String value : _counties_Global_AfterExclude) {
            HashMap<String, String> expectedRow = new HashMap<String, String>();
            String county = value.split("\\,")[0];
            String state = value.split("\\,")[1].trim();
            expectedRow.put("Level", "1");
            expectedRow.put("County", county);
            expectedRow.put("State", state);

            expectedRows.data.add(expectedRow);
        }

        for (String value : _counties_Level2_AfterExclude) {
            HashMap<String, String> expectedRow = new HashMap<String, String>();
            String county = value.split("\\,")[0];
            String state = value.split("\\,")[1].trim();
            expectedRow.put("Level", "2");
            expectedRow.put("County", county);
            expectedRow.put("State", state);

            expectedRows.data.add(expectedRow);
        }
        _county_ResultsTable.verify_TableSize(_counties_Global_AfterExclude.length + _counties_Level2_AfterExclude.length);

        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_county_ResultsTable, expectedRows);
    }

    // Step 39
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 80)
    public void PT386_SearchOrderRecords_PickRecords_FooterRequested_MatchesTotalWePick() throws Exception {
        CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(_county_ResultsTable, 10,
                10);
        _expectedTotal = _county_ResultsTable.return_Sum_ofColumn("Requested").intValueExact();
        _audienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));
        CreateAudience_SearchOrderRecords.verify_Footer_Requested(_expectedTotal);
    }

    // Step 45
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 90)
    public void PT386_PurchaseRecords_VerifyAudienceSummary() throws Exception {
        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(_poNumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_AddAdditionalOutputs();
        CreateAudience_Order_OutputOptions.additionalOptionsModal.checkbox_ByText("COUNTY_NAME").check_Checkbox();
        CreateAudience_Order_OutputOptions.additionalOptionsModal.click_SaveAndAddFields();
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(_audienceID, _audienceMart, _marketer,
                _review_CreationTime, _expectedTotal, _poNumber);
    }

    // Step 45
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT386_PurchaseRecords_Results_FooterAccurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(_expectedTotal),
                String.valueOf(_expectedTotal));
    }

    // Step 45
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT386_PurchaseRecords_OutputFields_Accurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.outputFields_Table.verify_TablesMatch(
                CreateAudience_PurchaseRecordsPage.return_OutputFields_ExpectedData_ForCountyNameTests());
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 105)
    public void PT386_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 47
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 110)
    public void PT386_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";

        _csvTable = CreateAudience_DownloadRecords_Page.return_CSV_Data(typeName);

        _countyQueryData = CreateAudience_DownloadRecords_Page.return_County_DatabaseQuery(_audienceID);

        _csvTable.verify_TableSize(_expectedTotal);
    }

    // Step 47
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT386_DownloadRecords_Verify_CSV_Lines_FICO_County_AndState() throws Exception {

        expectedRows.forEach_Row(a -> {
            String level = a.get("Level");
            if (level.equals("1")) {
                a.put("ficoMin", String.valueOf(_ficoRange_Level1.getMinimum()));
                a.put("ficoMax", String.valueOf(_ficoRange_Level1.getMaximum()));
            } else if (level.equals("2")) {
                a.put("ficoMin", String.valueOf(_ficoRange_Level2.getMinimum()));
                a.put("ficoMax", String.valueOf(_ficoRange_Level2.getMaximum()));
            }
        });

        CreateAudience_DownloadRecords_Page.validate_Fico_InCSV(_csvTable, expectedRows);
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_CityDBQuery(_csvTable, _countyQueryData);

    }

    // Step 47
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT386_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificCounties() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_COUNTY_RequestedCounts_MatchCSVCount_AndDB(_county_ResultsTable, _csvTable, _countyQueryData);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();

    }

}// End of Class
