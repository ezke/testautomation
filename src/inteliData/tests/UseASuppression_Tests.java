package inteliData.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class UseASuppression_Tests extends baseTest {

	String name_CreateAudienceModal = "Auto - Using Suppression";
	String mart = "Auto Mart";
	String marketer = "1";
	String account = "ACME";

	String[] statesToSelect = { "ALASKA", "RHODE ISLAND" };
	String[] statesAbbreviated = { "AK", "RI" };

	String levelName = "AutoTest Suppression";
	Integer ficoRange1 = 500;
	Integer ficoRange2 = 510;

	String suppressionLevel;
	String suppressionName;
	String poNumber;

	String totalRecords;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

		HomePage.click_CreateAudience_Button();
		CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(name_CreateAudienceModal, mart, account, marketer, null);
		SuppliersAndUsage_Modal.directMail_Checkbox.check_Checkbox();
		SuppliersAndUsage_Modal.click_CreateAudience();
		CreateAudience_GlobalCriteria.stateSelector.select_States(statesToSelect);
		CreateAudience_GlobalCriteria.globalSuppression.expand_Section();
		suppressionLevel = CreateAudience_GlobalCriteria.return_AddressSuppressionLevel();


	}

	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests" }, priority = 5)
	public void PT251_GlobalCriteria_AbleToSearchForAndAddSuppression() throws Exception {
		CreateAudience_GlobalCriteria.suppressByOrder
				.search_For_And_Add_FirstSelection_ToSelectedList(GlobalVariables.defaultSuppression);
		suppressionName = CreateAudience_GlobalCriteria.return_SelectedSuppression_OrderName();
		BaseUI.verify_true_AndLog(!BaseUI.stringEmpty(suppressionName.trim()), "Found Suppression.", "Did NOT find suppression.");
	}

	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests" }, priority = 10)
	public void PT251_SearchReview_VerifySuppressionsPanel() throws Exception {
		CreateAudience_GlobalCriteria.suppressByOrder.check_Checkbox();

		CreateAudience_GlobalCriteria.click_SaveAndContinue();

		CreateAudience_DefineLevels.enter_Level(levelName);
		CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
		CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
		CreateAudience_DefineLevels.click_SaveLevel_Button();
		CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();

		CreateAudience_SearchReview.verify_SuppressionsPanel(suppressionLevel, suppressionName);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests" }, priority = 15)
	public void PT251_SearchOrderRecords_VerifySuppressionsPanel() throws Exception {
		CreateAudience_SearchReview.click_Top_BuildAudience_Button();
		totalRecords = CreateAudience_SearchOrderRecords.get_mainContentPanel_Value("Total Records");
		CreateAudience_SearchOrderRecords.click_ViewDetailsLink();
		CreateAudience_SearchOrderRecords.verify_SuppressionsPanel(suppressionLevel, suppressionName);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests" }, priority = 16)
	public void PT251_SearchOrderRecords_VerifySelectedSuppressions_ContainsSuppressionWeSelected() throws Exception {

		CreateAudience_SearchOrderRecords
				.verify_Suppression_Found_In_SelectedSuppressions(GlobalVariables.defaultSuppression, suppressionName);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of class
