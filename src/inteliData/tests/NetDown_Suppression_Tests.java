package inteliData.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_Order_OutputOptions;
import inteliData.pages.CreateAudience_PurchaseRecordsPage;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.SuppressionCompleted;
import inteliData.pages.SuppressionDisplay;
import inteliData.pages.Suppressions;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class NetDown_Suppression_Tests extends baseTest {

	String audienceName = "Automation_NetDown";
	String audienceMart = "Auto Mart";
	String audienceAccount = "ACME";
	String account_DBName = "su_4586";
	Integer numberOfLevels = 1;
	String marketer = GlobalVariables.defaultMarketer;
	String review_CreationTime = "";
	String[] statesToSelect = { "ALASKA", "RHODE ISLAND" };
	String[] statesAbbreviated = { "AK", "RI" };

	String expectedTotalAsString;
	Integer expectedTotal;
	Integer audienceID;
	String orderID;

	String suppressionType = "Net Down Order";

	HashMap<String, String> expectedFilterValues = new HashMap<String, String>();
	TableData audienceSummaryTable;

	String levelName = "AutoTest";
	Integer ficoRange1 = 500;
	Integer ficoRange2 = 510;

	Integer expectedTotalRecords_Minimum = 2000;
	String purchaseAmount = "";
	String purchaseTime = "";

	String poNumber = "Auto Test - Net Down";
	String createdBy = "Test Automation";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedFilterValues.put("Geography", "Use global geography");
		expectedFilterValues.put("FICO auto score", "From 500 To 510");
		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

		HomePage.click_CreateAudience_Button();
		CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(audienceName, audienceMart, audienceAccount, null,
				null);
		SuppliersAndUsage_Modal.select_AllSuppliers();
		SuppliersAndUsage_Modal.click_CreateAudience();
		CreateAudience_GlobalCriteria.stateSelector.select_States(statesToSelect);
		CreateAudience_GlobalCriteria.click_SaveAndContinue();

		CreateAudience_DefineLevels.enter_Level(levelName);
		CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
		CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
		CreateAudience_DefineLevels.click_SaveLevel_Button();

		review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);
		CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();

		CreateAudience_SearchReview.click_Top_BuildAudience_Button();

		HashMap<String, String> mainContentPanelValues = CreateAudience_SearchOrderRecords.mainContent_Panel
				.return_PanelData();
		expectedTotalAsString = mainContentPanelValues.get("Total Records");
		expectedTotal = Integer.parseInt(mainContentPanelValues.get("Total Records").replace(",", ""));
		audienceID = Integer.parseInt(mainContentPanelValues.get("Audience ID"));

		CreateAudience_SearchOrderRecords.resultsTable.checkbox_ByIndex(1).check_Checkbox();
		CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();

		CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
	}

	// Step 21
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 55)
	public void PT247_CreateAudience_PurchaseRecords_AudienceSummary_Accurate() throws Exception {
		CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(poNumber);
		CreateAudience_Order_OutputOptions.click_PostNetdown_Yes();
		CreateAudience_Order_OutputOptions.netdown_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name",
				"CSV - TIS Custom");
		CreateAudience_Order_OutputOptions.netdown_CreateSuppressionCheckbox.check_Checkbox();
		CreateAudience_Order_OutputOptions.click_PlaceOrder();

		CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(audienceID, audienceMart, marketer,
				review_CreationTime, expectedTotal, poNumber);
	}

	// Step 22
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 59)
	public void PT247_DownloadRecords_AbleToGetToPage() throws Exception {
		purchaseTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_DownloadRecords_Page.dateFormat);
		CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
		CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
	}

	// Step 22
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 60)
	public void PT247_CreateAudience_FinalPage_ValidateCSV() throws Exception {
		CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

		String typeName = "CSV - TIS Custom (Order File)";
		String fileName = "";

		orderID = CreateAudience_DownloadRecords_Page.return_OrderID();
		TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
		fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
		TableData csvTable = CreateAudience_DownloadRecords_Page
				.download_File_FromDownloadRecords(indexOfLineToDownload + 1, fileName);
		CreateAudience_DownloadRecords_Page.verify_CSV_TIS_Custom_MatchesExpected(csvTable, "1");
	}

	// Step 24
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 70)
	public void PT247_SuppressionPage_AbleToFindOrderID() throws Exception {
		SuppressionCompleted.wait_For_AllDBs_ToContainSuppression(account_DBName, orderID);
		Navigation.navigate_Suppressions();
		Suppressions.search_ByText_UntilFound(orderID);
		Suppressions.verify_Suppression1Entry_Correct(orderID, poNumber, audienceAccount, marketer, audienceMart,
				suppressionType, expectedTotal, purchaseTime);
	}

	// Step 25
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 75)
	public void PT247_SuppressionDisplay_VerifyData() throws Exception {
		Suppressions.suppressionTable.click_View_ForRow(1);
		SuppressionDisplay.verify_DetailsPanel(orderID, suppressionType, poNumber, marketer, createdBy, expectedTotal,
				audienceMart);
	}

	// Step 26
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT247_Verify_Mart1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart1,
				account_DBName, orderID, expectedTotal);
	}

	// Step 26
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT247_Verify_Mart2_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart2,
				account_DBName, orderID, expectedTotal);
	}

	// Step 26
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT247_Verify_Ful1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Ful1,
				account_DBName, orderID, expectedTotal);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
