package inteliData.tests;

import java.time.Duration;
import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.DataSource_Modal;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.DataSourceImport;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.FirstParty_DataSources_Display;
import inteliData.pages.FirstParty_Mappings;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.Programs;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class FirstPartyData_AccountTemplate_FullnameMapped_Tests extends baseTest {

	String name = "Auto-DataSource-Account-FullnameMapped";
	String account = "ACME";
	String marketer = "1";
	String expectedMarketer = "1 (15899)";
	String template = "Account";
	TableData mappingsTable;
	int expectedRecords = 17;
	int expectedErrors = 12;

	String file = "\\src\\inteliData\\data\\account_small_fullname.csv";
	String fileName = "account_small_fullname.csv";

	String expectedStandardizedResultsFile = "\\src\\inteliData\\data\\account_small_fullname_ExpectedStandardizedResults.csv";

	HashMap<String, String> expectedMappings;

	// String id = "70008054";
	String id = "";
	// String marketer_TableKey = "15899";
	String marketer_TableKey = "";

	TableData accounts;
	TableData accountPassThroughs;
	TableData accountValidationErrors;

	TableData csvToCheckAgainst;

	TableData account_CSV_Standardized_ExpectedResults;
	String expectedErrorResultsFile = "\\src\\inteliData\\data\\account_small_fullname_ExpectedErrors.csv";
	TableData accountErrors_ExpectedValues;

	String expectedPassThroughs = "\\src\\inteliData\\data\\account_small_fullname_ExpectedPassThroughs.csv";
	TableData accountPassThroughs_ExpectedValues;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings = FirstParty_Mappings.return_fullName_ExpectedDefaultMappings();

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.click_CreateButton();
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer, template);
		DataSourceImport.chooseFile(file, true);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 10)
	public void PT312_Step7_FirstPartyMappings_VerifyDetailsPanel() throws Exception {
		id = FirstParty_Mappings.data_Panel.return_SpecificValue("Id");
		FirstParty_Mappings.verify_detailsPanel_FirstPartyData(name, fileName, expectedMarketer, expectedRecords);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 15)
	public void PT312_Step7_FirstPartyMappings_VerifyDefaultMappings() throws Exception {
		mappingsTable = FirstParty_Mappings.mapTable.return_TableData();
		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 16)
	public void PT312_Step8_FirstPartyMappings_ClickMapRemainingAsPassThrough_VerifyTableUpdated() throws Exception {
		expectedMappings.put("HomePhone", "Phone");
		expectedMappings.put("EmailOptOutIndicator", "Email Opt Out");
		expectedMappings.put("PT1", "Pass Through");

		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "HomePhone", "Phone");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "EmailOptOutIndicator", "Email Opt Out");

		FirstParty_Mappings.click_MapRemainingAsPassThrough();
		TableData mappingsTable = FirstParty_Mappings.mapTable.return_TableData();

		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
		FirstParty_Mappings.mapTable.verify_AllCheckboxes_Checked(mappingsTable);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 20)
	public void PT312_Step9_ConfirmMappings_VerifyConfirmMappingTable() throws Exception {
		FirstParty_Mappings.click_Map_Button();
		ConfirmMappedFields.verify_Mappings(expectedMappings);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 21)
	public void PT312_Step9_ConfirmMappings_Verify_DetailsPanel() throws Exception {
		ConfirmMappedFields.verify_DetailsPanel_ForFirstPartyData_MatchesExpected(id, name, fileName, expectedRecords,
				expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 30)
	public void PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		FirstParty_DataSources_Display.refresh_Until_Mapped(Duration.ofMinutes(10));
		marketer_TableKey = FirstParty_DataSources_Display.return_MarketerKey();
		FirstParty_DataSources_Display.verify_DetailsPanel(id, name, fileName, expectedRecords, expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 31, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT312_Step10_FirstPartyDataSourcesDisplay_VerifyErrorsPanel() throws Exception {
		String addressStandards = "8";
		String latLongMatch = "8";

		// populate DB's
		accounts = FirstParty_DataSources_Display.return_accounts(id, marketer_TableKey);
		accountPassThroughs = FirstParty_DataSources_Display.return_accountPassThroughs(id, marketer_TableKey);
		accountValidationErrors = FirstParty_DataSources_Display.return_accountValidationErrors(id, marketer_TableKey);

		csvToCheckAgainst = FirstParty_DataSources_Display.return_CSVFile_ToCheckAgainst(file);
		account_CSV_Standardized_ExpectedResults = FirstParty_DataSources_Display
				.return_Account_ExpectedStandardizedResults(expectedStandardizedResultsFile);

		accountPassThroughs_ExpectedValues = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedPassThroughs);
		accountPassThroughs_ExpectedValues.remove_Character("NULL");

		accountErrors_ExpectedValues = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedErrorResultsFile);
		accountErrors_ExpectedValues.sort_ByColumn_Ascending("Error");
		FirstParty_DataSources_Display.apply_newIDs_ToExpectedTables(account_CSV_Standardized_ExpectedResults,
				new TableData[] { accountErrors_ExpectedValues, accountPassThroughs_ExpectedValues },
				Integer.parseInt(accounts.data.get(0).get("AccountId")), "AccountId");

		FirstParty_DataSources_Display.verify_ErrorsPanel(expectedErrors, addressStandards, latLongMatch);
	}

	// Step 11
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT312_Step11_VerifyDatabase_csv_CorrectCount_OfEntries() throws Exception {
		csvToCheckAgainst.verify_TableSize(expectedRecords);
	}

	@DataProvider(name = "compare_CSV_To_Accounts_Standardized")
	public Object[][] csv_To_accounts_TableMappings_Standardized() {
		Object[][] linksToNavigate = new Object[account_CSV_Standardized_ExpectedResults.data.get(0).size()][1];

		int indexToPlace_ColumnHeader = 0;
		for (String key : account_CSV_Standardized_ExpectedResults.data.get(0).keySet()) {
			linksToNavigate[indexToPlace_ColumnHeader][0] = key;

			indexToPlace_ColumnHeader++;
		}

		return linksToNavigate;
	}

	// Step 3
	@Test(dataProvider = "compare_CSV_To_Accounts_Standardized", priority = 40, groups = { "all_Tests",
			"regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step3_VerifyColumnsMatch_CSV_to_AccountsTable_WithFilters(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, account_CSV_Standardized_ExpectedResults.data, accounts.data);
	}

	// Step 4
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step4_VerifyDatabase_Accounts_FileIdMatches_Id() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("FileId", id, accounts.data);
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "compare_CSV_To_Accounts")
	public Object[][] csv_To_accounts_TableMappings() {
		Object[][] linksToNavigate = new Object[csvToCheckAgainst.data.get(0).size() - 1][1];

		int indexToPlace_ColumnHeader = 0;
		for (String key : csvToCheckAgainst.data.get(0).keySet()) {
			if (!key.equals("PT1")) {
				linksToNavigate[indexToPlace_ColumnHeader][0] = key;

				indexToPlace_ColumnHeader++;

			}
		}

		return linksToNavigate;
	}

	// Step 5
	@Test(dataProvider = "compare_CSV_To_Accounts", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step5_Compare_CSV_To_Accounts(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, csvToCheckAgainst.data, accounts.data);
	}

	@DataProvider(name = "compare_CSV_To_Errors")
	public Object[][] csv_To_Error_TableMappings() {
		Object[][] linksToNavigate = new Object[2][1];

		linksToNavigate[0][0] = "AccountId";
		linksToNavigate[1][0] = "Error";

		return linksToNavigate;
	}

	// Step 6
	@Test(dataProvider = "compare_CSV_To_Errors", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step6_Compare_ExpectedErrorColumns_To_ActualErrorsTable(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, accountErrors_ExpectedValues.data,
				accountValidationErrors.data);
	}

	@DataProvider(name = "compare_CSV_To_PassThroughs")
	public Object[][] csv_To_PassThrough_TableMappings() {
		Object[][] linksToNavigate = new Object[3][1];

		linksToNavigate[0][0] = "AccountId";
		linksToNavigate[1][0] = "Name";
		linksToNavigate[2][0] = "Value";

		return linksToNavigate;
	}

	// Step 8
	@Test(dataProvider = "compare_CSV_To_PassThroughs", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step6_Compare_ExpectedPassThroughColumns_To_ActualPassThroughTable(String csvColumnName)
			throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, accountPassThroughs_ExpectedValues.data,
				accountPassThroughs.data);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step7_VerifyDatabase_PassThroughs_CorrectNumberOfEntries() throws Exception {
		accountPassThroughs.verify_TableSize(accountPassThroughs_ExpectedValues.data.size());
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step8_VerifyDatabase_ErrorTable_VerifyCountOfUniqueProductIds() throws Exception {
		accountValidationErrors.verify_CountOfUniqueColumnValues(expectedErrors, "AccountId");
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT312_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT311_Step9_VerifyDatabase_ErrorTable_CorrectCountOfEntries() throws Exception {
		accountValidationErrors.verify_TableSize(accountErrors_ExpectedValues.data.size());
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
