package inteliData.tests;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.page_controls.Geography_ZipCode_Info;
import inteliData.pages.*;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.ArrayList;
import java.util.HashMap;

public class Audience_Geography_ZipCodeUploadList_Tests extends baseTest {

    private final String _poNumber = "Zip Code Upload List Geo-Automation PO";

    private final String _audienceName = "Zip Code Upload List - Automation";
    private final String _audienceMart = "Auto Mart";
    private final String _audienceAccount = "ACME";

    private final String _marketer = GlobalVariables.defaultMarketer;
    private String _review_CreationTime = "";

    private int _expectedTotal;
    private int _audienceID;

    // Global Criteria variables
    private final String[] _zipCodes_Global_AfterExclusion = {"00961", "01001", "10001", "20001", "30002",
            "50001", "70001", "80001", "90001"};
    private final String _zipCodeFile_Global = "\\src\\inteliData\\data\\ZipCodeFiles\\global_geo_zips.csv";


    // Level 1 variables
    private final String _level1_Name = "auto level 1";
    private final Range<Integer> _ficoRange_Level1 = Range.between(600, 650);


    // Level 2 variables
    private final String _level2_Name = "auto level 2";
    private final Range<Integer> _ficoRange_Level2 = Range.between(650, 700);
    private final String[] _zipCodes_Level2_AfterExclusion = {"00610", "01002", "10002", "20002", "30004", "50005",
            "70009", "80002", "90002"};

    private final String _zipCodeFile_Level2 = "\\src\\inteliData\\data\\ZipCodeFiles\\level_zips.csv";

    private HashMap<String, String> _expectedGlobalGeography;
    private HashMap<String, String> _level1ExpectedValues;
    private HashMap<String, String> _level2ExpectedValues;

    private TableData _zip_ResultsTable;
    private TableData _csvTable;
    private final ArrayList<Geography_ZipCode_Info> _zipCodeLevelInfo = new ArrayList<>();


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        _expectedGlobalGeography = new HashMap<String, String>();
        _expectedGlobalGeography.put("Zip code list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_zipCodes_Global_AfterExclusion));
        _expectedGlobalGeography.put("Zip Upload File", _zipCodeFile_Global.substring(_zipCodeFile_Global.lastIndexOf("\\") + 1, _zipCodeFile_Global.length()));

        _level1ExpectedValues = new HashMap<String, String>();
        _level1ExpectedValues.put("Geography", "Use global geography");
        _level1ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level1.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level1.getMaximum()));

        _level2ExpectedValues = new HashMap<String, String>();
        _level2ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level2.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level2.getMaximum()));
        _level2ExpectedValues.put("Zip code list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_zipCodes_Level2_AfterExclusion));
        _level2ExpectedValues.put("Zip Upload File", _zipCodeFile_Level2.substring(_zipCodeFile_Level2.lastIndexOf("\\") + 1, _zipCodeFile_Level2.length()));


        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(_audienceName, _audienceMart, _audienceAccount,
                null, null);
        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();

        CreateAudience_GlobalCriteria.zipCodes_ChooseFile.chooseFile(_zipCodeFile_Global);
    }

    // Step 7
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 5)
    public void PT370_GlobalCriteria_Upload_ChooseFile_VerifyError() throws Exception {
        String expectedText = "2 zip code(s) were invalid. Only the valid zip codes will be used in your audience. Invalid zip codes include: 99999,<script>alert(/XSS/);</script>";
        CreateAudience_GlobalCriteria.zipCodes_ChooseFile.verify_ErrorTextDisplayedAndAccurate(expectedText);
    }

    // Step 20
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 7)
    public void PT370_DefineLevels_Upload_ChooseFile_VerifyError() throws Exception {
        String expectedText = "2 zip code(s) were invalid. Only the valid zip codes will be used in your audience. Invalid zip codes include: 99998,<script>alert(/XSS/);</script>";
        CreateAudience_GlobalCriteria.zipCodes_ChooseFile.click_OK();

        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(_level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();

        CreateAudience_DefineLevels.zipCodes_ChooseFile.chooseFile(_zipCodeFile_Level2);
        CreateAudience_DefineLevels.zipCodes_ChooseFile.verify_ErrorTextDisplayedAndAccurate(expectedText);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT370_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
        CreateAudience_DefineLevels.zipCodes_ChooseFile.click_OK();
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();

        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT370_SearchReview_Verify_Level1Panel() throws Exception {
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT370_SearchReview_Verify_Level2Panel() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT370_SearchReview_Verify_SuppressionsPanel() throws Exception {
        HashMap<String, String> expectedSuppressions = new HashMap<String, String>();
        expectedSuppressions.put("Suppression level", "Address (Address Only)");
        CreateAudience_SearchReview.suppressions_Panel.verify_ExpectedData_Matches_ReturnedData(expectedSuppressions);
    }

    // Step 24
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 20)
    public void PT370_SearchReview_Click_EditGlobal_Verify_ZipCodesRetained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.verify_ZipCodesUploadMessage(_zipCodes_Global_AfterExclusion.length,
                _zipCodeFile_Global.substring(_zipCodeFile_Global.lastIndexOf("\\") + 1, _zipCodeFile_Global.length()));
    }

    // Step 25
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 30)
    public void PT370_SearchReview_GlobalGeography_Verify_ZipCodesRetained() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 28
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 40)
    public void PT370_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());

    }

    // Step 31
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 41)
    public void PT370_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());

    }

    // Step 32
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 42)
    public void PT370_SearchReview_EditLevels_Level2_ZipCodes_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_GlobalCriteria.verify_ZipCodesUploadMessage(_zipCodes_Level2_AfterExclusion.length,
                _zipCodeFile_Level2.substring(_zipCodeFile_Level2.lastIndexOf("\\") + 1, _zipCodeFile_Level2.length()));
    }

    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 64)
    public void PT370_SearchReview_Level1_StillValid() throws Exception {

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }


    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 65)
    public void PT370_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 70)
    public void PT370_AbleToGetTo_SearchOrderRecords() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.verify_AtPage();
    }

    // Step 36
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 75)
    public void PT370_SearchReview_BuildAudience_Level1_And2_ZipsFoundInTable() throws Exception {
        CreateAudience_SearchOrderRecords.retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing();
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("Zip Code");
        _zip_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();


        _zip_ResultsTable.verify_TableSize(_zipCodes_Global_AfterExclusion.length + _zipCodes_Level2_AfterExclusion.length);

        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_zip_ResultsTable, "1",
                _zipCodes_Global_AfterExclusion, "Zip Code");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_zip_ResultsTable, "2",
                _zipCodes_Level2_AfterExclusion, "Zip Code");
    }


    // Step 38
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 80)
    public void PT370_SearchOrderRecords_PickRecords_FooterRequested_MatchesTotalWePick() throws Exception {
        CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(_zip_ResultsTable, 10,
                10);
        _expectedTotal = _zip_ResultsTable.return_Sum_ofColumn("Requested").intValueExact();
        _audienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));
        CreateAudience_SearchOrderRecords.verify_Footer_Requested(_expectedTotal);
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 90)
    public void PT370_PurchaseRecords_VerifyAudienceSummary() throws Exception {
        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(_poNumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(_audienceID, _audienceMart, _marketer,
                _review_CreationTime, _expectedTotal, _poNumber);
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT370_PurchaseRecords_Results_FooterAccurate() throws Exception {

        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(_expectedTotal),
                String.valueOf(_expectedTotal));
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 105)
    public void PT370_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 43
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 110)
    public void PT370_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";
        String fileName = "";

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
        fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
        _csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1,
                fileName);

        _zipCodeLevelInfo.add(new Geography_ZipCode_Info("1", _ficoRange_Level1, _zipCodes_Global_AfterExclusion));
        _zipCodeLevelInfo.add(new Geography_ZipCode_Info("2", _ficoRange_Level2, _zipCodes_Level2_AfterExclusion));

        _csvTable.verify_TableSize(_expectedTotal);
    }

    // Step 43
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT370_DownloadRecords_Verify_CSV_Lines_FICO_AndState() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_FICO_And_Zips(_csvTable, _zipCodeLevelInfo);
    }

    // Step 43
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT370_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificStates() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_RequestedCounts_MatchCSVCounts(_zip_ResultsTable, _csvTable, "Zip Code", "ZIP");
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();

    }

}// End of Class
