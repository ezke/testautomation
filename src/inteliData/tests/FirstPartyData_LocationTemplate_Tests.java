package inteliData.tests;

import java.time.Duration;
import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.DataSource_Modal;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.DataSourceImport;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.FirstParty_DataSources_Display;
import inteliData.pages.FirstParty_Mappings;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.Programs;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class FirstPartyData_LocationTemplate_Tests extends baseTest {

	String name = "AutomationDataSource - Locations";
	String account = "ACME";
	String marketer = "1";
	String expectedMarketer = "1 (15899)";
	String template = "Location";
	int expectedRecords = 11;
	int expectedErrors = 1;
	TableData mappingsTable;
	String file = "\\src\\inteliData\\data\\Locations.csv";
	String fileName = "Locations.csv";

	HashMap<String, String> expectedMappings;

	// String id = "70007349";
	String id = "";
	// String marketer_TableKey = "15899";
	String marketer_TableKey = "";

	// TableData products;
	// TableData productPassThroughs;
	// TableData productValidationErrors;
	TableData locations;
	TableData locationPassThroughs;
	TableData locationValidationErrors;

	TableData csvToCheckAgainst;
	TableData csvToCheckAgainst_reformatted;

	TableData csv_ExpectedStandardizedResults;
	String expectedStandardizedResultsFile = "\\src\\inteliData\\data\\Locations-ExpectedStandardizedResults.csv";

	TableData csv_ExpectedPassThroughs;
	String expectedPassThroughsFile = "\\src\\inteliData\\data\\Locations-ExpectedPassThroughTable.csv";

	TableData csv_ExpectedErrors;
	String expectedErrorsFile = "\\src\\inteliData\\data\\Locations-ExpectedErrorTable.csv";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {

		expectedMappings = new HashMap<String, String>();
		expectedMappings.put("ActiveIndicator", "Active Indicator");
		expectedMappings.put("ID", "");
		expectedMappings.put("Address1", "Address Line1");
		expectedMappings.put("Address2", "Address Line2");
		expectedMappings.put("CITY_X", "City");
		expectedMappings.put("LocationCode", "Location Code");
		expectedMappings.put("Direction1", "Direction 1");
		expectedMappings.put("Direction2", "Direction 2");
		expectedMappings.put("Latitude", "Original Latitude");
		expectedMappings.put("Longitude", "Original Longitude");
		expectedMappings.put("LocationMailIndicator", "Location Mail Indicator");
		expectedMappings.put("LocationManager", "Location Manager");
		expectedMappings.put("LocationName", "Location Name");
		expectedMappings.put("PHONE_X", "");
		expectedMappings.put("RegionCode", "Region Code");
		expectedMappings.put("STATE_X", "State");
		expectedMappings.put("Type", "");
		expectedMappings.put("ZIP4_X", "Zip 4");
		expectedMappings.put("Zip5", "Zip");
		expectedMappings.put("PT1", "");
		expectedMappings.put("PT2", "");

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.click_CreateButton();
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer, template);
		DataSourceImport.chooseFile(file, true);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 10)
	public void PT278_FirstPartyMappings_VerifyDetailsPanel() throws Exception {
		id = FirstParty_Mappings.data_Panel.return_SpecificValue("Id");
		FirstParty_Mappings.verify_detailsPanel_FirstPartyData(name, fileName, expectedMarketer, expectedRecords);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 15)
	public void PT278_FirstPartyMappings_VerifyDefaultMappings() throws Exception {
		mappingsTable = FirstParty_Mappings.mapTable.return_TableData();
		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 16)
	public void PT278_FirstPartyMappings_ClickMapRemainingAsPassThrough_VerifyTableUpdated() throws Exception {

		expectedMappings.put("Type", "Location Type");
		expectedMappings.put("PHONE_X", "Phone");
		expectedMappings.put("PT1", "Pass Through");
		expectedMappings.put("PT2", "Pass Through");
		expectedMappings.put("ID", "Pass Through");

		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Type", "Location Type");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "PHONE_X", "Phone");

		FirstParty_Mappings.click_MapRemainingAsPassThrough();
		mappingsTable = FirstParty_Mappings.mapTable.return_TableData();

		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
		FirstParty_Mappings.mapTable.verify_AllCheckboxes_Checked(mappingsTable);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 20)
	public void PT278_ConfirmMappings_VerifyConfirmMappingTable() throws Exception {
		FirstParty_Mappings.click_Map_Button();
		ConfirmMappedFields.verify_Mappings(expectedMappings);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 21)
	public void PT278_ConfirmMappings_Verify_DetailsPanel() throws Exception {
		ConfirmMappedFields.verify_DetailsPanel_ForFirstPartyData_MatchesExpected(id, name, fileName, expectedRecords,
				expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 30)
	public void PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		FirstParty_DataSources_Display.refresh_Until_Mapped(Duration.ofMinutes(7));
		marketer_TableKey = FirstParty_DataSources_Display.return_MarketerKey();
		FirstParty_DataSources_Display.verify_DetailsPanel(id, name, fileName, expectedRecords, expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel", priority = 31)
	public void PT278_FirstPartyDataSourcesDisplay_VerifyErrorsPanel() throws Exception {
		String addressStandards = "1";
		String latLongMatch = "1";

		// populate Tables
		locations = FirstParty_DataSources_Display.return_locations(id, marketer_TableKey);
		locations.logTable();
		locationPassThroughs = FirstParty_DataSources_Display.return_locationPassThroughs(id, marketer_TableKey);
		locationValidationErrors = FirstParty_DataSources_Display.return_locationValidationErrors(id,
				marketer_TableKey);
		csvToCheckAgainst = FirstParty_DataSources_Display.return_CSVFile_ToCheckAgainst(file);
		csvToCheckAgainst_reformatted = FirstParty_DataSources_Display
				.format_CSVFile_ToMatchDB_ForProducts(csvToCheckAgainst.cloneTable());

		csv_ExpectedStandardizedResults = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedStandardizedResultsFile);
		csv_ExpectedStandardizedResults.replace_Null_With_Empty();

		csv_ExpectedPassThroughs = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedPassThroughsFile);
		csv_ExpectedPassThroughs.replace_Null_With_Empty();
		csv_ExpectedPassThroughs.replaceValue_ForColumn_WhenEqualTo("Value", "NULL", "");
		csv_ExpectedPassThroughs.sort_ByColumn_Ascending("Name");
		csv_ExpectedPassThroughs.sort_ByColumn_numeric_Ascending("LocationId");

		csv_ExpectedErrors = FirstParty_DataSources_Display.return_CSVFile_ToCheckAgainst(expectedErrorsFile);

		FirstParty_DataSources_Display.apply_newIDs_ToExpectedTables(csv_ExpectedStandardizedResults,
				new TableData[] { csv_ExpectedPassThroughs, csv_ExpectedErrors },
				Integer.parseInt(locations.data.get(0).get("LocationId")), "LocationId");

		FirstParty_DataSources_Display.verify_ErrorsPanel(expectedErrors, addressStandards, latLongMatch);
	}

	// Step 11
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT278_VerifyDatabase_csv_CorrectCount_OfEntries() throws Exception {
		csvToCheckAgainst_reformatted.verify_TableSize(expectedRecords);
	}

	// Step 14
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests"  }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_VerifyDatabase_Locations_LatLongMatchLevel_ArePopulated_if_StandardizedZip5_Populated()
			throws Exception {
		locations.verify_Column_MatchesValue_WhenOtherColumnPopulated_elseEmpty("LatLongMatchLevel", "99",
				"StandardizedZip5");

	}

	// Step 13
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_VerifyDatabase_Locations_FileIdMatches_Id() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("FileId", id, locations.data);
	}

	// Step 4
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_VerifyDatabase_LocationsTable_ActiveIndicator_Matches_CSV() throws Exception {
		BaseUI.verify_TableColumns_Match("ActiveIndicator", "Active", csvToCheckAgainst_reformatted.data,
				locations.data);
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "compare_CSV_To_Locations")
	public Object[][] csv_To_locations_TableMappings() {
		Object[][] linksToNavigate = { { "LocationName", "Name" }, { "RegionCode", "RegionId" },
				{ "Address1", "Address1" }, { "Address2", "Address2" }, { "LocationCode", "Code" }, { "City", "City" },
				{ "State", "State" }, { "Direction1", "Direction1" }, { "Direction2", "Direction2" },
				{ "Zip5", "Zip5" }, { "Zip4", "Zip4" }, { "Latitude", "Latitude" }, { "Longitude", "Longitude" },
				{ "LocationMailIndicator", "MailIndicator" }, { "LocationManager", "Manager" }, { "Phone", "Phone" },
				{ "Type", "Type" } };

		return linksToNavigate;
	}

	// Step 3
	@Test(dataProvider = "compare_CSV_To_Locations", priority = 40, groups = { "all_Tests",
			"regression_Tests", "firstPartyData_Tests" }, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_VerifyColumnsMatch_CSV_to_LocationsTable(String csvColumnName, String locationsColumnName)
			throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, locationsColumnName, csvToCheckAgainst.data, locations.data);
	}

	@DataProvider(name = "compare_CSV_To_Locations_Standardized")
	public Object[][] csv_To_locations_TableMappings_Standardized() {
		Object[][] linksToNavigate = {
				{ "StandardizedAddress1", "StandardizedAddress1", csv_ExpectedStandardizedResults },
				{ "StandardizedAddress2", "StandardizedAddress2", csv_ExpectedStandardizedResults },
				{ "State", "StandardizedState", csvToCheckAgainst_reformatted },
				{ "City", "StandardizedCity", csvToCheckAgainst_reformatted },
				{ "Zip5", "StandardizedZip5", csvToCheckAgainst_reformatted },
				{ "StandardizedZip5", "StandardizedZip5", csv_ExpectedStandardizedResults },
				{ "StandardizedZip4", "StandardizedZip4", csv_ExpectedStandardizedResults },
				{ "StandardizationError", "StandardizationError", csv_ExpectedStandardizedResults } };

		return linksToNavigate;
	}

	// Step 7 and Step 8
	@Test(dataProvider = "compare_CSV_To_Locations_Standardized", priority = 40, groups = {
			"all_Tests", "regression_Tests", "firstPartyData_Tests" }, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_Step11_Step12_VerifyColumnsMatch_CSV_to_LocationsTable_WithFilters(String csvColumnName,
			String locationsColumnName, TableData tableWithExpectedResults) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, locationsColumnName, tableWithExpectedResults.data,
				locations.data);
	}

	// Step 5
	@DataProvider(name = "compare_PassThroughs")
	public Object[][] locationPassThroughColumns() {
		Object[][] linksToNavigate = { { "LocationId" }, { "Name" }, { "Value" } };

		return linksToNavigate;
	}

	// Step 5
	@Test(dataProvider = "compare_PassThroughs", groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_VerifyDatabase_PassThroughs(String columnName) throws Exception {
		BaseUI.verify_TableColumns_Match(columnName, csv_ExpectedPassThroughs.data, locationPassThroughs.data);
	}

	@DataProvider(name = "compare_Errors")
	public Object[][] locationErrorsColumns() {
		Object[][] linksToNavigate = { { "LocationId" }, { "Error" } };

		return linksToNavigate;
	}

	// Step 3
	@Test(dataProvider = "compare_Errors", groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT279_VerifyDatabase_Errors(String columnName) throws Exception {
		BaseUI.verify_TableColumns_Match(columnName, csv_ExpectedErrors.data, locationValidationErrors.data);
	}

	// Step 6
	// Expecting 3 passthroughs per record.
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT280_VerifyDatabase_PassThroughs_CorrectNumberOfEntries() throws Exception {
		locationPassThroughs.verify_TableSize(expectedRecords * 3);
	}

	// Step 4
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT279_VerifyDatabase_ErrorTable_VerifyCountOfUniqueProductIds() throws Exception {
		locationValidationErrors.verify_CountOfUniqueColumnValues(expectedErrors, "LocationId");
	}

	// Step 5
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 40, dependsOnMethods="PT278_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT279_VerifyDatabase_ErrorTable_CorrectCountOfEntries() throws Exception {
		locationValidationErrors.verify_TableSize(7);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
