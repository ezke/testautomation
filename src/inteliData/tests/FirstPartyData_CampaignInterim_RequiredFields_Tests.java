package inteliData.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.DataSource_Modal;
import inteliData.pages.DataSourceImport;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.FirstParty_Mappings;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class FirstPartyData_CampaignInterim_RequiredFields_Tests extends baseTest {

	String name = "Automation - Campaign Interim Required Fields";
	String account = "ACME";
	String marketer = "1";
	String template = "Campaign Drop - Interim";

	String file = "\\src\\inteliData\\data\\DummyHeaderFile.csv";
	String fileName = "DummyHeaderFile.csv";

	HashMap<String, String> expectedMappings;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings = new HashMap<String, String>();
		expectedMappings.put("test1", "");
		expectedMappings.put("test2", "");
		expectedMappings.put("test3", "");
		expectedMappings.put("test4", "");
		expectedMappings.put("test5", "");

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.click_CreateButton();
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer, template);
		DataSourceImport.chooseFile(file, true);
	}

	// Step 6
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 10)
	public void PT314_Step6_FirstPartyMappings_VerifyDefaultMappings_Empty() throws Exception {
		TableData mappingsTable = FirstParty_Mappings.mapTable.return_TableData();
		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 15)
	public void PT314_Step7_FirstPartyMappings_VerifyDefaultMappings_ClickMap_ValidateError() throws Exception {
		String[] requiredFields = { "Address Line1", "City", "State", "Zip" };

		FirstParty_Mappings.click_Map_Button();
		FirstParty_Mappings.verify_Error_RequiredFields(requiredFields);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
