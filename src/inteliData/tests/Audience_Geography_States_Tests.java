package inteliData.tests;

import java.util.ArrayList;
import java.util.HashMap;

import inteliData.page_controls.Geography_State_Info;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_Order_OutputOptions;
import inteliData.pages.CreateAudience_PurchaseRecordsPage;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;

import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Audience_Geography_States_Tests extends baseTest {

    private final String poNumber = "State Geo-Automation PO";

    private final String audienceName = "StateGeo-Automation";
    private final String audienceMart = "Auto Mart";
    private final String audienceAccount = "ACME";

    private final String marketer = GlobalVariables.defaultMarketer;
    private String review_CreationTime = "";

    // private String expectedTotalAsString;
    private int expectedTotal;
    private int audienceID;

    private HashMap<String, String> expectedFilterValues = new HashMap<String, String>();

    // Level 1 variables
    private final String level1_Name = "auto level 1";
    private final int ficoRange1 = 500;
    private final int ficoRange2 = 501;
    private final String[] statesToSelect_Initial = {"ALABAMA", "ALASKA", "ARIZONA"};
    private final String[] statesAbbreviated_Initial = {"AL", "AK", "AZ"};

    // Level 2 variables
    private final String level2_Name = "auto level 2";
    private final int ficoRange1_Level2 = 502;
    private final int ficoRange2_Level2 = 503;
    private final String[] statesToSelect_Level2 = {"ALABAMA", "WISCONSIN"};
    private final String[] statesAbbreviated_Level2 = {"AL", "WI"};

    private HashMap<String, String> expectedGlobalGeography;
    private HashMap<String, String> level1ExpectedValues;
    private HashMap<String, String> level2ExpectedValues;

    private TableData resultsTable;
    private TableData csvTable;
    //private final ArrayList<HashMap<String, Object>> _stateLevelInfo = new ArrayList<HashMap<String, Object>>();
    private ArrayList<Geography_State_Info> _stateLevelInfo;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        expectedGlobalGeography = new HashMap<String, String>();
        expectedGlobalGeography.put("State list",
                CreateAudience_SearchReview.returnArray_AsString_ForPanels(statesToSelect_Initial));

        level1ExpectedValues = new HashMap<String, String>();
        level1ExpectedValues.put("Geography", "Use global geography");
        level1ExpectedValues.put("FICO auto score",
                "From " + String.valueOf(ficoRange1) + " To " + String.valueOf(ficoRange2));

        level2ExpectedValues = new HashMap<String, String>();
        level2ExpectedValues.put("State list",
                CreateAudience_SearchReview.returnArray_AsString_ForPanels(statesToSelect_Level2));
        level2ExpectedValues.put("FICO auto score",
                "From " + String.valueOf(ficoRange1_Level2) + " To " + String.valueOf(ficoRange2_Level2));

        expectedFilterValues.put("Geography", "Use global geography");
        expectedFilterValues.put("FICO auto score", "From 500 To 510");
        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(audienceName, audienceMart, audienceAccount, null,
                null);
        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();
        CreateAudience_GlobalCriteria.stateSelector.select_States(statesToSelect_Initial);
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1_Level2, ficoRange2_Level2);
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();
        CreateAudience_DefineLevels.stateSelector.select_States(statesToSelect_Level2);
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT354_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(expectedGlobalGeography);
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT354_SearchReview_Verify_Level1Panel() throws Exception {
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(level1ExpectedValues);
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT354_SearchReview_Verify_Level2Panel() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(level2ExpectedValues);
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 20)
    public void PT354_SearchReview_Click_EditGlobal_VerifyStatesRetained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.stateSelector.verify_StatesSelected(statesToSelect_Initial);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 30)
    public void PT354_SearchReview_GlobalVariables_Verify_StatesAreStillAccurate() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(expectedGlobalGeography);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 40)
    public void PT354_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(ficoRange1, ficoRange2);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 41)
    public void PT354_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(ficoRange1_Level2, ficoRange2_Level2);
    }

    // Step 30
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 42)
    public void PT354_SearchReview_EditLevels_Level2_GeographySelections_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();

        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.verify_CheckboxNOTChecked();

        CreateAudience_DefineLevels.stateSelector.verify_StatesSelected(statesToSelect_Level2);
    }

    // Step 32
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 44)
    public void PT354_SearchReview_Level1_StillValid() throws Exception {

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(level1ExpectedValues);
    }

    // Step 32
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 45)
    public void PT354_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(level2ExpectedValues);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 49)
    public void PT354_AbleToGetTo_SearchOrderRecords() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.verify_AtPage();
    }

    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 50)
    public void PT354_SearchReview_BuildAudience_Level1_And2_StatesFoundInTable() throws Exception {
        CreateAudience_SearchOrderRecords.retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing();
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("State");
        resultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(resultsTable, "1",
                statesAbbreviated_Initial, "State");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(resultsTable, "2",
                statesAbbreviated_Level2, "State");
    }

    // Step 36
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 60)
    public void PT354_SearchOrderRecords_PickRecords_FooterRequested_MatchesTotalWePick() throws Exception {
        CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(resultsTable, 10, 10);
        expectedTotal = resultsTable.return_Sum_ofColumn("Requested").intValueExact();
        audienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));
        CreateAudience_SearchOrderRecords.verify_Footer_Requested(expectedTotal);
    }

    // Step 39
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 70)
    public void PT354_PurchaseRecords_VerifyAudienceSummary() throws Exception {
        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(poNumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(audienceID, audienceMart, marketer,
                review_CreationTime, expectedTotal, poNumber);
    }

    // Step 39
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 71)
    public void PT354_PurchaseRecords_Results_FooterAccurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(expectedTotal),
                String.valueOf(expectedTotal));
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 75)
    public void PT354_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 80)
    public void PT354_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";
        String fileName = "";

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
        fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
        csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1,
                fileName);

        _stateLevelInfo = new ArrayList<Geography_State_Info>();
        _stateLevelInfo.add(Geography_State_Info.get_GeographyState_NoDistance("1", Range.between(ficoRange1, ficoRange2), statesAbbreviated_Initial));
        _stateLevelInfo.add(Geography_State_Info.get_GeographyState_NoDistance("2", Range.between(ficoRange1_Level2, ficoRange2_Level2), statesAbbreviated_Level2));

        csvTable.verify_TableSize(expectedTotal);
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 81)
    public void PT354_DownloadRecords_Verify_CSV_Lines_FICO_AndState() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_FICO_And_States(csvTable, _stateLevelInfo);
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 81)
    public void PT354_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificStates() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_RequestedCounts_MatchCSVCounts(resultsTable, csvTable, "State", "STATE");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();
    }
}// End of Class
