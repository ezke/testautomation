package inteliData.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.Data_Import;
import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.SuppressionCompleted;
import inteliData.pages.SuppressionImport;
import inteliData.pages.SuppressionMap;
import inteliData.pages.Suppressions;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class External_Suppressions_Tests extends baseTest {

	String mainWindowHandle = "";
	String poNumber = "automation123";
	String account = "ACME";
	String account_DBName = "su_4586";
	String marketer = "1";
	String suppressionType = "Individual, Phone";

	String uploadTime = "";
	TableData suppressionMappings;

	Integer initialRecords = 248;
	// One record gets removed due to not following rules or something.
	Integer finalRecords = 247;

	HashMap<String, String> expectedMappings = new HashMap<String, String>();

	String orderID = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings.put("fname", "First Name");
		expectedMappings.put("lname", "Last Name");
		expectedMappings.put("addr1", "Address Line1");
		expectedMappings.put("zip", "Zip");
		expectedMappings.put("PHONE_X", "");
		if(GlobalVariables.environment.equals("DEV")){
			expectedMappings.put("PHONE_X", "Phone");
		}

		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

		Navigation.navigate_Suppressions();
		Suppressions.createButton.click_Button();
		Suppressions.createButton.click_DropdownLink("Suppression");
	}

	// Step 3
	@Test(priority = 10, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_AbleToGetTo_SuppressionImportPage() throws Exception {
		SuppressionImport.verify_SuppresionImport_TitleVisible();
	}

	// Step 6
	@Test(priority = 20, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_SuppressionMap_VerifyDetails() throws Exception {
		SuppressionImport.enterText_PONumber(poNumber);
		SuppressionImport.select_Account(account);
		uploadTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, SuppressionMap.dateUploaded_Format);
		SuppressionImport.chooseFile(GlobalVariables.suppressionSampleFile);
		SuppressionMap.verify_detailsPanel(GlobalVariables.suppressionSampleFile_Name,
				GlobalVariables.suppressionSampleFile_Name, uploadTime, GlobalVariables.default_UserDesignation, initialRecords);
	}

	// Step 7
	@Test(priority = 21, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_SuppressionMap_Verify_InitialMappings() throws Exception {
		suppressionMappings = SuppressionMap.mapTable.return_TableData();

		SuppressionMap.verify_Mappings(suppressionMappings, expectedMappings);
	}

	// Steps 8-9
	@Test(priority = 30, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_ConfirmMappedFields_VerifyDetails() throws Exception {
		expectedMappings.put("PHONE_X", "Phone");
		
		SuppressionMap.mapTable.selectRow_BySource(suppressionMappings, "fname");
		SuppressionMap.mapTable.selectRow_BySource(suppressionMappings, "lname");
		SuppressionMap.mapTable.selectRow_BySource(suppressionMappings, "addr1");
		SuppressionMap.mapTable.selectRow_BySource(suppressionMappings, "zip");
		//SuppressionMap.mapTable.selectRow_BySource(suppressionMappings, "PHONE_X");
		SuppressionMap.mapTable.setDestination(suppressionMappings, "PHONE_X", "Phone");
		SuppressionMap.mapTable.selectRow_BySource(suppressionMappings, "PHONE_X");
		SuppressionMap.click_Map_Button();
		String uploadTimeReformatted = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				SuppressionMap.dateUploaded_Format, ConfirmMappedFields.details_Date_Format, 0);

		ConfirmMappedFields.verify_DetailsPanel_MatchesExpected(GlobalVariables.suppressionSampleFile_Name,
				initialRecords, GlobalVariables.default_UserDesignation, uploadTimeReformatted);
	}

	// Step 10
	@Test(priority = 31, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_ConfirmMappedFields_VerifyMappings() throws Exception {
		TableData confirmedMappingTable = ConfirmMappedFields.mappingTable.return_TableData();
		// updated to match the mapping that we added.
		ConfirmMappedFields.verify_Mappings(confirmedMappingTable, expectedMappings);

	}

	// Step 11
	@Test(priority = 40, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_SuppressionCompleted_VerifyTitle_Matches_FileName() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		orderID = SuppressionCompleted.return_OrderID();
		SuppressionCompleted.verify_Title(GlobalVariables.suppressionSampleFile_Name);
	}

	// Step 12
	@Test(priority = 41, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_SuppressionCompleted_VerifyDetailsPanel() throws Exception {
		SuppressionCompleted.verify_ExternalSuppressions_DetailsPanel(poNumber, account, marketer, GlobalVariables.default_UserDesignation, finalRecords, uploadTime,
				GlobalVariables.suppressionSampleFile_Name, initialRecords, suppressionType);
	}

	// Step 13
	@Test(priority = 41, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_SuppressionCompleted_VerifySuppressionLevels() throws Exception {
		TableData suppressionLevelsTable = SuppressionCompleted.suppressionLevelsTable.return_TableData();
		TableData expectedTable = SuppressionCompleted.return_TestData_ForPT243(finalRecords);

		suppressionLevelsTable.verify_TableSize(3);
		expectedTable.verify_TableSize(3);

		for (Integer i = 0; i < expectedTable.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), expectedTable.data.get(i), suppressionLevelsTable.data.get(i));
		}

	}

	// Step 15
	@Test(priority = 41, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_Verify_Mart1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart1,
				account_DBName, orderID, finalRecords);
	}

	// Step 15
	@Test(priority = 41, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_Verify_Mart2_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart2,
				account_DBName, orderID, finalRecords);
	}

	// Step 15
	@Test(priority = 41, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT243_Verify_Ful1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Ful1,
				account_DBName, orderID, finalRecords);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
		Data_Import.updateSuppressionFile(orderID);
	}

}// End of Class
