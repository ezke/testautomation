package inteliData.tests;

import java.util.ArrayList;
import java.util.HashMap;

import inteliData.page_controls.Geography_State_Info;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.page_controls.ZipCodeRadiusSelector.ZipRadiusInclusionLevel;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_Order_OutputOptions;
import inteliData.pages.CreateAudience_PurchaseRecordsPage;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;

import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class Audience_Geography_ZipCodeRadius_AND_Exclusion_Tests extends baseTest {

    private final String _poNumber = "Zip Geo-Automation PO";

    private final String _audienceName = "Zip Code Radius - Automation";
    private final String _audienceMart = "Auto Mart";
    private final String _audienceAccount = "ACME";

    private final String _marketer = GlobalVariables.defaultMarketer;
    private String _review_CreationTime = "";

    private int _expectedTotal;
    private int _audienceID;

    // Global Criteria variables
    private final String _zipCode_Global = "60091";
    private final Range<Integer> _zipCode_Global_RadiusRange = Range.between(0, 50);

    // Level 1 variables
    private final String _level1_Name = "auto level 1";
    private final Range<Integer> _ficoRange_Level1 = Range.between(605, 610);
    private final String[] _expectedStates_Level1 = {"IL", "WI", "IN"};

    // Level 2 variables
    private final String _level2_Name = "auto level 2";
    private final Range<Integer> _ficoRange_Level2 = Range.between(611, 615);
    private final String _zipCode_Level2 = "60076";
    private final Range<Integer> _zipCode_RadiusRange_Level2 = Range.between(1, 49);
    private final String[] _expectedStates_Level2 = {"IL"};

    // Level 3 variables
    private final String _level3_Name = "auto level 3";
    private final Range<Integer> _ficoRange_Level3 = Range.between(616, 620);
    private final String _zipCode_Level3 = "60025";
    private final Range<Integer> _zipCode_RadiusRange_Level3 = Range.between(2, 48);
    private final String[] _expectedStates_Level3 = {"WI", "IN"};

    private HashMap<String, String> _expectedGlobalGeography;
    private HashMap<String, String> _level1ExpectedValues;
    private HashMap<String, String> _level2ExpectedValues;
    private HashMap<String, String> _level3ExpectedValues;

    private TableData _state_ResultsTable;
    private TableData _zip_ResultsTable;
    private TableData _csvTable;
    private final ArrayList<Geography_State_Info> _stateLevelInfo = new ArrayList<>();

    // Variables for Rebuild Tests
    private TableData _chicago_Info;
    private TableData _hobart_Info;
    private TableData _racine_Info;

    private String[] _zipCodesToExclude = {"60629", "46342", "53403"};
    private HashMap<String, String> _expectedGlobalRejects;
    private TableData _state_ResultsTable_AfterExclusionAndRebuild;

    private int _rebuild_NewExpectedTotal;
    private int _rebuild_NewAudienceID;

    private final String _rebuild_PONumber = "Zip Geo-Automation PO - Rebuild";

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        _expectedGlobalGeography = new HashMap<String, String>();
        _expectedGlobalGeography.put("Zip code", _zipCode_Global);
        _expectedGlobalGeography.put("Zip code radius",
                "From " + String.valueOf(_zipCode_Global_RadiusRange.getMinimum()) + " To "
                        + String.valueOf(_zipCode_Global_RadiusRange.getMaximum()));
        _expectedGlobalGeography.put("Zip code radius option", "All");

        _level1ExpectedValues = new HashMap<String, String>();
        _level1ExpectedValues.put("Geography", "Use global geography");
        _level1ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level1.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level1.getMaximum()));

        _level2ExpectedValues = new HashMap<String, String>();
        _level2ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level2.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level2.getMaximum()));
        _level2ExpectedValues.put("Zip code", _zipCode_Level2);
        _level2ExpectedValues.put("Zip code radius", "From " + String.valueOf(_zipCode_RadiusRange_Level2.getMinimum())
                + " To " + String.valueOf(_zipCode_RadiusRange_Level2.getMaximum()));
        _level2ExpectedValues.put("Zip code radius option", "Include");

        _level3ExpectedValues = new HashMap<String, String>();
        _level3ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level3.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level3.getMaximum()));
        _level3ExpectedValues.put("Zip code", _zipCode_Level3);
        _level3ExpectedValues.put("Zip code radius", "From " + String.valueOf(_zipCode_RadiusRange_Level3.getMinimum())
                + " To " + String.valueOf(_zipCode_RadiusRange_Level3.getMaximum()));
        _level3ExpectedValues.put("Zip code radius option", "Exclude");

        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(_audienceName, _audienceMart, _audienceAccount,
                null, null);
        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();

        CreateAudience_GlobalCriteria.zip_RadiusSelector.enter_ZipCodeInfo(_zipCode_Global,
                _zipCode_Global_RadiusRange.getMinimum(), _zipCode_Global_RadiusRange.getMaximum());
        CreateAudience_GlobalCriteria.zip_RadiusSelector
                .select_ZipCode_InclusionOrExclusion(ZipRadiusInclusionLevel.INCLUDE_ALL_ZIPS);

        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(_level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();
        CreateAudience_DefineLevels.zip_RadiusSelector.enter_ZipCodeInfo(_zipCode_Level2,
                _zipCode_RadiusRange_Level2.getMinimum(), _zipCode_RadiusRange_Level2.getMaximum());
        CreateAudience_DefineLevels.zip_RadiusSelector
                .select_ZipCode_InclusionOrExclusion(ZipRadiusInclusionLevel.INCLUDE_ONLY_THE_STATE);
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level3_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level3.getMinimum(), _ficoRange_Level3.getMaximum());
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();
        CreateAudience_DefineLevels.zip_RadiusSelector.enter_ZipCodeInfo(_zipCode_Level3,
                _zipCode_RadiusRange_Level3.getMinimum(), _zipCode_RadiusRange_Level3.getMaximum());
        CreateAudience_DefineLevels.zip_RadiusSelector
                .select_ZipCode_InclusionOrExclusion(ZipRadiusInclusionLevel.EXCLUDE_THE_STATE);
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT363_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT363_SearchReview_Verify_Level1Panel() throws Exception {
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT363_SearchReview_Verify_Level2Panel() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT363_SearchReview_Verify_Level3Panel() throws Exception {
        CreateAudience_SearchReview.level3_Panel.verify_ExpectedData_Matches_ReturnedData(_level3ExpectedValues);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT363_SearchReview_Verify_SuppressionsPanel() throws Exception {
        HashMap<String, String> expectedSuppressions = new HashMap<String, String>();
        expectedSuppressions.put("Suppression level", "Address (Address Only)");
        CreateAudience_SearchReview.suppressions_Panel.verify_ExpectedData_Matches_ReturnedData(expectedSuppressions);
    }

    // Step 30
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 20)
    public void PT363_SearchReview_Click_EditGlobal_Verify_ZipRadiusRetained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.zip_RadiusSelector.verify_ZipRadiusFields(_zipCode_Global,
                _zipCode_Global_RadiusRange.getMinimum(), _zipCode_Global_RadiusRange.getMaximum(),
                ZipRadiusInclusionLevel.INCLUDE_ALL_ZIPS);
    }

    // Step 31
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 30)
    public void PT363_SearchReview_GlobalGeography_Verify_ZipsAreStillAccurate() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 40)
    public void PT363_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());

    }

    // Step 37
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 41)
    public void PT363_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());

    }

    // Step 38
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 42)
    public void PT363_SearchReview_EditLevels_Level2_ZipRadius_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.zip_RadiusSelector.verify_ZipRadiusFields(_zipCode_Level2,
                _zipCode_RadiusRange_Level2.getMinimum(), _zipCode_RadiusRange_Level2.getMaximum(),
                ZipRadiusInclusionLevel.INCLUDE_ONLY_THE_STATE);
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 50)
    public void PT363_SearchReview_EditLevels_Level3_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 3");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level3.getMinimum(), _ficoRange_Level3.getMaximum());

    }

    // Step 42
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 51)
    public void PT363_SearchReview_EditLevels_Level3_ZipRadius_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.zip_RadiusSelector.verify_ZipRadiusFields(_zipCode_Level3,
                _zipCode_RadiusRange_Level3.getMinimum(), _zipCode_RadiusRange_Level3.getMaximum(),
                ZipRadiusInclusionLevel.EXCLUDE_THE_STATE);
    }

    // Step 43
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 64)
    public void PT363_SearchReview_Level1_StillValid() throws Exception {

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 43
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 65)
    public void PT363_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }

    // Step 43
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 65)
    public void PT363_SearchReview_Level3_StillValid() throws Exception {
        CreateAudience_SearchReview.level3_Panel.verify_ExpectedData_Matches_ReturnedData(_level3ExpectedValues);
    }

    
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 69)
    public void PT363_AbleToGetTo_SearchOrderRecords() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.verify_AtPage();
    }

    // Step 45
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 70)
    public void PT363_SearchReview_BuildAudience_FoundExpectedZips() throws Exception {
        CreateAudience_SearchOrderRecords.retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing();

        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("Zip Code");
        _zip_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();

        HashMap<String, String> hobartZip = new HashMap<String, String>();
        hobartZip.put("Zip Code", "46342");
        hobartZip.put("City", "HOBART");
        hobartZip.put("County", "LAKE");
        hobartZip.put("State", "IN");

        HashMap<String, String> racineZip = new HashMap<String, String>();
        racineZip.put("Zip Code", "53403");
        racineZip.put("City", "RACINE");
        racineZip.put("County", "RACINE");
        racineZip.put("State", "WI");

        HashMap<String, String> chicagoZip = new HashMap<String, String>();
        chicagoZip.put("Zip Code", "60629");
        chicagoZip.put("City", "CHICAGO");
        chicagoZip.put("County", "COOK");
        chicagoZip.put("State", "IL");

        _chicago_Info = _zip_ResultsTable.return_Table_BasedOn_AnyNumberOfMatchingFields(chicagoZip);
        _hobart_Info = _zip_ResultsTable.return_Table_BasedOn_AnyNumberOfMatchingFields(hobartZip);
        _racine_Info = _zip_ResultsTable.return_Table_BasedOn_AnyNumberOfMatchingFields(racineZip);

        _zip_ResultsTable.verify_MatchingRow_Found(hobartZip);
        _zip_ResultsTable.verify_MatchingRow_Found(racineZip);
        _zip_ResultsTable.verify_MatchingRow_Found(chicagoZip);
    }

    // Step 46
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 75)
    public void PT363_SearchReview_BuildAudience_Level1_And2_StatesFoundInTable() throws Exception {

        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("State");
        _state_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();

        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_state_ResultsTable, "1",
                _expectedStates_Level1, "State");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_state_ResultsTable, "2",
                _expectedStates_Level2, "State");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_state_ResultsTable, "3",
                _expectedStates_Level3, "State");
    }

    // Step 48
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 80)
    public void PT363_SearchOrderRecords_PickRecords_FooterRequested_MatchesTotalWePick() throws Exception {
        CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(_state_ResultsTable, 10,
                10);
        _expectedTotal = _state_ResultsTable.return_Sum_ofColumn("Requested").intValueExact();
        _audienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));
        CreateAudience_SearchOrderRecords.verify_Footer_Requested(_expectedTotal);
    }

    // Step 51
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 90)
    public void PT363_PurchaseRecords_VerifyAudienceSummary() throws Exception {
        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(_poNumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(_audienceID, _audienceMart, _marketer,
                _review_CreationTime, _expectedTotal, _poNumber);
    }

    // Step 51
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT363_PurchaseRecords_Results_FooterAccurate() throws Exception {

        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(_expectedTotal),
                String.valueOf(_expectedTotal));
    }

    // Step 53
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 105)
    public void PT363_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 53
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 110)
    public void PT363_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";
        String fileName = "";

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
        fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
        _csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1,
                fileName);

        _stateLevelInfo.add(Geography_State_Info.get_GeographyState_AndDistance("1", _ficoRange_Level1, _expectedStates_Level1, _zipCode_Global_RadiusRange));
        _stateLevelInfo.add(Geography_State_Info.get_GeographyState_AndDistance("2", _ficoRange_Level2, _expectedStates_Level2, _zipCode_RadiusRange_Level2));
        _stateLevelInfo.add(Geography_State_Info.get_GeographyState_AndDistance("3", _ficoRange_Level3, _expectedStates_Level3, _zipCode_RadiusRange_Level3));

        _csvTable.verify_TableSize(_expectedTotal);
    }

    // Step 53
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT363_DownloadRecords_Verify_CSV_Lines_FICO_AndState() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_FICO_And_States(_csvTable, _stateLevelInfo);
    }

    // Step 53
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT363_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificStates() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_RequestedCounts_MatchCSVCounts(_state_ResultsTable, _csvTable, "State", "STATE");
    }

    // Step 7
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 120)
    public void PT365_DownloadRecords_RebuildAudience_Reject_ZipCodes_ValidateGlobalRejects() throws Exception {
        _expectedGlobalRejects = new HashMap<String, String>();
        _expectedGlobalRejects.put("Zip code list",
                CreateAudience_SearchReview.returnArray_AsString_ForPanels(_zipCodesToExclude));

        CreateAudience_DownloadRecords_Page.click_Actions_RecreateAudience();
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.click_Exclude();
        CreateAudience_GlobalCriteria.exclude_ZipCodes_TextBox.enterText(_zipCodesToExclude);
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalRejects_Panel.verify_ExpectedData_Matches_ReturnedData(_expectedGlobalRejects);
    }

    // Step 8
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 130)
    public void PT365_SearchReview_Click_EditGlobal_Verify_ZipRadiusRetained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.zip_RadiusSelector.verify_ZipRadiusFields(_zipCode_Global,
                _zipCode_Global_RadiusRange.getMinimum(), _zipCode_Global_RadiusRange.getMaximum(),
                ZipRadiusInclusionLevel.INCLUDE_ALL_ZIPS);
    }

    // Step 9
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 135)
    public void PT365_SearchReview_Click_EditGlobal_Exclude_ZipCodesRetained() throws Exception {
        CreateAudience_GlobalCriteria.click_Exclude();
        CreateAudience_GlobalCriteria.exclude_ZipCodes_TextBox.verify_OptionsSelected(_zipCodesToExclude);
    }

    // Step 10
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 140)
    public void PT365_SearchReview_GlobalGeography_Verify_ZipsAreStillAccurate() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);

    }

    // Step 10
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 141)
    public void PT365_SearchReview_GlobalRejects_Verify_ZipsAreStillAccurate() throws Exception {
        CreateAudience_SearchReview.globalRejects_Panel.verify_ExpectedData_Matches_ReturnedData(_expectedGlobalRejects);
    }

    // Step 12
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 150)
    public void PT365_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());

    }

    // Step 14
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 160)
    public void PT365_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());

    }

    // Step 15
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 170)
    public void PT365_SearchReview_EditLevels_Level2_ZipRadius_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.zip_RadiusSelector.verify_ZipRadiusFields(_zipCode_Level2,
                _zipCode_RadiusRange_Level2.getMinimum(), _zipCode_RadiusRange_Level2.getMaximum(),
                ZipRadiusInclusionLevel.INCLUDE_ONLY_THE_STATE);
    }

    // Step 17
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 180)
    public void PT365_SearchReview_EditLevels_Level3_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 3");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level3.getMinimum(), _ficoRange_Level3.getMaximum());

    }

    // Step 18
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 190)
    public void PT365_SearchReview_EditLevels_Level3_ZipRadius_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.zip_RadiusSelector.verify_ZipRadiusFields(_zipCode_Level3,
                _zipCode_RadiusRange_Level3.getMinimum(), _zipCode_RadiusRange_Level3.getMaximum(),
                ZipRadiusInclusionLevel.EXCLUDE_THE_STATE);
    }

    // Step 20
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 200)
    public void PT365_SearchReview_Level1_StillValid() throws Exception {

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 20
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 201)
    public void PT365_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }

    // Step 20
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 201)
    public void PT365_SearchReview_Level3_StillValid() throws Exception {
        CreateAudience_SearchReview.level3_Panel.verify_ExpectedData_Matches_ReturnedData(_level3ExpectedValues);
    }

    // Step 22
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 210)
    public void PT365_SearchOrderRecords_ExcludedZips_NotInList() throws Exception {
        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("Zip Code");
        _zip_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();
        BaseUI.verify_true_AndLog(_zip_ResultsTable.data.size() > 0, "Found Zip Codes table.",
                "Did NOT find Zip Codes table.");

        for (String zip : _zipCodesToExclude) {
            _zip_ResultsTable.verify_Value_NOT_InColumn("Zip Code", zip);
        }
    }

    // Step 22
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 210)
    public void PT365_SearchOrderRecords_ZipDistances_WithinRange() throws Exception {
        CreateAudience_SearchOrderRecords.verify_Distance_Within_DistanceRange(_zip_ResultsTable, _stateLevelInfo);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 220)
    public void PT365_SearchOrderRecords_StatesTable_MatchesExpected() throws Exception {
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("State");
        _state_ResultsTable_AfterExclusionAndRebuild = CreateAudience_SearchOrderRecords.resultsTable
                .return_TableData();

        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_state_ResultsTable, "1",
                _expectedStates_Level1, "State");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_state_ResultsTable, "2",
                _expectedStates_Level2, "State");
        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_state_ResultsTable, "3",
                _expectedStates_Level3, "State");
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 221)
    public void PT365_SearchOrderRecords_StatesTable_TotalsDecreased_Chicago() throws Exception {
        CreateAudience_SearchOrderRecords.verify_RecordCount_Decreased_FromEarlier(_chicago_Info, _state_ResultsTable,
                _state_ResultsTable_AfterExclusionAndRebuild);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 221)
    public void PT365_SearchOrderRecords_StatesTable_TotalsDecreased_hobart() throws Exception {
        CreateAudience_SearchOrderRecords.verify_RecordCount_Decreased_FromEarlier(_hobart_Info, _state_ResultsTable,
                _state_ResultsTable_AfterExclusionAndRebuild);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 221)
    public void PT365_SearchOrderRecords_StatesTable_TotalsDecreased_racine() throws Exception {
        CreateAudience_SearchOrderRecords.verify_RecordCount_Decreased_FromEarlier(_racine_Info, _state_ResultsTable,
                _state_ResultsTable_AfterExclusionAndRebuild);
    }

    // Step 27
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 230)
    public void PT365_RebuildAudience_PurchaseRecords_ValidateAudienceSummaryPanel() throws Exception {
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(
                _state_ResultsTable_AfterExclusionAndRebuild, 10, 10);


        _rebuild_NewExpectedTotal = _state_ResultsTable_AfterExclusionAndRebuild.return_Sum_ofColumn("Requested").intValueExact();
        _rebuild_NewAudienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));

        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(_rebuild_PONumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(_rebuild_NewAudienceID, _audienceMart, _marketer,
                _review_CreationTime, _rebuild_NewExpectedTotal, _rebuild_PONumber);
    }

    // Step 27
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 231)
    public void PT365_RebuildAudience_PurchaseRecords_Results_FooterAccurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(_rebuild_NewExpectedTotal),
                String.valueOf(_rebuild_NewExpectedTotal));
    }


    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 240)
    public void PT365_RebuildAudience_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 250)
    public void PT365_RebuildAudience_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";
        String fileName = "";

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
        fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
        _csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1,
                fileName);

        _csvTable.verify_TableSize(_rebuild_NewExpectedTotal);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 251)
    public void PT365_RebuildAudience_DownloadRecords_Verify_CSV_Lines_FICO_AndState() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_FICO_And_States(_csvTable, _stateLevelInfo);
    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 251)
    public void PT365_RebuildAudience_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificStates() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_RequestedCounts_MatchCSVCounts(_state_ResultsTable_AfterExclusionAndRebuild, _csvTable, "State", "STATE");
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();

    }

}// End of Class
