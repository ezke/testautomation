package inteliData.tests;


import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAppend_Modal;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.GenericModal;
import inteliData.modals.InTheMarketAlerts_Modal;
import inteliData.modals.LendingSignals_Modal;
import inteliData.pages.Accounting_Credits;
import inteliData.pages.Accounting_Invoices;
import inteliData.pages.Activities;
import inteliData.pages.Administration_Automations;
import inteliData.pages.Administration_Checklist;
import inteliData.pages.Administration_DNS;
import inteliData.pages.Administration_Marketers;
import inteliData.pages.DataSources;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.Help;
import inteliData.pages.Help_HowDoI;
import inteliData.pages.Help_SeeWhatsNew;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.Packages;
import inteliData.pages.Programs;
import inteliData.pages.Suppressions;
import inteliData.pages.Tools_MarketAreas;
import inteliData.pages.HomePage;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;

public class Navigation_Tests extends baseTest {

	String mainWindowHandle = "";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		mainWindowHandle = Browser.driver.getWindowHandle();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT179_LoginSuccessful() throws Exception {
		Navigation.navigate_Home();
		HomePage.verify_HomePage_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT186_NavigateSuppressions() throws Exception {
		Navigation.navigate_Suppressions();
		Suppressions.verify_Suppressions_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT181_Navigate_FirstParty_DataSources() throws Exception {
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.verify_firstParty_DataSources_Page_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT184_Navigate_Activities() throws Exception {
		Navigation.navigate_Activities();
		Activities.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT185_Navigate_Programs() throws Exception {
		Navigation.navigate_Programs();
		Programs.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT187_Navigate_DataSources() throws Exception {
		Navigation.navigate_DataSources();
		DataSources.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT189_Navigate_Administration_DNS() throws Exception {
		Navigation.navigate_Administration_DNS();
		Administration_DNS.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT190_Navigate_Administration_Automations() throws Exception {
		Navigation.navigate_Administration_Automations();
		Administration_Automations.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT191_Navigate_Administration_Checklist() throws Exception {
		Navigation.navigate_Administration_Checklist();
		Administration_Checklist.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT192_Navigate_Administration_Marketers() throws Exception {
		Navigation.navigate_Administration_Marketers();
		Administration_Marketers.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT194_Navigate_Accounting_Invoices() throws Exception {
		Navigation.navigate_Accounting_Invoices();
		Accounting_Invoices.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT195_Navigate_Accounting_Credits() throws Exception {
		Navigation.navigate_Accounting_Credits();
		Accounting_Credits.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT196_Navigate_Packages() throws Exception {
		Navigation.navigate_Packages();
		Packages.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT197_Tools_MarketAreas() throws Exception {
		Navigation.navigate_Tools_MarketAreas();
		Tools_MarketAreas.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT198_HelpPage() throws Exception {
		Navigation.navigate_Help();
		Help.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT199_HelpPage_SeeWhatsNew() throws Exception {
		Navigation.navigate_Help();
		Help.click_SeeWhatsNew_Button();
		Help_SeeWhatsNew.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT200_HelpPage_click_SeeGuides_HowDoI_PageLoads() throws Exception {
		Navigation.navigate_Help();
		Help.click_SeeGuides_Button();
		Help_HowDoI.verify_PageLoaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT201_HelpPage_EmailAddress_Encoded_InHREF_AndSubject_Valid() throws Exception {
		Navigation.navigate_Help();
		Help.verify_SendUsANote_HasValidHREF();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT202_HomePage_Click_CreateAudience_NewAudience_Popup_Displays() throws Exception {
		Navigation.navigate_Home();
		HomePage.click_CreateAudience_Button();
		CreateAudience_Modal.verify_CreateAudience_NewAudienceModal_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT203_HomePage_Click_LendingSignals_Popup_Displays() throws Exception {
		Navigation.navigate_Home();
		HomePage.click_CreateLendingSignals_Button();
		LendingSignals_Modal.verify_CreateLendingSignals_NewAudienceModal_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT204_HomePage_Click_InTheMarketAlerts_Popup_Displays() throws Exception {
		Navigation.navigate_Home();
		HomePage.click_CreateProgram_Button();
		InTheMarketAlerts_Modal.verify_InTheMarketAlerts_NewAudienceModal_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT205_HomePage_Click_CreateAppend_Popup_Displays() throws Exception {
		Navigation.navigate_Home();
		HomePage.click_CreateAppend_Button();
		CreateAppend_Modal.verify_CreateAppend_NewAudienceModal_Loaded();
	}

	@Test(groups = { "all_Tests", "smoke_Tests" })
	public void PT206_HomePage_Click_LearnMore_PDF_InNewWindow() throws Exception {
		Navigation.navigate_Home();
		HomePage.click_LearnMore_Button();
		HomePage.verify_LearnMore_PDF_Loaded();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		GenericModal.click_Close_IfDisplayed();

		String currentHandle = Browser.driver.getWindowHandle();
		if (!currentHandle.equals(mainWindowHandle)) {
			BaseUI.close_ExtraWindows(mainWindowHandle);
		}

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

		Browser.closeBrowser();
	}

}// End of Class
