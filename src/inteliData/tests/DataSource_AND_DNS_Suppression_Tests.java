package inteliData.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.DataSource_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.modals.Suppression_Modal;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.DataSourceDisplay;
import inteliData.pages.DataSourceImport;
import inteliData.pages.DataSourceMap;
import inteliData.pages.DataSources;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.SuppressionCompleted;
import inteliData.pages.SuppressionMap;
import inteliData.pages.Suppressions;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class DataSource_AND_DNS_Suppression_Tests extends baseTest {

	String mart = "Auto Mart";
	String name = "automation-DataSource";
	String account = "ACME";
	String account_DBName = "su_4586";
	String marketer = "1";
	Integer initialRecords = 248;
	Integer finalCount = 247;
	String uploadTime = "";
	String uploadTimeReformatted = "";
	HashMap<String, String> expectedMappings = new HashMap<String, String>();
	TableData dataSourceMappings = new TableData();
	String orderID = null;
	
	String[] statesToSelect = { "ALASKA", "RHODE ISLAND" };

	String suppressionType = "DNS Suppression";
	
	String levelName = "AutoTest";
	Integer ficoRange1 = 500;
	Integer ficoRange2 = 510;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings.put("fname", "First Name");
		expectedMappings.put("lname", "Last Name");
		expectedMappings.put("addr1", "Address Line1");
		expectedMappings.put("addr2", "Address Line2");
		expectedMappings.put("zip", "Zip");
		expectedMappings.put("PHONE_X", "");
		if(GlobalVariables.environment.equals("DEV")){
			expectedMappings.put("PHONE_X", "Phone");
		}

		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_DataSources();
		DataSources.createButton.click_Button();
		DataSources.createButton.click_DropdownLink("Data Source");
	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT248_VerifyDataSource_ModalAppears() throws Exception {
		DataSource_Modal.verify_DataSource_Modal_Appears();
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests", "regression_Tests" }, priority = 15)
	public void PT248_Enter_DataSourceModal_Fields_TakenToDataSourceImportPage() throws Exception {
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer);
		DataSourceImport.verify_DataSourceImport_pageLoaded();
	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests", "regression_Tests" }, priority = 20)
	public void PT248_DataSourceImport_ChooseFile_DataSourceMap_PageLoads() throws Exception {
		uploadTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, DataSourceMap.dateUploaded_Format);
		DataSourceImport.chooseFile(GlobalVariables.suppressionSampleFile);
		DataSourceMap.verify_DataSourceMap_Title();
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 21)
	public void PT248_DataSourceMap_VerifyDetailsPanel() throws Exception {
		DataSourceMap.verify_detailsPanel(name, GlobalVariables.suppressionSampleFile_Name, uploadTime, GlobalVariables.default_UserDesignation,
				initialRecords);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests", "regression_Tests" }, priority = 21)
	public void PT248_DataSourceMap_VerifyMappingTable() throws Exception {
		dataSourceMappings = DataSourceMap.mapTable.return_TableData();
		DataSourceMap.verify_Mappings(dataSourceMappings, expectedMappings);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests", "suppression_Tests", "regression_Tests" }, priority = 30)
	public void PT248_SelectMappings_ClickMap_VerifyDetailsPanel_OnConfirmMappedFieldsPage() throws Exception {
		DataSourceMap.mapTable.selectRow_BySource(dataSourceMappings, "fname");
		DataSourceMap.mapTable.selectRow_BySource(dataSourceMappings, "lname");
		DataSourceMap.mapTable.selectRow_BySource(dataSourceMappings, "addr1");
		DataSourceMap.mapTable.selectRow_BySource(dataSourceMappings, "addr2");
		DataSourceMap.mapTable.selectRow_BySource(dataSourceMappings, "zip");

		DataSourceMap.click_Map_Button();
		uploadTimeReformatted = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				SuppressionMap.dateUploaded_Format, ConfirmMappedFields.details_Date_Format, 0);

		ConfirmMappedFields.verify_DetailsPanel_MatchesExpected(name, initialRecords, GlobalVariables.default_UserDesignation, uploadTimeReformatted);
	}

	// Step 11
	@Test(priority = 31, groups = { "all_Tests", "critical_Tests" })
	public void PT248_ConfirmMappedFields_VerifyMappings() throws Exception {
		expectedMappings.put("CITY_X", "");
		expectedMappings.put("STATE_X", "");
		if(GlobalVariables.environment.equals("DEV")){
			expectedMappings.put("PHONE_X", "");
		}
		TableData confirmedMappingTable = ConfirmMappedFields.mappingTable.return_TableData();
		ConfirmMappedFields.verify_Mappings(confirmedMappingTable, expectedMappings);
	}

	// Step 13
	@Test(priority = 35, groups = { "all_Tests", "critical_Tests", "suppression_Tests", "regression_Tests" })
	public void PT248_DataSourceDisplay_VerifyTitle_Matches_Name() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		DataSourceDisplay.verify_Title(name);
	}

	// Step 13
	@Test(priority = 36, groups = { "all_Tests", "critical_Tests" })
	public void PT248_DataSourceDisplay_VerifyDetailsPanel() throws Exception {
		DataSourceDisplay.verify_DetailsPanel(GlobalVariables.suppressionSampleFile_Name, uploadTimeReformatted,
				initialRecords, finalCount);
	}

	// Step 14
	@Test(priority = 36, groups = { "all_Tests", "critical_Tests" })
	public void PT248_DataSourceDisplay_Validate_CurrentFileMappingTable() throws Exception {
		DataSourceDisplay.currentFileMapping_Section.expand_Section();
		TableData currentFileMapping = DataSourceDisplay.currentFileMapping.return_TableData();
		DataSourceDisplay.verify_Mappings(currentFileMapping, expectedMappings);

	}

	// Step 15
	@Test(priority = 36, groups = { "all_Tests", "critical_Tests" })
	public void PT248_DataSourceDisplay_ProgramsTable_HasNoData() throws Exception {
		DataSourceDisplay.programsTable.verify_NoDataInTable();
	}

	// Step 15
	@Test(priority = 36, groups = { "all_Tests", "critical_Tests" })
	public void PT248_DataSourceDisplay_SuppressionsTable_HasNoData() throws Exception {
		DataSourceDisplay.programsTable.verify_NoDataInTable();
	}

	// Step 3
	@Test(priority = 40, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_DataSourceDisplay_SelectActions_CreateSuppresion() throws Exception {
		DataSourceDisplay.actionsButtonDropdown.click_Button();
		DataSourceDisplay.actionsButtonDropdown.click_DropdownLink("Create Suppression");
		Suppression_Modal.verify_Suppression_Modal_Appears();
	}

	// Steps 4 and 5
	@Test(priority = 45, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_SuppressionModal_EnterValues_GoesToSuppressionComplete() throws Exception {
		Suppression_Modal.enter_Suppression_Fields_AndSaveContinue(suppressionType, null, account, marketer, null,
				name);
		orderID = SuppressionCompleted.return_OrderID();
		SuppressionCompleted.verify_Title(name);
	}

	// Step 6
	@Test(priority = 46, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_SuppressionComplete_VerifyDetailsPanel() throws Exception {
		SuppressionCompleted.verify_DNSSuppressions_DetailsPanel(account, marketer, GlobalVariables.default_UserDesignation, finalCount, uploadTime,
				GlobalVariables.suppressionSampleFile_Name, initialRecords, "Address");
	}

	// Step 7
	@Test(priority = 46, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_SuppressionComplete_VerifySuppressionLevels() throws Exception {
		TableData suppressionLevelsTable = SuppressionCompleted.suppressionLevelsTable.return_TableData();
		TableData expectedTable = SuppressionCompleted.return_TestData_ForPT244(finalCount);

		suppressionLevelsTable.verify_TableSize(3);
		expectedTable.verify_TableSize(3);

		for (Integer i = 0; i < expectedTable.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), expectedTable.data.get(i), suppressionLevelsTable.data.get(i));
		}
	}

	// Step 9
	@Test(priority = 46, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_Verify_Mart1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart1,
				account_DBName, orderID, finalCount);
	}

	// Step 9
	@Test(priority = 46, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_Verify_Mart2_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart2,
				account_DBName, orderID, finalCount);
	}

	// Step 9
	@Test(priority = 46, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT244_Verify_Ful1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Ful1,
				account_DBName, orderID, finalCount);
	}

	// Step 
	@Test(priority = 50, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT262_BuildAudience_SearchOrderRecords_Verify_DNSSuppression_ShowsUp() throws Exception {
		Navigation.navigate_Home();
		HomePage.click_CreateAudience_Button();
		CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(name, mart, account, null,
				null);
		SuppliersAndUsage_Modal.equifax_DraggableElement.check_Checkbox();
		SuppliersAndUsage_Modal.experian_DraggableElement.uncheck_Checkbox();
		SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
		SuppliersAndUsage_Modal.click_CreateAudience();
		CreateAudience_GlobalCriteria.stateSelector.select_States(statesToSelect);
		CreateAudience_GlobalCriteria.click_SaveAndContinue();

		CreateAudience_DefineLevels.enter_Level(levelName);
		CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
		CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
		CreateAudience_DefineLevels.click_SaveLevel_Button();
		CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
		CreateAudience_SearchReview.click_Top_BuildAudience_Button();
		CreateAudience_SearchOrderRecords.click_ViewDetailsLink();
		CreateAudience_SearchOrderRecords.verify_DNS_Suppression_Found_AndDateWithinAcceptableRange(orderID, account, marketer, "Address", uploadTime);
	}
	
	// Step 
	@Test(priority = 60, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT263_Suppressions_Delete_DNS_Suppression() throws Exception {
		Navigation.navigate_Suppressions();
		Suppressions.quickSearchBox.search_WithCriteria(orderID);
		Suppressions.delete_Suppression(1);
		Suppressions.quickSearchBox.search_WithCriteria(orderID);
		Suppressions.suppressionTable.verify_NoDataInTable();
	}
	

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
