package inteliData.tests;

import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_Order_OutputOptions;
import inteliData.pages.CreateAudience_PurchaseRecordsPage;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.Netdown_Import;
import inteliData.pages.Netdown_Map;
import inteliData.pages.Netdown_Suppress;
import inteliData.pages.SuppressionDisplay;
import inteliData.pages.Suppressions;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class NetDown_Postback_Tests extends baseTest {

	String audienceName = "Automation_NetDown";
	String audienceMart = "Auto Mart";
	String audienceAccount = "ACME";
	String account_DBName = "su_4586";
	Integer numberOfLevels = 1;
	String marketer = GlobalVariables.defaultMarketer;
	String review_CreationTime = "";
	String[] statesToSelect = { "ALASKA", "RHODE ISLAND" };
	String[] statesAbbreviated = { "AK", "RI" };

	String expectedTotalAsString;
	Integer expectedTotal;
	Integer audienceID;
	String orderID;

	String suppressionType = "Net Down Order";

	HashMap<String, String> expectedFilterValues = new HashMap<String, String>();
	TableData audienceSummaryTable;

	String levelName = "AutoTest";
	Integer ficoRange1 = 500;
	Integer ficoRange2 = 510;

	Integer expectedTotalRecords_Minimum = 2000;
	String purchaseAmount = "";
	String purchaseTime = "";

	String poNumber = "Auto Test - Net Down";
	String createdBy = "Test Automation";
	String fileName = "";
	Integer expectedCSV_TIS_TableSize = 100;

	// Postback variables
	String todaysDate;
	String poNumber_NetdownPostback = "Automation - NetdownPostback";
	String audienceID_NetdownPostback;
	String time_Postback;
	String totalPrice_Postback;
	String orderID_Postback;
	String finalOrderTime;
	String suppressionType_Postback = "Sales Order";

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedFilterValues.put("Geography", "Use global geography");
		expectedFilterValues.put("FICO auto score", "From 500 To 510");
		todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "M/d/yyyy");

		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

		HomePage.click_CreateAudience_Button();
		CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(audienceName, audienceMart, audienceAccount, null,
				null);
		SuppliersAndUsage_Modal.select_AllSuppliers();
		SuppliersAndUsage_Modal.click_CreateAudience();
		CreateAudience_GlobalCriteria.stateSelector.select_States(statesToSelect);
		CreateAudience_GlobalCriteria.click_SaveAndContinue();

		CreateAudience_DefineLevels.enter_Level(levelName);
		CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
		CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
		CreateAudience_DefineLevels.click_SaveLevel_Button();

		review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);
		CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();




	}


	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT256_SearchOrderRecords_AbleToGetTo() throws Exception {
		CreateAudience_SearchReview.click_Top_BuildAudience_Button();
		CreateAudience_SearchOrderRecords.verify_AtPage();
	}


	// Step 21 of 247
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 55)
	public void PT256_CreateAudience_PurchaseRecords_AudienceSummary_Accurate() throws Exception {
		CreateAudience_SearchOrderRecords.retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing();

		HashMap<String, String> mainContentPanelValues = CreateAudience_SearchOrderRecords.mainContent_Panel
				.return_PanelData();
		expectedTotalAsString = mainContentPanelValues.get("Total Records");
		expectedTotal = Integer.parseInt(mainContentPanelValues.get("Total Records").replace(",", ""));
		audienceID = Integer.parseInt(mainContentPanelValues.get("Audience ID"));

		CreateAudience_SearchOrderRecords.resultsTable.checkbox_ByIndex(1).check_Checkbox();
		CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();

		CreateAudience_SearchOrderRecords.click_SelectRecords_Button();

		CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(poNumber);
		CreateAudience_Order_OutputOptions.click_PostNetdown_Yes();
		CreateAudience_Order_OutputOptions.netdown_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name",
				"CSV - TIS Custom");
		// CreateAudience_Order_OutputOptions.netdown_CreateSuppressionCheckbox.check_Checkbox();
		CreateAudience_Order_OutputOptions.click_PlaceOrder();

		CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(audienceID, audienceMart, marketer,
				review_CreationTime, expectedTotal, poNumber);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 59)
	public void PT256_DownloadRecords_AbleToGetToPage() throws Exception {
		purchaseTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_DownloadRecords_Page.dateFormat);
		CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
		CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
	}
	
	// Step 22 of 247
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 60)
	public void PT256_CreateAudience_FinalPage_ValidateCSV() throws Exception {
		CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();
		
		String typeName = "CSV - TIS Custom (Order File)";
		orderID = CreateAudience_DownloadRecords_Page.return_OrderID();
		TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
		fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
		TableData csvTable = CreateAudience_DownloadRecords_Page
				.download_File_FromDownloadRecords(indexOfLineToDownload + 1, fileName);
		CreateAudience_DownloadRecords_Page.verify_CSV_TIS_Custom_MatchesExpected(csvTable, "1");
		csvTable.verify_TableSize(expectedCSV_TIS_TableSize);
	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 69)
	public void PT256_FinalPage_OrderStatus_UpdatesToShipped() throws Exception {
		CreateAudience_DownloadRecords_Page.refresh_Until_Shipped();
		CreateAudience_DownloadRecords_Page.orderDetails_Panel.verify_ValueMatches("Status", "Shipped");
	}

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 70)
	public void PT256_FinalPage_PostNetdown_TakesYouTo_NetdownImportPage() throws Exception {
		CreateAudience_DownloadRecords_Page.click_Actions_PostNetdown();
		Netdown_Import.verify_TitleDisplayed();
	}

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 71)
	public void PT256_NetdownImport_VerifyNetdownSummary() throws Exception {
		Netdown_Import.verify_NetdownSummary_Panel(orderID, poNumber, marketer, audienceMart, expectedTotal);
	}

	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 80)
	public void PT256_NetdownMap_ValidateDetailsPanel() throws Exception {
		Netdown_Import.chooseFile(Browser.downloadLocation + "\\" + fileName);
		Netdown_Map.verify_DetailsPanel(orderID, expectedCSV_TIS_TableSize, createdBy, todaysDate);
	}

	// Steps 5 and 6
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 90)
	public void PT256_ConfirmMapping_VerifyDetailsPanel() throws Exception {
		TableData currentMapping = Netdown_Map.mapTable.return_TableData();
		Netdown_Map.mapTable.setDestination(currentMapping, "LEVEL_X", "Pass Through");
		Netdown_Map.mapTable.setDestination(currentMapping, "ORDERRECORDID_X", "Order Record Id");
		Netdown_Map.mapTable.selectRow_BySource(currentMapping, "LEVEL_X");
		Netdown_Map.mapTable.selectRow_BySource(currentMapping, "ORDERRECORDID_X");
		Netdown_Map.click_Map();
		ConfirmMappedFields.verify_DetailsPanel_ForNetdowns_MatchesExpected(orderID, expectedCSV_TIS_TableSize,
				createdBy, todaysDate);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 91)
	public void PT256_ConfirmMapping_VerifyMappings() throws Exception {
		HashMap<String, String> expectedMappings = new HashMap<String, String>();
		expectedMappings.put("LEVEL_X", "Pass-Through");
		expectedMappings.put("ORDERRECORDID_X", "Order Record Id");
		ConfirmMappedFields.verify_Mappings(expectedMappings);
	}

	// Step 10
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 100)
	public void PT256_SearchOrderRecords_Select_AndVerify_SelectRecords_Row() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		Netdown_Suppress.click_PlaceOrder();
		CreateAudience_SearchOrderRecords.resultsTable.checkbox_ByIndex(1).check_Checkbox();
		CreateAudience_SearchOrderRecords.verify_1Row_OfData_MatchesExpected(expectedCSV_TIS_TableSize, 0,
				expectedCSV_TIS_TableSize, true);

	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 110)
	public void PT256_OrderOutputOptions_VerifyNetdownOption_NotVisible() throws Exception {
		audienceID_NetdownPostback = CreateAudience_SearchOrderRecords.mainContent_Panel
				.return_SpecificValue("Audience ID");

		time_Postback = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_Order_OutputOptions.dateFormat);
		CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
		CreateAudience_Order_OutputOptions.verify_PostNetdown_Yes_NotDisplayed();
	}

	// Step 11
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 111)
	public void PT256_OrderOutputOptions_VerifyAudienceSummary() throws Exception {
		CreateAudience_Order_OutputOptions.verify_mainContentPanel(Integer.parseInt(audienceID_NetdownPostback),
				audienceMart, marketer, time_Postback, expectedCSV_TIS_TableSize, expectedCSV_TIS_TableSize);
	}

	// Step 14
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 120)
	public void PT256_PurchaseOrderRecords_VerifyValidTotalPrice() throws Exception {
		CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(poNumber_NetdownPostback);
		CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
		CreateAudience_Order_OutputOptions.click_PlaceOrder();

		totalPrice_Postback = CreateAudience_PurchaseRecordsPage.return_TotalPrice();

		// Validate price is valid.
		CreateAudience_PurchaseRecordsPage.verify_TotalPrice_Valid();
	}

	// Netdown Postback should start out as Shipped for the status. Did a Refresh
	// because sometimes it says Posted instead, but changes to Shipped after
	// refresh.
	// Step 15
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 130)
	public void PT256_DownloadRecords_OrderDetails_Shipped() throws Exception {
		finalOrderTime = BaseUI.getDateAsString_InRelationToTodaysDate(0,
				CreateAudience_PurchaseRecordsPage.dateFormat);
		CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();

		orderID_Postback = CreateAudience_DownloadRecords_Page.orderDetails_Panel.return_SpecificValue("Order ID");
		CreateAudience_DownloadRecords_Page.refreshPage();
		CreateAudience_DownloadRecords_Page.verify_OrderDetailsPanel(poNumber_NetdownPostback, time_Postback,
				expectedCSV_TIS_TableSize.toString(), totalPrice_Postback, true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 131)
	public void PT256_DownloadRecords_VerifyAudienceSummary() throws Exception {
		CreateAudience_DownloadRecords_Page.verify_AudienceSummaryPanel(Integer.parseInt(audienceID_NetdownPostback),
				poNumber, audienceMart, audienceAccount, marketer, time_Postback);
	}

	// Step 16
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 132)
	public void PT256_DownloadRecords_ValidateCSV() throws Exception {
		String typeName = "CSV (Order File)";

		TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
		fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
		TableData csvTable = CreateAudience_DownloadRecords_Page
				.download_File_FromDownloadRecords(indexOfLineToDownload + 1, fileName);
		HashMap<String, Range<Integer>> levelRanges = new HashMap<String, Range<Integer>>();
		levelRanges.put("1", Range.between(ficoRange1, ficoRange2));
		CreateAudience_DownloadRecords_Page.verify_CSV_MatchesExpected(csvTable, statesAbbreviated, levelRanges);
		csvTable.verify_TableSize(expectedCSV_TIS_TableSize);
	}

	// Step 17
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 135)
	public void PT256_SuppressionPage_ValidateSuppressionFound() throws Exception {
		Navigation.navigate_Suppressions();
		//Suppressions.quickSearchBox.search_WithCriteria(orderID_Postback);
		Suppressions.search_ByText_UntilFound(orderID_Postback);
		Suppressions.verify_Suppression1Entry_Correct(orderID_Postback, poNumber_NetdownPostback, audienceAccount,
				marketer, audienceMart, suppressionType_Postback, expectedCSV_TIS_TableSize, finalOrderTime);
	}

	// Step 18
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 136)
	public void PT256_SuppressionDetails_ValidateSuppressionFound() throws Exception {
		Suppressions.suppressionTable.click_View_ForRow(1);
		SuppressionDisplay.verify_DetailsPanel(orderID_Postback, suppressionType_Postback, poNumber_NetdownPostback,
				marketer, createdBy, expectedCSV_TIS_TableSize, audienceMart);

	}

	// Step 4
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 140)
	public void PT257_SuppressionPage_Netdown_Order_WithNoSuppression_ShowsNoData() throws Exception {
		Navigation.navigate_Suppressions();
		//Suppressions.search_ByText_UntilFound(orderID);
		Suppressions.quickSearchBox.search_WithCriteria(orderID);
		Suppressions.suppressionTable.verify_NoDataInTable();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of Class
