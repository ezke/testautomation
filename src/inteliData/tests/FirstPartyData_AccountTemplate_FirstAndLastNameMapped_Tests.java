package inteliData.tests;

import java.time.Duration;
import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.DataSource_Modal;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.DataSourceImport;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.FirstParty_DataSources_Display;
import inteliData.pages.FirstParty_Mappings;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.Programs;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class FirstPartyData_AccountTemplate_FirstAndLastNameMapped_Tests extends baseTest {

	String name = "Auto-DataSource-Account-FirstAndLastNameMapped";
	String account = "ACME";
	String marketer = "1";
	String expectedMarketer = "1 (15899)";
	String template = "Account";
	TableData mappingsTable;
	int expectedRecords = 7;
	int expectedErrors = 5;

	String file = "\\src\\inteliData\\data\\account_small_fnamelname.csv";
	String fileName = "account_small_fnamelname.csv";

	String expectedStandardizedResultsFile = "\\src\\inteliData\\data\\account_small_fnamelname_ExpectedStandardizedResults.csv";

	HashMap<String, String> expectedMappings;

	// String id = "70008054";
	String id = "";
	// String marketer_TableKey = "15899";
	String marketer_TableKey = "";

	TableData accounts;
	TableData accountPassThroughs;
	TableData accountValidationErrors;

	TableData csvToCheckAgainst;

	TableData account_CSV_Standardized_ExpectedResults;
	String expectedErrorResultsFile = "\\src\\inteliData\\data\\account_small_fnamelname_ExpectedErrors.csv";
	TableData accountErrors_ExpectedValues;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings = new HashMap<String, String>();
		expectedMappings.put("FirstName", "First Name");
		expectedMappings.put("Mi", "Middle Initial");
		expectedMappings.put("LastName", "Last Name");
		expectedMappings.put("FirstName2", "First Name 2");
		expectedMappings.put("Mi2", "Middle Initial 2");
		expectedMappings.put("LastName2", "Last Name 2");
		expectedMappings.put("Address1", "Address Line1");
		expectedMappings.put("Address2", "Address Line2");
		expectedMappings.put("CITY_X", "City");
		expectedMappings.put("STATE_X", "State");
		expectedMappings.put("Zip5", "Zip");
		expectedMappings.put("ZIP4_X", "Zip 4");
		expectedMappings.put("AccountKey", "Account Key");
		expectedMappings.put("HouseholdNumber", "Household Number");
		expectedMappings.put("AccountOpenDate", "Account Open Date");
		expectedMappings.put("AccountCloseDate", "Account Close Date");
		expectedMappings.put("LastWithdrawalDate", "Last Withdrawal Date");
		expectedMappings.put("LastDirectDepositDate", "Last Direct Deposit Date");
		expectedMappings.put("LastDirectDepositAmount", "Last Direct Deposit Amount");
		expectedMappings.put("BranchCode", "Location Code");
		expectedMappings.put("LastDirectDepositDate", "Last Direct Deposit Date");
		expectedMappings.put("RegionCode", "Region Code");
		expectedMappings.put("LendingInterestRate", "Lending Interest Rate");
		expectedMappings.put("LendingMaturityDate", "Lending Maturity Date");
		expectedMappings.put("LendingTerm", "Lending Term");
		expectedMappings.put("LendingPayment", "Lending Payment");
		expectedMappings.put("LendingPaymentSchedule", "Lending Payment Schedule");
		expectedMappings.put("MailIndicator", "Mail Indicator");
		expectedMappings.put("EmployeeIndicator", "Employee Indicator");
		expectedMappings.put("BirthDate", "Birth Date");
		expectedMappings.put("MinorIndicator", "Minor Indicator");
		expectedMappings.put("Gender", "Gender");
		expectedMappings.put("HomePhone", "");
		expectedMappings.put("Email", "Email Address");
		expectedMappings.put("EmailOptOutIndicator", "");
		expectedMappings.put("OnlineBankingDate", "Online Banking Date");
		expectedMappings.put("MobileBankingDate", "Mobile Banking Date");
		expectedMappings.put("EstatementDate", "Estatement Date");
		expectedMappings.put("DebitCardDate", "Debit Card Date");
		expectedMappings.put("BillPayDate", "Bill Pay Date");
		expectedMappings.put("DirectDepositDate", "Direct Deposit Date");
		expectedMappings.put("AverageMtdBalance", "Average MTD Balance");
		expectedMappings.put("AverageYtdBalance", "Average YTD Balance");
		expectedMappings.put("EndMonthLedgerBalance", "End Month Ledger Balance");
		expectedMappings.put("TotalAnnualProfit", "Total Annual Profit");
		expectedMappings.put("TotalDirectDepositAmount", "Total Direct Deposit Amount");
		expectedMappings.put("TotalDebitCardPurchases", "Total Debit Card Purchases");
		expectedMappings.put("OverdraftProtectionAmount", "Overdraft Protection Amount");
		expectedMappings.put("NsfAmount", "NSF Amount");
		expectedMappings.put("DebitCardInterchangeAmount", "Debit Card Interchange Amount");
		expectedMappings.put("ForeignAtmAmount", "Foreign ATM Amount");
		expectedMappings.put("DebitTransactionCount", "Debit Transaction Count");
		expectedMappings.put("OnlineBillPayCount", "Online Bill Pay Count");
		expectedMappings.put("OverdraftFeesCount", "Overdraft Fees Count");
		expectedMappings.put("NsfFeesCount", "NSF Fees Count");
		expectedMappings.put("DirectDepositsCount", "Direct Deposits Count");
		expectedMappings.put("ELotCode", "ELot Code");
		expectedMappings.put("Balance", "Balance");
		expectedMappings.put("ResponderDate", "Responder Date");
		expectedMappings.put("ApplicationDate", "Application Date");
		expectedMappings.put("LoanStatusCategory", "Loan Status Category");
		expectedMappings.put("CarrierRoute", "Carrier Route");
		expectedMappings.put("Level1", "Level 1");
		expectedMappings.put("Level2", "Level 2");
		expectedMappings.put("Level3", "Level 3");

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.click_CreateButton();
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer, template);
		DataSourceImport.chooseFile(file, true);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 10)
	public void PT292_Step7_FirstPartyMappings_VerifyDetailsPanel() throws Exception {
		id = FirstParty_Mappings.data_Panel.return_SpecificValue("Id");
		FirstParty_Mappings.verify_detailsPanel_FirstPartyData(name, fileName, expectedMarketer, expectedRecords);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 15)
	public void PT292_Step7_FirstPartyMappings_VerifyDefaultMappings() throws Exception {
		mappingsTable = FirstParty_Mappings.mapTable.return_TableData();
		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 16)
	public void PT292_Step8_FirstPartyMappings_ClickMapRemainingAsPassThrough_VerifyTableUpdated() throws Exception {

		expectedMappings.put("HomePhone", "Phone");
		expectedMappings.put("EmailOptOutIndicator", "Email Opt Out");

		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "HomePhone", "Phone");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "EmailOptOutIndicator", "Email Opt Out");

		FirstParty_Mappings.click_MapRemainingAsPassThrough();
		TableData mappingsTable = FirstParty_Mappings.mapTable.return_TableData();

		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
		FirstParty_Mappings.mapTable.verify_AllCheckboxes_Checked(mappingsTable);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 20)
	public void PT292_Step9_ConfirmMappings_VerifyConfirmMappingTable() throws Exception {
		FirstParty_Mappings.click_Map_Button();
		ConfirmMappedFields.verify_Mappings(expectedMappings);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 21)
	public void PT292_Step9_ConfirmMappings_Verify_DetailsPanel() throws Exception {
		ConfirmMappedFields.verify_DetailsPanel_ForFirstPartyData_MatchesExpected(id, name, fileName, expectedRecords,
				expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 30)
	public void PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		FirstParty_DataSources_Display.refresh_Until_Mapped(Duration.ofMinutes(10));
		marketer_TableKey = FirstParty_DataSources_Display.return_MarketerKey();
		FirstParty_DataSources_Display.verify_DetailsPanel(id, name, fileName, expectedRecords, expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 31, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT292_Step10_FirstPartyDataSourcesDisplay_VerifyErrorsPanel() throws Exception {
		String addressStandards = "1";
		String latLongMatch = "1";

		// populate DB's
		accounts = FirstParty_DataSources_Display.return_accounts(id, marketer_TableKey);
		accountPassThroughs = FirstParty_DataSources_Display.return_accountPassThroughs(id, marketer_TableKey);
		accountValidationErrors = FirstParty_DataSources_Display.return_accountValidationErrors(id, marketer_TableKey);
		csvToCheckAgainst = FirstParty_DataSources_Display.return_CSVFile_ToCheckAgainst(file);
		account_CSV_Standardized_ExpectedResults = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedStandardizedResultsFile);

		accountErrors_ExpectedValues = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedErrorResultsFile);
		accountErrors_ExpectedValues.sort_ByColumn_Ascending("Error");
		FirstParty_DataSources_Display.apply_newIDs_ToExpectedTables(account_CSV_Standardized_ExpectedResults,
				new TableData[] { accountErrors_ExpectedValues },
				Integer.parseInt(accounts.data.get(0).get("AccountId")), "AccountId");
		account_CSV_Standardized_ExpectedResults.replace_Null_With_Empty();
		account_CSV_Standardized_ExpectedResults.remove_Character("NULL");
		account_CSV_Standardized_ExpectedResults.set_decimal_Precision(0, "StandardizedLatitude");
		account_CSV_Standardized_ExpectedResults.set_decimal_Precision(0, "StandardizedLongitude");
		account_CSV_Standardized_ExpectedResults.remove_Character(".", "StandardizedLatitude");
		account_CSV_Standardized_ExpectedResults.remove_Character(".", "StandardizedLongitude");


		FirstParty_DataSources_Display.verify_ErrorsPanel(expectedErrors, addressStandards, latLongMatch);
	}

	// Step 11
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT292_Step11_VerifyDatabase_csv_CorrectCount_OfEntries() throws Exception {
		csvToCheckAgainst.verify_TableSize(expectedRecords);
	}

	@DataProvider(name = "compare_CSV_To_Accounts_Standardized")
	public Object[][] csv_To_accounts_TableMappings_Standardized() {
		Object[][] linksToNavigate = new Object[account_CSV_Standardized_ExpectedResults.data.get(0).size()][1];

		int indexToPlace_ColumnHeader = 0;
		for (String key : account_CSV_Standardized_ExpectedResults.data.get(0).keySet()) {
			linksToNavigate[indexToPlace_ColumnHeader][0] = key;

			indexToPlace_ColumnHeader++;
		}

		return linksToNavigate;
	}

	// Step 3
	@Test(dataProvider = "compare_CSV_To_Accounts_Standardized", priority = 40, groups = { "all_Tests",
			"regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step3_VerifyColumnsMatch_CSV_to_AccountsTable_WithFilters(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, account_CSV_Standardized_ExpectedResults.data, accounts.data);
	}

	// Step 4
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step4_VerifyDatabase_Accounts_FileIdMatches_Id() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("FileId", id, accounts.data);
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "compare_CSV_To_Accounts")
	public Object[][] csv_To_accounts_TableMappings() {
		Object[][] linksToNavigate = new Object[csvToCheckAgainst.data.get(0).size()][1];

		int indexToPlace_ColumnHeader = 0;
		for (String key : csvToCheckAgainst.data.get(0).keySet()) {
			linksToNavigate[indexToPlace_ColumnHeader][0] = key;

			indexToPlace_ColumnHeader++;
		}

		return linksToNavigate;
	}

	// Step 5
	@Test(dataProvider = "compare_CSV_To_Accounts", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step5_Compare_CSV_To_Accounts(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, csvToCheckAgainst.data, accounts.data);
	}

	@DataProvider(name = "compare_CSV_To_Errors")
	public Object[][] csv_To_Error_TableMappings() {
		Object[][] linksToNavigate = new Object[2][1];

		linksToNavigate[0][0] = "AccountId";
		linksToNavigate[1][0] = "Error";

		return linksToNavigate;
	}

	// Step 6
	@Test(dataProvider = "compare_CSV_To_Errors", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step6_Compare_ExpectedErrorColumns_To_ActualErrorsTable(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, accountErrors_ExpectedValues.data,
				accountValidationErrors.data);
	}

	// Step 7
	// Expecting 0 pass throughs. So need to check that table was empty.
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step7_VerifyDatabase_PassThroughs_CorrectNumberOfEntries() throws Exception {
		accountPassThroughs.verify_TableSize(0);
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step8_VerifyDatabase_ErrorTable_VerifyCountOfUniqueProductIds() throws Exception {
		accountValidationErrors.verify_CountOfUniqueColumnValues(expectedErrors, "AccountId");
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT292_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT293_Step9_VerifyDatabase_ErrorTable_CorrectCountOfEntries() throws Exception {
		accountValidationErrors.verify_TableSize(13);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
