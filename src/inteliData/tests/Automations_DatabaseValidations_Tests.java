package inteliData.tests;

import inteliData.data.GlobalVariables;
import inteliData.data.Utilities;
import inteliData.data.baseTest;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.*;

import java.util.HashMap;

public class Automations_DatabaseValidations_Tests extends baseTest {


	String _expectedDate;

	TableData _emails;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		_emails = DataBuilder.returnTableData_ForComparison("//src//inteliData//data//emails_automations.csv", "\\,",
				true);

		_expectedDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "M/d/yyyy") + " 12:00:00 AM";
		Email_API.return_EarliestEmail_ForRecipient(GlobalVariables.suppressionAutomationQA_Email);

	}

	@DataProvider(name = "run_Intelidata_SiteValidations")
	public Object[][] GenerateTests_ForAutomations() {

		int uniqueCount_OfEntries_ForCurrentEnvironment = _emails
				.countOf_Matches_ThatHave_ColumnName_AndCellValue("environment", GlobalVariables.environment);

		Object[][] scenariosToRun = new Object[uniqueCount_OfEntries_ForCurrentEnvironment][2];

		int indexToPlace_ColumnHeader = 0;
		for (HashMap<String, String> row : _emails.data) {
			if (row.get("environment").equals(GlobalVariables.environment)) {
				scenariosToRun[indexToPlace_ColumnHeader][0] = row.get("testCase") + "_";
				scenariosToRun[indexToPlace_ColumnHeader][1] = row;
				indexToPlace_ColumnHeader++;
			}
		}

		return scenariosToRun;
	}



	// After dev discussion, no longer running these. The Dev's are no longer
	// putting expected errors into elmah. Also typod
	@Test(dataProvider = "run_Intelidata_SiteValidations", alwaysRun = true, enabled = false, groups = { "all_Tests",
			"regression_Tests", "automations_Tests" })
	public void Run_ElmahErrorValidations_ForScenario(String testCase, HashMap<String, String> dataRow)
			throws Exception {
		// String location = dataRow.get("location");
		String scenario = dataRow.get("scenario");
		String type = dataRow.get("type");
		String expectedElmahError = "";

		if (type.equals("Programs")) {
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")
					|| scenario.equals("DuplicateFiles")) {

			} else if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")) {
				expectedElmahError = "There is no data in the file for automation " + dataRow.get("automationID");
			} else if (scenario.equals("NoHeaderHasRecords")) {
				String fileID = Utilities.returnFileId(Integer.valueOf(dataRow.get("automationID")));
				expectedElmahError = "Columns do not match between field headers and datadictionaryitems for automation "
						+ dataRow.get("automationID") + " and FileId " + fileID + ".";
			} else {
				BaseUI.log_AndFail("Scenario " + scenario + " NOT found.");
			}

		} else if (type.equals("Suppressions")) {
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")
					|| scenario.equals("DuplicateFiles")) {

			} else if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")) {
				expectedElmahError = "There is no data in the file for automation " + dataRow.get("automationID");
			} else if (scenario.equals("NoHeaderHasRecords")) {
				String fileID = Utilities.returnFileId(Integer.valueOf(dataRow.get("automationID")));
				expectedElmahError = "Columns do not match between field headers and datadictionaryitems for automation "
						+ dataRow.get("automationID") + " and file " + fileID + ".";
			} else {
				BaseUI.log_AndFail("Unidentified scenario of " + scenario);
			}

		} else {
			BaseUI.log_AndFail("Unidentified type of " + type);
		}

		if (expectedElmahError.equals("")) {
			Utilities.verify_NoElmaError(Integer.parseInt(dataRow.get("automationID")));
		} else {
			Utilities.verify_Database_Found1ElmaError(Integer.parseInt(dataRow.get("automationID")),
					expectedElmahError);
		}
	}

	@DataProvider(name = "run_DatabaseErrorValidations")
	public Object[][] GenerateTests_ForAutomations_DatabaseErrorValidations() {

		HashMap<String, String> valuesToMatch = new HashMap<String, String>();
		valuesToMatch.put("environment", GlobalVariables.environment);
		valuesToMatch.put("databaseError", "TRUE");

		int uniqueCount_OfEntries = _emails.countOf_Matches_ThatHave_matchingHashMapValues(valuesToMatch);

		Object[][] scenariosToRun = new Object[uniqueCount_OfEntries][2];

		int indexToPlace_ColumnHeader = 0;
		for (HashMap<String, String> row : _emails.data) {
			if (row.get("environment").equals(GlobalVariables.environment) && row.get("databaseError").equals("TRUE")) {
				scenariosToRun[indexToPlace_ColumnHeader][0] = row.get("testCase") + "_";
				scenariosToRun[indexToPlace_ColumnHeader][1] = row;
				indexToPlace_ColumnHeader++;
			}
		}

		return scenariosToRun;
	}

	@Test(dataProvider = "run_DatabaseErrorValidations", groups = { "all_Tests", "regression_Tests",
			"automations_Tests" })
	public void Run_DatabaseErrorValidations_ForScenario(String testCase, HashMap<String, String> dataRow)
			throws Exception {
		// String location = dataRow.get("location");
		String scenario = dataRow.get("scenario");
		String type = dataRow.get("type");
		int automationID = Integer.valueOf(dataRow.get("automationID"));

		if (type.equals("Programs")) {
			if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")
					|| scenario.equals("NoHeaderHasRecords")) {
				Utilities.verify_Database_FileNoDataFailure(automationID);
			} else if (scenario.equals("DuplicateFiles")) {
				Utilities.verify_Database_DuplicateFileFound(automationID);
			} else {
				BaseUI.log_AndFail("Scenario " + scenario + " NOT found.");
			}

		} else if (type.equals("Suppressions")) {
			int packageID = Integer.valueOf(dataRow.get("packageID"));
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")
					|| scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")
					|| scenario.equals("NoHeaderHasRecords")) {
				Utilities.verify_Package_Completed_2RecordsFound(packageID);
			} else if (scenario.equals("DuplicateFiles")) {
				Utilities.verify_Package_Completed_NoRecordsFound(packageID);
				Utilities.verify_Database_DuplicateFileFound(automationID);
			} else {
				BaseUI.log_AndFail("Unidentified scenario of " + scenario);
			}

		} else {
			BaseUI.log_AndFail("Unidentified type of " + type);
		}
	}


	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {

	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {

	}



}// End of Class
