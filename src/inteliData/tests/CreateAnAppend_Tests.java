package inteliData.tests;

import java.util.HashMap;

import jdk.nashorn.internal.objects.Global;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAppend_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.AppendImport;
import inteliData.pages.AppendMap;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_Order_OutputOptions;
import inteliData.pages.CreateAudience_PurchaseRecordsPage;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

public class CreateAnAppend_Tests extends baseTest {

	String name = "Automation - Appends 1";
	String mart = "Mortgage Mart";
	String account = "ACME";
	String marketer = "1";
	String poNumber = "Automation PONumber1";
	Integer recordsInSourceFile = 10000;
	HashMap<String, String> expectedMappings = new HashMap<String, String>();
	TableData appendMapTable;
	String appendLevel = "Individual";
	String levelText = "Individual - First Name, Last Name, and Address";
	String[] suppliers = { "1 - Equifax", "2 - Experian", "3 - TransUnion" };
	Integer ficoRange1 = 200;
	Integer ficoRange2 = 900;

	String levelName = "AutomationLevel1";
	String suppressionLevel;

	String timeUploaded;
	String timeUploaded_ShortDate;

	Integer audienceID;
	Integer selectedRecords;

	String purchaseAmount;
	String purchaseTime;
	String orderID;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings.put("fname", "First Name");
		expectedMappings.put("lname", "Last Name");
		expectedMappings.put("addr1", "Address Line1");
		expectedMappings.put("addr2", "Address Line2");
		expectedMappings.put("zip", "Zip");
		expectedMappings.put("PHONE_X", "");
		if(GlobalVariables.environment.equals("DEV")){
			expectedMappings.put("PHONE_X", "Phone");
		}

		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

		HomePage.click_CreateAppend_Button();
		CreateAppend_Modal.enter_Fields_And_Click_SaveAndContinue(name, mart, account, marketer);
		SuppliersAndUsage_Modal.select_AllSuppliers();

		SuppliersAndUsage_Modal.directMail_Checkbox.check_Checkbox();

	}

	// Step 6
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 10)
	public void PT252_Verify_TakenToAppendImport_Page() throws Exception {
		SuppliersAndUsage_Modal.click_CreateAppend();
		AppendImport.verify_TitleDisplayed();
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 20)
	public void PT252_SelectedFile_TakenTo_AppendMap_Page() throws Exception {
		timeUploaded = BaseUI.getDateAsString_InRelationToTodaysDate(0, AppendMap.dateUploadedFormat);
		AppendImport.chooseFile(GlobalVariables.appendSampleFile);
		AppendMap.verify_TitleDisplayed();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 21)
	public void PT252_AppendMap_VerifyDetailsPanel() throws Exception {
		AppendMap.verify_DetailsPanel(name, GlobalVariables.appendSampleFile_Name, timeUploaded, GlobalVariables.default_UserDesignation, marketer,
				recordsInSourceFile);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 21)
	public void PT252_AppendMap_VerifyDefaultMappings() throws Exception {
		appendMapTable = AppendMap.mapTable.return_TableData();
		AppendMap.verify_Mappings(appendMapTable, expectedMappings);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 30)
	public void PT252_ConfirmMappedFields_DetailsPanelMatchesExpected() throws Exception {
		AppendMap.select_AppendLevel(appendLevel);
		AppendMap.mapTable.selectRow_BySource(appendMapTable, "fname");
		AppendMap.mapTable.selectRow_BySource(appendMapTable, "lname");
		AppendMap.mapTable.selectRow_BySource(appendMapTable, "addr1");
		AppendMap.mapTable.selectRow_BySource(appendMapTable, "addr2");
		AppendMap.mapTable.selectRow_BySource(appendMapTable, "zip");

		timeUploaded_ShortDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(timeUploaded,
				AppendMap.dateUploadedFormat, ConfirmMappedFields.details_Date_Format, 0);
		AppendMap.click_Map_Button();
		ConfirmMappedFields.verify_DetailsPanel_MatchesExpected(name, recordsInSourceFile, GlobalVariables.default_UserDesignation,
				timeUploaded_ShortDate);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 31)
	public void PT252_ConfirmMappedFields_VerifyMappings() throws Exception {
		expectedMappings.put("PHONE_X", "");
		TableData confirmMappingsTable = ConfirmMappedFields.mappingTable.return_TableData();
		ConfirmMappedFields.verify_Mappings(confirmMappingsTable, expectedMappings);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 40)
	public void PT252_GlobalCriteria_Verify_MatchReportsTable() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();

		CreateAudience_GlobalCriteria.verify_MatchReport(suppliers, recordsInSourceFile, recordsInSourceFile);
	}

	// Step 18
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 50)
	public void PT252_LevelListPage_VerifyLevel1_DataPanel() throws Exception {
		CreateAudience_GlobalCriteria.stateSelector.click_AllStates();
		CreateAudience_GlobalCriteria.globalSuppression.expand_Section();
		if(GlobalVariables.environment.equals("DEV")) {
			CreateAudience_GlobalCriteria.include_phone_number_suppressions_Checkbox.check_Checkbox();
		}
		suppressionLevel = CreateAudience_GlobalCriteria.return_AddressSuppressionLevel();

		CreateAudience_GlobalCriteria.click_SaveAndContinue();
		CreateAudience_DefineLevels.enter_Level(levelName);
		CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
		if(GlobalVariables.environment.equals("DEV")) {
			CreateAudience_DefineLevels.select_FICO_DropdownOption("FICO");
		}
		CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
		CreateAudience_DefineLevels.click_SaveLevel_Button();

		CreateAudience_DefineLevels_LevelListPage.verify_Level1PanelHeader(levelName);
		CreateAudience_DefineLevels_LevelListPage.level1_Panel.verify_ValueMatches("Geography", "Use global geography");
		CreateAudience_DefineLevels_LevelListPage.level1_Panel.verify_ValueMatches("FICO", "From 200 To 900");
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 60)
	public void PT252_SearchReview_Verify_GeneralInformationPanel() throws Exception {
		CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();

		HashMap<String, String> expectedGeneralInfo = new HashMap<String, String>();
		expectedGeneralInfo.put("One Per Household", "Yes");
		expectedGeneralInfo.put("Unique Phones Only", "No");
		expectedGeneralInfo.put("Supplier (s)", "Equifax (1), Experian (2), TransUnion (3)");
		expectedGeneralInfo.put("Usage Types", "Direct Mail");
		CreateAudience_SearchReview.generalInformation_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedGeneralInfo);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 61)
	public void PT252_SearchReview_Verify_BuildSummaryPanel() throws Exception {
		CreateAudience_SearchReview.verify_MainContent(mart, marketer, timeUploaded, 1);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 61)
	public void PT252_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
		String stateString = CreateAudience_SearchReview.allStatesString;
		HashMap<String, String> expectedGlobalGeography = new HashMap<String, String>();
		expectedGlobalGeography.put("State list", stateString);
		CreateAudience_SearchReview.globalGeography_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedGlobalGeography);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 61)
	public void PT252_SearchReview_Verify_MatchReport() throws Exception {
		CreateAudience_SearchReview.verify_MatchReport(suppliers, recordsInSourceFile, recordsInSourceFile);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 61)
	public void PT252_SearchReview_Verify_Level1Panel() throws Exception {
		CreateAudience_SearchReview.verify_Level1PanelHeader(levelName);
	}

	// Step 19
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 61)
	public void PT252_SearchReview_Verify_Suppressions() throws Exception {
		HashMap<String, String> expectedSuppressionValues = new HashMap<String, String>();
		expectedSuppressionValues.put("Suppression level", suppressionLevel);
		expectedSuppressionValues.put("Include phone number suppressions", "Yes");

		CreateAudience_SearchReview.suppressions_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedSuppressionValues);
	}

	// Step 25
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 70)
	public void PT252_PurchaseRecords_FCRA_NoticeAppears() throws Exception {
		CreateAudience_SearchReview.click_Top_BuildAudience_Button();
		CreateAudience_SearchOrderRecords.resultsTable.checkbox_ByIndex(1).check_Checkbox();
		HashMap<String, String> audienceSummary = CreateAudience_SearchOrderRecords.mainContent_Panel
				.return_PanelData();
		audienceID = Integer.valueOf(audienceSummary.get("Audience ID").replace(",", ""));
		selectedRecords = Integer
				.valueOf(audienceSummary.get("Selected Records").split("\\s")[0].replace(",", "").trim());
		CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
		CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(poNumber);
		CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
		CreateAudience_Order_OutputOptions.click_FICO_ScoreLabel();
		CreateAudience_Order_OutputOptions.click_PlaceOrder();
		CreateAudience_PurchaseRecordsPage.verify_FCRA_Notice_Appears();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 71)
	public void PT252_PurchaseRecords_Verify_AudienceSummaryPanel() throws Exception {
		CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(audienceID, mart, marketer, timeUploaded,
				selectedRecords, poNumber);
	}
	
	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 75)
	public void PT252_DownloadRecords_AbleToGetToPage() throws Exception {
		purchaseAmount = CreateAudience_PurchaseRecordsPage.pricingInfo_Table.return_TotalAmount();
		purchaseTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_DownloadRecords_Page.dateFormat);
		CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
		CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "append_Tests" }, priority = 80)
	public void PT252_DownloadRecords_ValidateCSV() throws Exception {
		String typeName = "CSV (Order File)";
		String fileName = "";
		CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();
		
		orderID = CreateAudience_DownloadRecords_Page.return_OrderID();
		TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
		fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
		TableData csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1, fileName);

		HashMap<String, Range<Integer>> levelRanges = new HashMap<String, Range<Integer>>();
		levelRanges.put("1", Range.between(ficoRange1, ficoRange2));
		
		CreateAudience_DownloadRecords_Page.verify_CSV_MatchesExpected_NoMasking(csvTable, GlobalVariables.stateAbbreviations_52, levelRanges);


		csvTable.verify_TableSize(selectedRecords);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 81)
	public void PT252_FinalPage_AudienceSummary() throws Exception {
		CreateAudience_DownloadRecords_Page.verify_AudienceSummaryPanel(audienceID, name, mart, account,
				marketer, timeUploaded);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 81)
	public void PT252_FinalPage_OrderDetails() throws Exception {
		CreateAudience_DownloadRecords_Page.verify_OrderDetailsPanel(poNumber, purchaseTime, selectedRecords.toString(),
				purchaseAmount, false);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 82)
	public void PT252_FinalPage_Status_Updated_ToShipped_AfterRefresh() throws Exception {
		CreateAudience_DownloadRecords_Page.refreshPage();
		CreateAudience_DownloadRecords_Page.verify_OrderDetailsPanel(poNumber, purchaseTime, selectedRecords.toString(),
				purchaseAmount, true);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}
	
	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}

}// End of class
