package inteliData.tests;

import java.time.Duration;
import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.DataSource_Modal;
import inteliData.pages.ConfirmMappedFields;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.DataSourceImport;
import inteliData.pages.FirstParty_DataSources;
import inteliData.pages.FirstParty_DataSources_Display;
import inteliData.pages.FirstParty_Mappings;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.Programs;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.ResultWriter;
import utils.TableData;

public class FirstPartyData_CampaignInterimTemplate_Tests extends baseTest {

	String name = "Auto-DataSource-Campaign-Interim-DBValidations";
	String account = "ACME";
	String marketer = "1";
	String expectedMarketer = "1 (15899)";
	String template = "Campaign Drop - Interim";
	TableData mappingsTable;
	int expectedRecords = 6;
	int expectedErrors = 5;

	String file = "\\src\\inteliData\\data\\CampaignDrop.csv";
	String fileName = "CampaignDrop.csv";

	HashMap<String, String> expectedMappings;

	// String id = "70008054";
	String id = "";
	// String marketer_TableKey = "15899";
	String marketer_TableKey = "";

	TableData campaign;
	TableData campaignPassThroughs;
	TableData campaignValidationErrors;

	TableData csvToCheckAgainst;

	String expectedStandardizedResultsFile = "\\src\\inteliData\\data\\campaignDrop_Final_ExpectedStandardizedResults.csv";
	TableData campaign_CSV_Standardized_ExpectedResults;

	String expectedPassThroughs = "\\src\\inteliData\\data\\campaignDrop_Final_ExpectedPassThrough.csv";
	TableData campaignPassThroughs_ExpectedValues;

	String expectedErrorResultsFile = "\\src\\inteliData\\data\\campaignDrop_Interim_ExpectedErrors.csv";
	TableData campaignErrors_ExpectedValues;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		expectedMappings = FirstParty_Mappings.return_CampaignFinal_ExpectedDefaultMappings();

		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		Navigation.navigate_FirstParty_DataSources();
		FirstParty_DataSources.click_CreateButton();
		DataSource_Modal.enter_DataSource_Fields_AndSaveContinue(name, account, marketer, template);
		DataSourceImport.chooseFile(file, true);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 10)
	public void PT323_Step7_FirstPartyMappings_VerifyDetailsPanel() throws Exception {
		id = FirstParty_Mappings.data_Panel.return_SpecificValue("Id");
		FirstParty_Mappings.verify_detailsPanel_FirstPartyData(name, fileName, expectedMarketer, expectedRecords);
	}

	// Step 7
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 15)
	public void PT323_Step7_FirstPartyMappings_VerifyDefaultMappings() throws Exception {
		mappingsTable = FirstParty_Mappings.mapTable.return_TableData();
		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 16)
	public void PT323_Step8_FirstPartyMappings_ClickMapRemainingAsPassThrough_VerifyTableUpdated() throws Exception {
		expectedMappings.put("BranchId", "Location ID");
		expectedMappings.put("CellID", "Cell ID");
		expectedMappings.put("CustInd", "Customer Indicator");
		expectedMappings.put("Lending_CurrentMonthlyPayment", "Lending Current Monthly Payment");
		expectedMappings.put("Lending_FICOScore", "Lending FICO Score");
		expectedMappings.put("Lending_OriginalLoanAmount", "Lending Original Loan Amount");
		expectedMappings.put("Lending_OriginalTerm", "Lending Original Term");
		expectedMappings.put("Lending_RemainingBalance", "Lending Remaining Balance");
		expectedMappings.put("Lending_RemainingTerm", "Lending Remaining Term");
		expectedMappings.put("Lending_SegmentCd", "Lending Segment Code");
		expectedMappings.put("LendingOffer_CurrentMonthlyPayment", "Lending Offer Current Monthly Payment");
		expectedMappings.put("LendingOffer_LoanAmount", "Lending Offer Loan Amount");
		expectedMappings.put("LendingOffer_NewMonthlyPayment", "Lending Offer New Monthly Payment");
		expectedMappings.put("LendingOffer_Rate", "Lending Offer Rate");
		expectedMappings.put("LendingOffer_Savings", "Lending Offer Savings");
		expectedMappings.put("LendingOffer_Term", "Lending Offer Term");
		expectedMappings.put("ORDERID_X", "Order ID");
		expectedMappings.put("ORDERRECORDID_X", "Order Record ID");
		expectedMappings.put("RmvRteInd", "Remove Route Indicator");
		expectedMappings.put("SuprInd", "Suppress Indicator");
		expectedMappings.put("WalkSeq", "Walk Sequence");
		expectedMappings.put("PT1", "Pass Through");
		expectedMappings.put("PT2", "Pass Through");

		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "BranchId", "Location ID");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "CellID", "Cell ID");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "CustInd", "Customer Indicator");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_CurrentMonthlyPayment",
				"Lending Current Monthly Payment");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_FICOScore", "Lending FICO Score");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_OriginalLoanAmount",
				"Lending Original Loan Amount");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_OriginalTerm", "Lending Original Term");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_RemainingBalance",
				"Lending Remaining Balance");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_RemainingTerm", "Lending Remaining Term");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "Lending_SegmentCd", "Lending Segment Code");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "LendingOffer_CurrentMonthlyPayment",
				"Lending Offer Current Monthly Payment");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "LendingOffer_LoanAmount",
				"Lending Offer Loan Amount");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "LendingOffer_NewMonthlyPayment",
				"Lending Offer New Monthly Payment");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "LendingOffer_Rate", "Lending Offer Rate");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "LendingOffer_Savings", "Lending Offer Savings");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "LendingOffer_Term", "Lending Offer Term");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "ORDERID_X", "Order ID");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "ORDERRECORDID_X", "Order Record ID");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "RmvRteInd", "Remove Route Indicator");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "SuprInd", "Suppress Indicator");
		FirstParty_Mappings.mapTable.setDestination(mappingsTable, "WalkSeq", "Walk Sequence");

		FirstParty_Mappings.click_MapRemainingAsPassThrough();
		TableData mappingsTable = FirstParty_Mappings.mapTable.return_TableData();

		FirstParty_Mappings.verify_Mappings(mappingsTable, expectedMappings);
		FirstParty_Mappings.mapTable.verify_AllCheckboxes_Checked(mappingsTable);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 20)
	public void PT323_Step9_ConfirmMappings_VerifyConfirmMappingTable() throws Exception {
		FirstParty_Mappings.click_Map_Button();
		ConfirmMappedFields.verify_Mappings(expectedMappings);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 21)
	public void PT323_Step9_ConfirmMappings_Verify_DetailsPanel() throws Exception {
		ConfirmMappedFields.verify_DetailsPanel_ForFirstPartyData_MatchesExpected(id, name, fileName, expectedRecords,
				expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests", "firstPartyData_Tests" }, priority = 30)
	public void PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel() throws Exception {
		ConfirmMappedFields.click_ConfirmButton();
		FirstParty_DataSources_Display.refresh_Until_Mapped(Duration.ofMinutes(10));
		marketer_TableKey = FirstParty_DataSources_Display.return_MarketerKey();
		FirstParty_DataSources_Display.verify_DetailsPanel(id, name, fileName, expectedRecords, expectedMarketer);
	}

	// Step 10
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 31, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT323_Step10_FirstPartyDataSourcesDisplay_VerifyErrorsPanel() throws Exception {
		String addressStandards = "0";
		String latLongMatch = "0";

		// populate DB's
		campaign = FirstParty_DataSources_Display.return_campaign(id, marketer_TableKey);
		campaignPassThroughs = FirstParty_DataSources_Display.return_campaignPassThroughs(id, marketer_TableKey);
		campaignValidationErrors = FirstParty_DataSources_Display.return_campaignValidationErrors(id,
				marketer_TableKey);
		//
		csvToCheckAgainst = FirstParty_DataSources_Display.return_CSVFile_ToCheckAgainst(file);
		campaign_CSV_Standardized_ExpectedResults = FirstParty_DataSources_Display
				.return_Account_ExpectedStandardizedResults(expectedStandardizedResultsFile);

		//
		campaignPassThroughs_ExpectedValues = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedPassThroughs);
		campaignPassThroughs_ExpectedValues.sort_ByColumn_Ascending("Name");
		campaignPassThroughs_ExpectedValues.sort_ByColumn_numeric_Ascending("CampaignDropId");
		campaignPassThroughs_ExpectedValues.remove_Character("NULL");

		campaignErrors_ExpectedValues = FirstParty_DataSources_Display
				.return_CSVFile_ToCheckAgainst(expectedErrorResultsFile);
		campaignErrors_ExpectedValues.sort_ByColumn_Ascending("Error");
		FirstParty_DataSources_Display.apply_newIDs_ToExpectedTables(campaign_CSV_Standardized_ExpectedResults,
				new TableData[] { campaignErrors_ExpectedValues, campaignPassThroughs_ExpectedValues },
				Integer.parseInt(campaign.data.get(0).get("CampaignDropId")), "CampaignDropId");

		FirstParty_DataSources_Display.verify_ErrorsPanel(expectedErrors, addressStandards, latLongMatch);
	}

	// Step 11
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT323_Step11_VerifyDatabase_csv_CorrectCount_OfEntries() throws Exception {
		csvToCheckAgainst.verify_TableSize(expectedRecords);
	}

	@DataProvider(name = "compare_CSV_To_Campaign_Standardized")
	public Object[][] csv_To_campaign_TableMappings_Standardized() {
		Object[][] linksToNavigate = new Object[campaign_CSV_Standardized_ExpectedResults.data.get(0).size()][1];

		int indexToPlace_ColumnHeader = 0;
		for (String key : campaign_CSV_Standardized_ExpectedResults.data.get(0).keySet()) {
			linksToNavigate[indexToPlace_ColumnHeader][0] = key;

			indexToPlace_ColumnHeader++;
		}

		return linksToNavigate;
	}

	// Step 3
	@Test(dataProvider = "compare_CSV_To_Campaign_Standardized", priority = 40, groups = { "all_Tests",
			"regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step3_VerifyColumnsMatch_CSV_to_CampaignTable_WithFilters(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, campaign_CSV_Standardized_ExpectedResults.data, campaign.data);
	}

	// Step 4
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step4_VerifyDatabase_Accounts_FileIdMatches_Id() throws Exception {
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("FileId", id, campaign.data);
	}

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "compare_CSV_To_Campaign")
	public Object[][] csv_To_Campaign_TableMappings() {

		Object[][] linksToNavigate = { { "Zip", "Zip5" }, { "Lending_RemainingBalance", "Lending_RemainingBalance" },
				{ "LendingOffer_Term", "LendingOffer_Term" }, { "DeleteInd", "DeleteIndicator" },
				{ "Lending_CurrentMonthlyPayment", "Lending_CurrentMonthlyPayment" }, { "CellID", "CellId" },
				{ "ModelScore", "ModelScore" }, { "OrderRecordId", "OrderRecordId" },
				{ "SuprInd", "SuppressIndicator" }, { "ComparativeCd", "ComparativeCode" },
				{ "LendingOffer_CurrentMonthlyPayment", "LendingOffer_CurrentMonthlyPayment" },
				{ "Matchkey", "MatchKey" }, { "LendingOffer_Savings", "LendingOffer_Savings" },
				{ "AddressId01", "AddressId01" }, { "CustInd", "CustomerIndicator" }, { "Name1", "Name1" },
				{ "WalkSeq", "WalkSequence" }, { "Name2", "Name2" }, { "Lending_SegmentCd", "Lending_SegmentCode" },
				{ "ProductCode", "ProductCode" }, { "Address2", "Address2" }, { "ZipDP", "ZipDeliveryPoint" },
				{ "Address1", "Address1" }, { "BranchId", "BranchId" }, { "City", "City" }, { "OrderId", "OrderId" },
				{ "ControlInd", "IsControl" }, { "CRRT", "CarrierRoute" }, { "Lending_FICOScore", "Lending_FicoScore" },
				{ "LendingOffer_NewMonthlyPayment", "LendingOffer_NewMonthlyPayment" }, { "OrderDate", "OrderDate" },
				{ "SeedInd", "IsSeed" }, { "Lending_OriginalLoanAmount", "Lending_OriginalLoanAmount" },
				{ "Lending_OriginalTerm", "Lending_OriginalTerm" }, { "LendingOffer_Rate", "LendingOffer_Rate" },
				{ "State", "State" }, { "LendingOffer_LoanAmount", "LendingOffer_LoanAmount" },
				{ "Lending_RemainingTerm", "Lending_RemainingTerm" }, { "Zip4", "Zip4" },
				{ "ControlType", "ControlType" }, { "EPCode", "EpCode" }, { "RmvRteInd", "RemoveRouteIndicator" },
				{ "ModelNTile", "ModelNTile" } };

		return linksToNavigate;
	}

	// Step 5
	@Test(dataProvider = "compare_CSV_To_Campaign", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step5_Compare_CSV_To_Campaign(String csvColumnName, String campaignColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, campaignColumnName, csvToCheckAgainst.data, campaign.data);
	}

	@DataProvider(name = "compare_CSV_To_PassThroughs")
	public Object[][] csv_To_PassThrough_TableMappings() {
		Object[][] linksToNavigate = new Object[3][1];

		linksToNavigate[0][0] = "CampaignDropId";
		linksToNavigate[1][0] = "Name";
		linksToNavigate[2][0] = "Value";

		return linksToNavigate;
	}

	// Step 7
	@Test(dataProvider = "compare_CSV_To_PassThroughs", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step7_Compare_ExpectedPassThroughColumns_To_ActualPassThroughTable(String csvColumnName)
			throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, campaignPassThroughs_ExpectedValues.data,
				campaignPassThroughs.data);
	}

	// Step 6
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step6_VerifyDatabase_PassThroughs_CorrectNumberOfEntries() throws Exception {
		campaignPassThroughs.verify_TableSize(campaignPassThroughs_ExpectedValues.data.size());
	}

	// Step 8
	@Test(groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, priority = 40, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step8_VerifyDatabase_ErrorTable_CorrectCountOfEntries() throws Exception {
		campaignValidationErrors.verify_TableSize(5);
	}

	@DataProvider(name = "compare_CSV_To_Errors")
	public Object[][] csv_To_Error_TableMappings() {

		if (campaignValidationErrors.data.size() == 0) {
			BaseUI.log_AndFail("Errors not generating correctly from Error Table.");
		}

		Object[][] linksToNavigate = new Object[2][1];

		linksToNavigate[0][0] = "CampaignDropId";
		linksToNavigate[1][0] = "Error";

		return linksToNavigate;
	}

	// Step 9
	@Test(dataProvider = "compare_CSV_To_Errors", priority = 40, groups = { "all_Tests", "regression_Tests",
			"firstPartyData_Tests" }, dependsOnMethods = "PT323_Step10_FirstPartyDataSourcesDisplay_VerifyDetailsPanel")
	public void PT325_Step9_Compare_ExpectedErrorColumns_To_ActualErrorsTable(String csvColumnName) throws Exception {
		BaseUI.verify_TableColumns_Match(csvColumnName, campaignErrors_ExpectedValues.data,
				campaignValidationErrors.data);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
