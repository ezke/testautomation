package inteliData.tests;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.*;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class AutoOrder_Search_Searchid_And_SelectByLevel_Tests extends baseTest {

    private final String _audienceName = "AutoOrder Auto - Searchid";
    private final String _audienceMart = "Auto Mart";
    private final String _audienceAccount = "ACME";
    private String _modifiedTime = "";


    //Global Variables
    private final String[] _states = {"ALASKA"};
    private final String _stateAbbreviation = "AK";


    // Level 1 variables
    private final String _level1_Name = "auto level 1";
    private final Range<Integer> _ficoRange_Level1 = Range.between(500, 505);

    // Level 2 variables
    private final String _level2_Name = "auto level 2";
    private final Range<Integer> _ficoRange_Level2 = Range.between(510, 515);

    private HashMap<String, String> _expectedGlobalGeography;
    private HashMap<String, String> _level1ExpectedValues;
    private HashMap<String, String> _level2ExpectedValues;

    private String _audienceID;
    private String _todaysDate;

    private String _csvTypeName = "CSV (Order File)";
    private String _new_audienceID;

    private TableData _csvData;
    private String _lvl2Total;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        _todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "M/d/yyyy");
        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(_audienceName, _audienceMart, _audienceAccount,
                null, null);

        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();

        CreateAudience_GlobalCriteria.stateSelector.select_States(_states);
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(_level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();

    }

    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 10)
    public void PT389_Verify_AbleToGetTo_SearchOrderRecords() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.verify_AtPage();

    }


    // Step 13
    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 20)
    public void PT389_Click_SaveAsProgram_ProgramDisplayPage_VerifyProgramSummaryAccurate() throws Exception {
        CreateAudience_SearchOrderRecords.retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing();

        TableData resultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();
        _lvl2Total = resultsTable.return_Row_BasedOn_1MatchingField("Level", "2").get("Total");
        CreateAudience_SearchOrderRecords.click_SaveAsProgram_Button();
        _modifiedTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, ProgramDisplay.dateFormat);
        ProgramEdit.click_Save();

        ProgramDisplay.verify_ProgramSummary_Panel(_audienceName, _audienceMart, _todaysDate, _modifiedTime,
                "N/A", "Active");
    }

    // Step 13
    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 21)
    public void PT389_ProgramDisplay_VerifyGlobalGeographyAccurate() throws Exception {
        _expectedGlobalGeography = new HashMap<>();
        _expectedGlobalGeography.put("State list", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_states));

        ProgramDisplay.globalGeography_Panel.verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 13
    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 21)
    public void PT389_ProgramDisplay_VerifyLevel1Accurate() throws Exception {
        _level1ExpectedValues = new HashMap<>();
        _level1ExpectedValues.put("Geography", "Use global geography");
        _level1ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level1.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level1.getMaximum()));

        ProgramDisplay.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 13
    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 21)
    public void PT389_ProgramDisplay_VerifyLevel2Accurate() throws Exception {
        _level2ExpectedValues = new HashMap<>();
        _level2ExpectedValues.put("Geography", "Use global geography");
        _level2ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level2.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level2.getMaximum()));

        _audienceID = ProgramDisplay.mainContent_Panel.return_SpecificValue("Id");
        ProgramDisplay.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }


    // Step 16
    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 40)
    public void PT389_ProgramDisplay_Click_SetupAutoOrder_TakesYouTo_Program_AutoOrderSetup_Page_VerifyProgramDetailsPanel() throws Exception {
        ProgramDisplay.click_SetupAutoOrder();
        Program_AutoOrderSetup_Page.verify_AtPage();
        Program_AutoOrderSetup_Page.verify_ProgramDetailsPanel(_audienceID, _audienceName, _audienceAccount, GlobalVariables.defaultMarketer, GlobalVariables.default_UserDesignation, _audienceMart,
                "Active", "", _modifiedTime, GlobalVariables.default_UserDesignation, _todaysDate, "");
    }

    // Step 4
    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 50)
    public void PT389_PT399_Program_AutoOrderSetup_ClickSave_Verify_AutoOrderSettingsPanel() throws Exception {
        String poNumber = "!search.searchid";
        HashMap<String, String> expectedAutoOrderSettings = new HashMap<>();
        expectedAutoOrderSettings.put("Select results", "Select all records on level 2 : auto level 2");
        expectedAutoOrderSettings.put("Auto ship", "No");
        expectedAutoOrderSettings.put("PO number format", poNumber);
        expectedAutoOrderSettings.put("File format type", "CSV");
        expectedAutoOrderSettings.put("Outputs for auto order", "");

        Program_AutoOrderSetup_Page.enter_PONumber(poNumber);
        Program_AutoOrderSetup_Page.directMail_Checkbox.check_Checkbox();
        Program_AutoOrderSetup_Page.click_SelectByLevel();
        Program_AutoOrderSetup_Page.levelCheckbox_ByText(_level2_Name).check_Checkbox();
        Program_AutoOrderSetup_Page.click_Save();
        ProgramDisplay.autoOrderSettings_Panel.verify_ExpectedData_Matches_ReturnedData(expectedAutoOrderSettings);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 60)
    public void PT389_ProgramDisplay_ClickRunProgram_ValidateMessage() throws Exception {
        _audienceID = ProgramDisplay.mainContent_Panel.return_SpecificValue("Id");
        ProgramDisplay.click_RunProgram();

        _new_audienceID = ProgramDisplay.return_AudienceID_From_RunProgramAlert();
        ProgramDisplay.verify_RunProgramAlert(_new_audienceID);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 70)
    public void PT389_ProgramDisplay_ClickView_ForAudiencesTable_VerifyFileName() throws Exception {
        ProgramDisplay.click_Audiences_ViewLink_ByIndex(1);

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", _csvTypeName);
        String fileName = fileTable.data.get(indexOfLineToDownload).get("Files");

        BaseUI.verify_true_AndLog(fileTable.data.size() > 0, "Found File Table Data.", "Did NOT find File Table Data.");
        BaseUI.verify_true_AndLog(fileName.startsWith(_new_audienceID), "File Name starts with Audience ID.",
                "File Name does NOT start with Audience ID, expected  " + _new_audienceID + " seeing " + fileName);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 80)
    public void PT389_ProgramDisplay_DownloadCSV_VerifyAllStatesMatchState() throws Exception {
        _csvData = CreateAudience_DownloadRecords_Page.return_CSV_Data(_csvTypeName);
        BaseUI.verify_true_AndLog(_csvData.data.size() > 0, "Found CSV Data.", "Did NOT find CSV Data.");
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("STATE", _stateAbbreviation, _csvData.data);
    }


    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 81)
    public void PT399_ProgramDisplay_DownloadCSV_VerifySelectedRecords_Matches_RecordCountWePicked() throws Exception {
        CreateAudience_DownloadRecords_Page.orderDetails_Panel.verify_ValueMatches("Selected Records", _lvl2Total);
    }

    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 81)
    public void PT399_ProgramDisplay_DownloadCSV_VerifyCSVRowCount_Matches_RecordCountWePicked() throws Exception {
        _csvData.verify_TableSize(Integer.valueOf(_lvl2Total.replace(",","")));
    }

    @Test(groups = {"all_Tests", "regression_Tests", "autoOrder_Tests"}, priority = 81)
    public void PT399_ProgramDisplay_DownloadCSV_VerifyCSV_AllRowsMatchLevel2() throws Exception {
        BaseUI.verify_TableColumn_AllColumnValues_MatchString("LEVEL", "2", _csvData.data);
    }




    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        try {
            Navigation.navigate_Programs();
            Programs.deactivateProgram(_audienceID);
        } finally {
            Browser.closeBrowser();
        }
    }


} //End of Class
