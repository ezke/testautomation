package inteliData.tests;

import java.text.MessageFormat;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mailosaur.model.Email;

import inteliData.data.GlobalVariables;
import inteliData.data.Utilities;
import inteliData.data.baseTest;
import inteliData.pages.Administration_Automations;
import inteliData.pages.AutomationDisplay;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.PackageDisplay;
import inteliData.pages.Packages;
import inteliData.pages.ProgramDisplay;
import inteliData.pages.SuppressionDisplay;
import inteliData.pages.Suppressions;
import org.testng.asserts.SoftAssert;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.Email_API;
import utils.ResultWriter;
import utils.TableData;

public class Automations_Tests extends baseTest {

	String _mainHandle;

	String _expectedDate;

	TableData _emails;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		_emails = DataBuilder.returnTableData_ForComparison("//src//inteliData//data//emails_automations.csv", "\\,",
				true);

		_expectedDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "M/d/yyyy") + " 12:00:00 AM";
		Email_API.return_EarliestEmail_ForRecipient(GlobalVariables.suppressionAutomationQA_Email);
		Browser.openBrowser(GlobalVariables.url);
		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);
		_mainHandle = Browser.driver.getWindowHandle();
	}

	@DataProvider(name = "run_Intelidata_SiteValidations")
	public Object[][] GenerateTests_ForAutomations() {

		int uniqueCount_OfEntries_ForCurrentEnvironment = _emails
				.countOf_Matches_ThatHave_ColumnName_AndCellValue("environment", GlobalVariables.environment);

		Object[][] scenariosToRun = new Object[uniqueCount_OfEntries_ForCurrentEnvironment][2];

		int indexToPlace_ColumnHeader = 0;
		for (HashMap<String, String> row : _emails.data) {
			if (row.get("environment").equals(GlobalVariables.environment)) {
				scenariosToRun[indexToPlace_ColumnHeader][0] = row.get("testCase") + "_";
				scenariosToRun[indexToPlace_ColumnHeader][1] = row;
				indexToPlace_ColumnHeader++;
			}
		}

		return scenariosToRun;
	}

	@Test(dataProvider = "run_Intelidata_SiteValidations", alwaysRun = true, groups = { "all_Tests", "regression_Tests",
			"automations_Tests" })
	public void Run_WebsiteValidations_ForScenario(String testCase, HashMap<String, String> dataRow) throws Exception {
		// String location = dataRow.get("location");
		String scenario = dataRow.get("scenario");
		String type = dataRow.get("type");

		if (type.equals("Programs")) {
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")) {
				validate_Successful_Program_Automation_Intelidata(dataRow.get("automationID"), dataRow.get("fileName"),
						dataRow.get("location"));
			} else if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")
					|| scenario.equals("NoHeaderHasRecords") || scenario.equals("DuplicateFiles")) {
				validate_NotSuccessful_ProgramAutomation_Intelidata(dataRow.get("automationID"));
			} else {
				BaseUI.log_AndFail("Scenario " + scenario + " NOT found.");
			}

		} else if (type.equals("Suppressions")) {
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")) {
				validate_Successful_Suppression_Automation_Intelidata(dataRow.get("automationID"),
						dataRow.get("fileName"), dataRow.get("poNumber"), dataRow.get("packageID"),
						dataRow.get("packageProgramName"));
			} else if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")) {
				validate_FailNoData_Suppression_Automation_Intelidata(dataRow.get("automationID"),
						dataRow.get("fileName"), dataRow.get("poNumber"), dataRow.get("packageID"),
						dataRow.get("packageProgramName"));
			} else if (scenario.equals("NoHeaderHasRecords")) {
				validate_FailNoHeaders_Suppression_Automation_Intelidata(dataRow.get("automationID"),
						dataRow.get("fileName"), dataRow.get("poNumber"), dataRow.get("packageID"),
						dataRow.get("packageProgramName"));
				// Missing Empty and Duplicate
			} else {
				validate_NotSuccessful_SuppressionAutomation_Intelidata(dataRow.get("automationID"));
			}

		} else {
			BaseUI.log_AndFail("Unidentified type of " + type);
		}

	}

	@Test(dataProvider = "run_Intelidata_SiteValidations", alwaysRun = true, groups = { "all_Tests", "regression_Tests",
			"automations_Tests" })
	public void Run_EmailValidations_ForScenario(String testCase, HashMap<String, String> dataRow) throws Exception {
		// String location = dataRow.get("location");
		String scenario = dataRow.get("scenario");
		String type = dataRow.get("type");
		String expectedSubject = "";
		String expectedSuccessMessage = "";
		String fileName = dataRow.get("fileName");

		boolean expecting1email = true;

		if (type.equals("Programs")) {
			if (dataRow.get("location").equals("External")) {
				expecting1email = false;
			}
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")) {
				expectedSubject = "Program File Automation Notification - Success";
				expectedSuccessMessage = "The following file automation was successful.";

			} else if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")) {
				expectedSubject = "Program File Automation Notification - Success";
				expectedSuccessMessage = "There is no data in the file for automation " + dataRow.get("automationID");
			} else if (scenario.equals("NoHeaderHasRecords")) {

				expectedSubject = "Program File Automation Notification - Failure";
				expectedSuccessMessage = "The layout of the file does not match the Automation.";
			} else if (scenario.equals("DuplicateFiles")) {
				expectedSubject = "Program File Automation Notification - Failure";
				expectedSuccessMessage = "Could not process the automation's file. Too many files exist with the file's naming convention.";
				expecting1email = false;
			} else {
				BaseUI.log_AndFail("Scenario " + scenario + " NOT found.");
			}

		} else if (type.equals("Suppressions")) {
			if (scenario.equals("HeaderWithRecords") || scenario.equals("HeaderWith1Record")) {
				expectedSubject = "Suppression File Automation Notification - Success";
				expectedSuccessMessage = "The following file automation was successful.";
				fileName = fileName.replaceFirst("/automations/", "");
			} else if (scenario.equals("HeaderWithNoRecords") || scenario.equals("Empty")) {
				expectedSubject = "Suppression File Automation Notification - Success";
				expectedSuccessMessage = "There is no data in the file for automation " + dataRow.get("automationID");
			} else if (scenario.equals("NoHeaderHasRecords")) {
				expectedSubject = "Suppression File Automation Notification - Failure";
				expectedSuccessMessage = "The layout of the file does not match the Automation.";
			} else if (scenario.equals("DuplicateFiles")) {
				expectedSubject = "Suppression File Automation Notification - Failure";
				expectedSuccessMessage = "Could not process the automation's file. Too many files exist with the file's naming convention.";
			} else {
				BaseUI.log_AndFail("Unidentified scenario of " + scenario);
			}

		} else {
			BaseUI.log_AndFail("Unidentified type of " + type);
		}

		Utilities.verify_Email_AndValuesMatch(dataRow.get("email"), expectedSubject, dataRow.get("automationName"),
				fileName, _expectedDate, expectedSuccessMessage, expecting1email);

	}

//	@DataProvider(name = "run_DatabaseErrorValidations")
//	public Object[][] GenerateTests_ForAutomations_DatabaseErrorValidations() {
//
//		HashMap<String, String> valuesToMatch = new HashMap<String, String>();
//		valuesToMatch.put("environment", GlobalVariables.environment);
//		valuesToMatch.put("databaseError", "TRUE");
//
//		int uniqueCount_OfEntries = _emails.countOf_Matches_ThatHave_matchingHashMapValues(valuesToMatch);
//
//		Object[][] scenariosToRun = new Object[uniqueCount_OfEntries][2];
//
//		int indexToPlace_ColumnHeader = 0;
//		for (HashMap<String, String> row : _emails.data) {
//			if (row.get("environment").equals(GlobalVariables.environment) && row.get("databaseError").equals("TRUE")) {
//				scenariosToRun[indexToPlace_ColumnHeader][0] = row.get("testCase") + "_";
//				scenariosToRun[indexToPlace_ColumnHeader][1] = row;
//				indexToPlace_ColumnHeader++;
//			}
//		}
//
//		return scenariosToRun;
//	}


	public void validate_FailNoHeaders_Suppression_Automation_Intelidata(String automationID, String fileName,
			String poNumber, String packageID, String packageProgramName) throws Exception {
		String orderID = "";
		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(automationID);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.sort_Header_Descending("History Date");
		AutomationDisplay.verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndFailNoHeaders(
				"External Suppression File", "External Suppression", fileName, _expectedDate);

		orderID = AutomationDisplay.automationsTable.return_TableData().data.get(0).get("Order Id");

		Navigation.navigate_Suppressions();
		Suppressions.search_ByText_UntilFound(orderID);
		Suppressions.verify_SuppressionAutomation_1Entry_Failed(orderID, poNumber, "External Suppress");

		Suppressions.suppressionTable.click_View_ForRow(1);
		SuppressionDisplay.verify_DetailsPanel_SuppressionAutomation_NoHeaders(orderID, automationID,
				"External Suppression", poNumber, fileName.replaceFirst("/automations/", ""));

		Navigation.navigate_Packages();
		Packages.quickSearchBox.search_WithCriteria(packageID);
		Packages.packagesTable.click_View_ForRow(1);
		PackageDisplay.verify_PackageSummary(packageID, packageProgramName, "Ready");
	}

	public void validate_FailNoData_Suppression_Automation_Intelidata(String automationID, String fileName,
			String poNumber, String packageID, String packageProgramName) throws Exception {
		String orderID = "";
		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(automationID);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.sort_Header_Descending("History Date");
		AutomationDisplay.verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndFailNoRecords(
				"External Suppression File", "External Suppression", fileName, _expectedDate);

		orderID = AutomationDisplay.automationsTable.return_TableData().data.get(0).get("Order Id");

		Navigation.navigate_Suppressions();
		Suppressions.search_ByText_UntilFound(orderID);
		Suppressions.verify_SuppressionAutomation_1Entry_FailNoRecords(orderID, poNumber, "External Suppress");

		Suppressions.suppressionTable.click_View_ForRow(1);
		SuppressionDisplay.verify_DetailsPanel_SuppressionAutomation_NoRecords(orderID, automationID,
				"External Suppression", poNumber, fileName.replaceFirst("/automations/", ""));

		Navigation.navigate_Packages();
		Packages.quickSearchBox.search_WithCriteria(packageID);
		Packages.packagesTable.click_View_ForRow(1);
		PackageDisplay.verify_PackageSummary(packageID, packageProgramName, "Ready");
	}

	public void validate_Successful_Suppression_Automation_Intelidata(String automationID, String fileName,
			String poNumber, String packageID, String packageProgramName) throws Exception {

		String orderID = "";
		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(automationID);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.sort_Header_Descending("History Date");
		AutomationDisplay.verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndSuccessful(
				"External Suppression File", "External Suppression", fileName, _expectedDate);

		orderID = AutomationDisplay.automationsTable.return_TableData().data.get(0).get("Order Id");

		Navigation.navigate_Suppressions();
		Suppressions.search_ByText_UntilFound(orderID);
		Suppressions.verify_SuppressionAutomation_1Entry_Successful(orderID, poNumber, "External Suppress");

		Suppressions.suppressionTable.click_View_ForRow(1);
		SuppressionDisplay.verify_DetailsPanel_SuccessfulSuppressionAutomation(orderID, automationID,
				"External Suppression", poNumber, fileName.replaceFirst("/automations/", ""));

		Navigation.navigate_Packages();
		Packages.quickSearchBox.search_WithCriteria(packageID);
		Packages.packagesTable.click_View_ForRow(1);
		PackageDisplay.verify_PackageSummary(packageID, packageProgramName, "Ready");
	}

	public void validate_Successful_Program_Automation_Intelidata(String automationID, String fileName, String location)
			throws Exception {
		String name = "Append File";
		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(automationID);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.sort_Header_Descending("History Date");
		AutomationDisplay.verify_AutomationsTable_Program_1stRow_FromPriorNight_AndSuccessful(name, _expectedDate);

		AutomationDisplay.click_ProgramDetails_ID_Link();
		ProgramDisplay.verify_SubscriptionFiles_FromToday_Appended_AndPreviousDay_Archived(fileName, location);
	}

	public void validate_NotSuccessful_SuppressionAutomation_Intelidata(String automationID) throws Exception {
		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(automationID);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.verify_NoDataInTable();

	}

	public void validate_NotSuccessful_ProgramAutomation_Intelidata(String automationID) throws Exception {
		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(automationID);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		TableData automationsTable = AutomationDisplay.automationsTable.return_TableData();
		//validate we either had no entries or there were no entries within the last 2 days.
		if(automationsTable.data.size() > 0){

			for(HashMap<String, String> row : automationsTable.data){
				String historyDate = row.get("History Date");
				String dateFormat = "MM/dd/yyyy hh:mm:ss a";
				String date1 = BaseUI.getDateAsString_InRelationToTodaysDate(-2, dateFormat);
				String date2 = BaseUI.getDateAsString_InRelationToTodaysDate(0, dateFormat);
				BaseUI.verify_Date_IsNotBetween_DateRange(date1, date2, historyDate, dateFormat);
			}

		}else{
			BaseUI.log_Pass("No Data Found in table.");
		}
//		AutomationDisplay.automationsTable.verify_NoDataInTable();

		AutomationDisplay.click_ProgramDetails_ID_Link();
		ProgramDisplay.verify_SubscriptionFiles_NoNewFile();
	}

	@Test(groups = { "all_Tests", "regression_Tests", "automations_Tests" })
	public void PT259_SuppressionAutomation_Email_Successful() throws Exception {
		String expectedSubject = "Suppression File Automation Notification - Success";
		String expectectedAutomationName = "Regression Suppression Internal";
		String expectedFileName = "RegressionSuppressionInternal.csv";
		String addressToUse = null;

		if (GlobalVariables.environment.equals("QA")) {
			addressToUse = GlobalVariables.suppressionAutomationQA_Email;
		} else if (GlobalVariables.environment.equals("PROD")) {
			addressToUse = GlobalVariables.suppressionAutomationPROD_Email;
		} else {
			BaseUI.log_AndFail(
					"Unrecognized environment for this test.  Will need to be updated to support this environment.");
		}

		Email[] allEmailsForRecipient = Email_API.return_AllEmailsByRecipient(addressToUse);

		Email emailToUse = allEmailsForRecipient[0];
		Email_API.open_EmailHTML_In_NewBrowserWindow(emailToUse);

		Utilities.verify_Email_Successful_Automations(emailToUse, expectedSubject, expectectedAutomationName,
				expectedFileName, _expectedDate);

		Email_API.verify_EmailCount(allEmailsForRecipient, 1);

	}

	@Test(groups = { "all_Tests", "regression_Tests", "automations_Tests" })
	public void PT260_ProgramAutomation_Email_Successful() throws Exception {
		String expectedSubject = "Program File Automation Notification - Success";
		String expectectedAutomationName = "Regression Program Automation";
		String expectedFileName = "RegressionProgramInternal.csv";
		String addressToUse = null;

		if (GlobalVariables.environment.equals("QA")) {
			addressToUse = GlobalVariables.programAutomationQA_Email;
		} else if (GlobalVariables.environment.equals("PROD")) {
			addressToUse = GlobalVariables.programAutomationPROD_Email;
		} else {
			BaseUI.log_AndFail(
					"Unrecognized environment for this test.  Will need to be updated to support this environment.");
		}

		Email[] allEmailsForRecipient = Email_API.return_AllEmailsByRecipient(addressToUse);

		Email emailToUse = allEmailsForRecipient[0];

		Email_API.open_EmailHTML_In_NewBrowserWindow(emailToUse);

		Utilities.verify_Email_Successful_Automations(emailToUse, expectedSubject, expectectedAutomationName,
				expectedFileName, _expectedDate);

		Email_API.verify_EmailCount(allEmailsForRecipient, 1);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "automations_Tests" })
	public void PT259_Admin_Automations_SuppressionAutomation_HasEntry() throws Exception {

		String id = "578";

		String name = "External Suppression File";
		String orderType = "External Suppression";
		String fileName = "RegressionSuppressionInternal.csv";

		Navigation.navigate_Administration_Automations();

		Administration_Automations.quickSearchBox.search_WithCriteria(id);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.sort_Header_Descending("History Date");
		AutomationDisplay.verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndSuccessful(name, orderType,
				fileName, _expectedDate);
	}

	@Test(groups = { "all_Tests", "regression_Tests", "automations_Tests" })
	public void PT260_Admin_Automations_ProgramAutomation_HasEntry() throws Exception {
		String id = "579";
		String expectedName = "Append File";

		Navigation.navigate_Administration_Automations();
		Administration_Automations.quickSearchBox.search_WithCriteria(id);
		Administration_Automations.automationsTable.click_View_ForRow(1);
		AutomationDisplay.automationsTable.sort_Header_Descending("History Date");
		AutomationDisplay.verify_AutomationsTable_Program_1stRow_FromPriorNight_AndSuccessful(expectedName,
				_expectedDate);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);

		if (!Browser.driver.getWindowHandle().equals(_mainHandle)) {
			BaseUI.close_ExtraWindows(_mainHandle);
		}
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();
	}



}// End of Class
