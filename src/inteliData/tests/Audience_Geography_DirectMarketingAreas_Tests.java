package inteliData.tests;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.*;
import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.BaseUI;
import utils.Browser;
import utils.ResultWriter;
import utils.TableData;

import java.util.HashMap;

public class Audience_Geography_DirectMarketingAreas_Tests extends baseTest {

    private final String _poNumber = "DMA-Automation PO";

    private final String _audienceName = "Direct Marketing Areas - Automation";
    private final String _audienceMart = "Auto Mart";
    private final String _audienceAccount = "ACME";

    private final String _marketer = GlobalVariables.defaultMarketer;
    private String _review_CreationTime = "";

    private int _expectedTotal;
    private int _audienceID;

    // Global Criteria variables
    private final String[] _directMarketingAreas_Global = {"BAKERSFIELD", "CORPUS CHRISTI", "LA CROSSE-EAU CLAIRE", "PORTLAND-AUBURN"
    };

    // Level 1 variables
    private final String _level1_Name = "auto level 1";
    private final Range<Integer> _ficoRange_Level1 = Range.between(500, 510);

    // Level 2 variables
    private final String _level2_Name = "auto level 2";
    private final Range<Integer> _ficoRange_Level2 = Range.between(520, 525);
    private final String[] _directMarketingAreas_Level2 = {"CHICAGO", "EUGENE", "NEW YORK", "WAUSAU-RHINELANDER"};

    private HashMap<String, String> _expectedGlobalGeography;
    private HashMap<String, String> _level1ExpectedValues;
    private HashMap<String, String> _level2ExpectedValues;

    private TableData _dma_ResultsTable;
    private TableData _csvTable;
    private TableData _dmaQueryData;

    private final TableData expectedRows = new TableData();

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        _expectedGlobalGeography = new HashMap<String, String>();
        _expectedGlobalGeography.put("Direct Marketing Area", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_directMarketingAreas_Global, true));

        _level1ExpectedValues = new HashMap<String, String>();
        _level1ExpectedValues.put("Geography", "Use global geography");
        _level1ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level1.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level1.getMaximum()));

        _level2ExpectedValues = new HashMap<String, String>();
        _level2ExpectedValues.put("FICO auto score", "From " + String.valueOf(_ficoRange_Level2.getMinimum()) + " To "
                + String.valueOf(_ficoRange_Level2.getMaximum()));
        _level2ExpectedValues.put("Direct Marketing Area", CreateAudience_SearchReview.returnArray_AsString_ForPanels(_directMarketingAreas_Level2, true));

        Browser.openBrowser(GlobalVariables.url);

        LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

        HomePage.click_CreateAudience_Button();
        CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(_audienceName, _audienceMart, _audienceAccount,
                null, null);
        SuppliersAndUsage_Modal.experian_DraggableElement.check_Checkbox();
        SuppliersAndUsage_Modal.equifax_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
        SuppliersAndUsage_Modal.click_CreateAudience();

        CreateAudience_GlobalCriteria.include_DirectMarketingAreas_TextBox.enterText(_directMarketingAreas_Global);

        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_DefineLevels.enter_Level(_level1_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_AddLevel_Button();
        CreateAudience_DefineLevels.enter_Level(_level2_Name);
        CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
        CreateAudience_DefineLevels.enter_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.useGlobalGeography_Checkbox.uncheck_Checkbox();

        CreateAudience_DefineLevels.include_DirectMarketingAreas_TextBox.enterText(_directMarketingAreas_Level2);

        CreateAudience_DefineLevels.click_SaveLevel_Button();

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT395_SearchReview_Verify_GlobalGeographyPanel() throws Exception {
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT395_SearchReview_Verify_Level1Panel() throws Exception {
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 21
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 10)
    public void PT395_SearchReview_Verify_Level2Panel() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }

    // Step 22
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 20)
    public void PT395_SearchReview_Click_EditGlobal_Verify_DirectMarketingAreas_Retained() throws Exception {
        CreateAudience_SearchReview.click_EditGlobal();
        CreateAudience_GlobalCriteria.include_DirectMarketingAreas_TextBox.verify_OptionsSelected(_directMarketingAreas_Global);
    }

    // Step 23
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 30)
    public void PT395_SearchReview_GlobalGeography_Verify_DirectMarketingAreas_AreStillAccurate() throws Exception {
        CreateAudience_GlobalCriteria.click_SaveAndContinue();
        CreateAudience_SearchReview.globalGeography_Panel
                .verify_ExpectedData_Matches_ReturnedData(_expectedGlobalGeography);
    }

    // Step 26
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 40)
    public void PT395_SearchReview_EditLevels_Level1_FICO_StillValid() throws Exception {
        CreateAudience_SearchReview.click_EditLevels();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 1");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level1.getMinimum(), _ficoRange_Level1.getMaximum());

    }

    // Step 29
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 41)
    public void PT395_SearchReview_EditLevels_Level2_FICO_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();
        CreateAudience_DefineLevels_LevelListPage.select_Level("Level 2");
        CreateAudience_DefineLevels_LevelListPage.click_EditLevel_Button();
        CreateAudience_DefineLevels.verify_FICO_Score(_ficoRange_Level2.getMinimum(), _ficoRange_Level2.getMaximum());

    }

    // Step 30
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 42)
    public void PT395_SearchReview_EditLevels_Level2_DirectMarketingAreas_StillValid() throws Exception {
        CreateAudience_DefineLevels.geographySelections.expand_Section();
        CreateAudience_DefineLevels.include_DirectMarketingAreas_TextBox.verify_OptionsSelected(_directMarketingAreas_Level2);
    }

    // Step 32
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 64)
    public void PT395_SearchReview_Level1_StillValid() throws Exception {
        CreateAudience_DefineLevels.click_SaveLevel_Button();

        _review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);

        CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
        CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(_level1ExpectedValues);
    }

    // Step 32
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 65)
    public void PT395_SearchReview_Level2_StillValid() throws Exception {
        CreateAudience_SearchReview.level2_Panel.verify_ExpectedData_Matches_ReturnedData(_level2ExpectedValues);
    }


    // Step 34
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 75)
    public void PT395_SearchReview_BuildAudience_Level1_And2_DirectMarketingAreas_FoundInTable() throws Exception {
        CreateAudience_SearchReview.click_Top_BuildAudience_Button();
        CreateAudience_SearchOrderRecords.select_Display_DropdownValue("Direct marketing area");
        _dma_ResultsTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();

        for (String value : _directMarketingAreas_Global) {
            HashMap<String, String> expectedRow = new HashMap<String, String>();
            expectedRow.put("Level", "1");
            expectedRow.put("DMA", value);

            expectedRows.data.add(expectedRow);
        }

        for (String value : _directMarketingAreas_Level2) {
            HashMap<String, String> expectedRow = new HashMap<String, String>();
            expectedRow.put("Level", "2");
            expectedRow.put("DMA", value);

            expectedRows.data.add(expectedRow);
        }
        _dma_ResultsTable.verify_TableSize(_directMarketingAreas_Global.length + _directMarketingAreas_Level2.length);

        CreateAudience_SearchOrderRecords.verify_Values_Found_ForLevel(_dma_ResultsTable, expectedRows);
    }

    // Step 36
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 80)
    public void PT395_SearchOrderRecords_PickRecords_Verify_FooterRequested_MatchesTotalWePick() throws Exception {
        CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();
        CreateAudience_SearchOrderRecords.select_AndUpdate_RecordsFromEachLine_IncrementedBy(_dma_ResultsTable, 10,
                10);
        _expectedTotal = _dma_ResultsTable.return_Sum_ofColumn("Requested").intValueExact();
        _audienceID = Integer
                .parseInt(CreateAudience_SearchOrderRecords.mainContent_Panel.return_SpecificValue("Audience ID"));
        CreateAudience_SearchOrderRecords.verify_Footer_Requested(_expectedTotal);
    }

    // Step 39
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 90)
    public void PT395_PurchaseRecords_VerifyAudienceSummary() throws Exception {
        CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
        CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(_poNumber);
        CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
        CreateAudience_Order_OutputOptions.click_PlaceOrder();

        CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(_audienceID, _audienceMart, _marketer,
                _review_CreationTime, _expectedTotal, _poNumber);
    }

    // Step 39
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 100)
    public void PT395_PurchaseRecords_Results_Verify_FooterAccurate() throws Exception {
        CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(String.valueOf(_expectedTotal),
                String.valueOf(_expectedTotal));
    }

    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 105)
    public void PT395_Verify_DownloadRecords_AbleToNavigateTo() throws Exception {
        CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
        CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 110)
    public void PT395_DownloadRecords_VerifyCSV_SizeMatchesExpected() throws Exception {
        CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();

        String typeName = "CSV (Order File)";
        String fileName = "";

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
        fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
        _csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1,
                fileName);

        _dmaQueryData = CreateAudience_DownloadRecords_Page.return_DMA_DatabaseQuery(_audienceID);

        _csvTable.verify_TableSize(_expectedTotal);
    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT395_DownloadRecords_Verify_CSV_Lines_FICO_City_AndState() throws Exception {
        CreateAudience_DownloadRecords_Page.add_FicoMin_Max_ToTable_ForGivenLevel(expectedRows, "1", _ficoRange_Level1);
        CreateAudience_DownloadRecords_Page.add_FicoMin_Max_ToTable_ForGivenLevel(expectedRows, "2", _ficoRange_Level2);

        CreateAudience_DownloadRecords_Page.validate_Fico_InCSV(_csvTable, expectedRows);
        CreateAudience_DownloadRecords_Page.verify_CSV_Matches_dmaDBQuery(_csvTable, _dmaQueryData);

    }

    // Step 41
    @Test(groups = {"all_Tests", "regression_Tests", "geography_Tests"}, priority = 111)
    public void PT395_DownloadRecords_Verify_CSV_Lines_Counts_OF_LevelsWithSpecificStates() throws Exception {
        CreateAudience_DownloadRecords_Page.verify_DMA_RequestedCounts_MatchCSVCount_AndDB(_dma_ResultsTable, _csvTable, _dmaQueryData);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        ResultWriter.checkForFailureAndScreenshot(result);
    }

    @AfterClass(alwaysRun = true)
    public void writeResultsAndTearDown() throws Exception {
        Browser.closeBrowser();

    }

}// End of Class
