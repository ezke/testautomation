package inteliData.tests;

import java.util.HashMap;

import org.apache.commons.lang3.Range;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import inteliData.data.GlobalVariables;
import inteliData.data.baseTest;
import inteliData.modals.CreateAudience_Modal;
import inteliData.modals.SuppliersAndUsage_Modal;
import inteliData.pages.CreateAudience_SearchOrderRecords;
import inteliData.pages.Activities;
import inteliData.pages.CreateAudience_DefineLevels;
import inteliData.pages.CreateAudience_DefineLevels_LevelListPage;
import inteliData.pages.CreateAudience_DownloadRecords_Page;
import inteliData.pages.CreateAudience_GlobalCriteria;
import inteliData.pages.CreateAudience_Order_OutputOptions;
import inteliData.pages.CreateAudience_PurchaseRecordsPage;
import inteliData.pages.CreateAudience_SearchReview;
import inteliData.pages.HomePage;
import inteliData.pages.LoginPage;
import inteliData.pages.Navigation;
import inteliData.pages.SuppressionCompleted;
import inteliData.pages.SuppressionDisplay;
import inteliData.pages.Suppressions;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.FileOperations;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class BuildAnAudience_AndPlaceOrder_AndSalesOrderSuppression_AndRecreate_Tests extends baseTest {

	String audienceName = "AutomationTC1";
	String audienceMart = "Auto Mart";
	String audienceAccount = "ACME";
	String account_DBName = "su_4586";
	Integer numberOfLevels = 1;
	String marketer = GlobalVariables.defaultMarketer;
	String review_CreationTime = "";
	String[] statesToSelect = { "ALASKA", "RHODE ISLAND" };
	String[] statesAbbreviated = { "AK", "RI" };

	String expectedTotalAsString;
	Integer expectedTotal;
	Integer audienceID;
	String orderID;

	String suppressionType = "Sales Order";

	HashMap<String, String> expectedFilterValues = new HashMap<String, String>();
	TableData audienceSummaryTable;

	String levelName = "AutoTest";
	Integer ficoRange1 = 500;
	Integer ficoRange2 = 510;

	Integer expectedTotalRecords_Minimum = 2000;
	String purchaseAmount = "";
	String purchaseTime = "";

	String poNumber = "Auto Test";
	HashMap<String, String> expectedGlobalGeography;
	HashMap<String, String> expectedGeneralInfo;
	HashMap<String, String> expectedSuppressions;

	String firstPartyDataFileName = "";

	String recreateTime;

	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		if(GlobalVariables.environment.equals("DEV")){
			expectedTotalRecords_Minimum = 100;
		}

		expectedFilterValues.put("Geography", "Use global geography");
		expectedFilterValues.put("FICO auto score", "From 500 To 510");
		Browser.openBrowser(GlobalVariables.url);

		LoginPage.login(GlobalVariables.userName, GlobalVariables.password);

		HomePage.click_CreateAudience_Button();
		CreateAudience_Modal.enter_NewAudience_Fields_AndSaveContinue(audienceName, audienceMart, audienceAccount, null,
				null);
		SuppliersAndUsage_Modal.equifax_DraggableElement.check_Checkbox();
		SuppliersAndUsage_Modal.experian_DraggableElement.uncheck_Checkbox();
		SuppliersAndUsage_Modal.transUnion_DraggableElement.uncheck_Checkbox();
		SuppliersAndUsage_Modal.onePerHousehold_Checkbox.check_Checkbox();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 10)
	public void PT250_SelectDirectMail_CheckDirectMail_VerifyAlert() throws Exception {
		SuppliersAndUsage_Modal.directMail_Checkbox.check_Checkbox();
		SuppliersAndUsage_Modal.directMail_Checkbox.verify_AlertMessage_Appears_AndCorrect();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 15)
	public void PT250_SelectDirectMail_UnCheckDirectMail_Verify_DoesntAppear() throws Exception {
		SuppliersAndUsage_Modal.directMail_Checkbox.uncheck_Checkbox();
		SuppliersAndUsage_Modal.directMail_Checkbox.verify_AlertMessage_DoesNOTAppear();
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 20)
	public void PT250_CreateAudience_GlobalCriteria() throws Exception {
		SuppliersAndUsage_Modal.click_CreateAudience();
		BaseUI.verifyElementAppears(Locator.lookupElement("createAud_GlobalCrit_ActiveHeader"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 25)
	public void PT250_CreateAudience_SelectStates_SaveAndContinue() throws Exception {
		CreateAudience_GlobalCriteria.stateSelector.select_States(statesToSelect);
		CreateAudience_GlobalCriteria.click_SaveAndContinue();
		BaseUI.verifyElementAppears(Locator.lookupElement("createAud_DefineLevs_AddALevel_Header"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 30)
	public void PT250_CreateAudience_DefineLevel_EnterFicoScore_VerifyFiltersOnNextPage() throws Exception {

		CreateAudience_DefineLevels.enter_Level(levelName);
		CreateAudience_DefineLevels.click_InitialLevelNameEntry_SaveAndContinue();
		CreateAudience_DefineLevels.enter_FICO_Score(ficoRange1, ficoRange2);
		CreateAudience_DefineLevels.click_SaveLevel_Button();
		CreateAudience_DefineLevels_LevelListPage.level1_Panel.verify_HeaderText_Matches("Level 1 : " + levelName);
		CreateAudience_DefineLevels_LevelListPage.level1_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedFilterValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 35)
	public void PT250_CreateAudience_ReviewAndRun_GeneralInformation_MatchesExpected() throws Exception {
		review_CreationTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);
		CreateAudience_DefineLevels_LevelListPage.click_Continue_Button();
		expectedGeneralInfo = new HashMap<String, String>();
		expectedGeneralInfo.put("One Per Household", "Yes");
		expectedGeneralInfo.put("Unique Phones Only", "No");
		expectedGeneralInfo.put("Supplier (s)", "Equifax (1)");
		CreateAudience_SearchReview.generalInformation_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedGeneralInfo);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT250_CreateAudience_ReviewAndRun_GlobalGeography_MatchesExpected() throws Exception {
		String stateString = "";
		for (int i = 0; i < statesToSelect.length; i++) {
			if (i > 0) {
				stateString += " , ";
			}
			stateString += statesToSelect[i].toUpperCase();
		}
		expectedGlobalGeography = new HashMap<String, String>();
		expectedGlobalGeography.put("State list", stateString);
		CreateAudience_SearchReview.globalGeography_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedGlobalGeography);

	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT250_CreateAudience_ReviewAndRun_Level1_MatchesExpected() throws Exception {
		CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(expectedFilterValues);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT250_CreateAudience_ReviewAndRun_Suppressions_MatchesExpected() throws Exception {
		expectedSuppressions = new HashMap<String, String>();
		expectedSuppressions.put("Suppression level", "Address (Address Only)");

		CreateAudience_SearchReview.suppressions_Panel.verify_ExpectedData_Matches_ReturnedData(expectedSuppressions);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 36)
	public void PT250_CreateAudience_ReviewAndRun_MainContent() throws Exception {
		CreateAudience_SearchReview.verify_MainContent(audienceMart, marketer, review_CreationTime, numberOfLevels);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 40)
	public void PT250_CreateAudience_AudienceSummary_MainContent_Valid() throws Exception {
		Integer selectedRecords = 0;

		CreateAudience_SearchReview.click_Top_BuildAudience_Button();
		CreateAudience_SearchOrderRecords.verify_mainContentPanel(audienceMart, marketer, review_CreationTime,
				expectedTotalRecords_Minimum, selectedRecords);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 41)
	public void PT250_CreateAudience_AudienceSummary_InitialTable_Validations() throws Exception {
		HashMap<String, String> mainContentPanelValues = CreateAudience_SearchOrderRecords.mainContent_Panel
				.return_PanelData();
		expectedTotalAsString = mainContentPanelValues.get("Total Records");
		expectedTotal = Integer.parseInt(mainContentPanelValues.get("Total Records").replace(",", ""));
		audienceID = Integer.parseInt(mainContentPanelValues.get("Audience ID"));

		CreateAudience_SearchOrderRecords.verify_1Row_OfData_MatchesExpected(expectedTotal, 0, 0, false);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 41)
	public void PT250_CreateAudience_AudienceSummary_FooterMatchesRow1() throws Exception {
		audienceSummaryTable = CreateAudience_SearchOrderRecords.resultsTable.return_TableData();

		CreateAudience_SearchOrderRecords.verify_Footer(audienceSummaryTable.data.get(0).get("Total"),
				audienceSummaryTable.data.get(0).get("Ordered"), audienceSummaryTable.data.get(0).get("Requested"),
				audienceSummaryTable.data.get(0).get("Phone"));
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT250_CreateAudience_AudienceSummary_VerifySuppliers_ControlText() throws Exception {
		String expectedValue = "Equifax";
		CreateAudience_SearchOrderRecords.suppliersDropdown.verify_DropdownButton_Text_Accurate(expectedValue);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 41)
	public void PT250_CreateAudience_AudienceSummary_VerifySuppliers_SelectRecords_Disabled() throws Exception {
		BaseUI.verifyElementDisabled(Locator.lookupElement("createAud_AudSum_SelectRecords_Button"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 45)
	public void PT250_CreateAudience_AudienceSummary_CheckRow1Checkbox_Verify_ValuesUpdate_InTable() throws Exception {
		CreateAudience_SearchOrderRecords.resultsTable.checkbox_ByIndex(1).check_Checkbox();
		CreateAudience_SearchOrderRecords.verify_1Row_OfData_MatchesExpected(expectedTotal, 0, expectedTotal, true);
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 46)
	public void PT250_CreateAudience_AudienceSummary_CheckRow1Checkbox_Verify_ValuesUpdate_InFooter() throws Exception {
		// Values for footer should match previous values except the total will also be
		// the requested this time around.
		CreateAudience_SearchOrderRecords.verify_Footer(audienceSummaryTable.data.get(0).get("Total"),
				audienceSummaryTable.data.get(0).get("Ordered"), audienceSummaryTable.data.get(0).get("Total"),
				audienceSummaryTable.data.get(0).get("Phone"));
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 47)
	public void PT250_CreateAudience_AudienceSummary_CheckDirectMail_VerifyMessage() throws Exception {
		CreateAudience_SearchOrderRecords.directMail_Checkbox.check_Checkbox();
		CreateAudience_SearchOrderRecords.directMail_Checkbox.verify_AlertMessage_Appears_AndCorrect();
	}

	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 48)
	public void PT250_CreateAudience_AudienceSummary_VerifySelectedRecords_Updated() {
		CreateAudience_SearchOrderRecords.mainContent_Panel.verify_ValueMatches("Selected Records",
				expectedTotalAsString + " (100.0%)");
	}

	// Beginning of Place Order workflow

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 50)
	public void PT245_CreateAudience_OrderDetails_MainContentPanel() throws Exception {
		CreateAudience_SearchOrderRecords.click_SelectRecords_Button();
		CreateAudience_Order_OutputOptions.verify_mainContentPanel(audienceID, audienceMart, marketer,
				review_CreationTime, expectedTotal, expectedTotal);

	}

	// Steps 4 and 5
	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 55)
	public void PT245_CreateAudience_PurchaseRecords_AudienceSummary_Accurate() throws Exception {
		CreateAudience_Order_OutputOptions.enterText_IntoPONumberTextBox(poNumber);
		CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name", "CSV");
		if(GlobalVariables.environment.equals("DEV")){
			CreateAudience_Order_OutputOptions.standard_Table.uncheck_Checkbox_ByColumnHeader_AndValueMatch("Name",
					"QA CSV with Supplier");
		}else {
			CreateAudience_Order_OutputOptions.standard_Table.check_Checkbox_ByColumnHeader_AndValueMatch("Name",
					"QA CSV with Supplier");
		}
		CreateAudience_Order_OutputOptions.click_PlaceOrder();

		CreateAudience_PurchaseRecordsPage.verify_AudienceSummaryPanel(audienceID, audienceMart, marketer,
				review_CreationTime, expectedTotal, poNumber);
	}

	// Ignores Phone column per discussion with product SMEs
	// Step 5
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 56)
	public void PT245_CreateAudience_PurchaseRecords_Results_FooterAccurate() throws Exception {

		CreateAudience_PurchaseRecordsPage.verify_resultsTable_Footer(expectedTotalAsString, expectedTotalAsString);
	}

	// Ignores Phone column per discussion with product SMEs
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 56)
	public void PT245_CreateAudience_PurchaseRecords_Results_TableAccurate() throws Exception {
		CreateAudience_PurchaseRecordsPage.verify_resultsTable_1Row_OfData_MatchesExpected(expectedTotal,
				expectedTotal);
	}

	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 57)
	public void PT245_DownloadRecords_AbleToGetToPage() throws Exception {
		purchaseAmount = CreateAudience_PurchaseRecordsPage.pricingInfo_Table.return_TotalAmount();
		purchaseTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_DownloadRecords_Page.dateFormat);
		
		CreateAudience_PurchaseRecordsPage.click_SubmitOrder_Button();
		CreateAudience_DownloadRecords_Page.verify_DownloadRecordsPage_Loads();
	}
	
	
	@Test(groups = { "all_Tests", "critical_Tests", "regression_Tests", "suppression_Tests" }, priority = 60)
	public void PT245_CreateAudience_FinalPage_ValidateCSV() throws Exception {
		CreateAudience_DownloadRecords_Page.retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage();
		
		String typeName = "CSV (Order File)";
		String fileName = "";

		orderID = CreateAudience_DownloadRecords_Page.return_OrderID();
		TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
		fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
		TableData csvTable = CreateAudience_DownloadRecords_Page
				.download_File_FromDownloadRecords(indexOfLineToDownload + 1, fileName);
		
		HashMap<String, Range<Integer>> levelRanges = new HashMap<String, Range<Integer>>();
		levelRanges.put("1", Range.between(ficoRange1, ficoRange2));
		
		CreateAudience_DownloadRecords_Page.verify_CSV_MatchesExpected(csvTable, statesAbbreviated, levelRanges);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 61)
	public void PT245_CreateAudience_FinalPage_AudienceSummary() throws Exception {

		CreateAudience_DownloadRecords_Page.verify_AudienceSummaryPanel(audienceID, audienceName, audienceMart,
				audienceAccount, marketer, review_CreationTime);
	}

	// Step 7
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 61)
	public void PT245_CreateAudience_FinalPage_OrderDetails() throws Exception {
		CreateAudience_DownloadRecords_Page.verify_OrderDetailsPanel(poNumber, purchaseTime, expectedTotalAsString,
				purchaseAmount, false);
	}

	// Step 8
	@Test(groups = { "all_Tests", "critical_Tests" }, priority = 62)
	public void PT245_CreateAudience_FinalPage_Status_Updated_ToShipped_AfterRefresh() throws Exception {
		CreateAudience_DownloadRecords_Page.refreshPage();
		CreateAudience_DownloadRecords_Page.verify_OrderDetailsPanel(poNumber, purchaseTime, expectedTotalAsString,
				purchaseAmount, true);
	}

	// Step
	@Test(groups = { "all_Tests", "critical_Tests", "not_DEV" }, priority = 65)
	public void PT269_OrderFileFTP_FirstPartyDataSource_Delivered() throws Exception {
		String firstPartyDataFileText = "QA CSV with Supplier (Order File)";

		CreateAudience_DownloadRecords_Page.refresh_Page_Until_FileDelivered(firstPartyDataFileText);
		TableData filesTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
		firstPartyDataFileName = filesTable.return_Row_BasedOn_1MatchingField("Type", firstPartyDataFileText)
				.get("Files").split("\\(")[0].trim();

		CreateAudience_DownloadRecords_Page.verify_FileDelivered(filesTable, firstPartyDataFileText);

	}

	// Step 3
	@Test(groups = { "all_Tests", "critical_Tests", "not_DEV" }, priority = 66)
	public void PT269_OrderFileFTP_Verify_CSVFile() throws Exception {
		String fileLocationAndName = "\\\\" + GlobalVariables.environment_Abbreviation
				+ "ftp01\\ftp\\Users\\test_internal\\" + firstPartyDataFileName;
		
		String[] suppliers = { "Equifax" };
		HashMap<String, Range<Integer>> levelRanges = new HashMap<String, Range<Integer>>();
		levelRanges.put("1", Range.between(ficoRange1, ficoRange2));

		TableData ftpFileData = DataBuilder.returnTableData_ForComparison(fileLocationAndName, ",", false);
		FileOperations.delete_SpecificFile(fileLocationAndName);

		CreateAudience_DownloadRecords_Page.verify_FirstPartyData_CSV(ftpFileData, statesAbbreviated, suppliers, levelRanges);
		ftpFileData.verify_TableSize(expectedTotal);
	}

	// Step 9
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 69)
	public void PT245_Verify_DBRecord_ForEquifaxDataPurchase() throws Exception {
		SuppressionCompleted.wait_ForDB_ToContain_Suppression(GlobalVariables.suppression_Mart2, account_DBName,
				orderID);
		CreateAudience_DownloadRecords_Page.verify_Equifax_DB_PurchaseOrderRecord_Created(orderID, expectedTotal);

	}

	// Step 5
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 70)
	public void PT246_SuppressionPage_AbleToFindOrderID() throws Exception {

		Navigation.navigate_Suppressions();
		Suppressions.quickSearchBox.search_WithCriteria(orderID);
		Suppressions.verify_Suppression1Entry_Correct(orderID, poNumber, audienceAccount, marketer, audienceMart,
				suppressionType, expectedTotal, purchaseTime);
	}

	// Step 6
	@Test(groups = { "all_Tests", "regression_Tests", "suppression_Tests" }, priority = 75)
	public void PT246_SuppressionDisplay_VerifyData() throws Exception {
		Suppressions.suppressionTable.click_View_ForRow(1);
		SuppressionDisplay.verify_DetailsPanel(orderID, suppressionType, poNumber, marketer, GlobalVariables.default_UserDesignation, expectedTotal,
				audienceMart);
	}

	// Step 7
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT246_Verify_Mart1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart1,
				account_DBName, orderID, expectedTotal);
	}

	// Step 7
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT246_Verify_Mart2_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Mart2,
				account_DBName, orderID, expectedTotal);
	}

	// Step 7
	@Test(priority = 80, groups = { "all_Tests", "regression_Tests", "suppression_Tests" })
	public void PT246_Verify_Ful1_Database() throws Exception {
		SuppressionCompleted.verify_Suppression_DatabaseTable_LooksAcceptable(GlobalVariables.suppression_Ful1,
				account_DBName, orderID, expectedTotal);
	}

	// Step 3
	@Test(priority = 90, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT258_Activities_RebuildAudience_Validate_SearchReview_GeneralInfoPanel() throws Exception {
		Navigation.navigate_Activities();

		TableData activityData = Activities.allAudiences_ActivitiesTable.return_TableData();
		HashMap<String, String> rowWeWant = activityData.return_Row_BasedOn_1MatchingField("Name", audienceName);

		Activities.allAudiences_SearchBox.search_WithCriteria(rowWeWant.get("Id"));
		recreateTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, CreateAudience_SearchReview.dateFormat);
		Activities.allAudiences_ActivitiesTable.click_RecreateAudience_ForRow(1);
		// Direct Mail was added later on during last run.
		expectedGeneralInfo.put("Usage Types", "Direct Mail");
		CreateAudience_SearchReview.generalInformation_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedGeneralInfo);

	}

	// Step 4
	@Test(priority = 91, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT258_SearchReview_GlobalGeographyPanel() throws Exception {
		CreateAudience_SearchReview.globalGeography_Panel
				.verify_ExpectedData_Matches_ReturnedData(expectedGlobalGeography);
	}

	// Step 4
	@Test(priority = 91, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT258_SearchReview_Level1Panel() throws Exception {
		CreateAudience_SearchReview.level1_Panel.verify_ExpectedData_Matches_ReturnedData(expectedFilterValues);
	}

	// Step 4
	@Test(priority = 91, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT258_SearchReview_SuppressionsPanel() throws Exception {
		CreateAudience_SearchReview.suppressions_Panel.verify_ExpectedData_Matches_ReturnedData(expectedSuppressions);
	}

	// Step 4
	@Test(priority = 91, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT258_SearchReview_MainContentPanel() throws Exception {
		CreateAudience_SearchReview.verify_MainContent(audienceMart, marketer, recreateTime, numberOfLevels);
	}

	// Step 5
	@Test(priority = 100, groups = { "all_Tests", "critical_Tests", "suppression_Tests" })
	public void PT258_SearchOrderRecords_ValidateMainContentPanel() throws Exception {
		CreateAudience_SearchReview.click_Top_BuildAudience_Button();
		CreateAudience_SearchOrderRecords.verify_mainContentPanel(audienceMart, marketer, recreateTime,
				expectedTotalRecords_Minimum, 0);
		// Check record count.
		CreateAudience_SearchOrderRecords.verify_AudienceSummary_TotalRecords(expectedTotal);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}

	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		Browser.closeBrowser();

	}

}// End of Class
