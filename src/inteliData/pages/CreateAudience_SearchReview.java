package inteliData.pages;

import java.util.HashMap;

import inteliData.data.GlobalVariables;

import inteliData.page_controls.DataPanelBase;
import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Table_WithFooter;
import utils.BaseUI;
import utils.Locator;

//formerly known as CreateAudience_ReviewAndRun
public class CreateAudience_SearchReview {

    // This table specific to Append
    public static Table_WithFooter matchReport = new Table_WithFooter("createAud_GlobalCrit_MatchReport_TableBody",
            "createAud_GlobalCrit_MatchReport_TableHeaders", "createAud_GlobalCrit_MatchReport_TableFooters");

    // "12/4/2017 12:08:05 PM"
    public static String dateFormat = "M/d/yyyy h:mm:ss a";
    public static DataPanelBase generalInformation_Panel = new DataPanelBase("General Information");
    public static DataPanelBase globalGeography_Panel = new DataPanelBase("Global Geography");
    public static DataPanelBase suppressions_Panel = new DataPanelBase("Suppressions");
    public static DataPanelBase level1_Panel = new DataPanelBase("Level 1 :");
    public static DataPanelBase level2_Panel = new DataPanelBase("Level 2 :");
    public static DataPanelBase level3_Panel = new DataPanelBase("Level 3 :");
    public static DataPanelBase globalRejects_Panel = new DataPanelBase("Global Rejects");
    public static DataPanel_MainContent mainContent_Panel = new DataPanel_MainContent("Build Summary");

    public static String allStatesString = "ALABAMA , ALASKA , ARIZONA , ARKANSAS , CALIFORNIA , COLORADO , CONNECTICUT , DELAWARE , DISTRICT OF COLUMBIA , FLORIDA , GEORGIA , HAWAII , IDAHO , ILLINOIS , INDIANA , IOWA , KANSAS , KENTUCKY , LOUISIANA , MAINE , MARYLAND , MASSACHUSETTS , MICHIGAN , MINNESOTA , MISSISSIPPI , MISSOURI , MONTANA , NEBRASKA , NEVADA , NEW HAMPSHIRE , NEW JERSEY , NEW MEXICO , NEW YORK , NORTH CAROLINA , NORTH DAKOTA , OHIO , OKLAHOMA , OREGON , PENNSYLVANIA , PUERTO RICO , RHODE ISLAND , SOUTH CAROLINA , SOUTH DAKOTA , TENNESSEE , TEXAS , UTAH , VERMONT , VIRGINIA , WASHINGTON , WEST VIRGINIA , WISCONSIN , WYOMING";

    public static void click_EditGlobal() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_ReviewAndRun_EditGlobal_Link"));
        CreateAudience_GlobalCriteria.wait_For_PageToLoad();
    }

    public static void click_EditLevels() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_ReviewAndRun_EditLevels_Link"));
        BaseUI.waitForElementToBeDisplayed("createAud_DefineLevs_LvlList_ContinueButton", null, null);
        Thread.sleep(500);
    }

    public static void verify_Level1PanelHeader(String levelName) {
        String expectedText = "Level 1 : " + levelName;
        level1_Panel.verify_HeaderText_Matches(expectedText);
    }

    public static void wait_ForSearchReviewPage_ToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("createAud_ReviewAndRun_Top_BuildAudience_Button", null, null);
        Thread.sleep(500);
    }

    public static String returnArray_AsString_ForPanels(String[] arrayToAppend, boolean removeCommas) {
        String outputString = "";
        for (int i = 0; i < arrayToAppend.length; i++) {
            if (i > 0) {
                outputString += " , ";
            }
            String valueToAppend = arrayToAppend[i].toUpperCase();
            if (removeCommas) {
                valueToAppend = valueToAppend.replace(",", "");
            }
            outputString += valueToAppend;
        }
        return outputString;
    }

    public static String returnArray_AsString_ForPanels(String[] arrayToAppend) {
        return returnArray_AsString_ForPanels(arrayToAppend, false);
    }

    // Only for Append
    // Assumes level text is same for all suppliers. Same validation as on Global
    // Criteria page so just did a pass through.
    public static void verify_MatchReport(String[] suppliers, int expectedImport, int expectedDistinct) {
        CreateAudience_GlobalCriteria.verify_MatchReport(suppliers, expectedImport, expectedDistinct);
    }

    public static void click_Top_BuildAudience_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_ReviewAndRun_Top_BuildAudience_Button"));
        //Wait timeout for //table[@id='list']//tbody/tr[1]/td[2] (no results table on page) likely due to connection string issue with the "IE" connection string
        CreateAudience_SearchOrderRecords.wait_ForPage_ToLoad();

    }

    // validation checks that we just have suppression level and suppress by order.
    // May need different validation if other suppressions are used.
    public static void verify_SuppressionsPanel(String suppressionLevel, String suppressByOrder) {
        HashMap<String, String> suppressionPanel_Data = suppressions_Panel.return_PanelData();

        BaseUI.verify_true_AndLog(suppressionPanel_Data.keySet().size() == 2, "Found correct number of suppressions.",
                "Found more suppressions than the expected count of 2.");

        BaseUI.baseStringCompare("Suppression Level", suppressionLevel, suppressionPanel_Data.get("Suppression level"));
        BaseUI.baseStringCompare("Suppress by order", suppressByOrder, suppressionPanel_Data.get("Suppress by order"));
    }

    public static void verify_SuppressionsPanel(HashMap<String, String> expectedSuppressions) {

        suppressions_Panel.verify_ExpectedData_Matches_ReturnedData(expectedSuppressions);

    }

    public static void verify_MainContent(String mart, String marketer, String createdTime, Integer levelCount)
            throws Exception {
        HashMap<String, String> mainContent_Data = mainContent_Panel.return_PanelData();

        BaseUI.baseStringCompare("Mart", mart, mainContent_Data.get("Mart"));
        BaseUI.baseStringCompare("Marketer", marketer, mainContent_Data.get("Marketer"));

        String createdTime_range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, -2);
        String createdTime_range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, 2);
        if(GlobalVariables.run_DateTimeChecks_Dev) {
            BaseUI.verify_Date_IsBetween_DateRange(createdTime_range1, createdTime_range2, mainContent_Data.get("Created"),
                    dateFormat);
        }
        BaseUI.baseStringCompare("Levels", levelCount.toString() + " levels", mainContent_Data.get("Levels"));
    }

}// End of Class
