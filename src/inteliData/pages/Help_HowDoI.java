package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Help_HowDoI {

	public static void verify_PageLoaded() {
		Help_Navigation.verify_Title_Appears();
		Help_Navigation.verify_SearchBox_Appears();
		
		WebElement whatsNewHeader = Locator.lookupElement("help_HowDoI_HowDoI_Title");
		BaseUI.verifyElementAppears(whatsNewHeader);
		BaseUI.verifyElementHasExpectedText(whatsNewHeader, "How do I...");
	}
	
}//End of Class
