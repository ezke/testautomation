package inteliData.pages;

import java.time.Duration;
import java.util.concurrent.Callable;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.TableBase;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class SearchDisplay {

    public static final TableBase orders_Table = new TableBase("SearchOrders");

    // Determine if we're at the Search Display page
    public static boolean atSearchDisplayPage() {
        boolean searchDisplayActive = Browser.driver.getCurrentUrl().contains("/Search/Display/") ? true : false;
        return searchDisplayActive;
    }

    public static String return_OrderId_ForFirstRow() {
        TableData orderTableData = orders_Table.return_TableData();

        return orderTableData.data.get(0).get("Id");
    }

    public static void refreshUntil_AtLeast1RowPresent() throws Exception {
        String textToInject = "Automation - refreshing page";
        if (orders_Table.tableWasEmpty()) {
            BaseUI.wait_ForCondition_ToBeMet(() -> {
                BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("nav_TopElement"), textToInject);
                Browser.driver.navigate().refresh();
                BaseUI.waitForElementToNOTContain_PartialAttributeMatch("nav_TopElement", null, null, "innerText",
                        textToInject, 20);
                BaseUI.waitForElementToBeDisplayed("progDisplay_ClickView_ElementToWaitFor", null, null, 45);
                wait_ForSearchOrdersTableToLoad();
                return orders_Table.return_TableData().data.size() > 0;
            }, Duration.ofMinutes(10), Duration.ofMinutes(1));
        }

    }


    public static void wait_ForSearchOrdersTableToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("table_tableBody_ByTableID", "SearchOrders", null);
        Thread.sleep(500);
    }

}
