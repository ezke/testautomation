package inteliData.pages;

import inteliData.page_controls.*;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;


public class CreateAudience_DefineLevels {

	public static final Expand_Section_LeftPanel geographySelections = new Expand_Section_LeftPanel("Geography Selections");
	public static final ZipCodeRadiusSelector zip_RadiusSelector = new ZipCodeRadiusSelector();

	public static final TextBox_WithLinkWrapper include_ZipCodes_TextBox =
			TextBox_WithLinkWrapper.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_ZipCode_SearchTextBox",
					"createAud_GlobalCrit_Include_ZipCode_ListOfEntries");


	public static final TextBox_WithLinkWrapper include_AreaCodes_TextBox =
			TextBox_WithLinkWrapper.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_AreaCode_SearchTextBox",
					"createAud_GlobalCrit_Include_AreaCode_ListOfEntries");

	public static final TextBox_WithLinkWrapper_WithDropdownTypeDetection include_County_TextBox =
			TextBox_WithLinkWrapper_WithDropdownTypeDetection.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_County_SearchTextBox",
					"createAud_GlobalCrit_Include_County_ListOfEntries");

	public static final ZipCodes_ChooseFile zipCodes_ChooseFile = new ZipCodes_ChooseFile("createAud_GlobalCrit_ZipCode_ChooseFileButton");

	public static final RadioSelector phoneOptionSelector = new RadioSelector("Phone option");

	public enum phone_options{
		NOT_SELECTED("Not Selected"), ALL_PHONES("100% phones"), NO_PHONES("No phones");
		private String value;

		phone_options(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}


	public static final TextBox_WithLinkWrapper include_DirectMarketingAreas_TextBox =
			TextBox_WithLinkWrapper.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_DirectMarketingAreas_SearchTextBox",
					"createAud_GlobalCrit_Include_DirectMarketingAreas_ListOfEntries");

	public static final TextBox_WithLinkWrapper_WithDropdownTypeDetection include_Cities_TextBox =
			TextBox_WithLinkWrapper_WithDropdownTypeDetection.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_City_SearchTextBox",
					"createAud_GlobalCrit_Include_City_ListOfEntries");


	public static CheckboxBase useGlobalGeography_Checkbox = new CheckboxBase("Use Global Geography") {
		@Override
		public WebElement elementTo_SeeIfChecked() {
			return Locator.lookupElement("createAud_DefineLevs_Checkbox_ElementToCheck", label_text, null);
		}
		
	};
	
	
	public static StateSelector stateSelector = new StateSelector("SelectedStates");
	
	/// Methods for initial Level Name entry
	//
	//
	public static void enter_Level(String levelName) throws Exception {
		BaseUI.enterText(Locator.lookupElement("createAud_DefineLevs_Name_TextBox"), levelName);
		Thread.sleep(200);
	}
	
	public static void wait_ForPage_ToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("createAud_DefineLevs_AddALevel_Header", null, null);
		Thread.sleep(500);
	}
	
	public static void click_InitialLevelNameEntry_SaveAndContinue() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_DefineLevs_SaveAndContinue_Button"));
		BaseUI.waitForElementToBeDisplayed("createAud_DefineLevs_BaseLineSelects_Header", null, null);
		Thread.sleep(500);
	}

	// Methods for the Main Page
	//
	//

	public static void select_FICO_DropdownOption(String optionToPick){
		BaseUI.selectValueFromDropDown(Locator.lookupElement("createAud_DefineLevs_FicoScore_Dropdown"), optionToPick);
	}

	public static void enter_FICO_Score(Integer range1, Integer range2) throws Exception {
		BaseUI.scroll_to_element(Locator.lookupElement("createAud_DefineLevs_FicoScore_Label"));

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("createAud_DefineLevs_FicoScore_Range1_TextInput"),
				range1.toString());
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("createAud_DefineLevs_FicoScore_Range2_TextInput"),
				range2.toString());
	}
	
	

	public static void verify_FICO_Score(Integer range1, Integer range2) throws Exception {
		String fico1ActualText = BaseUI.getTextFromInputBox(Locator.lookupElement("createAud_DefineLevs_FicoScore_Range1_TextInput"));
		String fico2ActualText = BaseUI.getTextFromInputBox(Locator.lookupElement("createAud_DefineLevs_FicoScore_Range2_TextInput"));
		
		BaseUI.baseStringCompare("FICO 1", range1.toString(), fico1ActualText);
		BaseUI.baseStringCompare("FICO 2", range2.toString(), fico2ActualText);
	}

	public static void click_SaveLevel_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_DefineLevs_SaveLevel_Button"));
		BaseUI.waitForElementToBeDisplayed("createAud_DefineLevs_LvlList_ContinueButton", null, null);
		Thread.sleep(500);
	}

}// End of Class
