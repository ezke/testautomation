package inteliData.pages;

import java.util.HashMap;

import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.TableBase;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

//Currently using for both Data Source, Suppressions, and Append Confirm pages.

public class ConfirmMappedFields {

	public static TableBase mappingTable = new TableBase("confirmMap_TableBody", "confirmMap_TableHeaders");

	public static DataPanel_MainContent detailsPanel = new DataPanel_MainContent("Details");

	public static String details_Date_Format = "M/d/yyyy";

	public static void wait_For_PageToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("confirmMap_ElementToWaitForToAppear", null, null);
		Thread.sleep(1000);
	}

	// Use date format details_Date_Format
	public static void verify_DetailsPanel_MatchesExpected(String fileName, Integer inputRecordCount, String createdBy,
			String date) {
		HashMap<String, String> confirmMappings_DetailsPanel = detailsPanel.return_PanelData();

		BaseUI.baseStringCompare("Name", fileName, confirmMappings_DetailsPanel.get("Name"));
		BaseUI.baseStringCompare("Input Record Count", inputRecordCount.toString(),
				confirmMappings_DetailsPanel.get("Input Record Count").replace(",", ""));
		BaseUI.baseStringCompare("Created By", createdBy, confirmMappings_DetailsPanel.get("Created By"));
		BaseUI.baseStringCompare("Date", date, confirmMappings_DetailsPanel.get("Date"));
	}

	// Use date format details_Date_Format
	public static void verify_DetailsPanel_ForNetdowns_MatchesExpected(String orderID, Integer inputRecordCount,
			String createdBy, String date) {
		HashMap<String, String> confirmMappings_DetailsPanel = detailsPanel.return_PanelData();

		BaseUI.baseStringCompare("Name", "Netdown Import File: " + orderID, confirmMappings_DetailsPanel.get("Name"));
		BaseUI.baseStringCompare("Input Record Count", inputRecordCount.toString(),
				confirmMappings_DetailsPanel.get("Input Record Count").replace(",", ""));
		BaseUI.baseStringCompare("Created By", createdBy, confirmMappings_DetailsPanel.get("Created By"));
		BaseUI.baseStringCompare("Date", date, confirmMappings_DetailsPanel.get("Date"));
	}

	public static void verify_DetailsPanel_ForFirstPartyData_MatchesExpected(String id, String name, String sourceFile,
			int quantityRecords, String marketer) {
		HashMap<String, String> confirmMappings_DetailsPanel = detailsPanel.return_PanelData();

		BaseUI.baseStringCompare("Id", id, confirmMappings_DetailsPanel.get("Id"));
		BaseUI.baseStringCompare("Name", name, confirmMappings_DetailsPanel.get("Name"));
		BaseUI.baseStringCompare("Source File", sourceFile, confirmMappings_DetailsPanel.get("Source File"));
		BaseUI.baseStringCompare("Quantity Records Uploaded", String.valueOf(quantityRecords),
				confirmMappings_DetailsPanel.get("Quantity Records Uploaded"));
		BaseUI.baseStringCompare("Marketer", marketer,
				confirmMappings_DetailsPanel.get("Marketer"));
		BaseUI.baseStringCompare("Status", "Imported", confirmMappings_DetailsPanel.get("Status"));
	}

	public static void click_ConfirmButton() throws Exception {
		BaseUI.click(Locator.lookupElement("confirmMap_ConfirmButton"));
		BaseUI.waitForElementToBeDisplayed("confirmMap_ConfirmButton_ElementToWaitFor", null, null, 600);
		Thread.sleep(500);
	}

//	public static void click_ConfirmButton_ForSuppressions() throws Exception {
//		BaseUI.click(Locator.lookupElement("confirmMap_ConfirmButton"));
//		BaseUI.waitForElementToBeDisplayed("suppressionCompleted_SuppressionCompleted_Text", null, null, 300);
//		Thread.sleep(500);
//	}
//
//	public static void click_ConfirmButton_ForDataSource() throws Exception {
//		BaseUI.click(Locator.lookupElement("confirmMap_ConfirmButton"));
//		BaseUI.waitForElementToBeDisplayed("dataSourceDisplay_ProgramsTitle", null, null, 300);
//		Thread.sleep(500);
//	}
//
//	public static void click_ConfirmButton_ForAppend() throws Exception {
//		BaseUI.click(Locator.lookupElement("confirmMap_ConfirmButton"));
//		BaseUI.waitForElementToBeDisplayed("createAud_GlobalCrit_ActiveHeader", null, null, 300);
//		Thread.sleep(500);
//	}
//
//	public static void click_ConfirmButton_ForNetdown() throws Exception {
//		BaseUI.click(Locator.lookupElement("confirmMap_ConfirmButton"));
//		BaseUI.waitForElementToBeDisplayed("netdownSuppress_Header", null, null, 300);
//		Thread.sleep(500);
//	}

	public static void verify_Mappings(HashMap<String, String> valuesToCheck) {
		TableData tableMappings = mappingTable.return_TableData();
		verify_Mappings(tableMappings, valuesToCheck);
	}

	// valuesToCheck has the Key set to Source and the Value set to Destination
	public static void verify_Mappings(TableData mappingTableData, HashMap<String, String> valuesToCheck) {
		for (String source : valuesToCheck.keySet()) {
			HashMap<String, String> mappingRow = mappingTableData.return_Row_BasedOn_2MatchingFields("Source", source, "Destination",
					valuesToCheck.get(source));
			BaseUI.verify_true_AndLog(mappingRow != null,
					"Found row matching source of " + source + " with Destination of " + valuesToCheck.get(source),
					"Did NOT find row matching source of " + source + " with Destination of "
							+ valuesToCheck.get(source));
		}

	}

}// End of Class
