package inteliData.pages;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.SearchBox;
import inteliData.page_controls.Table_SuppressionsPage;
import utils.BaseUI;
import utils.Locator;

public class Administration_Automations {

	public static final Table_SuppressionsPage automationsTable = new Table_SuppressionsPage("Automations") {
		@Override
		public void wait_ForView_PageToLoad() throws Exception {
			BaseUI.waitForElementToBeDisplayed("suppressionDisplay_IdCell", null, null, 20);
			BaseUI.waitForElementToBeDisplayed("automationDisplay_FileHistoryTable_FirstCell", null, null, Duration.ofSeconds(20));
			Thread.sleep(500);
		}
	};

	public static final SearchBox quickSearchBox = new SearchBox("AutomationsSearchBox");

	public static String dateFormat = "M/d/yyyy h:mm:ss a";

	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Automations");
		BaseUI.verifyElementAppears(Locator.lookupElement("administration_Automations_FirstRow_FirstCell"));
	}

	

}// End of Class
