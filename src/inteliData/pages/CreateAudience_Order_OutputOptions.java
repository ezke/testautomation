package inteliData.pages;

import java.util.HashMap;

import inteliData.data.GlobalVariables;
import inteliData.modals.AdditionalOptions_Modal;
import org.openqa.selenium.WebElement;

import inteliData.page_controls.CheckboxBase;
import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Table_OrderDetailsFinal;
import utils.BaseUI;
import utils.Locator;

public class CreateAudience_Order_OutputOptions {

	public static String dateFormat = "M/d/yyyy h:mm:ss a";
	public static DataPanel_MainContent mainContent_Panel = new DataPanel_MainContent("Audience Summary");
	public static Table_OrderDetailsFinal standard_Table = new Table_OrderDetailsFinal("standardFileFormats");
	public static Table_OrderDetailsFinal netdown_Table = new Table_OrderDetailsFinal("netdownFileFormats");

	public static AdditionalOptions_Modal additionalOptionsModal = new AdditionalOptions_Modal();

	public static CheckboxBase netdown_CreateSuppressionCheckbox = new CheckboxBase() {

		@Override
		public WebElement elementTo_SeeIfChecked() {
			return Locator.lookupElement("createAud_OrderDetailsFinal_Netdown_CreateSuppression_Checkbox_ClickElement",
					label_text, null);
		}

		@Override
		public WebElement clickElement() {
			return Locator.lookupElement("createAud_OrderDetailsFinal_Netdown_CreateSuppression_Checkbox_ClickElement",
					label_text, null);
		}
	};

	public static void click_FICO_ScoreLabel() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_OrderDetailsFinal_FICO_Score_Label"));
		Thread.sleep(200);

	}

	public static void enterText_IntoPONumberTextBox(String textToEnter) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("createAud_OrderDetailsFinal_PONumber_Textbox"),
				textToEnter);
	}

	public static void click_PlaceOrder() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_OrderDetailsFinal_PlaceOrderButton"));
		try {
			BaseUI.waitForElementToBeDisplayed("createAud_PurcRec_CreditCard_Icon", null, null, 300);
			BaseUI.waitForElementToBeDisplayed("tableFootAndCheck_footerCells", "list", null, 20);
			Thread.sleep(1500);
		} catch (org.openqa.selenium.TimeoutException e) {
			String logOutput = BaseUI.return_Browser_Output();
			BaseUI.log_AndFail("Timed out waiting for Download Records page to load. \nError: " + e.getMessage() + "\n"
					+ logOutput);
		}

	}

	public static void click_PostNetdown_Yes() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_OrderDetailsFinal_PostNetdown_YesButton"));
		BaseUI.waitForElementToContain_AttributeMatch("createAud_OrderDetailsFinal_PostNetdown_YesButton", null, null,
				"class", "btn btn-default active", 3);
		Thread.sleep(200);
	}

	// Not there when doing Netdown Postback
	public static void verify_PostNetdown_Yes_NotDisplayed() {
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("createAud_OrderDetailsFinal_PostNetdown_YesButton"));
	}

	public static void verify_mainContentPanel(Integer audienceID, String mart, String marketer, String createdTime,
			Integer totalRecords, Integer selectedRecords) throws Exception {
		HashMap<String, String> mainContent_Data = mainContent_Panel.return_PanelData();

		// Valid Audience ID
		Integer audience_ID = Integer.parseInt(mainContent_Data.get("Audience Id"));
		BaseUI.verify_true_AndLog(audience_ID.equals(audienceID), "Audience ID matched " + audienceID.toString(),
				"Expected Audience ID of " + audienceID.toString() + " but seeing "
						+ mainContent_Data.get("Audience Id"));

		// Mart matches
		BaseUI.baseStringCompare("Mart", mart, mainContent_Data.get("Mart"));

		// Marketer matches
		BaseUI.baseStringCompare("Marketer", marketer, mainContent_Data.get("Marketer"));

		// Created within an acceptable range from expected Time.
		String createdTime_range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
				dateFormat, -2);
		String createdTime_range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
				dateFormat, 2);
		if(GlobalVariables.run_DateTimeChecks_Dev) {
			BaseUI.verify_Date_IsBetween_DateRange(createdTime_range1, createdTime_range2, mainContent_Data.get("Created"),
					dateFormat);
		}

		// Total Records count matches.
		Integer totalRecords_Actual = Integer.parseInt(mainContent_Data.get("Total Records").replace(",", ""));
		BaseUI.verify_true_AndLog(totalRecords.equals(totalRecords_Actual),
				"Total Records matched " + totalRecords.toString(), "Expected Total Records of "
						+ totalRecords.toString() + " but seeing " + totalRecords_Actual.toString());

		// Selected Records = expectedAmount
		String selectedRecords_Actual = mainContent_Data.get("Selected Records").replace(",", "");
		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(selectedRecords_Actual), "Selected Records was an Integer.",
				"Seected Records was not an Integer.");
		BaseUI.baseStringCompare("Selected Records", selectedRecords.toString(), selectedRecords_Actual);

		// Status = complete
		BaseUI.baseStringCompare("Status", "Complete", mainContent_Data.get("Status"));
	}

	public static void click_AddAdditionalOutputs() throws Exception{
		BaseUI.click(Locator.lookupElement("createAud_OrderDetailsFinal_AdditionalOptionsButton"));
		additionalOptionsModal.waitForPageToLoad();
	}

}// End of Class
