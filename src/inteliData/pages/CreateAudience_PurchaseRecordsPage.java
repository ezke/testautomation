package inteliData.pages;

import java.util.HashMap;

import inteliData.data.GlobalVariables;
import org.openqa.selenium.WebElement;

import inteliData.page_controls.DataPanelBase;
import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.DataPanel_NoCellHeaders;
import inteliData.page_controls.TableBase;
import inteliData.page_controls.Table_PurchaseRecords_PricingInfo;
import inteliData.page_controls.Table_WithFooter;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class CreateAudience_PurchaseRecordsPage {

    public static String dateFormat = "M/d/yyyy h:mm:ss a";
    public static DataPanel_MainContent audienceSummary_Panel = new DataPanel_MainContent("Audience Summary");
    public static DataPanelBase generalInformation_Panel = new DataPanelBase("General Information");
    public static DataPanelBase level1_Panel = new DataPanelBase("Level 1 :");
    public static DataPanelBase globalGeography_Panel = new DataPanelBase("Global Geography");
    public static DataPanelBase suppressions_Panel = new DataPanelBase("Suppressions");
    public static TableBase outputFields_Table = new TableBase("OrderOutputOrderItemsGrid");
    public static TableBase orderItems_Table = new TableBase("OrderItems");
    public static Table_WithFooter results_Table = new Table_WithFooter("list");
    public static TableBase selectedSuppressions_Table = new TableBase("SuppressionSelections");
    public static TableBase excludedOrdersFromSuppressions_Table = new TableBase("SuppressionExclusions");

    public static Table_PurchaseRecords_PricingInfo pricingInfo_Table = new Table_PurchaseRecords_PricingInfo();

    public static DataPanel_NoCellHeaders fileFormatTypes_Panel = new DataPanel_NoCellHeaders("File Format Types",
            "formatType");

    public static void click_SubmitOrder_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_PurcRec_SubmitOrder_Button"));
        CreateAudience_DownloadRecords_Page.wait_ForPageToLoad();
    }

    public static void verify_TotalPrice_IsDoubleOver0(String totalPrice) {
        totalPrice = totalPrice.replace(",", "").replace("$", "").trim();
        Double totalPriceAsDouble = Double.parseDouble(totalPrice);
        BaseUI.verify_true_AndLog(totalPriceAsDouble > 0.0, "Total Price was a double over 0.0.",
                "Total Price was NOT a double over 0.0.  Seeing " + totalPrice);
    }

    public static TableData return_OutputFields_ExpectedData_ForAreaCodeTests() {
        HashMap<String, String> areaCode = new HashMap<String, String>();
        areaCode.put("Name", "AREACODE");
        areaCode.put("Caption", "Area Code");
        areaCode.put("Description", "Area Code");

        HashMap<String, String> ficoAuto = new HashMap<String, String>();
        ficoAuto.put("Name", "FICOAUTO");
        ficoAuto.put("Caption", "FICO auto score");
        ficoAuto.put("Description", "FICO AUTO is similar to FICO except it is heavily based on auto trades...");

        HashMap<String, String> phone = new HashMap<String, String>();
        phone.put("Name", "PHONE");
        phone.put("Caption", "Phone number");
        phone.put("Description", "Consumer phone number.");

        HashMap<String, String> phoneFlag = new HashMap<String, String>();
        phoneFlag.put("Name", "PHONE_FLAG");
        phoneFlag.put("Caption", "Phone option");
        phoneFlag.put("Description", "Select an option to include only records with or without phones");

        TableData expectedOutputFields = new TableData();
        expectedOutputFields.data.add(areaCode);
        expectedOutputFields.data.add(ficoAuto);
        expectedOutputFields.data.add(phone);
        expectedOutputFields.data.add(phoneFlag);

        return expectedOutputFields;
    }

    public static TableData return_OutputFields_ExpectedData_ForCountyNameTests() {
        HashMap<String, String> areaCode = new HashMap<String, String>();
        areaCode.put("Name", "COUNTY_NAME");
        areaCode.put("Caption", "County name");
        areaCode.put("Description", "County name");

        HashMap<String, String> ficoAuto = new HashMap<String, String>();
        ficoAuto.put("Name", "FICOAUTO");
        ficoAuto.put("Caption", "FICO auto score");
        ficoAuto.put("Description", "FICO AUTO is similar to FICO except it is heavily based on auto trades...");


        TableData expectedOutputFields = new TableData();
        expectedOutputFields.data.add(areaCode);
        expectedOutputFields.data.add(ficoAuto);

        return expectedOutputFields;
    }

    public static void verify_TotalPrice_Valid() {
        String totalPrice = return_TotalPrice();
        totalPrice = totalPrice.replace(",", "").replace("$", "").trim();


        if (BaseUI
                .elementAppears(Locator.lookupElement("createAud_PurcRec_PricingInfo_TableCells_ExcludingFooterRow"))) {
            TableData pricingInfo = pricingInfo_Table.return_TableData();
            //In this case the amount would be 0.00
            if (pricingInfo.data.get(0).get("Total Price").equals(pricingInfo.data.get(0).get("Amount to Apply"))) {
                Double totalPriceAsDouble = Double.parseDouble(totalPrice);
                BaseUI.verify_true_AndLog(totalPriceAsDouble == 0.0, "Total Price was a == 0.0.",
                        "Total Price was NOT a double == 0.0.  Seeing " + totalPrice);
            } else {
                verify_TotalPrice_IsDoubleOver0(totalPrice);
            }

        } else {
            verify_TotalPrice_IsDoubleOver0(totalPrice);
        }
    }

    public static void verify_FCRA_Notice_Appears() {
        WebElement noticeElement = Locator.lookupElement("createAud_PurcRec_FCRA_Alert");
        String expectedText = "Any prescreen marketing offers delivered to consumers selected in this order must follow Fair Credit Reporting Act guidelines. By submitting this order you agree to remit full and punctual payment in accordance with all Service and Pricing commitments in place with your company.";
        String actualText = BaseUI.getTextFromField(noticeElement).trim();
        BaseUI.verifyElementAppears(noticeElement);
        BaseUI.baseStringPartialCompare("FCRA Alert", expectedText, actualText);
    }

    public static String return_TotalPrice() {

        String totalPrice = "";
        if (BaseUI
                .elementAppears(Locator.lookupElement("createAud_PurcRec_PricingInfo_TableCells_ExcludingFooterRow"))) {
            totalPrice = pricingInfo_Table.return_TotalAmount();
        } else {
            totalPrice = audienceSummary_Panel.return_SpecificValue("Total Price");
        }

        return totalPrice;
    }

    public static void verify_AudienceSummaryPanel(Integer audienceID, String mart, String marketer, String createdTime,
                                                   Integer selectedRecords, String poNumber) throws Exception {
        HashMap<String, String> audienceSummary_Data = audienceSummary_Panel.return_PanelData();

        // Valid Audience ID
        Integer audience_ID = Integer.parseInt(audienceSummary_Data.get("Audience Id"));
        BaseUI.verify_true_AndLog(audience_ID.equals(audienceID), "Audience ID matched " + audienceID.toString(),
                "Expected Audience ID of " + audienceID.toString() + " but seeing "
                        + audienceSummary_Data.get("Audience Id"));

        // Mart matches
        BaseUI.baseStringCompare("Mart", mart, audienceSummary_Data.get("Mart"));

        // Marketer matches
        BaseUI.baseStringCompare("Marketer", marketer, audienceSummary_Data.get("Marketer"));

        // Created within an acceptable range from expected Time.
        String createdTime_range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, -13);
        String createdTime_range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, 2);
        if(GlobalVariables.run_DateTimeChecks_Dev) {
            BaseUI.verify_Date_IsBetween_DateRange(createdTime_range1, createdTime_range2,
                    audienceSummary_Data.get("Created"), dateFormat);
        }

        // Selected Records = expectedAmount
        String selectedRecords_Actual = audienceSummary_Data.get("Selected Records").replace(",", "");
        BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(selectedRecords_Actual), "Selected Records was an Integer.",
                "Seected Records was not an Integer.");
        BaseUI.baseStringCompare("Selected Records", selectedRecords.toString(), selectedRecords_Actual);

        // Validate PO number.
        BaseUI.baseStringCompare("PO Number", poNumber, audienceSummary_Data.get("PO Number"));

        // Status = complete
        BaseUI.baseStringCompare("Status", "Complete", audienceSummary_Data.get("Status"));

    }

    public static void verify_resultsTable_Footer(String expectedTotal, String expectedOrdered) throws Exception {
        HashMap<String, String> footerRow = results_Table.return_footerRow();

        // Verify Total matches expected
        BaseUI.baseStringCompare("Total", expectedTotal.replace(",", ""), footerRow.get("Total").replace(",", ""));

        // Verify Ordered matches expected
        BaseUI.baseStringCompare("Ordered", expectedOrdered.replace(",", ""), footerRow.get("Ordered").replace(",", ""));
    }

    // Assumes and verifies there is only one row of Data.
    public static void verify_resultsTable_1Row_OfData_MatchesExpected(Integer expectedTotal, Integer expectedOrdered) {
        TableData results_TableInfo = results_Table.return_TableData();
        HashMap<String, String> row = results_TableInfo.data.get(0);

        results_TableInfo.verify_TableSize(1);

        // Verify Level is 1. Since there's only 1 row this is hard coded to 1.
        BaseUI.baseStringCompare("Level", "1", row.get("Level"));

        // Verify Total matches expected
        BaseUI.baseStringCompare("Total", expectedTotal.toString(), row.get("Total").replace(",", ""));

        // Verify Ordered matches expected
        BaseUI.baseStringCompare("Ordered", expectedOrdered.toString(), row.get("Ordered").replace(",", ""));

        // Verify Phone is an Integer.
        BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(row.get("Phone").replace(",", "")),
                "Phone Number was an Integer.", "Phone number was NOT an Integer.");
    }

}// End of Class
