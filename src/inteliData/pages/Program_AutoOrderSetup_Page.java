package inteliData.pages;

import inteliData.page_controls.CheckboxBase;
import inteliData.page_controls.Checkbox_ClickLabel_WithAlerts;
import inteliData.page_controls.DataPanel_MainContent;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

import java.time.Duration;
import java.util.HashMap;

public class Program_AutoOrderSetup_Page {

    public static String dateFormat = "M/d/yyyy h:mm:ss a";

    public static DataPanel_MainContent programDetails_Panel = new DataPanel_MainContent("Program Details");
    public static Checkbox_ClickLabel_WithAlerts directMail_Checkbox = new Checkbox_ClickLabel_WithAlerts("Direct Mail",
            "Mail Piece for Direct Mail: Compliant.");


    public static CheckboxBase levelCheckbox_ByText(String levelText){

        CheckboxBase levelCheckbox = new CheckboxBase(){
            @Override
            public WebElement elementTo_SeeIfChecked() {
                return Locator.lookupElement("progAutoOrderSetup_LevelCheckbox_ByText", levelText, null);
            }

            @Override
            public WebElement clickElement() {
                return Locator.lookupElement("progAutoOrderSetup_LevelCheckbox_ByText", levelText, null);
            }

            @Override
            protected void click_Checkbox(WebElement elementToClick){
                try {
                    BaseUI.click_js(elementToClick);
                }catch(Exception e){
                    BaseUI.log_AndFail("Encountered error attempting to click checkbox: " + e.getMessage());
                }
            }

        };

        return levelCheckbox;
    }

    public static void waitForPageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("progAutoOrderSetup_Header", null, null, Duration.ofSeconds(40));
        Thread.sleep(500);
    }

    public static void verify_AtPage() {
        BaseUI.verifyElementAppears(Locator.lookupElement("progAutoOrderSetup_Header"));
    }

    public static void enter_PONumber(String textToEnter) {
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("progAutoOrderSetup_PONumber_TextBox"), textToEnter);
    }

    public static void click_Save() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("progAutoOrderSetup_Save_Button"));
        ProgramDisplay.wait_ForPage_ToLoad();
    }

    public static void click_SelectAllRecords() {
        click_AutomatedOrderSettings_Option("Select all records");
    }

    public static void click_ByNthing() {
        click_AutomatedOrderSettings_Option("By nthing");
        BaseUI.waitForElementToBeDisplayed("progAutoOrderSetup_NthPopulationTo_TextBox", null, null);
    }

    public static void click_SelectByLevel() {
        click_AutomatedOrderSettings_Option("Select by level");
        BaseUI.waitForElementToBeDisplayed("progAutoOrderSetup_LevelCheckbox_List", null, null);
    }

    public static void click_SelectFirstXNumberOfRecords() throws Exception {
        click_AutomatedOrderSettings_Option("Select first x number of records");
        BaseUI.waitForElementToBeDisplayed("progAutoOrderSetup_NumberOfRecords_Textbox", null, null);
        Thread.sleep(500);
    }

    public static void enterText_NumberOfRecords(String textToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("progAutoOrderSetup_NumberOfRecords_Textbox"), textToEnter);
    }

    public static void select_SortBy(String optionTextToPick){
       BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("progAutoOrderSetup_NumberOfRecords_SortBy_Dropdown"), optionTextToPick);
    }

    public static void select_SortOrder(String optionTextToPick){
        BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("progAutoOrderSetup_NumberOfRecords_SortOrder_Dropdown"), optionTextToPick);
    }



    public static void enterText_Into_Nth_Population_To_TextBox(String textToEnter){
        BaseUI.enterText_IntoInputBox(Locator.lookupRequiredElement("progAutoOrderSetup_NthPopulationTo_TextBox", null, null),
                textToEnter);
    }

    private static void click_AutomatedOrderSettings_Option(String option) {
        BaseUI.click(Locator.lookupRequiredElement("progAutoOrderSetup_AutomatedOrderSettings_LabelByText", option, null));
        BaseUI.waitForElementToContain_PartialAttributeMatch(
                "progAutoOrderSetup_AutomatedOrderSettings_LabelByText", option, null, "class", "active", 5);
    }

    //Pass Start Date in format 6/8/2018.  Method will tack on the " 12:00:00 AM"
    public static void verify_ProgramDetailsPanel(String expectedAudienceID, String expectedName, String expectedAccount,
                                                  String expectedMarketer, String expectedBuiltFor, String expectedMart, String expectedStatus,
                                                  String expectedNotificationEmaill, String expectedCreated, String expectedCreatedBy, String startDate, String expectedEndDate) throws Exception {
        HashMap<String, String> programDetails = programDetails_Panel.return_PanelData();
        BaseUI.baseStringCompare("Audience Id", expectedAudienceID, programDetails.get("Audience Id"));
        BaseUI.baseStringCompare("Name", expectedName, programDetails.get("Name"));
        BaseUI.baseStringCompare("Account", expectedAccount, programDetails.get("Account"));
        BaseUI.baseStringCompare("Marketer", expectedMarketer, programDetails.get("Marketer"));
        BaseUI.baseStringCompare("Built for", expectedBuiltFor, programDetails.get("Built for"));
        BaseUI.baseStringCompare("Mart", expectedMart, programDetails.get("Mart"));
        BaseUI.baseStringCompare("Status", expectedStatus, programDetails.get("Status"));
        BaseUI.baseStringCompare("Notification email", expectedNotificationEmaill, programDetails.get("Notification email"));
        BaseUI.verify_Date_IsAcceptable_DateRange(expectedCreated, programDetails.get("Created"),
                -10, 5, dateFormat);
        BaseUI.baseStringCompare("Created by", expectedCreatedBy, programDetails.get("Created by"));
        BaseUI.baseStringCompare("Start date", startDate + " 12:00:00 AM", programDetails.get("Start date"));
        BaseUI.baseStringCompare("End date", expectedEndDate, programDetails.get("End date"));
    }


}//End of Class
