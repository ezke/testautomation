package inteliData.pages;

import org.openqa.selenium.WebElement;

import inteliData.data.Data_Import;
import inteliData.data.GlobalVariables;
import utils.BaseUI;
import utils.Locator;

public class Account_ChangePassword {

	public static void changePassword(String password, String confirmPassword) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("changePass_PasswordTextBox"), password);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("changePass_ConfirmPasswordTextBox"), confirmPassword);
		click_SavePassword();
	}

	public static void click_SavePassword() throws Exception {
		BaseUI.click(Locator.lookupElement("changePass_SavePassword_Button"));
		BaseUI.waitForElementToBeDisplayed("changePass_ElementToWaitFor", null, null);
		Thread.sleep(500);
	}

	// A successful password change should take you to the login page.
	// Will also update passwordfile if successful.
	public static void verify_PasswordChange_Successful_AndIfSuccessful_UpdatePWFile(String user, String password)
			throws Exception {
		WebElement loginTextBox = Locator.lookupElement("login_Username");
		boolean loginUserAppears = BaseUI.elementAppears(loginTextBox);

		if (loginUserAppears) {
			Data_Import.update_PasswordFile(GlobalVariables.user_PasswordReset, password);
			BaseUI.verifyElementAppears(loginTextBox);
		} else {
			BaseUI.log_AndFail("Password update not successful.");
		}
	}

	public static void verify_PasswordChange_AlertError(PasswordChangeError error) {
		WebElement passwordChangeErrorElement = Locator.lookupElement("changePass_AlertError");
		BaseUI.verifyElementAppears(passwordChangeErrorElement);
		BaseUI.verifyElementHasExpectedPartialTextByElement(passwordChangeErrorElement, error.getValue());
	}

	public static void verify_PasswordFieldRequiredError() {
		WebElement passwordChangeErrorElement = Locator.lookupElement("changePass_PasswordError");
		BaseUI.verifyElementAppears(passwordChangeErrorElement);
		BaseUI.verifyElementHasExpectedPartialTextByElement(passwordChangeErrorElement, "The Password field is required.");
	}
	
	public static void verify_ConfirmPasswordFieldRequiredError() {
		WebElement passwordChangeErrorElement = Locator.lookupElement("changePass_ConfirmPasswordError");
		BaseUI.verifyElementAppears(passwordChangeErrorElement);
		BaseUI.verifyElementHasExpectedPartialTextByElement(passwordChangeErrorElement, "The Confirm Password field is required.");
	}
	
	
	public enum PasswordChangeError {
		CannotBeSameAsCurrent("Your new password cannot be the same as your current password."), CannotBeSameAsLast5(
				"Your new password cannot be the same as your previous five passwords."), CannotBeLessThan8Chars(
						"The password entered must be at least 8 characters long"), PasswordMustMatchConfirmPassword(
								"The password entered must match the confirm password entered"), PasswordMustContain(
										"Your password must contain at least one lower case character, one digit, one upper case character and one special character.");

		private String value;

		private PasswordChangeError(final String val) {
			value = val;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

}// End of Class
