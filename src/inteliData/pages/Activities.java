package inteliData.pages;

import java.time.Duration;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.SearchBox;
import inteliData.page_controls.TabSwitch;
import inteliData.page_controls.Table_ActivitiesPage;
import utils.BaseUI;
import utils.Locator;

public class Activities {


	public static final SearchBox allOrders_SearchBox = new SearchBox("OrderGridSearchBox");
	public static final Table_ActivitiesPage allOrders_ActivitiesTable = new Table_ActivitiesPage("OrderGrid");

	public static final SearchBox allAudiences_SearchBox = new SearchBox("SearchGridSearchBox");
	public static final Table_ActivitiesPage allAudiences_ActivitiesTable = new Table_ActivitiesPage("SearchGrid");

	public static void click_Tab_AllOrders() throws Exception {
		TabSwitch allOrdersTab = new TabSwitch("All Orders");
		allOrdersTab.switchTab();
		BaseUI.waitForElementToBeDisplayed("activities_AllOrders_Table_FirstCell", null, null, Duration.ofSeconds(30));
		//seeing a lot of animations in the table, want to give it a proper chance to load.
		Thread.sleep(3500);
	}

	public static void verify_PageLoaded() {
		WebElement pageHeader = Locator.lookupElement("activities_PageHeader");
		BaseUI.verifyElementAppears(pageHeader);
		BaseUI.verifyElementHasExpectedText(pageHeader, "Activities");
		BaseUI.verifyElementAppears(Locator.lookupElement("activities_Stable_FirstRow_Checkbox"));
	}

}// End of Class
