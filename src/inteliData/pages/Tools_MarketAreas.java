package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Tools_MarketAreas {

	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Market Areas");
		BaseUI.verifyElementAppears(Locator.lookupElement("tools_MarketAreas_table_FirstRow_FirstCell"));
	}
	
	
	
	
}//End of Class
