package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class AppendImport {

	public static void chooseFile(String fileToChoose) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("appendImport_ChooseFile_Input"), fileToChoose);
		BaseUI.waitForElementToBeDisplayed("appendMap_Header", null, null, 300);
		Thread.sleep(500);
	}

	public static void verify_TitleDisplayed() {
		WebElement pageTitle = Locator.lookupElement("appendImport_Header");
		BaseUI.verifyElementAppears(pageTitle);
		BaseUI.verifyElementHasExpectedText(pageTitle, "Append Import");
	}

}// End of Class
