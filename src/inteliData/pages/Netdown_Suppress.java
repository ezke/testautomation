package inteliData.pages;

import utils.BaseUI;
import utils.Locator;

public class Netdown_Suppress {

	public static void click_PlaceOrder() throws Exception {
		BaseUI.click(Locator.lookupElement("netdownSuppress_PlaceOrder_Button"));
		BaseUI.waitForElementToBeDisplayed("createAud_AudSum_AudienceSummaryHeader", null, null, 300);
		BaseUI.waitForElementToBeDisplayed("createAud_AndSum_SelectRecordsTable_FirstRow_SecondCell", null, null, 20);
		Thread.sleep(1500);
	}

}// End of Class
