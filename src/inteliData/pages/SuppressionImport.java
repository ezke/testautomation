package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class SuppressionImport {

	public static void verify_SuppresionImport_TitleVisible() {
		WebElement suppressionImportTitle = Locator.lookupElement("suppressionImport_Header");
		BaseUI.verifyElementAppears(suppressionImportTitle);
		BaseUI.verifyElementHasExpectedText(suppressionImportTitle, "Suppression Import");

	}

	public static void enterText_PONumber(String poNumber) {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("suppressionImport_PONumber"), poNumber);
	}

	public static void select_Account(String accountText) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("suppressionImport_AccountDropdown"), accountText);
	}

	public static void chooseFile(String fileNameAndLocation) throws Exception {

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("suppressionImport_ChooseFileButton"), fileNameAndLocation);
	//	BaseUI.waitForElementToBeDisplayed("processFile_Title", null, null);
	//	Thread.sleep(400);
		ProcessingFile.wait_ForFile_ToFinish("Suppression Map");

	}

}// End of Class
