package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class DataSourceImport {

	public static void verify_DataSourceImport_pageLoaded() {
		WebElement dataSourceImportHeader = Locator.lookupElement("dataSourceImport_Title");
		BaseUI.verifyElementAppears(dataSourceImportHeader);
		BaseUI.verifyElementHasExpectedText(dataSourceImportHeader, "Data Source Import");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataSourceImport_ChooseFileButton_VisibleELement"));
	}

	//Pass true if your path is local "Example //src//inteliData//data//somename.csv"
	public static void chooseFile(String fileName, boolean isLocal) {
		if (isLocal) {
			fileName = System.getProperty("user.dir") + fileName;
		}

		BaseUI.enterText_IntoInputBox(Locator.lookupElement("dataSourceImport_ChooseFileButton"), fileName);
		BaseUI.waitForElementToBeDisplayed("dataSourceMap_Title", null, null, 300);

	}

	public static void chooseFile(String fileName) {
		chooseFile(fileName, false);
	}

}// End of Class
