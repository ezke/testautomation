package inteliData.pages;

import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class ProcessingFile {

	public static void verify_ProcessingFile() {
		BaseUI.verifyElementAppears(Locator.lookupElement("processFile_Title"));

	}

	public static void wait_ForFile_ToFinish(String headerTextNextPage) throws Exception {
		BaseUI.waitForElementToBeDisplayed("processFile_HeaderForNextPage", headerTextNextPage, null, Duration.ofMinutes(3));
		Thread.sleep(500);
	}

}// End of Class
