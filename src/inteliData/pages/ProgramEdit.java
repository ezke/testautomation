package inteliData.pages;

import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class ProgramEdit {

    public static void waitForPageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("programEdit_DetailsHeader", null, null, Duration.ofSeconds(30));
        Thread.sleep(500);
    }

    public static void click_Save() throws Exception {
        BaseUI.click(Locator.lookupElement("programEdit_SaveButton"));
        ProgramDisplay.wait_ForPage_ToLoad();
    }

    public static void select_Status_Inactive() {
        BaseUI.selectValueFromDropDown(Locator.lookupRequiredElement("programEdit_Status_Dropdown"), "Inactive");
    }

}//End of Class
