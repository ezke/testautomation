package inteliData.pages;

import org.openqa.selenium.WebElement;

import inteliData.data.GlobalVariables;
import inteliData.modals.GenericModal;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;

public class HomePage {

	public static void wait_ForHomePageToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("homePage_BannerImage", null, null);
		Thread.sleep(500);
	}
	
	public static void verify_HomePage_Loaded() {
		verify_CreateAudience_ButtonAppears();
		verify_CreateLendingSignal_ButtonAppears();
		verify_CreateProgram_ButtonAppears();
		verify_CreateAppend_ButtonAppears();
		BaseUI.verifyElementAppears(Locator.lookupElement("homePage_LearnMore_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homePage_BannerImage"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homePage_AudienceTargetingHeader"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homePage_LendingSignalsHeader"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homePage_InTheMarketAlerts_Header"));
		BaseUI.verifyElementAppears(Locator.lookupElement("homePage_DataAppend_Header"));
	}

	public static void verify_CreateAudience_ButtonAppears() {
		WebElement createAudienceButton = Locator.lookupElement("homePage_CreateAudience_Button");
		BaseUI.verifyElementAppears(createAudienceButton);
		BaseUI.verifyElementHasExpectedText(createAudienceButton, " Create Audience");
	}

	public static void verify_CreateLendingSignal_ButtonAppears() {
		WebElement createLendingSignalButton = Locator.lookupElement("homePage_CreateLendingSignals_Button");
		BaseUI.verifyElementAppears(createLendingSignalButton);
		BaseUI.verifyElementHasExpectedText(createLendingSignalButton, " Create Lending Signal");
	}

	public static void verify_CreateProgram_ButtonAppears() {
		WebElement createProgramButton = Locator.lookupElement("homePage_CreateProgram_Button");
		BaseUI.verifyElementAppears(createProgramButton);
		BaseUI.verifyElementHasExpectedText(createProgramButton, " Create Program");
	}

	public static void verify_CreateAppend_ButtonAppears() {
		WebElement createAppendButton = Locator.lookupElement("homePage_CreateAppend_Button");
		BaseUI.verifyElementAppears(createAppendButton);
		BaseUI.verifyElementHasExpectedText(createAppendButton, " Create Append");
	}

	public static void click_CreateAudience_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("homePage_CreateAudience_Button"));
		GenericModal.wait_ForForm_ToAppear();
	}

	public static void click_CreateLendingSignals_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("homePage_CreateLendingSignals_Button"));
		GenericModal.wait_ForForm_ToAppear();
	}

	public static void click_CreateProgram_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("homePage_CreateProgram_Button"));
		GenericModal.wait_ForForm_ToAppear();
	}

	public static void click_CreateAppend_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("homePage_CreateAppend_Button"));
		GenericModal.wait_ForForm_ToAppear();
	}

	public static void click_LearnMore_Button() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("homePage_LearnMore_Button"), false, 2000);
	}

	public static void verify_LearnMore_PDF_Loaded() {
		String url = Browser.driver.getCurrentUrl();
		BaseUI.baseStringCompare("Current URL",
				GlobalVariables.url + "/Content/documents/IntelidataExpress.pdf", url);
		WebElement pdfEmbedElement = Locator.lookupElement("learnMore_PDF_Embed");
		BaseUI.verifyElementHasExpectedAttributeValue(pdfEmbedElement, "type", "application/pdf");
	}

}// end of class
