package inteliData.pages;

import java.util.HashMap;

import inteliData.data.GlobalVariables;
import org.openqa.selenium.WebElement;

import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Table_SuppressionMap;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class AppendMap {

	public static DataPanel_MainContent details_Panel = new DataPanel_MainContent("Details");
	public static String dateUploadedFormat = "M/d/yyyy h:mm:ss a";
	public static Table_SuppressionMap mapTable = Table_SuppressionMap.get_Table_SuppressionMap_byID("mappingTable");

	public static void verify_TitleDisplayed() {
		WebElement pageTitle = Locator.lookupElement("appendMap_Header");
		BaseUI.verifyElementAppears(pageTitle);
		BaseUI.verifyElementHasExpectedText(pageTitle, "Append Map");
	}

	public static void select_AppendLevel(String textToSelect) {
		BaseUI.selectValueFromDropDown(Locator.lookupElement("appendMap_AppendLevel_Dropdown"), textToSelect);
	}

	public static void verify_DetailsPanel(String name, String sourceFile, String dateUploaded, String createdBy,
			String marketer, Integer recordsInSourceFile) throws Exception {
		HashMap<String, String> details_Data = details_Panel.return_PanelData();

		BaseUI.baseStringCompare("Name", name, details_Data.get("Name"));
		BaseUI.baseStringCompare("Source File", sourceFile, details_Data.get("Source File"));

		// Validate Date Uploaded is +/- 2 minutes of what we expected.
		if(GlobalVariables.run_DateTimeChecks_Dev) {
			BaseUI.verify_Date_IsAcceptable_DateRange(dateUploaded, details_Data.get("Date Uploaded"), -2, 2,
					dateUploadedFormat);
		}

		BaseUI.baseStringCompare("Status", "Imported", details_Data.get("Status"));

		BaseUI.baseStringCompare("Created By", createdBy, details_Data.get("Created By"));
		
		BaseUI.baseStringCompare("Marketer", marketer, details_Data.get("Marketer"));
		
		BaseUI.baseStringCompare("Records In Source File", recordsInSourceFile.toString(), details_Data.get("Records In Source File"));
	}
	
	public static void verify_Mappings(TableData mappingTableData, HashMap<String, String> valuesToCheck) {
		for (String source : valuesToCheck.keySet()) {
			HashMap<String, String> mappingRow = mappingTableData.return_Row_BasedOn_2MatchingFields("Source", source, "Destination",
					valuesToCheck.get(source));
			BaseUI.verify_true_AndLog(mappingRow != null,
					"Found row matching source of " + source + " with Destination of " + valuesToCheck.get(source),
					"Did NOT find row matching source of " + source + " with Destination of " + valuesToCheck.get(source));
		}

	}
	
	public static void click_Map_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("suppressionMap_MapButton"));
		BaseUI.waitForElementToBeDisplayed("confirmMap_Header", null, null);
		Thread.sleep(500);
	}


}// End of Class
