package inteliData.pages;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.SearchBox;
import inteliData.page_controls.Table_SuppressionsPage;
import utils.BaseUI;
import utils.Locator;

public class Packages {

	public static final Table_SuppressionsPage packagesTable = new Table_SuppressionsPage("PackagesGrid") {
		@Override
		public void wait_ForView_PageToLoad() throws Exception {
			PackageDisplay.wait_ForPageToLoad();
		}	
	};
	
	public static final SearchBox quickSearchBox = new SearchBox("PackagesGridSearchBox");
	
	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Packages");
		BaseUI.verifyElementAppears(Locator.lookupElement("packages_table_FirstRow_FirstCell"));
	}
	
	
}//End of Class
