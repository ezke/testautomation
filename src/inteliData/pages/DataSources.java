package inteliData.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.Button_Dropdown;
import inteliData.page_controls.Button_Dropdown_ListItem;
import utils.BaseUI;
import utils.Locator;

public class DataSources {

	public static final ArrayList<Button_Dropdown_ListItem> creatButtonList = suppressions_CreateButton_Options();
	
	public static final Button_Dropdown createButton = new Button_Dropdown(creatButtonList, "Create");

	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Data Sources");
		BaseUI.verifyElementAppears(Locator.lookupElement("dataSources_table_MyDataSources_FirstRow_FirstCell"));
	}

	public static ArrayList<Button_Dropdown_ListItem> suppressions_CreateButton_Options() {
		ArrayList<Button_Dropdown_ListItem> dropdown_Items = new ArrayList<Button_Dropdown_ListItem>();
		Button_Dropdown_ListItem suppressionItem = new Button_Dropdown_ListItem("Data Source") {
			@Override
			public void wait_ForPageToLoad() throws Exception {
				BaseUI.waitForElementToBeDisplayed("dataSourceModal_Title", null, null,120);
				Thread.sleep(500);
			}
		};

		dropdown_Items.add(suppressionItem);
		return dropdown_Items;
	}

}// End of Class
