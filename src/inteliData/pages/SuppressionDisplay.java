package inteliData.pages;

import java.util.HashMap;

import inteliData.page_controls.DataPanel_MainContent;
import utils.BaseUI;

public class SuppressionDisplay {

	public static DataPanel_MainContent details_Panel = new DataPanel_MainContent("Details");

	public static void verify_DetailsPanel(String id, String type, String poNumber, String marketer, String createdBy,
			Integer quantity, String searchType) {
		HashMap<String, String> detailsPanel = details_Panel.return_PanelData();
		BaseUI.baseStringCompare("Id", id, detailsPanel.get("Id"));
		BaseUI.baseStringCompare("Type", type, detailsPanel.get("Type"));
		BaseUI.baseStringCompare("PO Number", poNumber, detailsPanel.get("PO Number"));
		BaseUI.baseStringCompare("Status", "Shipped", detailsPanel.get("Status"));
		BaseUI.baseStringCompare("Marketer", marketer, detailsPanel.get("Marketer"));
		BaseUI.baseStringCompare("Created By", createdBy, detailsPanel.get("Created By"));
		BaseUI.baseStringCompare("Quantity", quantity.toString(), detailsPanel.get("Quantity"));
		BaseUI.baseStringCompare("Search Type", searchType, detailsPanel.get("Search Type"));
	}

	public static void verify_DetailsPanel_SuccessfulSuppressionAutomation(String id, String automationID, String type,
			String poNumber, String fileName) throws Exception {
		HashMap<String, String> detailsPanel = details_Panel.return_PanelData();
		BaseUI.baseStringCompare("Id", id, detailsPanel.get("Id"));
		BaseUI.baseStringCompare("Automation Id", automationID, detailsPanel.get("Automation Id"));
		BaseUI.baseStringCompare("Type", type, detailsPanel.get("Type"));
		BaseUI.baseStringCompare("PO Number", poNumber, detailsPanel.get("PO Number"));
		BaseUI.baseStringCompare("Status", "Complete", detailsPanel.get("Status"));

		fileName = fileName.replaceFirst("^//", "");
		BaseUI.baseStringCompare("File Name", fileName, detailsPanel.get("File Name"));

		BaseUI.verify_true_AndLog(Integer.parseInt(detailsPanel.get("Quantity")) > 0, "Found Quantity > 0.",
				"Did NOT find Quantity > 0.");
		BaseUI.verify_true_AndLog(Integer.parseInt(detailsPanel.get("Records In Source File")) > 0,
				"Found Records In Source File > 0.", "Did NOT find Records In Source File > 0.");
		BaseUI.verify_true_AndLog(Integer.parseInt(detailsPanel.get("Verified Record Count")) > 0,
				"Found Verified Record Count > 0.", "Did NOT find Verified Record Count > 0.");

		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, Suppressions.tableDateFormat);

		BaseUI.verify_Date_IsAcceptable_DateRange(currentTime, detailsPanel.get("Date Uploaded"), -1300, 2,
				Suppressions.tableDateFormat);
	}

	
	public static void verify_DetailsPanel_SuppressionAutomation_NoRecords(String id, String automationID, String type,
			String poNumber, String fileName) throws Exception {
		HashMap<String, String> detailsPanel = details_Panel.return_PanelData();
		BaseUI.baseStringCompare("Id", id, detailsPanel.get("Id"));
		BaseUI.baseStringCompare("Automation Id", automationID, detailsPanel.get("Automation Id"));
		BaseUI.baseStringCompare("Type", type, detailsPanel.get("Type"));
		BaseUI.baseStringCompare("PO Number", poNumber, detailsPanel.get("PO Number"));
		BaseUI.baseStringCompare("Status", "No Records", detailsPanel.get("Status"));

		fileName = fileName.replaceFirst("^//", "");
		BaseUI.baseStringCompare("File Name", fileName, detailsPanel.get("File Name"));

		BaseUI.verify_true_AndLog(Integer.parseInt(detailsPanel.get("Quantity")) == 0, "Found Quantity == 0.",
				"Did NOT find Quantity == 0.");
		BaseUI.verify_true_AndLog(detailsPanel.get("Records In Source File").equals(""),
				"Records In Source File was empty.", "Records In Source File was NOT empty.");
		BaseUI.verify_true_AndLog(detailsPanel.get("Verified Record Count").equals(""),
				"Verified Record Count was empty.", "Verified Record Count was NOT empty.");
		
		BaseUI.verify_true_AndLog(detailsPanel.get("Suppression Type").equals(""),
				"Suppression Type was empty.", "Suppression Type was NOT empty.");


		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, Suppressions.tableDateFormat);

		BaseUI.verify_Date_IsAcceptable_DateRange(currentTime, detailsPanel.get("Date Uploaded"), -1300, 2,
				Suppressions.tableDateFormat);
	}
	
	public static void verify_DetailsPanel_SuppressionAutomation_NoHeaders(String id, String automationID, String type,
			String poNumber, String fileName) throws Exception {
		HashMap<String, String> detailsPanel = details_Panel.return_PanelData();
		BaseUI.baseStringCompare("Id", id, detailsPanel.get("Id"));
		BaseUI.baseStringCompare("Automation Id", automationID, detailsPanel.get("Automation Id"));
		BaseUI.baseStringCompare("Type", type, detailsPanel.get("Type"));
		BaseUI.baseStringCompare("PO Number", poNumber, detailsPanel.get("PO Number"));
		BaseUI.baseStringCompare("Status", "Fulfillment", detailsPanel.get("Status"));

		fileName = fileName.replaceFirst("^//", "");
		BaseUI.baseStringCompare("File Name", fileName, detailsPanel.get("File Name"));

		BaseUI.verify_true_AndLog(detailsPanel.get("Quantity").equals(""), "Found Quantity empty.",
				"Did NOT find Quantity empty.");
		BaseUI.verify_true_AndLog(detailsPanel.get("Records In Source File").equals(""),
				"Records In Source File was empty.", "Records In Source File was NOT empty.");
		BaseUI.verify_true_AndLog(detailsPanel.get("Verified Record Count").equals(""),
				"Verified Record Count was empty.", "Verified Record Count was NOT empty.");
		
		BaseUI.verify_true_AndLog(detailsPanel.get("Suppression Type").equals(""),
				"Suppression Type was empty.", "Suppression Type was NOT empty.");


		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, Suppressions.tableDateFormat);

		BaseUI.verify_Date_IsAcceptable_DateRange(currentTime, detailsPanel.get("Date Uploaded"), -1300, 2,
				Suppressions.tableDateFormat);
	}


	
}// End of Class
