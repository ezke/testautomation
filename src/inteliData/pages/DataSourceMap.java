package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class DataSourceMap extends SuppressionMap {

	public static void verify_DataSourceMap_Title() {
		WebElement dataSourceMapTitle = Locator.lookupElement("dataSourceMap_Title");
		BaseUI.verifyElementAppears(dataSourceMapTitle);
		BaseUI.verifyElementHasExpectedText(dataSourceMapTitle, "Datasource Map");
	}

}// End of Class
