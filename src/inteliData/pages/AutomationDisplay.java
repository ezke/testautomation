package inteliData.pages;

import java.util.HashMap;

import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.TableBase;
import utils.BaseUI;

public class AutomationDisplay {

	public static TableBase automationsTable = new TableBase("Automations");

	public static DataPanel_MainContent programDetails_Panel = new DataPanel_MainContent("Program Details");

	public static void click_ProgramDetails_ID_Link() throws Exception {
		programDetails_Panel.click_Link_ByHeader("Id");
		ProgramDisplay.wait_ForPage_ToLoad();
	}

	public static void verify_AutomationsTable_Suppression_FromPriorNight(String expectedName, String expectedOrderType,
			String expectedFileName, String expectedDate, String expectedFileStatus, String expectedOrderStatusType)
			throws Exception {
		HashMap<String, String> firstRow = automationsTable.return_TableData().data.get(0);
		
		String historyDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(firstRow.get("History Date"),
				Administration_Automations.dateFormat, "M/d/yyyy", 0);
		
		String expectedDay = BaseUI.getDate_WithFormat_X_Days_FromInitialString(expectedDate,
				Administration_Automations.dateFormat, "M/d/yyyy", 0);

		BaseUI.baseStringCompare("History Date", expectedDay, historyDate);

		BaseUI.baseStringCompare("Name", expectedName, firstRow.get("Name"));
		BaseUI.baseStringCompare("File Status", expectedFileStatus, firstRow.get("File Status"));

		BaseUI.baseStringCompare("Order Type", expectedOrderType, firstRow.get("Order Type"));

		BaseUI.baseStringCompare("Order Status Type", expectedOrderStatusType, firstRow.get("Order Status Type"));

		BaseUI.baseStringCompare("File Name", expectedFileName, firstRow.get("File Name"));
	}

	public static void verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndSuccessful(String expectedName,
			String expectedOrderType, String expectedFileName, String expectedDate) throws Exception {
		verify_AutomationsTable_Suppression_FromPriorNight(expectedName, expectedOrderType, expectedFileName,
				expectedDate, "Complete", "Complete");
	}

	public static void verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndFailNoRecords(String expectedName,
			String expectedOrderType, String expectedFileName, String expectedDate) throws Exception {
		verify_AutomationsTable_Suppression_FromPriorNight(expectedName, expectedOrderType, expectedFileName,
				expectedDate, "Failed", "No Records");
	}

	public static void verify_AutomationsTable_Suppression_1stRow_FromPriorNight_AndFailNoHeaders(String expectedName,
			String expectedOrderType, String expectedFileName, String expectedDate) throws Exception {
		verify_AutomationsTable_Suppression_FromPriorNight(expectedName, expectedOrderType, expectedFileName,
				expectedDate, "Failed", "Fulfillment");
	}

	public static void verify_AutomationsTable_Program_1stRow_FromPriorNight_AndSuccessful(String expectedName,
			String expectedDate) throws Exception {
		HashMap<String, String> firstRow = automationsTable.return_TableData().data.get(0);

		String historyDate = BaseUI.getDate_WithFormat_X_Days_FromInitialString(firstRow.get("History Date"),
				Administration_Automations.dateFormat, "M/d/yyyy", 0);
		
		String expectedDay = BaseUI.getDate_WithFormat_X_Days_FromInitialString(expectedDate,
				Administration_Automations.dateFormat, "M/d/yyyy", 0);

		BaseUI.baseStringCompare("History Date", expectedDay, historyDate);
//		BaseUI.verify_Date_IsAcceptable_DateRange(expectedDate, firstRow.get("History Date"), -720, 1080,
//				Administration_Automations.dateFormat);

		BaseUI.baseStringCompare("Name", expectedName, firstRow.get("Name"));
		BaseUI.baseStringCompare("File Status", "Appended", firstRow.get("File Status"));

		BaseUI.baseStringCompare("Program Status", "Active", firstRow.get("Program Status"));
	}

}// End of class
