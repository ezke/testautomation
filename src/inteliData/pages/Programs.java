package inteliData.pages;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.SearchBox;
import inteliData.page_controls.TabSwitch;
import inteliData.page_controls.Table_SuppressionsPage;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

import java.time.Duration;
import java.util.HashMap;

public class Programs {

    public static final Table_SuppressionsPage allAcquisitionTable = new Table_SuppressionsPage("AcquisitionGrid");

    public static final TabSwitch allAcquisitionTab = new TabSwitch("All Acquisition"){
        @Override
        public void wait_ForTabSwitch() throws Exception {
            BaseUI.waitForElementToBeDisplayed("programs_table_AllAcquisition_FirstRow_FirstCell", null, null, 50);
            Thread.sleep(2500);
        }
    };

    public static final SearchBox acquisitionQuickSearchBox = new SearchBox("AcquisitionGridSearchBox") {
        public void wait_ForResultToPopulate(String textToSearchFor) throws Exception {
            BaseUI.wait_ForCondition_ToBeMet(() -> {
                HashMap<String, String> firstRowOfAcquisitionTable = allAcquisitionTable.return_TableData().data.get(0);
                for (String key : firstRowOfAcquisitionTable.keySet()) {
                    if (firstRowOfAcquisitionTable.get(key).contains(textToSearchFor)) {
                        return true;
                    }
                }

                return false;

            }, Duration.ofSeconds(10), Duration.ofMillis(500));
        }
    };


    public static void deactivateProgram(String audienceID) throws Exception {
        Programs.allAcquisitionTab.switchTab();
        Programs.acquisitionQuickSearchBox.search_WithCriteria(audienceID);
        Programs.allAcquisitionTable.click_View_ForRow(1);
        ProgramDisplay.click_Actions_EditProgram();
        ProgramEdit.select_Status_Inactive();
        ProgramEdit.click_Save();
    }

    public static void verify_PageLoaded() {
        WebElement header = Locator.lookupElement("nav_PageHeader");
        BaseUI.verifyElementAppears(header);
        BaseUI.verifyElementHasExpectedText(header, "Programs");
        BaseUI.verifyElementAppears(Locator.lookupElement("programs_table_AllITMA_FirstRow_FirstCell"));
    }

}// End of Class
