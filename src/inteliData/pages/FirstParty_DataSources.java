package inteliData.pages;

import org.openqa.selenium.WebElement;

import inteliData.modals.GenericModal;
import utils.BaseUI;
import utils.Locator;

public class FirstParty_DataSources {

	public static void verify_firstParty_DataSources_Page_Loaded() {
		WebElement firstParty_DataSources_Header = Locator.lookupElement("firstparty_DataSources_Header");
		BaseUI.verifyElementAppears(firstParty_DataSources_Header);
		BaseUI.verifyElementHasExpectedText(firstParty_DataSources_Header, "First Party Data Sources");
		BaseUI.verifyElementAppears(Locator.lookupElement("firstparty_DataSources_Table_FirstRow_FirstCell"));
	}
	
	public static void click_CreateButton() throws Exception {
		BaseUI.click(Locator.lookupElement("firstparty_DataSources_CreateButton"));
		GenericModal.wait_ForForm_ToAppear();
	}

}// End of Class
