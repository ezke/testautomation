package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Accounting_Credits {

	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Credits");
		BaseUI.verifyElementAppears(Locator.lookupElement("accounting_Credits_table_FirstRow_FirstCell"));
	}
	
	
	
	
	
	
}//End of Class
