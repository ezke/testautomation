package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Administration_Checklist {

	public static void verify_PageLoaded() {
		WebElement marketer_header = Locator.lookupElement("administration_Checklist_MarketerHeader");
		BaseUI.verifyElementAppears(marketer_header);
		BaseUI.verifyElementHasExpectedText(marketer_header, "Marketers");
		
		WebElement checklist_header = Locator.lookupElement("administration_Checklist_ChecklistHeader");
		BaseUI.verifyElementAppears(checklist_header);
		BaseUI.verifyElementHasExpectedText(checklist_header, "Checklist");
		
		BaseUI.verifyElementAppears(Locator.lookupElement("administration_Checklist_MarketerChecklist_Dropdown"));
	}
	
	
	
	
	
	
	
}//End of Class
