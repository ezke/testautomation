package inteliData.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Table_SuppressionMap;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class FirstParty_Mappings {

	public static String dateUploaded_Format = "M/d/yyyy h:mm:ss a";

	public static Table_SuppressionMap mapTable = Table_SuppressionMap.get_Table_SuppressionMap_NoID(
			"firstpartyMappings_TableDestination_ByIndex", "firstpartyMappings_tableRows",
			"firstpartyMappings_ElementToCheck_ForCheckbox", "firstpartyMappings_ElementToClick_ForCheckbox");

	public final static DataPanel_MainContent data_Panel = new DataPanel_MainContent("Details");

	// valuesToCheck has the Key set to Source and the Value set to Destination
	public static void verify_Mappings(TableData mappingTableData, HashMap<String, String> valuesToCheck) {
		for (String source : valuesToCheck.keySet()) {
			HashMap<String, String> mappingRow = mappingTableData.return_Row_BasedOn_2MatchingFields("Source", source,
					"Destination", valuesToCheck.get(source));
			BaseUI.verify_true_AndLog(mappingRow != null,
					"Found row matching source of " + source + " with Destination of " + valuesToCheck.get(source),
					"Did NOT find row matching source of " + source + " with Destination of "
							+ valuesToCheck.get(source) + " on mapping page.");
		}

	}

	// Used for Test Cases PT311 and PT312
	public static HashMap<String, String> return_fullName_ExpectedDefaultMappings() {

		HashMap<String, String> expectedMappings = new HashMap<String, String>();
		expectedMappings = new HashMap<String, String>();
		expectedMappings.put("FullName", "Full Name");
		expectedMappings.put("FullName2", "Full Name 2");
		expectedMappings.put("Address1", "Address Line1");
		expectedMappings.put("Address2", "Address Line2");
		expectedMappings.put("CITY_X", "City");
		expectedMappings.put("STATE_X", "State");
		expectedMappings.put("Zip5", "Zip");
		expectedMappings.put("ZIP4_X", "Zip 4");
		expectedMappings.put("AccountKey", "Account Key");
		expectedMappings.put("HouseholdNumber", "Household Number");
		expectedMappings.put("AccountOpenDate", "Account Open Date");
		expectedMappings.put("AccountCloseDate", "Account Close Date");
		expectedMappings.put("LastWithdrawalDate", "Last Withdrawal Date");
		expectedMappings.put("LastDirectDepositDate", "Last Direct Deposit Date");
		expectedMappings.put("LastDirectDepositAmount", "Last Direct Deposit Amount");
		expectedMappings.put("BranchCode", "Location Code");
		expectedMappings.put("LastDirectDepositDate", "Last Direct Deposit Date");
		expectedMappings.put("RegionCode", "Region Code");
		expectedMappings.put("LendingInterestRate", "Lending Interest Rate");
		expectedMappings.put("LendingMaturityDate", "Lending Maturity Date");
		expectedMappings.put("LendingTerm", "Lending Term");
		expectedMappings.put("LendingPayment", "Lending Payment");
		expectedMappings.put("LendingPaymentSchedule", "Lending Payment Schedule");
		expectedMappings.put("MailIndicator", "Mail Indicator");
		expectedMappings.put("EmployeeIndicator", "Employee Indicator");
		expectedMappings.put("BirthDate", "Birth Date");
		expectedMappings.put("MinorIndicator", "Minor Indicator");
		expectedMappings.put("Gender", "Gender");
		expectedMappings.put("HomePhone", "");
		expectedMappings.put("Email", "Email Address");
		expectedMappings.put("EmailOptOutIndicator", "");
		expectedMappings.put("OnlineBankingDate", "Online Banking Date");
		expectedMappings.put("MobileBankingDate", "Mobile Banking Date");
		expectedMappings.put("EstatementDate", "Estatement Date");
		expectedMappings.put("DebitCardDate", "Debit Card Date");
		expectedMappings.put("BillPayDate", "Bill Pay Date");
		expectedMappings.put("DirectDepositDate", "Direct Deposit Date");
		expectedMappings.put("AverageMtdBalance", "Average MTD Balance");
		expectedMappings.put("AverageYtdBalance", "Average YTD Balance");
		expectedMappings.put("EndMonthLedgerBalance", "End Month Ledger Balance");
		expectedMappings.put("TotalAnnualProfit", "Total Annual Profit");
		expectedMappings.put("TotalDirectDepositAmount", "Total Direct Deposit Amount");
		expectedMappings.put("TotalDebitCardPurchases", "Total Debit Card Purchases");
		expectedMappings.put("OverdraftProtectionAmount", "Overdraft Protection Amount");
		expectedMappings.put("NsfAmount", "NSF Amount");
		expectedMappings.put("DebitCardInterchangeAmount", "Debit Card Interchange Amount");
		expectedMappings.put("ForeignAtmAmount", "Foreign ATM Amount");
		expectedMappings.put("DebitTransactionCount", "Debit Transaction Count");
		expectedMappings.put("OnlineBillPayCount", "Online Bill Pay Count");
		expectedMappings.put("OverdraftFeesCount", "Overdraft Fees Count");
		expectedMappings.put("NsfFeesCount", "NSF Fees Count");
		expectedMappings.put("DirectDepositsCount", "Direct Deposits Count");
		expectedMappings.put("ELotCode", "ELot Code");
		expectedMappings.put("Balance", "Balance");
		expectedMappings.put("ResponderDate", "Responder Date");
		expectedMappings.put("ApplicationDate", "Application Date");
		expectedMappings.put("LoanStatusCategory", "Loan Status Category");
		expectedMappings.put("CarrierRoute", "Carrier Route");
		expectedMappings.put("Level1", "Level 1");
		expectedMappings.put("Level2", "Level 2");
		expectedMappings.put("Level3", "Level 3");
		expectedMappings.put("PT1", "");

		return expectedMappings;

	}
	
	public static HashMap<String, String> return_CampaignFinal_ExpectedDefaultMappings() {

		HashMap<String, String> expectedMappings = new HashMap<String, String>();
		expectedMappings = new HashMap<String, String>();
		expectedMappings.put("Address1", "Address Line1");
		expectedMappings.put("Address2", "Address Line2");
		expectedMappings.put("AddressId01", "Address ID 01");
		expectedMappings.put("BranchId", "");
		expectedMappings.put("CellID", "");
		expectedMappings.put("CRRT_X", "Carrier Route");
		expectedMappings.put("CITY_X", "City");
		expectedMappings.put("ComparativeCd", "Comparative Code");
		expectedMappings.put("ControlType", "Control Type");
		expectedMappings.put("ControlInd", "Control Indicator");
		expectedMappings.put("CustInd", "");
		expectedMappings.put("DeleteInd", "Delete Indicator");
		expectedMappings.put("EPCode", "EP Code");
		expectedMappings.put("Lending_CurrentMonthlyPayment", "");
		expectedMappings.put("Lending_FICOScore", "");
		expectedMappings.put("Lending_OriginalLoanAmount", "");
		expectedMappings.put("Lending_OriginalTerm", "");
		expectedMappings.put("Lending_RemainingBalance", "");
		expectedMappings.put("Lending_RemainingTerm", "");
		expectedMappings.put("Lending_SegmentCd", "");
		expectedMappings.put("LendingOffer_CurrentMonthlyPayment", "");
		expectedMappings.put("LendingOffer_LoanAmount", "");
		expectedMappings.put("LendingOffer_NewMonthlyPayment", "");
		expectedMappings.put("LendingOffer_Rate", "");
		expectedMappings.put("LendingOffer_Savings", "");
		expectedMappings.put("LendingOffer_Term", "");
		expectedMappings.put("Matchkey", "Matchkey");
		expectedMappings.put("ModelNTile", "Model NTile");
		expectedMappings.put("ModelScore", "Model Score");
		expectedMappings.put("Name1", "Full Name");
		expectedMappings.put("Name2", "Full Name 2");
		expectedMappings.put("OrderDate", "Order Date");
		expectedMappings.put("ORDERID_X", "");
		expectedMappings.put("ORDERRECORDID_X", "");
		expectedMappings.put("ProductCode", "Product Code");
		expectedMappings.put("RmvRteInd", "");
		expectedMappings.put("SeedInd", "Seed Indicator");
		expectedMappings.put("STATE_X", "State");
		expectedMappings.put("SuprInd", "");
		expectedMappings.put("WalkSeq", "Walk Sequence");
		expectedMappings.put("ZIP4_X", "Zip 4");
		expectedMappings.put("Zip", "Zip");
		expectedMappings.put("ZipDP", "Zip Delivery Point");
		expectedMappings.put("PT1", "");
		expectedMappings.put("PT2", "");
	
		return expectedMappings;
	}
	

	public static void verify_Error_RequiredFields(String[] requiredFields) {
		String expectedErrorMessage = "The following destinations are required to be mapped: ";

		for (int i = 0; i < requiredFields.length - 1; i++) {
			expectedErrorMessage += requiredFields[i] + ", ";
		}

		expectedErrorMessage += requiredFields[requiredFields.length - 1] + ".";
		verify_ErrorMessage(expectedErrorMessage);
	}

	public static void verify_FirstNameRequired_WhenFullNameUnmapped_Error() {
		String errorMessage = "First Name is required to be mapped when the following are unmapped: Full Name.";
		verify_ErrorMessage(errorMessage);
	}

	public static void verify_LastNameRequired_WhenFullNameUnmapped_Error() {
		String errorMessage = "Last Name is required to be mapped when the following are unmapped: Full Name.";
		verify_ErrorMessage(errorMessage);
	}

	public static void verify_FullName_IsRequired_WhenFirstAndLastNames_Unmapped_Error() {
		String errorMessage = "Full Name is required to be mapped when the following are unmapped: First Name, Last Name.";
		verify_ErrorMessage(errorMessage);
	}

	public static void verify_ErrorMessage(String expectedText) {
		WebElement errorMessageElement = Locator.lookupElement("firstpartyMappings_ErrorMessage_ByText", expectedText,
				null);
		BaseUI.verifyElementAppears(errorMessageElement);
		BaseUI.verifyElementHasExpectedPartialTextByElement(errorMessageElement, expectedText);
	}

	public static void verify_detailsPanel_FirstPartyData(String name, String fileName, String marketer,
			int recordsInSourceFile) throws Exception {
		HashMap<String, String> details = data_Panel.return_PanelData();

		BaseUI.baseStringCompare("Name", name, details.get("Name"));
		BaseUI.baseStringCompare("Source File", fileName, details.get("Source File"));

		BaseUI.baseStringCompare("Quantity Records Uploaded", String.valueOf(recordsInSourceFile),
				details.get("Quantity Records Uploaded"));

		BaseUI.baseStringCompare("Marketer", marketer, details.get("Marketer"));

		BaseUI.baseStringCompare("Status", "Imported", details.get("Status"));

	}

	// pass through
	public static void click_Map_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("firstpartyMappings_MapFile_Button"));
		ConfirmMappedFields.wait_For_PageToLoad();
	}

	public static void click_MapRemainingAsPassThrough() throws Exception {
		BaseUI.click(Locator.lookupElement("dataSourceMap_MapRemainingAsPassThrough"));
		Thread.sleep(500);
	}

}// end of class
