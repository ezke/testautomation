package inteliData.pages;

import utils.Browser;
import utils.Locator;
import utils.TableData;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.Button_Dropdown;
import inteliData.page_controls.Button_Dropdown_ListItem;
import inteliData.page_controls.SearchBox;
import inteliData.page_controls.ShowEntries_Dropdown;
import inteliData.page_controls.ShowEntries_Dropdown.showEntriesOptions;
import inteliData.page_controls.Table_SuppressionsPage;
import utils.BaseUI;

public class Suppressions {

	// 2/6/2018 11:37:27 AM
	public static String tableDateFormat = "M/d/yyyy h:mm:ss a";

	public static final Table_SuppressionsPage suppressionTable = new Table_SuppressionsPage("SuppressionsGrid");

	public static final ArrayList<Button_Dropdown_ListItem> creatButtonList = suppressions_CreateButton_Options();

	public static final Button_Dropdown createButton = new Button_Dropdown(creatButtonList, "Create");

	public static final SearchBox quickSearchBox = new SearchBox("SuppressionsGridSearchBox");

	public static final ShowEntries_Dropdown showEntriesDropdown = new ShowEntries_Dropdown("SuppressionsGrid_length");

	public static ArrayList<Button_Dropdown_ListItem> suppressions_CreateButton_Options() {
		ArrayList<Button_Dropdown_ListItem> dropdown_Items = new ArrayList<Button_Dropdown_ListItem>();
		Button_Dropdown_ListItem suppressionItem = new Button_Dropdown_ListItem("Suppression") {
			@Override
			public void wait_ForPageToLoad() throws Exception {
				BaseUI.waitForElementToBeDisplayed("suppressionImport_Header", null, null);
				Thread.sleep(500);
			}
		};

		dropdown_Items.add(suppressionItem);
		return dropdown_Items;
	}

	public static void search_ByText_UntilFound(String textToEnterIntoSearchBox) throws Exception {

		BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
			public Boolean call() throws Exception {
				Browser.driver.navigate().refresh();
				Thread.sleep(2000);
				BaseUI.wait_forPageToFinishLoading();
				wait_ForPageToLoad();
				quickSearchBox.search_WithCriteria(textToEnterIntoSearchBox);
				return !suppressionTable.tableWasEmpty();
			}
		}, Duration.ofMinutes(10), Duration.ofSeconds(5));

	}

	public static void wait_ForPageToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("suppressions_Table_FirstRow_FirstCell", null, null, 120);
		Thread.sleep(500);
	}

	// Cleans up very specific old DNS Suppressions. Might refactor at some point if
	// we find the need to clean up other suppression types.
	public static void cleanup_AndRemove_OldDNSSuppressions() throws Exception {
		while (true) {
			select_ShowEntries_Value(showEntriesOptions.option_ViewAll);
			quickSearchBox.search_WithCriteria("DNS Suppression");
			suppressionTable.sort_Header_Descending("Order Date");
			TableData suppressionData = Suppressions.suppressionTable.return_TableData();

			HashMap<String, String> rowToDelete = null;

			int index = 0;
			for (HashMap<String, String> row : suppressionData.data) {
				index++;
				boolean row_Is_DNSSuppression = row.get("Order Type").equals("DNS Suppression") ? true : false;
				boolean row_Is_AccountACME = row.get("Account").equals("ACME") ? true : false;
				boolean row_Is_NameEmpty = row.get("Name").equals("") ? true : false;
				boolean row_Is_Marketer1 = row.get("Marketer").equals("1") ? true : false;
				boolean row_Is_StatusComplete = row.get("Status").equals("Complete") ? true : false;
				String orderDate = row.get("Order Date").split("\\s")[0];

				DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yyyy");
				LocalDate beforeDate = LocalDate.parse(BaseUI.getDateAsString_InRelationToTodaysDate(-3, "M/d/yyyy"),
						format);
				LocalDate actualDate = LocalDate.parse(orderDate, format);
				boolean row_Is_OlderDate = beforeDate.isAfter(actualDate);

				if (row_Is_DNSSuppression && row_Is_AccountACME && row_Is_NameEmpty && row_Is_Marketer1
						&& row_Is_StatusComplete && row_Is_OlderDate) {
					rowToDelete = row;
					break;
				}
			}

			if (rowToDelete == null) {
				break;
			} else {
			}

			delete_Suppression(index);
		}

	}

	// xpath indexing starts at 1.
	public static void delete_Suppression(Integer indexToPick) throws Exception {
		String textToInject = "Waiting for Automation Deletion to refresh page.";
		BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("suppressions_GridElement_ToInjectTextInto"),
				textToInject);

		suppressionTable.click_Delete_ForRow(indexToPick);
		click_ModalDeleteEntry_OK_ToDelete();

		BaseUI.waitForElementToNOTContain_PartialAttributeMatch("suppressions_GridElement_ToInjectTextInto", null, null,
				"innerText", textToInject, 40);
		wait_ForPageToLoad();
	}

	private static void click_ModalDeleteEntry_OK_ToDelete() {
		BaseUI.click(Locator.lookupElement("suppressions_DeleteItemModal_OK_Button"));
	}

	public static void select_ShowEntries_Value(showEntriesOptions optionToPick) throws Exception {
		String textToInject = "Waiting For Automation Script to trigger refresh";

		BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("suppressions_showingXNumberOfEntries"), textToInject);

		showEntriesDropdown.select_DropdownOption(optionToPick);

		BaseUI.waitForElementToNOTContain_PartialAttributeMatch("suppressions_showingXNumberOfEntries", null, null,
				"innerText", textToInject, 30);
		Thread.sleep(500);
	}

	// Assumes there is only one entry, which would happen via the Quick Search
	public static void verify_Suppression1Entry_Correct(String id, String name, String account, String marketer,
			String searchType, String orderType, Integer quantity, String orderTime) throws Exception {
		TableData suppression_Info = suppressionTable.return_TableData();
		suppression_Info.logTable();

		suppression_Info.verify_TableSize(1);
		HashMap<String, String> firstRow = suppression_Info.data.get(0);
		BaseUI.baseStringCompare("Id", id, firstRow.get("Id"));
		BaseUI.baseStringCompare("Name", name, firstRow.get("Name"));
		BaseUI.baseStringCompare("Account", account, firstRow.get("Account"));
		BaseUI.baseStringCompare("Marketer", marketer, firstRow.get("Marketer"));
		BaseUI.baseStringCompare("Search Type", searchType, firstRow.get("Search Type"));
		BaseUI.baseStringCompare("Order Type", orderType, firstRow.get("Order Type"));
		BaseUI.baseStringCompare("Quantity", quantity.toString().replace(",", ""), firstRow.get("Quantity"));

		String orderDateMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(orderTime,
				tableDateFormat, tableDateFormat, -2);
		String orderDatePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(orderTime, tableDateFormat,
				tableDateFormat, 2);
		// Date Uploaded
		// 1/31/2018 8:54:05 AM
		BaseUI.verify_Date_IsBetween_DateRange(orderDateMinus2Minutes, orderDatePlus2Minutes, orderTime,
				tableDateFormat);
	}

	// Assumes there is only one entry, which would happen via the Quick Search
	public static void verify_SuppressionAutomation_1Entry_Successful(String id, String name, String orderType) throws Exception {
		TableData suppression_Info = suppressionTable.return_TableData();

		suppression_Info.verify_TableSize(1);
		HashMap<String, String> firstRow = suppression_Info.data.get(0);
		BaseUI.baseStringCompare("Id", id, firstRow.get("Id"));
		BaseUI.baseStringCompare("Name", name, firstRow.get("Name"));

		BaseUI.baseStringCompare("Order Type", orderType, firstRow.get("Order Type"));
		BaseUI.verify_true_AndLog(Integer.parseInt(firstRow.get("Quantity")) > 0, "Found Quantity > 0.",
				"Did NOT find valid quantity.");

		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, tableDateFormat);
		
		String orderDateMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime,
				tableDateFormat, tableDateFormat, -1300);
		String orderDatePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime, tableDateFormat,
				tableDateFormat, 2);
		// Date Uploaded
		// 1/31/2018 8:54:05 AM
		BaseUI.verify_Date_IsBetween_DateRange(orderDateMinus2Minutes, orderDatePlus2Minutes, currentTime,
				tableDateFormat);
	}
	
	public static void verify_SuppressionAutomation_1Entry_FailNoRecords(String id, String name, String orderType) throws Exception {
		TableData suppression_Info = suppressionTable.return_TableData();

		suppression_Info.verify_TableSize(1);
		HashMap<String, String> firstRow = suppression_Info.data.get(0);
		BaseUI.baseStringCompare("Id", id, firstRow.get("Id"));
		BaseUI.baseStringCompare("Name", name, firstRow.get("Name"));
		BaseUI.baseStringCompare("Status", "No Records", firstRow.get("Status"));

		BaseUI.baseStringCompare("Order Type", orderType, firstRow.get("Order Type"));
		BaseUI.verify_true_AndLog(Integer.parseInt(firstRow.get("Quantity")) == -1, "Found Quantity == -1.",
				"Quantity was NOT equal to -1.");

		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, tableDateFormat);
		
		String orderDateMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime,
				tableDateFormat, tableDateFormat, -1300);
		String orderDatePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime, tableDateFormat,
				tableDateFormat, 2);
		// Date Uploaded
		// 1/31/2018 8:54:05 AM
		BaseUI.verify_Date_IsBetween_DateRange(orderDateMinus2Minutes, orderDatePlus2Minutes, currentTime,
				tableDateFormat);
	}

	
	public static void verify_SuppressionAutomation_1Entry_Failed(String id, String name, String orderType) throws Exception {
		TableData suppression_Info = suppressionTable.return_TableData();

		suppression_Info.verify_TableSize(1);
		HashMap<String, String> firstRow = suppression_Info.data.get(0);
		BaseUI.baseStringCompare("Id", id, firstRow.get("Id"));
		BaseUI.baseStringCompare("Name", name, firstRow.get("Name"));
		BaseUI.baseStringCompare("Status", "Failed", firstRow.get("Status"));

		BaseUI.baseStringCompare("Order Type", orderType, firstRow.get("Order Type"));
		BaseUI.verify_true_AndLog(Integer.parseInt(firstRow.get("Quantity")) == -1, "Found Quantity == -1.",
				"Quantity was NOT equal to -1.");

		String currentTime = BaseUI.getDateAsString_InRelationToTodaysDate(0, tableDateFormat);
		
		String orderDateMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime,
				tableDateFormat, tableDateFormat, -1300);
		String orderDatePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(currentTime, tableDateFormat,
				tableDateFormat, 2);
		// Date Uploaded
		// 1/31/2018 8:54:05 AM
		BaseUI.verify_Date_IsBetween_DateRange(orderDateMinus2Minutes, orderDatePlus2Minutes, currentTime,
				tableDateFormat);
	}

	public static void verify_Suppressions_PageLoaded() {
		WebElement header = Locator.lookupElement("suppressions_Header");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Suppressions");
		BaseUI.verifyElementAppears(Locator.lookupElement("suppressions_Table_FirstRow_Checkbox"));
	}

}// End Of Class
