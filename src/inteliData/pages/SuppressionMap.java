package inteliData.pages;

import java.util.HashMap;

import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Table_SuppressionMap;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class SuppressionMap {

	public static String dateUploaded_Format = "M/d/yyyy h:mm:ss a";

	public static Table_SuppressionMap mapTable =Table_SuppressionMap.get_Table_SuppressionMap_byID("mappingTable");
	
	public final static DataPanel_MainContent data_Panel = new DataPanel_MainContent("Details");

	// upload time should match dateUploaded_Format
	//DNS - name will be the name we entered previous page.
	//External Suppressions - name will be filename because there is no name field up to this point.
	public static void verify_detailsPanel(String name, String fileName, String uploadTime, String createdBy,
			Integer recordsInSourceFile) throws Exception {
		HashMap<String, String> details = data_Panel.return_PanelData();

		BaseUI.baseStringCompare("Name", name, details.get("Name"));
		BaseUI.baseStringCompare("Source File", fileName, details.get("Source File"));

		String expectedDateMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				dateUploaded_Format, dateUploaded_Format, -2);
		String expectedDatePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				dateUploaded_Format, dateUploaded_Format, 2);
		BaseUI.verify_Date_IsBetween_DateRange(expectedDateMinus2Minutes, expectedDatePlus2Minutes, uploadTime,
				dateUploaded_Format);

		BaseUI.baseStringCompare("Created By", createdBy, details.get("Created By"));
		BaseUI.baseStringCompare("Records In Source File", recordsInSourceFile.toString(),
				details.get("Records In Source File"));
	}

	// valuesToCheck has the Key set to Source and the Value set to Destination
	public static void verify_Mappings(TableData mappingTableData, HashMap<String, String> valuesToCheck) {
		for (String source : valuesToCheck.keySet()) {
			HashMap<String, String> mappingRow = mappingTableData.return_Row_BasedOn_2MatchingFields("Source", source, "Destination",
					valuesToCheck.get(source));
			BaseUI.verify_true_AndLog(mappingRow != null,
					"Found row matching source of " + source + " with Destination of " + valuesToCheck.get(source),
					"Did NOT find row matching source of " + source + " with Destination of " + valuesToCheck.get(source));
		}

	}

	public static void click_Map_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("suppressionMap_MapButton"));
		ConfirmMappedFields.wait_For_PageToLoad();
	}

}// End of Class
