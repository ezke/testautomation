package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Administration_DNS {

	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "DNS Records");
		BaseUI.verifyElementAppears(Locator.lookupElement("administration_DNS_table_FirstRow_Checkbox"));
	}
	
	
	
	
}//End of Class
