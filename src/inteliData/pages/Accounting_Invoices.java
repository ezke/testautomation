package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Accounting_Invoices {

	public static void verify_PageLoaded() {
		WebElement header = Locator.lookupElement("nav_PageHeader");
		BaseUI.verifyElementAppears(header);
		BaseUI.verifyElementHasExpectedText(header, "Invoices");
		BaseUI.verifyElementAppears(Locator.lookupElement("accounting_Invoices_table_FirstRow_FirstCell"));
	}
	
	
	
	
	
}//End of Class
