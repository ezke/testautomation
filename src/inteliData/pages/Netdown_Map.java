package inteliData.pages;

import java.util.HashMap;

import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Table_SuppressionMap;
import utils.BaseUI;

public class Netdown_Map {

	public static String dateUploaded_Format = "M/d/yyyy";

	public static Table_SuppressionMap mapTable = Table_SuppressionMap.get_Table_SuppressionMap_byID("mappingTable");
	
	public final static DataPanel_MainContent data_Panel = new DataPanel_MainContent("Details");

	public static void verify_DetailsPanel(String orderID, Integer expectedRecordCount, String createdBy, String date) {
		HashMap<String, String> details_Data = data_Panel.return_PanelData();

		BaseUI.baseStringCompare("Name", "Netdown Import File: " + orderID, details_Data.get("Name"));
		BaseUI.baseStringCompare("Input Record Count", expectedRecordCount.toString(),
				details_Data.get("Input Record Count"));
		BaseUI.baseStringCompare("Created By", createdBy, details_Data.get("Created By"));
		BaseUI.baseStringCompare("Date", date, details_Data.get("Date"));
	}

	//Currently same logic as Suppression Map, did pass through.
	public static void click_Map() throws Exception {
		SuppressionMap.click_Map_Button();
	}

}
