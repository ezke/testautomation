package inteliData.pages;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.Callable;

import inteliData.page_controls.*;
import org.apache.commons.lang3.Range;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import inteliData.data.GlobalVariables;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.DatabaseConnection;
import utils.FileOperations;
import utils.Locator;
import utils.TableData;

public class CreateAudience_DownloadRecords_Page {

    public static String timeFormat_FileName = "MMddyyyy hmma (z)";

    public static String dateFormat = "M/d/yyyy h:mm:ss a";
    public static DataPanel_MainContent audienceSummary_Panel = new DataPanel_MainContent("Audience Summary");
    public static DataPanel_MainContent orderDetails_Panel = new DataPanel_MainContent("Order Details");
    public static Table_WithLinks filesTable = new Table_WithLinks("filespanel") {
        @Override
        public void wait_AfterClick() throws Exception {
            Thread.sleep(500);
            BaseUI.switchToAlert_Accept();
            FileOperations.wait_ForFile_WithExtension_ToExist("CSV", Browser.downloadLocation, 50);
            FileOperations.wait_ForFile_WithExtension_ToNOTExist("crdownload", Browser.downloadLocation, 50);
        }
    };

    public static void verify_DownloadRecordsPage_Loads() {
        BaseUI.verifyElementAppears(Locator.lookupElement("createAud_DownloadRecords_CurrentDownloadIcon"));
    }

    public static void retry_DownloadRecordsNavigation_IfNotAtDownloadRecordsPage() throws Exception {
        String currentURL = Browser.driver.getCurrentUrl();
        if (!currentURL.contains("Order/DownloadRecords/")) {

            String orderID = "";
            if (currentURL.contains("/Order/Processing/")) {
                orderID = currentURL.substring(currentURL.lastIndexOf("relatedObjectId="), currentURL.length())
                        .split("\\=")[1];
            } else {
                BaseUI.log_AndFail("Unrecognized url of " + currentURL + "\n Please update retry logic for this url.");
            }

            Navigation.navigate_Activities();
            Activities.click_Tab_AllOrders();
            Activities.allOrders_SearchBox.search_WithCriteria(orderID);
            Activities.allOrders_ActivitiesTable.click_View_ForRow(1);
        }

    }

    public static void wait_ForPageToLoad() throws Exception {

        WebElement waitElement = BaseUI.waitForElementToBeDisplayed_ButDontFail(
                "createAud_DownloadRecords_CurrentDownloadIcon", null, null, 600);
        if (waitElement != null) {
            Thread.sleep(500);
        } else {
            String logOutput = BaseUI.return_Browser_Output();
            BaseUI.log_AndFail(
                    "Timed out waiting for Download Records page to load. Showing browser output: \n" + logOutput);
        }
    }

    public static void refreshPage() throws Exception {
        String textToInject = "Automation - Testing Refresh";
        BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("createAud_DownloadRecords_MainDiv"), textToInject);

        Browser.driver.navigate().refresh();
        BaseUI.waitForElementToNOTContain_PartialAttributeMatch("createAud_DownloadRecords_MainDiv", null, null,
                "innerText", textToInject, 120);
        BaseUI.waitForElementToBeDisplayed("createAud_DownloadRecords_CurrentDownloadIcon", null, null, 40);
        Thread.sleep(2500);
    }

    public static void refresh_Page_Until_FileDelivered(String fileToBeDelivered) throws Exception {

        BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
            public Boolean call() throws Exception {
                refreshPage();

                TableData files_TableData = filesTable.return_TableData();
                String fileCell = files_TableData.return_Row_BasedOn_1MatchingField("Type", fileToBeDelivered)
                        .get("Files");
                if (fileCell.contains("(Delivered")) {
                    return true;
                }
                return false;
            }
        }, Duration.ofSeconds(30), Duration.ofMillis(500));
    }

    public static void verify_FileDelivered(String fileTypeName) {
        TableData files_TableData = filesTable.return_TableData();
        verify_FileDelivered(files_TableData, fileTypeName);
    }

    // Overload so I don't have to keep grabbing TableData, since I need it for
    // other things.
    public static void verify_FileDelivered(TableData files_TableData, String fileTypeName) {

        String fileCell = files_TableData.return_Row_BasedOn_1MatchingField("Type", fileTypeName).get("Files");
        BaseUI.baseStringPartialCompare("Delivered", "(Delivered)", fileCell);
    }

    public static String return_OrderID() {
        HashMap<String, String> orderData = orderDetails_Panel.return_PanelData();

        return orderData.get("Order ID");
    }

    // Append doesn't have Selected Records in Audience Summary Panel
    public static void verify_AudienceSummaryPanel(Integer audienceID, String name, String mart, String account,
                                                   String marketer, String createdTime) throws Exception {
        HashMap<String, String> audienceSummary_Data = audienceSummary_Panel.return_PanelData();

        // Valid Audience ID
        Integer audience_ID = Integer.parseInt(audienceSummary_Data.get("Audience Id"));
        BaseUI.verify_true_AndLog(audience_ID.equals(audienceID), "Audience ID matched " + audienceID.toString(),
                "Expected Audience ID of " + audienceID.toString() + " but seeing "
                        + audienceSummary_Data.get("Audience Id"));

        // Mart matches
        BaseUI.baseStringCompare("Mart", mart, audienceSummary_Data.get("Mart"));

        // Marketer matches
        BaseUI.baseStringCompare("Marketer", marketer, audienceSummary_Data.get("Marketer"));

        if (name.length() > 25) {
            name = name.substring(0, 25);
        }

        BaseUI.baseStringCompare("Name", name, audienceSummary_Data.get("Name"));

        // Created within an acceptable range from expected Time.
        String createdTime_range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, -2);
        String createdTime_range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, 2);
        if(GlobalVariables.run_DateTimeChecks_Dev) {
            BaseUI.verify_Date_IsBetween_DateRange(createdTime_range1, createdTime_range2,
                    audienceSummary_Data.get("Created"), dateFormat);
        }

        // Status = complete
        BaseUI.baseStringCompare("Status", "Complete", audienceSummary_Data.get("Status"));
    }

    private static void click_Actions_ListItem(String listItem) throws Exception {
        Button_Dropdown_V2 actionButton = new Button_Dropdown_V2("createAud_DownloadRecords_ActionButton",
                "createAud_DownloadRecords_Action_ListItem_ByText", listItem);
        actionButton.open_List_AndClickItem();
    }

    public static void click_Actions_PostNetdown() throws Exception {
        click_Actions_ListItem("Post Netdown");
        BaseUI.waitForElementToBeDisplayed("netdownImport_Header", null, null);
        Thread.sleep(500);
    }

    public static void click_Actions_RecreateAudience() throws Exception {
        click_Actions_ListItem("Recreate Audience");
        CreateAudience_SearchReview.wait_ForSearchReviewPage_ToLoad();
    }


    public static void refresh_Until_Shipped() throws Exception {
        BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
            public Boolean call() throws Exception {
                refreshPage();

                String fileState = orderDetails_Panel.return_SpecificValue("Status");
                if (fileState.contains("Shipped")) {
                    return true;
                } else if (fileState.contains("Quality Assurance") || fileState.contains("Failed")) {
                    BaseUI.log_Status("Order Details had a failed status of " + fileState);
                }
                return false;
            }
        }, Duration.ofSeconds(30), Duration.ofMillis(500));
    }

    public static void verify_OrderDetailsPanel(String poNumber, String expectedPurchaseDate,
                                                String expectedSelectedRecords, String totalPrice, Boolean shipped) throws Exception {
        HashMap<String, String> orderDetails_Data = orderDetails_Panel.return_PanelData();

        // Valid Order ID
        Integer order_ID = Integer.parseInt(orderDetails_Data.get("Order ID"));
        BaseUI.verify_true_AndLog(order_ID > 0, "Order ID was a valid Integer",
                "Order ID of " + order_ID.toString() + " was not valid.");

        // PO Number
        BaseUI.baseStringCompare("PO Number", poNumber, orderDetails_Data.get("PO Number"));

        // Date within an acceptable range from expected Time.
        String createdTime_range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(expectedPurchaseDate,
                dateFormat, dateFormat, -2);
        String createdTime_range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(expectedPurchaseDate,
                dateFormat, dateFormat, 2);
        if(GlobalVariables.run_DateTimeChecks_Dev) {
            BaseUI.verify_Date_IsBetween_DateRange(createdTime_range1, createdTime_range2, orderDetails_Data.get("Date"),
                    dateFormat);
        }
        String expectedStatus = shipped ? "Shipped" : "Posted";

        // Status
        BaseUI.baseStringCompare("Status", expectedStatus, orderDetails_Data.get("Status"));

        BaseUI.baseStringCompare("Selected Records", expectedSelectedRecords.replace(",", ""),
                orderDetails_Data.get("Selected Records").replace(",", ""));

        if (totalPrice != null) {
            Double expectedTotalPriceDoub = Double.parseDouble(totalPrice.replace("$", "").replace(",", ""));

            Double actualTotalPriceDoub = Double
                    .parseDouble(orderDetails_Data.get("Total Price").replace("$", "").replace(",", ""));

            BaseUI.verify_true_AndLog(expectedTotalPriceDoub.equals(actualTotalPriceDoub),
                    "Total matched expected value of " + totalPrice,
                    "Expected Total of " + totalPrice + " but seeing " + orderDetails_Data.get("Total Price"));

        } else {
            Double totalPriceDoub = Double
                    .parseDouble(orderDetails_Data.get("Total Price").replace("$", "").replace(",", ""));
            BaseUI.verify_true_AndLog(totalPriceDoub > 0, "Seeing Total Price of " + totalPriceDoub.toString(),
                    "Total Price was NOT a Double Greater than 0. Seeing " + orderDetails_Data.get("Total Price"));
        }
    }

    // Row To Pick indexing starts at 1.
    public static TableData download_File_FromDownloadRecords(Integer rowToPick, String fileName) throws Exception {
        TableData exportResults = new TableData();

        fileName = fileName.replace(" (Delivered)", "");

        DataBuilder.delete_Files_WithExtention_FromLocation(".CSV", Browser.downloadLocation);
        DataBuilder.delete_Files_WithExtention_FromLocation("crdownload", Browser.downloadLocation);
        filesTable.clickLink_ByRowIndex_ColumnName(rowToPick, "Files");
        FileOperations.wait_ForFile_WithExtension_ToExist(".CSV", Browser.downloadLocation, 60);
        // String fileToReturn = DataBuilder.return_File_WithExtension(".csv",
        // Browser.downloadLocation);
        exportResults = DataBuilder.returnTableData_ForComparison(Browser.downloadLocation + "\\" + fileName, ",",
                false);

        exportResults.remove_Character_FromKeys("\"");
        exportResults.remove_Character("\"");
        return exportResults;
    }

    public static TableData return_CSV_Data(String typeName) throws Exception {
        TableData csvTable = new TableData();
        String fileName = "";

        TableData fileTable = CreateAudience_DownloadRecords_Page.filesTable.return_TableData();
        Integer indexOfLineToDownload = fileTable.first_IndexOf("Type", typeName);
        fileName = fileTable.data.get(indexOfLineToDownload).get("Files");
        csvTable = CreateAudience_DownloadRecords_Page.download_File_FromDownloadRecords(indexOfLineToDownload + 1,
                fileName);
        return csvTable;
    }


    // states must be in format AL
    public static void verify_CSV_MatchesExpected_NoMasking(TableData csvTable, String[] states,
                                                            HashMap<String, Range<Integer>> levelsAndRanges) {

        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        csvTable.remove_Character_FromKeys("\"");
        csvTable.remove_Character("\"");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        int currentRow = 0;
        // String output = "";
        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> row : csvTable.data) {

                check_LName(softAssert, row.get("LNAME"), currentRow);
                check_FNAME(softAssert, row.get("FNAME"), currentRow);
                check_MI_OneChar_OR_Empty(softAssert, row.get("MI"), currentRow);
                check_Address1(softAssert, row.get("ADDR1"), currentRow);
                // Cannot validate Address 2 as it could be null, empty, or have a value.
                check_City_Isnt_Empty(softAssert, row.get("CITY"), currentRow);
                check_State_IsValid(softAssert, row.get("STATE"), states, currentRow);
                check_ValidZip(softAssert, row.get("ZIP"), currentRow);
                check_Valid_ZIP4(softAssert, row.get("ZIP4"), currentRow);
                check_LevelIsValid(softAssert, row.get("LEVEL"), levelsAndRanges, currentRow);
                check_ORDERRECORDID_IsValid(softAssert, row.get("ORDERRECORDID"), currentRow);
                check_FICO_IsValid(softAssert, row.get(ficoText), row.get("LEVEL"), levelsAndRanges, currentRow);

                currentRow++;
            }

        } finally {
            softAssert.assertAll();
        }

        // BaseUI.verify_true_AndLog(output.equals(""), "CSV Values were acceptable.",
        // output);

    }

    //Expected time should be in format "MMddyyyy hmma"
    public static void verify_FileName_Matches_search_searchOrder(String audienceName, String expectedDateTime, String actualName) throws Exception {
        //Auto Order Tests - Automation 06112018 923AM (CST)_1_40070972_70024119.CSV

        BaseUI.verify_true_AndLog(actualName.startsWith(audienceName), "File Name started with PO Number: " + audienceName,
                "File Name did not start with PO Number '" + audienceName + "' Seeing file name of " + actualName);

        String nameWithoutPONumber = actualName.substring(audienceName.length(), actualName.length() - 1).trim();

        String[] nameSplit = nameWithoutPONumber.split("\\s");
        expectedDateTime =return_DateTime_FormattedForComparison(expectedDateTime);



        String actualDateTime = nameSplit[0] + " " + nameSplit[1] + " " + nameSplit[2].substring(0, nameSplit[2].indexOf("_"));
        actualDateTime = return_DateTime_FormattedForComparison(actualDateTime);
        BaseUI.verify_Date_IsAcceptable_DateRange(expectedDateTime, actualDateTime, -5, 3, "MMddyyyy h:mma (z)");
    }


    private static String return_DateTime_FormattedForComparison(String dateTimeString){

        String[] updateDateFormat = dateTimeString.split("\\s");
        String timeString = updateDateFormat[1];
        if(timeString.length() == 6){
            timeString = timeString.substring(0, 2) + ":" + timeString.substring(2, timeString.length());
        } else if(timeString.length() == 5){
            timeString = timeString.substring(0, 1) + ":" + timeString.substring(1, timeString.length());
        }

        updateDateFormat[1] = timeString;

        dateTimeString =  updateDateFormat[0];
        for(int i = 1; i < updateDateFormat.length; i++){
            dateTimeString += " " + updateDateFormat[i];

        }

        return dateTimeString;

    }



    public static void verify_CSV_MatchesExpected(TableData csvTable, String[] states,
                                                  HashMap<String, Range<Integer>> levelsAndRanges) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        csvTable.remove_Character_FromKeys("\"");
        csvTable.remove_Character("\"");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        int currentRow = 0;
        // String output = "";
        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> row : csvTable.data) {

                check_LNAME_WithFixedXs(softAssert, row.get("LNAME"), currentRow);
                check_FNAME_WithFixedXs(softAssert, row.get("FNAME"), currentRow);
                check_MI_With1X_OrNone(softAssert, row.get("MI"), currentRow);
                check_Address1_With_FixedXs(softAssert, row.get("ADDR1"), currentRow);
                check_ADDR2_WithFixedXs(softAssert, row.get("ADDR2"), currentRow);
                check_City_Isnt_Empty(softAssert, row.get("CITY"), currentRow);
                check_State_IsValid(softAssert, row.get("STATE"), states, currentRow);
                check_ValidZip(softAssert, row.get("ZIP"), currentRow);
                check_Valid_ZIP4(softAssert, row.get("ZIP4"), currentRow);
                check_LevelIsValid(softAssert, row.get("LEVEL"), levelsAndRanges, currentRow);
                check_ORDERRECORDID_IsValid(softAssert, row.get("ORDERRECORDID"), currentRow);
                check_FICO_IsValid(softAssert, row.get(ficoText), row.get("LEVEL"), levelsAndRanges, currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }
    }

    // tableInfo - each row has 3 fields
    // level - returns a string with the level
    // ficoRange - returns a Range object with the valid fico range
    // states - String[] of available states, will be 2 letter abbreviations
    public static Geography_AreaCode_Info get_AreaCode_LevelWeWant(ArrayList<Geography_AreaCode_Info> tableInfo,
                                                          String levelToFind) {
        for (Geography_AreaCode_Info levelInfo : tableInfo) {
            if (levelInfo.getLevel().equals(levelToFind)) {
                return levelInfo;
            }
        }

        return null;
    }

    public static Geography_ZipCode_Info get_ZipCode_LevelWeWant(ArrayList<Geography_ZipCode_Info> tableInfo,
                                                             String levelToFind) {
        for (Geography_ZipCode_Info levelInfo : tableInfo) {
            if (levelInfo.getLevel().equals(levelToFind)) {
                return levelInfo;
            }
        }

        return null;
    }



    public static Geography_State_Info get_State_LevelWeWant(ArrayList<Geography_State_Info> tableInfo,
                                                          String levelToFind) {
        for (Geography_State_Info levelInfo : tableInfo) {
            if (levelInfo.getLevel().equals(levelToFind)) {
                return levelInfo;
            }
        }

        return null;
    }

    // tableInfo - each row has 3 fields
    // level - returns a string with the level
    // ficoRange - returns a Range object with the valid fico range
    // states - String[] of available states, will be 2 letter abbreviations
    public static void verify_CSV_Matches_FICO_And_States(TableData csvTable,
                                                          ArrayList<Geography_State_Info> tableInfo) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        csvTable.remove_Character_FromKeys("\"");
        csvTable.remove_Character("\"");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        int currentRow = 0;
        // String output = "";
        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> row : csvTable.data) {
                Geography_State_Info levelInfo = get_State_LevelWeWant(tableInfo, row.get("LEVEL"));

                softAssert.assertNotNull(levelInfo);
                check_State_IsValid(softAssert, row.get("STATE"), levelInfo.get_states(), currentRow);
                check_FICO_IsValid(softAssert, row.get(ficoText), levelInfo.get_ficoRange(), currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }
    }


    // tableInfo - each row has 3 fields
    // level - returns a string with the level
    // ficoRange - returns a Range object with the valid fico range
    // zipCodes - String[] of expected zip codes
    public static void verify_CSV_Matches_FICO_And_Zips(TableData csvTable,
                                                        ArrayList<Geography_ZipCode_Info> tableInfo) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        csvTable.remove_Character_FromKeys("\"");
        csvTable.remove_Character("\"");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        int currentRow = 0;

        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> row : csvTable.data) {

                Geography_ZipCode_Info levelInfo = get_ZipCode_LevelWeWant(tableInfo, row.get("LEVEL"));

                softAssert.assertNotNull(levelInfo);
                check_Zip_Matches(softAssert, row.get("ZIP"), (String[]) levelInfo.get_zips(), currentRow);
                check_FICO_IsValid(softAssert, row.get(ficoText),
                        (Range<Integer>) levelInfo.get_ficoRange(), currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }
    }

    // tableInfo - each row has 3 fields
    // level - returns a string with the level
    // ficoRange - returns a Range object with the valid fico range
    // areaCodes - String[] of expected area codes
    public static void verify_CSV_Matches_FICO_And_AreaCodes(TableData csvTable,
                                                             ArrayList<Geography_AreaCode_Info> tableInfo) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        csvTable.remove_Character_FromKeys("\"");
        csvTable.remove_Character("\"");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        int currentRow = 0;

        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> row : csvTable.data) {

                Geography_AreaCode_Info levelInfo = get_AreaCode_LevelWeWant(tableInfo, row.get("LEVEL"));

                softAssert.assertNotNull(levelInfo);
                check_AreaCode_Matches(softAssert, row.get("AREACODE"), (String[]) levelInfo.get_areaCodes_AfterExclusion(), currentRow);
                check_FICO_IsValid(softAssert, row.get(ficoText),
                        (Range<Integer>) levelInfo.getFicoRange_Initial(), currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }
    }


    // expectedRows - each row has 5 fields
    // Level - returns a string with the level
    // State
    // City
    // ficoMin
    // ficoMax

    public static void verify_CSV_Matches_CityDBQuery(TableData csvTable,
                                                      TableData cityDBData) throws Exception {

        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> csvRow : csvTable.data) {
                HashMap<String, String> dbRow_ValuesToFind = new HashMap<>();
                dbRow_ValuesToFind.put("zipcode", csvRow.get("ZIP"));
                dbRow_ValuesToFind.put("state", csvRow.get("STATE"));
                dbRow_ValuesToFind.put("level", csvRow.get("LEVEL"));

                HashMap<String, String> matchingRow = cityDBData.return_Row_BasedOn_AnyNumberOfMatchingFields(dbRow_ValuesToFind);

                softAssert.assertTrue(matchingRow != null,
                        "Did NOT find matching row for " + csvRow.toString());
            }
        } finally {
            softAssert.assertAll();
        }
    }


    public static void add_FicoMin_Max_ToTable_ForGivenLevel(TableData tableToUpdate, String levelToUpdate, Range<Integer> ficoRange){
        tableToUpdate.forEach_Row(a -> {
            String level = a.get("Level");
            if (level.equals(levelToUpdate)) {
                a.put("ficoMin", String.valueOf(ficoRange.getMinimum()));
                a.put("ficoMax", String.valueOf(ficoRange.getMaximum()));
            }
        });

    }


    public static void verify_CSV_Matches_dmaDBQuery(TableData csvTable,
                                                      TableData dmaDBData) throws Exception {

        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> csvRow : csvTable.data) {
                HashMap<String, String> dbRow_ValuesToFind = new HashMap<>();
                dbRow_ValuesToFind.put("zipcode", csvRow.get("ZIP"));
                dbRow_ValuesToFind.put("level", csvRow.get("LEVEL"));

                HashMap<String, String> matchingRow = dmaDBData.return_Row_BasedOn_AnyNumberOfMatchingFields(dbRow_ValuesToFind);

                softAssert.assertTrue(matchingRow != null,
                        "Did NOT find matching row for " + csvRow.toString());
            }
        } finally {
            softAssert.assertAll();
        }
    }

    public static void validate_Fico_InCSV(TableData csvTable, TableData expectedRows) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        Integer currentRow = 0;

        SoftAssert softAssert = new SoftAssert();

        try {
            for (HashMap<String, String> row : csvTable.data) {
                HashMap<String, String> matchingRow_ExpectedRows = expectedRows.return_Row_BasedOn_1MatchingField("Level", row.get("LEVEL"));
                Range<Integer> _ficoRange_ForMatchingRow = Range.between(Integer.parseInt(matchingRow_ExpectedRows.get("ficoMin")),
                        Integer.parseInt(matchingRow_ExpectedRows.get("ficoMax")));

                check_FICO_IsValid(softAssert, row.get(ficoText), _ficoRange_ForMatchingRow, currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }
    }


    // Checks Last Name from file with fixed number of X's (OR Not empty string for
    // PROD environment). Set up to return an
    // empty string if Last Name was acceptable. Use for our CSV validation design
    // to cut down on console output.
    private static void check_LNAME_WithFixedXs(SoftAssert softAssert, String lname_FromFile, int currentRow) {
        if (GlobalVariables.environment.equals("PROD")) {
            check_LName(softAssert, lname_FromFile, currentRow);
        } else {
            softAssert.assertTrue(
                    !BaseUI.stringEmpty(lname_FromFile) && lname_FromFile.equals("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),
                    "Last Name was " + lname_FromFile + " but was expecting X's. Row: " + String.valueOf(currentRow));

        }
    }


    public static TableData return_City_DatabaseQuery(int audienceID) throws Exception {

        String query = "SELECT DISTINCT\n" +
                "\t\t\tz.zipcode,\n" +
                "\t\t\tz.city,\n" +
                "\t\t\tz.state,\n" +
                "\t\t\texclusion,\n" +
                "\t\t\tlevel \n" +
                "\t\tFROM\n" +
                "\t\t\t\tzip z\n" +
                "\t\t\tJOIN\n" +
                "\t\t\t(\tSELECT \n" +
                "\t\t\t\t\tz.state, \n" +
                "\t\t\t\t\tz.city,\n" +
                "\t\t\t\t\tzsiv.exclusion,\n" +
                "\t\t\t\t\tzsiv.orderindex level\n" +
                "\t\t\t\tFROM \n" +
                "\t\t\t\t\t\tzip z\n" +
                "\t\t\t\t\tJOIN  \n" +
                "\t\t\t\t\t\tvs_geo_searchitemvalues zsiv ON z.zipcode = zsiv.value\n" +
                "\t\t\t\tWHERE \n" +
                "\t\t\t\t\tzsiv.searchitemtypeid = 38 -- City list\n" +
                "\t\t\t\t\tand zsiv.parentid = " + Integer.valueOf(audienceID) + "\n" +
                "\t\t\t) c ON z.city = c.city AND z.state = c.state\n" +
                "\tORDER by level ";

        DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;
        return DatabaseConnection.runSQLServerQuery(query);
    }

    public static TableData return_DMA_DatabaseQuery(int audienceID) throws Exception {

        String query = "SELECT DISTINCT\n" +
                "\t\t\tz.zipcode,\n" +
                "\t\t\tdmaname,\n" +
                "\t\t\texclusion,\n" +
                "\t\t\torderindex level\n" +
                "\t\tFROM\n" +
                "\t\t\t\tzip z\n" +
                "\t\t\tJOIN\n" +
                "\t\t\t\tvs_geo_searchitemvalues zsiv ON z.dma = zsiv.value\n" +
                "\t\tWHERE\n" +
                "\t\t\tzsiv.searchitemtypeid IN (56) --dma\n" +
                "\t\t\tand zsiv.parentid = " + String.valueOf(audienceID) + "\n" +
                "\t\tORDER by level ";

        DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;
        return DatabaseConnection.runSQLServerQuery(query);
    }

    public static TableData return_County_DatabaseQuery(int audienceID) throws Exception {

        String query = "\tSELECT DISTINCT\n" +
                "\t\t\tz.zipcode,\n" +
                "\t\t\tcountyname as 'county',\n" +
                "\t\t\tstate,\n" +
                "\t\t\texclusion,\n" +
                "\t\t\torderindex level\n" +
                "\t\tFROM\n" +
                "\t\t\t\tzip z\n" +
                "\t\t\tJOIN\n" +
                "\t\t\t\tvs_geo_searchitemvalues zsiv ON z.countyfips = zsiv.value\n" +
                "\t\tWHERE\n" +
                "\t\t\tzsiv.searchitemtypeid IN (9) --County list\n" +
                "\t\t\tand zsiv.parentid = " + String.valueOf(audienceID) + "\n" +
                "\t\tORDER by level ";

        DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;
        return DatabaseConnection.runSQLServerQuery(query);
    }

    public static void verify_Column_DoesNotMatch_Value(TableData tableToCheck, String columnToCheck,
                                                        String valueToNotMatch) {

        SoftAssert softAssert = new SoftAssert();

        try {
            tableToCheck.forEach_Row(a -> {
                String columnValue = a.get(columnToCheck);
                if (columnValue == null) {
                    softAssert.fail("Column of " + columnToCheck + " cell contained null value.");
                } else {
                    softAssert.assertNotEquals(columnValue, valueToNotMatch);
                }

            });
        } finally {
            softAssert.assertAll();
        }

    }

    // No X's, can only check that it's not empty
    private static void check_LName(SoftAssert softAssert, String lname_FromFile, int currentRow) {

        softAssert.assertTrue(!BaseUI.stringEmpty(lname_FromFile),
                "Last Name was not retrieved for row " + String.valueOf(currentRow));

    }

    private static void check_FNAME_WithFixedXs(SoftAssert softAssert, String fname_FromFile, int currentRow) {
        if (GlobalVariables.environment.equals("PROD")) {
            check_FNAME(softAssert, fname_FromFile, currentRow);
        } else {
            softAssert.assertTrue(
                    !BaseUI.stringEmpty(fname_FromFile) && fname_FromFile.equals("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
        }
    }

    private static void check_FNAME(SoftAssert softAssert, String fname_FromFile, int currentRow) {
        softAssert.assertTrue(!BaseUI.stringEmpty(fname_FromFile),
                "First Name was not retrieved for row " + String.valueOf(currentRow));
    }

    private static void check_MI_With1X_OrNone(SoftAssert softAssert, String middleInitial, int rowNumber) {
        middleInitial = middleInitial == null ? "" : middleInitial;
        if (GlobalVariables.environment.equals("PROD")) {
            check_MI_OneChar_OR_Empty(softAssert, middleInitial, rowNumber);

        } else {
            Boolean miAcceptableValue = BaseUI.stringEmpty(middleInitial) || middleInitial.equals("X");
            softAssert.assertTrue(miAcceptableValue,
                    "Middle initial was " + middleInitial + " but was expecting an X or an empty string.");
        }
    }

    private static void check_MI_OneChar_OR_Empty(SoftAssert softAssert, String middleInitial, int rowNumber) {
        Boolean miAcceptableValue = BaseUI.stringEmpty(middleInitial) || middleInitial.length() == 1;
        softAssert.assertTrue(miAcceptableValue,
                "Middle initial was suppsed to be empty or 1 character on row " + String.valueOf(rowNumber));
    }

    private static void check_Address1_With_FixedXs(SoftAssert softAssert, String address1, int rowNumber) {

        // Validate address 1
        if (GlobalVariables.environment.equals("PROD")) {
            check_Address1(softAssert, address1, rowNumber);
        } else {
            softAssert.assertTrue(
                    address1 != null && address1.equals("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),
                    "Address 1 did not match XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX for row "
                            + String.valueOf(rowNumber));
        }
    }

    private static void check_Address1(SoftAssert softAssert, String address1, int rowNumber) {
        softAssert.assertTrue(!BaseUI.stringEmpty(address1),
                "Address 1 was not found for row " + String.valueOf(rowNumber));
    }

    private static void check_ADDR2_WithFixedXs(SoftAssert softAssert, String address2, int rowNumber) {
        if (GlobalVariables.environment.equals("PROD")) {
            // Verify Address 2 - Nothing to verify for ADDR2 in unmasked field, it could be
            // NULL, empty, or a value.
        } else {
            Boolean address2IsAcceptableValue = BaseUI.stringEmpty(address2) || address2.equals("XXXXXXXXXXX");
            softAssert.assertTrue(address2IsAcceptableValue,
                    "Address 2 was not an acceptable format for line " + String.valueOf(rowNumber) + ".");
        }
    }

    private static void check_City_Isnt_Empty(SoftAssert softAssert, String city, int rowNumber) {
        softAssert.assertTrue(!BaseUI.stringEmpty(city),
                "City was not filled for line " + String.valueOf(rowNumber) + ".");
    }

    private static void check_ValidZip(SoftAssert softAssert, String zip, int rowNumber) {

        if (!BaseUI.stringEmpty(zip)) {
            int zipAsInt = Integer.parseInt(zip);

            softAssert.assertTrue(zip.length() == 5 && zipAsInt > 0,
                    "Zip was not acceptable length OR not greater than 0 for line " + String.valueOf(rowNumber)
                            + ". Seeing " + zip);

        } else {
            softAssert.fail("Zip was empty for line " + String.valueOf(rowNumber));
        }

    }

    // Assumes ZIP4 won't be masked. Some CSV's have it masked.
    private static void check_Valid_ZIP4(SoftAssert softAssert, String zip4, int rowNumber) {

        if (!BaseUI.stringEmpty(zip4)) {
            Integer zip4Int = Integer.valueOf(zip4);
            Boolean zip4WithinAcceptableParameters = zip4Int != null && zip4Int > 0 && zip4.length() == 4;
            softAssert.assertTrue(zip4WithinAcceptableParameters,
                    "Zip 4 was not an acceptable number for line " + String.valueOf(rowNumber) + ".");

        } else {
            softAssert.fail("Zip4 was empty for line " + String.valueOf(rowNumber));
        }

    }

    private static void check_LevelIsValid(SoftAssert softAssert, String actualLevel,
                                           HashMap<String, Range<Integer>> levelsAndRanges, int currentRow) {

        if (!BaseUI.stringEmpty(actualLevel)) {
            Boolean foundKey = false;
            // Verify Level and FICO Scores
            for (String key : levelsAndRanges.keySet()) {
                if (key.equals(actualLevel)) {
                    foundKey = true;
                    break;
                }
            }

            softAssert.assertTrue(foundKey,
                    "Level of " + actualLevel + " was not expected for row " + String.valueOf(currentRow) + ".");
        } else {
            softAssert.fail("Level was empty for line " + String.valueOf(currentRow));
        }
    }

    private static void check_ORDERRECORDID_IsValid(SoftAssert softAssert, String orderRecordID, int currentRow) {

        if (!BaseUI.stringEmpty(orderRecordID)) {
            Integer orderID = Integer.valueOf(orderRecordID);
            Boolean foundOrderRecord = orderID > 0;
            softAssert.assertTrue(foundOrderRecord, "Order Record ID NOT found" + String.valueOf(currentRow) + ".");

        } else {
            softAssert.fail("Order Record ID was empty for line " + String.valueOf(currentRow));

        }

    }

    private static void check_FICO_IsValid(SoftAssert softAssert, String actualFico, String level,
                                           HashMap<String, Range<Integer>> levelsAndRanges, int rowNumber) {
        if (!BaseUI.stringEmpty(actualFico)) {
            // Verify Fico Auto
            Integer ficoRange1 = levelsAndRanges.get(level).getMinimum();
            Integer ficoRange2 = levelsAndRanges.get(level).getMaximum();

            Integer ficoScore = Integer.valueOf(actualFico);
            Boolean ficoScoreWithinRange = ficoScore >= ficoRange1 && ficoScore <= ficoRange2;
            softAssert.assertTrue(ficoScoreWithinRange,
                    "Fico score " + actualFico + " was NOT between " + ficoRange1.toString() + " and "
                            + ficoRange2.toString() + " for row " + String.valueOf(rowNumber) + ".");

        } else {
            softAssert.fail("FICO was empty for line " + String.valueOf(rowNumber));
        }
    }

    private static void check_FICO_IsValid(SoftAssert softAssert, String actualFico,
                                           Range<Integer> expectedRange, int rowNumber) {
        if (!BaseUI.stringEmpty(actualFico)) {
            // Verify Fico Auto
            int ficoRange1 = expectedRange.getMinimum();
            int ficoRange2 = expectedRange.getMaximum();

            int ficoScore = Integer.valueOf(actualFico);
            Boolean ficoScoreWithinRange = ficoScore >= ficoRange1 && ficoScore <= ficoRange2;
            softAssert.assertTrue(ficoScoreWithinRange,
                    "Fico score " + actualFico + " was NOT between " + String.valueOf(ficoRange1) + " and "
                            + String.valueOf(ficoRange2) + " for row " + String.valueOf(rowNumber) + ".");

        } else {
            softAssert.fail("FICO was empty for line " + String.valueOf(rowNumber));
        }
    }

    private static void check_State_IsValid(SoftAssert softAssert, String actualState, String[] states, int row) {
        if (!BaseUI.stringEmpty(actualState)) {
            Boolean stateMatchesOneOfStates = false;
            for (String expectedState : states) {
                if (expectedState.equals(actualState)) {
                    stateMatchesOneOfStates = true;
                    break;
                }
            }
            softAssert.assertTrue(stateMatchesOneOfStates,
                    "State did not match expected list on row " + String.valueOf(row) + ".");
        } else {
            softAssert.fail("State was empty for line " + String.valueOf(row));
        }
    }

    private static void check_Zip_Matches(SoftAssert softAssert, String actualZip, String[] zips, int row) {
        if (!BaseUI.stringEmpty(actualZip)) {
            Boolean zipMatchesOneOfZips = false;
            for (String expectedState : zips) {
                if (expectedState.equals(actualZip)) {
                    zipMatchesOneOfZips = true;
                    break;
                }
            }
            softAssert.assertTrue(zipMatchesOneOfZips,
                    "Zip did not match expected list on row " + String.valueOf(row) + ".");
        } else {
            softAssert.fail("Zip was empty for line " + String.valueOf(row));
        }
    }

    private static void check_AreaCode_Matches(SoftAssert softAssert, String actualAreaCode, String[] areaCodes, int row) {

        softAssert.assertTrue(Arrays.asList(areaCodes).contains(actualAreaCode),
                "Area Code did not match expected list on row " + String.valueOf(row) + ".");

    }

    private static void check_Supplier_IsValid(SoftAssert softAssert, String actualSupplier, String[] suppliers,
                                               int row) {
        if (!BaseUI.stringEmpty(actualSupplier)) {
            Boolean supplierFound = false;
            for (String expectedSupplier : suppliers) {
                if (expectedSupplier.equals(actualSupplier)) {
                    supplierFound = true;
                    break;
                }
            }
            softAssert.assertTrue(supplierFound,
                    "Supplier did not match expected list on row " + String.valueOf(row) + ".");
        } else {
            softAssert.fail("Supplier was empty for line " + String.valueOf(row));
        }
    }

    public static void verify_FirstPartyData_CSV(TableData csvTable, String[] states, String[] suppliers,
                                                 HashMap<String, Range<Integer>> levelsAndRanges) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        csvTable.remove_Character_FromKeys("\"");
        csvTable.remove_Character("\"");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        int currentRow = 0;

        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> row : csvTable.data) {

                check_LNAME_WithFixedXs(softAssert, row.get("LNAME"), currentRow);
                check_FNAME_WithFixedXs(softAssert, row.get("FNAME"), currentRow);
                check_MI_With1X_OrNone(softAssert, row.get("MI"), currentRow);
                check_Address1_With_FixedXs(softAssert, row.get("ADDR1"), currentRow);
                check_ADDR2_WithFixedXs(softAssert, row.get("ADDR2"), currentRow);
                check_City_Isnt_Empty(softAssert, row.get("CITY"), currentRow);
                check_State_IsValid(softAssert, row.get("STATE"), states, currentRow);
                check_ValidZip(softAssert, row.get("ZIP"), currentRow);
                check_Valid_ZIP4(softAssert, row.get("ZIP4"), currentRow);
                check_LevelIsValid(softAssert, row.get("LEVEL"), levelsAndRanges, currentRow);
                check_Supplier_IsValid(softAssert, row.get("Supplier"), suppliers, currentRow);
                check_ORDERRECORDID_IsValid(softAssert, row.get("ORDERRECORDID"), currentRow);
                check_FICO_IsValid(softAssert, row.get(ficoText), row.get("LEVEL"), levelsAndRanges, currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }

    }

    // I need to update this method at some point to utilize "check" methods like
    // above
    public static void verify_RunProgram_CSV_MatchesExpected(TableData csvTable, String[] states,
                                                             HashMap<String, Range<Integer>> levelsAndRanges) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        String ficoText = csvTable.data.get(0).get("FICOAUTO");
        ficoText = ficoText == null ? "FICO" : "FICOAUTO";

        Integer currentRow = 0;

        SoftAssert softAssert = new SoftAssert();

        try {
            for (HashMap<String, String> row : csvTable.data) {

                String lastName = row.get("LNAME").trim();
                if (GlobalVariables.environment.equals("PROD")) {
                    softAssert.assertTrue(!BaseUI.stringEmpty(lastName),
                            "Last Name was not retrieved for row " + currentRow.toString());

                } else {
                    softAssert.assertTrue((lastName.length() > 0) && lastName.matches("^[X]+$"),
                            "Last Name was " + lastName + " but was expecting X's.");

                }

                // Verify First Name
                String firstName = row.get("FNAME").trim();
                if (GlobalVariables.environment.equals("PROD")) {
                    softAssert.assertTrue(!BaseUI.stringEmpty(firstName),
                            "First Name was not retrieved for row " + currentRow.toString());
                } else {
                    softAssert.assertTrue((firstName.length() > 0) && firstName.matches("^[X]+$"),
                            "First Name was " + firstName + " but was expecting X's.");
                }

                // Verify MI
                String middleInitial = row.get("MI").trim();
                check_MI_With1X_OrNone(softAssert, middleInitial, currentRow);

                // Validate address 1
                if (GlobalVariables.environment.equals("PROD")) {
                    softAssert.assertTrue(!BaseUI.stringEmpty(row.get("ADDR1").trim()),
                            "Address 1 was not found for row " + currentRow.toString());
                } else {
                    softAssert.assertTrue((row.get("ADDR1").length() > 0) && row.get("ADDR1").matches("^[X]+$"),
                            "Address 1 did not match X's");
                }

                // Verify Address 2
                if (GlobalVariables.environment.equals("PROD")) {
                    // Verify Address 2 - Nothing to verify for ADDR2 in unmasked field, it could be
                    // NULL, empty, or a value.
                } else {
                    String address2 = row.get("ADDR2");
                    address2 = address2 == null ? "" : address2;
                    Boolean address2IsAcceptableValue = BaseUI.stringEmpty(address2) || address2.matches("^[X]+$");
                    softAssert.assertTrue(address2IsAcceptableValue, "Address 2 was not an acceptable format.");
                }

                // Verify City
                check_City_Isnt_Empty(softAssert, row.get("CITY"), currentRow);

                // Verify State
                String state = row.get("STATE");
                check_State_IsValid(softAssert, state, states, currentRow);

                // Verify Zip
                check_ValidZip(softAssert, row.get("ZIP"), currentRow);

                // Verify Zip4
                String zip4 = row.get("ZIP4");

                if (GlobalVariables.environment.equals("PROD")) {
                    check_Valid_ZIP4(softAssert, zip4, currentRow);
                } else {
                    softAssert.assertTrue(zip4.matches("XXXX"),
                            "Zip 4 was not XXXX for row " + String.valueOf(currentRow) + ".");
                }

                check_LevelIsValid(softAssert, row.get("LEVEL"), levelsAndRanges, currentRow);

                // Verify OrderRecordID
                check_ORDERRECORDID_IsValid(softAssert, row.get("ORDERRECORDID"), currentRow);

                check_FICO_IsValid(softAssert, row.get(ficoText), row.get("LEVEL"), levelsAndRanges, currentRow);

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }
    }

    public static void verify_CSV_TIS_Custom_MatchesExpected(TableData csvTable, String expectedLevel) {
        BaseUI.verify_true_AndLog(csvTable.data.size() > 0, "Found records in CSV.", "Did not find records in CSV.");

        Integer currentRow = 0;
        SoftAssert softAssert = new SoftAssert();
        try {
            for (HashMap<String, String> csvRow : csvTable.data) {
                String actualLevel = csvRow.get("LEVEL");
                softAssert.assertTrue(actualLevel.equals(expectedLevel), "Row " + currentRow.toString() + ": "
                        + "Expected LEVEL of " + expectedLevel + ", but seeing " + actualLevel);

                String actualOrderRecordID = csvRow.get("ORDERRECORDID");
                Boolean orderRecordID_IsInteger = BaseUI.string_IsInteger(actualOrderRecordID);
                Boolean orderRecordID_IsGreaterThan0 = Integer.parseInt(actualOrderRecordID) > 0;
                softAssert.assertTrue(orderRecordID_IsInteger && orderRecordID_IsGreaterThan0,
                        "Row " + currentRow.toString() + ": " + "ORDERRECORDID of " + actualOrderRecordID
                                + " was not a valid Integer greater than 0.");

                String actualZip = csvRow.get("Zip");
                Boolean zip_IsInteger = BaseUI.string_IsInteger(actualZip);
                Boolean zip_IsValidLength = actualZip.length() == 5;
                softAssert.assertTrue(zip_IsInteger && zip_IsValidLength, "Row " + currentRow.toString() + ": "
                        + "Zip of " + actualZip + " was not a valid Integer of 5 characters.");

                String actualCity = csvRow.get("CITY");
                softAssert.assertTrue(!BaseUI.stringEmpty(actualCity.trim()), "Row " + currentRow.toString() + ": "
                        + "CITY of " + actualCity + " should not have been empty.");

                currentRow++;
            }
        } finally {
            softAssert.assertAll();
        }

    }


    public static void verify_DMA_RequestedCounts_MatchCSVCount_AndDB(TableData resultsTable, TableData csvTable, TableData dbData) {
        verify_RequestedCounts_MatchCSVCount_AndDB(resultsTable, csvTable, dbData, "DMA", "dmaname");
    }

    public static void verify_CITY_RequestedCounts_MatchCSVCount_AndDB(TableData resultsTable, TableData csvTable, TableData dbData) {
        verify_RequestedCounts_MatchCSVCount_AndDB(resultsTable, csvTable, dbData, "City", "city");
    }

    public static void verify_COUNTY_RequestedCounts_MatchCSVCount_AndDB(TableData resultsTable, TableData csvTable, TableData dbData) {
        verify_RequestedCounts_MatchCSVCount_AndDB(resultsTable, csvTable, dbData, "County", "county");
    }

    public static void verify_RequestedCounts_MatchCSVCount_AndDB(TableData resultsTable, TableData csvTable, TableData cityData,
                                                                  String fieldNameSite, String fieldNameDB) {
        SoftAssert softAssert = new SoftAssert();

        TableData actualCountData = new TableData();

        for (HashMap<String, String> resultsRow : resultsTable.data) {
            HashMap<String, String> actualCountRow = new HashMap<>();

            actualCountRow.put("Count", "0");
            actualCountRow.put(fieldNameSite, resultsRow.get(fieldNameSite));
            actualCountRow.put("Level", resultsRow.get("Level"));

            actualCountData.data.add(actualCountRow);
        }

        for (HashMap<String, String> csvTableRow : csvTable.data) {
            String zipCode = csvTableRow.get("ZIP");
            String level = csvTableRow.get("LEVEL");
            String city = cityData.return_Row_BasedOn_1MatchingField("zipcode", zipCode).get(fieldNameDB);
            HashMap<String, String> actualCountRow = actualCountData.return_Row_BasedOn_2MatchingFields(fieldNameSite, city, "Level", level);
            actualCountRow.put("Count", String.valueOf(Integer.valueOf(actualCountRow.get("Count")) + 1));
        }

        try {
            resultsTable.forEach_Row(a -> {
                int requestedAmount = Integer.valueOf(a.get("Requested"));
                int actualCount = Integer.valueOf(actualCountData.return_Row_BasedOn_2MatchingFields(fieldNameSite, a.get(fieldNameSite),
                        "Level", a.get("Level")).get("Count"));

                softAssert.assertEquals(actualCount, requestedAmount);
            });
        } finally {
            softAssert.assertAll();
        }


    }


    public static void verify_RequestedCounts_MatchCSVCounts(TableData resultsTable, TableData csvTable, String fieldNameResultsTable, String fieldNameCSVFile) {
        SoftAssert softAssert = new SoftAssert();
        try {
            resultsTable.forEach_Row(a -> {

                HashMap<String, String> rowToMatch = new HashMap<String, String>();
                rowToMatch.put(fieldNameCSVFile, a.get(fieldNameResultsTable));
                rowToMatch.put("LEVEL", a.get("Level"));

                int csvMatchingCount = csvTable.countOf_Matches_ThatHave_matchingHashMapValues(rowToMatch);
                int requestedCount = Integer.valueOf(a.get("Requested"));
                softAssert.assertEquals(csvMatchingCount, requestedCount);
            });
        } finally {
            softAssert.assertAll();
        }
    }

    public static void verify_Equifax_DB_PurchaseOrderRecord_Created(String orderID_OnScreen, Integer expectedTotal)
            throws Exception {
        String firstQuery_ToGetDB_OrderID = "select toorderid \r\n" + "from dbo.orderitemrelationships oir\r\n"
                + "join dbo.orderpartyroles opr on oir.toorderid = opr.orderid \r\n" + "where \r\n" + "	fromorderid = "
                + orderID_OnScreen + "\r\n" + "and fromroleid = 80 \r\n" + "and toroleid = 130\r\n"
                + "and opr.roleid = 78\r\n" + "and opr.partyid = 25662";

        DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;
        TableData firstQueryTable = DatabaseConnection.runSQLServerQuery(firstQuery_ToGetDB_OrderID);
        String dbOrderID = firstQueryTable.data.get(0).get("toorderid");

        String query_ToCheckEquifax = "select * from orderitems where orderid = " + dbOrderID;
        TableData secondQueryTable = DatabaseConnection.runSQLServerQuery(query_ToCheckEquifax);

        secondQueryTable.verify_TableSize(2);

        HashMap<String, String> ficoRowExpected = new HashMap<String, String>();
        ficoRowExpected.put("quantity", expectedTotal.toString());
        ficoRowExpected.put("title", "FICO");
        HashMap<String, String> baseRecordRow = new HashMap<String, String>();
        baseRecordRow.put("quantity", expectedTotal.toString());
        baseRecordRow.put("title", "Base Record");

        secondQueryTable.verify_MatchingRow_Found(ficoRowExpected);
        secondQueryTable.verify_MatchingRow_Found(baseRecordRow);
    }

}// End of Class
