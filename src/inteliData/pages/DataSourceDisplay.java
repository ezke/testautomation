package inteliData.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.Button_Dropdown;
import inteliData.page_controls.Button_Dropdown_ListItem;
import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.Expand_Section;
import inteliData.page_controls.TableBase;
import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class DataSourceDisplay {

	public static String dateUploaded_Format = "M/d/yyyy";
	public static DataPanel_MainContent details_Panel = new DataPanel_MainContent("Details");

	public static Expand_Section programs_Section = new Expand_Section("Programs");
	public static TableBase programsTable = new TableBase("Programs");

	public static Expand_Section suppressions_Section = new Expand_Section("Suppressions");
	public static TableBase suppressionsTable = new TableBase("Suppressions");

	public static Expand_Section currentFileMapping_Section = new Expand_Section("Current File Mapping");
	public static TableBase currentFileMapping = new TableBase("filemapping");

	public static final ArrayList<Button_Dropdown_ListItem> actionButtonList = actions_Options();
	public static final Button_Dropdown actionsButtonDropdown = new Button_Dropdown(actionButtonList, "Actions");

	public static ArrayList<Button_Dropdown_ListItem> actions_Options() {
		ArrayList<Button_Dropdown_ListItem> dropdown_Items = new ArrayList<Button_Dropdown_ListItem>();
		Button_Dropdown_ListItem create_suppressionItem = new Button_Dropdown_ListItem("Create Suppression") {
			@Override
			public void wait_ForPageToLoad() throws Exception {
				BaseUI.waitForElementToBeDisplayed("newAudienceModal_NewAudienceTitle", null, null);
				Thread.sleep(500);
			}
		};

		Button_Dropdown_ListItem create_AppendItem = new Button_Dropdown_ListItem("Create Append") {
			@Override
			public void wait_ForPageToLoad() throws Exception {
				BaseUI.waitForElementToBeDisplayed("newAudienceModal_NewAudienceTitle", null, null);
				Thread.sleep(500);
			}
		};

		Button_Dropdown_ListItem updateProgramItem = new Button_Dropdown_ListItem("Update Program") {
			@Override
			public void wait_ForPageToLoad() throws Exception {
				BaseUI.waitForElementToBeDisplayed("newAudienceModal_NewAudienceTitle", null, null);
				Thread.sleep(500);
			}
		};

		Button_Dropdown_ListItem editItem = new Button_Dropdown_ListItem("Edit") {
			@Override
			public void wait_ForPageToLoad() throws Exception {
				BaseUI.waitForElementToBeDisplayed("newAudienceModal_NewAudienceTitle", null, null);
				Thread.sleep(500);
			}
		};

		dropdown_Items.add(create_suppressionItem);
		dropdown_Items.add(create_AppendItem);
		dropdown_Items.add(updateProgramItem);
		dropdown_Items.add(editItem);
		return dropdown_Items;
	}

	// Date Uploaded should match format for variable dateUploaded_Format
	public static void verify_DetailsPanel(String sourceFile, String dateUploaded, Integer initialRecordCount,
			Integer recordsMapped) throws Exception {
		HashMap<String, String> detailsPanelData = details_Panel.return_PanelData();

		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(detailsPanelData.get("Id")), "Value was Integer.",
				"Value was NOT an Integer.");
		BaseUI.baseStringCompare("Source file", sourceFile, detailsPanelData.get("Source file"));
		BaseUI.baseStringCompare("Date Uploaded", dateUploaded, detailsPanelData.get("Date Uploaded"));
		BaseUI.baseStringCompare("Status", "Mapped", detailsPanelData.get("Status"));
		BaseUI.baseStringCompare("Records in source file", initialRecordCount.toString(),
				detailsPanelData.get("Records in source file"));
		BaseUI.baseStringCompare("Records Mapped", recordsMapped.toString(), detailsPanelData.get("Records Mapped"));
	}

	public static String return_OrderID() {
		HashMap<String, String> detailsPanelData = details_Panel.return_PanelData();
		return detailsPanelData.get("Id");
	}

	public static void verify_Title(String fileName) {
		WebElement titleElement = Locator.lookupElement("dataSourceDisplay_TitleElement");
		BaseUI.verifyElementAppears(titleElement);
		BaseUI.verifyElementHasExpectedText(titleElement, fileName);
	}

	// valuesToCheck has the Key set to Source and the Value set to Destination
	public static void verify_Mappings(TableData mappingTableData, HashMap<String, String> valuesToCheck) {
		for (String source : valuesToCheck.keySet()) {
			HashMap<String, String> mappingRow = mappingTableData.return_Row_BasedOn_2MatchingFields("Source", source, "Destination",
					valuesToCheck.get(source));
			BaseUI.verify_true_AndLog(mappingRow != null,
					"Found row matching source of " + source + " with Destination of " + valuesToCheck.get(source),
					"Did NOT find row matching source of " + source + " with Destination of "
							+ valuesToCheck.get(source));
		}

	}

}// End of Class
