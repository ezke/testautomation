package inteliData.pages;

import java.util.HashMap;

import inteliData.page_controls.DataPanel_MainContent;
import utils.BaseUI;

public class PackageDisplay {

	public static DataPanel_MainContent packageSummary_Panel = new DataPanel_MainContent("Package Summary");

	public static void wait_ForPageToLoad() throws Exception {
		BaseUI.waitForElementToBeDisplayed("packageDisplay_Header", null, null);
		Thread.sleep(500);
	}

	public static void verify_PackageSummary(String id, String name, String status) {
		HashMap<String, String> packageSummary_Details = packageSummary_Panel.return_PanelData();

		BaseUI.baseStringCompare("Id", id, packageSummary_Details.get("Id"));
		BaseUI.baseStringCompare("Name", name, packageSummary_Details.get("Name"));
		BaseUI.baseStringCompare("Status", status, packageSummary_Details.get("Status"));
	}

}// End of Class
