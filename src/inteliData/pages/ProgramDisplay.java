package inteliData.pages;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import inteliData.page_controls.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;
import utils.TableData;

public class ProgramDisplay {

    public static String dateFormat = "M/d/yyyy h:mm:ss a";

    public static DataPanel_MainContent mainContent_Panel = new DataPanel_MainContent("Program Summary");
    public static DataPanelBase level1_Panel = new DataPanelBase("Level 1 :");
    public static DataPanelBase level2_Panel = new DataPanelBase("Level 2 :");
    public static DataPanelBase autoOrderSettings_Panel = new DataPanelBase("Auto Order Settings") {
        @Override
        public HashMap<String, String> return_PanelData() {
            ArrayList<WebElement> rows = Locator.lookup_multipleElements("dataPanel_RowList", headerText, null);

            HashMap<String, String> rowData = new HashMap<>();
            for (WebElement row : rows) {
                WebElement headerElement = row.findElement(By.xpath(".//dt"));
                WebElement cellValue = null;
                try {
                    cellValue = row.findElement(By.xpath(".//dd"));
                }catch(NoSuchElementException e){
                    cellValue = null;
                }

                String cellValueText = cellValue == null ? "" : BaseUI.get_Attribute_FromField(cellValue, "innerText");

                rowData.put(BaseUI.get_Attribute_FromField(headerElement, "innerText"), cellValueText);
            }
            return format_Data(rowData);
        }

    };

    public static DataPanelBase generalInformation_Panel = new DataPanelBase("General Information");
    public static DataPanelBase globalGeography_Panel = new DataPanelBase("Global Geography");

    public static TableBase subscriptionFiles_Table = new TableBase("SubscriptionFiles");

    public static final Table_WithLinks audiencesTable = new Table_WithLinks("SubscriptionSearches") {

        @Override
        public void wait_AfterClick() throws Exception {

            WebElement waitElement = BaseUI
                    .waitForElementToBeDisplayed_ButDontFail("progDisplay_ClickView_ElementToWaitFor", null, null, 600);

            if (SearchDisplay.atSearchDisplayPage()) {
                SearchDisplay.wait_ForSearchOrdersTableToLoad();
            }
            if (waitElement != null) {
                Thread.sleep(500);
            } else {
                String logOutput = BaseUI.return_Browser_Output();
                BaseUI.log_AndFail(
                        "Timed out waiting for Download Records page to load. Showing browser output: \n" + logOutput);
            }
        }
    };

    private static void click_ActionsMenu_AndOption(String optionToPick) throws Exception {
        Button_Dropdown_V2 actionButtonControl = new Button_Dropdown_V2("progDisplay_Actions_Button", "progDisplay_Actions_ListItem", optionToPick);
        actionButtonControl.open_List_AndClickItem();

    }

    public static void click_Actions_EditProgram() throws Exception {
        click_ActionsMenu_AndOption("Edit Program");
        ProgramEdit.waitForPageToLoad();
    }


    public static void verify_SubscriptionFiles_FromToday_Appended_AndPreviousDay_Archived(String fileName, String location)
            throws Exception {
        subscriptionFiles_Table.sort_Header_Descending("Appended Date");
        TableData subscriptionFiles = subscriptionFiles_Table.return_TableData();

        //	LocalDateTime todayAtUpdateTime = LocalDate.now().atTime(16, 0);

        String expectedDate = "";

        expectedDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "M/d/yyyy");


        BaseUI.baseStringCompare("File Name from Today", fileName, subscriptionFiles.data.get(0).get("File Name"));
        String appendedDateToday = subscriptionFiles.data.get(0).get("Appended Date");
        appendedDateToday = BaseUI.getDate_WithFormat_X_Days_FromInitialString(appendedDateToday, dateFormat,
                "M/d/yyyy", 0);
        BaseUI.baseStringCompare("Append Date for Today", expectedDate, appendedDateToday);
        BaseUI.baseStringCompare("Today's Status", "Appended", subscriptionFiles.data.get(0).get("Status"));

        BaseUI.baseStringCompare("Previous Status", "Archived", subscriptionFiles.data.get(1).get("Status"));
        BaseUI.baseStringCompare("File Name from Yesterday", fileName, subscriptionFiles.data.get(1).get("File Name"));

    }

    // We are expecting there to not be a new entry with today's date.
    public static void verify_SubscriptionFiles_NoNewFile() throws Exception {
        TableData subscriptionFiles = subscriptionFiles_Table.return_TableData();

        BaseUI.verify_true_AndLog(subscriptionFiles.data.size() > 0, "Found Subscription File data.",
                "Did NOT find Subscription File data..");

        // Expecting first entry to say Appended as there were no new entries.
        BaseUI.verify_true_AndLog(subscriptionFiles.data.get(0).get("Status").equals("Appended"),
                "First Entry was Appended.", "First Entry was NOT Appended.");

        String todaysDate = BaseUI.getDateAsString_InRelationToTodaysDate(0, "M/d/yyyy");

        subscriptionFiles.forEach_Row(a -> {
            String appendedDate = a.get("Appended Date");
            try {
                appendedDate = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(appendedDate, dateFormat,
                        "M/d/yyyy", 0);
                BaseUI.verify_true_AndLog(!todaysDate.equals(appendedDate),
                        "Date listed on page was not equal to today's date.",
                        "Date listed on page was equal to today's date.");

            } catch (Exception e) {
                BaseUI.log_AndFail("Encountered error: " + e.getMessage());
            }

        });

    }

    public static void wait_ForPage_ToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("progDisplay_ElementToWaitFor", null, null, Duration.ofSeconds(30));
        BaseUI.waitForElementToBeDisplayed_ButDontFail("progDisplay_SubscriptionTable_FirstCell", null, null, 5);
        Thread.sleep(500);
    }

    public static void click_RunProgram() throws Exception {
        String textToInject = "Automation Script - Wait for this text to disappear.";
        BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("progDisplay_HeaderContainer"), textToInject);

        BaseUI.click(Locator.lookupElement("progDisplay_RunProgram_Button"));
        BaseUI.waitForElementToNOTContain_PartialAttributeMatch("progDisplay_HeaderContainer", null, null, "innerText",
                textToInject, 45);
        BaseUI.waitForElementToBeDisplayed("progDisplay_AudiencesTable_FirstCell", null, null, 45);
        Thread.sleep(500);
    }

    public static String return_AudienceID_From_RunProgramAlert() {

        String alertText = BaseUI.getTextFromField(Locator.lookupElement("progDisplay_ProgramRun_Alert")).trim();

        String audienceID = alertText.substring(alertText.lastIndexOf("for") + 4, alertText.lastIndexOf("to")).trim();

        return audienceID;
    }

    public static void click_SetupAutoOrder() throws Exception {
        BaseUI.click(Locator.lookupRequiredElement("progDisplay_SetupAutoOrder_Button"));
        Program_AutoOrderSetup_Page.waitForPageToLoad();
    }

    public static void verify_RunProgramAlert(String audienceID) {
        String actualText = BaseUI.getTextFromField(Locator.lookupElement("progDisplay_ProgramRun_Alert")).trim();

        String expectedText = "Your new audience has been successfully created. Please check the audiences list below for "
                + audienceID + " to check it's status.";

        BaseUI.baseStringPartialCompare("Run Program Alert Message", expectedText, actualText);
    }

    // Xpath indexing starts at 1.
    public static void click_Audiences_ViewLink_ByIndex(int index) throws Exception {
        audiencesTable.clickLink_ByRowIndex_ColumnName(index, "Actions");
    }

    public static void verify_ProgramSummary_Panel(String expectedName, String expectedMart, String expectedStartDate,
                                                   String expectedLastModifiedDate, String expectedEndDate, String expectedStatus) throws Exception {
        HashMap<String, String> programSummaryPanel = mainContent_Panel.return_PanelData();

        String id = programSummaryPanel.get("Id");
        BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(id) && Integer.valueOf(id) > 0, "Id was value greater than 0.",
                "Id of " + id + " was NOT an Integer greater than 0.");
        BaseUI.baseStringCompare("Name", expectedName, programSummaryPanel.get("Name"));
        BaseUI.baseStringCompare("Mart", expectedMart, programSummaryPanel.get("Mart"));
        BaseUI.baseStringCompare("Start Date", expectedStartDate, programSummaryPanel.get("Start Date"));
        BaseUI.verify_Date_IsAcceptable_DateRange(expectedLastModifiedDate, programSummaryPanel.get("Last Modified Date"),
                -5, 5, dateFormat);
        BaseUI.baseStringCompare("End Date", expectedEndDate, programSummaryPanel.get("End Date"));
        BaseUI.baseStringCompare("Status", expectedStatus, programSummaryPanel.get("Status"));
    }

}// End of Class
