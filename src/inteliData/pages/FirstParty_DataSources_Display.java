package inteliData.pages;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import inteliData.data.GlobalVariables;
import inteliData.page_controls.DataPanelBase;
import inteliData.page_controls.DataPanel_MainContent;
import utils.BaseUI;
import utils.Browser;
import utils.DataBuilder;
import utils.DatabaseConnection;
import utils.Locator;
import utils.TableData;

public class FirstParty_DataSources_Display {

	public static DataPanel_MainContent detailsPanel = new DataPanel_MainContent("Details");

	public static DataPanel_MainContent errorsPanel = new DataPanel_MainContent("Errors",
			"firstparty_DataSourcesDisplay_main_CellHeaders_ByMainHeader",
			"firstparty_DataSourcesDisplay_main_CellTexts_ByHeader",
			"firstparty_DataSourcesDisplay_main_CellTexts_AndCellHeader");

	public static void refresh_Until_Mapped(Duration maxTimeToWait) throws Exception {
		BaseUI.log_Status("Attempting to refresh until Mapped.");

		try {

			BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
				public Boolean call() throws Exception {
					refreshPage();

					String status = detailsPanel.return_SpecificValue("Status");
					if (status.equals("Mapped")) {
						return true;
					} else if (status.equals("Failed")) {
						BaseUI.log_AndFail(
								"Encountered status of Failed while attempting to wait for First Party Data to process.");
					}
					return false;
				}
			}, maxTimeToWait, Duration.ofSeconds(10));
		} catch (java.util.concurrent.TimeoutException e) {
			BaseUI.log_AndFail("Encountered error due to page becoming unresponsive - " + e.getClass());
		}

	}

	// Updates AccountId's. AccountId's will be different every time, but they will
	// increment by 1 from row to row. This logic is meant to update them in our
	// Standardized Expected Results table and then update the values in our other
	// table so we have accurate expected results.
	public static void apply_newIDs_ToExpectedTables(TableData tableWithIDs, TableData[] tablesToUpdate,
			int startingAccountNum, String idField) throws Exception {

		for (int i = 0; i < tableWithIDs.data.size(); i++) {
			String newAccountNumber = String.valueOf(startingAccountNum + i);
			String oldAccountNumber = tableWithIDs.data.get(i).get(idField);

			tableWithIDs.data.get(i).put(idField, newAccountNumber);
			// Need to update the AccountId's in Error table to match the new Id's
			Consumer<HashMap<String, String>> updateAccountId;
			updateAccountId = x -> {
				if (x.get(idField).equals(oldAccountNumber)) {
					x.put(idField, newAccountNumber);
				}
			};

			for (TableData table : tablesToUpdate) {
				table.forEach_Row(updateAccountId);
			}

		}

	}

	public static void refresh_Until_Mapped() throws Exception {

		try {
			refresh_Until_Mapped(Duration.ofSeconds(30));
		} catch (java.util.concurrent.TimeoutException e) {
			BaseUI.log_AndFail(
					"Encountered exception - java.util.concurrent.TimeoutException - Likely due to website becoming unresponsive");
		}
	}

	public static TableData return_products(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".products where fileid = @fileid";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();

		return databaseData;
	}

	public static TableData return_productPassThroughs(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".productPassThroughs where productid in (select productid from p" + marketerKey
				+ ".products where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.trim_column("Name");

		return databaseData;
	}

	public static TableData return_productValidationErrors(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".productValidationErrors where productid in (select productid from p" + marketerKey
				+ ".products where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();

		return databaseData;
	}

	public static TableData return_locations(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".Locations where fileid = @fileid";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();

		return databaseData;
	}

	public static TableData return_locationPassThroughs(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".LocationPassThroughs where locationid in (select locationid from p" + marketerKey
				+ ".Locations where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.trim_column("Name");
		databaseData.sort_ByColumn_Ascending("Name");
		databaseData.sort_ByColumn_numeric_Ascending("LocationId");

		return databaseData;
	}

	public static TableData return_locationValidationErrors(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".LocationValidationErrors where locationid in (select locationid from p" + marketerKey
				+ ".Locations where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();

		return databaseData;
	}

	public static TableData return_productGroups() throws Exception {
		String query = "select * from marketing.productgroups";

		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);

		return databaseData;
	}

	public static TableData return_accounts(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".Accounts where fileid = @fileid ";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.set_decimal_Precision(0, "StandardizedLatitude");
		databaseData.set_decimal_Precision(0, "StandardizedLongitude");
		databaseData.remove_Character(".", "StandardizedLatitude");
		databaseData.remove_Character(".", "StandardizedLongitude");

		return databaseData;
	}

	public static TableData return_accountPassThroughs(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".AccountPassThroughs where Accountid in (select Accountid from p" + marketerKey
				+ ".Accounts where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.trim_column("Name");

		return databaseData;
	}

	public static TableData return_accountValidationErrors(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select distinct * from p" + marketerKey
				+ ".AccountValidationErrors where Accountid in (select Accountid from p" + marketerKey
				+ ".Accounts where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.sort_ByColumn_Ascending("Error");

		return databaseData;
	}

	public static TableData return_CSVFile_ToCheckAgainst(String file) throws Exception {
		TableData csvToCheckAgainst = DataBuilder.returnTableData_ForComparison(file, ",", true);

		return csvToCheckAgainst;
	}

	public static TableData return_campaign(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".CampaignDrops where fileid = @fileid ";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.set_decimal_Precision(0, "StandardizedLatitude");
		databaseData.set_decimal_Precision(0, "StandardizedLongitude");
		databaseData.remove_Character(".", "StandardizedLatitude");
		databaseData.remove_Character(".", "StandardizedLongitude");


		return databaseData;
	}

	public static TableData return_campaignPassThroughs(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + "\r\n" + "select * from p" + marketerKey
				+ ".CampaignDropPassThroughs where campaigndropid  in (select campaigndropid from p" + marketerKey
				+ ".CampaignDrops where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.trim_column("Name");
		databaseData.sort_ByColumn_Ascending("Name");
		databaseData.sort_ByColumn_numeric_Ascending("CampaignDropId");

		return databaseData;
	}

	public static TableData return_campaignValidationErrors(String id, String marketerKey) throws Exception {
		String query = "declare @fileid int = " + id + " \n select * from p" + marketerKey
				+ ".CampaignDropValidationErrors where campaigndropid in (select campaigndropid from p" + marketerKey
				+ ".CampaignDrops where fileid = @fileid)";

		DatabaseConnection.dbUrl = GlobalVariables.firstPartyDBConnectionString;

		TableData databaseData = DatabaseConnection.runSQLServerQuery(query);
		databaseData.replace_Null_With_Empty();
		databaseData.sort_ByColumn_Ascending("Error");

		return databaseData;
	}

	public static String return_MarketerKey() {
		String marketer_TableKey = detailsPanel.return_SpecificValue("Marketer");
		marketer_TableKey = marketer_TableKey.substring(marketer_TableKey.indexOf("(") + 1,
				marketer_TableKey.indexOf(")"));

		return marketer_TableKey;
	}

	private static String[] validActiveValues = { "TRUE", "T", "1", "0", "Yes", "Y", "Active", "A", "", "F", "N",
			"Inactive", "I", "FALSE", "NO" };

	// Applies post office standardization to address columns
	public static TableData return_formatCSV_ToCASS(TableData csvToFormat) {

		return csvToFormat;
	}

	public static TableData format_CSVFile_ToMatchDB_ForProducts(TableData csvToFormat) {
		csvToFormat.remove_Character("\"");

		Set<String> values_ThatTranslate_To1 = new HashSet<String>(
				Arrays.asList(new String[] { "TRUE", "T", "1", "Yes", "Y", "Active", "A", "" }));

		Set<String> values_ThatTranslate_To0 = new HashSet<String>(
				Arrays.asList(new String[] { "F", "N", "Inactive", "I", "FALSE", "NO", "0" }));

		// Will contain the entries from previous sets.
		Set<String> values_ThatAreValid = new HashSet<String>();
		values_ThatAreValid.addAll(values_ThatTranslate_To1);
		values_ThatAreValid.addAll(values_ThatTranslate_To0);

		String nameOfActiveColumn = "Active";
		if (csvToFormat.data.get(0).get(nameOfActiveColumn) == null) {
			nameOfActiveColumn = "ActiveIndicator";
		}

		csvToFormat.updateColumn_WithReplacementValues(nameOfActiveColumn, values_ThatTranslate_To1, "1");
		csvToFormat.updateColumn_WithReplacementValues(nameOfActiveColumn, values_ThatTranslate_To0, "0");
		csvToFormat.updateColumn_WhereDoesNOTMatchSet_WithReplacementValue(nameOfActiveColumn, values_ThatAreValid,
				"0");

		if (csvToFormat.data.get(0).get("City") != null) {
			csvToFormat.capitalize_FirstLetter_ForColumn("City");
		}

		csvToFormat.trim_column("PT1");
		csvToFormat.trim_column("PT2");

		return csvToFormat;

	}

	public static void verify_DetailsPanel(String id, String name, String sourceFile, int quantityRecords,
			String marketer) {
		HashMap<String, String> detailsData = detailsPanel.return_PanelData();

		BaseUI.baseStringCompare("Id", id, detailsData.get("Id"));
		BaseUI.baseStringCompare("Name", name, detailsData.get("Name"));
		BaseUI.baseStringCompare("Source File", sourceFile, detailsData.get("Source File"));
		BaseUI.baseStringCompare("Quantity Records Uploaded", String.valueOf(quantityRecords),
				detailsData.get("Quantity Records Uploaded"));
		BaseUI.baseStringCompare("Marketer", marketer, detailsData.get("Marketer"));
	}

	public static void verify_ErrorsPanel(int validationCount, String addressStandardization, String latLongMatch) {
		HashMap<String, String> errorsData = errorsPanel.return_PanelData();
		BaseUI.baseStringCompare("Validation Count", String.valueOf(validationCount), errorsData.get("Validation"));
		BaseUI.baseStringCompare("Address Standardization", addressStandardization,
				errorsData.get("Address Standardization"));
		BaseUI.baseStringCompare("Lat / Long Match", latLongMatch, errorsData.get("Lat / Long Match"));
	}

	public static void refreshPage() throws Exception {
		String textToInject = "Automation - Testing Refresh";
		BaseUI.inject_TextNode_IntoElement(Locator.lookupElement("createAud_DownloadRecords_MainDiv"), textToInject);
		Thread.sleep(200);
		Browser.driver.navigate().refresh();
		BaseUI.waitForElementToNOTContain_PartialAttributeMatch("createAud_DownloadRecords_MainDiv", null, null,
				"innerText", textToInject, 120);
		BaseUI.waitForElementToBeDisplayed("firstparty_DataSourcesDisplay_Header", null, null, 40);
		Thread.sleep(500);
	}

	public static void verify_PriorLevelsPopulated_Error(TableData products, TableData errorTable) throws Exception {

		boolean foundLevel1MissingError = false;
		boolean foundLevel2MissingError = false;

		for (HashMap<String, String> row : products.data) {
			if (!BaseUI.stringEmpty(row.get("Level2"))) {
				if (BaseUI.stringEmpty(row.get("Level1"))) {
					foundLevel1MissingError = true;
					HashMap<String, String> expectedError = new HashMap<String, String>();
					expectedError.put("ProductId", row.get("ProductId"));
					expectedError.put("Error", "Level 1 is null or empty and Level 2 is populated");
					errorTable.verify_MatchingRow_Found(expectedError);
				}
			}

			if (!BaseUI.stringEmpty(row.get("Level3"))) {
				if (BaseUI.stringEmpty(row.get("Level2"))) {
					foundLevel2MissingError = true;
					HashMap<String, String> expectedError = new HashMap<String, String>();
					expectedError.put("ProductId", row.get("ProductId"));
					expectedError.put("Error", "Level 2 is null or empty and Level 3 is populated");
					errorTable.verify_MatchingRow_Found(expectedError);
				}
			}
		}

		BaseUI.verify_true_AndLog(foundLevel1MissingError, "Found a corresponding error for Level 1.",
				"Did NOT find a corresponding error for Level 1. Please check that CSV wasn't tampered with.");

		BaseUI.verify_true_AndLog(foundLevel2MissingError, "Found a corresponding error for Level 2.",
				"Did NOT find a corresponding error for Level 1. Please check that CSV wasn't tampered with.");

	}

	public static void verify_products_RowsWithoutErrors_ThrewNoExceptions(TableData products, TableData errorTable,
			TableData csvDataUnformatted, String[] requiredFields) {
		boolean foundGoodRow = false;
		boolean foundErrorRow = false;

		for (int i = 0; i < products.data.size(); i++) {

			boolean rowFailedCheck = false;
			boolean foundValidActiveStatus = false;

			if (!BaseUI.stringEmpty(products.data.get(i).get("Level2"))
					&& BaseUI.stringEmpty(products.data.get(i).get("Level1"))) {
				rowFailedCheck = true;
			}

			if (!BaseUI.stringEmpty(products.data.get(i).get("Level3"))
					&& BaseUI.stringEmpty(products.data.get(i).get("Level2"))) {
				rowFailedCheck = true;
			}

			String activeStatus = csvDataUnformatted.data.get(i).get("Active").trim();

			for (String activeValue : validActiveValues) {
				if (activeStatus.equals(activeValue)) {
					foundValidActiveStatus = true;
					break;
				}
			}

			if (!foundValidActiveStatus) {
				rowFailedCheck = true;
			}

			for (String requiredField : requiredFields) {
				if (BaseUI.stringEmpty(products.data.get(i).get(requiredField))) {
					rowFailedCheck = true;
				}
			}

			if (!rowFailedCheck) {
				foundGoodRow = true;
				errorTable.verify_Value_NOT_InColumn("ProductId", products.data.get(i).get("ProductId"));
			} else {
				foundErrorRow = true;
				errorTable.verify_Value_InColumn("ProductId", products.data.get(i).get("ProductId"));
			}

		}

		BaseUI.verify_true_AndLog(foundGoodRow, "Found a good row with no errors.",
				"Did NOT a good row with no errors. Check CSV was not tampered with.");
		BaseUI.verify_true_AndLog(foundErrorRow, "Found a row with errors.",
				"Did NOT a row with errors. Check CSV was not tampered with.");

	}

	// Verifies field was required. Also checks that there were errors for entries
	// that were missing field.
	public static void verify_WhenFieldRequired_IfNullOrEmpty_ErrorInErrorTable(TableData products,
			TableData errorTable, String fieldToCheck_productsName, String fieldToCheck_errorTableName, String idField)
			throws Exception {
		verify_FieldsRequired_IfNullOrEmpty_ErrorInErrorTable_WithGivenErrorMessage(products, errorTable,
				fieldToCheck_productsName, idField,
				"Required field is either null or empty : '" + fieldToCheck_errorTableName + "'");
	}

	// Verifies field was required. Also checks that there were errors for entries
	// that were missing field.
	public static void verify_FieldsRequired_IfNullOrEmpty_ErrorInErrorTable_WithGivenErrorMessage(TableData products,
			TableData errorTable, String fieldToCheck_productsName, String idField, String errorText) throws Exception {
		boolean foundEmptyEntry = false;

		String errorColumn = "Error";
		errorColumn = errorTable.data.get(0).get("Error") == null ? "error" : "Error";

		for (HashMap<String, String> row : products.data) {
			if (BaseUI.stringEmpty(row.get(fieldToCheck_productsName))) {
				foundEmptyEntry = true;

				HashMap<String, String> expectedRow = new HashMap<String, String>();
				expectedRow.put(idField, row.get(idField));
				expectedRow.put(errorColumn, errorText);

				errorTable.verify_MatchingRow_Found(expectedRow);
			}
		}

		BaseUI.verify_true_AndLog(foundEmptyEntry, "Found a corresponding empty for field " + fieldToCheck_productsName,
				"Did NOT find a corresponding empty field " + fieldToCheck_productsName
						+ ". Please check that CSV wasn't tampered with.");

	}

	public static void verify_PassThrough_Value(TableData products, TableData productPassThroughs,
			TableData csvToCheckAgainst_reformatted, String passThroughToCheck, String idFieldToUse) {

		Boolean emptyPT1Found = false;
		Boolean nonEmptyPT1Found = false;

		for (int i = 0; i < csvToCheckAgainst_reformatted.data.size(); i++) {
			String productID = products.data.get(i).get(idFieldToUse);

			HashMap<String, String> matchingRow = productPassThroughs.return_Row_BasedOn_2MatchingFields(idFieldToUse,
					productID, "Name", passThroughToCheck);

			String csv_PassThroughValue = csvToCheckAgainst_reformatted.data.get(i).get(passThroughToCheck);

			if (BaseUI.stringEmpty(csv_PassThroughValue)) {
				emptyPT1Found = true;
			} else {
				nonEmptyPT1Found = true;
			}

			BaseUI.baseStringCompare("Name", csv_PassThroughValue, matchingRow.get("Value"));
		}

		BaseUI.verify_true_AndLog(emptyPT1Found,
				"Found an empty " + passThroughToCheck + ".  Checking to prevent false positive.",
				"Did NOT find empty " + passThroughToCheck + ".  Check CSV to make sure it wasn't tampered with.");

		BaseUI.verify_true_AndLog(nonEmptyPT1Found,
				"Found a non empty " + passThroughToCheck + ".  Checking to prevent false positive.",
				"Did NOT find non empty " + passThroughToCheck + ".  Check CSV to make sure it wasn't tampered with.");
	}

	public static void verify_product_Active_Option_ThatIsntAcceptable_ThrowsException(TableData products,
			TableData productErrorTable, TableData csvToCheckAgainst, String incorrectValue, String idField)
			throws Exception {

		String activeText = csvToCheckAgainst.data.get(0).get("Active") == null ? "ActiveIndicator" : "Active";

		int rowWeWant = csvToCheckAgainst.first_IndexOf(activeText, incorrectValue);

		HashMap<String, String> dbRowWithInvalidActive = products.data.get(rowWeWant);

		HashMap<String, String> expectedRow = new HashMap<String, String>();
		expectedRow.put(idField, dbRowWithInvalidActive.get(idField));
		expectedRow.put("Error", activeText + ": " + incorrectValue
				+ " does not match any expected values. Converted value defaulted to False.");

		productErrorTable.verify_MatchingRow_Found(expectedRow);
	}

	public static void verify_ProductGroupIdEmpty_WhenProductGroupEmpty(TableData productsTable) throws Exception {

		TableData productGroupsTable = return_productGroups();
		for (HashMap<String, String> row : productsTable.data) {
			// Blah wouldn't have a matching product group id.
			// If Product Group is NOT empty or "blah", we check that ProductGroupId matches
			// between product groups table and product table.
			if (!row.get("ProductGroup").equals("") && !row.get("ProductGroup").equals("Blah")) {
				HashMap<String, String> matchingProductGroupRow = productGroupsTable
						.return_Row_BasedOn_1MatchingField("Name", row.get("ProductGroup"));
				BaseUI.baseStringCompare("Product Group Id", row.get("ProductGroupId"),
						matchingProductGroupRow.get("ProductGroupId"));

			} else {
				// else we check that products table has a ProductGroupId that is empty.
				BaseUI.verify_true_AndLog(BaseUI.stringEmpty(row.get("ProductGroupId")),
						"Product Group Id was empty when Product Group was empty.",
						"Product Group Id should be empty if Product Group is empty.");
			}
		}

	}

	// Verifies field was required. Also checks that there were errors for entries
	// that were missing field.
	public static void verify_Field_Required_Null(TableData products, TableData errorTable,
			String fieldToCheck_productsName, String fieldToCheck_errorTableName) throws Exception {
		boolean foundEmptyEntry = false;
		for (HashMap<String, String> row : products.data) {
			if (BaseUI.stringEmpty(row.get(fieldToCheck_productsName))) {
				foundEmptyEntry = true;

				HashMap<String, String> expectedRow = new HashMap<String, String>();
				expectedRow.put("ProductId", row.get("ProductId"));
				expectedRow.put("Error", "Required field is null : '" + fieldToCheck_errorTableName + "'");

				errorTable.verify_MatchingRow_Found(expectedRow);
			}
		}

		BaseUI.verify_true_AndLog(foundEmptyEntry, "Found a corresponding empty for field " + fieldToCheck_productsName,
				"Did NOT find a corresponding empty for field " + fieldToCheck_productsName
						+ ". Please check that CSV wasn't tampered with.");

	}

	public static TableData return_Account_ExpectedStandardizedResults(String fileName) throws Exception {
		TableData account_CSV_Standardized_ExpectedResults = return_CSVFile_ToCheckAgainst(fileName);
		account_CSV_Standardized_ExpectedResults.replace_Null_With_Empty();
		account_CSV_Standardized_ExpectedResults.remove_Character("NULL");
		account_CSV_Standardized_ExpectedResults.replaceValue_ForColumn_WhenEqualTo("StandardizedLatitude", "0", "0.0");
		account_CSV_Standardized_ExpectedResults.replaceValue_ForColumn_WhenEqualTo("StandardizedLongitude", "0",
				"0.0");
		account_CSV_Standardized_ExpectedResults.set_decimal_Precision(0, "StandardizedLatitude");
		account_CSV_Standardized_ExpectedResults.set_decimal_Precision(0, "StandardizedLongitude");
		account_CSV_Standardized_ExpectedResults.remove_Character(".", "StandardizedLatitude");
		account_CSV_Standardized_ExpectedResults.remove_Character(".", "StandardizedLongitude");

		return account_CSV_Standardized_ExpectedResults;
	}

}// End of Class
