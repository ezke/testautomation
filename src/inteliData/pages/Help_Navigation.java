package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

//This class is for the Help Navigation title, search  and navigation functionality at the top of the Help See What's New and Help How Do I pages

public class Help_Navigation {

	public static void verify_Title_Appears() {
		WebElement titleElement = Locator.lookupElement("help_nav_Title", null, null);
		BaseUI.verifyElementAppears(titleElement);
		BaseUI.verifyElementHasExpectedText(titleElement, "Intelidata Express");
	}

	public static void verify_SearchBox_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("help_nav_SearchBox"));
		BaseUI.verifyElementAppears(Locator.lookupElement("help_nav_SearchButton"));

	}

}
