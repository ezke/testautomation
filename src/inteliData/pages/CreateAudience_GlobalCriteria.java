package inteliData.pages;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

import inteliData.page_controls.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

public class CreateAudience_GlobalCriteria {

    // This table specific to Append
    public static final Table_WithFooter matchReport = new Table_WithFooter("createAud_GlobalCrit_MatchReport_TableBody",
            "createAud_GlobalCrit_MatchReport_TableHeaders", "createAud_GlobalCrit_MatchReport_TableFooters");

    public static final Expand_Section_LeftPanel globalSuppression = new Expand_Section_LeftPanel("Global Suppressions");
    public static final Expand_Section_LeftPanel globalRejects = new Expand_Section_LeftPanel("Global Rejects");
    public static final Expand_Section_LeftPanel globalGeography = new Expand_Section_LeftPanel("Global Geography");

    //Inclusion controls
    public static final Suppress_ByOrder_Control suppressByOrder = new Suppress_ByOrder_Control("Suppress by order");
    public static final CheckboxBase useGlobalGeography_Checkbox = new CheckboxBase("Use Global Geography");
    public static final CheckboxBase include_phone_number_suppressions_Checkbox = new CheckboxBase("Include phone number suppressions");
    public static final StateSelector stateSelector = new StateSelector("SelectedStates");
    public static final ZipCodeRadiusSelector zip_RadiusSelector = new ZipCodeRadiusSelector();

    public static final TextBox_WithLinkWrapper_WithDropdownTypeDetection include_Cities_TextBox =
            TextBox_WithLinkWrapper_WithDropdownTypeDetection.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_City_SearchTextBox",
                    "createAud_GlobalCrit_Include_City_ListOfEntries");
    public static final TextBox_WithLinkWrapper include_DirectMarketingAreas_TextBox =
            TextBox_WithLinkWrapper.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_DirectMarketingAreas_SearchTextBox",
                    "createAud_GlobalCrit_Include_DirectMarketingAreas_ListOfEntries");
    public static final TextBox_WithLinkWrapper_WithDropdownTypeDetection include_County_TextBox =
            TextBox_WithLinkWrapper_WithDropdownTypeDetection.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_County_SearchTextBox",
                    "createAud_GlobalCrit_Include_County_ListOfEntries");
    public static final TextBox_WithLinkWrapper include_ZipCodes_TextBox =
            TextBox_WithLinkWrapper.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_ZipCode_SearchTextBox",
                    "createAud_GlobalCrit_Include_ZipCode_ListOfEntries");

    public static final TextBox_WithLinkWrapper include_AreaCodes_TextBox =
            TextBox_WithLinkWrapper.getTextBox_WithSearchTextIdentifier_AndListOfEntriesIdentifier("createAud_GlobalCrit_Include_AreaCode_SearchTextBox",
                    "createAud_GlobalCrit_Include_AreaCode_ListOfEntries");

    public static final ZipCodes_ChooseFile zipCodes_ChooseFile = new ZipCodes_ChooseFile("createAud_GlobalCrit_ZipCode_ChooseFileInput");


    //Exclusion controls
    public static final StateSelector exclude_StateSelector = new StateSelector("ExcludedStates");

    public static final TextBox_WithLinkWrapper exclude_ZipCodes_TextBox = TextBox_WithLinkWrapper.getTextBox_ByIdentifyingText("Zip Codes");
    public static final TextBox_WithLinkWrapper exclude_AreaCodes_TextBox = TextBox_WithLinkWrapper.getTextBox_ByIdentifyingText("Area Codes");
    public static final TextBox_WithLinkWrapper exclude_DirectMarketingAreas_TextBox =
            TextBox_WithLinkWrapper.getTextBox_ByIdentifyingText("Direct Marketing Areas");
    public static final TextBox_WithLinkWrapper_WithDropdownTypeDetection exclude_Cities_TextBox =
            TextBox_WithLinkWrapper_WithDropdownTypeDetection.getTextBox_ByIdentifyingText("Cities");
    public static final TextBox_WithLinkWrapper_WithDropdownTypeDetection exclude_Counties_TextBox =
            TextBox_WithLinkWrapper_WithDropdownTypeDetection.getTextBox_ByIdentifyingText("County");


    public static void click_Exclude() throws Exception {
        ((JavascriptExecutor) Browser.driver).executeScript("window.scrollTo(0, 0)");
        BaseUI.click(Locator.lookupElement("createAud_GlobalCrit_Exclude_Button"));
        BaseUI.waitForElementToContain_PartialAttributeMatch("createAud_GlobalCrit_Exclude_Button", null, null, "class", "active");
        Thread.sleep(500);
    }

    public static void verify_ZipCodesUploadMessage(int numberZips, String nameOfFile) throws Exception {
        String expectedMessage = "You have " + String.valueOf(numberZips) +
                " zip codes for this audience based on the " + nameOfFile +
                " file. If you would like to use the same zip code file do nothing, otherwise upload a new file which will overwrite your existing zips or check the box below to remove the zip codes completely.";

        WebElement zipCodeMessage = Locator.lookupElement("createAud_GlobalCrit_ZipCode_UploadMessage");
        BaseUI.scroll_element_to_middleOfScreen(zipCodeMessage);

        BaseUI.verifyElementAppears(zipCodeMessage);
        BaseUI.verifyElementHasExpectedText(zipCodeMessage, expectedMessage);

    }


    public static void wait_For_PageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("createAud_GlobalCrit_ActiveHeader", null, null);
        Thread.sleep(500);
    }

    public static String return_AddressSuppressionLevel() {
        String dropdownValue = BaseUI.getSelectedOptionFromDropdown("createAud_DefineLevs_SuppressionLevel_Dropdown");
        return dropdownValue;
    }

    public static String return_SelectedSuppression_OrderName() {
        String orderName = suppressByOrder.return_First_Option_InSelectedList();
        orderName = orderName.split("\\(", 2)[0].trim();
        return orderName;
    }

    public static void click_SaveAndContinue() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_GlobalCrit_SaveAndContinue_Button"));
        BaseUI.waitForElementToBeDisplayed("createAud_GlobalCrit_ClickAndSave_ElementToWaitFor", null, null, Duration.ofSeconds(20));
        Thread.sleep(500);
        //CreateAudience_DefineLevels.wait_ForPage_ToLoad();
    }

    // Assumes level text is same for all suppliers
    public static void verify_MatchReport(String[] suppliers, int expectedImportedRecords, int expectedDistinctRecords) {
        TableData matchReportTable = matchReport.return_TableData();
        HashMap<String, String> matchReportFooter = matchReport.return_footerRow();

        // Table size should match the number of suppliers.
        matchReportTable.verify_TableSize(suppliers.length);

        // Verify all our suppliers are found.
        for (String supplier : suppliers) {
            matchReportTable.verify_Value_InColumn("Supplier", supplier);
        }

        Integer expectedMatchedRecordsTotalCount = 0;
        Integer expectedIncrementalRecordsCount = 0;

        int importedRecords = Integer.parseInt(BaseUI.getTextFromField(Locator.lookupRequiredElement(
                "createAud_GlobalCrit_MatchRecords_ImportedRecords")).trim().replace(",",""));
        int distinctRecords = Integer.parseInt(BaseUI.getTextFromField(Locator.lookupRequiredElement(
                "createAud_GlobalCrit_MatchRecords_DistinctRecords")).trim().replace(",",""));
        int standardizedRecords = Integer.parseInt(BaseUI.getTextFromField(Locator.lookupRequiredElement(
                "createAud_GlobalCrit_MatchRecords_StandardizedRecords")).trim().replace(",",""));

        BaseUI.verify_true_AndLog(expectedImportedRecords == importedRecords,
                "Import Records matched " + String.valueOf(importedRecords),
                "Import Records expected " + String.valueOf(expectedImportedRecords) +" Seeing " + String.valueOf(importedRecords));

        BaseUI.verify_true_AndLog(expectedDistinctRecords == distinctRecords,
                "Distinct Records matched " + String.valueOf(distinctRecords),
                "Distinct Records expected " + String.valueOf(expectedDistinctRecords) +" Seeing " + String.valueOf(distinctRecords));

        BaseUI.verify_true_AndLog(standardizedRecords > 0, "Found Standardized Records", "Did NOT find Standardized Records");

        // Verify that our Counts are all integers greater than 0.
        for (HashMap<String, String> row : matchReportTable.data) {
            Integer totalCount = Integer.parseInt(row.get("Matched Records").replace(",", ""));
            expectedMatchedRecordsTotalCount += totalCount;
            BaseUI.verify_true_AndLog(totalCount > 0, "Found Matched Records that was greater than 0.",
                    "Did NOT find Matched Records that was greater than 0.");
            Integer distinctCount = Integer.parseInt(row.get("Incremental Records").replace(",", ""));
            expectedIncrementalRecordsCount += distinctCount;
            BaseUI.verify_true_AndLog(distinctCount > 0, "Found Incremental Records Count that was greater than 0.",
                    "Did NOT find Incremental Records Count that was greater than 0.");
        }

        Integer footerMatchedRecords = Integer.parseInt(matchReportFooter.get("Matched Records").replace(",", ""));
        Integer footerDistinctCount = Integer.parseInt(matchReportFooter.get("Incremental Records").replace(",", ""));

        BaseUI.verify_true_AndLog(footerMatchedRecords.equals(expectedMatchedRecordsTotalCount),
                "Footer Matched Records Count matched " + expectedMatchedRecordsTotalCount.toString(), "Expected Footer Matched Records Count of "
                        + expectedMatchedRecordsTotalCount.toString() + " but seeing " + footerMatchedRecords.toString());

        BaseUI.verify_true_AndLog(footerDistinctCount.equals(expectedIncrementalRecordsCount),
                "Footer Distinct Count matched " + expectedIncrementalRecordsCount.toString(),
                "Expected Footer Distinct Count of " + expectedIncrementalRecordsCount.toString() + " but seeing "
                        + footerDistinctCount.toString());
    }

}// End of Class
