package inteliData.pages;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.Callable;

import org.openqa.selenium.WebElement;

import inteliData.data.GlobalVariables;
import inteliData.page_controls.DataPanel_MainContent;
import inteliData.page_controls.TableBase;
import utils.BaseUI;
import utils.DatabaseConnection;
import utils.Locator;
import utils.TableData;

public class SuppressionCompleted {

	public static String dateUploaded_Format = "M/d/yyyy h:mm:ss a";
	public static DataPanel_MainContent details_Panel = new DataPanel_MainContent("Details");

	public static TableBase suppressionLevelsTable = new TableBase("suppressionCompleted_SuppressionLevels_TableBody",
			"suppressionCompleted_SuppressionLevels_TableHeaders") {
	};

	public static TableData return_TestData_ForPT243(Integer finalRecords) {
		TableData expectedTable = new TableData();
		// Hard coded these values. Carlene said they shouldn't change.
		HashMap<String, String> row1 = new HashMap<String, String>();
		row1.put("Level", "Individual");
		row1.put("Total Count", finalRecords.toString());
		row1.put("Distinct Count", finalRecords.toString());
		row1.put("Description", "Name and Street Address");
		HashMap<String, String> row2 = new HashMap<String, String>();
		row2.put("Level", "Phone");
		row2.put("Total Count", "54");
		row2.put("Distinct Count", "54");
		row2.put("Description", "10 Digit Phone Number");

		HashMap<String, String> row3 = new HashMap<String, String>();
		row3.put("Level", "Address");
		row3.put("Total Count", finalRecords.toString());
		row3.put("Distinct Count", finalRecords.toString());
		row3.put("Description", "Street Address");

		expectedTable.data.add(row1);
		expectedTable.data.add(row2);
		expectedTable.data.add(row3);

		return expectedTable;
	}

	public static TableData return_TestData_ForPT244(Integer finalRecords) {
		TableData expectedTable = new TableData();
		// Hard coded these values. Carlene said they shouldn't change.
		HashMap<String, String> row1 = new HashMap<String, String>();
		row1.put("Level", "Individual");
		row1.put("Total Count", finalRecords.toString());
		row1.put("Distinct Count", finalRecords.toString());
		row1.put("Description", "Name and Street Address");
		HashMap<String, String> row2 = new HashMap<String, String>();
		row2.put("Level", "Phone");
		row2.put("Total Count", "0");
		row2.put("Distinct Count", "0");
		row2.put("Description", "10 Digit Phone Number");

		HashMap<String, String> row3 = new HashMap<String, String>();
		row3.put("Level", "Address");
		row3.put("Total Count", finalRecords.toString());
		row3.put("Distinct Count", finalRecords.toString());
		row3.put("Description", "Street Address");

		expectedTable.data.add(row1);
		expectedTable.data.add(row2);
		expectedTable.data.add(row3);

		return expectedTable;
	}
	
	public static void wait_For_AllDBs_ToContainSuppression(String accountID, String orderID)
	{
		wait_ForDB_ToContain_Suppression(GlobalVariables.suppression_Ful1, accountID, orderID);
		wait_ForDB_ToContain_Suppression(GlobalVariables.suppression_Mart1, accountID, orderID);
		wait_ForDB_ToContain_Suppression(GlobalVariables.suppression_Mart2, accountID, orderID);
	}
	
	public static void wait_ForDB_ToContain_Suppression(String connectionString, String accountID, String orderID) {
		try {
			BaseUI.wait_ForCondition_ToBeMet(new Callable<Boolean>() {
				public Boolean call() throws Exception {
					TableData sqlTable;
					DatabaseConnection.dbUrl = connectionString;

					String query = "select top 1 * from " + accountID + " where orderid =  " + orderID;

					sqlTable = DatabaseConnection.runSQLServerQuery(query);

					return sqlTable.data.size() > 0;
				}
			}, 300, 1000);
		} catch (Exception e) {
			BaseUI.log_AndFail("Encountered error while waiting for suppression to exist in database. Exception: "
					+ e.getMessage());
		}
	}

	public static TableData return_suppressionDB_Data(String connectionString, String accountID, String orderID)
			throws Exception, SQLException {
		TableData sqlTable;
		DatabaseConnection.dbUrl = connectionString;

		String query = "select * from " + accountID + " where orderid =  " + orderID;

		sqlTable = DatabaseConnection.runSQLServerQuery(query);

		return sqlTable;
	}

	public static void verify_Suppression_DatabaseTable_LooksAcceptable(String connectionString, String accountID,
			String orderID, Integer expectedCount) throws Exception {
		TableData sqlTable = return_suppressionDB_Data(connectionString, accountID, orderID);

		// verify orderid all match the order id from the website
		BaseUI.verify_TableColumn_AllColumnValues_MatchString("orderid", orderID, sqlTable.data);

		// verify table size matches expected.
		BaseUI.verify_true_AndLog(sqlTable.data.size() == expectedCount,
				"Table matched size of " + expectedCount.toString(), "Table did NOT match size of "
						+ expectedCount.toString() + " Seeing " + String.valueOf(sqlTable.data.size()));

		sqlTable.verify_Column_NotEmptyOrNull("orderrecordid");
		sqlTable.verify_Column_NotEmptyOrNull("checksum10");
		sqlTable.verify_Column_NotEmptyOrNull("matchcode02");
		sqlTable.verify_Column_NotEmptyOrNull("matchcode05");
		sqlTable.verify_Column_NotEmptyOrNull("adkey01");

	}

	// DNS Suppression will use the name field entered on previous field
	// External Suppression will display file name.
	public static void verify_Title(String fileName) {
		WebElement titleElement = Locator.lookupElement("suppressionCompleted_TitleElement");
		BaseUI.verifyElementAppears(titleElement);
		BaseUI.verifyElementHasExpectedText(titleElement, fileName);
	}

	public static String return_OrderID() {
		HashMap<String, String> detailsPanelData = details_Panel.return_PanelData();
		return detailsPanelData.get("Id");
	}

	public static void verify_ExternalSuppressions_DetailsPanel(String poNumber, String account, String marketer,
			String createdBy, Integer actualQuantity, String uploadTime, String fileName, Integer initialRecordCount,
			String suppressionType) throws Exception {
		HashMap<String, String> detailsPanelData = details_Panel.return_PanelData();

		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(detailsPanelData.get("Id")), "Value was Integer.",
				"Value was NOT an Integer.");
		BaseUI.baseStringCompare("PO Number", poNumber, detailsPanelData.get("PO Number"));
		BaseUI.baseStringCompare("Status", "Complete", detailsPanelData.get("Status"));
		BaseUI.baseStringCompare("Account", account, detailsPanelData.get("Account"));
		BaseUI.baseStringCompare("Marketer", marketer, detailsPanelData.get("Marketer"));
		BaseUI.baseStringCompare("Created By", createdBy, detailsPanelData.get("Created By"));
		BaseUI.baseStringCompare("Quantity", actualQuantity.toString(), detailsPanelData.get("Quantity"));
		BaseUI.baseStringCompare("Created By", createdBy, detailsPanelData.get("Created By"));
		String uploadTimeMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				dateUploaded_Format, dateUploaded_Format, -2);
		String uploadTimePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				dateUploaded_Format, dateUploaded_Format, 2);
		// Date Uploaded
		// 1/31/2018 8:54:05 AM
		BaseUI.verify_Date_IsBetween_DateRange(uploadTimeMinus2Minutes, uploadTimePlus2Minutes, uploadTime,
				dateUploaded_Format);
		BaseUI.baseStringCompare("File Name", fileName, detailsPanelData.get("File Name"));
		BaseUI.baseStringCompare("Records In Source File", initialRecordCount.toString(),
				detailsPanelData.get("Records In Source File"));
		BaseUI.baseStringCompare("Verified Record Count", actualQuantity.toString(),
				detailsPanelData.get("Verified Record Count"));

		BaseUI.baseStringCompare("Search Type", "Any", detailsPanelData.get("Search Type"));
		BaseUI.baseStringCompare("Suppression Type", suppressionType, detailsPanelData.get("Suppression Type"));
	}

	public static void verify_DNSSuppressions_DetailsPanel(String account, String marketer, String createdBy,
			Integer actualQuantity, String uploadTime, String fileName, Integer initialRecordCount,
			String suppressionType) throws Exception {
		HashMap<String, String> detailsPanelData = details_Panel.return_PanelData();

		BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(detailsPanelData.get("Id")), "Value was Integer.",
				"Value was NOT an Integer.");
		// PO Number doesn't get used for DNS Suppressions, might be removed in the
		// future.
		BaseUI.baseStringCompare("PO Number", "", detailsPanelData.get("PO Number"));
		BaseUI.baseStringCompare("Status", "Complete", detailsPanelData.get("Status"));
		BaseUI.baseStringCompare("Account", account, detailsPanelData.get("Account"));
		BaseUI.baseStringCompare("Marketer", marketer, detailsPanelData.get("Marketer"));
		BaseUI.baseStringCompare("Created By", createdBy, detailsPanelData.get("Created By"));
		BaseUI.baseStringCompare("Quantity", actualQuantity.toString(), detailsPanelData.get("Quantity"));
		BaseUI.baseStringCompare("Created By", createdBy, detailsPanelData.get("Created By"));
		String uploadTimeMinus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				dateUploaded_Format, dateUploaded_Format, -2);
		String uploadTimePlus2Minutes = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(uploadTime,
				dateUploaded_Format, dateUploaded_Format, 2);
		// Date Uploaded
		// 1/31/2018 8:54:05 AM
		BaseUI.verify_Date_IsBetween_DateRange(uploadTimeMinus2Minutes, uploadTimePlus2Minutes, uploadTime,
				dateUploaded_Format);

		BaseUI.baseStringCompare("Records In Source File", initialRecordCount.toString(),
				detailsPanelData.get("Records In Source File"));
		BaseUI.baseStringCompare("Verified Record Count", actualQuantity.toString(),
				detailsPanelData.get("Verified Record Count"));

		BaseUI.baseStringCompare("Search Type", "Any", detailsPanelData.get("Search Type"));
		BaseUI.baseStringCompare("Suppression Type", suppressionType, detailsPanelData.get("Suppression Type"));
		BaseUI.baseStringCompare("File Name", fileName, detailsPanelData.get("File Name"));
	}

}// Suppression Completed page
