package inteliData.pages;

import java.time.Duration;

import inteliData.page_controls.DataPanelBase;
import utils.BaseUI;
import utils.Locator;

//Define Levels page that we see after defining our levels.
public class CreateAudience_DefineLevels_LevelListPage {

	public static DataPanelBase level1_Panel = new DataPanelBase("Level 1 :");

	public static DataPanelBase level2_Panel = new DataPanelBase("Level 2 :");

	public static void click_Continue_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_DefineLevs_LvlList_ContinueButton"));
		CreateAudience_SearchReview.wait_ForSearchReviewPage_ToLoad();
	}

	public static void verify_Level1PanelHeader(String levelName) {
		String expectedText = "Level 1 : " + levelName;
		level1_Panel.verify_HeaderText_Matches(expectedText);
	}

	public static void click_AddLevel_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_DefineLevs_LvlList_AddLevel_Button"));
		CreateAudience_DefineLevels.wait_ForPage_ToLoad();
	}

	public static void click_EditLevel_Button() throws Exception {
		BaseUI.click(Locator.lookupElement("createAud_DefineLevs_LvlList_EditLevel_Button"));
		BaseUI.waitForElementToBeDisplayed("createAud_DefineLevs_BaseLineSelects_Header", null, null);
		Thread.sleep(500);
		
	}
	
	
	// Not looking for exact match, using contains solution
	public static void select_Level(String levelToPick) throws Exception {
		BaseUI.selectValueFromDropDownByPartialText(Locator.lookupElement("createAud_DefineLevs_LvlList_LevelDropdown"),
				levelToPick);
		BaseUI.waitForElementToBeDisplayed("createAud_DefineLevs_LvlList_LevelHeader_ByLevelName", levelToPick, null,
				Duration.ofSeconds(20));
		Thread.sleep(500);
	}

}// End of Class
