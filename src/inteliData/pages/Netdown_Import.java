package inteliData.pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;

import inteliData.page_controls.DataPanel_MainContent;
import utils.BaseUI;
import utils.Locator;

public class Netdown_Import {

	public static DataPanel_MainContent netdownSummary_Panel = new DataPanel_MainContent("Netdown Summary");
	
	public static void chooseFile(String fileToChoose) throws Exception {
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("netdownImport_ChooseFile"), fileToChoose);
		BaseUI.waitForElementToBeDisplayed("netdownMap_Header", null, null, 500);
		Thread.sleep(500);
	}

	public static void verify_TitleDisplayed() {
		WebElement pageTitle = Locator.lookupElement("netdownImport_Header");
		BaseUI.verifyElementAppears(pageTitle);
		BaseUI.verifyElementHasExpectedText(pageTitle, "Netdown Import");
	}
	
	public static void verify_NetdownSummary_Panel(String orderID, String poNumber, String marketer, String mart, Integer selectedRecords) {
		HashMap<String, String> netdownSummary_Data = netdownSummary_Panel.return_PanelData();
		
		BaseUI.baseStringCompare("Id", orderID, netdownSummary_Data.get("Id"));
		BaseUI.baseStringCompare("PO Number", poNumber, netdownSummary_Data.get("PO Number"));
		
		Integer searchID = Integer.parseInt(netdownSummary_Data.get("Search Id"));
		BaseUI.verify_true_AndLog(searchID > 0, "Found Search Id of " + netdownSummary_Data.get("Search Id"), "Search ID was not valid.");
		
		BaseUI.baseStringCompare("Search Type", mart, netdownSummary_Data.get("Search Type"));
		BaseUI.baseStringCompare("Selected Records", selectedRecords.toString(), netdownSummary_Data.get("Selected Records"));
	}
	
}
