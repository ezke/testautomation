package inteliData.pages;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class Help_SeeWhatsNew {

	

	public static void verify_PageLoaded() {
		Help_Navigation.verify_Title_Appears();
		Help_Navigation.verify_SearchBox_Appears();
		
		WebElement whatsNewHeader = Locator.lookupElement("help_SeeWhatsNew_WhatsNewTitle");
		BaseUI.verifyElementAppears(whatsNewHeader);
		BaseUI.verifyElementHasExpectedText(whatsNewHeader, "What's New ...");
	}

}// End of Class
