package inteliData.pages;

import inteliData.data.GlobalVariables;
import utils.BaseUI;
import utils.Browser;
import utils.DatabaseConnection;
import utils.Locator;
import utils.TableData;

public class LoginPage {

	public static void login(String userName, String password) throws Exception {
		Browser.set_ImplicitWait_AndSaveValue(0);

		BaseUI.waitForElementToBeDisplayed("login_Username", null, null);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_Username"), userName);
		BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_Password"), password);
		BaseUI.click(Locator.lookupElement("login_SignInButton"));
		BaseUI.waitForElementToBeDisplayed("login_ElementToWaitFor", null, null);
		if (BaseUI.elementAppears(Locator.lookupElement("login_ValidationCodeTextBox"))) {
//			Email_API.Wait_ForEmail_ToExist(GlobalVariables.accountEmail, 15);
//			Email firstEmail = Email_API.return_AllEmailsByRecipient(GlobalVariables.accountEmail)[0];
//			String emailVerificationCode = firstEmail.text.body.split("\\r\\n")[1].split("\\:")[1].trim();
			String dbAuthCode = returnAuthCode(userName);

			BaseUI.checkCheckbox("login_RememberDeviceCheckbox");
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("login_ValidationCodeTextBox"), dbAuthCode);
			BaseUI.click(Locator.lookupElement("login_SignInButton"));
			BaseUI.waitForElementToBeDisplayed("homePage_BannerImage", null, null);
		}

		Thread.sleep(500);
	}

	public static String returnAuthCode(String userName) throws Exception {
		TableData dbData = new TableData();
		String query = "select pac.Code from [dbo].[PartyAuthenticationCodes] pac \r\n"
				+ "join parties p on p.partyid = pac.partyid \r\n" + "where username = '" + userName + "'\r\n"
				+ "and p.partystatustypeid = 1";
		DatabaseConnection.dbUrl = GlobalVariables.trzis_tran01;
		dbData = DatabaseConnection.runSQLServerQuery(query);
		return dbData.data.get(0).get("Code");
	}

}// End of Class
