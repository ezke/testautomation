package inteliData.pages;

import utils.Locator;

import org.openqa.selenium.WebElement;

import utils.BaseUI;

public class Help {

	public static void verify_PageLoaded() {
		BaseUI.verifyElementAppears(Locator.lookupElement("help_BannerImage"));

		WebElement whatsNewHeader = Locator.lookupElement("help_WhatsNew_Header");
		BaseUI.verifyElementAppears(whatsNewHeader);
		BaseUI.verifyElementHasExpectedText(whatsNewHeader, " What's New");

		WebElement howDoIHeader = Locator.lookupElement("help_HowDoI_Header");
		BaseUI.verifyElementAppears(howDoIHeader);
		BaseUI.verifyElementHasExpectedText(howDoIHeader, " How Do I...");

		WebElement feedbackHeader = Locator.lookupElement("help_Feedback_Header");
		BaseUI.verifyElementAppears(feedbackHeader);
		BaseUI.verifyElementHasExpectedText(feedbackHeader, " Feedback");

		BaseUI.verifyElementAppears(Locator.lookupElement("help_SeeWhatsNew_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("help_SeeGuides_Button"));
		BaseUI.verifyElementAppears(Locator.lookupElement("help_SendUsANote_Button"));
	}

	public static void click_SeeWhatsNew_Button() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("help_SeeWhatsNew_Button"), false, 1000);
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void click_SeeGuides_Button() throws Exception {
		BaseUI.ClickAndSwitchWindow(Locator.lookupElement("help_SeeGuides_Button"), false, 1000);
		BaseUI.wait_forPageToFinishLoading();
	}

	// Assumes email address will always be iefeedback@datamyx.com
	// mailto:iefeedback@datamyx.com?Subject=Intelidata%20Express%20feedback
	public static void verify_SendUsANote_HasValidHREF() {

		String expectedEmailAddress = "iefeedback@datamyx.com";
		String expectedSubject = "Intelidata%20Express%20feedback";

		String mailToInfo = BaseUI.get_Attribute_FromField(Locator.lookupElement("help_SendUsANote_Button"), "href");

		BaseUI.verify_true_AndLog(mailToInfo.startsWith("mailto:"), "HREF started with mailto:",
				"HREF did not start with mailto:");

		String mailToAddress = mailToInfo.substring(7, mailToInfo.indexOf("?"));
		String mailToSubject = mailToInfo.substring(mailToInfo.indexOf("?") + 9, mailToInfo.length());

		BaseUI.baseStringCompare("Email Address", expectedEmailAddress, mailToAddress);
		BaseUI.baseStringCompare("Email Subject", expectedSubject, mailToSubject);

	}

}// End of Class
