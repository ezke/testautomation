package inteliData.pages;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import inteliData.data.GlobalVariables;
import inteliData.page_controls.*;
import org.apache.commons.lang3.Range;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.TableData;

//formerly known as CreateAudience_AudienceSummary
public class CreateAudience_SearchOrderRecords {

    // "12/4/2017 12:08:05 PM"
    public static String dateFormat = "M/d/yyyy h:mm:ss a";
    public static DataPanel_MainContent mainContent_Panel = new DataPanel_MainContent("Audience Summary");
    public static Table_WithFooter_AndCheckboxes resultsTable = new Table_WithFooter_AndCheckboxes("list");
    public static Multiselect_ButtonDropdown suppliersDropdown = new Multiselect_ButtonDropdown("Suppliers");
    public static Checkbox_ClickLabel_WithAlerts directMail_Checkbox = new Checkbox_ClickLabel_WithAlerts("Direct Mail",
            "Mail Piece for Direct Mail: Compliant.");
    public static Checkbox_ClickLabel_WithAlerts telemarketing_Checkbox = new Checkbox_ClickLabel_WithAlerts(
            "Telemarketing",
            "SAN for Telemarketing: Code: 01234567890123. Telemarketing Script for Telemarketing: Compliant.");
    public static Checkbox_ClickLabel_WithAlerts email_Checkbox = new Checkbox_ClickLabel_WithAlerts("Email",
            "Email Script for Email: Compliant.");

    public static TableBase selectedSuppressions = new TableBase("SuppressionSelections");

    // Only appears after clicking "View Details" link.
    public static DataPanelBase suppressionsPanel = new DataPanelBase("Suppressions");

    public static boolean atSearchProcessing() {
        return Browser.driver.getCurrentUrl().contains("/Search/Processing/");
    }


    public static void verify_AtPage() {
        BaseUI.verify_true_AndLog(Browser.driver.getCurrentUrl().contains("/Search/OrderRecords/"), "Got to Order Records page",
                "Did NOT get to Order Records page.");
    }

    public static void retry_Navigation_ToSearchOrderRecords_IfAtSearchProcessing() throws Exception {
        if (atSearchProcessing()) {
            String currentURL = Browser.driver.getCurrentUrl();

            String audienceID = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length());

            Navigation.navigate_Activities();
            Activities.allAudiences_SearchBox.search_WithCriteria(audienceID);
            Activities.allAudiences_ActivitiesTable.click_ViewAudience_ForRow(1);
        }
    }


    public static void verify_AudienceSummary_TotalRecords(Integer expectedTotal) {
        String totalRecords = mainContent_Panel.return_SpecificValue("Total Records").replace(",", "");

        BaseUI.baseStringCompare("Total Records", expectedTotal.toString(), totalRecords);
    }

    public static void wait_ForPage_ToLoad() throws Exception {
        WebElement waitElement = BaseUI
                .waitForElementToBeDisplayed_ButDontFail("createAud_AudSum_AudienceSummaryHeader", null, null, 600);
        if (waitElement != null) {
            BaseUI.waitForElementToBeDisplayed("createAud_AndSum_SelectRecordsTable_FirstRow_SecondCell", null, null,
                    Duration.ofSeconds(20));
            Thread.sleep(1500);
        } else {
            String logOutput = BaseUI.return_Browser_Output();
            BaseUI.log_AndFail(
                    "Timed out waiting for Search Order Records page to load. Showing browser output: \n" + logOutput);
        }
    }

    public static void verify_ZipCodesRejected_Error(String[] zips) {
        String expectedText = String.valueOf(zips.length) + " zip code(s) were invalid and could not be loaded. Invalid zip codes include: ";

        for (int i = 0; i < zips.length; i++) {
            expectedText += zips[i];
            if (i != zips.length - 1) {
                expectedText += ",";
            }
        }

        WebElement errorElement = Locator.lookupElement("createAud_AndSum_ErrorMessage");
        BaseUI.verifyElementAppears(errorElement);

        String errorElementText = BaseUI.getTextFromField(errorElement).split("\\n")[2];
        BaseUI.baseStringCompare("Error Field", expectedText, errorElementText);

    }

    public static void verify_Distance_Within_DistanceRange(TableData zipCodeTable,
                                                            ArrayList<Geography_State_Info> levelInfo) {

        zipCodeTable.forEach_Row(a -> {
            String level = a.get("Level");
            int actual_Distance = Integer.valueOf(a.get("Distance").split("\\.")[0]);

            Geography_State_Info levelRow = CreateAudience_DownloadRecords_Page.get_State_LevelWeWant(levelInfo, level);

            Range expectedRange = levelRow.get_distanceRange();

            int minInt = (int) expectedRange.getMinimum();
            int maxInt = (int) expectedRange.getMaximum();

            BaseUI.verify_true_AndLog(minInt <= actual_Distance && actual_Distance <= maxInt,
                    "Actual Distance fell within provided range.",
                    "Distance of " + a.get("Distance") + " did NOT fall within range of "
                            + expectedRange.getMinimum().toString() + " to " + expectedRange.getMaximum().toString());

        });

    }

    public static void verify_DNS_Suppression_Found_AndDateWithinAcceptableRange(String orderID, String account,
                                                                                 String marketer, String suppressionType, String expectedDate) throws Exception {
        TableData suppression_Data = selectedSuppressions.return_TableData();
        HashMap<String, String> returnedRow;

        HashMap<String, String> expectedRowValues = new HashMap<String, String>();
        expectedRowValues.put("Order Id", orderID);
        expectedRowValues.put("Account", account);
        expectedRowValues.put("Marketer", marketer);
        expectedRowValues.put("Order Type", "DNS Suppression");
        expectedRowValues.put("Suppression Type(s)", suppressionType);

        returnedRow = suppression_Data.return_Row_BasedOn_AnyNumberOfMatchingFields(expectedRowValues);

        suppression_Data.verify_MatchingRow_Found(expectedRowValues);
        BaseUI.verify_Date_IsAcceptable_DateRange(expectedDate, returnedRow.get("Order Date"), -3, 3, dateFormat);
    }

    // Assumes and verifies there is only one row of Data.
    public static void verify_1Row_OfData_MatchesExpected(Integer expectedTotal, Integer expectedOrdered,
                                                          Integer expectedRequested, Boolean expectedRequestedIsTextbox) {
        TableData results_TableInfo = resultsTable.return_TableData();
        HashMap<String, String> row = results_TableInfo.data.get(0);

        results_TableInfo.verify_TableSize(1);

        // Verify Level is 1. Since there's only 1 row this is hard coded to 1.
        BaseUI.baseStringCompare("Level", "1", row.get("Level"));

        // Verify Total matches expected
        BaseUI.baseStringCompare("Total", expectedTotal.toString(), row.get("Total").replace(",", ""));

        // Verify Ordered matches expected
        BaseUI.baseStringCompare("Ordered", expectedOrdered.toString(), row.get("Ordered").replace(",", ""));

        // Verify Phone is an Integer > 0. Take out Commas.
        Integer phone = Integer.parseInt(row.get("Phone").replace(",", ""));
        BaseUI.verify_true_AndLog(phone > 0, "Found phone and was greater than 0.", "Phone was not greater than 0.");

        // Verify Requested matches expected.
        String actualRequested = "";
        if (!expectedRequestedIsTextbox) {
            actualRequested = row.get("Requested").replace(",", "");
        } else {
            WebElement tableTextBox = resultsTable.textBox_ByRowIndex(1);
            actualRequested = BaseUI.get_Attribute_FromField(tableTextBox, "previousvalue");
        }
        BaseUI.baseStringCompare("Requested", expectedRequested.toString(), actualRequested);
    }

    public static void click_ViewDetailsLink() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_AudSum_ViewOrHide_DetailsLink"));
        BaseUI.waitForElementToBeDisplayed("createAud_AudSum_SelectedSuppressionsTable_FirstCell", null, null, 10);
        Thread.sleep(500);
    }

    public static String get_mainContentPanel_Value(String valueToGet) {
        HashMap<String, String> mainContent_Data = mainContent_Panel.return_PanelData();
        return mainContent_Data.get(valueToGet);
    }

    public static void verify_mainContentPanel(String mart, String marketer, String createdTime,
                                               Integer totalRecords_minimum, Integer selectedRecords) throws Exception {
        HashMap<String, String> mainContent_Data = mainContent_Panel.return_PanelData();

        // Valid Audience ID
        Integer audienceID = Integer.parseInt(mainContent_Data.get("Audience ID"));
        BaseUI.verify_true_AndLog(audienceID > 0, "Found Audience ID and Greater than 0.",
                "Audience ID was NOT Greater than 0.");

        // Mart matches
        BaseUI.baseStringCompare("Mart", mart, mainContent_Data.get("Mart"));

        // Marketer matches
        BaseUI.baseStringCompare("Marketer", marketer, mainContent_Data.get("Marketer"));

        // Created within an acceptable range from expected Time.
        String createdTime_range1 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, -2);
        String createdTime_range2 = BaseUI.getDate_WithFormat_X_Minutes_FromInitialString(createdTime, dateFormat,
                dateFormat, 2);
        if(GlobalVariables.run_DateTimeChecks_Dev) {
            BaseUI.verify_Date_IsBetween_DateRange(createdTime_range1, createdTime_range2, mainContent_Data.get("Created"),
                    dateFormat);
        }

        // Total Records over a certain amount.
        Integer totalRecords_Actual = Integer.parseInt(mainContent_Data.get("Total Records").replace(",", ""));
        BaseUI.verify_true_AndLog(totalRecords_Actual >= totalRecords_minimum,
                "Records was above or equal to " + totalRecords_minimum.toString(),
                "Records was NOT above or equal to " + totalRecords_minimum.toString());

        // Selected Records = expectedAmount
        String selectedRecords_Actual = mainContent_Data.get("Selected Records").replace(",", "");
        BaseUI.verify_true_AndLog(BaseUI.string_IsInteger(selectedRecords_Actual), "Selected Records was an Integer.",
                "Seected Records was not an Integer.");
        BaseUI.baseStringCompare("Selected Records", selectedRecords.toString(), selectedRecords_Actual);

        // Status = complete
        BaseUI.baseStringCompare("Status", "Complete", mainContent_Data.get("Status"));
    }

    public static void verify_Footer(String expectedTotal, String expectedOrdered, String expectedRequested,
                                     String phone) throws Exception {
        HashMap<String, String> footerRow = resultsTable.return_footerRow();

        // Verify Total matches expected
        BaseUI.baseStringCompare("Total", expectedTotal.toString(), footerRow.get("Total"));

        // Verify Ordered matches expected
        BaseUI.baseStringCompare("Ordered", expectedOrdered.toString(), footerRow.get("Ordered"));

        // Verify Phone matches expected
        BaseUI.baseStringCompare("Phone", phone, footerRow.get("Phone"));

        // Verify Requested matches expected.
        BaseUI.baseStringCompare("Requested", expectedRequested.toString(), footerRow.get("Requested"));
    }

    public static void verify_Footer_Requested(int expectedTotalRequested) {
        HashMap<String, String> footerRow = resultsTable.return_footerRow();

        int actualRequested = Integer.parseInt(footerRow.get("Requested").replace(",", ""));
        BaseUI.verify_true_AndLog(expectedTotalRequested == actualRequested,
                "Requested amount matches " + String.valueOf(expectedTotalRequested), "Requested amount does NOT match "
                        + String.valueOf(expectedTotalRequested) + " seeing " + String.valueOf(actualRequested));
    }

    public static void click_SelectRecords_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_AudSum_SelectRecords_Button"));
        BaseUI.waitForElementToBeDisplayed("createAud_OrderDetailsFinal_PlaceOrderButton", null, null);
        Thread.sleep(500);
    }

    public static void click_SaveAsProgram_Button() throws Exception {
        BaseUI.click(Locator.lookupElement("createAud_SearchOrderRecords_SaveAsProgram_Button"));
        ProgramEdit.waitForPageToLoad();
    }

    // validation checks that we just have suppression level and suppress by order.
    // May need different validation if other suppressions are used.
    public static void verify_SuppressionsPanel(String suppressionLevel, String suppressByOrder) {
        HashMap<String, String> suppressionPanel_Data = suppressionsPanel.return_PanelData();

        BaseUI.verify_true_AndLog(suppressionPanel_Data.keySet().size() == 2, "Found correct number of suppressions.",
                "Found more suppressions than the expected count of 2.");

        BaseUI.baseStringCompare("Suppression Level", suppressionLevel, suppressionPanel_Data.get("Suppression level"));
        BaseUI.baseStringCompare("Suppress by order", suppressByOrder, suppressionPanel_Data.get("Suppress by order"));

    }

    public static void verify_Suppression_Found_In_SelectedSuppressions(String orderID, String poNumber)
            throws Exception {
        TableData selectedSuppressionsTable = selectedSuppressions.return_TableData();
        selectedSuppressionsTable.logTable();

        HashMap<String, String> expectedValues = new HashMap<String, String>();
        expectedValues.put("Order Id", orderID);
        expectedValues.put("PO Number", poNumber);
        expectedValues.put("Order Type", "External Suppress");

        BaseUI.log_Status("Attempting to find row for Order ID " + orderID);
        selectedSuppressionsTable.verify_MatchingRow_Found(expectedValues);
    }

    public static void select_Display_DropdownValue(String valueToSelect) throws Exception {
        WebElement displayDropdown = Locator.lookupElement("createAud_AudSum_DisplayDropdown");
        String currentSelectedValue = BaseUI.getSelectedOptionFromDropdown(displayDropdown);
        if (!currentSelectedValue.equals(valueToSelect)) {
            BaseUI.selectValueFromDropDown(displayDropdown, valueToSelect);

            BaseUI.waitForElementToNOTBeDisplayed("createAud_AndSum_SelectRecords_TableWrapper", null, null);

            wait_ForPage_ToLoad();
        }

    }

    // state should be 2 letter abbreviation (ex. WI, IA)
    private static void verify_Level_And_Field_RowFound(TableData resultsTableData, String level, String fieldName, String fieldValue)
            throws Exception {
        HashMap<String, String> expectedRow = new HashMap<String, String>();
        expectedRow.put("Level", level);
        expectedRow.put(fieldName, fieldValue);

        resultsTableData.verify_MatchingRow_Found(expectedRow);
    }

    public static void verify_Values_Found_ForLevel(TableData resultsTableData, String level,
                                                    String[] valuesThatShouldAppear, String fieldName) throws Exception {
        BaseUI.verify_true_AndLog(valuesThatShouldAppear.length > 0, "Passed in " + fieldName + " had values.",
                "Passed in " + fieldName + " did NOT have values.");

        for (String value : valuesThatShouldAppear) {
            verify_Level_And_Field_RowFound(resultsTableData, level, fieldName, value);
        }

        SoftAssert softAssert = new SoftAssert();

        List<String> valuesAsList = Arrays.asList(valuesThatShouldAppear);
        // Verify given level didn't have any states that aren't in state Abbreviations
        try {
            for (HashMap<String, String> row : resultsTableData.data) {
                // Don't need an else, we don't care about other levels, just validating the one
                // we passed in.
                if (row.get("Level").equals(level)) {
                    softAssert.assertTrue(valuesAsList.contains(row.get(fieldName)));
                }
            }
        } finally {
            softAssert.assertAll();
        }

    }


    public static void verify_Values_Found_ForLevel(TableData resultsTableData, TableData expectedTable) throws Exception {
        BaseUI.verify_true_AndLog(resultsTableData.data.size() == expectedTable.data.size(),
                "Tables matched in size.", "Tables did NOT match in size.");
        for (HashMap<String, String> expectedRow : expectedTable.data) {
            resultsTableData.verify_MatchingRow_Found(expectedRow);
        }
    }

    public static void verify_RecordCount_Decreased_FromEarlier(TableData dataToMatch, TableData earlierStatesTable,
                                                                TableData newStatesTable) {
        BaseUI.verify_true_AndLog(dataToMatch.data.size() > 0, "Found Data.", "Did NOT find data.");
        for (HashMap<String, String> row : dataToMatch.data) {
            int amountDecrease = Integer.parseInt(row.get("Total").replace(",", ""));

            int initialTotal = Integer.valueOf(earlierStatesTable
                    .return_Row_BasedOn_2MatchingFields("Level", row.get("Level"), "State", row.get("State"))
                    .get("Total").replace(",", ""));
            int totalAfterExclusion = Integer.valueOf(newStatesTable
                    .return_Row_BasedOn_2MatchingFields("Level", row.get("Level"), "State", row.get("State"))
                    .get("Total").replace(",", ""));

            BaseUI.verify_true_AndLog(amountDecrease > 0, "Amount of decrease found.", "Amount will not decrease.");
            BaseUI.verify_true_AndLog((initialTotal - amountDecrease) == totalAfterExclusion,
                    "Total After Esclusion matched " + String.valueOf(totalAfterExclusion),
                    "Expected " + String.valueOf((initialTotal - amountDecrease)) + " But seeing "
                            + String.valueOf(totalAfterExclusion));
        }

    }

    // Will select records from each line, increment the amount we'll select by
    // incrementAmount. If there are less records than we want, we'll pick the
    // amount of records available. Also updates the table that we passed in to
    // reflect the new values.
    public static void select_AndUpdate_RecordsFromEachLine_IncrementedBy(TableData resultRecordsData,
                                                                          int startingAmount, int incrementAmount) throws Exception {

        resultsTable.all_Checkbox.check_Checkbox();
        int amountToAdd = startingAmount;

        for (int i = 0; i < resultRecordsData.data.size(); i++) {

            int amountAvailable = Integer.parseInt(resultRecordsData.data.get(i).get("Total").replace(",", ""));

            if (amountAvailable < amountToAdd) {
                String amountAvailableString = resultRecordsData.data.get(i).get("Total");
                BaseUI.enterText_IntoInputBox(resultsTable.textBox_ByRowIndex(i + 1), amountAvailableString);
                resultRecordsData.data.get(i).put("Requested", amountAvailableString);
            } else {
                String amountToAddString = String.valueOf(amountToAdd);
                BaseUI.enterText_IntoInputBox(resultsTable.textBox_ByRowIndex(i + 1), amountToAddString);
                resultRecordsData.data.get(i).put("Requested", amountToAddString);
            }
            amountToAdd += incrementAmount;

        }

    }

}// End of Class
