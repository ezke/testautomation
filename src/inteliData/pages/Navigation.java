package inteliData.pages;

import java.time.Duration;

import inteliData.page_controls.Button_Dropdown_V2;
import utils.BaseUI;
import utils.Locator;

public class Navigation {

	public static void navigate_ChangePassword() throws Exception {
		Button_Dropdown_V2 userMenu = new Button_Dropdown_V2("nav_UserDropdown", "nav_UserDropdown_Opton",
				"Change Password");
		userMenu.open_List_AndClickItem();
		BaseUI.waitForElementToBeDisplayed("changePass_PasswordTextBox", null, null, Duration.ofSeconds(20));
		Thread.sleep(500);
	}

	public static void navigate_Menu(String menuText) throws Exception {
		BaseUI.click(Locator.lookupElement("nav_Menu_ByText", menuText, null));
	}

	public static void navigate_Menu_SubMenu(String menuText, String subMenuText) throws Exception {
		if (!BaseUI.elementExists("nav_Menu_Open_ByText", menuText, null)) {
			navigate_Menu(menuText);
		}
		BaseUI.waitForElementToBeDisplayed("nav_Submenu_ByText", subMenuText, null);
		BaseUI.click(Locator.lookupElement("nav_Submenu_ByText", subMenuText, null));
	}

	public static void navigate_Suppressions() throws Exception {
		navigate_Menu("Suppressions");
		Suppressions.wait_ForPageToLoad();
	}

	public static void navigate_Home() throws Exception {
		navigate_Menu("Home");
		BaseUI.waitForElementToBeDisplayed("homePage_BannerImage", null, null);
		Thread.sleep(500);
	}

	public static void navigate_FirstParty_DataSources() throws Exception {
		navigate_Menu_SubMenu("First Party", "Data Sources");
		BaseUI.waitForElementToBeDisplayed("firstparty_DataSources_Table_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Activities() throws Exception {
		navigate_Menu("Activities");
		BaseUI.waitForElementToBeDisplayed("activities_Stable_FirstRow_Checkbox", null, null, Duration.ofSeconds(120));
		Thread.sleep(500);
	}

	public static void navigate_Programs() throws Exception {
		navigate_Menu("Programs");
		BaseUI.waitForElementToBeDisplayed("programs_table_AllITMA_FirstRow_FirstCell", null, null, 120);
		Thread.sleep(500);
	}

	public static void navigate_DataSources() throws Exception {
		navigate_Menu("Data Sources");
		BaseUI.waitForElementToBeDisplayed("dataSources_table_MyDataSources_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Administration_DNS() throws Exception {
		navigate_Menu_SubMenu("Administration", "DNS");
		BaseUI.waitForElementToBeDisplayed("administration_DNS_table_FirstRow_Checkbox", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Administration_Automations() throws Exception {
		navigate_Menu_SubMenu("Administration", "Automations");
		BaseUI.waitForElementToBeDisplayed("administration_Automations_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Administration_Checklist() throws Exception {
		navigate_Menu_SubMenu("Administration", "Checklist");
		BaseUI.waitForElementToBeDisplayed("administration_Checklist_MarketerChecklist_Dropdown", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Administration_Marketers() throws Exception {
		navigate_Menu_SubMenu("Administration", "Marketers");
		BaseUI.waitForElementToBeDisplayed("administration_Marketers_table_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_DNS() throws Exception {
		navigate_Menu("DNS");
		BaseUI.waitForElementToBeDisplayed("dataSources_table_MyDataSources_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Accounting_Invoices() throws Exception {
		navigate_Menu_SubMenu("Accounting", "Invoices");
		BaseUI.waitForElementToBeDisplayed("accounting_Invoices_table_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Accounting_Credits() throws Exception {
		navigate_Menu_SubMenu("Accounting", "Credits");
		BaseUI.waitForElementToBeDisplayed("accounting_Credits_table_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Packages() throws Exception {
		navigate_Menu("Packages");
		BaseUI.waitForElementToBeDisplayed("packages_table_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Tools_MarketAreas() throws Exception {
		navigate_Menu_SubMenu("Tools", "Market Areas");
		BaseUI.waitForElementToBeDisplayed("tools_MarketAreas_table_FirstRow_FirstCell", null, null);
		Thread.sleep(500);
	}

	public static void navigate_Help() throws Exception {
		navigate_Menu("Help");
		BaseUI.waitForElementToBeDisplayed("help_BannerImage", null, null);
		Thread.sleep(500);
	}

}// End of Class
