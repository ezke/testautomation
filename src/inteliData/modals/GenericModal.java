package inteliData.modals;

import org.openqa.selenium.WebElement;

import utils.BaseUI;
import utils.Locator;

public class GenericModal {

	// Suppression field
	public static void verify_PoNumber_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("suppressionModal_PoNumber"));
	}

	public static void select_SuppressionType(String typeToSelect) {
		if (typeToSelect != null)
			BaseUI.selectValueFromDropDown(Locator.lookupElement("suppressionModal_SuppressionTypeDropdown"),
					typeToSelect);
	}

	public static void enter_PONumber(String poNumber) {
		if (poNumber != null)
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("suppressionModal_PoNumber"), poNumber);
	}

	// Takes you to non-modal page with standard page title.
	public static void click_SaveAndContinue_ToNewPage(String pageHeaderText) throws Exception {
		BaseUI.click(Locator.lookupElement("newAudienceModal_SaveAndContinueButton"));
		BaseUI.waitForElementToBeDisplayed("newAudienceModal_NonModalTitleToNavigateTo", pageHeaderText, null, 300);
		Thread.sleep(500);

	}

	public static void click_SaveAndContinue_ToNewModalPage() throws Exception {
		BaseUI.click(Locator.lookupElement("newAudienceModal_SaveAndContinueButton"));
		BaseUI.waitForElementToBeDisplayed("suppliersUsageModal_CreateAudienceButton", null, null);
		Thread.sleep(500);

	}

	public static void select_BuildFor(String buildFor) {
		if (buildFor != null)
			BaseUI.selectValueFromDropDown(Locator.lookupElement("newAudienceModal_BuildFor_Dropdown"), buildFor);
	}

	public static void select_Account(String account) {
		if (account != null)
			BaseUI.selectValueFromDropDown(Locator.lookupElement("newAudienceModal_Account_Dropdown"), account);
	}

	public static void select_Mart(String mart) {
		if (mart != null)
			BaseUI.selectValueFromDropDown(Locator.lookupElement("newAudienceModal_Mart_Dropdown"), mart);
	}

	public static void select_Marketer(String marketer) {
		if (marketer != null)
			BaseUI.selectValueFromDropDown(Locator.lookupElement("newAudienceModal_Marketer_Dropdown"), marketer);
	}

	public static void enter_Name(String nameToEnter) {
		if (nameToEnter != null)
			BaseUI.enterText_IntoInputBox(Locator.lookupElement("newAudienceModal_Name_TextBox"), nameToEnter);
	}

	public static void click_Close_IfDisplayed() throws Exception {
		if (BaseUI.elementAppears(Locator.lookupElement("newAudienceModal_Form"))) {
			click_CloseButton();
		}
	}

	public static void verify_ModalTitle_Displayed(String expectedText) {
		WebElement modalTitle = Locator.lookupElement("newAudienceModal_NewAudienceTitle");
		BaseUI.verifyElementAppears(modalTitle);
		BaseUI.verifyElementHasExpectedText(modalTitle, expectedText);
	}

	public static void verify_SaveAndContinue_Button_Displayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_SaveAndContinueButton"));
	}

	public static void verify_CloseButton_Displayed() {
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_CloseButton"));
	}

	public static void click_CloseButton() throws Exception {
		BaseUI.click(Locator.lookupElement("newAudienceModal_CloseButton"));
		BaseUI.waitForElementToNOTBeDisplayed("newAudienceModal_Form", null, null);
		Thread.sleep(500);
	}

	public static void verify_CurrentStep_1() {
		WebElement currentStepText = Locator.lookupElement("newAudienceModal_CurrentStepText");
		BaseUI.verifyElementAppears(currentStepText);
		BaseUI.verifyElementHasExpectedText(currentStepText, "Step 1 of 2");
	}

	public static void verify_CurrentStep_1(Integer stepCount) {
		WebElement currentStepText = Locator.lookupElement("newAudienceModal_CurrentStepText");
		BaseUI.verifyElementAppears(currentStepText);
		BaseUI.verifyElementHasExpectedText(currentStepText, "Step 1 of " + stepCount.toString());
	}

	public static void verify_NameField_Appears() {
		BaseUI.verifyElementHasExpectedAttributeValue(Locator.lookupElement("newAudienceModal_NameRequired_Label"),
				"innerText", "Name (required)");
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_Name_TextBox"));
	}

	public static void verify_Mart_Dropdown_Appears() {
		WebElement martLabel = Locator.lookupElement("newAudienceModal_Mart_Label");
		BaseUI.verifyElementAppears(martLabel);
		BaseUI.verifyElementHasExpectedText(martLabel, "Mart");
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_Mart_Dropdown"));
	}

	public static void verify_Mart_Dropdown_Does_NOT_Appear() {
		WebElement martLabel = Locator.lookupElement("newAudienceModal_Mart_Label");
		BaseUI.verifyElementDoesNotAppear(martLabel);
		BaseUI.verifyElementDoesNotAppear(Locator.lookupElement("newAudienceModal_Mart_Dropdown"));
	}

	public static void verify_Account_Dropdown_Appears() {
		WebElement label = Locator.lookupElement("newAudienceModal_Account_Label");
		BaseUI.verifyElementAppears(label);
		BaseUI.verifyElementHasExpectedText(label, "Account");
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_Account_Dropdown"));
	}

	public static void verify_Marketer_Dropdown_Appears() {
		WebElement label = Locator.lookupElement("newAudienceModal_Marketer_Label");
		BaseUI.verifyElementAppears(label);
		BaseUI.verifyElementHasExpectedText(label, "Marketer");
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_Marketer_Dropdown"));
	}

	public static void verify_BuildFor_Dropdown_Appears() {
		WebElement label = Locator.lookupElement("newAudienceModal_BuildFor_Label");
		BaseUI.verifyElementAppears(label);
		BaseUI.verifyElementHasExpectedText(label, "Build For");
		BaseUI.verifyElementAppears(Locator.lookupElement("newAudienceModal_BuildFor_Dropdown"));
	}

	public static void wait_ForForm_ToAppear() throws Exception {
		BaseUI.waitForElementToBeDisplayed("newAudienceModal_Form", null, null);
		Thread.sleep(500);
	}
	
}// End of Class
