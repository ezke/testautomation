package inteliData.modals;

import utils.BaseUI;
import utils.Locator;

public class DataSource_Modal {

	public static void verify_DataSource_Modal_Appears() {
		GenericModal.verify_ModalTitle_Displayed("Data Source");
		GenericModal.verify_SaveAndContinue_Button_Displayed();
		GenericModal.verify_CurrentStep_1(3);
		GenericModal.verify_CloseButton_Displayed();
		GenericModal.verify_NameField_Appears();
		GenericModal.verify_Mart_Dropdown_Does_NOT_Appear();
		GenericModal.verify_Account_Dropdown_Appears();
		GenericModal.verify_Marketer_Dropdown_Appears();

	}

	public static void enter_DataSource_Fields_AndSaveContinue(String name, String account, String marketer)
			throws Exception {
		GenericModal.enter_Name(name);
		GenericModal.select_Account(account);
		GenericModal.select_Marketer(marketer);
		GenericModal.click_SaveAndContinue_ToNewPage("Data Source Import");
	}

	public static void enter_DataSource_Fields_AndSaveContinue(String name, String account, String marketer,
			String template) throws Exception {
		GenericModal.enter_Name(name);
		GenericModal.select_Account(account);
		GenericModal.select_Marketer(marketer);
		select_Template(template);
		GenericModal.click_SaveAndContinue_ToNewPage("Data Source Import");
	}

	public static void select_Template(String templateToSelect) {
		if (templateToSelect != null) {
			BaseUI.selectValueFromDropDown(Locator.lookupElement("dataSourceModal_TemplateDropdown"), templateToSelect);
		}
	}

}
