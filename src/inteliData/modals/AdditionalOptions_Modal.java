package inteliData.modals;

import inteliData.page_controls.CheckboxBase;
import org.openqa.selenium.WebElement;
import utils.BaseUI;
import utils.Locator;

import java.time.Duration;

public class AdditionalOptions_Modal {

    public AdditionalOptions_Modal() {
    }


    public void waitForPageToLoad() throws Exception {
        BaseUI.waitForElementToBeDisplayed("addOutputMod_Header", null, null, Duration.ofSeconds(30));
        Thread.sleep(500);
    }

    public CheckboxBase checkbox_ByText(String optionText) {
        CheckboxBase checkboxWeWant = new CheckboxBase(optionText) {
            @Override
            public WebElement elementTo_SeeIfChecked() {
                return Locator.lookupElement("addOutputMod_Checkbox_ElementToSeeIfChecked", label_text, null);
            }

            @Override
            public WebElement clickElement() {
                return Locator.lookupElement("addOutputMod_Checkbox_ElementToClick", label_text, null);
            }
        };

        return checkboxWeWant;
    }



    public void click_SaveAndAddFields() throws Exception {
        BaseUI.click(Locator.lookupElement("addOuptutMod_SaveAndAddFields_Button"));
        BaseUI.waitForElementToNOTBeDisplayed("addOutputMod_Header", null, null, 20);
        Thread.sleep(500);
    }

}//End of Class
