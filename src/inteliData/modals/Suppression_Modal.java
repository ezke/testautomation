package inteliData.modals;

import utils.BaseUI;
import utils.Locator;

public class Suppression_Modal {

	public static void enter_Suppression_Fields_AndSaveContinue(String suppressionType, String poNumber, String account,
			String marketer, String mart, String name) throws Exception {
		GenericModal.select_SuppressionType(suppressionType);
		GenericModal.enter_PONumber(poNumber);
		GenericModal.select_Account(account);
		GenericModal.select_Marketer(marketer);
		GenericModal.select_Mart(mart);
		GenericModal.click_SaveAndContinue_ToNewPage(name);
		BaseUI.waitForElementToBeDisplayed("suppressionCompleted_DetailsTitle", null, null);
	}

	// Suppression field
	public static void verify_SuppressionType_Appears() {
		BaseUI.verifyElementAppears(Locator.lookupElement("suppressionModal_SuppressionTypeDropdown"));
	}

	public static void verify_Suppression_Modal_Appears() {
		GenericModal.verify_ModalTitle_Displayed("Suppression");
		GenericModal.verify_SaveAndContinue_Button_Displayed();
		GenericModal.verify_CloseButton_Displayed();
		verify_SuppressionType_Appears();
		GenericModal.verify_PoNumber_Appears();
		GenericModal.verify_Mart_Dropdown_Appears();
		GenericModal.verify_Account_Dropdown_Appears();
		GenericModal.verify_Marketer_Dropdown_Appears();
	
	}

}
