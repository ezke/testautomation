package inteliData.modals;

import inteliData.page_controls.CheckboxBase;
import inteliData.page_controls.Checkbox_ClickLabel_WithAlerts;
import inteliData.page_controls.Checkbox_InDraggableContainer;
import inteliData.pages.CreateAudience_GlobalCriteria;
import utils.BaseUI;
import utils.Locator;

public class SuppliersAndUsage_Modal {

	public static Checkbox_InDraggableContainer experian_DraggableElement = new Checkbox_InDraggableContainer("Experian");
	public static Checkbox_InDraggableContainer transUnion_DraggableElement = new Checkbox_InDraggableContainer("TransUnion");
	public static Checkbox_InDraggableContainer equifax_DraggableElement = new Checkbox_InDraggableContainer("Equifax");

	public static Checkbox_ClickLabel_WithAlerts directMail_Checkbox = new Checkbox_ClickLabel_WithAlerts("Direct Mail",
			"Mail Piece for Direct Mail: Compliant.");
	public static Checkbox_ClickLabel_WithAlerts telemarketing_Checkbox = new Checkbox_ClickLabel_WithAlerts(
			"Telemarketing",
			"SAN for Telemarketing: Code: 01234567890123. Telemarketing Script for Telemarketing: Compliant.");
	public static Checkbox_ClickLabel_WithAlerts email_Checkbox = new Checkbox_ClickLabel_WithAlerts("Email",
			"Email Script for Email: Compliant.");

	public static CheckboxBase onePerHousehold_Checkbox = new CheckboxBase("One per household");
	public static CheckboxBase uniquePhonesOnly_Checkbox = new CheckboxBase("Unique phones only");

	public static void select_AllSuppliers() throws Exception {
		experian_DraggableElement.check_Checkbox();
		transUnion_DraggableElement.check_Checkbox();
		equifax_DraggableElement.check_Checkbox();
	}

	public static void click_CreateAudience() throws Exception {
		BaseUI.click(Locator.lookupElement("suppliersUsageModal_CreateAudienceButton"));
		CreateAudience_GlobalCriteria.wait_For_PageToLoad();
	}

	public static void click_CreateAppend() throws Exception {
		BaseUI.click(Locator.lookupElement("suppliersUsageModal_CreateAudienceButton"));
		BaseUI.waitForElementToBeDisplayed("appendImport_Header", null, null);
		Thread.sleep(500);
	}

	
}// End of Class
