package inteliData.modals;

public class InTheMarketAlerts_Modal {

	public static void verify_InTheMarketAlerts_NewAudienceModal_Loaded() {
		GenericModal.verify_ModalTitle_Displayed("New In The Market Alerts");
		GenericModal.verify_SaveAndContinue_Button_Displayed();
		GenericModal.verify_CurrentStep_1();
		GenericModal.verify_CloseButton_Displayed();
		GenericModal.verify_NameField_Appears();
		GenericModal.verify_Mart_Dropdown_Appears();
		GenericModal.verify_Account_Dropdown_Appears();
		GenericModal.verify_Marketer_Dropdown_Appears();
	}

}
