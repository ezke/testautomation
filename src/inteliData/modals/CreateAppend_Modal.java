package inteliData.modals;

public class CreateAppend_Modal {

	public static void verify_CreateAppend_NewAudienceModal_Loaded() {
		GenericModal.verify_ModalTitle_Displayed("New Append");
		GenericModal.verify_SaveAndContinue_Button_Displayed();
		GenericModal.verify_CurrentStep_1();
		GenericModal.verify_CloseButton_Displayed();
		GenericModal.verify_NameField_Appears();
		GenericModal.verify_Mart_Dropdown_Appears();
		GenericModal.verify_Account_Dropdown_Appears();
		GenericModal.verify_Marketer_Dropdown_Appears();
	}

	public static void enter_Fields_And_Click_SaveAndContinue(String name, String mart, String account, String marketer)
			throws Exception {
		GenericModal.enter_Name(name);
		GenericModal.select_Mart(mart);
		GenericModal.select_Account(account);
		GenericModal.select_Marketer(marketer);
		GenericModal.click_SaveAndContinue_ToNewModalPage();
	}
	
	
	
	

}// End of Class
