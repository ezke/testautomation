package mailTracker.rest;

import java.sql.SQLException;
import java.text.MessageFormat;
import API.JSON;
import API.REST;
import mailTracker.data.AuthorizationToken;
import mailTracker.pages.GlobalVariables;
import utils.BaseUI;
import utils.DatabaseConnection;
import utils.TableData;

public class Lockboxes_API {

	public static String lockboxID_Required_ErrorMessage = "Lockbox Id is required, must be numeric, and must be greater than 0. ";

	public static String lockbox_Already_Exists_ErrorMessage(String lockboxNumber, String customerNumber,
			String bankNumber) {
		return MessageFormat.format("Lockbox with Id {0} and Customer Id {1} and Bank Id {2} already exists. ",
				lockboxNumber, customerNumber, bankNumber);
	}

	public static String lockbox_doesNOT_Exist_ErrorMessage(String lockboxNumber, String customerNumber,
			String bankNumber) {
		return MessageFormat.format("Lockbox with Id {0} and Customer Id {1} and Bank Id {2} does not exist. ",
				lockboxNumber, customerNumber, bankNumber);
	}

	public static String lockbox_PreviouslyDeleted_ErrorMessage(String lockboxID, String customerID, String bankID) {
		return MessageFormat.format(
				"Lockbox with Id {0} and Customer Id {1} and Bank Id {2} has been previously deleted. ", lockboxID,
				customerID, bankID);

	}

	public static String get_ALL_Lockboxes() throws Exception {

		String newURL = GlobalVariables.startingURL + "lockboxes/";

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String get_Lockbox(String lockboxNumber, String customerNumber, String bankID) throws Exception {

		// http://scscdevqa/MailTracker.Services/api/lockboxes/800/customer/700/bank/600
		String newURL = GlobalVariables.startingURL + "lockboxes/" + lockboxNumber + "/customer/" + customerNumber
				+ "/bank/" + bankID;

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String delete_Lockbox(String lockboxNumber, String customerNumber, String bankID) throws Exception {

		String newURL = GlobalVariables.startingURL + "lockboxes/" + lockboxNumber + "/customer/" + customerNumber
				+ "/bank/" + bankID;

		String output = REST.httpDelete(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String post_Lockbox(String lockboxNumber, String customerID, String bankID, String shortName,
			String longName, String mailoutInstructions, String isDisabled) throws Exception {

		String postMessage1 = "{\"LockboxId\": \"" + lockboxNumber + "\"," + "\"ShortName\": \"" + shortName + "\","
				+ "\n" + "\"LongName\": \"" + longName + "\"," + "\n" + "\"BankId\": \"" + bankID + "\"," + "\n"
				+ "\"CustomerId\": \"" + customerID + "\"," + "\n" + "\"MailoutInstructions\" : \""
				+ mailoutInstructions + "\"," + "\n" + "\"IsDisabled\": \"" + isDisabled + "\"}";

		String newURL = GlobalVariables.startingURL + "lockboxes";

		String output = REST.httpPost(newURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String put_Lockbox(String lockboxNumber, String customerID, String bankID, String shortName,
			String longName, String mailoutInstructions, String isDisabled) throws Exception {

		lockboxNumber = lockboxNumber.equals("") ? "0" : lockboxNumber;

		String postMessage1 = "{\"LockboxId\": \"" + lockboxNumber + "\"," + "\"ShortName\": \"" + shortName + "\","
				+ "\n" + "\"LongName\": \"" + longName + "\"," + "\n" + "\"BankId\": \"" + bankID + "\"," + "\n"
				+ "\"CustomerId\": \"" + customerID + "\"," + "\n" + "\"MailoutInstructions\" : \""
				+ mailoutInstructions + "\"," + "\n" + "\"IsDisabled\": \"" + isDisabled + "\"}";

		String newURL = GlobalVariables.startingURL + "lockboxes/" + lockboxNumber;

		String output = REST.httpPut(newURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static TableData return_ALL_Lockboxes_fromDatabase() throws Exception {
		String query = "SELECT  lock.[LockboxId]" + "\n" + ",lock.[LockboxNumber]" + "\n"
				+ "      ,cust.CustomerNumber as 'CustomerId'" + "\n" + "      ,lock.[ShortName]" + "\n"
				+ "	      ,lock.[LongName]" + "\n" + "	      ,lock.[IsDeleted]" + "\n" + "	      ,lock.[IsDisabled]"
				+ "\n" + "	      ,lock.[MailoutInstructions]" + "\n" + "		  , cust.BankNumber as 'BankId'" + "\n"
				+ "	  FROM [WFSDB_MAIL_TRACKER].[dbo].[Lockboxes] as lock" + "\n"
				+ "	  inner join dbo.Customers as cust on lock.CustomerId = cust.CustomerId" + "\n";

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;

		TableData lockboxObject = DatabaseConnection.runSQLServerQuery(query);

		lockboxObject.replace_Null_With_Value_ForColumn("IsDeleted", "false");
		lockboxObject.replaceValue_ForColumn_WhenEqualTo("IsDeleted", "0", "false");
		lockboxObject.remove_Rows_WithColumnName_WhereValues_DoNOTMatch("IsDeleted", "false");

		lockboxObject.replace_Null_With_Value_ForColumn("IsDisabled", "false");
		lockboxObject.replaceValue_ForColumn_WhenEqualTo("IsDisabled", "0", "false");
		lockboxObject.replaceValue_ForColumn_WhenEqualTo("IsDisabled", "1", "true");
		lockboxObject.replace_Null_With_Value_ForColumn("MailoutInstructions", "");
		lockboxObject.sort_ByColumn_numeric_Descending("LockboxNumber");
		lockboxObject.sort_ByColumn_numeric_Descending("CustomerId");
		lockboxObject.sort_ByColumn_numeric_Descending("BankId");

		return lockboxObject;

	}

	public static TableData return_ALL_Lockboxes_from_APICall() throws Exception {
		String output = get_ALL_Lockboxes();
		TableData lockboxData = JSON.return_JSON_Array_AsTableData(output, "Result");
		lockboxData.replaceKey_ForGivenKey("LockboxId", "LockboxNumber");
		lockboxData.replaceKey_ForGivenKey("Id", "LockboxId");

		// lockboxData.replaceKey_ForGivenKey("CustomerId", "CustomerNumber");
		// lockboxData.replaceKey_ForGivenKey("BankId", "BankNumber");
		//
		// lockboxData.replaceKey_ForGivenKey("Id", "CustomerId");

		lockboxData.sort_ByColumn_numeric_Descending("LockboxNumber");
		lockboxData.sort_ByColumn_numeric_Descending("CustomerId");
		lockboxData.sort_ByColumn_numeric_Descending("BankId");

		return lockboxData;

	}

	public static void verify_GET_Lockbox_Exists(String lockboxNumber, String customerID, String bankID,
			String shortName, String longName, String mailoutInstructions, String isDisabled) throws Exception {

		String getOutput = get_Lockbox(lockboxNumber, customerID, bankID);

		String result_JSON = JSON.return_JSON_Value(getOutput, "Result");

		BaseUI.baseStringCompare("Lockbox Number", lockboxNumber,
				JSON.return_JSON_Value(result_JSON, "LockboxId"));
		BaseUI.baseStringCompare("Short Name", shortName, JSON.return_JSON_Value(result_JSON, "ShortName"));
		BaseUI.baseStringCompare("Long Name", longName, JSON.return_JSON_Value(result_JSON, "LongName"));
		BaseUI.baseStringCompare("Bank Number", bankID, JSON.return_JSON_Value(result_JSON, "BankId"));
		BaseUI.baseStringCompare("Customer Number", customerID,
				JSON.return_JSON_Value(result_JSON, "CustomerId"));
		BaseUI.baseStringCompare("IsDisabled", isDisabled.equals("1") ? "true" : "false",
				JSON.return_JSON_Value(result_JSON, "IsDisabled"));
		BaseUI.baseStringCompare("IsDeleted", "false", JSON.return_JSON_Value(result_JSON, "IsDeleted"));
	}

	public static void verify_Able_ToDelete_LockBox(String lockboxNumber, String customerNumber, String bankID)
			throws Exception {

		String deleteResponse = delete_Lockbox(lockboxNumber, customerNumber, bankID);

		String errorMessage = JSON.return_JSON_Value(deleteResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(deleteResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.
		customerNumber = GlobalVariables.format_ID_ForComparison(customerNumber);
		BaseUI.baseStringCompare("Result",
				MessageFormat.format("Lockbox with Id {0} and Customer Id {1} and Bank Id {2} Deleted", lockboxNumber,
						customerNumber, bankID),
				result);

		BaseUI.verify_false_AndLog(lockboxDeletedOrDoesNOTExist_ByLockboxNumber(lockboxNumber, customerNumber, bankID),
				"Lockbox does not exist or was soft deleted.", "Lockbox was NOT successfully deleted.");

	}

	public static Boolean lockbox_Entry_Exists_InDatabase(String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {
		TableData lockboxObject = return_LockboxInfo_ByLockboxNumber_CustomerNumber_BankNumber(lockboxNumber,
				customerNumber, bankNumber);

		return lockboxObject.data.size() > 0 ? true : false;
	}

	// Returns true if element exists and is not marked as deleted.
	public static Boolean lockboxDeletedOrDoesNOTExist_ByLockboxNumber(String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {
		TableData lockboxObject = return_LockboxInfo_ByLockboxNumber_CustomerNumber_BankNumber(lockboxNumber,
				customerNumber, bankNumber);

		Boolean lockboxExists = false;

		if (lockboxObject.data.size() > 0) {
			if (lockboxObject.data.get(0).get("IsDeleted") == null
					|| lockboxObject.data.get(0).get("IsDeleted").equals("0")) {
				lockboxExists = true;
			} else {
				lockboxExists = false;
			}

		} else {

			lockboxExists = false;
		}

		return lockboxExists;
	}

	public static void verify_Able_ToAddLockbox(String lockboxNumber, String customerNumber, String bankID,
			String shortName, String longName, String mailoutInstructions, String isDisabled) throws Exception {

		String deleteResponse = delete_Lockbox(lockboxNumber, customerNumber, bankID);

		String apiResponse = post_Lockbox(lockboxNumber, customerNumber, bankID, shortName, longName,
				mailoutInstructions, isDisabled);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.
		customerNumber = GlobalVariables.format_ID_ForComparison(customerNumber);
		BaseUI.baseStringCompare("Result",
				MessageFormat.format("Lockbox with Id {0} with Customer Id {1} and Bank Id {2} Created", lockboxNumber,
						customerNumber, bankID),
				result);

		verify_Lockbox_Exists(lockboxNumber, customerNumber, bankID, shortName, longName, mailoutInstructions,
				isDisabled);
	}

	public static void verify_Lockbox_Exists(String lockboxNumber, String customerNumber, String bankID,
			String shortName, String longName, String mailoutInstructions, String isDisabled) throws Exception {

		BaseUI.verify_true_AndLog(lockboxDeletedOrDoesNOTExist_ByLockboxNumber(lockboxNumber, customerNumber, bankID),
				MessageFormat.format("Lockbox {0} was found in database.", customerNumber),
				MessageFormat.format("Lockbox {0} was NOT found in database.", customerNumber));

		TableData lockboxData = return_LockboxInfo_ByLockboxNumber_CustomerNumber_BankNumber(lockboxNumber,
				customerNumber, bankID);

		BaseUI.verify_true_AndLog(lockboxData.data.size() == 1, "Found 1 entry.", "Entries found did not equal 1.");

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("LockboxNumber").equals(lockboxNumber),
				MessageFormat.format("Lockbox Number matched {0}", lockboxNumber), "Lockbox Number did NOT match.");

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("ShortName").equals(shortName),
				MessageFormat.format("Lockbox Short Name matched {0}", shortName), "Lockbox Short Name did NOT match.");

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("LongName").equals(longName),
				MessageFormat.format("Lockbox Long Name  matched {0}", longName), "Lockbox Long Name did NOT match.");

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("CustomerNumber").equals(customerNumber),
				MessageFormat.format("Customer Number matched {0}", customerNumber), "Customer Number did NOT match.");

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("BankNumber").equals(bankID),
				MessageFormat.format("Bank Number matched {0}", bankID), "Bank Number did NOT match.");

		String expectedValue = "";
		if (isDisabled.toLowerCase().equals("false") || isDisabled.equals("") || isDisabled.equals("0")) {
			expectedValue = "0";
		} else if (isDisabled.toLowerCase().equals("true") || isDisabled.equals("1")) {
			expectedValue = "1";
		}

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("IsDisabled").equals(expectedValue),
				MessageFormat.format("IsDisabled matched {0}", expectedValue),
				MessageFormat.format("IsDisabled did NOT match, expected {0}, seeing {1}.", expectedValue,
						lockboxData.data.get(0).get("IsDisabled")));

		BaseUI.verify_true_AndLog(lockboxData.data.get(0).get("MailoutInstructions").equals(mailoutInstructions),
				MessageFormat.format("MailoutInstructions matched {0}", mailoutInstructions),
				"MailoutInstructions did NOT match.");

	}

	public static void verify_Lockbox_IsInvalid(String lockboxNumber, String customerID, String bankID,
			String shortName, String longName, String mailoutInstructions, String isDisabled, String expectedErrorText)
			throws Exception {

		String response = post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

	}

	public static void verify_Get_Lockbox_IsInvalid(String lockboxNumber, String customerID, String bankID,
			String expectedErrorText) throws Exception {
		String response = get_Lockbox(lockboxNumber, customerID, bankID);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

	}

	public static void verify_Update_Lockbox_IsInvalid(String lockboxNumber, String customerID, String bankID,
			String shortName, String longName, String mailoutInstructions, String isDisabled, String expectedErrorText)
			throws Exception {
		String response = put_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		customerID = customerID.replace("~", "").replace("!", "").replace("%", "").replace("^", "").replace("&", "");

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		if (!lockboxNumber.equals("") && lockboxNumber.matches("[0-9]+") && !customerID.equals("")
				&& !bankID.equals("")) {
			verify_Lockbox_DoesNOT_Exist(lockboxNumber, customerID, bankID);
		}
	}

	public static void verify_Delete_Lockbox_IsInvalid(String lockboxNumber, String customerID, String bankID,
			String expectedErrorText) throws Exception {
		String response = delete_Lockbox(lockboxNumber, customerID, bankID);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		customerID = customerID.replace("~", "").replace("!", "").replace("%", "").replace("^", "").replace("&", "");

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		if (!lockboxNumber.equals("") && lockboxNumber.matches("[0-9]+")) {
			verify_Lockbox_DoesNOT_Exist(lockboxNumber, customerID, bankID);
		}
	}

	public static void verify_Lockbox_DoesNOT_Exist(String lockboxNumber, String customerID, String bankID)
			throws Exception {
		BaseUI.verify_true_AndLog(!lockboxDeletedOrDoesNOTExist_ByLockboxNumber(lockboxNumber, customerID, bankID),
				MessageFormat.format("Lockbox with Id {0}, Customer Id {1}, bank ID {2} was NOT found in Database.",
						lockboxNumber, customerID, bankID),
				MessageFormat.format("Lockbox with Id {0}, Customer Id {1}, bank ID {2} was found in Database.",
						lockboxNumber, customerID, bankID));

	}

	public static TableData return_LockboxInfo_ByLockboxNumber_CustomerNumber_BankNumber(String lockboxNumber,
			String customerNumber, String bankNumber) throws Exception, SQLException {

		String query = "SELECT  lock.[LockboxId]" + "\n" + ",lock.[LockboxNumber]" + "\n" + ",cust.CustomerNumber"
				+ "\n" + ",lock.[ShortName]" + "\n" + ",lock.[LongName]" + "\n" + ",lock.[IsDeleted]" + "\n"
				+ ",lock.[IsDisabled]" + "\n" + ",lock.[MailoutInstructions]" + "\n" + " , cust.BankNumber" + "\n"
				+ "FROM [WFSDB_MAIL_TRACKER].[dbo].[Lockboxes] as lock" + "\n"
				+ "inner join dbo.Customers as cust on lock.CustomerId = cust.CustomerId" + "\n"
				+ "where LockboxNumber = " + lockboxNumber + "\n" + "and CustomerNumber = " + customerNumber + "\n"
				+ "and BankNumber = " + bankNumber + "\n";

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;

		TableData customerObject = DatabaseConnection.runSQLServerQuery(query);

		return customerObject;
	}

}// End of class
