package mailTracker.rest;

import API.REST;
import mailTracker.data.AuthorizationToken;
import mailTracker.pages.GlobalVariables;

public class DeliveryTypes_API {

	public static String post_DeliveryTypes(String contactId, String deliveryMethodId, String address,
			String deliveryTypeCriteriaId, String numberOfDays, String numberOfLabels, String[] deliverableItems)
			throws Exception {
		String deliverableItems_Formatted = "";

		// {"Id" : 178}
		for (String deliverable : deliverableItems) {
			if (deliverableItems_Formatted.equals("")) {
				deliverableItems_Formatted += "{\"Id\" : " + deliverable + "}";
			} else {
				deliverableItems_Formatted += ",\n" + "{\"Id\" : " + deliverable + "}";
			}
		}

		String postMessage1 = "{\"ContactId\": \"" + contactId + "\", " + "\n" + "\"DeliveryMethodId\": \""
				+ deliveryMethodId + "\"," + "\n" + "\"Address\":\"" + address + "\"," + "\n"
				+ "\"DeliveryTypeCriteriaId\": \"" + deliveryTypeCriteriaId + "\"," + "\n" + "\"NumberOfDays\": \""
				+ numberOfDays + "\"," + "\n" + "\"NumberOfLabels\": \"" + numberOfLabels + "\"," + "\n"
				+ "\"DeliverableItems\": [" + deliverableItems_Formatted + "]}";
		
		

		String newURL = GlobalVariables.startingURL + "deliverytype";

		String output = REST.httpPost(newURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

}// End of Class
