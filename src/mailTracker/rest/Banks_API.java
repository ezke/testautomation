package mailTracker.rest;

import java.sql.SQLException;
import java.text.MessageFormat;
import API.JSON;
import API.REST;
import mailTracker.data.AuthorizationToken;
import mailTracker.pages.GlobalVariables;
import utils.BaseUI;
import utils.DatabaseConnection;
import utils.TableData;

public class Banks_API {

	// private static String post_BankURL =
	// "http://scscdevqa/MailTracker.services/api/banks";

	// private static String post_BankURL =
	// "http://scscdevqa.qalabs.nwk/MailTracker.services/api/package";
	private static String post_BankURL = GlobalVariables.startingURL + "Banks";

	// ## should be the Bank ID you want to update.
	private static String put_BankURL = GlobalVariables.startingURL + "banks/";

	// NOTE: ## should be the Bank ID you want to delete.
	private static String delete_BankURL = GlobalVariables.startingURL + "Banks/";

	public static String bankIDNotValid_ErrorMessage = "Bank Id is required, must be numeric, and must be greater than 0. ";
	public static String bankID_Blank_ErrorMessage = "Invalid Bank Object, Bank is null. ";

	public static String bankShortName_Invalid_ErrorMessage_TooLong = "Invalid Short Name length, must be 50 characters or less. ";
	public static String bankLongName_Invalid_ErrorMessage_TooLong = "Invalid Long Name length, must be 100 characters or less. ";
	public static String bank_DoesNotExist_ErrorMessage = "Bank with Id {0} does not exist. ";

	public static String bank_DoesNotExist_ErrorMessage(String bankID) {
		return MessageFormat.format(bank_DoesNotExist_ErrorMessage, bankID);
	}

	public static String delete_Bank(String bankID) throws Exception {

		String output = REST.httpDelete(delete_BankURL + bankID, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String put_Bank(String bankID, String bankShortName, String bankLongName) throws Exception {

		String postMessage1 = "{\"Id\":" + bankID + ",\"ShortName\":\"" + bankShortName + "\",\"LongName\":\""
				+ bankLongName + "\"}";

		String output = REST.httpPut(put_BankURL + bankID, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String post_Bank(String bankID, String bankShortName, String bankLongName) throws Exception {

		String postMessage1 = "{\"Id\":" + "\"" + bankID + "\"" + ",\"ShortName\":\"" + bankShortName
				+ "\",\"LongName\":\"" + bankLongName + "\"}";

		String output = REST.httpPost(post_BankURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static void verify_BankField_IsInvalid(String bankID, String shortName, String longName,
			String expectedErrorText) throws Exception {
		String response = post_Bank(bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		bankID = bankID.replace("~", "").replace("!", "").replace("%", "").replace("^", "").replace("&", "");

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		if (!bankID.equals("") && bankID.matches("[0-9]+")) {
			verify_Bank_DoesNOT_Exist(bankID);
		}

	}

	public static void verify_Able_ToAddBank(String bankID, String shortName, String longName) throws Exception {

		String deleteResponse = delete_Bank_IfItExists(bankID);
		Thread.sleep(500);

		String apiResponse = Banks_API.post_Bank(bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.
		bankID = GlobalVariables.format_ID_ForComparison(bankID);
		BaseUI.baseStringCompare("Result", MessageFormat.format("Bank with Id {0} Created", bankID.trim()), result);

		BaseUI.verify_true_AndLog(Banks_API.bankExists_ByBankNumber(bankID),
				MessageFormat.format("Bank {0} was found in database.", bankID),
				MessageFormat.format("Bank {0} was NOT found in database.", bankID));

		TableData bankData = return_BankInfo_ByBankID(bankID);

		BaseUI.verify_true_AndLog(bankData.data.get(0).get("ShortName").equals(shortName),
				MessageFormat.format("Bank Short Name matched {0}", shortName), "Bank Short Name did NOT match.");

		BaseUI.verify_true_AndLog(bankData.data.get(0).get("LongName").equals(longName),
				MessageFormat.format("Bank Long Name  matched {0}", longName), "Bank Long Name did NOT match.");

	}

	public static void verify_Bank_SoftDeleted(String bankID) throws Exception {
		delete_Bank(bankID);
		TableData bankData = Banks_API.return_BankInfo_ByBankID(bankID);
		BaseUI.verify_true_AndLog(bankData.data.get(0).get("IsDeleted").equals("1"), "Previous bank was soft deleted.",
				"Previous bank was NOT soft deleted.");

	}

	public static void verify_Able_ToUpdateBank(String bankID, String shortName, String longName) throws Exception {

		String apiResponse = Banks_API.put_Bank(bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		BaseUI.baseStringCompare("Result", MessageFormat.format("Bank with Id {0} Created", bankID), result);

		BaseUI.verify_true_AndLog(Banks_API.bankExists_ByBankNumber(bankID),
				MessageFormat.format("Bank {0} was found in database.", bankID),
				MessageFormat.format("Bank {0} was NOT found in database.", bankID));

		TableData bankData = return_BankInfo_ByBankID(bankID);

		BaseUI.verify_true_AndLog(bankData.data.get(0).get("ShortName").equals(shortName),
				MessageFormat.format("Bank Short Name matched {0}", shortName), "Bank Short Name did NOT match.");

		BaseUI.verify_true_AndLog(bankData.data.get(0).get("LongName").equals(longName),
				MessageFormat.format("Bank Long Name  matched {0}", longName), "Bank Long Name did NOT match.");

	}

	// This overload. also checks to see that the short name and long names are
	// different.
	public static void verify_CannotAdd_Bank_AlreadyExists_AndLongAndShortNamesDontMatch(String bankID,
			String shortName, String longName) throws Exception {
		verify_CannotAdd_Bank_AlreadyExists(bankID, shortName, longName);

		TableData bankData = return_BankInfo_ByBankID(bankID);

		BaseUI.verify_true_AndLog(!bankData.data.get(0).get("ShortName").equals(shortName),
				MessageFormat.format("Bank Short Name should NOT have matched {0}", shortName),
				MessageFormat.format("Bank Short Name did not match. Expected {0}, saw {1}", shortName,
						bankData.data.get(0).get("ShortName")));

		BaseUI.verify_true_AndLog(!bankData.data.get(0).get("LongName").equals(longName),
				MessageFormat.format("Bank Long Name should NOT have matched {0}", longName),
				"Bank Long Name did not match.");

	}

	public static void verify_CannotAdd_Bank_AlreadyExists(String bankID, String shortName, String longName)
			throws Exception {
		BaseUI.verify_true_AndLog(bankExists_ByBankNumber(bankID),
				MessageFormat.format("Bank with Id {0} was found in Database.", bankID),
				MessageFormat.format("Bank with Id {0} was NOT found in Database.", bankID));

		String apiResponse = Banks_API.post_Bank(bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", MessageFormat.format("Bank with Id {0} already exists. ",
				GlobalVariables.format_ID_ForComparison(bankID)), errorMessage);

		BaseUI.baseStringCompare("Result", "", result);

	}

	public static void verify_Bank_DoesNOT_Exist(String bankID) throws Exception {
		BaseUI.verify_true_AndLog(!bankExists_ByBankNumber(bankID),
				MessageFormat.format("Bank with Id {0} was NOT found in Database.", bankID),
				MessageFormat.format("Bank with Id {0} was found in Database.", bankID));

	}

	public static TableData return_BankInfo_ByBankID(String bankID) throws Exception, SQLException {

		String query = "SELECT TOP 1 [BankNumber]" + "\n" + ",[ShortName]" + "\n" + ",[LongName]" + "\n"
				+ ",[IsDeleted]" + "\n" + "FROM [WFSDB_MAIL_TRACKER].[dbo].[Banks]" + "\n" + "where [BankNumber] = "
				+ bankID;

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;

		TableData bankObject = DatabaseConnection.runSQLServerQuery(query);

		return bankObject;
	}

	public static Boolean bankExists_ByBankNumber(String bankNumber) throws Exception {
		TableData bankObject = return_BankInfo_ByBankID(bankNumber);

		Boolean bankExists = false;

		if (bankObject.data.size() > 0) {
			if (bankObject.data.get(0).get("IsDeleted") == null
					|| bankObject.data.get(0).get("IsDeleted").equals("0")) {
				bankExists = true;
			} else {
				bankExists = false;
			}

		} else {

			bankExists = false;
		}

		return bankExists;
	}

	public static String delete_Bank_IfItExists(String bankNumber) throws Exception {
		bankNumber = bankNumber.trim();
		Boolean bankExists = bankExists_ByBankNumber(bankNumber);
		String outputString = null;
		if (bankExists) {
			outputString = delete_Bank(bankNumber);
		}

		return outputString;
	}

}
