package mailTracker.rest;

import java.text.MessageFormat;
import java.util.HashMap;
import API.JSON;
import API.REST;
import mailTracker.data.AuthorizationToken;
import mailTracker.pages.GlobalVariables;
import utils.BaseUI;
import utils.DatabaseConnection;
import utils.TableData;

public class Contacts_API {
	
	public static String email_InvalidLength_ErrorMessage = "Invalid Email length, must be 60 characters or less. ";

	public static String contact_DoesNOTExist_ErrorMessage(String contactID) {
		return MessageFormat.format("Contact with Id {0} does not exist. ", contactID);
	}

	public static String contact_AlreadyExists_ErrorMessage(String contactID) {
		return MessageFormat.format("Contact with Id {0} already exists. ", contactID);
	}

	public static String contact_AlreadyExists_ErrorMessage(String contactName, String lockboxId, String customerId,
			String bankId) {
		return MessageFormat.format(
				"Contact with Name {0} and Lockbox Id {1} and Customer Id {2} and Bank Id {3} already exists. ",
				contactName, lockboxId, customerId, bankId);
	}

	public static String contact_Phone_Invalid_ErrorMessage = "Invalid Phone. Allowed format (###-###-####) and must begin with number 2-9. ";
	public static String contact_fax_Invalid_ErrorMessage = "Invalid Fax. Allowed format (###-###-####) and must begin with number 2-9. ";
	public static String contact_InvalidContactObject_Null_ErrorMessage = "Invalid Contact Object, Contact is null. ";
	public static String name_is_required_ErrorMessage = "Name is required. ";

	public static String post_Contact(String name, String lockboxNumber, String customerNumber, String bankNumber,
			String address1, String email, String phone, String fax) throws Exception {
		String postMessage1 = "{ \"LockboxId\": \"" + lockboxNumber + "\"," + "\n" + "\"CustomerId\": \""
				+ customerNumber + "\"," + "\n" + "\"BankId\" : \"" + bankNumber + "\"," + "\n" + "\"Name\" : \"" + name
				+ "\"," + "\n" + "\"Address\" : \"" + address1 + "\"," + "\n" + "\"Email\" : \"" + email + "\"," + "\n"
				+ "\"Phone\" : \"" + phone + "\"," + "\n" + "\"Fax\" : \"" + fax + "\"" + "\n" + "}";

		String newURL = GlobalVariables.startingURL + "contacts";

		String output = REST.httpPost(newURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String get_Contact(String contactName, String lockboxNumber, String customerNumber, String bankID)
			throws Exception {

		String contactID = Contacts_API.return_Contact_ID(contactName, lockboxNumber, customerNumber, bankID);

		String newURL = GlobalVariables.startingURL + "contacts/" + contactID;

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String get_Contact(String contactId) throws Exception {
		String newURL = GlobalVariables.startingURL + "contacts/" + contactId;

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static TableData get_ALL_Contacts(String lockboxNumber, String customerNumber, String bankNumber)
			throws Exception {
		String newURL = GlobalVariables.startingURL + "contacts/lockbox/" + lockboxNumber + "/customer/"
				+ customerNumber + "/bank/" + bankNumber;

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		TableData all_ContactsData = JSON.return_JSON_Array_AsTableData(output, "Result");
		all_ContactsData.replaceKey_ForGivenKey("Id", "ContactId");
		all_ContactsData.replaceKey_ForGivenKey("BankId", "BankNumber");
		all_ContactsData.replaceKey_ForGivenKey("LockboxId", "LockboxNumber");
		all_ContactsData.replaceKey_ForGivenKey("CustomerId", "CustomerNumber");
		
		return all_ContactsData;
	}

	// Need a HashMap with lockboxNumber, customerNumber, bankNumber,
	// contactName,
	// Fields address1, email, phone, fax are optional.

	public static String post_Contact_NotAllFields(HashMap<String, String> fieldsMap) throws Exception {
		String postMessage1 = "{ \"LockboxId\": \"" + fieldsMap.get("lockboxNumber") + "\"," + "\n"
				+ "\"CustomerId\": \"" + fieldsMap.get("customerNumber") + "\"," + "\n" + "\"BankId\" : \""
				+ fieldsMap.get("bankNumber") + "\"," + "\n" + "\"Name\" : \"" + fieldsMap.get("contactName") + "\"";

		if (fieldsMap.get("address1") != null) {
			postMessage1 += "," + "\n" + "\"Address\" : \"" + fieldsMap.get("address1") + "\"";
		}
		if (fieldsMap.get("email") != null) {
			postMessage1 += "," + "\n" + "\"Email\" : \"" + fieldsMap.get("email") + "\"";
		}
		if (fieldsMap.get("phone") != null) {
			postMessage1 += "," + "\n" + "\"Phone\" : \"" + fieldsMap.get("phone") + "\"";
		}
		if (fieldsMap.get("fax") != null) {
			postMessage1 += "," + "\n" + "\"Fax\" : \"" + fieldsMap.get("fax") + "\"";
		}

		String newURL = GlobalVariables.startingURL + "contacts";

		String output = REST.httpPost(newURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String return_Contact_ID(String contactName, String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {
		TableData contact = return_Contact(contactName, lockboxNumber, customerNumber, bankNumber);

		return contact.data.get(0).get("ContactId");
	}

	public static TableData return_Contact(String contactName, String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {
		String query = "SELECT [ContactId]" + "\n" + ",bank.BankNumber" + "\n" + ", customer.CustomerNumber" + "\n"
				+ ", lockbox.LockboxNumber" + "\n" + ",contact.[LockboxId]" + "\n" + ",[Name]" + "\n" + ",[Address]"
				+ "\n" + ",[Phone]" + "\n" + ",[Email]" + "\n" + ",[Fax]" + "\n" + ",contact.[IsDeleted]" + "\n"
				+ "FROM [WFSDB_MAIL_TRACKER].[dbo].[Contacts] as contact" + "\n"
				+ "inner join [WFSDB_MAIL_TRACKER].[dbo].Lockboxes as lockbox on lockbox.LockboxId = contact.LockboxId"
				+ "\n"
				+ "inner join [WFSDB_MAIL_TRACKER].[dbo].Customers as customer on customer.CustomerId = lockbox.CustomerId"
				+ "\n" + " inner join [WFSDB_MAIL_TRACKER].[dbo].Banks as bank on bank.BankNumber = customer.BankNumber"
				+ "\n"

				+ "where lockbox.LockboxNumber = '" + lockboxNumber + "'" + "\n" + "and bank.BankNumber = '"
				+ bankNumber + "'" + "\n" + "and customer.CustomerNumber = '" + customerNumber + "'" + "\n"
				+ "and Name = '" + contactName + "'" + "\n";

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;
		TableData contactObject = DatabaseConnection.runSQLServerQuery(query);

		contactObject.replace_Null_With_Empty();

		return contactObject;
	}

	public static TableData return_Contact(String contactID) throws Exception {
		String query = "SELECT [ContactId]" + "\n" + ",bank.BankNumber" + "\n" + ", customer.CustomerNumber" + "\n"
				+ ", lockbox.LockboxNumber" + "\n" + ",contact.[LockboxId]" + "\n" + ",[Name]" + "\n" + ",[Address]"
				+ "\n" + ",[Phone]" + "\n" + ",[Email]" + "\n" + ",[Fax]" + "\n" + ",contact.[IsDeleted]" + "\n"
				+ "FROM [WFSDB_MAIL_TRACKER].[dbo].[Contacts] as contact" + "\n"
				+ "inner join [WFSDB_MAIL_TRACKER].[dbo].Lockboxes as lockbox on lockbox.LockboxId = contact.LockboxId"
				+ "\n"
				+ "inner join [WFSDB_MAIL_TRACKER].[dbo].Customers as customer on customer.CustomerId = lockbox.CustomerId"
				+ "\n" + " inner join [WFSDB_MAIL_TRACKER].[dbo].Banks as bank on bank.BankNumber = customer.BankNumber"
				+ "\n"

				+ "where ContactId = '" + contactID + "'";

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;
		TableData contactObject = DatabaseConnection.runSQLServerQuery(query);

		contactObject.replace_Null_With_Empty();

		return contactObject;
	}

	public static TableData return_AllContacts_FromDatabase_ForData(String lockboxNumber, String customerNumber, String bankNumber) throws Exception {
		String query = "SELECT top 1000 [ContactId],bank.BankNumber, customer.CustomerNumber, lockbox.LockboxNumber"
				+ ",[Name],[Address],[Phone],[Email],[Fax],contact.[IsDeleted]" + "\n"
				+ "FROM [WFSDB_MAIL_TRACKER].[dbo].[Contacts] as contact" + "\n"
				+ " inner join [WFSDB_MAIL_TRACKER].[dbo].Lockboxes as lockbox on lockbox.LockboxId = contact.LockboxId"
				+ "\n"
				+ " inner join [WFSDB_MAIL_TRACKER].[dbo].Customers as customer on customer.CustomerId = lockbox.CustomerId"
				+ "\n" + " inner join [WFSDB_MAIL_TRACKER].[dbo].Banks as bank on bank.BankNumber = customer.BankNumber"
				+ "\n" + "   where lockbox.LockboxNumber = '" + lockboxNumber + "'\n"
				+ "	and customer.CustomerNumber = '" + customerNumber + "'\n" + "	and bank.BankNumber = '"
				+ bankNumber + "'";

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;
		TableData contactObject = DatabaseConnection.runSQLServerQuery(query);

		contactObject.replace_Null_With_Empty();

		return contactObject;
	}

	public static String delete_Contact(String name, String lockboxNumber, String customerNumber, String bankNumber)
			throws Exception {

		TableData contactToDelete = return_Contact(name, lockboxNumber, customerNumber, bankNumber);

		String contactId = contactToDelete.data.get(0).get("ContactId");

		String newURL = GlobalVariables.startingURL + "contacts/" + contactId;
		// GlobalVariables.startingURL + "lockboxes/" + lockboxNumber +
		// "/customer/" + customerNumber
		// + "/bank/" + bankID;

		String output = REST.httpDelete(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String delete_Contact_IfExists(String name, String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {

		String output = "";

		if (Contacts_API.contact_ExistsAndIsNOTDeleted(name, lockboxNumber, customerNumber, bankNumber)) {
			output = delete_Contact(name, lockboxNumber, customerNumber, bankNumber);
		}

		return output;

	}

	public static String delete_Contact(String contactID) throws Exception {

		String newURL = GlobalVariables.startingURL + "contacts/" + contactID;
		// GlobalVariables.startingURL + "lockboxes/" + lockboxNumber +
		// "/customer/" + customerNumber
		// + "/bank/" + bankID;

		String output = REST.httpDelete(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static void verify_Able_ToAddContact(String contactName, String lockboxNumber, String customerNumber,
			String bankNumber, String address1, String email, String phone, String fax) throws Exception {

		String deleteResponse = "";
		if (contact_ExistsAndIsNOTDeleted(contactName, lockboxNumber, customerNumber, bankNumber)) {
			deleteResponse = delete_Contact(contactName, lockboxNumber, customerNumber, bankNumber);
		}

		String apiResponse = post_Contact(contactName, lockboxNumber, customerNumber, bankNumber, address1, email,
				phone, fax);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.

		String contactId = return_Contact_ID(contactName, lockboxNumber, customerNumber, bankNumber);
		BaseUI.baseStringCompare("Result", MessageFormat.format("Contact with Id {0} Created", contactId), result);

		verify_Contact_Exists(contactName, lockboxNumber, customerNumber, bankNumber, address1, email, phone, fax);
	}

	public static void verify_Get_Contact_IsInvalid(String contactId, String expectedErrorText) throws Exception {
		String response = get_Contact(contactId);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);
	}

	public static void verify_Able_ToAddContact(HashMap<String, String> fieldsMap) throws Exception {

		String deleteResponse = "";
		if (contact_ExistsAndIsNOTDeleted(fieldsMap.get("contactName"), fieldsMap.get("lockboxNumber"),
				fieldsMap.get("customerNumber"), fieldsMap.get("bankNumber"))) {
			deleteResponse = delete_Contact(fieldsMap.get("contactName"), fieldsMap.get("lockboxNumber"),
					fieldsMap.get("customerNumber"), fieldsMap.get("bankNumber"));
		}

		String apiResponse = post_Contact_NotAllFields(fieldsMap);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.

		String contactId = return_Contact_ID(fieldsMap.get("contactName"), fieldsMap.get("lockboxNumber"),
				fieldsMap.get("customerNumber"), fieldsMap.get("bankNumber"));
		BaseUI.baseStringCompare("Result", MessageFormat.format("Contact with Id {0} Created", contactId), result);

		verify_Contact_Exists(fieldsMap.get("contactName"), fieldsMap.get("lockboxNumber"),
				fieldsMap.get("customerNumber"), fieldsMap.get("bankNumber"), fieldsMap.get("address1"),
				fieldsMap.get("email"), fieldsMap.get("phone"), fieldsMap.get("fax"));
	}

	public static void verify_Contact_Exists(String contactName, String lockboxNumber, String customerNumber,
			String bankID, String address, String email, String phone, String fax) throws Exception {

		// BaseUI.verify_true_AndLog(contact_ExistsAndIsNOTDeleted(contactName,
		// lockboxNumber, customerNumber, bankID),
		// MessageFormat.format("Contact {0} was found in database.",
		// contactName),
		// MessageFormat.format("Contact {0} was NOT found in database.",
		// contactName));

		TableData contactData = return_Contact(contactName, lockboxNumber, customerNumber, bankID);

		BaseUI.verify_true_AndLog(contactData.data.size() == 1, "Found 1 entry.", "Entries found did not equal 1.");

		BaseUI.verify_true_AndLog(contactData.data.get(0).get("LockboxNumber").equals(lockboxNumber),
				MessageFormat.format("Lockbox Number matched {0}", lockboxNumber), "Lockbox Number did NOT match.");

		BaseUI.verify_true_AndLog(contactData.data.get(0).get("Name").equals(contactName),
				MessageFormat.format("Contact Name matched {0}", contactName), "Contact Name did NOT match.");

		address = address == null ? "" : address;
		BaseUI.verify_true_AndLog(contactData.data.get(0).get("Address").equals(address),
				MessageFormat.format("Contact Address matched {0}", address), "Contact Address did NOT match.");

		email = email == null ? "" : email;
		BaseUI.verify_true_AndLog(contactData.data.get(0).get("Email").equals(email),
				MessageFormat.format("Contact Email matched {0}", email), "Contact Email did NOT match.");

		phone = phone == null ? "" : phone;
		BaseUI.verify_true_AndLog(contactData.data.get(0).get("Phone").equals(phone),
				MessageFormat.format("Contact Phone matched {0}", phone), "Contact Phone did NOT match.");

		fax = fax == null ? "" : fax;
		BaseUI.verify_true_AndLog(contactData.data.get(0).get("Fax").equals(fax),
				MessageFormat.format("Contact Fax matched {0}", fax), "Contact Fax did NOT match.");

		BaseUI.verify_true_AndLog(contactData.data.get(0).get("CustomerNumber").equals(customerNumber),
				MessageFormat.format("Customer Number matched {0}", customerNumber), "Customer Number did NOT match.");

		BaseUI.verify_true_AndLog(contactData.data.get(0).get("BankNumber").equals(bankID),
				MessageFormat.format("Bank Number matched {0}", bankID), "Bank Number did NOT match.");

		String expectedValue = "0";

		//If this initially fails when you create a test, run the test a second time.
		BaseUI.verify_true_AndLog(contactData.data.get(0).get("IsDeleted").equals(expectedValue),
				MessageFormat.format("IsDeleted matched {0}", expectedValue),
				MessageFormat.format("IsDeleted did NOT match, expected {0}, seeing {1}.", expectedValue,
						contactData.data.get(0).get("IsDeleted")));

	}

	public static void verify_Able_ToDelete_Contact(String contactName, String lockboxNumber, String customerNumber,
			String bankID) throws Exception {

		String deleteResponse = delete_Contact(contactName, lockboxNumber, customerNumber, bankID);

		String errorMessage = JSON.return_JSON_Value(deleteResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(deleteResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		String contactID = return_Contact_ID(contactName, lockboxNumber, customerNumber, bankID);
		BaseUI.baseStringCompare("Result", MessageFormat.format("Contact with Id {0} Deleted", contactID), result);

		BaseUI.verify_false_AndLog(contact_ExistsAndIsNOTDeleted(contactName, lockboxNumber, customerNumber, bankID),
				"Contact does not exist or was soft deleted.", "Contact was NOT successfully deleted.");
	}

	public static void verify_Delete_Contact_IsInvalid(String contactName, String lockboxNumber, String customerID,
			String bankID, String expectedErrorText) throws Exception {
		String response = delete_Contact(contactName, lockboxNumber, customerID, bankID);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		if (!contactName.equals("") && !lockboxNumber.equals("") && lockboxNumber.matches("[0-9]+")
				&& !customerID.equals("") && customerID.matches("[0-9]+") && !bankID.equals("")
				&& bankID.matches("[0-9]+")) {
			BaseUI.verify_true_AndLog(contact_ExistsAndIsNOTDeleted(contactName, lockboxNumber, customerID, bankID),
					"Contact was NOT successfully deleted.", "Contact does not exist or was soft deleted.");
		}
	}

	public static void verify_Delete_Contact_IsInvalid(String contactID, String expectedErrorText) throws Exception {
		String response = delete_Contact(contactID);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		// if (!contactID.equals("") && contactID.matches("[0-9]+")) {
		// BaseUI.verify_true_AndLog(contact_ExistsAndIsNOTDeleted(contactID),
		// "Contact was NOT successfully deleted.",
		// "Contact does not exist or was soft deleted.");
		// }
	}

	public static void verify_Contact_Post_IsInvalid(String contactName, String lockboxNumber, String customerNumber,
			String bankNumber, String address1, String email, String phone, String fax, String expectedErrorText)
			throws Exception {

		String response = post_Contact(contactName, lockboxNumber, customerNumber, bankNumber, address1, email, phone,
				fax);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

	}
	
	public static void verify_GET_Contact_Exists(String contactName, String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {

		TableData customerData = Contacts_API.return_Contact(contactName, lockboxNumber, customerNumber, bankNumber);
		
		String contactId = Contacts_API.return_Contact_ID(contactName, lockboxNumber, customerNumber, bankNumber);
		
		String getOutput = get_Contact(contactId);

		String result_JSON = JSON.return_JSON_Value(getOutput, "Result");

		BaseUI.baseStringCompare("Contact Id", contactId, JSON.return_JSON_Value(result_JSON, "Id"));
		BaseUI.baseStringCompare("Contact Name", customerData.data.get(0).get("Name"), JSON.return_JSON_Value(result_JSON, "Name"));
		BaseUI.baseStringCompare("Address", customerData.data.get(0).get("Address"), JSON.return_JSON_Value(result_JSON, "Address"));

		BaseUI.baseStringCompare("Bank Number", bankNumber, JSON.return_JSON_Value(result_JSON, "BankId"));
		BaseUI.baseStringCompare("Customer Number", customerNumber,
				JSON.return_JSON_Value(result_JSON, "CustomerId"));
	}

	// Returns true if element exists and is not marked as deleted.
	public static Boolean contact_ExistsAndIsNOTDeleted(String contactID) throws Exception {
		TableData contactObject = return_Contact(contactID);
		Boolean contactExists = false;

		if (contactObject.data.size() > 0) {
			if (contactObject.data.get(0).get("IsDeleted") == null
					|| contactObject.data.get(0).get("IsDeleted").equals("0")
					|| contactObject.data.get(0).get("IsDeleted").equals("")) {
				contactExists = true;
			} else {
				contactExists = false;
			}

		} else {

			contactExists = false;
		}

		return contactExists;
	}

	// Returns true if element exists and is not marked as deleted.
	public static Boolean contact_ExistsAndIsNOTDeleted(String contactName, String lockboxNumber, String customerNumber,
			String bankNumber) throws Exception {
		TableData contactObject = return_Contact(contactName, lockboxNumber, customerNumber, bankNumber);
		Boolean contactExists = false;

		if (contactObject.data.size() > 0) {
			if (contactObject.data.get(0).get("IsDeleted") == null
					|| contactObject.data.get(0).get("IsDeleted").equals("0")
					|| contactObject.data.get(0).get("IsDeleted").equals("")) {
				contactExists = true;
			} else {
				contactExists = false;
			}

		} else {

			contactExists = false;
		}

		return contactExists;
	}

}// End of Class
