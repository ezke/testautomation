package mailTracker.rest;

import java.sql.SQLException;
import java.text.MessageFormat;
import API.JSON;
import API.REST;
import mailTracker.data.AuthorizationToken;
import mailTracker.pages.GlobalVariables;
import utils.BaseUI;
import utils.DatabaseConnection;
import utils.TableData;

public class Customers_API {

	// public static String delete_CustomerURL = GlobalVariables.startingURL +
	// "customers/";
	public static String put_CustomerURL = GlobalVariables.startingURL + "customers/";
	public static String post_CustomerURL = GlobalVariables.startingURL + "customers";

	public static String customerID_NotValid_ErrorMessage = "Customer Id is required, must be numeric, and must be greater than 0. ";

	public static String customer_AlreadyExists_ErrorMessage = "Customer with Id {0} and Bank Id {1} already exists. ";

	public static String customer_DoesNOT_Exist(String customerID, String bankID)
	{
		return MessageFormat.format("Customer with Id {0} and Bank Id {1} does not exist. ", customerID, bankID);
	}

	public static String customer_PreviouslyDeleted = "Customer with Id {0} and Bank Id {1} has been previously deleted. ";

	public static String customer_CannotBeDeleted_HasLockbox(String customerID, String bankID) {

		return MessageFormat.format(
				"Cannot Delete Customer with Id {0} and Bank Id {1}, it is associated to one or more Lockboxes. ",
				customerID, bankID);
	}

	public static String delete_Customer(String customerNumber, String bankID) throws Exception {

		String newURL = GlobalVariables.startingURL + "customers/" + customerNumber + "/bank/" + bankID;

		String output = REST.httpDelete(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String get_Customer(String customerNumber, String bankID) throws Exception {

		String newURL = GlobalVariables.startingURL + "customers/" + customerNumber + "/bank/" + bankID;

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String get_ALL_Customers() throws Exception {

		String newURL = GlobalVariables.startingURL + "customers/";

		String output = REST.httpGet(newURL, null, AuthorizationToken.return_JWT_Token_forAPI());

		return output;

	}

	public static String put_Customer(String customerNumber, String bankID, String customerShortName,
			String customerLongName) throws Exception {

		customerNumber = customerNumber.equals("") ? "0" : customerNumber;

		String postMessage1 = "{\"CustomerId\": \"" + customerNumber + "\"," + "\"BankId\": \"" + bankID + "\","
				+ "\"ShortName\" : \"" + customerShortName + "\"," + "\"LongName\" : \"" + customerLongName + "\""
				+ "}";

		String newURL = GlobalVariables.startingURL + "customers/" + customerNumber;

		String output = REST.httpPut(newURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static String post_Customer(String customerName, String bankID, String customerShortName,
			String customerLongName) throws Exception {

		String postMessage1 = "{\"CustomerId\": \"" + customerName + "\"," + "\"BankId\": \"" + bankID + "\","
				+ "\"ShortName\" : \"" + customerShortName + "\"," + "\"LongName\" : \"" + customerLongName + "\""
				+ "}";

		String output = REST.httpPost(post_CustomerURL, postMessage1, AuthorizationToken.return_JWT_Token_forAPI());

		return output;
	}

	public static TableData return_ALL_Customers_from_APICall() throws Exception {
		String output = get_ALL_Customers();
		TableData customerData = JSON.return_JSON_Array_AsTableData(output, "Result");
		customerData.replaceKey_ForGivenKey("CustomerId", "CustomerNumber");
		customerData.replaceKey_ForGivenKey("BankId", "BankNumber");

		customerData.replaceKey_ForGivenKey("Id", "CustomerId");
		customerData.sort_ByColumn_numeric_Descending("CustomerId");

		return customerData;

	}

	public static TableData return_ALL_Customers_fromDatabase() throws Exception {
		String query = "SELECT  [CustomerId]" + ",[CustomerNumber]" + "\n" + ",[BankNumber]" + "\n" + ",[ShortName]"
				+ "\n" + ",[LongName]" + "\n" + ",[IsDeleted]" + "\n" + "FROM [WFSDB_MAIL_TRACKER].[dbo].[Customers]"
				+ "\n" + "where IsDeleted != 1 or IsDeleted is null" + "\n";

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;

		TableData customerObject = DatabaseConnection.runSQLServerQuery(query);
		customerObject.replace_Null_With_Value_ForColumn("IsDeleted", "false");
		customerObject.replaceValue_ForColumn_WhenEqualTo("IsDeleted", "0", "false");
		customerObject.sort_ByColumn_numeric_Descending("CustomerId");

		return customerObject;

	}

	public static TableData return_CustomerInfo_ByCustomerNumber(String customerID, String bankID)
			throws Exception, SQLException {

		String query = "SELECT TOP 1 [CustomerId]" + "\n" + ",[CustomerNumber]" + "\n" + ",[BankNumber]" + "\n"
				+ ",[ShortName]" + "\n" + ",[LongName]" + "\n" + ",[IsDeleted]" + "\n"
				+ "FROM [WFSDB_MAIL_TRACKER].[dbo].[Customers]" + "\n" + "where CustomerNumber =" + customerID
				+ "and BankNumber = " + bankID;

		DatabaseConnection.dbUrl = GlobalVariables.dbConnectionString;

		TableData customerObject = DatabaseConnection.runSQLServerQuery(query);

		return customerObject;
	}

	public static Boolean customerExists_ByCustomerNumber_AndBankNumber(String customerID, String bankID)
			throws Exception {
		TableData customerObject = return_CustomerInfo_ByCustomerNumber(customerID, bankID);

		Boolean customerExists = false;

		if (customerObject.data.size() > 0) {
			if (customerObject.data.get(0).get("IsDeleted") == null
					|| customerObject.data.get(0).get("IsDeleted").equals("0")) {
				customerExists = true;
			} else {
				customerExists = false;
			}

		} else {

			customerExists = false;
		}

		return customerExists;
	}

	public static String delete_Customer_IfItExists(String customerName, String bankID) throws Exception {
		customerName = customerName.trim();
		Boolean customerExists = customerExists_ByCustomerNumber_AndBankNumber(customerName, bankID);
		String outputString = null;
		if (customerExists) {
			outputString = delete_Customer(customerName, bankID);
		}

		return outputString;
	}

	public static void verify_CustomerField_IsInvalid_AndCustomerDoesNOTExist(String customerID, String bankID,
			String shortName, String longName, String expectedErrorText) throws Exception {
		String response = post_Customer(customerID, bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		customerID = customerID.replace("~", "").replace("!", "").replace("%", "").replace("^", "").replace("&", "");

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		if (!customerID.equals("") && customerID.matches("[0-9]+")) {
			verify_Customer_DoesNOT_Exist(customerID, bankID);
		}
	}

	public static void verify_CustomerField_IsInvalid(String customerID, String bankID, String shortName,
			String longName, String expectedErrorText) throws Exception {
		String response = post_Customer(customerID, bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);


	}

	public static void verify_Update_CustomerField_IsInvalid_(String customerID, String bankID, String shortName,
			String longName, String expectedErrorText) throws Exception {
		String response = put_Customer(customerID, bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		customerID = customerID.replace("~", "").replace("!", "").replace("%", "").replace("^", "").replace("&", "");

		// Cannot run bank query if blank or contains letters. Will cause
		// database error.
		if (!customerID.equals("") && customerID.matches("[0-9]+")) {
			verify_Customer_DoesNOT_Exist(customerID, bankID);
		}
	}

	public static void verify_Delete_Customer_IsInvalid_(String customerID, String bankID, String shortName,
			String longName, String expectedErrorText) throws Exception {
		String response = delete_Customer(customerID, bankID);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

		customerID = customerID.replace("~", "").replace("!", "").replace("%", "").replace("^", "").replace("&", "");

	}

	public static void verify_Customer_DoesNOT_Exist(String customerID, String bankID) throws Exception {
		BaseUI.verify_true_AndLog(!customerExists_ByCustomerNumber_AndBankNumber(customerID, bankID),
				MessageFormat.format("Customer with Id {0} was NOT found in Database.", customerID),
				MessageFormat.format("Customer with Id {0} was found in Database.", customerID));

	}

	public static void verify_Able_ToAddCustomer(String customerNumber, String bankID, String shortName,
			String longName) throws Exception {

		String deleteResponse = delete_Customer_IfItExists(customerNumber, bankID);

		String apiResponse = post_Customer(customerNumber, bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.
		customerNumber = GlobalVariables.format_ID_ForComparison(customerNumber);
		BaseUI.baseStringCompare("Result",
				MessageFormat.format("Customer with Id {0} and Bank Id {1} Created", customerNumber, bankID), result);

		verify_Customer_Exists(customerNumber, bankID, shortName, longName);
	}

	public static void verify_Customer_Exists(String customerNumber, String bankID, String shortName, String longName)
			throws Exception {

		BaseUI.verify_true_AndLog(customerExists_ByCustomerNumber_AndBankNumber(customerNumber, bankID),
				MessageFormat.format("Customer {0} was found in database.", customerNumber),
				MessageFormat.format("Customer {0} was NOT found in database.", customerNumber));

		TableData customerData = return_CustomerInfo_ByCustomerNumber(customerNumber, bankID);

		BaseUI.verify_true_AndLog(customerData.data.get(0).get("ShortName").equals(shortName),
				MessageFormat.format("Customer Short Name matched {0}", shortName),
				"Customer Short Name did NOT match.");

		BaseUI.verify_true_AndLog(customerData.data.get(0).get("LongName").equals(longName),
				MessageFormat.format("Customer Long Name  matched {0}", longName), "Customer Long Name did NOT match.");

		BaseUI.verify_true_AndLog(customerData.data.get(0).get("BankNumber").equals(bankID),
				MessageFormat.format("Bank Number matched {0}", bankID), "Bank Number did NOT match.");

	}

	public static void verify_Get_Customer_IsInvalid_(String customerID, String bankID, String expectedErrorText)
			throws Exception {
		String response = get_Customer(customerID, bankID);

		String errorMessage = JSON.return_JSON_Value(response, "ErrorMessage");

		String result = JSON.return_JSON_Value(response, "Result");

		BaseUI.baseStringCompare("Error Message.", expectedErrorText, errorMessage);

		BaseUI.baseStringCompare("Error Message.", "", result);

	}

	public static void verify_Able_ToUpdateCustomer(String customerNumber, String bankID, String shortName,
			String longName) throws Exception {

		String apiResponse = put_Customer(customerNumber, bankID, shortName, longName);

		String errorMessage = JSON.return_JSON_Value(apiResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(apiResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.
		customerNumber = GlobalVariables.format_ID_ForComparison(customerNumber);
		BaseUI.baseStringCompare("Result",
				MessageFormat.format("Customer with Id {0} and Bank Id {1} Updated", customerNumber, bankID), result);

		BaseUI.verify_true_AndLog(customerExists_ByCustomerNumber_AndBankNumber(customerNumber, bankID),
				MessageFormat.format("Customer {0} was found in database.", customerNumber),
				MessageFormat.format("Customer {0} was NOT found in database.", customerNumber));

		TableData customerData = return_CustomerInfo_ByCustomerNumber(customerNumber, bankID);

		BaseUI.verify_true_AndLog(customerData.data.get(0).get("ShortName").equals(shortName),
				MessageFormat.format("Customer Short Name matched {0}", shortName),
				"Customer Short Name did NOT match.");

		BaseUI.verify_true_AndLog(customerData.data.get(0).get("LongName").equals(longName),
				MessageFormat.format("Customer Long Name  matched {0}", longName), "Customer Long Name did NOT match.");

		BaseUI.verify_true_AndLog(customerData.data.get(0).get("BankNumber").equals(bankID),
				MessageFormat.format("Bank Number matched {0}", bankID), "Bank Number did NOT match.");

	}

	public static void verify_Customer_SoftDeleted(String customerNumber, String bankID) throws Exception {
		delete_Customer(customerNumber, bankID);
		TableData customerData = return_CustomerInfo_ByCustomerNumber(customerNumber, bankID);
		BaseUI.verify_true_AndLog(customerData.data.get(0).get("IsDeleted").equals("1"), "Customer was soft deleted.",
				"Customer was NOT soft deleted.");
	}

	public static void verify_GetCustomer(String customerNumber, String bankID, String shortName, String longName)
			throws Exception {
		String getResponse = Customers_API.get_Customer(customerNumber, bankID);

		BaseUI.baseStringCompare("Status Code", "OK", JSON.return_JSON_Value(getResponse, "StatusCode"));

		BaseUI.baseStringCompare("Error Message", "", JSON.return_JSON_Value(getResponse, "ErrorMessage"));

		String resultJSON = JSON.return_JSON_Value(getResponse, "Result");

		BaseUI.baseStringCompare("Customer Id", customerNumber, JSON.return_JSON_Value(resultJSON, "CustomerId"));

		BaseUI.baseStringCompare("Short Name", shortName, JSON.return_JSON_Value(resultJSON, "ShortName"));

		BaseUI.baseStringCompare("Long Name", longName, JSON.return_JSON_Value(resultJSON, "LongName"));

		BaseUI.baseStringCompare("Bank Id", bankID, JSON.return_JSON_Value(resultJSON, "BankId"));

		BaseUI.baseStringCompare("IsDeleted", "false", JSON.return_JSON_Value(resultJSON, "IsDeleted"));
	}

	public static void verify_Able_ToDelete_Customer(String customerNumber, String bankID, String shortName,
			String longName) throws Exception {

		String deleteResponse = delete_Customer(customerNumber, bankID);

		String errorMessage = JSON.return_JSON_Value(deleteResponse, "ErrorMessage");

		String result = JSON.return_JSON_Value(deleteResponse, "Result");

		BaseUI.baseStringCompare("Error Message.", "", errorMessage);

		// Trims bankID to remove leading and ending spaces. Also converted to
		// Integer and back to String to simulate
		// Api manipulation. Should work for removing leading 0's.
		customerNumber = GlobalVariables.format_ID_ForComparison(customerNumber);
		BaseUI.baseStringCompare("Result",
				MessageFormat.format("Customer with Id {0} and Bank Id {1} Deleted", customerNumber, bankID), result);

		BaseUI.verify_false_AndLog(customerExists_ByCustomerNumber_AndBankNumber(customerNumber, bankID),
				"Customer does not exist or was soft deleted.", "Customer was NOT successfully deleted.");

	}

}
