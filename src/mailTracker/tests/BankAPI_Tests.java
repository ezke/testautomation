package mailTracker.tests;

import org.testng.annotations.Test;
import mailTracker.pages.GlobalVariables;
import mailTracker.rest.Banks_API;

public class BankAPI_Tests {

	@Test
	public void TC119_MailTracker_AddNewBank_ZeroBankID_ReturnsError() throws Exception {
		String bankID = "0";
		String shortName = "WFS";
		String longName = "Zero Bank";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}

	@Test
	public void TC120_MailTracker_AddNewBank_BlankBankID_ReturnsError() throws Exception {
		String bankID = "";
		String shortName = "WFS";
		String longName = "Zero Bank";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}

	// check the bank ID to make sure it's not in use if this test fails
	// stupendously.
	@Test
	public void TC121_MailTracker_AddNewBank_NoLongNameReturnsError() throws Exception {
		String bankID = "70001";
		String shortName = "WFS";
		String longName = "";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, GlobalVariables.LongName_Empty_ErrorMessage);
	}

	@Test
	public void TC122_MailTracker_AbleToAddBank() throws Exception {
		String bankID = "70000";
		String shortName = "shortName";
		String longName = "STuff1";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}

	@Test
	public void TC123_MailTracker_AddBank_BankID_SameAs_ExistingBank() throws Exception {
		String bankID = "70002";
		String shortName = "newShort";
		String longName = "newLong";

		Banks_API.post_Bank(bankID, "oldName", "OldLongName");
		Banks_API.verify_CannotAdd_Bank_AlreadyExists_AndLongAndShortNamesDontMatch(bankID, shortName, longName);
	}

	@Test
	public void TC124_MailTracker_AddBank_BankID_OVER_MaxLength_CausesError() throws Exception {
		String bankID = "12345678901234567890";
		String shortName = "Max";
		String longName = "Length";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}

	@Test
	public void TC125_MailTracker_AddBank_ShortName_OVER_MaxLength_CausesError() throws Exception {
		String bankID = "70003";
		String shortName = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
		String longName = "Max Length";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankShortName_Invalid_ErrorMessage_TooLong);
	}

	@Test
	public void TC126_MailTracker_AddBank_LongName_OVER_MaxLength_CausesError() throws Exception {
		String bankID = "70004";
		String shortName = "Max Length";
		String longName = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxa";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankLongName_Invalid_ErrorMessage_TooLong);
	}

	@Test
	public void TC127_MailTracker_AddBank_BankNumber_Letters_NotAllowed() throws Exception {
		String bankID = "abcdefghijklmnopqrstuvwxyz";
		String shortName = "shortName";
		String longName = "longName";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}

	@Test
	public void TC127_MailTracker_AddBank_BankNumber_LettersAndNumbers_NotAllowed() throws Exception {
		String bankID = "abcd123efg456";
		String shortName = "shortName";
		String longName = "longName";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}

	@Test
	public void TC127_MailTracker_AddBank_BankNumber_SpecialChars_NotAllowed() throws Exception {
		String[] bankID = { "~70005", "!70005", "%70005", "^70005", "&70005", "-70005" };
		String shortName = "shortName";
		String longName = "longName";

		for (String bank_ID : bankID) {
			Banks_API.verify_BankField_IsInvalid(bank_ID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
		}
	}

	@Test
	public void TC128_MailTracker_AbleToAddBank_ShortName_AlphaNumeric() throws Exception {
		String bankID = "70006";
		String shortName = "abcdefghijklmnopqrstuvwxyz1234567890";
		String longName = "Alpha Numeric";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}

	@Test
	public void TC129_MailTracker_AbleToAddBank_ShortName_Symbols_Invalid_CausesError() throws Exception {
		String bankID = "70007";
		String[] shortName = { "~!@#$%^&*()_+{}|:\"<>?`-=[]/;,.", "~!@#$%^&*()_+{}|:\\<>?`-=[]/;,." };
		String longName = "Symbols";

		Banks_API.delete_Bank_IfItExists(bankID);

		for (String s : shortName) {
			Banks_API.verify_BankField_IsInvalid(bankID, s, longName, Banks_API.bankID_Blank_ErrorMessage);
		}

	}

	@Test
	public void TC129_MailTracker_AbleToAddBank_ShortName_Symbols() throws Exception {
		String bankID = "70008";
		String shortName = "~!@#$%^&*()_+{}|:<>?`-=[]/;,.";
		String longName = "Symbols";
		// String output = Banks.delete_Bank(bankID);
		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}

	@Test
	public void TC130_MailTracker_AbleToAddBank_LongName_AlphaNumeric() throws Exception {
		String bankID = "70009";
		String shortName = "Alpha Numeric";
		String longName = "abcdefghijklmnopqrstuvwxyz1234567890";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}

	@Test
	public void TC131_MailTracker_AbleToAddBank_LongName_Symbols_Invalid_CausesError() throws Exception {
		String bankID = "70010";
		String shortName = "Symbols";
		String[] longName = { "~!@#$%^&*()_+{}|:\"<>?`-=[]/;,.", "~!@#$%^&*()_+{}|:\\<>?`-=[]/;,." };

		Banks_API.delete_Bank_IfItExists(bankID);

		for (String s : longName) {
			Banks_API.verify_BankField_IsInvalid(bankID, shortName, s, Banks_API.bankID_Blank_ErrorMessage);
		}

	}
	
	@Test
	public void TC131_MailTracker_AbleToAddBank_LongName_Symbols() throws Exception {
		String bankID = "70011";
		String shortName = "Symbols";
		String longName = "~!@#$%^&*()_+{}|:<>?`-=[]/;,.";
		// String output = Banks.delete_Bank(bankID);
		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC143_MailTracker_AddBank_BankNumber_Spaces_Invalid() throws Exception {
		String bankID = " ";
		String shortName = "Spaces ONLY";
		String longName = "Bank ID Spaces ONLY";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}
	
	@Test
	public void TC143_MailTracker_AddBank_BankNumber_Spaces_BetweenNumbers_Invalid() throws Exception {
		String bankID = "7000 5";
		String shortName = "In between Spaces";
		String longName = "Bank ID In between Spaces";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, Banks_API.bankIDNotValid_ErrorMessage);
	}
	
	@Test
	public void TC143_MailTracker_AddBank_BankNumber_Spaces_BeforeAndAfterNumber() throws Exception {
		String bankID = " 70012 ";
		String shortName = "Spaces";
		String longName = "Bank ID Spaces Before And After";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC144_MailTracker_AddBank_ShortName_Spaces_BeforeAndAfterNumber() throws Exception {
		String bankID = "70013";
		String shortName = "W FS";
		String longName = "Short Name with Spaces";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC145_MailTracker_AddBank_LongName_Spaces_BeforeAndAfterNumber() throws Exception {
		String bankID = "70014";
		String shortName = "Long Name with Spaces";
		String longName = "Wausau Financial Systems";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC150_MailTracker_AddBank_NoShortName_Invalid() throws Exception {
		String bankID = "70015";
		String shortName = "";
		String longName = "No Short Name";

		Banks_API.verify_BankField_IsInvalid(bankID, shortName, longName, GlobalVariables.ShortName_Empty_ErrorMessage);
	}
	
	@Test
	public void TC151_MailTracker_AddBank_BankName_StartsWith0_GetsTrimmed() throws Exception {
		String bankID = "070016";
		String shortName = "Add Bank";
		String longName = "Leading 0";

		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC155_MailTracker_AddBank_BankName_StartsWith0_NewBank_AlreadyExists() throws Exception {
		String bankID = "070017";
		String shortName = "Bank Exists";
		String longName = "Leading 0";

		Banks_API.delete_Bank_IfItExists(bankID);
		Banks_API.post_Bank(bankID, shortName + "old", longName + "old");
		
		Banks_API.verify_CannotAdd_Bank_AlreadyExists(bankID, shortName, longName);
	}
	
	@Test
	public void TC155_MailTracker_AddBank_BankName_StartsWith0_AlreadyExists() throws Exception {
		String bankID = "01";
		String shortName = "Bank Exists";
		String longName = "Leading 0";

		
		Banks_API.verify_CannotAdd_Bank_AlreadyExists(bankID, shortName, longName);
	}
	
	@Test
	public void TC156_MailTracker_AddBank_MaxLengths() throws Exception {
		String bankID = "1134567890";
		String shortName = "ShortNameMaxLengthShortNameMaxLengthShortNameMaxLe";
		String longName = "LongNameMaxLengthLongNameMaxLengthLongNameMaxLengthLongNameMaxLengthLongNameMaxLengthLongNameMaxLeng";

		
		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC156_MailTracker_AddBank_MaxLengths_BigNumber() throws Exception {
		String bankID = "999999997";
		String shortName = "ShortNameMaxLengthShortNameMaxLengthShortNameMaxLe";
		String longName = "LongNameMaxLengthLongNameMaxLengthLongNameMaxLengthLongNameMaxLengthLongNameMaxLengthLongNameMaxLeng";

		
		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC157_MailTracker_AddBank_BankName_PreviouslyExisted() throws Exception {
		String bankID = "070018";
		String shortName = "Bank Exists";
		String longName = "Leading 0";

		Banks_API.post_Bank(bankID, shortName + "old", longName + "old");
		Banks_API.verify_Bank_SoftDeleted(bankID);
		Banks_API.verify_Able_ToAddBank(bankID, shortName, longName);
	}
	
	@Test
	public void TC158_MailTracker_AddBank_BankName_PreviouslyExisted_NowWithLeading0() throws Exception {
		String old_bankID = "70019";
		String new_bankID = "070019";
		
		String shortName = "Bank Exists";
		String longName = "Leading 0";

		Banks_API.post_Bank(old_bankID, shortName + "old", longName + "old");
		Banks_API.verify_Bank_SoftDeleted(old_bankID);
		Banks_API.verify_Able_ToAddBank(new_bankID, shortName, longName);
	}
}
