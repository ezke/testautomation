package mailTracker.tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import mailTracker.rest.Banks_API;
import mailTracker.rest.Contacts_API;
import mailTracker.rest.Customers_API;
import mailTracker.rest.DeliveryTypes_API;
import mailTracker.rest.Lockboxes_API;

public class DeliveryTypeAPI_Tests {


	@Test
	public void TC815_DeliveryType_Add5_DeliverableItems() throws Exception {
		HashMap<String, String> contactInfo = new HashMap<String, String>();
		contactInfo.put("contactName", "Darren Sproles");
		contactInfo.put("lockboxNumber", "70064");
		contactInfo.put("customerNumber", "70064");
		contactInfo.put("bankNumber", "70064");

		
		Banks_API.post_Bank(contactInfo.get("bankNumber"), "DeliveryType Bank", "DeliveryType Bank");
		Customers_API.post_Customer(contactInfo.get("customerNumber"), contactInfo.get("bankNumber"), "DeliveryType Customer", "DeliveryType Customer");
		Lockboxes_API.post_Lockbox(contactInfo.get("lockboxNumber"), contactInfo.get("customerNumber"), contactInfo.get("bankNumber"), 
				"DeliveryType Lockbox", "DeliveryType Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(contactInfo);
		
		//Info needed for Delivery Type
		String contactId = Contacts_API.return_Contact_ID(contactInfo.get("contactName"), contactInfo.get("lockboxNumber"), contactInfo.get("customerNumber"), contactInfo.get("bankNumber"));
		String deliveryMethodId = "195";
		String address = "New Delivery Type";
		String deliveryTypeCriteriaId = "70064";
		String numberOfDays = "0";
		String numberOfLabels = "2";
		String[] deliverableItems = {"178", "179", "180", "181", "182"} ;
		
		DeliveryTypes_API.post_DeliveryTypes(contactId, deliveryMethodId, address, deliveryTypeCriteriaId, numberOfDays, numberOfLabels, deliverableItems);
		
	}
	
}
