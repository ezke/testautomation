package mailTracker.tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import mailTracker.pages.GlobalVariables;
import mailTracker.rest.Banks_API;
import mailTracker.rest.Contacts_API;
import mailTracker.rest.Customers_API;
import mailTracker.rest.Lockboxes_API;
import utils.BaseUI;
import utils.TableData;

public class ContactAPI_Tests {


	@Test
	public void TC698_Contact_AddContacts_AndDiffBankSameCustomer_DiffBankDiffCustomer() throws Exception {
		String lockboxId = "70050";
		String customerId = "70050";
		String bankId = "70050";
		String name = "Jared Abbrederis";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223-456-7890";
		String fax = "223-456-7890";
		
		String bankId2 = "70051";
		String customerId2 = "700050";
		String lockboxId2 = "70050";
		
		String bankId3 = "70052";
		String customerId3 = "700052";
		String lockboxId3 = "70050";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Banks_API.post_Bank(bankId2, "Second Bank", "Second Bank");
		Customers_API.post_Customer(customerId2, bankId2, "Same Customer as First Bank", "Same Customer as First Bank");
		Lockboxes_API.post_Lockbox(lockboxId2, customerId2, bankId2, "Same Lockbox as First Lockbox", "Same Lockbox as First Lockbox", "", "false");
		
		Banks_API.post_Bank(bankId3, "Third Bank", "Third Bank");
		Customers_API.post_Customer(customerId3, bankId3, "3rd Customer", "3rd Customer");
		Lockboxes_API.post_Lockbox(lockboxId3, customerId3, bankId3, "Same Lockbox as First Lockbox", "Same Lockbox as First Lockbox", "", "false");
		
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
		Contacts_API.verify_Able_ToAddContact(name, lockboxId2, customerId2, bankId2, address1, email, phone, fax);
		Contacts_API.verify_Able_ToAddContact(name, lockboxId3, customerId3, bankId3, address1, email, phone, fax);
	}
	
	
	@Test
	public void TC700_Contact_DeleteContact() throws Exception {
		String lockboxId = "70053";
		String customerId = "70053";
		String bankId = "70053";
		String name = "Jared Abbrederis";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223-456-7890";
		String fax = "223-456-7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
		Contacts_API.verify_Able_ToDelete_Contact(name, lockboxId, customerId, bankId);
	}
	
	@Test
	public void TC701_Contact_DeleteContact_InvalidID() throws Exception {
		String contactID = "70003";

		Contacts_API.delete_Contact(contactID);
		Contacts_API.verify_Delete_Contact_IsInvalid(contactID, Contacts_API.contact_DoesNOTExist_ErrorMessage(contactID));
	}
	
	@Test
	public void TC702_Contact_DeleteContact_PreviouslyDeleted() throws Exception {
		String lockboxId = "70054";
		String customerId = "70054";
		String bankId = "70054";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223-456-7890";
		String fax = "223-456-7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
		
		String contactID = Contacts_API.return_Contact_ID(name, lockboxId, customerId, bankId);
		Contacts_API.delete_Contact(contactID);
		Contacts_API.verify_Delete_Contact_IsInvalid(contactID, Contacts_API.contact_DoesNOTExist_ErrorMessage(contactID));	
	}
	
	
	@Test
	public void TC705_Contact_AddContact_WithoutAddress1() throws Exception {
		HashMap<String, String> contactHashMap = new HashMap<String, String>();
		contactHashMap.put("contactName", "Darren Sproles");
		contactHashMap.put("lockboxNumber", "70055");
		contactHashMap.put("customerNumber", "70055");
		contactHashMap.put("bankNumber", "70055");
//		contactHashMap.put("address1", "123 Sesame St");
		contactHashMap.put("email", "abrakadabra@email.com");
		contactHashMap.put("phone", "223-456-7890");
		contactHashMap.put("fax", "223-456-7890");
	
		String bankResponse = Banks_API.post_Bank(contactHashMap.get("bankNumber"), "First Bank", "First Bank");
		String customerResponse = Customers_API.post_Customer(contactHashMap.get("customerNumber"), contactHashMap.get("bankNumber"), "First Customer", "First Customer");
		String lockboxesResponse = Lockboxes_API.post_Lockbox(contactHashMap.get("lockboxNumber"), contactHashMap.get("customerNumber"), 
				contactHashMap.get("bankNumber"), "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(contactHashMap);
	}
	
	@Test
	public void TC705_Contact_AddContact_WithoutEmail() throws Exception {
		HashMap<String, String> contactHashMap = new HashMap<String, String>();
		contactHashMap.put("contactName", "Darren Sproles");
		contactHashMap.put("lockboxNumber", "70055");
		contactHashMap.put("customerNumber", "70055");
		contactHashMap.put("bankNumber", "70055");
		contactHashMap.put("address1", "123 Sesame St");
//		contactHashMap.put("email", "abrakadabra@email.com");
		contactHashMap.put("phone", "223-456-7890");
		contactHashMap.put("fax", "223-456-7890");
	
		String bankResponse = Banks_API.post_Bank(contactHashMap.get("bankNumber"), "First Bank", "First Bank");
		String customerResponse = Customers_API.post_Customer(contactHashMap.get("customerNumber"), contactHashMap.get("bankNumber"), "First Customer", "First Customer");
		String lockboxesResponse = Lockboxes_API.post_Lockbox(contactHashMap.get("lockboxNumber"), contactHashMap.get("customerNumber"), 
				contactHashMap.get("bankNumber"), "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(contactHashMap);
	}
	
	@Test
	public void TC705_Contact_AddContact_WithoutPhone() throws Exception {
		HashMap<String, String> contactHashMap = new HashMap<String, String>();
		contactHashMap.put("contactName", "Darren Sproles");
		contactHashMap.put("lockboxNumber", "70055");
		contactHashMap.put("customerNumber", "70055");
		contactHashMap.put("bankNumber", "70055");
		contactHashMap.put("address1", "123 Sesame St");
		contactHashMap.put("email", "abrakadabra@email.com");
//		contactHashMap.put("phone", "223-456-7890");
		contactHashMap.put("fax", "223-456-7890");
	
		String bankResponse = Banks_API.post_Bank(contactHashMap.get("bankNumber"), "First Bank", "First Bank");
		String customerResponse = Customers_API.post_Customer(contactHashMap.get("customerNumber"), contactHashMap.get("bankNumber"), "First Customer", "First Customer");
		String lockboxesResponse = Lockboxes_API.post_Lockbox(contactHashMap.get("lockboxNumber"), contactHashMap.get("customerNumber"), 
				contactHashMap.get("bankNumber"), "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(contactHashMap);
	}
	
	@Test
	public void TC705_Contact_AddContact_WithoutFax() throws Exception {
		HashMap<String, String> contactHashMap = new HashMap<String, String>();
		contactHashMap.put("contactName", "Darren Sproles");
		contactHashMap.put("lockboxNumber", "70055");
		contactHashMap.put("customerNumber", "70055");
		contactHashMap.put("bankNumber", "70055");
		contactHashMap.put("address1", "123 Sesame St");
		contactHashMap.put("email", "abrakadabra@email.com");
		contactHashMap.put("phone", "223-456-7890");
//		contactHashMap.put("fax", "223-456-7890");
	
		String bankResponse = Banks_API.post_Bank(contactHashMap.get("bankNumber"), "First Bank", "First Bank");
		String customerResponse = Customers_API.post_Customer(contactHashMap.get("customerNumber"), contactHashMap.get("bankNumber"), "First Customer", "First Customer");
		String lockboxesResponse = Lockboxes_API.post_Lockbox(contactHashMap.get("lockboxNumber"), contactHashMap.get("customerNumber"), 
				contactHashMap.get("bankNumber"), "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(contactHashMap);
	}
	
	@Test
	public void TC711_Contact_AddContact_AlreadyExists_WithLeadingSpaces() throws Exception {
		String lockboxId = "70056";
		String customerId = "70056";
		String bankId = "70056";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223-456-7890";
		String fax = "223-456-7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
		String contactId = Contacts_API.return_Contact_ID(name, lockboxId, customerId, bankId);
		
		Contacts_API.verify_Contact_Post_IsInvalid(" " + name + " ", " " + lockboxId + " ", " " + customerId + " ", " " + bankId + " ", 
				" " + address1 + " ", " " + email + " ", " " + phone + " ", " " + fax + " ", Contacts_API.contact_AlreadyExists_ErrorMessage(name, lockboxId, customerId, bankId));
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_AlphabetCharacters() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "abc-def-hijk";
		String fax = "lmn-opq-rstv";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_NoHyphens() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "1234567890";
		String fax = "1234567890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_AlphaNumeric() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "123-abc-7890";
		String fax = "abc-4567890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_InvalidContactInfo() throws Exception {
		String lockboxId = "###";
		String customerId = "###";
		String bankId = "###";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "\"";
		String fax = "\"";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		//Contacts.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_InvalidContactObject_Null_ErrorMessage);
	}
	
	
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_SpecialChars() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "~!@-#$%-^&*(";
		String fax = ")_+-{}[-]!@#";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_Periods() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223.456.7890";
		String fax = "223.456.7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_Parentheses_AroundAreaCode() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "(223)456-7890";
		String fax = "(223)456-7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	

	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_Parentheses_AroundAreaCode_AndSpace() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "(223)456- 7890";
		String fax = "(223)456- 7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_Parentheses_AroundAreaCode_DashesAndSpaces() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223 - 456 - 7890";
		String fax = "223 - 456 - 7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Valid() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "555-555-5555";
		String fax = "555-555-5555";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_Leading1() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "155-555-5555";
		String fax = "155-555-5555";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC628_Contact_AddContact_PhoneAndFax_Invalid_Leading0() throws Exception {
		String lockboxId = "70057";
		String customerId = "70057";
		String bankId = "70057";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "055-555-5555";
		String fax = "055-555-5555";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage);
	}
	
	@Test
	public void TC633_Contact_AddContact_AlreadyExists() throws Exception {
		String lockboxId = "70058";
		String customerId = "70058";
		String bankId = "70058";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "255-555-5555";
		String fax = "255-555-5555";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.delete_Contact_IfExists(name, lockboxId, customerId, bankId);
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address1, email, phone, fax, Contacts_API.contact_AlreadyExists_ErrorMessage(name, lockboxId, customerId, bankId));
	}
	
	@Test
	public void TC643_Contact_GetContact_PreviouslyDeleted() throws Exception {
		String lockboxId = "70059";
		String customerId = "70059";
		String bankId = "70059";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "255-555-5555";
		String fax = "255-555-5555";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address1, email, phone, fax);
		String contactId = Contacts_API.return_Contact_ID(name, lockboxId, customerId, bankId);
		Contacts_API.delete_Contact(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Get_Contact_IsInvalid(contactId, Contacts_API.contact_DoesNOTExist_ErrorMessage(contactId));
	}
	
	//641 Do Delivery Types and then come back to this.
	@Test
	public void TC641_Contact_GetAllContacts() throws Exception {
		String lockboxId = "70060";
		String customerId = "70060";
		String bankId = "70060";
		String name = "Daren Sproles";
		String address1 = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "255-555-5555";
		String fax = "255-555-5555";
	
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address1, email, phone, fax);

		Contacts_API.post_Contact(name +"1", lockboxId, customerId, bankId, address1, email, phone, fax);
		
		TableData apiResults = Contacts_API.get_ALL_Contacts(lockboxId, customerId, bankId);
		TableData databaseResults = Contacts_API.return_AllContacts_FromDatabase_ForData(lockboxId, customerId, bankId);
		
		BaseUI.verify_true_AndLog(apiResults.data.size() > 0, "Found API Data.", "Did NOT find API Data.");

		BaseUI.verify_true_AndLog(apiResults.data.size() == databaseResults.data.size(),
				"API data matched Database data in size.", "API data did NOT match Database data in size.");

		//Need to finish Delivery Type code before I finish this.
		for (Integer i = 0; i < apiResults.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), apiResults.data.get(i), databaseResults.data.get(i));
		}
	}
	
	@Test
	public void TC644_Contact_GetContact_DoesNotExist() throws Exception {
		String contactId = "70777";
		Contacts_API.verify_Get_Contact_IsInvalid(contactId, Contacts_API.contact_DoesNOTExist_ErrorMessage(contactId));
	}
	
	@Test
	public void TC597_Contact_AddContact_NoValues() throws Exception {
		String lockboxId = "";
		String customerId = "";
		String bankId = "";
		String name = "";
		String address = "";
		String email = "";
		String phone = "";
		String fax = "";
		
//		Banks.post_Bank(bankId, "First Bank", "First Bank");
//		Customers.post_Customer(customerId, bankId, "First Customer", "First Customer");
//		Lockboxes.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		String errorMessage = Lockboxes_API.lockboxID_Required_ErrorMessage + Contacts_API.name_is_required_ErrorMessage + Lockboxes_API.lockbox_doesNOT_Exist_ErrorMessage("0", "0", "0");
		
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address, email, phone, fax, errorMessage);
	}
	

	@Test
	public void TC605_Contact_AddContact_PreviouslyDeleted() throws Exception {
		String lockboxId = "70061";
		String customerId = "70061";
		String bankId = "70061";
		String name = "Daren Sproles";
		String address = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223-456-7890";
		String fax = "223-456-7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address, email, phone, fax);
		
		Contacts_API.verify_Able_ToDelete_Contact(name, lockboxId, customerId, bankId);
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	

	@Test
	public void TC606_Contact_GetContact_ById() throws Exception {
		String lockboxId = "70061";
		String customerId = "70061";
		String bankId = "70061";
		String name = "Daren Sproles";
		String address = "0000 Green Bay Ln";
		String email = "abrakadabra@email.com";
		String phone = "223-456-7890";
		String fax = "223-456-7890";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		Contacts_API.post_Contact(name, lockboxId, customerId, bankId, address, email, phone, fax);
		Contacts_API.verify_GET_Contact_Exists(name, lockboxId, customerId, bankId);
	}

	
	@Test
	public void TC608_Contact_AddContact_UnrequiredFields() throws Exception {
		String lockboxId = "70062";
		String customerId = "70062";
		String bankId = "70062";
		String name = "Daren Sproles";
		String address = "";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	@Test
	public void TC609_Contact_AddContact_OverMaxLength_Invalid() throws Exception {
		String lockboxId = "70062";
		String customerId = "70062";
		String bankId = "70062";
		String name = "NameNameNameNameNameNameNameNameNameNameN";
		//Address goes a lot higher than 501, but according to Jonathan we're only testing to 500.
		String address = "ThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLong1";
		String email = "EmailEmailEmailEmailEmailEmailEmailEmailEmailEmailEmailEmailE";
		String phone = "223456789012345678901";
		String fax = "223456789012345678901";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		String errorMessage = GlobalVariables.Name_LengthInvalid_ErrorMessage + Contacts_API.contact_Phone_Invalid_ErrorMessage + Contacts_API.contact_fax_Invalid_ErrorMessage 
				+ Contacts_API.email_InvalidLength_ErrorMessage;
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address, email, phone, fax, errorMessage);
	}
	
	@Test
	public void TC610_Contact_AddContact_MaxLength() throws Exception {
		String lockboxId = "70062";
		String customerId = "70062";
		String bankId = "70062";
		String name = "NameNameNameNameNameNameNameNameNameName";
		String address = "ThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLongThisTexthastobereallyLon";
		String email = "EmailEmailEmailEmailEmailEmailEmailEmailEmailEmailEmailEmail";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	@Test
	public void TC611_Contact_AddContact_NameChars_Alphabetical() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "abcdefghijklmnopqrustuvwxyz";
		String address = "Alpha";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	@Test
	public void TC611_Contact_AddContact_NameChars_Numeric() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "123467890";
		String address = "Numeric";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	@Test
	public void TC611_Contact_AddContact_NameChars_AlphaNumeric() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "123abc456efg";
		String address = "AlphaNumeric";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	@Test
	public void TC611_Contact_AddContact_NameChars_Invalid() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "\"";
		String address = "Invalid";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		
		String errorMessage = Contacts_API.contact_InvalidContactObject_Null_ErrorMessage;
		Contacts_API.verify_Contact_Post_IsInvalid(name, lockboxId, customerId, bankId, address, email, phone, fax, errorMessage);
	}
	
	@Test
	public void TC611_Contact_AddContact_NameChars_SpecialChars() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "~!@#$%^&*()_+{}|:<>?`-=[];.//";
		String address = "SpecialChars";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	

	@Test
	public void TC611_Contact_AddContact_NameChars_MoreSpecialChars() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "?<+*&%#.>/";
		String address = "SpecialChars";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	@Test
	public void TC611_Contact_AddContact_NameChars_SpecialChars_And_Letters() throws Exception {
		String lockboxId = "70063";
		String customerId = "70063";
		String bankId = "70063";
		String name = "qweq()*&%$#@!";
		String address = "SpecialChars";
		String email = "";
		String phone = "";
		String fax = "";
		
		Banks_API.post_Bank(bankId, "First Bank", "First Bank");
		Customers_API.post_Customer(customerId, bankId, "First Customer", "First Customer");
		Lockboxes_API.post_Lockbox(lockboxId, customerId, bankId, "First Lockbox", "First Lockbox", "", "false");
		Contacts_API.verify_Able_ToAddContact(name, lockboxId, customerId, bankId, address, email, phone, fax);
	}
	
	
	
}//End of Class
