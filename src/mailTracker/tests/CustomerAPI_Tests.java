package mailTracker.tests;

import java.text.MessageFormat;
import org.testng.annotations.Test;
import mailTracker.pages.GlobalVariables;
import mailTracker.rest.Banks_API;
import mailTracker.rest.Customers_API;
import mailTracker.rest.Lockboxes_API;
import utils.BaseUI;
import utils.TableData;

public class CustomerAPI_Tests {

	@Test
	public void TC199_MailTracker_AddNewCustomer_NoValues() throws Exception {
		String customerNumber = "";
		String bankID = "";
		String shortName = "";
		String longName = "";

		String errorMessage = Customers_API.customerID_NotValid_ErrorMessage + Banks_API.bankIDNotValid_ErrorMessage
				+ GlobalVariables.ShortName_Empty_ErrorMessage + GlobalVariables.LongName_Empty_ErrorMessage
				+ MessageFormat.format(Banks_API.bank_DoesNotExist_ErrorMessage, "0");

		Customers_API.verify_CustomerField_IsInvalid_AndCustomerDoesNOTExist(customerNumber, bankID, shortName, longName,
				errorMessage);

	}

	@Test
	public void TC204_MailTracker_AddNewCustomer() throws Exception {
		String customerNumber = "70000";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "DOE";

		Customers_API.verify_Able_ToAddCustomer(customerNumber, bankID, shortName, longName);

	}

	@Test
	public void TC208_MailTracker_AddNewCustomer_WithSpaces_InCustomerName() throws Exception {
		String customerNumber = " 70001 ";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "Lee Doe";

		Customers_API.verify_Able_ToAddCustomer(customerNumber, bankID, shortName, longName);
	}

	@Test
	public void TC209_MailTracker_AddNewCustomer_BankDoesNOT_Exist() throws Exception {
		String customerNumber = "70002";
		String bankID = "70021";
		String shortName = "Jon";
		String longName = "Lee Doe";

		Banks_API.delete_Bank_IfItExists(bankID);

		Customers_API.verify_CustomerField_IsInvalid_AndCustomerDoesNOTExist(customerNumber, bankID, shortName, longName,
				MessageFormat.format(Banks_API.bank_DoesNotExist_ErrorMessage, bankID));
	}

	@Test
	public void TC210_MailTracker_AddNewCustomer_BankDoesNOT_Exist_ButPreviouslyDid() throws Exception {
		String customerNumber = "70003";
		String bankID = "70022";
		String shortName = "Jon";
		String longName = "Lee Doe";

		Banks_API.post_Bank(bankID, "priorBank", "priorBank_LongName");

		Banks_API.delete_Bank_IfItExists(bankID);

		Customers_API.verify_CustomerField_IsInvalid_AndCustomerDoesNOTExist(customerNumber, bankID, shortName, longName,
				MessageFormat.format(Banks_API.bank_DoesNotExist_ErrorMessage, bankID));
	}

	@Test
	public void TC211_MailTracker_AddNewCustomer_CustomerAlreadyExists() throws Exception {
		String customerNumber = "70004";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "Shmon";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);

		Customers_API.verify_CustomerField_IsInvalid(customerNumber, bankID, shortName, longName,
				MessageFormat.format(Customers_API.customer_AlreadyExists_ErrorMessage, customerNumber, bankID));
	}

	@Test
	public void TC214_MailTracker_AddNewCustomer_MaxLength() throws Exception {
		String customerNumber = "1134567899";
		String bankID = "1134567899";
		String shortName = "MaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLe";
		String longName = "MaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthM";

		Banks_API.post_Bank(bankID, "MaxLength", "Max Length");

		Customers_API.verify_Able_ToAddCustomer(customerNumber, bankID, shortName, longName);
	}

	@Test
	public void TC216_MailTracker_AddNewCustomer_CustomerPreviouslyDeleted() throws Exception {
		String customerNumber = "70005";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "Sh mon";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);
		Customers_API.delete_Customer(customerNumber, bankID);

		Customers_API.verify_Able_ToAddCustomer(customerNumber, bankID, shortName, longName);
	}

	@Test
	public void TC223_MailTracker_DeleteCustomer() throws Exception {
		String customerNumber = "70006";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "Sh mon";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);
		Customers_API.verify_Customer_SoftDeleted(customerNumber, bankID);
	}

	@Test
	public void TC225_MailTracker_DeleteCustomer_AlreadyDeleted() throws Exception {
		String customerNumber = "70007";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "Sh mon";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);
		Customers_API.delete_Customer(customerNumber, bankID);
		Customers_API.verify_Delete_Customer_IsInvalid_(customerNumber, bankID, shortName, longName,
				Customers_API.customer_DoesNOT_Exist(customerNumber, bankID));
	}

	@Test
	public void TC233_MailTracker_GetCustomer() throws Exception {
		String customerNumber = "70008";
		String bankID = "70020";
		String shortName = "Jon";
		String longName = "The Mon";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);
		Customers_API.verify_GetCustomer(customerNumber, bankID, shortName, longName);
	}

	@Test
	public void TC235_MailTracker_GetALL_Customer() throws Exception {

		TableData apiData = Customers_API.return_ALL_Customers_from_APICall();
		TableData databaseData = Customers_API.return_ALL_Customers_fromDatabase();

		BaseUI.verify_true_AndLog(apiData.data.size() > 0, "Found API Data.", "Did NOT find API Data.");

		BaseUI.verify_true_AndLog(apiData.data.size() == databaseData.data.size(),
				"API data matched Database data in size.", "API data did NOT match Database data in size.");

		for (Integer i = 0; i < apiData.data.size(); i++) {
			BaseUI.verify_TableRow_Matches(i.toString(), apiData.data.get(i), databaseData.data.get(i));
		}

	}

	@Test
	public void TC236_MailTracker_UpdateCustomer_NoValues() throws Exception {
		String customerNumber = "";
		String bankID = "";
		String shortName = "";
		String longName = "";

		String errorMessage = Customers_API.customerID_NotValid_ErrorMessage + Banks_API.bankIDNotValid_ErrorMessage
				+ GlobalVariables.ShortName_Empty_ErrorMessage + GlobalVariables.LongName_Empty_ErrorMessage
				+ MessageFormat.format(Banks_API.bank_DoesNotExist_ErrorMessage, "0");

		Customers_API.verify_Update_CustomerField_IsInvalid_(customerNumber, bankID, shortName, longName, errorMessage);

	}

	@Test
	public void TC243_MailTracker_UpdateCustomer() throws Exception {
		String customerNumber = "70009";
		String bankID = "70020";
		String shortName_old = "Old ShortName";
		String longName_old = "Old LongName";

		String shortName_new = "New ShortName";
		String longName_new = "New LongName";

		if (Customers_API.customerExists_ByCustomerNumber_AndBankNumber(customerNumber, bankID)) {
			Customers_API.delete_Customer(customerNumber, bankID);
		}

		Customers_API.post_Customer(customerNumber, bankID, shortName_old, longName_old);

		Customers_API.verify_Able_ToUpdateCustomer(customerNumber, bankID, shortName_new, longName_new);

	}

	@Test
	public void TC245_MailTracker_UpdateCustomer_CustomerWasAlreadyDeleted() throws Exception {
		String customerNumber = "70010";
		String bankID = "70020";
		String shortName_old = "Old ShortName";
		String longName_old = "Old LongName";

		String shortName_new = "New ShortName";
		String longName_new = "New LongName";

		Customers_API.post_Customer(customerNumber, bankID, shortName_old, longName_old);
		Customers_API.delete_Customer(customerNumber, bankID);

		Customers_API.verify_Update_CustomerField_IsInvalid_(customerNumber, bankID, shortName_new, longName_new,
				MessageFormat.format(Customers_API.customer_PreviouslyDeleted, customerNumber, bankID));

	}

	@Test
	public void TC323_MailTracker_DeleteCustomer_WhoIsAssignedToLockbox() throws Exception {
		String customerNumber = "70011";
		String bankID = "70020";
		String shortName = "Assigned To Lockbox";
		String longName = "Assigned To Lockbox";

		// Customers.post_Customer(customerNumber, bankID, shortName, longName);
		// Lockboxes.post_Lockbox("70000", "Automation Lockbox", "Automation
		// Lockbox", bankID, customerNumber, "", "False");

		Customers_API.verify_Delete_Customer_IsInvalid_(customerNumber, bankID, shortName, longName,
				Customers_API.customer_CannotBeDeleted_HasLockbox(customerNumber, bankID));
	}

	@Test
	public void TC325_MailTracker_DeleteCustomer_WhoIsAssignedToLockbox_DeleteLockbox_AndThenDeleteCustomer()
			throws Exception {
		String customerNumber = "70012";
		String bankID = "70020";
		String shortName = "Assigned To Lockbox";
		String longName = "Assigned To Lockbox";

		String lockboxNumber = "70001";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);
		Lockboxes_API.post_Lockbox(lockboxNumber, bankID, customerNumber, "Automation Lockbox1", "Automation Lockbox1", "",
				"False");

		Customers_API.verify_Delete_Customer_IsInvalid_(customerNumber, bankID, shortName, longName,
				Customers_API.customer_CannotBeDeleted_HasLockbox(customerNumber, bankID));

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerNumber, bankID);

		Customers_API.verify_Able_ToDelete_Customer(customerNumber, bankID, shortName, longName);

	}

	@Test
	public void TC350_MailTracker_DeleteCustomer_WhoIsAssignedTo_MultipleLockboxes() throws Exception {
		String customerNumber = "70013";
		String bankID = "70020";
		String shortName = "Assigned To Lockbox";
		String longName = "Assigned To Lockbox";

		String lockboxNumber1 = "70002";
		String lockboxNumber2 = "70003";
		String lockboxNumber3 = "70004";

		Customers_API.post_Customer(customerNumber, bankID, shortName, longName);
		Lockboxes_API.post_Lockbox(lockboxNumber1, customerNumber, bankID, "Automation Lockbox1", "Automation Lockbox1", "",
				"False");
		Lockboxes_API.post_Lockbox(lockboxNumber2, customerNumber, bankID, "Automation Lockbox2", "Automation Lockbox2", "",
				"False");
		Lockboxes_API.post_Lockbox(lockboxNumber3, customerNumber, bankID, "Automation Lockbox3", "Automation Lockbox3", "",
				"False");

		Customers_API.verify_Delete_Customer_IsInvalid_(customerNumber, bankID, shortName, longName,
				Customers_API.customer_CannotBeDeleted_HasLockbox(customerNumber, bankID));

		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber1, customerNumber, bankID);

		Customers_API.verify_Delete_Customer_IsInvalid_(customerNumber, bankID, shortName, longName,
				Customers_API.customer_CannotBeDeleted_HasLockbox(customerNumber, bankID));

		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber2, customerNumber, bankID);

		Customers_API.verify_Delete_Customer_IsInvalid_(customerNumber, bankID, shortName, longName,
				Customers_API.customer_CannotBeDeleted_HasLockbox(customerNumber, bankID));

		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber3, customerNumber, bankID);

		Customers_API.verify_Able_ToDelete_Customer(customerNumber, bankID, shortName, longName);

	}

	@Test
	public void TC589_MailTracker_GetCustomer_CustomerDoesntExist_BankDoesntExist() throws Exception {
		String customerNumber = "70014";
		String bankID = "70023";
		String shortName = "Does not exist.";
		String longName = "Does not exist.";

		Customers_API.verify_Get_Customer_IsInvalid_(customerNumber, bankID,
				Customers_API.customer_DoesNOT_Exist(customerNumber, bankID));
	}

	@Test
	public void TC590_MailTracker_AddSameCustomer_ToMultipleBanks() throws Exception {
		String customerNumber = "70015";

		String[] bankIDs = { "70024", "70025", "70026" };

		for (String bank : bankIDs) {
			Customers_API.delete_Customer_IfItExists(customerNumber, bank);
		}

		for (String bank : bankIDs) {
			Customers_API.verify_Able_ToAddCustomer(customerNumber, bank, "Multiple Banks", "Multiple Banks");
		}

	}

	@Test
	public void TC595_MailTracker_DeleteCustomer_SameCustomerID() throws Exception {
		String customerNumber = "70016";
		String shortName = "shortName";
		String longName = "longName";

		String[] bankIDs = { "70024", "70025", "70026" };

		for (String bank : bankIDs) {
			Customers_API.post_Customer(customerNumber, bank, shortName, longName);
		}

		Customers_API.verify_Able_ToDelete_Customer(customerNumber, bankIDs[0], shortName, longName);

		Customers_API.verify_Customer_Exists(customerNumber, bankIDs[1], shortName, longName);
		Customers_API.verify_Customer_Exists(customerNumber, bankIDs[2], shortName, longName);
	}

}// End of class
