package mailTracker.tests;

import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import mailTracker.pages.GlobalVariables;
import mailTracker.pages.Lockbox;
import mailTracker.pages.Navigation;
import mailTracker.rest.Banks_API;
import mailTracker.rest.Customers_API;
import mailTracker.rest.Lockboxes_API;
import utils.BaseUI;
import utils.Browser;
import utils.Locator;
import utils.ResultWriter;
import utils.TableData;

public class Lockbox_Website_Tests {

	String lockboxNumber = "70040";

	// first lockbox
	String shortName1 = "Lockbox1A";
	String longName1 = "Lockbox1B";
	String bankNumber1 = "70040";
	String customerNumber1 = "70040";
	String IsDisabled1 = "False";
	
	TableData lockboxes_Website;
	TableData lockboxes_Database;
	TableData lockboxes_API_Response;


	@BeforeClass
	public void setup_method() throws Exception {
		Banks_API.post_Bank(bankNumber1, "bank 1", "bank 1");
		Customers_API.post_Customer(customerNumber1, bankNumber1, shortName1, longName1);
		Lockboxes_API.delete_Lockbox(lockboxNumber, customerNumber1, bankNumber1);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber1, bankNumber1, shortName1, longName1, "",
				IsDisabled1);

		Locator.loadObjectRepository("\\src\\mailTracker\\mailTrackerRepo.txt");
		Browser.openBrowser(GlobalVariables.website_URL());
		Navigation.navigate_To_Lockboxes();

		BaseUI.selectValueFromDropDown(Locator.lookupElement("lockbox_Show_Dropdown"), "100");
	}

	@Test
	public void TC506_Lockbox_Appears_OnWebsite() throws Exception {

		lockboxes_Website = Lockbox.lockboxRows();

		HashMap<String, String> lockbox = lockboxes_Website.dataRow("Lockbox", lockboxNumber);
		
		BaseUI.baseStringCompare("Bank", bankNumber1, lockbox.get("Bank"));
		BaseUI.baseStringCompare("Customer", customerNumber1, lockbox.get("Customer"));
		BaseUI.baseStringCompare("Lockbox", lockboxNumber, lockbox.get("Lockbox"));
		BaseUI.baseStringCompare("Short Name", shortName1, lockbox.get("Short Name"));
		BaseUI.baseStringCompare("Long Name", longName1, lockbox.get("Long Name"));
	}
	
	@Test
	public void TC506_Lockbox_Appears_InDatabase() throws Exception {

		lockboxes_Database = Lockboxes_API.return_ALL_Lockboxes_fromDatabase();
		HashMap<String, String> lockbox = lockboxes_Database.dataRow("LockboxNumber", lockboxNumber);
		
		BaseUI.baseStringCompare("Bank", bankNumber1, lockbox.get("BankId"));
		BaseUI.baseStringCompare("Customer", customerNumber1, lockbox.get("CustomerId"));
		BaseUI.baseStringCompare("Lockbox", lockboxNumber, lockbox.get("LockboxNumber"));
		BaseUI.baseStringCompare("Short Name", shortName1, lockbox.get("ShortName"));
		BaseUI.baseStringCompare("Long Name", longName1, lockbox.get("LongName"));
	}
	
	@Test
	public void TC506_Lockbox_Appears_FromAPICall() throws Exception {

		lockboxes_API_Response = Lockboxes_API.return_ALL_Lockboxes_from_APICall();
		HashMap<String, String> lockbox = lockboxes_API_Response.dataRow("LockboxNumber", lockboxNumber);
		
		BaseUI.baseStringCompare("Bank", bankNumber1, lockbox.get("BankId"));
		BaseUI.baseStringCompare("Customer", customerNumber1, lockbox.get("CustomerId"));
		BaseUI.baseStringCompare("Lockbox", lockboxNumber, lockbox.get("LockboxNumber"));
		BaseUI.baseStringCompare("Short Name", shortName1, lockbox.get("ShortName"));
		BaseUI.baseStringCompare("Long Name", longName1, lockbox.get("LongName"));

	}
	
	
	
	@AfterMethod(alwaysRun = true)
	public void testCleanup(ITestResult result) throws Exception {
		ResultWriter.checkForFailureAndScreenshot(result);
	}


	@AfterClass(alwaysRun = true)
	public void writeResultsAndTearDown() throws Exception {
		
		Browser.closeBrowser();
	}

}// End of Class
