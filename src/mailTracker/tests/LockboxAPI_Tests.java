package mailTracker.tests;

import java.util.HashMap;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import mailTracker.pages.GlobalVariables;
import mailTracker.rest.Banks_API;
import mailTracker.rest.Customers_API;
import mailTracker.rest.Lockboxes_API;
import utils.BaseUI;
import utils.TableData;

public class LockboxAPI_Tests {

	@Test
	public void TC261_Lockbox_AddLockbox_AllFields_Empty() throws Exception {
		String lockboxNumber = "";
		String shortName = "";
		String longName = "";
		String bankID = "";
		String customerID = "";
		String mailoutInstructions = "";
		String isDisabled = "";

		String errorMessage = Lockboxes_API.lockboxID_Required_ErrorMessage + Banks_API.bankIDNotValid_ErrorMessage
				+ Customers_API.customerID_NotValid_ErrorMessage + GlobalVariables.ShortName_Empty_ErrorMessage
				+ GlobalVariables.LongName_Empty_ErrorMessage + Customers_API.customer_DoesNOT_Exist("0", "0");

		Lockboxes_API.verify_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled, errorMessage);
	}

	@Test
	public void TC267_Lockbox_AddLockbox_AllFields_ZeroValues() throws Exception {
		String lockboxNumber = "0";
		String shortName = "0";
		String longName = "0";
		String bankID = "0";
		String customerID = "0";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		String errorMessage = Lockboxes_API.lockboxID_Required_ErrorMessage + Banks_API.bankIDNotValid_ErrorMessage
				+ Customers_API.customerID_NotValid_ErrorMessage + Customers_API.customer_DoesNOT_Exist("0", "0");

		Lockboxes_API.verify_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled, errorMessage);
	}

	@Test
	public void TC268_Lockbox_AddLockbox() throws Exception {
		String lockboxNumber = "70005";
		String shortName = "Add Lockbox";
		String longName = "Add Lockbox";
		String bankID = "70020";
		String customerID = "70017";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Customers_API.post_Customer(customerID, bankID, "AddLockbox", "Add Lockbox");

		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);
	}

	// Not currently working
	@Test
	public void TC269_Lockbox_AddLockbox_OverMaxLength() throws Exception {
		String lockboxNumber = "72345678991";
		String shortName = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
		String longName = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
		String bankID = "72345678991";
		String customerID = "72345678991";
		String mailoutInstructions = "0";
		String isDisabled = "False";

		// String lockboxNumber = "12345678991";
		// String shortName =
		// "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
		// String longName =
		// "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
		// String bankID = "";
		// String customerID = "";
		// String mailoutInstructions = "0";
		// String isDisabled = "False";

		Customers_API.post_Customer(customerID, bankID, "AddLockbox", "Add Lockbox over Max Length");

		String errorMessage = Lockboxes_API.lockboxID_Required_ErrorMessage + Banks_API.bankIDNotValid_ErrorMessage
				+ Customers_API.customerID_NotValid_ErrorMessage + GlobalVariables.ShortName_LengthInvalid_ErrorMessage
				+ GlobalVariables.LongName_LengthInvalid_ErrorMessage + Customers_API.customer_DoesNOT_Exist("0", "0");

		Lockboxes_API.verify_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled, errorMessage);
	}

	@Test
	public void TC270_Lockbox_AddLockbox_MaxLength() throws Exception {
		String lockboxNumber = "723456789";
		String shortName = "MaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLe";
		String longName = "MaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLeMaxLengthMaxLengthMaxLengthMaxLengthMaxLengthMaxLe";
		String bankID = "723456789";
		String customerID = "723456789";
		String mailoutInstructions = "0";
		String isDisabled = "false";

		Banks_API.post_Bank(bankID, "Max Length Bank", "Max Length Bank");
		Customers_API.post_Customer(customerID, bankID, "Max Length Customer", "Max Length Customer");
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);

	}

	@Test
	public void TC277_Lockbox_AddLockbox_AlreadyExists() throws Exception {
		String lockboxNumber = "70006";
		String shortName = "Add Lockbox";
		String longName = "Add Lockbox Already Exists";
		String bankID = "70020";
		String customerID = "70018";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Customers_API.post_Customer(customerID, bankID, "AddLockbox", "Stuff");
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);

		String errorMessage = Lockboxes_API.lockbox_Already_Exists_ErrorMessage(lockboxNumber, customerID, bankID);

		Lockboxes_API.verify_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled, errorMessage);

	}

	@Test
	public void TC278_Lockbox_AddLockbox_PreviouslyDeleted() throws Exception {
		String lockboxNumber = "70007";
		String shortName = "Add Lockbox";
		String longName = "Add Lockbox Already Exists";
		String bankID = "70020";
		String customerID = "70019";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Customers_API.post_Customer(customerID, bankID, "AddLockbox", "Lockbox already Deleted");

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);
		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);

		String errorMessage = Lockboxes_API.lockbox_Already_Exists_ErrorMessage(lockboxNumber, customerID, bankID);

		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);
	}

	// TC not valid due to changes.
	// @Test
	// public void TC279_Lockbox_AddLockbox_BankPreviouslyDeleted() throws
	// Exception {
	// String lockboxNumber = "70008";
	// String shortName = "Bank Previously Deleted.";
	// String longName = "Bank Previously Deleted.";
	// String bankID = "70028";
	// String customerID = "70019";
	// String mailoutInstructions = "0";
	// String isDisabled = "0";
	//
	// Lockboxes.delete_Lockbox(lockboxNumber, customerID, bankID);
	// Banks.post_Bank(bankID, "Bank to Delete", "Bank to Delete.");
	// Customers.delete_Customer(customerID, bankID);
	// Customers.post_Customer(customerID, bankID, "BankDelete", "BankDelete");
	// String output = Banks.delete_Bank(bankID);
	//
	// Lockboxes.verify_Lockbox_IsInvalid(lockboxNumber, customerID, bankID,
	// shortName, longName, mailoutInstructions,
	// isDisabled, Banks.bank_DoesNotExist_ErrorMessage(bankID));
	// }

	@Test
	public void TC280_Lockbox_AddLockbox_CustomerPreviouslyDeleted() throws Exception {
		String lockboxNumber = "70009";
		String shortName = "Bank Previously Deleted.";
		String longName = "Bank Previously Deleted.";
		String bankID = "70029";
		String customerID = "70020";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);
		Banks_API.post_Bank(bankID, "Bank to Delete", "Bank to Delete.");

		Customers_API.post_Customer(customerID, bankID, "BankDelete", "BankDelete");
		Customers_API.delete_Customer(customerID, bankID);

		Lockboxes_API.verify_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled, Customers_API.customer_DoesNOT_Exist(customerID, bankID));
	}

	@Test
	public void TC286_Lockbox_GetLockbox() throws Exception {
		String lockboxNumber = "70010";
		String shortName = "Bank Previously Deleted.";
		String longName = "Bank Previously Deleted.";
		String bankID = "70030";
		String customerID = "70021";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);
		Customers_API.delete_Customer(customerID, bankID);
		Banks_API.delete_Bank(bankID);

		Banks_API.post_Bank(bankID, "Get Lockbox", "Get Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Get Lockbox Customer", "Get Lockbox Customer");
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);

		Lockboxes_API.verify_GET_Lockbox_Exists(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);
	}

	@Test
	public void TC290_Lockbox_GetLockbox_PreviouslyDeleted() throws Exception {
		String lockboxNumber = "70011";
		String shortName = "Lockbox Previously Deleted.";
		String longName = "Lockbox Previously Deleted.";
		String bankID = "70031";
		String customerID = "70022";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);
		Customers_API.delete_Customer(customerID, bankID);
		Banks_API.delete_Bank(bankID);

		Banks_API.post_Bank(bankID, "Get Lockbox", "Get Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Get Lockbox Customer", "Get Lockbox Customer");
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);
		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber, customerID, bankID);

		Lockboxes_API.verify_Get_Lockbox_IsInvalid(lockboxNumber, customerID, bankID,
				Lockboxes_API.lockbox_doesNOT_Exist_ErrorMessage(lockboxNumber, customerID, bankID));
	}

	@Test
	public void TC293_Lockbox_UpdateLockbox_NoValues() throws Exception {
		String lockboxNumber = "70012";
		String shortName = "Lockbox Updated, No Values.";
		String longName = "Lockbox Updated, No Values.";
		String bankID = "70032";
		String customerID = "70023";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);

		Banks_API.post_Bank(bankID, "Update Lockbox", "Update Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Update Lockbox Customer", "Update Lockbox Customer");
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions,
				isDisabled);

		String expectedErrorText = Banks_API.bankIDNotValid_ErrorMessage + Customers_API.customerID_NotValid_ErrorMessage
				+ GlobalVariables.ShortName_Empty_ErrorMessage + GlobalVariables.LongName_Empty_ErrorMessage
				+ Customers_API.customer_DoesNOT_Exist("0", "0")
				+ Lockboxes_API.lockbox_doesNOT_Exist_ErrorMessage(lockboxNumber, "0", "0");

		Lockboxes_API.verify_Update_Lockbox_IsInvalid(lockboxNumber, "", "", "", "", "", "", expectedErrorText);
	}

	@Test
	public void TC309_Lockbox_UpdateLockbox_PreviouslyDeleted() throws Exception {
		String lockboxNumber = "70013";
		String shortName = "Lockbox Updated, Previously Deleted.";
		String longName = "Lockbox Updated, Previously Deleted.";
		String bankID = "70033";
		String customerID = "70024";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);

		Banks_API.post_Bank(bankID, "Update Lockbox", "Update Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Update Lockbox Customer", "Update Lockbox Customer");
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);
		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber, customerID, bankID);

		String expectedErrorText = Lockboxes_API.lockbox_PreviouslyDeleted_ErrorMessage(lockboxNumber, customerID, bankID);

		Lockboxes_API.verify_Update_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, shortName, longName,
				mailoutInstructions, isDisabled, expectedErrorText);
	}

	@Test
	public void TC314_Lockbox_DeleteLockbox_DoesNOT_Exist() throws Exception {
		// never use this lockbox number for another test.
		String lockboxNumber = "70015";
		String shortName = "Lockbox Doesn't Exist.";
		String longName = "Lockbox Doesn't Exist.";
		String bankID = "70034";
		String customerID = "70025";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Banks_API.post_Bank(bankID, "Update Lockbox", "Update Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Update Lockbox Customer", "Update Lockbox Customer");

		String expectedErrorText = Lockboxes_API.lockbox_doesNOT_Exist_ErrorMessage(lockboxNumber, customerID, bankID);
		// Seeing an extra blank space after the bank ID. Not really wrong, but
		// also causes test to fail if it's not adjusted.
		expectedErrorText = expectedErrorText.replace(" does not exist", "  does not exist");

		Lockboxes_API.verify_Delete_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, expectedErrorText);
		BaseUI.verify_false_AndLog(Lockboxes_API.lockbox_Entry_Exists_InDatabase(lockboxNumber, customerID, bankID),
				"Lockbox did not exist.", "Lockbox was NOT supposed to exist.");
	}

	@Test
	public void TC315_Lockbox_DeleteLockbox_PreviouslyDeleted() throws Exception {
		String lockboxNumber = "70014";
		String shortName = "Lockbox Deleted, Previously Deleted.";
		String longName = "Lockbox Deleted, Previously Deleted.";
		String bankID = "70034";
		String customerID = "70025";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);

		Banks_API.post_Bank(bankID, "Update Lockbox", "Update Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Update Lockbox Customer", "Update Lockbox Customer");
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);
		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber, customerID, bankID);

		String expectedErrorText = Lockboxes_API.lockbox_doesNOT_Exist_ErrorMessage(lockboxNumber, customerID, bankID);
		// Seeing an extra blank space after the bank ID. Not really wrong, but
		// also causes test to fail if it's not adjusted.
		expectedErrorText = expectedErrorText.replace(" does not exist", "  does not exist");

		Lockboxes_API.verify_Delete_Lockbox_IsInvalid(lockboxNumber, customerID, bankID, expectedErrorText);
	}

	@Test
	public void TC320_Lockbox_DeleteLockbox() throws Exception {
		String lockboxNumber = "70016";
		String shortName = "Delete Lockbox.";
		String longName = "Delete Lockbox.";
		String bankID = "70034";
		String customerID = "70025";
		String mailoutInstructions = "0";
		String isDisabled = "0";

		Lockboxes_API.delete_Lockbox(lockboxNumber, customerID, bankID);

		Banks_API.post_Bank(bankID, "Update Lockbox", "Update Lockbox");
		Customers_API.post_Customer(customerID, bankID, "Update Lockbox Customer", "Update Lockbox Customer");
		Lockboxes_API.post_Lockbox(lockboxNumber, customerID, bankID, shortName, longName, mailoutInstructions, isDisabled);

		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxNumber, customerID, bankID);
	}

	@Test
	public void TC593_Lockbox_AddLockboxes_WithSameLockboxNumber() throws Exception {

		String lockboxNumber = "70017";

		// first lockbox
		String shortName1 = "Lockbox1A";
		String longName1 = "Lockbox1A";
		String bankNumber1 = "70035";
		String customerNumber1 = "70026";
		String IsDisabled1 = "False";

		// second lockbox - different bank number and customer number
		String shortName2 = "Lockbox1A";
		String longName2 = "Lockbox1A";
		String bankNumber2 = "70036";
		String customerNumber2 = "70027";
		String IsDisabled2 = "False";

		// third lockbox - different bank number and customer number
		String shortName3 = "Lockbox1A";
		String longName3 = "Lockbox1A";
		String bankNumber3 = "70037";
		String customerNumber3 = "70028";
		String IsDisabled3 = "False";

		// fourth lockbox - same bank number, but different customer number as
		// third lockbox
		String shortName4 = "Lockbox1A";
		String longName4 = "Lockbox1A";
		String bankNumber4 = "70037";
		String customerNumber4 = "70029";
		String IsDisabled4 = "False";

		// fifth lockbox - different bank number and customer number
		String shortName5 = "Lockbox1A";
		String longName5 = "Lockbox1A";
		String bankNumber5 = "70038";
		String customerNumber5 = "70038";
		String IsDisabled5 = "False";

		// sixth lockbox - same bank number as fifth, but same customer number
		String shortName6 = "Lockbox1A";
		String longName6 = "Lockbox1A";
		String bankNumber6 = "70039";
		String customerNumber6 = "70038";
		String IsDisabled6 = "False";

		Banks_API.post_Bank(bankNumber1, "bank 1", "bank 1");
		Customers_API.post_Customer(customerNumber1, bankNumber1, shortName1, longName1);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber1, bankNumber1, shortName1, longName1, "",
				IsDisabled1);

		Banks_API.post_Bank(bankNumber2, "bank 2", "bank 2");
		Customers_API.post_Customer(customerNumber2, bankNumber2, shortName2, longName2);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber2, bankNumber2, shortName2, longName2, "",
				IsDisabled2);

		Banks_API.post_Bank(bankNumber3, "bank 3", "bank 3");
		Customers_API.post_Customer(customerNumber3, bankNumber3, shortName3, longName3);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber3, bankNumber3, shortName3, longName3, "",
				IsDisabled3);

		Banks_API.post_Bank(bankNumber4, "bank 4", "bank 4");
		Customers_API.post_Customer(customerNumber4, bankNumber4, shortName4, longName4);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber4, bankNumber4, shortName4, longName4, "",
				IsDisabled4);

		Banks_API.post_Bank(bankNumber5, "bank 5", "bank 5");
		Customers_API.post_Customer(customerNumber5, bankNumber5, shortName5, longName5);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber5, bankNumber5, shortName5, longName5, "",
				IsDisabled5);

		Banks_API.post_Bank(bankNumber6, "bank 6", "bank 6");
		Customers_API.post_Customer(customerNumber6, bankNumber6, shortName6, longName6);
		Lockboxes_API.verify_Able_ToAddLockbox(lockboxNumber, customerNumber6, bankNumber6, shortName6, longName6, "",
				IsDisabled6);
	}

	@Test
	public void TC594_Lockbox_AddLockbox_CustomerExists_BankDoesntExist() throws Exception {
		String lockboxID = "70018";
		String shortName = "Cust and Bank Combo";
		String longName = "Cust and Bank Combo";
		// BankID should not exist.
		String bankID = "70040";
		String customerID = "70040";

		String bankID2 = "70020";

		Customers_API.post_Customer(customerID, "70020", shortName, longName);

		Lockboxes_API.verify_Lockbox_IsInvalid(lockboxID, customerID, bankID, shortName, longName, "", "false",
				Customers_API.customer_DoesNOT_Exist(customerID, bankID));

		Lockboxes_API.verify_Able_ToAddLockbox(lockboxID, customerID, bankID2, shortName, longName, "", "false");

	}

	@Test
	public void TC596_Lockbox_DeleteLockbox_SameID() throws Exception {

		String lockboxID = "70019";
		String shortName = "Same ID";
		String longName = "Same ID for 3 lockboxes";

		String bankID1 = "70041";
		String customerID1 = "70041";

		String bankID2 = "70042";
		String customerID2 = "70042";

		String bankID3 = "70043";
		String customerID3 = "70043";

		Banks_API.post_Bank(bankID1, shortName, longName);
		Banks_API.post_Bank(bankID2, shortName, longName);
		Banks_API.post_Bank(bankID3, shortName, longName);

		Customers_API.post_Customer(customerID1, bankID1, shortName, longName);
		Customers_API.post_Customer(customerID2, bankID2, shortName, longName);
		Customers_API.post_Customer(customerID3, bankID3, shortName, longName);

		Lockboxes_API.post_Lockbox(lockboxID, customerID1, bankID1, shortName, longName, "", "false");
		Lockboxes_API.post_Lockbox(lockboxID, customerID2, bankID2, shortName, longName, "", "false");
		Lockboxes_API.post_Lockbox(lockboxID, customerID3, bankID3, shortName, longName, "", "false");

		Lockboxes_API.verify_Able_ToDelete_LockBox(lockboxID, customerID2, bankID2);

		Lockboxes_API.verify_Lockbox_Exists(lockboxID, customerID1, bankID1, shortName, longName, "", "false");
		Lockboxes_API.verify_Lockbox_Exists(lockboxID, customerID3, bankID3, shortName, longName, "", "false");

	}

	TableData databaseData = new TableData();

	// This method provides the data that our test will use.
	// Each set of data will generate a new test.
	@DataProvider(name = "default")
	public Object[][] createData() throws Exception {
		TableData apiData = Lockboxes_API.return_ALL_Lockboxes_from_APICall();
		databaseData = Lockboxes_API.return_ALL_Lockboxes_fromDatabase();
		BaseUI.verify_true_AndLog(apiData.data.size() > 0, "Found API Data.", "Did NOT find API Data.");

		Object[][] rowObject = new Object[apiData.data.size()][2];

		for (int i = 0; i < apiData.data.size(); i++) {
			rowObject[i][0] = i;
			rowObject[i][1] = apiData.data.get(i);
		}

		return rowObject;

	}

	@Test(dataProvider = "default")
	public void TC289_GetALL_Lockboxes(Integer index, HashMap<String, String> apiRow) throws Exception {
		// Need to re-think this, can't be calling database connection each time

		// BaseUI.verify_true_AndLog(apiData.data.size() ==
		// databaseData.data.size(), "API data matched Database data in size.",
		// "API data did NOT match Database data in size.");
		BaseUI.verify_TableRow_Matches(index.toString(), databaseData.data.get(index), apiRow);

	}

}
