package mailTracker.pages;

import mailTracker.data.AuthorizationToken;

public class GlobalVariables {

	public static String dbConnectionString = "jdbc:sqlserver://scscdevqa.qalabs.nwk;user=sa;password=Wausau#1;databaseName=WFSDB_MAIL_TRACKER";

	public static String startingURL = "http://scscdevqa.qalabs.nwk/MailTracker.services/api/";

	public static String LongName_Empty_ErrorMessage = "Long Name is required. ";
	public static String ShortName_Empty_ErrorMessage = "Short Name is required. ";
	public static String ShortName_LengthInvalid_ErrorMessage = "Invalid Short Name length, must be 50 characters or less. ";
	public static String Name_LengthInvalid_ErrorMessage = "Invalid Name length, must be 40 characters or less. ";
	public static String LongName_LengthInvalid_ErrorMessage = "Invalid Long Name length, must be 100 characters or less. ";

	public static String website_URL() throws Exception {
		String url = "http://scscdevqa.qalabs.nwk/MailTracker.Web/?auth=" + AuthorizationToken.return_JWT_Token();

		return url;
	}

	public static String format_ID_ForComparison(String id) {
		id = id.trim();
		id = String.valueOf(Integer.parseInt(id));

		return id;
	}
}
