package mailTracker.pages;

import utils.BaseUI;
import utils.Locator;

public class Navigation {

	public static void navigate_To_Page(String linkText) throws Exception {
		BaseUI.click(Locator.lookupElement("nav_DropdownLink"));
		Thread.sleep(300);

		BaseUI.click(Locator.lookupElement("nav_Dropdown_Item_ByText", linkText, null));
		BaseUI.wait_forPageToFinishLoading();
	}

	public static void navigate_To_Lockboxes() throws Exception {
		navigate_To_Page("Lockboxes");
	}

}// End of Class
