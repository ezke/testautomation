package mailTracker.pages;

import java.util.HashMap;

import utils.BaseUI;
import utils.Browser;
import utils.TableData;

public class Lockbox {

	public static TableData lockboxRows() {
		TableData lockboxes = new TableData();
		if (Browser.currentBrowser.equals("internetexplorer")) {
			HashMap<String, String> tableCellMappings = new HashMap<String, String>();
			tableCellMappings.put("Bank", "./td[1]");
			tableCellMappings.put("Customer", "./td[2]");
			tableCellMappings.put("Lockbox", "./td[3]");
			tableCellMappings.put("Short Name", "./td[4]");
			tableCellMappings.put("Long Name", "./td[5]");

			lockboxes.data = BaseUI.tableExtractor("lockbox_TableRows", tableCellMappings);
		} else {
			String[] headers = { "Bank", "Customer", "Lockbox", "Short Name", "Long Name", "ContactsCell",
					"InstructionsCell" };

			lockboxes = BaseUI.tableExtractorV2("lockbox_Table_Body", headers);
		}

		return lockboxes;
	}

}// End of Class
