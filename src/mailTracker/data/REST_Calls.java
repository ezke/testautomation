package mailTracker.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;



import utils.TableData;

public class REST_Calls {

//	public static String httpPost(String urlStr, String paramName) throws Exception {
//		return http_RESTCall(urlStr, paramName, "POST");
//	}
//
//	public static String httpDelete(String urlStr, String paramName) throws Exception {
//
//		return http_RESTCall(urlStr, paramName, "DELETE");
//	}
//
//	public static String httpPut(String urlStr, String paramName) throws Exception {
//
//		return http_RESTCall(urlStr, paramName, "PUT");
//	}
//
//	public static String httpGet(String urlStr, String paramName) throws Exception {
//
//		return http_RESTCall(urlStr, paramName, "GET");
//	}
//
//	public static String http_RESTCall(String urlStr, String paramName, String requestType) throws Exception {
//		URL url = new URL(urlStr);
//		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//		String token = JWT_Token.return_JWT_Token();
//
//		conn.setDoOutput(true);
//		conn.setRequestMethod(requestType);
//
//		conn.setRequestProperty("Content-Type", "application/json");
//
//		conn.setRequestProperty("Authorization", "Bearer " + token);
//
//		// Create the form content
//		if (paramName != null) {
//			OutputStream out = conn.getOutputStream();
//			Writer writer = new OutputStreamWriter(out, "UTF-8");
//			writer.write(paramName);
//
//			writer.close();
//			out.close();
//		}
//
//		BufferedReader rd;
//
//		if (conn.getResponseCode() != 200) {
//			rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
//			// throw new IOException(conn.getResponseMessage());
//		} else {
//			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//		}
//
//		// Buffer the result into a string
//		StringBuilder sb = new StringBuilder();
//		String line;
//		while ((line = rd.readLine()) != null) {
//			sb.append(line);
//		}
//		rd.close();
//
//		conn.disconnect();
//		return sb.toString();
//	}
//
//
//	public static TableData return_JSON_Array_AsTableData(String jsonArrayString, String nameOfArray) throws Exception {
//
//		TableData results = new TableData();
//
//		JSONObject initialJSON = new JSONObject(jsonArrayString);
//		String result = initialJSON.getString(nameOfArray);
//
//		result = result.substring(1, result.length() - 1);
//
//		List<String> dataReturned = splitUpArray(result);
//
//		for (String row : dataReturned) {
//			JSONObject newJSON = new JSONObject(row);
//			HashMap<String, String> tableRow = new HashMap<String, String>();
//
//			Iterator<?> keys = newJSON.keys();
//
//			while (keys.hasNext()) {
//				String key = (String) keys.next();
//				String value = return_JSON_Value(row, key);
//				tableRow.put(key, value);
//			}
//
//			results.data.add(tableRow);
//
//		}
//
//		return results;
//	}
//
//	public static List<String> splitUpArray(String data) {
//		List<String> list = new ArrayList();
//		for (int i = 0; i < data.length(); i++) {
//			if ((Character.isDigit(data.charAt(i))) ||
//			// Include negative numbers
//					(data.charAt(i) == '-') && (i + 1 < data.length() && Character.isDigit(data.charAt(i + 1)))) {
//				// Get the number before the comma, unless it's the last number
//				int commaIndex = data.indexOf(",", i);
//				String number = commaIndex > -1 ? data.substring(i, commaIndex) : data.substring(i);
//				list.add(number);
//				i += number.length();
//			} else if (data.charAt(i) == '{') {
//				// Get the group of numbers until you reach the final
//				// closing curly brace
//				StringBuilder sb = new StringBuilder();
//				int openCount = 0;
//				int closeCount = 0;
//				do {
//					if (data.charAt(i) == '{') {
//						openCount++;
//					} else if (data.charAt(i) == '}') {
//						closeCount++;
//					}
//					sb.append(data.charAt(i));
//					i++;
//				} while (closeCount < openCount);
//				list.add(sb.toString());
//			}
//		}
//
//		return list;
//	}
//
//	public static String return_JSON_Value(String jsonString, String retrieveValue) throws Exception {
//		JSONObject jsonMessage = new JSONObject(jsonString);
//
//		Object jsonObject = jsonMessage.get(retrieveValue);
//
//		String jsonValue_ToReturn = "";
//
//		if (jsonObject instanceof String) {
//			// do you work here for string like below
//			jsonValue_ToReturn = (String) jsonObject;
//		} else if (jsonObject instanceof Integer) {
//			jsonValue_ToReturn = Integer.toString((Integer) jsonObject);
//			// do cell work for Integer
//		} else if (jsonObject instanceof Boolean) {
//			jsonValue_ToReturn = (Boolean) jsonObject == true ? "true" : "false";
//		}
//
//		return jsonValue_ToReturn;
//	}

}
