﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DafAggregator.Import;
using DafAggregator.Reports;
using Microsoft.Extensions.CommandLineUtils;

namespace DafAggregator
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                return Run(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 1;
            }
        }
        private static int Run(string[] args)
        {
            var application = DefineApplication();

            return application.Execute(args);
        }
        private static CommandLineApplication DefineApplication()
        {
            var application = new CommandLineApplication();
            application.HelpOption("-?|--help");
            application.Command("report", DefineReportCommand);

            application.OnExecute(() =>
            {
                Console.WriteLine("No command was specified. Nothing to do.");
                application.ShowHelp();
                return 0;
            });
            return application;
        }
        private static void DefineReportCommand(CommandLineApplication app)
        {
            var testNgOption = app.Option(
                "--testng <path>",
                "Path to a TestNG testng-results.xml file.",
                CommandOptionType.MultipleValue);
            var titleOption = app.Option(
                "--title <title>",
                "Title to display at the top of the report.",
                CommandOptionType.SingleValue);
            var outOption = app.Option(
                "-o|--out <path>",
                "Output filename. If not specified, the report will be saved as a temp file.",
                CommandOptionType.SingleValue);
            var openOption = app.Option(
                "--open",
                "Open the report in the default browser.",
                CommandOptionType.NoValue);
            app.OnExecute(() =>
            {
                if (!testNgOption.Values.Any())
                {
                    Console.WriteLine("No input files were specified. Nothing to do.");
                    app.ShowHelp();
                    return 0;
                }

                var testNgInputPaths = testNgOption.Values;
                var title = GetTitle(titleOption);
                var outputPath = GetOutputPath(outOption);
                var openInBrowser = openOption.HasValue();

                var (tests, reportWarnings) = LoadTests(testNgInputPaths);
                SaveReport(title, reportWarnings, tests, outputPath);
                if (openInBrowser)
                    OpenReport(outputPath);
                return 0;
            });
        }
        private static string GetTitle(CommandOption titleOption)
        {
            return titleOption.HasValue() ? titleOption.Value() : "Summary";
        }
        private static string GetOutputPath(CommandOption outOption)
        {
            return outOption.HasValue()
                ? outOption.Value()
                : Path.Combine(Path.GetTempPath(), "DafAggregator.html");
        }
        private static (IEnumerable<Test>, IEnumerable<string>) LoadTests(IEnumerable<string> testNgInputPaths)
        {
            var tests = new List<Test>();
            var warnings = new List<string>();
            foreach (var path in testNgInputPaths)
            {
                if (File.Exists(path))
                {
                    Console.WriteLine($"Reading TestNG results from '{path}'");
                    tests.AddRange(TestNgImporter.ImportTests(XElement.Load(path)));
                }
                else
                {
                    var warning = $"Input path '{path}' does not exist.";
                    Console.WriteLine($"Warning: {warning}");
                    warnings.Add(warning);
                }
            }
            return (tests, warnings);
        }
        private static void SaveReport(string title, IEnumerable<string> reportWarnings, IEnumerable<Test> tests,
            string outputPath)
        {
            var report = TestCaseReport.BuildHtmlDocument(title, reportWarnings, tests);

            Console.WriteLine($"Saving report to '{outputPath}'");
            report.Save(outputPath);
        }
        private static void OpenReport(string reportPath)
        {
            Console.WriteLine("Opening report in browser");
            Process.Start(new ProcessStartInfo(reportPath) {UseShellExecute = true});
        }
    }
}
