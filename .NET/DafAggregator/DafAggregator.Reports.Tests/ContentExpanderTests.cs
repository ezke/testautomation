﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class ContentExpanderTests
    {
        [SetUp]
        public void SetUp()
        {
            HeaderContent = new ContentElement("h2");
            ParentAttributes = null;
            ParentClass = null;
            HeaderAttributes = null;
            HeaderClass = null;
            InlineIfNoSiblings = false;
        }

        private IEnumerable<Attribute> ParentAttributes { get; set; }
        private string ParentClass { get; set; }
        private IEnumerable<Attribute> HeaderAttributes { get; set; }
        private string HeaderClass { get; set; }
        private IContent HeaderContent { get; set; }
        private bool InlineIfNoSiblings { get; set; }

        private ContentExpander CreateExpander()
        {
            return new ContentExpander(headerContent: HeaderContent,
                parentClass: ParentClass, parentAttributes: ParentAttributes,
                headerClass: HeaderClass, headerAttributes: HeaderAttributes,
                inlineIfNoSiblings: InlineIfNoSiblings);
        }

        [Test]
        public void ConstructedWithHeaderContent_NotAddedToChildren()
        {
            var expander = CreateExpander();
            Assert.That(expander.Children, Does.Not.Contain(HeaderContent));
        }
        [Test]
        public void ConstructedWithHeaderContent_IfNull_Throws()
        {
            HeaderContent = null;
            Assert.Throws<ArgumentNullException>(() => CreateExpander());
        }
        [Test]
        public void ConstructedWithParentClass()
        {
            ParentClass = "class1";
            var expander = CreateExpander();
            Assert.That(expander.ParentAttributes, Is.EqualTo(new[] {new Attribute("class", "class1")}));
        }
        [Test]
        public void ConstructedWithParentClassAndParentAttributes()
        {
            ParentClass = "class1";
            ParentAttributes = new[] {new Attribute("id", "id1")};
            var expander = CreateExpander();
            Assert.That(expander.ParentAttributes, Is.EqualTo(new[]
            {
                new Attribute("class", "class1"),
                new Attribute("id", "id1"),
            }));
        }
        [Test]
        public void ConstructedWithParentAttributes_SavedInProperty()
        {
            ParentAttributes = new[] {new Attribute("data-value", "myValue")};
            var expander = CreateExpander();
            Assert.That(expander.ParentAttributes, Is.EqualTo(ParentAttributes));
        }
        [Test]
        public void ConstructedWithParentAttributes_IfNull_SavesEmptyListInProperty()
        {
            ParentAttributes = null;
            var expander = CreateExpander();
            Assert.That(expander.ParentAttributes, Is.Empty);
        }
        [Test]
        public void ConstructedWithParentAttributes_IfOmitted_SavesEmptyListInProperty()
        {
            var expander = new ContentExpander(HeaderContent);
            Assert.That(expander.ParentAttributes, Is.Empty);
        }
        [Test]
        public void ConstructedWithParentAttributes_IfContainsNull_Throws()
        {
            ParentAttributes = new Attribute[] {null};
            Assert.Throws<ArgumentException>(() => CreateExpander());
        }
        [Test]
        public void ConstructedWithHeaderClass()
        {
            HeaderClass = "class1";
            var expander = CreateExpander();
            Assert.That(expander.HeaderAttributes, Is.EqualTo(new[] {new Attribute("class", "class1")}));
        }
        [Test]
        public void ConstructedWithHeaderClassAndParentAttributes()
        {
            HeaderClass = "class1";
            HeaderAttributes = new[] {new Attribute("id", "id1")};
            var expander = CreateExpander();
            Assert.That(expander.HeaderAttributes, Is.EqualTo(new[]
            {
                new Attribute("class", "class1"),
                new Attribute("id", "id1"),
            }));
        }
        [Test]
        public void ConstructedWithHeaderAttributes_SavedInProperty()
        {
            HeaderAttributes = new[] {new Attribute("data-value", "myValue")};
            var expander = CreateExpander();
            Assert.That(expander.HeaderAttributes, Is.EqualTo(HeaderAttributes));
        }
        [Test]
        public void ConstructedWithHeaderAttributes_IfNull_SavesEmptyListInProperty()
        {
            HeaderAttributes = null;
            var expander = CreateExpander();
            Assert.That(expander.HeaderAttributes, Is.Empty);
        }
        [Test]
        public void ConstructedWithHeaderAttributes_IfOmitted_SavesEmptyListInProperty()
        {
            var expander = new ContentExpander(HeaderContent);
            Assert.That(expander.HeaderAttributes, Is.Empty);
        }
        [Test]
        public void ConstructedWithHeaderAttributes_IfContainsNull_Throws()
        {
            HeaderAttributes = new Attribute[] {null};
            Assert.Throws<ArgumentException>(() => CreateExpander());
        }
        [Test]
        public void ConstructedWithInlineIfNoSiblings_DefaultsToFalse()
        {
            var expander = new ContentExpander(new ContentElement("h2"));
            Assert.That(expander.InlineIfNoSiblings, Is.False);
        }
        [Test]
        public void ConstructedWithInlineIfNoSiblings_SavedInProperty()
        {
            InlineIfNoSiblings = true;
            var expander = CreateExpander();
            Assert.That(expander.InlineIfNoSiblings, Is.True);
        }
        [Test]
        public void AddBodyContent_AddedToChildren()
        {
            var bodyContent = new ContentElement("p");
            var expander = CreateExpander().AddBodyContent(bodyContent);
            Assert.That(expander.Children, Does.Contain(bodyContent));
        }
        [Test]
        public void AddBodyContent_IfNull_Throws()
        {
            var expander = CreateExpander();
            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => expander.AddBodyContent(null));
        }
        [Test]
        public void InnerText_IncludesHeaderAndBodyInnerTextValues_NewlineSeparatedForReadability()
        {
            HeaderContent = new ContentElement("h2", text: "Header");
            var bodyContent1 = new ContentElement("p", text: "Body 1");
            var bodyContent2 = new ContentElement("p", text: "Body 2");
            var expander = CreateExpander().AddBodyContent(bodyContent1).AddBodyContent(bodyContent2);
            Assert.That(expander.InnerText, Is.EqualTo("Header\r\nBody 1\r\nBody 2"));
        }
        [Test]
        public void ToHtml_BareBones()
        {
            var expander = new ContentExpander(new ContentElement("h2", text: "Header Text"));
            Assert.That(expander.ToHtmlString(),
                Is.EqualTo("<details><summary><h2>Header Text</h2></summary></details>"));
        }
        [Test]
        public void ToHtml_WithParentClass()
        {
            var expander = new ContentExpander(parentClass: "class1", headerContent: new ContentText("text"));
            Assert.That(expander.ToHtmlString(), Is.EqualTo(
                "<details class=\"class1\"><summary>text</summary></details>"));
        }
        [Test]
        public void ToHtml_WithParentAttributes()
        {
            var expander = new ContentExpander(
                parentAttributes: new[] {new Attribute("id", "myExpander"), new Attribute("class", "myClass")},
                headerContent: new ContentElement("h2", text: "Header Text"));
            Assert.That(expander.ToHtmlString(), Is.EqualTo(
                "<details id=\"myExpander\" class=\"myClass\"><summary><h2>Header Text</h2></summary></details>"));
        }
        [Test]
        public void ToHtml_WithHeaderClass()
        {
            var expander = new ContentExpander(headerClass: "class1", headerContent: new ContentText("text"));
            Assert.That(expander.ToHtmlString(), Is.EqualTo(
                "<details><summary class=\"class1\">text</summary></details>"));
        }
        [Test]
        public void ToHtml_WithHeaderAttributes()
        {
            var expander = new ContentExpander(
                headerContent: new ContentElement("h2", text: "Header Text"),
                headerAttributes: new[] {new Attribute("id", "myExpander"), new Attribute("class", "myClass")});
            Assert.That(expander.ToHtmlString(), Is.EqualTo(
                "<details><summary id=\"myExpander\" class=\"myClass\"><h2>Header Text</h2></summary></details>"));
        }
        [Test]
        public void ToHtml_WithBodyContents()
        {
            var expander = new ContentExpander(new ContentElement("h2"))
                .AddBodyContent(new ContentElement("p", text: "Body 1"))
                .AddBodyContent(new ContentElement("p", text: "Body 2"));
            Assert.That(expander.ToHtmlString(), Is.EqualTo(
                "<details><summary><h2></h2></summary><p>Body 1</p><p>Body 2</p></details>"));
        }
        [Test]
        public void ToHtml_OneChildExpander()
        {
            // Test with both one and multiple, to make sure we don't inline by default
            var outer = new ContentExpander(new ContentElement("h2", text: "Outer"))
                .AddBodyContent(new ContentExpander(new ContentElement("h3", text: "Inner"))
                    .AddBodyContent(new ContentElement("p", text: "Inner Content")));
            Assert.That(outer.ToHtmlString(), Is.EqualTo(
                "<details><summary><h2>Outer</h2></summary>" +
                "<details><summary><h3>Inner</h3></summary><p>Inner Content</p></details>" +
                "</details>"));
        }
        [Test]
        public void ToHtml_MultipleChildExpanders()
        {
            var outer = new ContentExpander(new ContentElement("h2", text: "Outer"))
                .AddBodyContent(new ContentExpander(new ContentElement("h3", text: "Inner 1"))
                    .AddBodyContent(new ContentElement("p", text: "Inner 1 Content")))
                .AddBodyContent(new ContentExpander(new ContentElement("h3", text: "Inner 2"))
                    .AddBodyContent(new ContentElement("p", text: "Inner 2 Content")));

            Assert.That(outer.ToHtmlString(), Is.EqualTo(
                "<details><summary><h2>Outer</h2></summary>" +
                "<details><summary><h3>Inner 1</h3></summary><p>Inner 1 Content</p></details>" +
                "<details><summary><h3>Inner 2</h3></summary><p>Inner 2 Content</p></details>" +
                "</details>"));
        }
        [Test]
        public void ToHtml_OneChildExpander_WithInlineIfNoSiblings()
        {
            // InlineIfNoSiblings should take effect, and replace the nested expander with its body
            var outer = new ContentExpander(new ContentElement("h2", text: "Outer"))
                .AddBodyContent(new ContentExpander(new ContentElement("h3", text: "Inner"), inlineIfNoSiblings: true)
                    .AddBodyContent(new ContentElement("p", text: "Inner Content 1"))
                    .AddBodyContent(new ContentElement("p", text: "Inner Content 2")));
            Assert.That(outer.ToHtmlString(), Is.EqualTo(
                "<details><summary><h2>Outer</h2></summary>" +
                "<p>Inner Content 1</p><p>Inner Content 2</p>" +
                "</details>"));
        }
        [Test]
        public void ToHtml_MultipleChildExpanders_WithInlineIfNoSiblings()
        {
            // InlineIfNoSiblings should not take effect in this case, because we *do* have siblings
            var outer = new ContentExpander(new ContentElement("h2", text: "Outer"))
                .AddBodyContent(new ContentExpander(new ContentElement("h3", text: "Inner 1"), inlineIfNoSiblings: true)
                    .AddBodyContent(new ContentElement("p", text: "Inner 1 Content 1"))
                    .AddBodyContent(new ContentElement("p", text: "Inner 1 Content 2")))
                .AddBodyContent(new ContentExpander(new ContentElement("h3", text: "Inner 2"), inlineIfNoSiblings: true)
                    .AddBodyContent(new ContentElement("p", text: "Inner 2 Content 1"))
                    .AddBodyContent(new ContentElement("p", text: "Inner 2 Content 2")));

            Assert.That(outer.ToHtmlString(), Is.EqualTo(
                "<details><summary><h2>Outer</h2></summary>" +
                "<details><summary><h3>Inner 1</h3></summary><p>Inner 1 Content 1</p><p>Inner 1 Content 2</p></details>" +
                "<details><summary><h3>Inner 2</h3></summary><p>Inner 2 Content 1</p><p>Inner 2 Content 2</p></details>" +
                "</details>"));
        }
    }
}