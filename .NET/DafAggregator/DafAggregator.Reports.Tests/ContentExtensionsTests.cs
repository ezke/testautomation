﻿using System;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class ContentExtensionsTests
    {
        private string ViewSource(HtmlDocument document)
        {
            var stream = new MemoryStream();
            document.Save(stream);
            return Encoding.UTF8.GetString(stream.ToArray());
        }

        [Test]
        public void HasHtmlDocType()
        {
            var content = new ContentElement("html");
            var document = content.ToHtmlDocument();
            Assert.That(ViewSource(document), Does.Match(@"^<!DOCTYPE html>\r?\n"));
        }
        [Test]
        public void ContainsElement()
        {
            var content = new ContentElement("html");
            var document = content.ToHtmlDocument();
            Assert.That(ViewSource(document), Does.Match(@"<html></html>"));
        }
        [Test]
        public void Descendants()
        {
            IContent head, title, titleText, body, h1, h1Text;
            var content = new ContentElement("html")
                .Add(head = new ContentElement("head")
                    .Add(title = new ContentElement("title")
                        .Add(titleText = new ContentText("My Title"))))
                .Add(body = new ContentElement("body")
                    .Add(h1 = new ContentElement("h1")
                        .Add(h1Text = new ContentText("Hello, World!"))));
            Assert.That(content.Descendants(), Is.EqualTo(new[] {head, title, titleText, body, h1, h1Text}));
        }
        [Test]
        public void Descendants_WithQuery()
        {
            IContent match1, match2, match3;
            var content = new ContentElement("body")
                .Add(new ContentElement("section")
                    .Add(match1 = new ContentElement("p", cssClass: "blue")))
                .Add(new ContentElement("section")
                    .Add(match2 = new ContentElement("p", cssClass: "blue"))
                    .Add(new ContentElement("p")
                        .Add(match3 = new ContentElement("span", cssClass: "blue"))));
            Assert.That(content.Descendants("blue"), Is.EqualTo(new[] {match1, match2, match3}));
        }
        [Test]
        public void Child_WithOneMatch_ReturnsMatch()
        {
            IContent h1;
            var content = new ContentElement("body")
                .Add(h1 = new ContentElement("h1"));
            Assert.That(content.Child("h1"), Is.SameAs(h1));
        }
        [Test]
        public void Child_WithNoMatches_Throws()
        {
            var content = new ContentElement("body");
            Assert.Throws<InvalidOperationException>(() => content.Child("h1"));
        }
        [Test]
        public void Child_WithTooManyMatches_Throws()
        {
            var content = new ContentElement("body")
                .Add(new ContentElement("h1"))
                .Add(new ContentElement("h1"));
            Assert.Throws<InvalidOperationException>(() => content.Child("h1"));
        }
        [Test]
        public void Child_FindsMatchingChild_AndIgnoresGrandchildren()
        {
            IContent blueChild;
            var content = new ContentElement("body")
                .Add(new ContentElement("h1")
                    .Add(new ContentElement("span", cssClass: "blue")))
                .Add(new ContentElement("p"))
                .Add(blueChild = new ContentElement("p", cssClass: "blue"))
                .Add(new ContentElement("p"));
            Assert.That(content.Child("blue"), Is.SameAs(blueChild));
        }
        [Test]
        public void Descendant_WithOneMatch_ReturnsMatch()
        {
            IContent i;
            var content = new ContentElement("body")
                .Add(new ContentElement("h1")
                    .Add(i = new ContentElement("i")));
            Assert.That(content.Descendant("i"), Is.SameAs(i));
        }
        [Test]
        public void Descendant_WithNoMatches_Throws()
        {
            var content = new ContentElement("body")
                .Add(new ContentElement("h1"));
            Assert.Throws<InvalidOperationException>(() => content.Descendant("i"));
        }
        [Test]
        public void Descendant_WithTooManyMatches_Throws()
        {
            var content = new ContentElement("body")
                .Add(new ContentElement("h1")
                    .Add(new ContentElement("i")))
                .Add(new ContentElement("p")
                    .Add(new ContentElement("i")));
            Assert.Throws<InvalidOperationException>(() => content.Descendant("i"));
        }
    }
}