﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class ContentElementTests
    {
        [Test]
        public void ConstructedWithName()
        {
            var content = new ContentElement("element");
            Assert.That(content.Name, Is.EqualTo("element"), "Name");
            Assert.That(content.Attributes, Is.Empty, "Attribute count");
            Assert.That(content.Children, Is.Empty, "Child count");
        }
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ConstructedWithName_IfEmptyOrWhitespace_Throws(string name)
        {
            // ReSharper disable once ObjectCreationAsStatement
            Assert.Throws<ArgumentException>(() => new ContentElement(name));
        }
        [Test]
        public void ConstructedWithId_IsShortcutForAttribute()
        {
            var content = new ContentElement("element", id: "myId");
            Assert.That(content.Attributes, Is.EqualTo(new[] {new Attribute("id", "myId")}));
        }
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ConstructedWithId_IfEmptyOrWhitespace_IsIgnored(string id)
        {
            var content = new ContentElement("element", id: id);
            Assert.That(content.Attributes, Is.Empty, "Attributes");
        }
        [Test]
        public void ConstructedWithCssClass_IsShortcutForAttribute()
        {
            var content = new ContentElement("element", cssClass: "myClass");
            Assert.That(content.Attributes, Is.EqualTo(new[] {new Attribute("class", "myClass")}));
        }
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ConstructedWithCssClass_IfEmptyOrWhitespace_IsIgnored(string cssClass)
        {
            var content = new ContentElement("element", cssClass: cssClass);
            Assert.That(content.Attributes, Is.Empty, "Attributes");
        }
        [Test]
        public void ConstructedWithText_IsShortcutForTextChild()
        {
            var content = new ContentElement("element", text: "My Text");
            Assert.That(content.Children.Count, Is.EqualTo(1), "Child count");
            Assert.That(content.Children[0], Is.InstanceOf<ContentText>());
            var contentText = (ContentText) content.Children[0];
            Assert.That(contentText.Text, Is.EqualTo("My Text"));
            Assert.That(contentText.Raw, Is.False);
        }
        [TestCase(null)]
        [TestCase("")]
        public void ConstructedWithText_IfEmpty_IsIgnored(string text)
        {
            var content = new ContentElement("element", text: text);
            Assert.That(content.Children.Count, Is.EqualTo(0), "Child count");
        }
        [Test]
        public void ConstructedWithText_IfWhitespace_IsIncluded()
        {
            var content = new ContentElement("element", text: " ");
            Assert.That(content.Children.Count, Is.EqualTo(1), "Child count");
            Assert.That(content.Children[0], Is.InstanceOf<ContentText>());
            Assert.That(((ContentText) content.Children[0]).Text, Is.EqualTo(" "));
        }
        [Test]
        public void SetAttribute_PreservesOrder()
        {
            var content = new ContentElement("element")
                .SetAttribute("abc", "abcValue")
                .SetAttribute("xyz", "xyzValue")
                .SetAttribute("def", "defValue");
            Assert.That(content.Attributes, Is.EqualTo(new[]
            {
                new Attribute("abc", "abcValue"),
                new Attribute("xyz", "xyzValue"),
                new Attribute("def", "defValue"),
            }));
        }
        [Test]
        public void SetAttribute_ReplacesExistingValue()
        {
            var content = new ContentElement("element")
                .SetAttribute("abc", "abcValue")
                .SetAttribute("abc", "abcValue");
            Assert.That(content.Attributes, Is.EqualTo(new[]
            {
                new Attribute("abc", "abcValue"),
            }));
        }
        [Test]
        public void SetAttribute_InternallyPreservesSpecialCharacters()
        {
            var content = new ContentElement("element").SetAttribute("abc", "<>&'\"");
            Assert.That(content.Attributes, Is.EqualTo(new[] {new Attribute("abc", "<>&'\"")}));
        }
        [Test]
        public void AddSingle()
        {
            var inner1 = new ContentElement("inner1");
            var inner2 = new ContentText("My Text");
            var inner3 = new ContentElement("inner2");
            var outer = new ContentElement("element").Add(inner1).Add(inner2).Add(inner3);
            Assert.That(outer.Children, Is.EqualTo(new IContent[] {inner1, inner2, inner3}));
        }
        [Test]
        public void AddSingle_IfNull_Throws()
        {
            var content = new ContentElement("outer");
            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => content.Add((IContent) null));
        }
        [Test]
        public void AddEnumerable()
        {
            var inner1 = new ContentElement("inner1");
            var inner2 = new ContentText("My Text");
            var inner3 = new ContentElement("inner2");
            var content = new ContentElement("element").Add(new IContent[] {inner1, inner2, inner3});
            Assert.That(content.Children, Is.EqualTo(new IContent[] {inner1, inner2, inner3}));
        }
        [Test]
        public void AddEnumerable_Empty()
        {
            var content = new ContentElement("outer").Add(new IContent[] { });
            Assert.That(content.Children, Is.Empty);
        }
        [Test]
        public void AddEnumerable_IfNull_Throws()
        {
            var content = new ContentElement("outer");
            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => content.Add((IEnumerable<IContent>) null));
        }
        [Test]
        public void AddEnumerable_IfContainsNull_Throws_AndDoesNotAddAnyValues()
        {
            var content = new ContentElement("outer");
            Assert.Throws<ArgumentException>(() => content.Add(new IContent[]
            {
                new ContentElement("inner1"),
                null,
                new ContentElement("inner2"),
            }));
            Assert.That(content.Children.Count, Is.EqualTo(0));
        }
        [Test]
        public void IsMatch_WhenQueryMatchesElementName_ReturnsTrue()
        {
            var content = new ContentElement("myElement");
            Assert.That(content.IsMatch("myElement"), Is.True);
        }
        [Test]
        public void IsMatch_WhenQueryMatchesElementId_ReturnsTrue()
        {
            var content = new ContentElement("element", id: "myId");
            Assert.That(content.IsMatch("myId"), Is.True);
        }
        [Test]
        public void IsMatch_WhenQueryMatchesACssClass_ReturnsTrue()
        {
            var content = new ContentElement("element", cssClass: "foo bar");
            Assert.That(content.IsMatch("foo"), Is.True);
        }
        [Test]
        public void IsMatch_ForCssClasses_RespectsWordBoundaries()
        {
            var content = new ContentElement("element", cssClass: "foobar");
            Assert.That(content.IsMatch("foo"), Is.False);
        }
        [Test]
        public void IsMatch_WhenQueryDoesNotMatch_ReturnsFalse()
        {
            var content = new ContentElement("myElement", id: "myId", cssClass: "myClass");
            Assert.That(content.IsMatch("other"), Is.False);
        }
        [Test]
        public void InnerText_WithNoChildren_IsEmptyString()
        {
            var content = new ContentElement("p");
            Assert.That(content.InnerText, Is.EqualTo(""));
        }
        [Test]
        public void InnerText_ConcatenatesAllChildInnerTextValues()
        {
            var content = new ContentElement("p")
                .Add(new ContentText("Hello, "))
                .Add(new ContentElement("i")
                    .Add(new ContentText("World")))
                .Add(new ContentText("!"));
            Assert.That(content.InnerText, Is.EqualTo("Hello, World!"));
        }
        [Test]
        public void GetAttribute_ReturnsAttributeValue()
        {
            var content = new ContentElement("canvas").SetAttribute("width", "100");
            Assert.That(content.GetAttribute("width"), Is.EqualTo("100"));
        }
        [Test]
        public void GetAttribute_WhenAttributeDoesNotExist_ReturnsEmptyString()
        {
            var content = new ContentElement("canvas");
            Assert.That(content.GetAttribute("width"), Is.EqualTo(""));
        }
        [Test]
        public void ToHtml_WithNameOnly()
        {
            var content = new ContentElement("element");
            Assert.That(content.ToHtmlString(), Is.EqualTo("<element></element>"));
        }
        [Test]
        public void ToHtml_WithAttributes()
        {
            var content = new ContentElement("element")
                .SetAttribute("attr1", "value1")
                .SetAttribute("attr2", "value2");
            Assert.That(content.ToHtmlString(), Is.EqualTo("<element attr1=\"value1\" attr2=\"value2\"></element>"));
        }
        [Test]
        public void ToHtml_EncodesAttributes()
        {
            var content = new ContentElement("element").SetAttribute("attr", "<>&'\"");
            Assert.That(content.ToHtmlString(), Is.EqualTo("<element attr=\"&lt;&gt;&amp;'&quot;\"></element>"));
        }
        [Test]
        public void ToHtml_WithChildren()
        {
            var content = new ContentElement("outer")
                .Add(new ContentElement("inner1"))
                .Add(new ContentText("Text"))
                .Add(new ContentElement("inner2"));
            Assert.That(content.ToHtmlString(), Is.EqualTo("<outer><inner1></inner1>Text<inner2></inner2></outer>"));
        }
    }
}