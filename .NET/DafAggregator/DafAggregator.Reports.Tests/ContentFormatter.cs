﻿using System.Collections.Generic;
using System.Linq;

namespace DafAggregator.Reports
{
    public class ContentFormatter
    {
        private bool _includeCssClassesAtMaxExpanderDepth;
        private int _maxExpanderDepth = int.MaxValue;
        private int _minExpanderDepth;
        private readonly List<string> _parenthesizedElements = new List<string>();

        public ContentFormatter IncludeCssClassesAtMaxExpanderDepth()
        {
            _includeCssClassesAtMaxExpanderDepth = true;
            return this;
        }
        public ContentFormatter MaxExpanderDepth(int maxExpanderDepth)
        {
            _maxExpanderDepth = maxExpanderDepth;
            return this;
        }
        public ContentFormatter MinExpanderDepth(int minExpanderDepth)
        {
            _minExpanderDepth = minExpanderDepth;
            return this;
        }
        public ContentFormatter ParenthesizeElements(string query)
        {
            _parenthesizedElements.Add(query);
            return this;
        }
        public string Stringify(IEnumerable<IContent> contents)
        {
            return StringifyInternal(contents, 0);
        }
        public string Stringify(IContent content)
        {
            return StringifyInternal(content, 0);
        }
        private string Indent(int expanderDepth)
        {
            var indentLevel = expanderDepth - _minExpanderDepth;
            return indentLevel >= 0 ? new string(' ', indentLevel * 2) : "";
        }
        private string StringifyInternal(IEnumerable<IContent> contents, int expanderDepth)
        {
            var separator = expanderDepth >= _minExpanderDepth ? "|" : "";
            return string.Join(separator, contents.Select(content =>
                Indent(expanderDepth) + StringifyInternal(content, expanderDepth)));
        }
        private string StringifyInternal(IContent content, int expanderDepth)
        {
            var result = "";

            // If we haven't yet reached MinExpanderDepth, suppress most of the output. The only thing we will output
            // is the square brackets as we recurse into each expander, so there's *some* reflection of the structure.
            var atMinExpanderDepth = expanderDepth >= _minExpanderDepth;

            var parenthesize = _parenthesizedElements.Any(content.IsMatch);
            if (parenthesize && atMinExpanderDepth)
                result += "(";

            if (atMinExpanderDepth)
                result += StringifyMetadata(content, expanderDepth);

            if (content is ContentExpander expander)
            {
                if (atMinExpanderDepth)
                    result += StringifyInternal(expander.HeaderContent, expanderDepth);
                if (expanderDepth < _maxExpanderDepth)
                {
                    result += Indent(expanderDepth) + "[" +
                              StringifyInternal(content.Children, expanderDepth + 1) +
                              Indent(expanderDepth) + "]";
                }
            }
            else if (atMinExpanderDepth)
            {
                if (content is ContentText)
                    result += content.InnerText;
                else
                {
                    foreach (var child in content.Children)
                        result += StringifyInternal(child, expanderDepth);
                }
            }

            if (parenthesize && atMinExpanderDepth)
                result += ")";

            return result;
        }
        private string StringifyMetadata(IContent content, int expanderDepth)
        {
            var atMaxExpanderDepth = expanderDepth >= _maxExpanderDepth;
            var includeCssClasses = _includeCssClassesAtMaxExpanderDepth && atMaxExpanderDepth;

            var parts = new List<string>();
            if (content is ContentExpander expander)
            {
                if (atMaxExpanderDepth && expander.InlineIfNoSiblings)
                    parts.Add("inlineIfNoSiblings");

                if (includeCssClasses)
                {
                    var parentClass = expander.ParentAttributes.FirstOrDefault(a => a.Name == "class");
                    if (parentClass != null)
                        parts.Add($"parentClass='{parentClass.Value}'");

                    var headerClass = expander.HeaderAttributes.FirstOrDefault(a => a.Name == "class");
                    if (headerClass != null)
                        parts.Add($"headerClass='{headerClass.Value}'");
                }
            } else if (content is ContentElement element)
            {
                if (includeCssClasses)
                {
                    var classValue = element.GetAttribute("class");
                    if (!string.IsNullOrWhiteSpace(classValue))
                        parts.Add($"class='{classValue}'");
                }
            }

            return parts.Any()
                ? "{" + string.Join(",", parts) + "}"
                : "";
        }
    }
}