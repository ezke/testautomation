﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DafAggregator.Builders;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class TestCaseReportTests
    {
        private const string Title = "My Title";

        private IContent BuildReport(params TestBuilder[] testBuilders)
        {
            return TestCaseReport.BuildContent(Title, Enumerable.Empty<string>(),
                testBuilders.Select(testBuilder => testBuilder.Build()));
        }
        private IContent BuildReportWithWarnings(params string[] warnings)
        {
            return TestCaseReport.BuildContent(Title, warnings, Enumerable.Empty<Test>());
        }
        private IContent BuildReportWithUniqueTestCaseIdsAndStatuses(params TestStatus[] statuses)
        {
            var testCaseId = 1;
            var testBuilders = statuses.Select(level =>
            {
                var testBuilder = new TestBuilder().WithTestCaseId(testCaseId++);
                switch (level)
                {
                    case TestStatus.Pass:
                        return testBuilder;
                    case TestStatus.Fail:
                        return testBuilder.WithFailure(new FailureBuilder(FailureLevel.Fail));
                    case TestStatus.Busted:
                        return testBuilder.WithFailure(new FailureBuilder(FailureLevel.Busted));
                    default:
                        throw new ArgumentOutOfRangeException(nameof(level), level, null);
                }
            });
            return BuildReport(testBuilders.ToArray());
        }
        private static ContentElement GetSummaryBannerElement(IContent report)
        {
            return (ContentElement) report.Descendant("summaryBanner");
        }
        private static IContent GetSummaryBannerBigTextElement(IContent report)
        {
            return GetSummaryBannerElement(report).Descendant("bigText");
        }
        private static IContent GetSummaryBannerSupportingTextElement(IContent report)
        {
            return GetSummaryBannerElement(report).Descendant("supportingText");
        }
        private static IReadOnlyList<ContentElement> GetSummaryByStatusItems(IContent report)
        {
            return report.Descendants()
                .Where(item => item.IsMatch("summaryByStatus"))
                .SelectMany(summaryElement => summaryElement.Children)
                .Cast<ContentElement>()
                .ToList();
        }
        private string StringifyInnerText(IEnumerable<IContent> elements)
        {
            return string.Join(",", elements.Select(element => element.InnerText));
        }
        private string StringifyStatusClasses(ContentElement element)
        {
            var value = element.GetAttribute("class");
            var matches = Regex.Matches(value, @"\b(pass|fail|busted)\b");
            return string.Join(" ", matches.Select(match => match.Value).OrderBy(s => s));
        }
        private string StringifyStatusClasses(IEnumerable<ContentElement> elements)
        {
            return string.Join(",", elements.Select(StringifyStatusClasses));
        }

        [Test]
        public void HeadHasCorrectTitle()
        {
            var report = BuildReport();
            var titleElement = report.Descendant("head").Descendant("title");
            Assert.That(titleElement.InnerText, Is.EqualTo(Title));
        }
        [Test]
        public void HasEmbeddedStylesheet()
        {
            var report = BuildReport();
            Assert.DoesNotThrow(() => report.Child("head").Child("style"));
        }
        [Test]
        public void BodyHasCorrectTitle()
        {
            var report = BuildReport();
            var h1Element = report.Descendant("body").Descendant("h1");
            Assert.That(h1Element.InnerText, Is.EqualTo(Title));
        }
        [Test]
        public void GivenNoWarnings_DoesNotContainReportWarningsDiv()
        {
            var report = BuildReportWithWarnings(Array.Empty<string>());
            Assert.That(report.Descendants("reportWarnings").Count(), Is.EqualTo(0));
        }
        [Test]
        public void GivenWarnings_ContainsReportWarnings_AsListItems()
        {
            var report = BuildReportWithWarnings("Warning 1", "Warning 2");
            var listItems = report.Descendant("reportWarnings").Descendants("li");
            var listItemText = string.Join(",", listItems.Select(e => e.InnerText));
            Assert.That(listItemText, Is.EqualTo("Warning 1,Warning 2"));
        }
        [Test]
        public void SummaryBanner_WithNoTests_BigTextSaysPass()
        {
            var report = BuildReport();
            var element = GetSummaryBannerBigTextElement(report);
            Assert.That(element.InnerText, Is.EqualTo("Pass"));
        }
        [Test]
        public void SummaryBanner_WhenAllTestsPass_HasPassClass()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Pass);
            var element = GetSummaryBannerElement(report);
            Assert.That(element.GetAttribute("class"), Does.Match(@"\bpass\b"));
        }
        [Test]
        public void SummaryBanner_WhenAllTestsPass_BigTextSaysPass()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Pass);
            var element = GetSummaryBannerBigTextElement(report);
            Assert.That(element.InnerText, Is.EqualTo("Pass"));
        }
        [Test]
        public void SummaryBanner_WhenAllTestsPass_HasAppropriateSupportingText()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Pass);
            var element = GetSummaryBannerSupportingTextElement(report);
            Assert.That(element.InnerText, Contains.Substring("All requirements verified"));
        }
        [Test]
        public void SummaryBanner_WhenOneTestFails_HasFailClass()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Fail, TestStatus.Pass);
            var element = GetSummaryBannerElement(report);
            Assert.That(element.GetAttribute("class"), Does.Match(@"\bfail\b"));
        }
        [Test]
        public void SummaryBanner_WhenOneTestFails_BigTextSaysFail()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Fail, TestStatus.Pass);
            var bigTextElement = GetSummaryBannerBigTextElement(report);
            Assert.That(bigTextElement.InnerText, Contains.Substring("Fail"));
        }
        [Test]
        public void SummaryBanner_WhenOneTestFails_HasAppropriateSupportingText()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Fail, TestStatus.Pass);
            var element = GetSummaryBannerSupportingTextElement(report);
            Assert.That(element.InnerText, Contains.Substring("Requirements failed or not verified"));
        }
        [Test]
        public void SummaryBanner_WhenOneTestIsBusted_HasBustedClass()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(
                TestStatus.Pass, TestStatus.Fail, TestStatus.Busted);
            var element = GetSummaryBannerElement(report);
            Assert.That(element.GetAttribute("class"), Does.Match(@"\bbusted\b"));
        }
        [Test]
        public void SummaryBanner_WhenOneTestIsBusted_BigTextSaysBusted()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(
                TestStatus.Pass, TestStatus.Fail, TestStatus.Busted);
            var bigTextElement = GetSummaryBannerBigTextElement(report);
            Assert.That(bigTextElement.InnerText, Contains.Substring("Busted"));
        }
        [Test]
        public void SummaryBanner_WhenOneTestIsBusted_HasAppropriateSupportingText()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(
                TestStatus.Pass, TestStatus.Fail, TestStatus.Busted);
            var element = GetSummaryBannerSupportingTextElement(report);
            Assert.That(element.InnerText, Contains.Substring("Errors in test code. One or more tests cannot run."));
        }
        [Test]
        public void SummaryByStatus_WithNoTests_ReportsZeroPassingTests()
        {
            // We should show *something* after the "Details:" heading, so the report doesn't look unfinished.
            var report = BuildReport();
            var elements = GetSummaryByStatusItems(report);
            Assert.That(StringifyInnerText(elements), Is.EqualTo("Passed: 0 test cases (0%)"));
        }
        [Test]
        public void SummaryByStatus_WithPassingTests_ContainsPass()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Pass, TestStatus.Pass);
            var elements = GetSummaryByStatusItems(report);
            Assert.That(StringifyStatusClasses(elements), Is.EqualTo("pass"));
        }
        [Test]
        public void SummaryByStatus_WithFailingTests_ContainsFail()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Fail, TestStatus.Fail);
            var elements = GetSummaryByStatusItems(report);
            Assert.That(StringifyStatusClasses(elements), Is.EqualTo("fail"));
        }
        [Test]
        public void SummaryByStatus_WithBustedTests_ContainsBusted()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(TestStatus.Busted, TestStatus.Busted);
            var elements = GetSummaryByStatusItems(report);
            Assert.That(StringifyStatusClasses(elements), Is.EqualTo("busted"));
        }
        [Test]
        public void SummaryByStatus_WithAllStatuses_ContainsAllStatusesInStandardOrder()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(
                TestStatus.Pass, TestStatus.Busted, TestStatus.Fail);
            var elements = GetSummaryByStatusItems(report);
            Assert.That(StringifyStatusClasses(elements), Is.EqualTo("pass,fail,busted"));
        }
        [Test]
        public void SummaryByStatus_ShowsStatusAsAdjective()
        {
            var report = BuildReportWithUniqueTestCaseIdsAndStatuses(
                TestStatus.Pass, TestStatus.Fail, TestStatus.Busted);
            var elements = GetSummaryByStatusItems(report);
            var valuesDisplay = "(" + StringifyInnerText(elements) + ")";
            Assert.That(elements.Count, Is.EqualTo(3), $"Count of {valuesDisplay}");
            Assert.That(elements[0].InnerText, Does.Contain("Passed"), $"Item 0 of {valuesDisplay}");
            Assert.That(elements[1].InnerText, Does.Contain("Failed"), $"Item 1 of {valuesDisplay}");
            Assert.That(elements[2].InnerText, Does.Contain("Busted"), $"Item 2 of {valuesDisplay}");
        }
        [Test]
        public void SummaryByStatus_ShowsTestCaseCountsAndPercentages()
        {
            var report = BuildReport(
                // Test cases 1-3: pass
                new TestBuilder().WithTestCaseId(1),
                new TestBuilder().WithTestCaseId(2),
                new TestBuilder().WithTestCaseId(3),
                new TestBuilder().WithTestCaseId(3),
                // Test cases 4-5: fail
                new TestBuilder().WithTestCaseId(4),
                new TestBuilder().WithTestCaseId(4).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                new TestBuilder().WithTestCaseId(5).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                // Test case 6: busted
                new TestBuilder().WithTestCaseId(6).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                new TestBuilder().WithTestCaseId(6).WithFailure(new FailureBuilder(FailureLevel.Busted)));
            var elements = GetSummaryByStatusItems(report);
            var valuesDisplay = "(" + StringifyInnerText(elements) + ")";
            Assert.That(elements.Count, Is.EqualTo(3), $"Count of {valuesDisplay}");
            Assert.That(elements[0].InnerText, Does.Contain("3 test cases (50%)"), $"Item 0 of {valuesDisplay}");
            Assert.That(elements[1].InnerText, Does.Contain("2 test cases (33%)"), $"Item 1 of {valuesDisplay}");
            Assert.That(elements[2].InnerText, Does.Contain("1 test case (17%)"), $"Item 2 of {valuesDisplay}");
        }
        [Test]
        public void BustedSection_WithNoBustedTests_IsNotPresent()
        {
            var report = BuildReport(
                new TestBuilder(),
                new TestBuilder().Failing());
            Assert.That(report.Descendants("bustedSection"), Is.Empty);
        }
        [Test]
        public void BustedSection_WithBustedTests_IsPresent()
        {
            var report = BuildReport(
                new TestBuilder().Busted());
            Assert.That(report.Descendants("bustedSection"), Is.Not.Empty);
        }
        [Test]
        public void TestCaseSection_WithNoTests_IsNotPresent()
        {
            var report = BuildReport();
            Assert.That(report.Descendants("testCaseSection"), Is.Empty);
        }
        [Test]
        public void TestCaseSection_WithTests_IsPresent()
        {
            var report = BuildReport(
                new TestBuilder());
            Assert.That(report.Descendants("testCaseSection"), Is.Not.Empty);
        }
    }
}