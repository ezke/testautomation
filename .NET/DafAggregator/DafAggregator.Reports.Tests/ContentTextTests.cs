﻿using System;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class ContentTextTests
    {
        [Test]
        public void Text()
        {
            var content = new ContentText("My Text");
            Assert.That(content.Text, Is.EqualTo("My Text"));
        }
        [Test]
        public void TextCannotBeNull()
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            // ReSharper disable once ObjectCreationAsStatement
            Assert.Throws<ArgumentNullException>(() => new ContentText(null));
        }
        [TestCase("")]
        [TestCase(" ")]
        public void TextCanBeEmptyOrWhitespace(string text)
        {
            var content = new ContentText(text);
            Assert.That(content.Text, Is.EqualTo(text));
        }
        [Test]
        public void TextInternallyPreservesSpecialCharacters()
        {
            var content = new ContentText("<>&'\"");
            Assert.That(content.Text, Is.EqualTo("<>&'\""));
        }
        [Test]
        public void NotRawByDefault()
        {
            var content = new ContentText("My Text");
            Assert.That(content.Raw, Is.False);
        }
        [Test]
        public void Raw()
        {
            var content = new ContentText("My Text", raw: true);
            Assert.That(content.Raw, Is.True);
        }
        [Test]
        public void InnerText()
        {
            IContent content = new ContentText("My Text");
            Assert.That(content.InnerText, Is.EqualTo("My Text"));
        }
        [Test]
        public void ToHtml_Text()
        {
            var content = new ContentText("text");
            Assert.That(content.ToHtmlString(), Is.EqualTo("text"));
        }
        [Test]
        public void ToHtml_InNonRawMode_EncodesEntities()
        {
            // ReSharper disable once RedundantArgumentDefaultValue
            var content = new ContentText("<>&'\"", raw: false);
            Assert.That(content.ToHtmlString(), Is.EqualTo("&lt;&gt;&amp;'&quot;"));
        }
        [Test]
        public void ToHtml_InRawMode_DoesNotEncodeEntities()
        {
            var content = new ContentText("<>&'\"", raw: true);
            Assert.That(content.ToHtmlString(), Is.EqualTo("<>&'\""));
        }
    }
}