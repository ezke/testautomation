﻿using System.Linq;
using DafAggregator.Builders;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class BustedSectionTests
    {
        private IContent BuildSection(params TestBuilder[] testBuilders)
        {
            return BustedSection.Build(testBuilders.Select(builder => builder.Build()));
        }

        [Test]
        public void TopLevel_CreatesOneExpanderPerExceptionTypeAndMessage()
        {
            var section = BuildSection(
                new TestBuilder().Busted("exception1", "Message A"),
                new TestBuilder().Busted("exception1", "Message A"),
                new TestBuilder().Busted("exception1", "Message B"),
                new TestBuilder().Busted("exception2", "Message A"),
                new TestBuilder().Busted("exception2", "Message B"),
                new TestBuilder().Failing());
            var expanders = section.Children.OfType<ContentExpander>();

            Assert.That(new ContentFormatter().MaxExpanderDepth(0).Stringify(expanders), Is.EqualTo(
                "exception1: Message A|exception1: Message B|exception2: Message A|exception2: Message B"));
        }
        [Test]
        public void TopLevel_WithAndWithoutMessage()
        {
            var section = BuildSection(
                new TestBuilder().Busted("exception1"),
                new TestBuilder().Busted("exception1", "Message A"));
            var expanders = section.Children.OfType<ContentExpander>();

            Assert.That(new ContentFormatter().MaxExpanderDepth(0).Stringify(expanders), Is.EqualTo(
                "exception1|exception1: Message A"));
        }
        [Test]
        public void TopLevel_ItemsAreSorted()
        {
            var section = BuildSection(
                new TestBuilder().Busted("A", "1"),
                new TestBuilder().Busted("B", "1"),
                new TestBuilder().Busted("B", "2"),
                new TestBuilder().Busted("A", "2"));
            var expanders = section.Children.OfType<ContentExpander>();

            Assert.That(new ContentFormatter().MaxExpanderDepth(0).Stringify(expanders), Is.EqualTo(
                "A: 1|A: 2|B: 1|B: 2"));
        }
        [Test]
        public void SecondLevel_CreatesOneExpanderPerStackTraceOrigin()
        {
            var section = BuildSection(
                new TestBuilder().Busted("exception", "", new StackTraceEntry(text: "method1")),
                new TestBuilder().Busted("exception", "", new StackTraceEntry(text: "method2"),
                    new StackTraceEntry(text: "more stack trace")),
                new TestBuilder().Failing());
            var expanders = section.Children.OfType<ContentExpander>();

            Assert.That(new ContentFormatter().MaxExpanderDepth(1).Stringify(expanders), Is.EqualTo(
                "exception" +
                "[" +
                "  Occurred at: method1|" +
                "  Occurred at: method2" +
                "]"));
        }
        [Test]
        public void SecondLevel_CanDisplayMultipleStackTraceFormats()
        {
            var section = BuildSection(
                new TestBuilder().Busted("ex"),
                new TestBuilder().Busted("ex", "", new StackTraceEntry()),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(text: "T")),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(methodName: "M")),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(methodName: "M", text: "T")),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(className: "C")),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(className: "C", text: "T")),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(className: "C", methodName: "M")),
                new TestBuilder().Busted("ex", "", new StackTraceEntry(className: "C", methodName: "M", text: "T"))
            );
            var expanders = section.Children.OfType<ContentExpander>();

            Assert.That(new ContentFormatter().MaxExpanderDepth(1).Stringify(expanders), Is.EqualTo(
                "ex" +
                "[" +
                "  Occurred at: unknown location|" +
                "  Occurred at: unknown location|" +
                "  Occurred at: T|" +
                "  Occurred at: M|" +
                "  Occurred at: M T|" +
                "  Occurred at: C|" +
                "  Occurred at: C T|" +
                "  Occurred at: C.M|" +
                "  Occurred at: C.M T" +
                "]"));
        }
        [Test]
        public void ThirdLevel_CreatesOneExpanderPerTestMethod()
        {
            var origin1 = new StackTraceEntry(text: "origin1");
            var origin2 = new StackTraceEntry(text: "origin2");
            var section = BuildSection(
                new TestBuilder().WithMethod("class1", "method1").Busted("exception", "", origin1),
                new TestBuilder().WithMethod("class1", "method1").Busted("exception", "", origin2),
                new TestBuilder().WithMethod("class2", "method2", "param1").Busted("exception", "", origin2),
                new TestBuilder().WithMethod("class2", "method2", "param2").Busted("exception", "", origin2),
                new TestBuilder().Failing());
            var expanders = section.Children.OfType<ContentExpander>();

            Assert.That(new ContentFormatter().MaxExpanderDepth(2).Stringify(expanders), Is.EqualTo(
                "exception" +
                "[" +
                "  Occurred at: origin1" +
                "  [" +
                "    Test: class1.method1" +
                "  ]|" +
                "  Occurred at: origin2" +
                "  [" +
                "    Test: class1.method1|" +
                "    Test: class2.method2" +
                "  ]" +
                "]"));
        }
        [Test]
        public void FourthLevel_CreatesOneExpanderPerParameter()
        {
            var origin = new StackTraceEntry(text: "origin");
            var section = BuildSection(
                new TestBuilder().WithMethod("class1", "method1").Busted("exception", "", origin),
                new TestBuilder().WithMethod("class2", "method2", "param1").Busted("exception", "", origin),
                new TestBuilder().WithMethod("class2", "method2", "param2").Busted("exception", "", origin),
                new TestBuilder().Failing());
            var expanders = section.Children.OfType<ContentExpander>();
            Assert.That(new ContentFormatter().MaxExpanderDepth(3).Stringify(expanders), Is.EqualTo(
                "exception" +
                "[" +
                "  Occurred at: origin" +
                "  [" +
                "    Test: class1.method1" +
                "    [" +
                "      {inlineIfNoSiblings}No parameters" +
                "    ]|" +
                "    Test: class2.method2" +
                "    [" +
                "      Parameters: param1|" +
                "      Parameters: param2" +
                "    ]" +
                "  ]" +
                "]"));
        }
        [Test]
        public void LeafLevel_ShowsStackTrace()
        {
            var report = BuildSection(
                new TestBuilder().WithMethod("class1", "method1").Busted("exception", "",
                    new StackTraceEntry(text: "stack1"), new StackTraceEntry(), new StackTraceEntry(text: "stack2")));
            var failureDetails = report.Descendant("failureDetails");
            var listItems = failureDetails.Descendants("li");
            Assert.That(new ContentFormatter().Stringify(listItems), Is.EqualTo("stack1||stack2"));
        }
    }
}