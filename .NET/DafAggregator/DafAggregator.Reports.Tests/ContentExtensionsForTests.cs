﻿using System.Linq;
using HtmlAgilityPack;

namespace DafAggregator.Reports
{
    public static class ContentExtensionsForTests
    {
        public static string ToHtmlString(this IContent content)
        {
            var document = new HtmlDocument();
            var nodes = content.ToHtmlNodes(document);
            return string.Join("", nodes.Select(node => node.OuterHtml));
        }
    }
}