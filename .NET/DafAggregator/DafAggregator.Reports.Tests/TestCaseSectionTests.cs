﻿using System.Linq;
using DafAggregator.Builders;
using NUnit.Framework;

namespace DafAggregator.Reports
{
    public class TestCaseSectionTests
    {
        private IContent BuildSection(params TestBuilder[] testBuilders)
        {
            var tests = testBuilders.Select(testBuilder => testBuilder.Build()).ToList();
            return TestCaseSection.Build(tests);
        }
        private static ContentFormatter CreateFormatter()
        {
            return new ContentFormatter().ParenthesizeElements("badge");
        }

        [Test]
        public void TopLevel_HasTestCaseIds_Statuses_AndCssClasses()
        {
            var section = BuildSection(
                // First, the pure statuses
                new TestBuilder().WithTestCaseId(1),
                new TestBuilder().WithTestCaseId(2).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                new TestBuilder().WithTestCaseId(3).WithFailure(new FailureBuilder(FailureLevel.Busted)),
                // Then mixed statuses
                new TestBuilder().WithTestCaseId(4),
                new TestBuilder().WithTestCaseId(4).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                new TestBuilder().WithTestCaseId(5),
                new TestBuilder().WithTestCaseId(5).WithFailure(new FailureBuilder(FailureLevel.Busted)),
                new TestBuilder().WithTestCaseId(6).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                new TestBuilder().WithTestCaseId(6).WithFailure(new FailureBuilder(FailureLevel.Busted)),
                new TestBuilder().WithTestCaseId(7),
                new TestBuilder().WithTestCaseId(7).WithFailure(new FailureBuilder(FailureLevel.Fail)),
                new TestBuilder().WithTestCaseId(7).WithFailure(new FailureBuilder(FailureLevel.Busted)));

            var actual = CreateFormatter()
                .MaxExpanderDepth(0)
                .IncludeCssClassesAtMaxExpanderDepth()
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "By Test Case|" +
                "{headerClass='pass'}PASS - PT1|" +
                "{headerClass='fail'}FAIL - PT2|" +
                "{headerClass='busted'}BUSTED - PT3|" +
                "{headerClass='fail'}MIXED({class='badge fail'}Fail)({class='badge pass'}Pass) - PT4|" +
                "{headerClass='busted'}MIXED({class='badge busted'}Busted)({class='badge pass'}Pass) - PT5|" +
                "{headerClass='busted'}MIXED({class='badge busted'}Busted)({class='badge fail'}Fail) - PT6|" +
                "{headerClass='busted'}MIXED({class='badge busted'}Busted)({class='badge fail'}Fail)({class='badge pass'}Pass) - PT7"));
        }
        [Test]
        public void TopLevel_TestCasesAreSorted()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(3),
                new TestBuilder().WithTestCaseId(1),
                new TestBuilder().WithTestCaseId(2));

            var actual = CreateFormatter()
                .MaxExpanderDepth(0)
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "By Test Case|PASS - PT1|PASS - PT2|PASS - PT3"));
        }
        [Test]
        public void SecondLevel_HasTestClasses_Statuses_AndCssClasses()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method2").Failing(),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class2", "Method3").Busted(),
                new TestBuilder().WithTestCaseId(2).WithMethod("Class3", "Method4"));

            var actual = CreateFormatter()
                .MaxExpanderDepth(1)
                .IncludeCssClassesAtMaxExpanderDepth()
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "By Test Case|" +
                "MIXED(Busted)(Fail)(Pass) - PT1" +
                "[" +
                "  {parentClass='indent',headerClass='fail'}MIXED({class='badge fail'}Fail)({class='badge pass'}Pass) - Class1|" +
                "  {parentClass='indent',headerClass='busted'}BUSTED - Class2" +
                "]|" +
                "PASS - PT2" +
                "[" +
                "  {parentClass='indent',headerClass='pass'}PASS - Class3" +
                "]"));
        }
        [Test]
        public void SecondLevel_TestClassesAreSorted()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class3", "Method1"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method2"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class2", "Method3"));

            var actual = CreateFormatter()
                .MinExpanderDepth(1)
                .MaxExpanderDepth(1)
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "[" +
                "PASS - Class1|" +
                "PASS - Class2|" +
                "PASS - Class3" +
                "]"));
        }
        [Test]
        public void ThirdLevel_HasTestMethods_Statuses_AndCssClasses()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method2").Failing(),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class2", "Method3").Busted());

            var actual = CreateFormatter()
                .MaxExpanderDepth(2)
                .IncludeCssClassesAtMaxExpanderDepth()
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "By Test Case|" +
                "MIXED(Busted)(Fail)(Pass) - PT1" +
                "[" +
                "  MIXED(Fail)(Pass) - Class1" +
                "  [" +
                "    {parentClass='indent',headerClass='pass'}PASS - Method1|" +
                "    {parentClass='indent',headerClass='fail'}FAIL - Method2" +
                "  ]|" +
                "  BUSTED - Class2" +
                "  [" +
                "    {parentClass='indent',headerClass='busted'}BUSTED - Method3" +
                "  ]" +
                "]"));
        }
        [Test]
        public void ThirdLevel_IsOrderedChronologically()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method9"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1"));

            var actual = CreateFormatter()
                .MinExpanderDepth(2)
                .MaxExpanderDepth(2)
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "[[" +
                "PASS - Method9|" +
                "PASS - Method1" +
                "]]"));
        }
        [Test]
        public void FourthLevel_HasTestParameters_Statuses_AndCssClasses()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method2", "Param1", "ParamA").Failing(),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method2", "Param2", "ParamB").Busted());

            var actual = CreateFormatter()
                .MaxExpanderDepth(3)
                .IncludeCssClassesAtMaxExpanderDepth()
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "By Test Case|" +
                "MIXED(Busted)(Fail)(Pass) - PT1" +
                "[" +
                "  MIXED(Busted)(Fail)(Pass) - Class1" +
                "  [" +
                "    PASS - Method1" +
                "    [" +
                "      {inlineIfNoSiblings,parentClass='indent',headerClass='pass'}PASS - " +
                "    ]|" +
                "    MIXED(Busted)(Fail) - Method2" +
                "    [" +
                "      {parentClass='indent',headerClass='fail'}FAIL - Param1 / ParamA|" +
                "      {parentClass='indent',headerClass='busted'}BUSTED - Param2 / ParamB" +
                "    ]" +
                "  ]" +
                "]"));
        }
        [Test]
        public void FourthLevel_IsOrderedChronologically()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1", "Param3"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1", "Param1"),
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1", "Param2"));

            var actual = CreateFormatter()
                .MinExpanderDepth(3)
                .MaxExpanderDepth(3)
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "[[[" +
                "PASS - Param3|" +
                "PASS - Param1|" +
                "PASS - Param2" +
                "]]]"));
        }
        [Test]
        public void FifthAndSixthLevels_WithNoStackTrace_HasExceptionTypeAndMessage_AndNoStackTraceHeader()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1", "Param3").WithFailure(
                    new FailureBuilder(FailureLevel.Fail)
                        .WithExceptionType("java.lang.AssertionError")
                        .WithMessage("Expected '1' but was '0'")));

            var actual = CreateFormatter()
                .MinExpanderDepth(4)
                .MaxExpanderDepth(5)
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "[[[[" +
                "java.lang.AssertionError" +
                "Expected '1' but was '0'" +
                "]]]]"));
        }
        [Test]
        public void FifthAndSixthLevels_WithStackTrace_HasExceptionTypeAndMessage_AndStackTraceHeader()
        {
            var section = BuildSection(
                new TestBuilder().WithTestCaseId(1).WithMethod("Class1", "Method1", "Param3").WithFailure(
                    new FailureBuilder(FailureLevel.Fail)
                        .WithExceptionType("java.lang.AssertionError")
                        .WithMessage("Expected '1' but was '0'")
                        .WithStackTrace(
                            new StackTraceEntry("utils.Helper", "DoHelpfulThing", "(Helper.java:5)"),
                            new StackTraceEntry("dr.Class1", "TestName", "(Class1.java:5)"))));

            var actual = CreateFormatter()
                .MinExpanderDepth(4)
                .MaxExpanderDepth(5)
                .Stringify(section.Children);
            Assert.That(actual, Is.EqualTo(
                "[[[[" +
                "java.lang.AssertionError" +
                "Expected '1' but was '0'" +
                "Stack Trace" +
                "[" +
                "  utils.Helper.DoHelpfulThing (Helper.java:5)" +
                "dr.Class1.TestName (Class1.java:5)" +
                "]" +
                "]]]]"));
        }
    }
}