using JetBrains.Annotations;

namespace DafAggregator.Import
{
    public static class NullableInt32
    {
        public static int? TryParse([CanBeNull] string value)
        {
            if (int.TryParse(value, out var result))
                return result;
            return null;
        }
    }
}