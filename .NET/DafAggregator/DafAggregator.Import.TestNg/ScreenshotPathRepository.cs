﻿using System.Collections.Generic;
using System.Linq;

namespace DafAggregator.Import
{
    public class ScreenshotPathRepository
    {
        private readonly Dictionary<string, string> _pathsByFullMethodCall = new Dictionary<string, string>();

        public void Add(string className, string methodName, string parameterList, string screenshotPath)
        {
            var fullMethodCall = $"{className}.{methodName}({parameterList})";
            _pathsByFullMethodCall[fullMethodCall] = screenshotPath;
        }
        private string EncodeParameter(string parameter)
        {
            const string quote = "\"";
            const string backslash = "\\";
            var escapedParameter = parameter
                .Replace(backslash, backslash + backslash)
                .Replace(quote, backslash + quote);
            return quote + escapedParameter + quote;
        }
        public string GetPath(string className, string methodName, IEnumerable<string> parameters)
        {
            var parameterList = string.Join(", ", parameters.Select(EncodeParameter));
            var fullMethodCall = $"{className}.{methodName}({parameterList})";
            return _pathsByFullMethodCall.TryGetValue(fullMethodCall, out var result) ? result : "";
        }
    }
}