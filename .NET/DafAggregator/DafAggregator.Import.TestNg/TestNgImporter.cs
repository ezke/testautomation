﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace DafAggregator.Import
{
    public static class TestNgImporter
    {
        public static IEnumerable<Test> ImportTests(XElement root)
        {
            var screenshotPathRepository = LoadScreenshotPaths(root);

            return
                from classElement in root.Descendants("class")
                let className = classElement.Attribute("name")?.Value ?? ""
                from testMethodElement in classElement.Elements("test-method")
                from testMethod in ParseTestMethodElement(className, testMethodElement, screenshotPathRepository)
                where testMethod != null
                select testMethod;
        }

        private static FailureLevel GetFailureLevel(string exceptionType)
        {
            switch (exceptionType)
            {
                case "java.lang.ArrayIndexOutOfBoundsException":
                case "java.lang.IllegalArgumentException":
                case "java.lang.IndexOutOfBoundsException":
                case "java.lang.NullPointerException":
                case "utils.BustedTestException":
                    return FailureLevel.Busted;

                default:
                    return FailureLevel.Fail;
            }
        }
        private static ScreenshotPathRepository LoadScreenshotPaths(XElement root)
        {
            var screenshotPathRepository = new ScreenshotPathRepository();
            foreach (var lineElement in root.Elements("reporter-output").Elements("line"))
            {
                var match = Regex.Match(lineElement.Value,
                    @"^Screenshot: path=(?<path>.*); class=(?<className>.*); method=(?<methodName>.*)\((?<argList>.*)\)$");
                if (!match.Success)
                    continue;

                var path = match.Groups["path"].Value;
                var className = match.Groups["className"].Value;
                var methodName = match.Groups["methodName"].Value;
                var argList = match.Groups["argList"].Value;
                screenshotPathRepository.Add(className, methodName, argList, path);
            }
            return screenshotPathRepository;
        }
        private static IEnumerable<Test> ParseTestMethodElement(string className, XElement testMethodElement,
            ScreenshotPathRepository screenshotPathRepository)
        {
            // Skip setup and teardown methods
            var isConfig = testMethodElement.Attribute("is-config")?.Value;
            if (isConfig == "true")
                return Enumerable.Empty<Test>();

            // Skip tests that explicitly threw SkipException
            var failure = ParseExceptionElement(testMethodElement.Element("exception"));
            if (failure?.ExceptionType == "org.testng.SkipException")
                return Enumerable.Empty<Test>();

            // Otherwise, we show up as a Test in the output
            var methodName = testMethodElement.Attribute("name")?.Value ?? "";
            var parameters = ParseParamsElement(testMethodElement.Element("params")).ToList();
            var testMethod = new TestMethod(className, methodName, parameters);
            var screenshotPath = screenshotPathRepository.GetPath(className, methodName, parameters);
            var testSteps = ParseTestStepsFromMethodName(methodName);
            return
                from testStep in testSteps
                select new Test(testMethod, testStep, failure, screenshotPath);
        }
        private static Failure ParseExceptionElement(XElement exceptionElement)
        {
            if (exceptionElement == null)
                return null;

            var exceptionType = exceptionElement.Attribute("class")?.Value ?? "";
            var message = exceptionElement.Element("message")?.Value ?? "";
            var rawStackTrace = exceptionElement.Element("full-stacktrace")?.Value ?? "";
            var stackTrace = ParseStackTrace(rawStackTrace, exceptionType, message);
            var failureLevel = GetFailureLevel(exceptionType);
            return new Failure(exceptionType, message, stackTrace, failureLevel);
        }
        private static IEnumerable<StackTraceEntry> ParseStackTrace(string rawStackTrace, string exceptionType,
            string exceptionMessage)
        {
            var lines = rawStackTrace.Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries).ToList();

            var firstLineToIgnore = string.IsNullOrEmpty(exceptionMessage)
                ? exceptionType
                : exceptionType + ": " + exceptionMessage;
            if (lines.FirstOrDefault() == firstLineToIgnore)
                lines.RemoveAt(0);

            var stackTraceEntries = lines.Select(ParseStackTraceLine).ToList();
            return TrimTrailingBoilerplate(stackTraceEntries);
        }
        private static IEnumerable<StackTraceEntry> TrimTrailingBoilerplate(IReadOnlyList<StackTraceEntry> entries)
        {
            var trimmedEntries = entries
                .Reverse()
                .SkipWhile(IsTrailingBoilerplate)
                .Reverse()
                .ToList();

            // If, say, TestNG blew up before it ever got to our code, then return the original stack trace, not empty
            return trimmedEntries.Any() ? trimmedEntries : entries;
        }
        private static bool IsTrailingBoilerplate(StackTraceEntry entry)
        {
            var className = entry.ClassName;
            return className.StartsWith("org.testng.") ||
                   className.StartsWith("java.lang.reflect.") ||
                   className.StartsWith("sun.reflect.");
        }
        private static StackTraceEntry ParseStackTraceLine(string line)
        {
            // Example line: "<tab>at utils.BaseUI.log_AndFail(BaseUI.java:2458)"
            // Class group = "utils.BaseUI" - greedy match up to (not including) last '.' before first '('
            // Method group = "log_AndFail" - last word before first '('
            // Paren group = "(BaseUI.java:2458)" - everything from first '(' on
            var match = Regex.Match(line, @"^\s+at ((?<Class>[^(]*)\.)?(?<Method>[^(]*?)\s*(?<Paren>\(.*)?$");
            if (match.Success)
            {
                return new StackTraceEntry(
                    className: match.Groups["Class"].Value,
                    methodName: match.Groups["Method"].Value,
                    text: match.Groups["Paren"].Value);
            }

            return new StackTraceEntry(text: line);
        }
        private static IEnumerable<string> ParseParamsElement(XElement paramsElement)
        {
            if (paramsElement == null)
                return Enumerable.Empty<string>();

            var paramValues =
                from paramElement in paramsElement.Elements("param")
                let index = NullableInt32.TryParse(paramElement.Attribute("index")?.Value)
                orderby index
                from valueElement in paramElement.Elements("value")
                select valueElement.Value;

            return paramValues;
        }
        private static IEnumerable<TestStep> ParseTestStepsFromMethodName(string methodName)
        {
            var testSteps = new List<TestStep>();

            foreach (var segment in methodName.Split('_'))
            {
                if (segment.StartsWith("PT") && long.TryParse(segment.Substring(2), out var id))
                {
                    // Add the test case to the list. No steps yet.
                    testSteps.Add(new TestStep(id));
                }
                else if (testSteps.Any())
                {
                    var stepRange = TryParseStepRange(segment);
                    if (stepRange != null)
                    {
                        var priorStep = testSteps.Last();
                        var testCaseId = priorStep.TestCaseId;

                        // If the last item in the list doesn't have a step range yet, remove it from the list,
                        // so the item we're about to add (complete with step range) can basically replace it.
                        if (!priorStep.HasSteps)
                            testSteps.RemoveAt(testSteps.Count - 1);

                        testSteps.Add(new TestStep(testCaseId, stepRange.Item1, stepRange.Item2));
                    }
                }
            }

            if (!testSteps.Any())
                testSteps.Add(new TestStep(0));

            return testSteps;
        }
        private static Tuple<int, int> TryParseStepRange(string segment)
        {
            var match = Regex.Match(segment, @"^Steps?(?<FromStep>\d+)(To(?<ToStep>\d+))?$", RegexOptions.IgnoreCase);
            if (!match.Success)
                return null;

            var fromString = match.Groups["FromStep"].Value;
            var toString = match.Groups["ToStep"].Value;
            if (string.IsNullOrEmpty(toString))
                toString = fromString;

            return int.TryParse(fromString, out var from) && int.TryParse(toString, out var to)
                ? Tuple.Create(from, to)
                : null;
        }
    }
}