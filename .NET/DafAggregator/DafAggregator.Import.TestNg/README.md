﻿# Schema

This appears to be the structure of TestNG's test-output/testng-results.xml:

    <testng-results>
      <reporter-output>
        <line> - zero or more
          raw text
      <suite>
        <groups>
          <group> - (zero? one?) or more
            <method> - (zero? one?) or more
        <test>
          <class> - (zero? one?) or more
            <test-method> - (zero? one?) or more
              <params> - optional
                <param> - (zero? one?) or more
                  <value>
                    raw text
              <exception> - optional
                <message> - optional
                  raw text
                <full-stacktrace>
                  raw text
              <reporter-output>
                <line> - zero or more
                  raw text

## class element
* Attributes
  * `name="dRewards.tests_regression.Sweepstakes_UpcomingSweepstakes_Tests"` - always present, text
* Child elements
  * `<test-method>` - (zero? one?) or more

## exception element
* Attributes
  * `class="java.lang.AssertionError"` - always present, text
* Child elements
  * `<message>` - single, optional
  * `<full-stacktrace>` - single, always present

## full-stacktrace element
* No attributes
* Contains raw text (no child elements)

## group element
* Attributes
  * `name="critical_Tests"` - always present, text (definitely allows word characters)
* Child elements
  * `<method>` - (zero? one?) or more

## groups element
* No attributes
* Child elements
  * `<group>` - (zero? one?) or more

## line element
* No attributes
* Contains raw text (no child elements)

## message element
* No attributes
* Contains raw text (no child elements)

## method element
* Attributes
  * `class="dRewards.tests_regression.Auctions_MicroAuctions_DualCurrency"` - always present, text
  * `name="PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_1499Points"` - always present, text
  * `signature="Auctions_MicroAuctions_DualCurrency.PT4052_Step2To7_Verify_Auctions_MicroAuctions_UsersWith_1499Points()[pri:100020020, instance:dRewards.tests_regression.Auctions_MicroAuctions_DualCurrency@6108b2d7]"` - always present, text
* No child elements

## param element
* Attributes
  * `index="0"` - always present, always numeric
* Child elements
  * `<value>` - single, always present

## params element
* No attributes
* Child elements
  * `<param>` - (zero? one?) or more

## reporter-output element
* No attributes
* Child elements
  * `<line>` - zero or more

## suite element
* Attributes
  * `duration-ms="0"` - always present, always numeric
  * `finished-at="2018-01-29T23:20:06Z"` - always present, always a UTC date, formatted as shown
  * `name="DRewards-Regression-RC-Chrome-AARP"` - always present, text (definitely allows word characters and hyphens)
  * `started-at="2018-01-29T23:20:06Z"` - always present, always a UTC date, formatted as shown
* Child elements
  * `<groups>` - single, always present
  * `<test>` - single, always present

## test element
* Attributes
  * `duration-ms="0"` - always present, always numeric
  * `finished-at="2018-01-29T23:20:06Z"` - always present, always a UTC date, formatted as shown
  * `name="Test"` - always present, text (always seems to be "Test", probably because of the XML command file we're passing to TestNG)
  * `started-at="2018-01-29T23:20:06Z"` - always present, always a UTC date, formatted as shown
* Child elements
  * `<class>` - (zero? one?) or more

## test-method element
* Attributes
  * `duration-ms="0"` - always present, always numeric
  * `finished-at="2018-01-29T23:20:06Z"` - always present, always a UTC date, formatted as shown
  * `name="PT30_Step_UpcomingSweepstakes_Verify_Breadcrumb"` - always present, text
  * `started-at="2018-01-29T23:20:06Z"` - always present, always a UTC date, formatted as shown
  * `is-config="true"` - optional, always "true" when present ("true" means it's a setup or teardown method, not a test method)
  * `data-provider="e-Gift Cards"` - optional, text
  * `depends-on-methods="dRewards.tests_regression.DailyDeals_Checkout_3.PT3951_DailyDeal_Setup_SecondRun_ShoppingCart_Preexisting_ShippingAndBilling_Info"` - optional, text (not sure what happens if it depends on multiple methods)
* Child elements
  * `<params>` - single, optional
  * `<exception>` - single, optional
  * `<reporter-output>` - single, always present

## testng-results element
* Attributes
  * `failed="0"` - always present, always numeric
  * `ignored="0"` - always present, always numeric
  * `passed="0"` - always present, always numeric
  * `skipped="0"` - always present, always numeric
  * `total="0"` - always present, always numeric
* Child elements
  * `<reporter-output>` - single, always present
  * `<suite>` - single, always present

## value element
* No attributes
* Contains raw text (no child elements)
