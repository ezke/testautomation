﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestStatusSetTestsForParameterValidation
    {
        [SetUp]
        public void SetUp()
        {
            Values = Enumerable.Empty<TestStatus>();
        }

        private IEnumerable<TestStatus> Values { get; set; }

        private void CreateTestStatusSet()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new TestStatusSet(Values);
        }

        [Test]
        public void SucceedsWithDefaults()
        {
            Assert.DoesNotThrow(CreateTestStatusSet);
        }
        [Test]
        public void SucceedsWithValues()
        {
            Values = new[] {TestStatus.Pass, TestStatus.Busted};
            Assert.DoesNotThrow(CreateTestStatusSet);
        }
        [Test]
        public void IfValuesIsNull_Throws()
        {
            Values = null;
            Assert.Throws<ArgumentNullException>(CreateTestStatusSet);
        }
    }
}