﻿using System;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestCaseNodeTestsForParameterValidation
    {
        [SetUp]
        public void SetUp()
        {
            Statuses = TestStatusSet.Empty;
        }

        private TestStatusSet Statuses { get; set; }

        private void CreateTestCaseNode()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new TestCaseNode(0, Statuses);
        }

        [Test]
        public void SucceedsWithDefaults()
        {
            Assert.DoesNotThrow(CreateTestCaseNode);
        }
        [Test]
        public void IfStatusesIsNull_Throws()
        {
            Statuses = null;
            Assert.Throws<ArgumentNullException>(CreateTestCaseNode);
        }
    }
}