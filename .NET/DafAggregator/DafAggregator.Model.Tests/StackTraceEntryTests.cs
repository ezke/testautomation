﻿using System;
using NUnit.Framework;

namespace DafAggregator
{
    public class StackTraceEntryTests
    {
        [SetUp]
        public void SetUp()
        {
            ClassName = "";
            MethodName = "";
            Text = "";
        }

        private string ClassName { get; set; }
        private string MethodName { get; set; }
        private string Text { get; set; }

        private StackTraceEntry CreateEntry()
        {
            return new StackTraceEntry(ClassName, MethodName, Text);
        }

        [Test]
        public void ConstructedWithDefaults_Succeeds()
        {
            Assert.DoesNotThrow(() => CreateEntry());
        }
        [Test]
        public void StoresConstructorValues()
        {
            ClassName = "MyClassName";
            MethodName = "MyMethodName";
            Text = "MyText";
            var entry = CreateEntry();
            Assert.That(entry.ClassName, Is.EqualTo("MyClassName"));
            Assert.That(entry.MethodName, Is.EqualTo("MyMethodName"));
            Assert.That(entry.Text, Is.EqualTo("MyText"));
        }
        [Test]
        public void ConstructedWithNullClassName_Throws()
        {
            ClassName = null;
            Assert.Throws<ArgumentNullException>(() => CreateEntry());
        }
        [Test]
        public void ConstructedWithNullMethodName_Throws()
        {
            MethodName = null;
            Assert.Throws<ArgumentNullException>(() => CreateEntry());
        }
        [Test]
        public void ConstructedWithNullText_Throws()
        {
            Text = null;
            Assert.Throws<ArgumentNullException>(() => CreateEntry());
        }
    }
}