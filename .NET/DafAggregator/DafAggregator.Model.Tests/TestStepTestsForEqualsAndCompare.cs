﻿using System;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace DafAggregator
{
    public class TestStepTestsForEqualsAndCompare
    {
        private static readonly Random Random = new Random();

        [SetUp]
        public void SetUp()
        {
            var testCaseId = 10000 + Random.Next(89999);
            var firstStep = Random.Next(10);
            var lastStep = firstStep + Random.Next(10);

            LeftTestCaseId = testCaseId;
            LeftFirstStep = firstStep;
            LeftLastStep = lastStep;
            Right = new TestStep(testCaseId, firstStep, lastStep);
        }

        private long LeftTestCaseId { get; set; }
        private int LeftFirstStep { get; set; }
        private int LeftLastStep { get; set; }
        private TestStep Right { get; set; }

        private void AssertCompare(IResolveConstraint satisfiesCondition)
        {
            var left = new TestStep(LeftTestCaseId, LeftFirstStep, LeftLastStep);
            var actual = left.CompareTo(Right);
            Assert.That(actual, satisfiesCondition, $"<{left}>.CompareTo(<{Right}>)");
        }
        private void AssertEquals(IResolveConstraint satisfiesCondition)
        {
            var left = new TestStep(LeftTestCaseId, LeftFirstStep, LeftLastStep);
            var actual = left.Equals(Right);
            Assert.That(actual, satisfiesCondition, $"<{left}> == <{Right}>");
        }

        [Test]
        public void Equals_WhenAllPropertiesAreEqual_IsTrue()
        {
            AssertEquals(Is.True);
        }
        [Test]
        public void Equals_WhenIdIsDifferent_IsFalse()
        {
            LeftTestCaseId = Right.TestCaseId + 1;
            AssertEquals(Is.False);
        }
        [Test]
        public void Equals_WhenFirstStepIsDifferent_IsFalse()
        {
            LeftFirstStep = Right.FirstStep + 1;
            AssertEquals(Is.False);
        }
        [Test]
        public void Equals_WhenLastStepIsDifferent_IsFalse()
        {
            LeftLastStep = Right.LastStep + 1;
            AssertEquals(Is.False);
        }
        [Test]
        public void Compare_WhenAllPropertiesAreEqual_IsZero()
        {
            AssertCompare(Is.EqualTo(0));
        }
        [Test]
        public void Compare_WhenLeftIdIsLess_IsLess()
        {
            LeftTestCaseId = Right.TestCaseId - 1;
            AssertCompare(Is.EqualTo(-1));
        }
        [Test]
        public void Compare_WhenLeftIdIsGreater_IsGreater()
        {
            LeftTestCaseId = Right.TestCaseId + 1;
            AssertCompare(Is.EqualTo(1));
        }
        [Test]
        public void Compare_WhenLeftFirstStepIsLess_IsLess()
        {
            LeftFirstStep = Right.FirstStep - 1;
            AssertCompare(Is.EqualTo(-1));
        }
        [Test]
        public void Compare_WhenLeftFirstStepIsGreater_IsGreater()
        {
            LeftFirstStep = Right.FirstStep + 1;
            AssertCompare(Is.EqualTo(1));
        }
        [Test]
        public void Compare_WhenLeftLastStepIsLess_IsLess()
        {
            LeftLastStep = Right.LastStep - 1;
            AssertCompare(Is.EqualTo(-1));
        }
        [Test]
        public void Compare_WhenLeftLastStepIsGreater_IsGreater()
        {
            LeftLastStep = Right.LastStep + 1;
            AssertCompare(Is.EqualTo(1));
        }
        [Test]
        public void Compare_IdBeatsFirstStep()
        {
            LeftTestCaseId = Right.TestCaseId + 1;
            LeftFirstStep = Right.FirstStep - 1;
            AssertCompare(Is.EqualTo(1));
        }
        [Test]
        public void Compare_FirstStepBeatsLastStep()
        {
            LeftFirstStep = Right.FirstStep + 1;
            LeftLastStep = Right.LastStep - 1;
            AssertCompare(Is.EqualTo(1));
        }
    }
}