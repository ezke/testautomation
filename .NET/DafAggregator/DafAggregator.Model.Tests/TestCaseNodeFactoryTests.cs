﻿using System.Collections.Generic;
using System.Linq;
using DafAggregator.Builders;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestCaseNodeFactoryTests
    {
        private static IEnumerable<TestCaseNode> Execute(IEnumerable<Test> tests)
        {
            return TestCaseNodeFactory.CreateTestCaseNodes(tests);
        }
        private string StringifyTestCaseIds(IEnumerable<TestCaseNode> testCaseNodes)
        {
            return string.Join(",", testCaseNodes.Select(testCaseNode => testCaseNode.Id));
        }

        [Test]
        public void GivenEmptyList_ReturnsEmptyList()
        {
            var tests = new Test[] { };
            var testCaseNodes = Execute(tests);
            Assert.That(testCaseNodes, Is.Empty);
        }
        [Test]
        public void CreatesOneTestCaseNodePerTestCaseId()
        {
            var tests = new[]
            {
                new TestBuilder().WithTestCaseId(123).Build(),
                new TestBuilder().WithTestCaseId(234).Build(),
                new TestBuilder().WithTestCaseId(123).Build(),
                new TestBuilder().WithTestCaseAndSteps(123, 9).Build(),
                new TestBuilder().WithTestCaseAndSteps(123, 9, 99).Build(),
            };
            var testCaseNodes = Execute(tests);
            Assert.That(StringifyTestCaseIds(testCaseNodes), Is.EqualTo("123,234"));
        }
        [Test]
        public void TestCaseNodesAreSorted()
        {
            var tests = new[]
            {
                new TestBuilder().WithTestCaseId(3).Build(),
                new TestBuilder().WithTestCaseId(1).Build(),
                new TestBuilder().WithTestCaseId(2).Build(),
            };
            var testCaseNodes = Execute(tests);
            Assert.That(StringifyTestCaseIds(testCaseNodes), Is.EqualTo("1,2,3"));
        }
    }
}