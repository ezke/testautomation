﻿using NUnit.Framework;

namespace DafAggregator
{
    public class TestStepTests
    {
        [Test]
        public void GivenOneParameter_SetsId_LeavesStepsAsZero()
        {
            var testStep = new TestStep(123);
            Assert.That(testStep.TestCaseId, Is.EqualTo(123), "Id");
            Assert.That(testStep.FirstStep, Is.EqualTo(0), "FirstStep");
            Assert.That(testStep.LastStep, Is.EqualTo(0), "LastStep");
        }
        [Test]
        public void GivenTwoParameters_SetsIdAndBothSteps()
        {
            var testStep = new TestStep(123, 5);
            Assert.That(testStep.TestCaseId, Is.EqualTo(123), "Id");
            Assert.That(testStep.FirstStep, Is.EqualTo(5), "FirstStep");
            Assert.That(testStep.LastStep, Is.EqualTo(5), "LastStep");
        }
        [Test]
        public void GivenThreeParameters_SetsIdAndSteps()
        {
            var testStep = new TestStep(123, 5, 12);
            Assert.That(testStep.TestCaseId, Is.EqualTo(123), "Id");
            Assert.That(testStep.FirstStep, Is.EqualTo(5), "FirstStep");
            Assert.That(testStep.LastStep, Is.EqualTo(12), "LastStep");
        }
        [Test]
        public void HasSteps_IfNoStepsSpecified_ReturnsFalse()
        {
            var testStep = new TestStep(123);
            Assert.That(testStep.HasSteps, Is.False);
        }
        [Test]
        public void HasSteps_IfStepNumbersAreZero_ReturnsFalse()
        {
            var testStep = new TestStep(123, 0, 0);
            Assert.That(testStep.HasSteps, Is.False);
        }
        [Test]
        public void HasSteps_IfStepNumbersAreNonzero_ReturnsTrue()
        {
            var testStep = new TestStep(123, 1, 1);
            Assert.That(testStep.HasSteps, Is.True);
        }
        [Test]
        public void ToString_IfNoStepsSpecified_IncludesOnlyTestCaseId()
        {
            var testStep = new TestStep(123);
            Assert.That(testStep.ToString(), Is.EqualTo("123"));
        }
        [Test]
        public void ToString_IfStepNumbersAreZero_IncludesOnlyTestCaseId()
        {
            var testStep = new TestStep(123, 0, 0);
            Assert.That(testStep.ToString(), Is.EqualTo("123"));
        }
        [Test]
        public void ToString_IfCreatedWithOneStepNumber_IncludesStepNumber()
        {
            var testStep = new TestStep(123, 5);
            Assert.That(testStep.ToString(), Is.EqualTo("123:5"));
        }
        [Test]
        public void ToString_IfStepsDoNotIndicateARange_IncludesStepNumber()
        {
            var testStep = new TestStep(123, 5, 5);
            Assert.That(testStep.ToString(), Is.EqualTo("123:5"));
        }
        [Test]
        public void ToString_IfStepsIndicateARange_IncludesStepRange()
        {
            var testStep = new TestStep(123, 5, 12);
            Assert.That(testStep.ToString(), Is.EqualTo("123:5-12"));
        }
    }
}