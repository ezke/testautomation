﻿using DafAggregator.Builders;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestTests
    {
        [Test]
        public void Status_GivenNoFailure_IsPass()
        {
            var test = new TestBuilder()
                .WithFailure(null)
                .Build();
            Assert.That(test.Status, Is.EqualTo(TestStatus.Pass));
        }
        [Test]
        public void Status_GivenFailureWithLevelFail_IsFail()
        {
            var test = new TestBuilder()
                .WithFailure(new FailureBuilder(FailureLevel.Fail))
                .Build();
            Assert.That(test.Status, Is.EqualTo(TestStatus.Fail));
        }
        [Test]
        public void Status_GivenFailureWithLevelBusted_IsBusted()
        {
            var test = new TestBuilder()
                .WithFailure(new FailureBuilder(FailureLevel.Busted))
                .Build();
            Assert.That(test.Status, Is.EqualTo(TestStatus.Busted));
        }
    }
}