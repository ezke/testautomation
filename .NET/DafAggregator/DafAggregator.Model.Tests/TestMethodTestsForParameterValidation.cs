﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestMethodTestsForParameterValidation
    {
        [SetUp]
        public void SetUp()
        {
            ClassName = "";
            MethodName = "";
            Parameters = Enumerable.Empty<string>();
        }

        private string ClassName { get; set; }
        private string MethodName { get; set; }
        private IEnumerable<string> Parameters { get; set; }

        private void CreateTestMethodObject()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new TestMethod(ClassName, MethodName, Parameters);
        }

        [Test]
        public void SucceedsWithDefaults()
        {
            Assert.DoesNotThrow(CreateTestMethodObject);
        }
        [Test]
        public void SucceedsWithStringParameter()
        {
            Parameters = new[] {"Param1"};
            Assert.DoesNotThrow(CreateTestMethodObject);
        }
        [Test]
        public void IfClassNameIsNull_Throws()
        {
            ClassName = null;
            Assert.Throws<ArgumentNullException>(CreateTestMethodObject);
        }
        [Test]
        public void IfMethodNameIsNull_Throws()
        {
            MethodName = null;
            Assert.Throws<ArgumentNullException>(CreateTestMethodObject);
        }
        [Test]
        public void IfParametersIsNull_Throws()
        {
            Parameters = null;
            Assert.Throws<ArgumentNullException>(CreateTestMethodObject);
        }
        [Test]
        public void IfParametersContainsNull_Throws()
        {
            Parameters = new[] {"foo", null, "bar"};
            Assert.Throws<ArgumentException>(CreateTestMethodObject);
        }
    }
}