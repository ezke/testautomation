﻿using System;
using System.Linq;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestTestsForParameterValidation
    {
        [SetUp]
        public void SetUp()
        {
            TestMethod = new TestMethod("", "", Enumerable.Empty<string>());
            Failure = new Failure("", "", Enumerable.Empty<StackTraceEntry>(), FailureLevel.Fail);
            ScreenshotPath = "";
            TestStep = new TestStep(0);
        }

        private TestMethod TestMethod { get; set; }
        private Failure Failure { get; set; }
        private string ScreenshotPath { get; set; }
        private TestStep TestStep { get; set; }

        private void CreateTestObject()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Test(TestMethod, TestStep, Failure, ScreenshotPath);
        }

        [Test]
        public void SucceedsWithDefaults()
        {
            Assert.DoesNotThrow(CreateTestObject);
        }
        [Test]
        public void IfTestMethodIsNull_Throws()
        {
            TestMethod = null;
            Assert.Throws<ArgumentNullException>(CreateTestObject);
        }
        [Test]
        public void IfFailureIsNull_NoError()
        {
            Failure = null;
            Assert.DoesNotThrow(CreateTestObject);
        }
        [Test]
        public void IfTestStepIsNull_Throws()
        {
            TestStep = null;
            Assert.Throws<ArgumentNullException>(CreateTestObject);
        }
        [Test]
        public void IfScreenshotPathIsNull_Throws()
        {
            ScreenshotPath = null;
            Assert.Throws<ArgumentNullException>(CreateTestObject);
        }
    }
}