﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DafAggregator
{
    public class FailureTestsForParameterValidation
    {
        [SetUp]
        public void SetUp()
        {
            ExceptionType = "";
            Message = "";
            StackTrace = Enumerable.Empty<StackTraceEntry>();
        }

        private string ExceptionType { get; set; }
        private string Message { get; set; }
        private IEnumerable<StackTraceEntry> StackTrace { get; set; }

        private void CreateFailureObject()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Failure(ExceptionType, Message, StackTrace, FailureLevel.Fail);
        }

        [Test]
        public void SucceedsWithDefaults()
        {
            Assert.DoesNotThrow(CreateFailureObject);
        }
        [Test]
        public void IfExceptionTypeIsNull_Throws()
        {
            ExceptionType = null;
            Assert.Throws<ArgumentNullException>(CreateFailureObject);
        }
        [Test]
        public void IfMessageIsNull_Throws()
        {
            Message = null;
            Assert.Throws<ArgumentNullException>(CreateFailureObject);
        }
        [Test]
        public void IfStackTraceIsNull_Throws()
        {
            StackTrace = null;
            Assert.Throws<ArgumentNullException>(CreateFailureObject);
        }
        [Test]
        public void IfStackTraceContainsNull_Throws()
        {
            StackTrace = new StackTraceEntry[] {null};
            Assert.Throws<ArgumentException>(CreateFailureObject);
        }
    }
}