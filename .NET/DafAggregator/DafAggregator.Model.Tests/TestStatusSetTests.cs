﻿using System;
using System.Linq;
using NUnit.Framework;

namespace DafAggregator
{
    public class TestStatusSetTests
    {
        [Test]
        public void Enumerable_GivenNoValues_IsEmpty()
        {
            var set = new TestStatusSet(new TestStatus[] { });
            Assert.That(set, Is.Empty);
        }
        [Test]
        public void Enumerable_RemovesDuplicates()
        {
            var set = new TestStatusSet(new[] {TestStatus.Fail, TestStatus.Fail, TestStatus.Fail});
            Assert.That(set, Is.EqualTo(new[] {TestStatus.Fail}));
        }
        [Test]
        public void Enumerable_ReturnsValuesInOrder()
        {
            var set = new TestStatusSet(new[] {TestStatus.Pass, TestStatus.Busted, TestStatus.Fail});
            Assert.That(set, Is.EqualTo(new[] {TestStatus.Pass, TestStatus.Fail, TestStatus.Busted}));
        }
        [Test]
        public void Union_ReturnsUnion()
        {
            var set1 = new TestStatusSet(new[] {TestStatus.Pass});
            var set2 = new TestStatusSet(Enumerable.Empty<TestStatus>());
            var set3 = new TestStatusSet(new[] {TestStatus.Busted});

            var combinedSet = TestStatusSet.Union(new[] {set1, set2, set3});
            Assert.That(combinedSet, Is.EqualTo(new[] {TestStatus.Pass, TestStatus.Busted}));
        }
        [Test]
        public void Union_GivenNull_Throws()
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => TestStatusSet.Union(null));
        }
        [Test]
        public void Union_GivenListContainingNull_Throws()
        {
            var set1 = new TestStatusSet(new[] {TestStatus.Pass});
            Assert.Throws<ArgumentException>(() => TestStatusSet.Union(new[] {set1, null}));
        }
        [Test]
        public void WorstStatus_GivenNoValues_IsPass()
        {
            var set = new TestStatusSet(new TestStatus[] { });
            Assert.That(set.WorstStatus, Is.EqualTo(TestStatus.Pass));
        }
        [Test]
        public void WorstStatus_WhenValuesIncludeOnlyPass_IsPass()
        {
            var set = new TestStatusSet(new[] {TestStatus.Pass, TestStatus.Pass});
            Assert.That(set.WorstStatus, Is.EqualTo(TestStatus.Pass));
        }
        [Test]
        public void WorstStatus_WhenValuesIncludeFail_IsFail()
        {
            var set = new TestStatusSet(new[] {TestStatus.Pass, TestStatus.Fail, TestStatus.Pass});
            Assert.That(set.WorstStatus, Is.EqualTo(TestStatus.Fail));
        }
        [Test]
        public void WorstStatus_WhenValuesIncludeBusted_IsBusted()
        {
            var set = new TestStatusSet(new[] {TestStatus.Pass, TestStatus.Fail, TestStatus.Busted, TestStatus.Pass});
            Assert.That(set.WorstStatus, Is.EqualTo(TestStatus.Busted));
        }
    }
}