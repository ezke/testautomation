﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using NUnit.Framework;

namespace DafAggregator.Import.TestNg.Tests
{
    public class TestNgImporterTests
    {
        private XElement _rootElement;
        private XElement _testElement;

        [SetUp]
        public void SetUp()
        {
            _rootElement = new XElement("testng-results",
                new XElement("suite",
                    _testElement = new XElement("test")));
        }

        private void AddClassElement(params object[] content)
        {
            _testElement.Add(new XElement("class", content));
        }
        private void AddGlobalReporterOutputLine(object content)
        {
            GetCurrentGlobalReporterOutputElement().Add(new XElement("line", content));
        }
        private void AddTestMethodElement(params object[] content)
        {
            var testMethodElement = new XElement("test-method", content);
            GetCurrentClassElement().Add(testMethodElement);
        }
        private void AddTestMethodElementWithStackTraceLines(params string[] stackTraceLines)
        {
            var fullStackTrace = string.Join("\r\n", stackTraceLines);
            AddTestMethodElement(
                new XElement("exception",
                    new XElement("full-stacktrace", fullStackTrace)));
        }
        private XElement GetCurrentElement(XElement parent, string name)
        {
            var element = parent.Elements(name).LastOrDefault();
            if (element == null)
            {
                element = new XElement(name);
                parent.Add(element);
            }
            return element;
        }
        private XElement GetCurrentClassElement()
        {
            return GetCurrentElement(_testElement, "class");
        }
        private XElement GetCurrentGlobalReporterOutputElement()
        {
            return GetCurrentElement(_rootElement, "reporter-output");
        }
        private Failure ImportSingleFailure()
        {
            var test = ImportSingleTest();
            Assert.That(test.Failure, Is.Not.Null, "Failure object");
            return test.Failure;
        }
        private Test ImportSingleTest()
        {
            var tests = ImportTests();
            Assert.That(tests.Count, Is.EqualTo(1), "Count of returned tests");
            return tests.First();
        }
        private IReadOnlyList<Test> ImportTests()
        {
            return TestNgImporter.ImportTests(_rootElement).ToList();
        }
        private string StringifyTestSteps(IEnumerable<Test> tests)
        {
            return string.Join(",", tests.Select(test => test.TestStep));
        }

        [Test]
        public void EmptyDocumentYieldsEmptyResults()
        {
            _rootElement = new XElement("testng-results");
            var tests = ImportTests();
            Assert.IsEmpty(tests);
        }
        [Test]
        public void OneTestMethod()
        {
            AddTestMethodElement();
            var tests = ImportTests();
            Assert.That(tests.Count, Is.EqualTo(1), "Count of returned tests");
        }
        [Test]
        public void TwoTestMethods()
        {
            AddTestMethodElement();
            AddTestMethodElement();
            var tests = ImportTests();
            Assert.That(tests.Count, Is.EqualTo(2), "Count of returned tests");
        }
        [Test]
        public void SetUpAndTearDownMethodsAreNotIncludedInOutput()
        {
            AddTestMethodElement(new XAttribute("name", "setup_method"), new XAttribute("is-config", "true"));
            AddTestMethodElement(new XAttribute("name", "FirstTest"));
            AddTestMethodElement(new XAttribute("name", "SecondTest"));
            AddTestMethodElement(new XAttribute("name", "teardown"), new XAttribute("is-config", "true"));
            var tests = ImportTests();
            Assert.That(string.Join(",", tests.Select(t => t.TestMethod.MethodName)),
                Is.EqualTo("FirstTest,SecondTest"));
        }
        [Test]
        public void ClassName()
        {
            AddClassElement(new XAttribute("name", "com.deluxe.daf.TestClass1"));
            AddTestMethodElement();
            var test = ImportSingleTest();
            Assert.That(test.TestMethod.ClassName, Is.EqualTo("com.deluxe.daf.TestClass1"));
        }
        [Test]
        public void MethodName()
        {
            AddTestMethodElement(new XAttribute("name", "MyTestName"));
            var test = ImportSingleTest();
            Assert.That(test.TestMethod.MethodName, Is.EqualTo("MyTestName"));
        }
        [Test]
        public void MultipleClasses_MultipleTests()
        {
            AddClassElement(new XAttribute("name", "1"));
            AddTestMethodElement(new XAttribute("name", "1.1"));
            AddTestMethodElement(new XAttribute("name", "1.2"));
            AddClassElement(new XAttribute("name", "2"));
            AddTestMethodElement(new XAttribute("name", "2.1"));
            AddTestMethodElement(new XAttribute("name", "2.2"));

            var tests = ImportTests();
            Assert.That(
                string.Join(", ", tests.Select(t => t.TestMethod.MethodName)),
                Is.EqualTo("1.1, 1.2, 2.1, 2.2"),
                "Test method names");
            Assert.That(
                string.Join(", ", tests.Select(t => t.TestMethod.ClassName)),
                Is.EqualTo("1, 1, 2, 2"),
                "Test class names");
        }
        [Test]
        public void NoTestCases_YieldsTestCaseWithIdZero()
        {
            AddTestMethodElement(new XAttribute("name", "MyTestName"));
            var test = ImportSingleTest();
            Assert.That(test.TestStep.TestCaseId, Is.EqualTo(0), "Test case ID");
        }
        [Test]
        public void OneTestCase_NoSteps()
        {
            AddTestMethodElement(new XAttribute("name", "PT1234"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234"));
        }
        [Test]
        public void TwoTestCases_NoSteps_ReturnsTwoTests()
        {
            AddTestMethodElement(new XAttribute("name", "PT1234_PT2345"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234,2345"));
        }
        [Test]
        public void TestCaseId_CanBeGreaterThanFourBillion()
        {
            AddTestMethodElement(new XAttribute("name", "PT123456789012345"));
            var test = ImportSingleTest();
            Assert.That(test.TestStep.TestCaseId, Is.EqualTo(123_456_789_012_345));
        }
        [Test]
        public void OneTestCase_SingleStep()
        {
            AddTestMethodElement(new XAttribute("name", "PT1234_Step5"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234:5"));
        }
        [Test]
        public void OneTestCase_StepRange()
        {
            AddTestMethodElement(new XAttribute("name", "PT1234_Step5To8"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234:5-8"));
        }
        [Test]
        public void StepKeywordsAreCaseInsensitive()
        {
            AddTestMethodElement(new XAttribute("name", "PT1234_step5to8"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234:5-8"));
        }
        [Test]
        public void OneTestCase_StepRangePlural()
        {
            // Allow "Steps" in the test name instead of just "Step"
            AddTestMethodElement(new XAttribute("name", "PT1234_Steps5To8"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234:5-8"));
        }
        [Test]
        public void OneTestCase_MultipleSteps_ReturnsMultipleTests()
        {
            AddTestMethodElement(new XAttribute("name", "PT1234_Step1_Steps3To5_Step99"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("1234:1,1234:3-5,1234:99"));
        }
        [Test]
        public void MultipleTestCasesAndSteps_ReturnsMultipleTests()
        {
            AddTestMethodElement(new XAttribute("name", "PT100_PT200_Step1To3_Step9_PT300_Step4_PT400"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("100,200:1-3,200:9,300:4,400"));
        }
        [Test]
        public void StepsBeforeTestCase_AreIgnored_ButStepsAfterTestCase_AreIncluded()
        {
            AddTestMethodElement(new XAttribute("name", "Step1_Step2_PT100_Step3"));
            var tests = ImportTests();
            Assert.That(StringifyTestSteps(tests), Is.EqualTo("100:3"));
        }
        [Test]
        public void NoParameters_YieldsEmptyParameterCollection()
        {
            AddTestMethodElement();
            var test = ImportSingleTest();
            Assert.That(test.TestMethod.Parameters.Count, Is.EqualTo(0), "Count of returned parameters");
        }
        [Test]
        public void OneParameter()
        {
            AddTestMethodElement(
                new XElement("params",
                    new XElement("param",
                        new XElement("value", "FirstParameterValue"))));
            var test = ImportSingleTest();
            Assert.That(string.Join(",", test.TestMethod.Parameters), Is.EqualTo("FirstParameterValue"));
        }
        [Test]
        public void ParameterValue_CanBeCData()
        {
            AddTestMethodElement(
                new XElement("params",
                    new XElement("param",
                        new XElement("value", new XCData("FirstParameterValue")))));
            var test = ImportSingleTest();
            Assert.That(string.Join(",", test.TestMethod.Parameters), Is.EqualTo("FirstParameterValue"));
        }
        [Test]
        public void TwoParameters()
        {
            AddTestMethodElement(
                new XElement("params",
                    new XElement("param",
                        new XElement("value", "FirstParameterValue")),
                    new XElement("param",
                        new XElement("value", "SecondParameterValue"))));
            var test = ImportSingleTest();
            Assert.That(string.Join(",", test.TestMethod.Parameters),
                Is.EqualTo("FirstParameterValue,SecondParameterValue"));
        }
        [Test]
        public void ParametersAreSortedByIndex()
        {
            AddTestMethodElement(
                new XElement("params",
                    new XElement("param", new XAttribute("index", "0"),
                        new XElement("value", "FirstParameterValue")),
                    new XElement("param", new XAttribute("index", "2"),
                        new XElement("value", "ThirdParameterValue")),
                    new XElement("param", new XAttribute("index", "1"),
                        new XElement("value", "SecondParameterValue"))));
            var test = ImportSingleTest();
            Assert.That(string.Join(",", test.TestMethod.Parameters),
                Is.EqualTo("FirstParameterValue,SecondParameterValue,ThirdParameterValue"));
        }
        [Test]
        public void IfNoException_OutputTestWithNoFailure()
        {
            AddTestMethodElement();
            var test = ImportSingleTest();
            Assert.That(test.Failure, Is.Null, "Expected to find no failure");
        }
        [Test]
        public void IfSkipExceptionIsThrown_RemoveEntireTestFromOutput()
        {
            // Don't trust the test's "status" attribute: TestNG reports "SKIP" for test methods
            // that weren't run because a setup method failed, and we want to classify that as
            // failure. But if a test programmatically throws SkipException, it's skipped.

            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "org.testng.SkipException")));
            var tests = ImportTests();
            Assert.That(tests.Count, Is.EqualTo(0),
                "Test programmatically threw SkipException and should have been excluded from output");
        }
        [Test]
        public void IfAnyOtherExceptionIsThrown_OutputTestWithFailure()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.RuntimeException")));
            var test = ImportSingleTest();
            Assert.That(test.Failure, Is.Not.Null, "Failure object");
        }
        [Test]
        public void FailureLevel_IfExceptionTypeIsArrayIndexOutOfBoundsException_IsBusted()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.ArrayIndexOutOfBoundsException")));
            var failure = ImportSingleFailure();
            Assert.That(failure.FailureLevel, Is.EqualTo(FailureLevel.Busted));
        }
        [Test]
        public void FailureLevel_IfExceptionTypeIsIllegalArgumentException_IsBusted()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.IllegalArgumentException")));
            var failure = ImportSingleFailure();
            Assert.That(failure.FailureLevel, Is.EqualTo(FailureLevel.Busted));
        }
        [Test]
        public void FailureLevel_IfExceptionTypeIsIndexOutOfBoundsException_IsBusted()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.IndexOutOfBoundsException")));
            var failure = ImportSingleFailure();
            Assert.That(failure.FailureLevel, Is.EqualTo(FailureLevel.Busted));
        }
        [Test]
        public void FailureLevel_IfExceptionTypeIsNullPointerException_IsBusted()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.NullPointerException")));
            var failure = ImportSingleFailure();
            Assert.That(failure.FailureLevel, Is.EqualTo(FailureLevel.Busted));
        }
        [Test]
        public void FailureLevel_IfExceptionTypeIsSomethingElse_IsFail()
        {
            var exceptionClass = "java.lang." + Path.GetRandomFileName();
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", exceptionClass)));
            var failure = ImportSingleFailure();
            Assert.That(failure.FailureLevel, Is.EqualTo(FailureLevel.Fail), $"Exception class = {exceptionClass}");
        }
        [Test]
        public void ExceptionMessage()
        {
            AddTestMethodElement(
                new XElement("exception",
                    new XElement("message", "An error occurred")));
            var failure = ImportSingleFailure();
            Assert.That(failure.Message, Is.EqualTo("An error occurred"));
        }
        [Test]
        public void ExceptionMessage_CanBeCData()
        {
            AddTestMethodElement(
                new XElement("exception",
                    new XElement("message", new XCData("An error occurred"))));
            var failure = ImportSingleFailure();
            Assert.That(failure.Message, Is.EqualTo("An error occurred"));
        }
        [Test]
        public void StackTrace_LinesWithoutLeadingAt_AreStoredAsPlainText()
        {
            AddTestMethodElementWithStackTraceLines(
                "Line 1",
                "Line 2");
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(text: "Line 1"),
                new StackTraceEntry(text: "Line 2"),
            }));
        }
        [Test]
        public void StackTrace_LinesWithLeadingWhitespaceAt_AreParsed()
        {
            AddTestMethodElementWithStackTraceLines(
                "\tat MethodName",
                "\tat MethodName(FileName:LineNumber)",
                "\tat ClassName.MethodName",
                "    at ClassName.MethodName (FileName:LineNumber)");
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(methodName: "MethodName"),
                new StackTraceEntry(methodName: "MethodName", text: "(FileName:LineNumber)"),
                new StackTraceEntry(className: "ClassName", methodName: "MethodName"),
                new StackTraceEntry(className: "ClassName", methodName: "MethodName", text: "(FileName:LineNumber)"),
            }));
        }
        [Test]
        public void StackTrace_WhenExceptionDoesNotHaveAMessage_RemovesFirstLineIfItIsExceptionType()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.NullPointerException"),
                    new XElement("full-stacktrace", "java.lang.NullPointerException\r\nLine 1\r\nLine 2")));
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(text: "Line 1"),
                new StackTraceEntry(text: "Line 2"),
            }));
        }
        [Test]
        public void StackTrace_WhenExceptionHasAMessage_RemovesFirstLineIfItIsExceptionTypeAndMessage()
        {
            AddTestMethodElement(
                new XElement("exception", new XAttribute("class", "java.lang.IndexOutOfBoundsException"),
                    new XElement("message", "Index: 0, Size: 0"),
                    new XElement("full-stacktrace",
                        "java.lang.IndexOutOfBoundsException: Index: 0, Size: 0\r\nLine 1\r\nLine 2")));
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(text: "Line 1"),
                new StackTraceEntry(text: "Line 2"),
            }));
        }
        [Test]
        public void StackTrace_RemovesTestNgBoilerplateFromTheEnd()
        {
            AddTestMethodElementWithStackTraceLines(
                "\tat SomeMethodName",
                "\tat SomeOtherMethodName",
                // beginning of TestNG boilerplate
                "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
                "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
                "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
                "\tat java.lang.reflect.Method.invoke(Method.java:498)",
                "\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:108)",
                "\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:661)",
                "\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:869)",
                "\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1193)",
                "\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:126)",
                "\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:109)",
                "\tat org.testng.TestRunner.privateRun(TestRunner.java:744)",
                "\tat org.testng.TestRunner.run(TestRunner.java:602)",
                "\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:380)",
                "\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:375)",
                "\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:340)",
                "\tat org.testng.SuiteRunner.run(SuiteRunner.java:289)",
                "\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)",
                "\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)",
                "\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1301)",
                "\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1226)",
                "\tat org.testng.TestNG.runSuites(TestNG.java:1144)",
                "\tat org.testng.TestNG.run(TestNG.java:1115)",
                "\tat org.testng.TestNG.privateMain(TestNG.java:1442)",
                "\tat org.testng.TestNG.main(TestNG.java:1411)");
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(methodName: "SomeMethodName"),
                new StackTraceEntry(methodName: "SomeOtherMethodName"),
            }));
        }
        [Test]
        public void StackTrace_DoesNotRemoveTestNgBoilerplate_IfThatWouldMeanReturningAnEmptyList()
        {
            AddTestMethodElementWithStackTraceLines(
                "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
                "\tat java.lang.reflect.Method.invoke(Method.java:498)",
                "\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:108)");
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace.Count, Is.EqualTo(3), "Count of entries in the stack trace");
        }
        [Test]
        public void StackTrace_DoesNotRemoveTestNgOrReflectionBoilerplateFromTheMiddle()
        {
            AddTestMethodElementWithStackTraceLines(
                "\tat SomeMethodName",
                "\tat SomeOtherMethodName",
                "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
                "\tat java.lang.reflect.Method.invoke(Method.java:498)",
                "\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:661)",
                "\tat ThirdMethodName",
                "\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:661)");
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(methodName: "SomeMethodName"),
                new StackTraceEntry(methodName: "SomeOtherMethodName"),
                new StackTraceEntry(
                    className: "sun.reflect.NativeMethodAccessorImpl",
                    methodName: "invoke",
                    text: "(NativeMethodAccessorImpl.java:62)"),
                new StackTraceEntry(
                    className: "java.lang.reflect.Method",
                    methodName: "invoke",
                    text: "(Method.java:498)"),
                new StackTraceEntry(
                    className: "org.testng.internal.Invoker",
                    methodName: "invokeMethod",
                    text: "(Invoker.java:661)"),
                new StackTraceEntry(methodName: "ThirdMethodName"),
            }));
        }
        [Test]
        public void StackTrace_CanBeCData()
        {
            AddTestMethodElement(
                new XElement("exception",
                    new XElement("full-stacktrace", new XCData("Line 1\r\nLine 2"))));
            var failure = ImportSingleFailure();
            Assert.That(failure.StackTrace, Is.EqualTo(new[]
            {
                new StackTraceEntry(text: "Line 1"),
                new StackTraceEntry(text: "Line 2"),
            }));
        }
        [Test]
        public void ScreenshotPath_NoScreenshot()
        {
            AddTestMethodElement();
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(""), "Screenshot path");
        }
        [Test]
        public void ScreenshotPath()
        {
            AddGlobalReporterOutputLine(@"Screenshot: path=C:\Shot1.png; class=namespace.Class; method=TestMethod()");
            AddClassElement(new XAttribute("name", "namespace.Class"));
            AddTestMethodElement(new XAttribute("name", "TestMethod"));
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(@"C:\Shot1.png"));
        }
        [Test]
        public void ScreenshotPath_IgnoresNonScreenshotReporterOutput()
        {
            AddGlobalReporterOutputLine("Something not about screenshots");
            AddTestMethodElement(new XAttribute("name", "TestMethod"));
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(""));
        }
        [Test]
        public void ScreenshotPath_TestWithOneParameter()
        {
            AddGlobalReporterOutputLine(@"Screenshot: path=C:\Shot1.png; class=Class; method=Test1(""param1"")");
            AddClassElement(new XAttribute("name", "Class"));
            AddTestMethodElement(new XAttribute("name", "Test1"),
                new XElement("params",
                    new XElement("param", new XAttribute("index", "0"),
                        new XElement("value", "param1"))));
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(@"C:\Shot1.png"));
        }
        [Test]
        public void ScreenshotPath_TestParameterContainingBackslashes()
        {
            // In "Screenshot:" line, backslashes are escaped (doubled)
            AddGlobalReporterOutputLine(@"Screenshot: path=C:\Shot1.png; class=Class; method=Test1(""D:\\Foo\\Bar"")");
            AddClassElement(new XAttribute("name", "Class"));
            // TestNG <param> contains plain content (no escaping)
            AddTestMethodElement(new XAttribute("name", "Test1"),
                new XElement("params",
                    new XElement("param", new XAttribute("index", "0"),
                        new XElement("value", @"D:\Foo\Bar"))));
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(@"C:\Shot1.png"));
        }
        [Test]
        public void ScreenshotPath_TestParameterContainingQuotes()
        {
            // In "Screenshot:" line, quotes are backslash-escaped.
            // Note that quotes are doubled here in the source code, and backslashes are not, because @-string.
            AddGlobalReporterOutputLine(
                @"Screenshot: path=C:\Shot1.png; class=Class; method=Test1(""{name: \""value\""}"")");
            AddClassElement(new XAttribute("name", "Class"));
            // TestNG <param> contains plain content (no escaping).
            AddTestMethodElement(new XAttribute("name", "Test1"),
                new XElement("params",
                    new XElement("param", new XAttribute("index", "0"),
                        new XElement("value", @"{name: ""value""}"))));
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(@"C:\Shot1.png"));
        }
        [Test]
        public void ScreenshotPath_TwoParameters()
        {
            AddGlobalReporterOutputLine(@"Screenshot: path=C:\Shot1.png; class=Class; method=Test1(""a"", ""b"")");
            AddClassElement(new XAttribute("name", "Class"));
            AddTestMethodElement(new XAttribute("name", "Test1"),
                new XElement("params",
                    new XElement("param", new XAttribute("index", "0"),
                        new XElement("value", "a")),
                    new XElement("param", new XAttribute("index", "1"),
                        new XElement("value", "b"))));
            var test = ImportSingleTest();
            Assert.That(test.ScreenshotPath, Is.EqualTo(@"C:\Shot1.png"));
        }
        [Test]
        public void ScreenshotPath_TwoTests()
        {
            AddGlobalReporterOutputLine(@"Screenshot: path=C:\Shot1.png; class=Class1; method=Test1()");
            AddGlobalReporterOutputLine(@"Screenshot: path=C:\Shot2.png; class=Class2; method=Test2()");
            AddClassElement(new XAttribute("name", "Class1"));
            AddTestMethodElement(new XAttribute("name", "Test1"));
            AddClassElement(new XAttribute("name", "Class2"));
            AddTestMethodElement(new XAttribute("name", "Test2"));
            var tests = ImportTests();
            var screenshotPaths = string.Join(";", tests.Select(t => t.ScreenshotPath));
            Assert.That(screenshotPaths, Is.EqualTo(@"C:\Shot1.png;C:\Shot2.png"));
        }
    }
}