﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using JetBrains.Annotations;

namespace DafAggregator.Reports
{
    public class ContentElement : IContent
    {
        /// <summary>
        /// Attributes. Kept as a list instead of a dictionary so we can keep them in insertion order.
        /// </summary>
        private readonly List<Attribute> _attributes = new List<Attribute>();
        private readonly List<IContent> _children = new List<IContent>();

        public ContentElement(string name, object placeholderToMakeSureWeDontUsePositionalParametersByMistake = null,
            string id = null, string cssClass = null, string text = null)
        {
            if (placeholderToMakeSureWeDontUsePositionalParametersByMistake != null)
            {
                throw new ArgumentException("Don't use positional parameters!",
                    paramName: nameof(placeholderToMakeSureWeDontUsePositionalParametersByMistake));
            }

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Name must have a value", paramName: nameof(name));

            Name = name;
            if (!string.IsNullOrWhiteSpace(id))
                SetAttribute("id", id);
            if (!string.IsNullOrWhiteSpace(cssClass))
                SetAttribute("class", cssClass);
            if (!string.IsNullOrEmpty(text))
                Add(new ContentText(text));
        }

        public IReadOnlyList<Attribute> Attributes => _attributes;
        public IReadOnlyList<IContent> Children => _children;
        public string InnerText => string.Join("", Children.Select(child => child.InnerText));
        public string Name { get; }

        public ContentElement Add([NotNull, ItemNotNull] IEnumerable<IContent> children)
        {
            var childList = (children ?? throw new ArgumentNullException(nameof(children))).ToList();
            if (childList.Contains(null))
                throw new ArgumentException("Collection may not contain nulls", nameof(children));

            foreach (var child in childList)
                _children.Add(child);
            return this;
        }
        public ContentElement Add([NotNull] IContent child)
        {
            if (child == null)
                throw new ArgumentNullException(nameof(child));

            _children.Add(child);
            return this;
        }
        [NotNull]
        public string GetAttribute(string name)
        {
            return _attributes.FirstOrDefault(a => a.Name == name)?.Value ?? "";
        }
        public bool IsMatch(string query)
        {
            return
                query == Name ||
                query == GetAttribute("id") ||
                GetAttribute("class").Split(' ').Contains(query);
        }
        public ContentElement SetAttribute(string name, string value)
        {
            _attributes.RemoveAll(a => a.Name == name);
            _attributes.Add(new Attribute(name, value));
            return this;
        }
        public IEnumerable<HtmlNode> ToHtmlNodes(HtmlDocument document)
        {
            var element = document.CreateElement(Name);
            foreach (var attribute in _attributes)
            {
                element.SetAttributeValue(attribute.Name,
                    HtmlEntity.Entitize(attribute.Value, useNames: true, entitizeQuotAmpAndLtGt: true));
            }

            foreach (var childElement in _children.SelectMany(child => child.ToHtmlNodes(document)))
                element.ChildNodes.Add(childElement);

            return new[] {element};
        }
        public override string ToString()
        {
            return $"<{Name}>";
        }
    }
}