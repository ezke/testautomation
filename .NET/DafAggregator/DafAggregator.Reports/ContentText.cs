﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using JetBrains.Annotations;

namespace DafAggregator.Reports
{
    public class ContentText : IContent
    {
        public ContentText([NotNull] string text, bool raw = false)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            Raw = raw;
        }

        string IContent.InnerText => Text;
        public bool Raw { get; }
        [NotNull]
        public string Text { get; }

        public IReadOnlyList<IContent> Children => Array.Empty<IContent>();
        public bool IsMatch(string query)
        {
            return false;
        }
        public IEnumerable<HtmlNode> ToHtmlNodes(HtmlDocument document)
        {
            var encodedText = Raw ? Text : HtmlEntity.Entitize(Text, useNames: true, entitizeQuotAmpAndLtGt: true);
            return new[] {document.CreateTextNode(encodedText)};
        }
        public override string ToString()
        {
            var descriptor = Raw ? "raw" : "text";
            return $"[{descriptor}={Text}]";
        }
    }
}