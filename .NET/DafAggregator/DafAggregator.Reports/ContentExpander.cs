﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using JetBrains.Annotations;

namespace DafAggregator.Reports
{
    public class ContentExpander : IContent
    {
        private readonly List<IContent> _bodyContents = new List<IContent>();

        public ContentExpander([NotNull] IContent headerContent,
            string parentClass = null, [ItemNotNull] IEnumerable<Attribute> parentAttributes = null,
            string headerClass = null, [ItemNotNull] IEnumerable<Attribute> headerAttributes = null,
            bool inlineIfNoSiblings = false)
        {
            HeaderContent = headerContent ?? throw new ArgumentNullException(nameof(headerContent));
            ParentAttributes = CreateAttributeList(parentClass, parentAttributes);
            HeaderAttributes = CreateAttributeList(headerClass, headerAttributes);
            InlineIfNoSiblings = inlineIfNoSiblings;
        }

        [NotNull]
        public IContent HeaderContent { get; }
        [NotNull, ItemNotNull]
        public IReadOnlyList<Attribute> ParentAttributes { get; }
        [NotNull, ItemNotNull]
        public IReadOnlyList<Attribute> HeaderAttributes { get; }
        public bool InlineIfNoSiblings { get; }
        public IReadOnlyList<IContent> Children => _bodyContents;
        private IEnumerable<IContent> AllChildren => new[] {HeaderContent}.Concat(Children);
        public string InnerText => string.Join("\r\n", AllChildren.Select(child => child.InnerText));

        public ContentExpander AddBodyContent([NotNull] IContent item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _bodyContents.Add(item);
            return this;
        }
        private static IReadOnlyList<Attribute> CreateAttributeList([CanBeNull] string cssClass,
            [CanBeNull, ItemNotNull] IEnumerable<Attribute> attributes)
        {
            var list = new List<Attribute>();

            if (cssClass != null)
                list.Add(new Attribute("class", cssClass));
            if (attributes != null)
                list.AddRange(attributes);

            if (list.Contains(null))
                throw new ArgumentException("Collection may not contain nulls", paramName: nameof(attributes));

            return list;
        }
        private IEnumerable<HtmlNode> GetBodyHtmlNodes(HtmlDocument document)
        {
            // Check for the special case of one child ContentExpander with InlineIfNoSiblings
            if (_bodyContents.Count == 1 && _bodyContents[0] is ContentExpander childExpander &&
                childExpander.InlineIfNoSiblings)
            {
                // Inline the child's contents
                foreach (var htmlNode in childExpander.GetBodyHtmlNodes(document))
                    yield return htmlNode;
            }
            else
            {
                // Normal case: render the children as usual
                foreach (var htmlNode in _bodyContents.SelectMany(item => item.ToHtmlNodes(document)))
                    yield return htmlNode;
            }
        }
        public bool IsMatch(string query)
        {
            return false;
        }
        public IEnumerable<HtmlNode> ToHtmlNodes(HtmlDocument document)
        {
            var detailsElement = document.CreateElement("details");
            var summaryElement = document.CreateElement("summary");
            detailsElement.ChildNodes.Add(summaryElement);

            foreach (var attribute in ParentAttributes)
                detailsElement.SetAttributeValue(attribute.Name, attribute.Value);
            foreach (var attribute in HeaderAttributes)
                summaryElement.SetAttributeValue(attribute.Name, attribute.Value);
            foreach (var htmlNode in HeaderContent.ToHtmlNodes(document))
                summaryElement.ChildNodes.Add(htmlNode);
            foreach (var htmlNode in GetBodyHtmlNodes(document))
                detailsElement.ChildNodes.Add(htmlNode);

            return new[] {detailsElement};
        }
    }
}