﻿using System;
using JetBrains.Annotations;

namespace DafAggregator.Reports
{
    public class Attribute : IEquatable<Attribute>
    {
        public Attribute([NotNull] string name, [NotNull] string value)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        [NotNull]
        public string Name { get; }
        [NotNull]
        public string Value { get; }

        public bool Equals(Attribute other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(Name, other.Name) && string.Equals(Value, other.Value);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((Attribute) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return (Name.GetHashCode() * 397) ^ Value.GetHashCode();
            }
        }
        public override string ToString()
        {
            return $"[{Name}={Value}]";
        }
    }
}