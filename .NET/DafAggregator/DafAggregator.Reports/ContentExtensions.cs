﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace DafAggregator.Reports
{
    public static class ContentExtensions
    {
        public static IContent Child(this IContent parent, string query)
        {
            return parent.SingleMatch(parent.Children, query);
        }
        public static IContent Descendant(this IContent parent, string query)
        {
            return parent.SingleMatch(parent.Descendants(), query);
        }
        public static IEnumerable<IContent> Descendants(this IContent parent)
        {
            foreach (var child in parent.Children)
            {
                yield return child;
                foreach (var descendant in child.Descendants())
                    yield return descendant;
            }
        }
        public static IEnumerable<IContent> Descendants(this IContent parent, string query)
        {
            return parent.Descendants().Where(descendant => descendant.IsMatch(query));
        }
        private static IContent SingleMatch(this IContent parent, IEnumerable<IContent> possibleMatches, string query)
        {
            var matches = possibleMatches.Where(possibleMatch => possibleMatch.IsMatch(query)).ToList();
            if (matches.Count != 1)
            {
                throw new InvalidOperationException(
                    $"{parent}: looking for '{query}': expected 1 match but found {matches.Count}");
            }

            return matches[0];
        }
        public static HtmlDocument ToHtmlDocument(this IContent content)
        {
            var document = new HtmlDocument();
            document.DocumentNode.ChildNodes.Add(HtmlNode.CreateNode("<!DOCTYPE html>"));
            document.DocumentNode.ChildNodes.Add(document.CreateTextNode("\r\n"));
            var htmlNodes = content.ToHtmlNodes(document);
            foreach (var htmlNode in htmlNodes)
                document.DocumentNode.ChildNodes.Add(htmlNode);
            return document;
        }
    }
}