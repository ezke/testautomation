﻿using System.IO;

namespace DafAggregator.Reports
{
    public static class Resources
    {
        public static string PieScript => LoadEmbeddedResource("PieScript.js");
        public static string Stylesheet => LoadEmbeddedResource("Stylesheet.css");

        private static string LoadEmbeddedResource(string relativeName)
        {
            var type = typeof(Resources);
            var qualifiedName = type.Namespace + "." + relativeName;
            using (var stream = type.Assembly.GetManifestResourceStream(qualifiedName))
            using (var reader = new StreamReader(stream))
                return reader.ReadToEnd();
        }
    }
}