var canvas = document.getElementById("pie");
var ctx = canvas.getContext("2d");
var lastend = -Math.PI / 2;
var data = __PIE_VALUES__;
var total = 0;
var colors = ['#6E6', '#F77', '#B0F']; // Colors of each slice

for (var i = 0; i < data.length; ++i) {
    total += data[i];
}

for (var i = 0; i < data.length; ++i) {
    ctx.fillStyle = colors[i];
    ctx.beginPath();
    ctx.moveTo(canvas.width / 2, canvas.height / 2);
    ctx.arc(canvas.width / 2, canvas.height / 2, canvas.height / 2 - 1, lastend, lastend + (Math.PI * 2 * (data[i] / total)), false);
    ctx.lineTo(canvas.width / 2, canvas.height / 2);
    ctx.fill();
    lastend += Math.PI * 2 * (data[i] / total);
}
