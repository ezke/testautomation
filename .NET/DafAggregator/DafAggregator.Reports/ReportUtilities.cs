﻿using System;
using System.Linq;

namespace DafAggregator.Reports
{
    public static class ReportUtilities
    {
        public static string GetAdjectiveString(TestStatus value)
        {
            switch (value)
            {
                case TestStatus.Pass:
                    return "Passed";
                case TestStatus.Fail:
                    return "Failed";
                case TestStatus.Busted:
                    return "Busted";
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }
        public static string GetCssClass(TestStatus value)
        {
            switch (value)
            {
                case TestStatus.Pass:
                    return "pass";
                case TestStatus.Fail:
                    return "fail";
                case TestStatus.Busted:
                    return "busted";
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }
        public static string GetCssClass(TestStatusSet statuses)
        {
            return string.Join(" ", statuses.Select(GetCssClass));
        }
        public static string GetDisplayStatus(TestStatusSet statuses)
        {
            return statuses.Count == 1
                ? statuses.First().ToString().ToUpperInvariant()
                : "MIXED";
        }
        public static string GetStepDescription(TestStep step)
        {
            var stepDescription = !step.HasSteps
                ? "Non-step-specific"
                : step.FirstStep == step.LastStep
                    ? $"Step {step.FirstStep}"
                    : $"Steps {step.FirstStep} to {step.LastStep}";
            return stepDescription;
        }
    }
}
