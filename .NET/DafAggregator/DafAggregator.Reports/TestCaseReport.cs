﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace DafAggregator.Reports
{
    public static class TestCaseReport
    {
        private const char EnDash = '\u2013';

        public static HtmlDocument BuildHtmlDocument(string title, IEnumerable<string> reportWarnings,
            IEnumerable<Test> tests)
        {
            return BuildContent(title, reportWarnings, tests).ToHtmlDocument();
        }
        public static ContentElement BuildContent(string title, IEnumerable<string> reportWarnings,
            IEnumerable<Test> tests)
        {
            return new ContentElement("html")
                .Add(BuildHead(title))
                .Add(BuildBody(title, reportWarnings.ToList(), tests.ToList()));
        }
        private static IContent BuildBody(string title, IReadOnlyList<string> reportWarnings, IReadOnlyList<Test> tests)
        {
            var testCaseNodes = TestCaseNodeFactory.CreateTestCaseNodes(tests);

            var statuses = TestStatusSet.Union(testCaseNodes.Select(testCaseNode => testCaseNode.Statuses));
            return new ContentElement("body")
                .Add(BuildSummarySection(title, reportWarnings, statuses.WorstStatus, testCaseNodes))
                .Add(BustedSection.Build(tests))
                .Add(TestCaseSection.Build(tests))
                .Add(BuildPieScript(testCaseNodes));
        }
        private static IContent BuildHead(string title)
        {
            return new ContentElement("head")
                .Add(new ContentElement("title", text: title))
                .Add(new ContentElement("style")
                    .Add(new ContentText(Resources.Stylesheet, raw: true)));
        }
        private static IContent BuildPieScript(IReadOnlyList<TestCaseNode> testCaseNodes)
        {
            var byWorstStatus = testCaseNodes.ToLookup(testCase => testCase.Statuses.WorstStatus);
            var passCount = byWorstStatus[TestStatus.Pass].Count();
            var failCount = byWorstStatus[TestStatus.Fail].Count();
            var bustedCount = byWorstStatus[TestStatus.Busted].Count();
            var script = Resources.PieScript.Replace("__PIE_VALUES__", $"[{passCount},{failCount},{bustedCount}]");
            return new ContentElement("script")
                .Add(new ContentText(script, raw: true));
        }
        private static IContent BuildSummaryBannerDiv(TestStatus worstStatus)
        {
            var cssClass = ReportUtilities.GetCssClass(worstStatus);

            return new ContentElement("div", id: "summaryBanner", cssClass: "mb " + cssClass)
                .Add(new ContentElement("span", cssClass: "bigText", text: worstStatus.ToString()))
                .Add(new ContentElement("span", cssClass: "supportingText",
                    text: $" {EnDash} {GetTestStatusSupportingText(worstStatus)}"));
        }
        private static IContent BuildSummaryByStatusDiv(IReadOnlyList<TestCaseNode> testCaseNodes)
        {
            var byWorstStatus = testCaseNodes
                .GroupBy(testCaseGroup => testCaseGroup.Statuses.WorstStatus)
                .OrderBy(group => group.Key);

            var itemDivs = byWorstStatus
                .Select(group => BuildSummaryByStatusItemDiv(group.Key, group, testCaseNodes.Count))
                .ToList();

            if (!itemDivs.Any())
            {
                // totalCount will be the denominator for the percentage, so let's use something other than zero.
                const int totalCount = 1;

                itemDivs.Add(
                    BuildSummaryByStatusItemDiv(TestStatus.Pass, Enumerable.Empty<TestCaseNode>(), totalCount));
            }

            return new ContentElement("div", id: "summaryByStatus").Add(itemDivs);
        }
        private static IContent BuildSummaryByStatusItemDiv(TestStatus status,
            IEnumerable<TestCaseNode> statusNodes, int totalTestCaseCount)
        {
            var statusAdjective = ReportUtilities.GetAdjectiveString(status);
            var testCaseCount = statusNodes.Count();
            var plural = testCaseCount == 1 ? "" : "s";
            var percent = testCaseCount * 100.0 / totalTestCaseCount;
            var value = $"{statusAdjective}: {testCaseCount} test case{plural} ({percent:0}%)";

            var cssClass = ReportUtilities.GetCssClass(status);

            return new ContentElement("div", cssClass: "indent " + cssClass, text: value);
        }
        private static IContent BuildSummarySection(string title, IReadOnlyList<string> reportWarnings,
            TestStatus worstStatus, IReadOnlyList<TestCaseNode> testCaseNodes)
        {
            return new ContentElement("section")
                .Add(new ContentElement("h1", text: title))
                .Add(BuildReportWarningsDiv(reportWarnings))
                .Add(new ContentElement("div", cssClass: "pie-container")
                    .Add(new ContentElement("div", cssClass: "next-to-pie")
                        .Add(BuildSummaryBannerDiv(worstStatus))
                        .Add(new ContentElement("div", cssClass: "mb", text: "Details:"))
                        .Add(BuildSummaryByStatusDiv(testCaseNodes)))
                    .Add(new ContentElement("canvas", id: "pie", cssClass: "pie")
                        .SetAttribute("width", "150")
                        .SetAttribute("height", "150")));
        }
        private static IContent BuildReportWarningsDiv(IReadOnlyList<string> reportWarnings)
        {
            if (!reportWarnings.Any())
                return new NullContent();

            return new ContentElement("div", cssClass: "reportWarnings")
                .Add(new ContentElement("div", cssClass: "mb", text: "There were problems creating the report."))
                .Add(new ContentElement("ul")
                    .Add(reportWarnings.Select(warningText => new ContentElement("li", text: warningText))));
        }
        private static string GetTestStatusSupportingText(TestStatus status)
        {
            switch (status)
            {
                case TestStatus.Pass:
                    return "All requirements verified";
                case TestStatus.Fail:
                    return "Requirements failed or not verified";
                case TestStatus.Busted:
                    return "Errors in test code. One or more tests cannot run.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }
    }
}