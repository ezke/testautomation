﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace DafAggregator.Reports
{
    /// <summary>
    /// An IContent with nothing in it. Does not generate any output HTML.
    /// </summary>
    public class NullContent : IContent
    {
        public IReadOnlyList<IContent> Children => Array.Empty<IContent>();
        public string InnerText => "";

        public bool IsMatch(string query)
        {
            return false;
        }
        public IEnumerable<HtmlNode> ToHtmlNodes(HtmlDocument document)
        {
            return Enumerable.Empty<HtmlNode>();
        }
    }
}