﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator.Reports
{
    public static class BustedSection
    {
        private enum IfEmpty
        {
            LeaveEmpty,
            ShowLabel,
        }

        public static IContent Build(IEnumerable<Test> tests)
        {
            const string introBlurb =
                "Tests are failing due to bugs in the test code. These bugs require the attention of automation engineers.";

            var bustedTests = tests.Where(test => test.Status == TestStatus.Busted).ToList();
            if (!bustedTests.Any())
                return new NullContent();

            var sectionElement = new ContentElement("section", cssClass: "bustedSection")
                .Add(new ContentElement("h2", text: "Busted Tests"))
                .Add(new ContentElement("p", text: introBlurb));
            var exceptionGroups = bustedTests
                .OrderBy(test => test.Failure?.ExceptionType)
                .ThenBy(test => test.Failure?.Message)
                .ThenBy(test => test.Failure?.StackTrace.FirstOrDefault()?.ClassName)
                .ThenBy(test => test.Failure?.StackTrace.FirstOrDefault()?.MethodName)
                .ThenBy(test => test.Failure?.StackTrace.FirstOrDefault()?.Text)
                .GroupBy(test => new {test.Failure?.ExceptionType, test.Failure?.Message});

            sectionElement.Add(exceptionGroups.Select(group =>
                BuildExceptionTypeAndMessageExpander(group.Key.ExceptionType, group.Key.Message, group)));

            return sectionElement;
        }
        private static IContent BuildExceptionTypeAndMessageExpander(string exceptionType, string message,
            IEnumerable<Test> tests)
        {
            var headerText = string.IsNullOrWhiteSpace(message) ? exceptionType : $"{exceptionType}: {message}";
            var expander = new ContentExpander(new ContentText(headerText), headerClass: "busted");

            var originGroups = tests.GroupBy(test => test.Failure?.StackTrace.FirstOrDefault());
            foreach (var originGroup in originGroups)
            {
                expander.AddBodyContent(BuildOriginExpander(originGroup.Key, originGroup));
            }

            return expander;
        }
        private static IContent BuildOriginExpander([CanBeNull] StackTraceEntry origin,
            [NotNull] IEnumerable<Test> tests)
        {
            if (tests == null)
                throw new ArgumentNullException(nameof(tests));

            var header = BuildOriginHeader(origin);

            var expander = new ContentExpander(header, parentClass: "indent");

            var methodGroups = tests
                .GroupBy(test => new {test.TestMethod.ClassName, test.TestMethod.MethodName});
            foreach (var methodGroup in methodGroups)
            {
                var method = methodGroup.Key;
                var methodExpander = BuildMethodExpander(method.ClassName, method.MethodName, methodGroup);
                expander.AddBodyContent(methodExpander);
            }

            return expander;
        }
        private static IContent BuildOriginHeader([CanBeNull] StackTraceEntry origin)
        {
            return new ContentGroup(new ContentText("Occurred at: "), BuildStackTraceEntry(origin, IfEmpty.ShowLabel));
        }
        private static IContent BuildStackTraceEntry([CanBeNull] StackTraceEntry entry, IfEmpty ifEmpty)
        {
            var content = new List<IContent>();
            if (entry != null)
            {
                var hasClassName = !string.IsNullOrWhiteSpace(entry.ClassName);
                var hasMethodName = !string.IsNullOrWhiteSpace(entry.MethodName);
                var hasText = !string.IsNullOrWhiteSpace(entry.Text);

                if (hasClassName)
                {
                    content.Add(new ContentElement("span", cssClass: "class-name",
                        text: entry.ClassName + (hasMethodName ? "." : "")));

                    if (hasMethodName)
                        content.Add(new ContentElement("wbr"));
                }

                if (hasMethodName)
                    content.Add(new ContentElement("span", cssClass: "method-name", text: entry.MethodName));

                if (hasText)
                    content.Add(new ContentText((content.Any() ? " " : "") + entry.Text));
            }

            if (!content.Any() && ifEmpty == IfEmpty.ShowLabel)
                return new ContentElement("i", text: "unknown location");
            return new ContentGroup(content);
        }
        private static ContentExpander BuildMethodExpander(string className, string methodName, IEnumerable<Test> tests)
        {
            var header = new ContentGroup(
                new ContentText("Test: "),
                new ContentElement("span", cssClass: "class-name", text: className + "."),
                new ContentElement("wbr"),
                new ContentElement("span", cssClass: "method-name", text: methodName));

            var methodExpander = new ContentExpander(header, parentClass: "indent");

            foreach (var test in tests)
                methodExpander.AddBodyContent(BuildParametersExpander(test));
            return methodExpander;
        }
        private static ContentExpander BuildParametersExpander(Test test)
        {
            var parameters = test.TestMethod.Parameters;
            var parametersHeader = parameters.Any()
                ? "Parameters: " + string.Join(", ", parameters)
                : "No parameters";

            var parametersExpander = new ContentExpander(
                new ContentText(parametersHeader),
                parentClass: "indent",
                inlineIfNoSiblings: !parameters.Any());
            parametersExpander.AddBodyContent(BuildFailureDiv(test.Failure));
            return parametersExpander;
        }
        private static ContentElement BuildFailureDiv(Failure failure)
        {
            var failureDiv = new ContentElement("div", cssClass: "indent failureDetails");
            failureDiv.Add(new ContentElement("div", text: "Stack trace:"));
            failureDiv.Add(BuildStackTraceList(failure));
            return failureDiv;
        }
        private static ContentElement BuildStackTraceList(Failure failure)
        {
            return new ContentElement("ul")
                .Add(failure.StackTrace.Select(BuildStackTraceItem));
        }
        private static ContentElement BuildStackTraceItem(StackTraceEntry entry)
        {
            return new ContentElement("li")
                .Add(BuildStackTraceEntry(entry, IfEmpty.LeaveEmpty));
        }
    }
}