﻿using System.Collections.Generic;
using System.Linq;

namespace DafAggregator.Reports
{
    public static class TestCaseSection
    {
        public static IContent Build(IReadOnlyList<Test> tests)
        {
            if (!tests.Any())
                return new NullContent();

            var sectionElement = new ContentElement("section", id: "testCaseSection")
                .Add(new ContentElement("h2", text: "By Test Case"));

            var byTestCaseId = tests
                .GroupBy(test => test.TestStep.TestCaseId)
                .OrderBy(group => group.Key);

            foreach (var testCaseGroup in byTestCaseId)
            {
                var testCaseElement = BuildTestCaseExpander(testCaseGroup.Key, testCaseGroup.ToList());
                sectionElement.Add(testCaseElement);
            }

            return sectionElement;
        }
        private static IContent BuildHeaderContent(TestStatusSet statuses, IContent headerLabel)
        {
            var headerIntroText = ReportUtilities.GetDisplayStatus(statuses);
            var headerContent = new ContentGroup();
            headerContent.Add(new ContentText(headerIntroText));

            // If we're mixed, add badges
            if (statuses.Count > 1)
            {
                foreach (var status in statuses.Reverse())
                {
                    var spanClass = "badge " + ReportUtilities.GetCssClass(status);
                    var spanText = status.ToString();
                    headerContent.Add(new ContentElement("span", cssClass: spanClass, text: spanText));
                }
            }

            headerContent.Add(new ContentText(" - "));
            headerContent.Add(headerLabel);
            return headerContent;
        }
        private static IContent BuildTestCaseExpander(long id, IReadOnlyList<Test> tests)
        {
            var statuses = new TestStatusSet(tests.Select(test => test.Status));
            var headerContent = BuildHeaderContent(statuses, new ContentElement("b", text: $"PT{id}"));
            var headerClass = ReportUtilities.GetCssClass(statuses.WorstStatus);
            var expander = new ContentExpander(new ContentGroup(headerContent), headerClass: headerClass);

            var byTestClass = tests
                .GroupBy(test => test.TestMethod.ClassName)
                .OrderBy(group => group.Key);

            foreach (var testClassGroup in byTestClass)
            {
                var testClassDiv = BuildTestClassExpander(testClassGroup.Key, testClassGroup.ToList());
                expander.AddBodyContent(testClassDiv);
            }

            return expander;
        }
        private static IContent BuildClassNameContent(string testClass)
        {
            var lastDot = testClass.LastIndexOf('.');
            if (lastDot >= 0)
            {
                return new ContentGroup(
                    new ContentText(testClass.Substring(0, lastDot + 1)),
                    new ContentElement("wbr"),
                    new ContentElement("b", text: testClass.Substring(lastDot + 1)));
            }

            return new ContentText(testClass);
        }
        private static IContent BuildTestClassExpander(string testClass, IReadOnlyList<Test> tests)
        {
            var statuses = new TestStatusSet(tests.Select(test => test.Status));
            var headerClass = ReportUtilities.GetCssClass(statuses.WorstStatus);
            var expander = new ContentExpander(BuildHeaderContent(statuses, BuildClassNameContent(testClass)),
                parentClass: "indent", headerClass: headerClass);

            var byTestMethod = tests.GroupBy(test => test.TestMethod.MethodName);
            foreach (var testMethodGroup in byTestMethod)
            {
                var testMethodExpander = BuildTestMethodExpander(testMethodGroup.Key, testMethodGroup.ToList());
                expander.AddBodyContent(testMethodExpander);
            }

            return expander;
        }
        private static IContent BuildTestMethodExpander(string testMethod, IReadOnlyList<Test> tests)
        {
            var statuses = new TestStatusSet(tests.Select(test => test.Status));
            var headerClass = ReportUtilities.GetCssClass(statuses.WorstStatus);
            var expander = new ContentExpander(BuildHeaderContent(statuses, new ContentElement("b", text: testMethod)),
                parentClass: "indent", headerClass: headerClass);

            foreach (var test in tests)
            {
                var testExpander = BuildTestExpander(test);
                expander.AddBodyContent(testExpander);
            }

            return expander;
        }
        private static IContent BuildTestExpander(Test test)
        {
            var statuses = new TestStatusSet(new[] {test.Status});
            var headerClass = ReportUtilities.GetCssClass(statuses.WorstStatus);
            var headerLabel = string.Join(" / ", test.TestMethod.Parameters);
            var expander = new ContentExpander(BuildHeaderContent(statuses, new ContentElement("b", text: headerLabel)),
                parentClass: "indent", headerClass: headerClass, inlineIfNoSiblings: !test.TestMethod.Parameters.Any());

            var failure = test.Failure;
            if (failure != null)
            {
                var div = BuildFailureDiv(test.Status, failure);

                expander.AddBodyContent(div);
            }

            return expander;
        }
        private static IContent BuildFailureDiv(TestStatus status, Failure failure)
        {
            var group = new ContentGroup();
            group.Add(BuildFailureSummaryDiv(failure));
            group.Add(BuildStackTraceExpander(status, failure));
            return group;
        }
        private static IContent BuildFailureSummaryDiv(Failure failure)
        {
            var div = new ContentElement("div", cssClass: "failureDetails indent");
            div.Add(new ContentElement("small", text: failure.ExceptionType));
            div.Add(new ContentElement("br"));
            div.Add(new ContentElement("b", text: failure.Message));
            return div;
        }
        private static IContent BuildStackTraceExpander(TestStatus status, Failure failure)
        {
            if (!failure.StackTrace.Any())
                return new NullContent();

            var headerClass = ReportUtilities.GetCssClass(status);
            var expander = new ContentExpander(
                new ContentText("Stack Trace"), parentClass: "indent", headerClass: headerClass);
            var div = new ContentElement("div", cssClass: "indent failureDetails");
            var stackTraceList = new ContentElement("ul");
            foreach (var entry in failure.StackTrace)
            {
                var listItem = new ContentElement("li");
                if (!string.IsNullOrWhiteSpace(entry.ClassName))
                    listItem.Add(new ContentText(entry.ClassName + "."));
                listItem.Add(new ContentElement("wbr"));
                listItem.Add(new ContentText(entry.MethodName));
                if (!string.IsNullOrWhiteSpace(entry.Text))
                    listItem.Add(new ContentText(" " + entry.Text));

                stackTraceList.Add(listItem);
            }

            div.Add(stackTraceList);
            expander.AddBodyContent(div);
            return expander;
        }
    }
}