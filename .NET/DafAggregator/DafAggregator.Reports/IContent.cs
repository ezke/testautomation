﻿using System.Collections.Generic;
using HtmlAgilityPack;

namespace DafAggregator.Reports
{
    public interface IContent
    {
        /// <summary>
        /// The element's logical children. This exists for inspection by tests, and should contain the IContent
        /// elements that were passed to the constructor (even if the HTML representation is more complex).
        /// </summary>
        IReadOnlyList<IContent> Children { get; }
        /// <summary>
        /// The element's text (if a text element), or the combined text of all the element's children.
        /// This exists purely for inspection by tests, so can be in any form that tests would find meaningful.
        /// </summary>
        string InnerText { get; }

        /// <summary>
        /// Returns true if this element matches the given query. This can be any relevant match: element name,
        /// id, class, etc. This exists purely for inspection by tests.
        /// </summary>
        bool IsMatch(string query);
        IEnumerable<HtmlNode> ToHtmlNodes(HtmlDocument document);
    }
}