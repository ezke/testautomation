﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using JetBrains.Annotations;

namespace DafAggregator.Reports
{
    /// <summary>
    /// An IContent with a group of sibling items. This does not generate a parent element in the output HTML;
    /// it just outputs its contents as a series of sibling HTML elements.
    /// </summary>
    public class ContentGroup : IContent
    {
        private readonly List<IContent> _contents;

        public ContentGroup([NotNull] params IContent[] contents)
            : this(contents.AsEnumerable())
        {
        }
        public ContentGroup(IEnumerable<IContent> contents)
        {
            _contents = (contents ?? throw new ArgumentNullException(nameof(contents))).ToList();
        }

        public IReadOnlyList<IContent> Children => _contents;
        public string InnerText => string.Join("", Children.Select(child => child.InnerText));

        public void Add([NotNull] IContent item)
        {
            _contents.Add(item ?? throw new ArgumentNullException(nameof(item)));
        }
        public bool IsMatch(string query)
        {
            return false;
        }
        public IEnumerable<HtmlNode> ToHtmlNodes(HtmlDocument document)
        {
            return _contents.SelectMany(item => item.ToHtmlNodes(document));
        }
    }
}