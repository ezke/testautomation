﻿using System.Collections.Generic;
using System.Linq;

namespace DafAggregator
{
    public static class TestCaseNodeFactory
    {
        public static IReadOnlyList<TestCaseNode> CreateTestCaseNodes(IEnumerable<Test> tests)
        {
            var testCaseGroups = tests
                .GroupBy(test => test.TestStep.TestCaseId)
                .OrderBy(group => group.Key);
            var testCaseNodes = testCaseGroups.Select(group => CreateTestCaseNode(group.Key, group));
            return testCaseNodes.ToList();
        }
        private static TestCaseNode CreateTestCaseNode(long testCaseId, IEnumerable<Test> tests)
        {
            var statuses = new TestStatusSet(tests.Select(test => test.Status));
            return new TestCaseNode(testCaseId, statuses);
        }
    }
}