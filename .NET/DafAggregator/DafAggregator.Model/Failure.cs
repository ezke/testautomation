﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator
{
    public class Failure
    {
        public Failure([NotNull] string exceptionType, [NotNull] string message,
            [NotNull] IEnumerable<StackTraceEntry> stackTrace, FailureLevel failureLevel)
        {
            ExceptionType = exceptionType ?? throw new ArgumentNullException(nameof(exceptionType));
            Message = message ?? throw new ArgumentNullException(nameof(message));
            StackTrace = (stackTrace ?? throw new ArgumentNullException(nameof(stackTrace))).ToList();
            FailureLevel = failureLevel;

            if (StackTrace.Contains(null))
                throw new ArgumentException("Collection may not contain nulls", paramName: nameof(stackTrace));
        }

        [NotNull]
        public string ExceptionType { get; }
        [NotNull]
        public string Message { get; }
        [NotNull]
        public IReadOnlyList<StackTraceEntry> StackTrace { get; }
        public FailureLevel FailureLevel { get; }
    }
}