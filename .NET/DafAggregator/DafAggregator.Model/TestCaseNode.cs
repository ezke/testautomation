﻿using System;
using JetBrains.Annotations;

namespace DafAggregator
{
    public class TestCaseNode
    {
        public TestCaseNode(long id, [NotNull] TestStatusSet statuses)
        {
            Id = id;
            Statuses = statuses ?? throw new ArgumentNullException(nameof(statuses));
        }

        public long Id { get; }
        [NotNull]
        public TestStatusSet Statuses { get; }
    }
}