﻿using System;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator
{
    public sealed class StackTraceEntry : IEquatable<StackTraceEntry>
    {
        public StackTraceEntry([NotNull] string className = "", [NotNull] string methodName = "",
            [NotNull] string text = "")
        {
            ClassName = className ?? throw new ArgumentNullException(nameof(className));
            MethodName = methodName ?? throw new ArgumentNullException(nameof(methodName));
            Text = text ?? throw new ArgumentNullException(nameof(text));
        }

        /// <summary>
        /// The class name, if available.
        /// </summary>
        [NotNull]
        public string ClassName { get; }
        /// <summary>
        /// The method name, if available.
        /// </summary>
        [NotNull]
        public string MethodName { get; }
        /// <summary>
        /// Any other text associated with the stack-trace entry, if available.
        /// </summary>
        [NotNull]
        public string Text { get; }

        public bool Equals(StackTraceEntry other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(ClassName, other.ClassName) &&
                   string.Equals(MethodName, other.MethodName) &&
                   string.Equals(Text, other.Text);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            return obj is StackTraceEntry entry && Equals(entry);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ClassName.GetHashCode();
                hashCode = (hashCode * 397) ^ MethodName.GetHashCode();
                hashCode = (hashCode * 397) ^ Text.GetHashCode();
                return hashCode;
            }
        }
        public override string ToString()
        {
            var segments = new[]
            {
                !string.IsNullOrWhiteSpace(ClassName) ? $"Class={ClassName}" : null,
                !string.IsNullOrWhiteSpace(MethodName) ? $"Method={MethodName}" : null,
                !string.IsNullOrWhiteSpace(Text) ? $"Text='{Text}'" : null,
            }.Where(item => item != null);
            return "[" + string.Join(", ", segments) + "]";
        }
    }
}