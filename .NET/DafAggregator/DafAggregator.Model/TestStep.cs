﻿using System;

namespace DafAggregator
{
    public sealed class TestStep : IEquatable<TestStep>, IComparable<TestStep>
    {
        public TestStep(long testCaseId, int firstStep = 0, int? lastStep = null)
        {
            TestCaseId = testCaseId;
            FirstStep = firstStep;
            LastStep = lastStep ?? firstStep;
        }

        public long TestCaseId { get; }
        public bool HasSteps => FirstStep != 0;
        public int FirstStep { get; }
        public int LastStep { get; }

        public int CompareTo(TestStep other)
        {
            if (ReferenceEquals(this, other))
                return 0;
            if (ReferenceEquals(null, other))
                return 1;

            var idComparison = TestCaseId.CompareTo(other.TestCaseId);
            if (idComparison != 0)
                return idComparison;

            var firstStepComparison = FirstStep.CompareTo(other.FirstStep);
            if (firstStepComparison != 0)
                return firstStepComparison;

            return LastStep.CompareTo(other.LastStep);
        }
        public bool Equals(TestStep other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return TestCaseId == other.TestCaseId && FirstStep == other.FirstStep && LastStep == other.LastStep;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            return obj is TestStep testCase && Equals(testCase);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = TestCaseId.GetHashCode();
                hashCode = (hashCode * 397) ^ FirstStep;
                hashCode = (hashCode * 397) ^ LastStep;
                return hashCode;
            }
        }
        public override string ToString()
        {
            var result = TestCaseId.ToString();
            if (HasSteps)
            {
                result += ":" + FirstStep;
                if (LastStep != FirstStep)
                    result += "-" + LastStep;
            }
            return result;
        }
    }
}