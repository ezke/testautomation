﻿namespace DafAggregator
{
    public enum TestStatus
    {
        Pass,
        Fail,
        Busted,
    }
}