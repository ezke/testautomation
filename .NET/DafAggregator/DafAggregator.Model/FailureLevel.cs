﻿namespace DafAggregator
{
    public enum FailureLevel
    {
        /// <summary>
        /// Test failure that has not been identified as out-of-the-ordinary.
        /// </summary>
        Fail,

        /// <summary>
        /// The test failed with an avoidable error in the test code that blocks the test
        /// from even running, such as a NullReferenceException.
        /// </summary>
        Busted,
    }
}