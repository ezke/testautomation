﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator
{
    public class TestStatusSet : IEnumerable<TestStatus>
    {
        public static readonly TestStatusSet Empty = new TestStatusSet(Enumerable.Empty<TestStatus>());

        private readonly IReadOnlyList<TestStatus> _values;

        public TestStatusSet([NotNull] IEnumerable<TestStatus> values)
        {
            _values = (values ?? throw new ArgumentNullException(nameof(values)))
                .Distinct()
                .OrderBy(value => value)
                .ToArray();
        }

        public int Count => _values.Count;
        public TestStatus WorstStatus => _values.Any() ? _values.Max() : TestStatus.Pass;

        public static TestStatusSet Union([NotNull] IEnumerable<TestStatusSet> sets)
        {
            var allSets = (sets ?? throw new ArgumentNullException(nameof(sets))).ToList();
            if (allSets.Contains(null))
                throw new ArgumentException("Collection may not contain nulls", paramName: nameof(sets));

            var values = allSets.Aggregate(Enumerable.Empty<TestStatus>(), (acc, nextSet) => acc.Union(nextSet));
            return new TestStatusSet(values);
        }
        public IEnumerator<TestStatus> GetEnumerator()
        {
            return _values.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}