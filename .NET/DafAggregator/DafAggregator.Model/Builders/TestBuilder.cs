﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator.Builders
{
    public class TestBuilder
    {
        [NotNull] private string _className = "";
        [CanBeNull] private Failure _failure;
        [NotNull] private string _methodName = "";
        [NotNull] private IEnumerable<string> _parameters = Enumerable.Empty<string>();
        [NotNull] private TestStep _testStep = new TestStep(0);

        public Test Build()
        {
            var testMethod = new TestMethod(_className, _methodName, _parameters);
            return new Test(testMethod, _testStep, _failure, "");
        }
        public TestBuilder Busted(string exceptionType = "", string message = "",
            params StackTraceEntry[] stackTraceLines)
        {
            return WithFailure(new FailureBuilder(FailureLevel.Busted)
                .WithExceptionType(exceptionType)
                .WithMessage(message)
                .WithStackTrace(stackTraceLines));
        }
        public TestBuilder Failing()
        {
            return WithFailure(new FailureBuilder(FailureLevel.Fail));
        }
        public TestBuilder WithMethod([NotNull] string className, [NotNull] string methodName,
            [NotNull] params string[] parameters)
        {
            _className = className ?? throw new ArgumentNullException(nameof(className));
            _methodName = methodName ?? throw new ArgumentNullException(nameof(methodName));
            _parameters = parameters ?? throw new ArgumentNullException(nameof(parameters));
            return this;
        }
        public TestBuilder WithFailure(FailureBuilder failureBuilder)
        {
            _failure = failureBuilder?.Build();
            return this;
        }
        public TestBuilder WithTestCaseId(long id)
        {
            _testStep = new TestStep(id);
            return this;
        }
        public TestBuilder WithTestCaseAndSteps(long id, int firstStep, int? lastStep = null)
        {
            _testStep = new TestStep(id, firstStep, lastStep);
            return this;
        }
    }
}