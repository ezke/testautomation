﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator.Builders
{
    public class FailureBuilder
    {
        [NotNull] private string _exceptionType = "";
        private readonly FailureLevel _failureLevel;
        [NotNull] private string _message = "";
        [NotNull] private IEnumerable<StackTraceEntry> _stackTraceEntries = Enumerable.Empty<StackTraceEntry>();

        public FailureBuilder(FailureLevel failureLevel)
        {
            _failureLevel = failureLevel;
        }

        public Failure Build()
        {
            return new Failure(_exceptionType, _message, _stackTraceEntries, _failureLevel);
        }
        public FailureBuilder WithExceptionType([NotNull] string exceptionType)
        {
            _exceptionType = exceptionType ?? throw new ArgumentNullException(nameof(exceptionType));
            return this;
        }
        public FailureBuilder WithMessage([NotNull] string message)
        {
            _message = message ?? throw new ArgumentNullException(nameof(message));
            return this;
        }
        public FailureBuilder WithStackTrace([NotNull] params StackTraceEntry[] stackTraceEntries)
        {
            _stackTraceEntries = stackTraceEntries ?? throw new ArgumentNullException(nameof(stackTraceEntries));
            return this;
        }
    }
}