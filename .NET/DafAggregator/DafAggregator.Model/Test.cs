﻿using System;
using JetBrains.Annotations;

namespace DafAggregator
{
    public class Test
    {
        public Test([NotNull] TestMethod testMethod, [NotNull] TestStep testStep,
            [CanBeNull] Failure failure, [NotNull] string screenshotPath)
        {
            TestMethod = testMethod ?? throw new ArgumentNullException(nameof(testMethod));
            TestStep = testStep ?? throw new ArgumentNullException(nameof(testStep));
            Failure = failure;
            ScreenshotPath = screenshotPath ?? throw new ArgumentNullException(nameof(screenshotPath));

            Status = Failure == null
                ? TestStatus.Pass
                : Failure.FailureLevel == FailureLevel.Fail
                    ? TestStatus.Fail
                    : TestStatus.Busted;
        }

        [NotNull]
        public TestMethod TestMethod { get; }
        [NotNull]
        public TestStep TestStep { get; }
        [CanBeNull]
        public Failure Failure { get; }
        [NotNull]
        public string ScreenshotPath { get; }
        public TestStatus Status { get; }
    }
}