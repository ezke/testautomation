﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace DafAggregator
{
    [DebuggerDisplay("{" + nameof(Inspect) + "(),nq}")]
    public class TestMethod
    {
        public TestMethod([NotNull] string className, [NotNull] string methodName,
            [NotNull, ItemNotNull] IEnumerable<string> parameters)
        {
            ClassName = className ?? throw new ArgumentNullException(nameof(className));
            MethodName = methodName ?? throw new ArgumentNullException(nameof(methodName));
            Parameters = (parameters ?? throw new ArgumentNullException(nameof(parameters))).ToList();

            if (Parameters.Contains(null))
                throw new ArgumentException("Collection may not contain nulls", paramName: nameof(parameters));
        }

        [NotNull]
        public string ClassName { get; }
        [NotNull]
        public string MethodName { get; }
        [NotNull, ItemNotNull]
        public IReadOnlyList<string> Parameters { get; }

        [UsedImplicitly]
        private string Inspect()
        {
            var parts = new List<string>
            {
                $"{nameof(ClassName)}: '{ClassName}'",
                $"{nameof(MethodName)}: '{MethodName}'",
            };
            parts.AddRange(Parameters.Select((value, index) => $"{nameof(Parameters)}[{index}]: '{value}'"));

            return string.Join(", ", parts);
        }
    }
}