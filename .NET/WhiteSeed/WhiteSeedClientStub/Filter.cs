﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhiteSeed;

namespace WhiteSeedClientStub
{
    public class Filter : FormElementFilter
    {
        public Filter(FormElementAttributes field, string value)
        {
            Field = field;
            Value = value;
        }
    }
}
