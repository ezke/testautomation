﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhiteSeedClientStub
{
    public partial class dlgImageDisplay : Form
    {
        private Image _screenshot;
        public dlgImageDisplay(Image screenshot)
        {
            _screenshot = screenshot;
            InitializeComponent();
        }

        private void pnlDisplay_Paint(object sender, PaintEventArgs e)
        {
            var pictureSize = OptimizeSize(_screenshot.Size, pnlDisplay.Size);
            e.Graphics.DrawImage(_screenshot, LayoutSize(pictureSize, pnlDisplay.Size), new Rectangle(new Point(0,0), _screenshot.Size), GraphicsUnit.Pixel);
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private Size OptimizeSize(Size pictureSize, Size frameSize)
        {
            var newSize = pictureSize;
            var ratios = new []
            {
                (double)frameSize.Width / (double)pictureSize.Width,
                (double)frameSize.Height / (double)pictureSize.Height
            };
            var ratio = ratios[0] < ratios[1] ? ratios[0] : ratios[1];
            
            return new Size((int)(pictureSize.Width * ratio), (int)(pictureSize.Height * ratio));
        }

        private Rectangle LayoutSize(Size pictureSize, Size frameSize)
        {
            var location = new Point(
                (int)((frameSize.Width - pictureSize.Width) * 0.5),
                (int)((frameSize.Height - pictureSize.Height) * 0.5)
            );
            return new Rectangle(location, pictureSize);
        }

        private void pnlDisplay_SizeChanged(object sender, EventArgs e)
        {
            pnlDisplay.Invalidate();
        }
    }
}
