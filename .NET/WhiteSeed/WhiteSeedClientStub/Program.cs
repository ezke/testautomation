﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using WhiteSeed;

namespace WhiteSeedClientStub
{
    class Program
    {
        static void Main(string[] args)
        {
            var binding = new BasicHttpBinding();
            var exit = false;
            var testHub = UpdateLink_Test();
            int applicationId = -1;

            var commands = new List<Command>();
            commands.AddRange(new[]
            {
                new Command(
                    "help",
                    "",
                    arg =>
                    {
                        var sortedCommands = commands.OrderBy(c => c.Keyword, StringComparer.CurrentCultureIgnoreCase);
                        Console.WriteLine("Available commands:");
                        foreach (var command in sortedCommands)
                        {
                            Console.Write("  " + command.Keyword + " ");
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write(command.Args);
                            Console.ResetColor();
                            Console.WriteLine();
                        }
                    }),
                new Command(
                    "open",
                    "<exe-path>",
                    arg => { applicationId = LaunchAndAttachApplication(testHub, arg); }
                ),
                new Command(
                    "close",
                    "",
                    arg =>
                    {
                        // Make sure we exit after handling the command, even if we get an error
                        // (e.g. if the server has stopped)
                        exit = true;
                        
                        if (applicationId != -1)
                        {
                            testHub.CloseApplication(applicationId);
                        }
                    }
                ),
                new Command(
                    "click target",
                    "<automation-id>",
                    arg =>
                    {
                        testHub.ElementAction(applicationId,
                            new[] {new FormElementFilter {Field = FormElementAttributes.AutomationID, Value = arg}},
                            FormElementActions.Click);
                    }
                ),
                new Command(
                    "click by handle",
                    "<window-handle>",
                    arg =>
                    {
                        testHub.ElementAction(applicationId,
                            new[] {new FormElementFilter {Field = FormElementAttributes.WindowHandle, Value = arg}},
                            FormElementActions.Click);
                    }
                ),
                new Command(
                    "wait for",
                    "<automation-id>",
                    arg =>
                    {
                        testHub.WaitForElement(applicationId,
                            new[] {new FormElementFilter {Field = FormElementAttributes.AutomationID, Value = arg}},
                            15);
                    }
                ),
                new Command(
                    "read text",
                    "<automation-id>",
                    arg =>
                    {
                        Console.WriteLine(
                            $">> {testHub.QueryElement(applicationId, new[] {new FormElementFilter {Field = FormElementAttributes.AutomationID, Value = arg}}, FormElementAttributes.Text)}");
                    }
                ),
                new Command(
                    "read table data",
                    "<table-automation-id>",
                    arg =>
                    {
                        Console.WriteLine(
                            FormateTableData(testHub.QueryTableMetaData(applicationId,
                                new[]
                                {
                                    new FormElementFilter {Field = FormElementAttributes.AutomationID, Value = arg}
                                })));
                    }
                ),
                new Command(
                    "read cell",
                    "<grid-automation-id> <column-name> <row-index>",
                    arg =>
                    {
                        var cmdArgs = BreakCommandParts(arg);
                        var filter = new[]
                        {
                            new FormElementFilter {Field = FormElementAttributes.AutomationID, Value = cmdArgs[0]}
                        };

                        Console.WriteLine(
                            $">> {testHub.QueryTableCell(applicationId, filter, cmdArgs[1], int.Parse(cmdArgs[2]), FormElementAttributes.Text)}");
                    }
                ),
                new Command(
                    "type into cell",
                    "<grid-automation-id> <column-name> <row-index> <text>",
                    arg =>
                    {
                        var cmdArgs = BreakCommandParts(arg);
                        var filter = new[]
                        {
                            new FormElementFilter
                            {
                                Field = FormElementAttributes.AutomationID,
                                Value = cmdArgs[0]
                            }
                        };
                        testHub.TableCellType(applicationId, filter, cmdArgs[1], int.Parse(cmdArgs[2]), cmdArgs[3]);
                    }
                ),
                new Command(
                    "type into element",
                    "<automation-id> <text>",
                    arg =>
                    {
                        var cmdArgs = BreakElementCommandParts(arg);
                        var filter = new[]
                        {
                            new FormElementFilter
                            {
                                Field = FormElementAttributes.AutomationID,
                                Value = cmdArgs[0]
                            }
                        };
                        testHub.ElementType(applicationId, filter, cmdArgs[1]);
                    }
                ),
                new Command(
                    "screenshot",
                    "",
                    arg =>
                    {
                        var display = new dlgImageDisplay(ByteArrayToImage(testHub.GetScreenShot(applicationId)));
                        display.ShowDialog();
                    }
                ),
                new Command(
                    "focus window",
                    "<window-type>",
                    arg =>
                    {
                        testHub.SetCurrentWindow(applicationId, new[]
                        {
                            new FormElementFilter
                            {
                                Field = FormElementAttributes.Type,
                                Value = arg
                            }
                        });
                    }
                ),
                new Command(
                    "list windows",
                    "",
                    arg => { Console.WriteLine(string.Join("\n", testHub.ListWindows(applicationId))); }
                ),
                new Command(
                    "attach",
                    "<executable>",
                    arg => { applicationId = testHub.AttachApplication(arg.Replace("\"", ""), 2.0); }),
                new Command(
                    "type",
                    "<text>",
                    arg => { testHub.TypeOnKeyboard(applicationId, arg); }),
                new Command(
                    "xtype",
                    "<text-with-special-keys>",
                    arg => { testHub.TypeOnKeyboardWithSpecialKeys(applicationId, arg); }),
                new Command(
                    "list elements",
                    "",
                    arg =>
                    {
                        var elements = testHub.GetElementInfo(applicationId, new FormElementFilter[] { })
                            .OrderBy(element => element.ScreenY)
                            .ThenBy(element => element.ScreenX)
                            .ToList();
                        Console.WriteLine($"Elements: (count={elements.Count})");
                        var headers = new[]
                        {
                            "UIAClass",
                            "WhiteClass",
                            "Name (text)",
                            "Id",
                            "Bounds",
                            "Hwnd",
                            "Notes",
                        };
                        var table = new[] {headers}.Concat(elements.Select(element => new[]
                        {
                            element.UiAutomationClassName,
                            element.WhiteClassName,
                            '"' + element.Name + '"',
                            element.Id,
                            $"{element.ScreenX},{element.ScreenY} {element.Width}x{element.Height}",
                            $"0x{element.WindowHandle:x}",
                            string.Join(",", new[]
                            {
                                element.Visible ? "" : "Hidden",
                                element.Enabled ? "" : "Disabled",
                                element.IsOffscreen ? "Offscreen" : "",
                                element.CheckState == CheckState.Checked ? "Checked" : "",
                                element.CheckState == CheckState.Indeterminate ? "Indeterminate" : "",
                            }.Where(s => !string.IsNullOrEmpty(s))),
                        }));
                        WriteTable(table.ToList());
                    }),
            });

            Console.WriteLine("WhiteSeedClientStub started.");
            Console.WriteLine("Enter commands, 'help' for help, or 'close' to exit.");
            while (!exit)
            {
                try
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("Seed> ");
                    Console.ResetColor();
                    ParseLine(Console.ReadLine(), commands);
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR: " + ex);
                    Console.ResetColor();
                }
            }

            Console.WriteLine("WhiteSeedClientStub exiting.");
        }

        private static void WriteTable(IReadOnlyList<string[]> rows)
        {
            if (rows.Count == 0)
                return;

            var columnWidths = new int[rows[0].Length];
            foreach (var row in rows)
            {
                for (var columnIndex = 0; columnIndex < columnWidths.Length; ++columnIndex)
                {
                    columnWidths[columnIndex] = Math.Max(columnWidths[columnIndex], row[columnIndex].Length);
                }
            }

            Console.ForegroundColor = ConsoleColor.Cyan;
            foreach (var row in rows)
            {
                var cells = row.Zip(columnWidths, (text, width) => text.PadRight(width));
                Console.WriteLine(string.Join("  ", cells));
                Console.ResetColor();
            }
        }

        private static int LaunchAndAttachApplication(IRoots testHub, string command)
        {
            var engine = new Regex($"(?<command>(\"[^\"]+\"|[^ ])+)(\\s+(?<arg>[^ ]+))*");
            var match = engine.Match(command);

            if (match.Success)
            {
                return testHub.LaunchAndAttachApplication(match.Groups["command"].Value.Replace("\"", ""),
                    match.Groups["arg"].Captures.Cast<Capture>().Select(capture => capture.Value).ToArray());
            }
            else
            {
                var errorMessage = "Can not parse commandline";
                Console.WriteLine(errorMessage);
                throw new Exception(errorMessage);
            }
        }

        private static Image ByteArrayToImage(byte[] data)
        {
            return Image.FromStream(new MemoryStream(data));
        }

        private static string FormateTableData(FormTableMetaData tableData)
        {
            string columns = $"\t{string.Join("\n\t", tableData.Columns)}";
            return $"Columns:\n{columns}\n\nRow Count: {tableData.RowCount}";
        }

        private static List<string> BreakElementCommandParts(string value)
        {
            var engine = new Regex("(?<elementId>[^\\s]+)\\s+(?<text>.+)");
            var match = engine.Match(value);
            return new List<string>
            {
                match.Groups["elementId"].Value,
                match.Groups["text"].Value
            };
        }

        private static List<string> BreakCommandParts(string value)
        {
            var engine =
                new Regex("(?<gridId>[^\\s]+)\\s+(?<columnName>.*?)\\s+(?<rowIndex>\\d+)(\\s(?<text>[^\\s]+))?");
            var match = engine.Match(value);
            return new List<string>
            {
                match.Groups["gridId"].Value,
                match.Groups["columnName"].Value,
                match.Groups["rowIndex"].Value,
                match.Groups["text"]?.Value
            };
        }

        private static void ParseLine(string line, IReadOnlyList<Command> commands)
        {
            var commandsByKeyword = commands.ToDictionary(command => command.Keyword);
            var engine = new Regex($"(?i)(?<command>({string.Join("|", commandsByKeyword.Keys)}))( (?<arg>.*))?");
            var match = engine.Match(line);

            if (match.Success)
            {
                commandsByKeyword[match.Groups["command"].Value.ToLower()].Invoke(match.Groups["arg"].Value);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Unrecognized command. Type 'help' for a list of commands.");
                Console.ResetColor();
            }
        }

        private static IRoots UpdateLink_Test()
        {
            BasicHttpBinding bndLink = new BasicHttpBinding();
            bndLink.MessageEncoding = WSMessageEncoding.Mtom;
            ChannelFactory<IRoots> scf = new ChannelFactory<IRoots>(
                bndLink, System.Configuration.ConfigurationManager.AppSettings["HostURL"]);
            return scf.CreateChannel();
        }
    }
}