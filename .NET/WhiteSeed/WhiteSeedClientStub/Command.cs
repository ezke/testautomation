﻿using System;

namespace WhiteSeedClientStub
{
    public class Command
    {
        private readonly Action<string> _action;

        public Command(string keyword, string args, Action<string> action)
        {
            _action = action;
            Keyword = keyword;
            Args = args;
        }

        public string Args { get; }
        public string Keyword { get; }

        public void Invoke(string arg)
        {
            _action(arg);
        }
    }
}