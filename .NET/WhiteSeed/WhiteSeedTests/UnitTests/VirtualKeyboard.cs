﻿using System;
using System.Collections.Generic;
using TestStack.White.InputDevices;
using TestStack.White.WindowsAPI;

namespace WhiteSeedTests.UnitTests
{
    public class VirtualKeyboard:IKeyboard
    {
        public List<KeyBlock> History { get; }

        public VirtualKeyboard()
        {
            History = new List<KeyBlock>();
        }

        public void Enter(string keysToType)
        {
            History.Add(KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, keysToType));
        }

        public void PressSpecialKey(KeyboardInput.SpecialKeys key)
        {
            History.Add(KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.SPECIAL, Enum.GetName(typeof(KeyboardInput.SpecialKeys), key)));
        }

        public void HoldKey(KeyboardInput.SpecialKeys key)
        {
            History.Add(KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, Enum.GetName(typeof(KeyboardInput.SpecialKeys), key)));
        }

        public void LeaveKey(KeyboardInput.SpecialKeys key)
        {
            History.Add(KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, Enum.GetName(typeof(KeyboardInput.SpecialKeys), key)));
        }

        public bool CapsLockOn { get; set; }
    }
}
