﻿using System;
using NUnit.Framework;
using TestStack.White;
using WhiteSeedDriver;

namespace WhiteSeedTests.UnitTests
{
    [TestFixture]
    public class ApplicationLaunchAndAttachTests
    {
        private const bool STILL_TIME_REMAINING = false;
        private const bool TIMEDOUT = true;

        [Test]
        public void WaitForApplicationToLaunchThenAttach()
        {
            var appName = "test.app";
            TestProcessManager processManager = new TestProcessManager();
            var stopWatch = new TestStopWatch(cycleCount =>
            {
                //Find process before timing out
                switch (cycleCount)
                {
                    case 2:
                        processManager.Processes.Add(appName);
                        return STILL_TIME_REMAINING;
                    case 3:
                        Assert.Fail("The process has already reported success.  It should not be checked again.");
                        return TIMEDOUT;
                    default:
                        return STILL_TIME_REMAINING;
                }
            });
            var whiteEngine = new WhiteSeedEngine(processManager);
            Func<bool> hasTimedoutFunction = () => stopWatch.Expired;
            
            var applicationHandle = whiteEngine.AttachApplicationInternal(appName, hasTimedoutFunction, TimeSpan.FromSeconds(0.0));
            Assert.That(applicationHandle, Is.GreaterThanOrEqualTo(0), $"Application Handle");
        }

        [Test]
        public void TimeoutWhileWaitingForApplicationToLaunch()
        {
            var appName = "test.app";
            TestProcessManager processManager = new TestProcessManager();
            var stopWatch = new TestStopWatch(cycleCount =>
            {
                //Timing out before finding process
                switch (cycleCount)
                {
                    case 2:
                        return TIMEDOUT;
                    case 3:
                        processManager.Processes.Add(appName);
                        return STILL_TIME_REMAINING;
                    default:
                        return STILL_TIME_REMAINING;
                }
            });
            var whiteEngine = new WhiteSeedEngine(processManager);
            Func<bool> hasTimedoutFunction = () => stopWatch.Expired;

            try
            {
                whiteEngine.AttachApplicationInternal(appName, hasTimedoutFunction, TimeSpan.FromSeconds(0.0));
            }
            catch (WhiteException)
            {
                Assert.Pass();
            }
            Assert.Fail("The process was not found in allotted time and yet the process did not timeout.");
        }
    }
}
