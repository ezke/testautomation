﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSeedTests.UnitTests
{
    public class TestStopWatch
    {
        private int _checkCount = 0;
        private Func<int, bool> _expireBasedOnCycleCount;

        public TestStopWatch(Func<int, bool> expireBasedOnCycleCount)
        {
            _expireBasedOnCycleCount = expireBasedOnCycleCount;
        }

        public bool Expired
        {
            get
            {
                _checkCount++;
                return _expireBasedOnCycleCount(_checkCount);
            }
        }
    }
}
