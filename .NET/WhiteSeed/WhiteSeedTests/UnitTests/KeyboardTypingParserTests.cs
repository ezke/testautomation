﻿using NUnit.Framework;
using WhiteSeedDriver;

namespace WhiteSeedTests.UnitTests
{
    [TestFixture]
    public class KeyboardTypingParserTests
    {
        [Test]
        public void StraightKeyingTest()
        {
            var textToType = "Hello World";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, textToType)
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }

        [Test]
        public void SpecialKeyTest()
        {
            var textToType = "<F1 />";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.SPECIAL, "F1")
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }

        [Test]
        public void KeyCombinationTest()
        {
            var textToType = "<CONTROL>X</CONTROL>";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "CONTROL"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, "X"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "CONTROL")
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }

        [Test]
        public void ComplexKeyCombinationTest()
        {
            var textToType = "<CONTROL><ALT><DELETE /></ALT></CONTROL>";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "CONTROL"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "ALT"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.SPECIAL, "DELETE"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "ALT"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "CONTROL")
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }

        [Test]
        public void MixedTypingAndKeyCombinationTest()
        {
            var textToType = "Copy This Text<CONTROL>A</CONTROL><CONTROL>C</CONTROL>";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, "Copy This Text"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "CONTROL"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, "A"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "CONTROL"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "CONTROL"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, "C"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "CONTROL")
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }

        [Test]
        public void KeyNameCaseInsensitivityTest()
        {
            var textToType = "<alt>f</alt>";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "ALT"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.NORMAL, "f"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "ALT")
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }

        [Test]
        public void KeyNameAliasTest()
        {
            var textToType = "<ctl><enter/></ctl>";
            var expected = new KeyBlock[]
            {
                KeyBlock.CreateKeyBlock(KeyStrokeType.HOLD, KeyType.SPECIAL, "CONTROL"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.ENTER, KeyType.SPECIAL, "RETURN"),
                KeyBlock.CreateKeyBlock(KeyStrokeType.RELEASE, KeyType.SPECIAL, "CONTROL")
            };
            var keyboard = new VirtualKeyboard();
            KeyboardTypingParser.TypeSpecialKeys(textToType, keyboard);
            Assert.AreEqual(expected, keyboard.History.ToArray());
        }
    }
}
