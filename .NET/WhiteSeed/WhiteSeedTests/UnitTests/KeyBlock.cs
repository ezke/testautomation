﻿namespace WhiteSeedTests.UnitTests
{
    public class KeyBlock
    {
        protected bool Equals(KeyBlock other)
        {
            return KeyStrokeType == other.KeyStrokeType && KeyType == other.KeyType && string.Equals(Key, other.Key);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((KeyBlock) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) KeyStrokeType;
                hashCode = (hashCode*397) ^ (int) KeyType;
                hashCode = (hashCode*397) ^ (Key != null ? Key.GetHashCode() : 0);
                return hashCode;
            }
        }

        private KeyBlock(KeyStrokeType keyStrokeType, KeyType keyType, string key)
        {
            KeyStrokeType = keyStrokeType;
            KeyType = keyType;
            Key = key;
        }
        public KeyStrokeType KeyStrokeType { get; set; }
        public KeyType KeyType { get; set; }
        public string Key { get; set; }

        public static KeyBlock CreateKeyBlock(KeyStrokeType keyStrokeType, KeyType keyType, string key)
        {
            return new KeyBlock(keyStrokeType, keyType, key);
        }
    }
}
