﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WhiteSeedDriver;

namespace WhiteSeedTests.UnitTests
{
    internal class TestProcessManager : IProcessManager
    {
        public Process[] GetProcessesByNameAndSession(string appPath)
        {
            return
                Processes.Where(item => item.Equals(appPath, StringComparison.InvariantCultureIgnoreCase))
                    .Select(item => new Process() { StartInfo = new ProcessStartInfo(item) })
                    .ToArray();
        }

        public List<string> Processes { get; set; }

        public TestProcessManager()
        {
            Processes = new List<string>();
        }
    }
}