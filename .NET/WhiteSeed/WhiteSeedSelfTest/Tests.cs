﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhiteSeed;
using static WhiteSeed.FormElementActions;
using static WhiteSeed.FormElementAttributes;
using static WhiteSeed.FormElementFilter;

namespace WhiteSeedSelfTest
{
    public class Tests:BaseTestClass
    {
        [SelfTest]
        public void ClickButtonAndTypeTextTest(frmActivityPanel form)
        {
            var expected =
                EventHistoryBuilder.StartWith(EventData.CreateEventData(form.ClickMeButton, EventType.Click, new EventArgs()))
                .ThenType(form.TypeHereTextBox, "Hello\u0001")
                .Build();
            _whiteSeedRoots.ElementAction(
                _applicationID, 
                new [] {CreateFilter(AutomationID, form.ClickMeButton.Name)}, 
                Click);
            _whiteSeedRoots.ElementTypeWithSpecialKeys(
                _applicationID,
                new[] { CreateFilter(AutomationID, form.TypeHereTextBox.Name)}, 
                "Hello<ctrl>a</ctrl>");
            form.History.AssertListContains("Event History", expected);
        }

        [SelfTest]
        public void ValidateButtonText(frmActivityPanel form)
        {
            _whiteSeedRoots.QueryElement(
                _applicationID,
                new [] {CreateFilter(AutomationID, form.ClickMeButton.Name)},
                FormElementAttributes.Text).AssertEqual(form.ClickMeButton.Text);
        }

        [SelfTest]
        public void SetFocusOnControlTest(frmActivityPanel form)
        {
            var expected =
                EventHistoryBuilder.StartWith(EventData.CreateEventData(form.ClickMeButton, EventType.Focused,
                    new EventArgs()))
                .Build();
            _whiteSeedRoots.ElementAction(
                _applicationID, 
                new [] {CreateFilter(AutomationID, form.ClickMeButton.Name)}, 
                Focus);
            form.History.AssertListContains("Event History", expected);
        }

        [SelfTest]
        public void OverwriteTextInTextbox(frmActivityPanel form)
        {
            var textForBox = "Good Bye";
            _whiteSeedRoots.ElementClear(
                _applicationID, new [] {CreateFilter(AutomationID, form.TypeHereTextBox.Name)});
            _whiteSeedRoots.ElementType(
                _applicationID, 
                new [] {CreateFilter(AutomationID, form.TypeHereTextBox.Name)}, 
                textForBox);
            form.TypeHereTextBox.Text.AssertEqual(textForBox);
        }

        [SelfTest]
        public void FilterOutDisabledControls(frmActivityPanel form)
        {
            var controlFound = true;
            try
            {
                var controlName = _whiteSeedRoots.QueryElement(_applicationID,
                    new [] {CreateFilter(AutomationID, form.DisabledButton.Name), CreateFilter(FormElementAttributes.Enabled, "true")}, AutomationID);
                
            }
            catch (FaultException<UnknownFault>)
            {
                //If the test is working this is where the process should go
                controlFound = false;
            }
            controlFound.AssertFalse("Disabled control is found");
        }

        [SelfTest]
        public void FilterInDisabledControls(frmActivityPanel form)
        {
            var controlName = _whiteSeedRoots.QueryElement(_applicationID,
                new [] {CreateFilter(AutomationID, form.DisabledButton.Name), CreateFilter(FormElementAttributes.Enabled, "false")}, AutomationID);
            controlName.AssertEqual(form.DisabledButton.Name);
        }
    }
}
