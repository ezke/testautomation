﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSeedSelfTest
{
    public static class Validations
    {
        public static bool CompareLists<T>(this List<T> list1, List<T> list2)
        {
            return (list1.Count == list2.Count) && (list1.Zip(list2, (one, two) => one.Equals(two)).All(item => item));
        }

        public static bool ListContains<T>(this List<T> list, T value)
        {
            return list.Any(item => item.Equals(value));
        }
        public static bool ListContains<T>(this List<T> list1, List<T> list2)
        {
            return list2.All(item => list1.ListContains(item));
        }

        public static void AssertListContains<T>(this List<T> list1, string nameOfList, T value)
        {
            if (!list1.ListContains(value))
            {
                throw new Exception($"Unable to find item ({value}) in {nameOfList}");
            }
        }

        public static void AssertListContains<T>(this List<T> list1, string nameOfList, List<T> list2)
        {
            list2.ForEach(item=>list1.AssertListContains(nameOfList, item));
        }

        public static void AssertEqual(this string value, string expectedValue)
        {
            if (!expectedValue.Equals(value))
            {
                throw new Exception($"Value [{value}] does not match expected value [{expectedValue}].");
            }
        }

        public static void AssertTrue(this bool value, string descriptionOfValue)
        {
            if (!value)
            {
                throw new Exception($"{descriptionOfValue} should be true but is not.");
            }
        }
        
        public static void AssertFalse(this bool value, string descriptionOfValue)
        {
            if (value)
            {
                throw new Exception($"{descriptionOfValue} should not be true but is.");
            }
        }
    }
}
