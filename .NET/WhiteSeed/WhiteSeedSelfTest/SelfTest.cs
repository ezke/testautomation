﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSeedSelfTest
{
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class SelfTest : System.Attribute
    {
        public SelfTest() { }
    }
}
