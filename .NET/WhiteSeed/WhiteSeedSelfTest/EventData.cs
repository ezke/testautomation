﻿using System;
using System.Windows.Forms;

namespace WhiteSeedSelfTest
{
    public class EventData
    {
        protected bool Equals(EventData other)
        {
            return EventType == other.EventType && Equals(Control, other.Control) && CompareEventArgs(EventArgs, other.EventArgs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((EventData) obj);
        }

        private bool CompareEventArgs(EventArgs arg1, EventArgs arg2)
        {
            if (arg1 is KeyPressEventArgs)
            {
                return ((KeyPressEventArgs) arg1).KeyChar == ((KeyPressEventArgs) arg2).KeyChar;
            }
            else if (arg1 != null)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) EventType;
                hashCode = (hashCode*397) ^ (Control != null ? Control.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (EventArgs != null ? EventArgs.GetHashCode() : 0);
                return hashCode;
            }
        }

        public EventType EventType { get; set; }
        public Control Control { get; set; }

        public static EventData CreateEventData(Control control, EventType eventType, EventArgs args)
        {
            return new EventData()
            {
                Control = control,
                EventType = eventType,
                EventArgs = args
            };
        }

        public override string ToString()
        {
            switch (EventType)
            {
                case EventType.Type:
                    var args = (KeyPressEventArgs)EventArgs;
                    return $"Typed \"{args.KeyChar}\" into {Control.Name}";

                case EventType.Focused:
                    return $"Focused on {Control.Name}";

                default:
                    return $"{EventType} on {Control.Name}";
            }
        }

        public EventArgs EventArgs { get; set; }
    }
}