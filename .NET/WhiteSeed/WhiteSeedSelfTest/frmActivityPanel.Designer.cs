﻿namespace WhiteSeedSelfTest
{
    partial class frmActivityPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdClickMe = new System.Windows.Forms.Button();
            this.txtTypeHere = new System.Windows.Forms.TextBox();
            this.cmdRunTest = new System.Windows.Forms.Button();
            this.cmdDisabled = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Controls.Add(this.cmdClose, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmdRunTest, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(388, 311);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdClose
            // 
            this.cmdClose.Location = new System.Drawing.Point(311, 286);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(74, 22);
            this.cmdClose.TabIndex = 0;
            this.cmdClose.Text = "Close";
            this.cmdClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.cmdClickMe, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtTypeHere, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmdDisabled, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(382, 277);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // cmdClickMe
            // 
            this.cmdClickMe.Location = new System.Drawing.Point(3, 3);
            this.cmdClickMe.Name = "cmdClickMe";
            this.cmdClickMe.Size = new System.Drawing.Size(75, 23);
            this.cmdClickMe.TabIndex = 0;
            this.cmdClickMe.Text = "Click Me";
            this.cmdClickMe.UseVisualStyleBackColor = true;
            this.cmdClickMe.Click += new System.EventHandler(this.Control_Click);
            this.cmdClickMe.Enter += new System.EventHandler(this.Control_Enter);
            // 
            // txtTypeHere
            // 
            this.txtTypeHere.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTypeHere.Location = new System.Drawing.Point(84, 3);
            this.txtTypeHere.Multiline = true;
            this.txtTypeHere.Name = "txtTypeHere";
            this.tableLayoutPanel2.SetRowSpan(this.txtTypeHere, 2);
            this.txtTypeHere.Size = new System.Drawing.Size(295, 187);
            this.txtTypeHere.TabIndex = 1;
            this.txtTypeHere.Enter += new System.EventHandler(this.Control_Enter);
            this.txtTypeHere.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTypeHere_KeyPress);
            // 
            // cmdRunTest
            // 
            this.cmdRunTest.Location = new System.Drawing.Point(3, 286);
            this.cmdRunTest.Name = "cmdRunTest";
            this.cmdRunTest.Size = new System.Drawing.Size(74, 22);
            this.cmdRunTest.TabIndex = 2;
            this.cmdRunTest.Text = "Run Test";
            this.cmdRunTest.UseVisualStyleBackColor = true;
            this.cmdRunTest.Click += new System.EventHandler(this.cmdRunTest_Click);
            // 
            // cmdDisabled
            // 
            this.cmdDisabled.Enabled = false;
            this.cmdDisabled.Location = new System.Drawing.Point(84, 196);
            this.cmdDisabled.Name = "cmdDisabled";
            this.cmdDisabled.Size = new System.Drawing.Size(75, 23);
            this.cmdDisabled.TabIndex = 2;
            this.cmdDisabled.Text = "Disabled";
            this.cmdDisabled.UseVisualStyleBackColor = true;
            // 
            // frmActivityPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 311);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frmActivityPanel";
            this.Text = "Activity Panel";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button cmdClickMe;
        private System.Windows.Forms.TextBox txtTypeHere;
        private System.Windows.Forms.Button cmdRunTest;
        private System.Windows.Forms.Button cmdDisabled;
    }
}

