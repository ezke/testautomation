﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhiteSeedSelfTest
{
    public class EventHistoryBuilder
    {
        protected EventData[] _history;
        public List<EventData> Build()
        {
            return _history.ToList();
        }

        public EventHistoryBuilder ThenClick(Control control)
        {
            return AppendEvent(EventData.CreateEventData(control, EventType.Click, new EventArgs()));
        }

        public EventHistoryBuilder ThenType(Control control, string typedText)
        { 
            return AppendEvents(StringToEventData(control, typedText));
        }

        private EventHistoryBuilder AppendEvents(EventData[] events)
        {
            var currentBuilder = this;
            foreach (var item in events)
            {
                currentBuilder = currentBuilder.AppendEvent(item);
            }
            return currentBuilder;
        }

        private EventHistoryBuilder AppendEvent(EventData eventData)
        {
            return new EventHistoryBuilder()
            {
                _history = this._history.Concat(new [] { eventData }).ToArray()
            };
        }

        private static EventData[] StringToEventData(Control control, string typedText)
        {
            return typedText.ToCharArray()
                .Select(letter => (EventArgs)new KeyPressEventArgs(letter))
                .Select(item => EventData.CreateEventData(control, EventType.Type, item))
                .ToArray();
        }

        public static EventHistoryBuilder StartTyping(Control control, string typedText)
        {
            var result = new EventHistoryBuilder();
            return result.AppendEvents(StringToEventData(control, typedText));
        }

        public static EventHistoryBuilder StartWith(EventData eventData)
        {
            return new EventHistoryBuilder()
            {
                _history = new EventData[] {eventData}
            };
        }
    }
}
