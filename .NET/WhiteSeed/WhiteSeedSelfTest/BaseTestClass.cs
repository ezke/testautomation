﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WhiteSeed;

namespace WhiteSeedSelfTest
{
    public abstract class BaseTestClass
    {
        protected IRoots _whiteSeedRoots { get; set; }
        protected int _applicationID;

        public void LinkToForm()
        {
            _whiteSeedRoots = UpdateLink_Test();
            _applicationID = _whiteSeedRoots.AttachApplication(Process.GetCurrentProcess().ProcessName, 20.0);
        }

        private IRoots UpdateLink_Test()
        {

            BasicHttpBinding bndLink = new BasicHttpBinding();
            bndLink.MessageEncoding = WSMessageEncoding.Mtom;
            ChannelFactory<IRoots> scf = new ChannelFactory<IRoots>(
                    bndLink, "http://localhost:9995/Seed");
            return scf.CreateChannel();
        }
    }
}
