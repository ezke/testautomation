﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using WhiteSeed;

namespace WhiteSeedSelfTest
{
    public partial class frmActivityPanel : Form
    {
        public List<Action<frmActivityPanel>> Tests { get; set; }
        private BaseTestClass _testObject;
        public frmActivityPanel(BaseTestClass tests)
        {
            History = new List<EventData>();
            Tests = new List<Action<frmActivityPanel>>();
            _testObject = tests;
            Tests.AddRange(_testObject.GetType().GetMethods()
                    .Where(IsTestSignature)
                    .Select(TestMethodToAction));
            InitializeComponent();
        }

        private Action<frmActivityPanel> TestMethodToAction(MethodInfo method)
        {
            return (Action<frmActivityPanel>)Delegate.CreateDelegate(
                typeof(Action<frmActivityPanel>),
                _testObject,
                method
             );
        }

        private bool IsTestSignature(MethodInfo method)
        {
            return method.GetCustomAttributes(typeof(SelfTest), false).Any();
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public List<EventData> History { get; }

        public Button ClickMeButton => cmdClickMe;
        public TextBox TypeHereTextBox => txtTypeHere;

        public Button DisabledButton => cmdDisabled;

        private void Control_Click(object sender, EventArgs e)
        {
            History.Add(EventData.CreateEventData((Control) sender, EventType.Click, e));
        }

        private void txtTypeHere_KeyPress(object sender, KeyPressEventArgs e)
        {
            History.Add(EventData.CreateEventData(txtTypeHere, EventType.Type, e));
        }

        private void cmdRunTest_Click(object sender, EventArgs e)
        {
            var thread = new Thread(TestEngine);
            thread.Start();
        }

        private void TestEngine()
        {
            bool successful = true;
            List<string> successMessage = new List<string>();

            _testObject.LinkToForm();

            foreach (var test in Tests)
            {
                try
                {
                    History.Clear();
                    test(this);
                    successMessage.Add($"Passed...{test.Method.Name}");
                }
                catch (Exception ex)
                {
                    successful = false;
                    DisplayResults($"Test {test.Method.Name} Failed:\r\n{ex.Message}", true);
                    break;
                }
            }
            if (successful)
            {
                DisplayResults($"All tests passed:\r\n{string.Join("\r\n", successMessage)}", false);
            }
        }

        public void CloseMe()
        {
            var act = new Action(() => this.Close());
            if (InvokeRequired)
            {
                this.Invoke(act);
            }
            else
            {
                this.Close();
            }
        }

        public void DisplayResults(string message, bool failed)
        {
            var act = new Action(() =>
            {
                this.txtTypeHere.Text = message;
                this.txtTypeHere.ForeColor = failed ? Color.Red : Color.Green;
            });
            if (InvokeRequired)
            {
                this.Invoke(act);
            }
            else
            {
                act();
            }
        }

        private void Control_Enter(object sender, EventArgs e)
        {
            History.Add(EventData.CreateEventData((Control) sender, EventType.Focused, e));
        }
    }
}
