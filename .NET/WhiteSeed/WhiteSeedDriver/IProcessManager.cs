﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSeedDriver
{
    public interface IProcessManager
    {
        Process[] GetProcessesByNameAndSession(string appPath);
    }
}
