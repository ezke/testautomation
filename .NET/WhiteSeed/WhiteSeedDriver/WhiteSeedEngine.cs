﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Automation;
using Castle.Core.Internal;
using TestStack.White;
using TestStack.White.Configuration;
using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.TableItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;
using WhiteSeed;
using Application = TestStack.White.Application;
using Image = System.Drawing.Image;
using SendKeys = System.Windows.Forms.SendKeys;

namespace WhiteSeedDriver
{
    public class WhiteSeedEngine
    {
        /// <summary>
        /// Default amount of time to wait for an element to appear on the screen. This is fairly short, on the
        /// assumption that if you thought it might not be there right away, you would have done an explicit wait
        /// beforehand.
        /// </summary>
        private IProcessManager _processManager;
        private static readonly TimeSpan DefaultElementTimeout = TimeSpan.FromSeconds(2);

        private static IdentityList<ApplicationData> _applications = new IdentityList<ApplicationData>();
        
        private static Dictionary<FormElementAttributes, Func<UIItem, string>> _getAttributeTable =
            new Dictionary<FormElementAttributes, Func<UIItem, string>>()
            {
                {FormElementAttributes.Text, element => element is TextBox?((TextBox)element).Text:element.Name},
                {FormElementAttributes.AutomationID, element => element.PrimaryIdentification},
                {FormElementAttributes.Bounds, element => BoundsToString(element.Bounds)}
            };

        private static Dictionary<FormElementAttributes, Func<TableCell, string>> _getCellAttributeTable =
            new Dictionary<FormElementAttributes, Func<TableCell, string>>()
            {
                {FormElementAttributes.Text, element => element.Value.ToString()},
                {FormElementAttributes.AutomationID, element => element.PrimaryIdentification},
                {FormElementAttributes.Bounds, element => BoundsToString(element.Bounds)}
            };

        private static Dictionary<FormElementActions, Action<IUIItem>> _getActionTable =
            new Dictionary<FormElementActions, Action<IUIItem>>()
            {
                {FormElementActions.Click, element => element.Click()}
            };

        private static string BoundsToString(System.Windows.Rect bounds)
        {
            return $"{bounds.Left}, {bounds.Top}, {bounds.Width}, {bounds.Height}";
        }

        private static SearchCriteria FilterToSearchCriteria(FormElementFilter filter)
        {
            var searchValue = filter.Value;

            switch (filter.Field)
            {
                case FormElementAttributes.AutomationID:
                    return SearchCriteria.ByAutomationId(searchValue);
                case FormElementAttributes.Text:
                    return SearchCriteria.ByText(searchValue);
                case FormElementAttributes.Type:
                    var controlType = ControlTable.GetControlType(searchValue.ToLower());
                    return SearchCriteria.ByControlType(controlType);
                case FormElementAttributes.WindowHandle:
                    var windowHandleProperty = AutomationElementIdentifiers.NativeWindowHandleProperty;
                    return SearchCriteria.ByNativeProperty(windowHandleProperty, ParsePossiblyHexInt(filter.Value));
                case FormElementAttributes.Enabled:
                    return SearchCriteria.ByNativeProperty(AutomationElementIdentifiers.IsEnabledProperty, StringToBoolean(filter.Value));
                default:
                    throw new InvalidOperationException("Unexpected filter field: " + filter.Field);
            }
        }

        private static bool StringToBoolean(string value)
        {
            var trueValues = new string[]
            {
                "true",
                "yes",
                "1"
            };
            return trueValues.Any(item => item.Equals(value, StringComparison.InvariantCultureIgnoreCase));
        }

        private static int ParsePossiblyHexInt(string value)
        {
            // Use Int32Converter because it supports "0x..." hex numbers -
            // handy for copying out of the Inspect app for testing
            var convertedValue = new Int32Converter().ConvertFromString(value);
            if (convertedValue == null)
                throw new InvalidOperationException($"'{value}' is not a valid integer");
            return (int)convertedValue;
        }

        private static SearchCriteria FiltersToSearchCriteria(FormElementFilter[] filters)
        {
            return filters.Aggregate(SearchCriteria.All, (total, next) => total.Merge(FilterToSearchCriteria(next)));
        }

        public WhiteSeedEngine(IProcessManager processManager)
        {
            CoreAppXmlConfiguration.Instance.LoggerFactory = new FilteredConsoleLoggerFactory();
            _processManager = processManager;
        }

        public void ElementAction(int applicationId, FormElementFilter[] filters, FormElementActions action)
        {
            var element = (UIItem)FindElement(applicationId, filters);
            LogMessage($"Element: {element.Id}");
            switch (action)
            {
                case FormElementActions.Click:
                    LogMessage($"Clicking element [{element.Name}]");
                    element.Click();
                    break;

                case FormElementActions.Focus:
                    LogMessage($"Focusing element [{element.Name}]");
                    element.Focus();
                    break;

                default:
                    break;
            }
        }

        public void ElementType(int applicationId, FormElementFilter[] filter, string text)
        {
            var element = (UIItem)FindElement(applicationId, filter);
            LogMessage($"Typing into [{element.Id}] \"{text}\"");
            element.Enter(text);
        }

        public void ElementClear(int applicationId, FormElementFilter[] filter)
        {
            var element = (UIItem)FindElement(applicationId, filter);
            LogMessage($"Clearing text in [{element.Id}]");

            element.Focus();
            SendKeys.SendWait("{HOME}+{END}{DEL}");
        }

        public void ElementTypeWithSpecialKeys(int applicationId, FormElementFilter[] filter, string keys)
        {
            var element = (UIItem)FindElement(applicationId, filter);

            element.Focus();
            LogMessage($"Typing using special keys into [{element.Id}] \"{keys}\"");
            TypeOnKeyboardWithSpecialKeys(applicationId, keys);
        }

        public void WaitForElement(int applicationId, FormElementFilter[] filters, int timeoutInSeconds)
        {
            FindElement(applicationId, filters, TimeSpan.FromSeconds(timeoutInSeconds));
        }

        public string QueryElement(int applicationId, FormElementFilter[] filters, FormElementAttributes attribute)
        {
            var element = (UIItem)FindElement(applicationId, filters);
            LogMessage($"Element: {element.Id}");
            return _getAttributeTable[attribute](element);
        }

        private IUIItem FindElement(int applicationId, FormElementFilter[] filters, TimeSpan? timeout = null)
        {
            filters.ForEach(filter =>
                LogMessage($"Searching For [{Enum.GetName(typeof(FormElementAttributes), filter.Field)}]: \"{filter.Value}\"")
            );

            var currentWindow = _applications[applicationId].CurrentWindow;
            var searchCriteria = FiltersToSearchCriteria(filters);
            var actualTimeout = timeout ?? DefaultElementTimeout;
            return currentWindow.Get(searchCriteria, actualTimeout);
        }

        public void TypeOnKeyboard(int applicationId, string text)
        {
            var window = _applications[applicationId].CurrentWindow;
            LogMessage($"Typing \"{text}\"");
            window.Keyboard.Enter(text);
        }

        public void TypeOnKeyboardWithSpecialKeys(int applicationId, string keys)
        {
            var window = _applications[applicationId].CurrentWindow;
            LogMessage($"Typing with special keys \"{keys}\"");
            KeyboardTypingParser.TypeSpecialKeys(keys, window.Keyboard);
        }

        public void CloseApplication(int applicationId)
        {
            var window = _applications[applicationId].CurrentWindow;
            if (!window.IsClosed)
            {
                window.Close();
            }
            _applications[applicationId].Application.Close();
        }

        public byte[] GetScreenShot(int applicationId)
        {
            return ImageToByteArray(_applications[applicationId].CurrentWindow.VisibleImage);
        }

        private static byte[] ImageToByteArray(Image image)
        {
            var converter = new ImageConverter();
            return (byte[])converter.ConvertTo(image, typeof(byte[]));
        }

        public string Ping()
        {
            return "Ping";
        }

        public string QueryTableCell(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex,
            FormElementAttributes attribute)
        {
            var cell = GetCell(applicationId, filter, columnName, rowIndex);

            return _getCellAttributeTable[attribute](cell);
        }

        public void TableCellAction(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex,
            FormElementActions action)
        {
            var cell = GetCell(applicationId, filter, columnName, rowIndex);

            switch (action)
            {
                case FormElementActions.Click:
                    cell.Click();
                    break;

                default:
                    break;
            }
        }

        public void TableCellType(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex, string text)
        {
            var cell = GetCell(applicationId, filter, columnName, rowIndex);

            cell.Click();
            cell.Enter(text);
        }

        private TableCell GetCell(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex)
        {
            var grid = (Table)FindElement(applicationId, filter);
            return grid.Rows[rowIndex].Cells[columnName];
        }

        public Application LaunchApplication(string appPath, params string[] args)
        {
            var allArgs = string.Join(" ", args);
            LogMessage($"Launching \"{appPath}\" {allArgs}");

            var processInfo = new ProcessStartInfo(appPath, allArgs);
            return Application.Launch(processInfo);
        }

        public int LaunchAndAttachApplication(string appPath, string[] args, TimeSpan timeToWaitForInitialWindow)
        {
            var application = LaunchApplication(appPath, args);
            var initialWindow = GetInitialWindow(application, timeToWaitForInitialWindow);
            return _applications.Add(new ApplicationData(application, initialWindow));
        }

        public int AttachApplication(string appPath,
            TimeSpan timeToWaitForProcessStart, TimeSpan timeToWaitForInitialWindow)
        {
            LogMessage($"Attaching {appPath}");
            var timeoutStopwatch = Stopwatch.StartNew();
            try
            {
                var application = AttachApplicationInternal(appPath,
                    () => timeoutStopwatch.Elapsed >= timeToWaitForProcessStart,
                    TimeSpan.FromSeconds(0.5));

                var initialWindow = GetInitialWindow(application, timeToWaitForInitialWindow);
                var applicationId = _applications.Add(new ApplicationData(application, initialWindow));
                LogMessage($"Attached successfully: {appPath}");
                return applicationId;
            }
            catch (Exception ex)
            {
                LogMessage($"Failed to attach: {ex.Message}");
                throw;
            }
        }

        public Application AttachApplicationInternal(string appPath, Func<bool> isTimedout, TimeSpan pollingSpan)
        {
            var processes = RetryUntil(
                () =>
                {
                    var foundProcesses = _processManager.GetProcessesByNameAndSession(appPath);
                    return foundProcesses.Any() ? foundProcesses : null;
                },
                () => isTimedout() ? new WhiteException("Could not find process named " + appPath) : null,
                pollingSpan);

            return Application.Attach(processes[0]);
        }

        private T RetryUntil<T>(Func<T> loopedProcess, Func<Exception> untilCondition, TimeSpan delayBetweenCycles)
        {
            while (true)
            {
                var result = loopedProcess();
                if (result != null)
                {
                    return result;
                }

                var exception = untilCondition();
                if (exception != null)
                {
                    throw exception;
                }
                Thread.Sleep(delayBetweenCycles);
            }
        }

        public FormTableMetaData QueryTableMetaData(int applicationId, FormElementFilter[] filters)
        {
            var table = (Table)FindElement(applicationId, filters);
            var result = new FormTableMetaData()
            {
                Columns = table.Header.Columns.Select(column => column.Name).ToArray(),
                RowCount = table.Rows.Count
            };

            return result;
        }

        public string[] ListWindows(int applicationId)
        {
            return _applications[applicationId].Application.GetWindows().Select(window => window.Title).ToArray();
        }

        public void SetFirstWidowToCurrent(int applicationId)
        {
            Window newWindow = null;

            try
            {
                newWindow = _applications[applicationId].Application.GetWindows().First();
            }
            catch (Exception)
            {
                LogMessage($"** Error:  Couldn't find the window");
            }
            if (newWindow != null)
            {
                _applications[applicationId].CurrentWindow = newWindow;
            }
        }

        public void SetCurrentWindow(int applicationId, FormElementFilter[] filter)
        {
            Window newWindow = null;

            try
            {
                newWindow = _applications[applicationId].Application.GetWindow(FiltersToSearchCriteria(filter),
                    InitializeOption.NoCache);
            }
            catch (Exception ex)
            {
                LogMessage($"** Error:  {ex.Message}");
            }
            if (newWindow == null)
            {
                LogMessage($"** Error:  Couldn't find the window");
            }
            else
            {
                _applications[applicationId].CurrentWindow = newWindow;
            }
        }

        public ElementInfo[] GetElementInfo(int applicationId, FormElementFilter[] filters)
        {
            var window = _applications[applicationId].CurrentWindow;
            var searchCriteria = FiltersToSearchCriteria(filters);
            var uiItems = window.GetMultiple(searchCriteria);
            return uiItems.Select(UiItemToElementInfo).ToArray();
        }

        private ElementInfo UiItemToElementInfo(IUIItem uiItem)
        {
            return new ElementInfo
            {
                CheckState = GetCheckState(uiItem),
                Enabled = uiItem.Enabled,
                Id = uiItem.Id,
                IsOffscreen = uiItem.IsOffScreen,
                Name = uiItem.Name,
                UiAutomationClassName = uiItem.AutomationElement.Current.ClassName,
                Visible = uiItem.Visible,
                WhiteClassName = uiItem.GetType().Name,
                WindowHandle = uiItem.AutomationElement.Current.NativeWindowHandle,
                ScreenX = uiItem.Bounds.Left,
                ScreenY = uiItem.Bounds.Top,
                Width = uiItem.Bounds.Width,
                Height = uiItem.Bounds.Height,
            };
        }

        private CheckState GetCheckState(IUIItem uiItem)
        {
            if (uiItem is RadioButton)
            {
                return ((RadioButton)uiItem).IsSelected ? CheckState.Checked : CheckState.Unchecked;
            }

            if (uiItem is CheckBox)
            {
                switch (((CheckBox)uiItem).State)
                {
                    case ToggleState.On:
                        return CheckState.Checked;
                    case ToggleState.Indeterminate:
                        return CheckState.Indeterminate;
                    default:
                        return CheckState.Unchecked;
                }
            }

            return CheckState.Unchecked;
        }

        private Window GetInitialWindow(Application application, TimeSpan timeout)
        {
            var timeoutStopwatch = Stopwatch.StartNew();
            return RetryUntil(
                () =>
                {
                    if (application.Process.MainWindowHandle == IntPtr.Zero)
                    {
                        LogMessage("Waiting for process to have a main window...");
                        return null;
                    }

                    application.WaitWhileBusy();

                    var windows = application.GetWindows();
                    if (!windows.Any())
                    {
                        LogMessage("Waiting for application to have windows...");
                        return null;
                    }

                    var window = windows[0];
                    window.WaitWhileBusy();
                    return window;
                },
                () => timeoutStopwatch.Elapsed >= timeout
                    ? new WhiteException("Failed to find the main window for this application.")
                    : null,
                TimeSpan.FromSeconds(0.5));
        }

        private void LogMessage(string message)
        {
            Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss tt")}   {message}");
        }
    }
}
