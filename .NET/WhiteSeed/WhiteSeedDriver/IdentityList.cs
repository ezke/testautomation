﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSeedDriver
{
    public class IdentityList<TValue> : IEnumerable<TValue>
    {
        private Queue<int> _unusedIndexes = new Queue<int>();
        private int _maxUsedIndex = 0;
        private Dictionary<int, TValue> _storage = new Dictionary<int, TValue>();
        private int _position = -1;

        public int Add(TValue value)
        {
            int result = GetNextOpenIndex();

            _storage.Add(result, value);
            return result;
        }

        public object Current => _storage[_storage.Keys.ToArray()[_position]];

        public void Remove(int index)
        {
            _storage.Remove(index);
            _unusedIndexes.Enqueue(index);
        }

        public TValue this[int index] => _storage[index];

        private int GetNextOpenIndex()
        {
            var result = _maxUsedIndex;
            if (_unusedIndexes.Count == 0)
            {
                _maxUsedIndex++;
            }
            else
            {
                result = _unusedIndexes.Dequeue();
            }
            return result;
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return _storage.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
