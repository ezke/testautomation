﻿using System;
using System.Linq;
using Castle.Core.Logging;

namespace WhiteSeedDriver
{
    internal class FilterdConsoleLogger : LevelFilteredLogger
    {
        private Func<LoggerLevel, string, string, Exception, bool>[] _filteredOut = new Func<LoggerLevel, string, string, Exception, bool>[]
        {
            (logLevel, loggerName, message, exception)=>
                logLevel==LoggerLevel.Warn 
                && loggerName.Equals("TestStack.White.Factory.WindowFactory", StringComparison.InvariantCultureIgnoreCase)
        };

        public FilterdConsoleLogger(string name):base(name)
        {}

        public override ILogger CreateChildLogger(string loggerName)
        {
            if (loggerName == null)
            {
                throw new ArgumentNullException("loggerName", "To create a child logger you must supply a non null name");
            }
            return new ConsoleLogger($"{Name}.{loggerName}", Level);
        }

        private bool IsFilteredOut(LoggerLevel loggerLevel, string loggerName, string message, Exception exception)
        {
            return _filteredOut.Any(filter => filter(loggerLevel, loggerName, message, exception));
        }

        protected override void Log(LoggerLevel loggerLevel, string loggerName, string message, Exception exception)
        {
            if (IsFilteredOut(loggerLevel, loggerName, message, exception))
                return;
            if (exception == null)
            {
                Console.WriteLine($"[{loggerLevel} - {DateTime.Now.ToLongTimeString()}] '{loggerName}' {message}");
            }
            else
            {
                Console.WriteLine($"[{loggerLevel} - {DateTime.Now.ToLongTimeString()}] '{loggerName}' {exception.GetType().FullName}: {exception.Message} {exception.StackTrace}");
            }
        }
    }
}