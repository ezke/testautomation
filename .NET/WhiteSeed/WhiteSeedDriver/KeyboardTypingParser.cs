﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Castle.Core.Internal;
using TestStack.White.InputDevices;
using TestStack.White.WindowsAPI;

namespace WhiteSeedDriver
{
    public class KeyboardTypingParser
    {
        private const string MASTER_ELEMENT_NAME = "ReallyLongTypedTextMasterElement";

        private static readonly Dictionary<string, string> KEY_ALIASES = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"ENTER", "RETURN" },
            {"DEL", "DELETE" },
            {"CTL", "CONTROL" },
            {"CTRL", "CONTROL" },
            {"INS", "INSERT" },
            {"BS", "BACKSPACE" },
            {"ESC", "ESCAPE" },
            {"WIN", "LWIN" },

        };

        public static void TypeSpecialKeys(string typedText, IKeyboard keyboard)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(WrapTypedTextInMasterElement(typedText))))
            {
                while (reader.Read())
                {
                    if (!reader.Name.Equals(MASTER_ELEMENT_NAME))
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (reader.IsEmptyElement)
                                {
                                    keyboard.PressSpecialKey(SpecialKeyFromString(reader.Name));
                                }
                                else
                                {
                                    keyboard.HoldKey(SpecialKeyFromString(reader.Name));
                                }
                                break;

                            case XmlNodeType.Text:
                                keyboard.Enter(reader.Value);
                                break;

                            case XmlNodeType.EndElement:
                                keyboard.LeaveKey(SpecialKeyFromString(reader.Name));
                                break;
                        }
                    }
                }
            }
        }

        private static string WrapTypedTextInMasterElement(string typedText)
        {
            return $"<{MASTER_ELEMENT_NAME}>{typedText}</{MASTER_ELEMENT_NAME}>";
        }

        private static string TranslateKeyName(string key)
        {
            if (KEY_ALIASES.ContainsKey(key))
            {
                return KEY_ALIASES[key];
            }
            return key;
        }

        private static KeyboardInput.SpecialKeys SpecialKeyFromString(string key)
        {
            return (KeyboardInput.SpecialKeys) Enum.Parse(typeof (KeyboardInput.SpecialKeys), TranslateKeyName(key), ignoreCase: true);
        }
    }
}
