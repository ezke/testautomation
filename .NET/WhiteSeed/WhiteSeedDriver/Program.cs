﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WhiteSeedDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            var url = ConfigurationManager.AppSettings["HostURL"];
            ServiceHost host=null;

            try
            {
                host = new ServiceHost(typeof(WhiteSeedService));
                host.AddServiceEndpoint(GetContractInterface(typeof(WhiteSeedService)), GetBinding(), url);
                host.Description.Behaviors.Add(new ServiceMetadataBehavior {HttpGetEnabled = true, HttpGetUrl = new Uri(url)});
                host.Open();
                Console.WriteLine($"White Seed Planted [{url}]");
                Console.WriteLine("  Press <Enter> to stop the service.");
                Console.ReadLine();
                host.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}   {ex.Message}");
            }
            host?.Close();
        }

        private static BasicHttpBinding GetBinding()
        {
            BasicHttpBinding bhbBinding = new BasicHttpBinding();
            bhbBinding.MaxReceivedMessageSize = 2147483647;
            bhbBinding.MessageEncoding = WSMessageEncoding.Mtom;
            bhbBinding.TransferMode = TransferMode.Streamed;
            bhbBinding.ReaderQuotas.MaxArrayLength = 2147483647;
            bhbBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            bhbBinding.ReaderQuotas.MaxDepth = 2147483647;
            bhbBinding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
            bhbBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
            bhbBinding.Security.Mode = BasicHttpSecurityMode.None;
            return bhbBinding;
        }

        private static Type GetContractInterface(Type typHostedType)
        {
            Type typAns = null;

            foreach (Type typCurInterface in typHostedType.GetInterfaces())
            {
                if (typCurInterface.GetCustomAttributes(typeof(System.ServiceModel.ServiceContractAttribute), true).Count() > 0)
                {
                    typAns = typCurInterface;
                    break;
                }
            }
            return typAns;
        }
    }
}
