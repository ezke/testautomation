﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;

namespace WhiteSeedDriver
{
    public class ApplicationData
    {
        public ApplicationData(Application application, Window currentWindow)
        {
            Application = application;
            CurrentWindow = currentWindow;
        }


        public Application Application { get; }

        public Window CurrentWindow { get; set; }
    }
}
