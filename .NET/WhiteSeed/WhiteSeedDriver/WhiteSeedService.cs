﻿using System;
using System.ServiceModel;
using WhiteSeed;

namespace WhiteSeedDriver
{
    public class WhiteSeedService : IRoots
    {
        private static WhiteSeedEngine _engine = new WhiteSeedEngine(new SystemProcessManager());

        private static void GeneralErrorHandler(Action methodToRun)
        {
            try
            {
                methodToRun();
            }
            catch (Exception ex)
            {
                throw new FaultException<UnknownFault>(new UnknownFault() {Message = ex.Message}, ex.Message);
            }
        }

        private static T GeneralErrorHandler<T>(Func<T> functionToRun)
        {
            try
            {
                return functionToRun();
            }
            catch (Exception ex)
            {
                throw new FaultException<UnknownFault>(new UnknownFault() {Message = ex.Message}, ex.Message);
            }
        }

        public void ElementAction(int applicationId, FormElementFilter[] filters, FormElementActions action)
        {
            GeneralErrorHandler(()=>_engine.ElementAction(applicationId, filters, action));
        }

        public void ElementType(int applicationId, FormElementFilter[] filter, string text)
        {
            GeneralErrorHandler(()=>_engine.ElementType(applicationId,filter, text));
        }

        public void ElementClear(int applicationId, FormElementFilter[] filter)
        {
            GeneralErrorHandler(() => _engine.ElementClear(applicationId, filter));
        }

        public void ElementTypeWithSpecialKeys(int applicationId, FormElementFilter[] filter, string keys)
        {
            GeneralErrorHandler(()=>_engine.ElementTypeWithSpecialKeys(applicationId, filter, keys));
        }

        public void WaitForElement(int applicationId, FormElementFilter[] filters, int timeoutInSeconds)
        {
            GeneralErrorHandler(() => _engine.WaitForElement(applicationId, filters, timeoutInSeconds));
        }

        public string QueryElement(int applicationId, FormElementFilter[] filters, FormElementAttributes attribute)
        {
            return GeneralErrorHandler(()=>_engine.QueryElement(applicationId, filters, attribute));
        }

        public void TypeOnKeyboard(int applicationId, string text)
        {
            GeneralErrorHandler(()=>_engine.TypeOnKeyboard(applicationId, text));
        }

        public void TypeOnKeyboardWithSpecialKeys(int applicationId, string keys)
        {
            GeneralErrorHandler(()=>_engine.TypeOnKeyboardWithSpecialKeys(applicationId, keys));
        }

        public void CloseApplication(int applicationId)
        {
            GeneralErrorHandler(()=>_engine.CloseApplication(applicationId));
        }

        public byte[] GetScreenShot(int applicationId)
        {
            return GeneralErrorHandler(() => _engine.GetScreenShot(applicationId));
        }

        public ElementInfo[] GetElementInfo(int applicationId, FormElementFilter[] filters)
        {
            return GeneralErrorHandler(() => _engine.GetElementInfo(applicationId, filters));
        }

        public string Ping()
        {
            return GeneralErrorHandler(() => _engine.Ping());
        }

        public string QueryTableCell(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex,
            FormElementAttributes attribute)
        {
            return
                GeneralErrorHandler(() => _engine.QueryTableCell(applicationId, filter, columnName, rowIndex, attribute));
        }

        public void TableCellAction(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex,
            FormElementActions action)
        {
           GeneralErrorHandler(()=>_engine.TableCellAction(applicationId, filter, columnName, rowIndex, action));
        }

        public void TableCellType(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex, string text)
        {
            GeneralErrorHandler(()=>_engine.TableCellType(applicationId, filter, columnName, rowIndex, text));
        }

        public void LaunchApplication(string appPath, params string[] args)
        {
            GeneralErrorHandler(() => _engine.LaunchApplication(appPath, args));
        }

        public int LaunchAndAttachApplication(string appPath, params string[] args)
        {
            var timeToWaitForInitialWindow = TimeSpan.FromSeconds(30);
            return GeneralErrorHandler(() =>
                _engine.LaunchAndAttachApplication(appPath, args, timeToWaitForInitialWindow));
        }

        public int AttachApplication(string appPath, double timeoutInSeconds)
        {
            var timeToWaitForInitialWindow = TimeSpan.FromSeconds(30);
            return GeneralErrorHandler(() =>
                _engine.AttachApplication(appPath, TimeSpan.FromSeconds(timeoutInSeconds), timeToWaitForInitialWindow));
        }

        public FormTableMetaData QueryTableMetaData(int applicationId, FormElementFilter[] filters)
        {
            return GeneralErrorHandler(() => _engine.QueryTableMetaData(applicationId, filters));
        }

        public string[] ListWindows(int applicationId)
        {
            return GeneralErrorHandler(() => _engine.ListWindows(applicationId));
        }

        public void SetFirstWidowToCurrent(int applicationId)
        {
            GeneralErrorHandler(() => _engine.SetFirstWidowToCurrent(applicationId));
        }

        public void SetCurrentWindow(int applicationId, FormElementFilter[] filter)
        {
            GeneralErrorHandler(()=>_engine.SetCurrentWindow(applicationId, filter));
        }
    }
}
