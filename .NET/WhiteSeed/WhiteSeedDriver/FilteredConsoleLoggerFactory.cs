﻿using System;
using Castle.Core.Logging;

namespace WhiteSeedDriver
{
    internal class FilteredConsoleLoggerFactory : ILoggerFactory
    {
        private const LoggerLevel DEFAULT_LOGGER_LEVEL = LoggerLevel.Info;
        public ILogger Create(Type type)
        {
            return Create(type.FullName, DEFAULT_LOGGER_LEVEL);
        }

        public ILogger Create(string name)
        {
            return Create(name, DEFAULT_LOGGER_LEVEL);
        }

        public ILogger Create(string name, LoggerLevel level)
        {
            return new FilterdConsoleLogger(name)
            {
                Level = level
            };
        }

        public ILogger Create(Type type, LoggerLevel level)
        {
            return Create(type.FullName, level);
        }
    }
}