﻿using System.Diagnostics;
using System.Linq;

namespace WhiteSeedDriver
{
    internal class SystemProcessManager : IProcessManager
    {
        public Process[] GetProcessesByNameAndSession(string appPath)
        {
            return Process.GetProcessesByName(appPath)
                .Where(process=>process.SessionId== Process.GetCurrentProcess().SessionId)
                .ToArray();
        }
    }
}