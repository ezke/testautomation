using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract(Name = "ElementAttributes")]
    public enum FormElementAttributes
    {
        [EnumMember]
        Text,
        [EnumMember]
        AutomationID,
        [EnumMember]
        Bounds,
        [EnumMember]
        Type,
        [EnumMember]
        Value,
        [EnumMember]
        WindowHandle,
        [EnumMember]
        Enabled
    }
}