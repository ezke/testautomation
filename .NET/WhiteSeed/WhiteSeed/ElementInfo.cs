﻿using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public class ElementInfo
    {
        [DataMember] public CheckState CheckState { get; set; }
        [DataMember] public bool Enabled { get; set; }
        [DataMember] public string Id { get; set; }
        [DataMember] public bool IsOffscreen { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string UiAutomationClassName { get; set; }
        [DataMember] public bool Visible { get; set; }
        [DataMember] public string WhiteClassName { get; set; }
        [DataMember] public long WindowHandle { get; set; }
        [DataMember] public double ScreenX { get; set; }
        [DataMember] public double ScreenY { get; set; }
        [DataMember] public double Width { get; set; }
        [DataMember] public double Height { get; set; }
    }
}