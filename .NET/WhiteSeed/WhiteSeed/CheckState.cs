﻿using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public enum CheckState
    {
        [EnumMember] Unchecked,
        [EnumMember] Checked,
        [EnumMember] Indeterminate,
    }
}