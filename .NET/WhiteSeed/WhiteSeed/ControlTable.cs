﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace WhiteSeed
{
    public class ControlTable
    {
        private static Dictionary<string, ControlType> _controlTypesTable = 
            new Dictionary<string, ControlType>()
            {
                {"button", ControlType.Button},
                {"calendar", ControlType.Calendar},
                {"checkbox", ControlType.CheckBox},
                {"combobox", ControlType.ComboBox},
                {"custom", ControlType.Custom},
                {"datagrid", ControlType.DataGrid},
                {"dataitem", ControlType.DataItem},
                {"document", ControlType.Document},
                {"edit", ControlType.Edit},
                {"group", ControlType.Group},
                {"header", ControlType.Header},
                {"headeritem", ControlType.HeaderItem},
                {"hyperlink", ControlType.Hyperlink},
                {"image", ControlType.Image},
                {"list", ControlType.List},
                {"listitem", ControlType.ListItem},
                {"menu", ControlType.Menu},
                {"menubar", ControlType.MenuBar},
                {"menuitem", ControlType.MenuItem},
                {"pane", ControlType.Pane},
                {"progressbar", ControlType.ProgressBar},
                {"radiobutton", ControlType.RadioButton},
                {"scrollbar", ControlType.ScrollBar},
                {"separator", ControlType.Separator},
                {"slider", ControlType.Slider},
                {"spinner", ControlType.Spinner},
                {"splitbutton", ControlType.SplitButton},
                {"statusbar", ControlType.StatusBar},
                {"tab", ControlType.Tab},
                {"tabitem", ControlType.TabItem},
                {"table", ControlType.Table},
                {"text", ControlType.Text},
                {"thumb", ControlType.Thumb},
                {"titlebar", ControlType.TitleBar},
                {"toolbar", ControlType.ToolBar},
                {"tooltip", ControlType.ToolTip},
                {"tree", ControlType.Tree},
                {"treeitem", ControlType.TreeItem},
                {"window", ControlType.Window}
            };

        public static ControlType GetControlType(string typeName)
        {
            return _controlTypesTable[typeName];
        }
    }
}
