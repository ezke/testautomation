﻿using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public class FormTableMetaData
    {
        [DataMember]
        public string[] Columns { get; set; }
        [DataMember]
        public int RowCount { get; set; }
    }
}