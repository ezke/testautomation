﻿using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public class FormElementFilter
    {
        [DataMember]
        public FormElementAttributes Field { get; set; }
        [DataMember]
        public string Value { get; set; }

        public static FormElementFilter CreateFilter(FormElementAttributes field, string value)
        {
            return new FormElementFilter()
            {
                Field = field,
                Value = value
            };
        }
    }
}