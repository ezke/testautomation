using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract(Name = "ElementActions")]
    public enum FormElementActions
    {
        [EnumMember]
        Click,
        [EnumMember]
        Focus
    }
}