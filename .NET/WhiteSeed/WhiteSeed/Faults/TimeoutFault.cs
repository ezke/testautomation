using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public class TimeoutFault
    {
        [DataMember]
        public string Message { get; set; }
    }
}