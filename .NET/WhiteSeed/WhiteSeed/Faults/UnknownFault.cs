using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public class UnknownFault
    {
        [DataMember]
        public string Message { get; set; }
    }
}