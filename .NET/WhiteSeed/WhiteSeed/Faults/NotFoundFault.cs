using System.Runtime.Serialization;

namespace WhiteSeed
{
    [DataContract]
    public class NotFoundFault
    {
        [DataMember]
        public string Message { get; set; }
    }
}