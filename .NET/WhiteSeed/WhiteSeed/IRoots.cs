﻿using System;
using System.ServiceModel;

namespace WhiteSeed
{
    [ServiceContract(Namespace = "http://localhost:9995/Seed")]
    public interface IRoots
    {
        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void ElementAction(int applicationId, FormElementFilter[] filter, FormElementActions action);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void ElementType(int applicationId, FormElementFilter[] filter, String text);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void ElementClear(int applicationId, FormElementFilter[] filter);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void ElementTypeWithSpecialKeys(int applicationId, FormElementFilter[] filter, String keys);

        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void WaitForElement(int applicationId, FormElementFilter[] filters, int timeoutInSeconds);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        string QueryElement(int applicationId, FormElementFilter[] filter, FormElementAttributes attribute);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        FormTableMetaData QueryTableMetaData(int applicationId, FormElementFilter[] filter);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        string QueryTableCell(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex,
            FormElementAttributes attribute);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void TableCellAction(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex,
            FormElementActions action);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void TableCellType(int applicationId, FormElementFilter[] filter, string columnName, int rowIndex, String text);

        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void LaunchApplication(string appPath, params string[] args);

        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        int LaunchAndAttachApplication(string appPath, params string[] args);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        int AttachApplication(string appPath, double timeoutInSeconds);

        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void TypeOnKeyboard(int applicationId, String text);

        [FaultContract(typeof(TimeoutFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void TypeOnKeyboardWithSpecialKeys(int applicationId, string keys);

        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void CloseApplication(int applicationId);

        [FaultContract(typeof (UnknownFault))]
        [OperationContract]
        void SetFirstWidowToCurrent(int applicationId);

        [FaultContract(typeof(NotFoundFault))]
        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        void SetCurrentWindow(int applicationId, FormElementFilter[] filter);

        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        byte[] GetScreenShot(int applicationId);

        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        string[] ListWindows(int applicationId);

        [FaultContract(typeof(UnknownFault))]
        [OperationContract]
        ElementInfo[] GetElementInfo(int applicationId, FormElementFilter[] filters);

        [OperationContract]
        string Ping();
    }
}
