﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace CodeScanner
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 1)
            {
                Console.WriteLine("Code Scan\n");
                Console.WriteLine("Scanning...");
                foreach (var file in GetAllFilesInDirectory(new DirectoryInfo(args[0]), args.Skip(1).ToArray()))
                {
                    ScanFile(file);
                }
            }
            else
            {
                Console.Error.WriteLine(Properties.Resources.usage);
            }
        }

        private static void ScanFile(string file)
        {
            var data = GetBadCharacterBlocks(File.ReadAllBytes(file));
            if (data.Any(badByteBlock => badByteBlock.Count > 0))
            {
                var blocks = data.Select(BytesToHex);
                Console.WriteLine(file);
                Console.WriteLine($"    {string.Join("...", blocks)}");
            }
        }

        static string[] GetAllFilesInDirectory(DirectoryInfo root, params string[] patterns)
        {
            var result = new List<string>();

            foreach (var pattern in patterns)
            {
                result.AddRange(root.GetFiles(pattern).Select(file => file.FullName));
            }
            foreach (var directoryInfo in root.GetDirectories())
            {
                result.AddRange(GetAllFilesInDirectory(directoryInfo, patterns));
            }

            return result.ToArray();
        }

        static List<List<byte>> GetBadCharacterBlocks(byte[] data)
        {
            var inBadGroup = true;
            var result = new List<List<byte>>() {new List<byte>()};
            foreach (var currentChar in data)
            {
                if (currentChar < 128)
                {
                    if (inBadGroup)
                    {
                        result.Add(new List<byte>());
                    }
                    inBadGroup = false;
                }
                else
                {
                    result.Last().Add(currentChar);
                    inBadGroup = true;
                }
            }
            return result;
        }

        public static string BytesToHex(List<byte> values)
        {
            return string.Join(" ", values.Select(item => item.ToString("X2")));
        }
    }
}
